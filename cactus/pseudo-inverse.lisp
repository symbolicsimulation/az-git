;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;;=======================================================

(defmethod pseudo-inverse  ((t0 Identity-Map)) t0)
(defmethod pseudo-inverse! ((t0 Identity-Map)) t0)

;;;=======================================================

(defmethod pseudo-inverse ((t0 Block-Projection)) (transpose t0))
(defmethod pseudo-inverse ((t0 Block-Embedding))  (transpose t0))

(defmethod pseudo-inverse! ((t0 Block-Projection)) (transpose! t0))
(defmethod pseudo-inverse! ((t0 Block-Embedding))  (transpose! t0))

;;;=======================================================
;;;
;;; Even though these are destructive versions of pseudo-inverse, it's
;;; important to use non-destructive decompositions, eg.
;;; inverse-lu-decompose rather than inverse-lu-decompose!, so
;;; that the source slot is filled correctly. pseudo-inverse!  is only
;;; destructive in the sense that t0 is put in the source slot rather than
;;; a copy of t0.

(defmethod pseudo-inverse! ((t0 Matrix-Super))
  (cond
    ((diagonal? t0)
     (let ((2d (2d-array t0)) x)
	(az:for (i (coord-start (domain t0)) (coord-end (domain t0)))
	 (setf x (aref 2d i i))
	 (unless (zerop x) (setf (aref 2d i i) (/ x))))))
    ((positive-definite? t0)
     (pseudo-inverse (cholesky-decompose t0)))
    ((upper-triangular? t0)
     (cond
       ((embedding? t0)
	(make-instance 'Inverse-Upper-Triangular-Matrix :source t0))
       ((projecting? t0)
	(inverse-lq-decompose t0))
       (t (error "shouldn't ever get here."))))
    ((lower-triangular? t0)
     (cond
       ((projecting? t0)
	(make-instance 'Inverse-Lower-Triangular-Matrix :source t0))
       ((embedding? t0)
	(inverse-qr-decompose t0))
       (t (error "shouldn't ever get here."))))
    (t
     (cond
       ((automorphic? t0) (inverse-lu-decompose t0))
       ((strictly-embedding? t0)  (inverse-qr-decompose t0))
       ((strictly-projecting? t0) (inverse-lq-decompose t0))
       (t (error "shouldn't ever get here."))))))

;;;-------------------------------------------------------

(defmethod pseudo-inverse! ((t0 Householder)) t0)

(defmethod pseudo-inverse! ((t0 Pivot)) t0)

;;;-------------------------------------------------------

(defmethod pseudo-inverse! ((t0 1d-Annihilator)) t0)

;;;-------------------------------------------------------
#||latex

 The pseudo-inverse of the Gauss Mapping, $I - \alpha \otimes e_k$,
 is simply $I - \alpha \otimes e_k$.

||#

(defmethod pseudo-inverse! ((t0 Gauss))
  (minus! (vec t0))
  t0)

;;;-------------------------------------------------------

(defmethod pseudo-inverse! ((t0 Diagonal-Vector))
  (let ((v (1d-array (vec t0)))
	x)
    (az:for (i (coord-start (domain t0)) (coord-end (domain t0)))
      (setf x (aref v i))
      (unless (zerop x)	(setf (aref v i) (/ x)))))
  t0)

;;;-------------------------------------------------------

(defmethod pseudo-inverse ((t0 Symmetric-Inner-Product))
  (make-instance 'Symmetric-Outer-Product
		 :left (pseudo-inverse (right t0))))

(defmethod pseudo-inverse! ((t0 Symmetric-Inner-Product))
  (make-instance 'Symmetric-Outer-Product
		 :left (pseudo-inverse! (right t0))))

(defmethod pseudo-inverse ((t0 Symmetric-Outer-Product))
  (make-instance 'Symmetric-Inner-Product
		 :right (pseudo-inverse (left t0))))

(defmethod pseudo-inverse! ((t0 Symmetric-Outer-Product))
  (make-instance 'Symmetric-Inner-Product
		 :right (pseudo-inverse! (left t0))))

;;;-------------------------------------------------------

(defmethod pseudo-inverse  ((t0 Orthogonal)) (transpose  t0)) 
(defmethod pseudo-inverse! ((t0 Orthogonal)) (transpose! t0)) 

;;;-------------------------------------------------------

(defmethod pseudo-inverse ((t0 Linear-Product))
  (cond
    ((orthogonal? t0) (transpose t0))
    (t (make-instance
	 'Linear-Product
	 :factors (map 'List #'pseudo-inverse (reverse (factors t0)))))))

(defmethod pseudo-inverse! ((t0 Linear-Product))
  (cond
    ((orthogonal? t0) (transpose! t0))
    (t
     (setf (factors t0) (map 'List #'pseudo-inverse! (nreverse (factors t0))))
     t0)))

;;;-------------------------------------------------------

(defmethod pseudo-inverse  ((t0 Inverse)) (az:copy (source t0)))
(defmethod pseudo-inverse! ((t0 Inverse))       (source t0)) 

