;;;-*- Package: Cactus; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Cactus)

;;;============================================================

(defparameter *cactus0*
    (br:make-class-browser
     (az:all-subclasses 'ca:Cactus-Object)
     :width 740 :height 450
     :left 400 :top 400))

(defparameter *points*
    (br:make-class-browser
     (delete-if #'(lambda (c) (subtypep c 'ca::Mapping))
		(cons (find-class 'ca::Abstract-Point)
		      (az:all-subclasses 'ca::Abstract-Point)))
     :width 740 :height 450 :left 400 :top 400 :rest-length 8))
 
(xlt:ps-dump (clay:diagram-window *points*) "points.ps" :showpage? t)

(defparameter *spaces*
    (br:make-class-browser
     (cons (find-class 'ca:Abstract-Space)
	   (az:all-subclasses 'ca:Abstract-Space))
     :width 740 :height 450 :left 400 :top 400 :rest-length 8))

(xlt:ps-dump (clay:diagram-window *spaces*) "spaces.ps" :scale 3 :showpage? t)

(defparameter *mappings*
    (br:make-class-browser
     (cons (find-class 'ca::Mapping)
	   (az:all-subclasses 'ca::Mapping))
     :width 740 :height 450 :left 400 :top 400 :rest-length 16))

(xlt:ps-dump (clay:diagram-window *mappings*)
	     "mappings.ps" :scale 3 :showpage? t)

(defparameter *linear-mappings*
    (br:make-class-browser
     (cons (find-class 'ca::Linear-Mapping)
	   (az:all-subclasses 'ca::Linear-Mapping))
     :width 740 :height 450 :left 400 :top 400 :rest-length 14))

(xlt:ps-dump (clay:diagram-window *linear-mappings*)
	     "linear-mappings.ps" :scale 3 :showpage? t)

(defparameter *optimizers*
     (make-class-browser
      (cons (find-class 'Optimizer)
	    (Az:all-subclasses 'Optimizer))
       :width 740 :height 450 :left 400 :top 400))

(xlt:ps-dump (clay:diagram-window *optimizers*)
	     "/belgica-2g/jam/az/cactus/doc/optimizers.ps")
 
;;;============================================================

(optimize -amoeba-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))

(optimize *sd-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))
(optimize *dsd-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))

(optimize *cg-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))
(optimize *dcg-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))

(optimize *idfp-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))
(optimize *didfp-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))

(optimize *ibfgs-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))
(optimize *dibfgs-opt* *rosenbrock-function* (lpoint -1.2d0 1.0d0))

;;;============================================================

(test-linear-algebra 6 :n1 7 :n2 8)

;;;============================================================

(setf hesse (make-random-upper-hessenberg-matrix 5 6))

(trace test-inverse-lu-decompose)

(test-inverse-lu-decompose hesse)


browser/