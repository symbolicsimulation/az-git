;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================

(defvar *trace-tests?* nil)

(defun test-linear-algebra (n0
			    &key
			    (n1 (+ 2 (random (* n0 2))))
			    (n2 (+ 2 (random (* n0 2))))
			    (trace-tests? t)
			    (verify-bands? t))
  (when (= n0 n1 n2) (decf n1) (incf n2))
  (when (= n0 n1) (incf n1))
  (when (= n0 n2) (incf n2))
  (when (= n1 n2) (incf n2))
  (format *standard-output* " ~d,~d,~d~%" n0 n1 n2)
  (let* ((*verify-bands?* verify-bands?) ;; ensure cached bandedness correct
	 (*trace-tests?* trace-tests?)
	 (space0 (LVector-Space n0))
	 (space1 (LVector-Space n1))
	 (subspace0 (random-block-subspace space0))
	 (subspace1 (random-block-subspace space1))
	 (v00 (make-random-element space0))
	 (v01 (make-random-element space0))
	 (v1 (make-random-element space1))
	 (s00 (make-random-element subspace0))
	 (s01 (make-random-element subspace0))
	 (s1 (make-random-element subspace1))
	 (mtx00 (list-of-random-automorphic-matrices n0))
	 (mod00 (list-of-random-modifiers n0))
	 (mod11 (list-of-random-modifiers n1))
	 (pro00 (list (make-random-symmetric-inner-product n0)
		      (make-random-symmetric-outer-product n0)
		      (make-random-orthogonal-similarity-product n0)
		      (make-instance 'Linear-Product
			:factors (copy-list mtx00))
		      (make-instance 'Linear-Product
			:factors (copy-list mod00))))
	 (lst00 (concatenate 'list mtx00 mod00 pro00))
	 (mtx10 (list-of-random-rectangular-matrices n1 n0))
	 (mtx01 (list-of-random-rectangular-matrices n0 n1))
	 (mtx21 (list-of-random-rectangular-matrices n2 n1)))
    (declare (special *verify-bands?* *trace-tests?*))
    mtx01 mtx21 mtx10 mtx00 pro00 lst00 v00 v01 v1
    (setf mod00 (cons (make-random-1d-Annihilator n0) mod00)) 
    (setf mod11 (cons (make-random-1d-Annihilator n1) mod11))

    (when *trace-tests?* (print "...testing automorphic matrices..."))
    (mapc #'(lambda (t00)
	      (when *trace-tests?* (print t00))
	      (test-transform t00 v00 v01)
	      (test-transform t00 s00 s01)
	      (test-transform-transpose t00 v00 v01)
	      (test-transform-transpose t00 s00 s01)
	      (mapc #'(lambda (m00)
			(when *trace-tests?* (print m00))
			(test-compose t00 m00)
			(test-compose-inner t00 m00)
			(test-compose-outer t00 m00))
		    lst00)
	      (mapc #'(lambda (t10)
			(when *trace-tests?* (print t10))
			(test-compose t10 t00)
			(test-compose-outer t00 t10))
		    mtx10)
	      (mapc #'(lambda (t01)
			(when *trace-tests?* (print t01))
			(test-compose t00 t01)
			(test-compose-inner t01 t00))
		    mtx01)
	      (mapc #'(lambda (m00)
			(when (orthogonal? m00)
			  (when *trace-tests?* (print m00))
			  (test-rotate-mapping m00 t00)))
		    mod00))
	  lst00)
    (when *trace-tests?* (print "...testing rectangular matrices..."))
    (mapc #'(lambda (t10)
	      (when *trace-tests?* (print t10))
	      (test-transform t10 v01 v1)
	      (test-transform-transpose t10 v1 v01)
	      (test-transform t10 s01 s1)
	      (test-transform-transpose t10 s1 s01)
	      (mapc #'(lambda (m00)
			(when *trace-tests?* (print m00))
			(test-compose-left! (az:copy t10) m00))
		    mod00)
	      (mapc #'(lambda (m11)
			(when *trace-tests?* (print m11))
			(test-compose-right! m11 (az:copy t10)))
		    mod11)
	      (mapc #'(lambda (t21)
			(when *trace-tests?* (print t21))
			(test-compose t21 t10))
		    mtx21))
	  mtx10)

    (when *trace-tests?* (print "...testing automorphic matrices..."))
    (mapc #'(lambda (t00)
	      (when *trace-tests?* (print t00))
	      (test-ordered-diagonal-decompose t00)
	      (test-qr-decompose t00)
	      (test-lq-decompose t00)
	      (test-householder-bidiagonalize t00)
	      (test-hessenberg-decompose t00)
	      (test-inverse-lu-decompose t00)
	      (test-singular-value-decompose t00)
	      #||
	      (when (symmetric? t00) (test-eigen-decompose t00))
	      (when (and (symmetric? t00) (tridiagonal? t00))
		(test-implicit-qr-decompose t00))
	      ||#
	      (when (upper-bidiagonal? t00)
		(test-golub-kahan-decompose t00))
	      (when (positive-definite? t00)
		(test-left-triangular-sqrt t00)
		(test-right-triangular-sqrt t00))
	      (test-pseudo-inverse t00 v00 v01)
	      (test-pseudo-inverse t00 s00 s01))
	  mtx00)

    (when *trace-tests?* (print "...testing rectangular matrices..."))
    (mapc #'(lambda (t10)
	      (when *trace-tests?* (print t10))
	      (test-qr-decompose t10)
	      (test-lq-decompose t10)
	      (test-householder-bidiagonalize t10)
	      ;;(test-nash-svd-decompose t10)
	      (test-singular-value-decompose t10)
	      (test-pseudo-inverse t10 v01 v1)
	      (test-pseudo-inverse t10 s01 s1))
	  mtx10)
					;    (test-determinants lst00)
    t))

;;;=======================================================

(defun list-of-random-automorphic-matrices (n)
  (list (make-random-matrix n n)
	;;(make-random-Restricted-Matrix n n)
	(make-random-upper-triangular-matrix n n)
	(make-random-lower-triangular-matrix n n)
	(make-random-upper-hessenberg-matrix n n)
	(make-random-lower-hessenberg-matrix n n)
	(make-random-upper-bidiagonal-matrix n n)
	(make-random-symmetric-matrix n)
	(make-random-symmetric-tridiagonal-matrix n)
	(make-random-positive-definite-matrix n)
        (make-random-positive-definite-tridiagonal-matrix n)
	))

(defun list-of-random-modifiers (n)
  (list (make-random-pivot n)
	(make-random-householder n)
	(make-random-gauss n)
	(make-random-givens n)))

(defun list-of-random-rectangular-matrices (n0 n1)
  (list (make-random-matrix n0 n1)
	(make-random-upper-hessenberg-matrix n0 n1)
	(make-random-lower-hessenberg-matrix n0 n1)
	(make-random-upper-triangular-matrix n0 n1)
	(make-random-lower-triangular-matrix n0 n1)
	(make-random-upper-bidiagonal-matrix n0 n1)
	))

;;;=======================================================

(defun test-transform (t0 vd vr)
  (when *trace-tests?* (princ "transform "))
  (with-borrowed-element (v1 (codomain t0))
    (let* ((ta (copy-to-array t0))
	   (vd1d (1d-array (embed vd (domain t0))))
	   (v0 (make-lvector-from-array 
		 (bm:multiply-matrices ta vd1d))))
      (setf v1 (transform t0 vd :result v1))
      (assert (~= v0 v1 100.0d0))
      (when (eq (home-space vr) (codomain t0))
	(transform t0 vd :result vr)
	(assert (~= v0 vr 100.0d0)))))
  t)
    
(defun test-transform-transpose (t0 vd vr)
  (when *trace-tests?* (princ "transform-transpose "))
  (with-borrowed-element (v1 (domain t0))
    (let* ((ta (copy-to-array (transpose t0)))
	   (vd1d (1d-array (embed vd (codomain t0))))
	   (v0 (make-lvector-from-array
		 (bm:multiply-matrices ta vd1d))))
      (setf v1 (transform-transpose t0 vd :result v1))
      (assert (~= v0 v1 100.0d0))
      (when (eq (home-space vr) (domain t0))
	(transform-transpose t0 vd :result vr)
	(assert (~= v0 vr 100.0d0)))))
  t)
    
;;;=======================================================

(defun test-compose (t0 t1)
  (when *trace-tests?* (princ "compose "))
  (let* ((a0 (copy-to-array t0))
	 (a1 (copy-to-array t1))
	 (a2 (bm:multiply-matrices a0 a1))
	 (t3 (compose t0 t1))
	 (a3 (copy-to-array t3)))
    (assert (~= a2 a3 10.0d0))
    (when (typep t3 'Matrix)
      (assert (verify-bands? t3))
      (assert-properties t3)))
  t) 

(defun test-compose-inner (t0 t1)
  (when *trace-tests?* (princ "compose-inner "))
  (let* ((t2 (transpose t0))
	 (a0 (copy-to-array t2))
	 (a1 (copy-to-array t1))
	 (t3 (compose-inner t0 t1))
	 (a2 (copy-to-array t3))
	 (a3 (bm:multiply-matrices a0 a1))
	 (a4 (copy-to-array (compose t2 t1))))    
    (assert (~= a2 a3 10.0d0))
    (assert (~= a2 a4 10.0d0))
    (when (typep t3 'Matrix)
      (assert (verify-bands? t3))
      (assert-properties t3)))
  t)

(defun test-compose-outer (t0 t1)
  (when *trace-tests?* (princ "compose-outer "))
  (let* ((t2 (transpose t1))
	 (t3 (compose-outer t0 t1))
	 (t4 (compose t0 t2))
	 (a0 (copy-to-array t0))
	 (a2 (copy-to-array t2))
	 (a3 (copy-to-array t3))
	 (a4 (bm:multiply-matrices a0 a2)))
    (assert (~= a3 a4 10.0d0))
    (assert (~= t3 t4 10.0d0))
    (when (typep t3 'Matrix)
      (assert (verify-bands? t3))
      (assert-properties t3)))
  t)

(defun test-compose-left! (t0 t1)
  (when *trace-tests?* (princ "compose-left! "))
  (let* ((t3 (az:copy t0))
	 (t4 (az:copy t0))
	 (a0 (copy-to-array t0))
	 (a1 (copy-to-array t1))
	 (a2 (bm:multiply-matrices a0 a1))
	 (t5 (compose t3 t1 :result t3))
	 (t6 (compose-left! t4 t1)))
    (assert (eq t5 t3))
    (assert (eq t6 t4))
    (assert (~= t5 t6 0.1e0))
    (assert (~= a2 (2d-array t5) 10.0d0))
    (assert (~= a2 (2d-array t6) 10.0d0))
    (when (typep t3 'Matrix)
      (assert (verify-bands? t3))
      (assert-properties t3))) 
  t)

(defun test-compose-right! (t0 t1)
  (when *trace-tests?* (princ "compose-right! "))
  (let* ((t3 (az:copy t1))
	 (t4 (az:copy t1))
	 (a0 (copy-to-array t0))
	 (a1 (copy-to-array t1))
	 (a2 (bm:multiply-matrices a0 a1))
	 (t5 (compose t0 t3 :result t3))
	 (t6 (compose-right! t0 t4)))
    (assert (eq t5 t3))
    (assert (eq t6 t4))
    (assert (~= t5 t6 10.0d0))
    (assert (~= a2 (2d-array t5) 10.0d0))
    (assert (~= a2 (2d-array t6) 10.0d0))
    (when (typep t3 'Matrix)
      (assert (verify-bands? t3))
      (assert-properties t3)))
  t)

;;;-------------------------------------------------------
;;; this assumes <compose> and <pseudo-inverse> work

(defun test-rotate-mapping (u t0)
  (when *trace-tests?* (princ "rotate-mapping "))
  (let ((t1 (rotate-mapping u t0))
	(t2 (compose (pseudo-inverse u) (compose t0 u))))
    (assert (~= t1 t2 10.0d0))
    (when (typep t2 'Matrix)
      (assert (verify-bands? t2))
      (assert-properties t2)))
  t)

;;;=======================================================
 
(defun test-pseudo-inverse (t0 vd vr)
  (when *trace-tests?* (princ "pseudo-inverse "))
  (let ((size (* 100.0d0 (dimension (codomain t0)) (dimension (domain t0)))))
    (if (and (automorphic? t0)
	     (bm:small?  (determinant t0)))
	(warn "~a has a small determinant; inverse not computed" t0)
	;; else
	(with-borrowed-elements ((vd0 (domain t0))
				 (vr0 (codomain t0)))
	  (let ((t0-1 (pseudo-inverse t0)))
	    (when (embedding? t0)
	      (let* ((t1 (compose t0-1 t0)))
		(setf vr0 (transform t0 vd :result vr0))
		(setf vd0 (transform t0-1 vr0 :result vd0))
		(assert (~= vd vd0 size))
		(assert (bm:small?  (sup-dist-from-identity-map t1) size))))
	    (when (projecting? t0)
	      (let* ((t1 (compose t0 t0-1)))
		(setf vd0 (transform t0-1 vr :result vd0))
		(setf vr0 (transform t0 vd0 :result vr0))
		(assert (~= vr vr0 size))
		(assert (bm:small? (sup-dist-from-identity-map t1) size))))))))
  t)

;;;=======================================================

(defun test-determinants (tlist)
  (when *trace-tests?* (print "testing determinants "))
  (let ((dlist (mapcar #'determinant tlist)))
    (mapc
      #'(lambda (t0 d0)
	  (when *trace-tests?* (print t0))
	  (mapc
	    #'(lambda (t1 d1)
		(when *trace-tests?* (print t1))
		(test-determinant t0 t1 d0 d1))
	    tlist dlist))
      tlist dlist)))
  
(defun test-determinant (t0 t1
			 &optional
			 (d0 (determinant t0))
			 (d1 (determinant t1)))
  (when *trace-tests?* (princ "determinant "))
  (let (t2 d2)
    (cond
      ((positive-definite? t0)
       (assert (> d0 0.0d0) (t0 d0)
	       "det of pos def should be >0. "))
      ((positive-semi-definite? t0)
       (assert (>= d0 0.0d0) (t0 d0)
	       "det of pos semi def should be >=0. ")))
    (cond
      ((positive-definite? t1)
       (assert (> d1 0.0d0) (t1 d1)
	       "det of pos def should be >0. "))
      ((positive-semi-definite? t1)
       (assert (>= d1 0.0d0) (t1 d1)
	       "det of pos semi def should be >=0. ")))
    (unless (or (bm:small?  (* d1 d0) 100.0d0)
		(bm:small?  d0 100.0d0)
		(bm:small?  d1 100.0d0)
		(bm:small?  d0 (* 100.0d0 d1))
		(bm:small?  d1 (* 100.0d0 d0)))
      (setf t2 (compose t0 t1))
      (setf d2 (determinant t2))
      (assert (bm:small?  (- d2 (* d0 d1)) 10000.0d0))))
  t)

;;;=======================================================

(defun test-inverse-lu-decompose (t0)
  (when *trace-tests?* (princ "inverse-lu-decompose "))

  (let (slow-lu-1 slow-lu slow-mtx0 slow-mtx1 slow-u lu-1 lu mtx1 mtx0 u)
     ;;(user::print-db
      (setf slow-lu-1 (slow-inverse-lu-decompose t0))
	 (setf slow-lu (pseudo-inverse slow-lu-1))
	 (setf slow-mtx0 (copy-to-matrix slow-lu-1))
	 (setf slow-mtx1 (copy-to-matrix slow-lu))
	 (setf slow-u (pseudo-inverse (az:first-elt (factors slow-lu-1))))
	 (setf lu-1 (inverse-lu-decompose t0))
	 (setf lu (pseudo-inverse lu-1))
	 (setf mtx1 (copy-to-matrix lu))
	 (setf mtx0 (copy-to-matrix lu-1))
	 (setf u (pseudo-inverse (az:first-elt (factors lu-1))))
    (unless (lower-triangular? t0)
      (assert (upper-triangular? slow-u))
      (assert (upper-triangular? u)))
    (assert (~= mtx0 slow-mtx0 10.0d0))
    (assert (~= u slow-u 10.0d0))
    (assert (~= slow-mtx1 t0 10.0d0))
    (assert (~= mtx1 t0 10.0d0)))
;;)
  t)

(defun test-qr-decompose (t0)
  (when *trace-tests?* (princ "qr-decompose "))
  (let* ((slow-qr (slow-qr-decompose t0))
	 (slow-mtx0 (copy-to-matrix slow-qr))
	 (slow-mtx1 (reduce #'compose (factors slow-qr)))
	 (qr (qr-decompose t0))
	 (mtx0 (copy-to-matrix qr))
	 (mtx1 (reduce #'compose (factors qr))))
    (assert (upper-triangular? (az:last-elt (factors slow-qr)) :verify? t))
    (assert (~= t0 slow-mtx0 10.0d0))
    (assert (~= t0 slow-mtx1 10.0d0))
    (assert (upper-triangular? (az:last-elt (factors qr)) :verify? t))
    (assert (~= t0 mtx0 10.0d0))
    (assert (~= t0 mtx1 10.0d0)))
  t)

(defun test-lq-decompose (t0)
  (when *trace-tests?* (princ "lq-decompose "))
  (let* ((slow-lq (slow-lq-decompose t0))
	 (slow-mtx0 (copy-to-matrix slow-lq))
	 (slow-mtx1 (reduce #'compose (factors slow-lq)))
	 (lq (lq-decompose t0))
	 (mtx0 (copy-to-matrix lq))
	 (mtx1 (reduce #'compose (factors lq))))
    (assert (lower-triangular? (az:first-elt (factors slow-lq)) :verify? t))
    (assert (~= t0 slow-mtx0 5.0d1))
    (assert (~= t0 slow-mtx1 5.0d1))
    (assert (lower-triangular? (az:first-elt (factors lq)) :verify? t))
    (assert (~= t0 mtx0 5.0d1))
    (assert (~= t0 mtx1 5.0d1)))
  t)

(defun test-left-triangular-sqrt (t0)
  (when *trace-tests?* (princ "left-triangular-sqrt "))
  (let ((l (left-triangular-sqrt t0)))
    (assert (lower-triangular? l :verify? t))
    (assert (~= t0 (compose-outer l l) 5.0d1)))
  t)

(defun test-right-triangular-sqrt (t0)
  (when *trace-tests?* (princ "right-triangular-sqrt "))
  (let ((r (right-triangular-sqrt t0)))
    (assert (upper-triangular? r))
    (assert (~= t0 (compose-inner r r) 5.0d1)))
  t)

;;;=======================================================

(defun test-eigen-decompose (t0)
  (when *trace-tests?* (princ "eigen-decompose "))
  (let* ((size  (* 10.0d0 (dimension (codomain t0)) (dimension (domain t0))))
	 (eigen (eigen-decompose t0))
	 (mtx0 (copy-to-matrix eigen))
	 (mtx1 (rotate-mapping (right eigen) (middle eigen))))
    (assert (diagonal? (middle eigen)))
    (assert (~= t0 mtx0 size))
    (assert (~= t0 mtx1 size))) 
  t)

;;;-------------------------------------------------------

(defun test-hessenberg-decompose (t0)
  (when *trace-tests?* (princ "hessenberg-decompose "))
  (let* ((hesse (hessenberg-decompose t0))
	 (mtx0 (copy-to-matrix hesse))
	 (mtx1 (rotate-mapping (right hesse) (middle hesse))))
    (assert (upper-hessenberg? (middle hesse)))
    (assert (~= t0 mtx0 5.0d1))
    (assert (~= t0 mtx1 5.0d1)))
  t)

;;;-------------------------------------------------------

(defun test-implicit-qr-decompose (t0)
  (when *trace-tests?* (princ "implicit-qr-decompose "))
  (let* ((sp (implicit-qr-decompose t0))
	 (mtx0 (copy-to-matrix sp))
	 (mtx1 (rotate-mapping (right sp) (middle sp))))
    (assert (~= t0 mtx0 5.0d1))
    (assert (~= t0 mtx1 5.0d1)))
  t)

;;;-------------------------------------------------------

(defun test-ordered-diagonal-decompose (t0)
  (when *trace-tests?* (princ "ordered-diagonal-decompose "))
  (let* ((sp (ordered-diagonal-decompose t0))
	 (mtx0 (copy-to-matrix sp))
	 (mtx1 (rotate-mapping (right sp) (middle sp))))
    (assert (~= t0 mtx0 5.0d1))
    (assert (~= t0 mtx1 5.0d1)))
  t)

;;;-------------------------------------------------------

(defun test-nash-svd-decompose (t0)
  (when *trace-tests?* (princ "nash-svd-decompose "))
  (unless (embedding? t0) (setf t0 (transpose t0)))
  (let* ((svd (nash-svd-decompose t0))
	 (t1 (reduce #'compose (factors svd))))
    (assert (~= t0 t1 5.0d1))) 
  t)

;;;=======================================================

(defun test-singular-value-decompose (t0)
  (when *trace-tests?* (princ "singular-value-decompose "))
  (let* ((svd (singular-value-decompose t0))
	 (t1 (reduce #'compose (factors svd))))
    (assert (~= t0 t1 5.0d1)))
  t)

;;;-------------------------------------------------------

(defun test-golub-kahan-decompose (t0)
  (when *trace-tests?* (princ "slow-golub-kahan-decompose "))
  (let* ((gk (slow-golub-kahan-decompose t0))
	 (mtx0 (reduce #'compose (factors gk)))
	 (mtx1 (copy-to-matrix gk)))
    (assert (~= t0 mtx0 5.0d1))
    (assert (~= t0 mtx1 5.0d1)))
  t)

;;;-------------------------------------------------------

(defun test-decouple-last-diagonal-zero (t0)
  (when *trace-tests?* (princ "decouple-last-diagonal-zero "))
  (multiple-value-bind (ls t1) (decouple-last-diagonal-zero! (az:copy t0))
    (unless (null ls)
      (let* ((l (make-instance 'Linear-Product :factors ls))
	     (t2 (compose l t1)))
	(assert (~= t0 t2 5.0d1)))))
  t)

;;;-------------------------------------------------------

(defun test-householder-bidiagonalize (t0)
  (when *trace-tests?* (princ "householder-bidiagonalize "))
  (let* ((ubv (householder-bidiagonalize t0))
	 (b (find-if #'(lambda (f) (typep f 'Matrix-Super)) (factors ubv)))
	 (mtx0 (reduce #'compose (factors ubv)))
	 (mtx1 (copy-to-matrix ubv)))
    (assert (upper-bidiagonal? b :eps 5.0d1))
    (assert (~= t0 mtx0 5.0d1))
    (assert (~= t0 mtx1 5.0d1)))
  t)


;;;=======================================================

(defmethod random-block-subspace ((vspace LVector-Space))
  (let* ((n (dimension vspace))
	 (start (if (> n 2)
		    (random (- n 2))
		    0))
	 (end (if (> (- n start) 2)
		  (+ 2 start (random (- n start 2)))
		  n)))
    (block-subspace (LVector-Space n) start end)))

;;;=======================================================

(defmethod make-random-element ((vspace LVector-Space)
				&rest options)
  (apply #'fill-randomly! (make-element vspace) options))

;;;-------------------------------------------------------
;;; This, and other, "make-random-" functions are intended
;;; for testing purposes.

(defun make-random-matrix (nrows &optional (ncols nrows)
			   &key (min -1.0d0) (max 1.0d0))
  (let ((t0 (make-matrix nrows ncols)))
    (fill-randomly! t0 :min min :max max)
    t0))

(defmethod fill-randomly! ((t0 Matrix) &key (min -1.0d0) (max 1.0d0))
  (bm:fill-random-2d-array (2d-array t0) :min min :max max)
  t0)

;;;-------------------------------------------------------

(defun make-random-symmetric-Matrix (dim &key (min -1.0d0) (max 1.0d0))
  (let ((t0 (make-instance
	     'Matrix
	     :2d-array (bm:make-random-symmetric-2d-array dim
							  :min min :max max)
	     :domain (LVector-Space dim)
	     :codomain (LVector-Space dim))))
    (add-property! t0 'symmetric?)
    t0))

;;;-------------------------------------------------------

(defun make-random-positive-definite-Matrix (dim &key (min -1.0d0) (max 1.0d0))
  (let* ((m (make-random-matrix dim))
	 (d (make-random-diagonal-vector
	      dim
	      :min (max 1.0d0 (+ min 1.0d0))
	      :max (max 2.0d0 (+ max 1.0d0))))
	 (mtm (compose-inner m m))
	 (p (add d mtm :result mtm)))
    (add-property! p 'positive-definite?)
    p))

;;;-------------------------------------------------------
  
(defun make-random-upper-triangular-matrix (nrows
					    &optional (ncols nrows)
					    &key (min -1.0d0) (max 1.0d0))
  (let* ((t0 (make-random-matrix nrows ncols :min min :max max))
	 (s0 (find-if #'(lambda (f) (typep f 'Matrix-Super))
		      (factors (qr-decompose! t0)))))
    (if (typep s0 'Restricted-Matrix)
	(setf t0 (source s0))
	(setf t0 s0))
    (add-property! t0 'upper-triangular? :coerce? t)
    t0))

;;;-------------------------------------------------------
  
(defun make-random-lower-triangular-matrix (nrows &optional (ncols nrows)
					    &key (min -1.0d0) (max 1.0d0))
  (let* ((t0 (make-random-matrix nrows ncols :min min :max max))
	 (s0 (find-if #'(lambda (f) (typep f 'Matrix-Super))
		      (factors (lq-decompose! t0)))))
    (if (typep s0 'Restricted-Matrix)
	(setf t0 (source s0))
	(setf t0 s0))
    (add-property! t0 'lower-triangular? :coerce? t)
    t0))

;;;-------------------------------------------------------

(defun make-random-upper-hessenberg-matrix (nrows
					    &optional (ncols nrows)
					    &key (min -1.0d0) (max 1.0d0))
  (let ((t0 (make-random-matrix nrows ncols :min min :max max)))
    (add-property! t0 'upper-hessenberg? :coerce? t)
    t0))

;;;-------------------------------------------------------

(defun make-random-lower-hessenberg-matrix (nrows &optional (ncols nrows)
					    &key (min -1.0d0) (max 1.0d0))
  (let ((t0 (make-random-matrix nrows ncols :min min :max max)))
    (add-property! t0 'lower-hessenberg? :coerce? t)
    t0))

;;;-------------------------------------------------------

(defun make-random-symmetric-tridiagonal-Matrix (nrows
						 &key (min -1.0d0) (max 1.0d0))
  (let ((t0 (make-random-symmetric-matrix nrows :min min :max max)))
    (add-property! t0 'tridiagonal? :coerce? t)
    t0))

;;;-------------------------------------------------------

(defun make-random-upper-bidiagonal-matrix (nrows
					    &optional (ncols nrows)
					    &key (min -1.0d0) (max 1.0d0))
  (let* ((t0 (make-random-matrix nrows ncols :min min :max max))
	 (s0 (find-if #'(lambda (f) (typep f 'Matrix-Super))
		      (factors (householder-bidiagonalize! t0)))))
    (if (typep s0 'Restricted-Matrix)
	(setf t0 (source s0))
	(setf t0 s0))
    (add-property! t0 'upper-bidiagonal? :coerce? t)
    t0))

;;;-------------------------------------------------------
  
(defun make-random-positive-definite-tridiagonal-matrix (nrows
							 &key
							 (min -1.0d0)
							 (max 1.0d0))
  (let ((t0 (make-random-upper-bidiagonal-matrix
	      nrows nrows :min min :max max)))
    (setf t0 (compose (transpose t0) t0))
    (add-property! t0 'positive-definite?)
    (add-property! t0 'tridiagonal? :coerce? t)
    t0))

;;;-------------------------------------------------------

(defun make-random-orthogonal-matrix (dim
				      &key (min -1.0d0) (max 1.0d0))
  (let ((lq (lq-decompose (make-random-matrix dim dim :min min :max max)))
	mtx )
    (setf mtx (copy-to-matrix (second (factors lq))))
    (add-property! mtx 'orthogonal?)))

;;;-------------------------------------------------------

(defun make-random-Restricted-Matrix (m &optional (n m)
				    &key (min -1.0d0) (max 1.0d0))
  (let* ((nrows (+ m (random m) 1))
	 (ncols (+ n (random n) 1))
	 (t0 (make-random-matrix nrows ncols :min min :max max)))
    (restrict
      t0
      (random-block-subspace (codomain t0))
      (random-block-subspace (domain   t0)))))

;;;-------------------------------------------------------

(defun make-random-block-projection (ran-dim dom-dim)
  (assert (>= dom-dim ran-dim))
  (if (= ran-dim dom-dim)
      (the-identity-map (LVector-Space dom-dim))
      ;; else
      (let* ((start (random (- dom-dim ran-dim)))
	     (end (+ start ran-dim))
	     (dom (LVector-Space dom-dim))
	     (cod (block-subspace dom start end)))
	(make-instance 'Block-Projection :domain dom :codomain cod))))

;;;-------------------------------------------------------

(defun make-random-block-embedding (cod-dim dom-dim)
  (assert (>= cod-dim dom-dim))
  (if (= dom-dim cod-dim)
      (the-identity-map (LVector-Space cod-dim))
      ;; else
      (let* ((start (random (- cod-dim dom-dim)))
	     (end (+ start dom-dim))
	     (cod (LVector-Space cod-dim))
	     (dom (block-subspace cod start end)))
	(make-instance 'Block-Embedding :codomain cod :domain dom))))

;;;-------------------------------------------------------

(defun make-random-gauss (n)
  (let* ((v (make-random-element (LVector-Space  n)))
	 (start (if (<= n 2) 1 (random (- n 2))))
	 (end (if (<= n 2) 2 (+ start 2 (random (- n start 1) )))))
    (make-gauss-for (LVector-Space N) v start end)))
    
;;;-------------------------------------------------------

(defun make-random-householder (dim
				&optional
				(start (random (- dim 1)))
				(end (+ 2 start (random (- dim start 1))))
				&key (min -1.0d0) (max 1.0d0))
  (let ((v (make-random-element (LVector-Space dim) :max max :min min)))
    (make-householder (LVector-Space dim) v start end)))

;;;-------------------------------------------------------

(defun make-random-givens (n &optional (min -1.0d0) (max 1.0d0))
  (let ((ran (- max min)))
    (make-givens-for
      (LVector-Space n) (+ (random ran) min) (+ (random ran) min)
      (random n) (random n))))

;;;-------------------------------------------------------

(defun make-random-diagonal-vector (nrows &key (min -1.0d0) (max 1.0d0))
  (make-instance 'Diagonal-Vector
		 :vec (make-random-element (LVector-Space nrows)
					   :min min :max max)
		 :domain (LVector-Space nrows)
		 :codomain (LVector-Space nrows)))

;;;-------------------------------------------------------

(defun make-random-pivot (n)
  (make-pivot (LVector-Space n) (random n) (random n)))

;;;-------------------------------------------------------

(defun make-random-1d-Annihilator (dim
				   &key
				   (min -1.0d0)
				   (max 1.0d0))
  (make-1d-Annihilator-for (make-random-element (LVector-Space dim)
						:min min :max max)))

;;;-------------------------------------------------------

(defun make-random-symmetric-outer-product (m &optional (n m))
  (make-instance 'Symmetric-Outer-Product
		 :left (make-random-matrix m n)))


(defun make-random-symmetric-inner-product (m &optional (n m))
  (make-instance 'Symmetric-Inner-Product
		 :right (make-random-matrix m n)))

(defun make-random-orthogonal-similarity-product (n)
  (make-instance 'Orthogonal-Similarity-Product
		 :right (make-random-orthogonal-matrix n)
		 :middle (make-random-matrix n)))
