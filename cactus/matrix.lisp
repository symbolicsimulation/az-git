;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;; Ancestor class for Matrix and Restricted-Matrix.
;;; The purpose is to allow methods to be shared;
;;; making either Matrix or Restricted-Matrix
;;; a subclass of the other results in things being inherited that
;;; shouldn't be.
;;;
;;; To do: add a Constant-Matrix class that doesn't allow any of its
;;; slot values to be altered (as far as possible).  Eg. (2d-array
;;; constant-matrix) would return a copy of the 2d-array; (setf
;;; (2d-array constant-matrix) x) would generate an error.  We can then
;;; use information about properties (eg. positive-definite?)  of
;;; Constant-Matrix's will reasonable safety. Have modifiable matrices
;;; return nil or the equivalent (eg. maximum possible bandwidths) for
;;; all the property predicates.

(defclass Matrix-Super (Linear-Mapping)
     ((bands-start
	:type az:Array-Index
	:initarg :bands-start
	:accessor bands-start)
      (bands-end
	:type az:Array-Index
	:initarg :bands-end
	:accessor bands-end)))

;;;-------------------------------------------------------

(defmethod col ((t0 Matrix-Super) (i Integer)
		&key
		(result
		  (make-element (codomain t0))))

  (let ((cod (codomain t0)))

    (az:type-check (or LVector-Space LVector-Block-Subspace) cod)
    (assert (and (element? result cod)
		 (eq (home-space result) cod)))

    (bm:v<-v! (1d-array result) (2d-array t0)
	       :end0 (dimension cod) 
	       :start1 (coord-start cod) :end1 (coord-end cod)
	       :vtype1 :col :on1 i))

  result)

(defmethod row ((t0 Matrix-Super) (i Integer)
                &key
                (result
		  (make-element (domain t0))))

  (let ((dom (domain t0)))

    (az:type-check (or LVector-Space LVector-Block-Subspace) dom)
    (assert (and (element? result dom)
		 (eq (home-space result) dom)))

    (bm:v<-v! (1d-array result) (2d-array t0)
	       :end0 (dimension dom)
	       :start1 (coord-start dom) :end1 (coord-end dom)
	       :vtype1 :row :on1 i))

  result)

;;;-------------------------------------------------------

(defmethod (setf col) ((v Lvector-Super) (t0 Matrix-Super) (i Integer))

  (let* ((cod (codomain t0))
	 (s0 (coord-start v))
	 (e0 (coord-end v))
	 (s1 (coord-start cod))
	 (e1 (coord-end cod))
	 (n (- e0 s0))
	 (1d (1d-array v))
	 (2d (2d-array t0)))

    (az:type-check (or LVector-Space LVector-Block-Subspace) cod)
    (assert (element? v cod))
    (when (> s0 s1)
      (bm:v<-x! 2d 0.0d0
		:start s1 :end s0 :vtype :col :on i))
    (bm:v<-v! 2d 1d :start0 s0 :end0 e0 :vtype0 :col :on0 i :end1 n)
    (when (> e1 e0)
      (bm:v<-x! 2d 0.0d0
		:start e1 :end e0 :vtype :col :on i))) 
  v)

(defmethod (setf row) ((v Lvector-Super) (t0 Matrix-Super) (i Integer))

  (let* ((dom (domain t0))
	 (s0 (coord-start v))
	 (e0 (coord-end v))
	 (s1 (coord-start dom))
	 (e1 (coord-end dom))
	 (n (- e0 s0))
	 (1d (1d-array v))
	 (2d (2d-array t0)))

    (az:type-check (or LVector-Space LVector-Block-Subspace) dom)
    (assert (element? v dom))
    (when (> s0 s1)
      (bm:v<-x! 2d 0.0d0 :start s1 :end s0 :vtype :row :on i))
    (bm:v<-v! 2d 1d :start0 s0 :end0 e0 :vtype0 :row :on0 i :end1 n)
    (when (> e1 e0)
      (bm:v<-x! 2d 0.0d0 :start e1 :end e0 :vtype :row :on i))) 
  v)

;;;-------------------------------------------------------
;;; Bandedness
;;;-------------------------------------------------------
;;;
;;; The band coordinate is (- j i) for any x_ij that's in the band.
;;; This means that sub-diagonal bands have negative coordinates.
;;; <bands-start> is the band coordinate of the start band that is not all
;;; zeros, eg., for a upper triangular matrix <bands-start> = 0, for an
;;; upper hessenberg matrix it's -1.  <bands-end> is the band
;;; coordinate of the start band that is all zeros after the non-zero
;;; bands, eg., for a lower triangular matrix <bands-end> = 1, for a
;;; lower hessenberg it's 2.  The reason for this  asymmetry is to be
;;; consistent with the specifications for integer intervals used in the
;;; CL sequence functions and the Basic-Math v functions.

(defvar *coerce-bands?* nil)
(defvar *verify-bands?* nil)

;;;-------------------------------------------------------    

(defmethod min-band-coord ((t0 Matrix-Super))
  (- 1 (dimension (codomain t0))))

(defmethod max-band-coord ((t0 Matrix-Super))
  (dimension (domain t0)))

;;;-------------------------------------------------------


(defmethod min-bands-start? ((t0 Matrix-Super) &key (start (bands-start t0)))
  (<= start (min-band-coord t0)))

(defmethod max-bands-end? ((t0 Matrix-Super) &key (end (bands-end t0)))
  (>= end (max-band-coord t0)))

(defmethod trivial-bands? ((t0 Matrix-Super)
			   &key
			   (start (bands-start t0))
			   (end (bands-end t0)))
  (and (min-bands-start? t0 :start start)
       (max-bands-end? t0 :end end)))

;;;-------------------------------------------------------
;; These could be speeded up by operating directly on the <2d-array>.
;; However, using the <row> "accessor" is easier, safer, and more modular.

(defmethod verify-bands? ((t0 Matrix-Super)
			  &key
			  (start (bands-start t0))
			  (end (bands-end t0))
			  (eps 10.0d0))
  (unless (trivial-bands? t0)
    (let ((x 0.0d0)
	  (n (dimension (domain t0)))
	  (norm (l1-norm t0))
	  (s (coord-start (domain t0)))
	  i-s)
      (with-borrowed-element (v (domain t0))
	(let ((1d (1d-array v)))
	  (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	    (setf v (row t0 i :result v))
	    (setf i-s (- i s))
	    (setf
	      x
	      (+ (bm:v-abs-max 1d :end (az:bound 0 (+ i-s start) n)) 
		 (bm:v-abs-max 1d :start (az:bound 0 (+ i-s end) n) :end n)))
	    (unless (bm:small? x (* (+ 1.0d0 norm) eps))
	      (return-from verify-bands? nil)))))))
  t)

(defmethod coerce-bands! ((t0 Matrix-Super)
			  &key
			  (start (bands-start t0))
			  (end (bands-end t0)))
  (let ((n (dimension (domain t0))))
    (with-borrowed-element (v (domain t0))
      (let ((1d (1d-array v))
	    (s (coord-start (domain t0)))
	    i-s)      
	(az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	  (setf v (row t0 i :result v))
	  (setf i-s (- i s))
	  (bm:v<-x! 1d 0.0d0 :end (az:bound 0 (+ i-s start) n))
	  (bm:v<-x! 1d 0.0d0 :start (az:bound 0 (+ i-s end) n) :end n)
	  (setf (row t0 i) v))))))

;;;-------------------------------------------------------

(defmethod diagonal? ((t0 Matrix-Super)
		      &key
		      (verify? *verify-bands?*)
		      (eps 10.0d0)
		      &allow-other-keys)
  (and (upper-triangular? t0) (lower-triangular? t0)
       (if verify?
	   (verify-bands? t0 :start 0 :end 1 :eps eps)
	   ;; else
	   t)))
  
(defmethod identity? ((t0 Matrix-Super)
		      &key
		      (verify? *verify-bands?*)
		      (eps 10.0d0)
		      &allow-other-keys)
  (and (diagonal? t0 :eps eps :verify? verify?)
       (unit-diagonal? t0 :eps eps :verify? verify?)))
  
;;; - - - - - - - - - - - - - - - - - - - - - - - - - - -

(defmethod upper-hessenberg? ((t0 Matrix-Super)
			      &key
			      (verify? *verify-bands?*)
			      (eps 10.0d0)
			      &allow-other-keys)
  (and (>= (bands-start t0) -1)
       (if verify?
	   (verify-bands? t0 :start -1 :eps eps) 
	   ;; else
	   t)))
  
(defmethod lower-hessenberg? ((t0 Matrix-Super)
			      &key
			      (verify? *verify-bands?*)
			      (eps 10.0d0)
			      &allow-other-keys)
  (and (<= (bands-end t0) 2)
       (if verify?
	   (verify-bands? t0 :end 2 :eps eps) 
	   ;; else
	   t)))
  
;;; - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
(defmethod upper-triangular? ((t0 Matrix-Super)
			      &key
			      (verify? *verify-bands?*)
			      (eps 10.0d0)
			      &allow-other-keys)
  (and (>= (bands-start t0) 0)
       (if verify?
	   (verify-bands? t0 :start 0 :eps eps) 
	   ;; else
	   t)))
  
(defmethod lower-triangular? ((t0 Matrix-Super)
			      &key
			      (verify? *verify-bands?*)
			      (eps 10.0d0)
			      &allow-other-keys)
  (and (<= (bands-end t0) 1)
       (if verify?
	   (verify-bands? t0 :end 1 :eps eps) 
	   ;; else
	   t)))
  
;;; - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
(defmethod unit-upper-triangular? ((t0 Matrix-Super)
				   &key
				   (verify? *verify-bands?*)
				   (eps 10.0d0)
				   &allow-other-keys)
  (and (unit-diagonal? t0 :eps eps :verify? verify?)
       (upper-triangular? t0 :eps eps :verify? verify?)))

(defmethod unit-lower-triangular? ((t0 Matrix-Super)
				   &key
				   (verify? *verify-bands?*)
				   (eps 10.0d0)
				   &allow-other-keys)
  (and (unit-diagonal? t0 :eps eps :verify? verify?)
       (lower-triangular? t0 :eps eps :verify? verify?)))

;;; - - - - - - - - - - - - - - - - - - - - - - - - - - -

(defmethod upper-bidiagonal? ((t0 Matrix-Super)
			      &key
			      (verify? *verify-bands?*)
			      (eps 10.0d0)
			      &allow-other-keys)
  (and (upper-triangular? t0 :eps eps :verify? verify?)
       (lower-hessenberg? t0 :eps eps :verify? verify?)))
  
(defmethod lower-bidiagonal? ((t0 Matrix-Super)
			      &key
			      (verify? *verify-bands?*)
			      (eps 10.0d0)
			      &allow-other-keys)
  (and (lower-triangular? t0 :eps eps :verify? verify?)
       (upper-hessenberg? t0 :eps eps :verify? verify?)))
  
;;; - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
(defmethod tridiagonal? ((t0 Matrix-Super)
			 &key
			 (verify? *verify-bands?*)
			 (eps 10.0d0)
			 &allow-other-keys)
  (and (lower-hessenberg? t0 :eps eps :verify? verify?)
       (upper-hessenberg? t0 :eps eps :verify? verify?)))
  
(defmethod symmetric-tridiagonal? ((t0 Matrix-Super)
				   &key
				   (verify? *verify-bands?*)
				   (eps 10.0d0)
				   &allow-other-keys)
  (and (symmetric? t0 :eps eps :verify? verify?)
       (tridiagonal? t0 :eps eps :verify? verify?))) 

;;; - - - - - - - - - - - - - - - - - - - - - - - - - - -

(defparameter *bandedness-nicknames*
	      '( upper-triangular? lower-triangular? diagonal?
		upper-hessenberg? lower-hessenberg? tridiagonal?
		upper-bidiagonal? lower-bidiagonal?))

(defmethod set-bandedness! ((t0 Matrix-Super) (prop Symbol)
			    &key
			    (verify? *verify-bands?*)
			    (coerce? nil) 
			    (eps 10.0d0))
  (case prop
    (upper-triangular? (setf (bands-start t0) 0))
    (lower-triangular? (setf (bands-end t0) 1))
    (diagonal? (setf (bands-start t0) 0) (setf (bands-end t0) 1))
    (upper-hessenberg? (setf (bands-start t0) -1))
    (lower-hessenberg? (setf (bands-end t0) 2))
    (tridiagonal? (setf (bands-start t0) -1) (setf (bands-end t0) 2))
    (upper-bidiagonal? (setf (bands-start t0) 0) (setf (bands-end t0) 2))
    (lower-bidiagonal? (setf (bands-end t0) 1) (setf (bands-start t0) -1))
    (otherwise (warn "unknown bandedness nickname ~a" prop)))
  (cond (coerce? (coerce-bands! t0))
	(verify? (assert (verify-bands? t0 :eps eps))))
  t0)

(defmethod set-default-bandedness! ((t0 Matrix-Super))
  (setf (bands-start t0) (min-band-coord t0))
  (setf (bands-end t0) (max-band-coord t0)))

;;;=======================================================
;;;		       Properties
;;;=======================================================
;;; Each property symbol should have a function definition which is a
;;; generic function whose methods are predicates that test the
;;; property. The generic functions must accept a keyword :verify?
;;; argument that suggests the method should do a more expensive test of
;;; the property, rather than relying on, for example, a cache of
;;; symbols.

(defvar *verify-properties?* nil) 

(defmethod add-property! ((t0 Matrix-Super) (prop Symbol)
			  &key
			  (verify? *verify-properties?*)
			  (coerce? nil)
			  (eps 10.0d0))
  (if (member prop *bandedness-nicknames*)
      (set-bandedness! t0 prop :verify? verify? :coerce? coerce? :eps eps)
      ;; else
      (case prop
	(positive-definite? 
	  (setf (properties t0)
		(delete 'negative-definite? (properties t0)))
	  (pushnew 'symmetric? (properties t0))
	  (when verify? (symmetric? t0 :verify? t :eps eps))
	  (pushnew 'positive-definite? (properties t0)))
	(negative-definite?
	  (setf (properties t0)
		(delete 'positive-definite? (properties t0)))
	  (pushnew 'symmetric? (properties t0))
	  (when verify? (symmetric? t0 :verify? t :eps eps))
	  (pushnew 'negative-definite? (properties t0)))
	((symmetric? unit-diagonal?)
	 (pushnew prop (properties t0))
	 (when verify? (funcall prop t0 :verify? t :eps eps)))
	((orthogonal? orthogonal-rows? orthogonal-columns? unitary?)
	 (pushnew prop (properties t0)))
	(otherwise
	  (warn "adding unknown property ~a" prop)
	  (pushnew prop (properties t0)))))
  t0)

(defmethod delete-property! ((t0 Matrix-Super) (prop Symbol))
  (case prop
    (positive-definite? 
      (setf (properties t0)
	    (delete 'positive-definite? (properties t0))))
    (negative-definite?
      (setf (properties t0)
	    (delete 'negative-definite? (properties t0))))
    (symmetric?
      (setf (properties t0)
	    (delete 'positive-definite? (properties t0)))
      (setf (properties t0)
	    (delete 'negative-definite? (properties t0)))
      (setf (properties t0)
	    (delete 'symmetric-definite? (properties t0))))
    ((orthogonal? orthogonal-rows? orthogonal-columns? unitary?
		  unit-diagonal?)
     (setf (properties t0) (delete prop (properties t0))))
    (otherwise
      (warn "deleting unknown property ~a" prop)
      (setf (properties t0) (delete prop (properties t0))))))


;;;-------------------------------------------------------

(defmethod positive-definite? ((t0 Matrix-Super)
			       &key
			       (verify? *verify-properties?*)
			       &allow-other-keys)
  (declare (ignore verify?))
  ;;(when verify? (warn "Can't verify positive definiteness for ~a" t0))
  (member 'positive-definite? (properties t0)))

(defmethod negative-definite? ((t0 Matrix-Super)
			       &key
			       (verify? *verify-properties?*)
			       &allow-other-keys)
  (declare (ignore verify?))
  ;;(when verify? (warn "Can't verify negative definiteness for ~a" t0))
  (member 'negative-definite? (properties t0)))

(defmethod orthogonal? ((t0 Matrix-Super)
			&key
			(verify? *verify-properties?*)
			&allow-other-keys)
  (declare (ignore verify?))
  ;;(when verify? (warn "Can't verify orthogonality for ~a" t0))
  (member 'orthogonal? (properties t0)))

(defmethod orthogonal-rows? ((t0 Matrix-Super)
			     &key
			     (verify? *verify-properties?*)
			     &allow-other-keys)
  (declare (ignore verify?))
  ;;(when verify? (warn "Can't verify orthogonality for ~a" t0))
  (or (orthogonal? t0)
      (member 'orthogonal-rows? (properties t0))))

(defmethod orthogonal-columns? ((t0 Matrix-Super)
				&key
				(verify? *verify-properties?*)
				&allow-other-keys)
  (declare (ignore verify?))
  ;;(when verify? (warn "Can't verify orthogonality for ~a" t0))
  (or (orthogonal? t0)
      (member 'orthogonal-columns? (properties t0))))

(defmethod unitary? ((t0 Matrix-Super)
		     &key
		     (verify? *verify-properties?*)
		     &allow-other-keys)
  (declare (ignore verify?))
  ;;(when verify? (warn "Can't verify unitarity for ~a" t0)) 
  (member 'unitary? (properties t0)))

(defmethod symmetric? ((t0 Matrix-Super)
		       &key
		       (verify? *verify-properties?*)
		       ;; Setting <eps> to a non-zero value permits
		       ;; testing for approximate symmetry.
		       (eps 10.0d0)
		       &allow-other-keys)
  (if verify?
      (and (automorphic? t0)
	   (let ((2d (2d-array t0))
		 (ioff (coord-start (codomain t0)))
		 (joff (coord-start (domain t0))))
	     (loop for i from 0 below (dimension (codomain t0))
		   always (loop for j from 0 below i
				for 2d.ij = (aref 2d (+ i ioff) (+ j joff))
				for 2d.ji = (aref 2d (+ j ioff) (+ i joff))
				always
				  (or (bm:small? (+ (abs 2d.ij) (abs 2d.ji))
						 eps)
				      (bm:small? (- 2d.ij 2d.ji)
						 (* eps
						    (+ 1.0d0
						       (abs 2d.ij)
						       (abs 2d.ji)))))))))
      ;; else check cache
      (member 'symmetric? (properties t0))))

;;;-------------------------------------------------------

(defmethod unit-diagonal? ((t0 Matrix-Super)
			   &key
			   (verify? *verify-properties?*)
			   (eps 10.0d0)
			   &allow-other-keys)
  (if verify?  
      (let ((2d (2d-array t0))
	    (i0 (coord-start (codomain t0)))
	    (j0 (coord-start (domain t0))))
	(block :test
	  (az:for (i (coord-start (domain t0)) (coord-end (domain t0)))
	    (unless (bm:small?  (- 1.0d0 (aref 2d (+ i i0) (+ i j0))) eps)
	      (return-from :test nil)))
	  (return-from :test t)))
      ;; else check the cache
      (member 'unit-diagonal? (properties t0))))
  
;;;=======================================================

(defmethod tail! ((t0 Matrix-Super))
  (restrict! t0 (tail-space (codomain t0)) (tail-space (domain t0))))

(defmethod codomain-tail! ((t0 Matrix-Super))
  (restrict! t0 (tail-space (codomain t0)) (domain t0)))

(defmethod domain-tail! ((t0 Matrix-Super))
  (restrict! t0 (codomain t0) (tail-space (domain t0))))

(defmethod restrict-to-square ((t0 Matrix-Super))
  (cond
    ((strictly-embedding? t0)
     (let* ((n (dimension (domain t0)))
	    (cod (block-subspace (codomain t0)
				 (coord-start (codomain t0))
				 (+ (coord-start (codomain t0)) n))))
       (make-instance 'Linear-Product
		      :factors
		      (list (make-instance
			      'Block-Embedding
			      :domain cod :codomain (codomain t0))
			    (restrict t0 cod)))))
    ((strictly-projecting? t0)
     (let* ((m (dimension (codomain t0)))
	    (dom (block-subspace (domain t0)
				 (coord-start (domain t0))
				 (+ (coord-start (domain t0)) m))))
       (make-instance 'Linear-Product
		      :factors
		      (list (restrict t0 (codomain t0) dom)
			    (make-instance
			      'Block-Projection
			      :domain (domain t0) :codomain dom)))))))
    
;;;=======================================================

(defmethod index-of-abs-max-in-col ((t0 Matrix-Super) j)
  (bm:v-abs-max-index
    (2d-array t0)
    :start (coord-start (codomain t0))
    :end (coord-end (codomain t0))
    :vtype :col :on j))

;;;=======================================================

(defmethod zero! ((t0 Matrix-Super) &key (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space)) 
  
  (let* ((2d (2d-array t0))
	 (start (coord-start (domain t0)))
	 (end (coord-end (domain t0))))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
      (bm:v<-x! 2d 0.0d0 :start start :end end :vtype :row :on i)))
  t0)

;;;=======================================================

(defmethod transform-to! ((t0 Matrix-Super)
			  (vd Lvector-Super)
			  (vr Lvector-Super))
  (assert (subspace? (home-space vd) (domain t0)))
  (assert (eq (home-space vr) (codomain t0)))

  (let* ((2d (2d-array t0))
	 (1dd (1d-array vd))
	 (s (coord-start vd))
	 (e (coord-end vd))
	 (n (- e s)))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0))) 
      (setf (coordinate vr i)
	    (bm:v.v 2d 1dd
		     :start0 s :end0 e :vtype0 :row :on0 i
		     :end1 n))))
  vr)

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Matrix-Super)
				    (vd Lvector-Super)
				    (vr Lvector-Super))
  ;; a temporary restriction:
  (assert (subspace? (home-space vd) (dual-space (codomain t0))))
  (assert (eq (home-space vr) (dual-space (domain t0))))
  
  (let* ((2d (2d-array t0))
	 (1dd (1d-array vd))
	 (s (coord-start vd))
	 (e (coord-end vd))
	 (n (- e s)))
    (az:for (i (coord-start (domain t0)) (coord-end (domain t0))) 
      (setf (coordinate vr i)
	    (bm:v.v 2d 1dd
		     :start0 s :end0 e :vtype0 :col :on0 i
		     :end1 n))))
  vr)

;;;-------------------------------------------------------

(defmethod transpose ((t0 Matrix-Super)
		      &key
		      (result
			(make-instance (class-of t0)
				       :domain (dual-space (codomain t0))
				       :codomain (dual-space (domain t0)))
			result-supplied?))
  (when result-supplied?
    (az:type-check Matrix-Super result)
    (assert (eql (domain t0) (codomain result)))
    (assert (eql (domain result) (codomain t0))))

  (let ((cod (codomain t0))
	(dom (domain t0)))
    (cond ((symmetric? t0)
	   (return-from transpose (az:copy t0 :result result)))
	  ((eq t0 result)
	   (return-from transpose (transpose! t0)))
	  ((projecting? t0)
	   (with-borrowed-element (v dom)
	     (az:for (i (coord-start cod) (coord-end cod))
	       (setf (col result i) (row t0 i :result v)))))
	  ((embedding? t0)
	   (with-borrowed-element (v cod)
	     (az:for (i (coord-start dom) (coord-end dom))
	       (setf (row result i) (col t0 i :result v)))))
	  (t (error "shouldn't ever get here."))))
  
  (cond ((orthogonal? t0) (add-property! result 'orthogonal?))
	((orthogonal-columns? t0) (add-property! result 'orthogonal-rows?))
	((orthogonal-rows? t0) (add-property! result 'orthogonal-columns?)))
  (setf (bands-end result) (- 1 (bands-start t0)))
  (setf (bands-start  result) (- 1 (bands-end t0)))
  result) 

;;;=======================================================
;;;			 Matrix
;;;=======================================================

(defclass Matrix (Matrix-Super)
     ((2d-array
	:type Array
	:initarg :2d-array
	:reader 2d-array)))

;;;-------------------------------------------------------
;;; a pseudo slot, for compatability with Restricted-Matrix's

(defmethod source ((t0 Matrix)) t0)

;;;-------------------------------------------------------
;;;	   make- functions and initialization
;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 Matrix)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (unless (slot-boundp t0 '2d-array)	   
    (setf (slot-value t0 '2d-array)
      (az:make-float-array
       (list (dimension (codomain t0))
	     (dimension (domain t0))))))
  (unless (slot-boundp t0 'bands-start)
    (setf (bands-start t0) (min-band-coord t0)))
  (unless (slot-boundp t0 'bands-end)
    (setf (bands-end   t0) (max-band-coord t0)))
  (assert (= (array-dimension (2d-array t0) 1)
	     (dimension (domain t0))))
  (assert (= (array-dimension (2d-array t0) 0)
	     (dimension (codomain t0))))
  (assert (verify-bands? t0))
  (assert-properties t0))

;;;-------------------------------------------------------

(defmethod diagonal-block? ((t0 Matrix)) t)

;;;-------------------------------------------------------

(defmethod (setf bands-start) ((start Integer) (t0 Matrix))
  (setf (slot-value t0 'bands-start)
	(az:bound (min-band-coord t0) start (max-band-coord t0)))
  start)

(defmethod (setf bands-end) ((end Integer) (t0 Matrix))
  (setf (slot-value t0 'bands-end)
	(az:bound (min-band-coord t0) end (max-band-coord t0)))
  end)

;;;-------------------------------------------------------

(defmethod restrict ((t0 Matrix)
		     &optional
		     (codomain (codomain t0))
		     (domain (domain t0))
		     &key
		     (result
		      (make-instance 'Restricted-Matrix
			:source t0
			:codomain codomain
			:domain domain)
		      result-supplied?))
  (when result-supplied?
    (setf (slot-value result 'source) t0)
    (setf (slot-value result 'domain) domain)
    (setf (slot-value result 'codomain) codomain)
    (reinitialize-instance result))
  result)

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Matrix))
  (make-instance 'Matrix
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :2d-array (az:copy (2d-array t0))
		 :bands-start (bands-start t0)
		 :bands-end   (bands-end   t0)
		 :properties (copy-seq (properties t0))))

(defmethod az:copy-to! ((t0 Matrix) (result Matrix))
  (az:copy (2d-array t0) :result (2d-array result))
  (setf (bands-start result) (bands-start t0))
  (setf (bands-end   result) (bands-end   t0))
  (setf (properties result) (copy-seq (properties t0)))
  result)

;;;-------------------------------------------------------
;;; coerce an arbitrary Mapping into a Matrix

(defmethod copy-to-matrix ((t0 Linear-Mapping)
			   &key
			   (result (make-instance 'Matrix 
					      :domain (domain t0)
					      :codomain (codomain t0))
			       result-supplied?))
  (when result-supplied?
    (when (eq t0 result) (warn "Copying ~s to itself." t0))
    (assert (eql (domain t0) (domain result)))
    (assert (eql (codomain t0) (codomain result))))
  (with-borrowed-element (v (domain t0))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
      (setf (row result i) (row t0 i :result v))))
  (setf (bands-start result) (bands-start t0))
  (setf (bands-end result) (bands-end t0))
  (map nil #'(lambda (prop)
	       (when (funcall prop t0) (add-property! result prop)))
       *properties-of-linear-mappings-preserved-by-copy-to-matrix*)
  result)

(defmethod copy-to-matrix ((t0 Matrix) &key (result nil))
  (when (eq t0 result) (warn "Copying ~s to itself." t0))
  (if (null result) (az:copy t0) (az:copy t0 :result result)))

;;;=======================================================

(defmethod copy-to-array ((t0 Linear-Mapping)
			  &key
			  (result (az:make-float-array
				    (list (dimension (codomain t0))
					  (dimension (domain t0))))
				  result-supplied?))
  (when result-supplied?
    (assert (eql (dimension (domain t0)) (array-dimension result 1)))
    (assert (eql (dimension (codomain t0)) (array-dimension result 0))))
  (let* ((dom (domain t0))
	 (cod (codomain t0))
	 (s (coord-start dom))
	 (e (coord-end dom))
	 (e-s (- e s)))
    (with-borrowed-element (v dom)
      (let ((1d (1d-array v)))
	(az:for (i (coord-start cod) (coord-end cod))
	  (row t0 i :result v)
	  (bm:v<-v! result 1d
		     :start0 s :end0 e :vtype0 :row :on0 i
		     :end1 e-s)))))
  result)

;;;-------------------------------------------------------

(defmethod copy-to-array ((t0 Matrix)
			  &key
			  (result (az:make-float-array
				    (list (dimension (codomain t0))
					  (dimension (domain t0))))
				  result-supplied?))
  (when result-supplied?
    (assert (eql (dimension (domain t0)) (array-dimension result 1)))
    (assert (eql (dimension (codomain t0)) (array-dimension result 0)))
    (not (eq result (2d-array t0))))
  (az:copy (2d-array t0) :result result)) 

;;;-------------------------------------------------------

(defun make-matrix (nrows &optional (ncols nrows))
  (make-instance
    'Matrix
    :2d-array (az:make-float-array (list nrows ncols) :initial-element 0.0d0)
    :domain (LVector-Space ncols)
    :codomain (LVector-Space nrows)))

(defun read-matrix-from-file (pathname nrows &optional (ncols nrows))
  (with-open-file (st pathname :direction :input)
    (let ((2d (az:make-float-array (list nrows ncols))))
      (dotimes (i nrows) (dotimes (j ncols) (setf (aref 2d i j) (read st))))
      (make-instance 'Matrix
		     :2d-array 2d
		     :domain (LVector-Space ncols)
		     :codomain (LVector-Space nrows)))))

(defun write-matrix-to-file (pathname t0)
  (az:type-check Matrix-Super t0)
  (with-open-file (st pathname :direction :output)
    (let ((2d (2d-array t0)))
      (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	(az:for (j (coord-start (domain t0)) (coord-end (domain t0)))
	  (print (aref 2d i j) st))))))

;;;-------------------------------------------------------

(defun make-matrix-from-array (a)
  (make-instance 'Matrix
		 :2d-array a
		 :domain (LVector-Space (array-dimension a 1))
		 :codomain (LVector-Space (array-dimension a 0))))

;;;-------------------------------------------------------

(defun make-identity-matrix (vspace)
  (let* ((dim (dimension vspace))
	 (t0 (make-matrix dim)))
    (identity! t0)
    (add-property! t0 'positive-definite?)
    t0))

;;;-------------------------------------------------------
;;; make a Mapping from <vs1> to <vs0>,
;;; following convention of codomain first.

(defmethod make-linear-mapping ((vs0 LVector-Space)
				(vs1 LVector-Space)
				&rest options)
  (declare (ignore options))
  (make-matrix (dimension vs0) (dimension vs1)))

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Matrix) (vd Lvector) (vr Lvector))
  (let ((2d (2d-array t0))
	(1dd (1d-array vd))
	(1dr (1d-array vr))
	(m (dimension (codomain t0)))
	(n (dimension (domain t0))))
    (cond
      ((upper-triangular? t0)
       (dotimes (i (min m n))
	 (setf (aref 1dr i)
              (bm:v.v 2d 1dd
		       :start0 i :end0 n :vtype0 :row :on0 i
		       :start1 i :end1 n)))
       (when (> m n)
	 (bm:v<-x! 1dr 0.0d0 :start n :end m)))
      ((lower-triangular? t0)
       (dotimes (i (min m n))
	 (let ((i+1 (+ i 1)))
	   (setf (aref 1dr i)
                (bm:v.v 2d 1dd
			 :end0 i+1 :vtype0 :row :on0 i
			 :end1 i+1))))
       (az:for (i n m)
	 (setf (aref 1dr i) 
              (bm:v.v 2d 1dd
		       :end0 n :vtype0 :row :on0 i
		       :end1 n))))
      (t
       (dotimes (i (dimension (codomain t0)))
	 (setf (aref 1dr i)
	       (bm:v.v 2d 1dd
			:end0 n :vtype0 :row :on0 i
			:end1 n))))))
  vr)

;;;-------------------------------------------------------

(defmethod transpose! ((t0 Matrix))
  (cond
    ((symmetric? t0) t0)
    ((automorphic? t0)
     (let ((2d (2d-array t0)))
       (dotimes (i (dimension (codomain t0)))
	 (dotimes (j i)
	   (rotatef (aref 2d i j) (aref 2d j i)))))
     (let ((start (bands-start t0))
	   (end (bands-end t0)))
       (setf (bands-end t0) (- 1 start))
       (setf (bands-start  t0) (- 1 end)))
     t0)
    (t  ;; don't know how to re-use the space
     (transpose t0))))

;;;-------------------------------------------------------

(defmethod identity! ((t0 Matrix))
  
  (let ((2d (2d-array t0))
	(n (dimension (domain t0))))
    (dotimes (i (dimension (codomain t0)))
      (bm:v<-x! 2d 0.0d0 :end n :vtype :row :on i)
      (when (< i n) (setf (aref 2d i i) 1.0d0))))
  t0)

;;;=======================================================
;;;		       Restricted-Matrix
;;;=======================================================
;;;
;;; Restricted-Matrix's are typically used for efficiency within
;;; decompose algorithms and are usually only composed with Modifier
;;; Mappings.
;;; 
;;; A Restricted-Matrix is a Mapping between block
;;; subspaces of the domain and codomain of its source. The codomain
;;; and domain of a Restricted-Matrix are usually Block-Subspaces,
;;; but in the boundary case, where the submatrix covers its source,
;;; they are the same as the codomain and domain of the source.

(defclass Restricted-Matrix (Matrix-Super Indirect-Mapping) ())

;;;-------------------------------------------------------

(defmethod 2d-array ((t0 Restricted-Matrix)) (2d-array (source t0)))

(defmethod (setf codomain) ((new Euclidean-Vector-Space)
			    (t0 Restricted-Matrix))
  (unless (eql new (codomain t0))
    (let ((delta-bc (- (coord-start new) (coord-start (codomain t0)))))
      (incf (bands-start t0) delta-bc)
      (incf (bands-end   t0) delta-bc))
    (setf (slot-value t0 'codomain) new)
    (reinitialize-instance t0))
  new)

(defmethod (setf domain) ((new Euclidean-Vector-Space)
			  (t0 Restricted-Matrix))
  (unless (eql new (domain t0))
    (let ((delta-bc (- (coord-start (domain t0)) (coord-start new))))
      (incf (bands-start t0) delta-bc)
      (incf (bands-end   t0) delta-bc))
    (setf (slot-value t0 'domain) new)
    (reinitialize-instance t0))
  new)

;;;-------------------------------------------------------

(defmethod print-object ((t0 Restricted-Matrix) str)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 str)
	(format str "~@[~a ~]~:(~a~) ~a->~a"
		(mapping-adjective t0)
		(class-name (class-of t0))
		(domain t0)
		(codomain t0)))
      (call-next-method)))

;;;-------------------------------------------------------

(defmethod restrict! ((t0 Restricted-Matrix)
		      &optional
		      (codomain (codomain t0))
		      (domain (domain t0)))
  (unless (and (eql codomain (codomain t0)) (eql domain (domain t0)))
    (setf (slot-value t0 'codomain) codomain)
    (setf (slot-value t0 'domain) domain)
    (setf (bands-start t0) (min-bands-start t0))
    (setf (bands-end t0) (max-bands-end t0))
    (reinitialize-instance t0))
  t0)

(defmethod restrict ((t0 Restricted-Matrix)
		     &optional
		     (codomain (codomain t0))
		     (domain (domain t0))
		     &key
		     (result
		      (make-instance 'Restricted-Matrix
			:source (source t0)
			:codomain codomain
			:domain domain)
		      result-supplied?))
  (when result-supplied?
    (setf (slot-value result 'source) t0)
    (setf (slot-value result 'domain) domain)
    (setf (slot-value result 'codomain) codomain)
    (reinitialize-instance result))
  result)

;;;-------------------------------------------------------    
;;; Infer the maximum bandedness of a Restricted-Matrix from
;;; the bandedness of its source:

(defmethod min-bands-start ((t0 Restricted-Matrix))
  (az:bound (min-band-coord t0)
	    (+ (bands-start (source t0))
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord t0)))

(defmethod max-bands-end ((t0 Restricted-Matrix))
  (az:bound (min-band-coord t0)
	    (+ (bands-end (source t0))
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord t0)))

;;;-------------------------------------------------------    
;;; Compute the implied minimum bandedness of the source Matrix from
;;; the bandedness of a Restricted-Matrix:

(defmethod source-max-bands-start ((t0 Restricted-Matrix))
  (az:bound (min-band-coord (source t0))
	    (+ (bands-start t0)
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord (source t0))))

(defmethod source-min-bands-end ((t0 Restricted-Matrix))
  (az:bound (min-band-coord (source t0))
	    (+ (bands-end t0)
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord (source t0))))

;;;-------------------------------------------------------

(defmethod (setf bands-start) ((start Integer) (t0 Restricted-Matrix))
  (setf (slot-value t0 'bands-start)
	(az:bound (min-band-coord t0) start (max-band-coord t0)))
  (setf (bands-start (source t0))
	(min (source-max-bands-start t0) (bands-start (source t0))))
  start)

(defmethod (setf bands-end) ((end Integer) (t0 Restricted-Matrix))
  (setf (slot-value t0 'bands-end)
	(az:bound (min-band-coord t0) end (max-band-coord t0)))
  (setf (bands-end (source t0))
	(max (source-min-bands-end t0) (bands-end (source t0))))
  end)

;;;-------------------------------------------------------

(defmethod diagonal-block? ((t0 Restricted-Matrix))
  (and (= (coord-start (codomain t0)) (coord-start (domain t0)))
       (= (coord-end (codomain t0)) (coord-end (domain t0)))))

;;;-------------------------------------------------------
;;; Infer whatever we can that is true of the submatrix
;;; based on its source:

(defmethod shared-initialize :after ((t0 Restricted-Matrix)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names

  (unless (slot-boundp t0 'bands-start)
    (setf (slot-value t0 'bands-start) (min-bands-start t0)))
  (unless (slot-boundp t0 'bands-end)
    (setf (slot-value t0 'bands-end) (max-bands-end t0)))
  (let ((source (source t0))) 
    (cond
     ;; the boundary case---t0 covers its source
     ((and (eql (codomain t0) (codomain source))
	   (eql (domain t0) (domain source)))
      (setf (properties t0) (copy-list (properties source))))
     (t
      (delete-all-properties! t0)
      ;; in the general case, symmetry is the only inference
      ;; at present.
      (when (and (diagonal-block? t0) (symmetric? source))
	(add-property! t0 'symmetric?))))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Restricted-Matrix))
  (make-instance 'Restricted-Matrix
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :source (source t0)) )

(defmethod az:copy-to! ((t0 Restricted-Matrix)
		     (result Restricted-Matrix))
  (assert (eql (domain t0) (domain result)))
  (assert (eql (codomain t0) (codomain result)))
  (setf (source result) (source t0))
  result)

;;;-------------------------------------------------------

(defun make-Restricted-Matrix (t0
			       &key
			       (start-row 0)
			       (end-row (dimension (codomain t0)))
			       (start-col 0)
			       (end-col (dimension (domain t0))))
  (az:type-check Matrix t0)
  (make-instance
    'Restricted-Matrix
    :source t0
    :codomain (block-subspace (codomain t0) start-row end-row)
    :domain   (block-subspace (domain   t0) start-col end-col)))

;;;=======================================================

(defclass Inverse-Upper-Triangular-Matrix (Inverse Linear-Mapping) ())

(defclass Inverse-Lower-Triangular-Matrix (Inverse Linear-Mapping) ())

;;;-------------------------------------------------------
;;; Back substitution:

(defmethod transform-to! ((t0 Inverse-Upper-Triangular-Matrix)
			  (vd Lvector)
			  (vr Lvector))
  (assert (not (bm:diagonal-zeros? (2d-array (source t0)))))
  (let* ((t- (source t0))
	 (2d (2d-array t-))
	 (m (dimension (codomain t0)))
	 (n (dimension (domain t0)))
	 (end (min m n))
	 (e-1 (- end 1))
	 (1dr (1d-array vr))
	 (1dd (1d-array vd)))
    (setf (aref 1dr e-1) (/ (aref 1dd e-1) (aref 2d e-1 e-1)))
    (loop for i downfrom  (- e-1 1) to 0
	  for i+1 = (+ i 1)
	  do (setf (aref 1dr i)
		   (/ (- (aref 1dd i)
			 (bm:v.v 2d 1dr
				  :start0 i+1 :end0 end :vtype0 :row :on0 i
				  :start1 i+1 :end1 end))
		      (aref 2d i i))))
    (when (< end m) (bm:v<-x! 1dr 0.0d0 :start end :end m)))
  vr) 

;;;-------------------------------------------------------
;;; Transposed Forward elimination:

(defmethod transform-transpose-to! ((t0 Inverse-Upper-Triangular-Matrix)
				    (vd Lvector)
				    (vr Lvector))
  (assert (not (bm:diagonal-zeros? (2d-array (source t0)))))
  (let* ((t- (source t0))
	 (2d (2d-array t-))
	 (1dr (1d-array vr))
	 (1dd (1d-array vd))
	 (m (dimension (dual-space (codomain t0))))
	 (n (dimension (dual-space (domain t0)))))
    (setf (aref 1dr 0) (/ (aref 1dd 0) (aref 2d 0 0)))
    (az:for (i 1 (min m n))
      (setf (aref 1dr i)
	    (/ (- (aref 1dd i) (bm:v.v 2d 1dr
					:end0 i :vtype0 :col :on0 i
					:end1 i))
	       (aref 2d i i))))
    (when (> n m) (bm:v<-x! 1dr 0.0d0 :start m :end n)))
  vr)

;;;-------------------------------------------------------

(defmethod transpose ((t0 Inverse-Upper-Triangular-Matrix) 
		      &key
		      (result
			(make-instance 'Inverse-Lower-Triangular-Matrix
				       :source (transpose (source t0)))
			result-supplied?))
  (when result-supplied?
    (az:type-check Inverse-Lower-Triangular-Matrix result)
    (setf (source result) (transpose (source t0))))
  result) 

;;;=======================================================
;;; Forward elimination:

(defmethod transform-to! ((t0 Inverse-Lower-Triangular-Matrix)
			  (vd Lvector)
			  (vr Lvector))
  (assert (not (bm:diagonal-zeros? (2d-array (source t0)))))
  (let* ((t- (source t0))
	 (2d (2d-array t-))
	 (m (dimension (codomain t0)))
	 (n (dimension (domain t0)))
	 (1dr (1d-array vr))
	 (1dd (1d-array vd)))
    (setf (aref 1dr 0) (/  (aref 1dd 0) (aref 2d 0  0)))
    (az:for (i 1 (min m n))
      (setf (aref 1dr i)
	    (/ (- (aref 1dd i) (bm:v.v 2d 1dr
					:end0 i :vtype0 :row :on0 i
					:end1 i))
	       (aref 2d i i))))
    (when (> m n) (bm:v<-x! 1dr 0.0d0 :start n :end m)))
  
  vr)

;;;-------------------------------------------------------
;;; Transposed Back substitution:

(defmethod transform-transpose-to! ((t0 Inverse-Lower-Triangular-Matrix)
				    (vd Lvector)
				    (vr Lvector))
  (assert (not (bm:diagonal-zeros? (2d-array (source t0)))))
  (let* ((t- (source t0))
	 (2d (2d-array t-))
	 (m (dimension (dual-space (codomain t0))))
	 (n (dimension (dual-space (domain t0))))
	 (end (min m n))
	 (e-1 (- end 1))
	 (1dr (1d-array vr))
	 (1dd (1d-array vd)))
    (setf (aref 1dr e-1) (/ (aref 1dd e-1) (aref 2d e-1 e-1)))
    (loop for i downfrom  (- e-1 1) to 0
	  for i+1 = (+ i 1)
	  do (setf (aref 1dr i)
		   (/ (- (aref 1dd i)
			 (bm:v.v 2d 1dr
				  :start0 i+1 :end0 end :vtype0 :col :on0 i
				  :start1 i+1 :end1 end))
		      (aref 2d i i))))
    (when (< end n) (bm:v<-x! 1dr 0.0d0 :start end :end n)))
  vr) 

;;;-------------------------------------------------------

(defmethod transpose ((t0 Inverse-Lower-Triangular-Matrix) 
		      &key
		      (result
			(make-instance 'Inverse-Upper-Triangular-Matrix
				       :source (transpose (source t0)))
			result-supplied?))
  (when result-supplied?
    (az:type-check Inverse-Upper-Triangular-Matrix result)
    (setf (source result) (transpose (source t0))))
  result) 

