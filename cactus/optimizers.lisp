;;;-*- Package: Cactus; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;============================================================

(in-package :Cactus)

;;;============================================================
;;; Optimizers
;;;============================================================

(defclass Optimizer (Cactus-Object)
     ((curr-p
	:type Flat-Point
	:accessor curr-p 
	:initarg :curr-p)
      (prev-p
	:type Flat-Point
	:accessor prev-p 
	:initarg :prev-p)
      (curr-val
	:type Double-Float
	:accessor curr-val
	:initform most-positive-double-float)
      (prev-val
	:type Double-Float
	:accessor prev-val
	:initform most-positive-double-float)
      (tolerance
	:type Double-Float
	:accessor tolerance
	:initarg :tolerance)
      (objective-f
	:type Objective-Function
	:accessor objective-f
	:initarg :objective-f)
      (iteration-count
	:type az:Positive-Fixnum
	:accessor iteration-count
	:initarg :iteration-count)
      (max-iterations
	:type az:Positive-Fixnum
	:accessor max-iterations
	:initarg :max-iterations))
  (:default-initargs
    :tolerance 1.0d-5
    :iteration-count 0
    :max-iterations 1000))

;;;------------------------------------------------------------

(defmethod domain ((op Optimizer)) (domain (objective-f op)))

(defmethod evaluation-count ((op Optimizer))
  (evaluation-count (objective-f op)))

(defmethod (setf evaluation-count) (count (op Optimizer))
  (setf (evaluation-count (objective-f op)) count))

;;;------------------------------------------------------------

(defmethod initialize-optimizer :before ((op Optimizer) ob start)
	   (az:type-check Objective-Function ob)
	   (assert (element? start (domain ob)))
	   (clear ob))

;;;------------------------------------------------------------
;;; This should work for most Optimizers.
;;; It relies on the following generic functions:
;;; <initialize-optimizer>, <loop-optimizer>.

(defmethod optimize ((op Optimizer)
		     (ob Objective-Function)
		     &optional
		     (start (curr-p op)))
  (assert (element? start (domain ob)))
  (catch :end-search
    (initialize-optimizer op ob start)
    (loop-optimizer op)))

;;;------------------------------------------------------------
;;; This is a loop which should work for most Optimizers.
;;; It relies on the following generic functions:
;;; <step-optimizer> and <converged-optimizer?>

(defmethod loop-optimizer ((op Optimizer))
  (loop (incf (iteration-count op))
    (when (>= (iteration-count op) (max-iterations op))
      (warn "too many iterations")
      (throw :end-search (values (curr-val op) (curr-p op))))
    (step-optimizer op))) 

;;;============================================================
;;; Optimizers that use first derivatives:
;;;============================================================

(defclass Gradient-Optimizer (Optimizer)
     ((curr--g
	:type Abstract-Vector
	:accessor curr--g)
      (prev--g
	:type Abstract-Vector
	:accessor prev--g)))

;;;------------------------------------------------------------
;;; This little indirection makes it possible for discrete and
;;; analytical gradient strategies to share the <step-optimizer>
;;; method.

(defmethod -grad-f ((op Gradient-Optimizer)) 'minus-grad)

;;;============================================================

(defclass Discrete-Gradient-Optimizer (Optimizer)
     ((-grad-f
       :type Function
       :accessor -grad-f)
      (forward-difference-step
       :type Abstract-Vector 
       :accessor forward-difference-step)
      (central-difference-step
       :type Abstract-Vector
       :accessor central-difference-step)))

;;;------------------------------------------------------------

(defmethod optimize ((op Discrete-Gradient-Optimizer) ob
		     &optional
		     (start
		      (progn
			(az:type-check Objective-Function ob)
			(make-random-element (domain ob)))))
  (az:type-check Objective-Function ob)
  (assert (element? start (domain ob)))

  (setf (forward-difference-step op)
    (forward-dif-stepsize ob start
			  (make-element (translation-space (domain ob)))))
  (setf (central-difference-step op)
    (central-dif-stepsize ob start
			  (make-element (translation-space (domain ob)))))
  (setf (-grad-f op)
    #'(lambda (ob p dir)
	(assert (element? p (domain ob)))
	(assert (element? dir (translation-space (domain ob))))

	(forward-dif-minus-grad ob p
				(forward-difference-step op)
				(curr-val op)
				dir)))	    
  (catch :end-search
    (initialize-optimizer op ob start)
  
    ;; First iterate with the forward difference gradient, which is
    ;; usually about twice as fast as central differences, but not
    ;; accurate close to the solution
    (loop-optimizer op)

    (print "!!!!!!!!!CENTRAL DIFFERENCES!!!!!!!!!!")

    ;; Then switch to the more expensive central difference gradient for a
    ;; final (hopefully) short iteration to get an accurate answer
    (setf (-grad-f op)
      #'(lambda (ob p dir)
	  (assert (element? p (domain ob)))
	  (assert (element? dir (translation-space (domain ob))))

	  (central-dif-minus-grad ob p
				  (central-difference-step op)
				  (curr-val op)
				  dir)))	    
    (loop-optimizer op))) 

;;;============================================================
;;; Optimizers that use second derivatives
;;;============================================================

(defclass Hessian-Optimizer (Optimizer)
     ((hess
       :type Linear-Mapping
       :accessor hess)))

;;;------------------------------------------------------------

(defmethod hess-f ((op Hessian-Optimizer)) 'hessian)

;;;============================================================

(defclass Discrete-Hessian-Optimizer (Hessian-Optimizer)
     ((hess-f
       :type Function
       :accessor hess-f)))

;;;============================================================
;;; Newton Methods 
;;;============================================================

(defclass Newton-Optimizer (Gradient-Optimizer
			     Hessian-Optimizer)
     ())

;;;============================================================
;;; Simplex methods (not linear programming):
;;; see Press et al., Numerical Recipes in C, Sec 10.4.
;;;============================================================

(defconstant -amoeba-optimizer-alpha- -1.0d0)
(defconstant -amoeba-optimizer-beta-   0.5d0)
(defconstant -amoeba-optimizer-gamma-  2.0d0)

(defclass Amoeba-Optimizer (Optimizer)
     ((vertex-sum
	:type Flat-Point
	:accessor vertex-sum)
      (vertices
	:type Vector ;; of Flat-Points
	:accessor vertices)
      (vertex-values
	:type az:Float-Vector 
	:accessor vertex-values)
      (worst-vertex
	:type az:Array-Index
	:accessor worst-vertex)
      (next-worst-vertex
	:type az:Array-Index
	:accessor next-worst-vertex)
      (best-vertex
	:type az:Array-Index
	:accessor best-vertex))
  (:default-initargs
    :max-iterations 10000
    :tolerance 0.001))

;;;------------------------------------------------------------

(defmethod initialize-optimizer ((op Amoeba-Optimizer) ob start)
  (az:type-check Objective-Function ob)
  (assert (element? start (domain ob)))

  (setf (iteration-count op) 0)
  (setf (objective-f op) ob)
  (setf (curr-p op) start)
  (setf (curr-val op) (transform ob (curr-p op)))
  (setf (vertices op) (amoeba-initialize ob start))
  (setf (vertex-sum op) (make-element (domain op)))
  (amoeba-sum (vertices op) (vertex-sum op))
  (setf (vertex-values op) (map 'Vector
				#'(lambda (p) (transform ob p))
				(vertices op)))
  (search-vertices op)
  (setf (prev-val op) (aref (vertex-values op) (worst-vertex op)))
;  (initialize-animator ob
;		       (aref (vertices op) (best-vertex op))
;		       0.0d0
;		       (aref (vertex-values op) (best-vertex op)))
  )
;;;------------------------------------------------------------

;;; random initialization
;(defun amoeba-initialize (sc start &optional (step 1.0d0))
;  (declare (type Objective-Function sc)
;	   (type Abstract-Point start)
;	   (type Double-Float step))
;  (let* ((n (dimension (domain sc)))
;	 (p (make-array (+ n 1))))
;    (setf (aref p n) (az:copy start :result (make-element (domain sc))))
;    (dotimes (k n)
;      (setf start (fill-randomly! start :min -1.0d0 :max 1.0d0))
;      (setf (aref p k)
;	     (add (aref p n) start :result (make-element (domain sc)))))
;    p))

;;; optimized canonical basis
;(defun amoeba-initialize (sc start
;			  &key
;			  (step 1.0d0)
;			  (amoeba (make-array (+ (dimension (domain sc)) 1)))
;			  (temp (make-element (domain sc))))
;  (declare (type Objective-Function sc)
;	   (type Abstract-Point start)
;	   (type Double-Float step)
;	   (type Vector amoeba))
;  (setf (aref amoeba (dimension (domain sc))) (az:copy start :result (make-element (domain sc))))
;  (dotimes (k (dimension (domain sc)))
;    (let ((ek (fill-with-canonical-basis-vector! (make-element (domain sc)) k)))
;      (setf step (bm:minimize-1d
;		   #'(lambda (a)
;		       (move-by start a ek :result temp)
;		       (transform sc temp))
;		   (- step) step :relative-tolerance 0.01))
;      (setf (aref amoeba k) (move-by start step ek :result ek))))
;  amoeba)

;;; canonical basis
(defun amoeba-initialize (sc start
			  &key
			  (step 1.0d0)
			  (amoeba (make-array (+ (dimension (domain sc)) 1))))
  (declare (type Objective-Function sc)
	   (type Abstract-Point start)
	   (type Double-Float step)
	   (type Vector amoeba))
  (setf (aref amoeba (dimension (domain sc)))
	(az:copy start :result (make-element (domain sc))))
  (with-borrowed-element (dk (translation-space (domain sc)))
    (dotimes (k (dimension (domain sc)))
      (fill-with-canonical-basis-vector! dk k)
      (setf (aref amoeba k) (move-by start step dk))))
  amoeba)

;;;------------------------------------------------------------

(defun amoeba-sum (p psum)
  (az:type-check Sequence p)
  (az:type-check Flat-Point psum)
  (zero! psum)
  (map nil #'(lambda (st) (move-by st 1.0d0 psum :result psum)) p))

;;;------------------------------------------------------------

(defmethod converged-optimizer? ((op Amoeba-Optimizer))
  (bm:close-enough? (aref (vertex-values op) (worst-vertex op))
		    (aref (vertex-values op) (best-vertex op))
		    (tolerance op)))

;;;------------------------------------------------------------

(defmethod step-optimizer ((op Amoeba-Optimizer))

  (search-vertices op)
  (when (> (prev-val op) (aref (vertex-values op) (best-vertex op)))
    (setf (prev-val op) (aref (vertex-values op) (best-vertex op)))) 
  (step-amoeba op -amoeba-optimizer-alpha-)
  (cond
    ((< (curr-val op) (aref (vertex-values op) (best-vertex op)))
     ;; it worked, try a little more:
     ;;(format t "~%more")
     (step-amoeba op -amoeba-optimizer-gamma-))
    ((>= (curr-val op) (aref (vertex-values op) (next-worst-vertex op)))
     ;; Didn't work, try an intermediate point:
     ;;(format t "~%less")
     (let ((lsave (aref (vertex-values op) (worst-vertex op))))
       (step-amoeba op -amoeba-optimizer-beta-)
       ;; when still no good, shrink towards (best-vertex op):
       (when (> (curr-val op) lsave) (shrink-amoeba op)))))
  ;;(animate-optimizer op)
  (when (converged-optimizer? op)
    (throw :end-search (values (curr-val op) (curr-p op)))))

;;;------------------------------------------------------------
;;; Extrapolates by a factor <alpha> through the face of the amoeba
;;; across from the <ihi> vertex, and replaces the ihi vertex
;;; if the new one is better

(defun step-amoeba (op amount)
  (az:type-check Amoeba-Optimizer op)
  (az:type-check Double-Float amount)
  (assert (not (zerop (dimension (domain (objective-f op))))))
  (let* ((psum (vertex-sum op))
	 (ihi (worst-vertex op))
	 (p (vertices op))
	 (y (vertex-values op))
	 (amount1 (/ (- 1.0d0 amount)
		     (dimension (domain (objective-f op)))))
	 (amount2 (- amount amount1))
	 (curr-val most-positive-double-float))
    (declare (type Flat-Point psum)
	     (type az:Array-Index ihi)
	     (type Vector p)
	     (type az:Float-Vector y)
	     (type Double-Float amount dim amount1 amount2 curr-val))
    (linear-mix amount1 psum amount2 (aref p ihi) :result (curr-p op))
    (setf curr-val (transform (objective-f op) (curr-p op)))
    (when (< curr-val (aref y ihi))
      ;; update psum
      (move-by (curr-p op) 1.0d0 psum :result psum)
      (sub psum (aref p ihi) :result psum)
      ;; install the new worst position
      (az:copy (curr-p op) :result (aref p ihi))
      (setf (aref y ihi) curr-val))
    (setf (curr-val op) curr-val)))

;;;------------------------------------------------------------

(defun shrink-amoeba (op)
  (az:type-check Amoeba-Optimizer op)
  (let* ((p (vertices op))
	 (pilo (aref p (best-vertex op))))
    (declare (type Vector p))
    (format t "~%shrink amoeba")
    (dotimes (i (length p))
      (when (/= i (best-vertex op))
	(affine-mix 0.5d0pilo 0.5d0(aref p i) :result (aref p i))
	(setf (aref (vertex-values op) i)
	      (transform (objective-f op) (aref p i)))))
    (amoeba-sum p (vertex-sum op))))

;;;------------------------------------------------------------

(defun search-vertices (op)
  (az:type-check Amoeba-Optimizer op)
  (let ((y (vertex-values op))
	(ihi -1)
	(inhi -1)
	(ilo -1)
	(yihi most-negative-double-float)
	(yinhi most-negative-double-float)
	(yilo most-positive-double-float)
	(ytry most-positive-double-float))
    (declare (type az:Array-Index ihi inhi ilo)
	     (type Double-Float yihi yinhi yilo ytry))
    (dotimes (i (length y))
      (setf ytry (aref y i))
      (when (< ytry yilo) (setf yilo ytry) (setf ilo i))
      (when (> ytry yihi)
	(setf yinhi yihi)
	(setf inhi ihi)
	(setf yihi ytry)
	(setf ihi i))
      (when (and (> ytry yinhi) (/= i ihi))
	(setf yinhi ytry)
	(setf inhi i)))

    (assert (not (or (minusp ihi) (minusp inhi) (minusp ilo))))
    (assert (and (/= ihi inhi) (/= ihi ilo) (/= inhi ilo)))

    (setf (worst-vertex op) ihi)
    (setf (next-worst-vertex op) inhi)
    (setf (best-vertex op) ilo)))

;;;------------------------------------------------------------

(defun amoeba-search (y)
  (declare (type az:Float-Vector y))
  (let ((ihi -1)
	(inhi -1)
	(ilo -1)
	(yihi most-negative-double-float)
	(yinhi most-negative-double-float)
	(yilo most-positive-double-float)
	(ytry most-positive-double-float))
    (declare (type az:Array-Index ihi inhi ilo)
	     (type Double-Float yihi yinhi yilo ytry))
    (dotimes (i (length y))
      (setf ytry (aref y i))
      (when (< ytry yilo) (setf yilo ytry) (setf ilo i))
      (when (> ytry yihi)
	(setf yinhi yihi) (setf inhi ihi)
	(setf yihi ytry) (setf ihi i))
      (when (and (> ytry yinhi) (/= i ihi))
	(setf yinhi ytry) (setf inhi i)))

    (assert (not (or (minusp ihi) (minusp inhi) (minusp ilo))))
    (assert (and (/= ihi inhi) (/= ihi ilo) (/= inhi ilo)))

    (values ihi inhi ilo)))

;;;------------------------------------------------------------

;(defun amoeba-animate (sc k p y ilo ihi old-ilo)
;  (declare (ignore k ihi))
;  (when (and (> old-ilo (aref y ilo)) (animate? sc))
;    ;;(format t "~%~d steps, ~d stress evaluation-count."
;    ;;        k (evaluation-count sc))
;    ;;(format t "~%ihi, ilo ytry= ~f, ~f." (aref y ihi) (aref y ilo))
;    (animate sc (aref p ilo) (evaluation-count sc) (aref y ilo))))


;(defmethod animate-optimizer ((op Amoeba-Optimizer))
;  (search-vertices op)
;  (amoeba-animate (objective-f op)
;		  (iteration-count op)
;		  (vertices op)
;		  (vertex-values op)
;		  (best-vertex op)
;		  (worst-vertex op)
;		  (prev-val op)))

;;;============================================================
;;; Iterated-1D-Search Methods:
;;;============================================================

(defclass Line-Search-Optimizer (Optimizer)
	  ((curr-dir :type Abstract-Vector
		     :accessor curr-dir
		     :initarg :curr-dir)
	   (min-step :type Double-Float
		     :accessor min-step
		     :initarg :min-step)
	   (max-step :type Double-Float
		     :accessor max-step
		     :initarg :max-step)
	   (step-size :type Double-Float
		      :accessor step-size
		      :initarg :step-size)
	   (1d-absolute-tolerance :type Double-Float
				  :accessor 1d-absolute-tolerance
				  :initarg :1d-absolute-tolerance)
	   (1d-relative-tolerance :type Double-Float
				  :accessor 1d-relative-tolerance
				  :initarg :1d-relative-tolerance)
	   ;; stuff for animation
	   (animate-path?
	    :type (or T Null)
	    :accessor animate-path?
	    :initarg :animate-path?)
	   (animate-function?
	    :type (or T Null)
	    :accessor animate-function?
	    :initarg :animate-function?)
	   (animate-1d-bracket?
	    :type (or T Null)
	    :accessor animate-1d-bracket?
	    :initarg :animate-1d-bracket?)
	   (strip-chart :type (or Null chart:2d-Strip-Chart)
			:accessor strip-chart
			:initarg :strip-chart)
	   (max-strip-length :type az:Array-Index
			     :accessor max-strip-length
			     :initarg :max-strip-length))
	  (:default-initargs
	   :tolerance 1.0d-4
	   :min-step 0.0d0
	   :max-step 1.0d0
	   :step-size 1.0d0
	   :1d-absolute-tolerance  1.0d-8
	   :1d-relative-tolerance  1.0d-3
	   :animate-path? nil
	   :animate-function? t
	   :animate-1d-bracket? nil
	   :strip-chart nil
	   :max-strip-length 128))

;;;------------------------------------------------------------

(defmethod (setf objective-f) :after (f (op Line-Search-Optimizer))
  f
  (when (animate-path? op)
    (setf (strip-chart op)
      (chart:make-2d-strip-chart :name (format nil "~a" op)
				 :x-label "x0" :y-label "x1")))) 

;;;------------------------------------------------------------

(defmethod step-optimizer ((op Line-Search-Optimizer))
  (compute-search-direction op)
  (step-optimizer-in-current-direction op)
  (when (or (animate-path? op) (animate-function? op))
    (animate op))
  (when (converged-optimizer? op)
    (throw :end-search (values (curr-val op) (curr-p op)))))

;;;------------------------------------------------------------

(defmethod strip-chart ((op Line-Search-Optimizer))
  (when (az:empty-slot? op 'strip-chart)
    (setf (slot-value op 'strip-chart)
      (chart:make-2d-strip-chart
       :name (format nil "~a" op)
       :x-label "x0" :y-label "x1")))  
  (slot-value op 'strip-chart))

(defmethod animate ((op Line-Search-Optimizer))
  (when (animate-function? op) (animate (objective-f op)))
  (when (animate-path? op)
    (let ((a (1d-array (curr-p op))))
      (chart:strip-crawl
       (strip-chart op)
       (g:make-chart-point :x (aref a 0) :y (aref a 1))))))

;;;------------------------------------------------------------

(defmethod step-optimizer-in-current-direction ((op Line-Search-Optimizer))
  (multiple-value-bind
    (step val) (search-direction op (curr-p op) (curr-dir op))
    (when (zerop step) (throw :end-search (values (curr-val op) (curr-p op))))
    (setf (max-step op) (* 2.0d0 step))
    (setf (step-size op) step)
    (shiftf (prev-val op) (curr-val op) val)
    (az:copy (curr-p op) :result (prev-p op))
    (move-by (curr-p op) step (curr-dir op) :result (curr-p op))))

;;;------------------------------------------------------------

(defmethod search-direction ((op Line-Search-Optimizer) p dir)
  (assert (element? p (domain op)))
  (assert (element? dir (translation-space (domain op))))

  (when (bm:small? (l2-norm2 dir) (curr-val op))
    (print "search direction too short")
    (throw :end-search (values (curr-val op) p)))

  (with-borrowed-element (trial-p (domain op))
    (flet ((try-step (len)
	     (move-by p len dir :result trial-p)
	     (transform (objective-f op) trial-p)))
      (when (animate-1d-bracket? op)
	(chart:draw-function #'try-step
			     (min (min-step op) (- (max-step op)))
			     (max (max-step op) (- (min-step op)))
			     :title (format nil "Bracket: ~s" op)
			     :x-label "Step Length"
			     :y-label "Objective"))
      (bm:minimize-1d #'try-step (min-step op) (max-step op)
		      :absolute-tolerance (1d-absolute-tolerance op)
		      :relative-tolerance (1d-relative-tolerance op)))))

;;;------------------------------------------------------------

(defmethod converged-optimizer? ((op Line-Search-Optimizer))
  (or
    (when (bm:close-enough? (curr-val op) (prev-val op) (tolerance op))
      (print "not enough improvement")
      t)
    (when (bm:small? (l2-norm2 (curr-dir op)) (+ 1.0d0 (curr-val op)))
      (print "search direction too short")
      t)))

;;;============================================================
;;; Direction set method, Numerical Recipes, Section 10.5.
;;;============================================================

(defclass Direction-Set-Optimizer (Line-Search-Optimizer)
     ((pt
	:type Flat-Point 
	:accessor pt)
      (directions
	:type Vector ;; of Abstract-Vectors
	:accessor directions)
      (bestd
	:type Abstract-Vector
	:accessor bestd)
      (tt
	:type Double-Float
	:accessor tt)
      (fret
	:type Double-Float
	:accessor fret)
      (fp
	:type Double-Float
	:accessor fp)
      (del
	:type Double-Float
	:accessor del)
      (l
	:type az:Array-Index
	:accessor l))

  (:default-initargs
    :tolerance 0.01))

;;;------------------------------------------------------------

(defmethod initialize-optimizer ((op Direction-Set-Optimizer) ob start)
  (az:type-check Objective-Function ob)
  (assert (element? start (domain ob)))

  (setf (objective-f op) ob)
  (setf (evaluation-count op) 0)
  (setf (iteration-count op) 0)
  (setf (curr-p op) start)
  (setf (prev-p op) (az:copy start :result (make-element (domain op))))
  (setf (curr-val op) (transform ob (curr-p op)))
  )

;;;------------------------------------------------------------

;(defun search-direction (op)
;  (az:type-check Direction-Set-Optimizer op)
;  (assert (element? start (domain op)))
;
;  (when (bm:small? (l2-norm2 d) fret) (error "Direction vector too small."))
;
;  (setf fpt fret)
;  (multiple-value-setq (step-size fret)
;    (bm:minimize-1d
;      #'(lambda (size)
;	  (declare (type Double-Float size))
;	  (setf pt (move-by (curr-p op) size d :result pt))
;	  (transform (objective-f op) pt))
;      (min-step op) (max-step op)
;      :relative-tolerance (tolerance op)))
;  (setf (max-step op) step-size)
;  (setf (curr-p op)
;	(move-by (curr-p op)
;		    step-size d
;		    :result(curr-p op)))
;  (format t "~%directions=~d, evals=~d, loss=~f."
;	  (incf l) (evaluation-count sc) fret)
;  (when (animate? sc)
;    (animate (objective-f op) (curr-p op) (evaluation-count sc) fret))
;  (when (> (abs (- fpt fret)) del)
;    (setf del (abs (- fpt fret)))
;    (setf bestd d)))

;;;============================================================
;;; Steepest Descent (not recommended)
;;;============================================================

(defclass Steepest-Descent-Optimizer (Gradient-Optimizer
				       Line-Search-Optimizer)
     ())

;;;------------------------------------------------------------

(defmethod initialize-optimizer ((op Steepest-Descent-Optimizer) ob start)
  (az:type-check Objective-Function ob)
  (assert (element? start (domain ob)))

  (setf (objective-f op) ob)
  (setf (evaluation-count op) 0)
  (setf (iteration-count op) 0)
  (setf (curr-p op) start)
  (setf (prev-p op) (az:copy start))
  (setf (curr-val op) (transform ob (curr-p op)))
  (setf (curr-dir op) (make-element (translation-space (domain ob))))
  (compute-search-direction op)
  )

;;;------------------------------------------------------------

(defmethod compute-search-direction ((op Steepest-Descent-Optimizer))
  (funcall (-grad-f op) (objective-f op) (curr-p op) (curr-dir op)))

;;;============================================================

(defclass Discrete-Steepest-Descent-Optimizer (Discrete-Gradient-Optimizer
						Steepest-Descent-Optimizer)
     ())

;;;============================================================
;;; Conjugate Gradient
;;;============================================================

(defclass Conjugate-Gradient-Optimizer (Gradient-Optimizer
					 Line-Search-Optimizer)
     ((curr-g.g
	:type Abstract-Vector
	:accessor curr-g.g)
      (prev-g.g
	:type Abstract-Vector
	:accessor prev-g.g)))

;;;------------------------------------------------------------

(defmethod initialize-optimizer ((op Conjugate-Gradient-Optimizer) ob start)
  (az:type-check Objective-Function ob)
  (assert (element? start (domain ob)))

  (setf (objective-f op) ob)
  (setf (evaluation-count op) 0)
  (setf (iteration-count op) 0)
  (setf (curr-p op) start)
  (setf (prev-p op) (az:copy start))
  (setf (curr-val op) (transform ob (curr-p op)))
  (setf (curr--g op) (funcall (-grad-f op)
			      (objective-f op)
			      (curr-p op)
			      (make-element (translation-space (domain ob)))))
  (setf (curr-g.g op) (l2-norm2 (curr--g op)))
  (setf (prev--g op) (az:copy (curr--g op)))
  (setf (prev-g.g op) (curr-g.g op))
  (setf (curr-dir op) (az:copy (curr--g op)))
  ;;(initialize-animator ob (curr-p op) 0.0d0 (curr-val op))
  (step-optimizer-in-current-direction op))

;;;------------------------------------------------------------

(defmethod compute-search-direction ((op Conjugate-Gradient-Optimizer))
  (when (bm:small? (curr-g.g op))
    (format *standard-output*
	    "~%Exiting because search direction too short ~%(length=~f)."
	    (curr-g.g op))
    (throw :end-search (values (curr-val op) (curr-p op))))
  (let (gamma dg.g)
    (rotatef (curr--g op) (prev--g op))
    (setf (prev-g.g op) (curr-g.g op))

    (setf (curr--g op)
	  (funcall (-grad-f op) (objective-f op) (curr-p op) (curr--g op)))
    (setf (curr-g.g op) (l2-norm2 (curr--g op)))

    (setf dg.g (- (curr-g.g op) (inner-product (curr--g op) (prev--g op))))
    (setf gamma (/ dg.g (prev-g.g op)))
    (linear-mix
      1.0d0 (curr--g op)
      gamma (curr-dir op)
      :result (curr-dir op))))

;;;============================================================

(defclass Discrete-Conjugate-Gradient-Optimizer (Discrete-Gradient-Optimizer
						  Conjugate-Gradient-Optimizer)
     ())

;;;============================================================
;;; Quasi-Newton Methods
;;;============================================================

(defclass Quasi-Newton-Optimizer (Gradient-Optimizer
				   Line-Search-Optimizer)
     ((approx-inv-hessian
       :type Linear-Mapping
       :accessor approx-inv-hessian)))

;;;------------------------------------------------------------

(defmethod compute-search-direction ((op Quasi-Newton-Optimizer))
  (update-inverse-hessian-approximation op)
  (transform (approx-inv-hessian op) (curr--g op) :result (curr-dir op)))

;;;============================================================
;;; Quasi-Newton Methods 
;;; Numerical Recipes, Section 10.7
;;; 
;;; These strategies update a matrix approximating the inverse of the
;;; Hessian.  They are therefore numerically unstable and probably
;;; shouldn't be used, though there seesm to be some disagreement on
;;; this point.
;;;============================================================
 
(defclass Inverse-Matrix-QN-Optimizer (Quasi-Newton-Optimizer)
     ((dp
	:type Abstract-Vector
	:accessor dp)
      (dg
	:type Abstract-Vector
	:accessor dg)
      (hdg
	:type Abstract-Vector
	:accessor hdg)
      (dp@dp
	:type Symmetric-Tensor-Product
	:accessor dp@dp)
      (hdg@hdg
	:type Symmetric-Tensor-Product
	:accessor hdg@hdg)))

;;;============================================================
;;; DFP Quasi-Newton minimization
;;;============================================================

(defclass Inverse-DFP-Optimizer (Inverse-Matrix-QN-Optimizer)
     ())

;;;------------------------------------------------------------

(defmethod initialize-optimizer ((op Inverse-DFP-Optimizer) ob start)
  (az:type-check Objective-Function ob)
  (let* ((dom (domain ob))
	 (translations (translation-space dom)))
    (assert (element? start dom))

    (setf (objective-f op) ob)
    (setf (evaluation-count op) 0)
    (setf (iteration-count op) 0)
    (setf (curr-p op) start)
    (setf (prev-p op) (az:copy start))
    (setf (curr-val op) (transform ob (curr-p op)))
    (setf (prev-val op) most-positive-double-float)
    (setf (curr--g op) (funcall (-grad-f op) (objective-f op) (curr-p op)
				(make-element translations)))
    (setf (prev--g op) (make-element translations))
    (setf (curr-dir op) (az:copy (curr--g op)))
    (setf (dp op) (make-element translations))
    (setf (dg op) (make-element translations))
    (setf (hdg op) (make-element translations))
    (setf (approx-inv-hessian op)
	  (identity! (make-linear-Mapping translations translations)))
    (unless (zerop (curr-val op))
      (scale! (/ (abs (curr-val op))) (approx-inv-hessian op)))
    (setf (dp@dp op) (tensor-product (dp op) (dp op)))
    (setf (hdg@hdg op) (tensor-product (hdg op) (hdg op)))
  
    ;;(initialize-animator ob (curr-p op) 0.0d0 (curr-val op))
    (step-optimizer-in-current-direction op)))

;;;------------------------------------------------------------

(defmethod update-inverse-hessian-approximation ((op Inverse-DFP-Optimizer))
  ;; change in position from last step:
  (sub (curr-p op) (prev-p op) :result (dp op))
  ;; gradient and its change:
  (az:copy (curr--g op) :result (prev--g op))
  (funcall (-grad-f op) (objective-f op) (curr-p op) (curr--g op))
  ;; subtracting minus gradients to get change in gradient:
  (sub (prev--g op) (curr--g op) :result (dg op))
  (transform (approx-inv-hessian op) (dg op) :result (hdg op))

  (let ((dg.dp (inner-product (dg op) (dp op)))
	(dg.hdg (inner-product (dg op) (hdg op)))
	1/dg.dp	-1/dg.hdg)
    (assert (not (zerop dg.dp)))
    (assert (not (zerop dg.hdg)))
    ;;(assert (plusp dg.hdg))
    (setf  1/dg.dp  (/  1.0d0 dg.dp))
    (setf -1/dg.hdg (/ -1.0d0 dg.hdg))
    ;; two terms to add to hessian inverse approximation:
    (linear-mix 1/dg.dp (dp@dp op) 1.0d0 (approx-inv-hessian op)
		:result (approx-inv-hessian op))
    (linear-mix -1/dg.hdg (hdg@hdg op) 1.0d0 (approx-inv-hessian op)
		:result (approx-inv-hessian op))))

;;;============================================================

(defclass Discrete-Inverse-DFP-Optimizer (Inverse-DFP-Optimizer
					   Discrete-Gradient-Optimizer)
     ())

;;;============================================================
;;; BFGS Quasi-Newton minimization
;;;============================================================

(defclass Inverse-BFGS-Optimizer (Inverse-Matrix-QN-Optimizer)
     ((dg@dg
	:type Symmetric-Tensor-Product
	:accessor dg@dg)))

;;;============================================================

(defclass Discrete-Inverse-BFGS-Optimizer (Inverse-BFGS-Optimizer
					    Discrete-Gradient-Optimizer)
     ())

;;;------------------------------------------------------------

(defmethod initialize-optimizer ((op Inverse-BFGS-Optimizer) ob start)
  (let* ((dom (domain ob))
	 (translations (translation-space dom)))
    (assert (element? start dom))

    (setf (objective-f op) ob)
    (setf (evaluation-count op) 0)
    (setf (iteration-count op) 0)
    (setf (curr-p op) start)
    (setf (prev-p op) (az:copy start))
    (setf (curr-val op) (transform ob (curr-p op)))
    (setf (prev-val op) most-positive-double-float)
    (setf (curr--g op) (funcall (-grad-f op) (objective-f op) (curr-p op)
				(make-element translations)))
    (setf (prev--g op) (make-element translations))
    (setf (curr-dir op) (az:copy (curr--g op)))
    (setf (dp op) (make-element translations))
    (setf (dg op) (make-element translations))
    (setf (hdg op) (make-element translations))
    (setf (approx-inv-hessian op)
	  (identity! (make-linear-Mapping translations translations)))
    (unless (zerop (curr-val op))
      (scale! (/ (abs (curr-val op))) (approx-inv-hessian op)))
    (setf (dp@dp op) (tensor-product (dp op) (dp op)))
    (setf (dg@dg op) (tensor-product (dg op) (dg op))) ;; only change from DFP
    (setf (hdg@hdg op) (tensor-product (hdg op) (hdg op)))
  
    ;;(initialize-animator ob (curr-p op) 0.0d0 (curr-val op))
    (step-optimizer-in-current-direction op)))

;;;------------------------------------------------------------

(defmethod update-inverse-hessian-approximation ((op Inverse-BFGS-Optimizer))
  ;; change in position from last step:
  (sub (curr-p op) (prev-p op) :result (dp op))
  ;; gradient and its change:
  (az:copy (curr--g op) :result (prev--g op))
  (funcall (-grad-f op) (objective-f op) (curr-p op) (curr--g op))
  ;; subtracting minus gradients to get change in gradient:
  (sub (prev--g op) (curr--g op) :result (dg op))
  (transform (approx-inv-hessian op) (dg op) :result (hdg op))

  (let ((dg.dp (inner-product (dg op) (dp op)))
	(dg.hdg (inner-product (dg op) (hdg op)))
	1/dg.dp	-1/dg.hdg)
    (assert (not (zerop dg.dp)))
    (assert (plusp dg.hdg))
    (setf  1/dg.dp  (/  1.0d0 dg.dp))
    (setf -1/dg.hdg (/ -1.0d0 dg.hdg))
    (linear-mix 1/dg.dp (dp op) -1/dg.hdg (hdg op) :result (dg op))
    ;; three term update:
    (linear-mix 1/dg.dp   (dp@dp op)   1.0d0 (approx-inv-hessian op)
		:result (approx-inv-hessian op))
    (linear-mix -1/dg.hdg (hdg@hdg op) 1.0d0 (approx-inv-hessian op)
		:result (approx-inv-hessian op))
    (linear-mix dg.hdg    (dg@dg op)   1.0d0 (approx-inv-hessian op)
		:result (approx-inv-hessian op))))

;;;============================================================
;;; Quasi-Newton Methods that approximate a Cholesky decomposition
;;;  of the Hessian
;;;============================================================

(defclass Cholesky-QN-Optimizer (Quasi-Newton-Optimizer)
     ())

(defclass Cholesky-DFP-Optimizer (Cholesky-QN-Optimizer)
     ())

(defclass Discrete-Cholesky-DFP-Optimizer (Cholesky-DFP-Optimizer
					    Discrete-Gradient-Optimizer)
     ())

(defclass Cholesky-BFGS-Optimizer (Cholesky-QN-Optimizer)
     ())

(defclass Discrete-Cholesky-BFGS-Optimizer (Cholesky-BFGS-Optimizer
					     Discrete-Gradient-Optimizer)
     ())

;;;============================================================
;;; Trust region methods
;;;============================================================

(defclass Trust-Region-Optimizer (Optimizer)
     ())

