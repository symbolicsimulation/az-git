;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; This file defines a sample brick implementation.

(defclass Test-Brick (Brick-Object)
	  ((left
	    :type az:Positive-Fixnum
	    :accessor left
	    :initarg :left)
	   (width
	    :type az:Positive-Fixnum
	    :reader width
	    :initarg :width)
	   (fixed-width?
	    :type Boolean
	    :reader fixed-width?
	    :initarg :fixed-width?)
	   (top
	    :type az:Positive-Fixnum
	    :accessor top
	    :initarg :top)
	   (height
	    :type az:Positive-Fixnum
	    :reader height
	    :initarg :height)
	   (fixed-height?
	    :type Boolean
	    :reader fixed-height?
	    :initarg :fixed-height?)
	   (name
	    :type String
	    :accessor name
	    :initarg :name)
	   (gcontext
	    :type xlib:Gcontext
	    :accessor gcontext
	    :initarg :gcontext))
  (:default-initargs :left 0 :width 0 :fixed-width? nil
		     :top 0 :height 0 :fixed-height? nil
		     :name (string (gentemp))
		     :gcontext (make-gcontext))
  (:documentation
   "Test-Brick is a sample implementation of the Brick protocol
used for debugging and performance measurement."))

;;;============================================================

(defun make-test-brick (&key
			(left 0)
			(width 0)
			(top 0)
			(height 0)
			(name (string (gentemp)))
			(gcontext (make-gcontext)))
  (make-instance 'Test-Brick
    :left left :width width :top top :height height :name name
    :gcontext gcontext))

;;;============================================================
#||
(defmethod implements-brick-protocol? ((b Test-Brick))
  T)
||#
;;;============================================================
;;; correct writer methods

(defmethod (setf width) ((w Integer) (b Test-Brick))
  (declare (type az:Positive-Fixnum w))
  (when (fixed-width? b)
    (error "Trying to set the width of the fixed width brick ~a" b))
  (setf (slot-value b 'width) w))

(defmethod (setf height) ((h Integer) (b Test-Brick))
  (declare (type az:Positive-Fixnum h))
  (when (fixed-height? b)
    (error "Trying to set the height of the fixed height brick ~a" b))
  (setf (slot-value b 'height) h))

;;;============================================================

(defmethod print-object ((brick Test-Brick) stream)
  (format stream "'(:Brick ~a ~d ~d ~d ~d)"
	  (name brick)
	  (left brick) (top brick)
	  (width brick) (height brick)))

;;;============================================================

(defun name-string-width (b)
  (xlib:text-width (xlib:gcontext-font (gcontext b)) (name b)))

(defun name-string-height (b)
  (multiple-value-bind
      (width ascent descent left-bearing right-bearing
       overall-ascent overall-descent overall-direction next-start)
      (xlib:text-extents (xlib:gcontext-font (gcontext b)) (name b))
    (declare (ignore width ascent descent left-bearing right-bearing
		     overall-direction next-start))
    (+ overall-ascent overall-descent)))

(defun name-string-ascent (b)
  (multiple-value-bind
      (width ascent descent left-bearing right-bearing
       overall-ascent overall-descent overall-direction next-start)
      (xlib:text-extents (xlib:gcontext-font (gcontext b)) (name b))
    (declare (ignore width ascent descent left-bearing right-bearing
		     overall-descent overall-direction next-start))
    overall-ascent))

;;;============================================================

(defun draw-brick (brick window &key (box? nil) (name? nil))
  (declare (type Test-Brick brick)
		 (type xlib:Window window))

  (when box?
    (xlib:draw-rectangle window (gcontext brick)
			 (left brick)
			 (top brick)
			 (width brick)
			 (height brick)
			 nil))

  (when name?
    (xlib:draw-glyphs window (gcontext brick)
		      (left brick)
		      (+ (top brick) (name-string-ascent brick))
		      (name brick))))