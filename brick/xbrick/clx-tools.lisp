;;;-*- Package: :Bricklayer; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; Getting at some X defaults:
;;;============================================================

(defun clx-host ()

  "Returns a string containing the name of the X host, as given in the
Unix DISPLAY environment variable, minus the colon and numbers."

  (declare (:returns (type String host-name)))
  (let* ((display-var #+:excl (system:getenv "DISPLAY")
		      #+:cmu (cdr (assoc :display ext:*environment-list*)))
	 (colon-pos (position #\: display-var)))
    (if colon-pos
	(subseq display-var 0 colon-pos)
      display-var)))

(defparameter *clx-display* nil)

(defun clx-display ()

  "Returns a clx display object corresponding to the Unix DISPLAY
environment variable. <clx-display> only calls <xlib:open-display> the
first time it is called, so all references based on <clx-display> will
share the same connection to the X server and can therefore share
gcontexts, input event streams, etc."

  (declare (:returns (type xlib:Display)))

  (when (null *clx-display*)
    (setf *clx-display* (xlib:open-display (clx-host))))
  *clx-display*)

(defun default-screen (&key (display (clx-display)))
  (xlib:display-default-screen display))

(defun default-visual-info-class (&key 
				  (display (clx-display))
				  (screen (default-screen :display display)))
  (xlib:visual-info-class 
   (xlib:visual-info
    display
    (xlib:window-visual (xlib:screen-root screen)))))

(defun default-visual-info (&key
			    (display (clx-display))
			    (screen (default-screen :display display)))
  (xlib:visual-info
   display
   (xlib:window-visual (xlib:screen-root screen))))

(defun default-colormap (&key 
			 (display (clx-display))
			 (screen (default-screen
				     :display display)))
  (xlib:screen-default-colormap screen))

(defun default-screen-depth (&key
			     (display (clx-display))
			     (screen (default-screen :display display)))
  (xlib:screen-root-depth screen))

(defun root-window (&key (screen (default-screen)))
  (xlib:screen-root screen))

;;;============================================================

(defun color->lightness (color)
  "This function is intended to be used for approximating color displays
in gray scale."
  (declare (type xlib:Color color))
  (/ (+ (xlib:color-red color)
	(xlib:color-green color)
	(xlib:color-blue color))
     3.0))

(defun colormap-lit (window
		     &key
		     (colormap (xlib:window-colormap window))
		     (depth (xlib:drawable-depth window))
		     (length (ash 1 depth))
		     (result
		      (make-array length
				  :element-type `(Unsigned-Byte ,depth))))

  "Extract a vector of lightness values (the average of r, g, and b
scaled to a maximum of 2^pixel-depth) from a colormap."

  (declare (type xlib:Window window)
		 (type xlib:Colormap colormap)
		 (type Vector result)
		 (:returns result))
  (let* ((l-1 (- length 1))
	 (lut (xlib:query-colors colormap
				 (loop for i from 0 to l-1 collect i)
				 :result-type 'Vector)))
    (dotimes (i length)
      (setf (aref result i)
	(truncate (* (color->lightness (aref lut i)) l-1)))))
  result)

;;;============================================================

(defun make-gcontext (&key
		      (drawable (root-window))
		      (background (xlib:screen-white-pixel (default-screen)))
		      (foreground (xlib:screen-black-pixel (default-screen)))
		      (font (xlib::open-font (clx-display) "fixed")))
  (xlib:create-gcontext :drawable drawable
			:font font
			:background background
			:foreground foreground))

;;;============================================================

(defun make-window (&key
		    (left -1) (top -1)
		    (width -1) (height -1)
		    (name (gentemp "Brick-Window-"))
		    (screen (default-screen))
		    (colormap (default-colormap :screen screen))
		    (backing-store :not-useful)
		    (visual :copy))
  (let ((w (xlib:create-window
	    :parent (xlib:screen-root screen)
	    :background (xlib:screen-white-pixel screen)
	    :border (xlib:screen-black-pixel screen)
	    :border-width 1
	    :colormap colormap
	    :bit-gravity :north-west
	    :x left :y top
	    :width (if (minusp width) 256 width)
	    :height (if (minusp height) 256 height)
	    ;;:override-redirect :on
	    :visual visual
	    :backing-store (ecase (xlib:screen-backing-stores screen)
			     (:always backing-store)
			     (:when-mapped
			      (if (eq backing-store :when-mapped)
				  :when-mapped :not-useful))
			     (:never :not-useful)))))
    ;; Set the window's name and icon name to be the same
    ;; as the slate's.
    (setf (xlib:wm-name w) name)
    (setf (xlib:wm-icon-name w) name)
    (setf (xlib:wm-protocols w) '(:wm_delete_window))
    ;; set the wm hints so the window appears at the specified position,
    ;; unless either position is negative, in which case put it at the mouse
    ;; position:
    (setf (xlib:wm-normal-hints w)
      (xlib:make-wm-size-hints
       ;; :base-width (if (minusp width) 256 width)
       ;; :base-height (if (minusp height) 256 height)
       :user-specified-position-p (not (or (minusp left) (minusp top)))
       :user-specified-size-p (not (or (minusp width) (minusp height)))
       :x (max 0 left) :y (max 0 top)
       :width (if (minusp width) 256 width)
       :height (if (minusp height) 256 height)       
       ))

    (setf (xlib:window-override-redirect w) :off)

    ;; map the window
    (xlib:map-window w)
    ;; wait until the window is actually on the screen before returning
    (loop (when (eq (xlib:window-map-state w) :viewable) (return)))
    w))

;;;============================================================

(defun window-parent (w)
  (multiple-value-bind (children parent root) (xlib:query-tree w)
    (declare (ignore children root))
    parent))

(defun window-array (window whole-window?)

  "Return an array holding an image of the contents of the window.  The
array may not have the same dimensions as the window. In particular, if
<whole-window?> is not <nil>, then borders, title bars, etc., will be
included as well as the strictly Window contents of the window.  Also
return the effective width and height of the window's image, which may
be smaller that the array size, due to padding."

  (declare (type xlib:Window window)
		 (:returns
		  (values (type (Array Number (* *)) image)
			  (type xlib:Card16 width height))))
  
  (let* ((win (if whole-window?
		  ;; then get the parent window,
		  ;; which includes the title bar,, etc.
		  (window-parent window)
		;; else just the window itself
		window))
	 (w (xlib:drawable-width win))
	 (h (xlib:drawable-height win)))
    (values
     (xlib:image-z-pixarray
      (xlib:get-image win :x 0 :y 0 :width w :height h :format :z-pixmap))
     w h)))

;;;============================================================

(defun ps-dump-header (stream width height depth iscale)

  "Dump an image with lower left corner at current origin, scaling by
iscale (iscale=1 means 1/300 inch per pixel)."

  (let ((box-width (ceiling (* width iscale 72) 300))
	(box-height (ceiling (* height iscale 72) 300)))
    (format stream
	    "~
%!PS-Adobe-1.0
%%BoundingBox: 0 0 ~d ~d
%%EndComments
%%Pages: 0
%%EndProlog

gsave

% scale appropriately
~d ~d scale

% allocate space for one scanline of input
/picstr ~d string def

% read and dump the image
~d ~d ~d
[~d 0 0 ~d 0 ~d]
{ currentfile picstr readhexstring pop }
image
"
	    ;; bounding box:
	    box-width box-height
	    ;; scaling
	    box-width box-height
	    ;; length of string  to allocate
	    (ceiling (* width depth) 8)

	    ;; first args to image operator
	    width height depth

	    ;; transformation mtx
	    width (- height) height)

    (fresh-line stream)
    (fresh-line stream)))

;;;============================================================

(defconstant *hex-digits* "0123456789abcdef")
(declaim (type Simple-String *hex-digits*))

;;;-----------------------------------------------------------

(defparameter *ps-dump-buffer* "")

(defun ps-dump-buffer (size)
  (when (/= (length *ps-dump-buffer*) size)
    (setf *ps-dump-buffer* (make-string size :initial-element #\F)))
  *ps-dump-buffer*)

;;;-----------------------------------------------------------

(defun ps-dump-1bit-data (stream width height data lit)
  (declare (type Stream stream)
	   (type xlib:Card16 width height)
	   (type (Simple-Array Bit (* *)) data)
	   (type (Simple-Array Bit (*)) lit))
  (let* ((buflen (* (ceiling width 8) 2))
	 ;; We need to print an integer number of bytes for each scanline.
	 ;; Each byte is represented by 2 hex digit chars.
	 (buf (ps-dump-buffer buflen))
	 (nibble 0)
	 (ibuf 0)
	 (shift 3))
    (declare (type xlib:Card16 buflen nibble ibuf)
	     (type Simple-String buf)
	     (type (Integer -1 3) shift))
    (dotimes (j height)
      (declare (type xlib:Card16 j))
      (setf shift 3)
      (setf ibuf 0)
      (setf nibble 0)
      (dotimes (i width)
	(declare (type xlib:Card16 i))
	(setf nibble
	  (logior nibble (ash (the Bit (sbit lit (aref data j i))) shift)))
	(decf shift)
	(when (< shift 0)
	  (setf (schar buf ibuf) (schar *hex-digits* nibble))
	  (setf nibble 0)
	  (setf shift 3)
	  (incf ibuf)))
      (unless (= shift 3) (setf (schar buf ibuf) (schar *hex-digits* nibble)))
      (fresh-line stream)
      (write-string buf stream))
    (fresh-line stream)))

;;;-----------------------------------------------------------

(defun ps-dump-4bit-data (stream width height data lit)
  (declare (type Stream stream)
	   (type xlib:Card16 width height)
	   (type (Simple-Array (Unsigned-Byte 4) (* *)) data)
	   (type (Simple-Array (Unsigned-Byte 4) (*)) lit))
  (let* ((buflen (* 2 (ceiling width 2)))
	 ;; need to print an integer number of bytes,
	 ;; each represented by 2 hex digits.
	 (buf (ps-dump-buffer buflen)))
    (declare (type Simple-String buf))
    (dotimes (j height)
      (declare (type xlib:Card16 j))
      (dotimes (i width)
	(declare (type xlib:Card16 i))
	(setf (schar buf i) (schar *hex-digits* (aref lit (aref data j i)))))
      (fresh-line stream)
      (write-string buf stream))
    (fresh-line stream)))

;;;-----------------------------------------------------------

(defun ps-dump-8bit-data (stream width height data lit)
  (declare (type Stream stream)
	   (type xlib:Card16 width height)
	   (type (Simple-Array (Unsigned-Byte 8) (* *)) data)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) lit))
  (dotimes (j height)
    (declare (type xlib:Card16 j))
    (let ((buf (ps-dump-buffer (* 2 width)))
	  (2i 0)
	  (byte 0))
      (declare (type xlib:Card16 2i byte)
	       (type Simple-String buf))
      (dotimes (i width)
	(declare (type xlib:Card16 i))
	(setf byte (aref lit (aref data j i)))
	(setf (schar buf 2i) (schar *hex-digits* (ash byte -4)))
	(incf 2i)
	(setf (schar buf 2i) (schar *hex-digits* (logand byte #x0f)))
	(incf 2i))
      (fresh-line stream)
      (write-string buf stream)))
  (fresh-line stream))

;;;============================================================

(defun ps-dump-trailer (stream showpage?)
  (fresh-line stream)
  (fresh-line stream)
  (when showpage? (format stream "showpage"))
  (fresh-line stream)
  (format stream "grestore")
  (fresh-line stream)
  (format stream "%%Trailer")
  (fresh-line stream)
  (fresh-line stream))

;;;============================================================

(defun ps-dump (window file-name
		&key
		(scale 3)
		(whole-window? nil)
		(showpage? nil)
		(depth (xlib:screen-root-depth (default-screen))))

  "Dump the window (as an image) to a postscript file.

<scale> determines the magnification from window pixels to printer
pixels, eg., if <scale> is 1, then a 300x300 window will print as a 1
inch x 1 inch bitmap on a 300 dpi printer.

If <whole-window?> is not <nil>, then borders, title bars, etc., will
be dumped as well as the strictly Window contents of the window.

If <showpage?> is not <nil>, then a <showpage> instruction will be
appended to the file. <showpage?> should be <nil> for dumps that are
intended to be included in other documents as figures and should be
<t> if the dump is intended to be printed by itself.

<depth> determines the number of bits per pixel sent to the postscript
printer."

  (declare (type xlib:Window window)
		 (type String file-name)
		 (type (Integer 0 128) scale)
		 (type (Member 1 2 4 8) depth))
  (multiple-value-bind
      (data width height) (window-array window whole-window?)
    (let ((lit (colormap-lit window)))
      (with-open-file (stream file-name
		       :direction :output
		       :if-exists :supersede
		       :if-does-not-exist :create)
	(ps-dump-header stream width height depth scale)
	(ecase depth
	  (1 (ps-dump-1bit-data stream width height data lit))
	  (4 (ps-dump-4bit-data stream width height data lit))
	  (8 (ps-dump-8bit-data stream width height data lit)))
	(ps-dump-trailer stream showpage?))))
  t)

