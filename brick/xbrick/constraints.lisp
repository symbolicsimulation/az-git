;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; A derived constraint is represented by a function
;;; that returns a list of affine inequalities
;;;============================================================

(defun fixed (domain parameter value &rest bricks)

  "For example, (fixed domain :width 10 b0 b1) will return affine inequalities
that ensure the width of <b0> and <b1> is fixed at 10."

  (declare (type Domain domain)
	   (type Parameter parameter)
	   (type Number value)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  value '=
	  (make-functional domain (make-term 1.0d0 parameter brick)))))))

;;;-----------------------------------------------------------

(defun fixed-position (domain left top &rest bricks)

  "For example, (fixed 5 10 b0) will return affine inequalities
that ensure the upper left corner of  <b0> is (5,10)."

  (declare (type Domain domain)
	   (type Number left top)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect (make-inequality
		  left '=
		  (make-functional domain (make-term 1.0d0 :left brick))))
	(collect (make-inequality
		  top '=
		  (make-functional domain (make-term 1.0d0 :top brick)))))))

;;;-----------------------------------------------------------

(defun fixed-size (domain width height &rest bricks)

  "For example, (fixed 5 10 b0) will return affine inequalities
that ensure the width of <b0> is 5 and the height is 10."

  (declare (type Domain domain)
	   (type Number width height)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect (make-inequality
		  width '=
		  (make-functional domain (make-term 1.0d0 :width brick))))
	(collect (make-inequality
		  height '=
		  (make-functional domain (make-term 1.0d0 :height brick)))))))

;;;-----------------------------------------------------------

(defun minimum (domain parameter value &rest bricks)

  "For example, (minimum :width 10 b0 b1) will return affine inequalities
that ensure the width of <b0> and <b1> is at least 10."

  (declare (type Domain domain)
	   (type Parameter parameter)
	   (type Number value)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect (make-inequality
		  value '<=
		  (make-functional domain (make-term 1.0d0 parameter brick)))))))

;;;-----------------------------------------------------------

(defun maximum (domain parameter value &rest bricks)

  "For example, (maximum :width 10 b0 b1) will return affine inequalities
that ensure the width of <b0> and <b1> is no more than 10."

  (declare (type Parameter parameter)
	   (type Number value)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect (make-inequality
		  value '>=
		  (make-functional domain (make-term 1.0d0 parameter brick)))))))

;;;============================================================

(defun equal-parameter (domain parameter brick0 &rest bricks)

  "For example, (equal-parameter :width b0 b1 b2) will return
affine inequalities that ensure the width of <b0> equals the width of
<b1> and the width of <b2>."

  (declare (type Domain domain)
	   (type Parameter parameter)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  0.0d0 '=
	  (make-functional domain
			   (make-term -1.0d0 parameter brick0)
			   (make-term 1.0d0 parameter brick)))))))

;;;-----------------------------------------------------------

(defun equal-right (domain brick0 &rest bricks)

  "For example, (equal-right b0 b1 b2) will return affine inequalities
that ensure the right side of <b0> is same as that of <b1> and <b2>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  0.0d0 '=
	  (make-functional domain
			   (make-term -1.0d0 :left brick0)
			   (make-term -1.0d0 :width brick0)
			   (make-term 1.0d0 :left brick)
			   (make-term 1.0d0 :width brick)))))))

;;;-----------------------------------------------------------

(defun equal-bottom (domain brick0 &rest bricks)

  "For example, (equal-bottom b0 b1 b2) will return affine
inequalities that ensure the bottom of <b0> is same as that of <b1>
and <b2>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  0.0d0 '=
	  (make-functional domain
			   (make-term -1.0d0 :top brick0)
			   (make-term -1.0d0 :height brick0)
			   (make-term 1.0d0 :top brick)
			   (make-term 1.0d0 :height brick)))))))

;;;-----------------------------------------------------------

(defun equal-vertical-side (domain &rest bricks)

  "<equal-vertical-side> returns affine inequalities that ensure 
that the vertical extents of all the bricks match."

  (declare (type Domain domain)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (nconc (apply #'equal-parameter domain :top bricks)
	 (apply #'equal-parameter domain :height bricks)))

;;;-----------------------------------------------------------

(defun equal-horizontal-side (domain &rest bricks)

  "<equal-horizontal-side> returns affine inequalities that ensure 
that the horizontal extents of all the bricks match."

  (declare (type Domain domain)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (nconc (apply #'equal-parameter domain :left bricks)
	 (apply #'equal-parameter domain :width bricks)))

;;;============================================================

(defun surround-x (domain brick0 margin &rest bricks)

  "For example, (surround-x b0 10 b1 b2) will return affine inequalities
that ensure that the horizontal extents of <b1> and <b2> are within 
the horizontal extent of <b0> with a margin of 10 on each side."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect (make-inequality
		  margin '<=
		  (make-functional domain
				   (make-term -1.0d0 :left brick0)
				   (make-term 1.0d0 :left brick))))
	(collect
	 (make-inequality
	  margin '<=
	  (make-functional domain
			   (make-term 1.0d0 :left brick0)
			   (make-term 1.0d0 :width brick0)
			   (make-term -1.0d0 :left brick)
			   (make-term -1.0d0 :width brick)))))))

;;;-----------------------------------------------------------

(defun surround-y (domain brick0 margin &rest bricks)

  "For example, (surround-y b0 10 b1 b2) will return affine inequalities
that ensure that the vertical extents of <b1> and <b2> are within 
the within the vertical extent of <b0> with a margin of 10 on each side."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  margin '<=
	  (make-functional domain
			   (make-term -1.0d0 :top brick0)
			   (make-term 1.0d0 :top brick))))
	(collect
	 (make-inequality
	  (+ margin margin) '<=
	  (make-functional domain
			   (make-term 1.0d0 :top brick0)
			   (make-term 1.0d0 :height brick0)
			   (make-term -1.0d0 :top brick)
			   (make-term -1.0d0 :height brick)))))))

;;;-----------------------------------------------------------

(defun surround (domain brick0 margin-x margin-y &rest bricks)

  "For example, (surround b0 10 20 b1 b2) will return affine inequalities
that ensure that the extents of <b1> and <b2> are within 
the within the extent of <b0> with a margin of 10 on left and right
and a margin of 20 on the top and bottom."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Number margin-x margin-y)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (nconc (apply #'surround-x domain brick0 margin-x bricks)
	 (apply #'surround-y domain brick0 margin-y bricks)))

;;;============================================================

(defun cover-x (domain brick0 &rest bricks)

  "For example (cover-x b0 b1 b2) will return affine inequalities
that ensure that the horizontal extent of <b0> is the smallest
interval covering the horizontal extents of <b1> and <b2>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect (make-inequality
		  0.0d0 '=
		  (make-functional domain
				   (make-term -1.0d0 :left brick0)
				   (make-term 1.0d0 :left brick))))
	(collect (make-inequality
		  0.0d0 '=
		  (make-functional domain
				   (make-term 1.0d0 :left brick0)
				   (make-term 1.0d0 :width brick0)
				   (make-term -1.0d0 :left brick)
				   (make-term -1.0d0 :width brick)))))))

;;;-----------------------------------------------------------

(defun cover-y (domain brick0 &rest bricks)

  "For example, (cover-y b0 b1 b2) will return affine inequalities
that ensure that the vertical extent of <b0> is the smallest interval
covering the vertical extents of <b1> and <b2>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect (make-inequality
		  0.0d0 '=
		  (make-functional domain
				   (make-term -1.0d0 :top brick0)
				   (make-term 1.0d0 :top brick))))
	(collect (make-inequality
		  0.0d0 '=
		  (make-functional domain
				   (make-term 1.0d0 :top brick0)
				   (make-term 1.0d0 :height brick0)
				   (make-term -1.0d0 :top brick)
				   (make-term -1.0d0 :height brick)))))))

;;;-----------------------------------------------------------

(defun cover (domain brick0 &rest bricks)

  "For example, (cover b0 b1 b2) will return affine inequalities
that ensure that <b0> is the smallest brick covering <b1> and <b2>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (nconc (apply #'cover-x domain brick0 bricks)
	 (apply #'cover-y domain brick0 bricks)))

;;;============================================================

(defun to-right (domain brick0 margin &rest bricks)

  "<to-right> returns affine inequalities that ensure that <brick0> is
at least <margin> pixels to the right of all the <bricks>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  margin '<=
	  (make-functional domain
			   (make-term 1.0d0 :left brick0)
			   (make-term -1.0d0 :left brick)
			   (make-term -1.0d0 :width brick)))))))

;;;-----------------------------------------------------------

(defun to-left (domain brick0 margin &rest bricks)

  "<to-left> returns affine inequalities that ensure that <brick0> is
at least <margin> pixels to the left of all the <bricks>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  margin '<=
	  (make-functional domain
			   (make-term -1.0d0 :left brick0)
			   (make-term -1.0d0 :width brick0)
			   (make-term 1.0d0 :left brick) ))))))

;;;-----------------------------------------------------------

(defun below (domain brick0 margin &rest bricks)

  "<below> returns affine inequalities that ensure that <brick0> is
at least <margin> pixels below all the <bricks>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  margin '<=
	  (make-functional domain
			   (make-term 1.0d0 :top brick0)
			   (make-term -1.0d0 :top brick)
			   (make-term -1.0d0 :height brick)))))))

;;;-----------------------------------------------------------

(defun above (domain brick0 margin &rest bricks)

  "<above> returns affine inequalities that ensure that <brick0> is
at least <margin> pixels above all the <bricks>."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  margin '<=
	  (make-functional domain
			   (make-term -1.0d0 :top brick0)
			   (make-term -1.0d0 :height brick0)
			   (make-term 1.0d0 :top brick)))))))

;;;============================================================

(defun in-row (domain margin &rest bricks) 

  "<in-row> returns affine inequalities that ensure that the <bricks>
are horizontally in sequence from left to right each separated by
<margin>.  It says nothing about vertical position."

  (declare (type Domain domain)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))
  
  (let ((brick0 (first bricks)))
    (with-collection
	(dolist (brick1 (rest bricks))
	  (collect (make-inequality
		    margin '<=
		    (make-functional domain
				     (make-term -1.0d0 :left brick0)
				     (make-term -1.0d0 :width brick0)
				     (make-term 1.0d0 :left brick1))))
	  (setf brick0 brick1)))))

;;;-----------------------------------------------------------

(defun in-column (domain margin &rest bricks) 

  "<in-column> returns affine inequalities that ensure that the <bricks>
are vertically in sequence from top to bottom each separated by
<margin>.  It says nothing about horizontal position."

  (declare (type Domain domain)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))
  
  (let ((brick0 (first bricks)))
    (with-collection
	(dolist (brick1 (rest bricks))
	  (collect (make-inequality
		    margin '<=
		    (make-functional domain
				     (make-term -1.0d0 :top brick0)
				     (make-term -1.0d0 :height brick0)
				     (make-term 1.0d0 :top brick1))))
	  (setf brick0 brick1)))))

;;;============================================================

(defun center-x (domain brick0 &rest bricks)

  "<center-x> returns affine inequalities that ensure that the bricks
have the same horizontal center."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  0.0d0 '=
	  (make-functional domain
			   (make-term -1.0d0 :left brick0)
			   (make-term -0.5d0 :width brick0)
			   (make-term 1.0d0 :left brick)
			   (make-term 0.5d0 :width brick)))))))

;;;-----------------------------------------------------------

(defun center-y (domain brick0 &rest bricks) 

  "<center-y> returns affine inequalities that ensure that the bricks
have the same vertical center."

  (declare (type Domain domain)
	   (type Brick brick0)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (with-collection
      (dolist (brick bricks)
	(collect
	 (make-inequality
	  0.0d0 '=
	  (make-functional domain
			   (make-term -1.0d0 :top brick0)
			   (make-term -0.5d0 :height brick0)
			   (make-term 1.0d0 :top brick)
			   (make-term 0.5d0 :height brick)))))))

;;;-----------------------------------------------------------

(defun center (domain &rest bricks)

  "<center> returns affine inequalities that ensure that the bricks
have the same center point."

  (declare (type Domain domain)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))

  (nconc (apply #'center-x domain bricks)
	 (apply #'center-y domain bricks)))


;;;============================================================

(defun left-adjusted-column (domain margin &rest bricks) 

  "<left-adjusted-column> returns affine inequalities that ensure that
the <bricks> are vertically in sequence from top to bottom, each
separated by <margin> and have common left sides."

  (declare (type Domain domain)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))
  
  (nconc (apply #'in-column domain margin bricks)
	 (apply #'equal-parameter domain :left bricks)))

;;;-----------------------------------------------------------

(defun right-adjusted-column (domain margin &rest bricks) 

  "<right-adjusted-column> returns affine inequalities that ensure
that the <bricks> are vertically in sequence from top to bottom, each
separated by <margin> and have common right sides."

  (declare (type Domain domain)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))
  
  (nconc (apply #'in-column domain margin bricks)
	 (apply #'equal-right domain bricks)))

;;;-----------------------------------------------------------

(defun centered-column (domain margin &rest bricks) 

  "<centered-column> returns affine inequalities that ensure that the
<bricks> are vertically in sequence from top to bottom, each separated
by <margin> and have common horizontal centers."

  (declare (type Domain domain)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))
  
  (nconc (apply #'in-column domain margin bricks)
	 (apply #'center-x domain bricks)))

;;;-----------------------------------------------------------

(defun centered-row (domain margin &rest bricks) 

  "<centered-row> returns affine inequalities that ensure that the
<bricks> are horizontally in sequence from top to bottom, each separated
by <margin> and have common vertical centers."

  (declare (type Domain domain)
	   (type Number margin)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))
  
  (nconc (apply #'in-row domain margin bricks)
	 (apply #'center-y domain bricks)))

;;;============================================================

(defun square (domain &rest bricks) 

  "<square> returns affine inequalities that ensure that each brick in
<bricks> is square."

  (declare (type Domain domain)
	   (type Brick-List bricks)
	   (:returns (type Inequality-List)))
  
  (mapcar #'(lambda (b)
	      (make-inequality
	       0.0d0 '=
	       (make-functional domain
				(make-term 1.0d0 :width b)
				(make-term -1.0d0 :height b))))
	  bricks))