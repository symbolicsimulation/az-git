;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================

(deftype Tableau-Row ()
  "A tableau is represented by a vector of row vectors."
  '(Simple-Array Double-Float (*)))

(deftype Tableau ()
  "A tableau is represented by a vector of row vectors."
  '(Simple-Array Tableau-Row (*)))
(defun tableau? (a)
  (and (typep a '(Simple-Array T (*)))
       (every #'(lambda (r) (typep r 'Tableau-Row)) a)))

(deftype Tableau-Check ()
  "A type for type-check, because (Simple-Array Tableau-Row (*))
screws up Franz's type-check."
  '(satisfies tableau?))

(defmacro tableau-entry (tableau i j)
  `(the Double-Float (aref (the Tableau-Row (aref ,tableau ,i)) ,j)))

(defsetf tableau-entry (tableau i j) (val)
  `(setf (aref (aref ,tableau ,i) ,j) ,val))

(defun make-tableau-array (m n)
  (let ((tab (make-array (+ m 3))))
    (dotimes (i (+ m 3))
      (setf (aref tab i) (make-float-array (+ n 2))))
    tab))

(defun clear-tableau (tableau)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Tableau tableau))
  (let ((n (length (aref tableau 0))))
    (dotimes (i (length tableau))
      (declare (type Array-Index i))
      (let ((row (aref tableau i)))
	(declare (type Tableau-Row row))
	(dotimes (j n)
	  (declare (type Array-Index j))
	  (setf (aref row j) 0.0d0))))))

;;;============================================================

(defun allocate-tableau (inequalities domain)

  "<allocate-tableau> allocates a Tableau big enough for a
problem with the given number of <inequalities> and <domain>."

  (declare (type Inequality-List inequalities)
		 (type Domain domain))

  (make-tableau-array (length inequalities)
		      (dimension domain)))

;;;============================================================
;;; Update a tableau to reflect the cost functional
;;;============================================================

(defun set-tableau-cost (tableau cost domain)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Tableau tableau)
	   (type Functional cost)
	   (type Domain domain))
  (let ((row (aref tableau 1)))
    (declare (type Tableau-Row row))
    (dotimes (j (the Array-Index
		  (+ (the Array-Index (dimension domain)) 2)))
      (declare (type Array-Index j))
      (setf (aref row j) 0.0d0))
    (dolist (term (terms cost))
      (declare (type Term term))
      ;; The negation is needed because the Numerical Recipes
      ;; simplex alg is set up to maximize. I find minimization
      ;; more natural and more consistent with the word "cost".
      (setf (aref row (the Array-Index
			(+ 1 (the Array-Index (term-index term domain)))))
	(- (the Double-Float (term-coefficient term)))))))

;;;============================================================
;;; Converting an cost linear functional and
;;; a list of affine inequalities into a Tableau
;;;============================================================

(defun set-tableau-row (tableau row-index inequality domain)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Tableau tableau)
	   (type Array-Index row-index)
	   (type Inequality inequality)
	   (type Domain domain))
  (let ((row (aref tableau row-index)))
    (declare (type Tableau-Row row))
    (setf (aref row 1) (the Double-Float (bound inequality)))
    (dolist (term (terms (functional inequality)))
      (declare (type Term term))
      (setf (aref row (the Array-Index
			(+ 1 (the Array-Index (term-index term domain)))))
	(- (the Double-Float (term-coefficient term)))))))

;;;============================================================

(defun fill-tableau (tableau cost >=s <=s ==s domain)
  (declare (type Tableau tableau)
	   (type Functional cost)
	   (type Inequality-List >=s <=s ==s)
	   (type Domain domain))
  (let ((row 2))
    (set-tableau-cost tableau cost domain)
    (dolist (inequality >=s)
      (set-tableau-row tableau row inequality domain)
      (incf row))
    (dolist (inequality <=s)
      (set-tableau-row tableau row inequality domain)
      (incf row))
    (dolist (inequality ==s)
      (set-tableau-row tableau row inequality domain)
      (incf row))
    (values tableau)))



