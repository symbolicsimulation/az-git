;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; Brick Affine Inequalities:
;;;============================================================
(eval-when (compile load eval)
(defclass Inequality (Standard-Object)
	  ((bound
	    :type Double-Float
	    :accessor bound
	    :initarg :bound)
	   (relation
	    :type (Member <= = >=)
	    :accessor relation
	    :initarg :relation)
	   (functional
	    :type Functional
	    :accessor functional
	    :initarg :functional))
  (:documentation
   "This is a type whose instances are expressions representing affine
(usually called linear) inequalities.  An affine inequality is
represented by a list of the form: (bound relation functional) The
<bound> is a number.  The <relation> is one of '<=, '=, or '>=. The
functional is an instance of the Functional type."))
)
;;;============================================================

(defun make-inequality (bound relation functional)
  (declare (type Number bound)
		 (type (Member <= = >=) relation)
		 (type Functional functional))
  (make-instance 'Inequality
    :bound (fl bound)
    :relation relation
    :functional functional))

(defun inequality? (x) (typep x 'Inequality))

;;;============================================================

(defun inequality-list? (l)
  (and (listp l)
       (every #'inequality? l)))

(deftype Inequality-List ()
  "An Inequality-List is just a list of Inequalities."
'(satisfies inequality-list?))

;;;============================================================

(defun >=inequality? (inequality) (eq `>= (relation inequality)))
(defun ==inequality? (inequality) (eq `=  (relation inequality)))
(defun <=inequality? (inequality) (eq `<= (relation inequality)))

;;;============================================================

(defmethod domain ((inequality Inequality))
  (domain (functional inequality)))



