;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================


(defclass Domain (Standard-Object)
	  ((bricks
	    :type Brick-List
	    :accessor bricks
	    :initarg :bricks)
	   (index-table
	    :type Hash-Table
	    :reader index-table
	    :initform (make-hash-table :test #'eq)))
  (:documentation "A Domain is essentially a list of Bricks."))

(defclass Position-Domain (Domain) ()
  (:documentation
   "A Position-Domain consists of bricks with fixed sizes."))

;;;============================================================

(defgeneric fill-domain-index-table (domain))

(defmethod fill-domain-index-table ((domain Domain))
  (let ((i 0)
	(hash (index-table domain)))
    (dolist (brick (bricks domain))
      (setf (gethash brick hash) i)
      (incf i 4))))

(defmethod fill-domain-index-table ((domain Position-Domain))
  (let ((i 0)
	(hash (index-table domain)))
    (dolist (brick (bricks domain))
      (setf (gethash brick hash) i)
      (incf i 2))))

(defmethod fill-domain-index-table (domain)
  (let ((i 0)
	(hash (index-table domain)))
    (dolist (brick (bricks domain))
      (setf (gethash brick hash) i)
      (incf i 4))))

(defun brick-index (brick domain)
  (gethash brick (index-table domain)))

;;;============================================================

(defun make-domain (brick-list &key (position-only? nil))
  (declare (type Brick-List brick-list)
	   (:returns (type Domain)))
  (let ((domain
	 (make-instance (if position-only? 'Position-Domain 'Domain)
	   :bricks (copy-list brick-list))))
    (fill-domain-index-table domain)
    domain))

(defun make-position-domain (brick-list)
  (declare (type Brick-List brick-list)
	   (:returns (type Domain)))
  (let ((domain
	 (make-instance 'Position-Domain
	   :bricks (copy-list brick-list))))
    (fill-domain-index-table domain)
    domain))

(defun direct-sum (domain0 domain1)
  (let* ((class (type-of domain0))
	 (bricks (concatenate 'List
		   (bricks domain0)
		   (bricks domain1))))
    (assert (typep domain1 class))
    (ecase class
      (Domain (make-domain bricks))
      (Position-Domain (make-position-domain bricks)))))

;;;============================================================

(defgeneric dimension (domain)
  (declare (type Domain domain)
	   (:returns (type Integer)))
  (:documentation
   "Return the dimension of the domain."))

(defmethod dimension ((domain Domain))
  (* 4 (length (bricks domain))))

(defmethod dimension ((domain Position-Domain))
  (* 2 (length (bricks domain))))

;;;============================================================

(defgeneric domain->coordinate-vector! (domain cv)
  (:documentation
   "This function extracts coordinates from a list of bricks
and stuffs them in a vector."))

(defmethod domain->coordinate-vector! ((domain Domain) cv)
  (assert (= (length cv) (dimension domain)))
  (let ((i 1))
    (dolist (b (bricks domain))
      (setf (aref cv i) (left b)) (incf i)
      (setf (aref cv i) (width b)) (incf i)
      (setf (aref cv i) (top b)) (incf i)
      (setf (aref cv i) (height b)) (incf i))))

(defmethod domain->coordinate-vector! ((domain Position-Domain) cv)
  (assert (= (length cv) (dimension domain)))
  (let ((i 1))
    (dolist (b (bricks domain))
      (setf (aref cv i) (left b)) (incf i)
      (setf (aref cv i) (top b)) (incf i))))

(defgeneric coordinate-vector->domain! (cv domain)
  (:documentation
   "This function extracts coordinates from a list of bricks
and stuffs them in a vector."))

(defmethod coordinate-vector->domain! (cv (domain Domain))
  (assert (= (length cv) (dimension domain)))
  (let ((i 1))
    (dolist (b (bricks domain))
      (setf (left b) (aref cv i)) (incf i)
      (setf (width b) (aref cv i)) (incf i)
      (setf (top b) (aref cv i)) (incf i)
      (setf (height b) (aref cv i)) (incf i))))


(defmethod coordinate-vector->domain! (cv (domain Position-Domain))
  (assert (= (length cv) (dimension domain)))
  (let ((i 1))
    (dolist (b (bricks domain))
      (setf (left b) (aref cv i)) (incf i)
      (setf (top b) (aref cv i)) (incf i))))