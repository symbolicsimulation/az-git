;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; Brick Linear Functionals
;;;============================================================

(defclass Functional (Standard-Object)
	  ((domain
	    :type Domain
	    :accessor domain
	    :initarg :domain)
	   (terms
	    :type Term-List
	    :accessor terms
	    :initarg :terms))
  (:documentation
   "A Functional on a Domain is represented by a list of Terms."))

(defun make-functional (domain &rest terms)
  "Make a Functional on <domain> out of <terms>."
  (declare (type Term-List terms)
	   (type Domain domain))
  (make-instance 'Functional
    :terms (copy-list terms)
    :domain domain))

;;;============================================================

(defun default-cost (domain)
  "A common cost functional that simply minimizes the sum of all
coordinates, giving higher weight to width and height than left and
top."
  (declare (type Domain domain)
	   (:returns (type Functional)))

  (apply #'make-functional
	 domain
	 (with-collection
	     (dolist (brick (bricks domain))
	       (collect (make-term 1 :left brick))
	       (collect (make-term 2 :width brick))
	       (collect (make-term 1 :top brick))
	       (collect (make-term 2 :height brick))))))



