;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;;  All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;; an attempt to tranwindow the simplex algorithm printed on
;; pages 339-342 of "Numerical Recipes in C" into lisp.
;; here goes....

(defun simplx (a m n m1 m2 m3 
	       &key
	       (izrov (make-array (+ n 1) :element-type 'Array-Index))
	       (iposv (make-array (+ m 1) :element-type 'Array-Index))
	       (result (make-float-array (+ n 1)))
	       (l1 (make-array (+ n 2) :element-type 'Array-Index))
	       (l2 (make-array (+ m 1) :element-type 'Array-Index))
	       (l3 (make-array (+ m 1) :element-type 'Array-Index)))
  (declare (type Tableau a)
	   (type Tableau-Row result)
	   (type Array-Index m n m1 m2 m3)
	   (type (Simple-Array Array-Index (*)) izrov iposv l1 l2 l3))
  (unless (= m (+ m1 m2 m3)) (error "Bad input dimensions in SIMPLX"))
  (zero-float-vector! result)
  (multiple-value-bind
      (case nl1 nl2)
      (simplx-feasible-point a m n m1 m2 m3 izrov iposv l1 l2 l3)
    (ecase case
      (:no-feasible-point
       (values :no-feasible-point -1.0d0 result))
      (:feasible-point
       (simplx-optimize a m n l1 nl1 l2 nl2 izrov iposv result)))))

;;;=======================================================================

(defun simplx-feasible-point (a m n m1 m2 m3 izrov iposv l1 l2 l3)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Tableau a)
	   (type Array-Index m n m1 m2 m3)
	   (type (Simple-Array Array-Index (*)) izrov iposv l1 l2 l3))
  (let* ((is 0)	(ip 0) (ir 0) (k 1) (kh 0) (kp 0) (m12 0)
	 (nl1 n) (nl2 0)
	 (q1 0.0d0)
	 (bmax 0.0d0)
	 (n+1 (+ n 1))
	 (m+1 (+ m 1))
	 (m+2 (+ m 2))
	 (r-m+2 (aref a m+2)))
    (declare (type Array-Index is ip ir k kh kp m12 nl1 nl2 n+1 m+1 m+2)
	     (type (Simple-Array Array-Index (*)) l1 l2 l3)
	     (type Double-Float q1 bmax)
	     (type Tableau-Row r-m+2))
    (loop
      (setf (aref l1 k) k)
      (setf (aref izrov k) k)
      (setf k (+ k 1))
      (unless (<= k n) (return)))
    (setf nl2 m)
    (loop for i Fixnum from 1 to m do
	  (when (< (tableau-entry a (+ i 1) 1) 0.0d0)
	    (error "Bad input tableau in SIMPLX"))
	  (setf (aref l2 i) i)
	  (setf (aref iposv i) (+ n i)))
    (loop for i Fixnum from 1 to m2 do (setf (aref l3 i) 1))
    (setf ir 0)
    (when (< 0 (+ m2 m3))
      (setf ir 1)
      (setf k 1)
      (loop
	(setf q1 0.0d0)
	(loop for i Fixnum from (+ m1 1) to m do
	      (incf q1 (the Double-Float (tableau-entry a (+ i 1) k))))
	(setf (aref r-m+2 k) (- q1))
	(setf k (+ k 1))
	(unless (<= k n+1) (return)))
      (block break-from-do
	(loop
	    while (not (= ir 0)) do
	      (multiple-value-setq (kp bmax) (simplx-max a m+1 l1 nl1 0))
	      (block skip-to-one
		(cond
		 ((and (<= bmax *eps*) (< (aref r-m+2 1) *-eps*))
		  (return-from simplx-feasible-point
		    (values :no-feasible-point -1 -1)))
		 ((and (<= bmax *eps*) (<= (aref r-m+2 1) *eps*))
		  (setf m12 (+ m1 m2 1))
		  (when (<= m12 m)
		    (setf ip m12)
		    (loop
		      (when (= (aref iposv ip) (+ ip n))
			(multiple-value-setq
			    (kp bmax) (simplx-max a ip l1 nl1 1))
			(when (> bmax 0.0d0) (return-from skip-to-one)))
		      (setf ip (+ ip 1))
		      (unless (< ip m) (return))))
		  (setf ir 0)
		  (setf m12 (- m12 1))
		  (when (<= (+ m1 1) m12)
		    (loop for i Fixnum from (+ m1 1) to m12 do
			  (when (= (aref l3 (- i m1)) 1)
			    (let* ((i+1 (+ i 1))
				   (r-i+1 (aref a i+1)))
			      (declare (type Array-Index i+1)
				       (type Tableau-Row r-i+1))
			      (loop for k Fixnum from 1 to n+1 do
				    (negf (the Double-Float
					    (aref r-i+1 k))))))))
		  (return-from break-from-do)))
		(multiple-value-setq
		    (ip q1) (simplx-find-pivot a n l2 nl2 kp q1))
		(when (= ip 0)
		  (return-from simplx-feasible-point
		    (values :no-feasible-point -1 -1))))
	      (simplx-exchange a m+1 n ip kp)
	      (let ((kp+1 (+ kp 1)))
		(declare (type Array-Index kp+1))
		(cond ((>= (aref iposv ip) (+ n m1 m2 1))
		       (setf k 1)
		       (loop
			 (when (= (aref l1 k) kp) (return))
			 (setf k (+ k 1))
			 (unless (<= k nl1) (return)))
		       (setf nl1 (- nl1 1))
		       (loop for is Fixnum from k to nl1 do
			     (setf (aref l1 is) (aref l1 (+ is 1))))
		       (incf (aref r-m+2 kp+1) 1.0d0)
		       ;; for (i=1;i<=m+2;i++) a[i][kp+1] = -a[i][kp+1];
		       (loop for i Fixnum from 1 to m+2 do
			     (negf (the Double-Float
				     (tableau-entry a i kp+1)))))
		      (t
		       (when (>= (aref iposv ip) (+ n m1 1))
			 (setf kh (the Array-Index (- (aref iposv ip) m1 n)))
			 (when (not (= (aref l3 kh) 0))
			   (setf (aref l3 kh) 0)
			   (incf (aref r-m+2 kp+1) 1.0d0)
			   (loop for i Fixnum from 1 to m+2 do
				 (negf (tableau-entry a i kp+1))))))))
	      (setf is (aref izrov kp))
	      (setf (aref izrov kp) (aref iposv ip))
	      (setf (aref iposv ip) is))))
    (values :feasible-point nl1 nl2)))

;;;=======================================================================

(defun simplx-optimize (a m n l1 nl1 l2 nl2 izrov iposv result)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   ;;(:explain :calls :types)
	   (type Tableau a)
	   (type Array-Index m n nl1 nl2)
	   (type (Simple-Array Array-Index (*)) l1 l2 izrov iposv)
	   (type Tableau-Row result))
  (loop
    (multiple-value-bind (kp bmax) (simplx-max a 0 l1 nl1 0)
      (declare (type Array-Index kp)
	       (type Double-Float bmax))
      (when (<= bmax 0.0d0)
	(return (simplx-solution :solution a m n iposv result)))
      (let ((ip (simplx-find-pivot a n l2 nl2 kp 0.0d0)))
	(declare (type Array-Index ip))
	(when (= ip 0)
	  (return (simplx-solution :unbounded a m n iposv result)))
	(simplx-exchange a m n ip kp)
	(rotatef (aref izrov kp)
		 (aref iposv ip))))))

;;;=======================================================================

(defun simplx-solution (case a m n iposv result)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Keyword case)
	   (type Tableau a)
	   (type Array-Index m n)
	   (type (Simple-Array Array-Index (*)) iposv)
	   (type Tableau-Row result))
  (loop for i Fixnum from 1 to m do
    (let ((iposvi (aref iposv i)))
      (declare (type Array-Index iposvi))
      (when (<= iposvi n)
	(setf (aref result iposvi) (tableau-entry a (+ i 1) 1)))))
  (values case (tableau-entry a 1 1) result))

;;;=======================================================================

(defun simplx-max (a mm ll nll iabf)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Tableau a)
	   (type (Simple-Array Array-Index (*)) ll)
	   (type Array-Index mm nll iabf))
  (if (and (= mm 0) (= nll 0))
      (progn (warn "mm and nll are both zero.")
	     (values 0 0.0d0))
    ;; else
    (let* ((kp (aref ll 1))
	   (mm+1 (+ mm 1))
	   (r (aref a mm+1))
	   (bmax (aref r (+ kp 1))))
      (declare (type Array-Index kp mm+1)
	       (type Double-Float bmax)
	       (type Tableau-Row r))
      (if (= iabf 0)
	  (loop for k Fixnum from 2 to nll do
	    (let* ((llk (aref ll k))
		   (llk+1 (+ llk 1)))
	      (declare (type Array-Index llk llk+1))
	      (when (> (aref r llk+1) bmax)
		(setf bmax (aref r llk+1))
		(setf kp llk))))
	;; else
	(loop for k Fixnum from 2 to nll do
	  (let* ((llk (aref ll k))
		 (llk+1 (+ llk 1)))
	    (declare (type Array-Index llk llk+1))
	    (when (> (the (Double-Float 0.0d0 *) (dabs (aref r llk+1)))
		     (the (Double-Float 0.0d0 *) (dabs bmax)))
	      (setf bmax (aref r llk+1))
	      (setf kp llk)))))
      (values kp bmax))))

;;;=======================================================================

(defun simplx-find-pivot (a n l2 nl2 kp q1)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Tableau a)
	   (type Double-Float q1)
	   (type (Simple-Array Array-Index (*)) l2)
	   (type Array-Index n nl2 kp))
  (incf kp)
  (let ((ii 0) (ip 0) (qp 0.0d0) (q0 0.0d0) (q 0.0d0) (n+1 (+ n 1)))
    (declare (type Array-Index ii ip n+1)
	     (type Double-Float qp q0 q))
    (loop for i Fixnum from 1 to nl2 do
      (let* ((l2i (aref l2 i))
	     (ri (aref a (+ l2i 1))))
	(declare (type Array-Index l2i)
		 (type (Simple-Array Double-Float (*)) ri))
	(when (< (aref ri kp) *-eps*)
	  (setf q1 (- (/ (aref ri 1) (aref ri kp))))
	  (setf ip l2i)
	  (loop for j Fixnum from (+ i 1) to nl2 do
	    (setf ii (aref l2 j))
	    (let ((rii (aref a (+ ii 1))))
	      (declare (type (Simple-Array Double-Float (*)) rii))
	      (when (< (aref rii kp) *-eps*)
		(setf q (- (/ (aref rii 1) (aref rii kp))))
		(cond ((< q q1)
		       (setf ip ii)
		       (setf q1 q))
		      ((= q q1)
		       (let ((rip (aref a (+ ip 1))))
			 (declare (type (Simple-Array Double-Float (*)) rip))
			 (do ((k 2 (+ k 1)))
			     ((> k n+1))
			   (declare (type Array-Index k))
			   (setf qp (- (/ (aref rip k) (aref rip kp))))
			   (setf q0 (- (/ (aref rii k) (aref rii kp))))
			   (unless (= q0 qp) (return))))
		       (when (< q0 qp) (setf ip ii)))))))
	  (return-from simplx-find-pivot (values ip q1)))))))

;;;=======================================================================

(defun simplx-exchange (a i1 k1 ip kp)
  (declare (optimize (safety 0) (space 3) (speed 3))
	   (type Tableau a)
	   (type Array-Index i1 k1 ip kp))
  (incf kp 1) (incf ip 1) (incf k1 1)
  (let* ((rip (aref a ip))
	 (pivot (/ 1.0d0 (aref rip kp)))
	 (ri rip))
    (declare (type Double-Float pivot)
	     (type (Simple-Array Double-Float (*)) rip ri))
    (do ((i 1 (+ i 1))) ((> i (+ i1 1)))
      (declare (type Array-Index i))
      (unless (= i ip)
	(setf ri (aref a i))
	(unless (= 1.0d0 pivot) (mulf (aref ri kp) pivot))
	(let ((rikp (aref ri kp)))
	  (declare (type Double-Float rikp))
	  (cond ((zerop rikp) t)
		((= 1.0d0 rikp)
		 (do ((k 1 (+ k 1))) ((> k k1))
		   (declare (type Array-Index k))
		   (unless (or (= k kp) (zerop (aref rip k)))
		     (decf (aref ri k) (aref rip k)))))
		(t 
		 (do ((k 1 (+ k 1))) ((> k k1))
		   (declare (type Array-Index k))
		   (unless (or (= k kp) (zerop (aref rip k)))
		     (decf (aref ri k) (* (aref rip k) rikp)))))))))
    (unless (= 1.0d0 pivot)
      (negf pivot)
      (do ((k 1 (+ k 1))) ((> k k1))
	(declare (type Array-Index k))
	(mulf (aref rip k) pivot))
      (negf pivot))
    (setf (aref rip kp) pivot))
  (values t))

