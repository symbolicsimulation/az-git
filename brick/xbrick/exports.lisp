;;;-*- Package: :Bricklayer; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer) 

(eval-when (compile load eval)
  (export '(;;simplex

	    Positive-Fixnum

	    left
	    width
	    ;;fixed-width?
	    top
	    height
	    ;;fixed-height?

	    ;;implements-brick-protocol?
	    Brick
	    Brick-List

	    Domain
	    Position-Domain
	    bricks
	    make-domain
	    make-position-domain
	    dimension
	    brick-index
	    coordinate-vector->domain!
	    domain->coordinate-vector!

	    Parameter
	    Term
	    make-term
	    term-coefficient
	    term-parameter
	    term-brick

	    Functional
	    terms
	    make-functional
	    default-cost

	    Inequality
	    bound
	    relation
	    functional
	    Inequality-List
	    make-inequality

	    allocate-tableau
	    solve-constraints

	    fixed
	    fixed-position
	    fixed-size
	    minimum
	    maximum
	    equal-parameter
	    equal-right
	    equal-bottom
	    equal-horizontal-side
	    equal-vertical-side
	    surround-x
	    surround-y
	    surround
	    to-right
	    to-left
	    below
	    above
	    in-row
	    in-column
	    center-x
	    center-y
	    center
	    centered-column
	    left-adjusted-column
	    right-adjusted-colum
	    centered-row
	    )))  

