;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================

(declaim (declaration :returns))

;;;=======================================================
;;; PCL/CLOS consistency
;;;=======================================================

#+:pcl
(eval-when (compile load eval)
  (let* ((package (find-package :pcl))
	(nicknames (package-nicknames package))
	(name (package-name package)))
    (unless (member "CLOS" nicknames :test #'string-equal)
      (rename-package package name (cons "CLOS" (copy-list nicknames))))))

#+:pcl
(eval-when (compile load eval)
  ;; have compiled classes and methods added to the environment
  ;; at compile time:
  (pushnew 'compile pcl::*defclass-times*)
  (pushnew 'compile pcl::*defgeneric-times*)
  (pushnew 'compile pcl::*defmethod-times*)
  )

#+:pcl
(eval-when (compile load eval)
  ;; PCL exports consistent with CLTL2:
  (export '(pcl::allocate-instance
	    pcl::class-direct-subclasses
	    pcl::class-direct-superclasses
	    pcl::class-slots
	    pcl::slot-definition-name
	    pcl::slot-definition-type
	    pcl::generic-function)
	  :pcl))

#+(and :pcl (not :cmu))
(defun pcl:slot-definition-name (slotd) (pcl::slotd-name slotd))
#+(and :pcl (not :cmu))
(defun pcl:slot-definition-type (slotd) (pcl::slotd-type slotd))

;;; redefine defgeneric to handle :method options
#+:pcl
(defmacro pcl:defgeneric (function-specifier lambda-list &body options)
  (flet ((method-option? (list) (eq (first list) :method))
         (make-defmethod (method-option)
           `(pcl:defmethod ,function-specifier ,@(rest method-option))))
    (let ((method-options (remove-if-not #'method-option? options))
          (options (remove-if #'method-option? options)))
      `(prog1
	   ,(pcl::expand-defgeneric function-specifier lambda-list options)
	 ,@(mapcar #'make-defmethod method-options)))))

;;;============================================================
;;; Handy Types

(eval-when (compile load eval)
  
(deftype Boolean () '(or T Nil))

(deftype Array-Index ()
  "The subset of Integer that can be used as an array index."
  #-:cmu '(Integer 0 #.array-dimension-limit)
  #+:cmu 'Fixnum)

(deftype Positive-Fixnum ()
  "This actually means non-negative Fixnum."
  '(Integer 0 #.most-positive-fixnum))
)
;;;============================================================

(defmacro type-check (type &rest args)

  "A <check-type> that takes arguments more like declarations, eg,
(declare (type Integer x y))."

  (let* ((args (remove-duplicates args))
 	 (variables (remove-if #'constantp args))
	 (runtime-checks (mapcar #'(lambda (arg)
				     `(check-type ,arg ,type))
				 variables)))
    ;; test constants at macro expand time:
    (dolist (constant (remove-if-not #'constantp args))
      ;; need to <eval> because <constant> might be a <defconstant>
      ;; rather than a literal constant
      (assert (typep (eval constant) type)))
    (unless (null runtime-checks) `(progn ,@runtime-checks))))

(defmacro declare-check (&rest decls)

  "This macro generates type checking forms and has a syntax like <declare>.
Unfortunately, we can't easily have it also generate the declarations."

  `(progn
     ,@(mapcar #'(lambda (d) `(type-check ,(second d) ,@(cddr d)))
	       ;; ignore non-type declarations
	       (remove-if-not #'(lambda (d) (eq (first d) 'type))
			      decls))))

;;;============================================================

(defmacro with-collection (&body body)

    "<with-collection> returns a list of whatever is collected within
its scope by calls to <collect>, in the order that <collect> is
called.  This is cleaner than a common idiom for the same purpose that
uses <let>, <push>, and <nreverse>."

    `(let ((.with-collection-result. nil)
	   .with-collection-tail.)
       (macrolet
	   ((collect (form)
	      ;;  The FORM is evaluated first so that COLLECT nests
	      ;; properly, i.e., The test to determine if this is
	      ;; the first value collected should be done after the
	      ;; value itself is generated in case it does
	      ;; collection as well.
	      `(let ((.collectable. ,form))
		 (if .with-collection-tail.
		     (rplacd .with-collection-tail.
			     (setq .with-collection-tail.
			       (list .collectable.)))
		   (setq .with-collection-result.
		     (setq .with-collection-tail. (list .collectable.))))
		 .with-collection-tail.)))
	 ,@body
	 .with-collection-result.)))

;;;============================================================

(defconstant *eps* (sqrt double-float-epsilon))

(defconstant *-eps* (- (sqrt double-float-epsilon)))

;;;=======================================================================
;;; useful numerical macros

(define-modify-macro mulf (&rest terms) * "Like <incf>")
(define-modify-macro divf (&rest terms) / "Like <incf>")
(define-modify-macro maxf (&rest terms) max "Like <incf>")
(define-modify-macro minf (&rest terms) min "Like <incf>")
(define-modify-macro negf () - "Like <incf>")

;;;============================================================

(defmacro fl (x)
  "Coerce a number to a floating point number of the default precision
(usually Double-Float)."
  (if (numberp x)
      (float x 1.0d0)
    `(float ,x 1.0d0)))

(defmacro dabs (x)
  "Trying to get an <abs> that compiles inline and doesn't cons."
  `(the (Double-Float 0.0d0 *)
     (if (minusp (the Double-Float ,x)) 
	 (- (the Double-Float ,x))
       (the Double-Float ,x))))

;;;============================================================

(defun make-float-array (dims &key
			      (initial-element 0.0d0 initial-element?)
			      (initial-contents nil))
  "Make a simple floating point array of the default precision,
usually Double-Float."
  (declare (type (or Fixnum List) dims initial-contents)
	   (type Double-Float initial-element)
	   (:returns (type (Simple-Array Double-Float))))
  (cond (initial-element?
	 (make-array dims :element-type 'Double-Float
		     :initial-element initial-element))
	(initial-contents
	 (make-array dims :element-type 'Double-Float
		     :initial-contents initial-contents))
	(t
	 (make-array dims :element-type 'Double-Float))))


;;;============================================================

(defun zero-float-vector! (v)
  (declare (optimize		     (safety 3)		     (speed 0))(optimize (safety 3) (speed 0))
	   (type (Simple-Array Double-Float (*)) v))
  (dotimes (i (array-dimension v 0))
    (declare (type Array-Index i))
    (setf (aref v i) 0.0d0)))

;;;============================================================

(defmacro make-fixnum-vector (low high)
  ;; return integer array with (at least) range low..high
  `(progn
     (assert (and (> ,low 0) (> ,high ,low)))
     (make-array (+ 1 ,high) :element-type 'Fixnum)))


(defmacro make-float-matrix (low1 high1 low2 high2 &key initial-contents)
  ;; return 2d float make-float-matrix with (at least) range low..high
  `(progn
     (when (or (< ,low1 0) (< ,high1 ,low1) (< ,low2 0) (< ,high2 ,low2))
       (error "bad bounds in make-float-matrix"))
     (make-float-array (list (1+ ,high1) (1+ ,high2))
		       :initial-contents ,initial-contents)))

;;;============================================================

(defun copy-float-matrix! (a result)
  "This function copies the contents of <a> into <result>.
They must be the same size and shape."
  (declare (optimize (safety 3) (speed 0))
	   (type (Simple-Array Double-Float (* *)) a result))
  (assert (and (= (array-dimension a 0) (array-dimension result 0))
	       (= (array-dimension a 1) (array-dimension result 1))))
  (let ((n (array-dimension a 1)))
    (declare (type Array-Index n))
    (dotimes (i (array-dimension a 1))
      (declare (type Array-Index i))
      (dotimes (j n)
	(declare (type Array-Index j))
	(setf (aref result i j) (aref a i j))))))

