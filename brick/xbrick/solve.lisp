;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; Solving a set of affine inequalities
;;;============================================================

(defun sort-inequalities (inequalities)
  (values
   (remove-if-not #'>=inequality? inequalities)
   (remove-if-not #'<=inequality? inequalities)
   (remove-if-not #'==inequality? inequalities)))

(defun solve-constraints (cost inequalities domain
			  &key
			  (tableau (allocate-tableau inequalities domain)))

  "<solve-constraints> destructively modifies the <bricks> to minimize
<cost> subject to the <inequalities>. It modifies the <bricks> by
applying the generic functions (setf left), (setf width), (setf top),
and (setf height) to the <bricks>, as needed."

  (declare (type Functional cost)
	   (type Inequality-List inequalities)
	   (type Domain domain)
	   (type Tableau-Check tableau))
  (clear-tableau tableau)
  (multiple-value-bind (>=s <=s ==s) (sort-inequalities inequalities)
    (fill-tableau tableau cost >=s <=s ==s domain)
    ;;(pm tableau)
    (let ((m1 (length >=s))
	  (m2 (length <=s))
	  (m3 (length ==s))
	  (n (dimension domain)))
      (multiple-value-bind
	  (case objf result) (simplx tableau (+ m1 m2 m3) n m1 m2 m3)
	(declare (ignore objf))
	;;(pm tableau)
	(when (eq case :solution)
	  (coordinate-vector->domain! result domain))
	case))))


