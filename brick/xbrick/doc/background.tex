\section{Introduction}
\label{Introduction}

By {\it layout}, we mean the problem of deciding 
on the size and location of the text and geometric objects
that make up a display.

Many page layout systems 
require layouts to be specified
``to the pixel,'' by hand, 
when a display is first designed.
This makes small changes to a design difficult,
in part because the system does not embody any 
of the conceptual constraints 
that were in the mind of the designer. 
It is also inadequate for the many applications
where the contents of a display change 
in complex or unpredictable ways at runtime.

Thus automatic layout is vital for graphical user interfaces.

Some user interface development systems provide
support for automatic layout,
but this is often one of the less successful parts of the system.
The behavior is typically difficult to predict,
giving surprising and unstable results.

There is a good reason 
why many systems stick with layout by hand
and why those which provide support for automatic layout,
do so poorly ---
layout problems consistently 
turn out to be much more difficult than they appear at first glance.
A general purpose facility for specifying and computing
layouts of any kind of picture is probably impossible.
Even in restricted domains,
such as graph layout, 
which has a long history and a sizable literature
(for a recent survey see \cite{Tama88}), 
there have been no completely satisfactory solutions.
One reason for the difficulty is 
that designers tend to think about layouts 
in terms of what properties a good layout has,
rather than the process by which one is created.

This leads us to divide methods for automatic layout
into {\it procedural} and {\it declarative} 
--- and suggests that we will prefer the declarative methods.

By procedural layout, we mean simply that there is a procedure,
which, given a description of a diagram, computes its layout.
A typical graph layout procedure
(such as that given by Rowe et al. \cite{Rowe87})
traverses the graph,
assigning each node to an unoccupied site 
in a regular grid of allowed positions.
The order of traversal and the method of site assignment are 
chosen in the hope of producing a ``readable'' layout:
a minimal number of crossed edges,
short (but not too short) edges,
almost straight paths between important pairs of non-adjacent nodes,
node positions whose y (or x) coordinates are consistent with the
partial ordering of a directed graph,
and so on.
There are often additional passes over the graph
during which the nodes are re-arranged
in an attempt to improve the layout.

Declarative layout could be thought of as a special case of procedural
layout, where the procedure definition is based on
three basic abstractions:
\begin{itemize}
\item a domain of possible layouts (for a given diagram),
\item a specification ``language'' that allows the user to express
        essential and desirable features of the layout, and
\item a generic solver, that takes a diagram and a specification,
        and returns an element of the domain that meets
        or well approximates the specification.
\end{itemize}

In graph layout, the domain might be all possible assignments of
nodes to positions in a discrete lattice in the plane.
The specification language might allow the user to ask to minimize
some loss function (eg. the number of edge crossings)
subject to constraints on the node positions (eg. no two nodes
can occupy the same position in the lattice).

The distinction between procedural and declarative layout
is, of course, very similar to the distinction between 
procedural and declarative programming languages.
In particular, our notion of declarative layout is 
intentionally close to constraint programming languages 
\cite{Lele88,Levi84}
and essentially identical to {\it constraint-based} layout
as described by Kamada \cite{Kama88,Kama91a}).

The advantage of procedural layout is usually speed.
A procedural method for graph layout makes one or a few passes over the graph;
the declarative example given above is 
an expensive integer programming problem,
whose solution would require the equivalent of many iterations
over the graph.

The advantage of the declarative approach is flexibility.
In general, the only way for a user to modify the result of a procedural
layout is to reprogram the procedure;
because of the ad hoc nature of most layout procedures,
it is difficult to know how to change the procedure to get the 
desired effect.
It's also difficult to have much intuition about how the procedure
will respond to changes in the specification.
The three abstractions of the declarative approach --- 
the domain, the specification, and the solver --- 
gives a user immediate access
to a whole class of layouts.

The subject of this paper is the layout of displays 
consisting of a number of rectangular regions, 
which may nest inside one another,
may tile an enclosing region, 
or have other constraints on size and position.
This type of layout problem is sometimes referred 
to as ``page layout'' or ``frame layout''.
We will use the term ``window layout''.

Our proposal is to cast window layout as a linear programming problem.
The two key ideas are:
\begin{itemize}
\item Most useful constraints on nested and tiled windows
can be expressed as affine inequalities 
on the window location (left, top) and size (width, height) coordinates.
\item A point satisfying a set of affine inequalities can be computed
efficiently using the simplex method for linear programming.
\end{itemize}
In addition, one can express additional desirable properties of a layout
in the choice of cost function to minimize.

\section{An Example}

A trivial example of a window layout, a hypothetical menu bar, 
is shown in figure~\ref{menu-fig}.

\begin{figure}[t]
\setlength{\unitlength}{1.0cm}
\center
\begin{picture}(5.3,0.7)
\put(0,0){\special{psfile=menu.ps
hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
\caption{A simple menu bar.
\label{menu-fig}}
\end{figure}

The menu bar consists of 4 display objects, 
whose rectangular regions are shown in figure~\ref{boxed-menu-fig}. 
The layout is designed with several constraints in mind:
the rectangular regions must be big enough to enclose their text, 
printed in the given font,
they should be adjacent horizontally, perhaps with a small spacing,
and they should have equal top coordinates.

\begin{figure}[t]
\setlength{\unitlength}{1.0cm}
\center
\begin{picture}(5.3,0.7)
\put(0,0){\special{psfile=boxed-menu.ps
hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
\caption{The bricks in the menu bar.
\label{boxed-menu-fig}}
\end{figure}

\section{Systems of Linear Equations}
\label{le-sec}

Most systems that provide menu bars would compute the layout
procedurally, doing something like the following.
Let $l_i$ be the left side of menu item $i$, 
$w_i$ its width,
$t_i$ its top,
and $h_i$ its height.
Then $w_i$ and $h_i$ would be determined from the size of the string
as printed in the given font.
For a menu bar at the top of the window, 
$t_i \leftarrow 0$ for all $i$.
Finally, $l_1 \leftarrow 0$ 
and $l_{i+1} \leftarrow l_i + w_i$, for $i=2 \ldots n$.

To convert this to a more declarative approach,
we first replace the assignment statements above
by corresponding equalities (eg. $l_{i+1} = l_i + w_i$).

Then the procedure can be viewed an example 
of solving a set of equations by local propagation,
a technique common in constraint languages \cite{Lele88}.
Local propagation requires
the equations to be organized in a way that allows individual coordinates
of the solution to be computed one at a time and substituted
into the remaining equations.
In other words,
the graph of dependencies between the coordinates cannot contain cycles.
It does not requires that the equations be linear 
(see, for example, IDEAL \cite{vanWyk82}).
Cycles can be expensive to detect and impossible to eliminate.
Cycles can sometimes be dealt with using relaxation \cite{Born79,Stee80},
but it not possible to guarantee that a relaxation iteration will
converge to a solution, or anything at all.
It is also difficult to detect under- and over-constrained problems.

An alternative approach to this example
is to note that all the equations are linear.
In applications where linear relationships 
provide a sufficiently rich set of constraints,
there are significant advantages to forming
and solving an linear system in matrix form:
\prob{
Given \= $B: \Re^n \mapsto \Re^m$
and $\vec{b} \in \Re^m$ \\
Find \> $\vec{x} \in \Re^n$  
such that $B\vec{x} = \vec{b}$ 
}
There exist well understood, efficient, numerically robust
algorithms for solving linear systems \cite{Golu89}
(and public domain code for those algorithms 
\cite{lapack-users-guide92,Dong79}),
with many variants to take advantage of special properties of $B$.
Circular dependencies among the coordinates are not an issue.
Under- and over-constrained problems are easily detected,
and can be dealt with in standard ways 
--- using least squares in the over-constrained case
and choosing a minimum norm solution in the under-constrained case.
Another advantage is that information developed in the solution
of one system can be reused via updating formulas 
to greatly speed the solution of related systems.
Updating formulas exist for adding or deleting columns or rows
in the matrix $B$,
which would happen, for example,
if one added or deleted menu items.

\section{Linear Programming}
\label{lp-sec}
 
If systems of linear equations have so many advantages,
why are we proposing the use of linear programming?

The general linear programming problem is \cite{Flet89,Gill91,Pres88}:
\prob{
Find an $\vec{x} \in \Re^n$ \= \\
that minimizes:\> $\vec{c} \cdot \vec{x}$\\
subject to: \= $A\vec{x} \geq \vec{a}$ \\
            \> $B\vec{x} = \vec{b}$ \\
\\
For \= $A: \Re^n \mapsto \Re^{m_1}$ \\
and \> $B: \Re^n \mapsto \Re^{m_2}$.
}
(The expression $A\vec{x} \geq \vec{a}$ is interpreted per coordinate,
that is,
$(A\vec{x})_i \geq {\vec{a}}_i$ for $i = 1 \ldots m_1$.)

Linear programming is a generalization of systems of linear equations:
If there are no inequality constraints ($m_1$ is 0),
and the cost vector $\vec{c}$ is $\vec{0}$,
then the linear programming problem 
reduces to the system of linear equations
$B\vec{x} = \vec{b}$.

A system based on linear programming is strictly
preferable to one based on linear equations,
because it is easy to recognize
the special case of pure linear equations
and solve it as efficiently as possible.
The code required to solve linear programming problems 
(via the simplex method)
is somewhat more complex than code to solve that solves linear equations,
but not forbiddingly so.
Each iteration of the simplex method requires
solving a system of linear equations;
doing this efficiently is probably the most complex part
of an implementation of the simplex method.

A more significant advantage of linear programming is that is allows
us to express our example in a less restrictive, more robust way.
We get the same layout if 
\begin{enumerate}
\item we replace the equalities with inequalities: \\
$w_i \geq w_i^0$, \\
$h_i \geq h_i^0$, \\
(where $w_i^0$ and $h_i^0$ are the width and height required to print
item $i$)\\
$t_i \geq 0$, \\
$l_1 \geq 0$, \\
and $l_{i+1} \geq l_i + w_i$.
\item use $\vec{1}$ as the cost vector $\vec{c}$,
which is equivalent to minimizing $\sum w_i + h_i + l_i + t_i$.
\end{enumerate}
Converting from equalities to inequalities simplifies making
small changes to the layout without disturbing important relationships.
We will illustrate this point in section~\ref{revisit-sec}.

Linear programming has a simple geometric interpretation,
which helps provide some intuition about what sorts of 
window layout constraints can and cannot be expressed.
If you imagine rotating the domain space so that
the cost vector $\vec{c}$ is pointed down,
then the goal of linear programming is to find a point on the bottom
of the {\it feasible region}.

The feasible region is a single convex polyhedron,
formed by taking the intersection 
of the $[n - rank(B)]$ dimensional hyperplane 
satisfying $B\vec{x} = \vec{b}$
with the $m_1$ halfspaces determined
by each inequality $(A\vec{x})_i \geq {\vec{a}}_i$.
If the feasible region is empty, 
then the problem is over-constrained and has no solution.
If the feasible region is open at the bottom,
then the problem is under-constrained.
If the problem is sufficiently constrained,
then the bottom of the feasible region 
is a face of the convex polyhedron.
The bottom face may be a single vertex (a zero-dimensional face),
or a one-dimensional edge, or a two-dimensional polygon, etc.
The bottom face is guaranteed to contain at least one vertex.

The simplex method is a well understood algorithm for solving
linear programming problems.
It also has a simple geometric interpretation:
First find one of the vertices of the feasible region.
(This can be done bby solving another linear programming problem
which is constructed in such a way that an initial vertex is known.)
Follow any edge going down to another vertex.
Continue until there are no edges going down.
(This intuitive description ignores a number of details
that are important for a successful implementation,
such as the fact that it is possible to encounter a vertex with
no edges going down before reaching the bottom.)

The simplex method is guaranteed 
to complete correctly in a finite number of steps,
either returning a global minimum
or reporting that the problem is under- or over-constrained.
Although the worst case bound on the number of steps grows rapidly 
with increasing $n$ and $m_1$,
the average costs grows much more slowly:
``For reasons not fully understood,
the number of simplex iterations needed to solve a `typical'
standard-form problem with $n$ variables and $m$constraints
is {\sl usually} a small multiple (say, 3--6) of {\it m}.
The number of iterations also seems 
to display a sublinear growth with $n$'' Gill et al. 396, \cite{Gill91}.

The bulk of the work in each iteration is solving an $n \times n$
system of linear equations,
which takes $n^3$ operations in a naive implementation (like ours)
and $n^2$ in an implementation that make proper use of updating formulas.
Thus the average complexity of the simplex method grows more slowly than
$m_1n^4$ ($m_1n^3$ with updating),
depending on what exactly ``sublinear growth with $n$'' turns out to be.
$n$ and $m_1$ have been approximately equal in all our layouts,
so the average complexity of this approach to solving window layout
grows more slowly than $n^5$ ($n^4$ with updating).

\section{An Implementation}

Bricklayer is a system 
for specifying and solving window layouts using linear programming.
Bricklayer is written in Common Lisp \cite{Stee90}.
It is derived from the masters thesis 
by Bill and Lundell \cite{Bill90}
and uses a translation 
to Common Lisp (with small modifications)
of the simplex method as presented 
in {\it Numerical Recipes in C}~\cite{Pres88}
by Michael Sannella.
A principal design goal of Bricklayer
is to produce a small, stand alone module 
that can be easily used by other Lisp systems 
to specify and solve window layouts.
Bricklayer is available on an as-is basis,
via anonymous ftp, in /pub/az/brick.tar.Z,
on belgica.stat.washington.edu.

The basic interface between Bricklayer and other Common Lisp
code is through a generic function interface 
based on a {\sf Brick} abstract type.
A brick corresponds to a rectangular region in $\Re^2$ 
whose sides are parallel to the $x$ and $y$ axes.
Such regions are common abstractions in graphics and window systems;
some terms that are used for such regions are:
``window'', ``rectangle'', ``region'', and ``box'';
we chose ``brick'' to avoid confusion 
with the abstractions in systems using Bricklayer.

Any Lisp object can be treated as a {\sf Brick}
if it has methods defined for eight generic functions ---
four reader generic functions,
{\sf left, width, top,} and {\sf height,}
and four corresponding {\sf setf} (writer) generic functions.

We choose the (left, width, top, height) representation 
over the (left, right, top, bottom)
alternative for somewhat obscure technical reasons.
To keep objects from turning inside out,
we have to ensure that $r \geq l$ or, equivalently,
that $w \geq 0$
(and similar relations between top, bottom, and height).
We choose the (width, height) representation
because our implementation of the simplex method is particularly efficient
with constraints of the form $x \geq 0$ for any coordinate $x$.

The simple generic function
makes it easy to use Bricklayer as an independent module,
to specify and compute layouts using the objects provided 
by any other Common Lisp graphics or user interface management system.
For example, Bricklayer can be used to directly specify and compute
the layout of a set of X windows, 
by defining the appropriate 8 methods 
for the {\sf xlib:Window} type \cite{CLX89}.

In linear programming, 
both the cost function and the affine inequalities 
are defined using linear functionals on the domain space.
A domain space in Bricklayer corresponds to a list of bricks.
The user's first step is computing a layout 
is to define the domain of the layout, 
represented by an instance of {\sf Domain}
which is created by applying {\sf make-domain} to a list of bricks.

A linear functional on a {\sf Domain}
is represented by an instance of {\sf Functional},
which is created by applying {\sf functional} to a list of {\sf Terms}.
Each {\sf Term} has the form: 
\cd{\sf (coefficient parameter brick)},
where the {\sf coefficient} is a {\sf Real},
the {\sf parameter} is one of the {\sf Keyword}s: \cd{\sf :left, :width, :top,}
or \cd{\sf :height},
and 
the {\sf brick} is one of the elements of the domain brick list.
For example, the functional corresponding to the vector $\vec{1}$
on a domain consisting of a single brick is $l + w + t + h$
and is represented by a list of the form
\cd{\sf ((1 :left b) (1 :width b) (1 :top b) (1 :height b))}.

An affine inequality over a {\sf Domain}
is represented by an instance of {\sf Affine-Inequality},
which has the form:
{\it (constant relation functional)},
where {\it constant} is a {\sf Real},
{\it relation} is one of the symbols {\sf '$<=$}, {\sf '$=$}, or {\sf '$>=$},
and {\it functional} is a {\sf Functional}.
So the inequality $l_2 \geq l_1 + w_1$,
equivalently $0 \geq -l_2 + l_1 + w_1$
is represented by a list of the form
\cd{\sf (0 {\tt>}= ((-1 :left b2) (1 :left b1) (1 :width b1))))}.

Layouts are computed by applying {\sf solve-constraints}
to three arguments: a cost functional,
a list of affine inequalities (including equalities), and a domain.
It ``returns'' the solution by 
destructively modifying the bricks in the domain ---
by applying the generic functions 
{\sf (setf left), (setf width), (setf top),}
and {\sf (setf height)} to the bricks, as needed.

\section{Natural Constraints}
\label{can-be-sec}

Most users and user interface designers do not think
of window layout in terms of linear functionals 
and affine inequalities over $\Re^n$.
Natural specifications consist of statements like:
``Menu bar item 1 should be to the left of menu bar item 2.''
``The items in a vertical menu should form a left-adjusted column.''

To get some intuition about what natural constraints
can and cannot be represented 
by sets of affine equalities and inequalities,
it helps to first realize that a brick is
the cartesian product of an interval on the x-axis $[l,l+w]$
and an interval on the y-axis $[t,t+h]$.
It turns out that many common constraints
on bricks relate the x-intervals of different bricks,
or the y-intervals, but not both.

Such natural constraints as ``fixed width,'' ``fixed left side,''
``centered-in-x,'' ``left-adjusted,'' and ``right-adjusted'' 
can be expressed as affine equalities on x-intervals.
For example, asserting that brick 1 and brick 2 are ``left-adjusted''
is the same as saying $0 = l_1 - l_2$.
Constraints like ``left-of,'' ``right-of,'' ``within-x,''
and ``surround-in-x'' can be expressed as affine inequalities on x-intervals.
Asserting that brick 1 is left of brick 2 is equivalent to
$0 \leq l_2 - l_1 -w_1$.

A constraint such as ``left-adjusted column'' clearly
involves both x and y coordinates.
However, the affine equalities and inequalities 
that are required to define a left-adjusted column
can be separated into two independent subsets,
one involving only x-coordinates,
which results from asserting that each brick is left-adjusted 
with the one following it in the column,
and one involving only y-coordinates,
which results from asserting that each brick is 
above the one following it.

Constraints that result in an affine equality or inequality
that refers
to both x and y coordinates are rare.
Examples of non-separable equality constraints
are ``square'' and ``fixed aspect ratio.''
An example of a non-separable inequality constraint
is ``width must be greater than height.''

In Bricklayer, natural, high-level constraints are represented
by functions that return lists of affine inequalities.
Built in high level constraints include:
{\sf fixed, minimum, maximum, equal-parameter, equal-horizontal-side,
equal-vertical-side, surround-x, surround-y, surround, to-right, to-left,
below, above, in-row, in-column, center-x, center-y,}
and {\sf center.}
An interface designer can easily define additional high level constraints
by defining new functions.

A Lisp function for ``left-adjusted'' is:
\begin{tightcode}\sf
(defun left-adjusted (b1 b2) 
  (list (list 0 = (list 1 :left b1) (list -1 :left b2))))
\end{tightcode}
Another useful constraint is ``above'':
\begin{tightcode}\sf
(defun above (b1 b2)
  (list 
   (list 0 {\tt<=}
	 (list -1 :top b1) (list -1 :height b1)
	 (list 1 :top b2))))
\end{tightcode}
These two functions simplify the definition of a more complicated constraint,
``left-adjusted column'':
\begin{tightcode}\sf
(defun left-adjusted-column (bricks) 
  (mapcon
   #'(lambda (remainder)
       (let ((b1 (first remainder))
	     (b2 (second remainder)))
	 (nconc (left-adjusted b1 b2)
		(above b1 b2))))
   bricks))
\end{tightcode}
These functions are simplified versions of the ones in Bricklayer,
with options such as spacing parameters left out.
(For rusty Lisp programmers, 
{\sf mapcon} iterates,
applying its first function argument
to successive sublists of its second argument,
concatenating together the results of each application.
{\sf nconc} concatenates two lists together.)

Most applications of Bricklayer use a cost functional corresponding
to the vector $\vec{1}$.
This cost minimizes the sum of all the brick coordinates.
We always constrain every brick coordinate to be positive,
so minimizing the sum of the coordinates tends to minimize
each coordinate as well.
In other words, the widths and heights of each brick are minimized
and the bricks are placed as close to the upper left corner (0,0) as permitted.
This is roughly equivalent to minimizing the overall size of the layout,
which is what one usually wants.

We require all coordinates to be positive
for technical reasons relating to our implementation
of the simplex method.
This would usually be a reasonable assertion anyway.
Width and height must be positive, by the definition of a brick.
Requiring left and top to be positive keeps the bricks in the 
positive quadrant of the window or screen.

Another common choice of cost function
arises when one wants one brick to be as large as possible, 
subject to fitting everything necessary into a display with limited
total size.
An example of this is a text editor 
where one wants to devote as much space 
as possible to the text being edited, 
while leaving just enough room to fit
a title bar, a menu bar, a scroll bar, a status line, 
and a command buffer around the edges of the window.
A similar example is a data plot, 
where one wants to maximize the plotting region,
while leaving enough room for labels and coordinate axes, etc.
The desired result is achieved by minimizing 
the negative width and height of the important brick added to
the positive sum of all the other coordinates.

Note that a linear loss function is always separable 
into the sum of a linear function over the x-coordinates
and a linear function over the y-coordinates.
Minimizing the two functions separately gives the same result
as minimizing over all coordinates at once.

Identifying window layouts that can be solved 
as independent x and y subproblems 
is of interest partially for performance reasons;
solving two half-sized linear programming problems
will be roughly 16 times faster than one full-sized problem
(depending on how clever the implementation of the simplex method is).
It is perhaps more important as a conceptual simplification;
it is easier to think about constraints on intervals
than on rectangles.

\section{The Example revisited}
\label{revisit-sec}

We can develop some more intuition 
how linear programming can be used by considering the example
shown in figures \ref{menu-fig} and \ref{boxed-menu-fig}.
The code used to define the layout of the simple menu bar is:
\begin{tightcode}
\sf
(defparameter *border* (make-brick))

(defparameter *items*
    (list (make-brick :name "First")
          (make-brick :name "Second")
          (make-brick :name "Third")
          (make-brick :name "Fourth")))

(defparameter *domain*
    (make-domain (cons *border* *items*)))

(defparameter *cost* (default-cost *domain*))

(defparameter *constraints*
    (nconc
     (mapcan #'name-sized *items*)
     (apply #'surround *border* 2 2 *items*)
     (apply #'centered-row 4 *items*)))

(solve-constraints *cost* *constraints* *domain*)
\end{tightcode}
The layout domain contains 5 bricks:
one for the border of the menu bar and 4 for the menu items.
We use the default cost, which minimizes the sum of all the coordinates.
The set of constraints is created using
{\sf name-sized} to ensure that each brick is big enough
to print its name,
{\sf surround} to ensure that the boundary encloses all the items,
and {\sf centered-row}, which is similar to {\sf left-adjusted-column},
to ensure that the items are adjacent horizontally and lined up vertically.

We stated in section~\ref{lp-sec} 
that linear programming simplifies make small changes
to a display.

\begin{figure*}[t]
\setlength{\unitlength}{1.0cm}
\center
\begin{picture}(10.2,1.2)
\put(0,0){\special{psfile=extended-menu.ps
hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
\caption{A menu bar with brief explanations.
\label{extended-menu-fig}}
\end{figure*}

Consider a small elaboration
of the menu bar example.
It may be helpful
to be add some explanatory text to menu bars.
An example of this is shown in figure~\ref{extended-menu-fig}.
Because screen space is always scarce, 
it should be easy to switch 
between the terse and verbose versions of the menu bar.

\begin{figure*}[t]
\setlength{\unitlength}{1.0cm}
\center
\begin{picture}(10.2,1.2)
\put(0,0){\special{psfile=boxed-extended-menu.ps
hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
\caption{The bricks in the menu bar with explanations.
\label{boxed-extended-menu-fig}}
\end{figure*}

The rectangular regions corresponding to the display objects in
figure~\ref{extended-menu-fig}
are shown in figure~\ref{boxed-extended-menu-fig}.
The first line of menu items obeys the same constraints
as the items in figure~\ref{boxed-menu-fig}.
The second line of explanatory text obeys a set of analogous constraints.
In addition, each menu item and its explanatory text
should be placed in a left adjusted column.
The lisp code implementing the documented menu is:
\begin{tightcode}
\sf
(defparameter *new-items*
    (list (make-brick :name "Do number 1.")
          (make-brick :name "Try, try again.")
          (make-brick :name "3rd is lucky.")
          (make-brick :name "Go fourth.")))

(defparameter *new-domain*
    (make-domain (cons *border*
                       (concatenate 'List
                         *items*
                         *new-items*))))

(defparameter *new-cost*
    (default-cost *new-domain*))

(defparameter *new-constraints*
    (concatenate 'List
      *constraints*
      (nconc
       (mapcan #'name-sized *new-items*)
       (apply #'surround *border* 1 1 *new-items*)
       (apply #'centered-row 8 *new-items*)
       (mapcan
        #'(lambda (b0 b1)
            (left-adjusted-column 4 b0 b1))
        *items* *new-items*))))

(solve-constraints 
  *new-cost* *new-constraints* *new-domain*)
\end{tightcode}
The key points in this code are:
\begin{enumerate}
\item The domain is the old domain with the new items added.
\item The constraints are the old constraints concatenated
with new constraints relating new items to each other or relating
new items to old items.
\end{enumerate}
To go back to the original menu bar, we need only remove
the new items and the new constraints and resolve.

This illustrates one of the key advantages of linear programming.
We can usually add items to a display by extending the domain
and adding constraints,
without changing existing items and constraints.
We can usually delete items from a display by removing them
from the domain and deleting any constraints in which they appear.
The spatial relationships between items
which remain in a display remain constant,
providing important consistency for the user.

\section{Limitations}
\label{cant-be-sec}

An obvious limitation of linear programming is 
that no non-linear function, such as area, can appear 
in either the constraints or the loss function.
Thus we can't fix, bound, maximize, or minimize areas.
However, in some cases a satisfactory result can often 
be obtained by substituting a linear function,
such as width plus height, instead of area.

A less obvious limitation is the fact that
the feasible region is a single convex polyhedron,
the {\it intersection} of a hyperplane and $m_1$ half-spaces.
A natural constraint one might want to express
is the assertion that two bricks should not intersect,
but that their positions are otherwise unconstrained.
This is equivalent to saying that brick 1 is left of brick 2
{\it or} right of brick 2 {\it or} above {\it or} below.
The feasible region corresponding to this statement is the
union of half spaces and
so the problem cannot be solved simply using linear programming.

A related problem one might imagine could be solved with linear programming
is text formatting.
However, line breaking is really just a bit more complicated version
of a non-intersecting rectangle constraint.
One starts with a sequence of word bricks
One would like to be able to assert that
two adjacent bricks in the sequence should be either
adjacent and ordered left to right, {\it or} should be on adjacent lines.

A related approach to layout of rectangular regions,
which is used for line breaking,
is the boxes-and-glue model of \TeX~\cite{Knuth84}.
The boxes-and-glue model has also been used 
for layout in graphical user interfaces
in FormsVBT~\cite{Avrahami89}
and Interviews~\cite{Interviews89}.  

We find it difficult to make a precise comparison between linear programming
and boxes-and-glue, 
because it is difficult to tell from the published descriptions
exactly how a boxes-and-glue specification is solved,
beyond the fact that there is some attempt to minimize the glue stretch,,
whenever a line or page is not broken.
It is clear that boxes-and-glue is 
neither strictly more specialized nor more general 
than linear programming.
Boxes-and-glue is used for text formatting,
which can't be done with linear programming.
On the other hand, boxes-and-glue has a number of restrictions
which are not true of linear programming.
The boxes have a strict hierarchy --- hboxes
within vboxes within hboxes, etc.
Glue is only placed between adjacent boxes at a given level
of the hierarchy, with separate horizontal glue connecting hboxes
and vertical glue connecting vboxes.
In contrast, affine constraints can relate any two bricks.
The cost function can be used to minimize the spacing between bricks,
which is something like glue,
but it can also be used to maximize the size of a particular region,
which does not seem to be easily specified with boxes-and-glue.

\section{Performance}
\label{performance-sec}

Acceptable performance for automatic layout 
is on the order of tens of seconds
for layouts that only change at design time,
on the order of 1-2 seconds for layouts that are updated in response 
to an occasional user action (as described in the examples),
and less than 0.1 second for layouts that should update continuously,
eg., as a user reshapes a window.

The four trivial menu examples in the paper
take less than 0.1 seconds in the current implementation.
A more realistically sized problem, with 64 bricks (256 coordinates),
and 301 affine inequalities
is solved in 4.6 seconds on a Sparc 330, running Franz Allegro CL.

The current implementation is based 
on manipulations of a tableau data structure
(essentially a two dimensional array of floating point numbers).
Implementations of the simplex method that directly manipulate
a tableau have some advantages for pedagogical purposes,
but (as pointed out in Numerical Recipes \cite{Pres88}
and Gill et. al~\cite{Gill91}) 
are not as efficient or numerically robust
as implementations based on updating matrix decompositions.
Switching to a more modern implementation of the simplex method
should remove a factor of $n$ from the asymptotic cost of of the algorithm.

The original system developed by Bill and Lundell~\cite{Bill90}
had facilities for analyzing the constraint graph 
and automatically partitioning into independent subproblems
whenever possible.
The cost of the graph analysis was comparable to the cost of the simplex
method itself, so the performance gains were modest.
However, it is fairly easy to check for the common case
of independent x and y problems.
Splitting a linear programming problem in half should reduce
the time by about 1/16, 
bringing the realistic example down to a fraction of a second.

Profiling our simplex code shows that about 90\% of the time
is spent finding the initial feasible point.
A re-implementation of the simplex method could take advantage
of a good guess for an initial feasible point.
For example, when we delete items from a display,
the current state of the remaining items will often be feasible
for the remaining constraints.

\section{Acknowledgements}
 
The simplex code used in Bricklayer is
a slightly modified version 
of a translation of the Numerical Recipes simplex code from C to Common Lisp
by Michael Sannella, with some bug fixes by Molly Wilson.

