\documentstyle[code,twoside]{article}
\pagestyle{headings}

\topmargin=-0.2in
\textwidth=6.0in
\textheight=8.0in
\oddsidemargin=0.25in
\evensidemargin=0.25in

\newcommand{\prob}[1]
{\begin{center}
\fbox{
\begin{minipage}{\textwidth}
\begin{tabbing}
#1
\end{tabbing}
\end{minipage}} % end fbox
\end{center}}

\begin{document}

\title{Bricklayer: window layout using linear programming}

\author{{\sc Thomas Bill}\\
{\sc Bertil Lundell}\\
{\sc John Alan McDonald}\\
{\sc Michael Sannella}
\thanks{This work was supported in part 
the Dept. of Energy under contract FG0685-ER25006
and by NIH grant LM04174 from the National
Library of Medicine.}
\\
Dept. of Computer Science and Engineering\\
and\\
Dept. of Statistics\\
University of Washington}
\date{May 1992}

\maketitle
\begin{abstract}

This report describes the Bricklayer module.
Bricklayer is a system 
for specifying and solving constraints on {\it bricks} 
--- rectangular regions in $\Re^2$ whose sides are parallel
to the $x$ and $y$ axes.
It is intended as a tool 
for specifying and computing 
the layouts of typical user interface control panels consisting
of a number of nested and tiled subwindows.
The two key ideas underlying Bricklayer are:
\begin{itemize}
\item Nearly all useful constraints on nested and tiled windows
can be expressed as affine inequalities 
on the window location (left, top) and size (width, height) coordinates.
\item A point satisfying a set of affine inequalities can be found
using the simplex method for linear programming.
\end{itemize}

\end{abstract}

\begin{flushleft}
Keywords: Constraint Languages, Numerical Optimization, 
User Interface Management Systems
\end{flushleft}

\vfill
\pagebreak

\tableofcontents

\listoffigures

\vfill
\newpage

\part{An Arizona module}

\section{Installation}

This document describes the Bricklayer module,
a component of a system called Arizona,
now under development at the U. of Washington.

Arizona is intended to be a portable, public-domain collection 
of tools supporting scientific computing, quantitative graphics, 
and data analysis,
implemented in Common Lisp
and CLOS (the Common Lisp Object System) \cite{Stee90}.
This document assumes the reader is familiar with Common Lisp and CLOS.
An overview of Arizona is given in \cite{McDo88b}
and an introduction to the current release is in \cite{McDo91g}.

The source code implementing Bricklayer is found in brick/. 
The definitions are in the {\sf Bricklayer} package.
Bricklayer is independent of the rest of Arizona.

Bricklayer is a system 
for specifying and solving constraints on {\it bricks,} 
rectangular regions in $\Re^2$ whose sides are parallel
to the $x$ and $y$ axes.
The primary application of Bricklayer
is specifying and computing 
the layouts of diagrams that consist 
of a number of nested and tiled subwindows.
Bricklayer is a small, stand alone module that can be used to solve
window layout problems by any Common Lisp graphics or user interface system.

The two key ideas underlying Bricklayer are:
\begin{itemize}
\item Nearly all useful constraints on nested and tiled windows
can be expressed as affine inequalities 
on the window location (left, top) and size (width, height) coordinates.
\item A point satisfying a set of affine inequalities can be found
using the simplex method for linear programming.
\end{itemize}
One can express additional desirable properties of a layout
in the choice of cost function to minimize.
For example, one usually wants to make diagrams as small as possible,
which be achieved by minimizing the sum of all the 
(positive) window size and location coordinates.

Bricklayer is derived from the masters thesis 
by Bill and Lundell \cite{Bill90}.

\subsection{Getting Bricklayer}

Bricklayer is available via anonymous ftp to belgica.stat.washington.edu,
in the directory {\sf pub/az}, as the compressed file {\sf brick.tar.Z}.

\subsection{Documentation}

This document is {\sf brick/doc/tr.tex}.

\subsection{Compiling and Loading}

Bricklayer is compiled by loading the file {\sf make.lisp}.
make.lisp contains two versions of code for compiling Bricklayer:
one, normally commented out,
uses a defsystem facility written by Mark Kantrowitz \cite{Kant91},
the other, the default, requires no outside code.

The list of files for Bricklayer includes {\sf clx-tools},
{\sf test-brick}, and {\sf tests},
which are only used for testing.
These files can be commented out 
to save a small amount of space in a running system.

Once compiled, Bricklayer can be loaded by loading {\sf load.lisp}.

\subsection{Portability}

The test files require CLX \cite{CLX89}.
Other than that, Bricklayer requires no outside support,
and should run or be easily ported to any CLTL2 compliant lisp \cite{Stee90}.

Bricklayer has been tested in:
\begin{itemize}
\item Franz Inc., Allegro CL 4.0.1 [Sun4] (2/8/91)
\end{itemize}

\subsection{Known problems and limitations}

Our simplex code does not always
correctly recognize unbounded problems.
This may be our own bug or may be inherited from Numerical Recipes 
\cite{Pres88}.
We do not intend to fix it directly,
but rather replace the simplex code with a more efficient version, 
similar to that described in Gill et al. \cite{Gill91}.

The simplex method as implemented in Numerical Recipes
is done in a standard form the assumes all coordinates
are constrained to be positive. 
In other words, 
the problem is solved subject to the constraints:
$x_i \geq 0 \forall i$,
in addition to whatever inequalities the user defines.
This may or may not change in a redesign of the simplex code.

\newpage

\part{Background}

\input{background}

\bibliographystyle{plain} 

\bibliography{../../b/arizona,../../b/cs,../../b/cactus,../../b/constraint,../../b/database,../../b/layout,../../b/lisp,../../b/mcdonald}


\newpage
\part{Reference Manual}
\label{reference-manual}
\pagestyle{myheadings}

These reference manual entries were produced automatically,
using the Definitions module described in \cite{McDo91h}.

\vskip 1.5em

\input{ref}

\pagestyle{headings}

\end{document}
