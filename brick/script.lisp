;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Bricklayer -*-

(in-package :Bricklayer)

;;;============================================================

(mk:compile-system :Bricklayer)
(mk:compile-system :Bricklayer-Test)

;;;============================================================

(solve-twt)

(progn
(solve-menu)
(xlib:destroy-window *menu-window*)
(ps-dump *menu-window* "/belgica-2g/jam/az/brick/doc/menu.ps")

(draw-menu-with-boxes)

(ps-dump *menu-window* "/belgica-2g/jam/az/brick/doc/boxed-menu.ps")

(solve-extended-menu)

(ps-dump *menu-window* "/belgica-2g/jam/az/brick/doc/extended-menu.ps")

(draw-extended-menu-with-boxes)

(ps-dump *menu-window* "/belgica-2g/jam/az/brick/doc/boxed-extended-menu.ps")
)

;;;============================================================

(profile simplx-feasible-point)
(profile simplx-optimize)

(compile 'solve-it)

(solve-it)