;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; Simple menu example
;;;============================================================

(defparameter *menu-window* nil)

(progn
  (defparameter *menu-boundary* (make-test-brick :name "Enclosing Brick"))
  (defparameter *menu-font*
      (let ((font-names (xlib:list-font-names (clx-display) "*fixed*")))
	    (xlib:open-font (clx-display) (second font-names))))
  (defparameter *menu-gcontext* (make-gcontext :font *menu-font*))

  (defparameter *menu-items*
      (mapcar #'(lambda (s)
		  (make-test-brick :name s :gcontext *menu-gcontext*))
	      '("First"
		"Second"
		"Third"
		"Fourth")))
  (defparameter *menu-domain*
      (make-domain (cons *menu-boundary* *menu-items*)))
  (defparameter *menu-cost* (default-cost *menu-domain*))
  )

(defun name-sized (domain b)
  (fixed-size domain (name-string-width b) (name-string-height b) b))

(defun menu-constraints (domain boundary items)
  (nconc
   (mapcan #'(lambda (item) (name-sized domain item)) items)
   (apply #'surround domain boundary 2 2 items)
   (apply #'centered-row domain 4 items)
   ))

(defparameter *menu-constraints*
    (menu-constraints *menu-domain*
		      *menu-boundary*
		      *menu-items*))

(defparameter *menu-tableau*
    (allocate-tableau *menu-constraints* *menu-domain*))

(defun solve-menu ()
  (let ((case (time
	       (solve-constraints *menu-cost* *menu-constraints* *menu-domain*
				  :tableau *menu-tableau*))))
    (when *menu-window* (xlib:destroy-window *menu-window*))
    (setf *menu-window* (make-window :left 500 :top 550
				     :width (+ 2 (width *menu-boundary*))
				     :height (+ 2 (height *menu-boundary*))))
    (when (eq case :solution)
      (draw-brick *menu-boundary* *menu-window* :box? t)
      (map nil #'(lambda (b) (draw-brick b *menu-window* :name? t))
	   *menu-items*))
    (xlib:display-force-output (xlib:window-display *menu-window*))
    ))

(defun draw-menu-with-boxes ()
  (draw-brick *menu-boundary* *menu-window* :box? t)
  (map nil #'(lambda (b) (draw-brick b *menu-window* :name? t :box? t))
       *menu-items*)
  (xlib:display-force-output (xlib:window-display *menu-window*)))

;;;============================================================
;;; Extended menu example
;;;============================================================

(defparameter *new-menu-items*
    (mapcar
     #'(lambda (s)
	 (make-test-brick :name s
			  :gcontext *menu-gcontext*))
     '("Do number 1."
       "Try, try again."
       "3 is lucky."
       "Go fourth.")))

(defparameter *extended-menu-domain*
    (make-domain (cons *menu-boundary*
		       (concatenate 'List *menu-items* *new-menu-items*))))

(defparameter *extended-menu-cost* (default-cost *extended-menu-domain*))

(defun new-menu-constraints (domain boundary old-items new-items)
  (nconc
   (menu-constraints domain boundary old-items)
   (mapcan #'(lambda (b) (name-sized domain b)) new-items)
   (apply #'surround domain boundary 2 2 new-items)
   (apply #'centered-row domain 8 new-items)
   (mapcan #'(lambda (b0 b1) (left-adjusted-column domain 4 b0 b1))
	   old-items new-items)))


(defparameter *extended-menu-constraints*
    (new-menu-constraints *extended-menu-domain*
			  *menu-boundary*
			  *menu-items*
			  *new-menu-items*))

(defparameter *extended-menu-tableau*
    (allocate-tableau *extended-menu-constraints* *extended-menu-domain*))

(defun solve-extended-menu ()
  (let ((case (time
	       (print
		(solve-constraints *extended-menu-cost*
				   *extended-menu-constraints*
				   *extended-menu-domain*
				   :tableau *extended-menu-tableau*)))))
    (when (eq case :solution)
      (when *menu-window* (xlib:destroy-window *menu-window*))
      (setf *menu-window*
	(make-window :left 500 :top 550
			:width (+ 2 (width *menu-boundary*))
			:height (+ 2 (height *menu-boundary*))))
      (draw-brick *menu-boundary* *menu-window* :box? t)
      (map nil #'(lambda (b) (draw-brick b *menu-window* :name? t))
	   *menu-items*)
      (map nil #'(lambda (b) (draw-brick b *menu-window* :name? t))
	   *new-menu-items*))
    (xlib:display-force-output (xlib:window-display *menu-window*))))

(defun draw-extended-menu-with-boxes ()
  (draw-menu-with-boxes)
  (map nil #'(lambda (b) (draw-brick b *menu-window* :name? t :box? t))
       *new-menu-items*)
  (xlib:display-force-output (xlib:window-display *menu-window*)))

;;;============================================================
;;; 64 brick example
;;;============================================================


(defparameter *window* nil)

(defparameter *n* 16)
(defparameter *a-bricks*
      (az:with-collection
       (dotimes (i *n*)
	 (az:collect (make-test-brick :name (format nil "a~d" i))))))
(defparameter *b-bricks*
      (az:with-collection
       (dotimes (i *n*)
	 (az:collect (make-test-brick :name (format nil "b~d" i))))))
(defparameter *c-bricks*
      (az:with-collection
       (dotimes (i *n*)
	 (az:collect (make-test-brick :name (format nil "c~d" i))))))
(defparameter *d-bricks*
      (az:with-collection
       (dotimes (i *n*)
	 (az:collect (make-test-brick :name (format nil "d~d" i))))))
(defparameter *bricks*
      (concatenate 'List *a-bricks* *b-bricks* *c-bricks* *d-bricks*))
(defparameter *domain* (make-domain *bricks*))
(defparameter *cost* (default-cost *domain*))

(defparameter *constraints*
      (nconc
       (apply #'minimum *domain* :width  (+ 16 (random 16)) *a-bricks*)
       (apply #'minimum *domain* :height (+ 16 (random 16)) *a-bricks*)
       (apply #'minimum  *domain* :width  (+ 16 (random 16)) *b-bricks*)
       (apply #'minimum *domain* :height (+ 16 (random 16)) *b-bricks*)
       (apply #'minimum *domain* :width  (+ 16 (random 16)) *c-bricks*)
       (apply #'minimum *domain* :height (+ 16 (random 16)) *c-bricks*)
       (apply #'minimum *domain* :width  (+ 16 (random 16)) *d-bricks*)
       (apply #'minimum *domain* :height (+ 16 (random 16)) *d-bricks*)
       (apply #'equal-parameter *domain* :top *a-bricks*)
       (apply #'equal-parameter *domain* :top *b-bricks*)
       (apply #'equal-parameter *domain* :top *c-bricks*)
       (apply #'equal-parameter *domain* :top *d-bricks*)
       (mapcan #'(lambda (a b c d) (center-x *domain* a b c d))
	       *a-bricks* *b-bricks* *c-bricks* *d-bricks*)
       (apply #'in-row *domain* 2 *a-bricks*)
       (apply #'in-row *domain* 2 *b-bricks*)
       (apply #'in-row *domain* 2 *c-bricks*)
       (apply #'in-row *domain* 2 *d-bricks*)
       (above *domain* (first *a-bricks*) 4 (first *b-bricks*))
       (above *domain* (first *b-bricks*) 4 (first *c-bricks*))
       (above *domain* (first *c-bricks*) 4 (first *d-bricks*))
       (minimum *domain* :left (random 6) (first *a-bricks*))
       (minimum *domain* :top (random 6) (first *a-bricks*))
       ))
  
(defparameter *tableau* (allocate-tableau *constraints* *domain*))

(defun solve-it ()
  (let ((case (time
	       (print
		(solve-constraints *cost* *constraints* *domain*
				   :tableau *tableau*)))))
    (if *window*
	(xlib:clear-area *window*)
      (setf *window* (make-window :left 600 :top 550 :width 500 :height 300)))
    (when (eq case :solution)
      (dolist (brick *bricks*)
	(draw-brick brick *window* :box? t :name? t)))
    (xlib:display-force-output (xlib:window-display *window*))))

;;;============================================================
;;; Two way table example, from Freedman, Pisani, and Purves
;;;============================================================
#||
(defparameter *twt-window* nil)


(defparameter *twt-y-labels* (list
			      (make-test-brick :name "Right Handed")
			      (make-test-brick :name "Left Handed")
			      (make-test-brick :name "Total")))

(defparameter *twt-x-labels* (list
			      (make-test-brick :name "Men")
			      (make-test-brick :name "Women")
			      (make-test-brick :name "Total")))

(defparameter *twt-data*
    (make-array '(3 3)
		:initial-contents '((2780 3281 6061)
				    (311 300 611)
				    (3091 3581 6672))))
(defparameter *twt-cells*
    (let ((mtx (make-array '(3 3))))
      (dotimes (i 3)
	(dotimes (j 3)
	  (setf (aref mtx i j)
	    (make-test-brick
	     :name (format nil "~d" (aref *twt-data* i j))))))
      mtx))

(defparameter *twt-cell-labels*
    (let ((mtx (make-array '(3 3))))
      (dotimes (i 3)
	(dotimes (j 3)
	  (setf (aref mtx i j)
	    (make-test-brick
	     :name (format nil "~d" (aref *twt-data* i j))))))
      mtx))

(defparameter *twt-row0* (list (aref *twt-cells* 0 0)
			       (aref *twt-cells* 0 1)
			       (aref *twt-cells* 0 2)))
(defparameter *twt-row1* (list (aref *twt-cells* 1 0)
			       (aref *twt-cells* 1 1)
			       (aref *twt-cells* 1 2)))
(defparameter *twt-row2* (list (aref *twt-cells* 2 0)
			       (aref *twt-cells* 2 1)
			       (aref *twt-cells* 2 2)))

(defparameter *twt-row1* (list (make-test-brick :name "311")
			       (make-test-brick :name "300")
			       (make-test-brick :name "611")))

(defparameter *twt-row2* (list (make-test-brick :name "3,091")
			       (make-test-brick :name "3,581")
			       (make-test-brick :name "6,672")))

(defparameter *twt-col0* (list (first *twt-row0*)
			       (first *twt-row1*)
			       (first *twt-row2*)))
(defparameter *twt-col1* (list (second *twt-row0*)
			       (second *twt-row1*)
			       (second *twt-row2*)))
(defparameter *twt-col2* (list (third *twt-row0*)
			       (third *twt-row1*)
			       (third *twt-row2*)))

(defparameter *twt-2x2* (make-test-brick :name "2x2"))
(defparameter *twt-3x3* (make-test-brick :name "3x3"))
(defparameter *twt-4x4* (make-test-brick :name "4x4"))

(defparameter *twt-bricks*
    (concatenate 'List
      *twt-y-labels*
      *twt-x-labels*
      *twt-row0*
      *twt-row1*
      *twt-row2*
      (list *twt-2x2* *twt-3x3* *twt-4x4*)))

(defparameter *twt-domain* (make-domain *twt-bricks*))
(defparameter *twt-cost* (default-cost *twt-domain*))

(defun twt-cell-constraints ()
  (let ((constraints ())
	(-count00 (- (aref *twt-data* 0 0)))
	(cell00 (aref *twt-cells* 0 0)))
    (dotimes (i 3)
      (dotimes (j 3)
	(let ((count (aref *twt-data* i j))
	      (cell (aref *twt-cells* i j))
	      (label (aref *twt-cell-labels* i j)))
	  (setf constraints
	    (nconc
	     (unless (= i j 0)
	       (make-inequality
		0.0d0 '=
		(make-functional *twt-domain*
				 (make-term -count00 :width cell)
				 (make-term count :width cell00))))
	     (square *twt-domain* cell)
	     (surround *twt-domain* cell 1 1 label)
	     (center *twt-domain* cell label)
	     (minimum *twt-domain* :width (name-string-width label) label)
	     (minimum *twt-domain* :height (name-string-height label) label)
	     constraints)))))
    constraints))

(defun twt-row-constraints ()
  (nconc (apply #'centered-row *twt-domain* 2 *twt-x-labels*)
	 (centered-row *twt-domain* 2 (first *twt-y-labels*)
		       (aref *twt-cells* 0 0)
		       (aref *twt-cells* 0 1)
		       (aref *twt-cells* 0 2))
	 (centered-row *twt-domain* 2 (second *twt-y-labels*)
		       (aref *twt-cells* 1 0)
		       (aref *twt-cells* 1 1)
		       (aref *twt-cells* 1 2))
	 (centered-row *twt-domain* 2 (third *twt-y-labels*)
		       (aref *twt-cells* 2 0)
		       (aref *twt-cells* 2 1)
		       (aref *twt-cells* 2 2))))

(defun twt-col-constraints ()
  (nconc (apply #'centered-column *twt-domain* 2 *twt-y-labels*)
	 (centered-column *twt-domain* 2
			  (first *twt-x-labels*)
			  (aref *twt-cells* 0 0)
			  (aref *twt-cells* 1 0)
			  (aref *twt-cells* 2 0))
	 (centered-column *twt-domain* 2
			  (second *twt-x-labels*)
			  (aref *twt-cells* 0 1)
			  (aref *twt-cells* 1 1)
			  (aref *twt-cells* 2 1))
	 (centered-column *twt-domain* 2
			  (third *twt-x-labels*)
			  (aref *twt-cells* 0 2)
			  (aref *twt-cells* 1 2)
			  (aref *twt-cells* 2 2))))

(defparameter *twt-constraints*
    (nconc
     (twt-cell-constraints)
     (twt-row-constraints)
     (twt-col-constraints)
     (apply #'surround *twt-domain* *twt-2x2* 2 2
	    (list (first *twt-row0*) (second *twt-row0*)
		  (first *twt-row1*) (second *twt-row1*)))
     (apply #'to-right *twt-domain* *twt-2x2* 4 *twt-y-labels*)
     (apply #'to-left *twt-domain* *twt-2x2* 4 *twt-col2*)
     (apply #'below *twt-domain* *twt-2x2* 4 *twt-x-labels*)
     (apply #'above *twt-domain* *twt-2x2* 4 *twt-row2*)
     
     (apply #'surround *twt-domain* *twt-3x3* 2 2
	    (cons *twt-2x2* *twt-row2*))
     (apply #'to-right *twt-domain*  *twt-3x3* 2 *twt-y-labels*)
     (apply #'below *twt-domain* *twt-3x3* 2 *twt-x-labels*)

     (apply #'surround *twt-domain* *twt-4x4* 2 2
	    (concatenate 'List *twt-x-labels* *twt-y-labels*))
     ))

(defparameter *twt-tableau*
    (allocate-tableau *twt-constraints* *twt-domain*))
(defun solve-twt ()
  (print (dimension *twt-domain*))
  (print (length *twt-constraints*))
  (let ((case (time
	       (print
		(solve-constraints *twt-cost* *twt-constraints* *twt-domain*
				   :tableau *twt-tableau*)))))
    (when *twt-window* (xlib:destroy-window *twt-window*))
    (when (and (> (width *twt-4x4*) 0)
	       (> (height *twt-4x4*) 0))
      (setf *twt-window* (make-window :left 500 :top 550
					:width (width *twt-4x4*)
					:height (height *twt-4x4*)))
      (when (eq case :solution)
	(draw-brick *twt-3x3* *twt-window* :box? t)
	(draw-brick *twt-2x2* *twt-window* :box? t)
	(map nil #'(lambda (b) (draw-brick b *twt-window* :name? t))
	     *twt-x-labels*)
	(map nil #'(lambda (b) (draw-brick b *twt-window* :name? t))
	     *twt-y-labels*)
	(map nil #'(lambda (b) (draw-brick b *twt-window* :name? t))
	     *twt-row0*)
	(map nil #'(lambda (b) (draw-brick b *twt-window* :name? t))
	     *twt-row1*)
	(map nil #'(lambda (b) (draw-brick b *twt-window* :name? t))
	     *twt-row2*))
      (xlib:display-force-output (xlib:window-display *twt-window*)))))
||#



