;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; Brick Parameters
;;;============================================================

(deftype Parameter () '(member :left :width :top :height))

;;;============================================================
;;; Brick Terms
;;;============================================================

(defun term? (term)
  (and (listp term)
       (typep (first term) 'Double-Float)
       (typep (second term) 'Parameter)
       (typep (third term) 'Brick)))

(deftype Term ()

  "A Term is a list whose form is: (coefficient parameter brick),
where the <coefficient> is a Number, the <parameter> is one of :left,
:width, :top, or :height, and the <brick> is an object that obeys the
Brick protocol."

  '(satisfies term?))

(defun make-term (x parameter brick)
  "Creates a Term."
  (declare (type Number x)
	   (type (Member :left :width :top :height) parameter)
	   (type Brick brick))
  (list (az:fl x) parameter brick))

(defun term-coefficient (term)

  "A term is one of:
 (coefficient :left #<brick>)
 (coefficient :width #<brick>)
 (coefficient :top #<brick>)
 (coefficient :height #<brick>)"
  
  (declare (type Term term)
	   (:returns (type Double-Float)))
  
  (first term))

(defun term-parameter (term)

  "A term is one of:
 (coefficient :left #<brick>)
 (coefficient :width #<brick>)
 (coefficient :top #<brick>)
 (coefficient :height #<brick>)"
  
  (declare (type Term term)
	   (:returns (type Parameter)))

  (second term))

(defun term-brick (term)
  "A term is one of:
 (coefficient :left #<brick>)
 (coefficient :width #<brick>)
 (coefficient :top #<brick>)
 (coefficient :height #<brick>)"
  
  (declare (type Term term)
	   (:returns (type Brick)))

  (third term))

;;;============================================================

(defmethod term-index (term (domain Domain))
  (+ (brick-index (term-brick term) domain)
     (ecase (term-parameter term)
       (:left 1)
       (:width 2)
       (:top 3)
       (:height 4))))

;;;============================================================

(defun term-list? (l)
  (and (listp l)
       (every #'term? l)))

(deftype Term-List ()
  "A Term-List is a list of Terms."
  '(satisfies term-list?))
