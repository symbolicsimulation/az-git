;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Bricklayer
  (:use :Common-Lisp #+:clos :CLOS)
  (:nicknames :Brk :Brick)
  (:export 
   ;;simplex

   left
   width
   ;;fixed-width?
   top
   height
   ;;fixed-height?

   ;;implements-brick-protocol?
   Brick
   Brick-List

   Domain
   bricks
   make-domain
   make-position-domain
   dimension
   brick-index
   coordinate-vector->domain!
   domain->coordinate-vector!

   Parameter
   Term
   make-term
   term-coefficient
   term-parameter
   term-brick

   Functional
   terms
   make-functional
   default-cost

   Inequality
   bound
   relation
   Inequality-List
   make-inequality

   allocate-tableau
   solve-constraints

   fixed
   fixed-position
   fixed-size
   minimum-size
   maximum-size
   minimum
   maximum
   equal-parameter
   equal-size
   equal-right
   equal-bottom
   equal-horizontal-side
   equal-vertical-side
   surround-x
   surround-y
   surround
   to-right
   to-left
   below
   above
   in-row
   in-column
   center-x
   center-y
   center
   centered-column
   left-adjusted-column
   right-adjusted-column
   centered-row
   aspect-ratio
   square
   ))

(declaim (declaration :returns))
