;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================
;;; This file defines a generic interface to <bricks>,
;;; which are objects that are associated with rectangular
;;; regions in the plane.

(defgeneric left (brick)
  #-sbcl (declare (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should return the X
coordinate of leftmost pixel in the rectangular region."))

(defmethod left (brick)
  (error "need to define a #'brick:left method for ~a" brick))

(defgeneric (setf brick:left) (coord brick)
  #-sbcl (declare (type az:Positive-Fixnum coord)
		  (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should set the X
coordinate of leftmost pixel in the rectangular region."))

(defmethod (setf brick:left) (coord brick)
  (error "need to define a #'(setf brick:left) method for ~a and ~a"
	 brick coord))

;;;------------------------------------------------------------

(defgeneric width (brick)
  #-sbcl (declare (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should return the width
of the rectangular region."))

(defmethod width (brick)
  (error "need to define a #'brick:width method for ~a" brick))

(defgeneric fixed-width? (brick)
  #-sbcl (declare (type Brick brick)
		  (:returns (type Boolean)))
  (:documentation
   "Using a fixed width brick is more efficient than adding
the equivalent constraint. If (fixed-width? brick) is T, then calling (setf
width) should generate an error."))

(defgeneric (setf width) (coord brick)
  #-sbcl (declare (type az:Positive-Fixnum coord)
		  (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should set the width of the
rectangular region. If (fixed-width? brick) is T, then calling (setf
width) should generate an error."))

(defmethod (setf width) (coord brick)
  (error "need to define a #'(setf brick:width) method for ~a and ~a"
	 brick coord))

;;;------------------------------------------------------------

(defgeneric top (brick)
  #-sbcl (declare (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should return the Y
coordinate of topmost pixel in the rectangular region."))

(defmethod top (brick)
  (error "need to define a #'brick:top method for ~a" brick))

(defgeneric (setf top) (coord brick)
  #-sbcl (declare (type az:Positive-Fixnum coord)
		  (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should set the Y
coordinate of topmost pixel in the rectangular region."))

(defmethod (setf top) (coord brick)
  (error "need to define a #'(setf brick:top) method for ~a and ~a"
	 brick coord))

;;;------------------------------------------------------------

(defgeneric height (brick)
  #-sbcl (declare (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should return the height
of the rectangular region."))

(defmethod height (brick)
  (error "need to define a #'brick:height method for ~a" brick))

(defgeneric fixed-height? (brick)
  #-sbcl (declare (type Brick brick)
		  (:returns (type Boolean)))
  (:documentation
   "Using a fixed height brick is more efficient than adding
the equivalent constraint. If (fixed-height? brick) is T, then calling (setf
height) should generate an error."))

(defgeneric (setf height) (coord brick)
  #-sbcl (declare (type az:Positive-Fixnum coord)
		  (type Brick brick)
		  (:returns (type az:Positive-Fixnum)))
  (:documentation
   "Methods for this function should set the height of the
rectangular region. If (fixed-height? brick) is T, then calling (setf
height) should generate an error."))

(defmethod (setf height) (coord brick)
  (error "need to define a #'(setf brick:height) method for ~a and ~a"
	 brick coord))

;;;-----------------------------------------------------------

#||
(defgeneric implements-brick-protocol? (object))

(defmethod implements-brick-protocol? ((object T))
  
  "Does this object have the necessary methods defined
for it to be treated as a brick?"

  (and (compute-applicable-methods #'left (list object))
       (compute-applicable-methods #'(setf left) (list 0 object))
       (compute-applicable-methods #'width (list object))
       ;;(compute-applicable-methods #'fixed-width? (list object))
       (compute-applicable-methods #'(setf width) (list 0 object))
       (compute-applicable-methods #'top (list object))
       (compute-applicable-methods #'(setf top) (list 0 object))
       (compute-applicable-methods #'height (list object))
       ;;(compute-applicable-methods #'fixed-height? (list object))
       (compute-applicable-methods #'(setf height) (list 0 object))))
||#

(deftype Brick ()

  "Bricks are objects that correspond to (axis-parallel) rectangular
regions. Any object that has the necessary methods, for the generic
functions --- left, (setf left), width, (setf width) top, (setf top),
height, and (setf top) --- can be treated as a Brick."
  ;;'(satisfies implements-brick-protocol?)
  T)

;;;============================================================
#||
(defun brick-list? (l)
  (and (listp l)
       (every #'implements-brick-protocol? l)))
||#
(deftype Brick-List ()
  "Just a list of Bricks."
  ;;'(satisfies brick-list?)
  'List)

