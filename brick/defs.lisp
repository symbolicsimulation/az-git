;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Bricklayer -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Bricklayer)

;;;============================================================

(defconstant -eps- (sqrt double-float-epsilon))

;;;============================================================

(defmacro dabs (x)
  "Trying to get an <abs> that compiles inline and doesn't cons."
  `(the (Double-Float 0.0d0 *)
     (if (minusp (the Double-Float ,x)) 
	 (- (the Double-Float ,x))
       (the Double-Float ,x))))

;;;============================================================

(defclass Brick-Object (Standard-Object) ()
  (:documentation
   "A root for all classes in the Brick package."))