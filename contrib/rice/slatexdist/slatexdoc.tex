% SLaTeX Version 0.9, Feb 28, 1991
% Written by Dorai Sitaram
% Rice University, Houston 

% ***** slatexdoc.tex *****

\documentstyle[11pt,slatex,drafthead]{article}

% \begin{abstract.sty}

\setlength{\oddsidemargin}{.25in}
\setlength{\evensidemargin}{.25in}
\setlength{\textheight}{8.5 in}
\setlength{\textwidth}{6 in}
\setlength{\topmargin}{-.25 in}
\setlength{\baselineskip}{19pt}
\setlength{\marginparwidth}{0.9in}
\setlength{\marginparsep}{3pt}

\long\def\abs#1{#1}
\long\def\main#1{\relax}

% \end{abstract.sty}

% \begin{basic.sty}

\def\adjustedspace{\kern.45em}
\def\adjustedtab{\kern3.6em}
\def\adjustedpar{\par\mbox{}}
\newdimen\adjustedparindent
\adjustedparindent=0em
\newskip\adjustedparskip
\adjustedparskip=0pt plus 1pt

\def\makespace#1{\catcode`#110\relax}
\def\makeother#1{\catcode`#112\relax}
\def\makeactive#1{\catcode`#113\relax}

% \obeyspaces except in mmode

\def\obeyspaces{\makeactive\ \makeactive\^^I%
\everymath{\makespace\ \makespace\^^I}}
{\obeyspaces\global\let =\adjustedspace\global\let^^I=\adjustedtab}

% \obeylines 

{\makeactive\^^M
  \gdef\obeylines{%
    \parindent=\adjustedparindent%
    \parskip=\adjustedparskip%
    \makeactive\^^M%
    \let^^M=\adjustedpar%
    \mbox{}}}

% \obeyallspaces (all white space except tabs) 

\def\removebox{\setbox0\lastbox}
		
{\makeactive\^^M
  \gdef\obeyallspaces{%
    \parindent=\adjustedparindent%
    \parskip=\adjustedparskip%
    \makeactive\^^M%
    \let^^M=\adjustedpar%
    \aftergroup\removebox%
    \mbox{}%
    \obeyspaces}}

% readable _italics_ that takes care of ital corr

\def\ital{\begingroup\it}
\def\endital{\endgroup\italcorr}

\def\italcorr{\begingroup\futurelet\next\insertitalcorr}
\def\insertitalcorr{\ifx\next,\else\ifx\next.\else\/\fi\fi\endgroup}
\def\flipital{\ital\let\flipital\endital}
\makeactive\_
\def_{\ifmmode\sb\else\expandafter\flipital\fi}
    
% verbatim

\def\deactivate{%
\makeother\\%
\makeother\{%
\makeother\}%
\makeother\$%
\makeother\&%
\makeother\#%
\makeother\%%
\makeother\~%
\makeother\^%
\makeother\_}

{\makeactive\` \gdef`{\relax\lq}}

\newif\ifverbatimon \verbatimonfalse

\let\verbatimfont\tt

\def\verbatimtoggle{\ifverbatimon\endgroup\else%
\begingroup\verbatimontrue\deactivate\makeactive\`%
\obeyallspaces\verbatimfont\fi}

% uncomment following line to get "..." treated as verbatim

%\makeactive\" \let"\verbatimtoggle

\def\verbinput#1{{\makeother\"% in case " is \verbatimtoggle
\expandafter\verbatimtoggle\input{#1}\verbatimtoggle}}

% \end{basic.sty}

\def\separate{\vskip 3ex \hrule \vskip 3ex}
\adjustedparindent=2em

\makeactive\" \let"\verbatimtoggle

\leftcodeskip=2em

\def\tex/{{\TeX}}
\def\latex/{{\LaTeX}}
\def\slatex/{{S\LaTeX}}
%\def\slatex/{{\tt slatex}}

\title{How to use \slatex/}

\author{Dorai Sitaram\\
Department of Computer Science\\
Rice University\\
Houston, TX 77251--1892}

\date{Draft: Feb. 28, 1991}

\begin{document}

\maketitle

\section{Introduction}

The \slatex/ program allows you to write program code (or code
fragments) ``as is'' in your \tex/~\cite{texbook} or
\latex/~\cite{latexmanual} files.  The formatting of the code
includes assigning appropriate fonts to the various tokens in the
code, and retaining the proper indentation when going to the usual
non-monospace fonts provided by \tex/.

Existing styles for typesetting code fragments use a "verbatim"
environment that uses a monospace "typewriter" font.  This
monospace ensures that the indentation is not affected.  However, the
resulting output fails to distinguish between the various tokens used
in the code, e.g., {\bf boldface} for keywords like \scheme'define'
and \scheme'lambda', {\sf sans-serif} for constants like \scheme'#t'
and \scheme'42', and _italics_ for variables such as \scheme'x' and
\scheme'y' in \scheme|(lambda (x y) (cons x (cons y '())))|.

The program \slatex/ provides a convenient way of capturing the
indentation information as well as assigning distinguishing fonts to
code tokens without requiring the user to do worry about fonting and
spacing.  It uses temporary files to store its \tex/ set version of the
user's code and then calls \latex/ on the user's \latex/ files as well
as these temporaries.

\section{A quick illustration of using \slatex/}

For a quick tutorial on the basic \slatex/ control sequences,
their use, and the running of the \slatex/ program, consider the
following \latex/ (_and_ \slatex/) file, "quick.tex":

\separate

\undefschemetoken{scheme}
\undefschemedisplaytoken{schemedisplay}
\begin{verbatim}
% quick.tex
\documentstyle[11pt,slatex]{article}
\begin{document}

In Scheme, the expression \scheme+(set! x 42)+ returns an
unspecified value, rather than \scheme|42|.  However, one
could get a \scheme'set!' of the latter style by:

\schemedisplay|
(extend-syntax (my-set!)
  [(my-set! x a)
   (begin (set! x a)
          x)])
|

\end{document}
\end{verbatim}
\defschemetoken{scheme}
\defschemedisplaytoken{schemedisplay}

\separate

Thus, in-text code is surrounded by the \slatex/ control sequence
"\""scheme" and a pair of arbitrary but identical non-alphabetic
characters.  The \slatex/ control sequence for displayed code is
"\""schemedisplay" and is called the same way.  One of the
"\""documentstyle" options is "slatex".  (You could also have written
``"\""input slatex.sty"'' in your file, where "slatex.sty" contains
these and other \slatex/-specific control sequences.)

The file is now \slatex/'d by running the Un*x command "slatex" on it:
\begin{quote}
      "slatex quick"      (_or_ "slatex quick.tex")
\end{quote}

This calls a Scheme program that \tex/sets the code fragments
identified by the commands "\""scheme" and "\""schemedisplay", and
then calls \latex/ on the result.  The final result is the "quick.dvi"
file, which when viewed or printed, looks like:

\separate

\noindent 
In Scheme, the expression \scheme+(set! x 42)+ returns an
unspecified value, rather than \scheme|42|.  However, one
could get a \scheme'set!' of the latter style by:

\schemedisplay|
(extend-syntax (my-set!)
  [(my-set! x a)
   (begin (set! x a)
          x)])
|

\separate

\section{\slatex/'s control sequences}

You've already seen the \slatex/ control sequences "\""scheme" and
"\""schemedisplay".  These suffice for quite a few instances of
handling code in \LaTeX.  However, you will occasionally require more
control on the typesetting process, and the following
complete\footnote{At least that's what you're supposed to think...}
list of \slatex/ control sequences shows you the ropes.

%\bigskip
\begin{description}
\item
"\""scheme"

Typesets its argument, which is enclosed in arbitrary
but identical characters, as in-text code.   E.g.,

"\""scheme+(call/cc (lambda (x) x))+" produces \scheme+(call/cc
(lambda (x) x))+.

A call to "\""scheme" can't be nested in another "\""scheme" or
"\""schemedisplay".

%\pagebreak

\item
"\""schemedisplay"

Typesets its argument, which is usually several
lines of indented code, and which is enclosed in arbitrary but
identical characters, as displayed code.  E.g.,

"
\""schemedisplay|
(define compose          ;this is also known as $B$
  (lambda (f g)
    (lambda (x)
      (apply f (g x)))))
|
"

produces 
\schemedisplay|
(define compose        ;this is also known as $B$
  (lambda (f g)           
    (lambda (x)           
      (apply f (g x)))))    
|

Comments in Scheme are usually introduced by `";"'. 
The rest of the line after a `";"' is set as a line in \latex/ LR
mode. 

Separate blocks of code can either be introduced in different
"\""schemedisplay" calls or put in a single "\""schemedisplay"
and separated by a line with a `";"' in the first column.  This
`";"' is not typeset and anything following it on the line is set in
\latex/ LR paragraph mode.  I.e., consecutive lines with `";"' in the
first column are treated as input for a \latex/ paragraph, with words
possibly moved around from line to line to ensure justification.
E.g.,

"
\""schemedisplay*
(define even?               ; testing {\em{}even\/}ness
  (lambda (n)
    (if (= n 0) #t (not (odd? (- n 1))))))
;
; The procedures {\em even?\/} above
; and {\em odd?\/} below are mutually
; recursive.
;
(define odd?              ; testing {\em{}odd\/}ness
  (lambda (n)
    (if (= n 0) #f (not (even? (- n 1))))))
*
"

produces
\schemedisplay*
(define even?             ; testing {\em{}even\/}ness
  (lambda (n)
    (if (= n 0) #t (not (odd? (- n 1))))))
;
; The procedures {\em even?\/} above
; and {\em odd?\/} below are mutually
; recursive.
;
(define odd?              ; testing {\em{}odd\/}ness
  (lambda (n)
    (if (= n 0) #f (not (even? (- n 1))))))
*

Once again, calls to "\""schemedisplay" can't be nested in another
"\""scheme" or \linebreak "\""schemedisplay".

\item
"\""schemeinput"

This can be used to input Scheme files as typeset code.  They syntax
is the same as \latex/'s "\""input".  The Scheme file is specified by
its full name, or without its extension, if this is ".ss".  E.g.,

"
\""schemeinput evenodd.s % scheme
"

where "evenodd.ss" is the name of a Scheme file containing the code
for \scheme'even?' and \scheme'odd?' above produces the same effect as
the "\""schemedisplay" version.

\item
\begin{tabular}{l}
"\""setkeyword"\\
"\""setconstant"\\
"\""setvariable"
\end{tabular}
 
\setconstant{infinity -infinity}
\slatex/ has a database containing
information about which code tokens are to be treated as {\bf
keywords}, which as {\sf constants}, and which as _variables_.
However, there will always be instances where the user wants to add
his or her own tokens to these categories, or perhaps even modify the
categories as prescribed by \slatex/.  The control sequences that
enable the user to do these are "\""setkeyword",
"\""setconstant", and "\""setvariable".  Their arguments are
entered as a (space-separated) list enclosed in braces (\{\}), and
\slatex/ learns that these are henceforth to be typeset in the
appropriate font.  E.g., "\""setconstant{infinity -infinity}" tells
\slatex/ that \scheme|infinity| and \scheme|-infinity| are to be
typeset as constants.  (The user need not specify those new keywords
that are introduced by Scheme's \scheme|extend-syntax|: \slatex/
automatically recognizes them.)

\item
\begin{tabular}{l}
"\""abovecodeskip"\\
"\""belowcodeskip"\\
"\""leftcodeskip"\\
"\""rightcodeskip"
\end{tabular}

These are the parameters used by "\""schemedisplay" for
positioning the displayed code.  The default values are

"
\abovecodeskip=\medskipamount
\belowcodeskip=\medskipamount
\leftcodeskip=0pt
\rightcodeskip=0pt
"

This produces a flushleft display.  The defaults can be
changed to get new display styles.  E.g.,  the assignment

"
\leftcodeskip=5em
"

\noindent shifts the display from the left by a constant 5 ems.

On the other hand, the assignments

"
\leftcodeskip=0pt plus 1fil
\rightcodeskip=0pt plus 1fil
"

\noindent produce a centered display style.

\item
\begin{tabular}{l}
"\keywordfont"\\
"\constantfont"\\
"\variablefont"
\end{tabular}

These decide the typefaces used for keywords, constants, and
variables.  The default definitions are:

"
\def\keywordfont#1{{\bf#1}}
\def\constantfont#1{{\sf#1}}
\def\variablefont#1{{\it#1\/}}
"

This is close to the Little Lisper~\cite{ll} style.  Redefine these control
sequences for font changes.

\item
\begin{tabular}{l}
"\""defschemetoken"\\
"\""defschemedisplaytoken"\\
"\""defschemeinputtoken"
\end{tabular}

These define the tokens used by \slatex/ to trigger typesetting of
in-text code, display code, and Scheme files.  The default tokens are,
as already described, "\""scheme", "\""schemedisplay", and
"\""schemeinput" respectively.  If you want shorter or more 
mne\-monic tokens, the "\""defscheme..." control sequences prove useful.
E.g., if you want "\code" to be your new control sequence for
in-text code, use "\""defschemetoken{code}".  All instances
of "\code|...|" after this definition produce in-text code, unless
overridden by an "\undefscheme..." command.

\item
\begin{tabular}{l}
"\""undefschemetoken"\\
"\""undefschemedisplaytoken"\\
"\""undefschemeinputtoken"
\end{tabular}

The tokens for typesetting code can only be used as calls.  They can't
occur in the bodies of macro definitions, nor can their names be used
for defining other control sequences.  (They can't even occur in
verbatim environments, since all occurrences of them trigger
the codesetting half of \slatex/ that occurs before \latex/ is
called.)

Since this is unduly restrictive (especially when you're writing a
``How to \ldots''\ paper like this!), the "\undefscheme..."
commands provide a method of removing the \slatex/-specificity of
these names.  A typical example is when you use the name
"\""scheme" in a
"verbatim" environment.  E.g.,

"
\""undefschemetoken{scheme}
\begin{verbatim}
slatex provides the ""\""scheme control sequence
to typeset in-text Scheme code.
\end{verbatim}
""\""defschemetoken{scheme}
"

\noindent produces the required

\undefschemetoken{scheme}
\begin{verbatim}
     slatex provides the \scheme control sequence
     to typeset in-text Scheme code.
\end{verbatim}
\defschemetoken{scheme}

\end{description}

\section{Setting up a file that resets \slatex/'s defaults}

A sample style modification file for \slatex/ would include
redefinition of the names of the codesetting control sequences,
adjustment of the display parameters, modification of the font
assignments for \{keywords, constants, variables\}, and addition of
new \{keywords, constants, variables\} to \slatex/'s database.  

Let's assume you want 
\begin{enumerate}
\item a centered display style with no vertical skips;

\item the names "\code", "\cdisp", "\sinput" instead of
"\""scheme", "\""schemedisplay", and \linebreak "\""schemeinput"; 

\item the keywords to come out it "typewriter", the constants in
roman, and the variables in {\sl slant}; 

\item ``"und"'' and ``"oder"'' as keywords, ``"true"'' and
``"false"'' as constants, and ``"define"'' as a variable
(overriding default as keyword).
\end{enumerate}

This could be set up as

"
\abovecodeskip=0pt
\belowcodeskip=0pt
\leftcodeskip=0pt plus 1fil
\rightcodeskip=0pt plus 1fil

""\""undefschemetoken{scheme}
""\""undefschemedisplaytoken{schemedisplay}
""\""undefschemeinputtoken{schemeinput}

""\""defschemetoken{code}
""\""defschemedisplaytoken{cdisp}
""\""defschemeinputtoken{sinput}

\def\keywordfont#1{{\tt#1}}
\def\constantfont#1{{\rm#1}}
\def\variablefont#1{{\sl#1\/}}

""\""setkeyword{und oder}
""\""setconstant{true false}
""\""setvariable{define}
"

This file can then be "\""input"ed in the preamble of your \latex/
document. 

\appendix

\section{List of \slatex/ control sequences}

"
\abovecodeskip 
\belowcodeskip 
\constantfont 
""\""defschemedisplaytoken
""\""defschemeinputtoken
""\""defschemetoken
\keywordfont
\leftcodeskip
\rightcodeskip
""\""scheme
""\""schemedisplay
""\""schemeinput
""\""setconstant
""\""setkeyword
""\""setvariable
""\""undefschemedisplaytoken
""\""undefschemeinputtoken
""\""undefschemetoken
\variablefont
"

\begin{thebibliography}{9}
\bibitem{ll}
D.P. Friedman and M. Felleisen.  _The Little Lisper._  Science
Research Associates, 1989.  MIT Press, 1987.

\bibitem{texbook}
D.E. Knuth.  _The \tex/book._  Addison-Wesley, 1984.

\bibitem{latexmanual}
L. Lamport. _\latex/ User's Guide and Reference Manual._
Addison-Wesley, 1986.
\end{thebibliography}

\end{document}
