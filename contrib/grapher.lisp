;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: PCL -*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package 'pcl)

(export '(class-name
	  graph-subclasses
	  graph-superclasses
	  graph-class-subs-and-supers
	  graph-class-relatives
	  graph-class-neighbors))

;;;=======================================================

(defparameter *class-default-character-style* '(:swiss :roman :very-small))

(defun present-class (class stream
		      &key (style *class-default-character-style*))
  (scl:with-character-style (style)
    (let ((*print-case* :downcase)
	  (*standard-output* stream))
      (declare (special *print-case* *standard-output*))
      (scl:present (class-name class)))))

;;;=======================================================

(defmethod graph-subclasses ((root-name Symbol)
			     &key
			     (stream *standard-output*)
			     (include-root? t)
			     (style *class-default-character-style*)
			     (prune ())
			     (keep (all-subclasses
				     (find-class root-name)))
			     (orientation :horizontal))
  (graph-subclasses (find-class root-name)
		    :stream stream
		    :include-root? include-root?
		    :stream stream
		    :prune prune
		    :keep keep
		    :style style
		    :orientation orientation))
#+symbolics  
(defmethod graph-subclasses ((root-class Class)
			     &key
			     (stream *standard-output*)
			     (include-root? t)
			     (style *class-default-character-style*)
			     (prune ())
			     (keep (all-subclasses root-class))
			     (orientation :horizontal))
  (let* ((prune-classes
	  (map 'list #'(lambda (c)
			 (etypecase c
			   (Symbol (find-class c))
			   (Class c)))
	       prune))
	 (keep-classes
	  (map 'list #'(lambda (c)
			 (etypecase c
			   (Symbol (find-class c))
			   (Class c)))
	       keep))
	(classes (set-difference keep-classes prune-classes)))
    
    (fresh-line)
    (terpri)
    (scl:format-graph-from-root
      (if include-root?
	  root-class
	  (set-difference (direct-subclasses root-class)
			  prune-classes))
      #'(lambda (thing stream) (present-class thing stream :style style))
      #'(lambda (cl) (nintersection (direct-subclasses cl) classes))
      :stream stream
      :root-is-sequence (not include-root?)
      :dont-draw-duplicates t
      :orientation orientation
      :border '(:shape :rectangle)
      :row-spacing 32
      :within-row-spacing 8
      :column-spacing 16
      :within-column-spacing 8
      :default-drawing-mode :line
      :test #'eq)))

;;;-------------------------------------------------------

(defmethod graph-superclasses ((root-name Symbol)
			       &key
			       (stream *standard-output*)
			       (include-root? t)
			       (orientation :horizontal))
  (graph-superclasses (find-class root-name)
		      :stream stream
		      :include-root? include-root?
		      :orientation orientation))
#+symbolics
(defmethod graph-superclasses ((root-class Class)
			       &key
			       (stream *standard-output*)
			       (include-root? t)
			       (orientation :horizontal))
  (fresh-line)
  (scl:format-graph-from-root
    (if include-root?
	root-class
	(direct-superclasses root-class))
    #'(lambda (thing stream) (present-class thing stream))
    #'direct-superclasses
    :stream stream
    :root-is-sequence (not include-root?)
    :dont-draw-duplicates t
    :orientation orientation
    :border '(:shape :rectangle)
    :row-spacing 16
    :within-row-spacing 8
    :column-spacing 16 
    :within-column-spacing 4
    :default-drawing-mode :line ;;:arrow
    :test #'eq))

;;;-------------------------------------------------------

(defmethod graph-class-subs-and-supers ((center-name Symbol)
					&key
					(depth 2)
					(stream *standard-output*)
					(orientation :horizontal))
  (graph-class-subs-and-supers
    (find-class center-name) :depth depth :stream stream
    :orientation orientation))

(defmethod graph-class-subs-and-supers ((center-class Class)
					&key
					(depth 2)
					(stream *standard-output*)
					(orientation :horizontal))
  (do-graph-class-subs-and-supers center-class depth stream orientation))

;; temp patch over bug in Walker

(defun do-graph-class-subs-and-supers (center-class depth stream orientation)  
  (fresh-line)
  (scl:formatting-graph (stream
			  :orientation orientation
			  :row-spacing 16
			  :within-row-spacing 8
			  :column-spacing 16 
			  :within-column-spacing 4
			  :default-drawing-mode :line)
    (labels ((connect-to-children (class node)
	       (loop for child in (class-direct-subclasses class)
		     for child-node = (dw:find-graph-node stream child)
		     unless (null child-node)
		       do (dw::connect-graph-nodes
			    stream node `(:left ,child-node))))
	     (format-nodes-for-classes (class-list)
	       (loop for cl in class-list
		     for node =
			 (scl:with-character-style
			   (*class-default-character-style*)
			   (scl:formatting-graph-node (stream :id cl)
			     (scl:surrounding-output-with-border (stream)
			       (present-class cl stream))))
		     do (connect-to-children cl node))))
      
      ;; the center
      (let ((center-node 
	      (scl:formatting-graph-node (stream :id center-class)
		(scl:surrounding-output-with-border (stream :shape :rectangle)
		  (scl:with-character-style (*class-default-character-style*)
		    (present-class center-class stream))))))
	;; the children
	(loop for g from depth downto 1
	      for classes = (nth-generation-subclasses center-class g)
	      do (format-nodes-for-classes classes))
	(connect-to-children center-class center-node)
	;; the parents
	(loop for g from 1 to depth
	      for classes = (nth-generation-superclasses center-class g)
	      do (format-nodes-for-classes classes))))))

;;;-------------------------------------------------------

(defmethod graph-class-relatives ((name Symbol)
				  &key
				  (depth 2)
				  (stream *standard-output*)
				  (orientation :horizontal))
  (graph-class-relatives (find-class name)
			 :depth depth
			 :stream stream
			 :orientation orientation))

(defmethod graph-class-relatives ((center-class Class)
				  &key
				  (depth 2)
				  (stream *standard-output*)
				  (orientation :horizontal))
  (do-graph-class-relatives center-class depth stream orientation))

(defun do-graph-class-relatives (center-class depth stream orientation)
  (fresh-line)
  (scl:formatting-graph (stream
			  :orientation orientation
			  :row-spacing 16
			  :within-row-spacing 8
			  :column-spacing 16 
			  :within-column-spacing 4
			  :default-drawing-mode :line)
    (labels
      ((connect-to-children (class node)
	 (loop for child in (class-direct-subclasses class)
	       for child-node = (dw:find-graph-node stream child)
	       unless (null child-node)
		 do (dw::connect-graph-nodes
		      stream node `(:left ,child-node))))
       (format-nodes-for-classes (class-list)
	 (loop for cl in class-list
	       for node = (dw:find-graph-node stream cl)
	       collect
		 (scl:formatting-graph-node (stream :id cl)
		   (scl:surrounding-output-with-border
		     (stream :shape :rectangle)
		     (scl:with-character-style
		       (*class-default-character-style* )
		       (present-class cl stream)))))))
      
      (let* ((relatives (class-relatives center-class depth))
	     (center-node 
	       (scl:formatting-graph-node (stream :id center-class)
		 (scl:surrounding-output-with-border
		   (stream :shape :rectangle)
		   (scl:with-character-style
		     (*class-default-character-style*)
		     (present-class center-class stream)))))
	     (relative-nodes (format-nodes-for-classes relatives)))
	(connect-to-children center-class center-node)
	(loop for cl in relatives
	      for node in relative-nodes 
	      do (connect-to-children cl node))))))

;;;-------------------------------------------------------

(defmethod graph-class-neighbors ((name Symbol)
				  &key
				  (depth 2)
				  (stream *standard-output*)
				  (orientation :horizontal))
  (graph-class-neighbors (find-class name)
			 :depth depth
			 :stream stream
			 :orientation orientation))

(defmethod graph-class-neighbors ((center-class Class)
				  &key
				  (depth 2)
				  (stream *standard-output*)
				  (orientation :horizontal))
(do-graph-class-neighbors center-class depth stream orientation))

(defun do-graph-class-neighbors (center-class depth stream orientation)

  (fresh-line)
  (scl:formatting-graph (stream
			  :orientation orientation
			  :row-spacing 16
			  :within-row-spacing 8
			  :column-spacing 16 
			  :within-column-spacing 4
			  :default-drawing-mode :line)
    (labels
      ((connect-to-children (class node)
	 (loop for child in (class-direct-subclasses class)
	       for child-node = (dw:find-graph-node stream child)
	       unless (null child-node)
		 do (dw::connect-graph-nodes
		      stream node `(:left ,child-node))))
       (format-nodes-for-classes (class-list)
	 (loop for cl in class-list
	       for node = (dw:find-graph-node stream cl)
	       collect
		 (scl:formatting-graph-node (stream :id cl)
		   (scl:surrounding-output-with-border
		     (stream :shape :rectangle)
		     (scl:with-character-style
		       (*class-default-character-style* )
		       (present-class cl stream)))))))
      
      (let* ((neighbors (class-neighbors center-class depth))
	     (center-node 
	       (scl:formatting-graph-node (stream :id center-class)
		 (scl:surrounding-output-with-border
		   (stream :shape :rectangle)
		   (scl:with-character-style
		     (*class-default-character-style*)
		     (present-class center-class stream)))))
	     (relative-nodes (format-nodes-for-classes neighbors)))
	(connect-to-children center-class center-node)
	(loop for cl in neighbors
	      for node in relative-nodes 
	      do (connect-to-children cl node))))))