;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

;;;-------------------------------------------------------------

(make-imagegram (band *feb0893* "vegsh") :width 1234 :height 604)

(defparameter *r*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "quag") 
     (band *feb0893* "plra") 
     :left 500 :top 4 :fractions? t))

(dotimes (h -rotor-width-)
  (print h) (force-output)
  (setf (hither *r*) h))


(multiple-value-setq (*fd0* *fd1*)
  (split-space *feb0893-band-space*
		(band *feb0893* "vegsh") 153))


(defparameter *r0*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "plra") 
     (band *feb0893* "quag") 
     :left 500 :top 4 :fractions? t :band-space *fd0*))

(make-imagegram (band *feb0893* "vegsh") :width 1234 :height 604
		:band-space *fd0*)


(defparameter *r1*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "plra") 
     (band *feb0893* "quag") 
     :left 500 :top 4 :fractions? t :band-space *fd1*))

(make-imagegram (band *feb0893* "vegsh") :width 1234 :height 604
		:band-space *fd1*)


(make-histogram (band *feb0893* "vegsh") :width 256 :height 128)
(make-histogram (band *feb0893* "dimona") :width 256 :height 128)
(make-histogram (band *feb0893* "plra") :width 256 :height 128)
(make-histogram (band *feb0893* "quag") :width 256 :height 128)

(progn
(make-logogram (band *feb0893* "vegsh") :width 256 :height 128)
(make-logogram (band *feb0893* "dimona") :width 256 :height 128)
(make-logogram (band *feb0893* "plra") :width 256 :height 128)
(make-logogram (band *feb0893* "quag") :width 256 :height 128))

#||



(defparameter wins
    (loop
	with d = 32
	for i from 0
	for l = (+ 128 (* i d))
	for names on (list "dimona" "plra" "quag" "vegsh")
	for f0 = (first names)
	for b0 = (band *feb0893* f0)
	collect
	  (make-histogram b0 :left l :top (+ l d) :width 256 :height 128)
	do (loop 
	       for j from i
	       for f1 in (rest names)
	       for b1 = (band *feb0893* f1)
	       do (make-repscatgram b0 b1))))

(defparameter *r0*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "plra") 
     (band *feb0893* "quag") 
     :left 500 :top 4))

;;;-------------------------------------------------------------

(make-repscatgram (band *fsub* "b4") (band *fsub* "b5"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b4"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b5"))

(defparameter *fsub345*
    (make-rotor
     (band *fsub* "b3") (band *fsub* "b4") (band *fsub* "b5")
     :left 500 :top 4))

(make-imagegram (band *fazendas* "b4"))


||#

