;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defun split-space (parent-space band cut)

  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space parent-space)
	   (type Band band)
	   (type az:Card8 cut)
	   (:returns (values (type Band-Space-Bipart space0)
			     (type Band-Space-Bipart space1))))

  (let* ((partitioning (etypecase parent-space
			 (Complete-Band-Space
			  (make-partitioning parent-space))
			 (Band-Space-Bipart
			  (partitioning parent-space))))
	 (part-1d (part-vector partitioning))
	 (band-1d (band-vector band))
	 (mask (mask parent-space))
	 (space0 (make-band-space-bipart parent-space partitioning 0))
	 (space1 (make-band-space-bipart parent-space partitioning 1))
	 (key0 (key space0))
	 (key1 (key space1)))
    (declare (type Partitioning partitioning)
	     (type az:Card16-Vector part-1d)
	     (type az:Card16 mask key0 key1)
	     (type az:Card8-Vector band-1d)
	     (type Band-Space-Bipart space0 space1))
    (dotimes (i (length part-1d))
      (when (= key0 (logand mask (aref part-1d i)))
	(setf (aref part-1d i) (if (< (aref band-1d i) cut) key0 key1))))
    (values space0 space1)))

