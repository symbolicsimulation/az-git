;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Histograms, etc.
;;;============================================================

(defun 1d-histogram (ch hist)
  
  "Note that this assumes that the output array is passed
already zeroed when it's passed in."
  
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Vector ch)
	   (type (az:Card28-Vector 256) hist))
  
  #+(and :excl :accelerators)  ;; call a C accelerator
  (image_histogram (length ch) ch hist)
  #-(and :excl :accelerators)
  (dotimes (i (length ch))
    (declare (type az:Card28 i))
    (let ((p (aref ch i)))
      (declare (type az:Card8 p))
      (setf (aref hist p) (+ (aref hist p) 1))))
  hist)

;;;------------------------------------------------------------

(defun masked-1d-histogram (key mask part ch hist)

  "This computes the histogram of (aref ch i)
for <i> such that (= key (logand mask (aref part i)))."

  (declare (optimize (safety 0) (speed 3))
	   (type az:Card16 key mask)
	   (type az:Card16-Vector part)
	   (type az:Card8-Vector ch)
	   (type (az:Card28-Vector 256) hist))

  (dotimes (i (length ch))
    (declare (type az:Card28 i))
    (when (= key (logand mask (aref part i)))
      (let ((ci (aref ch i)))
	(declare (type az:Card8 ci))
	(setf (aref hist ci) (+ (aref hist ci) 1)))))
  hist)
  
;;;------------------------------------------------------------

(defun 2d-histogram (ch0 ch1 hist)
  
  "Note that this assumes that the output array is passed
already zeroed when it's passed in."
  
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Vector ch0 ch1)
	   (type (az:Card28-Vector 65536) hist))
  
  #+(and :excl :accelerators)  ;; call a C accelerator
  (image_histogram_2d (length ch0) ch0 ch1 hist)
  #-(and :excl :accelerators)
  (dotimes (i (length ch0))
    (declare (type az:Card28 i))
    (incf (aref hist (ppref ch0 ch1 i)))))

;;;------------------------------------------------------------

(defun masked-2d-histogram (key mask part ch0 ch1 hist)

  "Note that this assumes that the output array is passed
already zeroed when it's passed in."
    
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card16 key mask)
	   (type az:Card16-Vector part)
	   (type az:Card8-Vector ch0 ch1)
	   (type (az:Card28-Vector 65536) hist))
  
  (dotimes (i (length ch0))
    (declare (type az:Card28 i))
    (when (= key (logand mask (aref part i)))
      (incf (aref hist (ppref ch0 ch1 i))))))
 
;;;------------------------------------------------------------

(defun 2d-histobary (ch0 ch1 hist)
  "Note that this assumes that the output array is passed
already zeroed when it's passed in."
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Vector ch0 ch1)
	   (type (az:Card28-Vector 65536) hist))
  #+(and :excl :accelerators)  ;; call a C accelerator
  (image_histobary (length ch0) ch0 ch1 hist)
  #-(and :excl :accelerators)
  (dotimes (i (length ch0))
    (declare (type az:Card28 i))
    (incf (aref hist (yxbary ch0 ch1 i)))))

;;;------------------------------------------------------------

(defun masked-2d-histobary (key mask part ch0 ch1 hist)

  (declare (optimize (safety 0) (speed 3))
	   (type az:Card16 key mask)
	   (type az:Card16-Vector part)
	   (type az:Card8-Vector ch0 ch1)
	   (type (az:Card28-Vector 65536) hist))
  
  (dotimes (i (length ch0))
    (declare (type az:Card28 i))
    (when (= key (logand mask (aref part i)))
      (incf (aref hist (yxbary ch0 ch1 i))))))
 
;;;------------------------------------------------------------
;;; Caches for histograms with space objects
;;;------------------------------------------------------------

(defgeneric cache-band-histogram (space band)
  
  (:documentation
   "Compute a histogram array of <band> and save it in the
histogram cache. This function can be called to ensure that
the histogram cache is up to date for a particular band.")
  
  (declare (type Band-Space space)
	   (type Band band))

  (:method
   ((space Complete-Band-Space) band)
   (let ((hist (az:make-card28-vector 256 :initial-element 0)))
     (1d-histogram (band-vector band) hist)
     (setf (az:lookup band (band-histograms space) nil) hist)
     hist))

  (:method
   ((space Band-Space-Bipart) band)
   (let ((hist (az:make-card28-vector 256 :initial-element 0)))
     (masked-1d-histogram (key space) (mask space)
			  (part-vector (partitioning space))
			  (band-vector band) hist)
     (setf (az:lookup band (band-histograms space) nil) hist)
     hist))) 

(defun band-histogram (space band)

  "Returns an array containing the histogram of band,
getting it from a cache if possible. Does not guarantee
that the cache is up to date."
  
  (declare (type Band-Space space)
	   (type Band band))
  
  (let ((hist (az:lookup band (band-histograms space) nil)))
    (if (null hist) (cache-band-histogram space band) hist)))

;;;------------------------------------------------------------

(defgeneric cache-band-integram (space band)

  (:documentation
   "Compute a integram array of <band> and save it in the
integram cache. This function can be called to ensure that
the integram cache is up to date for a particular band.")
   
  (declare (type Band-Space space)
	   (type Band band))

  (:method
   ((space Band-Space) band)
   (let ((integral (az:integrate-card28-vector
		    (cache-band-histogram space band)
		    (az:make-card28-vector 256 :initial-element 0))))
     (setf (az:lookup band (band-integrams space) nil) integral)
     integral)))

(defun band-integram (space band)
  
  "Returns an array containing the integram of band,
getting it from a cache if possible. Does not guarantee
that the cache is up to date."
  
  (declare (type Band-Space space)
	   (type Band band))
  
  (let ((integral (az:lookup band (band-integrams space) nil)))
    (when (null integral)
      (setf integral (az:integrate-card28-vector
		      (band-histogram space band)
		      (az:make-card28-vector 256 :initial-element 0)))
      (setf (az:lookup band (band-integrams space) nil) integral))
    integral))

;;;------------------------------------------------------------

(defgeneric cache-2band-histogram (space band0 band1)
  
  (:documentation
   "Compute a histogram array of <band0> X <band1> and save it in the
histogram cache. This function can be called to ensure that
the histogram cache is up to date for a particular pair of bands.")
  
  (declare (type Band-Space space)
	   (type Band band0 band1))

  (:method
   ((space Complete-Band-Space) band0 band1)
   (declare (type Complete-Band-Space space)
	    (type Band band0 band1))
   (az:large-allocation
    (let ((hist (az:make-card28-matrix 256 256 :initial-element 0))
	  (inner (az:lookup band0 (2band-histograms space)
			  (make-hash-table :test #'eq))))
      (declare (type (az:Card28-Matrix 256 256) hist)
	       (type Hash-Table inner))
      (2d-histogram (band-vector band0) (band-vector band1)
		    (az:array-data hist))
      (setf (az:lookup band1 inner) hist)
      (setf (az:lookup band0 (2band-histograms space)) inner)
      hist)))

  (:method
   ((space Band-Space-Bipart) band0 band1)
   (declare (type Complete-Band-Space space)
	    (type Band band0 band1))
   (az:large-allocation
   (let ((hist (az:make-card28-matrix 256 256 :initial-element 0))
	 (inner (az:lookup band0 (2band-histograms space)
			 (make-hash-table :test #'eq))))
     (declare (type (az:Card28-Matrix 256 256) hist)
	      (type Hash-Table inner))
     (masked-2d-histogram
      (key space) (mask space)
      (part-vector (partitioning space))
      (band-vector band0) (band-vector band1)
      (az:array-data hist))
     (setf (az:lookup band1 inner) hist)
     (setf (az:lookup band0 (2band-histograms space)) inner)
     hist)))) 

(defun 2band-histogram (space band0 band1)

  "Returns an array containing the histogram of band0 X band1, getting
it from a cache if possible. Does not guarantee that the cache is up
to date."
  (declare (type Band-Space space)
	   (type Band band0 band1))
  (let* ((inner (az:lookup band0 (2band-histograms space) nil))
	 (hist (unless (null inner) (az:lookup band1 inner nil))))
    (declare (type (or Null Hash-Table) inner)
	     (type (or Null (az:Card28-Matrix 256 256)) hist))
    (if (null hist) (cache-2band-histogram space band0 band1) hist)))

;;;------------------------------------------------------------

(defgeneric cache-2band-integram (space band0 band1)

  (:documentation
   "Compute a integram array of <band> and save it in the
integram cache. This function can be called to ensure that
the integram cache is up to date for a particular band.")
   
  (declare (type Band-Space space)
	   (type Band band0 band1))

  (:method
   ((space Band-Space) band0 band1)
   (declare (type Complete-Band-Space space)
	    (type Band band0 band1))
   (az:large-allocation
    (let ((integral (az:make-card28-matrix 256 256 :initial-element 0))
	  (inner (az:lookup band0 (2band-integrams space)
			  (make-hash-table :test #'eq))))
      (declare (type (az:Card28-Matrix 256 256) integral)
	       (type Hash-Table inner))
      (az:integrate-card28-vector
       (az:array-data (cache-2band-histogram space band0 band1))
       (az:array-data integral))
      (setf (az:lookup band1 inner) integral)
      (setf (az:lookup band0 (2band-integrams space)) inner)
      integral))))

(defun 2band-integram (space band0 band1)
  
  "Returns an array containing the integram of band0band11,
getting it from a cache if possible. Does not guarantee
that the cache is up to date."
  
  (declare (type Band-Space space)
	   (type Band band0 band1))
  (let* ((inner (az:lookup band0 (2band-integrams space) nil))
	 (integral (unless (null inner) (az:lookup band1 inner nil))))
    (declare (type (or Null Hash-Table) inner)
	     (type (or Null (az:Card28-Matrix 256 256)) integral))
    (when (null inner) (setf inner (make-hash-table :test #'eq)))
    (az:large-allocation
     (when (null integral)
       (setf integral (az:make-card28-matrix 256 256 :initial-element 0))
       (az:integrate-card28-vector
	(az:array-data (2band-histogram space band0 band1))
	(az:array-data integral))
       (setf (az:lookup band1 inner) integral)
       (setf (az:lookup band0 (2band-integrams space)) inner)))
    integral))

;;;------------------------------------------------------------

(defgeneric cache-2band-histobary (space band0 band1)
  
  (:documentation
   "Compute a histogram array of the barycentric coordinates
of <band0> X <band1> and save it in the
histogram cache. This function can be called to ensure that
the histobary cache is up to date for a particular pair of bands.")
  
  (declare (type Band-Space space)
	   (type Fraction band0 band1))

  (:method
   ((space Complete-Band-Space) band0 band1)
   (declare (type Complete-Band-Space space)
	    (type Fraction band0 band1))
   (az:large-allocation
    (let ((hist (az:make-card28-matrix 256 256 :initial-element 0))
	  (inner (az:lookup band0 (2band-histobarys space)
			  (make-hash-table :test #'eq))))
      (declare (type (az:Card28-Matrix 256 256) hist)
	       (type Hash-Table inner))
      (2d-histobary (band-vector band0) (band-vector band1)
		    (az:array-data hist))
      (setf (az:lookup band1 inner) hist)
      (setf (az:lookup band0 (2band-histobarys space)) inner)
      hist)))

  (:method
   ((space Band-Space-Bipart) band0 band1)
   (declare (type Band-Space-Bipart space)
	    (type Fraction band0 band1))
   (az:large-allocation
    (let ((hist (az:make-card28-matrix 256 256 :initial-element 0))
	  (inner (az:lookup band0 (2band-histobarys space)
			  (make-hash-table :test #'eq))))
      (declare (type (az:Card28-Matrix 256 256) hist)
	       (type Hash-Table inner))
      (masked-2d-histobary
       (key space) (mask space)
       (part-vector (partitioning space))
       (band-vector band0) (band-vector band1)
       (az:array-data hist))
      (setf (az:lookup band1 inner) hist)
      (setf (az:lookup band0 (2band-histobarys space)) inner)
      hist)))) 

(defun 2band-histobary (space band0 band1)

  "Returns an array containing the histobary of band0 X band1, getting
it from a cache if possible. Does not guarantee that the cache is up
to date."
  (declare (type Band-Space space)
	   (type Fraction band0 band1))
  (let* ((inner (az:lookup band0 (2band-histobarys space) nil))
	 (hist (unless (null inner) (az:lookup band1 inner nil))))
    (declare (type (or Null Hash-Table) inner)
	     (type (or Null (az:Card28-Matrix 65536)) hist))
    (if (null hist) (cache-2band-histobary space band0 band1) hist)))

;;;------------------------------------------------------------

(defgeneric cache-2band-intebary (space band0 band1)

  (:documentation
   "Compute a intebary array of <band> and save it in the
intebary cache. This function can be called to ensure that
the intebary cache is up to date for a particular band.")
   
  (declare (type Band-Space space)
	   (type Fraction band0 band1))

  (:method
   ((space Band-Space) band0 band1)
   (declare (type Complete-Band-Space space)
	    (type Fraction band0 band1))
   (az:large-allocation
    (let ((integral (az:make-card28-matrix 256 256 :initial-element 0))
	  (inner (az:lookup band0 (2band-intebarys space)
			  (make-hash-table :test #'eq))))
      (declare (type (az:Card28-Matrix 256 256) integral)
	       (type Hash-Table inner))
      (az:integrate-card28-vector
       (az:array-data (cache-2band-histobary space band0 band1))
       (az:array-data integral))
      (setf (az:lookup band1 inner) integral)
      (setf (az:lookup band0 (2band-intebarys space)) inner)
      integral))))

(defun 2band-intebary (space band0 band1)
  
  "Returns an array containing the intebary of band0band11,
getting it from a cache if possible. Does not guarantee
that the cache is up to date."
  
  (declare (type Band-Space space)
	   (type Fraction band0 band1))
  (az:large-allocation
   (let* ((inner (az:lookup band0 (2band-intebarys space) nil))
	  (integral (unless (null inner) (az:lookup band1 inner nil))))
     (declare (type (or Null Hash-Table) inner)
	      (type (or Null (az:Card28-Matrix 256 256)) integral))
     (when (null inner) (setf inner (make-hash-table :test #'eq)))
     (when (null integral)
       (setf integral (az:make-card28-matrix 256 256 :initial-element 0))
       (az:integrate-card28-vector
	(az:array-data (2band-histobary space band0 band1))
	(az:array-data integral))
       (setf (az:lookup band1 inner) integral)
       (setf (az:lookup band0 (2band-intebarys space)) inner))
     integral)))

;;;============================================================

(defun count-colors (sorted-pins paint start end counts)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card28-Vector sorted-pins)
	   (type az:Card8-Vector paint)
	   (type az:Card28 start end)
	   (type (az:Card28-Vector 8) counts))
  (az:zero-card28-array counts)
  (do ((i start (+ i 1)))
      ((>= i end))
    (declare (type az:Card28 i))
    (incf (aref counts (ash (aref paint (aref sorted-pins i))
			    (- xlt:-paint-v-bits-)))))
  (values counts))
 
