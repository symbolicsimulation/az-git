;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(In-package :Mu) 

;;;============================================================

(defun shift-endmembers (ends)
  "Return shifted endmembers as columns of <a>."
  (declare (optimize (safety 0) (speed 3))
	   (type List ends))
  (let* ((e0 (first ends))
	 (rest-ends (rest ends))
	 (m (length e0))
	 (n (length rest-ends))
	 (j 0)
	 (a (az:make-float-matrix m n)))
    (declare (type List rest-ends)
	     (type az:Float-Vector e0)
	     (type az:Card16 m n j)
	     (type az:Float-Matrix a))
    (dolist (ej rest-ends)
      (declare (type az:Float-Vector ej))
      (dotimes (i m)
	(declare (type az:Card16 j))
	(setf (aref a i j) (* 1.0d-2 (- (aref ej i) (aref e0 i)))))
      (incf j))
    (values a)))

;;;------------------------------------------------------------

(defun shift-band-vector (e0 bands k b)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Float-Vector e0)
	   (type Simple-Vector bands)
	   (type az:Card28 k)
	   (type az:Float-Vector b))
  (dotimes (j (length bands))
    (declare (type az:Card16 j))
    (let ((band (aref bands j)))
      (declare (type az:Card8-Vector band))
      (setf (aref b j) (- (aref band k) (aref e0 j))))))

;;;------------------------------------------------------------

(defmacro floor-fraction (f)
  `(the az:Card8
     (floor
      (the (Double-Float 0.0d0 255.9d0)
	(bound 0.0d0 (+ 100.0d0 (the Double-Float ,f)) 255.9d0)))))

(defun floor-fractions (fractions f k)
  (declare (optimize (safety 0) (speed 3))
	   (type List fractions)
	   (type az:Float-Vector f)
	   (type az:Card28 k))
  (let ((fr0 (first fractions))
	(i 0)
	(fi 0.0d0)
	(f0 100.0d0))
    (declare (type az:Card8-Vector fr0)
	     (type az:Card16 i)
	     (type Double-Float f0 fi))
    (dolist (fri (rest fractions))
      (declare (type az:Card8-Vector fri))
      (setf fi (aref f i))
      (setf f0 (the Double-Float (- f0 fi)))
      (setf (aref fri k) (floor-fraction fi))
      (incf i))
    (setf (aref fr0 k) (floor-fraction f0))))
 
;;;------------------------------------------------------------

(defun unmix0 (bands ends fractions rms)
  "Compute fractions and residual norm for the mixing model
with end members <ends> for the pixels stored in <bands>."
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-Vector bands)
	   (type List ends fractions)
	   (type az:Card8-Vector rms))
  ;; check to see that sizes match
  (assert (list-of? `(az:Float-Vector ,(length bands)) ends))
  (let* ((n (length ends))
	 (n-1 (- n 1))
	 (m (length bands))
	 (npixels (length (the az:Card8-Vector (aref bands 0))))
	 (b (az:make-float-vector m))
	 (f (az:make-float-vector n-1))
	 (r2 (az:make-float-vector 1))
	 (qrd (g:qr-decompose (shift-endmembers ends)))
	 (rms-scale (/ 100.0d0 (az:fl m))))
    (declare (type az:Card16 m n n-1)
	     (type az:Float-Vector b f r2)
	     (type g:QR-Decomposition qrd)
	     (type (Double-Float 0.0d0 100.0d0) rms-scale))
    ;; compute fractions and residual norm for each pixel
    (dotimes (k npixels)
      (declare (type az:Card28 k))
      (shift-band-vector (first ends) bands k b)
      ;; solve for fraction vector
      (g:qr-solve qrd b f r2)
      ;; rescale and truncate fractions
      (floor-fractions fractions f k)
      ;; compute residual norm
      (setf (aref rms k)
	(the az:Card8
	  (tsqrt (the az:Card16
		   (truncate
		    (* rms-scale
		       (the (Double-Float 0.0d0 *) (aref r2 0))
		       )))))))))

;;;------------------------------------------------------------

(defun unmix1 (bands ends fractions rms)

  "Compute fractions and residual norm for the mixing model
with end members <ends> for the pixels stored in <bands>."

  (declare (optimize (safety 0) (speed 3))
	   (type Simple-Vector bands)
	   (type List ends fractions)
	   (type az:Card8-Vector rms))

  (multiple-value-bind (a- q1t q2t) (g:qr-inverse (shift-endmembers ends))
    (declare (type az:Float-Matrix a- q1t q2t)
	     (ignore q1t))
    (pprint a-)
    (let* ((n (length ends))
	   (n-1 (- n 1))
	   (m (length bands))
	   (m-n+1 (- m n-1))
	   (npixels (length (the az:Card8-Vector (aref bands 0))))
	   (b (az:make-float-vector m))
	   (f (az:make-float-vector n-1))
	   (c2 (az:make-float-vector m-n+1))
	   (r (az:make-float-vector 1))
	   (a-dv (az:array-data a-))
	   (q2tdv (az:array-data q2t))
	   (rms-scale (/ 100.0d0 (az:fl m))))
      (declare (type az:Card16 n n-1 m m-n+1)
	       (type az:Card28 npixels)
	       (type az:Float-Vector b f c2 r a-dv q2tdv)
	       (type (Double-Float 0.0d0 100.0d0) rms-scale))
      ;; compute fractions and root mean square residual for each pixel
      (dotimes (k npixels)
	(declare (type az:Card28 k))
	(shift-band-vector (first ends) bands k b)
	;; solve for fraction vector
	(az::%float-matrix*vector n-1 m a-dv b f)
	;; rescale and truncate fractions
	(floor-fractions fractions f k)
	;; compute residual norm
	(az::%float-matrix*vector m-n+1 m q2tdv b c2)
	(az::%float-vector-len2 m-n+1 c2 r)
	(setf (aref r 0) (* rms-scale (the (Double-Float 0.0d0 *) (aref r 0))))
	(setf (aref rms k)
	  (tsqrt (truncate (the (Double-Float 0.0d0 65535.0d0) (aref r 0))))
	  )))))

;;;------------------------------------------------------------

(defun compare-fraction-image (fractions references)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Vector fractions references))
  (let ((max 0)
	(min 0))
    (declare (type (Integer -255 255) min max))
    (dotimes (i (length fractions))
      (declare (type az:Card28 i))
      (let ((x (- (aref fractions i) (aref references i))))
	(declare (type (Integer -255 255) x))
	(when (> (abs x) 1) (print (cons i x)))
	(when (> x max) (setf max x))
	(when (< x min) (setf min x))))
    (print (cons min max)))
  t)
