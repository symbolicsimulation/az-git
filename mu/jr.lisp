;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
(in-package :Mu) 

(clay:show-diagram (clay:palette))
(loop for i0 from 0
    for names on '("459" "667" "745" "1650")
		 #||
		 (map 'List #'identity
		      (coordinate-names *AVIRIS-subset-spectral-space*))
		 ||#
    for f0 = (first names) do
      (loop for j0 from i0
	  for rnames on (rest names)
	  for f1 = (first rnames) do
	    (make-repscatgram (band *jasper-ridge* f0)
			      (band *jasper-ridge* f1)
			      :left (* i0 100) :top (* j0 100))
	    (loop for f3 in (rest rnames)
		do (make-rotor (band *jasper-ridge* f0) 
			       (band *jasper-ridge* f1) 
			       (band *jasper-ridge* f3)
			       :left 500 :top 4)))
      (make-histogram (band *jasper-ridge* f0) :height 96)
      (make-imagegram (band *jasper-ridge* f0)))
#||
(defparameter *i* (make-imagegram (band *jasper-ridge* "1650")))
(defparameter *h* (make-histogram (band *jasper-ridge* "1650")))
(defparameter *s* (make-repscatgram (band *jasper-ridge* "1650")
				    (band *jasper-ridge* "459")))
(defparameter *r*
    (make-rotor (band *jasper-ridge* "667") (band *jasper-ridge* "745") 
		(band *jasper-ridge* "1650") :left 500 :top 4))s
(dotimes (h -rotor-width-)
  (print h) (force-output)
  (setf (hither *r*) h))

(dotimes (k 64)
  (print (cons (incf (angle *r*) (angular-resolution *r*))
	        (setf (min-count *r*) k)))
  (xlt:drawable-force-output (clay:diagram-window *r*))
  (finish-output))

(setf (min-count *r*) 0)
(setf (min-count *r*) 2)
(setf (min-count *r*) 4)
(setf (min-count *r*) 8)
(setf (min-count *r*) 16)
||#