;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Multi-Band Image File Formats
;;;============================================================

(defclass File-Format (Mu-Object) ()
  (:documentation
   "A root class for all file formats (eg. GIF, JPG, etc)."))

(defclass Basic-File-Format (File-Format)
	  ((header-bytes
	    :reader header-bytes
	    :initarg :header-bytes
	    :type az:Card16)
	   (file-nrows
	    :reader file-nrows
	    :initarg :file-nrows
	    :type az:Card16)
	   (file-ncols
	    :reader file-ncols
	    :initarg :file-ncols
	    :type az:Card16)
	   (xstart
	    :reader xstart
	    :initarg :xstart
	    :type az:Card16)
	   (xend
	    :reader xend
	    :initarg :xend
	    :type az:Card16)
	   (ystart
	    :reader ystart
	    :initarg :ystart
	    :type az:Card16)
	   (yend
	    :reader yend
	    :initarg :yend
	    :type az:Card16))
  (:default-initargs
    :header-bytes 0 :file-nrows 0 :file-ncols 0
    :xstart 0 :xend 0 :ystart 0 :yend 0)
  (:documentation "This is a simple format for raw byte files." ))

(defun make-basic-file-format (&key
			       (file-nrows 0)
			       (file-ncols 0)
			       (header-bytes 0)
			       (xstart 0)
			       (xend file-ncols)
			       (ystart 0)
			       (yend file-nrows)
			       (documentation ""))
  (declare (type az:Card16 file-nrows file-ncols header-bytes
		 xstart xend ystart yend))
    (make-instance 'Basic-File-Format
      :header-bytes header-bytes
      :file-nrows file-nrows
      :file-ncols file-ncols
      :xstart xstart
      :xend xend
      :ystart ystart
      :yend yend
      :documentation documentation))






