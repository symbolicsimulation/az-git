;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; some spatial spaces

(defparameter *fazendas-band-space*
    (make-instance 'Complete-Band-Space
      :name "Fazendas"
      :coordinate-dimensions '(700 1500)
      :documentation "A Band-Space for the Fazendas Landsat TM images."))

(defparameter *fsub-band-space*
    (make-instance 'Complete-Band-Space
      :name "Mini Fazendas"
      :coordinate-dimensions '(167 257)
      :documentation
      "A Band-Space for a subset of the Fazendas Landsat TM images."))

(defparameter *manaus-band-space*
    (make-instance 'Complete-Band-Space
      :name "Manaus"
      :coordinate-dimensions '(1024 1024)
      :documentation "A Band-Space for the Manaus Landsat TM images."))

(defparameter *mri-band-space*
    (make-instance 'Complete-Band-Space
      :name "3 Band MRI Example"
      :coordinate-dimensions '(256 256)
      :documentation "A Band-Space for the sample MRI brain slice."))

(defparameter *jasper-ridge-band-space*
    (make-instance 'Complete-Band-Space
      :name "Jasper Ridge 92"
      :coordinate-dimensions '(512 614)
      :documentation "A Band-Space for some Jasper Rdige AVIRIS images."))
 
;;;============================================================
;;; some partitionings

(defparameter *fsub-paritioning* (make-partitioning *fsub-band-space*))

(defparameter *mri-paritioning* (make-partitioning *mri-band-space*))

;;;============================================================
;;; some spectral spaces

(defparameter *landsat-tm-spectral-space*
    (make-instance 'Spectral-Space
      :name "Landsat-TM"
      :dimension 6
      :coordinate-names '#("b1" "b2" "b3" "b4" "b5" "b7")
      :documentation "A Spectral-Space for Landsat TM images."))

(defparameter *landsat-mss-spectral-space*
    (make-instance 'Spectral-Space
      :name "Landsat-MSS"
      :dimension 6
      :coordinate-names '#("b1" "b2" "b3" "b4")
      :documentation "A Spectral-Space for Landsat MSS images."))

(defparameter *mri-spectral-space*
    (make-instance 'Spectral-Space
      :name "MRI 3 band"
      :dimension 6
      :coordinate-names '#("pd" "t1" "t2")
      :documentation "A Spectral-Space for 3 band magnetic resonance images"))

(defparameter *AVIRIS-subset-spectral-space*
    (make-instance 'Spectral-Space
      :name "AVIRIS 9 band subset"
      :dimension 6
      :coordinate-names (map 'Vector #'(lambda (x) (format nil "~d" x))
			     '#(459 548 627 667 745 803 937 1090 1650))
      :documentation
      "A Spectral-Space for a certain 9 band subset of AVIRIS images"))

;;;============================================================
;;; some sample endmembers

(defparameter *fazendas-endmembers*
    (list (make-endmember "dimona54-89" *landsat-tm-spectral-space*
			  '(91.7 56.5 75.0  92.9 148.4 39.3))
	  (make-endmember "plra000189-hw" *landsat-tm-spectral-space*
			  '(70.7 34.2 45.4  67.2 139.9 44.3)) 
	  (make-endmember "sasp000789-hw" *landsat-tm-spectral-space*
			  '(64.9 29.2 22.6 150.0  88.8 14.1)) 
	  (make-endmember "vegsh89" *landsat-tm-spectral-space*
			  '(51.2 18.5 14.0  35.2  25.5  4.7))))

;;;============================================================
;;; sample file formats

(defparameter *fazendas-format*
    (make-basic-file-format
     :file-nrows 700
     :file-ncols 1500
     :header-bytes 512
     :documentation "A Basic-File-Format for the Fazendas Landsat TM images."))

(defparameter *fsub-format*
    (make-basic-file-format
     :file-nrows 167
     :file-ncols 257
     :documentation
     "A Basic-File-Format for a subset of the Fazendas Landsat TM images."))

(defparameter *manaus-format*
    (make-basic-file-format
     :file-nrows 1024
     :file-ncols 1024
     :header-bytes 512
     :documentation "A Basic-File-Format for the Manaus Landsat TM images."))

(defparameter *mri-format*
    (make-basic-file-format
     :file-nrows 256
     :file-ncols 256
     :documentation "A Basic-File-Format for the sample MRI brain slice."))

(defparameter *AVIRIS-format*
    (make-basic-file-format
     :file-nrows 512
     :file-ncols 614
     :header-bytes 512
     :documentation
     "A Basic-File-Format for AVIRIS image bands."))

;;;============================================================
;;; sample mu's (actual data is only read when needed)

(defparameter *fazendas*
    (make-mu :directory-name "../examples/fazendas/"
		:file-names (list "90-b1.pic" "90-b2.pic" "90-b3.pic"
				  "90-b4.pic" "90-b5.pic" "90-b7.pic"
				  "90-rms.pic" "90-90-shade.pic"
				  "90-90-soil.pic" "90-90-veg.pic"
				  "90-90-woody.pic")
		:file-format *fazendas-format*
		:spectral-space *landsat-tm-spectral-space*
		:band-space *fazendas-band-space*))

(defparameter *fsub*
  (make-mu
   :directory-name "../examples/fazendas-sub/"
   :spectral-space *landsat-tm-spectral-space*
   :band-space *fsub-band-space*
   :file-format *fsub-format*))

(defparameter *manaus*
    (make-mu :directory-name "../examples/manaus/"
		:file-format *manaus-format*
		:band-space *manaus-band-space*
		:spectral-space *landsat-tm-spectral-space*))

(defparameter *mri*
    (make-mu :directory-name "../examples/mri/"
		:file-format *mri-format*
		:band-space *mri-band-space*
		:spectral-space *mri-spectral-space*))

(defparameter *jasper-ridge*
    (make-mu :directory-name "../examples/jasper-ridge/"
		:file-format *AVIRIS-format*
		:band-space *jasper-ridge-band-space*
		:spectral-space *AVIRIS-subset-spectral-space*))

;;;============================================================
;;; a sample Mixer and its fit to some data:

#+:lapack
(defparameter *fazendas-mixer*
    (make-mixer
     "fazendas-mixer"
     (list (make-endmember
	    "dimona54-89"
	    *landsat-tm-spectral-space*
	    '(91.7 56.5 75.0  92.9 148.4 39.3)
	    :documentation "A soil spectrum.")
	   (make-endmember
	    "plra000189-hw"
	    *landsat-tm-spectral-space*
	    '(70.7 34.2 45.4  67.2 139.9 44.3)
	    :documentation "A wood spectrum.") 
	   (make-endmember
	    "sasp000789-hw"
	    *landsat-tm-spectral-space*
	    '(64.9 29.2 22.6 150.0  88.8 14.1)
	    :documentation "A green vegetation spectrum.") 
	   (make-endmember
	    "vegsh89"
	    *landsat-tm-spectral-space*
	    '(51.2 18.5 14.0  35.2  25.5  4.7)
	    :documentation "A shade spectrum."))))

#+:lapack
(defparameter *fsub-mixing*
    (make-mixing "fsub-mixing"
		 *fazendas-mixer*
		 (vector (band *fsub* "b1")
			 (band *fsub* "b2")
			 (band *fsub* "b3")
			 (band *fsub* "b4")
			 (band *fsub* "b5")
			 (band *fsub* "b7"))))

#+:lapack 
(defparameter *fazendas-mixing*
    (make-mixing "fazendas-mixing"
		 *fazendas-mixer*
		 (vector (band *fazendas* "b1")
			 (band *fazendas* "b2")
			 (band *fazendas* "b3")
			 (band *fazendas* "b4")
			 (band *fazendas* "b5")
			 (band *fazendas* "b7"))))