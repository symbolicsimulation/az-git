;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

;;;-------------------------------------------------------------

(defparameter wins
    (loop
	with w = (* 2 257)
	with h = (* 2 167)
	with d = 32
	for i from 0
	for l = (+ 128 (* i d))
	for names on (list "b1" "b2" "b3" "b4" "b5" "b7")
	for f0 = (first names)
	for band = (band *fsub* f0)
	collect
	  (make-imagegram band :left l :top l :width w :height h)
	collect
	  (make-histogram band :left l :top (+ l h d) :width w :height 128)
	do (sleep 5)))
(progn
  (make-imagegram (band *fsub* "b4") :host "louise")
  (make-imagegram (band *fsub* "b5") :host "thelma"))
(make-imagegram (band *fsub* "b3"))
(make-repscatgram (band *fsub* "b4") (band *fsub* "b5"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b4"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b5"))

(progn
  (defparameter *f345*
      (make-rotor
       (band *fsub* "b3") (band *fsub* "b4") (band *fsub* "b5")
       :left 500 :top 4
       :host "thelma"))
  )
(make-imagegram (band *fsub* "b4"))


(defparameter *f345*
    (make-rotor
     (band *fsub* "b3") (band *fsub* "b4") (band *fsub* "b5")
     :left 500 :top 200))

(defparameter *fdps*
      (make-rotor
       (band *fsub* "plra") (band *fsub* "dimona") (band *fsub* "sasp")
       :left 500 :top 4 :fractions? t))

(dotimes (k (+ 1 (rep-maxcount *fdps*))
	   (setf (min-count *fdps*) 0))
  (print (cons (incf (angle *fdps*)
		     (angular-resolution *fdps*))
	       (setf (min-count *fdps*) k)))
  (finish-output))

(setf (min-count *fdps*) 0)
(setf (min-count *fdps*) 2)
(setf (min-count *fdps*) 4)
(setf (min-count *fdps*) 8)
(setf (min-count *fdps*) 16)
(setf (min-count *fdps*) 32)
(setf (min-count *fdps*) 64)

(dotimes (h -rotor-width-)
  (print h) (force-output)
  (setf (hither *f345*) h))

(dotimes (h (- -rotor-width- 2) (set-z-cuts *f345* 0 (+ 1 -rotor-width-)))
  (print h) (force-output)
  (set-z-cuts *f345* h (+ 2 h)))

(dotimes (k (+ 1 (rep-maxcount *f345*))
	   (setf (min-count *f345*) 0))
  (print k) (force-output)
  (setf (min-count *f345*) k))

(dotimes (k (+ 1 (rep-maxcount *fdps*))
	   (setf (min-count *fdps*) 0))
  (print k) (force-output)
  (setf (min-count *fdps*) k))


;;;--------------------------------------------------------------------

(make-imagegram (band *fazendas* "b4"))

(defparameter *fazendas345*
      (make-rotor
       (band *fazendas* "b3") (band *fazendas* "b4") (band *fazendas* "b5")
       :left 500 :top 4))

(dotimes (k (+ 1 (rep-maxcount *fazendas345*))
	   (setf (min-count *fazendas345*) 0))
  (print (cons (incf (angle *fazendas345*))
	       (setf (min-count *fazendas345*) k)))
  (finish-output))

(dotimes (h (- -rotor-width- 2) (set-z-cuts *fazendas345* 0 (+ 1 -rotor-width-)))
  (print h) (force-output)
  (set-z-cuts *fazendas345* h (+ 2 h)))
(setf (min-count *fazendas345*) 40)
(setf (min-count *fazendas345*) 30)
(setf (min-count *fazendas345*) 0)
(setf (min-count *fazendas345*) 2)
(setf (min-count *fazendas345*) 4)
(setf (min-count *fazendas345*) 8)
(setf (min-count *fazendas345*) 16)
(setf (min-count *fazendas345*) 32)
(setf (min-count *fazendas345*) 64)

(make-imagegram (band *fazendas* "veg"))

(defparameter *fazendas-svw*
    (make-rotor
     (band *fazendas* "soil")
     (band *fazendas* "veg")
     (band *fazendas* "woody")
     :left 500 :top 4
     :fractions? t))

(mu::spin-diagram mu::*fazendas-svw*)


(dotimes (k 128
	   (setf (min-count *fazendas-svw*) 0))
  (print (cons (incf (angle *fazendas-svw*)
		     (angular-resolution *fazendas-svw*))
	       (setf (min-count *fazendas-svw*) k)))
  (finish-output))

(dotimes (h (- -rotor-width- 2) (set-z-cuts *fazendas-svw* 0 (+ 1 -rotor-width-)))
  (print h) (force-output)
  (set-z-cuts *fazendas-svw* h (+ 2 h)))

(setf (min-count *fazendas-svw*) 0)
(setf (min-count *fazendas-svw*) 2)
(setf (min-count *fazendas-svw*) 4)
(setf (min-count *fazendas-svw*) 8)
(setf (min-count *fazendas-svw*) 16)
(setf (min-count *fazendas-svw*) 32)
(setf (min-count *fazendas-svw*) 64)


(multiple-value-setq (*fd0* *fd1*)
  (split-space *fazendas-band-space*
		(band *fazendas* "shade") 153))

(defparameter *fd0-svw*
      (make-rotor
       (band *fazendas* "soil")
       (band *fazendas* "veg")
       (band *fazendas* "woody")
       :left 500 :top 4
       :band-space *fd0*))

(defparameter *fd1-svw*
      (make-rotor
       (band *fazendas* "soil")
       (band *fazendas* "veg")
       (band *fazendas* "woody")
       :left 511 :top 4
       :band-space *fd1*))


(dotimes (k (+ 1 (rep-maxcount *fd0-svw*))
	   (setf (min-count *fd0-svw*) 0))
  (print (cons (incf (angle *fd0-svw*)
		     (angular-resolution *fd0-svw*))
	       (setf (min-count *fd0-svw*) k)))
  (finish-output))

(setf (min-count *fd0-svw*) 0)
(setf (min-count *fd0-svw*) 2)
(setf (min-count *fd0-svw*) 4)
(setf (min-count *fd0-svw*) 8)
(setf (min-count *fd0-svw*) 16)
(setf (min-count *fd0-svw*) 32)
(setf (min-count *fd0-svw*) 64)

(progn
  (setf (label-yxz *fd0-svw*) *fraction-label-yxz*)
  (setf (axis-yxz *fd0-svw*) *fraxis-yxz*))