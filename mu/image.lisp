;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defclass Imagegram (Diagram) ())

;;;------------------------------------------------------------
;;; Construction
;;;------------------------------------------------------------

(defun make-imagegram (mu-band
		       &key
		       (host (xlt:default-host))
		       (display (ac:host-display host))
		       (screen (xlib:display-default-screen display))
		       (band-space (band-space mu-band))
		       (left 0)
		       (top 0)
		       (width nil)
		       (height nil))
  (declare (type xlt:Hostname host)
	   (type xlib:Display display)
	   (type xlib:Screen screen)
	   (type (or Mu Band) mu-band)
	   (type Band-Space band-space)
	   (type xlib:Card16 left top)
	   (type (or Null az:Card16) width height))
  (let* ((name (format nil "Im(~a)~a" (name mu-band) (name band-space)))
	 (window (xlt:make-window
		  :parent (xlib:screen-root screen)
		  :colormap (xlt:paint-colormap (xlib:screen-root screen))
		  :left left :top top :width width :height height))
	 (root (make-instance 'clay:Plot-Root
		 :diagram-window window
		 :diagram-name name))
	 (plot (make-instance 'Imagegram
		 :diagram-subject band-space
		 :diagram-parent root
		 :diagram-name name
		 :band0 (when (typep mu-band 'Band) mu-band))))
    (declare (type Simple-String name)
	     (type xlib:Window window)
	     (type clay:Plot-Root root))
    (when (or width height)
      (setf (clay:scale (clay:diagram-lens plot)) nil))
    (clay:build-diagram plot nil)
    (clay:initialize-diagram
     root
     :build-options (list :plot-child plot)
     :mediator (mediator (root-space band-space)))))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Imagegram))
  (setf (clay:diagram-painter diagram)
    (make-instance 'Image-Painter :role-actor diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Role :role-actor diagram)))

(defmethod clay:build-diagram :after ((diagram Imagegram) build-options)
  (declare (ignore build-options))
  (let ((w (ncols (band-space diagram)))
	(h (nrows (band-space diagram))))
    (setf (clay:diagram-image diagram)
      (xlib:create-image :data (az:make-card8-matrix h w)
			 :format :z-pixmap :depth 8 :height h :width w))))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod make-pixmap->image ((diagram Imagegram))
  (let* ((pr (clay:diagram-pixmap-rect diagram))
	 (ir (clay:diagram-image-rect diagram))
	 (p->i (make-instance 'g:Int-Grid-Map
		 :domain (g:home pr) :codomain (g:home ir))))
    (g:set-grid-map p->i pr ir)
    p->i))

(defmethod clay:update-internal ((diagram Imagegram) (stroke T))
  (painted-band (band-space diagram)
		(band0 diagram)
		(xlib:image-z-pixarray (clay:diagram-image diagram))))

;;;============================================================
;;; Drawing 
;;;============================================================

;;;============================================================
;;; Paint
;;;============================================================

(defmethod clay:stroke-object ((diagram Imagegram) (stroke clay:Stroke))
  (clay:update-internal diagram stroke)
  (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id))
  (ac:atomic
   (az:deletef diagram (clay:stroke-objects-to-be-updated stroke))
   (when (null (clay:stroke-objects-to-be-updated stroke))
     (clay:return-stroke stroke))))

;;;------------------------------------------------------------

(defclass Rectangles-Stroke (clay:Stroke)
	  ((stroked-rectangles
	    :type List
	    :accessor stroked-rectangles
	    :initform ()))
  (:documentation
   "This stroke carries a list of those rectangles that have been changed.
The stroked diagram must be a Diagram."))

(defparameter *rectangles-strokes* () "A resource of Rectangles-Stroke's")

(defmethod clay:return-stroke ((stroke Rectangles-Stroke))
  (if (null (clay:stroke-objects-to-be-updated stroke))
      (push stroke *rectangles-strokes*)
    ;; else
    (error "Trying to return ~a before it's done." stroke)))

(defmethod clay:mark-stroke ((stroke Rectangles-Stroke) rect)
  (declare (type g:Rect rect))
  (push (az:copy rect) (stroked-rectangles stroke)))
 
;;;============================================================

(defclass Image-Painter (clay:Painter) ()
  (:documentation
   "The clay:painter-stroke must be a Rectangles-Stroke."))

(defmethod clay:borrow-stroke ((painter Image-Painter))
  (let ((stroke (pop *rectangles-strokes*))
	(stroked-diagram (ac:role-actor painter)))
    (declare (type Diagram stroked-diagram))
    (when (null stroke) (setf stroke (make-instance 'Rectangles-Stroke)))
    (setf (clay:stroked-diagram stroke) stroked-diagram)
    (setf (stroked-rectangles stroke) ())
    stroke))

;;;============================================================

(defmethod clay:stroke-object ((space Complete-Band-Space)
			       (stroke Rectangles-Stroke))
  (declare (optimize (safety 0) (speed 3)))
  (let* ((pixel (clay:stroke-pixel stroke))
	 (pmask (clay:stroke-pmask stroke))
	 (paint-array (paint-matrix (root-space space)))
	 (rowlen (array-dimension paint-array 1))
	 (paint-vector (paint-vector (root-space space))))
    (declare (type az:Card8 pixel pmask)
	     (type az:Card8-Matrix paint-array)
	     (type az:Card16 rowlen)
	     (type az:Card8-Vector paint-vector))
    (dolist (r (stroked-rectangles stroke))
      (declare (type g:Rect r))
      (let* ((xmin (g:x r))
	     (xmax (g:xmax r))
	     (ymin (g:y r))
	     (ymax (g:ymax r)))
	(declare (type az:Card16 xmin ymin xmax ymax))
	(do ((i (the az:Card28 (* rowlen ymin)) (+ i rowlen)))
	    ((>= i (the az:Card28 (* rowlen ymax))))
	  (declare (type az:Card28 i))
	  (do ((j xmin (+ j 1)))
	      ((>= j xmax))
	    (declare (type az:Card16 j))
	    (setf (aref paint-vector (+ i j))
	      (xlt:paint-wash (aref paint-vector (+ i j)) pixel pmask))))))))

(defmethod clay:stroke-object ((space Band-Space-Bipart)
			       (stroke Rectangles-Stroke))
  (declare (optimize (safety 0) (speed 3)))
  (let* ((pixel (clay:stroke-pixel stroke))
	 (pmask (clay:stroke-pmask stroke))
	 (key (key space))
	 (mask (mask space))
	 (part (part-vector space))
	 (paint-array (paint-matrix (root-space space)))
	 (rowlen (array-dimension paint-array 1))
	 (paint-vector (paint-vector (root-space space))))
    (declare (type az:Card8 pixel pmask)
	     (type az:Card16 key mask rowlen)
	     (type az:Card16-Vector part)
	     (type az:Card8-Matrix paint-array)
	     (type az:Card8-Vector paint-vector))
    (dolist (r (stroked-rectangles stroke))
      (declare (type g:Rect r))
      (let* ((xmin (g:x r))
	     (xmax (g:xmax r))
	     (ymin (g:y r))
	     (ymax (g:ymax r)))
	(declare (type az:Card16 xmin ymin xmax ymax))
	(do ((i (the az:Card28 (* rowlen ymin)) (+ i rowlen)))
	    ((>= i (the az:Card28 (* rowlen ymax))))
	  (declare (type az:Card28 i))
	  (do ((j xmin (+ j 1)))
	      ((>= j xmax))
	    (declare (type az:Card16 j))
	    (let ((i+j (+ i j)))
	      (declare (type az:Card28 i+j))
	      (when (= key (logand mask (aref part i+j)))
		(setf (aref paint-vector i+j)
		  (xlt:paint-wash (aref paint-vector i+j) pixel pmask))))))))))













