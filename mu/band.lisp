;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Multi-Band Band Objects
;;;============================================================

(defclass Band (Mu-Object)
	  ((mu
	    :accessor mu
	    :initarg :mu
	    :type Mu)
	   (band-matrix
	    :initarg :band-matrix
	    :type (or Null az:Card8-Matrix)))
  (:default-initargs
      :name (string (gensym "BAND-"))
    :band-matrix nil)

  (:documentation
   "First try at a multi-band image band (band) object."))

;;;------------------------------------------------------------
;;; pseudo slots

(defmethod band-space ((band Band))
  (band-space (mu band)))

;;;------------------------------------------------------------

(defmethod print-object (stream (band Band))
  (az:printing-random-thing
   (band stream)
   (format stream "{~a.~a}" (name (mu band)) (name band))))

;;;------------------------------------------------------------

(defun make-band (mu name)
  (declare (type Mu mu)
	   (type Simple-String name))
  (make-instance 'Band :mu mu :name name))

;;;------------------------------------------------------------

(defun read-band-matrix (band)
  (declare (type Band band))
  (let* ((name (name band))
	 (mu (mu band))
	 (format (file-format mu))
	 (path (find name (file-names mu) :test #'search))
	 (pathstring (concatenate 'String (directory-name mu) path))
	 (data (az:make-card8-matrix (nrows mu) (ncols mu)))
	 (nskip (header-bytes format))
	 (nrows (file-nrows format))
	 (ncols (file-ncols format))
	 (x0 (xstart format))
	 (x1 (xend format))
	 (y0 (ystart format))
	 (y1 (yend format)))
    (declare (type Mu mu)
	     (type File-Format format)
	     (type String name pathstring)
	     (type az:Card16 nskip nrows ncols x0 x1 y0 y1)
	     (type az:Card8-Matrix data))
    (if (and (= x0 y0 0) (= x1 ncols) (= y1 nrows))
	(az:read-card8-array pathstring nskip (* nrows ncols) data)
      (read-partial-image pathstring nskip ncols x0 x1 y0 y1 data))
    data))

;;;------------------------------------------------------------

(defun band-matrix (band)
  "Lazy reading of the band data into memory."
  (declare (type Band band))
  (let ((data (slot-value band 'band-matrix)))
    (when (null data)
      (setf data (read-band-matrix band))
      (setf (slot-value band 'band-matrix) data))
    data))

(defun band-vector (band)
  (declare (type Band band))
  (az:array-data (band-matrix band)))

;;;------------------------------------------------------------

(defun band (mu name)
  "Lazy creation of band object."
  (declare (type Mu mu)
	   (type Simple-String name))
  (let ((path (find name (file-names mu) :test #'search)))
    (when (null path) (error "No band named: ~a" name))
    (let* ((bands (bands mu))
	   (band (az:lookup path bands nil)))
      (declare (type Hash-Table bands)
	       (type (or Null Band) band))
      (when (null band)
	(setf band (make-band mu name))
	(setf (az:lookup path bands) band))
      band)))
 

(defgeneric painted-band (space band out)
  (declare (type Band-Space space)
	   (type Band band)
	   (type az:Card8-Matrix out))
  (:documentation
   "Overlay the paint on the band data for the pixels in the space
and put the result in <out>."))

;;;------------------------------------------------------------

(defmethod painted-band ((space Complete-Band-Space) (band Band) out)

  (declare (optimize (safety 0) (speed 3))
	   (type Complete-Band-Space space)
	   (type Band band)
	   (type az:Card8-Matrix out))

  (let* ((m0 (nrows space))
	 (n0 (ncols space))
	 (n1 (array-dimension out 1))
	 (b  (band-vector band))
	 (p (paint-vector space))
	 (o (az:array-data out)))
    (declare (type az:Card16 m0 n0 n1)
	     (type az:Card8-Vector b p o))
    
    (let ((skip (- n1 n0))
	  (npixels (* m0 n0))
	  (in 0)
	  (iout 0)
	  (line-end 0))
      (declare (type az:Card16 skip)
	       (type az:Card28 npixels in iout line-end))
 
      (loop ;; do each scanline
	(setf line-end (+ in n0))
	(loop
	  (setf (aref o iout) (xlt:hs+v-ref p b in))
	  (incf iout)
	  (when (>= (incf in) line-end) (return)))
	(when (>= in npixels) (return))
	(incf iout skip)))))

(defmethod painted-band ((space Band-Space-Bipart) (band Band) out)

  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type Band band)
	   (type az:Card8-Matrix out))

  (let* ((m0 (nrows space))
	 (n0 (ncols space))
	 (n1 (array-dimension out 1))
	 (b (band-vector band))
	 (p (paint-vector space))
	 (o (az:array-data out))
	 (key (key space))
	 (mask (mask space))
	 (part (part-vector space))
	 (skip (- n1 n0))
	 (npixels (* m0 n0))
	 (in 0)
	 (iout 0)
	 (line-end 0))
    (declare (type az:Card16 m0 n0 n1 skip)
	     (type az:Card8-Vector b p o)
	     (type az:Card16 key mask)
	     (type az:Card16-Vector part)
	     (type az:Card28 npixels in iout line-end))
    (az:unit-card8-vector o)
    (loop ;; do each scanline
      (setf line-end (+ in n0))
      (loop
	(when (= key (logand mask (aref part in)))
	  (setf (aref o iout) (xlt:hs+v-ref p b in)))
	(incf iout)
	(when (>= (incf in) line-end) (return)))
      (when (>= in npixels) (return))
      (incf iout skip))))


(defmethod painted-band ((space Complete-Band-Space) (band Null) out)

  "When <band> is null, just make a paint image."
  (declare (optimize (safety 0) (speed 3))
	   (type Complete-Band-Space space)
	   (type Null band)
	   (type az:Card8-Matrix out))

  (let* ((m0 (nrows space))
	 (n0 (ncols space))
	 (n1 (array-dimension out 1))
	 (p (paint-vector space))
	 (o (az:array-data out)))
    (declare (type az:Card16 m0 n0 n1)
	     (type az:Card8-Vector p o))
    
    (let ((skip (- n1 n0))
	  (npixels (* m0 n0))
	  (in 0)
	  (iout 0)
	  (line-end 0))
      (declare (type az:Card16 skip)
	       (type az:Card28 npixels in iout line-end))
 
      (loop ;; do each scanline
	(setf line-end (+ in n0))
	(loop
	  (setf (aref o iout) (xlt:hs+v (aref p in) 255))
	  (incf iout)
	  (when (>= (incf in) line-end) (return)))
	(when (>= in npixels) (return))
	(incf iout skip)))))

(defmethod painted-band ((space Band-Space-Bipart) (band Null) out)

  "When <band> is null, just make a paint image."
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type Null band)
	   (type az:Card8-Matrix out))

  (let* ((m0 (nrows space))
	 (n0 (ncols space))
	 (n1 (array-dimension out 1))
	 (p (paint-vector space))
	 (o (az:array-data out))
	 (key (key space))
	 (mask (mask space))
	 (part (part-vector space))
	 (skip (- n1 n0))
	 (npixels (* m0 n0))
	 (in 0)
	 (iout 0)
	 (line-end 0))
    (declare (type az:Card16 m0 n0 n1 skip)
	     (type az:Card8-Vector p o)
	     (type az:Card16 key mask)
	     (type az:Card16-Vector part)
	     (type az:Card28 npixels in iout line-end))
    (az:unit-card8-vector o)
    (loop ;; do each scanline
      (setf line-end (+ in n0))
      (loop
	(when (= key (logand mask (aref part in)))
	  (setf (aref o iout) (xlt:hs+v (aref p in) 255)))
	(incf iout)
	(when (>= (incf in) line-end) (return)))
      (when (>= in npixels) (return))
      (incf iout skip))))

;;============================================================

(defun sort-muxels (bands perm)
  "Modified from cmucl SORT. Sorting is done with a heap sort."
  (declare (optimize (safety 0) (speed 3))
	   (type List bands)
	   (type az:Card28-Vector perm))
  (let* ((n (length (the az:Card8-Vector (first bands))))
 	 (n-1 (- n 1)))
    (declare (type az:Card28 n n-1))
    (az:iota-card28-vector perm)
    (macrolet ((pixel< (i0 i1)
		 `(dolist (ch bands nil)
		      (declare (type az:Card8-Vector ch))
		      (let ((ch0 (aref ch ,i0))
			    (ch1 (aref ch ,i1)))
			(declare (type (Unsigned-Byte 8) ch0 ch1))
			(cond ((< ch0 ch1) (return t))
			      ((> ch0 ch1) (return nil)))))))
      (flet ((heapify (root max)
	       "HEAPIFY, assuming both sons of root are heaps, percolates
the root element through the sons to form a heap at root.  Root and
max are zero based coordinates, but the heap algorithm only works on
arrays indexed from 1 through N (not 0 through N-1); This is because a
root at I has sons at 2*I and 2*I+1 which does not work for a root at
0.  Because of this, boundaries, roots, and termination are computed
using 1..N indexes."
	       (declare (optimize (safety 0) (speed 3))
			(type az:Card28 root max))
	       (let* ((hroot (+ 1 root))
		      (hmax (+ 1 max))
		      (rootx (aref perm root))
		      (hmax/2 (ash hmax -1)))
		 (declare (type az:Card28 hroot hmax rootx hmax/2))
		 (loop
		   (when (> hroot hmax/2) (return))
		   (let* ((l (the az:Card28 (ash hroot 1)))
			  ;; l-son index in perm (0..N-1) is
			  ;; one less than heap computation
			  (l-1 (- l 1))
			  (lx (aref perm l-1)))
		     (declare (type az:Card28 l l-1 lx))
		     (when (< l hmax)
		       ;; there is a right son.
		       (let* ((rx (aref perm l)))
			 (declare (type az:Card28 rx))
			 ;; choose the greater of the two sons.
			 (when (pixel< lx rx) (setf l-1 l) (setf lx rx))))
		     ;; if greater son is less than root
		     ;; then we've formed a heap again.
		     (when (pixel< lx rootx) (return))
		     ;; else put greater son at root
		     ;; and make greater son node be the root.
		     (setf (aref perm root) lx)
		     (setf hroot (+ l-1 1)) ;; 1+ to be in heap coordinates.
		     (setf root l-1)))
		 ;; actual index into vector for root ele.
		 ;; now really put percolated value into heap
		 ;; at the appropriate root node.
		 (setf (aref perm root) rootx))))
	;; Rearrange perm elements into a heap to start heap sorting.
	(do ((i (floor n-1 2) (- i 1)))
	    ((minusp i) perm)
	  (declare (type Fixnum i))
	  (heapify i n-1)) 	
	;; sort it
	(do* ((i n-1 i-1)
	      (i-1 (- i 1) (- i-1 1)))
	    ((zerop i) perm)
	  (declare (type Fixnum i i-1))
	  (rotatef (aref perm 0) (aref perm i))
	  (heapify 0 i-1))))
    perm))

;;;------------------------------------------------------------

(defun count-distinct-muxels (bands perm)
  (declare (optimize (safety 0) (speed 3))
	   (type List bands)
	   (type az:Card28-Vector perm))
  (let* ((nbands (length bands))
	 (n (length (the az:Card8-Vector (first bands))))
	 (count 1)
	 (muxel0 (az:make-card8-vector nbands))
	 (muxel1 (az:make-card8-vector nbands)))
    (declare (type az:Card28 nbands n count)
	     (type az:Card8-Vector muxel0 muxel1))
    (macrolet ((get-muxel (i muxel)
		 `(do* ((k 0 (+ k 1))
			(chs bands (rest chs))
			(ch (first chs) (first chs)))
		      ((>= k nbands)
		       ,muxel)
		    (declare (type az:Card28 k)
			     (type List chs)
			     (type az:Card8-Vector ch))
		    (setf (aref ,muxel k) (aref ch ,i))))
	       (muxel= (muxel0 muxel1)
		 `(dotimes (k nbands t)
		    (declare (type az:Card28 k))
		    (unless (= (aref ,muxel0 k) (aref ,muxel1 k))
		      (return nil)))))
      (dotimes (i n)
	(declare (type az:Card28 i))
	(get-muxel (aref perm i) muxel1)
	(unless (muxel= muxel0 muxel1)
	  (incf count)
	  (rotatef muxel0 muxel1))))
    count))

;;;------------------------------------------------------------

(defun print-permuted-pixels (band1 band0 band2 perm
			      &optional
			      (start 0)
			      (end (g:dimension (band-space band1))))
  (declare (type Band band1 band0 band2)
	   (type az:Card28-Vector perm)
	   (type az:Card28 start end))
  (let ((xdv (band-vector band1))
	(ydv (band-vector band0))
	(zdv (band-vector band2)))
    (declare (type az:Card8-Vector xdv ydv zdv))
    (loop for i Fixnum from start below end do
      (let ((j (aref perm i)))
	(format t "~& ~d -> ~d: ~d ~d ~d"
		i j (aref xdv j) (aref ydv j) (aref zdv j))))) )




