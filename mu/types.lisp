;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; useful types for image objects

(deftype Pixmap-Vector (&optional (len nil))
  (when (null len) (setf len '*))
  `(Simple-Array (or Null xlib:Pixmap) (,len)))

(deftype Hither-Yon-Coord () '(Integer 0 #.(+ 1 -rotor-width-)))

(defun make-zr-vector (n)
  (make-array (list n) :element-type 'az:Card16 :initial-element 255))

