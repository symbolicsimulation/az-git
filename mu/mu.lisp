;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Multi-Band Image Objects
;;;============================================================

(defclass Mu (Mu-Object)
	  ((file-format
	    :type File-Format
	    :reader file-format
	    :initarg :file-format)
	   (directory-name
	    :reader directory-name
	    :initarg :directory-name
	    :type Simple-String)
	   (file-names
	    :reader file-names
	    :initarg :file-names
	    :type List)
	   (band-space
	    :reader band-space
	    :initarg :band-space
	    :type Complete-Band-Space)
	   (spectral-space
	    :reader spectral-space
	    :initarg :spectral-space
	    :type Spectral-Space)
	   (bands
	    :reader bands
	    :initform (make-hash-table :test #'equal)
	    :type Hash-Table))
  (:documentation
   "first try at a multi-band image object."))

;;;------------------------------------------------------------

(defmethod print-object (stream (mu Mu))
  (az:printing-random-thing
   (mu stream)
   (format stream "{Mu ~a ~dx~d}"
	  (name mu) (nrows mu) (ncols mu))))

;;;------------------------------------------------------------

(defun make-mu (&key
		(directory-name (error "A directory must be supplied."))
		(file-names (mapcar #'(lambda (p)
					(concatenate 'String
					  (pathname-name p)
					  "."
					  (pathname-type p)))
				    (directory directory-name)))
		(file-format (error "A File-format must be supplied."))
		(band-space
		 (error "A Complete-Band-Space must be supplied."))
		(spectral-space
		 (error "A Spectral-Space must be supplied."))
		(name
		 (let* ((end (- (length directory-name) 1))
			(start (+ 1 (position #\/ directory-name
					      :from-end t :end end))))
		   (subseq directory-name start end))))
  
  (declare (type Simple-String directory-name)
	   (type List file-names)
	   (type file-format file-format)
	   (type Complete-Band-Space band-space)
	   (type Spectral-Space spectral-space)
	   (type Simple-String name))
  
  (make-instance 'Mu
    :directory-name directory-name
    :file-names file-names
    :file-format file-format
    :name name
    :spectral-space spectral-space
    :band-space band-space))

;;;------------------------------------------------------------




