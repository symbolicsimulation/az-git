;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defclass Mediator (clay:Diagram-Mediator Mu-Object) ())

;;;------------------------------------------------------------

(defmethod band-space ((mediator Mediator))
  (declare (optimize (safety 0) (speed 3))
	   (type Mediator mediator))
  (clay:mediator-subject mediator))

;;;------------------------------------------------------------

(defmethod clay:activate-diagram ((diagram Diagram) (mediator Mediator))
  (clay:initialize-paint-dependency diagram mediator))

;;;------------------------------------------------------------

(defclass Mediator-Role (clay:Mediator-Role Mu-Object) ())

;;;------------------------------------------------------------

(defun make-mediator (band-space)
  (declare (type Complete-Band-Space band-space))
  (let ((m (make-instance 'Mediator :mediator-subject band-space)))
    (declare (type Mediator m))
    (ac:start-msg-handling m)
    m))

(defun mediator (band-space)
  (declare (type Complete-Band-Space band-space))
  (let ((m (first (clay:subject-mediators band-space))))
    (when (null m) (setf m (make-mediator band-space)))
    m))

;;;------------------------------------------------------------

(defmethod ac:build-roles ((c Mediator))
  (setf (ac:actor-default-role c)
    (make-instance 'Mediator-Role :role-actor c)))

;;;============================================================
;;; support for paint
;;;============================================================

(defmethod clay:stroke-object ((mediator Mediator) (stroke clay:Stroke))

  ;; update the paint array
  (clay:stroke-object (band-space (clay:stroked-diagram stroke)) stroke)
  ;;(update-zoomed-paints mediator)
  (let ((audience (mapcar #'first (an:audience mediator :painted))))
    (declare (type List audience))
    (if (null audience)
	;; there may be nobody to update
	(clay:return-stroke stroke)
      (progn
	(setf (clay:stroke-objects-to-be-updated stroke) audience)
	(an:announce mediator :painted stroke)))))

