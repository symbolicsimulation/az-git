;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; useful macros

(defmacro bound (xmin x xmax)
  `(max ,xmin (min ,x ,xmax)))

;;;============================================================

(defun sequence-of? (type seq)
  (and (typep seq 'Sequence)
       (every #'(lambda (x) (typep x type)) seq)))

(defun list-of? (type seq)
  (and (typep seq 'List)
       (every #'(lambda (x) (typep x type)) seq)))

(defun vector-of? (type seq)
  (and (typep seq 'Vector)
       (every #'(lambda (x) (typep x type)) seq)))

(defun simple-vector-of? (type seq)
  (and (typep seq 'Simple-Vector)
       (every #'(lambda (x) (typep x type)) seq)))

;;;============================================================

(defun read-partial-image (path nskip rowlen x0 x1 y0 y1 array)

  (declare (optimize (safety 0) (speed 3))
	   (type String path)
	   (type az:Card16 nskip rowlen x0 x1 y0 y1)
	   (type az:Card8-Array array)
	   (inline read-byte))
  (format t "~&Reading ~d bytes from ~s~%"
	  (* (the az:Card16 (- x1 x0)) (the az:Card16 (- y1 y0))) path)
  (force-output)
  #+(and :excl :accelerators)  ;; call a C accelerator
  (read_partial_image path nskip rowlen x0 x1 y0 y1 array)
  #-(and :excl :accelerators)
  (let* ((adv (az:array-data array))
	 (nbytes (- x1 x0))
	 (start 0))
    (declare (type az:Card8-Vector adv)
	     (type az:Card16 nbytes)
	     (type az:Card28 start))
    #+:cmu
    (block :file-open-error
      (let ((fd (unix:unix-open path unix:O_RDONLY 0)))
	(declare (type (or Null unix:Unix-FD) fd))
	(if (null fd)
	    ;; then file didn't open
	    (progn (cerror "Ignore error." "Can't open ~a." path)
		   (return-from :file-open-error))
	  ;; else
	  (unwind-protect
	      (progn
		(unix:unix-lseek fd nskip unix:L_SET)
		(unix:unix-lseek fd (* rowlen y0) unix:L_INCR)
		(setf nskip (- rowlen x1))
		(dotimes (i (the az:Card16 (- y1 y0)))
		  (declare (type az:Card16 i))
		  (unless (zerop x0) (unix:unix-lseek fd x0 unix:L_INCR))
		  (sys:without-gcing
		   (unix:unix-read
		    fd (sys:sap+ (sys:vector-sap adv) start) nbytes))
		  (incf start nbytes)
		  (unless (zerop nskip) 
		    (unix:unix-lseek fd nskip unix:L_INCR))))
	    (unix:unix-close fd)))))
    #-:cmu
    (with-open-file (stream path :direction :input :element-type 'Pixel)
      (flet ((advance-stream (nbytes)
	       (declare (type az:Card16 nbytes))
	       (dotimes (i nbytes)
		 (declare (type az:Card16 i))
		 (read-byte stream)))
	     (read-bytes (nbytes start)
	       (declare (type az:Card16 nbytes start))
	       (do ((i start (+ i 1)))
		   ((>= i (the az:Card16 (+ start nbytes))))
		 (declare (type az:Card16 i))
		 (setf (aref adv i) (the az:Card8 (read-byte stream))))))
	;; skip header and most of <y0> scanlines
	(advance-stream (+ nskip (* rowlen y0)))
	(setf nskip (- rowlen x1))
	(dotimes (i (the az:Card16 (- y1 y0)))
	  (declare (type az:Card16 i))
	  (advance-stream x0)
	  (read-bytes nbytes start)
	  (advance-stream nskip)
	  (incf start nbytes)))))
  (values))

;;;=============================================================
;;; a macro for histograms

(defmacro painthist-index (p b i)
  `(logior (the (Unsigned-Byte 12)
	     (ash (logand (aref ,p ,i) xlt:-paint-hs-mask-)
		  xlt:-paint-hs-bits-))
	   (aref ,b ,i)))

;;;============================================================
;;; A little trigonometry in integer degrees
;;;============================================================

(deftype Degrees () '(Mod 360))

(defparameter *degrees->radians*
    (let ((d->r (make-array '(360) :element-type 'Double-Float)))
      (dotimes (i 360)
	(setf (aref d->r i) (/ (* i pi) 180)))
      d->r))

(declaim (type (az:Float-Vector 360) *degrees->radians*))

(defmacro degrees->radians (degrees)
  `(the Double-Float
     (aref *degrees->radians* (mod (the Degrees ,degrees) 360))))

(defmacro dcos (degrees)
  `(the Double-Float (cos (degrees->radians ,degrees))))

(defmacro dsin (degrees)
  `(the Double-Float (sin (degrees->radians ,degrees))))

;;;============================================================
;;; Packing pixels (bytes) into longer words
;;;============================================================

(defmacro yxr->x (yxr)      `(logand ,yxr #x001ff))
(defmacro yxr->y (yxr) `(ash (logand ,yxr #x1fe00) -9))

(defmacro pp (p0 p1)
  `(the az:Card16
     (logior (ash (the az:Card8 ,p0) 8)
	          (the az:Card8 ,p1))))

(defmacro ppp (p0 p1 p2)
  `(the az:Card24
     (logior (ash (the az:Card8 ,p0) 16)
	     (pp ,p1 ,p2))))

(defmacro ppref (p0 p1 i)
  (let ((ii (gensym)))
    `(let ((,ii ,i))
       (declare (type az:Card28 ,ii))
       (pp (aref (the az:Card8-Vector ,p0) ,ii)
	   (aref (the az:Card8-Vector ,p1) ,ii)))))

(defmacro ppref- (p0 p1 i)
  (let ((ii (gensym)))
    `(let ((,ii ,i))
       (declare (type az:Card28 ,ii))
       (pp (- 255 (aref (the az:Card8-Vector ,p0) ,ii))
	   (aref (the az:Card8-Vector ,p1) ,ii)))))

(defmacro pppref (p0 p1 p2 i)
  (let ((ii (gensym)))
    `(let ((,ii ,i))
       (declare (type az:Card28 ,ii))
       (ppp (aref (the az:Card8-Vector ,p0) ,ii)
	    (aref (the az:Card8-Vector ,p1) ,ii)
	    (aref (the az:Card8-Vector ,p2) ,ii)))))

(defmacro pppref- (p0 p1 p2 i)
  (let ((ii (gensym)))
    `(let ((,ii ,i))
       (declare (type az:Card28 ,ii))
       (ppp (- 255 (aref (the az:Card8-Vector ,p0) ,ii))
	    (aref (the az:Card8-Vector ,p1) ,ii)
	    (aref (the az:Card8-Vector ,p2) ,ii)))))

;;;============================================================
;;; Transforming from fractions (barycentric coordinates)
;;; to cartesian coordinates
;;;============================================================

(defparameter -xxbary- (az:make-card8-vector 256))
(defparameter -yxbary- (az:make-card8-vector 256))
(defparameter -yybary- (az:make-card8-vector 256))
(declaim (type (az:Card8-Vector 256) -xxbary- -yxbary- -yybary-))

(eval-when (load eval)
  (dotimes (i 256)
    (setf (aref -xxbary- i) (truncate (* 2 i) 3))
    (setf (aref -yxbary- i) (truncate (* 2 i (cos (/ pi 3))) 3))
    (setf (aref -yybary- i) (- 212 (truncate (* 2 i (sin (/ pi 3))) 3)))))

(defmacro xbary (y x)
  `(the az:Card8
     (+ (the az:Card8 (aref (the (az:Card8-Vector 256) -xxbary-)
			 (the az:Card8 ,x)))
	(the az:Card8 (aref (the (az:Card8-Vector 256) -yxbary-)
			 (the az:Card8 ,y))))))
	
(defmacro ybary (y x)
  (declare (ignore x))
  `(the az:Card8
     (aref (the (az:Card8-Vector 256) -yybary-)
	   (the az:Card8 ,y))))

(defmacro yxbary (y x i)
  (let ((xx (gensym))
	(yy (gensym))
	(ii (gensym)))
    `(let* ((,ii ,i)
	    (,xx (aref ,x ,ii))
	    (,yy (aref ,y ,ii)))
       (declare (type az:Card28 ,ii)
		(type az:Card8 ,xx ,yy))
       (the az:Card16 (pp (ybary ,yy ,xx) (xbary ,yy ,xx))))))

;;;============================================================

(defclass Mu-Object (az:Arizona-Object)
	  ((name
	    :type Simple-String
	    :reader name
	    :initarg :name
	    :documentation "Mu objects have names.")
	   (doc-string
	    :type Simple-String
	    :reader doc-string
	    :initarg :documentation
	    :documentation
	    "All Mu objects may have a documentation string.
The slot and reader are named <doc-string> to avoid a conflict
with the <documentation> generic function in the CL package."))
  
  (:default-initargs
      :name (string (gensym))
    :documentation "")
  
  (:documentation
   "A root class for all objects in the Mu module."))

;;;------------------------------------------------------------

(defmethod print-object (stream (mo Mu-Object))
  (az:printing-random-thing
   (mo stream)
   (format stream "~:(~a~) ~a"
	   (class-name (class-of mo))
	   (name mo))))

(defmethod lay:node-name ((mo Mu-Object)) (name mo))

;;;============================================================

(defun pack-3-card8-vectors (b0 b1 b2 b3)

  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Vector b0 b1 b2)
	   (type az:Card24-Vector b3))

  (dotimes (i (length b0))
    (declare (type az:Card28 i))
    (setf (aref b3 i) (pppref b0 b1 b2 i)))
  b3)

