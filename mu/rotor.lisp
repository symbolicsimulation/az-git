;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defparameter *axis-yxz-default* nil)

(defun axis-yxz-default ()
  (when (null *axis-yxz-default*)
    (let ((yxz (az:make-card24-vector (+ 1 (* 3 64)))))
      (setf (aref yxz 0) 0)
      (loop for i from 1 to 64 do (setf (aref yxz i) (ash i 2))) ;; z axis
      (loop for i from 65 to 128 do (setf (aref yxz i) (ash i 10))) ;; x axis
      (loop for i from 129 to 192 do (setf (aref yxz i) (ash i 18))) ;; y axis
      (setf *axis-yxz-default* yxz)))
  *axis-yxz-default*)

(defparameter *label-yxz-default* nil)

(defun label-yxz-default ()
  (when (null *label-yxz-default*)
    (let ((yxz (az:make-card24-vector 3)))
      (setf (aref yxz 0) (ppp 245 12 12)) ;; y axis label
      (setf (aref yxz 1) (ppp 12 245 12)) ;; x axis  label
      (setf (aref yxz 2) (ppp 12 12 245)) ;; z axis label
      (setf *label-yxz-default* yxz)))
  *label-yxz-default*)

;;;------------------------------------------------------------

(defparameter *fraction-axis-yxz-default* nil)

(defun fraction-axis-yxz-default ()
  (when (null *fraction-axis-yxz-default*)
    (let ((yxz (az:make-card24-vector (* 6 26)))
	  (i 0))
      ;; z axis
      (dotimes (j 26)
	(setf (aref yxz i) (ppp 100 100 (+ 100 (* j 4))))
	(incf i))
      ;; x axis
      (dotimes (j 26)
	(setf (aref yxz i) (ppp 100 (+ 100 (* j 4)) 100))
	(incf i))
      ;; y axis
      (dotimes (j 26)
	(setf (aref yxz i) (ppp (+ 100 (* j 4)) 100 100))
	(incf i))
      ;; xy axis
      (dotimes (j 26)
	(setf (aref yxz i) (ppp (+ 100 (* j 4)) (- 200 (* j 4))  100))
	(incf i))
      ;; xz axis
      (dotimes (j 26)
	(setf (aref yxz i) (ppp 100 (- 200 (* j 4)) (+ 100 (* j 4))))
	(incf i))
      ;; yz axis
      (dotimes (j 26)
	(setf (aref yxz i) (ppp (- 200 (* j 4)) 100  (+ 100 (* j 4))))
	(incf i))
      (setf  *fraction-axis-yxz-default* yxz)))
  *fraction-axis-yxz-default*)

(defparameter *fraction-label-yxz-default* nil)

(defun fraction-label-yxz-default ()
  (when (null *fraction-label-yxz-default*)
    (let ((yxz (az:make-card24-vector 3)))
      (setf (aref yxz 0) (ppp 232 100 100)) ;; y axis label
      (setf (aref yxz 1) (ppp  100 232  68)) ;; x axis label
      (setf (aref yxz 2) (ppp  100  68 232)) ;; z axis label
      (setf *fraction-label-yxz-default* yxz)))
  *fraction-label-yxz-default*)

;;;============================================================

(defclass Rotor-Role (Role) ())

;;;------------------------------------------------------------

(defun role-set-angle (role msg-name sender receiver id angle)
  (declare (type Rotor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Rotor receiver)
	   (type ac:Msg-Id id)
	   (type Number angle)
	   (ignore role msg-name sender id))
  (setf (angle receiver) angle))
  
;;;============================================================

(defclass Rotor-Dragger (Rotor-Role)
	  ((x0
	    :type xlib:Int16
	    :accessor x0
	    :initform 0)))

;;;------------------------------------------------------------


(defclass Rotor (General-Scattergram)
	  ((angle
	    :type Number
	    :reader angle
	    :initform 45)
	   (angular-resolution
	    :type Degrees
	    :reader angular-resolution
	    :initform 5)

	   (yon
	    :type Hither-Yon-Coord
	    :reader yon
	    :initform 0
	    :documentation
	    "Only draw rotor cells with z value greter than <yon>,
             ie., in front of the yon plane.")
	   (hither
	    :type Hither-Yon-Coord
	    :reader hither
	    :initform #.(+ 1 -rotor-width-)
	    :documentation
	    "Only draw rotor cells with z value less than <hither>,
             ie., behind the hither plane.")

	   (min-count
	    :type az:Card28
	    :reader min-count
	    :initform 0
	    :documentation
	    "Used for thinning the plot. Only those cells with
             at least <min-count> observations are drawn.")

	   ;; axes
	   (axis-yxz
	    :type az:Card24-Vector
	    :reader axis-yxz
	    :initform (axis-yxz-default)
	    :documentation
	    "Packed 3d coords of coordinate axes and other reference marks.")
	   (axis-yxr
	    :type az:Card24-Vector
	    :reader axis-yxr
	    :documentation
	    "Rotated yx coords of coordinate axes and other reference marks.")
	   (axis-zr
	    :type az:Card16-Vector
	    :reader axis-zr
	    :documentation
	    "Rotated z coord of coordinate axes and other reference marks.")

	   ;; labels
	   (label-strings
	    :type List
	    :initform ()
	    :accessor label-strings
	    :documentation
	    "The strings that are printed at the label positions.")
	   (label-yxz
	    :type az:Card24-Vector
	    :reader label-yxz
	    :initform (label-yxz-default)
	    :documentation
	    "Packed 3d coords of coordinate axes and other reference marks.")
	   (label-yxr
	    :type az:Card24-Vector
	    :reader label-yxr
	    :documentation
	    "Rotated yx coords of coordinate axes and other reference marks.")
	   (label-zr
	    :type az:Card16-Vector
	    :reader label-zr
	    :documentation
	    "Rotated z coord of coordinate axes and other reference marks.")
	   
	   (ycuts
	    :type (az:Card28-Vector 256)
	    :accessor ycuts
	    :initform (az:make-card28-vector 256))
	   (yxz
	    :type az:Card24-Vector
	    :accessor yxz
	    :documentation
	    "yxz coordinates of the reps, packed into az:Card24 words.")
	   
	   
	   (yxr
	    :type az:Card28-Vector
	    :accessor yxr
	    :documentation
	    "Rotated y and x coordinates of the representative pixels,
            packed into a single word for each pixel.")
	   (zr
	    :type az:Card16-Vector
	    :accessor zr
	    :documentation
	    "Rotated z coordinate of the representative pixels.")

	   (dragger
	    :type Rotor-Dragger
	    :accessor dragger
	    :documentation "A role for manual rotation.")
	  
	   (z-buffer
	    :type (az:Card16-Matrix 256 512)
	    :reader z-buffer
	    :initform (az:make-card16-matrix 256 512)) 
	   (rotor-frames
	    :type Pixmap-Vector
	    :reader rotor-frames
	    :documentation
	    "A cache of pixmaps of angles previously visited.")
	   (xcos
	    :type az:Card16-Vector
	    :reader xcos
	    :initform (az:make-card16-vector 256)
	    :documentation
	    "Trig Tables replace multiplies by sines and cosines with arefs.")
	   (xsin
	    :type az:Card16-Vector
	    :reader xsin
	    :initform (az:make-card16-vector 256)
	    :documentation
	    "Trig Tables replace multiplies by sines and cosines with arefs."))
  (:documentation
   "A 3d rotating scatterplot."))

;;;------------------------------------------------------------

(defun make-rotor (band1 band0 band2
		   &key
		   (fractions?
		    (and (typep band0 'Fraction)
			 (typep band1 'Fraction)
			 (typep band2 'Fraction)))
		   (host (xlt:default-host))
		   (display (ac:host-display host))
		   (screen (xlib:display-default-screen display))
		   (band-space (band-space band1))
		   (left 0) (top 0)
		   (width nil) (height nil))
  (declare (type Band band1 band0 band2)
	   (type Simple-String host)
	   (type xlib:Display display)
	   (type xlib:Screen screen)
	   (type Band-Space band-space)
	   (type az:Card16 left top)
	   (type (or Null az:Card16) width height))
  (let* ((name (format nil "R[~a,~a,~a]~a"
		       (name band1) (name band0) (name band2)
		       (name band-space)))
	 (window (xlt:make-window
		  :parent (xlib:screen-root screen)
		  :colormap (xlt:paint-colormap (xlib:screen-root screen))
		  :left left :top top :width width :height height))
	 (root (make-instance 'clay:Plot-Root
		 :diagram-window window
		 :diagram-name name))
	 (plot (make-instance 'Rotor
		 :diagram-subject band-space
		 :diagram-parent root
		 :diagram-name name
		 :band1 band1 :band0 band0 :band2 band2)))
    (declare (type Simple-String name)
	     (type xlib:Window window)
	     (type clay:Plot-Root root)
	     (type Rotor plot))
    (when fractions?
      (setf (slot-value plot 'label-yxz) (fraction-label-yxz-default))
      (setf (slot-value plot 'axis-yxz) (fraction-axis-yxz-default)))
    (when (or width height)
      (setf (clay:scale (clay:diagram-lens plot)) nil))
    (clay:build-diagram plot nil)
    (clay:initialize-diagram
     root
     :build-options (list :plot-child plot)
     :mediator (mediator (root-space band-space)))))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Rotor))
  (setf (clay:diagram-painter diagram)
    (make-instance 'Rotor-Painter :role-actor diagram))
  (setf (dragger diagram)
    (make-instance 'Rotor-Dragger :role-actor diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Rotor-Role :role-actor diagram)))

(defun allocate-rotor-caches (diagram nreps)
  (declare (type Rotor diagram)
	   (type az:Card28 nreps))
  (setf (nreps diagram) nreps)
  (when (or (not (slot-boundp diagram 'rep-pins))
	    (< (length (rep-pins diagram)) nreps))
    ;; then assume everything is either unallocated or too small
    (setf (rep-pins diagram) (az:make-card28-vector nreps))
    (setf (rep-paint diagram) (az:make-card8-vector nreps))
    (setf (yxz diagram) (az:make-card24-vector nreps))
    (setf (rep-hist diagram) (az:make-card28-vector nreps))
    (setf (rep-speckle diagram) (az:make-card8-vector nreps))
    (setf (yxr diagram) (az:make-card24-vector nreps))
    (setf (zr diagram) (az:make-card16-vector nreps))
    (let ((axis-n (length (the az:Card28-Vector (axis-yxz diagram))))
	  (label-n (length (the az:Card28-Vector (label-yxz diagram)))))
      (declare (type az:Card28 axis-n label-n))
      (setf (slot-value diagram 'axis-yxr) (az:make-card24-vector axis-n))
      (setf (slot-value diagram 'axis-zr) (az:make-card16-vector axis-n))
      (setf (slot-value diagram 'label-yxr) (az:make-card24-vector label-n))
      (setf (slot-value diagram 'label-zr) (az:make-card16-vector label-n))))
  (values diagram))
      
;;;------------------------------------------------------------


(defmethod clay:build-diagram :after ((diagram Rotor) options)
  (declare (ignore options))
  (setf (clay:diagram-image diagram)
    (xlib:create-image :data (az:make-card8-matrix 256 512)
		       :format :z-pixmap :depth 8 :height 256 :width 512))
  (let ((r (clay:diagram-image-rect diagram)))
    (declare (type g:Rect r))
    
    (setf (g:x r) 0)
    (setf (g:y r) 0)
    (setf (g:w r) -rotor-width-)
    (setf (g:h r) -rotor-height-)

    
    (setf (slot-value diagram 'rotor-frames)
      (make-array (list (truncate 360 (angular-resolution diagram)))
		  :initial-element nil      
		  :element-type '(or Null xlib:Pixmap)))

    (setf (label-strings diagram)
      (list (name (band0 diagram))
	    (name (band1 diagram))
	    (name (band2 diagram))))))
 
;;;============================================================
;;; Layout
;;;============================================================

(defun count-nreps (yxz sorted-pins)

  (declare (optimize (safety 0) (speed 3))
	   (type az:Card24-Vector yxz)
	   (type az:Card28-Vector sorted-pins))

  (let* ((n (length sorted-pins))
	 (count 1)
	 (yxz0 (aref yxz (aref sorted-pins 0))))
    (declare (type az:Card28 n count)
	     (type az:Card24 yxz0))
    (dotimes (i n)
      (declare (type az:Card28 i))
      (let ((yxz1 (aref yxz (aref sorted-pins i))))
	(declare (type az:Card24 yxz1))
	(unless (= yxz0 yxz1)
	  (incf count)
	  (setf yxz0 yxz1))))
    count))

(defun select-rep-pins (diagram perm yxzall)
  
  (declare (optimize (safety 0) (speed 3))
	   (type Rotor diagram)
	   (type az:Card28-Vector perm)
	   (type az:Card24-Vector yxzall))

  (let* ((k 0)
	 (n (length perm))
	 (rep-pins (rep-pins diagram))
	 (yxz (yxz diagram))
	 (rep-hist (rep-hist diagram))
	 (j0 (aref perm 0))
	 (j1 j0)
	 (yxz0 (aref yxzall j0))
	 (yxz1 yxz0)
	 (i0 0))
    (declare (type az:Card28 n k j0 j1 i0)
	     (type az:Card28-Vector rep-pins rep-hist)
	     (type az:Card24-Vector yxz)
	     (type az:Card24 yxz0 yxz1))
    (dotimes (i n)
      (declare (type az:Card28 i))
      (setf j1 (aref perm i))
      (setf yxz1 (aref yxzall j1))
      (unless (= yxz0 yxz1)
	(setf (aref rep-pins k) j0)
	(setf (aref yxz k) yxz0)
	(setf (aref rep-hist k) (- i i0))
	(setf i0 i)
	(incf k)
	(setf yxz0 yxz1)
	(setf j0 j1))
      (setf (aref rep-pins k) j0)
      (setf (aref yxz k) yxz0)
      (setf (aref rep-hist k) (- n i0)))
    (setf (rep-maxcount diagram) (az:max-card28-vector rep-hist))
    (values rep-pins)))

;;;------------------------------------------------------------

(defmethod fill-rep-speckle ((diagram Rotor))
  (declare (optimize (safety 0) (speed 3))
	   (type Rotor diagram))
  (let ((rep-speckle (rep-speckle diagram)))
    (declare (type az:Card8-Vector rep-speckle))
    (dotimes (i (length rep-speckle))
      (declare (type az:Card28 i))
      (setf (aref rep-speckle i) (+ 131 (mod (ash i 4) 124))))))

;;;------------------------------------------------------------

(defmethod update-rep-paint ((diagram Rotor))
  (declare (optimize (safety 0) (speed 3))
	   (type Rotor diagram))
  (let ((rep-pins (rep-pins diagram))
	(rep-paint (rep-paint diagram))
	(rep-speckle (rep-speckle diagram))
	(p (paint-vector (band-space diagram))))
    (declare (type az:Card28-Vector rep-pins)
	     (type az:Card8-Vector rep-paint rep-speckle p))
    (dotimes (i (the az:Card28 (length rep-paint)))
      (declare (type az:Card28 i))
      (setf (aref rep-paint i)
	(xlt:hs+v (aref p (aref rep-pins i)) (aref rep-speckle i))))))
  
;;;------------------------------------------------------------

(defun get-ycuts (diagram)
  (declare (optimize (safety 0) (speed 3))
	   (type Rotor diagram))
  (let ((yxz (yxz diagram))
	(ycuts (ycuts diagram)))
    (declare (type az:Card24-Vector yxz)
	     (type az:Card28-Vector ycuts))
    ;; initialize
    (az:zero-card28-array ycuts)
    ;; compute histogram
    (dotimes (i (the az:Card28 (length yxz)))
      (declare (type az:Card28 i))
      (incf
       (aref ycuts (the az:Card8 (ash (the az:Card24 (aref yxz i)) -16)))))
    ;; integrate
    (az:integrate-card28-vector ycuts ycuts)))
 
;;;------------------------------------------------------------
;;; Rotation
;;;------------------------------------------------------------

(defun fill-trig-tables (angle xcos xsin)
  (declare (optimize (safety 0) (speed 3))
	   (type Degrees angle)
	   (type az:Card16-Vector xcos xsin))
  (let ((c (dcos angle))
	(s (dsin angle)))
    (declare (type Double-Float c s))
    (dotimes (x 256)
      (let* ((fx (- (the az:Card8 x) 127.5d0))
	     (cfx (* c fx))
	     (sfx (* s fx)))
	(declare (type (Double-Float -127.5d0 127.5d0) fx cfx sfx))
	;; the 181 is to make the output xr centered properly
	;; we don't have to worry about the output zr,
	;; as long as it's of the right type
	(setf (aref xcos x)
	  (the az:Card16 (+ 128 181 (the (Integer -128 127) (truncate cfx)))))
	(setf (aref xsin x)
	  (the az:Card16 (+ 128 (the (Integer -128 127) (truncate sfx)))))))))

;;;------------------------------------------------------------

(defun simple-rotate (yxz xcos xsin yxr zr)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card24-Vector yxz)
	   (type az:Card16-Vector xcos xsin)
	   (type az:Card28-Vector yxr)
	   (type az:Card16-Vector zr))
  (dotimes (i (length yxz))
    (declare (type az:Card28 i))
    (let* ((yxzi (aref yxz i))
	   (z (logand yxzi #x0000ff))
	   (y (logand (ash yxzi -7) #x1fe00))
	   (x (logand (ash yxzi -8) #x00ff))
	   (xri (- (aref xcos x) (aref xsin z)))
	   (zri (+ (aref xsin x) (aref xcos z)))
	   (yxri (logior y (the (Integer 0 511) xri))))
      (declare (type az:Card8 x z)
	       (type az:Card28 y)
	       (type az:Card16 xri zri)
	       (type az:Card28 yxri)
	       (type az:Card24 yxzi))
      (setf (aref yxr i) yxri)
      (setf (aref zr i) zri))))

;;;------------------------------------------------------------

(defun rotor-rotate (min-count ycuts rep-hist yxz xcos xsin yxr zr)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card28 min-count)
	   (type az:Card24-Vector yxz)
	   (type az:Card28-Vector ycuts rep-hist)
	   (type az:Card16-Vector xcos xsin)
	   (type az:Card28-Vector yxr)
	   (type az:Card16-Vector zr))
  (let ((y 0)
	(i0 0)
	(i1 0))
    (declare (type az:Card28 y i0 i1))
    (dotimes (j 256)
      (declare (type (Integer 0 256) j))
      (setf i0 i1)
      (setf i1 (aref ycuts j))
      (do ((i i0 (+ i 1)))
	  ((>= i i1))
	(declare (type az:Card28 i))
	(when (>= (aref rep-hist i) min-count)
	  ;; don't bother to calculate rotated coords for thinned data
	  (let* ((yxzi (aref yxz i))
		 (z (logand yxzi #x0000ff))
		 (x (logand (ash yxzi -8) #x00ff))
		 (xri (- (aref xcos x) (aref xsin z)))
		 (zri (+ (aref xsin x) (aref xcos z)))
		 (yxri (logior y (the (Integer 0 511) xri))))
	    (declare (type az:Card16 x z)
		     (type az:Card16 xri zri)
		     (type az:Card28 yxri)
		     (type az:Card24 yxzi)) 
	    (setf (aref yxr i) yxri)
	    (setf (aref zr i) zri))))
      (incf y 512))))

;;;------------------------------------------------------------

(defgeneric rotate-diagram (diagram)
  (declare (type Diagram diagram))
  (:documentation
   "Calculate rotated coordinates at current angle."))

(defmethod rotate-diagram ((diagram Rotor))
  (declare (type Rotor diagram))
  (let ((delta (angular-resolution diagram))
	(xcos (xcos diagram))
	(xsin (xsin diagram)))
    (declare (type Degrees delta))
    ;; fill tables
    (fill-trig-tables (* delta (truncate (angle diagram) delta)) xcos xsin )
    ;; calculate rotated coordinates
    (rotor-rotate (min-count diagram) (ycuts diagram) (rep-hist diagram)
		  (yxz diagram) xcos xsin
		  (yxr diagram) (zr diagram))
    (simple-rotate (axis-yxz diagram) xcos xsin
		   (axis-yxr diagram) (axis-zr diagram))
    (simple-rotate (label-yxz diagram) xcos xsin
		   (label-yxr diagram) (label-zr diagram))))

;;;------------------------------------------------------------
;;; overall layout
;;;------------------------------------------------------------

(defmethod clay:layout-diagram :before ((diagram Rotor) layout-options)
  (declare (type List layout-options)
	   (ignore layout-options))
  (with-index (index diagram)
    (let* ((b0 (band-vector (band0 diagram)))
	   (b1 (band-vector (band1 diagram)))
	   (b2 (band-vector (band2 diagram)))
	   (b3 (pack-3-card8-vectors
		b0 b1 b2 (work-pins (root-space (band-space diagram)))))
	   (perm (sorted-pins index)))
      (declare (type az:Card8-Vector b0 b1 b2)
	       (type az:Card24-Vector b3)
	       (type az:Card28-Vector perm))
      (allocate-rotor-caches diagram (count-nreps b3 perm))
      (fill-rep-speckle diagram)
      (select-rep-pins diagram perm b3)))
  (get-ycuts diagram))

;;;============================================================
;;; Drawing
;;;============================================================

(defun draw-back-labels (diagram fr)
  (declare (type Rotor diagram)
	   (type xlib:Pixmap fr))
  (let* ((gc (xlt:3d-mid-gcontext (clay:diagram-window diagram) :gray))
	(yxr (label-yxr diagram))
	(zr (label-zr diagram))
	(map (clay:pixmap->image (clay:diagram-lens diagram)))
	(p (g:make-element (g:codomain map)))
	(strings (label-strings diagram)))
    (declare (type xlib:Gcontext gc)
	     (type az:Card24-Vector yxr)
	     (type az:Card16-Vector zr)
	     (type g:Point p)
	     (type List strings))
    ;; put labels on frame pixmap
    (dotimes (i 3)
      ;; is the label behind the rotated Z center?
      (when (<= (aref zr i) (+ 255 181))
	(setf (g:x p) (yxr->x (aref yxr i)))
	(setf (g:y p) (yxr->y (aref yxr i)))
	(xlt:draw-string fr gc (g:inverse-transform map p) (elt strings i))))))

(defun draw-front-labels (diagram fr)
  (declare (type Rotor diagram)
	   (type xlib:Pixmap fr))
  (let* ((gc (xlt:3d-high-gcontext (clay:diagram-window diagram) :gray))
	 (yxr (label-yxr diagram))
	 (zr (label-zr diagram))
	 (map (clay:pixmap->image (clay:diagram-lens diagram)))
	 (p (g:make-element (g:codomain map)))
	 (strings (label-strings diagram)))
    (declare (type xlib:Gcontext gc)
	     (type az:Card24-Vector yxr)
	     (type az:Card16-Vector zr)
	     (type g:Point p)
	     (type List strings))
    ;; put labels on frame pixmap
    (dotimes (i 3)
      ;; is the label in front of the rotated Z center?
      (when (> (aref zr i) (+ 255 181))
	(setf (g:x p) (yxr->x (aref yxr i)))
	(setf (g:y p) (yxr->y (aref yxr i)))
	(xlt:draw-string fr gc (g:inverse-transform map p) (elt strings i))))))
  
;;;------------------------------------------------------------

(defun simple-render (yxr zr p zbf pix)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card28-Vector yxr)
	   (type az:Card16-Vector zr)
	   (type az:Card8 p)
	   (type az:Card16-Vector zbf)
	   (type az:Card8-Vector pix))
  (dotimes (i (length yxr))
    (declare (type az:Card28 i))
    (let ((yxri (aref yxr i))
	  (zri (aref zr i)))
      (declare (type (Unsigned-Byte 17) yxri)
	       (type az:Card16 zri))
      (when (> zri (aref zbf yxri))
	(setf (aref zbf yxri) zri)
	(setf (aref pix yxri) p)))))

(defun rotor-render (min-count yon hither
		     ycuts rep-hist rep-paint yxr zr
		     zbf pix)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card28 min-count)
	   (type az:Card16 yon hither)
	   (type az:Card28-Vector ycuts rep-hist)
	   (type az:Card8-Vector rep-paint)
	   (type az:Card28-Vector yxr)
	   (type az:Card16-Vector zr)
	   (type az:Card16-Vector zbf)
	   (type az:Card8-Vector pix))
  ;; render hidden points using z buffer
  (az:zero-card16-vector zbf)
  (let ((i0 0) (i1 0))
    (declare (type az:Card28 i0 i1))
    (dotimes (j 256)
      (declare (type (Integer 0 256) j))
      (setf i1 (aref ycuts j))
      (do ((i i0 (+ i 1)))
	  ((>= i i1))
	(declare (type az:Card28 i))
	(let ((yxri (aref yxr i))
	      (zri (aref zr i)))
	  (declare (type az:Card16 zri)
		   (type (Unsigned-Byte 17) yxri))
	  (when (and (>= (aref rep-hist i) min-count)
		     (>= zri yon)
		     (< zri hither)
		     (> zri (the (Unsigned-Byte 10) (aref zbf yxri))))
	    (setf (aref zbf yxri) zri)
	    (setf (aref pix yxri) (aref rep-paint i)))))
      (setf i0 i1))))

;;;------------------------------------------------------------

(defmethod clay:render-diagram ((diagram Rotor) frame)
  "Assume diagram-image is already cleared or whatever."
  (declare (type Rotor diagram)
	   (type xlib:Pixmap frame))
  (let* ((pix (xlt:image-pixel-vector (clay:diagram-image diagram)))
	 (zbf (az:array-data (z-buffer diagram)))
	 (p (xlib:gcontext-foreground
	     (xlt:3d-high-gcontext (clay:diagram-window diagram) :gray)))
	 (win (clay:diagram-window diagram))
	 (gc (xlt:drawable-xor-gcontext win)))
    (declare (type az:Card8-Vector pix)
	     (type az:Card16-Vector zbf)
	     (type az:Card8 p)
	     (type xlib:Window win)
	     (type xlib:Gcontext gc))
    ;;(az:unit-card8-vector pix)
    (az:zero-card8-vector pix)
    (xlt:clear-drawable frame)
    (draw-back-labels diagram frame)
     ;;; The 255 is because zr is not centered: see fill-trig-tables
    (rotor-render (min-count diagram)
		  (+ 255 (the Hither-Yon-Coord (yon diagram)))
		  (+ 255 (the Hither-Yon-Coord (hither diagram)))
		  (ycuts diagram) (rep-hist diagram) (rep-paint diagram)
		  (yxr diagram) (zr diagram)
		  zbf pix)
    (simple-render (axis-yxr diagram) (axis-zr diagram) p zbf pix)
    (xlt:draw-inverse-transformed-image 
     frame gc (clay:diagram-image diagram)
     (clay:pixmap->image (clay:diagram-lens diagram)))
    (draw-front-labels diagram frame)))

(defmethod clay:draw-diagram ((diagram Rotor))
  "This method uses a pixmap cache if this angle has been drawn before,
and saves the result in the cache if it hasn't."
  (declare (optimize (safety 0) (speed 3))
	   (type Rotor diagram))
  (ac:atomic
   (let* ((i (truncate (angle diagram) (angular-resolution diagram)))
	  (frames (rotor-frames diagram))
	  (frame (aref frames i))
	  (win (clay:diagram-window diagram))
	  (gc (clay:diagram-gcontext diagram))
	  (x (brk:left diagram))
	  (y (brk:top diagram))
	  (w (brk:width diagram))
	  (h (brk:height diagram)))
     (declare (type Degrees i)
	      (type Pixmap-Vector frames)
	      (type (or Null xlib:Pixmap) frame)
	      (type xlib:Window win)
	      (type az:Card16 w h))
     (unless frame
       ;; calculate the new frame and save it
       (setf frame
	 (xlib:create-pixmap :drawable win :depth 8 :width w :height h))
       (clay:render-diAgram diagram frame)
       (setf (aref frames i) frame))
     ;; put up the saved frame
     (xlib:copy-area frame gc 0 0 w h win x y))))

(defmethod clay:spin-diagram ((diagram Rotor))
  (declare (type Rotor diagram))
  (let ((delta (angular-resolution diagram)))
    (loop for angle from 0 to 360 by delta do
	  (ac:send-msg 'role-set-angle diagram diagram (ac:new-id) angle)
	  (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id)))))

(defun test-spin (diagram)
  (let ((delta (angular-resolution diagram)))
    (loop for angle from 0 to 360 by delta do
	  (setf (angle diagram) angle)
	  (clay:draw-diagram diagram))))
  
(defmethod clay:update-internal ((diagram Rotor) (stroke T))
  (update-rep-paint diagram)
  (rotate-diagram diagram))

(defmethod clay:record-diagram ((diagram Rotor))
  (let* ((w (brk:width diagram))
	 (h (brk:height diagram))
	 (px (xlib:create-pixmap :drawable (clay:diagram-window diagram)
				 :depth 8 :width w :height h)))
    (declare (type az:Card16 w h)
	     (type xlib:Pixmap px))
    (xlt:draw-inverse-transformed-image
     px (clay:diagram-gcontext diagram)
     (clay:diagram-image diagram)
     (clay:pixmap->image (clay:diagram-lens diagram)))
    (push px (clay:diagram-frames diagram))))

(defmethod clay:erase-record ((diagram Rotor))
  (declare (type Rotor diagram))
  (let ((frames (rotor-frames diagram)))
    (dotimes (i (length frames))
      (when (aref frames i) (az:kill-object (aref frames i)))
      (setf (aref frames i) nil))))

;;;============================================================
;;; Input
;;;============================================================

(defmethod clay:button-2-press ((role Rotor-Role) x y)
  (declare (type Rotor-Role role)
	   (type xlib:Int16 x y))
  (give-control-to-dragger (ac:role-actor role) x y))

;;;------------------------------------------------------------
;;; Rotation
;;;------------------------------------------------------------

(defmethod ac:interactor-role-cursor ((diagram Rotor) (role Rotor-Dragger))
  "A dragger gets a hand cursor."
  (xlt:window-cursor (ac:interactor-window diagram) :exchange))
 
;;;------------------------------------------------------------

(defun give-control-to-dragger (diagram x y)
  (declare (type Rotor diagram)
	   (type xlib:Int16 x y)
	   (ignore y))
  (let ((dragger (dragger diagram)))
    (declare (type Rotor-Dragger dragger))
    (setf (x0 dragger) x)
    (setf (ac:actor-current-role diagram) dragger)))

;;;------------------------------------------------------------

(defmethod ac:motion-notify ((role Rotor-Dragger) 
			     msg-name sender receiver id
			     hint-p x y state time root root-x root-y child 
			     same-screen-p)

  "This method changes the angle of projection."

  (declare (type Rotor-Dragger role)
	   (type xlib:Boolean hint-p)
	   (type xlib:Int16 x y)
	   (type az:Card16 state)
	   (type az:CArd32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   y hint-p state time root root-x root-y child same-screen-p))
  
     ;; jump to the last motion-notify msg on the queue
    ;; (skip-to-last role 'ac:motion-notify)
    (incf (angle receiver) (* 0.5d0 (- (the xlib:Int16 (x0 role)) x)))
    (setf (x0 role) x)
    (ac:send-msg 'clay:role-draw-diagram receiver receiver (ac:new-id))
  (values t))

;;;------------------------------------------------------------
;;; Paint
;;;------------------------------------------------------------

(defclass Rotor-Stroke (clay:Stroke)
	  ((stroke-marks
	    :type (az:Card8-Matrix 256 512)
	    :accessor stroke-marks
	    :initform (az:make-card8-matrix 256 512)))
  (:documentation
   "This stroke indicates which locations in a Rotor have been painted."))

(defparameter *rotor-strokes* () "A resource of  Rotor-Stroke's")

(defmethod clay:return-stroke ((stroke Rotor-Stroke))
  (if (null (clay:stroke-objects-to-be-updated stroke))
      (push stroke *rotor-strokes*)
    ;; else
    (error "Trying to return ~a before it's done." stroke)))

(defmethod clay:mark-stroke ((stroke Rotor-Stroke) rect)
  "Rect is in diagram coordinates."
  (declare (type g:Rect rect))
  (let* ((xmin (g:x rect))
	 (xmax (g:xmax rect))
	 (ymin (g:y rect))
	 (ymax (g:ymax rect))
	 (x1 (- xmax 1))
	 (y1 (ash (- ymax 1) 9))
	 (x xmin)
	 (y (ash ymin 9))
	 (s (az:array-data (stroke-marks stroke))))
    (declare (type (Integer 0 #.(- -rotor-width- 1)) xmin)
	     (type (Integer 0 #.(- -rotor-height- 1)) ymin)
	     (type (Integer 1 #.-rotor-width-) xmax)
	     (type (Integer 1 #.-rotor-height-) ymax)
	     (type (Integer 0 512) x x1)
	     (type az:Card28 y y1)
	     (type az:Card8-Vector s))
    (unless (or (> xmin x1) (> y y1))
      (loop (loop (setf (aref s (logior y x)) 1)
	      (when (= x x1) (return))
	      (incf x))
	(when (= y y1) (return))
	(setf x xmin)
	(incf y 512)))))

;;;------------------------------------------------------------

(defclass Rotor-Painter (clay:Painter Rotor-Role) ())

(defmethod clay:initialize-painter :after ((painter Rotor-Painter))
  "Make sure it at the current angle."
  (declare (type Rotor-Painter painter))
  (rotate-diagram (ac:role-actor painter))    
  (values t))
 
(defmethod clay:borrow-stroke ((painter Rotor-Painter))
  (let ((stroke (pop *rotor-strokes*))
	(stroked-diagram (ac:role-actor painter)))
    (when (null stroke) (setf stroke (make-instance 'Rotor-Stroke)))
    ;; else clear the stroke marks
    (az:zero-card8-array (stroke-marks stroke))
    (setf (clay:stroked-diagram stroke) stroked-diagram)
    stroke))

;;;------------------------------------------------------------

(defmethod clay:stroke-object ((space Complete-Band-Space)
			       (stroke Rotor-Stroke))
  (declare (optimize (safety 0) (speed 3)))
  (with-index (index (clay:stroked-diagram stroke)) 
    (let* ((p (clay:stroke-pixel stroke))
	   (pmask (clay:stroke-pmask stroke))
	   (stroked-diagram (clay:stroked-diagram stroke))
	   (min-count (min-count stroked-diagram))
	   (yon (+ 255 (the Hither-Yon-Coord (yon stroked-diagram))))
	   (hither (+ 255 (the Hither-Yon-Coord (hither stroked-diagram))))
	   (marks (az:array-data (stroke-marks stroke)))
	   (paint (paint-vector space))
	   (yxr (yxr stroked-diagram))
	   (zr (zr stroked-diagram))
	   (rep-hist (rep-hist stroked-diagram))
	   (sorted-pins (sorted-pins index))
	   (j0 0)
	   (j1 0))
      (declare (type az:Card8 p pmask)
	       (type Rotor stroked-diagram)
	       (type az:Card28 min-count)
	       (type az:Card16 yon hither)
	       (type az:Card8-Vector marks paint)
	       (type az:Card28-Vector yxr rep-hist sorted-pins)
	       (type az:Card16-Vector zr)
	       (type az:Card28 j0 j1))
      (dotimes (i (length yxr))
	(declare (type az:Card28 i))
	(setf j1 (+ j0 (aref rep-hist i)))
	(let ((zri (aref zr i)))
	  (declare (type az:Card16 zri))
	  (when (and (= 1 (aref marks (aref yxr i)))
		     (>= (aref rep-hist i) min-count)
		     (>= zri yon)
		     (< zri hither))
	    (do ((j j0 (+ j 1)))
		((>= j j1))
	      (declare (type az:Card28 j))
	      (let ((k (aref sorted-pins j)))
		(declare (type az:Card28 k))
		(setf (aref paint k) (xlt:paint-wash (aref paint k) p pmask)))
	      ))
	  (setf j0 j1))))))

(defmethod clay:stroke-object ((space Band-Space-Bipart)
			       (stroke Rotor-Stroke))

  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type Rotor-Stroke stroke))
  
  (with-index (index (clay:stroked-diagram stroke)) 
    (let* ((stroked-diagram (clay:stroked-diagram stroke))
	   (p (clay:stroke-pixel stroke))
	   (pmask (clay:stroke-pmask stroke))
	   (min-count (min-count stroked-diagram))
	   (yon (+ 255 (the Hither-Yon-Coord (yon stroked-diagram))))
	   (hither (+ 255 (the Hither-Yon-Coord (hither stroked-diagram))))
	   (marks (az:array-data (stroke-marks stroke)))
	   (paint (paint-vector space))
	   (yxr (yxr stroked-diagram))
	   (zr (zr stroked-diagram))
	   (rep-hist (rep-hist stroked-diagram))
	   (sorted-pins (sorted-pins index))
	   (key (key space))
	   (mask (mask space))
	   (part (part-vector space))
	   (j0 0) (j1 0))
      (declare (type Rotor stroked-diagram)
	       (type az:Card8 p pmask)
	       (type az:Card28 min-count j0 j1)
	       (type az:Card16 yon hither)
	       (type az:Card8-Vector marks paint)
	       (type az:Card28-Vector yxr rep-hist sorted-pins)
	       (type az:Card16-Vector zr)
	       (type az:Card16 key mask)
	       (type az:Card16-Vector part))
      (dotimes (i (length yxr))
	(declare (type az:Card28 i))
	(setf j1 (+ j0 (aref rep-hist i)))
	(let ((zri (aref zr i)))
	  (declare (type az:Card16 zri))
	  (when (and (= 1 (aref marks (aref yxr i)))
		     (>= (aref rep-hist i) min-count)
		     (>= zri yon)
		     (< zri hither))
	    (do ((j j0 (+ j 1)))
		((>= j j1))
	      (declare (type az:Card28 j))
	      (let ((pin (aref sorted-pins j)))
		(declare (type az:Card28 pin))
		(when (= key (logand mask (aref part pin)))
		  (setf (aref paint pin)
		    (xlt:paint-wash (aref paint pin) p pmask))))))
	  (setf j0 j1))))))

;;;------------------------------------------------------------

(defmethod (setf angle) ((new-angle Number) (diagram Rotor))
  (setf new-angle (mod new-angle 360))
  (unless (= new-angle (angle diagram))
    (ac:atomic
     (setf (slot-value diagram 'angle) new-angle)
     (unless (aref (rotor-frames diagram)
		   (truncate (angle diagram) (angular-resolution diagram)))
       ;; if there's a saved frame then the rotation has already be calculated
       (rotate-diagram diagram))))
  new-angle)

(defmethod (setf min-count) ((new-count Integer) (diagram Rotor))
  (declare (type az:Card28 new-count))
  (let ((old-count (min-count diagram)))
    (unless (= new-count old-count)
      (setf (slot-value diagram 'min-count) new-count)
      (clay:erase-record diagram)
      (when (< new-count old-count)
	;; then need to calculate rotated coordinates for thinned data
	(rotate-diagram diagram))
      (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id))))
  new-count)

(defmethod (setf yon) ((new-yon Integer) (diagram Rotor))
  (declare (type Hither-Yon-Coord new-yon))
  (unless (= new-yon (yon diagram))
    (setf (slot-value diagram 'yon) new-yon)
    (clay:erase-record diagram)
    (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id)))
  new-yon)

(defmethod (setf hither) ((new-hither Integer) (diagram Rotor))
  (declare (type Hither-Yon-Coord new-hither))
  (unless (= new-hither (hither diagram))
    (setf (slot-value diagram 'hither) new-hither)
    (clay:erase-record diagram)
    (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id)))
  new-hither)

(defun set-z-cuts (diagram yon hither)
  (declare (type Rotor diagram)
	   (type Hither-Yon-Coord yon hither))
  (setf (slot-value diagram 'yon) yon)
  (setf (slot-value diagram 'hither) hither)
  (clay:erase-record diagram)
  (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id))
  (values yon hither))

(defmethod (setf axis-yxz) ((new-yxz Vector) (diagram Rotor))
  (ac:atomic
   (setf (slot-value diagram 'axis-yxz) new-yxz)
   (setf (slot-value diagram 'axis-yxr)
     (az:make-card24-vector (length new-yxz)))
   (setf (slot-value diagram 'axis-zr)
     (az:make-card16-vector (length new-yxz)))
   (clay:erase-record diagram)
   (rotate-diagram diagram)
   (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id))
   new-yxz))

(defmethod (setf label-yxz) ((new-yxz Vector) (diagram Rotor))
  (ac:atomic
   (setf (slot-value diagram 'label-yxz) new-yxz)
   (setf (slot-value diagram 'label-yxr)
     (az:make-card24-vector (length new-yxz)))
   (setf (slot-value diagram 'label-zr)
     (az:make-card16-vector (length new-yxz)))
   (clay:erase-record diagram)
   (rotate-diagram diagram)
   (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id))
   new-yxz))
  
