;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defclass General-Scattergram (Diagram)
	  ((rep-hist
	    :type az:Card28-Vector
	    :accessor rep-hist
	    :documentation
	    "How many pixels does each rep stand for?")
	   (rep-speckle
	    :type az:Card8-Vector
	    :accessor rep-speckle
	    :documentation
	    "Random brightness variaqtion for enhanced 3d perception.")
	   (rep-maxcount
	    :type az:Card28
	    :accessor rep-maxcount
	    :documentation
	    "The maximum number of pixels any rep stands for?")
	   (nreps
	    :type az:Card28
	    :accessor nreps
	    :initform 0
	    :documentation "How many representative pixels are used?")
	   (rep-pins
	    :type az:Card28-Vector
	    :accessor rep-pins
	    :documentation "Pins of the representative pixels.")
	   (rep-paint
	    :type az:Card8-Vector
	    :accessor rep-paint
	    :documentation "Paints for the representative pixels."))
  (:documentation
   "This is an abstract base class for any instantiable
n-dimensional scatter plot diagram class."))

;;;============================================================
;;; Layout
;;;============================================================

(defgeneric update-rep-paint (diagram)
  (declare (type General-Scattergram diagram))
  (:documentation
   "Update the paint cache for the representative points."))

(defmethod update-rep-paint ((diagram General-Scattergram))
  (declare (optimize (safety 0) (speed 3))
	   (type General-Scattergram diagram))
  (let ((rep-paint (rep-paint diagram))
	(rep-pins (rep-pins diagram))
	(p (paint-vector (band-space diagram))))
    (declare (type az:Card8-Vector rep-paint p)
	     (type az:Card28-Vector rep-pins))
    (dotimes (i (the az:Card28 (length rep-paint)))
      (declare (type az:Card28 i))
      (setf (aref rep-paint i) (aref p (aref rep-pins i))))))
 
;;;============================================================

(defclass Scatgram (General-Scattergram) ())

;;;------------------------------------------------------------

(defun make-scatgram (band1 band0
		      &key
		      (host (xlt:default-host))
		      (display (ac:host-display host))
		      (screen (xlib:display-default-screen display))
		      (band-space (band-space band1))
		      (left 0) (top 0) (width nil) (height nil))
  (declare (type Band band1 band0)
	   (type Simple-String host)
	   (type xlib:Display display)
	   (type xlib:Screen screen)
	   (type Band-Space band-space)
	   (type xlib:Card16 left top)
	   (type (or Null az:Card16) width height))
  (assert (eq (band-space band1) (band-space band0)))
  (let* ((name (format nil "S[~a,~a]~a"
		       (name band1) (name band0) (name band-space)))
	 (window (xlt:make-window
		  :parent (xlib:screen-root screen)
		  :colormap (xlt:paint-colormap (xlib:screen-root screen))
		  :left left :top top :width width :height height))
	 (root (make-instance 'clay:Plot-Root
		 :diagram-window window
		 :diagram-name name))
	 (plot (make-instance 'Scatgram
		 :diagram-subject band-space
		 :diagram-parent root
		 :diagram-name name
		 :band1 band1
		 :band0 band0)))
    (declare (type xlib:Window window)
	     (type Simple-String name)
	     (type clay:Plot-Root root))
    (when (or width height)
      (setf (clay:scale (clay:diagram-lens plot)) nil))
    (clay:build-diagram plot nil)
    (clay:initialize-diagram
     root
     :build-options (list :plot-child plot)
     :mediator (mediator (root-space band-space)))))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Scatgram))
  (setf (clay:diagram-painter diagram)
    (make-instance 'Scat-Painter :role-actor diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Role :role-actor diagram)))

(defmethod clay:build-diagram :after ((diagram Scatgram) options)
  "The default diagram rect is not very interesting."
  (declare (ignore options))
  (setf (clay:diagram-image diagram)
    (xlib:create-image
     :data (az:make-card8-matrix 256 256 :initial-element #b00000001)
     :format :z-pixmap :depth 8 :height 256 :width 256)))

;;;============================================================
;;; Layout
;;;============================================================

;;;============================================================
;;; Drawing
;;;============================================================

;;;============================================================
;;; Input
;;;===========================================================
 
(defclass Scat-Stroke (clay:Stroke)
	  ((stroke-marks
	    :type (az:Card8-Matrix 256 256)
	    :accessor stroke-marks
	    :initform (az:make-card8-matrix 256 256)))
  (:documentation
   "This stroke carries an array that indicates which
locations in a scatterplot (really 2d yxhist) have been painted."))

(defparameter *scat-strokes* () "A resource of  Scat-Stroke's")

(defmethod clay:return-stroke ((stroke Scat-Stroke))
  (if (null (clay:stroke-objects-to-be-updated stroke))
      (push stroke *scat-strokes*)
    ;; else
    (error "Trying to return ~a before it's done." stroke)))

(defmethod clay:mark-stroke ((stroke Scat-Stroke) rect)
  (declare (type g:Rect rect))
  (let* ((xmin (g:x rect))
	 (xmax (g:xmax rect))
	 (ymin (g:y rect))
	 (ymax (g:ymax rect))
	 (x1 (- xmax 1))
	 (y1 (ash ymax 8))
	 (x xmin)
	 (y (ash ymin 8))
	 (sdv (az:array-data (stroke-marks stroke))))
    (declare (type az:Card8 x x1)
	     (type az:Card16 xmin xmax ymin ymax y y1)
	     (type (az:Card8-Vector 65536) sdv))
    (unless (or (> xmin x1) (> y y1))
      (loop
	(loop
	  (setf (aref sdv (logior y x)) 1)
	  (when (= x x1) (return))
	  (incf x))
	(when (= y y1) (return))
	(setf x xmin)
	(incf y 256)))))

;;;============================================================

(defclass Scat-Painter (clay:Painter) ())

(defmethod clay:borrow-stroke ((painter Scat-Painter))
  (let ((stroke (pop *scat-strokes*))
	(stroked-diagram (ac:role-actor painter)))
    (when (null stroke) (setf stroke (make-instance 'Scat-Stroke)))
    ;; else clear the stroke marks
    (az:zero-card8-array (stroke-marks stroke))
    (setf (clay:stroked-diagram stroke) stroked-diagram)
    stroke))
 
;;;============================================================

(defmethod fill-scatimage ((space Complete-Band-Space)
			   (diagram Scatgram)
			   (stroke T))
  (declare (optimize (safety 0) (speed 3))
	   (type Complete-Band-Space space)
	   (type Scatgram diagram)
	   (type T stroke))
  (let ((x (band-vector (band1 diagram)))
	(y (band-vector (band0 diagram)))
	(p (paint-vector (root-space space)))
	(iv (xlt:image-pixel-vector (clay:diagram-image diagram))))
    (declare (type az:Card8-Vector x y p iv))
    (dotimes (i (length p))
      (declare (type az:Card28 i))
      (setf (aref iv (ppref y x i)) (aref p i)))))

;;;--------------------------------------------------------------

(defmethod fill-scatimage ((space Band-Space-Bipart)
			   (diagram Scatgram)
			   (stroke T))
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type Scatgram diagram)
	   (type T stroke))
  (let ((x (band-vector (band1 diagram)))
	(y (band-vector (band0 diagram)))
	(p (paint-vector (root-space space)))
	(iv (xlt:image-pixel-vector (clay:diagram-image diagram)))
	(key (key space))
	(mask (mask space))
	(part (part-vector space)))
    (declare (type az:Card8-Vector x y p iv)
	     (type az:Card16 key mask)
	     (type az:Card16-Vector part))
    (dotimes (i (length p))
      (declare (type az:Card28 i))
      (when (= key (logand mask (aref part i)))
	(setf (aref iv (ppref y x i)) (aref p i))))))

;;;--------------------------------------------------------------

(defmethod fill-scatimage ((space Complete-Band-Space)
			   (diagram Scatgram)
			   (stroke Rectangles-Stroke))
  
  (declare (optimize (safety 0) (speed 3))
	   (type Complete-Band-Space space)
	   (type Scatgram diagram)
	   (type Rectangles-Stroke stroke))
  
  (let* ((x (band-matrix (band1 diagram)))
	 (y (band-matrix (band0 diagram)))
	 (im (xlib:image-z-pixarray (clay:diagram-image diagram)))
	 (rectangles (stroked-rectangles stroke))
	 (p (clay:stroke-pixel stroke)))
    (declare (type az:Card8-Matrix x y im)
	     (type List rectangles)
	     (type az:Card8 p))
    (dolist (r rectangles)
      (declare (type g:Rect r))
      (let* ((xmin (g:x r))
	     (ymin (g:y r))
	     (xmax (g:xmax r))
	     (ymax (g:ymax r)))
	(declare (type az:Card16 xmin xmax ymin ymax))
	(do ((i ymin (+ i 1)))
	    ((>= i ymax))
	  (declare (type az:Card16 i))
	  (do ((j xmin (+ j 1)))
	      ((>= j xmax))
	    (declare (type az:Card16 j))
	    (setf (aref im (aref y i j) (aref x i j)) p)))))))

;;;--------------------------------------------------------------

(defmethod fill-scatimage ((space Band-Space-Bipart)
			   (diagram Scatgram)
			   (stroke Rectangles-Stroke))
  
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type Scatgram diagram)
	   (type Rectangles-Stroke stroke))
  
  (let* ((x (band-matrix (band1 diagram)))
	 (y (band-matrix (band0 diagram)))
	 (im (xlib:image-z-pixarray (clay:diagram-image diagram)))
	 (rectangles (stroked-rectangles stroke))
	 (pixel (clay:stroke-pixel stroke))
	 (key (key space))
	 (mask (mask space))
	 (part (part-matrix space)))
    (declare (type az:Card8-Matrix x y im)
	     (type List rectangles)
	     (type az:Card8 pixel)
	     (type az:Card16 key mask)
	     (type az:Card16-Matrix part))
    (dolist (r rectangles)
      (declare (type g:Rect r))
      (let* ((xmin (g:x r))
	     (ymin (g:y r))
	     (xmax (g:xmax r))
	     (ymax (g:ymax r)))
	(declare (type az:Card16 xmin xmax ymin ymax))
	(do ((i ymin (+ i 1))) ((>= i ymax))
	  (declare (type az:Card16 i))
	  (do ((j xmin (+ j 1))) ((>= j xmax))
	    (declare (type az:Card16 j))
	    (when (= key (logand mask (aref part i j)))
	      (setf (aref im (- 255 (aref y i j)) (aref x i j)) pixel))))))))

;;;---------------------------------------------------------------

(defmethod fill-scatimage ((space Complete-Band-Space)
			   (diagram Scatgram)
			   (stroke Scat-Stroke))
  
  (declare (optimize (safety 0) (speed 3))
	   (type Complete-Band-Space space)
	   (type Scatgram diagram)
	   (type Scat-Stroke stroke)) 

  (with-index (index (clay:stroked-diagram stroke))
    (let* ((x (band-vector (band1 diagram)))
	   (y (band-vector (band0 diagram)))
	   (im (xlt:image-pixel-vector (clay:diagram-image diagram)))
	   (s (az:array-data (stroke-marks stroke)))
	   (pixel (clay:stroke-pixel stroke))
	   (po (az:array-data (2band-integram (band-space index)
					      (band0 index) (band1 index))))
	   (sorted-pins (sorted-pins index))
	   (p0 0)
	   (p1 0))
      (declare (type az:Card8-Vector x y im s)
	       (type az:Card8 pixel)
	       (type Index index)
	       (type az:Card28-Vector po sorted-pins)
	       (type az:Card28 p0 p1))
      (dotimes (i 65536)
	(declare (type (Integer 0 65536) i))
	(setf p1 (aref po i))
	(when (and (> p1 p0) (= 1 (aref s i)))
	  (do ((k p0 (+ k 1)))
	      ((>= k p1))
	    (declare (type az:Card28 k))
	    (let ((pin (aref sorted-pins k)))
	      (declare (type az:Card28 pin))
	      (setf (aref im (ppref y x pin)) pixel))))
	(setf p0 p1)))))

;;;---------------------------------------------------------------

(defmethod fill-scatimage ((space Band-Space-Bipart)
			   (diagram Scatgram)
			   (stroke Scat-Stroke))
  
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type Scatgram diagram)
	   (type Scat-Stroke stroke)) 
  (with-index (index (clay:stroked-diagram stroke))
    (let* ((x (band-vector (band1 diagram)))
	   (y (band-vector (band0 diagram)))
	   (im (xlt:image-pixel-vector (clay:diagram-image diagram)))
	   (s (az:array-data (stroke-marks stroke)))
	   (pixel (clay:stroke-pixel stroke))
	   (po (az:array-data (2band-integram (band-space index)
					      (band0 index) (band1 index))))
	   (sorted-pins (sorted-pins index))
	   (p0 0)
	   (p1 0)
	   (key (key space))
	   (mask (mask space))
	   (part (part-vector space)))
      (declare (type az:Card8-Vector x y im s)
	       (type az:Card8 pixel)
	       (type Index index)
	       (type az:Card28-Vector po sorted-pins)
	       (type az:Card28 p0 p1)
	       (type az:Card16 key mask)
	       (type az:Card16-Vector part))
      (dotimes (i 65536)
	(declare (type (Integer 0 65536) i))
	(setf p1 (aref po i))
	(when (and (> p1 p0) (= 1 (aref s i)))
	  (do ((k p0 (+ k 1)))
	      ((>= k p1))
	    (declare (type az:Card28 k))
	    (let ((pin (aref sorted-pins k)))
	      (declare (type az:Card28 pin))
	      (when (= key (logand mask (aref part pin)))
		(setf (aref im (ppref y x pin)) pixel)))))
	(setf p0 p1)))))

;;;--------------------------------------------------------------

(defmethod clay:update-internal ((diagram Scatgram) (stroke T))
  (fill-scatimage (band-space diagram) diagram stroke))

;;;============================================================

(defmethod clay:stroke-object ((diagram Scatgram) (stroke clay:Stroke))
  (clay:update-internal diagram stroke)
  (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id))
  (ac:atomic
   (az:deletef diagram (clay:stroke-objects-to-be-updated stroke))
   (when (null (clay:stroke-objects-to-be-updated stroke))
     (clay:return-stroke stroke)))
  (values t))

;;;------------------------------------------------------------

(defmethod clay:stroke-object ((space Complete-Band-Space)
			       (stroke Scat-Stroke))

  (declare (optimize (safety 0) (speed 3)))
  
  (with-index (index (clay:stroked-diagram stroke))
    (let ((p (clay:stroke-pixel stroke))
	  (pmask (clay:stroke-pmask stroke))
	  (st (az:array-data (stroke-marks stroke)))
	  (pa (paint-vector space))
	  (po (az:array-data
	       (2band-integram space (band0 index) (band1 index))))
	  (sorted-pins (sorted-pins index))
	  (p0 0)
	  (p1 0))
      (declare (type az:Card8 p pmask)
	       (type az:Card8-Vector st pa)
	       (type az:Card28-Vector po sorted-pins)
	       (type az:Card28 p0 p1))
      (dotimes (i 65536)
	(declare (type (Integer 0 65536) i))
	(setf p1 (aref po i))
	(when (and (> p1 p0) (= 1 (aref st i)))
	  (do ((k p0 (+ k 1)))
	      ((>= k p1))
	    (declare (type az:Card28 k))
	    (let ((pin (aref sorted-pins k)))
	      (declare (type az:Card28 pin))
	      (setf (aref pa pin) (xlt:paint-wash (aref pa pin) p pmask)))))
	(setf p0 p1)))))

;;;------------------------------------------------------------

(defmethod clay:stroke-object ((space Band-Space-Bipart)
			       (stroke Scat-Stroke))

  (declare (optimize (safety 0) (speed 3)))
  
  (with-index (index (clay:stroked-diagram stroke))
    (let ((p (clay:stroke-pixel stroke))
	  (pmask (clay:stroke-pmask stroke))
	  (st (az:array-data (stroke-marks stroke)))
	  (pa (paint-vector space))
	  (key (key space))
	  (mask (mask space))
	  (part (part-vector space))
	  (po (az:array-data
	       (2band-integram space (band0 index) (band1 index))))
	  (sorted-pins (sorted-pins index))
	  (p0 0) (p1 0))
      (declare (type az:Card8 p pmask)
	       (type az:Card8-Vector st pa)
	       (type az:Card16 key mask)
	       (type az:Card16-Vector part)
	       (type az:Card28-Vector po sorted-pins)
	       (type az:Card28 p0 p1))
      (dotimes (i 65536)
	(declare (type (Integer 0 65536) i))
	(setf p1 (aref po i))
	(when (and (> p1 p0) (= 1 (aref st i)))
	  (do ((k p0 (+ k 1)))
	      ((>= k p1))
	    (declare (type az:Card28 k))
	    (let ((pin (aref sorted-pins k)))
	      (declare (type az:Card28 pin))
	      (when (= key (logand mask (aref part pin)))
		(setf (aref pa pin) (xlt:paint-wash (aref pa pin) p pmask))))))
	(setf p0 p1)))))


 
