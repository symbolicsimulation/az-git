;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defclass Image-Node (lay:Node-Diagram Imagegram) ()
  (:documentation
   "This is used for graph browsers in which each node displays an image."))

;;;------------------------------------------------------------

(defmethod print-object ((node Image-Node) stream)
  (declare (type Stream stream))
  (az:printing-random-thing
   (node stream)
   (format stream "ImageNode ~a" (clay:diagram-subject node))))

;;;------------------------------------------------------------

(defun make-image-node (subject parent)
  (declare (type Band-Space subject)
	   (type lay:graph-Diagram parent))
  (let* ((scale (min (/ 192 (nrows subject))
		     (/ 192 (ncols subject))))
	 (nodegram (make-instance 'Image-Node
		     :diagram-subject subject
		     :diagram-parent parent)))
    (declare (type Image-Node nodegram))
    (setf (clay:scale (clay:diagram-lens nodegram)) scale)
    (clay:build-diagram nodegram nil)
    (ac:start-msg-handling nodegram)
    nodegram))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod clay:diagram-minimum-width ((diagram Image-Node))
  (+ (* 2 (clay:button-margin diagram))
     (if (clay:scale (clay:diagram-lens diagram))
       (floor (* (clay:scale (clay:diagram-lens diagram))
		 (g:w (clay:diagram-image-rect diagram))))
       ;; else
       2)))

(defmethod clay:diagram-minimum-height ((diagram Image-Node))
  (+ (* 2 (clay:button-margin diagram))
     (if (clay:scale (clay:diagram-lens diagram))
	 (floor (* (clay:scale (clay:diagram-lens diagram))
		   (g:h (clay:diagram-image-rect diagram))))
       ;; else
       2)))
 
;;;--------------------------------------------------------------

(defmethod clay:layout-diagram ((diagram Image-Node) layout-options)
  
  (declare (type List layout-options)
	   (ignore layout-options))

  (clay:ensure-big-enough-pixmaps diagram)
  
  (let ((lens (clay:diagram-lens diagram)))
    (setf (clay:window->pixmap lens) (make-window->pixmap diagram))
    (setf (clay:pixmap->image lens) (make-pixmap->image diagram)))
  (clay:update-internal diagram nil)
  diagram)

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod clay:diagram-update-pixmaps ((image-node Image-Node))
  (declare (type Image-Node image-node))
  (clay:draw-button-up-pixmap image-node)
  (clay:draw-button-down-pixmap image-node)
  (clay:draw-button-flat-pixmap image-node))

;;;------------------------------------------------------------

(defmethod clay:draw-button-up-pixmap ((diagram Image-Node))
  (declare (type Image-Node diagram))
  (clay:render-diagram diagram (clay:button-up-pixmap diagram)))

;;;------------------------------------------------------------

(defmethod clay:draw-button-down-pixmap ((diagram Image-Node))
  (declare (type Image-Node diagram))
  (clay:render-diagram diagram (clay:button-down-pixmap diagram)))

;;;------------------------------------------------------------

(defmethod clay:draw-button-flat-pixmap ((diagram Image-Node))
  (declare (type Image-Node diagram))
  (clay:render-diagram diagram (clay:button-flat-pixmap diagram)))

;;;------------------------------------------------------------

(defmethod clay:draw-diagram ((diagram Image-Node))
  (declare (type Image-Node diagram))
  (let ((r (clay:diagram-window-rect diagram)))
    (declare (type g:Rect r))
    (xlib:copy-area
     (clay:diagram-pixmap diagram) (clay:diagram-gcontext diagram)
     0 0 (g:w r) (g:h r)
     (clay:diagram-window diagram)
     (g:x r) (g:y r))))
 
;;;============================================================
;;; Input
;;;============================================================

(defmethod clay:activate-diagram ((diagram Image-Node) (mediator T))
  (clay:initialize-paint-dependency
   diagram (mediator (root-space (clay:diagram-subject diagram)))))

(defmethod clay:stroke-object ((diagram Image-Node) (stroke clay:Stroke))
  (clay:update-internal diagram stroke)
  (ac:send-msg 'clay:role-redraw-diagram diagram diagram (ac:new-id))
  (ac:atomic
   (az:deletef diagram (clay:stroke-objects-to-be-updated stroke))
   (when (null (clay:stroke-objects-to-be-updated stroke))
     (clay:return-stroke stroke))))

;;;============================================================

(defclass Partition-Browser (lay:graph-Diagram) ())

;;;------------------------------------------------------------

(defmethod clay:build-children ((diagram Partition-Browser))
  (let* ((subject (clay:diagram-subject diagram))
	 (snodes (gra:nodes subject))
	 (sedges (gra:edges subject)))
    ;; make diagrams for the nodes in the graph
    (setf (gra:nodes diagram)
      (az:with-collection
	  (dolist (snode snodes)
	    (az:collect (make-image-node snode diagram)))))
    (lay:graph-diagram-index-nodes diagram)

    ;; make diagrams for the edges in the graph
    ;; and recreate the graph structure
    ;; in the diagram nodes and edges
    (setf (gra:edges diagram)
      (az:with-collection
	  (dolist (sedge sedges)
	    (az:collect (lay:make-edge-diagram sedge diagram)))))
    (lay:graph-diagram-index-edges diagram)))

;;;------------------------------------------------------------

(defun make-part-browser (subject
			  &key
			  (host (xlt:default-host))
			  (display (ac:host-display host))
			  (screen (xlib:display-default-screen display))
			  (left 0)
			  (top 0)
			  (width 700)
			  (height 700)
			  (order-f 'superspace?))
  
  "Make a graph browser for a set of classes."
  
  (declare (type Partitioning subject)
	   (:returns (type Partition-Browser)))
  
  (let* ((window (xlt:make-window
		  :parent (xlib:screen-root screen)
		  :colormap (xlt:paint-colormap (xlib:screen-root screen))
		  :left left :top top :width width :height height))
	 (diagram (make-instance 'Partition-Browser
		    :diagram-name (name subject)
		    :diagram-subject subject
		    :diagram-window window)))
    (declare (type xlib:Window window)
	     (type Partition-Browser diagram))
    (clay:initialize-diagram
     diagram
     :layout-options
     (list :initial-node-configuration nil
	   :x-order-f nil
	   :y-order-f order-f
	   :node-pair-distance-f nil
	   :rest-length nil)
     :mediator (lay:graph-mediator subject))))

