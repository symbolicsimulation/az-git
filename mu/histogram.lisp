;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(eval-when (compile load eval)
  (defconstant -painthist-length- (* (ash 1 xlt:-paint-hs-bits-) 256)))

(deftype Painthist-Vector () '(az:Card28-Vector #.-painthist-length-))

(defclass Histogram (Diagram)
	  ((painthist
	    :type Painthist-Vector
	    :accessor painthist
	    :initform (az:large-allocation
		       (az:make-card28-vector -painthist-length-)))
	   (maxcount
	    :type Fixnum
	    :initform -1)
	   (y-transform
	    :type (or Null az:Funcallable)
	    :reader y-transform
	    :initarg :y-transform)
	   (gc-vec
	    :type Simple-Vector
	    :reader gc-vec
	    :initform (make-array xlt:-paint-hs-values-)))
  
  (:default-initargs :y-transform nil))

;;;============================================================

(defun maxcount (diagram)
  "Lazy evaluation of maxcount slot."
  (declare (type Histogram diagram))
  (let ((maxcount (slot-value diagram 'maxcount)))
    (declare (type Fixnum maxcount))
    (when (minusp maxcount)
      (setf maxcount
	(if (y-transform diagram)
	    (floor
	     (funcall
	      (y-transform diagram)
	      (az:max-card28-vector
	       (band-histogram (band-space diagram) (band0 diagram)))))
	  ;; else
	  (az:max-card28-vector
	   (band-histogram (band-space diagram) (band0 diagram)))))
      (setf (slot-value diagram 'maxcount) maxcount))
    maxcount))

;;;------------------------------------------------------------

(defgeneric compute-painthist (space band painthist)

  (declare (type Band-Space space)
	   (type Band band)
	   (type Painthist-Vector painthist))

  (:documentation
   "Computes a 2d 8x256 histogram of 3bit paint color cross 
an 8 bit image band, for the pixels in the space."))

(defmethod compute-painthist ((space Complete-Band-Space) band painthist)
  (declare (optimize (safety 0) (speed 3))
	   (type Complete-Band-Space space)
	   (type Band band)
	   (type Painthist-Vector painthist))
  (let ((p (paint-vector space))
	(b (band-vector band)))
    (declare (type az:Card8-Vector p b))
    (az:zero-card28-array painthist)
    (dotimes (i (length p))
      (declare (type az:Card28 i))
      (incf (aref painthist (painthist-index p b i))))))

(defmethod compute-painthist ((space Band-Space-Bipart) band painthist)
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type Band band)
	   (type Painthist-Vector painthist))
  (let ((p (paint-vector space))
	(b (band-vector band))
	(key (key space))
	(mask (mask space))
	(part (part-vector space)))
    (declare (type az:Card8-Vector p b)
	     (type az:Card16 key mask)
	     (type az:Card16-Vector part))
    (az:zero-card28-array painthist)
    (dotimes (i (length p))
      (declare (type az:Card28 i))
      (when (= key (logand mask (aref part i)))
	(incf (aref painthist (painthist-index p b i)))))))

;;;============================================================

(defun make-histogram (band
		       &key
		       (host (xlt:default-host))
		       (display (ac:host-display host))
		       (screen (xlib:display-default-screen display))
		       (band-space (band-space band))
		       (left 0) (top 0) (width nil) (height nil))
  (declare (type Band band)
	   (type Simple-String host)
	   (type xlib:Display display)
	   (type xlib:Screen screen)
	   (type Band-Space band-space)
	   (type xlib:Card16 left top)
	   (type (or Null xlib:Card16) width height))
  (let* ((name (format nil "H[~a]~a" (name band) (name band-space)))
	 (window (xlt:make-window
		  :parent (xlib:screen-root screen)
		  :colormap (xlt:paint-colormap (xlib:screen-root screen))
		  :left left :top top :width width :height height))
	 (root (make-instance 'clay:Plot-Root
		 :diagram-window window
		 :diagram-name name))
	 (plot (make-instance 'Histogram
		 :diagram-subject band-space
		 :diagram-parent root
		 :diagram-name name
		 :band0 band)))
    (declare (type xlib:Window window)
	     (type Simple-String name)
	     (type clay:Plot-Root root))
    (when (or width height)
      (setf (clay:scale (clay:diagram-lens plot)) nil))
    (clay:build-diagram plot nil)
    (clay:initialize-diagram
     root
     :build-options (list :plot-child plot)
     :mediator (mediator (root-space band-space)))))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Histogram))
  (setf (clay:diagram-painter diagram)
    (make-instance 'Histo-Painter :role-actor diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Role :role-actor diagram)))

(defmethod clay:build-diagram :after ((diagram Histogram) build-options)
  "The default diagram rect is not very interesting."
  (declare (ignore build-options))
  (let ((r (clay:diagram-view-rect diagram))
	(win (clay:diagram-window diagram))
	(gc-vec (gc-vec diagram)))
    (declare (type g:Rect r)
	     (type xlib:Window win)
	     (type Simple-Vector gc-vec))
    (setf (g:x r) 0.0d0)
    (setf (g:y r) 0.0d0)
    (setf (g:w r) 256.0d0)
    (setf (g:h r) (az:fl (maxcount diagram)))
    (dotimes (p xlt:-paint-hs-values-)
      (setf (aref gc-vec p) (xlt:pixel-default-gcontext win (xlt:hs p))))))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod clay:diagram-minimum-width ((diagram Histogram))
  (max 256
       (if (clay:scale (clay:diagram-lens diagram))
	 (floor
	  (* (clay:scale (clay:diagram-lens diagram))
	     (g:w (clay:diagram-view-rect diagram))))
	 ;; else
	 1)))

(defmethod clay:make-layout-constraints ((diagram Histogram)
					 (solver clay:Layout-Solver))
  (nconc
   (when (clay:scale (clay:diagram-lens diagram))
     (brk:fixed (clay:layout-domain solver)
		:width
		(* (clay:scale (clay:diagram-lens diagram))
		   (g:w (clay:diagram-view-rect diagram)))
		diagram))
   (brk:minimum-size (clay:layout-domain solver)
		     (clay:diagram-minimum-width diagram)
		     (clay:diagram-minimum-height diagram)
		     diagram)))
 
;;;--------------------------------------------------------------

(defmethod clay:update-internal ((diagram Histogram) (stroke T))
  (compute-painthist (band-space diagram) (band0 diagram)
		     (painthist diagram)))

;;;--------------------------------------------------------------

(defmethod clay:layout-diagram ((diagram Histogram) layout-options)
  (declare (type List layout-options)
	   (ignore layout-options))
  
  (clay:ensure-big-enough-pixmaps diagram)
  (let ((lens (clay:diagram-lens diagram)))
    (setf (clay:window->pixmap lens) (make-window->pixmap diagram))
    (setf (clay:pixmap->view lens) (make-pixmap->view diagram)))
  (clay:update-internal diagram nil)
  diagram)
 
;;;============================================================
;;; Drawing
;;;============================================================

(defmethod clay:render-diagram ((diagram Histogram) (drawable xlib:Drawable))
  (declare (optimize (safety 0) (speed 3))
	   (type Histogram diagram))
  (xlt:draw-rect drawable (clay:diagram-erasing-gcontext diagram)
		 (if (eq drawable (clay:diagram-window diagram))
		   (clay:diagram-window-rect diagram)
		   ;; else
		   (clay:diagram-pixmap-rect diagram))
		 t)
  (let* ((painthist (painthist diagram))
	 (y-transform (y-transform diagram))
	 (map (clay:pixmap->view (clay:diagram-lens diagram)))
	 (bar (g:make-rect (g:codomain map)))
	 (wbar (g:make-rect (xlt:drawable-space drawable)))
	 (gc-vec (gc-vec diagram)))
    (declare (type az:Card28-Vector painthist)
	     (type (or Null az:Funcallable) y-transform)
	     (type g:Int-Grid-Flip map)
	     (type g:Rect bar)
	     (type g:Rect wbar))
    (setf (g:w bar) 1.0d0)
    (dotimes (i 256)
      (declare (type (Integer 0 256) i))
      (let ((y 0))
	(declare (type xlib:Card16 y))
	(do ((j (+ i 256) (+ j 256))
	     (p 1 (+ p 1)))
	    ((>= p xlt:-paint-hs-values-))
	  (declare (type az:Card16 j p))
	  (locally (declare (type az:Card16 j)
			    (type xlt::HSI p))
	    (let ((count (aref painthist j))
		  (gc (aref gc-vec p)))
	      (declare (type az:Card28 count)
		       (type xlib:Gcontext gc))
	      (unless (zerop count)
		(when y-transform (setf count (funcall y-transform count)))
		(setf (g:x bar) (az:fl i))
		(setf (g:y bar) (az:fl y))
		(setf (g:h bar) (az:fl count))
		(g:inverse-transform map bar :result wbar)
		(xlt:draw-rect drawable gc wbar t)
		(incf y count)))))))))

;;;===========================================================
;;; Input
;;;===========================================================

(defclass Histo-Stroke (clay:Stroke)
	  ((stroke-marks
	    :type (az:Card8-Vector 256)
	    :accessor stroke-marks
	    :initform (az:make-card8-vector 256)))
  (:documentation
   "This stroke carries an array that indicates which
locations in a histogram have been painted."))

;;;------------------------------------------------------------

(defparameter *histo-strokes* () "A resource of  Histo-Stroke's")

(defmethod clay:return-stroke ((stroke Histo-Stroke))
  (if (null (clay:stroke-objects-to-be-updated stroke))
      (push stroke *histo-strokes*)
    ;; else
    (error "Trying to return ~a before it's done." stroke)))

(defmethod clay:mark-stroke ((stroke Histo-Stroke) rect)
  "Rect is in diagram coordinates."
  (declare (type g:Rect rect))
  (let ((xmin (max 0 (floor (g:x rect))))
	(xmax (min 256 (ceiling (g:xmax rect)))))
    (declare (type ))
    (az:for (i xmin xmax) (setf (aref (stroke-marks stroke) i) 1))))
 
;;;============================================================

(defclass Histo-Painter (clay:Painter) ())

;;;------------------------------------------------------------

(defmethod clay:borrow-stroke ((painter Histo-Painter))
  (let ((stroke (pop *histo-strokes*))
	(stroked-diagram (ac:role-actor painter)))
    (when (null stroke) (setf stroke (make-instance 'Histo-Stroke)))
    ;; else clear the stroke marks
    (az:zero-card8-array (stroke-marks stroke))
    (setf (clay:stroked-diagram stroke) stroked-diagram)
    stroke))

;;;============================================================

(defmethod clay:clip-to-diagram! ((diagram Histogram) wr)
  (declare (type Histogram diagram)
	   (type g:Rect wr)
	   (:returns (type g:Rect cr)))
  ;; <g:intersect2> returns nil for an empty intersection,
  ;; in which case we just set the extent of <r> to be <0,0>.
  ;; transform twice to make sure truncation is handled consistently
  (let* ((map0 (clay:window->pixmap (clay:diagram-lens diagram)))
	 (map1 (clay:pixmap->view (clay:diagram-lens diagram)))
	 (cr (g:make-rect (g:codomain map1)))
	 (dwr (clay:diagram-window-rect diagram))
	 (y0 (g:y dwr))
	 (h0 (g:h dwr)))
    (g:with-borrowed-rect (rr (g:codomain map0))
      (g:transform map0 wr :result rr)
      (g:transform map1 rr :result cr)
      (unless (g:intersect2 (clay:diagram-view-rect diagram) cr :result cr)
	(setf (g:w cr) 0.0d0)
	(setf (g:h cr) 0.0d0))
      (g:inverse-transform map1 cr :result rr)
      (g:inverse-transform map0 rr :result wr)
      (setf (g:y wr) y0)
      (setf (g:h wr) h0)
      cr)))

;;;============================================================

(defmethod clay:stroke-object ((space Complete-Band-Space)
			       (stroke Histo-Stroke))
 
  (declare (optimize (safety 0) (speed 3)))
  
  (let* ((stroked-diagram (clay:stroked-diagram stroke))
	 (marks (stroke-marks stroke))
	 (paint (paint-vector space))
	 (ch (band-vector (band0 stroked-diagram)))
	 (p (clay:stroke-pixel stroke))
	 (pmask (clay:stroke-pmask stroke)))
    (declare (type Histogram stroked-diagram)
	     (type az:Card8-Vector paint ch)
	     (type (az:Card8-Vector 256) marks)
	     (type az:Card8 p pmask))
    ;; update the paint array
    (dotimes (i (length paint))
      (declare (type az:Card28 i))
      (when (= 1 (aref marks (aref ch i)))
	(setf (aref paint i) (xlt:paint-wash (aref paint i) p pmask))))))

(defmethod clay:stroke-object ((space Band-Space-Bipart)
			       (stroke Histo-Stroke))
 
  (declare (optimize (safety 0) (speed 3)))
  
  (let* ((stroked-diagram (clay:stroked-diagram stroke))
	 (marks (stroke-marks stroke))
	 (paint (paint-vector space))
	 (ch (band-vector (band0 stroked-diagram)))
	 (p (clay:stroke-pixel stroke))
	 (pmask (clay:stroke-pmask stroke))
	 (key (key space))
	 (mask (mask space))
	 (part (part-vector space)))
    (declare (type Histogram stroked-diagram)
	     (type az:Card8-Vector paint ch)
	     (type (az:Card8-Vector 256) marks)
	     (type az:Card8 p pmask)
	     (type az:Card16 key mask)
	     (type az:Card16-Vector part))
    ;; update the paint array
    (dotimes (i (length paint))
      (declare (type az:Card28 i))
      (when (and (= 1 (aref marks (aref ch i)))
		 (= key (logand mask (aref part i))))
	(setf (aref paint i) (xlt:paint-wash (aref paint i) p pmask))))))

;;;------------------------------------------------------------

(defmethod clay:stroke-object ((diagram Histogram) (stroke clay:Stroke))
  (clay:update-internal diagram stroke)
  (ac:send-msg 'clay:role-redraw-diagram diagram diagram (ac:new-id))
  (az:deletef diagram (clay:stroke-objects-to-be-updated stroke))
  (when (null (clay:stroke-objects-to-be-updated stroke))
    (clay:return-stroke stroke))
  (values t))
 
