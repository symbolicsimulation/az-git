;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defun fill-tsqrt-table (table)
  (declare (optimize (safety 0) (speed 3))
	   (type (az:Card8-Vector 65536) table))
  (dotimes (i 65536) (setf (aref table i) (isqrt i)))
  table)

(defparameter *tsqrt-table*
    (fill-tsqrt-table (az:make-card8-vector 65536))
  "A table of precomputed square roots for (Unsigned-Byte 16).")

(declaim (type (az:Card8-Vector 65536) *tsqrt-table*))

(defmacro tsqrt (i) `(aref *tsqrt-table* ,i))
  