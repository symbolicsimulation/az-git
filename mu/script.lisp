;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
(in-package :Mu) 

(time
 (az:large-allocation
  (setf r
    (make-rotor (band *fazendas* "b3")
		(band *fazendas* "b4")
		(band *fazendas* "b5")))))

(time
 (az:large-allocation
  (setf r
    (make-repscatgram (band *fazendas* "b3")
		      (band *fazendas* "b4")))))

(progn (time (replay0 r))
       (time (replay1 r)))

(time (clay:layout-diagram r nil))
(progn (time (layout0 r))
       (time (layout1 r))
       (time (layout2 r))
       (time (layout3 r)))

(progn (time (layout0 r))
       (time (layout1 r))
       (time (layout2 r))
       (time (layout3 r)))

(time (compute-fractions *fazendas-mixing*))

(progn
  (time
   (multiple-value-setq (*fd0* *fd1*)
     (split-space *fazendas-band-space*
		   (fraction *fazendas-mixing* "vegsh") 152)))
  (time
   (multiple-value-setq (*fd00* *fd10*)
     (split-space *fd0* (fraction *fazendas-mixing* "sasp") 96)))
  (time 
   (multiple-value-setq (*fd010* *fd110*)
     (split-space *fd10* (fraction *fazendas-mixing* "dimona") 104))))

(time (display-fractions *fazendas-mixing* :band-space *fd0*))
(time
   (defparameter *fvsw*
       (make-rotor
	(fraction *fazendas-mixing* "sasp")
	(fraction *fazendas-mixing* "dimona")
	(fraction *fazendas-mixing* "plra")
	:band-space *fd0*
	:left 500 :top 4)))

(time
 (make-rotor
  (fraction *fazendas-mixing* "sasp")
  (fraction *fazendas-mixing* "vegsh")
  (fraction *fazendas-mixing* "plra")
  :band-space *fd0*
  :left 500 :top 4))

(time
 (az:large-allocation
  (let ((fracs (list "dimona" "plra" "sasp" "vegsh")))
    (make-imagegram (fraction *fazendas-mixing* "vegsh"))
    (loop for i0 from 0
	for names on fracs
	for f0 = (first names) do
	  (make-histogram
	   (fraction *fazendas-mixing* f0) :height 260 :top (* i0 260))
	  (apply #'make-rotor
		 (mapcar #'(lambda (n) (fraction *fazendas-mixing* n))
			 (remove f0 fracs)))
	  (excl:gc :tenure)))))

(wt:winspect (mediator *fazendas*))

(dolist (r (rotors (mediator *fazendas-band-space*)))
  (dotimes (i 8)
    (spin-diagram r)))



(make-imagegram (band *fazendas* "b4"))

;;;-------------------------------------------------------------

(time
 (az:large-allocation

  (defparameter *fsub-bands* (vector (band-vector (band *fsub* "b1"))
				     (band-vector (band *fsub* "b2"))
				     (band-vector (band *fsub* "b3"))
				     (band-vector (band *fsub* "b4"))
				     (band-vector (band *fsub* "b5"))
				     (band-vector (band *fsub* "b7"))))
  (defparameter *fsub-ends*
      (list (az:float-vector 91.7 56.5 75.0  92.9 148.4 39.3) ;; dimona54-89  
	    (az:float-vector 70.7 34.2 45.4  67.2 139.9 44.3) ;; plra000189-hw
	    (az:float-vector 64.9 29.2 22.6 150.0  88.8 14.1) ;; sasp000789-hw
	    (az:Float-Vector 51.2 18.5 14.0  35.2  25.5  4.7) ;; vegsh89
	    ))
  (let ((n (g:dimension (band-space *fsub*))))
    (defparameter *fsub-fractions0*
	(list (az:make-card8-vector n) (az:make-card8-vector n)
	      (az:make-card8-vector n) (az:make-card8-vector n)))
    (defparameter *fsub-fractions1*
	(list (az:make-card8-vector n) (az:make-card8-vector n)
	      (az:make-card8-vector n) (az:make-card8-vector n)))
    (defparameter *fsub-rms0* (az:make-card8-vector n))
    (defparameter *fsub-rms1* (az:make-card8-vector n)))))


(time (unmix1 *fsub-bands* *fsub-ends* *fsub-fractions1* *fsub-rms1*))

(progn
  (compare-fraction-image (first *fsub-fractions1*)
			  (band-vector (band *fsub* "dimona")))
  (compare-fraction-image (second *fsub-fractions1*)
			  (band-vector (band *fsub* "plra")))
  (compare-fraction-image (third *fsub-fractions1*)
			  (band-vector (band *fsub* "sasp")))
  (compare-fraction-image (fourth *fsub-fractions1*)
			  (band-vector (band *fsub* "vegsh"))))

(compare-fraction-image *fsub-rms1* (band-vector (band *fsub* "rms")))

(time (unmix0 *fsub-bands* *fsub-ends* *fsub-fractions0* *fsub-rms0*))
(progn
  (compare-fraction-image (first *fsub-fractions0*)
			  (band-vector (band *fsub* "dimona")))
  (compare-fraction-image (second *fsub-fractions0*)
			  (band-vector (band *fsub* "plra")))
  (compare-fraction-image (third *fsub-fractions0*)
			  (band-vector (band *fsub* "sasp")))
  (compare-fraction-image (fourth *fsub-fractions0*)
			  (band-vector (band *fsub* "vegsh"))))
(compare-fraction-image *fsub-rms0*  (band-vector (band *fsub* "rms")))
(compare-fraction-image *fsub-rms1* *fsub-rms0*)

;;;-------------------------------------------------------------

(time
 (az:large-allocation
  (defparameter *fazendas-nrows* 700)
  (defparameter *fazendas-ncols* 1500)
  (defparameter *fazendas*
      (make-mu "../examples/fazendas/"
		  :band-names (list "90-b1.pic"
				       "90-b2.pic"
				       "90-b3.pic"
				       "90-b4.pic"
				       "90-b5.pic"
				       "90-b7.pic"
				       "90-rms.pic"
				       "90-90-shade.pic"
				       "90-90-soil.pic"
				       "90-90-veg.pic"
				       "90-90-woody.pic")
		  :image-nrows *fazendas-nrows* :image-ncols *fazendas-ncols*
		  :header-bytes 512))
  
  (time (make-histogram (band *fazendas* "shade")
			:left 500 :top 100 :height 64))
  (time (make-histogram (band *fazendas* "veg")
			:left 500 :top 180 :height 64))
  (time (make-histogram (band *fazendas* "wood")
			:left 500 :top 260 :height 64))
  (time (make-histogram (band *fazendas* "soil")
			:left 500 :top 340 :height 64))
  #||
  (dolist (name (list "shade" "soil" "veg" "wood"))
    (time (make-imagegram (band *fazendas* name))))
  ||#
  (time (make-imagegram (band *fazendas* "shade")))
  (loop for i0 from 0
      for fractions on (list "shade" "soil" "veg" "wood")
      for f0 = (first fractions) do
	(loop for j0 from i0
	    for f1 in (rest fractions) do
	      (time
	       (make-barygram (band *fazendas* f0)
			      (band *fazendas* f1) 
				   :left (* i0 258) :top (* j0 280)))))
  (time
   (defparameter *fazendasvsw*
       (make-rotor (fraction *fazendas-mixing* "veg")
		   (fraction *fazendas-mixing* "soil")
		   (fraction *fazendas-mixing* "wood") 
		   :left 500 :top 128)))
  ))


;;;-------------------------------------------------------------







