#include 	<fcntl.h>
#include 	<stdio.h>
#include	<math.h>
#include        <memory.h>

/****************************************************************/
/* Reading Images                                               */
/****************************************************************/

#define MAXCHARS 128

/* for consistency with ANSI C: */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

/*------------------------------------------------------------------*/

void safe_byte_seek (path, file, nskip)
     char path[MAXCHARS];
     FILE *file;
     long int nskip;
{ int error;
  error = fseek(file, nskip, SEEK_CUR);
  if (error != 0)
    { fprintf(stderr, "\nError %d seeking past %d bytes in file %s\n",
	      error, nskip, path); }}

void safe_byte_read (path, file, nbytes, array)
     char path[MAXCHARS];
     FILE *file;
     long int nbytes;
     unsigned char *array;
{ if (fread(array, sizeof(unsigned char), nbytes, file) < nbytes) 
    { fprintf(stderr, "error reading from %s\n", path); }}

/*------------------------------------------------------------------*/

void read_byte_array (path, nskip, nbytes, array)

     char path[MAXCHARS];
     long int nskip, nbytes;
     unsigned char *array;

{ FILE *input;	
  
  /*
    printf("\nReading %d bytes from %s.\n", nbytes, path); 
    fflush(stdout);
    */

  input = fopen(path,"r");
  if (input == NULL) { fprintf(stderr,"\nCan't open input file %s.\n", path); }
  else {
    safe_byte_seek(path, input, nskip);
    safe_byte_read(path,input,nbytes,array); }
  /* close the image file */
  fclose(input); }


/*------------------------------------------------------------------*/

void read_partial_image (path, nskip, rowlen, x0, x1, y0, y1, array)
     char path[MAXCHARS];
     long int nskip, rowlen, x0, x1, y0, y1;
     unsigned char *array;

{ FILE *input;	
  long int i;
  long int nbytes = x1 - x0;

  /*
  printf("\nReading %d bytes from %s.\n", (x1 - x0) * (y1 - y0), path); 
  fflush(stdout);
  */

  /* open the image file */
  input = fopen(path,"r");
  
  if (input == NULL) { fprintf(stderr, "can't open input file %s\n", path); }
  else {			/* first skip header */
    safe_byte_seek(path, input, nskip);
    /* and  <y0> scanlines */
    safe_byte_seek(path, input, rowlen*y0);
    /* loop over wanted scanlines */
    nskip = rowlen - x1;	/* how much to skip at end of scanlines */
    for(i=y0;i<y1;i++) {
      /* skip start of scanline */
      safe_byte_seek(path,input,x0);
      /* read scanline and increment output pointer */
      safe_byte_read(path,input,nbytes,array);
      /* skip end of scanline */
      safe_byte_seek(path,input,nskip);
      array += nbytes; } }

  /* close the image file */
  fclose(input);}

/*------------------------------------------------------------------*/

void read_padded_image (path, nskip, nrows, ncols, padded_ncols, array)

     char path[MAXCHARS];
     long int nskip, nrows, ncols, padded_ncols;
     unsigned char* array;
{ FILE *input;	
  long int i, pad;

  pad = padded_ncols - ncols;

  printf("\Reading %d bytes from %s.\n", nrows*ncols, path); 
  fflush(stdout);
  
  /* open the image file */
  input = fopen(path,"r");
  
  if (input == NULL) {
    fprintf(stderr, "can't open input file %s\n", path); }
  else if ((nskip > 0) && (0 != fseek(input, nskip, SEEK_SET)) ) {
    /* skip the header, if any */
    fprintf(stderr,"error on file %s\n", path); }
  else {
    if (pad == 0) {		/* then read it in one chunk */
      if (fread(array,1,ncols*nrows,input) < ncols*nrows) {
	fprintf(stderr,"error on file %s\n", path); } }
    else {			/* read it in a row at a time */	
      for(i=0; i<nrows; i++) {
	if (fread(array,1,ncols,input) < ncols)
	  { fprintf(stderr,"error on file %s\n", path); }
	array = array + padded_ncols; } } } 

  /* close the image file */
  fclose(input); }

/****************************************************************/
/* Zooming Images                                               */
/****************************************************************/

void magnify_painted_image (mag, m0, n0, input, paint, n1, output)

     long int mag, m0, n0, n1;
     unsigned char *input, *paint, *output;

{ long int skip, stripe_len;
  unsigned char *ip, *in, *input_end, *line_end, *stripe_end, *step_end;
  unsigned char pixel;

  if( mag == 1) {
    skip = n1 - n0;
    input_end = input + m0*n0;
    while (input < input_end) { /* do each input scanline */
      line_end = input + n0;
      while (input < line_end)
	{ *output++ = (*paint++ & 0xE0) | (*input++ >> 3); }
      output += skip; } }
  else {
    skip = n1 - n0*mag;
    stripe_len = n1*mag;
    input_end = input + m0*n0;
    while (input < input_end) {	/* do each input scanline */
      line_end = input + n0;
      stripe_end = output + stripe_len;
      while (output < stripe_end) { /* do each output scanline */
	for (in=input, ip=paint; in<line_end; in++, ip++) {
	  /* do each input pixel */
	  step_end = output + mag;
	  pixel = (*ip & 0xE0) | (*in >> 3);
	  while (output < step_end) { *output++ = pixel; }}
	output += skip; }
      input = line_end;
      paint += n0; } } }

/*------------------------------------------------------------------*/
/* Image histograms                                                 */
/*------------------------------------------------------------------*/

void image_histogram (n, image, histogram)

     long int n; /* how many pixels total */
     unsigned char *image; /* input image band */
     long int histogram[256]; /* output histogram 256 */

{ register unsigned char *end = image + n;
  while (image < end) { (histogram[*(image++)])++; } }

/*------------------------------------------------------------------*/

void image_histogram_2d (n, image0, image1, histogram)

     long int n; /* how many pixels total */
     unsigned char *image0, *image1; /* input image bands */
     long int histogram[]; /* output histogram 256x256 */

{ register unsigned char *end = image0 + n;
  while (image0 < end) { (histogram[(*(image0++) << 8) | *(image1++)])++; } }

/*------------------------------------------------------------------*/

void integrate_long_int_array (n, input, output)
     long int n; /* length of input and output */
     long int *input, *output;
{ register long int *end = input + n;
  register long int x = 0;
  while (input < end) { x = *output++ = x + (*input++); }}
    
/*------------------------------------------------------------------*/

void fill_scatimage (n, paint, image0, image1, layer)

     long int n; /* how many pixels total */
     unsigned char *paint, *image0, *image1, *layer;

{ register unsigned char* end = image0 + n;
  while (image0 < end) {
    layer[(*(image0++)<<8) | *(image1++)] = *(paint++) | 0x1F;} }

/*------------------------------------------------------------------*/

void fill_scatimage_from_bin (pixel, binlen, bin, image0, image1, layer)

     long int pixel, binlen;
     long int *bin;
     unsigned char *image0, *image1, *layer;

{ register unsigned char p;
  register long int *b, *end;

  b = bin;
  p = (unsigned char) pixel;
  end = b + binlen;
  while (b < end) { layer[(image0[*b] << 8) | image1[*b++]] = p; } }


/*------------------------------------------------------------------*/
/* 2d Barycentric coordinate histograms                             */
/*------------------------------------------------------------------*/

/* trig constants */
#define PI        3.141592653589793e0
#define PI_3      1.0471975511965976e0
#define SIN_PI_3  0.8660254037844386e0
#define COS_PI_3  0.5e0

/* basic scale factor used to replace divides by shifts */

#define BARYLOG   11
#define BARYVAL   2048

/* coefficients of the affine transformation to bary coordinates */

#define BARYLEN   1365 /* ((int) ((BARYVAL * 2.0) / 3.0)) */
#define BARYSIN   1182 /* ((int) ((BARYVAL * 2.0 * SIN_PI_3) / 3.0)) */
#define BARYCOS   682 /* ((int) (BARYVAL / 3.0))*/

/* #define BARYCOS   ((int) ((BARYVAL * 2.0 * COS_PI_3) / 3.0)) */

/* macros implementing the affine transformation with shifts */
/*
#define BARYSCALE(z)   ((z) >> BARYLOG)

#define XBARY(y,x)   BARYSCALE( BARYLEN*(x) + BARYCOS*(y) )
#define YBARY(y,x)   (212 - BARYSCALE(BARYSIN*(y)))
*/

/* use a table lookup for conversion from fractions to cartesian coordinates
** to save multiplies.
*/

unsigned char xxbary[256] = {
  0,0,1,2,2,3,4,4,5,6,6,7,8,8,9,10,10,11,12,12,13,14,14,15,16,16,17,18,18,19,
  20,20,21,22,22,23,24,24,25,26,26,27,28,28,29,30,30,31,32,32,33,34,34,35,36,
  36,37,38,38,39,40,40,41,42,42,43,44,44,45,46,46,47,48,48,49,50,50,51,52,52,
  53,54,54,55,56,56,57,58,58,59,60,60,61,62,62,63,64,64,65,66,66,67,68,68,69,
  70,70,71,72,72,73,74,74,75,76,76,77,78,78,79,80,80,81,82,82,83,84,84,85,86,
  86,87,88,88,89,90,90,91,92,92,93,94,94,95,96,96,97,98,98,99,100,100,101,102,
  102,103,104,104,105,106,106,107,108,108,109,110,110,111,112,112,113,114,114,
  115,116,116,117,118,118,119,120,120,121,122,122,123,124,124,125,126,126,127,
  128,128,129,130,130,131,132,132,133,134,134,135,136,136,137,138,138,139,140,
  140,141,142,142,143,144,144,145,146,146,147,148,148,149,150,150,151,152,152,
  153,154,154,155,156,156,157,158,158,159,160,160,161,162,162,163,164,164,165,
  166,166,167,168,168,169,170};

unsigned char yxbary[256] = {
  0,0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9,10,10,10,11,11,
  11,12,12,12,13,13,13,14,14,14,15,15,15,16,16,16,17,17,17,18,18,18,19,19,19,
  20,20,20,21,21,21,22,22,22,23,23,23,24,24,24,25,25,25,26,26,26,27,27,27,28,
  28,28,29,29,29,30,30,30,31,31,31,32,32,32,33,33,33,34,34,34,35,35,35,36,36,
  36,37,37,37,38,38,38,39,39,39,40,40,40,41,41,41,42,42,42,43,43,43,44,44,44,
  45,45,45,46,46,46,47,47,47,48,48,48,49,49,49,50,50,50,51,51,51,52,52,52,53,
  53,53,54,54,54,55,55,55,56,56,56,57,57,57,58,58,58,59,59,59,60,60,60,61,61,
  61,62,62,62,63,63,63,64,64,64,65,65,65,66,66,66,67,67,67,68,68,68,69,69,69,
  70,70,70,71,71,71,72,72,72,73,73,73,74,74,74,75,75,75,76,76,76,77,77,77,78,
  78,78,79,79,79,80,80,80,81,81,81,82,82,82,83,83,83,84,84,84,85};

unsigned char yybary[256] = {
  212,212,211,211,210,210,209,208,208,207,207,206,206,205,204,204,203,203,202,
  202,201,200,200,199,199,198,197,197,196,196,195,195,194,193,193,192,192,191,
  191,190,189,189,188,188,187,187,186,185,185,184,184,183,182,182,181,181,180,
  180,179,178,178,177,177,176,176,175,174,174,173,173,172,172,171,170,170,169,
  169,168,167,167,166,166,165,165,164,163,163,162,162,161,161,160,159,159,158,
  158,157,156,156,155,155,154,154,153,152,152,151,151,150,150,149,148,148,147,
  147,146,146,145,144,144,143,143,142,141,141,140,140,139,139,138,137,137,136,
  136,135,135,134,133,133,132,132,131,131,130,129,129,128,128,127,126,126,125,
  125,124,124,123,122,122,121,121,120,120,119,118,118,117,117,116,116,115,114,
  114,113,113,112,111,111,110,110,109,109,108,107,107,106,106,105,105,104,103,
  103,102,102,101,100,100,99,99,98,98,97,96,96,95,95,94,94,93,92,92,91,91,90,
  90,89,88,88,87,87,86,85,85,84,84,83,83,82,81,81,80,80,79,79,78,77,77,76,76,
  75,75,74,73,73,72,72,71,70,70,69,69,68,68,67,66,66,65};

#define XBARY(y,x) (xxbary[x] + yxbary[y])
#define YBARY(y,x) (yybary[y])

/*------------------------------------------------------------------*/

void image_barygram (n,image0,image1,barygram)

     long int n; /* how many pixels total */
     unsigned char *image0,*image1; /* input image bands */
     long int barygram[]; /* output histogram 256x256 */

{ register unsigned char y;
  register unsigned char *end  = image0 + n;
  while (image0 < end) {
    y = *image0++;
    barygram[ (yybary[y] << 8) | (xxbary[*image1++] + yxbary[y]) ]++;  } }

/*------------------------------------------------------------------*/

void fill_baryimage (n, paint, image0, image1, layer)

     long int n; /* how many pixels total */
     unsigned char *paint, *image0, *image1, *layer;

{ register unsigned char y;
  register unsigned char *end  = image0 + n;
  while (image0 < end) {
    y = *image0++;
    layer[ (yybary[y] << 8) | (xxbary[*image1++] + yxbary[y]) ]
      = *paint++ | 0x1F; } }

/*------------------------------------------------------------------*/

void fill_baryimage_from_bin (pixel, binlen, bin, image0, image1, layer)

     long int  pixel, binlen;
     long int  *bin;
     unsigned char  *image0, *image1, *layer;

{ register unsigned char  y;
  register long int  pin;
  register unsigned char  p = (unsigned char) pixel;
  register long int  *end = bin + binlen;
  while (bin < end) {
    pin = *bin++;
    y = image0[pin];
    layer[ (yybary[y] << 8) | (xxbary[image1[pin]] + yxbary[y]) ] = p; } }

/*------------------------------------------------------------------*/

void inner_rotate (ycuts, yxz, preps, xcos, xsin, yxr, zbf, pix)
     long int *ycuts, *yxz, *yxr;
     unsigned short int *xcos, *xsin, *zbf;
     unsigned char *preps, *pix;
{ unsigned char x, z;
  long int *end = ycuts + 256;
  unsigned long int i0 = 0, i1, i, yxzi;
  unsigned short int *zb, zr;
  unsigned long int yxri, y = 0;

  while ( ycuts < end ) {
    i1 = *ycuts++;
    for (i = i0; i < i1; i++) {
      yxzi = *yxz++;
      z = yxzi & 0xFF;
      x = (yxzi >> 8) & 0xFF;
      zr = xsin[x] + xcos[z];
      yxri = (y | (xcos[x] - xsin[z]));
      *yxr++ = (long int) yxri;
      zb = zbf + yxri;
      if (zr > *zb) { *zb = zr; pix[yxri] = preps[i]; } }
    i0 = i1;
    y += 512; } }

/*------------------------------------------------------------------*/
/* I hope this is right? */

void zero_long_array (n, a) long int n, *a; { memset(a,0,n*4); }

/*------------------------------------------------------------------*/

void zero_unsigned_long_array (n, a)
     long int n;
     unsigned long *a;
{ memset(a,0,n*4); }
 
/*------------------------------------------------------------------*/

void zero_byte_array (n, a)

     long int n;
     unsigned char *a;

{ memset(a,0,n); }

/* DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
 * The following assumes that simple arrays of (unsigned-byte 8) and
 * (unsigned-byte 16) have their first element aligned on a 32 bit
 * word boundary.
 */

void zero_byte_array_1 (n, a)

     long int n;
     unsigned char *a;

{ register unsigned char *end = a + n;
  register long int n4 = n / 4;

  /* do whole words first, assuming 1st elt is word aligned. */
  zero_unsigned_long_array(n4,a);
  /* finish remaining bytes */
  a += 4 * n4;
  while (a < end) { *a++ = 0; } }

/*------------------------------------------------------------------*/

void unit_byte_array_by_4 (n, a)

     long int n;
     unsigned long *a;

{ register unsigned long *end = a + n;
  while (a < end) { *a++ = 0x1010101; } }
 
void unit_byte_array_1 (n, a)

     long int n;
     unsigned char *a;

{ register unsigned char *end = a + n;
  long int n4 = n / 4;

  unit_byte_array_by_4( n4, a);
  /* finish remaining bytes */
  a += 4 * n4;
  while (a < end) { *a++ = 1; } }
 
void unit_byte_array (n, a)

     long int n;
     unsigned char *a;

{ memset(a,1,n); }
 
/*------------------------------------------------------------------*/

void zero_unsigned_short_array_1 (n, a)

     long int n;
     unsigned short *a;

{ register unsigned short *end = a + n;
  register long int n2 = (n / 2);

  /* do whole words first, assuming 1st elt is word aligned. */
  zero_unsigned_long_array(n2,a);
  /* finish remaining 2byte ints */
  a += 2 * n2;
  while (a < end) { *a++ = 0; } }


void zero_unsigned_short_array (n, a)
     long int n; 
     unsigned short *a;
{ memset(a,0,n*2); }
