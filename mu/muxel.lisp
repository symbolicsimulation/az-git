;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Multi-Band Pixel Object
;;;============================================================

(defclass Muxel (Mu-Object)
	  ((spectral-space
	    :accessor spectral-space
	    :initarg :spectral-space
	    :type Spectral-Space)
	   (muxel-data
	    :accessor muxel-data
	    :initarg :muxel-data
	    :type az:Card8-Vector))
  (:default-initargs
    :band-data (az:make-card8-vector 0))

  (:documentation
   "First try at a multi-band pixel object."))

;;;------------------------------------------------------------

(defun muxel-length (muxel)
  (declare (type Muxel muxel))
  (length (the az:Card8-Vector (muxel-data muxel))))

;;;------------------------------------------------------------






