;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defclass RepScatgram (Scatgram) ()
  (:documentation
   "Only displays one representative image pixel for each pair
of band values."))

;;;------------------------------------------------------------

(defun make-repscatgram (band1 band0
			 &key
			 (host (xlt:default-host))
			 (display (ac:host-display host))
			 (screen (xlib:display-default-screen display))
			 (band-space (band-space band1))
			 (left 0) (top 0) (width nil) (height nil))
  (declare (type Band band1 band0)
	   (type Simple-String host)
	   (type xlib:Display display)
	   (type xlib:Screen screen)
	   (type Band-Space band-space)
	   (type xlib:Card16 left top)
	   (type (or Null az:Card16) width height))
  (assert (eq (band-space band1) (band-space band0)))
  (let* ((name (format nil "RS[~a,~a]~a"
		       (name band1) (name band0) (name band-space)))
	 (window (xlt:make-window
		  :parent (xlib:screen-root screen)
		  :colormap (xlt:paint-colormap (xlib:screen-root screen))
		  :left left :top top :width width :height height))
	 (root (make-instance 'clay:Plot-Root
		 :diagram-window window
		 :diagram-name name))
	 (plot (make-instance 'RepScatgram
		 :diagram-subject band-space
		 :diagram-parent root
		 :diagram-name name
		 :band1 band1
		 :band0 band0)))
    (declare (type xlib:Window window)
	     (type Simple-String name)
	     (type clay:Plot-Root root))
    (when (or width height)
      (setf (clay:scale (clay:diagram-lens plot)) nil))
    (clay:build-diagram plot nil)
    (clay:initialize-diagram
     root
     :build-options (list :plot-child plot)
     :mediator (mediator (root-space band-space)))))

;;;============================================================
;;; Building
;;;============================================================

;;;============================================================
;;; Layout
;;;============================================================

(defun rep-layout (diagram)
  (declare (optimize (safety 0) (speed 3))
	   (type RepScatgram diagram))
  (with-index (index diagram)
    (let* ((band0 (band0 diagram))
	   (band1 (band1 diagram))
	   (space (band-space diagram))
	   (perm (sorted-pins index))
	   (hist (az:array-data (2band-histogram space band0 band1)))
	   (inte (az:array-data (2band-integram space band0 band1)))
	   (k 0))
      (declare (type Band-Space space)
	       (type Band band0 band1)
	       (type az:Card28-Vector perm hist inte)
	       (type az:Card28 k))
      
      (dotimes (i 65536)
	(declare (type az:Card28 i))
	(unless (zerop (aref hist i))
	  (incf k)))
      (setf (nreps diagram) k)
      (setf (rep-pins diagram) (az:make-card28-vector k))
      
      (let ((reps (rep-pins diagram)))
	(declare (type az:Card28-Vector reps))
	(setf k 0)
	(dotimes (i 65536)
	  (declare (type az:Card28 i))
	  (unless (zerop (aref hist i))
	    (setf (aref reps k)
	      (aref perm (the az:Card28 (- (aref inte i) 1))))
	    (incf k))))))
  diagram)

(defmethod clay:layout-diagram :before ((diagram RepScatgram) layout-options)
  (declare (type List layout-options)
	   (ignore layout-options))
  (rep-layout diagram))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod fill-scatimage ((space Complete-Band-Space)
			   (diagram RepScatgram)
			   (stroke T))
  (declare (optimize (safety 0) (speed 3))
	   (type Complete-Band-Space space)
	   (type RepScatgram diagram)
	   (type T stroke))
  (let ((x (band-vector (band1 diagram)))
	(y (band-vector (band0 diagram)))
	(p (paint-vector (root-space space)))
	(iv (az:array-data (xlib:image-z-pixarray 
			    (clay:diagram-image diagram))))
	(reps (rep-pins diagram)))
    (declare (type az:Card8-Vector x y p iv)
	     (type az:Card28-Vector reps))
    (dotimes (i (the az:Card28 (nreps diagram)))
      (declare (type az:Card28 i))
      (let ((pin (aref reps i)))
	(declare (type az:Card28 pin))
	(setf (aref iv (ppref y x pin)) (aref p pin))))))

;;;--------------------------------------------------------------

(defmethod fill-scatimage ((space Band-Space-Bipart)
			   (diagram RepScatgram)
			   (stroke T))
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space)
	   (type RepScatgram diagram)
	   (type T stroke))
  (let ((x (band-vector (band1 diagram)))
	(y (band-vector (band0 diagram)))
	(p (paint-vector (root-space space)))
	(iv (az:array-data (xlib:image-z-pixarray
			    (clay:diagram-image diagram))))
	(reps (rep-pins diagram))
	(key (key space))
	(mask (mask space))
	(part (part-vector space)))
    (declare (type az:Card8-Vector x y p iv)
	     (type az:Card28-Vector reps)
	     (type az:Card16 key mask)
	     (type az:Card16-Vector part))
    (dotimes (i (the az:Card28 (nreps diagram)))
      (declare (type az:Card28 i))
      (let ((pin (aref reps i)))
	(declare (type az:Card28 pin))
	(when (= key (logand mask (aref part pin)))
	  (setf (aref iv (ppref y x pin)) (aref p pin)))))))

;;;===========================================================
;;; Input
;;;===========================================================

