;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Sorting
;;;============================================================

(defun test-sort (band perm)
  (declare (optimize (safety 0) (speed 3))
	   (type Band band)
	   (type az:Card28-Vector perm))
  (let ((ch (band-vector band))
	(answer t))
    (declare (type az:Card8-Vector ch))
    (dotimes (i (- (length perm) 1))
      (declare (type az:Card28 i))
      (unless (<= (aref ch (aref perm i))
		  (aref ch (aref perm (+ i 1))))
	(format t "at ~d, ~d > ~d"
		i
		(aref ch (aref perm i))
		(aref ch (aref perm (+ i 1))))
	(setf answer nil)))
    answer))

;;;------------------------------------------------------------
;;; Counting sort
;;;------------------------------------------------------------

(defun counting-sort-pixels-1 (space band perm work)
  
  "Rearrange <perm> to sort by the values in <band>."
  
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space space)
	   (type Band band)
	   (type az:Card28-Vector perm work))

  (let ((b (band-vector band))
	(integral (band-integram space band))) 
    (declare (type az:Card8-Vector b)
	     (type (az:Card28-Vector 256) integral))
    ;; sort the pins in perm
    (let ((i (g:dimension space)))
      (declare (type az:Card28 i))
      (loop 
	(decf i)
	(let* ((pin (aref perm i))
	       (pixel (aref b pin)))
	  (declare (type az:Card28 pin)
		   (type az:Card8 pixel))
	  (setf (aref work (decf (aref integral pixel))) pin))
	(when (zerop i) (return))))
    ;; restore integram
    (az:integrate-card28-vector (band-histogram space band) integral))

  (values work perm))

;;;------------------------------------------------------------

(defun counting-sort-pixels-2 (space band0 band1 perm work)
  
  "Rearrange <perm> to sort by the pairs of bytes in band0 and band1."
  
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space space)
	   (type Band band0 band1)
	   (type az:Card28-Vector perm work))

  (let* ((b0 (band-vector band0))
	 (b1 (band-vector band1))
	 (integral (az:array-data (2band-integram space band0 band1))))
    (declare (type Band-Space space)
	     (type az:Card8-Vector b0 b1)
	     (type (az:Card28-Vector 65536) integral))
    ;; sort the pins in perm
    (let ((i (g:dimension space)))
      (declare (type az:Card28 i))
      (loop 
	(decf i)
	(let* ((pin (aref perm i))
	       (p2 (ppref b0 b1 pin)))
	  (declare (type az:Card28 pin)
		   (type az:Card16 p2))
	  (setf (aref work (decf (aref integral p2))) pin))
	(when (zerop i) (return))))
    ;; restore integram
    (az:integrate-card28-vector
     (az:array-data (2band-histogram space band0 band1)) integral))

  (values work perm))

;;;------------------------------------------------------------

(defun counting-sort-pixels-3 (d b0 b1 b2 p w)
  "Sort pixels on three bands using counting-sort [Cormen et al., sec 9.2]."
  (declare (type Band-Space d)
	   (type Band b0 b1 b2)
	   (type az:Card28-Vector p w))
  (multiple-value-bind
      (p2 w2) (multiple-value-bind (p1 w1) (counting-sort-pixels-2 d b1 b2 p w)
		(counting-sort-pixels-1 d b0 p1 w1))
    (values p2 w2)))

;;;------------------------------------------------------------
;;; Heap sort
;;;------------------------------------------------------------
#||
(defmacro pixel< (i0 i1) `(< (aref pdv ,i0) (aref pdv ,i1)))

(defmacro heapify (root-in max-in)
  "HEAPIFY, assuming both sons of root are heaps, percolates
the root element through the sons to form a heap at root.  Root and
max are zero based coordinates, but the heap algorithm only works on
arrays indexed from 1 through N (not 0 through N-1); This is because a
root at I has sons at 2*I and 2*I+1 which does not work for a root at
0.  Because of this, boundaries, roots, and termination are computed
using 1..N indexes."
  (let ((root (gensym))
	(max (gensym)))
    `(let* ((,root ,root-in)
	    (,max ,max-in)
	    (hroot (+ 1 ,root))
	    (hmax (+ 1 ,max))
	    (rootx (aref perm ,root))
	    (hmax/2 (ash hmax -1)))
       (declare (type az:Card28 ,root ,max)
		(type az:Card28 hroot hmax rootx hmax/2))
       (loop
	 (when (> hroot hmax/2) (return))
	 (let* ((l (the az:Card28 (ash hroot 1)))
		;; l-son index in perm (0..N-1) is
		;; one less than heap computation
		(l-1 (- l 1))
		(lx (aref perm l-1)))
	   (declare (type az:Card28 l l-1 lx))
	   (when (< l hmax)
	     ;; there is a right son.
	     (let* ((rx (aref perm l)))
	       (declare (type az:Card28 rx))
	       ;; choose the greater of the two sons.
	       (when (pixel< lx rx) (setf l-1 l) (setf lx rx))))
	   ;; if greater son is less than root
	   ;; then we've formed a heap again.
	   (when (pixel< lx rootx) (return))
	   ;; else put greater son at root
	   ;; and make greater son node be the root.
	   (setf (aref perm ,root) lx)
	   (setf hroot (+ l-1 1)) ;; 1+ to be in heap coordinates.
	   (setf ,root l-1)))
       ;; actual index into vector for root ele.
       ;; now really put percolated value into heap
       ;; at the appropriate root node.
       (setf (aref perm ,root) rootx))))

(defun heap-sort-pixels (ch0 ch1 ch2 perm)
  "Modified from cmucl SORT. Sorting is done with a heap sort."
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Matrix ch0 ch1 ch2)
	   (type az:Card28-Vector perm))
  (let* ((pdv (pack-3-card8-vectors
	       ch0 ch1 ch2 (az:make-card24-vector (length ch0)))))
	 (n (length pdv))
 	 (n-1 (- n 1)))
    (declare (type az:Card24-Vector pdv)
	     (type az:Card28 n n-1))
    ;; Rearrange perm elements into a heap to start heap sorting.
    (do ((i (ash n-1 -1) (- i 1)))
	((minusp i))
      (declare (type Fixnum i))
      (heapify i n-1)) 	
    ;; sort it
    (do* ((i n-1 i-1)
	  (i-1 (- i 1) (- i-1 1)))
	((zerop i) perm)
      (declare (type Fixnum i i-1))
      (rotatef (aref perm 0) (aref perm i))
      (heapify 0 i-1)))
  perm)

(defun heap-sort-pixels-1 (ch perm)
  "Modified from cmucl SORT. Sorting is done with a heap sort."
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Vector ch)
	   (type az:Card28-Vector perm))
  (let* ((pdv ch)
	 (n (length pdv))
 	 (n-1 (- n 1)))
    (declare (type az:Card8-Vector pdv)
	     (type az:Card28 n n-1))
    ;; Rearrange perm elements into a heap to start heap sorting.
    (do ((i (ash n-1 -1) (- i 1)))
	((minusp i))
      (declare (type Fixnum i))
      (heapify i n-1)) 	
    ;; sort it
    (do* ((i n-1 i-1)
	  (i-1 (- i 1) (- i-1 1)))
	((zerop i) perm)
      (declare (type Fixnum i i-1))
      (rotatef (aref perm 0) (aref perm i))
      (heapify 0 i-1)))
  perm)

;;;------------------------------------------------------------
;;; Bucket sort
;;;------------------------------------------------------------

(defun bucket-sort-pixels-1 (ch perm start end)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8-Vector ch)
	   (type az:Card28-Vector perm)
	   (type az:Card28 start end))

  (let ((buckets (make-array '(256))))
    (declare (type (Simple-Array T (256)) buckets))
    ;; assign pins to buckets
    (do ((i start (+ i 1)))
	((>= i end))
      (declare (type az:Card28 i))      
      (let ((pin (aref perm i)))
	(declare (type az:Card28 pin))
	(push pin (aref buckets (aref ch pin)))))
    ;; flatten buckets
    (let ((i start))
      (declare (type az:Card28 i))
      (dotimes (p 256)
	(declare (type az:Card8 p))
	(dolist (pin (aref buckets p))
	  (setf (aref perm i) pin)
	  (incf i))))))


||#