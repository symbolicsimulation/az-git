;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Mixing-Models
;;;============================================================
 
(defclass Mixer (Mu-Object g:Point-Set)
	  (;; model input
	   (g:home
	    :reader g:home
	    :initarg :home
	    :type Spectral-Space)

	   ;; intermediate results
	   (mixer-ginverse
	    :accessor mixer-ginverse
	    :type az:Float-Matrix)
	   (mixer-q1t
	    :accessor mixer-q1t
	    :type az:Float-Matrix)
	   (mixer-q2t
	    :accessor mixer-q2t
	    :type az:Float-Matrix))
  
  (:documentation
   "A class for objects representing abstract mixing models.
A mixer can be applied to any image with the right spectral space."))

;;;------------------------------------------------------------

(defun mixer-endmatrix (mixer)
  "Return shifted endmembers as columns of a matrix."
  (declare (optimize (safety 0) (speed 3))
	   (type Mixer mixer)
	   (:returns az:Float-Matrix))
  (shift-endmembers (map 'List #'g:coords (g:points mixer))))

;;;------------------------------------------------------------

(defun compute-affine-maps (mixer)
  "Calculate the affine maps that are used to compute
fraction coords, fitted band vectors, band residuals, and rms."
  (declare (optimize (safety 0) (speed 3))
	   (type Mixer mixer))
  (multiple-value-bind (a- q1t q2t) (g:qr-inverse (mixer-endmatrix mixer))
    (declare (type az:Float-Matrix a- q1t q2t))
    (setf (mixer-ginverse mixer) a-)
    (setf (mixer-q1t mixer) q1t)
    (setf (mixer-q2t mixer) q2t))
  (values mixer))
  
;;;------------------------------------------------------------

(defun make-mixer (name endmembers)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String name)
	   (type Sequence endmembers))
  (assert (sequence-of? 'Endmember endmembers))
  (let* ((home (g:home (elt endmembers 0)))
	 (mixer (make-instance 'Mixer
		  :name name
		  :home home
		  :points endmembers)))
    (declare (type Spectral-Space home))
    (assert (every #'(lambda (e) (eq home (g:home e))) endmembers))
    (compute-affine-maps mixer)
    mixer))

;;;============================================================

(defclass Mixing (Mu-Object)
	  (;; model input
	   (mixer
	    :type Mixer
	    :reader mixer
	    :initarg :mixer)
	   (mu
	    :reader mu
	    :initarg :mu
	    :type Mu)
	   (bands
	    :reader bands
	    :initarg :bands
	    :type Sequence)

	   ;; model output
	   (fractions
	    :type (or Null Simple-Vector)
	    :initform nil)
	   (rms
	    :type (or Null Band)
	    :initform nil))
  
  (:documentation
   "A class for objects representing abstract mixing models.
A mixer can be applied to any image with the right spectral space."))


;;;------------------------------------------------------------
;;; lazy slots:

(defmethod fractions ((mixing Mixing))
  (let ((fractions (slot-value mixing 'fractions)))
    (when (null fractions)
      ;; allocate space for fractions
      (az:large-allocation
       (let* ((mixer (mixer mixing))
	      (n (length (g:points mixer)))
	      (ends (g:points mixer)))
	 (declare (type Mixer mixer)
		  (type az:Card28 n))
	 (setf fractions (make-array n))
	 (dotimes (i n)
	   (setf (aref fractions i) (make-fraction mixing (elt ends i)))) 
	 (setf (slot-value mixing 'fractions) fractions))))
    fractions))


(defmethod rms ((mixing Mixing))
  (let ((rms (slot-value mixing 'rms)))
    (when (null rms)
      ;; allocate space for fractions
      (az:large-allocation
       (setf rms (make-band (nrows mixing) (ncols mixing)))
       (setf (slot-value mixing 'rms) rms)))
    rms))

;;;------------------------------------------------------------

(defmethod band-space ((mixing Mixing))
  (band-space (mu mixing)))

(defmethod spectral-space ((mixing Mixing))
  (spectral-space (mu mixing)))

;;;------------------------------------------------------------

(defun make-mixing (name mixer bands)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String name)
	   (type Mixer mixer)
	   (type Sequence bands))
  (assert (sequence-of? 'Band bands))
  
  (let* ((mu (mu (elt bands 0))))
    (declare (type Mu mu))
    (assert (eq (spectral-space mu) (g:home mixer)))
    (assert (every #'(lambda (b) (eq mu (mu b))) bands))
    (make-instance 'Mixing
      :name name
      :mu mu
      :mixer mixer
      :bands bands)))
 
;;;------------------------------------------------------------

(defun compute-fractions (mixing)

  "Compute fractions for mmixing model."
  
  (declare (optimize (safety 0) (speed 3))
	   (type Mixing mixing))
  
  (let* ((mixer (mixer mixing))
	 (ends (g:points mixer))
	 (origin (g:coords (elt ends 0)))
	 (bands (map 'Vector #'band-vector (bands mixing)))
	 (fractions (map 'List #'band-vector (fractions mixing)))
	 (n (length ends))
	 (n-1 (- n 1))
	 (m (length bands))
	 (npixels (g:dimension (band-space mixing)))
	 (b (az:make-float-vector m))
	 (f (az:make-float-vector n-1))
	 (a- (mixer-ginverse mixer))
	 (a-dv (az:array-data a-)))
    (declare (type az:Float-Vector origin)
	     (type Sequence ends)
	     (type Simple-Vector bands)
	     (type List fractions)
	     (type az:Card16 n n-1 m)
	     (type az:Card28 npixels)
	     (type az:Float-Vector b f a-dv)
	     (type az:Float-Matrix a-))
    ;; compute fraction coordinates for each pixel
    (dotimes (k npixels)
      (declare (type az:Card28 k))
      (shift-band-vector origin bands k b)
      ;; solve for floating fraction coordinates
      (az::%float-matrix*vector n-1 m a-dv b f)
      ;; (az:float-matrix*vector a- b :result f)
      ;; rescale and truncate fractions
      (floor-fractions fractions f k)))
  (values mixing))


;;;------------------------------------------------------------

(defun display-fractions (mixing &key (band-space (band-space mixing)))
  (declare (type Mixing mixing))
  (az:large-allocation
   ;;(compute-fractions mixing)
   (let* ((fractions (fractions mixing))
	  (n (length fractions))
	  (dx (truncate 900 n)))
     (dotimes (i n) (make-imagegram (aref fractions i)
				    :band-space band-space))
     (dotimes (i n)
       (let ((f0 (aref fractions i))
	     (left (+ 100 (* i dx))))
	 (make-histogram f0 :left left :height dx
			 :band-space band-space)
	 (dotimes (j i)
	   (let ((f1 (aref fractions j))
		 (top (+ (* j dx) dx)))
	     (make-repscatgram f0 f1
			    :band-space band-space
			    :left left :top top))))))))