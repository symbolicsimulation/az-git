;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Mu -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Mu)

;;;=======================================================

#+(and :accelerators :excl)
(eval-when (compile load eval)
  #-:allegro-v4.0 (require :Foreign)
  #+:allegro-v4.0 (cltl1:require :Foreign))

#+(and :accelerators :excl)
(ff:defforeign-list
    (list
     (list 'magnify_painted_image
	   :entry-point (ff:convert-to-lang "magnify_painted_image")
	   :language :C
	   :arguments
	   '(Fixnum ;; mag
	     Fixnum ;; m0
	     Fixnum ;; n0
	     az:Card8-Array ;; input
	     az:Card8-Array ;; paint
	     Fixnum ;; n1
	     az:Card8-Array ;; output
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)
	  
     (list 'image_histogram
	   :entry-point (ff:convert-to-lang "image_histogram")
	   :language :C
	   :arguments
	   '((Unsigned-Byte 28) ;; n
	     az:Card8-Array ;; image
	     az:Card28-Array ;; Histogram
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'image_histogram_2d
	   :entry-point (ff:convert-to-lang "image_histogram_2d")
	   :language :C
	   :arguments
	   '((Unsigned-Byte 28) ;; n
	     az:Card8-Array ;; image0
	     az:Card8-Array ;; image1
	     az:Card28-Array ;; histogram
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'fill_scatimage
	   :entry-point (ff:convert-to-lang "fill_scatimage"
					    :language :C)
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Card8-Array ;; paint
	     az:Card8-Array ;; image0
	     az:Card8-Array ;; image1
	     az:Card8-Array ;; layer
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'fill_scatimage_from_bin
	   :entry-point (ff:convert-to-lang "fill_scatimage_from_bin")
	   :language :C
	   :arguments
	   '(Fixnum ;; pixel
	     Fixnum ;; binlen
	     az:Fixnum-Vector ;; bin
	     az:Card8-Array ;; image0
	     az:Card8-Array ;; image1
	     az:Card8-Array ;; layer
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)
	    
     (list 'image_barygram
	   :entry-point (ff:convert-to-lang "image_barygram")
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Card8-Array ;; image0
	     az:Card8-Array ;; image1
	     az:Fixnum-Array ;; histogram
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'fill_baryimage
	   :entry-point (ff:convert-to-lang "fill_baryimage")
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Card8-Array ;; paint
	     az:Card8-Array ;; image0
	     az:Card8-Array ;; image1
	     az:Card8-Array ;; layer
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'fill_baryimage_from_bin
	   :entry-point (ff:convert-to-lang "fill_baryimage_from_bin")
	   :language :C
	   :arguments
	   '(Fixnum ;; pixel
	     Fixnum ;; binlen
	     az:Fixnum-Vector ;; bin
	     az:Card8-Array ;; image0
	     az:Card8-Array ;; image1
	     az:Card8-Array ;; layer
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'inner_rotate
	   :entry-point (ff:convert-to-lang "inner_rotate")
	   :language :C
	   :arguments
	   '(az:Fixnum-Array ;; ycuts
	     az:Fixnum-Array ;; yxz
	     az:Card8-Array ;; preps
	     az:Card16-Array ;; xcos
	     az:Card16-Array ;; xsin
	     az:Fixnum-Array ;; yxr
	     az:Card16-Array ;; zbf
	     az:Card8-Array ;; pix
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     ))

