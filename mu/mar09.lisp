;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

;;;-------------------------------------------------------------

(progn
  (defparameter *w* 1234)
  (defparameter *h* 604)
  (defparameter *iw* (ceiling *w* 6))
  (defparameter *ih* (ceiling *h* 6))

  (defparameter *feb0893-band-space*
      (make-instance 'Complete-Band-Space
	:name "Fazendas demo Feb 08 93"
	:coordinate-dimensions (list *h* *w*)
	:documentation "A Band-Space for the Fazendas Landsat TM images."))


  (defparameter *feb0893-format*
      (make-basic-file-format
       :file-nrows *h*
       :file-ncols *w*
       :header-bytes 0
       :documentation 
       "A Basic-File-Format for the Feb 08 93 Landsat TM images."))

  (defparameter *feb0893*
      (make-mu :directory-name "/images/manaus/"
		  :file-names (list "fazendas-90-b1-reg" 
				    "fazendas-90-b2-reg" 
				    "fazendas-90-b3-reg"
				    "fazendas-90-b4-reg" 
				    "fazendas-90-b5-reg" 
				    "fazendas-90-b7-reg"
				    "fazendas-90-register-rms" 
				    "fazendas-90-register-dimona5a-89"
				    "fazendas-90-register-plra0002-89" 
				    "fazendas-90-register-quag22stack-89"
				    "fazendas-90-register-vegsh89")
		  :file-format *feb0893-format*
		  :spectral-space *landsat-tm-spectral-space*
		  :band-space *feb0893-band-space*))

  (defparameter *feb0893-mixing*
      (make-mixing "feb0893-mixing"
		   *fazendas-mixer*
		   (vector (band *feb0893* "b1")
			   (band *feb0893* "b2")
			   (band *feb0893* "b3")
			   (band *feb0893* "b4")
			   (band *feb0893* "b5")
			   (band *feb0893* "b7")))))


(make-imagegram (band *feb0893* "b4"))

(prof:with-profiling ()
  (time
   (compute-fractions *feb0893-mixing*)))
(prof:show-call-graph)
(progn
  (time (prof:with-profiling ()
	  (compute-fractions1 *feb0893-mixing*)))
  (prof:show-call-graph)
  (make-rotor (fraction *feb0893-mixing* "dimona")
	      (fraction *feb0893-mixing* "vegsh")
	      (fraction *feb0893-mixing* "sasp")
	      :fractions? t)
  (make-imagegram (fraction *feb0893-mixing* "vegsh"))
  (make-imagegram (fraction *feb0893-mixing* "vegsh")
		  :width *iw* :height *ih*)
  (time
   (multiple-value-setq (*fd0* *fd1*)
     (split-space *feb0893-band-space*
		   (fraction *feb0893-mixing* "vegsh") 152)))
  (make-imagegram (fraction *feb0893-mixing* "vegsh") :band-space *fd0*
		  :width *iw* :height *ih*)
  (make-imagegram (fraction *feb0893-mixing* "vegsh") :band-space *fd1*
		  :width *iw* :height *ih*)
  (time
   (multiple-value-setq (*fd00* *fd10*)
     (split-space *fd0* (fraction *feb0893-mixing* "sasp") 96)))
  (make-imagegram (fraction *feb0893-mixing* "vegsh") :band-space *fd00*
		  :width *iw* :height *ih*)
  (make-imagegram (fraction *feb0893-mixing* "vegsh") :band-space *fd10*
		  :width *iw* :height *ih*)
  (time 
   (multiple-value-setq (*fd010* *fd110*)
     (split-space *fd10* (fraction *feb0893-mixing* "dimona") 104)))
  (make-imagegram (fraction *feb0893-mixing* "vegsh") :band-space *fd010*
		  :width *iw* :height *ih*)
  (make-imagegram (fraction *feb0893-mixing* "vegsh") :band-space *fd110*
		  :width *iw* :height *ih*)
  (defparameter *p* (partitioning *fd0*))
  (make-part-browser *p*))

(make-imagegram (band *feb0893* "vegsh") :width 1234 :height 604)

(defparameter *r*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "quag") 
     (band *feb0893* "plra") 
     :left 500 :top 4 :fractions? t))

(dotimes (h $rw$)
  (print h) (force-output)
  (setf (hither *r*) h))


(multiple-value-setq (*fd0* *fd1*)
  (split-space *feb0893-band-space*
		(band *feb0893* "vegsh") 153))


(defparameter *r0*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "plra") 
     (band *feb0893* "quag") 
     :left 500 :top 4 :fractions? t :band-space *fd0*))

(make-imagegram (band *feb0893* "vegsh") :width 1234 :height 604
		:band-space *fd0*)


(defparameter *r1*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "plra") 
     (band *feb0893* "quag") 
     :left 500 :top 4 :fractions? t :band-space *fd1*))

(make-imagegram (band *feb0893* "vegsh") :width 1234 :height 604
		:band-space *fd1*)


(make-histogram (band *feb0893* "vegsh") :width 256 :height 128)
(make-histogram (band *feb0893* "dimona") :width 256 :height 128)
(make-histogram (band *feb0893* "plra") :width 256 :height 128)
(make-histogram (band *feb0893* "quag") :width 256 :height 128)

(progn
(make-logogram (band *feb0893* "vegsh") :width 256 :height 128)
(make-logogram (band *feb0893* "dimona") :width 256 :height 128)
(make-logogram (band *feb0893* "plra") :width 256 :height 128)
(make-logogram (band *feb0893* "quag") :width 256 :height 128))

#||



(defparameter wins
    (loop
	with d = 32
	for i from 0
	for l = (+ 128 (* i d))
	for names on (list "dimona" "plra" "quag" "vegsh")
	for f0 = (first names)
	for b0 = (band *feb0893* f0)
	collect
	  (make-histogram b0 :left l :top (+ l d) :width 256 :height 128)
	do (loop 
	       for j from i
	       for f1 in (rest names)
	       for b1 = (band *feb0893* f1)
	       do (make-repscatgram b0 b1))))

(defparameter *r0*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "plra") 
     (band *feb0893* "quag") 
     :left 500 :top 4))

;;;-------------------------------------------------------------

(make-repscatgram (band *fsub* "b4") (band *fsub* "b5"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b4"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b5"))

(defparameter *fsub345*
    (make-rotor
     (band *fsub* "b3") (band *fsub* "b4") (band *fsub* "b5")
     :left 500 :top 4))

(make-imagegram (band *fazendas* "b4"))


||#

