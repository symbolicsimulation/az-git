;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package "MU") 

;;;============================================================
;;; constants

(defconstant -rotor-width- 363 "Width of unmagnified rotor window.")

(defconstant -rotor-height- 256 "Height of unmagnified rotor window.")



