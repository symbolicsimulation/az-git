;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
(in-package :Mu) 

#||
(compile-file "spectra" :load t)


(print-spectral-library-for-xgobi lib0 "/home7/jam/rsl/jr4-92")
(print-spectral-library-for-xgobi lib0 "/belgica-4g/jam/rsl/spectra/jr4-92" 5)
(read-spectral-library "~jam/rsl/spectra/jr4-92.sub")
(read-spectral-library "/rsl/spectra/jr4-92.swp")

(defparameter lib0 
    (read-spectral-library "/belgica-4g/jam/rsl/spectra/jr4-92.avr"))

(defparameter lib1
    (make-instance 'Spectral-Library
      :wavelengths (wavelengths lib0)
      :spectra (remove-if 
		#'(lambda (s) 
		    (member (spectrum-name s) 
			    '("SASP0010" "SASP0011" "SASP0014" "SASP0015") 
			    :test #'string-equal))
		(spectra lib0))))


(defparameter lib2
    (make-instance 'Spectral-Library
      :wavelengths (wavelengths lib0)
      :spectra (remove-if 
		#'(lambda (s) 
		    (member (spectrum-name s) 
			    '("SASP0010" "SASP0011" "SASP0014" "SASP0015"
			      "DWRHY" "RORHY" 
			      "DIMONA4A" "DIMONA4B" "DIMONA4C" 
			      "HEMA-bec" "GF1X6F2-bec" "GF1X6F3-bec"
			      "CACO3-bec"
			      "GF67NHE"
			      "NDF2Y2L" 
			      "MONT" "ALUN" "KAOL" "OHIA" "FESMECT") 
			    :test #'string-equal))
		(spectra lib0))))

(print-spectral-library-for-xgobi lib0 "/home7/jam/rsl/jr0" 5)
(print-spectral-library-for-xgobi lib1 "/home7/jam/rsl/jr1" 5)
(print-spectral-library-for-xgobi lib2 "/home7/jam/rsl/jr2" 5)

||#

;;;----------------------------------------------------
#||
(eval-when (compile load eval)
  (load "../az/package")
  (load "../az/macros"))
||#

;;;----------------------------------------------------
;;; this assumes that code-char and char-code use ascii.

#+(or :cmu :excl)
(defun ascii-char (ascii) 
  "Convert an ascii code to a character object."
  (declare (type Fixnum ascii)
	   (:returns (type Character)))
  (code-char ascii))

#+(or :cmu :excl)
(defun char-ascii (char) 
  "Convert a character object to its ascii code."
  (declare (type Character char)
	   (:returns (type Fixnum)))
  (char-code char))

(defun read-ascii (stream) 
  "Read a byte from stream, interpret it as an ascii code, and return
the corresponding character object."
  (declare (type Stream stream) 
	   (:returns (type Character))) 
  (ascii-char (read-byte stream)))

(defun read-swapped-ascii-2 (stream)
  "Read two bytes from stream, interpret the first as an ascii code, 
and return the corresponding character object."
  (declare (type Stream stream)
	   (:returns (type Character)))
  (prog1 (ascii-char (read-byte stream))
    (read-byte stream)))

(defun read-2-swapped-bytes (stream)
  "Read a 16 bit integer from stream with the most significant byte second."
  (declare (type Stream stream)
	   (:returns (type Fixnum)))
  (logior (read-byte stream)
	  (ash (read-byte stream) 8)))

(defun read-name (stream n)
  "Read a null-terminated ascii string of length n from stream,
converting to a CL string object."
  (declare (type Stream stream)
	   (type Fixnum n))
  (let ((s (make-string n :initial-element #\Space)))
    (dotimes (i n) (setf (aref s i) (read-ascii stream)))
    (subseq s 0 (position (ascii-char 0) s))))

;;;----------------------------------------------------

(defclass Spectrum (Standard-Object)
	  ((spectrum-name
	    :type String
	    :reader spectrum-name
	    :initarg :spectrum-name)
	   (spectrum-data
	    :type (Simple-Array Fixnum (*))
	    :reader spectrum-data
	    :initarg :spectrum-data))
  (:documentation "A type for endmember spectra."))

(defun make-spectrum (name data)
  (declare (type String name)
	   (type (Simple-Array Fixnum (*)) data)
	   (:returns (type Spectrum)))
  (make-instance 'Spectrum :spectrum-name name :spectrum-data data))

(defmethod print-object ((spectrum Spectrum) stream)
  (az:printing-random-thing
   (spectrum stream)
   (format stream "~a" (spectrum-name spectrum))))

;;;----------------------------------------------------

(defclass Spectral-Library (Standard-Object)
	  ((path
	    :type (or Pathname String)
	    :accessor path)
	   (filename
	    :type String
	    :accessor filename
	    :initform (make-string 24 :initial-element #\Space))
	   (datatype
	    :type Character
	    :accessor datatype)
	   (gain
	    :type Fixnum
	    :accessor gain)
	   (compress?
	    :type Character
	    :accessor compress?)
	   (wavelength-filename
	    :type String
	    :accessor wavelength-filename)
	   (wavelengths
	    :type Sequence
	    :accessor wavelengths
	    :initarg :wavelengths)
	   (spectra
	    :type Sequence
	    :accessor spectra
	    :initarg :spectra))
  
  (:documentation
   "An object holding an endmember library."))

;;;----------------------------------------------------

(defun read-spectrum-data (stream nbands)
  (let ((spectrum (make-array nbands 
			      :initial-element 0
			      :element-type 'Fixnum)))
    (dotimes (i nbands)
      (setf (aref spectrum i) (read-2-swapped-bytes stream)))
    spectrum))

;;;----------------------------------------------------

(defun read-spectral-library (path)
  (declare (type (or Pathname String) path))
  (with-open-file (stream path
		   :direction :input
		   :if-does-not-exist :error
		   :element-type '(Unsigned-Byte 8))
    (declare (type Stream stream))
    (let ((lib (make-instance 'Spectral-Library))
	  (nspectra (read-2-swapped-bytes stream)) ;2
	  (nbands (read-2-swapped-bytes stream))) ;4 
      (declare (type Spectral-Library lib))
      (setf (path lib) path)
      
      (setf (datatype lib) (read-swapped-ascii-2 stream)) ;6
      (setf (gain lib) (read-2-swapped-bytes stream)) ;8
      (setf (compress? lib) (read-swapped-ascii-2 stream)) ;10
      (setf (filename lib) (read-name stream 24)) ;34
      (dotimes (i (- 256 34)) (read-byte stream))
      
      (setf (wavelength-filename lib) (read-name stream 48))
      (setf (wavelengths lib) (make-array nbands :element-type 'Fixnum))
      (dotimes (i nbands)
	(setf (aref (wavelengths lib) i) (read-2-swapped-bytes stream)))
      (setf (spectra lib) ())
      (dotimes (i nspectra)
	(push (make-spectrum (read-name stream 48)
			     (read-spectrum-data stream nbands))
	      (spectra lib)))
      (setf (spectra lib) (nreverse (spectra lib)))    
      (describe lib)
      lib)))

;;;----------------------------------------------------

(defun print-spectrum-data (spectrum stream &optional (inc 1))
  (declare (type Spectrum spectrum)
	   (type Stream stream))
  (let ((i 0)
	(data (spectrum-data spectrum)))
    (declare (type Fixnum i))
    (loop (when (>= i (length data)) (return))
      (princ (aref data i) stream)
      (princ #\Space stream)
      (incf i inc))))

;;;----------------------------------------------------

(defun print-spectral-library-for-xgobi (lib filename &optional (inc 1))

  (declare (type Spectral-Library lib)
	   (type String filename)
	   (type Fixnum inc))
  
  (with-open-file (row-stream (concatenate 'String filename ".row")
		   :direction :output
		   :if-does-not-exist :create
		   :if-exists :supersede )
    (declare (type Stream row-stream))
    (with-open-file (data-stream (concatenate 'String filename ".dat")
		     :direction :output
		     :if-does-not-exist :create
		     :if-exists :supersede )
      (declare (type Stream data-stream))
      (dotimes (i (length (spectra lib))) 
	(princ (spectrum-name (elt (spectra lib) i)) row-stream)
	(fresh-line row-stream)
	(print-spectrum-data (elt (spectra lib) i) data-stream inc)
	(fresh-line data-stream))))
      
  (with-open-file (col-stream (concatenate 'String filename ".col")
		   :direction :output
		   :if-does-not-exist :create
		   :if-exists :supersede )
    (declare (type Stream col-stream))
    (let ((nbands (length (wavelengths lib)))
	  (i 0))
      (declare (type Fixnum i))
      (loop (when (>= i nbands) (return))
	(princ (elt (wavelengths lib) i) col-stream)
	(incf i inc)
	(fresh-line col-stream)))))

