;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

(time (xlt:raw-dump (clay:diagram-window *i*) "dump.raw"))
(time (xlt:raw-dump (xlt:drawable-root (clay:diagram-window *i*)) "root.raw"))

(setf *host* "fram")
(make-imagegram *fsub* :left 400 :host *host*)
(setf *i* (make-imagegram (band *fsub* "b4") :left 500
			  :host *host*))

(wt:winspect *i*)
(defparameter *fsub345*
    (make-rotor (band *fsub* "b3") (band *fsub* "b5") (band *fsub* "b4")
		:left 600 :top 4 :host *host*))

(loop for i0 from 0
    for names on (list ;; "b1" "b2" 
		  "b3" "b4" "b5" "b7")
    for f0 = (first names) do
      (make-histogram (band *fsub* f0) :left 500 :height 128 :top (* i0 140)
		      :host *host*)
      (loop for j0 from i0
	  for f1 in (rest names) do
	    (make-repscatgram (band *fsub* f0) (band *fsub* f1)
			      :host *host*
			      :left (* i0 158) :top (* j0 180))))

#||
(time (compute-fractions *fsub-mixing*))
(time (compute-fractions *fazendas-mixing*))
(time (display-fractions *fsub-mixing*))

(progn
  (multiple-value-setq (*fd0* *fd1*)
    (split-space *fsub-band-space* (fraction *fsub-mixing* "vegsh") 153))
  (multiple-value-setq (*fd00* *fd10*)
    (split-space *fd0* (fraction *fsub-mixing* "sasp") 96))
  (multiple-value-setq (*fd010* *fd110*)
    (split-space *fd10* (fraction *fsub-mixing* "dimona") 104))
  
  (make-imagegram (fraction *fsub-mixing* "vegsh") 
		  :host *host*)
  (make-imagegram (fraction *fsub-mixing* "dimona")
		  :host *host*)
  (make-imagegram (fraction *fsub-mixing* "plra")
		  :host *host*)
  (make-imagegram (fraction *fsub-mixing* "sasp")
		  :host *host*)
  
  (defparameter *r* (make-rotor (fraction *fsub-mixing* "dimona")
				(fraction *fsub-mixing* "vegsh")
				(fraction *fsub-mixing* "sasp")
				:fractions? t
				:host *host*))
  
  (make-histogram (fraction *fsub-mixing* "vegsh") :height 128
		  :host *host*)
  (make-histogram (fraction *fsub-mixing* "sasp") :band-space *fd0*
		  :height 128
		  :host *host*)
  (make-histogram (fraction *fsub-mixing* "dimona") :band-space *fd10*
		  :height 128
		  :host *host*)
  
  ;;(make-imagegram *fsub* :band-space *fd0*)
  ;;(make-imagegram *fsub* :band-space *fd1*)
  ;;(make-imagegram *fsub* :band-space *fd00*)
  ;;(make-imagegram *fsub* :band-space *fd10*)
  ;;(make-imagegram *fsub* :band-space *fd110*)
  ;;(make-imagegram *fsub* :band-space *fd010*)

  (defparameter *p* (partitioning *fd0*))
  (defparameter *pb* (make-part-browser *p* :left 500 :width 640 :height 580
					:host *host* ))
  )

(time (xlt:raw-dump (clay:diagram-window *pb*) "tree.raw"))
(time (xlt:raw-dump (clay:diagram-window *r*) "rotor.raw"))

(dotimes (h -rotor-width-)
  (print h) (force-output)
  (setf (hither *r*) h)
  (xlt:drawable-force-output (clay:diagram-window *r*)))

(dotimes (k 64)
  (print (cons (incf (angle *r*)
		     (angular-resolution *r*))
	       (setf (min-count *r*) k)))
   (xlt:drawable-force-output (clay:diagram-window *r*))
  (finish-output))
(progn (display-fractions *fsub-mixing* :band-space *fd0*))
(make-rotor (fraction *fsub-mixing* "dimona")
	    (fraction *fsub-mixing* "sasp")
	    (fraction *fsub-mixing* "plra")
	    :band-space *fd0*)



(setf (clay:paintbrush-aspect clay:*paintbrush*) :hue)
(setf (clay:paintbrush-aspect clay:*paintbrush*) :sat)
(setf (clay:paintbrush-aspect clay:*paintbrush*) :hue-sat)

(dolist (b '("b1" "b2" "b3" "b4" "b5" "b7"))
  (make-logogram (band *fsub* b)))

(dolist (b (list "b1" "b2" "b3" "b4" "b5" "b7"))
  (make-rootogram (band *fsub* b)))

(dolist (b (list "b1" "b2" "b3" "b4" "b5" "b7"))
  (make-histogram (band *fsub* b)))

(time
 (az:large-allocation
  (let ((fracs (list "dimona" "plra" "sasp" "vegsh")))
    (loop for i0 from 0
	for names on fracs
	for f0 = (first names) do
	  (loop for f1 in (rest names)
	      for j0 from i0 do
		(make-barygram (fraction *fsub-mixing* f0)
			       (fraction *fsub-mixing* f1)
			       :left (* i0 158) :top (* j0 180)))))))

(make-imagegram
 (fraction *fsub-mixing* "vegsh")
 :left 500 :width (* 2 257) :height (* 2 167))

(dolist (name (list "dimona" "plra" "sasp" "vegsh"))
  (compare-fraction-image (band-vector (fraction *fsub-mixing* name))
			  (band-vector (band *fsub* name))))

(multiple-value-setq (*fd0* *fd1*)
  (split-space *fsub-band-space* (fraction *fsub-mixing* "vegsh") 155))
(multiple-value-setq (*fd00* *fd10*)
  (split-space *fd0* (fraction *fsub-mixing* "sasp") 96))
(multiple-value-setq (*fd010* *fd110*)
  (split-space *fd10* (fraction *fsub-mixing* "dimona") 104))

(time (display-fractions *fsub-mixing* :band-space *fd0*))
(progn
  (make-histogram (fraction *fsub-mixing* "vegsh") :height 128)
  (make-histogram (fraction *fsub-mixing* "sasp") :band-space *fd0*
		  :height 128)
  (make-histogram (fraction *fsub-mixing* "sasp") :band-space *fd1*
		  :height 128)
  (make-histogram (fraction *fsub-mixing* "dimona") :band-space *fd10*
		  :height 128)
  (make-histogram (fraction *fsub-mixing* "dimona") :band-space *fd00*
		  :height 128))

(progn
  (make-imagegram (fraction *fsub-mixing* "vegsh"))
  (make-imagegram (fraction *fsub-mixing* "sasp") :band-space *fd0*)
  (make-imagegram (fraction *fsub-mixing* "sasp") :band-space *fd1*)
  (make-imagegram (fraction *fsub-mixing* "dimona") :band-space *fd10*)
  (make-imagegram (fraction *fsub-mixing* "dimona") :band-space *fd00*))

(dolist (name (list "dimona" "plra" "sasp" "vegsh"))
   (make-histogram (fraction *fsub-mixing* name)
		   :band-space *fd0* :height 128))


(let ((i 0))
  (dolist (name (list "dimona" "plra" "sasp" "vegsh"))
    (make-histogram (fraction *fsub-mixing* name)
		    :band-space *fd010*
		    :left 256 :top (* i 150) :width 256 :height 128)
    (make-histogram (fraction *fsub-mixing* name)
		    :band-space *fd110*
		    :left 520 :top (* i 150) :width 256 :height 128)
    (incf i)))

(let ((i 0))
  (dolist (name (list "dimona" "plra" "sasp" "vegsh"))
    (make-histogram (fraction *fsub-mixing* name)
		    :left 256 :top (* i 150) :width 256 :height 128)
    (incf i)))

(let ((i 0))
  (dolist (name (list "dimona" "plra" "sasp" "vegsh"))
    (make-histogram (fraction *fsub-mixing* name)
		    :band-space *fd10*
		    :left 520 :top (* i 150) :width 256 :height 128)
    (incf i)))


(wt:winspect *fd0*)
(wt:winspect *fd1*)
(wt:winspect *fd00*)
(wt:winspect *fd01*)
(wt:winspect *fsub-mixing*)
(wt:winspect *fsub-band-space*)
(make-histogram (fraction *fsub-mixing* "vegsh")
		:band-space *fd0*
		:left 500 :width 512 :height 128)

(make-histogram (fraction *fsub-mixing* "dimona")
		:band-space *fd0*
		:left 500 :width 512 :height 128)

(make-rootogram (fraction *fsub-mixing* "dimona")
		:band-space *fd0*
		:left 500 :width 512 :height 128)

(make-histogram (fraction *fsub-mixing* "dimona")
		:left 500 :width 512 :height 128)
(defparameter *r*
    (make-rotor (fraction *fsub-mixing* "dimona")
		(fraction *fsub-mixing* "sasp")
		(fraction *fsub-mixing* "plra")))

(make-imagegram (band *fsub* "b4")
		:left 500 :width (* 2 257) :height (* 2 167))


(make-rotor (band *fsub* "b3") (band *fsub* "b4") (band *fsub* "b5"))



(dolist (name '("dimona" "plra" "sasp" "vegsh"))
  (compare-fraction-image (band-vector (fraction *fsub-mixing* name))
			  (band-vector (band *fsub* name))))

(wt:winspect *fsub-mixer*)

||#
  


