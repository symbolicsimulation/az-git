;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

;;;-------------------------------------------------------------


(multiple-value-setq (*fd0* *fd1*)
  (split-space *feb0893-band-space*
		(band *feb0893* "vegsh") 153))


(defparameter *r0*
    (make-rotor
     (band *fd0* "dimona") 
     (band *fd0* "plra") 
     (band *fd0* "quag") 
     :left 500 :top 4))
#||
(defparameter *w* 1234)
(defparameter *h* 604)

(defparameter *feb0893-band-space*
    (make-instance 'Complete-Band-Space
      :name "Fazendas demo Feb 08 93"
      :coordinate-dimensions (list *h* *w*)
      :documentation "A Band-Space for the Fazendas Landsat TM images."))


(defparameter *feb0893-format*
    (make-basic-file-format
     :file-nrows *h*
     :file-ncols *w*
     :header-bytes 0
     :documentation 
     "A Basic-File-Format for the Feb 08 93 Landsat TM images."))

(defparameter *feb0893*
    (make-mu :directory-name "/images/manaus/"
		:file-names (list "fazendas-90-b1-reg" 
				  "fazendas-90-b2-reg" 
				  "fazendas-90-b3-reg"
				  "fazendas-90-b4-reg" 
				  "fazendas-90-b5-reg" 
				  "fazendas-90-b7-reg"
				  "fazendas-90-register-rms" 
				  "fazendas-90-register-dimona5a-89"
				  "fazendas-90-register-plra0002-89" 
				  "fazendas-90-register-quag22stack-89"
				  "fazendas-90-register-vegsh89")
		:file-format *feb0893-format*
		:spectral-space *landsat-tm-spectral-space*
		:band-space *feb0893-band-space*))

(make-imagegram (band *feb0893* "vegsh") :width 1234 :height 604)

(defparameter wins
    (loop
	with d = 32
	for i from 0
	for l = (+ 128 (* i d))
	for names on (list "dimona" "plra" "quag" "vegsh")
	for f0 = (first names)
	for b0 = (band *feb0893* f0)
	collect
	  (make-histogram b0 :left l :top (+ l d) :width 256 :height 128)
	do (loop 
	       for j from i
	       for f1 in (rest names)
	       for b1 = (band *feb0893* f1)
	       do (make-repscatgram b0 b1))))

(defparameter *r0*
    (make-rotor
     (band *feb0893* "dimona") 
     (band *feb0893* "plra") 
     (band *feb0893* "quag") 
     :left 500 :top 4))

;;;-------------------------------------------------------------

(make-repscatgram (band *fsub* "b4") (band *fsub* "b5"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b4"))
(make-repscatgram (band *fsub* "b3") (band *fsub* "b5"))

(defparameter *fsub345*
    (make-rotor
     (band *fsub* "b3") (band *fsub* "b4") (band *fsub* "b5")
     :left 500 :top 4))

(make-imagegram (band *fazendas* "b4"))


||#

