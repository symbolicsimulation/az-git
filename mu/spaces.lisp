;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;;============================================================
;;; Spatial Space spaces for Image bands
;;;============================================================

(defclass Band-Space (g:Byte-Lattice Mu-Object)
	  ((indexes
	    :type List
	    :accessor indexes
	    :initform ())
	   (work-pins
	    :type (or Null az:Card28-Vector)
	    :initform nil)
	   (band-histograms
	    :type Hash-Table
	    :reader band-histograms
	    :initform (make-hash-table :test #'eq)
	    :documentation
	    "A cache for histograms of image bands.")
	   (band-integrams
	    :type Hash-Table
	    :reader band-integrams
	    :initform (make-hash-table :test #'eq)
	    :documentation
	    "A cache for integrams of image bands. An integram is the
             cumulative sum, or partial integral, of a histogram.")
	   (2band-histograms
	    :type Hash-Table
	    :reader 2band-histograms
	    :initform (make-hash-table :test #'eq)
	    :documentation
	    "A cache for histograms of image bands.")
	   (2band-integrams
	    :type Hash-Table
	    :reader 2band-integrams
	    :initform (make-hash-table :test #'eq)
	    :documentation
	    "A cache for integrams of image bands. An integram is the
             cumulative sum, or partial integral, of a histogram.")
	   (2band-histobarys
	    :type Hash-Table
	    :reader 2band-histobarys
	    :initform (make-hash-table :test #'eq)
	    :documentation
	    "A cache for histobarys of image bands.")
	   (2band-intebarys
	    :type Hash-Table
	    :reader 2band-intebarys
	    :initform (make-hash-table :test #'eq)
	    :documentation
	    "A cache for intebarys of image bands. An intebary is the
             cumulative sum, or partial integral, of a histobary."))
  (:documentation
   "A class for objects representing space spaces for the bands
of multiband images."))

;;;------------------------------------------------------------

(defun work-pins (space)
  (declare (type band-space space))
  (let ((work (slot-value space 'work-pins)))
    (when (null work)
      (setf work (az:make-card28-vector (g:dimension space)))
      (setf (slot-value space 'work-pins) work))
    work))

;;;------------------------------------------------------------

(defgeneric root-space (space)
  (declare (type Band-Space space)
	   (:returns (type Complete-Band-Space)))
  (:documentation
   "This is the ancestral space of any spatial space.
A Complete-Band-Space is its own root-space."))

(defgeneric mask (space)
  (declare (type Band-Space space)
	   (:returns (type az:Card16)))
  (:documentation
   "A <mask> generic function and default method are provided so that
functions can be written to operate on Complete-Band-Spaces
and Band-Space-Biparts uniformly.")
  (:method
   ((space Band-Space))
   0))

(defgeneric key (space)
  (declare (type Band-Space space)
	   (:returns (type az:Card16)))
  (:documentation
   "A <key> generic function and default method are provided so that
functions can be written to operate on Complete-Band-Spaces
and Band-Space-Biparts uniformly.")

  (:method
   ((space Band-Space))
   0))

(defgeneric level (space)
  (declare (type Band-Space space)
	   (:returns (type az:Card16)))
  (:documentation
   "A <level> generic function and default method are provided so that
functions can be written to operate on Complete-Band-Spaces
and Band-Space-Biparts uniformly.")(:method
   ((space Band-Space))
   0))

;;;------------------------------------------------------------

(defgeneric band-space (mu-object)
  (declare (type Mu-Object mu-object)
	   (:returns Band-Space))
  (:documentation
   "Many Mu-Object's have a spatial space associated with them.
This generic function is used to get it."))

(defun spatial-dimension (mu-object)
  (declare (type Mu-Object mu-object))
  (g:dimension (band-space mu-object)))

(defgeneric space-pins (space perm)
  (declare (type Band-Space space)
	   (type az:Card28-Vector perm)
	   (:returns perm))
  (:documentation
   "Fill <perm> with the Pins (pixel indexes) of the pixels in the space."))

;;;============================================================
;;; Complete Band-Spaces
;;;============================================================

(defclass Complete-Band-Space (Band-Space)
	  ((coordinate-dimensions
	    :type List ;; of Pinxy
	    :reader coordinate-dimensions
	    :initarg :coordinate-dimensions)
	   (paint-image
	    :type (or Null xlib:Image-Z)
	    :initform nil))
  (:documentation
   "A Complete-Band-Space includes all the pixels in its rectangular
region. This is in constrast to the Subspaces defined below."))

;;;------------------------------------------------------------
;;; lazy pseudo slots

(defun make-paint-image (nrows ncols)
  (declare (type az:Card16 nrows ncols))
  (az:large-allocation
   (xlib:create-image
    :data (az:make-card8-matrix nrows ncols
			     :initial-element (xlt:hue-sat-name->hs :gray))
    :format :z-pixmap :depth 8 :height nrows :width ncols)))

(defun paint-image (space)
  (declare (type Band-Space space))
  (let* ((root-space (root-space space))
	(image (slot-value root-space 'paint-image)))
    (declare (type Complete-Band-Space root-space)
	     (type (or Null xlib:Image-Z) image))
    (when (null image)
      (setf image (make-paint-image (nrows root-space) (ncols root-space)))
      (setf (slot-value root-space 'paint-image) image))
    image))

(defun paint-matrix (space)
  (declare (type Band-Space space))
  (the az:Card8-Matrix
    (xlib:image-z-pixarray
     (the xlib:Image-Z (paint-image (root-space space))))))

(defun paint-vector (space)
  (declare (type Band-Space space))
  (the az:Card8-Vector
    (az:array-data
     (the az:Card8-Matrix
       (xlib:image-z-pixarray
	(the xlib:Image-Z (paint-image (root-space space))))))))

;;;------------------------------------------------------------

(defmethod initialize-instance :after ((dom Complete-Band-Space)
				       &rest initargs)

  (declare (type Complete-Band-Space dom)
	   (type List initargs)
	   (ignore initargs))
  
  (setf (slot-value dom 'g:dimension)
    (apply #'* (coordinate-dimensions dom))))

;;;------------------------------------------------------------

(defmethod root-space ((dom Complete-Band-Space))
  "A Complete-Band-Space is its own root-space space."
  dom)

;;;------------------------------------------------------------

(defun rank (dom)
  (declare (type Band-Space dom))
  (length (coordinate-dimensions (root-space dom))))
	   
(defun coordinate-dimension (dom i)
  (declare (type Band-Space dom)
	   (type az:Card16 i))
  (elt (coordinate-dimensions (root-space dom)) i))

(defgeneric nrows (mu-object)
  (declare (type Mu-Object mu-object))
  (:method ((mo Mu-Object)) (coordinate-dimension (band-space mo) 0))
  (:method ((sd Band-Space)) (coordinate-dimension (root-space sd) 0))
  (:method ((sd Complete-Band-Space)) (coordinate-dimension sd 0)))

(defgeneric ncols (mu-object)
  (declare (type Mu-Object mu-object))
  (:method ((mo Mu-Object)) (coordinate-dimension (band-space mo) 1))
  (:method ((sd Band-Space)) (coordinate-dimension (root-space sd) 1))
  (:method ((sd Complete-Band-Space)) (coordinate-dimension sd 1)))

;;;------------------------------------------------------------

(defmethod space-pins ((space Complete-Band-Space) perm)
  (declare (type az:Card28-Vector perm))
  (assert (= (the az:Card28 (length perm))
	     (the az:Card28 (g:dimension space))))
  (az:iota-card28-vector perm))

;;;============================================================
;;; Sub spatial spaces
;;;============================================================

(defclass Sub-Band-Space (Band-Space)
	  ((parent-space
	    :type Complete-Band-Space
	    :reader parent-space
	    :initarg :parent-space
	    :documentation
	    "The space from which this subset was selected."))
  (:documentation
   "A subset of a complete spatial space."))

;;;------------------------------------------------------------

(defmethod root-space ((space Sub-Band-Space))
  (root-space (parent-space space)))

;;;============================================================
;;; Masked Sub spatial spaces
;;;============================================================

(defclass Masked-Band-Space (Sub-Band-Space)
	  ((mask-matrix
	    :type az:Card8-Matrix
	    :reader mask-matrix
	    :initarg :mask-matrix
	    :documentation
	    "A matrix of 0-1's indicating which pixels belong to the subset."
	    )))

;;;------------------------------------------------------------

(defun make-masked-band-space-bipart (parent mask)

  (declare (type Band-Space parent)
	   (type az:Card8-Matrix mask))
  
  (let* ((name  (format nil "Masked Child of ~a" (name parent)))
	 (subset (make-instance 'Masked-Band-Space
		   :name name
		   :parent-space parent
		   :mask mask)))
    subset))

;;;------------------------------------------------------------

(defun mask-vector (x)
  "A vector of bytes indicating which pixels belong to which subsets."
  (declare (type Masked-Band-Space x)
	   (:returns (type az:Card8-Vector)))
  (az:array-data (mask-matrix x)))

;;;------------------------------------------------------------

(defmethod g:dimension ((space Masked-Band-Space))
  (declare (type Masked-Band-Space space))
  (count 1 (mask space)))

;;;------------------------------------------------------------

(defmethod space-pins ((space Masked-Band-Space) perm)
  (declare (type az:Card28-Vector perm))
  (assert (= (the az:Card28 (length perm))
	     (the az:Card28 (g:dimension space))))
  (let ((mask (mask space))
	(j 0))
    (declare (optimize (safety 0) (speed 3))
	     (type az:Card8-Vector mask)
	     (type az:Card28 j))
    (dotimes (i (length mask))
      (declare (type az:Card28 i))
      (when (= 1 (aref mask i))
	(setf (aref perm j) i)
	(incf j)))
    perm))
 
;;;============================================================
;;; Paritioning Spatial Spaces
;;;============================================================

(defclass Partitioning (Mu-Object)
	  ((root-space
	    :type Complete-Band-Space
	    :reader root-space
	    :initarg :root-space)
	   (child0
	    :type Band-Space-Bipart
	    :accessor child0
	    :initform nil
	    :documentation
	    "The first 0 bit child of the root-space.")
	   (child1
	    :type Band-Space-Bipart
	    :accessor child1
	    :initform nil
	    :documentation
	    "The first 1 bit child of the root-space.")
	   (part-matrix
	    :type az:Card16-Matrix
	    :reader part-matrix
	    :initarg :part-matrix
	    :documentation
	    "A matrix of keys indicating which pixels belong
             to which subsets.")
	   (gra:edges
	    :type List
	    :accessor gra:edges
	    :initform ()
	    :documentation
	    "A list of conses, of the form (parent . child), for the graph
             browser. We need to hold this here because the graph browser
             uses eq to test identity of edges in the graph."))
  (:documentation
   "A Partitioning divides a spatial space into a number of subsets,
each corresponding to a given 16 bit value in the part array."))

;;;------------------------------------------------------------

(defun make-partitioning (root-space)
  "The constructor function for Partitionings."
  (declare (type Complete-Band-Space root-space))
  (make-instance 'Partitioning
    :name (format nil "~a Partitioning" (name root-space))
    :root-space root-space
    :part-matrix (az:make-card16-matrix
		  (nrows root-space) (ncols root-space))))

;;;------------------------------------------------------------

(defmethod gra:nodes ((p Partitioning))
  (cons (root-space p)
	(nconc
	 (space-descendents (child0 p))
	 (space-descendents (child1 p)))))

(defmethod gra:directed-graph? ((p Partitioning)) t)

;;;============================================================

(defclass Band-Space-Bipart (Sub-Band-Space)
	  ((parent-space
	    :type Band-Space
	    :reader parent-space
	    :initarg :parent-space
	    :documentation
	    "The spatial space of which this is 1/2 of a binary split.")
	   (child0
	    :type Band-Space-Bipart
	    :accessor child0
	    :initform nil
	    :documentation
	    "This is the zero bit descendent of this partition.")
	   (child1
	    :type Band-Space-Bipart
	    :accessor child1
	    :initform nil
	    :documentation
	    "This is the 1 bit descendent of this partition.")
	   (partitioning
	    :type Partitioning
	    :reader partitioning
	    :initarg :partitioning
	    :documentation
	    "Used to define which pixels are in the subspace.")
	   (key
	    :type az:Card16
	    :reader key
	    :initarg :key
	    :documentation
	    "The subspace is the pixels s.t. (= (logand x mask) key) 
             where <x> is the corresponding entry in the partition data.")
	   (mask
	    :type az:Card16
	    :reader mask 
	    :initarg :mask
	    :documentation
	    "The subspace is the pixels s.t. (= (logand x mask) key) 
             where <x> is the corresponding entry in the partition data.")
	   (level
	    :type (Integer 0 16)
	    :reader level
	    :initarg :level
	    :documentation
	    "The depth of this subset in the binary partitioning tree.")))

;;;------------------------------------------------------------

(defmethod root-space ((space Band-Space-Bipart))
  (root-space (partitioning space)))

(defmethod space-descendents (space)
  (declare (type (or Null Band-Space-Bipart) space))
  (when space
    ;; returns nil if space is nil
    (cons space
	  (nconc
	   (space-descendents (child0 space))
	   (space-descendents (child1 space))))))

;;;------------------------------------------------------------

(defgeneric subspace? (space0 space1)
  (declare (type Band-Space space0 space1))
  (:documentation "Is space0 a subspace of space1?"))

(defmethod subspace? ((space0 Complete-Band-Space)
		       (space1 Band-Space))
  "A Complete-Band-Space cannot be a subspace of any other."
  nil)

(defmethod subspace? ((space0 Band-Space-Bipart)
		       (space1 Complete-Band-Space))
  "A Band-Space-Bipart is a subspace of its root-space"
  (eq (root-space space0) space1))

(defmethod subspace? ((space0 Band-Space-Bipart)
		       (space1 Band-Space-Bipart))
  "A Band-Space-Bipart is a subspace of its parent and any ancestors."
  (or (eq (parent-space space0) space1)
      (subspace? (parent-space space0) space1)))

(defun superspace? (space0 space1)
  "Is space0 a superspace of space1?"
  (declare (type Band-Space space0 space1))
  (subspace? space1 space0))

;;;------------------------------------------------------------

(defun make-band-space-bipart (parent-space partitioning branch)

  (declare (type Band-Space parent-space)
	   (type Partitioning partitioning)
	   (type (Integer 0 1) branch))

  (let* ((level (+ 1 (level parent-space)))
	 (key (ecase branch
		(0 (key parent-space))
		(1 (logior (key parent-space)
			   (the az:Card16
			     (ash 1 (the (Integer 0 16)
				      (level parent-space))))))))
	 (mask (- (ash 1 level) 1))
	 (root-space (root-space partitioning))
	 (name (format nil (format nil "~~a:~~~d,'0b" level)
		       (name root-space) key))
	 (bipart (make-instance 'Band-Space-Bipart
		   :name name
		   :parent-space parent-space
		   :partitioning partitioning
		   :level level
		   :key key
		   :mask mask)))
    (declare (type (Integer 1 16) level)
	     (type az:Card16 mask)
	     (type Complete-Band-Space root-space)
	     (type Band-Space-Bipart bipart))
    (assert (>= level (integer-length key)))
    ;; update the graph edges in the partitioning
    (push (list parent-space bipart) (gra:edges partitioning))
    (etypecase parent-space
      (Band-Space-Bipart
       (ecase branch
	 (0 (setf (child0 parent-space) bipart))
	 (1 (setf (child1 parent-space) bipart))))
      (Complete-Band-Space
       (ecase branch
	 (0 (setf (child0 partitioning) bipart))
	 (1 (setf (child1 partitioning) bipart)))))))

;;;------------------------------------------------------------

(defmethod part-matrix ((space Band-Space-Bipart))
  "A matrix of keys indicating which pixels belong to which subsets."
  (declare (:returns (type az:Card16-Matrix)))
  (part-matrix (partitioning space)))

(defun part-vector (x)
  "A vector of keys indicating which pixels belong to which subsets."
  (declare (type (or Band-Space-Bipart Partitioning) x)
	   (:returns (type az:Card16-Vector)))
  (az:array-data (part-matrix x)))

;;;------------------------------------------------------------

(defun count-pixels (space)
  (declare (optimize (safety 0) (speed 3))
	   (type Band-Space-Bipart space))
  (let ((key (key space))
	(mask (mask space))
	(part (part-vector space))
	(count 0))
    (declare (type az:Card16 key mask)
	     (type az:Card16-Vector part)
	     (type az:Card28 count))
    (dotimes (i (length part))
      (declare (type az:Card28 i))
      (when (= key (logand mask (aref part i)))
	(incf count)))
    count))

(defmethod g:dimension ((space Band-Space-Bipart))
  (count-pixels space))

;;;------------------------------------------------------------

(defmethod space-pins ((space Band-Space-Bipart) perm)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card28-Vector perm))
  (assert (= (the az:Card28 (length perm))
	     (the az:Card28 (g:dimension space))))
  (let ((key (key space))
	(mask (mask space))
	(part (part-vector space))
	(j 0))
    (declare (type az:Card16 key mask)
	     (type az:Card16-Vector part)
	     (type az:Card28 j))
    (dotimes (i (length part))
      (declare (type az:Card28 i))
      (when (= key (logand mask (aref part i)))
	(setf (aref perm j) i)
	(incf j))))
    perm)

;;;============================================================
;;; Spectral-Space spaces for Multi-Band Pixel Objects
;;;============================================================

(defclass Spectral-Space (g:Byte-Lattice Mu-Object)
	  ((coordinate-names
	    :reader coordinate-names
	    :initarg :coordinate-names
	    :type Simple-Vector))
  (:documentation
   "A class for objects representing space spaces for the pixels
of multiband images."))

(defgeneric spectral-space (mu-object)
  (declare (type Mu-Object mu-object)
	   (:returns Spectral-Space))
  (:documentation
   "Many Mu-Object's have a spectral space associated with them.
This generic function is used to get it."))

(defun spectral-dimension (mu-object)
  (declare (type Mu-Object mu-object))
  (g:dimension (spectral-space mu-object)))








