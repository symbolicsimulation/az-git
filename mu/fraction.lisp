;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; Multi-Band Band Objects
;;;============================================================

(defclass Fraction (Band)
	  ((mixing
	    :reader mixing
	    :initarg :mixing
	    :type Mixer)
	   (endmember
	    :reader endmember
	    :initarg :endmember
	    :type Endmember))

  (:documentation
   "A specialized Band object used to represent the fraction images
computed from a mixing model."))

;;;------------------------------------------------------------

(defun make-fraction (mixing endmember)
  (declare (type Mixing mixing))
  (let ((mu (mu mixing)))
    (declare (type Mu mu))
    (make-instance 'Fraction
      :mixing mixing
      :endmember endmember
      :mu mu
      :name (copy-seq (name endmember))
      :band-matrix (az:make-card8-matrix (nrows mu) (ncols mu)))))


;;;------------------------------------------------------------

(defun fraction (mixing name)
  "Get fraction object by pattern match on name"
  (declare (type Mixing mixing)
	   (type Simple-String name))
  (find name (fractions mixing) :key #'name :test #'search))

