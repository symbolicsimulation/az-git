;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defclass Diagram (clay:Interactor-Leaf Mu-Object)
	  ((band0
	    :type Band
	    :reader band0
	    :initarg :band0
	    :initarg :band
	    :documentation
	    "Only Band slot used if there's only one;
             used for the y band if there's more than one.
             Always the most significant band for sorting pixels.")
	   (band1
	    :type (or Null Band)
	    :reader band1
	    :initarg :band1
	    :documentation
            "X band slot. The second most significant slot for sorting.")
	   (band2
	    :type (or Null Band)
	    :reader band2
	    :initarg :band2
	    :documentation
	    "Z band slot. The third most significant slot for sorting.")
	   (clay:diagram-lens
	    :type clay:Lens
	    :accessor clay:diagram-lens
	    :initform (make-instance 'clay:Lens :scale 1)))
  (:default-initargs :band0 nil :band1 nil :band2 nil)
  (:documentation "An ancestor class for all Mu diagrams."))

;;;------------------------------------------------------------

(defmethod name ((diagram Diagram)) (clay:diagram-name diagram))

(defmethod band-space ((diagram Diagram)) (clay:diagram-subject diagram))

;;;============================================================
;;; Building
;;;============================================================

(defmethod make-index ((diagram Diagram))
  (make-instance 'Index
    :band-space (band-space diagram)
    :band0 (band0 diagram) :band1 (band1 diagram) :band2 (band2 diagram)))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod clay:diagram-minimum-width ((diagram Diagram))
  (if (clay:scale (clay:diagram-lens diagram))
      (floor
       (* (clay:scale (clay:diagram-lens diagram))
	  (g:w (clay:diagram-image-rect diagram))))
    ;; else
    1))

(defmethod clay:diagram-minimum-height ((diagram Diagram))
  (if (clay:scale (clay:diagram-lens diagram))
      (floor
       (* (clay:scale (clay:diagram-lens diagram))
	  (g:h (clay:diagram-image-rect diagram))))
    ;; else
    1))

(defmethod clay:make-layout-constraints ((diagram Diagram)
					 (solver clay:Layout-Solver))
  (nconc
   (brk:minimum-size (clay:layout-domain solver)
		     (clay:diagram-minimum-width diagram)
		     (clay:diagram-minimum-height diagram)
		     diagram)
   (brk:aspect-ratio (clay:layout-domain solver)
		     (g:w (clay:diagram-image-rect diagram))
		     (g:h (clay:diagram-image-rect diagram))
		     diagram)))
 
;;;--------------------------------------------------------------

(defparameter *max-indexes* 2)
(declaim (type (Integer 0 15) *max-indexes*))

(defmethod get-index ((diagram Diagram))
  (declare (optimize (safety 0) (speed 3)))
  (ac:atomic
   (let* ((b0 (band0 diagram))
	  (b1 (band1 diagram))
	  (b2 (band2 diagram))
	  (indexes (indexes (band-space diagram)))
	  (index (find-if
		  #'(lambda (index)
		      (declare (type Index index))
		      (match-index? index :band0 b0 :band1 b1 :band2 b2))
		  indexes)))
     (declare (type (or Null Band) b0 b1 b2)
	      (type List indexes)
	      (type (or Null Index) index))
     (when (null index)
       (setf index (find-if #'unlocked-index? indexes))
       (ac:with-interactor-busy (diagram)
	 (if (or (null index) (< (length indexes) *max-indexes*))
	     (setf index (make-index diagram))
	   ;; else resort the az:Card8s of an index that's not being used
	   (progn
	     (setf (band0 index) b0)
	     (setf (band1 index) b1)
	     (setf (band2 index) b2)
	     (sort-pixels index)))))
     (pushnew diagram (locks index))
     index)))

;;;--------------------------------------------------------------

(defmethod make-window->pixmap ((diagram Diagram))
  (let* ((wr (clay:diagram-window-rect diagram))
	 (pr (clay:diagram-pixmap-rect diagram))
	 (w->p (make-instance 'g:Int-Grid-Map
		:domain (g:home wr) :codomain (g:home pr))))
    (g:set-grid-map w->p wr pr)
    w->p))

(defmethod make-pixmap->image ((diagram Diagram))
  (let* ((pr (clay:diagram-pixmap-rect diagram))
	 (ir (clay:diagram-image-rect diagram))
	 (p->i (make-instance 'g:Int-Grid-Flip
		:domain (g:home pr) :codomain (g:home ir))))
    (g:set-grid-map p->i pr ir)
    p->i))

(defmethod make-pixmap->view ((diagram Diagram))
  (let* ((pr (clay:diagram-pixmap-rect diagram))
	 (vr (clay:diagram-view-rect diagram))
	 (p->v (make-instance 'g:Int-Float-Flip
		:domain (g:home pr) :codomain (g:home vr))))
    (g:set-grid-map p->v pr vr)
    p->v))

;;;--------------------------------------------------------------

(defmethod clay:layout-diagram ((diagram Diagram) layout-options)
  (declare (type List layout-options)
	   (ignore layout-options))
  
  (clay:ensure-big-enough-pixmaps diagram)
  (let ((lens (clay:diagram-lens diagram)))
    (setf (clay:window->pixmap lens) (make-window->pixmap diagram))
    (setf (clay:pixmap->image lens) (make-pixmap->image diagram))
    (setf (clay:pixmap->view lens) (make-pixmap->view diagram)))
  (clay:update-internal diagram nil)
  diagram)

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod clay:render-diagram ((diagram Diagram) (drawable xlib:Drawable))
  (xlt:draw-inverse-transformed-image
   drawable
   (clay:diagram-gcontext diagram)
   (clay:diagram-image diagram)
   (clay:pixmap->image (clay:diagram-lens diagram))))

(defmethod clay:draw-diagram ((diagram Diagram))
  (xlt:copy-area (clay:diagram-gcontext diagram)
		 (clay:diagram-pixmap diagram)
		 (clay:diagram-pixmap-rect diagram)
		 (clay:diagram-window diagram)
		 (g:origin (clay:diagram-window-rect diagram))))

;;;============================================================
;;; Input
;;;============================================================

(defclass Role (clay:Diagram-Role Mu-Object) ())

;;;------------------------------------------------------------
;;; Paint
;;;------------------------------------------------------------

(defmethod clay:stroke-object ((diagram Diagram) (stroke clay:Stroke))
  (clay:update-internal diagram stroke)
  (ac:send-msg 'clay:role-draw-diagram diagram diagram (ac:new-id))
  (az:deletef diagram (clay:stroke-objects-to-be-updated stroke))
  (when (null (clay:stroke-objects-to-be-updated stroke))
    (clay:return-stroke stroke))
  (values t))



