;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

(make-imagegram *fazendas* :left 400 :host "belgica")

(make-imagegram (band *fazendas* "b4") :host "belgica")


(defparameter *fazendas345*
    (make-rotor (band *fazendas* "b3") 
		(band *fazendas* "b5") 
		(band *fazendas* "b4")
		:left 600 :top 4 :host "belgica"))

(loop for i from 0
    for names on (list "b1" "b2" "b3" "b4" "b5" "b7")
    for f0 = (first names) do
      #||
      (loop for j from i
	  for names1 on (rest names)
	  for f1 in names1 do
	    (make-repscatgram (band *fazendas* f0) (band *fazendas* f1)
			      :left (* i 158) :top (* j 180))
	    (loop for f2 in (rest names1) do
		  (make-rotor (band *fazendas* f1)
			      (band *fazendas* f2)
			      (band *fazendas* f0))))
      ||#
      (make-histogram
       (band *fazendas* f0) :left 500 :height 128 :top (* i 180)))

#||
(time (compute-fractions *fazendas-mixing*))

(time (display-fractions *fazendas-mixing*))

(progn
  (multiple-value-setq (*fd0* *fd1*)
    (split-space *fazendas-band-space* 
		  (fraction *fazendas-mixing* "vegsh") 152))
  (multiple-value-setq (*fd00* *fd10*)
    (split-space *fd0* (fraction *fazendas-mixing* "sasp") 96))
  (multiple-value-setq (*fd010* *fd110*)
    (split-space *fd10* (fraction *fazendas-mixing* "dimona") 104))
  
  (make-imagegram (fraction *fazendas-mixing* "vegsh"))
  (make-imagegram (fraction *fazendas-mixing* "dimona"))
  (make-imagegram (fraction *fazendas-mixing* "plra"))
  (make-imagegram (fraction *fazendas-mixing* "sasp"))
  
  (defparameter *r* (make-rotor (fraction *fazendas-mixing* "dimona")
				(fraction *fazendas-mixing* "vegsh")
				(fraction *fazendas-mixing* "sasp")
				:fractions? t))
  
  (make-histogram (fraction *fazendas-mixing* "vegsh") :height 128)
  (make-histogram (fraction *fazendas-mixing* "sasp") :band-space *fd0*
		  :height 128)
  (make-histogram (fraction *fazendas-mixing* "dimona") :band-space *fd10*
		  :height 128)
  
  
  ;;(make-imagegram *fazendas* :band-space *fd0*)
  ;;(make-imagegram *fazendas* :band-space *fd1*)
  ;;(make-imagegram *fazendas* :band-space *fd00*)
  ;;(make-imagegram *fazendas* :band-space *fd10*)
  ;;(make-imagegram *fazendas* :band-space *fd110*)
  ;;(make-imagegram *fazendas* :band-space *fd010*)

  (defparameter *p* (partitioning *fd0*))
  (defparameter *pb* (make-part-browser *p* :left 500))
  )

(time (xlt:raw-dump (clay:diagram-window *pb*) "tree.raw"))
(time (xlt:raw-dump (clay:diagram-window *r*) "rotor.raw"))


(make-rotor (fraction *fazendas-mixing* "dimona")
	    (fraction *fazendas-mixing* "sasp")
	    (fraction *fazendas-mixing* "plra"))

(progn
  (time
   (multiple-value-setq (*fd0* *fd1*)
     (split-space *fazendas-band-space*
		   (fraction *fazendas-mixing* "vegsh") 152)))
  (time
   (multiple-value-setq (*fd00* *fd10*)
     (split-space *fd0* (fraction *fazendas-mixing* "sasp") 96)))
  (time 
   (multiple-value-setq (*fd010* *fd110*)
     (split-space *fd10* (fraction *fazendas-mixing* "dimona") 104)))
  (time (display-fractions *fazendas-mixing* :band-space *fd0*)))

(make-rotor (fraction *fazendas-mixing* "dimona")
	    (fraction *fazendas-mixing* "sasp")
	    (fraction *fazendas-mixing* "plra")
	    :band-space *fd0*)

(make-rotor (fraction *fazendas-mixing* "vegsh")
	    (fraction *fazendas-mixing* "sasp")
	    (fraction *fazendas-mixing* "plra")
	    :band-space *fd0*)

(make-rotor (fraction *fazendas-mixing* "vegsh")
	    (fraction *fazendas-mixing* "dimona")
	    (fraction *fazendas-mixing* "plra")
	    :band-space *fd0*)

(make-rotor (fraction *fazendas-mixing* "vegsh")
	    (fraction *fazendas-mixing* "dimona")
	    (fraction *fazendas-mixing* "sasp")
	    :band-space *fd0*)
||#