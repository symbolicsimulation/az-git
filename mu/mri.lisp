;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

(time
 (az:large-allocation
  (multiple-value-setq (*mri0* *mri1*)
    (split-space *mri-band-space* (band *mri* "pd") 10))
  (time (make-histogram (band *mri* "pd") :left 500 :top 100 :height 64
			:band-space *mri1* :host "belgica"))
  (time (make-histogram (band *mri* "t1") :left 500 :top 170 :height 64
			:band-space *mri1*  :host "belgica"))
  (time (make-histogram (band *mri* "t2") :left 500 :top 240 :height 64
			:band-space *mri1*  :host "belgica"))
  (time (make-imagegram (band *mri* "pd") :width 512 :height 512
			:top 64 :band-space *mri1* :host "belgica"))
  (time (make-imagegram (band *mri* "t1") :top 600
			:band-space *mri1* :host "belgica"))
  (time (make-imagegram (band *mri* "t2") :left 264 :top 600
			:band-space *mri1*  :host "belgica"))
  (time
   (defparameter *mri-102*
       (make-rotor (band *mri* "t1") 
		   (band *mri* "pd") 
		   (band *mri* "t2")
		   :left 500 :top 100
		   :band-space *mri1*  :host "belgica")))
  (time (make-repscatgram (band *mri* "t1") (band *mri* "pd")
		       :left 600 :top 260
		       :band-space *mri1*  :host "belgica"))
  (time (make-repscatgram (band *mri* "t1") (band *mri* "t2")
		       :left 600 :top 540
		       :band-space *mri1*  :host "belgica"))
  (time (make-repscatgram (band *mri* "t2") (band *mri* "pd")
		       :left 864 :top 260
		       :band-space *mri1*  :host "belgica"))
  ))

#||

(let ((*print-array* t)
      (*print-length* nil))
  (with-open-file (f "../examples/mri/pd.ascii" 
		   :direction :output
		   :if-exists :overwrite
		   :if-does-not-exist :create)
    (print (band-vector (band *mri* "pd")) f))
  (with-open-file (f "../examples/mri/t1.ascii" 
		   :direction :output
		   :if-exists :overwrite
		   :if-does-not-exist :create)
    (print (band-vector (band *mri* "t1")) f))
  (with-open-file (f "../examples/mri/t2.ascii" 
		   :direction :output
		   :if-exists :overwrite
		   :if-does-not-exist :create)
    (print (band-vector (band *mri* "t2")) f)))

||#