;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Mu
  (:use :Common-Lisp #+:clos :CLOS)
  (:export 
   Mu
   Complete-Band-Space
   Spectral-Space
   make-basic-file-format
   make-mu
   make-imagegram
   make-histogram
   band
   make-rotor
   angle 
   angular-resolution
   min-count
   ))

(declaim (declaration :returns))
