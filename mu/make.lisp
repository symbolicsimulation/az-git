;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is givne or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================

(let* ((directory (namestring 
		  (make-pathname
		   :directory (pathname-directory *load-truename*)))))

  (load (truename
	 (pathname
	  (concatenate 'String directory "../az.system")))))

(cs :Mu)


