
;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================

(defclass Index (Mu-Object)
	  ((locks
	    :type List
	    :accessor locks
	    :initform ()
	    :documentation
	    "A list of the objects that are using this index.")
	   (band-space
	    :type Band-Space
	    :accessor band-space
	    :initarg :band-space)
	   (band0
	    :type Band
	    :accessor band0
	    :initarg :band0)
	   (band1
	    :type (or Null Band)
	    :accessor band1
	    :initarg :band1)
	   (band2
	    :type (or Null Band)
	    :accessor band2
	    :initarg :band2)
	   (sorted-pins
	    :type az:Card28-Vector
	    :accessor sorted-pins))
  (:default-initargs :band1 nil :band2 nil)
  (:documentation
   "An index data structure that can be conveniently shared with C code."))

;;;-------------------------------------------------------------------

(defmethod initialize-instance :after ((index Index) &rest initargs)
  (declare (optimize (safety 0) (speed 3))
	   (ignore initargs))
  (let ((space (band-space index)))
    (declare (type Band-Space space))
    (pushnew index (indexes space))
    (az:large-allocation
     (setf (sorted-pins index) (az:make-card28-vector (g:dimension space))))
    (sort-pixels index)))

;;;-------------------------------------------------------------------

(defun sort-pixels (index)
  (declare (optimize (safety 0) (speed 3))
	   (type Index index))
  (let* ((space (band-space index))
	 (b0 (band0 index))
	 (b1 (band1 index))
	 (b2 (band2 index))
	 (perm (sorted-pins index))
	 (work (work-pins space)))
    (declare (type Band-Space space)
	     (type Band b0 b1 b2)
	     (type az:Card28-Vector perm work))
    (space-pins space perm)
    (multiple-value-bind (new-perm new-work)
	(if (null (band2 index))
	    ;; then it's only indexed on at most two bands
	    (if (null (band1 index))
		;; then it's only indexed on one band
		(counting-sort-pixels-1 space b0 perm work)
	      ;; else it's indexed on two bands
	      (counting-sort-pixels-2 space b0 b1 perm work))
	  ;; else its indexed on 3 bands
	  (counting-sort-pixels-3 space b0 b1 b2 perm work))
      (setf (sorted-pins index) new-perm)
      (setf (slot-value space 'work-pins) new-work))
    index))

;;;-------------------------------------------------------------------

(defun unlocked-index? (index)
  (declare (type Index index))
  (null (locks index)))

;;;-------------------------------------------------------------------

(defun match-index? (index
		     &key
		     (band0 (band0 index))
		     (band1 (band1 index))
		     (band2 (band2 index)))
  
  "Is this index sorted on the supplied bands
--- with band0 the most significant, followed by band1 and band2?"

  (declare (type Index index)
	   (type (or Null Band) band0 band1 band2))
  (and (or (null band0) (eq band0 (band0 index)))
       (or (null band1) (eq band1 (band1 index)))
       (or (null band2) (eq band2 (band2 index)))))

;;;-------------------------------------------------------------------

(defmacro with-index ((index-var diagram) &body body)
  (let ((d (gensym)))
  `(let* ((,d ,diagram)
	  (,index-var (get-index ,d)))
     (declare (type Diagram ,d)
	      (type Index ,index-var))
     (unwind-protect (progn ,@body)
       (setf (locks ,index-var) (remove ,d (locks ,index-var)))))))



