;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Mu) 

;;;-------------------------------------------------------------

(time
 (az:large-allocation
  (time (make-imagegram (band *manaus* "b4") :left 0 :top 0))
  (loop for i0 from 0
      for names on (list "b1" "b2" "b3" "b4" "b5" "b7")
      for f0 = (first names) do
	(time (make-histogram
	       (band *manaus* f0) :left 500 :height 128 :top (* i0 180)))
	(loop for j0 from i0
	    for f1 in (rest names) do
	      (time (make-scatgram (band *manaus* f0) (band *manaus* f1)
				   :left (* i0 158) :top (* j0 180)))))
  (time
   (defparameter *manaus345*
       (make-rotor
	(band *manaus* "b3") (band *manaus* "b4") (band *manaus* "b5")
	:left 500 :top 4)))
  ))

