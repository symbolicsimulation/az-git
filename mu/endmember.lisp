;;;-*- Package: :Mu; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Mu) 

;;;============================================================
;;; A Multi-Band Pixel Object used in Mixing models
;;;============================================================

(defclass Endmember (Mu-Object g:Point)
	  ((g:home
	    :reader g:home
	    :initarg :home
	    :type Spectral-Space))
  (:documentation
   "A class for objects representing endmembers of mixing models."))

;;;------------------------------------------------------------

(defun make-endmember (name home data &key (documentation ""))
  (declare (type Simple-String name)
	   (type Spectral-Space home)
	   (type List data))
  (assert (= (the az:Card16 (length data)) (the az:Card16 (g:dimension home))))
  (assert (and (list-of? 'Real data)
	       (every #'(lambda (x)
			  (declare (type Real x))
			  (<= 0 x 255)) data)))
  (let ((floats (az:make-float-vector (length data)))
	(i 0))
    (declare (type az:Float-Vector floats)
	     (type az:Card16 i))
    (dolist (x data)
      (declare (type Number x))
      (setf (aref floats i) (az:fl x))
      (incf i))
    (make-instance 'Endmember
      :name name
      :home home
      :coords floats
      :documentation documentation)))





