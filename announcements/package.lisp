;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :cl-user)


#+(and :pcl (not :clos))
(eval-when (compile load eval)
  (let* ((package (find-package :pcl))
	 (nicknames (package-nicknames package))
	 (name (package-name package)))
    (unless (member "CLOS" nicknames :test #'string-equal)
      (rename-package package name (cons "CLOS" (copy-list nicknames))))))

#+:pcl
(eval-when (compile load eval)
  ;; have compiled classes and methods added to the environment
  ;; at compile time:
  (pushnew 'compile clos::*defclass-times*)
  (pushnew 'compile clos::*defgeneric-times*)
  (pushnew 'compile clos::*defmethod-times*)
  )

#+:pcl
(eval-when (compile load eval)
  ;; PCL exports consistent with CLTL2:
  (export '(#-:cmu17 clos::allocate-instance
	    clos::class-direct-subclasses
	    clos::class-direct-superclasses
	    clos::class-slots
	    clos::slot-definition-name
	    clos::slot-definition-type
	    #-:cmu17 clos::generic-function)
	  :clos))

(defpackage :Announcements
 
  (:use :Common-Lisp #+:clos Clos)
  
  (:nicknames :An)
  
  (:export 
   Announcement
   make-announcement
   defannouncement
   announcement-name
   audience
   find-announcement
   announce 
   join-audience
   leave-audience
   leave-audience-for-all-announcers
   leave-all-audiences
   dead-announcer))

