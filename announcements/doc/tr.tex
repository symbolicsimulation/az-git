%From mln@cs.washington.edu Thu Oct 17 21:55:02 1991

\documentstyle[code,twoside]{article}
\pagestyle{headings}

\topmargin=-0.2in
\textwidth=6.0in
\textheight=8.0in
\oddsidemargin=0.25in
\evensidemargin=0.25in

\begin{document}

\title{Announcements: an implementation of implicit invocation}

\author{{\sc John Alan McDonald}\\
	{\sc Mark Niehaus}
\thanks{This work was supported in part 
the Dept. of Energy under contract FG0685-ER25006
and by NIH grant LM04174 from the National
Library of Medicine.}\\
        Dept. of Computer Science and Engineering \\
	and \\
	Dept. of Statistics,\\
	University of Washington\\
         }
\date{October, 1991}

\maketitle

\begin{abstract}
This report describes the Announcements module, 
which provides a mechanism for implicit invocation
(i.e. broadcast message sending) 
for Common Lisp programs.
\end{abstract}

\section{Introduction}

The Announcements module provides a mechanism 
for maintaining simple dependencies between objects
through message broadcasting, 
similar to that used in the Field \cite{Reis90a} 
and Forest \cite{Garl90a} environments.
It is derived from 
a Common Lisp implementation 
of Events/Mediators \cite{Sull90a}
by Mark Niehaus for the Prism system \cite{Kale90a}.
It is in part motivated by 
dissatifaction with some aspects of earlier work by one of the authors
in the Antelope system \cite{McDo86b}
and the Plot Windows system of Stuetzle \cite{wxs87a,wxs87b}.

The Announcements module is
a component of a system called Arizona,
now under development at the U. of Washington.
Arizona is intended to be a portable, public-domain collection 
of tools supporting scientific computing, quantitative graphics, 
and data analysis,
implemented in Common Lisp
and CLOS (the Common Lisp Object System) \cite{Stee90}.
This document assumes the reader is familiar with Common Lisp and CLOS.
An overview of Arizona is given in \cite{McDo88b}
and an introduction to the current release is in \cite{McDo91g}.

Like events in Field and Forest, 
Announcements can be thought of as a mechanism for 
{\it implicit invocation} 
as discussed in \cite{Garl91a,Sull91a}.
The basic idea is to allow an object (the {\it announcer})
to notify other objects (the {\it audience})
of a change of state (or some other event)
without the announcer having to know which objects need to be
notified of which events.

The interface to Announcements supplies only the Events portion of an
Events/Mediators mechanism, which means that it can be used
to implement simple one-way dependencies,
but not the more complicated relationships that would require some
version of Mediators.
The reason for this is that we feel we understand the Events part 
and can produce a stable, self-contained implementation
that should be useful on its own in a wide variety of applications.
In contrast, our understanding 
of what would be a good set of broadly useful Mediators, 
or what an appropriate protocol for an 
abstract Mediator type might be,
is much less secure.
We expect that Announcements will be most useful when used together
with application-specific Mediators.

\section{How to use Announcements}

Conceptually, an Announcement object represents a type of event
that might occur in a number of announcer objects.
The Announcement object keeps track 
of an audience for each potential announcer 
and
sees that each member of the audience is properly notified
when the announcement takes place.

To make this a little more concrete, 
consider the case of a direct-manipulation graph browser.
The browser described in \cite{McDo91n} defines a simple protocol 
so that it can be used 
with a broad class of data structures representing graphs.
Any use of this browser involves  
two modules that implemented more-or-less independently:
the interactive browser
and the data structure representing the underlying graph.
The dependency that we want to represent and maintain with Announcements
is that the browser display should automatically update
when nodes or edges are added to or deleted from the underlying graph.

\subsection{Creating Announcements}

In normal use,
a globally accessible announcement 
clay:clay-objectis defined with a call to {\sf an:defannouncement},
which is a defining form similar to {\sf defclass}.
In particular, it is like {\sf defclass} in the sense
that re-evaluating {\sf an:defannouncement} for a given name
does not create a new Announcement object, but simply
updates the lambda list and documentation if they have changed.

It may sometimes be useful to create Announcements
in a function call, using {\sf an:make-announcement}.
Announcements created with {\sf an:make-announcement} are not automatically
accessible by name.
An Announcement can be be made globally accessible by name
with a {\sf setf} of {\sf an:find-announcement},
which will replace the existing announcement with the given name,
if there is one.


A simple way to handle the graph browser example is to
provide an Announcement that corresponds to any change in the state of 
the underlying graph, as follows:
\begin{code}
(an:defannouncement :subject-changed ())
\end{code}
This {\sf :subject-changed} announcement would most likely be defined
as part of a general toolkit for direct manipulation interfaces,
and not as part of the graph browser itself.

One could get higher performance, at the cost of a loss in simplicity,
by defining a more specialized Announcement interface,
eg., defining Announcements for {\sf :nodes-added}, {\sf :nodes-deleted}, etc.
Alternatively, one could define {\sf :subject-changed} with a lambda list
that would allow arguments to be passed that specify exactly how
the underlying graph had changed.

In the example above, the announcement's name is a keyword.
An announcement name can, in fact, be any symbol.
However, keywords may be a good idea
when the announcement is not clearly part of any particular package/module
and when potential announcers and audience members
may reside in many different packages.

\subsection{Joining an Audience}

The announcer of an announcement can be any lisp object,
though the normal, intended use is for it to be an instance of a CLOS class.
Any object can join the audience for a particular announcer-announcement
pair with a call to {\sf an:join-audience}.
What happens when a particular member of the audience is notified
is determined by the notification function supplied as the last argument
to {\sf an:join-audience}.
If {\sf an:join-audience} is called more than once with the same
announcer, announcement, and audience member,
the notification function will be that provided with the last call.

For the graph browser example,
the browser object will need to join the audience 
for announcements of changes to the state of its subject graph:
\begin{code}
(an:join-audience (subject graph-browser) :subject-changed 
                  graph-browser #'subject-state-changed)
\end{code}
The {\sf subject} of the browser is the graph data structure that it displays.
{\sf Subject-state-changed} is a generic function
that updates the display.

Such calls to {\sf an:join-audience} 
would typically occur in the initialization
code for the browser object, 
or in code that changes which graph a browser is displaying.

In other systems for implicit invocation,
like Field \cite{Reis90a} 
and Forest \cite{Garl90a},
some sort of pattern matching is done to determine
who gets notified of which announcements.
In the Announcements module,
the pattern matching is limited to testing for equality
of announcer, announcement, and audience member.
The sense of equality that's used is {\sf eq}.

If a member of the audience no longer wishes to be notified of 
announcements by the announcer, 
it can remove itself from the audience with a call to {\sf an:leave-audience}.
For example, if a browser were to change which graph it displayed,
it would first have to do something like:
\begin{code}
(an:leave-audience (subject graph-browser) :subject-changed graph-browser)
\end{code}

\subsection{Announcing}

Any announcer (any lisp object) can {\sf an:announce} any defined announcement,
which results in every member of the audience for that announcer-announcement
pair being notified.
Nothing happens if no object is in the audience 
for the given announcement by the given announcer.
Notification means that the function argument supplied 
to {\sf an:join-audience}
is applied to the corresponding member of the audience,
together with whatever other arguments are supplied to {\sf an:announce}.
The arguments that are supplied in the call to {\sf an:announce}
must be consistent with the lambda list supplied in {\sf an:defannouncement}
or {\sf an:make-announcement}.

In our simple example, the {\sf :subject-changed} announcment
take no arguments, so a graph object would announce thus:
\begin{code}
(an:announce graph :subject-changed)
\end{code}


\vfill
\pagebreak

\section{Reference Manual}
\label{reference-manual}
\pagestyle{myheadings}
\input{ref}

\vfill
\pagebreak

\pagestyle{headings}
\bibliographystyle{plain} 

\bibliography{../../b/arizona,../../b/cs,../../b/cactus,../../b/constraint,../../b/database,../../b/layout,../../b/lisp,../../b/image,../../b/buja,../../b/mcdonald,../../b/friedman,../../b/wxs}


\end{document}

\documentstyle[code,twoside]{article}
\pagestyle{headings}

\topmargin=-0.2in
\textwidth=6.0in
\textheight=8.0in
\oddsidemargin=0.25in
\evensidemargin=0.25in

\begin{document}

\title{Announcements: an implementation of implicit invocation}

\author{{\sc John Alan McDonald}\\
	{\sc Mark Niehaus}
\thanks{This work was supported in part 
the Dept. of Energy under contract FG0685-ER25006
and by NIH grant LM04174 from the National
Library of Medicine.}\\
        Dept. of Computer Science and Engineering \\
	and \\
	Dept. of Statistics,\\
	University of Washington\\
         }
\date{October, 1991}

\maketitle

\begin{abstract}
This report describes the Announcements module, 
which provides a mechanism for implicit invocation
(i.e. broadcast message sending) 
for Common Lisp programs.
\end{abstract}

\section{Introduction}

Announcements provides a mechanism 
for maintaining simple dependencies between objects
through message broadcasting, 
similar to the Field \cite{Reis90a} 
and Forest \cite{Garl90a} environments.
It is derived from 
a Common Lisp implementation 
of Events/Mediators \cite{Sull90a}
by Mark Niehaus for the Prism system \cite{Kale90a}.
It is in part motivated by 
dissatifaction with some aspects of earlier work by one of the authors
in the Antelope system \cite{McDo86b}
and the Plot Windows system of Stuetzle \cite{wxs87a,wxs87b}.

The Announcements module is
a component of a system called Arizona,
now under development at the U. of Washington.
Arizona is intended to be a portable, public-domain collection 
of tools supporting scientific computing, quantitative graphics, 
and data analysis,
implemented in Common Lisp
and CLOS (the Common Lisp Object System) \cite{Stee90}.
This document assumes the reader is familiar with Common Lisp and CLOS.
An overview of Arizona is given in \cite{McDo88b}
and an introduction to the current release is in \cite{McDo91g}.

Like Field and Forest, 
Announcements can be thought of as a mechanism for 
{\it implicit invocation} 
as discussed in \cite{Garl91a,Sull91a}.
The basic idea is to allow an object (the {\it announcer}
to notify other objects (the {\it audience})
of a change of state (or some other event)
without the announcer having to know which objects need to be
notified of which events.

Announcements provides just the Events part of an 
Events/Mediators mechanism, which means that it can be used
to implement simple one-way dependencies,
but not the more complicated relationships that would require some
version of Mediators.
The reason for this is that we feel we understand the Events part 
and can produce a stable, self-contained impelmentation
that should be useful on its own in a wide variety of applications.
In contrast, our understanding 
of what would be a good set of broadly useful Mediators, 
or what an appropriate protocol for an 
abstract Mediator type might be,
is much less secure.
We expect that Announcements will be most useful when used together
with application-specific Mediators.

\section{How to use Announcements}

Conceptually, an Announcement object represents a type of event
that might occur in a number of announcer objects.
The Announcement object keeps track 
of an audience for each potential announcer 
and
sees that each member of the audience is properly notified
when the announcement takes place.

To make this a little more concrete, 
consider the case of a direct-manipulation graph browser.
The browser described in \cite{McDo91n} defines a simple protocol 
so that it can be used 
with a broad class of data structures representing graphs.
Any use of this browser involves  
two modules that implemented more-or-less independently:
the interactive browser
and the data structure representing the underlying graph.
The dependency that we want to repesent and maintain with Announcements
is that the browser display should automatically update
when nodes or edges are added to or deleted from the underlying graph.

\subsection{Creating Announcements}

In normal use,
a globally accessible announcement 
is defined with a call to {\sf an:defannouncement},
which is a defining form similar to {\sf defclass}.
In particular, it is like {\sf defclass} in the sense
that re-evaluating {\sf an:defannouncement} for a given name
does not create a new Announcement object, but simply
updates the lambda list and documentation if they have changed.

It may sometimes be useful to create Announcements
in a function call, using {\sf an:make-announcement}.
Announcements created with {\sf an:make-announcement} are not automatically
accessible by name.
An Announcement can be be made globally accessible by name
with a {\sf setf} of {\sf an:find-announcement},
which will replace the existing announcement with the given name,
if there is one.


A simple way to handle the graph browser example is to
provide an Announcement that corresponds to any change in the state of 
the uderlying graph, as follows:
\begin{code}
(an:defannouncement :subject-changed ())
\end{code}
This {\sf :subject-changed} announcement would most likely be defined
as part of a general toolkit for direct manipulation interfaces,
and not as part of the graph browser itself.

One could get higher performance, at the cost of a loss in simplicity,
by defining a more specialized Announcement interface,
eg., defining {\sf :nodes-added}, {\sf :nodes-deleted}, etc.,
Announcements.
Alternatively, one could give {\sf :graph-changed} a lambda list
that would allow arguments to be passed that specify exactly how
the underlying graph had changed.

In the example above, the announcement's name is a keyword.
An announcement name can, in fact, be any symbol.
However, keywords may be a good idea
when the announcement is not clearly part of any particular package/module
and when potential announcers and audience members
may reside in many different packages.

\subsection{Joining an Audience}

The announcer of an announcement can be any lisp object,
though the normal, intended use is for it to be an instance of a CLOS class.
Any object can join the audience for a particular announcer-announcement
pair with a call to {\sf an:join-audience}.
What happens when a particular member of the audience is notified
is determined by the notification function supplied as the last argument
to {\sf an:join-audience}.
If {\sf an:join-audience} is called more than once with the same
announcer, announcement, and audience member,
the notification function will be that provided with the last call.

For the graph browser example,
the browser object will need to join the audience 
for announcements of changes to the state of its subject graph:
\begin{code}
(an:join-audience (subject graph-browser) :subject-changed 
                  graph-browser #'subject-state-changed)
\end{code}
The {\sf subject} of the browser is the graph data structure that it displays.
{\sf Subject-state-changed} is a generic function
that updates the display.

Such calls to {\sf an:join-audience} 
would typically occur in the initialization
code for the browser object, 
or in code that changes which graph a browser is displaying.

In other systems for implicit invocation,
like Field \cite{Reis90a} 
and Forest \cite{Garl90a},
some sort of pattern matching is done to determine
who gets notified of which announcements.
In the Announcements module,
the pattern matching is limited to testing for equality
of announcer, announcement, and audience member.
The sense of equality that's used is {\sf eq}.

If a member of the audience no longer wishes to be notified of 
announcements by the announcer, 
it can remove itself from the audience with a call to {\sf an:leave-audience}.
For example, if a browser were to change which graph it displayed,
it would first have to do something like:
\begin{code}
(an:leave-audience (subject graph-browser) :subject-changed graph-browser)
\end{code}

\subsection{Announcing}

Any announcer (any lisp object) can {\sf an:announce} any defined announcement,
which results in every member of the audience for that announcer-announcement
pair being notified.
Nothing happens if no object is in the audience 
for the given announcement by the given announcer.
Notification means that the function argument supplied 
to {\sf an:join-audience}
is applied to the corresponding member of the audience,
together with whatever other arguments are supplied to {\sf an:announce}.
The arguments that are supplied in the call to {\sf an:announce}
must be consistent with the lambda list supplied in {\sf an:defannouncement}
or {\sf an:make-announcement}.

In our simple example, the {\sf :subject-changed} announcment
take no arguments, so a graph object would announce thus:
\begin{code}
(an:announce graph :subject-changed)
\end{code}


\vfill
\pagebreak

\section{Reference Manual}
\label{reference-manual}
\pagestyle{myheadings}
\input{reference-manual}

\vfill
\pagebreak

\pagestyle{headings}
\bibliographystyle{plain} 

\bibliography{/belgica-2g/jam/bib/arizona,/belgica-2g/jam/bib/cs,/belgica-2g/jam/bib/cactus,/belgica-2g/jam/bib/constraint,/belgica-2g/jam/bib/database,/belgica-2g/jam/bib/layout,/belgica-2g/jam/bib/lisp,/belgica-2g/jam/bib/image,/belgica-2g/jam/bib/buja,/belgica-2g/jam/bib/mcdonald,/belgica-2g/jam/bib/friedman,/belgica-2g/jam/bib/wxs}


\end{document}
