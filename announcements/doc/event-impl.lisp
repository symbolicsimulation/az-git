From mln@merlin.rad.washington.edu Mon Oct 14 14:29:40 1991
Received: from lisbon.stat.washington.edu by belgica.stat.washington.edu
	(4.1/UW-NDC Revision: 2.13 ) id AA21662; Mon, 14 Oct 91 14:29:39 PDT
Received: from merlin.rad.washington.edu by lisbon.stat.washington.edu
	(4.1/UW-NDC Revision: 2.1 ) id AA26802; Mon, 14 Oct 91 14:44:24 PDT
Received: by merlin.rad.washington.edu
	(5.64/UW-NDC Revision: 2.21 ) id AA16189; Mon, 14 Oct 91 14:42:27 -0700
Date: Mon, 14 Oct 91 14:42:27 -0700
From: mark <mln@merlin.rad.washington.edu>
Message-Id: <9110142142.AA16189@merlin.rad.washington.edu>
To: jam@stat.washington.edu
Subject: events/announcements
Status: R

Regarding Ira's comment that the event/announcement package used in
prism should be the same as the distributed version, you might want to
look at what I've got so far on revisions to the code.  I've taken out
all references to clos, as I can't really foresee any need to
subclass, but tell me if you think that's going too far.
I haven't gotten to any of the changes discussed in the recent
meetings.

-------------------------------------------

(in-package :sys-tools)
(use-package :pcl)
(proclaim '(optimize (speed 0) (safety 0)))
(export '(event defevent make-local-event connect disconnect announce
	  clear-global-events))

;; struct  E V E N T
(defstruct (event (:conc-name nil))
  "An event maintains a table of lists of connectors to activate
   when its -announce- method is called."
  (source-table (make-hash-table :test #'eq :size 16 :rehash-size 2.0)))


;; struct   C O N N E C T O R
(defstruct (connector (:conc-name nil))
  "A connector simply records a target and a function to call"
  target
  fn)


;; tables for event accounting
(defvar *name->event-table*
  (make-hash-table :test #'eq :size 16 :rehash-size 2.0))
(defvar *event->name-table*
  (make-hash-table :test #'eq :size 16 :rehash-size 2.0))

(defun clear-event (event)
  "CLEAR-EVENT event
    Remove all members of event's audience"
  (clrhash (source-table event)))


(defun clear-global-events ()
  "CLEAR-GLOBAL-EVENTS
    remove all members of every globally declared event's audience"
  (maphash #'(lambda (event name)
	       (declare (ignore name) (type event event))
	       (clear-event event))
	   *event->name-table*))

(defmacro defevent (name &body options)
  "DEFEVENT name &body options
    Define `name' to be a global event and bind the symbol to a new event
    If `name' is already bound to an event, clear its audience.
    Returns the event."
  (declare (ignore options))  ;; for future expansion
  `(progn
    (defvar ,name)
    (cond
      ((and (boundp ',name) (typep ,name 'event))
       (clear-event ,name))
      (t
       (setf ,name (sys-tools::make-event)
	     (gethash ',name sys-tools::*name->event-table*) ,name
	     (gethash ,name  sys-tools::*event->name-table*) ',name)))
    ,name))


(defun make-local-event ()
  "MAKE-LOCAL-EVENT 
    Create an event intended to be used locally,
    within an object or function"
  (make-event))

(defun connector-equal (c1 c2)
  "CONNECTOR-EQUAL c1 c2
    returns whether the components of c1 and c2 are eq"
  (and
   (eq (target c1) (target c2))
   (eq (fn c1) (fn c2))))

(defun connectors (event &optional source)
  "CONNECTORS event source
    Return the list of connectors within `event' associated with `source'"
  (gethash source (source-table event)))

(defsetf connectors (event &optional source) (new-connectors)
  "CONNECTORS event source (new-connectors)
    Set the list of connectors in `event' associated with `source'"
  `(setf (gethash ,source (source-table ,event)) ,new-connectors))

(defun delete-connector (event source connector)
  "DELETE-CONNECTOR event source connector
    Delete `connector' from list of connectors in `event'
    associated with `source'"
  (setf (connectors event source)
	(delete connector (connectors event source))))

(defun connect (event &key source target fn)
  "CONNECT event &key source target fn
    Insert `target' as a member of the audience of `event'
    as announced by `source'.  Register `fn' as the function
    to call on `target'.  `Target' may be an single atom or it may
    be a list, in which case it is a multiple target: i.e., if `target'
    is '(a b c), _announce_ will cause something like (fn a b c) to be
    called."
  (declare (type event event))
  (pushnew
   (make-connector :target target :fn fn)
   (gethash source (source-table event))
   :test #'connector-equal))


(defun disconnect (event &key source target)
  "DISCONNECT event &key source target fn
    Remove `target' from the audience for `event' as announced by `source'"
  (let*
      ((connectors (gethash source event)))
    (dolist (c connectors)
      (cond
	((eq target (target c))
	 (setf connectors (delete c connectors)))))))


(defun announce (event &optional source &rest args)
  "ANNOUNCE event &optional source &rest args
    Notify each target which has registered interest in `event'
    as annouced by `source' by calling its associated function.
    If a target is not nil, it is included as the first argument
    to the function.  If `target' is a list, each object in the
    list is given as a parameter to the function.
    NB: if {\it source} is nil but arguments are necessary,
    nil should be supplied as the second argument."
  (dolist (c (gethash source (source-table event)))
    (if (listp (target c))
	(apply (fn c) (append (target c) args))
	(apply (fn c) (target c) args))))
  

