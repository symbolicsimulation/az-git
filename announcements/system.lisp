;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user;-*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================
;;; make sure Kantrowitz's defsystem (make) package is loaded

(eval-when (compile load eval)
  (unless (find-package :Make)
    (load
     (concatenate 'String
       (namestring
	(make-pathname :directory (pathname-directory *load-truename*)))
       "../cmu/defsystem"))))

;;;============================================================

(mk:defsystem :Announcements
    :source-pathname #,*this-directory*
    :source-extension "lisp"
    :binary-pathname nil
    :binary-extension nil
    :components ("package"
		 "exports"
		 "defs"
		 "announcements"))
