;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Announcements -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved.
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;=======================================================

(in-package :Announcements)

;;;=======================================================
;;; for documenting types of returned values

(declaim (declaration :returns))

;;;=======================================================
;;; PCL/CLOS consistency
;;;=======================================================

#+(and :pcl (not :cmu))
(defun clos:slot-definition-name (slotd) (clos::slotd-name slotd))
#+(and :pcl (not :cmu))
(defun clos:slot-definition-type (slotd) (clos::slotd-type slotd))

;;;=======================================================================

(defmacro type-check (type &rest args)

  "a {\tt check-type} that takes arguments more like declarations, eg,
{\tt (declare (type Integer x y))}"

  (let* ((args (remove-duplicates args))
 	 (variables (remove-if #'constantp args))
	 (runtime-checks (mapcar #'(lambda (arg)
				     `(check-type ,arg ,type))
				 variables)))
    ;; test constants at macro expand time:
    (dolist (constant (remove-if-not #'constantp args))
      ;; need to <eval> because <constant> might be a <defconstant>
      ;; rather than a literal constant
      (assert (typep (eval constant) type)))
    (unless (null runtime-checks) `(progn ,@runtime-checks))))

(defmacro declare-check (&rest decls)

  "This macro generates type checking forms and has a syntax like <declare>.
Unfortunately, we can't easily have it also generate the declarations."

  `(progn
     ,@(mapcar #'(lambda (d) `(type-check ,(second d) ,@(cddr d)))
	       ;; ignore non-type declarations
	       (remove-if-not #'(lambda (d) (eq (first d) 'type))
			      decls))))

;;;==================================================================
;;; printing random things, borrowed from PCL's *-low.lisp files


(defun printing-random-thing-internal (thing stream)
  #+:cmu
  (format stream "{~X}" (sys:%primitive c:make-fixnum thing))
  #+:coral
  (prin1 (ccl::%ptr-to-int thing) stream)
  #+(and dec vax common)
  (format stream "~X" (system::%sp-pointer->fixnum thing))
  #+:excl
  (format stream "~X" (excl::pointer-to-fixnum thing))
  #+:gclisp
  (multiple-value-bind (offaddr baseaddr) (sys:%pointer thing)
    (princ baseaddr stream)
    (princ ", " stream)
    (princ offaddr stream))
  #+Genera
  (format stream "~X" (si:%pointer thing))
  #+HP-HPLabs 
  (format stream "~X" (prim:@inf thing))          
  #+IBCL
  (format stream "~X" (si:address thing))                
  #+KCL
  (format stream "~X" (si:address thing))                 
  #+Lucid
  (format stream "~X" (%pointer thing))
  #+TI
  (format stream "~X" (si:%pointer thing))
  #+Xerox
  (let ((*print-base* 8))
    (princ (il:\\hiloc thing) stream)
    (princ "," stream)
    (princ (il:\\loloc thing) stream))
  #-(or :cmu :coral (and dec vax common) :excl Genera HP-HPLabs IBCL KCL
	Lucid TI Xerox)
  (prin1 thing stream)
  )
  ;;   
;;;;;; printing-random-thing
  ;;
;;; Similar to printing-random-object in the lisp machine but much simpler
;;; and machine independent.
(defmacro printing-random-thing ((thing stream) &body body)
  (let ((s (gensym)))
    `(let ((,s ,stream))
       (format ,s "#<")
       ,@body
       (format ,s " ")
       (printing-random-thing-internal ,thing ,stream)
       (format ,s ">"))))

