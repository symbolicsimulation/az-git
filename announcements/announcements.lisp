;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Announcements -*-
;;;=======================================================
;;; modified from M. Niehaus events.cl by jam 10-91.

(in-package :Announcements)

;;;=======================================================

(defclass Announcement (Standard-Object)
	  ((announcement-name
	    :type Symbol
	    :reader announcement-name
	    :initarg :announcement-name
	    :documentation "Announcements can be found by name.")
	   (announcement-lambda-list
	    :type List
	    :accessor announcement-lambda-list
	    :initarg :announcement-lambda-list
	    :documentation "A lambda list for calls to <announce>
with this announcement as the first argument. At present, this is only
used for documentation.")
	   (announcement-documentation
	    :type String
	    :accessor announcement-documentation
	    :initarg :announcement-documentation
	    :documentation "A documentation string for this announcement.")
	   (audience-table
	    :reader audience-table
	    :type Hash-Table
	    :initform (make-hash-table :test #'eq)
	    :documentation "A table that maps announcers to audiences."))

  (:default-initargs
   :announcement-name (gensym)
   :announcement-documentation "")

  (:documentation 
   "An announcement maintains an audience (for each announcer) to
notify when it is announceed by an announcer."))

;;;--------------------------------------------------------

(defgeneric audience (announcer announcement)
  #-sbcl (declare (type T announcer)
		  (type (or Symbol Announcement) announcement)
		  (:returns (type List audience-list)))
  (:documentation 
   "<Audience> returns a list of items for any announcer ---
announcement pair. There is one item in the list for each member of
the audience. Each item is a list of length 2; the first item in the
list is the member of the audience and the second item is a function
(or function name).  When an announcement is made by a given
announcer, each member of the audience is notified by applying the
function to the member and whatever additional arguments were passed
to <announce>."))

(defmethod audience (announcer (announcement Symbol))
  "Find the announcement object associated with <announcement> and recurse."
  (declare (type T announcer)
	   (:returns (type List audience-list)))
  (audience announcer (find-announcement announcement)))

(defmethod audience (announcer (announcement Announcement))
  "The audience table is currently represented by a hash table."
  (declare (type T announcer)
	   (:returns (type List audience-list)))
  (az:lookup announcer (audience-table announcement)))

(defgeneric (setf audience) (new-audience announcer announcement)
  #-sbcl (declare (type List new-audience)
		  (type T announcer)
		  (type (or Symbol Announcement) announcement)
		  (:returns new-audience))
  (:documentation 
   "<setf audience> associates a new audience list with a given
announcer --- announcement pair. One does not often want to completely
change an audience list, but the setf method needs to be available for
use by other functions that make smaller modification to an audience
list."))

(defmethod (setf audience) (new-audience announcer (announcement Symbol))
  "Find the announcement object associated with <announcement> and recurse."
  (declare (type List new-audience)
	   (type T announcer)
	   (type Symbol announcement)
	   (:returns new-audience))
  (setf (audience announcer (find-announcement announcement)) new-audience))

(defmethod (setf audience) (new-audience announcer (announcement Announcement))
  "The audience table is currently represented by a hash table."
  (declare (type List new-audience)
	   (type T announcer)
	   (type Announcement announcement)
	   (:returns new-audience))
  (setf (az:lookup announcer (audience-table announcement)) new-audience))

;;;=======================================================

(defun clear-announcement (announcement)
  "Remove all audience lists from an announcement for debugging."
  (declare (type Announcement announcement)
	   (:returns announcement))
  (clrhash (audience-table announcement))
  announcement)

;;;--------------------------------------------------------

(defvar *announcements* (make-hash-table :test #'eq)
  "The *announcements* table allows us to get at announcements by name.")

(defun find-announcement (name)
  "Find the announcement with a given <name>, returning nil if one
does not exist."  
  (declare (type Symbol name)
	   (:returns (type (or Announcement Null))))
  (az:lookup name *announcements* nil))


(defsetf find-announcement (name) (announcement)
  "Associate <announcement> with <name>. <Name> must be <eq> to
the <announcement>'s name. This setf function must be used with care."
  `(progn
     (assert (eq ,name (announcement-name ,announcement)))
     (setf (az:lookup ,name *announcements*) ,announcement)))

;;;--------------------------------------------------------

(defun make-announcement (&key (name (gensym))
			       (lambda-list ())
			       (documentation ""))

  "Make an announcement object with the specified attributes.  This
announcement object will not be automatically installed in any global
tables. To make it globally accessible, and overwrite existing
announcement with name <name>, use 

(setf (find-announcment name) announcement-object)."

  (declare (type Symbol name)
	   (type List lambda-list)
	   (type String documentation)
	   (:returns (type Announcement)))
  
  (make-instance 'Announcement
    :announcement-name name 
    :announcement-lambda-list lambda-list
    :announcement-documentation documentation))

;;;--------------------------------------------------------

(defmacro defannouncement (name lambda-list (&key (documentation "")))

  "If an announcement with the given name does not exist, create one
and make it globally accessible by name. Otherwise update the
<lambda-list> and <documentation> of the existing announcement
object with the given name. It is an error to change the <lambda-list>
so that it is incongruent with existing notification function, but no
error will be signaled at <defannouncement> time."

  (declare (type List lambda-list)
	   (type String documentation)
	   (:returns (type Announcement))) 

   `(eval-when (load eval)
     (let ((announcement (find-announcement ',name)))
       (cond (announcement
              (setf (announcement-lambda-list announcement) ',lambda-list)
              (setf (announcement-documentation announcement) ,documentation))
             (t
              (setf announcement (make-announcement
                                  :name ',name
                                  :lambda-list ',lambda-list
                                  :documentation ,documentation))
              (setf (find-announcement (announcement-name announcement))
                announcement)))
       announcement)))

;;;--------------------------------------------------------

(defgeneric announce (announcer announcement &rest args)
  #-sbcl (declare (type T announcer)
		  (type (or Symbol Announcement) announcement)
		  (:returns t))
  (:documentation
   "Notify all members of the audience of the given <announcer> 
and <announcement>."))

(defmethod announce (announcer (name Symbol) &rest args)
  "Look up the announcement with the given <name> and recurse."
  (declare (type T announcer)
	   (type List args)
	   (:returns t))
  (let ((announcement (find-announcement name)))
    (if (eq announcement nil)
	(error "No announcment named ~s" name)
      ;; else
      (apply #'announce announcer announcement args)))
  t)

(defmethod announce (announcer (announcement Announcement) &rest args)
  "Get the audience list for the <announcer> <announcement> pair and
apply each notification function to the corresponding member of the
audience."
  (declare (type T announcer)
	   (type List args)
	   (:returns t))
  (dolist (item (audience announcer announcement)) 
    (apply #'ac:send-msg 
	   (second item) announcer (first item) (ac:new-id)
	   args))
  t)

;;;=======================================================

(defgeneric join-audience (announcer announcement audience-member
			   notification-function)
  #-sbcl (declare (type T announcer)
		  (type (or Symbol Announcement) announcement)
		  (type T audience-member)
		  (type az:Funcallable notification-function)
		  (:returns t))
  (:documentation
   "If <audience-member> is not already represented in the
audience for the <announcer> <announcement> pair, then add a new
item. Otherwise, just update the notification function."))

(defmethod join-audience (announcer (announcement Symbol)
			  audience-member notification-function)
  
  "Look up the announcement with <announcement> and recurse."
  
  (declare (type T announcer)
	   (type Symbol announcement)
	   (type T audience-member)
	   (type az:Funcallable notification-function)
	   (:returns t))
  (let ((ann (find-announcement announcement)))
    (if (null ann)
	(error "No announcement named: ~a" announcement)
      ;;else
      (join-audience announcer ann audience-member notification-function))
    ann))

(defmethod join-audience (announcer (announcement Announcement)
			  audience-member notification-function)
  
  "If <audience-member> is not already represented in the
audience for the <announcer> <announcement> pair, then add a new
item. Otherwise, just update the notification function."
  
  (declare (type T announcer)
	   (type Announcement announcement)
	   (type T audience-member)
	   (type az:Funcallable notification-function)
	   (:returns t))

  (let* ((audience (audience announcer announcement))
	 (item (assoc audience-member audience)))
    (declare (type List audience item))
    (if item
	(setf (second item) notification-function)
      ;; else
      (push (list audience-member notification-function)
	    (audience announcer announcement))))
  announcement)

;;;=======================================================

(defgeneric leave-audience (announcer announcement audience-member)
  #-sbcl (declare (type T announcer audience-member)
		  (type (or Symbol Announcement) announcement)
		  (:returns t))
  (:documentation
   "Remove the item corresponding to <audience-member> from the 
audience list for the <announcer> <announcement> pair."))

(defmethod leave-audience (announcer (announcement Symbol) audience-member)
  "Look up the announcement for <announcement> and recurse."
  (declare (type T announcer audience-member)
	   (type Symbol announcement)
	   (:returns t))

 (leave-audience announcer (find-announcement announcement) audience-member)
 t)

(defmethod leave-audience (announcer (announcement Announcement)
			   audience-member)
  "Remove the item corresponding to <audience-member> from the 
audience list for the <announcer> <announcement> pair (if there is one)."
  (declare (type T announcer audience-member)
	   (type (or Symbol Announcement) announcement)
	   (:returns t))
  (setf (audience announcer announcement)
    (delete-if #'(lambda (item) (eql audience-member (first item)))
	       (audience announcer announcement)))
  t)

;;;=======================================================

(defgeneric leave-audience-for-all-announcers (announcement audience-member)
  #-sbcl (declare (type T audience-member)
		  (type (or Symbol Announcement) announcement)
		  (:returns t))
  (:documentation
   "Remove all the items corresponding to <audience-member> from the 
audience list for any announcer and this <announcement>."))

(defmethod leave-audience-for-all-announcers ((announcement Symbol)
					      audience-member)
  "Look up the announcement for <announcement> and recurse."
  (declare (type T audience-member)
	   (type Symbol announcement)
	   (:returns t))

  (leave-audience-for-all-announcers (find-announcement announcement)
				     audience-member)
  t)

(defmethod leave-audience-for-all-announcers ((announcement Announcement)
					      audience-member)
  "Remove all the items corresponding to <audience-member> from the 
audience list for any announcer and this <announcement>."
  (declare (type T audience-member)
	   (type Announcement announcement)
	   (:returns t))
  (maphash
   #'(lambda (announcer audience)
       (setf (audience announcer announcement)
	 (delete-if #'(lambda (item) (eql audience-member (first item)))
		    audience)))
   (audience-table announcement))
     
  t)

;;;=======================================================

(defun leave-all-audiences (audience-member)
  (declare (type T audience-member)
	   (:returns t))
  ;;(format t "~&~a leaving all audiences." audience-member)
  (maphash
   #'(lambda (name announcement)
       (declare (type Symbol name)
		(type Announcement announcement)
		(ignore name))
       (leave-audience-for-all-announcers announcement audience-member))
   *announcements*)
  t)

;;;=======================================================

(defun dead-announcer (announcer) 

  "This should be called when the <announcer> is killed.  It indicates
that the <announcer> will never make an announcement again, and so all
the space devoted to its audiences can be reclaimed."  
  
  (declare (type T announcer))

  (maphash
   #'(lambda (name announcement)
       (declare (type Symbol name)
		(type Announcement announcement)
		(ignore name))
       (az:remove-entry announcer (audience-table announcement)))
   *announcements*))


