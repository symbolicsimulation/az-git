;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Clay 
  (:use :Common-Lisp #+:clos :CLOS)
  (:export 
   Clay-Object

   subject-gcontext
   subject-menu-items

   Paintbrush
   paintbrush-width
   paintbrush-height
   paintbrush-hue-sat
   paintbrush-aspect
   *paintbrush*

   palette
	    
   Diagram
   diagram-name
   diagram-menu
   diagram-subject
   diagram-parent
   diagram-display
   diagram-window
   diagram-image
   diagram-pixmap
   diagram-lens
   diagram-gcontext
   diagram-erasing-gcontext
   diagram-view-rect
   diagram-image-rect
   diagram-pixmap-rect
   diagram-window-rect
   scale
   window->pixmap
   pixmap->image
   pixmap->view
   diagram-minimum-width
   diagram-minimum-height
   diagram-children
   map-over-children
   diagram-root
	    
   initialize-diagram
	  
   build-diagram
   build-children
	    
   layout-diagram
   layout-children
   reconfigure-diagram
   ensure-big-enough-pixmaps

   activate-diagram
   activate-children
   initialize-paint-dependency

   render-diagram
   clip-to-diagram!
   draw-diagram
   spin-diagram
   erase-diagram
   redraw-diagram
   role-draw-diagram
   role-redraw-diagram

   role-paintbrush
   role-paint-gcontext

   Layout-Solver
   layout-domain
   layout-cost
   layout-constraints
   make-layout-domain
   make-layout-cost
   make-layout-constraints
	    
   Interactor-Diagram
   Button
   button-margin
   button-up-pixmap
   button-down-pixmap
   button-flat-pixmap
   draw-button-up-pixmap
   draw-button-down-pixmap
   draw-button-flat-pixmap

   Root-Diagram
   show-diagram
   hide-diagram
	    
   Menu-Button
   Menu
   make-menu
   diagram-show-menu

   Control-Panel
   make-control-panel
	    
   diagram-painter
   diagram-unmapped-role
   diagram-menu-items
	    
   diagram-frames
   record-diagram
   playback-diagram
   erase-record
   diagram-update-pixmaps
   update-internal
	    
   Leaf-Diagram
   Interactor-Leaf
	    
   Diagram-Role
   button-1-press
   button-2-press
   button-3-press
	    
   Unmapped-Role

   Painter
   initialize-painter
   painter-stroke
   painter-window-rect
   painter-diagram-rect
   painter-moved?
   paint-feedback
   borrow-stroke

   Stroke
   stroke-pixel
   stroke-pmask
   stroked-diagram
   stroke-objects-to-be-updated
   return-stroke
   mark-stroke
   stroke-object
	    
   Lens
   apply-lens

   Diagram-Mediator
   subject-mediators
   mediator-subject
   Mediator-Role

   Plot-Root
   plot-child
   plot-control-panel

   ;; layout package
   node-name
   Graph-Mediator
   Graph-Diagram))

