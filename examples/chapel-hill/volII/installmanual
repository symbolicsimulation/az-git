Large Installation Instructions for the Chapel Hill Volume Rendering Test
			Dataset, Volume II

The Chapel Hill Volume Rendering Test Dataset Volume II is available in two
different formats.  The files on the tape were written from a DEC
VAX computer using either the UNIX file copy command ``dd'' or the
UNIX ``tar'' command.  Each file is separated by a tape mark and there
are two tape marks at the end.  Total block size is 8192 bytes written
at 1600 bpi.  You should find the following files on the tape:

Announcement
Installation Instructions, CH02 (This document).
CT Cadaver Head data
CT Cadaver Head data information article
MR Brain data
MR Brain data information article
RNA data
RNA data information article

These are the instructions you should use to install your tape, depending
on what type of format you've received.

Using the ``dd'' command:  Users of systems other than VAXes will
probably have to reverse the bytes of the binary files.  Users of
UNIX systems can read the files with  dd if=/dev/nrmt0 of=disk_file_name
ibs=8k [conv=swap] with /dev/nrmt0 being the no-rewind device on the
machine.  The optional last field will cause automatic byte swapping.

Using the ``tar'' command:  Users of UNIX systems can read the
files with tar xvf /dev/nrmt0 with /dev/nrmt0 being the no-rewind
device on the machine.

To read over the tape marks:  Use mt -f /dev/nrmt0 fsf 1.

We do not object to your further distributing these datasets, but we
request that full acknowledgement of the source of the data accompany
such distribution.  If you're going to send a dataset to somebody, please
also send the accompanying information file.

We ask that you report any problems or enhancements to the following
address:

Pamela M. Payne
Mail:SoftLab Coordinator
SoftLab Software Systems Laboratory
University of North Carolina
Department of Computer Science
CB\# 3175, 351 Sitterson Hall
Chapel Hill, NC  27599-3175
Phone:(919) 962-1775
Electronic Mail:softlab@cs.unc.edu
