;;;-*- Package: :cl-user; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :cl-user) 

;;;============================================================

(defparameter *quasar-band-space*
    (make-instance 'mu:Complete-Band-Space
      :name "Quasars"
      :coordinate-dimensions '(176 274)
      :documentation "Simulated Galaxy Data"))

(defparameter *quasar-spectral-space*
    (make-instance 'mu:Spectral-Space
      :name "Quasar variables"
      :dimension 7
      :coordinate-names '#("xx" "yy" "zz" "vx" "vy" "vz" "mass")
      :documentation "A Space for quasar variables"))

(defparameter *quasar-format*
    (mu:make-basic-file-format
     :file-nrows 176
     :file-ncols 274
     :documentation "A Basic-File-Format for the Quasar data"))

(defparameter *quasar*
    (mu:make-mu :directory-name "../examples/quasar/"
		 :file-names (list "xx.byte" "yy.byte" "zz.byte"
				   "vx.byte" "vy.byte" "vz.byte"
				   "mass.byte")
		 :file-format *quasar-format*
		 :spectral-space *quasar-spectral-space*
		 :band-space *quasar-band-space*))

;;;============================================================

(defun count-items (line)
  (with-input-from-string (s line)
    (let ((count 0))
      (loop (if (read s nil nil)
	      (return count)
	      (incf count))))))
    
(defun count-lines (path)
  (declare (optimize (safety 0)
		     (speed 3)))
  (with-open-file (str path :direction :input)
    (let ((count 0))
      (declare (type az:Array-Index count))
      (loop
	(if (null (read-line str nil nil))
	  (return count)
	  (incf count))))))


(defun read-columns (n path)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Card8 n)
	   (type String path))
  (let ((vectors (make-array n))
	(m (print (count-lines path))))
    (declare (type (Simple-Array T (*)) vectors)
	     (type az:Array-Index m))
    (dotimes (j n) 
      (declare (type az:Card8 j))
      (setf (aref vectors j) 
	(make-array m 
		    :element-type 'Double-Float
		    :initial-element 0.0d0)))
    (with-open-file (str path :direction :input)
      (dotimes (i m)
	(declare (type az:Array-Index i))
	(dotimes (j n)
	  (declare (type az:Card8 j))
	  (let ((vj (aref vectors j)))
	    (declare (type az:Float-Vector vj))
	    (setf (aref vj i) (the Double-Float (read str t nil)))))))
    (values vectors)))


(defun minmax (v)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Float-Vector v))
  (let ((vmin most-positive-double-float)
	(vmax most-negative-double-float))
    (declare (type Double-Float vmin vmax))
    (dotimes (i (length v))
      (declare (type az:Array-Index i))
      (let ((x (aref v i)))
	(declare (type Double-Float x))
	(when (> x vmax) (setf vmax x))
	(when (< x vmin) (setf vmin x))))
    (values vmin vmax)))
	   
(defun distinct-values (v)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Float-Vector v))
  (let ((values ()))
    (declare (type List values))
    (dotimes (i (length v))
      (declare (type az:Array-Index i))
      (let ((new (aref v i)))
	(declare (type Double-Float new))
	(unless (member new values :test #'=)
	  (push new values)
	  #|| (print (cons i new))||#)))
    (print (length values))
    (pprint (sort values #'<))))

(defun rect-root (n)
  (declare (type (Integer 0 *) n))
  (let* ((lower 1)
	 (upper n)
	 (x0 2)
	 (x1 (ceiling n 2)))
    (loop (when (> x0 x1) (return))
      (let ((p (* x0 x1)))
	(cond 
	 ((= p n) 
	  (setf lower x0)
	  (setf upper x1)
	  (incf x0)
	  (setf x1 (min x1 (ceiling n x0))))
	 ((> p n) 
	  (decf x1)
	  (setf x0 (max x0 (truncate n x1))))
	 ((< p n) 
	  (incf x0)
	  (setf x1 (min x1 (ceiling n x0)))))))
    (values lower upper)))

(defun byte-normalize (in min max)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Float-Vector in)
	   (type Double-Float min max))
  (let ((out (az:make-card8-vector (length in)))
	(delta (- max min)))
    (declare (type az:Card8-Vector out)
	     (type Double-Float delta))
    (dotimes (i (length in))
      (declare (type az:Array-Index i))
      (setf (aref out i) (truncate (* 255.0d0 (- (aref in i) min)) delta)))
    out))

#||
((-0.498958 . 0.499934) -0.5 0.5
 (-0.49989 . 0.499901) 
 (-0.434385 . 0.49957)  
 (-3.31285 . 2.05017)   -3.4 3.2
 (-2.38278 . 2.64891) 
 (-2.23262 . 3.14418)
 (5.23278e-9 . 0.00137174)) 0.0 0.0014

(rect-root 48224)
176
274

(setf data (read-columns 7 "../examples/quasar/quasar3.fmt"))

(progn
  (az:write-card8-array "../examples/quasar/xx.byte" 
			48224 (byte-normalize (aref data 0) -0.5d0 0.5d0))
  (az:write-card8-array "../examples/quasar/yy.byte" 
			48224 (byte-normalize (aref data 1) -0.5d0 0.5d0))
  (az:write-card8-array "../examples/quasar/zz.byte" 
			48224 (byte-normalize (aref data 2) -0.5d0 0.5d0))
  (az:write-card8-array "../examples/quasar/vx.byte" 
			48224 (byte-normalize (aref data 3) -3.4d0 3.4d0))
  (az:write-card8-array "../examples/quasar/vy.byte" 
			48224 (byte-normalize (aref data 4) -3.4d0 3.4d0))
  (az:write-card8-array "../examples/quasar/vz.byte" 
			48224 (byte-normalize (aref data 5) -3.4d0 3.4d0))
  (az:write-card8-array "../examples/quasar/mass.byte" 
			48224 (byte-normalize (aref data 6) -0.0d0 0.0015d0)))

(defparameter *host* "fram")
(mu:make-imagegram *quasar* :left 400 :host *host*)

(mu:make-histogram (mu:band *quasar* "mass") 
		   :left 500 :height 128
		   :host *host*)

(defparameter *position*
    (mu:make-rotor (mu:band *quasar* "xx") 
		   (mu:band *quasar* "yy") 
		   (mu:band *quasar* "zz")
		   :left 600 :top 4 :host *host*))

(defparameter *velocity*
    (mu:make-rotor (mu:band *quasar* "vx") 
		   (mu:band *quasar* "vy") 
		   (mu:band *quasar* "vz")
		   :left 600 :top 4 :host *host*))

(time
 (progn
   (clay:erase-record (clay:plot-child *velocity*))
   (prof:start-profiler :type :time)
   (clay:spin-diagram (clay:plot-child *velocity*))
   (prof:stop-profiler)
   ))
(prof:show-call-graph)

(let ((plot (clay:plot-child *velocity*)))
  (dotimes (k 64)
    (print (cons (incf (mu:angle plot)
		       (mu:angular-resolution plot))
		 (setf (mu:min-count plot) k)))
    (xlt:drawable-force-output (clay:diagram-window plot))
    (finish-output))
  (setf (mu:min-count plot) 0))
(setf (mu:min-count (clay:plot-child *velocity*)) 1)
(setf (mu:min-count (clay:plot-child *velocity*)) 2)
(setf (mu:min-count (clay:plot-child *position*)) 1)
(setf (mu:min-count (clay:plot-child *position*)) 2)
||#
