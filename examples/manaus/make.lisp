;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: User -*-

;;; data-viewer dataset
zl-user::(let ((data-set-label
		 "Manaus Landsat Pixels")
	       (data-file
		 "belgica:/belgica-2g/jam/az/data/manaus/data")
	       (variable-labels
		 "belgica:/belgica-2g/jam/az/data/manaus/labels" )
	       (case-labels     nil))
	   (setq manaus-pixel-data  (make-integer-data-from-file
				      data-set-label
				      data-file
				      variable-labels
				      case-labels)))
#||

#2A((61 462 17 10 3 0)
    (62 462 17 10 3 0)
    (63 462 14 8 3 0)
    (64 462 14 10 3 0)
    (65 462 14 10 3 0)
    (66 462 14 10 0 0)
    (67 462 14 10 5 0)
    (61 461 13 11 4 0)
    (62 461 13 11 2 0)
    (63 461 13 9 2 0)
    (64 461 13 11 2 0)
    (65 461 13 11 6 0)
    (66 461 13 9 2 0)
    (67 461 13 11 2 0)
    (61 460 15 9 3 0)
    (62 460 11 11 3 0)
    (63 460 15 11 3 0)
    (64 460 15 11 3 0)
    (65 460 15 13 3 0)
    (66 460 15 11 0 0)
    (67 460 15 11 3 0)
    (61 459 15 10 1 0)
    (62 459 15 10 4 0)
    (63 459 15 12 1 0)
    (64 459 15 10 4 0)
    (65 459 12 10 4 0)
    (66 459 15 12 4 0)
    (67 459 15 10 4 0)
    (61 458 15 9 2 0)
    (62 458 12 9 5 0)
    (63 458 15 9 2 0)
    (64 458 15 9 2 0)
    (65 458 15 9 2 0)
    (66 458 15 9 2 0)
    (67 458 15 9 5 0)
    (61 457 15 10 2 0)
    (62 457 12 10 2 0)
    (63 457 15 10 2 0)
    (64 457 15 8 4 0)
    (65 457 15 8 4 0)
    (66 457 15 10 4 0)
    (67 457 15 10 4 0)
    (61 456 14 10 3 0)
    (62 456 14 8 5 0)
    (63 456 14 10 5 0)
    (64 456 14 10 3 0)
    (65 456 14 13 3 0)
    (66 456 14 10 0 0)
    (67 456 17 10 3 0)
    (61 455 13 11 2 0)
    (62 455 13 11 2 0)
    (63 455 13 11 4 0)
    (64 455 16 11 4 0)
    (65 455 16 11 2 0)
    (66 455 16 9 2 0)
    (67 455 13 9 2 0)
    (61 454 15 9 3 0)
    (62 454 15 9 3 0)
    (63 454 15 11 3 0)
    (64 454 15 11 3 0)
    (65 454 18 11 3 0)
    (66 454 15 11 0 0)
    (67 454 15 11 3 0)
    (659 948 21 15 54 47)
    (660 948 17 15 50 47)
    (661 948 17 15 50 51)
    (662 948 17 13 54 51)
    (663 948 17 13 50 47)
    (664 948 17 15 47 47)
    (665 948 17 15 54 47)
    (666 948 17 13 50 47)
    (667 948 17 13 47 51)
    (659 947 19 17 48 48)
    (660 947 16 14 48 48)
    (661 947 16 14 48 48)
    (662 947 16 14 46 48)
    (663 947 16 14 52 52)
    (664 947 16 17 57 52)
    (665 947 16 14 52 52)
    (666 947 19 17 48 48)
    (667 947 19 17 48 48)
    (659 946 15 15 47 41)
    (660 946 18 11 47 45)
    (661 946 18 15 53 50)
    (662 946 18 15 53 50)
    (663 946 15 15 47 50)
    (664 946 18 15 53 45)
    (665 946 18 15 53 50)
    (666 946 18 15 53 45)
    (667 946 18 15 53 50)
    (659 945 18 15 45 41)
    (660 945 18 15 45 41)
    (661 945 18 17 48 49)
    (662 945 18 15 56 53)
    (663 945 18 15 53 53)
    (664 945 18 15 53 49)
    (665 945 18 15 48 49)
    (666 945 18 17 56 53)
    (667 945 18 17 56 57)
    (659 944 18 17 54 48)
    (660 944 21 14 50 48)
    (661 944 18 12 50 48)
    (662 944 18 14 50 52)
    (663 944 18 14 50 48)
    (664 944 18 17 50 48)
    (665 944 18 14 50 48)
    (666 944 18 14 50 48)
    (667 944 18 14 50 44)
    (659 943 18 15 49 45)
    (660 943 18 15 44 49)
    (661 943 18 15 49 45)
    (662 943 18 15 51 54)
    (663 943 18 15 49 49)
    (664 943 18 13 49 49)
    (665 943 18 15 49 49)
    (666 943 20 15 51 49)
    (667 943 18 15 51 49)
    (659 942 17 13 47 47)
    (660 942 17 15 50 47)
    (661 942 17 15 50 51)
    (662 942 17 15 54 47)
    (663 942 17 15 54 47)
    (664 942 17 18 54 56)
    (665 942 17 15 54 51)
    (666 942 17 15 54 47)
    (667 942 21 15 50 51)
    (659 941 16 14 52 48)
    (660 941 19 17 52 52)
    (661 941 16 14 48 52)
    (662 941 16 17 52 48)
    (663 941 16 17 48 48)
    (664 941 16 17 52 52)
    (665 941 22 14 52 48)
    (666 941 19 17 57 52)
    (667 941 19 14 48 48)
    (659 940 18 15 53 45)
    (660 940 18 15 44 45)
    (661 940 18 18 53 50)
    (662 940 18 15 53 50)
    (663 940 22 15 55 54)
    (664 940 22 13 55 50)
    (665 940 18 15 53 50)
    (666 940 22 15 55 50)
    (667 940 15 15 53 50)
    (659 939 18 15 45 45)
    (660 939 18 12 53 45)
    (661 939 18 15 56 49)
    (662 939 18 15 48 45)
    (663 939 18 15 56 49)
    (664 939 18 15 53 49)
    (665 939 18 15 53 49)
    (666 939 18 17 48 49)
    (667 939 18 15 53 49)
    (283 244 32 36 17 0)
    (284 244 36 36 17 0)
    (285 244 32 36 17 0)
    (286 244 32 36 17 0)
    (287 244 32 38 17 0)
    (288 244 32 38 17 0)
    (289 244 32 36 14 0)
    (290 244 36 38 14 0)
    (291 244 32 38 17 0)
    (283 243 28 38 15 0)
    (284 243 31 38 15 0)
    (285 243 31 38 15 0)
    (286 243 31 36 18 0)
    (287 243 31 36 15 0)
    (288 243 31 38 18 0)
    (289 243 31 38 15 0)
    (290 243 31 38 15 0)
    (291 243 31 38 18 0)
    (283 242 30 37 15 0)
    (284 242 36 37 15 0)
    (285 242 36 40 15 0)
    (286 242 30 37 15 0)
    (287 242 36 37 13 0)
    (288 242 36 37 15 0)
    (289 242 30 37 15 0)
    (290 242 36 40 17 0)
    (291 242 30 40 15 0)
    (283 241 35 37 16 0)
    (284 241 35 37 16 0)
    (285 241 35 40 16 0)
    (286 241 35 37 16 0)
    (287 241 35 40 18 0)
    (288 241 32 40 16 0)
    (289 241 32 40 18 0)
    (290 241 35 37 14 0)
    (291 241 35 40 16 0)
    (283 240 38 39 16 0)
    (284 240 38 34 16 0)
    (285 240 38 37 16 0)
    (286 240 31 37 16 0)
    (287 240 31 42 16 0)
    (288 240 31 39 16 0)
    (289 240 38 37 16 0)
    (290 240 31 42 16 0)
    (291 240 31 37 16 0)
    (283 239 34 36 17 0)
    (284 239 34 36 15 0)
    (285 239 34 39 17 0)
    (286 239 34 39 15 1)
    (287 239 34 39 15 0)
    (288 239 34 36 17 0)
    (289 239 34 39 15 0)
    (290 239 34 39 15 0)
    (291 239 34 39 17 0)
    (283 238 36 36 17 0)
    (284 238 32 38 17 0)
    (285 238 32 38 17 0)
    (286 238 32 38 17 0)
    (287 238 32 38 17 0)
    (288 238 32 38 17 0)
    (289 238 32 38 17 0)
    (290 238 32 36 17 0)
    (291 238 32 38 19 0)
    (283 237 37 38 15 0)
    (284 237 31 38 18 0)
    (285 237 37 38 15 0)
    (286 237 31 38 18 0)
    (287 237 31 40 18 0)
    (288 237 37 38 15 0)
    (289 237 37 36 18 0)
    (290 237 31 38 20 0)
    (291 237 31 38 20 0)
    (947 197 19 14 68 69)
    (948 197 19 14 63 60)
    (949 197 19 14 59 56)
    (950 197 19 17 57 52)
    (951 197 19 14 52 52)
    (952 197 22 17 59 60)
    (953 197 22 17 68 65)
    (954 197 19 14 72 69)
    (947 196 18 15 55 54)
    (948 196 22 15 64 66)
    (949 196 22 15 61 58)
    (950 196 22 15 61 62)
    (951 196 22 18 64 66)
    (952 196 22 15 61 58)
    (953 196 22 15 64 58)
    (954 196 22 15 64 62)
    (947 195 18 17 62 62)
    (948 195 18 15 56 62)
    (949 195 18 15 62 57)
    (950 195 18 17 70 62)
    (951 195 18 17 64 66)
    (952 195 21 17 64 62)
    (953 195 21 17 62 66)
    (954 195 21 17 70 70)
    (947 194 21 17 65 70)
    (948 194 21 14 65 66)
    (949 194 18 14 60 66)
    (950 194 18 14 69 66)
    (951 194 18 17 69 70)
    (952 194 21 17 65 61)
    (953 194 18 17 69 70)
    (954 194 21 17 69 70)
    (947 193 18 20 77 76)
    (948 193 20 15 77 76)
    (949 193 20 15 72 72)
    (950 193 20 18 77 76)
    (951 193 20 20 77 72)
    (952 193 20 18 68 67)
    (953 193 20 15 61 58)
    (954 193 20 18 61 58)
    (947 192 21 18 74 72)
    (948 192 21 18 74 72)
    (949 192 21 15 59 64)
    (950 192 21 15 59 60)
             (951 192 21 18 70 64)
             (952 192 21 18 70 68)
             (953 192 24 18 70 68)
             (954 192 24 22 83 76)
             (947 191 22 14 59 60)
             (948 191 22 17 59 56)
             (949 191 19 17 57 52)
             (950 191 22 17 68 65)
             (951 191 22 17 72 65)
             (952 191 22 20 72 69)
             (953 191 22 14 76 69)
             (954 191 22 17 68 65)
             (947 190 22 18 77 75)
             (948 190 22 20 77 75)
             (949 190 22 18 61 54)
             (950 190 22 15 61 50)
             (951 190 25 15 69 66)
             (952 190 18 18 69 66)
             (953 190 22 15 61 58)
             (954 190 18 15 61 62)
             (947 189 18 17 75 70)
             (948 189 21 17 70 74)
             (949 189 25 17 78 70)
             (950 189 21 19 64 57)
             (951 189 25 22 62 53)
             (952 189 21 22 75 62)
             (953 189 18 17 53 49)
             (954 189 21 15 62 57)
             (947 188 21 17 73 70)
             (948 188 18 17 69 66)
             (949 188 21 17 77 70)
             (950 188 21 19 60 61)
             (951 188 24 19 54 44)
             (952 188 24 25 60 52)
             (953 188 21 19 54 44)
             (954 188 18 14 50 52)
             (648 630 17 13 43 39)
             (649 630 17 15 50 47)
             (650 630 21 15 62 56)
             (651 630 17 13 43 43)
             (652 630 17 13 54 51)
             (653 630 17 15 59 60)
             (654 630 17 15 61 60)
             (648 629 19 14 52 48)
             (649 629 19 14 57 56)
             (650 629 19 14 57 48)
             (651 629 19 14 48 44)
             (652 629 19 14 57 56)
             (653 629 19 17 68 65)
             (654 629 19 14 72 77)
             (648 628 18 15 61 54)
             (649 628 22 15 61 58)
             (650 628 22 18 69 66)
             (651 628 18 13 55 50)
             (652 628 18 15 53 50)
             (653 628 22 13 61 54)
             (654 628 18 15 61 62)
             (648 627 18 15 56 53)
             (649 627 18 15 62 62)
             (650 627 18 17 70 70)
             (651 627 18 15 62 57)
             (652 627 15 15 42 36)
             (653 627 18 15 53 53)
             (654 627 18 17 64 62)
             (648 626 18 14 54 52)
             (649 626 18 17 56 61)
             (650 626 21 19 69 70)
             (651 626 21 17 65 61)
             (652 626 15 9 43 35)
             (653 626 18 14 43 39)
             (654 626 21 17 60 66)
             (648 625 15 18 51 49)
             (649 625 18 18 63 63)
             (650 625 20 18 68 76)
             (651 625 20 13 61 67)
             (652 625 20 13 44 45)
             (653 625 15 13 42 36)
             (654 625 20 15 51 49)
             (648 624 17 15 50 43)
             (649 624 17 15 61 60)
             (650 624 21 18 70 72)
             (651 624 21 18 61 64)
             (652 624 17 13 41 35)
             (653 624 17 10 41 35)
             (654 624 21 10 43 43)
             (648 623 19 14 46 39)
             (649 623 19 14 52 48)
             (650 623 19 14 46 39)
             (651 623 16 9 24 18)
             (652 623 16 11 35 31)
             (653 623 19 14 52 48)
             (654 623 19 11 46 44)
             (648 622 15 11 17 8)
             (649 622 18 13 25 12)
             (650 622 15 13 25 8)
             (651 622 15 13 19 3)
             (652 622 18 13 19 8)
             (653 622 15 13 19 8)
             (654 622 15 13 22 12)
             (648 621 18 17 34 24)
             (649 621 18 17 40 28)
             (650 621 18 17 40 28)
             (651 621 18 15 45 36)
             (652 621 21 15 45 36)
             (653 621 18 15 42 32)
             (654 621 18 15 40 24)
             (648 620 18 14 30 22)
             (649 620 18 14 39 35)
             (650 620 18 17 50 44)
             (651 620 18 14 54 44)
             (652 620 18 14 54 48)
             (653 620 18 14 56 52)
             (654 620 21 17 54 48)
             (648 619 15 13 11 0)
             (649 619 15 10 16 0)
             (650 619 15 8 11 0)
             (651 619 15 10 11 5)
             (652 619 15 10 14 5)
             (653 619 15 10 11 5)
             (654 619 15 13 16 5)
             (648 618 17 13 41 31)
             (649 618 21 15 54 47)
             (650 618 21 15 41 35)
             (651 618 21 15 34 27)
             (652 618 14 13 31 14)
             (653 618 17 13 25 18)
             (654 618 17 13 29 18)
             (648 617 16 14 30 27)
             (649 617 19 14 46 39)
             (650 617 19 14 39 31)
             (651 617 16 14 24 14)
             (652 617 16 14 17 1)
             (653 617 16 11 10 0)
             (654 617 13 11 6 0)
             (648 616 15 11 11 0)
             (649 616 15 13 14 0)
             (650 616 15 13 14 3)
             (651 616 11 11 6 0)
             (652 616 15 11 3 0)
             (653 616 15 11 6 0)
             (654 616 15 13 0 0)
             (648 615 15 10 4 0)
             (649 615 12 10 1 0)
             (650 615 12 12 4 0)
             (651 615 15 12 7 0)
             (652 615 15 12 1 0)
             (653 615 15 15 1 0)
             (654 615 15 12 4 0)
             (648 614 15 12 5 0)
             (649 614 15 12 5 0)
             (650 614 12 12 0 0)
             (651 614 15 14 7 0)
             (652 614 15 12 5 0)
             (653 614 12 12 5 0)
             (654 614 18 9 7 0)
             (648 613 15 13 2 0)
             (649 613 15 13 4 0)
             (650 613 12 13 7 0)
             (651 613 15 10 2 0)
             (652 613 15 10 7 0)
             (653 613 12 13 4 0)
             (654 613 15 10 4 0)
             (648 612 14 10 5 0)
             (649 612 14 13 3 0)
             (650 612 14 10 3 0)
             (651 612 14 13 3 0)
             (652 612 14 10 3 0)
             (653 612 17 10 3 0)
             (654 612 17 10 5 0)
             (648 611 16 11 4 0)
             (649 611 13 11 4 0)
             (650 611 13 14 4 0)
             (651 611 13 11 6 0)
             (652 611 16 11 4 0)
             (653 611 16 11 4 0)
             (654 611 13 11 2 0)
             (648 610 15 11 3 0)
             (649 610 15 13 6 0)
             (650 610 15 13 3 0)
             (651 610 15 13 3 0)
             (652 610 15 13 3 0)
             (653 610 15 13 6 0)
             (654 610 18 13 6 0)
             (648 609 15 12 1 0)
             (649 609 15 15 4 0)
             (650 609 15 15 4 0)
             (651 609 12 15 4 0)
             (652 609 18 12 4 0)
             (653 609 15 15 4 0)
             (654 609 18 12 4 0)
             (648 608 15 14 5 0)
             (649 608 15 14 7 0)
             (650 608 18 14 5 0)
             (651 608 15 14 5 0)
             (652 608 15 14 7 0)
             (653 608 15 14 5 0)
             (654 608 15 14 7 0)
             (648 607 18 13 7 0)
             (649 607 15 13 4 0)
             (650 607 15 15 4 0)
             (651 607 15 15 4 0)
             (652 607 18 15 7 0)
             (653 607 18 15 7 0)
             (654 607 18 15 9 0)
             (648 606 17 15 3 0)
             (649 606 17 15 5 0)
             (650 606 14 15 5 0)
             (651 606 17 15 5 0)
             (652 606 17 15 5 0)
             (653 606 17 15 5 0)
             (654 606 17 15 5 0)
             (648 605 16 17 4 0)
             (649 605 16 17 6 0)
             (650 605 16 14 6 0)
             (651 605 16 14 6 0)
             (652 605 16 17 4 0)
             (653 605 16 14 6 0)
             (654 605 16 17 8 0)
             (648 604 15 15 6 0)
             (649 604 15 15 6 0)
             (650 604 15 13 6 0)
             (651 604 18 15 6 0)
             (652 604 15 18 6 0)
             (653 604 15 15 8 0)
             (654 604 18 15 6 0)
             (648 603 15 15 4 0)
             (649 603 15 12 7 0)
             (650 603 15 17 7 0)
             (651 603 18 17 7 0)
             (652 603 15 15 7 0)
             (653 603 15 15 7 0)
             (654 603 15 15 4 0)
             (648 602 15 12 7 0)
             (649 602 18 14 7 0)
             (650 602 15 12 7 0)
             (651 602 15 14 7 0)
             (652 602 15 14 7 0)
             (653 602 15 14 7 0)
             (654 602 15 17 7 0)
             (648 601 15 13 7 0)
             (649 601 15 13 7 0)
             (650 601 15 13 4 0)
             (651 601 15 15 2 0)
             (652 601 15 15 4 0)
             (653 601 15 15 4 0)
             (654 601 15 15 4 0)
             (648 600 17 15 5 0)
             (649 600 17 15 5 0)
             (650 600 14 15 7 0)
             (651 600 14 15 5 0)
             (652 600 17 20 5 0)
             (653 600 17 18 9 0)
             (654 600 17 15 7 0)
             (648 599 19 17 8 0)
             (649 599 16 17 6 0)
             (650 599 19 17 6 0)
             (651 599 19 20 6 0)
             (652 599 16 20 8 0)
             (653 599 16 17 8 0)
             (654 599 19 20 6 0)
             (648 598 18 18 8 0)
             (649 598 18 18 3 0)
             (650 598 18 20 8 0)
             (651 598 18 18 6 0)
             (652 598 18 18 6 0)
             (653 598 18 18 6 0)
             (654 598 18 18 8 0)
             (648 597 15 17 7 0)
             (649 597 18 17 7 0)
             (650 597 18 17 7 0)
             (651 597 18 17 4 0)
             (652 597 18 19 7 0)
             (653 597 21 19 9 0)
             (654 597 18 17 7 0)
             (648 596 18 17 9 0)
             (649 596 18 17 7 0)
             (650 596 18 17 5 0)
             (651 596 18 17 7 0)
             (652 596 18 17 7 0)
             (653 596 18 17 7 0)
             (654 596 18 17 9 0)
             (648 595 15 18 7 0)
             (649 595 15 18 7 0)
             (650 595 15 15 7 0)
             (651 595 15 15 7 0)
             (652 595 15 18 4 0)
             (653 595 18 20 4 0)
             (654 595 18 20 7 0)
             (648 594 17 18 3 0)
             (649 594 17 18 5 0)
             (650 594 17 18 7 0)
             (651 594 17 18 3 0)
             (652 594 17 18 7 0)
             (653 594 17 18 7 0)
             (654 594 17 15 7 0)
             (648 593 16 17 6 0)
             (649 593 16 17 6 0)
             (650 593 16 17 8 0)
             (651 593 16 17 6 0)
             (652 593 16 17 6 0)
             (653 593 16 17 6 0)
             (654 593 19 20 6 0)
             (648 592 15 18 6 0)
             (649 592 15 18 6 0)
             (650 592 18 18 6 0)
             (651 592 15 18 6 0)
             (652 592 18 15 6 0)
             (653 592 18 18 8 0)
             (654 592 18 20 8 0)
             (648 591 18 19 7 0)
             (649 591 18 17 7 0)
             (650 591 18 17 7 0)
             (651 591 18 19 4 0)
             (652 591 18 17 7 0)
             (653 591 18 17 7 0)
             (654 591 18 19 7 0)
             (648 590 18 17 7 0)
             (649 590 18 17 7 0)
             (650 590 18 17 7 0)
             (651 590 18 19 7 0)
             (652 590 21 19 7 0)
             (653 590 21 19 9 0)
             (654 590 18 19 9 0)
             (648 589 18 18 7 0)
             (649 589 18 20 9 0)
             (650 589 18 20 7 0)
             (651 589 20 20 9 0)
             (652 589 20 20 9 0)
             (653 589 20 23 9 0)
             (654 589 20 23 9 0)
             (648 588 21 20 7 0)
             (649 588 17 20 9 0)
             (650 588 17 22 7 0)
             (651 588 21 22 9 0)
             (652 588 21 25 7 0)
             (653 588 21 25 12 0)
             (654 588 21 22 9 0)
             (648 587 22 22 8 0)
             (649 587 19 25 10 0)
             (650 587 22 25 13 0)
             (651 587 19 25 10 0)
             (652 587 22 22 10 0)
             (653 587 22 25 10 0)
             (654 587 19 25 13 0)
             (648 586 22 25 8 0)
             (649 586 22 25 11 0)
             (650 586 22 22 11 0)
             (651 586 22 22 11 0)
             (652 586 22 25 11 0)
             (653 586 22 27 11 0)
             (654 586 22 27 14 0)
             (648 585 25 26 9 0)
             (649 585 21 26 9 0)
             (650 585 21 29 12 0)
             (651 585 25 26 12 0)
             (652 585 21 26 12 0)
             (653 585 25 26 12 0)
             (654 585 25 26 9 0)
             (648 584 21 27 11 0)
             (649 584 24 27 11 0)
             (650 584 24 27 13 0)
             (651 584 24 25 9 0)
             (652 584 24 27 13 0)
             (653 584 21 27 11 0)
             (654 584 21 30 11 0)
             (648 583 23 27 11 0)
             (649 583 23 27 11 0)
             (650 583 23 27 11 0)
             (651 583 20 30 11 0)
             (652 583 23 27 11 0)
             (653 583 23 25 11 0)
             (654 583 23 27 11 0)
             (648 582 24 25 12 0)
             (649 582 21 30 12 0)
             (650 582 21 30 14 0)
             (651 582 24 27 12 0)
             (652 582 24 27 14 0)
             (653 582 24 27 12 0)
             (654 582 24 30 9 0)
             (648 581 22 28 13 0)
             (649 581 25 28 15 0)
             (650 581 25 28 13 0)
             (651 581 25 31 15 0)
             (652 581 25 28 10 0)
             (653 581 25 31 15 0)
             (654 581 25 28 15 0)
             (648 580 25 29 14 0)
             (649 580 25 27 14 0)
             (650 580 25 29 17 0)
             (651 580 25 29 14 0)
             (652 580 29 34 14 0)
             (653 580 25 34 14 0)
             (654 580 29 34 14 0)
             (648 579 25 33 15 0)
             (649 579 28 33 15 0)
             (650 579 28 36 12 0)
             (651 579 28 33 12 0)
             (652 579 28 33 15 0)
             (653 579 28 33 18 0)
             (654 579 25 33 18 0)
             (648 578 30 30 11 0)
             (649 578 27 30 13 0)
             (650 578 27 32 15 0)
             (651 578 27 32 15 0)
             (652 578 27 32 15 0)
             (653 578 30 32 17 0)
             (654 578 27 32 17 0)
             (648 577 26 35 14 0)
             (649 577 32 35 16 0)
             (650 577 26 37 16 0)
             (651 577 26 37 16 0)
             (652 577 26 35 16 0)
             (653 577 26 35 14 0)
             (654 577 32 37 16 0)
             (648 576 28 34 14 0)
             (649 576 28 37 16 0)
             (650 576 28 34 14 0)
             (651 576 28 37 16 0)
             (652 576 28 37 14 0)
             (653 576 28 34 14 0)
             (654 576 31 34 14 0)
             (648 575 28 36 15 0)
             (649 575 28 36 15 0)
             (650 575 28 36 17 0)
             (651 575 37 36 17 0)
             (652 575 34 36 17 0)
             (653 575 28 31 17 0)
             (654 575 28 36 19 0)
             (648 574 29 36 19 0)
             (649 574 29 34 17 0)
             (650 574 32 38 17 0)
             (651 574 29 36 17 0)
             (652 574 29 36 14 0)
             (653 574 32 36 17 0)
             (654 574 29 36 17 0)
             (648 573 28 38 15 0)
             (649 573 31 38 18 0)
             (650 573 31 38 18 0)
             (651 573 31 38 18 0)
             (652 573 28 38 15 0)
             (653 573 31 36 18 0)
             (654 573 28 36 18 0)
             (648 572 30 40 17 0)
             (649 572 30 37 17 0)
             (650 572 30 37 15 0)
             (651 572 27 37 15 0)
             (652 572 30 37 15 0)
             (653 572 30 37 17 0)
             (654 572 30 40 15 0)
             (648 571 32 37 16 0)
             (649 571 32 37 16 0)
             (650 571 32 37 16 0)
             (651 571 32 37 16 0)
             (652 571 32 37 14 0)
             (653 571 35 37 18 0)
             (654 571 35 37 18 0)
             (648 570 28 37 16 0)
             (649 570 31 37 16 0)
             (650 570 31 37 16 0)
             (651 570 31 37 14 0)
             (652 570 28 34 14 0)
             (653 570 31 42 16 0)
             (654 570 31 37 14 0)
             (648 569 34 36 17 0)
             (649 569 34 39 17 0)
             (650 569 28 39 13 0)
             (651 569 34 42 17 0)
             (652 569 34 36 17 0)
             (653 569 37 36 17 0)
             (654 569 37 36 15 0)
             (648 568 32 36 17 0)
             (649 568 32 36 17 0)
             (650 568 32 36 17 0)
             (651 568 29 36 17 0)
             (652 568 32 38 17 0)
             (653 568 32 38 17 0)
             (654 568 32 36 17 0)
             (648 567 31 38 18 0)
             (649 567 31 38 20 0)
             (650 567 28 38 20 0)
             (651 567 31 36 18 0)
             (652 567 31 38 18 0)
             (653 567 31 38 18 0)
             (654 567 31 38 18 0)
             (648 566 30 37 15 0)
             (649 566 30 40 15 0)
             (650 566 30 40 15 0)
             (651 566 30 40 17 0)
             (652 566 30 37 17 0)
             (653 566 30 37 15 0)
             (654 566 30 37 17 0)
             (648 565 32 40 16 0)
             (649 565 32 37 14 0)
             (650 565 32 40 16 0)
             (651 565 32 37 18 0)
             (652 565 32 40 16 0)
             (653 565 32 37 16 0)
             (654 565 32 40 18 0)
             (648 564 31 37 18 0)
             (649 564 31 34 16 0)
             (650 564 31 39 14 0)
             (651 564 31 37 16 0)
             (652 564 28 37 16 0)
             (653 564 31 39 16 0)
             (654 564 31 37 16 0)
             (648 563 34 39 15 0)
             (649 563 34 39 15 0)
             (650 563 34 39 17 0)
             (651 563 34 39 15 0)
             (652 563 34 39 17 0)
             (653 563 34 36 15 0)
             (654 563 34 39 17 0)
             (648 562 32 38 17 0)
             (649 562 32 36 19 0)
             (650 562 32 36 17 0)
             (651 562 32 38 17 0)
             (652 562 36 38 14 0)
             (653 562 32 36 17 0)
             (654 562 32 36 14 0)
             (648 561 31 38 18 0)
             (649 561 31 38 18 0)
             (650 561 37 38 18 0)
             (651 561 31 38 15 0)
             (652 561 31 38 15 0)
             (653 561 31 38 15 0)
             (654 561 37 38 15 0)
             (648 560 30 40 15 0)
             (649 560 36 40 15 0)
             (650 560 30 37 13 0)
             (651 560 30 40 15 0)
             (652 560 36 37 17 0)
             (653 560 36 40 17 0)
             (654 560 36 40 15 0)
             (648 559 35 40 14 0)
             (649 559 32 40 16 0)
             (650 559 35 37 16 0)
             (651 559 35 37 16 0)
             (652 559 35 40 14 0)
             (653 559 35 37 16 0)
             (654 559 35 40 16 0)
             (648 558 38 39 16 0)
             (649 558 38 39 14 0)
             (650 558 31 39 14 0)
             (651 558 31 39 14 0)
             (652 558 31 37 14 0)
             (653 558 31 37 16 0)
             (654 558 31 37 14 0)
             (648 557 34 39 15 0)
             (649 557 34 39 15 0)
             (650 557 34 36 17 0)
             (651 557 34 39 15 0)
             (652 557 34 39 15 0)
             (653 557 34 36 17 0)
             (654 557 34 39 17 0)
             (648 556 32 36 14 0)
             (649 556 36 36 17 0)
             (650 556 32 38 17 0)
             (651 556 32 38 17 0)
             (652 556 32 38 14 0)
             (653 556 32 36 14 0)
             (654 556 32 38 14 0)
             (648 555 31 38 18 0)
             (649 555 31 38 18 0)
             (650 555 31 38 20 0)
             (651 555 31 38 18 0)
             (652 555 31 38 15 0)
             (653 555 31 40 18 0)
             (654 555 37 38 20 0)
             (648 554 36 40 17 0)
             (649 554 30 37 15 0)
             (650 554 30 40 17 0)
             (651 554 30 40 17 0)
             (652 554 36 40 17 0)
             (653 554 36 37 15 0)
             (654 554 36 37 15 0)
             (648 553 35 37 14 0)
             (649 553 35 37 18 0)
             (650 553 35 37 14 0)
             (651 553 35 37 16 0)
             (652 553 35 40 16 0)
             (653 553 32 37 16 0)
             (654 553 32 40 14 0)
             (648 552 31 37 16 0)
             (649 552 31 39 16 0)
             (650 552 31 37 16 0)
             (651 552 31 39 16 0)
             (652 552 31 37 16 0)
             (653 552 31 37 18 0)
             (654 552 31 39 16 0)
             (648 551 34 39 17 0)
             (649 551 34 39 15 0)
             (650 551 34 39 17 0)
             (651 551 34 39 15 0)
             (652 551 37 39 15 0)
             (653 551 37 39 17 0)
             (654 551 37 39 19 0)
             (648 550 32 41 17 0)
             (649 550 32 38 17 0)
             (650 550 36 38 17 0)
             (651 550 36 36 17 0)
             (652 550 36 38 17 0)
             (653 550 32 38 17 0)
             (654 550 32 38 17 0)
             (648 549 37 38 20 0)
             (649 549 37 38 18 0)
             (650 549 37 38 15 0)
             (651 549 31 38 18 0)
             (652 549 31 38 20 0)
             (653 549 31 38 20 0)
             (654 549 31 38 20 0)
             (648 548 30 37 17 0)
             (649 548 30 37 20 0)
             (650 548 36 37 22 0)
             (651 548 36 37 26 4)
             (652 548 39 40 30 13)
             (653 548 39 40 43 26)
             (654 548 36 40 54 35)
             (648 547 32 35 51 36)
             (649 547 32 37 56 49)
             (650 547 32 35 44 36)
             (651 547 32 30 42 27)
             (652 547 32 35 44 27)
             (653 547 32 35 42 31)
             (654 547 26 27 51 40)
             (648 546 24 20 61 56)
             (649 546 24 22 61 56)
             (650 546 28 22 59 51)
             (651 546 28 25 54 43)
             (652 546 28 30 50 39)
             (653 546 28 25 50 39)
             (654 546 24 20 54 47)
             (648 545 25 20 59 56)
             (649 545 25 22 68 56)
             (650 545 22 20 59 56)
             (651 545 28 25 59 52)
             (652 545 28 31 52 35)
             (653 545 28 25 48 35)
             (654 545 25 25 59 48)
             (648 544 29 25 64 50)
             (649 544 25 22 61 50)
             (650 544 25 22 61 54)
             (651 544 25 22 61 54)
             (652 544 25 22 55 45)
             (653 544 29 25 55 45)
             (654 544 29 25 53 41)
             (648 543 25 24 78 78)
             (649 543 28 22 89 78)
             (650 543 28 24 75 70)
             (651 543 28 24 70 62)
             (652 543 25 22 62 49)
             (653 543 25 24 56 45)
             (654 543 25 24 53 36)
             (648 542 24 22 82 74)
             (649 542 27 22 88 79)
             (650 542 27 22 82 79)
             (651 542 27 22 73 66)
             (652 542 21 19 65 52)
             (653 542 21 22 50 35)
             (654 542 21 25 32 22)
             (648 541 23 18 77 63)
             (649 541 23 23 82 72)
             (650 541 26 23 82 72)
             (651 541 23 20 63 54)
             (652 541 23 18 61 54)
             (653 541 23 23 61 49)
             (654 541 23 20 49 40)
             (648 540 24 20 65 64)
             (649 540 24 22 65 60)
             (650 540 24 22 59 51)
             (651 540 24 18 61 56)
             (652 540 24 20 70 64)
             (653 540 28 22 54 43)
             (654 540 24 20 41 23)
             (648 539 22 17 63 56)
             (649 539 25 22 72 56)
             (650 539 25 22 52 35)
             (651 539 25 20 59 44)
             (652 539 22 17 63 52)
             (653 539 22 17 59 48)
             (654 539 22 20 59 52)
             (648 538 25 20 75 62)
             (649 538 29 18 75 62)
             (650 538 25 22 69 50)
             (651 538 25 22 47 29)
             (652 538 25 20 55 45)
             (653 538 22 18 61 54)
             (654 538 22 15 55 50)
             (648 537 25 19 70 62)
             (649 537 25 19 70 70)
             (650 537 28 24 64 49)
             (651 537 25 22 53 28)
             (652 537 25 19 53 41)
             (653 537 21 19 48 45)
             (654 537 21 17 53 53)
             (648 536 21 19 69 70)
             (649 536 21 19 73 70)
             (650 536 27 22 69 52)
             (651 536 27 22 54 39)
             (652 536 24 17 45 39)
             (653 536 21 19 56 52)
             (654 536 21 17 60 52)
             (648 535 20 18 61 58)
             (649 535 23 20 63 63)
             (650 535 26 23 68 58)
             (651 535 26 25 68 54)
             (652 535 26 23 68 54)
             (653 535 26 20 72 58)
             (654 535 26 18 68 67)
             (648 534 28 22 74 68)
             (649 534 28 22 79 68)
             (650 534 28 22 79 72)
             (651 534 28 25 83 72)
             (652 534 28 20 79 72)
             (653 534 28 22 83 72)
             (654 534 28 22 83 68)
             (648 533 28 22 63 56)
             (649 533 28 22 63 56)
             (650 533 25 22 68 56)
             (651 533 25 22 63 48)
             (652 533 25 22 68 56)
             (653 533 25 25 68 56)
             (654 533 25 22 59 48)
             (648 532 29 25 69 54)
             (649 532 29 22 55 41)
             (650 532 25 22 53 37)
             (651 532 29 25 55 41)
             (652 532 29 25 53 41)
             (653 532 29 25 55 41)
             (654 532 25 25 61 45)
             (648 531 28 24 62 49)
             (649 531 28 24 53 41)
             (650 531 25 26 53 41)
             (651 531 28 26 56 45)
             (652 531 28 24 56 45)
             (653 531 28 24 56 45)
             (654 531 25 24 56 45))
||#