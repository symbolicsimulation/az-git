;;; -*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defun nlines (path)
  (with-open-file (file path :direction :input)
    (let ((i 0))
      (loop
	(unless (read-line file nil nil) (return))
	(incf i))
      i)))

(defun rewrite-county-graph (in-path edges-path config-path)
  (let ((i -1))
    (with-open-file (in in-path :direction :input)
      (with-open-file (out edges-path
			   :direction :output
			   :if-exists :overwrite)
	(with-open-file (config config-path
				:direction :output
				:if-exists :overwrite)
	  (loop
	    (incf i)
	    (let ((line (read-line in nil nil)))
	      (when (null line) (return))
	      (with-input-from-string (s line)
		;; discard starting x and y
		(fresh-line config)
		(format config "~d ~d ~d" i (read s nil nil) (read s nil nil))
		(fresh-line out)
		(loop
		  (let ((j (read s nil nil)))
		    (when (or (null j) (minusp j)) (return))
		    (format out "(~d ~d) " i j)
		    #||(when (< i j) (format out "(~d ~d) " i j))||#))))))))))

(defun read-county-graph (path &optional (max-edges 100))
  (let ((nodes ())
	(edges (list `(221 184))) ;; force the graph to be connected
	(line "")
	(j 0)
	(i 0))
    (with-open-file (file path :direction :input)
      (loop 
	(setf line (read-line file nil nil))
	(when (null line) (return))
	(push i nodes)
	(with-input-from-string (s line)
	  ;; discard starting x and y
	  (read s nil nil)
	  (read s nil nil)
	  (dotimes (k max-edges)
	    (setf j (read s nil nil))
	    (when (or (null j) (minusp j)) (return))
	    (when (< i j) (push (list i j) edges))))
	(incf i)))
    (values (make-instance 'simple-graph
	      :nodes (nreverse nodes)
	      :edges (nreverse edges)))))

(defun read-starting-configuration (path slate)
  (let* ((line "")
	 (n (nlines path))
	 (x (make-array n))
	 (y (make-array n))
	 (config (make-array (* 2 n) :element-type 'Double-Float))
	 (xmin most-positive-fixnum)
	 (xmax -1)
	 (ymin most-positive-fixnum)
	 (ymax -1)
	 j ax ay)
    (with-open-file (file path :direction :input)
      (dotimes (i n)
	(setf line (read-line file nil nil))
	;;(print line)
	(with-input-from-string (s line)
	  (let ((xi (read s nil nil))
		(yi (read s nil nil)))
	    ;;(format t "~%~d ~f ~f" i xi yi)
	    (setf (aref x i) xi)
	    (setf (aref y i) yi)
	    (when (< xi xmin) (setf xmin xi))
	    (when (> xi xmax) (setf xmax xi))
	    (when (< yi ymin) (setf ymin yi))
	    (when (> yi ymax) (setf ymax yi))))))
    (setf ax (/ (- (slate:slate-width slate) 30.0d0) (- xmax xmin)))
    (setf ay (/ (- (slate:slate-height slate) 30.0d0) (- ymin ymax)))
    ;;(format t "~%~f ~f" ax ay)
    (dotimes (i n)
      (setf j (* 2 i))
      (setf (aref config j) (* ax (- (aref x i) xmin)))
      (incf j)
      (setf (aref config j) (* ay (- (aref y i) ymax))))
    config))
	
	      
