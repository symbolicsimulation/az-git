;;; -*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
(in-package :Clay) 

(progn
  (defparameter *county-graph*
      (read-county-graph
       #+symbolics 
       "belgica:/belgica-2g/jam/az/examples/england/England.graph"
       #+:excl
       "/belgica-2g/jam/az/examples/england/England.graph"
       ))

  (defparameter *county-slate* (slate:make-slate :width 512 :height 640
						 :left 620 :top 220
						 :backing-store :always))

  (defparameter *county-start*
      (read-starting-configuration
       #+symbolics 
       "belgica:/belgica-2g/jam/az/examples/england/England.graph"
       #+:excl
       "/belgica-2g/jam/az/examples/england/England.graph" 
       *county-slate*))

  (time
   (defparameter *county-pr*
       (make-presentation-for-graph
	*county-graph* *county-slate*
	:initial-node-configuration *county-start*
	:diagram-name "English Counties"))))


(time
(progn
  (defparameter *solver* (layout-solver *county-pr*))
  (progn
    (setf (active-node-pairs *solver*)
    (all-unordered-pairs (nodes *county-pr*)))
    t)
  (compute-graph-distances *solver*)
  (1/d2-weights *solver*)
  (setf (rest-length *solver*) 32
    ;;(* 0.99 (estimate-solver-rest-length *solver*))
    )
  (solve-layout *solver*)
  ;;(time (swap-solve-layout *county-pr* :animate? nil))

  ;;(calculate-energy *solver* nil nil t)
  ))

(time 
   (progn (compute-graph-distances *county-pr*)
	  (solve-layout *county-solver*)))

(time (compute-graph-distances *county-pr*))

(time (solve-layout *county-solver*))