;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Graph
  (:use :Common-Lisp #+:clos :CLOS)
  (:nicknames :gra)
  (:export 
   ;; edge protocol
   edge-node0
   edge-node1
   implements-edge-protocol
   Generic-Edge

   ;; graph protocol
   directed-graph?
   acyclic-graph?
   nodes
   edges
   implements-graph-protocol?
   Generic-Graph

   ;; generic graph editing
   add-nodes-and-edges
   delete-nodes-and-edges
   ;;add-nodes-and-edges-event
   ;;delete-nodes-and-edges-event
	    

   ;; Sample implementations
   Graph make-graph
   Digraph make-digraph
   Dag make-dag
	    
   ;; utilities
   ;;collect-nodes-from-edges
   ;;all-unordered-pairs
   ;;make-complete-graph
   ;;random-graph
   ;;random-digraph
   ;;make-complete-nary-tree
   ;;random-nary-tree
   ))



