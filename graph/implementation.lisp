;;;-*- Package: :Graph; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Graph) 

;;;============================================================
;;; Sample implementations of Node protocol
;;;============================================================

;;;============================================================
;;; Sample implementation of Edge protocol
;;;============================================================
;;; 

(defmethod edge-node0 ((edge Cons))
  "A simple edge representation is a list of length 2.
The methods specialize for Cons, rather that List,
so that they don't match treat () as equivalent to (nil nil)."
  (declare (:returns node0))
  (first edge))

(defmethod edge-node1 ((edge Cons))
  "A simple edge representation is a list of length 2.
The methods specialize for Cons, rather that List,
so that they don't match treat () as equivalent to (nil nil)."
  (declare (:returns node1))
  (second edge))

;;;-----------------------------------------------------------
;;; useful functions

(defun collect-nodes-from-edges (edges)
  "Make a set (list) of node objects from a list of edge objects."
  (declare (type List edges)
	   (:returns (type List nodes)))
  (let ((nodes ()))
    (declare (type List nodes))
    (dolist (edge edges)
      (pushnew (edge-node0 edge) nodes)
      (pushnew (edge-node1 edge) nodes))
    nodes))

(defun all-unordered-pairs (l)
  "Collect all distinct pairs of items in <l>, ignoring order.
This can be used to create a complete graph from a list of nodes."
  (declare (type List l)
	   (:returns (type List pairs)))
  (let ((result ()))
    (do ((items l (rest items)))
	((null items))
      (declare (type List items))
      (let ((item0 (first items)))
	(dolist (item1 (rest items))
	  (push (list item0 item1) result))))
    result))

;;;============================================================
;;; Sample implementations of Graph protocol
;;;============================================================

(defclass Graph (az:Arizona-Object)
	  ((nodes
	    :type List
	    :accessor nodes
	    :initarg :nodes)
	   (edges
	    :type List
	    :accessor edges
	    :initarg :edges))
  (:documentation
   "A simple sample implementation of the Graph protocol."))

(defun make-graph (edges)
  "Make a graph from a list of its edges."
  (declare (type List edges)
	   (:returns (type Graph)))
  (make-instance 'Graph
		 :nodes (collect-nodes-from-edges edges)
		 :edges edges))

;;;-----------------------------------------------------------

(defmethod add-nodes-and-edges (nodes edges (graph Graph))
  "This is not as trivial as you might think, because we need to
maintain consistency between the graph's nodes and edges."
  (declare (type List nodes edges)
	   (:returns graph))
  (let ((new-nodes (set-difference nodes (nodes graph)))
	(new-edges (set-difference edges (edges graph))))
    (setf (nodes graph)
      (nunion (collect-nodes-from-edges new-edges)
	      (nconc new-nodes (nodes graph))))
    (setf (edges graph) (nconc new-edges (edges graph))))
  graph)

(defmethod delete-nodes-and-edges (nodes edges (graph Graph))
  "This is not as trivial as you might think, because we need to
maintain consistency between the graph's nodes and edges."
  (declare (type List nodes edges)
	   (:returns graph))
  (labels ((delete-edge? (edge)
	     (dolist (node nodes)
	       (when (or (eql node (edge-node0 edge))
			 (eql node (edge-node1 edge)))
		 (return-from delete-edge? t)))
	     nil))
    (setf (edges graph)
      (nset-difference (delete-if #'delete-edge? (edges graph)) edges)))
  (setf (nodes graph) (nset-difference (nodes graph) nodes))
  graph)

;;;-----------------------------------------------------------
;;; Announce an event to displays of this graph can be updated

(defmethod add-nodes-and-edges :after (nodes edges (graph Graph))
  "The after method announces that the graph has changed."
  (declare (type List nodes edges))
  nodes edges 
 (an:announce graph :changed))

(defmethod delete-nodes-and-edges :after (nodes edges (graph Graph))
  "The after method announces that the graph has changed."
  (declare (type List nodes edges))
  nodes edges
  (an:announce graph :changed))

;;;============================================================

(defclass DiGraph (Graph) ()
  (:documentation
   "Using a Digraph implies that the direction of the edges is meaningful."))

(defmethod directed-graph? ((g Digraph))
  (declare (:returns t))
  t)

(defun make-digraph (edges)
  "Make a Digraph from a list of edges."
  (declare (type List edges)
	   (:returns (type Digraph)))
  (make-instance 'DiGraph
		 :nodes (collect-nodes-from-edges edges)
		 :edges edges))

;;;============================================================

(defclass Dag (Digraph) ()
  (:documentation
   "Using a Dag implies there are no cycles,  but doesn't enforce it."))

(defmethod acyclic-graph? ((g Dag))
  "Dags are assumed to have no cycles."
  (declare (:returns t))
  t)

(defun make-dag (edges)
  "Make a Dag from a list of edges. It is not checked for cycles."
  (declare (type List edges)
	   (:returns (type Dag)))
  (make-instance 'Dag
		 :nodes (collect-nodes-from-edges edges)
		 :edges edges))

;;;============================================================
;;; useful functions for generating test graphs:

(defparameter *node-counter* -1)

(defun make-node () (incf *node-counter*))

(defun make-n-nodes (n)
  (let ((nodes ()))
    (dotimes (i n) (push (make-node) nodes))
    nodes))

;;;------------------------------------------------------------

(defun make-complete-graph (size)
  (setf *node-counter* -1)
  (let ((nodes (make-n-nodes size))
	(edges ()))
    (mapcon
      #'(lambda (remaining-nodes)
	  (let ((node (first remaining-nodes))
		(others (rest remaining-nodes)))
	    (dolist (other others) (push (list node other) edges))))
      nodes)
    (make-instance 'Graph :nodes nodes :edges edges)))

;;;------------------------------------------------------------

(defun random-graph (size edge-probability)
  (setf *node-counter* -1)
  (let ((nodes (make-n-nodes size))
	(edges ()))
    (mapcon
      #'(lambda (remaining-nodes)
	  (let ((node (first remaining-nodes))
		(others (rest remaining-nodes)))
	    (dolist (other others)
	      (when (<= (random 1.0) edge-probability)
		(push (list node other) edges)))))
      nodes)
    (make-instance 'Graph :nodes nodes :edges edges)))

;;;------------------------------------------------------------

(defun random-digraph (size edge-probability)
  (setf *node-counter* -1)
  (let ((nodes (make-n-nodes size))
	(edges ()))
    (mapcan
     #'(lambda (node)
	 (dolist (other nodes)
	   (unless (or (eq node other) 
		       (> (random 1.0) edge-probability))
	     (push (list node other) edges))))
     nodes)
    (make-instance 'DiGraph :nodes nodes :edges edges)))

;;;------------------------------------------------------------

(defun %make-complete-nary-tree (root n depth)
  (let* ((children (make-n-nodes n))
	 (nodes (copy-list children))
	 (edges (map 'list #'(lambda (child) (list root child)) children)))
    (unless (= depth 1)
      (dolist (child children)
	(multiple-value-bind
	  (more-nodes more-edges)
	    (%make-complete-nary-tree child n (- depth 1))
	  (setf nodes (nconc nodes more-nodes))
	  (setf edges (nconc edges more-edges)))))
    (values nodes edges)))

(defun make-complete-nary-tree (n depth)
  (setf *node-counter* -1)
  (let ((root (make-node)))
    (multiple-value-bind
      (nodes edges) (%make-complete-nary-tree root n depth)
      (make-instance 'Dag
		     :nodes (cons root nodes)
		     :edges edges))))

;;;------------------------------------------------------------

(defun generate-binomial (n p)
  (let ((sum 0))
    (dotimes (i n) (when (<= (random 1.0) p) (incf sum)))
    sum))

(defun %random-nary-tree (root n depth p)
  (let* ((children (make-n-nodes (generate-binomial n p)))
	 (nodes (copy-list children))
	 (edges (map 'list #'(lambda (child) (list root child)) children)))
    (unless (= depth 1)
      (dolist (child children)
	(multiple-value-bind
	  (more-nodes more-edges)
	    (%random-nary-tree child n (- depth 1) p)
	  (setf nodes (nconc nodes more-nodes))
	  (setf edges (nconc edges more-edges)))))
    (values nodes edges)))

(defun random-nary-tree (n depth p)
  (setf *node-counter* -1)
  (let ((root (make-node)))
    (multiple-value-bind
      (nodes edges) (%random-nary-tree root n depth p)
      (make-instance 'Dag
		     :nodes (cons root nodes)
		     :edges edges))))
