;;;-*- Package: :Graph; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Graph) 

;;;============================================================
;;; Generic graph edge protocol
;;;============================================================

(defgeneric edge-node0 (edge)
  #-sbcl (declare (type T edge))
  (:documentation
   "If the edge is directed, then it goes from edge-node0 to edge-node1."))

(defgeneric edge-node1 (edge)
  #-sbcl (declare (type T edge))
  (:documentation
   "If the edge is directed, then it goes from edge-node0 to edge-node1."))

;;;-----------------------------------------------------------

(defun implements-edge-protocol? (object)
  
  "Does this object have the necessary methods defined
for it to be treated as a generic graph?"

  (and (not (null (compute-applicable-methods #'edge-node0 (list object))))
       (not (null (compute-applicable-methods #'edge-node1 (list object))))))

(deftype Generic-Edge () '(satisfies implements-edge-protocol?))

;;;============================================================
;;; Generic graph protocol:
;;;============================================================
;;;
;;; These are generic operations that should be defined on anything
;;; that can be interpreted as a graph.
;;;-----------------------------------------------------------

(defgeneric directed-graph? (graph)
  #-sbcl (declare (type T graph)
		  (:returns (type az:Boolean)))
  (:documentation "Does this graph have directed edges?"))

(defmethod directed-graph? ((graph T))
  "The default is that a graph's edges  are not considered directed."
  (declare (:returns nil))
  nil)

(defgeneric acyclic-graph? (graph)
  #-sbcl (declare (type T graph)
		  (:returns (type az:Boolean)))
  (:documentation
   "Is this graph guaranteed to have no cycles (in its directed edges)?"))

(defmethod acyclic-graph? ((graph T))
  "The default is that a graph is not guaranteed to be acyclic;
note that this does not mean that the graph is guaranteed to have cycles."
  (declare (:returns nil))
  nil)

;;;-----------------------------------------------------------

(defgeneric nodes (graph)
  #-sbcl (declare (type T graph)
		  (:returns (type List)))
  (:documentation
   "Returns a list of the nodes in the graph.

For performance reasons, this may be the actual list of nodes used by
the graph. If this list is modified, the results are unpredictable.
It should be copied before any changes are made.  To change the graph
itself, use one of the editing operations, eg. <add-nodes-and-edges>."))

;;;-----------------------------------------------------------

(defgeneric edges (graph) 
  #-sbcl (declare (type T graph)
		  (:returns (type List))) 
  (:documentation
   "Returns a list of the edges in the graph.

For performance reasons, this may be the actual list of edges used by
the graph. If this list is modified, the results are unpredictable.
It should be copied before any changes are made.  To change the graph
itself, use one of the editing operations, eg. <add-nodes-and-edges>."))

;;;-----------------------------------------------------------

(defun implements-graph-protocol? (object)
  
  "Does this object have the necessary methods defined
for it to be treated as a generic graph?"

  (declare (type T object)
	   (:returns (type az:Boolean)))

  (and (not (null (compute-applicable-methods #'nodes (list object))))
       (not (null (compute-applicable-methods #'edges (list object))))))

(deftype Generic-Graph ()
  "A type for objects that have methods for the graph protocol."
  '(satisfies implements-graph-protocol?))

;;;============================================================
;;; Editing operations:
;;;============================================================

(defgeneric add-nodes-and-edges (nodes edges graph)
  #-sbcl (declare (type List nodes edges)
		  (type T graph)
		  (:returns graph))
  (:documentation
   "Destructively modify <graph> by adding <nodes> and <edges>.

Note that nodes are the more primitive objects. Adding a node to
a graph does not imply adding any edges, even though some node
objects may contain references to edges. Adding an edge, however,
does imply ensuring that its node are contained in the graph.
Also, deleting a node implies deleting the corresponding edges,
but deleting an edge has no effect on the nodes.

Methods are responsible for maintaining consistency in the graph's
nodes and edges. After the operation is complete, any node referred to
by an edge in (edges graph) must be in (nodes graph).

If an edge in the argument <edges> contains a node that is not in
either the argument <nodes> or in (nodes graph), the node must be
added automatically."))

(defgeneric delete-nodes-and-edges (nodes edges graph)
  #-sbcl (declare (type List nodes edges)
		  (type T graph)
		  (:returns graph))
  (:documentation
   "Destructively modify <graph> by deleting <nodes> and <edges>.

Note that nodes are the more primitive objects. Adding a node to
a graph does not imply adding any edges, even though some node
objects may contain references to edges. Adding an edge, however,
does imply ensuring that its node are contained in the graph.
Also, deleting a node implies deleting the corresponding edges,
but deleting an edge has no effect on the nodes.

Methods are responsible for maintaining consistency in the graph's
nodes and edges. After the operation is complete, any node referred to
by an edge in (edges graph) must be in (nodes graph).

In addition to deleting the edges in the argument <edges>, a method
must delete all edges corresponding to deleted <nodes>."))


