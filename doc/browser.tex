\documentstyle[11pt,code]{article}

\topmargin=-0.2in
\textwidth=6.0in
\textheight=8.0in
\oddsidemargin=0.25in
\evensidemargin=0.25in

\begin{document}

\title{A Graph Browser}

\author{{\sc John Alan McDonald}\\
        Dept. of Statistics, University of Washington\\
         }
\date{August 1991}

\maketitle

\section{Introduction}
\label{Introduction}

The purpose of this document is to describe a minimal interface
to a graph browser that is part of the Clay user interface toolkit,
a component of Arizona \cite{McDo88b,McDo91a,McDo91d,McDo91e,McDo91f}.
Clay is an object of current research and development;
the idea here is to provide a useful interface to its graph browser
which places as few constraints as possible on future changes.

I've separated :Clay (the user interface package) into 3 packages
(and 3 modules or systems), 
to better expose the interface. 
The 3 packages are :Graph, :Clay, and :Browser.
The corresponding systems are defined in az/az.system 
(along with a number of other systems not discussed here).
The latest version of the three systems is on both 
belgica.stat.washington.edu and eowyn.rad.washington.edu,
under my home directory.
All three systems are compiled by loading
az/browser/make.lisp and 
loaded by loading az/browser/load.lisp.

\section{A Protocol for Graph data structures}
\label{Protocol}

The Graph package is in directory az/graph. 
It defines a simple functional protocol for data structures
that are used to represent graphs 
and that are to be displayed by Clay's graph browser.

\subsection{Graph protocol}
\label{Graph-Protocol}

A graph object must have an immutable identity, 
for example,
you cannot represent a graph by a list and
cons new nodes or edges on the front.

A graph object must provide methods for four generic functions
(defined in protocol.lisp):

\begin{code}
(gr:nodes {\it graph}) 
\end{code}

returns a list of the nodes in the graph.  
Nodes may be anything.
Nodes are considered identical if they are eql.

\begin{code}
(gr:edges {\it graph}) 
\end{code}

returns a list of the edges in the graph.  
Edges must obey the edge protocol defined below.  
Edges are considered identical if they are eq.

If an object has methods for gr:nodes and gr:edges, 
then it is considered an instance of the abstract gr:Generic-Graph type
and satisfies
\begin{code}
(typep {\it object} 'gr:Generic-Graph)
\end{code}

Graphs that want to be considered directed and/or acyclic
should define appropriate methods for the following two generic functions:

\begin{code}
(gr:directed-graph? {\it graph}) 
\end{code}

returns T or Nil depending on whether the graph should be considered
directed. The default method returns nil.

\begin{code}
(gr:acyclic-graph? {\it graph})
\end{code}

returns T or Nil depending on whether the graph can be assumed to be
acyclic. The default method returns nil.

\subsection{Edge protocol}
An object representing a graph edge must have methods for

\begin{code}
(gr:edge-node0 {\it edge})
\end{code}

and 

\begin{code}
(gr:edge-node1 {\it edge}),
\end{code}

which return the nodes the edge connects.  If the edge is considered
directed, then it goes from gr:edge-node0 to gr:edge-node1.

If an object has methods for gr:edge-node0 and gr:edge-node1, 
then it is considered an instance of the abstract gr:Generic-Edge type
and satisfies
\begin{code}
(typep {\it object} 'gr:Generic-Edge)
\end{code}

\subsection{A Sample Implementation}
{Implementation}

The file implementation.lisp provides sample implementation classes
gr:Graph, gr:Digraph, and gr:Dag that obey the graph protocol and may
be useful base classes for more specialized graphs.

\section{Clay}
\label{Clay}

The Clay package, in az/clay and az/layout, provides
a generic graph browser that can be used with any object that 
obeys the graph protocol. 

A graph browser is created by a call to clay:make-graph-presentation,
which has one required argument, the graph to be presented.

\begin{code}
(defun make-graph-presentation (graph
				&key
				(diagram-name nil)
				(x-order-f nil)
				(y-order-f nil)
				(node-pair-distance-f nil)
                                (rest-length nil)
                                \ldots
				&allow-other-keys)

  (az:type-check gr:Generic-Graph graph)
  (az:type-check (or String Symbol) diagram-name)
  (az:type-check (or Symbol Function) 
                 x-order-f y-order-f node-pair-distance-f)
  (az:type-check (or Null Number) rest-length)

  \ldots)
\end{code}

:diagram-name will be used for the label of the window in which
the graph is displayed.

x-order-f, y-order-f, node-pair-distance-f,
and rest-length are used to customize the layout and are discussed 
in section~\ref{Layout}.

\section{Customizing Behavior}
\label{Customizing}

\subsection{Updating the display}
\label{Updating}

Currently, consistency between graphs and their presentations
is maintained using my implementation of Announcements
(in az/tools/announcements.lisp),
based on Mark Niehaus's implementation in Prism
of Sullivan-Notkin ideas about events/mediators \cite{Sull90a}.
A full discussion of events, mediators, announcements, etc.,
is beyond the scope of this document.

All a client of the graph browser needs to know is to
\begin{code}
(az:announce {\it graph} :graph-changed)
\end{code}
when it wants displays of that graph to update in response to addition
or deletion of nodes or edges to or from {\it graph}.

(The use of Announcements, rather than Prism Events,
was made for the benefit of my users in Statistics.
It would be easy to add an interface to Prism Events
if that were desired.)

\subsection{Menus}

The behavior of the graph browser can be
specialized by adding items to the menus
that are produced when the right (righthand) button is pressed.
If the mouse is within a node, then the node menu appears.
If the mouse button is not within a node,
and is within the rectangle enclosing an edge, 
then the edge menu appears. 
Otherwise the whole graph menu appears.

If your graph, nodes, and edges are instances of classes or structures
that you have defined,
then you can add menu items by defining new methods for the three
generic functions listed below.
Your method must return a (newly constructed) list of menu items. 
A menu item is a list whose first item is the string
that is printed in the menu, and whose second item is a function that
is applied to the remaining items.  
Note that methods for these generic function must be defined with
{\tt append} methods combination.
\begin{code}
(defgeneric clay:graph-node-menu (graph node)
  (declare (type T graph node))
  (:method-combination append))
\end{code}
\begin{code}
(defgeneric clay:graph-edge-menu (graph edge)
  (declare (type T graph edge))
  (:method-combination append))
\end{code}
\begin{code}
(defgeneric clay:graph-menu (graph)
  (declare (type T graph))
  (:method-combination append))
\end{code}

For example, here is method for clay:graph-node-menu 
defined in az/browser/class.lisp:

\begin{code}
(defmethod clay:graph-node-menu append ((graph Class-Graph)
					(node Class))
  (list (list (format nil "Add direct subclasses of ~a" node)
	      #'add-subclass-nodes node graph)
	(list (format nil "Add direct superclasses of ~a" node)
	      #'add-superclass-nodes node graph)))
\end{code}

\subsection{Layout}
\label{Layout}

The function make-graph-presentation takes several keyword arguments
than can be used to customize the layout of the graph.
\begin{itemize}

\item x-order-f, y-order-f

The graph layout is computed subject to the constraint that all nodes
remain in the window and optionally subject to constraints
that require node A to be above node B or node A to be to the left
of node B.

If supplied, x-order-f must be a function of two arguments that can be applied
to any pair of nodes in the graph.
It should return T if its first argument should be constrained to be to the
left of its second argument and Nil otherwise.

If supplied, y-order-f must be a function of two arguments that can be applied
to any pair of nodes in the graph.
It should return T if its first argument should be constrained to be above
its second argument and Nil otherwise.

It is better for performance reasons to use a minimal, non-transitive
ordering function.
For example,
if A is above B and B is above C,
it is not necessary to also constrain A to be above C.

If neither x-order-f or y-order-f are supplied then default constraints
will be imposed.
For graph objects that return T for both
gr:directed-graph? and gr:acyclic-graph?,
the default constraints are that parent nodes must be left of their children.
Otherwise, there are no default constraints 
(other than the bounds constraints that all nodes must remian in the window).

\item node-pair-distance-f

If supplied, node-pair-distance-f must be a function of two arguments 
that can be applied to any pair of nodes in the graph.
It must return a positive Double-Float number.

The layout algorithm tries to place nodes so that the distance between
them on the screen is proportional to the value returned by
node-pair-distance-f.

If node-pair-distance-f is not supplied, 
then it defaults to the graph distance, that is,
the number of edges between the two nodes in the shortest
path in the graph, ignoring any directedness of the edges.

\item rest-length

Rest-length determines the average length of the displayed edges
in the graph, in a somewhat complicated fashion.
The distance between the centers of two nodes will be roughly
the rest-length times the geometric average of the widths or heights
of the nodes times a given desired distance (see below).
The overall scale of the layout will change linearly with rest-length,
as long as all nodes remain in the interior of the window.
The exact result of a given rest-length with a particular
graph is hard to predict ahead of time.
10 seems to be a good value for many graphs.
\end{itemize}

\section{Examples}
\label{Examples}

The Browser module, in az/browser/, provides three examples of how
to use the graph browser.
Script.lisp has examples of how to create all three kinds of browsers.
 
The first two examples show how to use the graph browser
with minimal specialization --- essentially just adding menu items.
The first example is a CLOS class hierarchy browser, in class.lisp.  
The second example is a Lisp object browser, in examiner.lisp. 

The third example is demonstrates alternate layouts,
using custom order and distance functions.
It is contained in two files, pedigree.lisp and horses.lisp.
The pedigree can be layed out using either the default graph distance
or kinship, the probable genetic similarity between two individuals.

\bibliographystyle{plain} 

\bibliography{/belgica-2g/jam/bib/arizona,/belgica-2g/jam/bib/cs,/belgica-2g/jam/bib/cactus,/belgica-2g/jam/bib/constraint,/belgica-2g/jam/bib/database,/belgica-2g/jam/bib/layout,/belgica-2g/jam/bib/lisp,/belgica-2g/jam/bib/image,/belgica-2g/jam/bib/buja,/belgica-2g/jam/bib/mcdonald,/belgica-2g/jam/bib/friedman,/belgica-2g/jam/bib/wxs}

\end{document}
