;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================
;;; Some standard operation names

(defconstant _wild_op_  (string->operation-name "*")
  "The wildcard for JOIN_AUDIENCE templates.")

(defconstant _meta_op_ (string->operation-name "**") 
  "The wildcard for LEAVE_AUDIENCE templates.")

(defconstant _connected_ (string->operation-name "CONNECTED")
  "The operation name for the first message sent by the dispatcher
to an accepted connection.")

(defconstant _new_tool_identifier_request_ 
    (string->operation-name "NEW_TOOL_IDENTIFIER_REQUEST")
  "The operation name for requests to the dispatcher 
to issue a new tool identifier.")

(defconstant _new_tool_identifier_ 
    (string->operation-name "NEW_TOOL_IDENTIFIER")
  "The operation name for a message, sent by the dispatcher,
issuing a new tool identifer to a connection.")

(defconstant _unknown_address_ (string->operation-name "UNKNOWN_ADDRESS")
  "The operation name for an error message, sent by the dispatcher,
whne it is asked to send a message to an unknown address id.")

(defconstant _join_audience_ (string->operation-name "JOIN_AUDIENCE")
  "The operation name for JOIN_AUDIENCE requests to the dispatcher.")

(defconstant _leave_audience_ (string->operation-name "LEAVE_AUDIENCE")
  "The operation name for LEAVE_AUDIENCE requests to the dispatcher.")

(defconstant _tool_killed_ (string->operation-name "TOOL_KILLED")
  "The operation name for tool death announcements.")

(defconstant _dispatcher_killed_ (string->operation-name "DISPATCHER_KILLED")
  "The operation name for dispatcher death announcements.")

(defconstant _cl_eval_service_request_ 
    (string->operation-name "CL_EVAL_SERVICE_REQUEST")
  "The operation name for CL eval service requests.")

(defconstant _cl_eval_service_ (string->operation-name "CL_EVAL_SERVICE")
  "The operation name for CL eval service offers.")

(defconstant _cl_eval_ (string->operation-name "CL_EVAL")
  "The operation name for CL eval requests.")

(defconstant _cl_values_ (string->operation-name "CL_VALUES")
  "The operation name for CL eval responses.")

