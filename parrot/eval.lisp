;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(defmethod print-object ((tool Eval-Server) stream)
  "Print an Eval-Server as briefly as possible."
  (declare (type Eval-Server tool)
	   (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (tool stream)
   (format stream "Eval-Server ~a" (tool-identifier tool))))

;;;============================================================
;;; Connecting and disconnecting
;;;============================================================

(defun make-eval-server ()

  "An Eval Server wants to hear any requests for CL eval service,
so this after method adds it to the audience for CL_EVAL_SERVICE_REQUEST 
messages."

  (declare (:returns (type Eval-Server)))
  
  (let ((server (make-tool :class 'Eval-Server)))
    (declare (type Eval-Server server))
    (join-audience server _cl_eval_service_request_ _wild_tool_ _wild_tool_)
    server))

;;;============================================================
;;; Message handling
;;;============================================================

(defun CL_EVAL_SERVICE_REQUEST (tool id operation sender address)
  "Respond to a broadcast request for CL eval service.
An Eval-Server will receive copies of any CL_EVAL_SERVICE_REQUEST
broadcast messages, which have the form:

id sender CL_EVAL_SERVICE_REQUEST *

It will respond by sending a response msg back to <sender> whose body
is the header of the CL_EVAL_SERVICE_REQUEST message."
  
  (declare (type Eval-Server tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (ignore id operation address))
  
  (send-message _cl_eval_service_ tool sender))

;;;------------------------------------------------------------

(defun CL_EVAL (tool id operation sender address expression-chunk)

  "An Eval-Server will be sent CL_EVAL messages directly,
which have the form:
  
id sender CL_EVAL Eval-Server expression

The expression of the message is a string of ascii characters that is
read into a lisp expression with a call to <read-from-string>.  The
message is handled by calling <eval> on the lisp expression in the
default environment.  Errors may or may not be reported back to the
<sender>.  If the expression evaluates successfully, a response
message is sent back to <sender> with a printed representation of the
value returned from the call to <eval>."

  (declare (type Eval-Server tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (type Chunk expression-chunk)
	   (:returns (type Message-Identifier response-message-id)))

  (let ((expression (chunk->lisp expression-chunk)))
    (when t
      (print expression)
      (force-output))
    (send-message _cl_values_ tool sender 
		  (message-identifier->chunk id) 
		  (tool-identifier->chunk sender)
		  (operation-name->chunk operation)
		  (tool-identifier->chunk address)
		  (multiple-value-list (eval expression)))))
     
;;;============================================================
;;; A test client for eval dispatchers
;;;============================================================

(defmethod print-object ((tool Eval-Client) stream)
  "Print an Eval-Client as briefly as possible."
  (declare (type Eval-Client tool)
	   (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (tool stream)
   (format stream "Eval-Client ~a" (tool-identifier tool))))

;;;============================================================

(defun make-eval-client ()
  "This :after method attempts to find a CL eval dispatcher, 
by broadcasting a CL_EVAL_SERVICE_REQUEST message."
  (declare (:returns (type Eval-Client)))
  (let ((client (make-tool :class 'Eval-Client)))
    (declare (type Eval-Client client))
    (send-message _cl_eval_service_request_ client _wild_tool_)
    client))

;;;============================================================

(defun send-eval-request (tool expression)
  
  "Send an expression to be evaluated to the remote eval dispatcher.
This function always returns <t>, so the remote eval is essentially
done only for side effect. 

A more useful function might wait for the response, 
apply <read-from-string> to the body of the response message,
which is the printed representation of the value returned 
by the remote eval, and finally return the value produced 
by <read-from-string>. The reason we haven't done this for 
the sample eval client is that the simple sample eval dispatcher
doesn't guarantee a response."
  
  (declare (type Eval-Client tool)
	   (type List expression)
	   (:returns (type Message-Identifier message-id)))

  (send-message _cl_eval_ tool (eval-server-id tool) expression))

;;;============================================================

(defun CL_EVAL_SERVICE (tool id operation sender address)
  (declare (type Eval-Client tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (ignore id))
  (assert (tool-identifier= (tool-identifier tool) address))
  (assert (operation-name= _cl_eval_service_ operation))
  (when (null (eval-server-id tool))
    (when *debug?* (format t "~&setting eval-server id: ~a~%" sender))
    (print
     (setf (eval-server-id tool) sender)
     )
    (print (eval-server-id tool))
    (force-output)
    (join-audience tool _tool_killed_ sender _wild_tool_)))

;;;------------------------------------------------------------

(defgeneric CL_VALUES (tool id operation sender address
		       original-id original-sender 
		       original-operation original-address values)
  (:documentation
   "Do something with the response to a CL_EVAL message.")
  (declare (type Eval-Client tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address) 
	   (type Chunk original-id original-sender original-operation
		 original-address values)))


(defmethod CL_VALUES ((tool Eval-Client) id operation sender address
		      original-id original-sender 
		      original-operation original-address values)
  "Default is to just print the returned values."
  (declare (type Eval-Client tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address) 
	   (type Chunk original-id original-sender original-operation
		 original-address values))
  (when *debug?* 
    (format t "~&CL_VALUES: ~a ~a ~a ~a ~a~{ ~a~}~%"
	    tool id operation sender address
	    (list (chunk->message-identifier original-id)
		  (chunk->operation-name original-operation)
		  (chunk->tool-identifier original-sender)
		  (chunk->tool-identifier original-address))))

  (format t "~&CL_VALUES:~{ ~a~}~%" (chunk->lisp values))
  (force-output))

;;;------------------------------------------------------------

(defmethod TOOL_KILLED ((tool Eval-Client) id operation sender address)
  
  "Handle a TOOL_KILLED message from this client's eval server.
Try to find a new eval server."
  
  (declare (type Eval-Client tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address))
  
  (assert (operation-name= _tool_killed_ operation))
  (when *debug?*
    (print (list tool id operation sender address))
    (force-output))
  (when (tool-identifier= sender (eval-server-id tool))
    (setf (eval-server-id tool) nil)
    (send-message _cl_eval_service_request_ tool _wild_tool_)))





