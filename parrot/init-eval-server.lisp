;;;-*- Package: :cl-user; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package  :cl-user) 

;;;============================================================

#+:cmu (setf ext:*gc-verbose* nil)
#+:cmu (setf ext:*bytes-consed-between-gcs* 1000000)
#+:excl (setf (sys:gsgc-switch :print) nil)
#+:excl (setf excl:*tenured-bytes-limit* 2000000)

(load (truename
       (pathname
	(concatenate
	    'String
	  (namestring 
	   (make-pathname
	    :directory (pathname-directory *load-truename*)))
	  "../az.system"))))

(mk:load-system :Parrot)

(defparameter *eval-server* (parrot:make-eval-server))
(loop (system:serve-event))
