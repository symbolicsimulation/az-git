;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Parrot; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :cl-user)

(defpackage :Parrot
  (:use :Common-Lisp #+:clos :CLOS #+:pcl :PCL)
  (:nicknames :p)
  (:export 
   kill

   Host-Name 
   unix-hostname
   full-hostname
   Port

   atomic
   allow-sigio-handling
	    
   Tool-Identifier
   string->tool-identifier
	    
   tool-identifier->string
	    
   Chunk
   make-chunk
   fill-chunk
   copy-chunk
   chunk->string
   string->chunk
   chunk->lisp
   lisp->chunk

   message-id
   message-operation
   message-receiver
   message-sender
   message-address

   Tool
   make-tool
   send-message
   handle-message
   handler-function
	    
   Dispatcher
   the-dispatcher

   send-eval-request
   Eval-Server
   make-eval-server
   Eval-Client
   make-eval-client
	    
   ))

(declaim (declaration :returns))

#+:cmu17 (pushnew :returns PCL::*NON-VARIABLE-DECLARATIONS*)
