;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================
;;; borrowed from cmucl internet.lisp

(defconstant -sock-stream- 1)
(defconstant -sock-dgram- 2)
(defconstant -sock-raw- 3)

(defconstant -af-unix- 1)
(defconstant -af-inet- 2)

(defconstant -message-oob- 1)
(defconstant -message-peek- 2)
(defconstant -message-dontroute- 4)

(defconstant -c-null-char- (code-char 0)
  "The character that corresponds to the byte value used by C
to terminate strings.")

(defvar *internet-protocols*
  (list (list :stream 6 -sock-stream-)
	(list :data-gram 17 -sock-dgram-))
  "AList of socket kinds and protocol values.")

(defun internet-protocol (kind)
  (let ((entry (assoc kind *internet-protocols*)))
    (unless entry
      (error "Invalid kind (~S) for internet domain sockets." kind))
    (values (second entry)
	    (third entry))))
 
;;;============================================================

(deftype Socket () '(Integer 0 #.most-positive-fixnum)) 
;; really a unix file descriptor

;;;============================================================

(deftype Host-Name () 
  "A type for names of hosts that might be running a message dispatcher."
  'String)

#+:excl
(ff:defforeign 'gethostname
    :entry-point (ff:convert-to-lang "gethostname")
    :language :C
    :arguments '(Simple-String ;; hostname buffer
		 Fixnum ;; length of hostname buffer
		 )
    :print t
    :arg-checking t
    ;; :call-direct t
    :callback nil
    :return-type :Fixnum
    )

(defun unix-hostname ()
  "Return the hostname of the Unix host running this CL,
like the C {\tt gethostname}(2)."
  (declare (:returns (type String)))
  #+:cmu (unix:unix-gethostname)
  #+:excl 
  (let ((buffer (make-string 64)))
    (gethostname buffer (length buffer))
    (subseq buffer 0 (position -c-null-char- buffer))))

#+:excl
(ff:defforeign 'get_full_hostname
    :entry-point (ff:convert-to-lang "get_full_hostname")
    :language :C
    :arguments '(Simple-String ;; short hostname
		 Simple-String ;; full hostname
		 Fixnum ;; length of full hostname buffer
		 )
    :print t
    :arg-checking t
    ;; :call-direct t
    :callback nil
    :return-type :Fixnum
    )

(defun full-hostname (short-name)
  "Return the complete Internet name of the Unix host running this CL,
equivalent to the C:

\begin{code}
hostent = gethostbyname(short_name); 
return hostent->h-name;
\end{code}"
  
  (declare (type String short-name)
	   (:returns (type String)))
  #+:cmu 
  (ext:host-entry-name (ext:lookup-host-entry short-name))
  #+:excl 
  (let ((buffer (make-string 128))) 
    (get_full_hostname short-name buffer (length buffer))
    (subseq buffer 0 (position -c-null-char- buffer))))

;;;============================================================

(deftype Port ()
  "A type for port numbers at which message dispatchers listen for connection
requests."
  '(Integer 0 #.most-positive-fixnum))

(deftype Internet-Protocol () '(Member :datagram :stream))

;;;============================================================

#+:cmu
(defun print-unix-error (s)
  (declare (type Simple-String s)
	   (ignore s))
  (warn "~&~s~%" (unix:get-unix-error-msg)))

#+:excl
(ff:defforeign 'print-unix-error
    :entry-point (ff:convert-to-lang "perror")
    :language :C
    :arguments '(Simple-String ;; 
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :void
    )

(defun warn-with-unix-error-msg (format-string &rest format-args)
  (print-unix-error "")
  (apply #'warn format-string format-args))

;;;============================================================

(defun unix-environment-var (stringable)
  (declare (type (Or String Symbol) stringable))
  (let ((s (string stringable)))
    (declare (type String s))
    #+:cmu
    (cdr (assoc (intern s :keyword) ext:*environment-list*))
    #+:excl
    (system:getenv s)))

;;;============================================================

#||
(deftype Socket-Stream () 'sys:FD-Stream)

(defun make-socket-stream (socket &key (name nil))
  (declare (type Socket socket))
  (sys:make-fd-stream socket
		      :input t
		      :output t
		      :buffering :none
		      :auto-close t
		      :name name
		      :timeout 60))
||#

;;;============================================================
;;; network byte order functions

(declaim (inline htonl ntohl htons ntohs))

(defun htonl (x)
  #+:cmu (ext:htonl x)
  #+:excl (ipc::htonl x))

(defun ntohl (x)
  #+:cmu (ext:ntohl x)
  #+:excl (ipc::ntohl x))

(defun htons (x)
  #+:cmu (ext:htons x)
  #+:excl (ipc::htons x))

(defun ntohs (x)
  #+:cmu (ext:ntohs x)
  #+:excl (ipc::ntohs x))

;;;============================================================

#+:cmu
(defun accept-connection (socket)
  (declare (type Socket socket)
	   (:returns (type Fixnum)))
  (ext:accept-tcp-connection socket))

#+:excl
(ff:defforeign 'accept-connection
    :entry-point (ff:convert-to-lang "accept_connection")
    :language :C
    :arguments '(Fixnum) ;; socket file descriptor int
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; new socket file descriptor int
    )

;;;------------------------------------------------------------

#+:cmu
(defun make-connection-request-socket (port)
  (declare (type Port port))
  (let ((socket (make-socket :stream)))
    (declare (type Socket socket))
    (alien:with-alien ((sockaddr ext::inet-sockaddr))
      (setf (alien:slot sockaddr 'ext::family) -af-inet-)
      (setf (alien:slot sockaddr 'ext::port) (htons port))
      (setf (alien:slot sockaddr 'ext::addr) 0)
      (when (minusp (unix:unix-bind
		     socket
		     (alien:alien-sap sockaddr)
		     (alien:alien-size ext::inet-sockaddr :bytes)))
	(close-socket socket)
	(print-unix-error "")
	(error "Error binding socket to port ~s~%" port))
      (values
       (socket-port socket
		    (alien:alien-sap sockaddr)
		    (alien:alien-size ext::inet-sockaddr :bytes))
       socket))))

#+:excl
(ff:defforeign 'create-connection-request-socket
    :entry-point (ff:convert-to-lang "create_connection_request_socket")
    :language :C
    :arguments '((Simple-Array Fixnum (1)) ;; port int
		 (Simple-Array Fixnum (1))) ;; socket file descriptor int
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :fixnum
    )

#+:excl
(defun make-connection-request-socket (port)
  (let ((port-a (make-array '(1) :element-type 'Fixnum))
	(socket-a (make-array '(1) :element-type 'Fixnum)))
    (declare (type (Simple-Array Fixnum (1)) port-a socket-a))
    (setf (aref port-a 0) port)
    (create-connection-request-socket port-a socket-a)
    (values (aref port-a 0)
	    (aref socket-a 0))))

;;;------------------------------------------------------------

(declaim (inline unix-close))

#+:cmu
(defun unix-close (fd) (if (unix:unix-close fd) 0 -1))

#+:excl
(ff:defforeign 'unix-close
    :entry-point (ff:convert-to-lang "close")
    :language :C
    :arguments '(Fixnum) ;; file descriptor int
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum) 
 
(defun close-socket (socket)
  (declare (type Socket socket))
  (when (minusp (unix-close socket))
    (print-unix-error "")
    (error "~&problem closing socket ~s~%" socket)))

;;;------------------------------------------------------------

#+:cmu
(defun connect-to-port (host port)
  (declare (type Host-Name host)
	   (type Port port))
  (ext:connect-to-inet-socket host port))

#+:excl
(ff:defforeign 'connect-to-port
    :entry-point (ff:convert-to-lang "connect_to_port")
    :language :C
    :arguments '(Host-Name ;; host
		 Port) ;; port
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum)

;;;------------------------------------------------------------

#+:cmu 
(alien:def-alien-routine ("connection_requested_p" connection-requested?)
    c-call:int
  (socket c-call:int))

#+:excl
(ff:defforeign 'connection-requested?
    :entry-point (ff:convert-to-lang "connection_requested_p")
    :language :C
    :arguments '(Fixnum)
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum)

;;;------------------------------------------------------------

#+:cmu 
(alien:def-alien-routine ("ready_for_reading" ready_for_reading)
    c-call:int
  (socket c-call:int))

#+:excl
(ff:defforeign 'ready_for_reading
    :entry-point (ff:convert-to-lang "ready_for_reading")
    :language :C
    :arguments '(Fixnum)
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum)

(defun readable? (socket) 
  (declare (type Socket socket))
  (plusp (ready_for_reading socket)))

;;;------------------------------------------------------------

#+:cmu 
(alien:def-alien-routine ("ready_for_writing" ready_for_writing)
    c-call:int
  (socket c-call:int))

#+:excl
(ff:defforeign 'ready_for_writing
    :entry-point (ff:convert-to-lang "ready_for_writing")
    :language :C
    :arguments '(Fixnum)
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum)

(defun writeable? (socket)
  (declare (type Socket socket))
  (plusp (ready_for_writing socket)))

;;;------------------------------------------------------------

(declaim (inline unix-socket))

#+:cmu
(defun unix-socket (domain type protocol)
  (unix:unix-socket domain type protocol))

#+:excl
(ff:defforeign 'unix-socket
    :entry-point (ff:convert-to-lang "socket")
    :language :C
    :arguments '(Fixnum Fixnum Fixnum)
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum)

(defun make-socket (&optional (kind :stream))
  (multiple-value-bind (proto type) (internet-protocol kind)
    (let ((socket (unix-socket -af-inet- type proto)))
      (when (minusp socket)
	(print-unix-error "")
	(error "Error creating socket"))
      socket)))

;;;------------------------------------------------------------

(declaim (inline unix-listen))

#+:cmu
(defun unix-listen (socket timeout) (unix:unix-listen socket timeout))

#+:excl
(ff:defforeign 'unix-listen
    :entry-point (ff:convert-to-lang "listen")
    :language :C
    :arguments '(Fixnum ;; socket file descriptor int
		 Fixnum ;; timeout
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; is anything there?
    )

(defun listen-for-connection-requests (socket &key (timeout 5))
  (declare (type Socket socket)
	   (type (Integer 0 #.most-positive-fixnum) timeout)
	   (:returns (type Boolean success?)))
  (if (minusp (unix-listen socket timeout))
    (progn 
      (close-socket socket)
      (print-unix-error "")
      (cerror "Error listening to dispatcher connection request socket ~s"
	      socket)
      nil)
    ;; else
    t))
 
;;;------------------------------------------------------------

#+:cmu
(defun unix-read (socket buffer nbytes)
  (declare (type Socket socket)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) buffer)
	   (type (Integer 0 #.most-positive-fixnum) nbytes))
  (unix:unix-read socket (sys:vector-sap buffer) nbytes))

#+:excl
(ff:defforeign 'unix-read
    :entry-point (ff:convert-to-lang "read")
    :language :C
    :arguments '(Fixnum ;; socket file descriptor int
		 (Simple-Array (Unsigned-Byte 8) (*)) ;; buffer
		 Fixnum ;; nbytes
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; bytes actually read
    )

#+:cmu
(alien:def-alien-routine ("offset_read" offset_read)
    c-call:int
  (fd c-call:int)
  (buffer (* char))
  (offset c-call:int)
  (nbytes c-call:int))

#+:cmu
(defun offset-read (socket buffer offset nbytes)
  (offset_read socket (sys:vector-sap buffer) offset nbytes))

#+:excl
(ff:defforeign 'offset-read
    :entry-point (ff:convert-to-lang "offset_read")
    :language :C
    :arguments '(Fixnum ;; socket file descriptor int
		 (Simple-Array (Unsigned-Byte 8) (*)) ;; buffer
		 Fixnum ;; offset
		 Fixnum ;; nbytes
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; bytes written
    )

(defun read-from-socket (socket buffer nbytes)
  "Read nbytes from socket to buffer. Keep trying until done or error."
  (declare (type Socket socket)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) buffer)
	   (type (Integer 0 #.most-positive-fixnum) nbytes))
  (let ((remainder nbytes)
	(offset 0))
    (declare (type Fixnum remainder offset))
    (loop
      (let ((nread (offset-read socket buffer offset remainder)))
	(when (<= nread 0) (return nread))
	(decf remainder nread)
	(incf offset nread)
	(when (<= remainder 0) (return (- nbytes remainder)))
	(allow-sigio-handling)))))


;;;------------------------------------------------------------

#+:cmu
(defun unix-write (socket buffer nbytes)
  (declare (type Socket socket)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) buffer)
	   (type (Integer 0 #.most-positive-fixnum) nbytes))
  (unix:unix-write socket (sys:vector-sap buffer) 0 nbytes))

#+:excl
(ff:defforeign 'unix-write
    :entry-point (ff:convert-to-lang "write")
    :language :C
    :arguments '(Fixnum ;; socket file descriptor int
		 (Simple-Array (Unsigned-Byte 8) (*)) ;; buffer
		 Fixnum ;; nbytes
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; bytes written
    )


#+:cmu
(alien:def-alien-routine ("offset_write" offset_write)
    c-call:int
  (fd c-call:int)
  (buffer (* char))
  (offset c-call:int)
  (nbytes c-call:int))

#+:cmu
(defun offset-write (socket buffer offset nbytes)
  (offset_write socket (sys:vector-sap buffer) offset nbytes))

#+:excl
(ff:defforeign 'offset-write
    :entry-point (ff:convert-to-lang "offset_write")
    :language :C
    :arguments '(Fixnum ;; socket file descriptor int
		 (Simple-Array (Unsigned-Byte 8) (*)) ;; buffer
		 Fixnum ;; offset
		 Fixnum ;; nbytes
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; bytes written
    )

(defun write-to-socket (socket buffer nbytes)
  "Write nbytes of buffer to socket. Keep trying until done or error."
  (declare (type Socket socket)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) buffer)
	   (type (Integer 0 #.most-positive-fixnum) nbytes))
  (let ((remainder nbytes)
	(offset 0))
    (declare (type Fixnum remainder offset))
    (loop
      (let ((nwritten (offset-write socket buffer offset remainder)))
	(when (<= nwritten 0) (return nwritten))
	(decf remainder nwritten)
	(incf offset nwritten)
	(when (<= remainder 0) (return (- nbytes remainder)))
	(allow-sigio-handling)))))

;;;------------------------------------------------------------

#+:cmu
(alien:def-alien-routine ("socket_port" socket-port)
    c-call:int
  (socket c-call:int)
  (name (* ext::inet-sockaddr))
  (namelen c-call:int))

#+:excl
(ff:defforeign 'socket-port
    :entry-point (ff:convert-to-lang "socket_port")
    :language :C
    :arguments '(Fixnum
		;; Inet-Sockaddr
		 Fixnum)
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum)
 
;;;============================================================
;;; Interrupt (signal) driven io for sockets
;;;============================================================

#+:excl
(eval-when (compile load eval)
  (unless (sys:sigio-supported-p)
    (warn "~&socket input handlers won't work in this lisp.")))

(deftype Socket-Input-Handler ()
  #+:cmu 'cl::Handler
  #+:excl '(or Symbol Function))

(defun set-socket-input-handler (socket handler-fn)
  (declare (type Socket socket)
	   (type (or Symbol Function) handler-fn)
	   (:returns (type Socket-Input-Handler)))
  
  #+:cmu (sys:add-fd-handler socket :input handler-fn)
  #+:excl (progn (sys:set-sigio-handler socket handler-fn)
		 handler-fn))

(defun remove-socket-input-handler (socket handler)
  (declare (type Socket socket)
	   (type Socket-Input-Handler handler)
	   #+:cmu (ignore socket)
	   #+:excl (ignore handler))
  
  #+:cmu (sys:remove-fd-handler handler)
  #+:excl (sys:remove-sigio-handler socket))

