;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(defmethod initialize-instance :after ((connection Connection)
				       &rest initargs)
  "This :after method ensures that <connect> is called on every
Connection that is instantiated."
  (declare (type Connection connection)
	   (type List initargs)
	   (ignore initargs))
  (connect connection)
  (values))

;;;============================================================
;;; Connecting and disconnecting
;;;============================================================

(defgeneric connect (connection)
  (declare (type Connection connection)
	   (:returns (type (Member t nil) success?)))
  (:documentation
   "This is a generic function called to connect a connection
object to the corresponding dispatcher connection object and vice versa.
It gets called once on both sides for each connected pair.
It is called automatically in an :after method to <initialize-instance>
for all subclasses of Connection, so it should not never be called
by the user."))

;;;------------------------------------------------------------

(defmethod kill ((connection Connection))
  
  "This method for <kill> calls <disconnected>,
to ensure proper cleanup of the broken connection."
  
  (declare (type Connection connection)
	   (:returns (type Dead-Object connection)))
  
  (disconnected connection)
  (change-class connection 'Dead-Object))

;;;------------------------------------------------------------

(defgeneric disconnected (connection)
  (declare (type Connection connection)
	   (:returns t))
  (:documentation
   "This is a generic function called to clean up after
a connection is broken, or disconvered to have been broken.
Its methods should do as much cleanup as possible. For example,
on the dispatcher side, the method will remove the connection from the
dispatcher's connection table."))

(defmethod disconnected ((connection Connection))

  "This method disables and removes the input handler for the
connection's socket, so that any future io events on the socket are
ignored.  It sets the connection's reference to the socket to nil, so
it can be gc-ed, and calls <close-socket> on the socket."

  (declare (type Connection connection))
  
  (let ((socket (connection-socket connection)))
    (declare (type (or Null Socket) socket))
    ;; test in case it's already closed or null
    (when socket
      (remove-socket-input-handler socket (input-handler connection))
      (close-socket socket))
    (setf (slot-value connection 'input-handler) nil)
    (setf (slot-value connection 'connection-socket) nil)))

;;;------------------------------------------------------------

(defun disconnected? (connection)
  "Has the connection been broken?"
  (declare (type Connection connection)
	   (:returns (type (Member t nil))))
  (or (null (input-handler connection))
      (null (connection-socket connection))))

;;;============================================================
;;; Reading and writing over connections
;;;============================================================

(defun read-from-connection (connection buffer n)
  "Read exactly <n> bytes from the <connection> and put it in <buffer>.
Signal an error if there's any read error or if the read completes but
returns fewer bytes than expected."

  (declare (type Connection connection)
	   (type Chunk buffer)
	   (type (Integer 0 #.most-positive-fixnum) n)
	   (:returns (values (type Chunk buffer)
			     (type (Integer 0 #.most-positive-fixnum) 
				   bytes-read))))

  (fill buffer 0)
  (assert (<= n (length buffer)))
  
  (if (= n 0)
    ;; then there's nothing to read
    (values buffer 0)
    ;; else
    (if (null (connection-socket connection))
      ;; then something has gone wrong
      (progn
	(warn "~&socket nil, probably disconnected~%")
	(disconnected connection)
	(throw :disconnected (values buffer 0)))
      ;; else try to read
      (let ((bytes-read (read-from-socket (connection-socket connection)
					  buffer n)))
	(declare (type Fixnum bytes-read))
	(cond ((zerop bytes-read)
	       (warn-with-unix-error-msg
		"~&EOF reading on ~s, ~%disconnecting~%" connection)
	       (disconnected connection)
	       (throw :disconnected (values buffer 0)))
	      ((minusp bytes-read)
	       (warn-with-unix-error-msg
		"~&Error reading on ~s, ~%disconnecting~%" connection)
	       (disconnected connection)
	       (throw :disconnected (values buffer 0)))
	      ((< bytes-read n)
	       (warn "~&Only ~d bytes found on ~s, ~d wanted,~%disconnecting~%"
		     bytes-read connection n)
	       (disconnected connection)
	       (throw :disconnected (values buffer 0)))
	      (t (values buffer bytes-read)))))))

(defun write-to-connection (connection buffer n)
  "Write exactly <n> bytes from <buffer> on the <connection>.  Signal
an error if there's any write error or if the write completes but sends
fewer bytes than expected."
  (declare (type Connection connection)
	   (type (Integer 0 #.most-positive-fixnum) n)
	   (type Chunk buffer)
	   (:returns (type (Integer 0 #.most-positive-fixnum) bytes-written)))
  (assert (<= n (length buffer)))
  (if (= n 0)
    (values 0)
    ;; else
    (if (null (connection-socket connection))
      ;; something has gone wrong
      (progn
	(warn "~&socket nil, probably disconnected~%")
	(disconnected connection)
	(throw :disconnected (values buffer 0)))
      (let ((bytes-written
	     (write-to-socket (connection-socket connection) buffer n)))
	(declare (type Fixnum bytes-written))
	(cond
	 ((minusp bytes-written)
	  (warn-with-unix-error-msg
	   "~&Error writing ~d bytes of ~s on ~s, ~%disconnecting~%"
	   n buffer connection)
	  (disconnected connection)
	  (throw :disconnected (values buffer 0)))
	 ((< bytes-written n)
	  (warn 
	   "~&Only ~d of ~d bytes of ~s ~%written on ~s,~%disconnecting~%"
	   bytes-written n buffer connection)
	  (disconnected connection)
	  (throw :disconnected (values buffer 0))))
	(values bytes-written)))))

;;;------------------------------------------------------------

(defparameter *count-in-buffer* (make-chunk -chunk-count-nbytes-))
(declaim (type Chunk *count-in-buffer*))

(defun read-chunk-count (connection)
  (declare (type Connection connection)
	   (special *count-in-buffer*)
	   (:returns (type (Integer 0 #.most-positive-fixnum))))
  (read-from-connection connection *count-in-buffer* -chunk-count-nbytes-)
  #||
  (when *debug?*
    (let ((*print-array* t)
	  (*print-pretty* t)
	  (*print-length* nil))
      (format t "~&read count bytes = ~a~%" *count-in-buffer*)
      (force-output)))
  ||#
  (the (Integer 0 #.most-positive-fixnum)
    (logior (ash (aref *count-in-buffer* 0) 24)
	    (ash (aref *count-in-buffer* 1) 16)
	    (ash (aref *count-in-buffer* 2)  8)
	    (ash (aref *count-in-buffer* 3)  0))))

(defparameter *count-out-buffer* (make-chunk -chunk-count-nbytes-))
(declaim (type Chunk *out-buffer*))

(defun write-chunk-count (connection n)
  (declare (type Connection connection)
	   (special *count-out-buffer*)
	   (type (Integer 0 #.most-positive-fixnum) n)
	   (:returns (type (Integer 0 #.most-positive-fixnum))))
  (fill *count-out-buffer* 0)
  (setf (aref *count-out-buffer* 0) (logand (ash n -24) #xFF))
  (setf (aref *count-out-buffer* 1) (logand (ash n -16) #xFF))
  (setf (aref *count-out-buffer* 2) (logand (ash n  -8) #xFF))
  (setf (aref *count-out-buffer* 3) (logand (ash n   0) #xFF))
  #||
  (when *debug?*
    (let ((*print-array* t)
	  (*print-pretty* t)
	  (*print-length* nil))
      (format t "~&write count bytes = ~a~%" *count-out-buffer*)
      (force-output)))
  ||#
  (write-to-connection connection *count-out-buffer* -chunk-count-nbytes-)
  n)

;;;------------------------------------------------------------

(defparameter *length-in-buffer* (make-chunk -chunk-length-nbytes-))
(declaim (type Chunk *length-in-buffer*))

(defun read-chunk-length (connection)
  (declare (type Connection connection)
	   (special *length-in-buffer*)
	   (:returns (type (Integer 0 #.most-positive-fixnum))))
  (read-from-connection connection *length-in-buffer* -chunk-length-nbytes-)
  #||
  (when *debug?*
    (let ((*print-array* t)
	  (*print-pretty* t)
	  (*print-length* nil))
      (format t "~&read length bytes = ~a~%" *length-in-buffer*)
      (force-output)))
  ||#
  (the (Integer 0 #.most-positive-fixnum)
    (logior (ash (aref *length-in-buffer* 0) 24)
	    (ash (aref *length-in-buffer* 1) 16)
	    (ash (aref *length-in-buffer* 2)  8)
	    (ash (aref *length-in-buffer* 3)  0))))

(defparameter *length-out-buffer* (make-chunk -chunk-length-nbytes-))
(declaim (type Chunk *length-out-buffer*))

(defun write-chunk-length (connection n)
  (declare (type Connection connection)
	   (special *length-out-buffer*)
	   (type (Integer 0 #.most-positive-fixnum) n)
	   (:returns (type (Integer 0 #.most-positive-fixnum))))
  (fill *length-out-buffer* 0)
  (setf (aref *length-out-buffer* 0) (logand (ash n -24) #xFF))
  (setf (aref *length-out-buffer* 1) (logand (ash n -16) #xFF))
  (setf (aref *length-out-buffer* 2) (logand (ash n  -8) #xFF))
  (setf (aref *length-out-buffer* 3) (logand (ash n   0) #xFF))
  #||
  (when *debug?*
    (let ((*print-array* t)
	  (*print-pretty* t)
	  (*print-length* nil))
      (format t "~&write length bytes = ~a~%" *length-out-buffer*)
      (force-output)))
  ||#
  (write-to-connection connection *length-out-buffer* -chunk-length-nbytes-)
  n)

;;;------------------------------------------------------------

(defun read-chunk (connection)

  "Assuming the next incoming bytes on <connection> are a message
chunk, read it."

  (declare (type Connection connection)
	   (:returns (type Chunk chunk)))

  (let ((length (read-chunk-length connection)))
    (read-from-connection connection (make-chunk length) length)))

(defun write-chunk (connection chunk)

  "Write the <chunk> on the <connection."

  (declare (type Connection connection)
	   (type Chunk chunk)
	   (:returns (type Chunk chunk)))
  (write-chunk-length connection (length chunk))
  (write-to-connection connection chunk (length chunk))
  (values chunk))

;;;------------------------------------------------------------

(defgeneric transmit-message (connection chunks)
  (:documentation "Transmit a message over connection.")
  (declare (type Connection connection)
	   (type List chunks)))

(defmethod transmit-message ((connection Connection) chunks)
  "Transmit a message over <connection>.
Assume the message items are already in chunk form."
  (declare (type Connection connection)
	   (type List chunks))
  (assert (>= (length chunks) 5))
  (when *debug?*
    (format t "~&transmit-message ~%~s~%" connection)
    (print-message chunks)
    (force-output))
   (catch :disconnected
     (write-chunk-count connection (length chunks))
     (dolist (chunk chunks) (write-chunk connection chunk))))

(defgeneric receive-message (connection)
  (declare (type Connection connection)
	   (:returns (type List message)))
  (:documentation
   "This is a generic function which is called to read messages sent
over a Connection, returning what is read in a list.  It should not be
called directly, but instead will be called automatically whenever
there is input on the connection."))

(defmethod receive-message ((connection Connection)) 
  "Receive the next message on <connection>."
  (declare (type Connection connection))
  (let ((chunks ()))
    (declare (type List chunks))
    (dotimes (i (print (read-chunk-count connection))) 
      (push (read-chunk connection) chunks))
    (setf chunks (nreverse chunks))
    (assert (>= (length chunks) 5))
    (when *debug?*
      (format t "~&receive-message: ~s~%" connection)
      (print-message chunks)
      (force-output))
    chunks))







