;;;-*- Package: :cl-user; Syntax: Common-Lisp; Mode: Lisp -*-
(in-package  :cl-user) 

(setf p::*debug?* nil)

(defparameter d (p:the-dispatcher))
(defparameter c (p::default-connection))
(p:kill d)
(p:kill c)
(change-class c 'p::Dead-Object)

(defparameter es (p:make-eval-server))
(defparameter ec (p:make-eval-client))
(p:send-eval-request ec '(* 2 pi))
(p:send-eval-request ec '(truncate 3 2))
(p:send-eval-request ec '(type-of pi))
(p:send-eval-request ec '(format nil "~s~%" es))

(dotimes (i 50) (p:send-eval-request ec `(list ,i (truncate ,i 2))))
