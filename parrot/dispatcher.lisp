;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;------------------------------------------------------------

(defun parrot-host ()
  "The Parrot host can be specified in the environment variable
{\tt PARROT_HOST} or defaulted to the host running this CL."
  (full-hostname (or (unix-environment-var "PARROT_HOST")
		     (unix-hostname))))

(defun dot-path (host)
  (declare (type String host))
  (pathname (concatenate 'String 
	      (namestring (truename (user-homedir-pathname host)))
	      ".parrot")))

(defun write-dot-file (dispatcher)
  
  "Write the host and port used by {\tt dispatcher} into
the {\tt .parrot} file in the user's home directory on the dispatcher's host."
  
  (declare (type Dispatcher dispatcher))
  (with-open-file (stream (dot-path (dispatcher-host dispatcher))
		   :direction :output
		   :if-exists :overwrite
		   :if-does-not-exist :create)
    (format stream "~a:~a~%" 
	    (dispatcher-host dispatcher) 
	    (dispatcher-port dispatcher))))

(defun read-dot-file (host)
  "Read the dispatcher's host and port from the {\tt .parrot}
file in the user's home directory on {\tt host}."
  (with-open-file (stream (dot-path host) :direction :input)
    (let* ((string (read-line stream))
	   (colon-pos (position #\: string))
	   (host (subseq string 0 colon-pos))
	   (port (read-from-string (subseq string (+ 1 colon-pos)))))
      (values host port))))
  
;;;------------------------------------------------------------

(defmethod initialize-instance :after ((dispatcher Dispatcher) &rest initargs)
  
  "This :after method ensures that the Dispatcher is properly initialized.
It sets up a socket on which connection requests will appear, and establishes 
an input handler function for that socket."
  
  (declare (type Dispatcher dispatcher)
	   (type List initargs)
	   (ignore initargs))

  (flet ((make-dispatcher-connection (fd)
	   (declare (ignore fd))
	   (make-instance 'Dispatcher-Connection :dispatcher dispatcher)))
    (multiple-value-bind (port socket) (make-connection-request-socket 0)
      (setf (slot-value dispatcher 'dispatcher-port) port)
      (setf (slot-value dispatcher 'dispatcher-socket) socket)
      (setf (slot-value dispatcher 'tool-identifier) 0)
      (setf (slot-value dispatcher 'input-handler)
	(set-socket-input-handler socket #'make-dispatcher-connection))))

  (listen-for-connection-requests (dispatcher-socket dispatcher))
  
  (when *debug?* (describe dispatcher) (force-output))
  (write-dot-file dispatcher)
  (values))

;;;------------------------------------------------------------

(defvar *dispatcher* nil "The unique dispatcher.")

(defun the-dispatcher () 
  "This function creates and initializes a message Dispatcher,
which immediately starts accepting connection requests."
  (declare (special *dispatcher*)
	   (:returns (type Dispatcher)))
  
  (unless (typep *dispatcher* 'Dispatcher) ;; might be Null or Dead-Object
    (setf *dispatcher* (make-instance 'Dispatcher)))
  *dispatcher*)

;;;------------------------------------------------------------

(defmethod kill ((dispatcher Dispatcher))
  
  "Shutdown the message dispatcher as gracefully as possible.
First stop accepting new connections by removing the input handler
for the <dispatcher-socket>, which is used to listen for connection requests.
Close the <dispatcher-socket>. Disconnect all the existing connections."
  
  (declare (type Dispatcher dispatcher))
  (let ((connections ()))
    (declare (type List connections))
    ;; collect a list of the dispatcher connections used,
    ;; and send out DISPATCHER_KILLED messages at the same time
    (maphash #'(lambda (id con)
		 (declare (type Tool-Identifier id)
			  (type Connection con))
		 (send-message _dispatcher_killed_ dispatcher id)
		 (pushnew con connections))
	     (id-connections dispatcher))
    (clrhash (id-connections dispatcher))
    (dolist (connection connections)
      (declare (type Connection connection))
      (kill connection)))
  (remove-socket-input-handler (dispatcher-socket dispatcher)
			       (input-handler dispatcher))
  (close-socket (dispatcher-socket dispatcher))
  (change-class dispatcher 'Dead-Object))

;;;============================================================
;;; Audience handling
;;;============================================================

(defun display-audiences (dispatcher)
  (print-hashtable t (audiences dispatcher)))

;;;------------------------------------------------------------

(defun mgethash (keys tables)
  "Return a list of all the values associated with any of the
keys in any of the tables. Duplicates in the {\tt keys}
or the {\tt tables} lists are only processed once, duplicate
values are also eliminated before the values list is returned."
  (declare (type List keys tables)
	   (:returns (type List)))
  (let ((rkeys (remove-duplicates keys))
	(rtables (remove-duplicates tables))
	(return-values ()))
    (dolist (key rkeys)
      (dolist (table rtables)
	(pushnew (gethash key table) return-values)))
    (delete nil return-values)))

;;;------------------------------------------------------------

(defun message-audience (dispatcher operation sender address)

  "Return a list of the ids of all connections that should
receive a copy of this message."

  (declare (type Dispatcher dispatcher)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (:returns (type List)))

  (let ((audience 
	 (delete-duplicates 
	  (apply 
	   #'concatenate 'List 
	   (unless (tool-identifier= address _wild_tool_) (list address))
	   (mgethash (list address _wild_tool_) 
		     (mgethash (list sender _wild_tool_) 
			       (mgethash (list operation _wild_op_) 
					 (list (audiences dispatcher)))))))))
    (when *debug?*
      (format t "~&message audience for ~a is: ~a"
	      (list operation sender address)
	      audience)
      (force-output))
    audience))


;;;------------------------------------------------------------

(defun add-to-audience (dispatcher member operation sender address)

  "Add <member> to the audience for messages that match the <operation>,
<sender>, <address> triple. Matching means that <operation>, <sender>, 
and <address> are tool-identifier= to the message operation, sender, 
and address, respectively, unless any of <operation>, <sender>, and 
<address> are wild, which will match any tool-identifier in the 
corresponding field of the message."

  (declare (type Dispatcher dispatcher)
	   (type Operation-Name operation)
	   (type Tool-Identifier member sender address))

  (when *debug?* 
	(print "add-to-audience:")
	(print (list member operation sender address))
	(print-audiences)
	(force-output))
  (let ((senders (gethash operation (audiences dispatcher))))
    (declare (type (or Null Hash-Table) senders))
    (when (null senders)
      (setf senders (make-hash-table :test #'eql))
      (setf (gethash operation (audiences dispatcher)) senders))
    (let ((addresses (gethash sender senders)))
      (declare (type (or Null Hash-Table) addresses))
      (when (null addresses)
	(setf addresses (make-hash-table :test #'eql))
	(setf (gethash sender senders) addresses))
      (let ((audience (gethash address addresses)))
	(declare (type List audience))
	(when *debug?* 
	  (format t "~&adding ~a to audience ~a for (~a ~a ~a)~%"
		  member audience operation sender address))
	(setf (gethash address addresses) (pushnew member audience)))))
  (when *debug?* 
	(print "add-to-audience:")
	(print-audiences)
	(force-output)))
 
;;;------------------------------------------------------------

(defun remove-from-audience (dispatcher member operation sender address)

  "Remove <member> from the audience for messages that match the
<operation>, <sender>, <address> triple. Matching means that
<operation>, <sender>, and <address> are equal tool-identifiers to the
audience sender, operation, and address, even if any of <operation>,
<sender>, or <address> are wild. If <operation>, <sender>, or
<address> are metawild, then the member is removed from all audiences that
match on the other fields."

  (declare (type Dispatcher dispatcher)
	   (type Operation-Name operation)
	   (type Tool-Identifier member sender address))

  (when *debug?* 
    (print "before remove-from-audience:")
    (print (list member operation sender address))
    (print-audiences)
    (force-output))
  
  (labels 
      (;;-----------
       (clean-audience (addr addresses)
	 (declare (type Tool-Identifier addr)
		  (type (or Null Hash-Table) addresses))
	 (let ((audience (gethash addr addresses)))
	   (declare (type List audience))
	   (unless (null audience)
	     (setf (gethash addr addresses) (delete member audience)))))
       ;;-----------
       (clean-addresses (addresses)
	 (declare (type (or Null Hash-Table) addresses))
	 (unless (null addresses)
	   (if (tool-identifier= address _meta_tool_)
	     ;; then recurse
	     (maphash #'(lambda (addr audience)
			  (declare (type Tool-Identifier addr)
				   (type List audience)
				   (ignore audience))
			  (clean-audience addr addresses))

		      addresses)
	     ;; else
	     (clean-audience address addresses))))
       ;;-----------
       (clean-senders (senders)
	 (declare (type (or Null Hash-Table) senders))
	 (unless (null senders)
	   (if (tool-identifier= sender _meta_tool_)
	     ;; then recurse
	     (maphash #'(lambda (key addresses)
			  (declare (ignore key))
			  (clean-addresses addresses))
		      senders)
	     ;; else
	     (clean-addresses (gethash sender senders))))))
    ;;-----------
    (if (operation-name= operation _meta_op_)
      ;; then remove member for all operations
      (maphash #'(lambda (op senders)
		   ;; recurse on every sender that's in the table
		   (declare (type Operation-Name op)
			    (type (or Null Hash-Table) senders)
			    (ignore op))
		   (clean-senders senders))
	       (audiences dispatcher))
      ;; else
      (clean-senders (gethash operation (audiences dispatcher))))
 
    (when *debug?* 
      (print "after remove-from-audience:")
      (print-audiences)
      (force-output))))

;;;------------------------------------------------------------

(defun remove-audiences (dispatcher dead-tool)

  "If a tool dies, then it can't send or receive any more messages.
In that case, we might as well clean up the audience tables by
removing any join-templates in which the dead tool is either the
sender or address."

  (declare (type Dispatcher dispatcher)
	   (type Tool-Identifier dead-tool))

  (when *debug?* 
    (print "before remove-audiences:")
    (print-audiences)
    (force-output))

  ;; iterate over all operations
  (maphash #'(lambda (op senders)
	       (declare (type Operation-Name op)
			(ignore op))
	       ;; clear all join-templates for which <dead-tool> = address.
	       (maphash #'(lambda (sender addresses)
			    (declare (type Tool-Identifier sender)
				     (type Hash-Table addresses)
				     (ignore sender))
			    (remhash dead-tool addresses))
			senders)
	       ;; clear all join-templates for which <dead-tool> = sender.
	       (remhash dead-tool senders)  
	       )
	   (audiences dispatcher))
  
  (when *debug?* 
    (print "after remove-audiences:")
    (print-audiences)
    (force-output)))
 
;;;============================================================
;;; Message dispatching
;;;============================================================

(defun id-connection (id dispatcher)
  "Get the connection used to transmit message to an tool-identifier."
  (declare (type Dispatcher dispatcher)
	   (type Tool-Identifier id))
  (gethash id (id-connections dispatcher) nil))

(defsetf id-connection (id dispatcher) (connection)
  "Set the connection used to transmit message to an tool-identifier."
  `(locally (declare (type Dispatcher ,dispatcher)
		     (type Tool-Identifier ,id)
		     (type Connection ,connection))
     (setf (gethash ,id (id-connections ,dispatcher)) ,connection)))

;;;------------------------------------------------------------

(defun dispatch-message (dispatcher msg)

  (declare (type Dispatcher dispatcher)
	   (type List msg))

  (let ((dispatcher-id (tool-identifier dispatcher))
	(id (message-id msg))
	(operation (message-operation msg))
	(sender (message-sender msg))
	(address (message-address msg)))
    (declare (type Operation-Name operation)
	     (type Tool-Identifier dispatcher-id sender address)
	     (type Message-Identifier id))
    
    (assert (tool-identifier= (message-receiver msg) dispatcher-id))
    (when *debug?*
      (format t "~&dispatch-message: ~a~%"
	      (list dispatcher-id id operation sender address))
      (force-output))
    
    (if (or (tool-identifier= address dispatcher-id)
	    (tool-identifier= address _wild_tool_)
	    (id-connection address dispatcher))

      ;; then it's ok to multiplex the message
      (let ((audience (message-audience dispatcher operation sender address)))
	(declare (type List audience))
	;; send copies to members of the audience
	(dolist (member audience)
	  (declare (type Tool-Identifier member))
	  (if (tool-identifier= dispatcher-id member)
	    ;; then handle a copy here
	    (apply #'handle-message dispatcher 
		   id operation sender address (message-arg-chunks msg))
	    ;; else transmit it over the appropriate connection
	    (let ((connection (id-connection member dispatcher)))
	      (declare (type (or Null Connection) connection))
	      (if connection
		(transmit-message 
		 connection 
		 (cons (tool-identifier->chunk member) (rest msg)))
		;; else the dispatcher doesn't recognize the member id
		;; so it shouldn't be in any audiences.
		(progn
		  (warn "~s is unknown to ~s, remove it from all audiences.~%"
			member dispatcher)
		  (when *debug?* (break))
		  (remove-from-audience 
		   dispatcher member _meta_op_ _meta_tool_ _meta_tool_)))))))

      ;; else the address is unknown, so send an error message back
      (progn
	(warn "~&Address is unknown!")
	(when *debug?* (break))
	(if (id-connection sender dispatcher)
	  ;; then return an error message 
	  (apply #'send-message _unknown_address_ dispatcher sender (rest msg))
	  ;; else sender is unknown too! Just forget it.
	  (progn (warn "~&Sender is also unknown!")
		 (when *debug?* (break))))))))


;;;============================================================
;;; Tool protocol:
;;;============================================================

(defmethod send-message (operation (sender Dispatcher) address &rest args)
  (declare (type Operation-Name operation)
	   (type Dispatcher sender)
	   (type Tool-Identifier address)
	   (type List args))
  (let ((id (make-message-id sender))
	(dispatcher-id-chunk 
	 (tool-identifier->chunk (tool-identifier sender))))
    (declare (type Message-Identifier id))
    (when *debug?*
      (format t "(send-message ~a ~a ~a ~a . ~a)" 
	      id operation sender address args))
    (dispatch-message sender
		      (list* dispatcher-id-chunk
			     (message-identifier->chunk id)
			     (operation-name->chunk operation)
			     dispatcher-id-chunk
			     (tool-identifier->chunk address)
			     (mapcar #'lisp->chunk args)))
    id))
 
;;;------------------------------------------------------------
;;; Registering for announcements
;;;------------------------------------------------------------

(defmethod join-audience ((dispatcher Dispatcher) operation sender address)
  "Add the dispatcher to the audience for the given join template."
  (declare (type Dispatcher dispatcher)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (:returns (type Message-Identifier message-id)))
  (add-to-audience dispatcher 
		   (tool-identifier dispatcher) operation sender address))

(defmethod leave-audience ((dispatcher Dispatcher) operation sender address)
  "Remove the dispatcher from the audience for the given leave template."
  (declare (type Dispatcher dispatcher)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (:returns (type Message-Identifier message-id)))
  (remove-from-audience dispatcher 
			(tool-identifier dispatcher) operation sender address))

;;;------------------------------------------------------------
;;; Message Handler functions
;;;------------------------------------------------------------

(defun JOIN_AUDIENCE (dispatcher id operation sender address
		      chunk-operation chunk-sender chunk-address)
  
  "This is called to handle a JOIN_AUDIENCE message.

id JOIN_AUDIENCE sender dispatcher 
template-operation template-sender template-address

JOIN_AUDIENCE asks the <dispatcher> to send <sender> a copy of any
message that matches the join template. The join template has the
form: (operation sender address), where <operation>, <sender> and
<address> are either Tool-Identifiers or *. A message matches a template
if the three fields match; the fields match if they are equal tool-identifiers 
or if the template's value is *."

  (declare (type Dispatcher dispatcher)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (type Chunk chunk-sender chunk-operation chunk-address))

  (let ((template-operation (chunk->operation-name chunk-operation))
	(template-sender (chunk->tool-identifier chunk-sender))
	(template-address (chunk->tool-identifier chunk-address)))
    (declare (type Operation-Name template-operation)
	     (type Tool-Identifier template-sender template-address))
    (when *debug?*
      (print (list dispatcher id operation sender address
		   template-operation template-sender template-address))
      (force-output))
    (assert (operation-name= operation _join_audience_))
    (add-to-audience dispatcher sender 
		     template-operation template-sender template-address)))

;;;------------------------------------------------------------

(defun LEAVE_AUDIENCE (dispatcher id operation sender address
		       chunk-operation chunk-sender chunk-address)
  
  "This is called to handle a LEAVE_AUDIENCE message:

id LEAVE_AUDIENCE sender dispatcher
template-operation template-sender template-address

LEAVE_AUDIENCE tells the <dispatcher> that <sender> should no longer
receive copies of messages matching one or more join templates.  
The semantics of the <dispatcher>'s handling of LEAVE_AUDIENCE can be modeled
as follows: Suppose that, for each <sender> of JOIN_AUDIENCE messages,
the <dispatcher> keeps a list of the join templates received. When a
LEAVE_AUDIENCE message is received, the <dispatcher> deletes all those 
join templates belonging to <sender> that match the <leave-template>.  
A join template matches the leave template if the three fields match.
A field of the leave template matches a field of a join template
if they are eq or if the field of the leave template is ** (not the
single *!).

The reason for the use of ** in leave templates is to allow a client
to differentiate between deleting specific join templates that contain
*'s and deleting sets of join templates specified with a wild cards."
  
  (declare (type Dispatcher dispatcher)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (type Chunk chunk-operation chunk-sender chunk-address))

  (let ((template-operation (chunk->operation-name chunk-operation))
	(template-sender (chunk->tool-identifier chunk-sender))
	(template-address (chunk->tool-identifier chunk-address)))
    (declare (type Operation-Name template-operation)
	     (type Tool-Identifier template-sender template-address))
    (when *debug?*
      (print (list dispatcher id operation sender address
		   template-operation template-sender template-address))
      (force-output))
    (assert (operation-name= operation _leave_audience_))
    (remove-from-audience 
     dispatcher sender 
     template-operation template-sender template-address)))

;;;------------------------------------------------------------

(defun NEW_TOOL_IDENTIFIER_REQUEST (dispatcher id operation sender address)
  "Handle a NEW_TOOL_IDENTIFIER_REQUEST by sending back a 
NEW_TOOL_IDENTIFIER message."
  (declare (type Dispatcher dispatcher)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address))
  (when *debug?*
    (print (list dispatcher id operation sender address))
    (force-output))
  (let ((id (generate-tool-identifier)))
    (declare (type Tool-Identifier id))
    (setf (id-connection id dispatcher) (id-connection sender dispatcher))
    (add-to-audience dispatcher (tool-identifier dispatcher) 
		     _tool_killed_ id _wild_tool_)
    (send-message _new_tool_identifier_ 
		  dispatcher sender (tool-identifier->chunk id))))

;;;------------------------------------------------------------

(defmethod TOOL_KILLED ((dispatcher Dispatcher) id operation sender address)
  
  "Handle a TOOL_KILLED message by removing the sender id 
from the <id-connections>. If no more tool-identifiers
use the connection, kill it also."
  
  (declare (type Dispatcher dispatcher)	   
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address))
  
  (assert (operation-name= _tool_killed_ operation))
  (when *debug?*
    (format t "~&Dispatcher TOOL_KILLED handler:~%" )    
    (print (list dispatcher id operation sender address))
    (force-output))
  
  (remove-from-audience dispatcher sender _meta_op_ _meta_tool_ _meta_tool_)
  (remove-audiences dispatcher sender)
  (let* ((ids (id-connections dispatcher))
	 (connection (id-connection sender dispatcher))
	 (kill? t))
    (declare (type Hash-Table ids)
	     (type Connection connection))
    (remhash sender ids)
    ;; any more entries that use <connection>?
    (maphash #'(lambda (id con)
		 (declare (type Tool-Identifier id)
			  (type Connection con)
			  (ignore id))
		 (when (eq con connection) (setf kill? nil)))
	     ids)
    (when kill? (kill connection))))

;;;------------------------------------------------------------

(defun print-audiences (&optional (dispatcher (the-dispatcher)))
  (print-hashtable dispatcher (audiences dispatcher)))