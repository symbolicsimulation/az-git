;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(defparameter *debug?* t
  "Should debugging information be printed?")

;;;============================================================

(deftype Funcallable () 
  "A type for funcall-able or apply-able objects."
  '(or Symbol Function))

(deftype Boolean () 
  "A type for predicate values."
  '(Member t nil))

;;;============================================================
;;; printing random things, borrowed from PCL's *-low.lisp files

(defun printing-random-thing-internal (thing stream)
  #+:cmu
  (format stream "{~X}" (sys:%primitive c:make-fixnum thing))
  #+:coral
  (prin1 (ccl::%ptr-to-int thing) stream)
  #+(and dec vax common)
  (format stream "~X" (system::%sp-pointer->fixnum thing))
  #+:excl
  (format stream "~X" (excl::pointer-to-fixnum thing))
  #+:gclisp
  (multiple-value-bind (offaddr baseaddr) (sys:%pointer thing)
    (princ baseaddr stream)
    (princ ", " stream)
    (princ offaddr stream))
  #+Genera
  (format stream "~X" (si:%pointer thing))
  #+HP-HPLabs 
  (format stream "~X" (prim:@inf thing))          
  #+IBCL
  (format stream "~X" (si:address thing))                
  #+KCL
  (format stream "~X" (si:address thing))                 
  #+Lucid
  (format stream "~X" (%pointer thing))
  #+TI
  (format stream "~X" (si:%pointer thing))
  #+Xerox
  (let ((*print-base* 8))
    (princ (il:\\hiloc thing) stream)
    (princ "," stream)
    (princ (il:\\loloc thing) stream))
  #-(or :cmu :coral (and dec vax common) :excl Genera HP-HPLabs IBCL KCL
	Lucid TI Xerox)
  (prin1 thing stream)
  )

  ;;   
;;;;;; printing-random-thing
  ;;

(defmacro printing-random-thing ((thing s) &body body)
  "Similar to printing-random-object in the lisp machine but much simpler
and machine independent. (borrowed from PCL)."
  (let ((stream (gensym)))
    `(let ((,stream ,s))
       (format ,stream "#<")
       ,@body
       (format ,stream " ")
       (printing-random-thing-internal ,thing ,stream)
       (format ,stream ">"))))

;;;------------------------------------------------------------

(defun print-hashtable (key value)
  (print (list key value))
  (when (typep value 'Hash-Table)
    (maphash #'print-hashtable value)))


;;;------------------------------------------------------------

(defmacro while (condition &body body)
  `(loop (unless ,condition (return)) ,@body))

(defmacro until (condition &body body)
  `(loop (when ,condition (return)) ,@body))