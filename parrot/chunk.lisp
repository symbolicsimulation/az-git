;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(defconstant -chunk-count-nbytes- 4 
  "How many bytes are used to specify the number of chunks in the
message?  We use 8 bytes = 64 bits to allow for future expansion,
though 32 bits = 4 bytes would be plenty for now.")

(defconstant -chunk-length-nbytes- 4
  "How many bytes are used to specify the length of the chunk?  We use
8 bytes = 64 bits to allow for future expansion, though 32 bits = 4
bytes would be plenty for now.")

;;;============================================================

(deftype Chunk (&optional (length '*)) 

  "The Lisp representation for a message chunk is just
(Simple-Array (Unsigned-Byte 8) (*)). When a chunk is transmitted,
the length of the chunk is sent before the chunk's data bytes."

  `(Simple-Array (Unsigned-Byte 8) (,length)))

;;;=============================================================

(declaim (inline zero-chunk))
(defun zero-chunk (chunk)
  (declare (optimize (safety 0) (speed 3))
	   (type Chunk chunk))
  (dotimes (i (length chunk))
    (declare (type (Unsigned-Byte 16) i))
    (setf (aref chunk i) 0))
  chunk)

(declaim (inline unit-chunk))
(defun unit-chunk (chunk)
  (declare (optimize (safety 0) (speed 3))
	   (type Chunk chunk))
  (dotimes (i (length chunk))
    (declare (type (Unsigned-Byte 16) i))
    (setf (aref chunk i) 1))
  chunk)

(defun fill-chunk (chunk value)
  (declare (optimize (safety 0) (speed 3))
	   (type Chunk chunk)
	   (type (Unsigned-Byte 8) value))
  (case value
    (0 (zero-chunk chunk))
    (1 (unit-chunk chunk))
    (otherwise
     (dotimes (i (length chunk))
       (declare (type (Unsigned-Byte 16) i))
       (setf (aref chunk i) value))))
  chunk)

(defun make-chunk (len &key (initial-element 0))
  (declare (type (Unsigned-Byte 16) len)
	   (type (Unsigned-Byte 8) initial-element)
	   (:returns (type Chunk)))
  (make-array (list len) 
	      :element-type '(Unsigned-Byte 8)
	      :initial-element initial-element))

(defun copy-chunk (v0 v1)
  (declare (optimize (safety 0) (speed 3))
	   (type Chunk v0 v1))
  (dotimes (i (length v0))
    (declare (type (Unsigned-Byte 16) i))
    (setf (aref v1 i) (aref v0 i))))

;;;------------------------------------------------------------
;;; this assumes that code-char and char-code use ascii.

#+(or :cmu :excl)
(defun ascii-char (ascii) (code-char ascii))
#+(or :cmu :excl)
(defun char-ascii (char) (char-code char))

(defun chunk->string (chunk &key (string (make-string (length chunk))))
  
  "Convert a Chunk to a String. 
If <string> is supplied and longer than <chunk>, 
the extra elements are untouched; if shorter, an error will occur."
  
  (declare (type Chunk chunk)
	   (type String string)
	   (:returns (type String string)))
  
  (dotimes (i (length chunk))
    (setf (aref string i) (ascii-char (aref chunk i))))
  string)

(defun string->chunk (string &key (chunk (make-chunk (length string))))
  
  "Convert a String to a Chunk.
If <chunk> is supplied and longer than <string>, 
the extra elements are untouched; if shorter, an error will occur."

  (declare (type String string)
	   (type Chunk chunk)
	   (:returns (type Chunk chunk)))
  
  (dotimes (i (length string)) 
    (setf (aref chunk i) (char-ascii (aref string i))))
  chunk)

;;;------------------------------------------------------------

(defun chunk->lisp (chunk)
  "Convert a Chunk to a lisp object by calling read-from-string (once)."
  
  (declare (type Chunk chunk)
	   (:returns (type T lisp-object)))
  
  (let ((*package* (find-package :parrot)))
    (declare (special *package*))
    (read-from-string (chunk->string chunk))))


(defun lisp->chunk (lisp-object) 
  "Convert an arbitrary lisp object into a Chunk by applying
string->chunk to the result of prin1-to-string. Try to print readably.
However, if the object is a chunk already, leave it alone."

  (declare (type T lisp-object)
	   (:returns (type Chunk)))

  (if (typep lisp-object 'Chunk)
    ;; no need to do anything
    lisp-object
    ;; else print it 
    (let ((*package* (find-package :parrot))
	  (*print-circle* t)
	  (*print-length* nil)
	  (*print-level* nil)
	  #||(*print-readably* t)||#)
      (declare (special *package*
			*print-circle* 
			*print-length* 
			*print-level* 
			*print-readably*))
      (string->chunk (prin1-to-string lisp-object)))))

