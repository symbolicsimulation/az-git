;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(deftype Operation-Name ()
  "An Operation-Name is used to name the operation
the message is intended to invoke in the address.

Operation-Names are currently represented by Strings.
The use of Symbols is an alternative whose performance is under investigation."
  'String)

(defun operation-name= (id0 id1)
  (declare (type Operation-Name id0 id1)
           (:returns (type (Member t nil))))
  (string= id0 id1))

(defun operation-name->string (op)
  "Return a string corresponding to <op>."
  (declare (type Operation-Name op)
	   (:returns (type String)))
  op)

(defun string->operation-name (string)
  "Return an operation name corresponding to <string>."
  (declare (type String string)
	   (:returns (type Operation-Name)))
  ;; operation names are represented by strings, so this is trivial
  string)

(defun operation-name->symbol (op)
  "Return a symbol corresponding to <op>."
  (declare (type Operation-Name op)
	   (:returns (type Symbol)))
  (let ((*package* (find-package :Parrot)))
    (declare (special *package*))
    (read-from-string op)))

(defun symbol->operation-name (symbol)
  "Return an operation name corresponding to <symbol>."
  (declare (type Symbol symbol)
	   (:returns (type Operation-Name)))
  (let ((*package* (find-package :Parrot)))
    (declare (special *package*))
    (prin1-to-string symbol)))

(defun operation-name->chunk (op)
  (declare (type Operation-Name op)
	   (:returns (type Chunk)))
  (string->chunk op))

(defun chunk->operation-name (chunk)
  (declare (type Chunk chunk)
	   (:returns (type Operation-Name)))
  (chunk->string chunk))

