;;; -*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;; 
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty --- about the software, its performance, or its
;;; conformity to any specification --- is given or implied.
;;;
;;;============================================================

(in-package :Parrot)

;;;============================================================

(defun make-tool (&key (class 'Tool))
  
  "Make a tool and start it up, if desired."
  
  (declare (type Symbol class)
	   (:returns (type Tool)))

  (let ((tool (make-instance class)))
    (declare (type Tool tool))
    (setf (slot-value tool 'connection) (default-connection))
    (assign-tool-identifier (connection tool) tool)
    tool))

;;;============================================================
;;; Tool protocol:
;;;============================================================

(defun make-message-id (sender)

  "Generate an Message-Identifier which has not been used 
as a message id by <sender> before."

  (declare (type (or Tool-Identifier Tool) sender)
	   (ignore sender)
	   (:returns (type Message-Identifier)))

  (generate-message-identifier))

;;;-----------------------------------------------------------

(defgeneric send-message (operation sender address &rest args)
  (declare (type Operation-Name operation) 	
	   (type Tool sender)
	   (type Tool-Identifier address)
	   (type List args) 	 
	   (:returns (type Message-Identifier message-id)))
  (:documentation "<send-message> is used to send a <message> to an
Tool.  <args> is a list of arbitrary lisp objects used as message
arguments."))


(defmethod send-message (operation (sender Tool) address &rest args) 

  "The default method for <send-message> uses the <sender>'s
connection to transmit the message to the dispatcher.  The header
items are translated to Chunks using <tool-identifier->chunk>.  The
arguments are translated using <lisp->chunk>. If any of the arguments
are Identifiers, they should be passed to <send-message> already
translated to Chunks, because <lisp->chunk> will not do the right
thing when given an Identifier."

  (declare (type Operation-Name operation) 	 
	   (type Tool sender)
	   (type Tool-Identifier address)
	   (type List args)) 

  (let ((id (make-message-id sender)) 	
	(connection (connection sender))) 
    (declare (type Message-Identifier id) 	
	     (type Connection #||Tool-Connection||# connection))
    (transmit-message connection 
		      (list* 
		       (tool-identifier->chunk (dispatcher-id connection))
		       (message-identifier->chunk id)
		       (operation-name->chunk operation)
		       (tool-identifier->chunk (tool-identifier sender))
		       (tool-identifier->chunk address)
		       (mapcar #'lisp->chunk args)))
    id))

;;;-----------------------------------------------------------
;;; Message handling
;;;-----------------------------------------------------------

(defgeneric handler-function (tool operation)
  (:documentation
   "Returns a function which is called to handle <operation> messages.
If no handler function is defined for tool and operation, returns nil.")
  (declare (type Tool tool)
	   (type Operation-Name operation)
	   (:returns (type (or Function Null)))))

(defmethod handler-function ((tool Tool) operation)
  "Returns a function which is called to handle <operation> messages.
If no handler function is defined for tool and operation, returns nil.
The default method just returns the symbol-function of <operation>,
which is normally a generic function."
  (declare (type Tool tool)
	   (type Operation-Name operation)
	   (:returns (type (or Function Null))))
  (let ((opsym (operation-name->symbol operation)))
    (when (fboundp opsym) (symbol-function opsym))))

;;;-----------------------------------------------------------

(defgeneric handle-message (tool id operation sender address &rest args)
  (declare (type Tool tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (type List args))
  (:documentation "Handle the message."))

(defmethod handle-message ((tool Tool) id operation sender address &rest args)
  "Handle the message"
  (declare (type Tool tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (type List args))
  (when *debug?*
    (format t "~&handle-message: ~a ~a ~a ~a ~{ ~a~}"
	    tool operation sender address
	    (mapcar #'chunk->string args))
    (force-output))
  (let ((handler-function (handler-function tool operation)))
    (if handler-function 
      ;; then
      (apply handler-function tool id operation sender address args)
      ;; else
      (warn "~a has no handler function for ~a" tool operation))))

(defmethod handle-message ((tool Dead-Object)
			   id operation sender address &rest args)
  "A dead tool shouldn't be asked to handle messages."
  (declare (type Dead-Object tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (type List args))
  (warn "~a was asked to handle the message: ~a"
	tool (list* operation sender address id args)))

;;;----------------------------------------------------------- 
;;; Killing
;;;-----------------------------------------------------------

(defmethod kill ((tool Tool))

  "This method sends the messages necessary to notify the
Dispatcher that a Tool is being shutdown. The tool needs to leave all
audiences so that the dispatcher doesn't try to send it any more
message copies."

  (declare (type Tool tool)
	   (:returns (type Message-Identifier message-id)))
    
  (leave-audience tool _meta_op_ _meta_tool_ _meta_tool_)
  (send-message _tool_killed_ tool _wild_tool_)
  (change-class tool 'Dead-Object))

;;;============================================================
;;; Registering for announcements
;;;============================================================

(defgeneric join-audience (tool operation sender address)
  (:documentation "Add <tool> to the audience for the given join template.")
  (declare (type Tool tool)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (:returns (type Message-Identifier message-id))))

(defmethod join-audience ((tool Tool) operation sender address)
  "Send a message to the dispatcher asking to add <tool> to
the audience for the given join template."
  (declare (type Tool tool)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (:returns (type Message-Identifier)))
  (send-message _join_audience_ tool (dispatcher-id (connection tool))
		(operation-name->chunk operation)
		(tool-identifier->chunk sender)
		(tool-identifier->chunk address)))

(defgeneric leave-audience (tool operation sender address)
  (:documentation 
   "Remove <tool> from the audience for the given leave template.")
  (declare (type Tool tool)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (:returns (type Message-Identifier))))

(defmethod leave-audience ((tool Tool) operation sender address)
  "Send a message to the dispatcher asking to remove <tool> from
the audience for the given leave template."
  (declare (type Tool tool)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (:returns (type Message-Identifier)))
  (send-message _leave_audience_ tool (dispatcher-id (connection tool))
		(operation-name->chunk operation)
		(tool-identifier->chunk sender)
		(tool-identifier->chunk address)))

;;;-----------------------------------------------------------
;;; Message Handler Functions
;;;-----------------------------------------------------------

(defgeneric DISPATCHER_KILLED (tool id operation sender address)
  (:documentation
   "If possible, a DISPATCHER_KILLED message will be sent to every
tool when the dispatcher is killed or discovered to be dead, to permit
tools to react in some reasonable fashion.")
  (declare (type Tool tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)))

(defmethod DISPATCHER_KILLED ((tool Tool) id operation sender address)
  
  "If possible, a DISPATCHER_KILLED message will be sent to every
tool when the dispatcher is killed or discovered to be dead, to permit
tools to react in some reasonable fashion. The default method is simply f
or the tool to commit suicide."
  
  (declare (type Tool tool)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (ignore id))

  ;; some consistency tests
  (assert (operation-name= operation _dispatcher_killed_))
  (assert (or (tool-identifier= sender (dispatcher-id (connection tool)))
	      (tool-identifier= sender (tool-identifier (connection tool)))))
  (assert (or (tool-identifier= address (tool-identifier tool))
	      (tool-identifier= address _wild_tool_)))

  ;; actually calling <kill> would try to send messages
  (change-class tool 'Dead-Object))

