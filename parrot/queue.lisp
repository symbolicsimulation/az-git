;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;; 
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty --- about the software, its performance, or its
;;; conformity to any specification --- is given or implied.
;;;
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(defun print-queue (q stream depth)
  "The printer function for the Queue abstract type."
  (declare (ignore depth))
  (printing-random-thing
   (q stream)
   (format stream "Q: ~s " (queue-head q))))

(defstruct (Queue
	    (:constructor %make-queue)
	    (:conc-name nil)
	    (:print-function print-queue))
  "A data structure for queues."
  (queue-head () :type List)
  (queue-tail () :type List))

;;;-----------------------------------------------------------
;;; don't allow initialization of slots

(defun make-queue ()
  "The constructor function for the Queue abstract type."
  (%make-queue))

;;;-----------------------------------------------------------

(defun %queue-length (q)
  (declare (optimize (safety 0) (speed 3))
	   (type Queue q))
  (length (queue-head q)))

(defun queue-length (q)
  "How many items are in the queue?"
  (declare (type Queue q)
	   (:returns (type (Integer 0 *))))
  (%queue-length q))

;;;-----------------------------------------------------------

(defun %queue-empty? (q)
  (declare (optimize (safety 0) (speed 3))
	   (type Queue q))
  (null (queue-head q)))

(defun queue-empty? (q)
  "Is the queue empty?"
  (declare (type Queue q)
	   (:returns (type Boolean)))
  (%queue-empty? q))

(defun %non-queue-empty? (q)
  (declare (optimize (safety 0) (speed 3))
	   (type Queue q))
  (queue-head q))

(defun non-queue-empty? (q)
  "Is there something in the queue?"
  (declare (type Queue q)
	   (:returns (type Boolean)))
  (%non-queue-empty? q))

;;;-----------------------------------------------------------

(defun %queue-clear (q)
  (declare (optimize (safety 0) (speed 3))
	   (type Queue q))
  (atomic
   (setf (queue-head q) nil)
   (setf (queue-tail q) nil))
  q)

(defun queue-clear (q)
  "Discard everything in the queue."
  (declare (type Queue q)
	   (:returns (type Queue q)))
  (%queue-clear q))

;;;-----------------------------------------------------------

(defun %enqueue (item q)
  (declare (optimize (safety 0) (speed 3))
	   (type T item)
	   (type Queue q))
  (atomic
   (cond ((%queue-empty? q)
	  ;; then we need to initialize it
	  (push item (queue-head q))
	  (setf (queue-tail q) (queue-head q)))
	 (t ;; else just put the item on the end
	  (setf (rest (queue-tail q)) (list item))
	  (setf (queue-tail q) (rest (queue-tail q))))))
  q)

(defun enqueue (item q)
  "Add <item> to the end of <q>."
  (declare (type Queue q)
	   (:returns q))
  (%enqueue item q))

;;;-----------------------------------------------------------
;;; This won't distinguish between an empty queue and one with
;;; nil's on it.

(defun %dequeue (q)
  (declare (optimize (safety 0) (speed 3))
	   (type Queue q))
  (atomic (pop (queue-head q))))

(defun dequeue (q)

  "Return the first item in <q>, removing it from <q>. <dequeue> is to
Queues as <pop> is to lists. If <q> is empty, <dequeue> returns <nil>,
so it is not possible to use <dequeue> to tell an empty queue from one
containing <nil> entries. To test if a queue is empty, call
<queue-empty?>. <dequeue>-ing is atomic and cannot be interrrupted."

  (declare (type Queue q))
  (%dequeue q))

;;;-----------------------------------------------------------

(defun %queue-first (q)
  (declare (optimize (safety 0) (speed 3))
	   (type Queue q))
  (atomic (first (queue-head q))))

(defun queue-first (q) 

  "Return the first item in <q>, without removing it from <q>.
<queue-first> is to Queues as <first> is to lists. If <q> is empty,
<queue-first> returns <nil>, so it is not possible to use <dequeue> to
tell an empty queue from one containing <nil> entries. To test if a
queue is empty, call <queue-empty?>.  <queue-first> is atomic and
cannot be interrrupted."

  (declare (type Queue q))
  (%queue-first q))

;;;-----------------------------------------------------------
;;; Queues can also be used like stacks:
;;; pop is the same as dequeue

(defun %queue-push (item q)
  (declare (optimize (safety 0) (speed 3))
	   (type T item)
	   (type Queue q))
  (atomic
   (if (%queue-empty? q)
       ;; then we need to initialize  the tail as well
       (progn
	 (push item (queue-head q))
	 (setf (queue-tail q) (queue-head q)))
     ;; else just push the item
     (push item (queue-head q)))
   q))

(defun queue-push (item q)

    "<queue-push> pushes <item> on the front of the <q>, in contrast
to <enqueue> which puts <item> at the back of the queue.  <queue-push>
is to Queues as <push> is to lists.  <queue-push> is atomic and cannot
be interrupted."

  (declare (type Queue q))
  (%queue-push item q))

;;;-----------------------------------------------------------
;;; like find

(defun %queue-find (item q &key (test nil) (key nil))
  (declare (optimize (safety 0) (speed 3))
	   (type T item)
	   (type Queue q))
  (if test
      (if key
	  (atomic (find item (queue-head q) :test test :key key))
	(atomic (find item (queue-head q) :test test)))
    (if key
	(atomic (find item (queue-head q) :key key))
      (atomic (find item (queue-head q))))))

(defun queue-find (item q &key (test nil) (key nil))

  "<queue-find> is to Queues as <find> is to lists.  <queue-find> is
atomic and cannot be interrupted."

  (declare (type Queue q)
	   (type Funcallable test key))
  (%queue-find item q :test test :key key))

;;;-----------------------------------------------------------
;;; like find-if

(defun %queue-find-if (pred q &key (key nil))
  (declare (optimize (safety 0) (speed 3))
	   (type Funcallable pred key)
	   (type Queue q))
  (if key
      (atomic (find-if pred (queue-head q) :key key))
    (atomic (find-if pred (queue-head q)))))

(defun queue-find-if (pred q &key (key nil))

   "<queue-find-if> is to Queues as <find-if> is to lists.
<queue-find-if> is atomic and cannot be interrupted."

   (declare (type Funcallable pred key)
	   (type Queue q))
  (%queue-find-if pred q :key key))

;;;-----------------------------------------------------------
;;; removing an item from a queue, wherever it is:

(defun %queue-delete (item q)
  (declare (optimize (safety 0) (speed 3))
	   (type T item)
	   (type Queue q))
  (atomic
   (setf (queue-head q) (delete item (queue-head q)))
   (setf (queue-tail q) (last (queue-head q)))
   q))

(defun queue-delete (item q)

     "<queue-delete> is to Queues as <delete> is to lists.  That is,
it destructuvely modifies the queue to remove all occurences of
<item>, where ever they may be in the queue.  <queue-delete> is atomic
and cannot be interrupted."

  (declare (type Queue q))
  (%queue-delete item q))

;;;-----------------------------------------------------------
;;; move someone to the front  of the queue

(defun %move-to-queue-head (item q)
  (declare (optimize (safety 0) (speed 3))
	   (type T item)
	   (type Queue q))
  (atomic
   (queue-delete item q)
   (queue-push item q)))

(defun move-to-queue-head (item q)

  "<move-to-queue-head> is an odd, but useful operation.  It moves
<item> from whereever it is in <q> to be the first item in <q> (the
next to be dequeued).  <move-to-queue-head> is atomic and cannot be
interrupted."

  (declare (type Queue q))
  (%move-to-queue-head item q))

;;;-----------------------------------------------------------
#|| tests:

(setf q (make-queue))
(describe q)
(queue-tail q)
(enqueue :a q)
(dequeue q)
(enqueue :b q)
(queue-push :c q)
(queue-delete :c q)
(move-to-queue-head :b q)

||#