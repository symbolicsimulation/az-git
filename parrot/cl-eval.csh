#!/bin/csh

# What to do with debugging messages

# set PARROT_LOG = "/dev/null" # to disable debugging messages
  set PARROT_LOG = `tty` # to see debugging messages on the terminal
# set PARROT_LOG = "parrot.log" # to save debugging messages in a file


# the source directory for the Parrot CL code.

  set PARROT_DIR = "/belgica-4g/jam/az/parrot"


# the file with lisp code initializing the eval-server:

  set PARROT_EVAL_SERVER = $PARROT_DIR/init-eval-server.lisp


# the CL binary to use

  set CL = "/usr/belgica/bin/cmucl17"
# set CL = "/usr/belgica/bin/cmucl"
# set CL = "/usr/belgica/bin/facl"


# a flag corresponding to the current lisp variety

  set LISP_TYPE = "cmucl"   # CMUCL version 16 on Belgica
# set LISP_TYPE = "facl"    # Franz Allegro CL version 


switch ($LISP_TYPE)
	
	case "cmucl" : # Carnegie Mellon CL version
		exec $CL -load $PARROT_EVAL_SERVER \
			>& $PARROT_LOG
		breaksw

	case "facl" : # Franz Allegro CL version
		exec $CL -batch < $PARROT_EVAL_SERVER \
			>& $PARROT_LOG
		breaksw
	
	endsw

