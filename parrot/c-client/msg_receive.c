
#include <ctype.h>

#include "msg.h"

static void read_header_chunk(msgIO *io);
static void getLpar(msgIO *io);
static void getRpar(msgIO *io);
static char * getField(msgIO *io);
static char * getData(msgIO *io, long nbytes);
static int MgetcError();
static client *cl_current; /* low-level io uses this for SERVER_ERROR */

/* definitions used in reading the header: they rely on the buffer having
/* been made big enough -- see read_header_chunk() */
#define Mgetc(io) ((++io->pos < io->nbytes) ?io->buf[io->pos]: MgetcError() )
#define Mungetc(io) (io->pos--)

/* parse the message: its format is "(type sender receiver msgid)"
/* where type, msgid are server symbols, which  may be quoted according to
/* the message server's convention */

void
read_msg_header(client *cl, message *msg)
{
	msgIO *io = cl->io;
	cl_current = cl;
	read_header_chunk(io);
	getLpar(io);
	msg->type = getField(io);
	msg->sender = getField(io);
	msg->receiver = getField(io);
	msg->messageId = getField(io);
	getRpar(io);
	msg->nbytes = read_nbytes(io);
}

void
read_msg_body(client *cl, message *msg, int keep)
{
	int fd; long nbytes;
	fd = MSG_FILENO(cl->io); nbytes = msg->nbytes;
	cl_current = cl;
	if(keep) {
		if(!msg->body) msg->body = New(nbytes+1, char);
		if(nbytes>0)
			Mread(fd, msg->body, nbytes);
		if(verbose_client) {
			char stuff[43]; int n;
			n = nbytes > 42 ? 42 : nbytes;
			strncpy(stuff, msg->body, n);
			stuff[n] = '\0';
			if(n< nbytes) fprintf(stderr, "read_msg_body: body = \"%s ...\"\n",
				stuff);
			else fprintf(stderr, "read_msg_body: body = \"%s\"\n", stuff);
		}
	}
	else {
		long n,buflen; msgIO *io; char *buf;
		io = cl->io; buflen = io->buf_length;; buf = io->buf;
		while(1) {
			n = nbytes>buflen ? buflen : nbytes;
			if(n<1)break;
			Mread(fd, buf, n);
			nbytes -= n;
		}
		if(verbose_client) fprintf(stderr, "read_msg_body: skipping %d bytes\n", nbytes);
		/* how should a skipped message be shown ? */
		/* At present, the body pointer is set to 0, but */
		/* nbytes is >0 */
	}
}

void
copy_msg_body(client *cl, message *msg, char *filename)
{
	FILE *file;
	long buflen, nbytes; int fdin, fdout; msgIO *io; char *buf;
	if((file = fopen(filename, "w"))==NULL) {
		char filebuf[120];
		PROBLEM "Couldn't open file \"%s\", will use /tmp/E%s",
			filename, MSG_ID(msg)
		PRINT_IT;
		sprintf(filebuf, "/tmp/E%s", MSG_ID(msg));
		if((file = fopen(filebuf, "w"))==NULL)
			PROBLEM "Couldn't create /tmp/E%", MSG_ID(msg)
			SERVER_ERROR(cl);
	}
	fdout = fileno(file);
	io = cl->io; buflen = io->buf_length;; buf = io->buf; fdin = cl->io->fd;
	cl_current = cl;
	nbytes = msg->nbytes;
	if(verbose_client) fprintf(stderr, "copy_msg_body: copying %d bytes to \"%s\"\n",
		nbytes, filename);
	while(1) {
		long i, n; 
		n = nbytes>buflen ? buflen : nbytes;
		if(n<1)break;
		Mread(fdin, buf, n);
		Mwrite(fdout, buf, n);
		nbytes -= n;
	}
}

/* read the standard message chunk length, 8 bytes, assuming that this fills  */
/* two unsigned long numbers.  See the comments on write_nbytes */
/* The first 4 bytes are required to be zero, since chunks can't be > 2^32 */
/* The 2 numbers are converted from network to host byte order if needed, but */
/* this is questionable if the remote  host had other length unsigned long*/
/* Note that htonl, ntohl are for UNISGNED longs */
int
read_nbytes(msgIO *io)
{
	unsigned long numbuf[2];
	int n, fd, c; unsigned value;
	int i;
	fd = io->fd;
	Mread(fd, (char *)numbuf, 8);
	if(ntohl(numbuf[0])) {
		char *ptr = (char *)numbuf;
		PROBLEM "Bad message chunk length: should have had 0 bytes in first word: %c%c%c%c",
		ptr[0],ptr[1],ptr[2],ptr[3] SERVER_ERROR(cl_current);
	}
	value = (int)ntohl(numbuf[1]);
	if(verbose_client) fprintf(stderr, "read_nbytes: %d\n", value);
	return(value);
}

/* read, but don't parse, the message header */
static void
read_header_chunk(msgIO *io)
{
	int nbytes;
	nbytes = read_nbytes(io);
	if(io->buf_length < nbytes)
		io->buf = Realloc(io->buf, nbytes, char);
	Mread(io->fd, io->buf, nbytes);
	if(verbose_client) {
		io->buf[nbytes] = '\0';
		fprintf(stderr, "read_header_chunk: \"%s\"\n", io->buf);
	}
	/* position before the first character */
	io->pos = -1; io->nbytes = nbytes;
}

static void
getLpar(msgIO *io)
{
	int c;
	while(isspace(c=Mgetc(io))) {}
	if(c!=LPAR) PROBLEM "Expected a left paren, got \"%c\"", c
		SERVER_ERROR(cl_current);
}

static void
getRpar(msgIO *io)
{
	int c;
	while(isspace(c=Mgetc(io))) {}
	if(c!=RPAR) PROBLEM "Expected a right paren, got \"%c\"", c
		SERVER_ERROR(cl_current);
}

/* this is the classic makestring() code (cf lex.l, rd_string in S) */
/* It differs a little  in REQUIRING the quote character, and in the */
/* treatment of escapes, to conform to the message system's defintion */
/* of a legal string */
static char *
getField(msgIO *io)
{
	int nchar, beg_field; char *value;
	int c, escaped = 0;
	while(isspace(Mgetc(io))) {}
	beg_field = io->pos; 
	if(io->buf[beg_field] != QUOTE_CHAR) PROBLEM
		"Expected string to begin with '%c', began with '%c'",
		QUOTE_CHAR, io->buf[beg_field] SERVER_ERROR(cl_current);
	while( (c=Mgetc(io)) != QUOTE_CHAR)
		if(c== ESCAPE_CHAR ) {
			escaped = 1;
			switch( Mgetc(io)) {
			case QUOTE_CHAR: case ESCAPE_CHAR: break;
			default: PROBLEM
			"Escape (%c) not followed by '%c' or '%c' (undefined)",
				ESCAPE_CHAR, ESCAPE_CHAR, QUOTE_CHAR SERVER_ERROR(cl_current);
			}
	}
	beg_field++;
	nchar = io->pos - beg_field;
	value = New(nchar +1, char);
	if(escaped) {
		int i, j; char *p;
		p = io->buf+beg_field;
		for(i=j=0; i<nchar; i++) {
			c = p[i];
			if(c==ESCAPE_CHAR) c = p[++i];
			value[j++] = c;
		}
	}
	else strncpy(value, io->buf+beg_field, nchar);
	return(value);
}

static char *
getData(msgIO *io, long nbytes)
{
	/* this behavior is not fully defined until we know the Lisp */
	/* convention for representing a vector of bytes */
	/* right now, assumes the io->cpos points at the first byte of
	/* the data; will allocate enough space & fill from buffer, then
	/* by reading */
	long i,n;
	char *value, *ptr;
	value = New(nbytes, char); n = io->nbytes - io->pos - 1;
	ptr = io->buf + io->pos;
	for(i=0; i<n; i++)
		value[i] = ptr[i];
	io->pos += n;
	nbytes -= n;
	if(nbytes>0)
		Mread(io->fd, value + n, nbytes);
	return(value);
}

void
Mread(int fd, char *where, long nbytes)
{
	int i;
	i = read(fd, where, (unsigned)nbytes);
	if(i < nbytes) PROBLEM "Tried to read %d bytes, only got %d",
		nbytes, i SERVER_ERROR(cl_current);
}

void
Mwrite(int fd, char *where, long nbytes)
{
	unsigned i;
	i = write(fd, where, (unsigned)nbytes);
	if(i < nbytes) PROBLEM "Tried to write %d bytes, only wrote %d",
		nbytes, i SERVER_ERROR(cl_current);
}

static
int
MgetcError() {
	PROBLEM "Read to the end of the header without completing list"
	SERVER_ERROR(cl_current);
	return(0);
}
