
#include <ctype.h>

#include "msg.h"

static void i_to_a(char *buf, long n);
static void write_nbytes(msgIO *io, unsigned value);
static unsigned make_reply_header(message *replyto);
static void flush_out_buf(msgIO *io);
static void putField(char *text);

static char *msg_out_buf;
int msg_out_pos, msg_buf_len = 0;
#define Mputc(c) (((msg_out_pos >= msg_buf_len) ? xpd_msg_buf(): 0), \
	msg_out_buf[msg_out_pos++]=c)

static int
xpd_msg_buf()
{
	int newlen;
	if(msg_buf_len == 0) {newlen = 1024; msg_out_buf = Calloc(newlen,char);}
	else {
		newlen = msg_buf_len*2;
		msg_out_buf = Realloc(msg_out_buf, newlen, char);
	}
	msg_buf_len = newlen;
	return(0);
}

void
send_message_body(client *cl, long nbytes, char *bytes, message *replyto)
{
	if(replyto) {
		unsigned ntotal = nbytes;
		ntotal += make_reply_header(replyto) + 16;
		write_nbytes(cl->io, ntotal); /* the total chunk size */
		flush_out_buf(cl->io); /* write the reply header  */
	}
	write_nbytes(cl->io, nbytes);
	if(verbose_client) {
		char stuff[43]; int n;
		n = nbytes > 42 ? 42 : nbytes;
		strncpy(stuff, bytes, n);
		stuff[n] = '\0';
		if(n< nbytes) fprintf(stderr, "send_message_body: body = \"%s ...\"\n",
			stuff);
		else fprintf(stderr, "send_message_body: body = \"%s\"\n", stuff);
	}
	if(nbytes>0) Mwrite(cl->io->fd, bytes, nbytes);
}

void
send_message_file(client *cl, char *filename, message *replyto)
{
	long nbytes, fdin, fdout; FILE *file;
	if((file = fopen(filename, "r"))==NULL)
		PROBLEM "Couldn't open file \"%s\"", filename
		MSG_ERROR;
	fseek(file, 0L, 2);
	nbytes = ftell(file);
	rewind(file);
	if(replyto) {
		unsigned ntotal = nbytes;
		ntotal += make_reply_header(replyto) + 16;
		if(verbose_client) {
			fprintf(stderr,"send_message_file: Sending reply\n");
		}
		write_nbytes(cl->io, ntotal); /* the total chunk size */
		flush_out_buf(cl->io); /* write the reply header  */
	}
	if(verbose_client) 
		fprintf(stderr, "send_message_file: file \"%s\"\n", filename);
	write_nbytes(cl->io, nbytes);
	fdin = fileno(file); fdout = cl->io->fd;
	while(nbytes>0) {
		long to_send, n;
		to_send = nbytes < msg_buf_len ? nbytes : msg_buf_len;
		if((n=read(fdin, msg_out_buf, to_send)) < to_send)
			PROBLEM "Could only read %ld of %ld bytes from file to be copied",
			n, to_send MSG_ERROR;
		if((n=write(fdout, msg_out_buf, to_send)) < to_send)
			PROBLEM "Could only write %ld of %ld bytes to message server",
			n, to_send SERVER_ERROR(cl);
		nbytes -= to_send;
	}
}

static void
i_to_a(char *buf, long n)
{
/* like the C book example, but knows n>=0 */
	long i, j; int c;
	i=0;
	do buf[i++] = n % 10 + '0'; while( (n /= 10) > 0);
	buf[i] = '\0';
	for(j=0, i--; j<i; j++, i--) {
		c = buf[i]; buf[i] = buf[j]; buf[j] = c;
	}
}

char *
send_message_header(client *cl, char *msgType, serverId receiver, char *msg_id,
	message *replyto)
{
	msgIO *io; int isreply = name_eq(msgType, "response");
	io = cl->io;
	if(!msg_id) msg_id = next_msg_id(0);
	if(isreply) {if(!replyto) PROBLEM "Message type \"response\" but no reply-to message given"
		MSG_ERROR;}
	else if(replyto) PROBLEM "Reply-to message only meaningful for type \"response\", not \"%s\"",
		msgType MSG_ERROR;
	write_msg_header(io, msgType, cl->id, receiver, msg_id);
	return(msg_id);
}

/* write the length of a chunk in the standard message format of 8 bytes */
/* This code assumes unsigned long is a 4 byte object.  A more portable form */
/* would be nice, but the standard format may itself change, so we will hang */
/* in as is until such questions are resolved */
static void
write_nbytes(msgIO *io, unsigned value)
{
	unsigned long numbuf[2];
	numbuf[0] = htonl(0);
	numbuf[1] = htonl(value);
	if(verbose_client) fprintf(stderr, "write_nbytes: %d\n", value);
	Mwrite(io->fd, (char *)numbuf, 8);
}

/* write the chunk (the message header) currently in the io buffer */
static void
flush_out_buf(msgIO *io)
{
	write_nbytes(io, (unsigned)msg_out_pos);
	if(verbose_client){
		msg_out_buf[msg_out_pos] = '\0';
		fprintf(stderr,
		"flush_out_buf: \"%s\"\n", msg_out_buf);
	}
	Mwrite(io->fd, msg_out_buf, msg_out_pos);
	msg_out_pos = 0;
}

/* output a text string so that it will be returned unchanged by the message */
/* server: enclose it in quotes and escape the quote and escape character */
static void
putField(char *text)
{
	int c;
	Mputc(QUOTE_CHAR);
	while((c = *text++)) {
		switch(c){
		case QUOTE_CHAR: case ESCAPE_CHAR:
			Mputc(ESCAPE_CHAR);
		}
		Mputc(c);
	}
	Mputc(QUOTE_CHAR);
}

/* construct the message header in the io buffer and optionally flush
it out to the message server.  Each field will be a standard quoted
string */
unsigned
write_msg_header(msgIO *io, char *msgType, serverId sender, serverId receiver, char *msg_id)
{
	unsigned nbytes;
	Mputc(LPAR);
	putField(msgType); Mputc(' ');
	putField(sender); Mputc(' ');
	putField(receiver); Mputc(' ');
	putField(msg_id);
	Mputc(RPAR);
	nbytes = msg_out_pos;
	if(io)flush_out_buf(io);
	return(nbytes);
}

static
unsigned
make_reply_header(message *msg)
{
	return(write_msg_header(0,
		msg->type,
		msg->sender,
		msg->receiver,
		msg->messageId));
}

/* Generate a message id.  Generates "s.n.m" where s is the prefix, n is the */
/* process id, and m is sequential starting at 1. Users of the C interface */
/* should reset msg_is_prefix if they don't want "C" as the start of msgId's */
long msg_id_counter; static char msg_id_buf[256], *msg_id_start;
char *msg_id_prefix = "C";

char *
next_msg_id(char *prefix)
{
	char *value;
	if(!prefix)prefix = msg_id_prefix;
	if(!msg_id_counter) {
		sprintf(msg_id_buf, "%s.%d.", prefix, getpid());
		msg_id_start = msg_id_buf + strlen(msg_id_buf);
	}
	sprintf(msg_id_start, "%d", ++msg_id_counter);
	value = New(strlen(msg_id_buf)+1, char);
	strcpy(value, msg_id_buf);
	return(value);
}
