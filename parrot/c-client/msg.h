#ifndef MSG_H
#define MSG_H 1
/* structures for communications via the message server */
typedef char* msg_type;
#define MSG_TYPE(msg) msg->type
#define MSG_ID(msg) msg->messageId
typedef char* serverId;

typedef struct Server {
	serverId id;
	char *host;
	int port;
} server;

#define MSG_BUF_LENGTH(io)   (io)->buf_length
#define STD_MSG_BUF_LENGTH 512
#define MSG_FILENO(io) (io)->fd
#define IN_MEMORY_FD(fd) (fd<0)
#define LPAR '('
#define RPAR ')'
#define QUOTE_CHAR '"'
#define ESCAPE_CHAR '\\'
#define A_LONG_TIME 3600000 /* 1000 hours */

typedef struct MsgIO {
	int fd;
	int buf_length;
	char *buf;
	int pos;
	int nbytes;
	int beg_field;
} msgIO;

typedef void (*Handler_fun) (void *, void *);
typedef struct Client client;
typedef struct Message message;
typedef void (*handler_fun) (client *, message *);
typedef void (*reply_fun) (client *, message *, message *reply_to);
struct Client {
	serverId id;
	serverId server;
	msgIO *io;
	int ntypes;
	msg_type *types;
	handler_fun *handlers;
	handler_fun default_handler;
};

struct Message {
	msg_type type;
	serverId sender;
	serverId receiver;
	char *messageId;
	long nbytes;
	char *body;
};

extern message *newMessage();
extern void handle_msg(client *cl, message *msg);
extern void handle_error(client *cl, message *msg);
extern void handle_disconnect(client *cl, message *msg);
extern void handle_response(client *cl, message *msg);
extern client * set_handlers(client *cl, int ntypes, char **types, handler_fun *handlers, handler_fun default_handler);
extern handler_fun select_handler(client *cl, char *type);
extern client * connect_msg_sock(char *hostname, unsigned port, client *cl, message *msg);
extern void disconnect_msg_sock(client *cl, char *reason);
extern client * newClient();
extern void deleteClient(client *cl);
extern message * newMessage();
extern void deleteMessage(message *msg);
extern int wait_msg(client *cl, int howlong);
extern void read_msg_header(client *cl, message *msg);
extern void read_msg_body(client *cl, message *msg, int keep);
extern void copy_msg_body(client *cl, message *msg, char *filename);
extern int read_nbytes(msgIO *io);
extern void Mread(int fd, char *where, long nbytes);
extern void Mwrite(int fd, char *where, long nbytes);
extern void send_message_body(client *cl, long nbytes, char *bytes, message *replyid);
extern void send_message_file(client *cl, char *filename, message *replyid);
extern char * send_message_header(client *cl, char *msgType, serverId receiver, char *msg_id, message *replyto);
extern unsigned write_msg_header(msgIO *io, char *msgType, serverId sender, serverId receiver, char *msg_id);
extern char * next_msg_id(char *prefix);
extern void Recover(char *Message, void *dummy2);
extern int name_eq(register char *s1,register char *s2);
extern void catchall(int i);
extern void handle_all(client *cl, message *msg);
extern char error_buf[], msg_buf[], *msg_id_prefix;
extern int verbose_client;  extern long msg_id_counter;
extern char * queue_for_reply(reply_fun handler, char *id);
extern int wait_for_reply(client *cl, char *msg_id, int howlong, message *reply);
extern void handle_response(client *cl, message *msg);
extern void request_waiting(void *cl);

/* if running standalone, define a few S-style macros */
#ifndef PROBLEM
#define STAND_ALONE_MESSAGE 1
#include <stdio.h>
/* some error handling macros */
#define PROBLEM		sprintf(error_buf,
#define PRINT_IT	), fprintf(stderr, "%s\n", error_buf);  (error_buf[0] = '\0')
/* some allocation macros */
extern char *S_ok_calloc(unsigned n, unsigned s);
#define Calloc(n,t)	(t *)S_ok_calloc((unsigned)(n),sizeof(t))
#define Realloc(p,n,t)	(t *)S_ok_realloc((char *)(p),(unsigned)(n)*sizeof(t))
#define Free(p)	S_ok_free((char *)(p))
#endif

/* an extra layer for now, to monitor and maybe later to record allocations*/
extern char *new_alloc(unsigned n, unsigned s);
#define New(n, t)	(t *)new_alloc(n, sizeof(t))

/* error handling macros, these go with PROBLEM */
#define DO_ERROR	Recover(error_buf,0)
#define MSG_ERROR	), DO_ERROR
#define SERVER_ERROR(cl)	), \
	disconnect_msg_sock(cl, "Client detected error"), DO_ERROR
#define IO_ERROR ), perror(error_buf), Recover(0,0)
#define NDIGITS 10

#endif
