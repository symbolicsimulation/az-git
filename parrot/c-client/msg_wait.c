
#include "msg.h"

static char **wait_ids = 0; static reply_fun *wait_handlers;
static int n_wait = 0, wait_length=0;
static int is_queued(char *id);

char *
queue_for_reply(reply_fun handler, char *id)
{
	if(n_wait >= wait_length) { /* initialize or expand */
		if(wait_ids) {
			wait_ids = Realloc(wait_ids, 2*wait_length, char *);
			wait_handlers = Realloc(wait_handlers, 2*wait_length, reply_fun);
			wait_length *= 2;
		}
		else {
			wait_ids = Calloc(50, char *);
			wait_handlers = Calloc(50, reply_fun);
			wait_length = 50;
		}
	}
	wait_handlers[n_wait] = handler;
	if(!id) id = next_msg_id(0);
/* make a permanent copy of the id, to stay around until reply received */
	wait_ids[n_wait] = Calloc(strlen(id)+1, char);
	strcpy(wait_ids[n_wait], id);
	n_wait++;
	return(id);
}

int
wait_for_reply(client *cl, char *msg_id, int howlong, message *reply_msg)
{
	int ctr = 0;
	if(howlong < 0) howlong = A_LONG_TIME;
	while(is_queued(msg_id) && ctr++ < howlong) {
		while(!input_waiting(cl->io->fd)){}
		handle_msg(cl, reply_msg);
	}
}

/* the response handler checks for a queued handler for the id in the response
/* part of the message */
void
handle_response(client *cl, message *msg)
{
	message *reply_header = newMessage(); int i;
	reply_fun h = 0;
	read_msg_header(cl, reply_header);
/* the stream is positioned at the beginning of the data part of the response */
/* By convention, this starts with the header of the message replied to */
	for(i = n_wait; i>0;i-- ) {
		if(name_eq(MSG_ID(reply_header), wait_ids[i-1])) {
			h = wait_handlers[i-1];
	/* free the copy made when the id was entered by queue_for_reply */
			Free(wait_ids[i-1]);
			break;
		}
	}
	if(i>0) {
		int j;
		for(j=i; j<n_wait; j++){
			wait_ids[j-1] = wait_ids[j];
			wait_handlers[j-1] = wait_handlers[j];
		}
		n_wait--;
	}
/* reading the reply header also read the header chunk from the real message */
	msg->nbytes = reply_header->nbytes;
	reply_header->nbytes = 0; /* just to be safe */
	if(!h) { /* flush the message */
		read_msg_body(cl, msg, 0);
		PROBLEM "No reply handler for received response (to message \"%s\")",
			MSG_ID(reply_header) SERVER_ERROR(cl);
	}
	else (*h)(cl, msg, reply_header);
}

static int
is_queued(char *id)
{
	int i;
	for(i = n_wait; i>0;i-- ) {
		if(name_eq(id, wait_ids[i-1])) 
			return(1);
	}
	return(0);
}
