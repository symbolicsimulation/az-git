
#include <ctype.h>
#include <signal.h> /* used only for standalone handle_error */
#include "msg.h"

void
handle_msg(client *cl, message *msg)
{
	handler_fun h;
	if(!msg->type) /* not initialized => read the header */
		read_msg_header(cl, msg);
	h = select_handler(cl, msg->type);
	if(!h) { /* flush the message */
		read_msg_body(cl, msg, 0);
		PROBLEM "No handler for received message of type \"%s\"",
			MSG_TYPE(msg) PRINT_IT;
		DO_ERROR;
	}
	else (*h)(cl, msg);
}

client *
set_handlers(client *cl, int ntypes, char **types, handler_fun *handlers, handler_fun default_handler)
{
	if(default_handler) cl->default_handler = default_handler;
	cl->ntypes = ntypes;
	if(ntypes>0) {
		cl->types = types;
		cl->handlers = handlers;
	}
	return(cl);
}

/* currently a linear search.  Could be a hash table. */
/* The S version will normally use a single default handler that */
/* disconnect, response and error messages have built-in handlers, although */
/* an error handler may be specified to override the built-in */
/* selects a handle method */
handler_fun
select_handler(client *cl, char *type)
{
	int i; char **types = cl->types;
	handler_fun *h = cl->handlers;
	if(name_eq(type, "disconnect"))
		return(handle_disconnect);
	if(name_eq(type, "response"))
		return(handle_response);
	for(i=0; i<cl->ntypes; i++)
		if(name_eq(type, types[i]))return(h[i]);
	if(name_eq(type, "error"))
		return(handle_error);
	return(cl->default_handler);
}

/* The following handlers are part of the C interface to the message server */
/* "disconnected" may not be overriden by application handler methods,
/* but "error" may. Note that "connected" messages are dealt with directly */
/* in the connect_msg routines, not via select_handler */

/* the handler for server-generated disconnects */
/* The message body is copied to the standard error */
void
handle_disconnect(client *cl, message *msg)
{
	close(cl->io->fd); /* close the socket */
	cl->io->fd = 0;
	if(msg->nbytes >0) { /* report the disconnect message */
		char *why = New(msg->nbytes, char);
		msg->body = why;
		read_msg_body(cl, msg, 1);
		PROBLEM "The message server disconnected: %s", why
		PRINT_IT;
		DO_ERROR;
	}
	else PROBLEM "The message server disconnected"
		MSG_ERROR;
}

/* the default handler for errors */
void
handle_error(client *cl, message *msg)
{
	if(msg->nbytes >0) { /* report the error message */
		char *why = New(msg->nbytes, char);
		msg->body = why;
		read_msg_body(cl, msg, 1);
		PROBLEM "Error from  message server: %s", why
		PRINT_IT;
	}
	else PROBLEM "Unspecified error from  message server"
		PRINT_IT;
	DO_ERROR;
}
