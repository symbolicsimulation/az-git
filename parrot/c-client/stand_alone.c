
/* these routines complete a few loose ends when the Message C interface */
/* is compiled outside of S. */

#include <stdio.h>
#include <signal.h>
#include <setjmp.h>

#include "msg.h"

char error_buf[1024];

void
Recover(char *Message, void *dummy2)
{
	if(Message && *Message)fprintf(stderr,"Error: %s\n",Message);
	else fputs("\n", stderr);
	error_buf[0] = '\0';
	kill(getpid(), SIGTERM); /* send a SIGTERM to myself: standalone */
		/* error handling needs to catch a SIGTERM */
}

/*
 * Versions of calloc(), realloc() and free() that check their
 * args and their return values.  All calls to memory allocation
 * should go through one of these routines.  Note that there is
 * no S_malloc() (just to keep things simple).
 */
char *
S_ok_calloc(unsigned n, unsigned size)
{
	char *p;
	extern void * calloc();

	if(n == 0) {
		n = 1;
		PROBLEM
			"Asked for 0 items of size %ld: using 1", size
		PRINT_IT;
	}
	p = calloc(n, size);
	if(p == 0)
		PROBLEM
			"Unable to obtain requested dynamic memory"
		MSG_ERROR;
	return(p);
}

char *
S_ok_realloc(char *s, unsigned n)
{
	char *p;
	extern void * realloc();

	if(s == 0)
		PROBLEM
			"Tried to reallocate a null pointer"
		MSG_ERROR;
	if(n == 0) {
		n = 1;
		PROBLEM
			"Tried to reallocate 0 bytes: using 1", n
		PRINT_IT;
	}
	p = realloc(s, n);
	if(p == 0)
		PROBLEM
			"Unable to obtain requested dynamic memory"
		MSG_ERROR;
	return(p);
}

void
S_ok_free(char *s)
{
	if(s == 0)
		PROBLEM
			"Tried to free a null pointer"
		MSG_ERROR;
	free((void *)s);
}
/* names are equal if ptrs equal, or if they point to same string of chars */
int
name_eq(register char *s1, char *s2)
{
	if(s1==s2)return(1);
	if(!s1 || !s2)return(0);
	while (*s1 == *s2++)
		if (*s1++=='\0')
			return(1);
	return(0);
}

/* arrange to use a long jump after errors: simplified version of the error */
/* control in S's main program.  The main() should call use_jmp_buf() with */
/* a POINTER to a jmp_buf as its argument.  Then catchall will jump to that */
/* buffer's indicated place when it catches an error.  Typically this is */
/* the top of some simple read-eval loop */
static jmp_buf *jmp_buf_ptr = 0;
void
use_jmp_buf(jmp_buf *p)
{
	jmp_buf_ptr = p;
}

/* simplified version of S's signal catcher: install if you don't want */
/* some signals to cause exit from main program, and insert calls of the */
/* form `signal(i, catchall)' for appropriate signals, i, into your main */
static int last_signal;
#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif
void
catchall(int i)
{
	extern int interrupt_code;
	static struct unix_sigs{
		char *death;
		int is_terminal;
		} sigs[] ={
		"no signal 0",TRUE,
		"hangup",TRUE,
		"interrupt",FALSE,	/* special action below */
		"quit",TRUE,
		"illegal instruction",TRUE,
		"trace trap",TRUE,
		"iot",TRUE,
		"emt instr.",TRUE,
		"floating point exception",FALSE,
		"kill",FALSE,
		"bus error",TRUE,
		"bad address",TRUE,
		"bad system call",TRUE,
		"pipe error",FALSE,
		"alarm clock",FALSE,
		"software signal",FALSE,
		"user 1",FALSE,
		"user 2",FALSE,
		"child death",FALSE,
		"power fail",TRUE,
		};
	last_signal = i;
	if((i>=2 && i<20)||i==1) { /* the signals I know about */
		signal(i,catchall);
		if(isatty(fileno(stdout)))fflush(stdout);
		if(!jmp_buf_ptr || (sigs[i]).is_terminal) {
			PROBLEM "Exiting: %s", (sigs[i]).death
			PRINT_IT;
			exit(1);
		}
		else {
			PROBLEM "Caught signal: %s", (sigs[i]).death
			PRINT_IT;
			longjmp(*jmp_buf_ptr, i);
		}
	} else exit(1); /*too weird to diagnose */
}
