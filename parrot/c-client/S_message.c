
#include "S.h"
#include "msg.h"

#define MAX_CONNECTION 50
static client *S_connections[MAX_CONNECTION];
static int nconnections = 0;

vector *last_S_handle_value = 0;

static client * open_S_connection(char *name, unsigned port);
static client * get_S_connection(char *id);
static char * make_one_string(vector *object, unsigned *nbytes_p);
static void handle_S(client *cl, message *msg);
static void handle_S_reply(client *cl, message *msg, message *replyto);
static message *as_message(vector *object);

static client *
open_S_connection(char *name, unsigned port)
{
	/* create a client connection : no storage is allocated in  this */
	/* routine, so that the permanent allocation in the C routines will */
	/* be stored in S_connections  (may be a small memory leak for many */
	/* connections )*/
	client *cl; extern void add_reader();
	S_connections[nconnections++] = cl =
		connect_msg_sock(name, port, 0, 0);
	/* all requests are handled by standard S handler */
	set_handlers(cl, 0L, 0, 0, handle_S);
	/* add this reader to next_event's list */
	add_reader(cl->io->fd, request_waiting, (void *)cl);
	return(cl);
}

static void
close_S_connection(char *id, char *srvr)
{
	int i; client *cl;
	for(i=0; i< nconnections; i++) {
		cl = S_connections[i];
		if(name_eq(cl->id, id)) {
			/* drop this reader from next_event's list */
			drop_reader(cl->io->fd);
			disconnect_msg_sock(cl, "closing at client's request");
			for(i=i+1; i< nconnections; i++)
				S_connections[i-1] = S_connections[i];
			nconnections--;
			deleteClient(cl);
			return;
		}
	}
	PROBLEM "Message server connection (id=\"%s\", server=\"%s\") not found",
		id, srvr
	RECOVER(NULL_ENTRY);
}

static client *
get_S_connection(char *id)
{
	int which;
	if(!nconnections) return(open_S_connection(NULL_STRING, 0));
	else if(!id) return(S_connections[0]);
	else {
		for(which=0; which<nconnections; which++)
			if(name_eq(S_connections[which]->id, id))break;
		if(which==nconnections) PROBLEM
		"Requested connection with id \"%s\" -- doesn't exist",
			id RECOVER(NULL_ENTRY);
		return(S_connections[which]);
	}
}

vector *
S_message(vector *call, vector *arglist)
{
	vector **args = arglist->value.tree, *value = blt_in_NULL;
	int which = sys_index;
	switch(which) {
	case 0: /* open.basic.connection(message.server) */
		/*  message.server is one or two character strings, "" */
		/* for the standard server, determined by envir. variables */
		{
		vector *srvr; char *hostname; unsigned port;
		client *cl;
		srvr = coevec(args[0], CHAR, TRUE, CHECK_IT);
		if(srvr->length == 0 ||
			(srvr->length == 1 && *(srvr->value.Char[0])=='\0'))
			{hostname = NULL_STRING; port = 0;}
		else if(srvr->length == 2)
			{hostname = srvr->value.Char[0];
			sscanf(srvr->value.Char[1], "%d", &port);
			}
		else if(srvr->length == 1)
			{ hostname = S_alloc(strlen(srvr->value.Char[0])+1,1);
			sscanf(srvr->value.Char[0], "%s %d",&hostname, &port);
			}
		else PROBLEM "message.server must be 0, 1, or 2 strings"
			RECOVER(NULL_ENTRY);
		cl = open_S_connection(hostname, port);
		value = alcvec(CHAR, 2L);
		value->value.Char[0] = c_s_cpy(cl->id);
		value->value.Char[1] = c_s_cpy(cl->server);
		break;
		}
	case 9: /* close.connection(cl) */
		value = coevec(args[0], CHAR, FALSE, CHECK_IT);
		if(value->length < 2) PROBLEM
			"Argument is not a valid connection object"
			RECOVER(NULL_ENTRY);
		close_S_connection(value->value.Char[0], value->value.Char[1]);
		break;
	case 1: /* send.message.file(filename, msgType, receiver, msgId, client, inreply, getreply) */
	case 2: /*send.message.character(data, msgType, receiver, msgId, client, inreply, getreply) */
		{
		char *cl_id, *msg_id; message *replyto; int getreply;
		client *cl;
		if(arglist->length < 7) PROBLEM
			"%ld args not enough for send(data, type, to, id, client, innreply, getreply)",
			arglist->length
			RECOVER(NULL_ENTRY);
		getreply = (int)long_value(args[6], call);
		replyto = as_message(args[5]); /* ptr to message or 0 */
		cl_id = string_value(args[4]);
		cl = get_S_connection(cl_id);
		msg_id = string_value(args[3]);
		if(!*msg_id) msg_id = 0;
		if(getreply)
			msg_id = queue_for_reply(handle_S_reply, msg_id);
		msg_id = send_message_header(cl, string_value(args[1]),
			string_value(args[2]), msg_id, replyto);
		switch(which) {
		case 1: send_message_file(cl, string_value(args[0]),
				replyto);
			break;
		case 2: {
			unsigned nbytes; char *bytes;
			bytes = make_one_string(args[0], &nbytes);
			send_message_body(cl, nbytes, bytes, replyto);
			break;
			}
		}
		if(getreply <0 || getreply > 1) {
			message *reply_msg = newMessage();
			wait_for_reply(cl, msg_id, getreply, reply_msg);
			if(!last_S_handle_value) {
				PROBLEM "No value obtained for reply to message \"%s\"",
				msg_id WARNING(NULL_ENTRY);
				value = blt_in_NULL;
			}
			else {
				value = last_S_handle_value;
				last_S_handle_value = 0;
			}
			/* deleteMessage(reply_msg); */
		}
		else {
			value = alcvec(CHAR, 1L);
			value->value.Char[0] = msg_id;
		}
		break;
		}
	case 10: /* next.msgid(prefix) */
		if(!msg_id_counter)  {
			int prev = set_alloc(PERM_FRAME);
			char *prefix;
			if(Nargs(call)>0)
				prefix = c_s_cpy(string_value(args[0]));
			else prefix = "S";
			if(strlen(prefix)>100) PROBLEM
				"Prefix too long (%d!!)", strlen(prefix)
				RECOVER(NULL_ENTRY);
			strcpy(msg_id_prefix, prefix);
			set_alloc(prev);
		}
		value = alcvec(CHAR, 1L);
		value->value.Char[0] = next_msg_id(msg_id_prefix);
		break;
	case 11: /* set verbose mode */
		value = verbose_client ? blt_in_TRUE : blt_in_FALSE;
		verbose_client = logical_value(args[0], call);
		break;
	default: PROBLEM "Invalid index (%d) to S_message", which
		RECOVER(NULL_ENTRY);
	}
	return(value);
}

static message *
as_message(vector *obj)
{
	message *value; char **strings;
	obj = coevec(obj, CHAR, FALSE, PRECIOUS(obj));
	if(obj->length < 4) return(0);
	strings = obj->value.Char;
	value = (message *)S_alloc(1, sizeof(message));
	value->type = *strings++;
	value->sender = *strings++;
	value->receiver = *strings++;
	value->messageId = *strings++;
	return(value);
}

static char *
make_one_string(vector *object, unsigned *nbytes_p)
{
	char *bytes, **strings; unsigned nbytes;
	int i, n; char *p;
	object = coevec(object, CHAR, TRUE, CHECK_IT);
	strings = object->value.Char;
	n = object->length; nbytes = 1;
	for(i=0; i<n; i++) nbytes += strlen(strings[i])+1;
	p = bytes = S_alloc(nbytes, 1);
	for(i=0; i<n; i++) {
		strcpy(p, strings[i]);
		p += strlen(p);
		*p++ = '\n';
	}
	*nbytes_p = nbytes;
	return(bytes);
}

static vector *handle_S_call; static char **handle_strings;
static char handle_file[30];
static void
handle_S(client *cl, message *msg)
{
/* constructs a call to the S function to do all the work */
	vector *expr;
	if(!handle_S_call) {
		int prev; vector *temp;
		extern void set_class(vector *dataset, vector *repl_val);
		prev = set_alloc(PERM_FRAME);
		temp = alcvec(CHAR, 5L);
		handle_strings = temp->value.Char;
		handle_S_call = alcf("handle", temp);
		set_precious(handle_S_call, cons_frame);
		set_alloc(prev);
	}
	strcpy(handle_file, "/tmp/M");
	strncat(handle_file,msg->messageId, 20);
	copy_msg_body(cl, msg, handle_file);
	handle_strings[0] = msg->type;
	handle_strings[1] = msg->sender;
	handle_strings[2] = msg->receiver;
	handle_strings[3] = msg->messageId;
	handle_strings[4] = handle_file;
	last_S_handle_value = eval(handle_S_call);
}

/* the reply handler for S messages that wanted a reply */
/* This calls the standard S function handle.response with a message */
/* structure similar to an ordinary incoming message EXCEPT that the message
/* id is the id for the original message sent by S.  The responder's
/* message id is appended to the end of the object
*/
static vector *handle_S_reply_call; static char **reply_strings;
static void
handle_S_reply(client *cl, message *msg, message *inreplyto)
{
/* constructs a call to the S function to do all the work */
	vector *expr;
	if(!handle_S_reply_call) {
		int prev; vector *temp;
		extern void set_class(vector *dataset, vector *repl_val);
		prev = set_alloc(PERM_FRAME);
		temp = alcvec(CHAR, 9L);
		reply_strings = temp->value.Char;
		handle_S_reply_call = alcf("handle.response", temp);
		set_precious(handle_S_reply_call, cons_frame);
		set_alloc(prev);
	}
	strcpy(handle_file, "/tmp/M");
	strncat(handle_file,msg->messageId, 20);
	copy_msg_body(cl, msg, handle_file);
	reply_strings[0] = msg->type;
	reply_strings[1] = msg->sender;
	reply_strings[2] = msg->receiver;
	reply_strings[3] = msg->messageId;
	reply_strings[4] = handle_file;
	reply_strings[5] = inreplyto->type;
	reply_strings[6] = inreplyto->sender;
	reply_strings[7] = inreplyto->receiver;
	reply_strings[8] = inreplyto->messageId;
	last_S_handle_value = eval(handle_S_reply_call);
}
