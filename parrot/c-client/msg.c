
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>

#include "msg.h"

int verbose_client = 0;

static void handle_connect(client *cl, message *msg);
char *start_handler_types[] = {"connected", "error"};
handler_fun start_handlers[] = {handle_connect, handle_error};
static void deleteMsgIO(msgIO *io);

client *
connect_msg_sock(char *hostname, unsigned port, client *cl, message *msg)
{
	int fd_socket; extern char  *getenv();
	int n; /* temporary for reading nbytes directly */
	struct sockaddr_in srv; struct hostent *hp, *gethostbyname();
	if(!hostname) {
		char *port_string;
		hostname = getenv("MESSAGE_SERVER_HOST");
		if(!hostname) PROBLEM "Can't get MESSAGE_SERVER_HOST environment variable"
			MSG_ERROR;
		port_string = getenv("MESSAGE_SERVER_PORT");
		if(!hostname) PROBLEM "Can't get MESSAGE_SERVER_PORT environment variable"
			MSG_ERROR;
		port = atoi(port_string);
	}
	fd_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(fd_socket < 0) 
		PROBLEM "Opening socket"
		IO_ERROR;
	srv.sin_family = AF_INET;
	if((hp = gethostbyname(hostname)) == 0)
		PROBLEM "Unknown host (%s)", hostname
		MSG_ERROR;
	memcpy((char *)&srv.sin_addr, (char *)hp->h_addr, hp->h_length);
	srv.sin_port = htons(port);
	if(connect(fd_socket,
		(struct sockaddr *)&srv, sizeof(srv)) < 0)
		PROBLEM "connecting stream socket"
		IO_ERROR;
	if(!cl) cl = newClient();
	cl->io->fd = fd_socket;
	wait_msg(cl, -10);
	if(!msg) msg = newMessage();
	read_msg_header(cl, msg);
	/* set connection to handle only "connected", "error" messages */
	set_handlers(cl, 2, start_handler_types, start_handlers, 0);
	handle_msg(cl, msg);
	return(cl);
}

static void
handle_connect(client *cl, message *msg)
{
		cl->id = msg->receiver;
		cl->server = msg->sender;
		read_msg_body(cl, msg, 0); /* throw away the body */
		/* don't delete the message, we need the 2 strings above */
}

void
disconnect_msg_sock(client *cl, char *reason)
{
	send_message_header(cl, "disconnect", cl->server, next_msg_id(0), 0);
	send_message_body(cl, strlen(reason), reason, 0);
	close(cl->io->fd);
}

client *
newClient()
{
	client *value;
	value = New(1, client);
	value-> io = New(1, msgIO);
	value->io->buf = New(STD_MSG_BUF_LENGTH, char);
	value->io->buf_length = STD_MSG_BUF_LENGTH;
	return(value);
}

message *
newMessage()
{
	return(New(1, message));
}

/* Will wait for howlong calls (if there is no input on this */
/* input stream, input_waiting() returns after 1 second).  If howlong is */
/* negative, waiting will continue for a long time.  */
int
wait_msg(client *cl, int howlong)
{
	int ctr = 0, value;
	if(howlong < 0) howlong = A_LONG_TIME;
	while(!(value = input_waiting(cl->io->fd)) && ctr++ < howlong)
		{}
	return(value);
}

void
deleteClient(client *cl)
{
	if(!cl)return;
	if(cl->id) Free(cl->id);
	if(cl->server) Free(cl->server);
	if(cl->io) deleteMsgIO(cl->io);
	if(cl->ntypes>0) {
		int i, n = cl->ntypes; msg_type *p = cl->types;
		for(i=0; i<n; i++) Free(p[i]);
	}
	Free(cl);
}

void
deleteMessage(message *msg)
{
	if(msg->type)Free(msg->type);
/* assume the sender id is from the client; don't delete */
	if(msg->messageId) Free(msg->messageId);
	if(msg->body)Free(msg->body);
	Free(msg);
}

static void
deleteMsgIO(msgIO *io)
{
	if(io->buf)Free(io->buf);
	Free(io);
}

/* intercept New() macro; currently for debugging help.  Eventually this */
/* could be used to maintain a threaded stack of allocations */
char *
new_alloc(unsigned n, unsigned s)
{
	char *ptr;
	ptr = S_ok_calloc(n, s);
	return(ptr);
}

void
request_waiting(void *cl_ptr)
{
/* a request is waiting on the clients socket.  initiate processing for it */
	client *cl; message *msg;
	cl = (client *)cl_ptr;
	msg = newMessage();
	handle_msg(cl, msg);
	deleteMessage(msg);
}
