
#include <stdio.h>
#include <signal.h>
#include <setjmp.h>
#include "msg.h"

extern client *print_client(); extern message *print_message();
extern void handle_all(), catchall();

extern void use_jmp_buf();
jmp_buf error_jmp;

#define BUF_LEN 120
static char buffer[BUF_LEN],type[BUF_LEN],me[BUF_LEN],to[BUF_LEN],
	msg_body[BUF_LEN];
#define GET(what) fgets(buffer, BUF_LEN, stdin), buffer[strlen(buffer)-1]='\0',\
	strcpy(what, buffer)
static char * print_unlex(char *string, int nbytes);
static void test_message(void *cl_ptr);

main()
{
	client *cl;
	extern void add_reader();
	verbose_client = 1;
	cl = connect_msg_sock(0, 0, 0, 0);
	print_client(cl, stderr);
	/* add this reader to next_event's list */
	add_reader(cl->io->fd, request_waiting, (void *)cl);
	add_reader(fileno(stdin), test_message, (void *)cl);
	cl = set_handlers(cl, 0, 0, 0, handle_all);
	print_client(cl, stderr);
	signal(SIGTERM, catchall);
	while(1) {
		use_jmp_buf(&error_jmp);
		setjmp(error_jmp);
		next_event();
	}
}

static void
test_message(void *cl_ptr)
{
	client *cl = (client *)cl_ptr; int nbytes; char *msg_id;
	GET(type);
	if(*type == '\0') {
		fputs("Message Type: ", stderr);
		GET(type);
	}
	fputs("Receiver ID: ", stderr);
	GET(to);
	fputs("Message or < filename: ", stderr);
	GET(msg_body);
	msg_id = send_message_header(cl, type, to, 0, 0);
	if(*msg_body == '<') {
		send_message_file(cl, msg_body+1, 0);
		fprintf(stderr, "sent message with id: \"%s\" from file \"%s\"\n",
			msg_id, msg_body+1);
	}
	else {
		nbytes = strlen(msg_body)+1;
		msg_body[nbytes-1] = '\n';
		msg_body[nbytes++] = '\0';
		send_message_body(cl, nbytes, msg_body, 0);
		fprintf(stderr, "sent message with id: \"%s\", %d bytes\n",
			msg_id, nbytes);
	}
}

void
handle_all(client *cl, message *msg)
{
	fputs("\nMesage received by handle_all: \n", stderr);
	read_msg_body(cl, msg, 1);
	print_message(msg, stderr);
	deleteMessage(msg);
}

client *
print_client(client *cl, FILE *file)
{
	fprintf(file, "Client id: \"%s\"; Server id: \"%s\"\n",
		cl->id, cl->server);
	fprintf(file, "\t%d special message handlers", cl->ntypes);
	if(cl->default_handler) fputs("plus default handler", file);
	fputs("\n", file);
	return(cl);
}

char tmp_file_name[BUF_LEN]; FILE *tmp = 0; char cmd[BUF_LEN];
message *
print_message(message *msg, FILE *file)
{
	int i, n;
	fprintf(file, "Message id: \"%s\"; Sender, Receiver: \"%s\", \"%s\"\n",
		msg->type, msg->sender, msg->receiver);
	fprintf(file, "Message (%d bytes): \n", msg->nbytes);
	if(!msg->nbytes)return(msg);
	if(!msg->body) PROBLEM "Empty message body"
		MSG_ERROR;
	print_unlex(msg->body, msg->nbytes);
	return(msg);
}

static char *
print_unlex(char *string, int nbytes)
{
	char *outstr; char *op; int c, i;
	unsigned char *s = (unsigned char *)string;
	putc('"',stderr);
	/* process per char */
	for(i=0; i<nbytes; i++) {
		c = *s++;
		switch(c) {
		case '"':
		case '\\':
			putc('\\', stderr); putc(c, stderr); break;
		case '\n':
			putc('\\', stderr); putc('n', stderr); break;
		case '\r':
			putc('\\', stderr); putc('r', stderr); break;
		case '\t':
			putc('\\', stderr); putc('t', stderr); break;
		case '\b':
			putc('\\', stderr); putc('b', stderr); break;
		default:  if(c < 040 || c >= 0177) {
			putc('\\', stderr);
			putc('0' + c/64, stderr);
			putc('0' + (c%64)/8, stderr);
			putc('0' + (c%64)%8, stderr);
			} else putc(c, stderr);
		}
	}
	putc('"', stderr);
	putc('\n', stderr);
	return(string);
}
