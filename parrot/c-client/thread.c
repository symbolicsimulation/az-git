
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#define READ_SELECT(n,f,t) select(n,f,0,0,t)
typedef void (*void_fun)(void *);
/*
	route allocation through the S routines (which check avaialability)
	To run w/o these, replace S_ok_calloc, S_ok_realloc with calloc, realloc
	in the definitions and in the declaration
*/
extern char *S_ok_calloc(unsigned n, unsigned s);
extern char *S_ok_realloc(char *p, unsigned n);
#define Calloc(n,t)	(t *)S_ok_calloc((unsigned)(n),sizeof(t))
#define Realloc(p,n,t)	(t *)S_ok_realloc((char *)(p),(unsigned)(n)*sizeof(t))


static int *reader_fds, n_reader_fds = 0, max_reader_fds, FD_RANGE;
static void_fun *reader_funs; static void **reader_args;
static fd_set input_fds, this_fdset; static struct timeval input_timeout;
extern void do_request();

/*
	next_event() loops until an event occurs.  This is currently input
	on one of the file descriptors for which a reader has been added
	to the list or a caught signal (which will longjmp back to the S
	main loop.  Each reader is responsible for handling the relevant
	event.  Events may later be extended to non-input events (e.g.,
	window events).
*/
int
next_event()
{
	int value;
	input_timeout.tv_sec = 1;
	while(1) {
		this_fdset =  input_fds; /* writable copy */
		value = READ_SELECT(FD_RANGE, &this_fdset, &input_timeout);
		if(value!=0)break;
	}
	if(value < 0)
		perror("Problem in testing for input ready");
	else do_request();
/* currently just returns the select value, which is ignored by do_S.  When othe
/* event types are recognized, should return the event type */
	return(value);
}

/* use select to wait for input on a particular file descriptor */
/* basically equivalent to sleeping for a second or until input waiting */
int
input_waiting(int this_fd)
{
	int value;
	input_timeout.tv_sec = 1;
	if(this_fd >= FD_RANGE) FD_RANGE = this_fd + 1;
	FD_ZERO(&this_fdset);
	FD_SET(this_fd, &this_fdset); 
	value = READ_SELECT(FD_RANGE, &this_fdset, &input_timeout);
	if(value < 0){
		perror("Error: Problem in testing for input waiting");
		Recover(0,0);
	}
	return(value);
}

/* some routines, structures for co-ordinating lex input and other requests */
/* fd is a file descriptor on which requests may be read (e.g., a socket) */
/* fun is a reader function for input on that df */
/* arg is the single argument to that function:  NB: it must have been */
/* permanently allocated.  Typical examples are that it is a client structure */
/* for an S message connection or the Program object for do_eval() */
void
add_reader(fd, fun, arg)
int fd; void_fun fun; void *arg;
{
	if(n_reader_fds>=max_reader_fds) {
		max_reader_fds = max_reader_fds > 0 ? 2*max_reader_fds : 10;
		reader_fds = reader_fds ? Realloc(reader_fds, max_reader_fds, int)
			: Calloc(max_reader_fds, int);
		reader_funs = reader_funs ? Realloc(reader_funs, max_reader_fds, void_fun)
			: Calloc(max_reader_fds, void_fun);
		reader_args = reader_args ? Realloc(reader_args, max_reader_fds, void *)
			: Calloc(max_reader_fds, void *);
	}
	reader_fds[n_reader_fds] = fd;
	reader_funs[n_reader_fds] = fun;
	reader_args[n_reader_fds] = arg;
	FD_SET(fd, &input_fds); /* use select to listen for input */
	if(fd>=FD_RANGE)FD_RANGE = fd+1;
	n_reader_fds++;
}

void
drop_reader(fd)
int fd;
{
	int i;
	for(i=0; i<n_reader_fds; i++)
		if(reader_fds[i] == fd) {
			for(i=i+1; i<n_reader_fds; i++) {
				reader_fds[i-1] = reader_fds[i];
				reader_funs[i-1] = reader_funs[i];
				reader_args[i-1] = reader_args[i];
			}
			n_reader_fds--;
			FD_CLR(fd, &input_fds);
			FD_RANGE = 1; /* recompute the range for listening */
			for(i=0; i<n_reader_fds; i++)
				if(reader_fds[i]>=FD_RANGE)FD_RANGE = reader_fds[i]+1;
			return;
		}
	fprintf(stderr, "Handler file descriptor to be dropped (%d) not found\n",
		fd);
}

void
do_request()
/* a request is waiting */
/* find out which one, and invoke that reader */
{
	int i;
	for(i=0; i<n_reader_fds; i++)
		if(FD_ISSET(reader_fds[i], &this_fdset)) break;
	if(i==n_reader_fds) fprintf(stderr,
		"Warning: input ignored, not from any known request\n");
	else (reader_funs[i])(reader_args[i]);
}

