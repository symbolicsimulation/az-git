;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Parrot -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved.
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;=======================================================

(in-package :Parrot)

;;;============================================================

(defmacro cache-value (arg-list &body body) 

  "This assumes that the function should give the same answer for the
same arguments (in the sense of #'eql).  It won't work, for example,
on functions of arrays that depend on the values of the array
elements, unless you are willing to assume that the array elements
will not be changed from call to call.

See Abelson and Sussman, SICP, ex 3.27."

  (let ((outer-table (make-hash-table :test #'eq :size 1024))
	(value (gensym))
	(found? (gensym)))
    (ecase (length arg-list)
      (1
       (let ((key (first arg-list)))
	 `(multiple-value-bind (,value ,found?) (gethash ,key ',outer-table)
	    (unless ,found?
	      (setf ,value (progn ,@body))
	      (setf (gethash ,key ',outer-table) ,value))
	    ,value)))
      (2
       (let ((outer-key (first arg-list))
	     (inner-key (second arg-list))
	     (inner-table (gensym))
	     (inner-found? (gensym)))
	 `(multiple-value-bind
	      (,inner-table ,inner-found?) (gethash ,outer-key ',outer-table)
	    (unless ,inner-found?
	      (setf ,inner-table (make-hash-table :test #'eq :size 1024))
	      (setf (gethash ,outer-key ',outer-table) ,inner-table))
	    (multiple-value-bind
		(,value ,found?) (gethash ,inner-key ,inner-table)
	      (unless ,found?
		(setf ,value (progn ,@body))
		(setf (gethash ,inner-key ,inner-table) ,value))
	      ,value))))
      ;; nothing for more than 2 args yet
      )))

;;;=======================================================================

(defmacro lazy-gethash (key table constructor) 

  "This is like gethash, except that constructor is a function, rather
than a default value, and the value returned by funcall-ing the
constructor is stored into the table if the lookup returns nil.  Note
that this means that nil cannot be stored in a lazy hashtable --- at
least, not efficiently, which is the whole point."

  (let ((k (gensym))
	(tab (gensym))
	(value (gensym)))
    `(let* ((,k ,key)
	    (,tab ,table)
	    (,value (gethash ,k ,tab)))
       (when (null ,value)
	 (setf ,value ,constructor)
	 (setf (gethash ,k ,tab) ,value))
       ,value)))
 
;;;=======================================================================
;;; Using association lists as tables
;;;=======================================================================

(defun alist-table? (table)
  (and (typep table 'List)
       (typep (first table) 'Funcallable)))

(deftype Alist-Table () '(satisfies alist-table?))
(deftype Table () '(or Alist-Table Hash-Table))

;;;-----------------------------------------------------------------------

(defun make-alist-table (&key (test #'eql))
  
  "Creates an association list that can be used like a hash-table.
The test function is kept in the first element of the list
and the actual associations are kept in the rest of the list."

  (declare (type Funcallable test))
  
  (list test))

;;;-----------------------------------------------------------------------

(defun getalist (key table &optional default) 

  "This is like a gethash for association list tables."
  
  (declare (optimize (safety 0) (speed 3))
	   (type T key default)
	   (type List table))
  
  (let ((test (first table))
	(value default))
    (declare (type Funcallable test)
	     (type T value))
    (dolist (entry (rest table))
      (declare (type Cons entry))
      (when (funcall test key (first entry))
	(setf value (rest entry))))
    value))

;;;-----------------------------------------------------------------------

(defun setalist (value key table)
  
  "This is used to define (setf (getalist ...) ...)."

  (declare (type T key value)
	   (type List table))

  (let ((entry (assoc key (rest table) :test (first table))))
    (if entry
	;; then just change the value in the existing entry
	(setf (cdr entry) value)
      ;; else need to add e new entry
      (setf (rest table) (cons (cons key value) (rest table))))))

;;;-----------------------------------------------------------------------

(defsetf getalist (key table &optional default) (value)

  "This permits (setf (getalist ---) ---)."

  (declare (type T key default value)
	   (type List table)
	   (ignore default))
  
  `(setalist ,value ,key ,table))

;;;-----------------------------------------------------------------------

(defmacro lazy-getalist (key table constructor) 

  "This is like getalist, except that constructor is a expression, rather
than a default value, and the value returned by funcall-ing the
constructor is stored into the table if the lookup returns nil.  Note
that this means that nil cannot be stored in a lazy alist --- at
least, not efficiently, which is the whole point."

  (let ((k (gensym))
	(tab (gensym))
	(value (gensym)))
    `(let* ((,k ,key)
	    (,tab ,table)
	    (,value (getalist ,k ,tab)))
       (when (null ,value)
	 (setf ,value ,constructor)
	 (setf (getalist ,k ,tab) ,value))
       ,value)))
 
;;;-----------------------------------------------------------------------

(defgeneric lookup (key table &optional default)
  (declare (type T key default)
	   (type (or List Hash-Table) table))
  (:documentation
   "Lookup provides a generic way to get and set entries in
association lists and hashtables (like gethash)."))

(defgeneric (setf lookup) (new-value key table &optional default)
  (declare (type T new-value key default)
	   (type (or List Hash-Table) table))
  (:documentation
   "Lookup provides a generic way to get and set entries in
association lists and hashtables."))

(defmethod lookup (key (table Hash-Table) &optional default)
  (gethash key table default))

(defmethod (setf lookup) (new-value key (table Hash-Table) &optional default)
  (declare (ignore default))
  (setf (gethash key table) new-value))

(defmethod lookup (key (table List) &optional default)
  (getalist key table default))

(defmethod (setf lookup) (new-value key (table List) &optional default)
  (declare (ignore default))
  (setf (getalist key table) new-value))

(defmacro lazy-lookup (key table constructor) 

  "This is like lookup, except that constructor is a expression, rather
than a default value, and the value returned by funcall-ing the
constructor is stored into the table if the lookup returns nil.  Note
that this means that nil cannot be stored in a lazy alist --- at
least, not efficiently, which is the whole point."

  (let ((k (gensym))
	(tab (gensym))
	(value (gensym)))
    `(let* ((,k ,key)
	    (,tab ,table)
	    (,value (lookup ,k ,tab)))
       (when (null ,value)
	 (setf ,value ,constructor)
	 (setf (lookup ,k ,tab) ,value))
       ,value)))

;;;-----------------------------------------------------------------------

(defgeneric remove-entry (key table)
  (declare (type T key)
	   (type (or List Hash-Table) table))
  (:documentation
   "Remove-entry provides a generic way to delete entries from
association lists and hashtables (like remhash)."))

(defmethod remove-entry (key (table Hash-Table))
  (remhash key table))

(defmethod remove-entry (key (table List))
  (setf (rest table) (delete key (rest table) :key #'car)))
