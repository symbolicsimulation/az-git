;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(let ((pathstring
       (namestring
	(truename
	 (pathname
	  (concatenate 'String
	    (namestring 
	     (make-pathname :directory (pathname-directory *load-truename*)))
	    "wrench.o"))))))
  #+:cmu
  (eval-when (load eval)
    (defvar *wrench-loaded?* nil
      "Can only load foreign files once in CMUCL.")

    (unless *wrench-loaded?*
      (alien:load-foreign pathstring)
      (setf *wrench-loaded?* t)))

  #+:excl
  (load "" :foreign-files (list pathstring)))