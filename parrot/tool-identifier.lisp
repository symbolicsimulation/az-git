;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(deftype Tool-Identifier () 
  "An Tool-Identifier is a unique id, produced by the Dispatcher,
used to distinguish tools. Identifiers are currently represented 
by Fixnums."
  'Fixnum)

(defvar *last-tool-identifier* 0
  "Gets incremented with each new identifier issued.")
(declaim (type Tool-Identifier *last-identifier*))

(defun generate-tool-identifier () (incf *last-tool-identifier*))
 
(defun tool-identifier= (id0 id1)
  (declare (type Tool-Identifier id0 id1)
	   (:returns (type (Member t nil))))
  (= id0 id1))

(defun tool-identifier->chunk (id)
  "Return a chunk encoding {\tt id}."
  (declare (type Tool-Identifier id)
	   (:returns (type Chunk)))
  (string->chunk (format nil "~d" id)))

(defun chunk->tool-identifier (chunk)
  "Return the tool identifier encoded in <chunk>."
  (declare (type Chunk chunk)
	   (:returns (type Tool-Identifier)))
  (read-from-string (chunk->string chunk)))

(defconstant _wild_tool_ -1
  "The wildcard value for tool identifiers in {\tt join-audience}.")

(defconstant _meta_tool_ -2
  "The meta wildcard value for tool identifiers in {\tt leave-audience}.")

