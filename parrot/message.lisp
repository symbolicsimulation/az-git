;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================
;;; Functions for taking apart raw messages (lists of chunks)

(defun message-receiver-chunk (message)
  "Return the receiver chunk from <message>."
  (declare (type List message)
	   (:returns (type Chunk)))
  (first message))

(defun message-id-chunk (message)
  "Return the id chunk from <message>."
  (declare (type List message)
	   (:returns (type Chunk)))
  (second message))

(defun message-operation-chunk (message)
  "Return the operation chunk from <message>."
  (declare (type List message)
	   (:returns (type Chunk)))
  (third message))

(defun message-sender-chunk (message)
  "Return the sender chunk from <message>."
  (declare (type List message)
	   (:returns (type Chunk)))
  (fourth message))

(defun message-address-chunk (message)
  "Return the address chunk from <message>."
  (declare (type List message)
	   (:returns (type Chunk)))
  (fifth message))

(defun message-arg-chunks (message)
  "Return the untranslated arg chunks of <message>."
  (declare (type List message)
	   (:returns (type List)))
  (nthcdr 5 message))

;;;------------------------------------------------------------

(defun message-receiver (message)
  "Return the receiver tool-identifier for <message>."
  (declare (type List message)
	   (:returns (type Tool-Identifier)))
  (chunk->tool-identifier (message-receiver-chunk message)))

(defun message-id (message)
  "Return the id for <message>."
  (declare (type List message)
	   (:returns (type Message-Identifier)))
  (chunk->message-identifier (message-id-chunk message)))

(defun message-operation (message)
  "Return the operation identifier for <message>."
  (declare (type List message)
	   (:returns (type Identifier)))
  (chunk->operation-name (message-operation-chunk message)))

(defun message-sender (message)
  "Return the sender tool-identifier for <message>."
  (declare (type List message)
	   (:returns (type Tool-Identifier)))
  (chunk->tool-identifier (message-sender-chunk message)))

(defun message-address (message)
  "Return the address tool-identifier for <message>."
  (declare (type List message)
	   (:returns (type Tool-Identifier)))
  (chunk->tool-identifier (message-address-chunk message)))

(defun message-args (message)
  "Return the args of <message>, using the default <chunk->lisp> translation."
  (declare (type List message)
	   (:returns (type List)))
  (mapcar #'chunk->lisp (message-arg-chunks message)))

;;;------------------------------------------------------------

(defun print-message (chunks)
  (format t "~&~s ~s ~s ~s ~s~{ ~s~}~%"
	  (message-receiver chunks)
	  (message-id chunks)
	  (message-operation chunks)
	  (message-sender chunks)
	  (message-address chunks)
	  (mapcar #'chunk->string (message-arg-chunks chunks))))

