;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================

(deftype Message-Identifier () 
  "An Message-Identifier is a unique id, produced by the Dispatcher,
used to distinguish messages. Identifiers are currently represented 
by Fixnums."
  'Fixnum)

(defvar *last-message-identifier* 1000
  "Gets incremented with each new identifier issued.")
(declaim (type Message-Identifier *last-identifier*))

(defun generate-message-identifier () (incf *last-message-identifier*))
 
(defun message-identifier= (id0 id1)
  (declare (type Message-Identifier id0 id1)
	   (:returns (type (Member t nil))))
  (= id0 id1))

(defun message-identifier->chunk (id)
  "Return a chunk encoding {\tt id}."
  (declare (type Message-Identifier id)
	   (:returns (type Chunk)))
  (string->chunk (format nil "~d" id)))

(defun chunk->message-identifier (chunk)
  "Return the tool identifier encoded in <chunk>."
  (declare (type Chunk chunk)
	   (:returns (type Tool-Identifier)))
  (read-from-string (chunk->string chunk)))

