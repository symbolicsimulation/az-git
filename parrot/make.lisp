;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-

(in-package :cl-user)

(load 
 (truename
  (pathname
   (concatenate 'String
     (namestring 
      (make-pathname :directory (pathname-directory *load-truename*)))
     "parrot.lisp"))))

(cs :Parrot)

