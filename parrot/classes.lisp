;;; -*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;; 
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty --- about the software, its performance, or its
;;; conformity to any specification --- is given or implied.
;;;
;;;============================================================

(in-package :Parrot)

;;;=======================================================

(defclass Dead-Object (Standard-Object) ()
  (:documentation
   "The default method for <kill> changes the class to
Dead-Object."))
 
;;;============================================================

(defclass Tool (Standard-Object)
	  ((tool-identifier
	    :type (or Null Tool-Identifier)
	    :reader tool-identifier
	    :initform nil
	    :documentation
	    "A unique Tool-Identifier assigned to each Tool
by the interprocess message Dispatcher. Tools use Tool-IdentifierS 
to address messages to other tools.")
	   (connection
	    :type (or Null #+:excl Connection #+:cmu T)
	    :reader connection
	    :initform nil
	    :documentation
	    "The Connection object that this tool uses to communicate
with the Interprocess Message Dispatcher."))
  
  (:documentation
   "A Tool is an object that sends and receives messages.
How it handles a message is determined by its current mode,
which operates on a queue of messages received."))

;;;============================================================

(defclass Dispatcher (Tool)
  ((dispatcher-host
    :type Host-Name
    :reader dispatcher-host
    :initform (parrot-host)
    :initarg :dispatcher-host
    :documentation
    "The host on which the Dispatcher is running.")
   (dispatcher-port
    :type Port
    :reader dispatcher-port
    :documentation "The port used by clients to request connections.")
   (dispatcher-socket
    :type Socket
    :reader dispatcher-socket
    :documentation
    "The socket used by the dispatcher to receive connection requests.")
   (input-handler
    :type Socket-Input-Handler
    :reader input-handler
    :documentation
    "This is the Socket-Input-Handler used to accept connection requests.")
   (id-connections
    :type Hash-Table 
    :accessor id-connections
    :initform (make-hash-table :test #'eql)
    :documentation
    "Which connection is used to transmit messages to each tool-identifier?")
   (audiences
    :type Hash-Table
    :reader audiences
    :initform (make-hash-table :test #'equal)
    :documentation
    "The <audiences> table is used to map a message 
(based on the sender, operation, and receiver) to a list of audience 
member ids. For each message received by the message dispatcher, it will
check the audience table to see if anyone else wants to see a the message
and forward a copy if so.

The <audiences> maps senders (including wildcards) to operation tables.
An operation table maps operations (including wildcards) to receiver tables. A
receiver table maps receivers (including wildcards) to audience lists."))

  (:documentation
   "A CL implementation of an interprocess multicast message dispatcher."))

;;;============================================================

(defclass Connection (Standard-Object)

  ((connection-socket
    :type Socket
    :reader connection-socket
    :initarg :connection-socket
    :documentation
    "The socket that the Message Dispatcher uses to send messages to
and receive messages from the connection.")
   (input-handler
    :type (or Null Socket-Input-Handler)
    :reader input-handler
    :documentation
    "An input-handler for messages to the dispatcher on this connection."))

  (:documentation
   "A Connection is an object used to send messages between Unix processes
via the Dispatcher."))

;;;============================================================

(defclass Dispatcher-Connection (Connection)

  ((dispatcher
    :type Dispatcher
    :reader dispatcher
    :initarg :dispatcher
    :documentation "The dispatcher that owns and uses this connection."))

  (:documentation
   "A Dispatcher-Connection is an object used by an interprocess
message dispatcher to send and receive messages from tools
living in a given unix process."))

;;;============================================================

(defclass Tool-Connection (Tool Connection) 
	  ((dispatcher-host 
	    :type Host-Name 
	    :reader dispatcher-host 
	    :initarg :dispatcher-host 
	    :documentation "The host that the dispatcher is running on.")  
	   (dispatcher-port
	    :type Port 
	    :reader dispatcher-port 
	    :initarg :dispatcher-port
	    :documentation "The dispatcher's connection request port.") 
	   (dispatcher-id
	    :type Tool-Identifier 
	    :reader dispatcher-id 
	    :documentation "The dispatcher's tool-identifier.")
	   (unidentified-tools
	    :type List 
	    :accessor unidentified-tools 
	    :initform ()
	    :documentation "A list of tools waiting for an tool-identifier.")  
	   (tool-identifier-table
	    :type Hash-Table
	    :reader tool-identifier-table
	    :initform (make-hash-table :test #'eql)
	    :documentation "Map tool-identifiers to tool objects."))  
  
  (:documentation 
   "A Tool-Connection object is the interface between CL Tools and the
Message Dispatcher. It represents a particular connection from a Lisp
environment to the Message Dispatcher, which may be running in the
same Lisp environment or in a different one, possibly on a physically
different host.  In fact, the Message Dispatcher need not be
implemented in Lisp."))

;;;============================================================

(defclass Eval-Server (Tool) ()
  (:documentation
   "An Eval-Server handles CL_EVAL_SERVICE_REQUEST and CL_EVAL messages.
An eval message has the form:
  
id sender CL_EVAL Eval-Server lisp-expression

The body of the message is a string of ascii characters that can
be read into a lisp expression with a single call to
<read-from-string>.  The message is handled by calling <eval> on the
expression returned by <read-from-string> in the default environment.
Errors may or may not be reported back to the <sender>.  
If the expression evaluates successfully, 
a response message is sent back to
<sender> with a printed representation of the value returned from
the call to <eval>.

A running Eval-Server will receive copies of any message of the form:

id sender CL_EVAL_SERVICE_REQUEST *

It will respond by sending a response msg back to <sender> whose body
is the header of the CL_EVAL_SERVICE_REQUEST message."))

;;;============================================================

(defclass Eval-Client (Tool)
	  ((eval-server-id
	    :type (or Null Tool-Identifier)
	    :accessor eval-server-id
	    :initform nil
	    :documentation "The Tool-Identifier that is used to address messages 
to the eval dispatcher." ))
  
  (:documentation
   "A Eval-Client sends CL_EVAL messages. An eval message has the form:
  
id sender CL_EVAL Eval-Server lisp-expression

The body of the message is a string of ascii characters that can
be read into a lisp expression with a single call to
<read-from-string>.  The message is handled by calling <eval> on the
expression returned by <read-from-string> in the default environment.
Errors may or may not be reported back to the <sender>.  If the
expression evaluates successfully, a response message is sent back to
the <sender> with a printed representation of the value returned from
the call to <eval.>

A Eval-Client finds a dispatcher by broadcasting a message of the form:

id sender CL_EVAL_SERVICE_REQUEST *

If any Eval-Servers are known to the message dispatcher,
they will respond by  sending a response msg back to <sender> whose body
is the header of the eval service request."))

