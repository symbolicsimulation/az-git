;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot)

;;;============================================================
;;; Event loops for multi-threaded systems
;;;============================================================

(defun x-event-loop (display)
  (declare (optimize (safety 0) (speed 3)))
  (loop (xlib:event-listen display nil)
    (dispatch-events display 1)))

(defun exit-event-loop (value)
  "Exit the tool event loop, stopping msg handling."
  (throw 'exit-event-loop value))

#+:excl
(defun tool-event-loop ()
  (declare (optimize (safety 0) (speed 3)))
  (catch 'exit-event-loop
    (loop
      (mp:process-wait "Waiting for tool msgs." #'any-tool-msgs?)
      (dolist (tool *active-tools*)
	(declare (type Tool tool))
	(when (tool-msgs? tool)
	  (handle-next-message (tool-current-mode tool)
		      (tool-message-queue tool)))))))

;;;============================================================
;;; Event handler for interrupt driven systems
;;;============================================================

(defun event-handler (display)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Display display))
  (catch 'exit-event-loop
    (loop
      (dispatch-events display 1)
      (loop (unless (any-tool-msgs?) (throw 'exit-event-loop t))
	(dolist (tool *active-tools*)
	  (declare (type Tool tool))
	  (when (tool-msgs? tool)
	    (handle-next-message (tool-current-mode tool)
			(tool-message-queue tool)))))))
  t)

;;;============================================================

(defun start-x-event-handling (display)
  (declare (type xlib:Display display))

  #+:excl
  ;; spawn a process to handle tool msgs and clx events
  (let ((process-name (format nil "~a X Event Loop"
			      (xlib:display-host display))))
    (declare (type Simple-String process-name))
    (mp:process-run-function process-name 'x-event-loop display))

  #+:cmu
  (progn
    ;; (re)install X event handler
    (ext:disable-clx-event-handling display)
    (ext:enable-clx-event-handling display #'event-handler)
    #'event-handler))

;;;============================================================

(defvar *display-event-handlers* (make-alist-table :test #'eql)
  "Map xlib:Displays to event handlers.")

(defun host-display (host)

  "Returns an event handler corresponding to the Unix host running the
display X server.  If necessary, starts event handling for this
connection to the host."

  (declare (:returns (type xlib:Display)))
  (let* ((display (xlt:host-default-display :host host))
	 (event-handler (lookup display *display-event-handlers*)))
    (when (null event-handler)
      (setf event-handler (start-x-event-handling display))
      (setf (lookup display *display-event-handlers*) event-handler))
    display))

