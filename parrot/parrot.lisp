;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================

#+:excl ;; use Composer if available
(when (find-package :WT)
  (funcall (intern :composer :wt)))  

;;;============================================================

(declaim (optimize (compilation-speed 0)
		   (debug 3)
		   (safety 3)
		   (space 0)		   
		   (speed 0)))

(export '(*machine*
	  *architecture*
	  *binary-file-type*
	  probe-name
	  unix-make
	  cs))

;;;============================================================

(defparameter *machine*
    #+:sun4 "sun4"
    #+:dec3100 "dec3100")

(defparameter *architecture*
    #+:sparc "sparc"
    #+:mips "mips")

(defparameter *binary-file-type*
    #+genera "bin"
    #+:coral "fasl"
    #+(and :excl :sparc :sun4)    "sfasl"
    #+(and :excl :mips  :dec3100) "dfasl"
    #+(and :cmu  :sparc :sun4)    "sparcf")

;;;============================================================
;;; need to able to <make> foreign code, too.

(defun unix-make (directory)
  (let* ((d (namestring (truename directory)))
	 (makefile-name (concatenate 'String d "Makefile." *machine*))
	 (makefile
	  (namestring (or (probe-file makefile-name)
			  (error "~a not found" makefile-name)))))

    #+:excl
    (excl:run-shell-command (format nil "cd ~a; make -f ~a all;" d makefile))
    #+:cmu
    (let ((current-d (print (namestring (truename ".")))))
      (unwind-protect
	  (progn
	    (unix:unix-chdir d)
	    (ext:run-program "make" (list "-f" makefile "all") :output t))
	(unix:unix-chdir current-d)))))

;;;============================================================
;;; get filenames and directories right

(defparameter *parrot-directory* 
    (namestring 
     (make-pathname :directory (pathname-directory *load-truename*))))
  
;;;============================================================
;;; needed for compiling and loading defsystem

(defun probe-name (&rest names)
  (let ((path (probe-file (apply #'concatenate 'String names))))
    (when path (namestring path))))

(defun compile-if-needed (path-string &key (print t) (verbose t))
  (declare (type String path-string))
  (let ((bin (concatenate 'String path-string "." *binary-file-type*))
        (source (concatenate 'String path-string ".lisp"))
        #+:coral (*warn-if-redefine-kernel* nil))
    #+:coral
    (declare (special *warn-if-redefine-kernel*))
    ;;#-genera (setf bin (truename bin))
    ;;#-genera (setf source (truename source))
    (unless (probe-file source) (error "No file named ~s" source))
    (unless (and (probe-file bin)
		 (< (file-write-date source) (file-write-date bin)))
      (compile-file source
		    :output-file bin
		    :print print
		    :verbose verbose))
    (load bin)))
 
;;; make sure Kantrowitz's defsystem (make) package is compiled and loaded

(eval-when (compile load eval)
  (unless (find-package :Make)
    (compile-if-needed (concatenate 'String *parrot-directory* "defsystem"))))

(defparameter *last-system* nil)

(defun cs (&optional (name *last-system*))
  (setf *last-system* name)
  (format t "~%Compiling the ~a system.~%" name)
  #+:excl (excl:tenuring (mk:compile-system name))
  #+:cmu (with-compilation-unit () (mk:compile-system name)))

;;;============================================================
;;; The system
;;;============================================================

(make:defsystem :Parrot
    :source-pathname #.*parrot-directory*
    :source-extension "lisp"
    :binary-pathname nil
    :binary-extension #.*binary-file-type*
    :initially-do (unix-make #.*parrot-directory*) 
    :components
    ("package"
     "foreign"
     "defs"
     "classes"
     "atomic"
     "kill"
     ;; "queue"
     ;; "table"
     "chunk"
     "socket"
     "tool-identifier"
     "message-identifier"
     "operation-name"
     "operations"
     "message"
     ;; "mode"
     "connection"
     "tool"
     "tool-connection"
     "dispatcher"
     "dispatcher-connection"
     "eval"
     ;;"dispatch"
     ;;"interactor"
     ;;"x-dispatch"
     ;;"initializations"
     ))




