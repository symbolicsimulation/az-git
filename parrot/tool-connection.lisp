;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;------------------------------------------------------------

(defun tool-identifier-tool (tool-identifier connection)
  "Return the tool object associated with <tool-identifier>,
if the tool uses this connection. Otherwise, return nil."
  (declare (type Tool-Identifier tool-identifier)
	   (type Tool-Connection connection)
	   (:returns (type (or Tool Null))))
  (if (tool-identifier= tool-identifier (tool-identifier connection))
    connection
    (gethash tool-identifier (tool-identifier-table connection) nil)))

;;;------------------------------------------------------------

(defun chunk->tool (chunk connection)
  "Convert a Chunk to a Tool, if the chunk can be interpreted
as an tool-identifier for a tool that used <connection>. Otherwise,
return nil."  
  (declare (type Chunk chunk)
	   (type Tool-Connection connection)
	   (:returns (type (or Null Tool))))
  (tool-identifier-tool (chunk->tool-identifier chunk) connection))

(defun tool->chunk (tool)
  "Return a chunk representing the tool-identifier of <tool>."  
  (declare (type Tool tool)
	   (:returns (type Chunk chunk)))
  (tool-identifier->chunk (tool-identifier tool)))

;;;------------------------------------------------------------

(defun make-connection (&key (host (parrot-host)))
  
  "Make a connection object which be used to send 
and receive messages through the Dispatcher that is running on <host> 
and listening for connection requests on <port>. 
The connection will be an instance of <class>, 
which must be a subclass of Tool-Connection and
which defaults to Tool-Connection itself. 
The value of {\tt host} can be specified in the Unix environment variable
{\tt PARROT_HOST} or defaulted to the host running this CL.
This host is only used to find the Parrot dot file,
which contains the host and port number of the Parrot dispatcher,
if on is running."

  (declare (type Host-Name host)
	   (:returns (type Tool-Connection connection)))

  (multiple-value-bind (dispatcher-host dispatcher-port) (read-dot-file host)
    (make-instance 'Tool-Connection
      :dispatcher-host dispatcher-host :dispatcher-port dispatcher-port)))

;;;------------------------------------------------------------

(defparameter *default-connection* nil
  "The default connection to the message dispatcher.")

(defun default-connection ()
  "Returns the default connection to the message dispatcher.
If the default connection hasn't been established or is broken,
then a new connection is made."
  (declare (:returns (type Tool-Connection)))
  (when (or (null *default-connection*)
	    (typep *default-connection* 'Dead-Object)
	    (disconnected? *default-connection*))
    (setf *default-connection* (make-connection)))
  *default-connection*)
 
;;;------------------------------------------------------------

(defmethod kill ((connection Tool-Connection))
  
  "This method for <kill> removes the connection from
all audiences, announces TOOL_KILLED, and calls <disconnected>,
to ensure proper cleanup of the broken connection."
  
  (declare (type Connection connection)
	   (:returns (type Dead-Object connection)))
  
  (leave-audience connection _meta_op_ _meta_tool_ _meta_tool_)
  (send-message _tool_killed_ connection _wild_tool_)
  (disconnected connection)
  (change-class connection 'Dead-Object))

;;;------------------------------------------------------------
;;; Message handling
;;;------------------------------------------------------------

(defgeneric deliver-message (connection message)
  (:documentation
   "This function forwards <message> to its receiver tool, by calling
<handle-message>. The <message> is a list of Chunks returned by
<receive-message>.  The first chunk encodes the tool-identifier of the
receiver tool, which must be a tool known to this connection.  The
first chunk is translated to a tool object, the remaining header
chunks are translated to tool-identifiers, and the argument chunks are left
untouched.")  
  
  (declare (type Tool-Connection connection) 
	   (type List message)))

(defmethod deliver-message ((connection Tool-Connection) message) 

  "This function forwards <message> to its receiver tool, by calling
<handle-message>. The <message> is a list of Chunks returned by
<receive-message>.  The first chunk encodes the tool-identifier of the
receiver tool, which must be a tool known to this connection.  The
first chunk is translated to a tool object, the remaining header
chunks are translated to tool-identifiers, and the argument chunks are left
untouched."  
  
  (declare (type Tool-Connection connection) 
	   (type List message))

  (let ((receiver (chunk->tool (message-receiver-chunk message) connection)))
    (when *debug?* 
      (format t "~&~a deliver-message to ~a~%" connection receiver)
      (print-message message)
      (force-output))    
    (if receiver
      (apply #'handle-message 
	     receiver
	     (message-id message)
	     (message-operation message)
	     (message-sender message)
	     (message-address message)
	     (message-arg-chunks message))
      (warn "~%~a knows no tool with tool-identifier ~a~%"
	    connection (chunk->tool-identifier (first message))))))
 
;;;============================================================
;;; Connecting and Disconnectiong
;;;============================================================

(defun input-handler-f (connection)
  (declare (type Tool-Connection connection))
  #'(lambda (fd)
      (declare (ignore fd))
      (catch :disconnected
	(let* ((message (receive-message connection))
	       (receiver (chunk->tool (message-receiver-chunk message) 
				     connection)))
	  (declare (type List message)
		   (type (or Null Tool) receiver))
	  (when *debug?* 
	    (format t "~&~a deliver-message to ~a~%" connection receiver)
	    (print-message message)
	    (force-output))    
	  (if receiver
	    (apply #'handle-message 
		   receiver
		   (message-id message)
		   (message-operation message)
		   (message-sender message)
		   (message-address message)
		   (message-arg-chunks message))
	    (warn "~%~a knows no tool with tool-identifier ~a~%"
		  connection (chunk->tool-identifier (first message))))))))

(defmethod connect ((connection Tool-Connection))

  "This requests a connection from the Message Dispatcher and handles the
first, acknowledgement message sent by the dispatcher on this connection."

  (declare (type Tool-Connection connection)
	   (:returns (type (Member t nil) success?)))

  (let ((socket (connect-to-port (dispatcher-host connection) 
				 (dispatcher-port connection))))
    (declare (type Fixnum socket))
    (cond #-:cmu 
	  ;; negative socket error is handled in ext:connect-to-inet-socket,
	  ;; so we can't ever get here
	  ((minusp socket)
	   ;; then some problem in connect-to-port
	   (print-unix-error "")
	   (cerror "Error connecting to port ~d on host ~a."
		   (dispatcher-host connection) (dispatcher-port connection))
	   nil)
	  ;; else it worked, finish setting up the connection
	  (t
	   (setf (slot-value connection 'connection-socket) socket)
	   (let ((acknowledgement (receive-message connection)))
	     (declare (type List acknowledgement))
	     (unless (operation-name= _connected_ 
				  (message-operation acknowledgement))
	       (error "First msg to connection ~s not a 'connected msg:~s~%"
		      connection acknowledgement))
	     (setf (slot-value connection 'dispatcher-id)
	       (message-sender acknowledgement))
	     (setf (slot-value connection 'tool-identifier)
	       (message-address acknowledgement))
	     (setf (slot-value connection 'input-handler)
	       (set-socket-input-handler socket (input-handler-f connection))))
	   t))))

;;;------------------------------------------------------------

(defmethod disconnected :before ((connection Tool-Connection))

  "This :before method notifies all the tools that use this connection
that it is disconnected by simulating the broacast of a
DISPATCHER_KILLED message."

  (declare (type Tool-Connection connection))
  
  (maphash #'(lambda (id tool)
	       (declare (type Tool-Identifier id)
			(type Tool tool))
	       (handle-message tool 
			       (make-message-id connection)
			       _dispatcher_killed_
			       (tool-identifier connection)
			       id))
	   (tool-identifier-table connection)))

;;;------------------------------------------------------------

(defun assign-tool-identifier (connection tool)
  "Get a new tool identifier from the dispatcher and assign it to <tool>."
  (declare (type Tool-Connection connection)
	   (type Tool tool))
  (pushnew tool (unidentified-tools connection))
  (send-message _new_tool_identifier_request_ connection 
		(dispatcher-id connection))
  (loop ;; wait until the dispatcher sends back a tool-identifier
    (when (tool-identifier tool) (return))
    (allow-sigio-handling)))

;;;============================================================
;;; Tool protocol:
;;;============================================================

(defmethod connection ((tool Tool-Connection))
  "A Tool-Connection is its own connection to the Dispatcher."
  tool)
 
;;;------------------------------------------------------------

(defun NEW_TOOL_IDENTIFIER (connection id operation sender address 
			    tool-identifier-chunk)
  "This is the handler function for NEW_TOOL_IDENTIFIER messages, 
which are sent by the dispatcher in response to NEW_TOOL_IDENTIFIER_REQUEST 
messages."
  (declare (type Tool-Connection connection)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (type Chunk tool-identifier-chunk)
	   (ignore id))
  (assert (tool-identifier= sender (dispatcher-id connection)))
  (assert (operation-name= operation _new_tool_identifier_))
  (assert (tool-identifier= address (tool-identifier connection)))
  (let ((tool (pop (unidentified-tools connection)))
	(tool-identifier (chunk->tool-identifier tool-identifier-chunk)))
    (when tool
      (setf (slot-value tool 'tool-identifier) tool-identifier)
      (setf (gethash tool-identifier (tool-identifier-table connection))
	tool)
      ;; the connection needs to hear if any of it's tools die
      (join-audience connection _tool_killed_ tool-identifier _wild_tool_))))

;;;------------------------------------------------------------

(defmethod DISPATCHER_KILLED ((connection Tool-Connection)
			      id operation sender address)
  
  "If possible, a DISPATCHER_KILLED message will be sent to every
tool when the dispatcher is killed or discovered to be dead, to permit
tools to react in some reasonable fashion. The default method is simply f
or the tool to commit suicide."
  
  (declare (type Tool-Connection connection)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (ignore id))
  
  ;; some consistency tests
  (assert (tool-identifier= sender (dispatcher-id connection)))
  (assert (operation-name= operation _dispatcher_killed_))
  (assert (or (tool-identifier= address (tool-identifier connection))
	      (tool-identifier= address _wild_tool_)))

  ;; actually calling <kill> would try to send messages
  (disconnected connection)
  (change-class connection 'Dead-Object))

;;;------------------------------------------------------------

(defmethod TOOL_KILLED ((connection Tool-Connection) 
			id operation sender address)
  
  "If this is one of this connection's tools has died, 
do the necessary cleanup."
  
  (declare (type Tool-Connection connection)
	   (type Message-Identifier id)
	   (type Operation-Name operation)
	   (type Tool-Identifier sender address)
	   (ignore id))
  
  ;; some consistency tests
  (assert (operation-name= operation _tool_killed_))
  (assert (or (tool-identifier= address (tool-identifier connection))
	      (tool-identifier= address _wild_tool_)))

  (remhash sender (tool-identifier-table connection)))