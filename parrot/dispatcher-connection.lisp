;;;-*- Package: :Parrot; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Parrot) 

;;;============================================================
;;; Dispatcher side connection objects
;;;============================================================

(defun dispatch-messages (dispatcher)
  "Have the dispatcher process any waiting messages."
  (declare (type Dispatcher dispatcher))
  (maphash 
   #'(lambda (id connection)
       (declare (ignore id))
       (let ((fd (connection-socket connection)))
	 (declare (type (or Null Socket) fd))
	 (unless (null fd)
	   (catch :disconnected
	     (while (readable? fd) 
	       (dispatch-message dispatcher (receive-message connection)))))))
   (id-connections dispatcher)))

(defmethod connect ((connection Dispatcher-Connection))

  "Accept a connection from a client."
  
  (declare (type Dispatcher-Connection connection)
	   (:returns (type Boolean success?)))
  
  (let* ((dispatcher (dispatcher connection))
	 (socket (accept-connection (dispatcher-socket dispatcher)))
	 (handler #'(lambda (fd) 
		      (declare (ignore fd))
		      (dispatch-messages dispatcher))))
    (declare (type Dispatcher dispatcher)
	     (type Fixnum socket))
    (cond
     #-:cmu
     ;; negative socket error is handled in cmu sys code,
     ;; so we can't ever get here.
     ((minusp socket)
      ;; then some problem in accepting the connection
      (atomic
       (close-socket (dispatcher-socket dispatcher))
       (print-unix-error "")
       (cerror "Error accepting connection on dispatcher socket ~s"
	       (dispatcher-socket dispatcher))
       nil))
     ;; else accept-connection worked
     (t
      (let ((id (generate-tool-identifier)))
	(setf (id-connection id dispatcher) connection)
	(add-to-audience dispatcher (tool-identifier dispatcher)
			 _tool_killed_ id _wild_tool_)
	(setf (slot-value connection 'connection-socket) socket)	
	(setf (slot-value connection 'input-handler)
	  (set-socket-input-handler (connection-socket connection) handler))
	(send-message _connected_ dispatcher id))
      (when *debug?*
	(format t "~&~s ~%accepted connection from ~%~s~%" 
		dispatcher connection)
	(describe connection)
	(force-output))
      t))))

;;;------------------------------------------------------------

(defmethod kill :before ((connection Dispatcher-Connection))
  "This :before method ensures that the <connection> is deleted from
the dispatcher's list of active connections."
  (declare (type Dispatcher-Connection connection))

  (let ((id-table (id-connections (dispatcher connection))))
    (declare (type Hash-Table id-table))
    (maphash #'(lambda (id con)
		 (declare (type Tool-Identifier id)
			  (type Dispatcher-Connection con))
		 (when (eq con connection) (remhash id id-table)))
	     id-table))  
  connection)


  
