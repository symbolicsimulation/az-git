;;;-*- Package: :Browser; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Browser)

;;;============================================================

(defclass Pedigree (gra:Graph)
     ((individuals
	:type Sequence
	:accessor individuals
	:initform ())
      (families
	:type Sequence
	:accessor families
	:initform ())
      (kinship-table
	:type Hash-Table
	:accessor kinship-table
	:initform (make-hash-table :test #'eq))
      (ancestor-table
	:type Hash-Table
	:accessor ancestor-table
	:initform (make-hash-table :test #'eq))
      (kinship-order-table
	:type Hash-Table
	:accessor kinship-order-table
	:initform (make-hash-table :test #'eq))
      (kinship-distance-table
	:type Hash-Table
	:accessor kinship-distance-table
	:initform (make-hash-table :test #'eq))))

;;;------------------------------------------------------------

(defun collect-pedigree-edges (p)
  (az:type-check Pedigree p)
  (mapcan #'(lambda (f)
	      (list* (list (father f) f)
		     (list (mother f) f)
		     (map 'List #'(lambda (c) (list f c)) (children f))))
	  (families p)))

;;;============================================================
;;; Pedigree nodes (Individual's and Family's) only make sense with
;;; respect to a particular pedigree. A more natural approach would have
;;; the Nodes independent of any particular pedigree; kinship
;;; calculations, founder? predicates, etc., would have to take a
;;; pedigree as an additional argument.

(defclass Pedigree-Node (az:Arizona-Object)
     ((pedigree
	:type Pedigree
	:accessor pedigree
	:initarg :pedigree)))

;;;============================================================

(defclass Individual (Pedigree-Node)
     ((father
	:type Individual
	:accessor father)
      (mother
	:type Individual
	:accessor mother)
      (birth-family
	:type Family
	:accessor birth-family)
      (families
	:type List
	:accessor families
	:initform ())
      (sex-code ;; (MALE=1, FEMALE=2)
	:type Fixnum
	:accessor sex-code
	:initarg :sex-code)))
  
;;;------------------------------------------------------------

(defmethod male? ((ind Individual))
  (= (sex-code ind) 1))

(defmethod female? ((ind Individual))
  (= (sex-code ind) 2))

(defmethod terminal? ((ind Individual))
  (or (not (slot-boundp ind 'families))
      (null (families ind)))) 

(defmethod founder? ((ind Individual))
  (not (or (slot-boundp ind 'father)
	   (slot-boundp ind 'mother))))

;;;------------------------------------------------------------

(defmethod lay:node-name ((ind Individual))
  (if (male? ind) :m :f))

;;;------------------------------------------------------------

(defmethod get-family ((father Individual)
		       (mother Individual)
		       (pedigree Pedigree))
  (assert (and (eq (pedigree father) (pedigree mother))
	       (eq (pedigree father) pedigree)))
  (let ((fam (find-if #'(lambda (f) (and (eq father (father f))
					 (eq mother (mother f))))
		      (families pedigree))))
    (when (null fam)
      (setf fam (make-instance 'Family
				:father father
				:mother mother
				:pedigree pedigree))
      (push fam (families pedigree)))
    fam))

;;;------------------------------------------------------------

(defmethod compute-ancestor? ((a Individual) (b Individual))
  (unless (founder? b)
    (or (eq a (father b))
	(eq a (mother b))
	(ancestor? a (father b))
	(ancestor? a (mother b)))))

;;; often there are faster methods for <not-ancestor?>

(defmethod not-ancestor? ((a Individual) (b Individual))
  (not (ancestor? a b)))

;;;------------------------------------------------------------
;;; memo-ized!

(defmethod ancestor? ((a Individual) (b Individual))
  (let* ((outer-table (ancestor-table (pedigree a)))
	 (inner-table (az:lookup a outer-table)))
    (when (null inner-table)
      (setf inner-table (make-hash-table :test #'eq))
      (setf (az:lookup a outer-table) inner-table))
    (let ((p (az:lookup b inner-table)))
      (when (null p)
	(setf p (compute-ancestor? a b))
	(setf (az:lookup b inner-table) p))
      p)))

;;;============================================================

(defclass Family (Pedigree-Node)
     ((father
	:type Individual
	:accessor father
	:initarg :father)
      (mother
	:type Individual
	:accessor mother
	:initarg :mother)
      (children
	:type List ;; of Individual's
	:accessor children
	:initform ())))

;;;-----------------------------------------------------------

(defmethod lay::node-name ((f Family)) :x)

;;;============================================================

(defmethod parent? ((ind Individual) (family Family))
  (or (eq ind (father family))
      (eq ind (mother family))))

(defmethod child? ((ind Individual) (family Family))
  (member ind (children family) :test #'eq))

(defmethod family-member? ((ind Individual) (family Family))
  (or (parent? ind family)
      (child? ind family)))

;;;------------------------------------------------------------

(declaim (ftype (function (Pedigree-Node Pedigree-Node) Double-Float)
	  compute-kinship kinship))

(defgeneric compute-kinship (a b)
  #-sbcl (declare (type Pedigree-Node a b))
  (:documentation
   "Compute the kinship between two nodes in a pedigree."))

(defgeneric kinship (a b)
  #-sbcl (declare (type Pedigree-Node a b))
  (:documentation
   "Get the kinship between two nodes in a pedigree,
computing it, if necessary, 
looking it up in a cache, if possible."))

;;;------------------------------------------------------------

(defmethod compute-kinship ((a Individual) (b Individual))
  (declare (optimize (safety 0) (speed 3)))
  ;; assumes (not-ancestor? a b)
  (cond
   ((eq a b)
    (if (founder? a)
	0.5d0
      (* 0.5d0 (+ 1.0d0 (the Double-Float (kinship (father a) (mother a)))))))
   (t 
    (if (founder? a)
	0.0d0
      (* 0.5d0 (+ (the Double-Float (kinship (father a) b))
		  (the Double-Float (kinship (mother a) b))))))))

;;; memo-ized!

(defmethod kinship ((a Individual) (b Individual))
  (declare (optimize (safety 0) (speed 3)))
  (assert (eq (pedigree a) (pedigree b)))
  (unless (not-ancestor? a b) (rotatef a b))
  (let* ((kinship-table (kinship-table (pedigree a)))
	 (inner-table (az:lookup a kinship-table)))
    (declare (type Hash-Table kinship-table))
    (when (null inner-table)
      (setf inner-table (make-hash-table :test #'eq))
      (setf (az:lookup a kinship-table) inner-table))
    (locally (declare (type Hash-Table inner-table))
      (let ((k (az:lookup b inner-table)))
	(when (null k)
	  (setf k (compute-kinship a b))
	  (setf (az:lookup b inner-table) k))
	k))))

;;;------------------------------------------------------------

(declaim (ftype (function (Pedigree-Node Pedigree-Node) Double-Float)
	  compute-kinship-distance))

(defgeneric compute-kinship-distance (a b)
  #-sbcl (declare (type Pedigree-Node a b))
  (:documentation
   "Compute the kinship-distance between two nodes in a pedigree."))

;;;------------------------------------------------------------

(defmethod compute-kinship-distance ((a Individual) (b Individual))
  (declare (optimize (safety 0) (speed 3)))
  (* -2.0d0
     (the Double-Float
       (log
	(the (Double-Float #.least-positive-double-float *)
	  (/ (+ 1.0d-5 (the Double-Float (kinship a b)))
	     1.00001d0))))))

(defmethod compute-kinship-distance ((h Individual) (f Family))
  (declare (optimize (safety 0) (speed 3)))
  (let ((min most-positive-double-float)
	(dist most-positive-double-float)
	(closest nil))
    (declare (type Double-Float min dist))
    (cond ((parent? h f) ;; half the distance to the closest child
	   (dolist (c (children f))
	     (setf dist (kinship-distance h c))
	     (when (< dist min) (setf min dist)))
	   (/ min 3.0d0))
	  ((child? h f) ;; half the distance to the closest parent
	   (/ (min (kinship-distance h (father f))
		   (kinship-distance h (mother f))) 3.0d0))
	  (t ;; not a member of the family;
	   ;; use distance to closest member of family plus the distance
	   ;; from that member to the family node
	   (dolist (c (children f))
	     (setf dist (kinship-distance h c))
	     (when (< dist min) (setf min dist) (setf closest c)))
	   (setf dist (kinship-distance h (father f)))
	   (when (< dist min) (setf min dist) (setf closest (father f)))
	   (setf dist (kinship-distance h (mother f)))
	   (when (< dist min) (setf min dist) (setf closest (mother f)))
	   (+ min (kinship-distance closest f))))))

(defmethod compute-kinship-distance ((f Family) (h Individual))
  (compute-kinship-distance h f))

(defmethod compute-kinship-distance ((f0 Family) (f1 Family))
  (declare (optimize (safety 0) (speed 3)))
  ;; return the distance from f0 to closest member of f1,
  ;; plus the distance from that member to f1
  (let ((min most-positive-double-float)
	(dist most-positive-double-float)
	(closest nil))
    (declare (type Double-float min dist))
    (dolist (c (children f1))
      (setf dist (kinship-distance f0 c))
      (when (< dist min) (setf min dist) (setf closest c)))
    (setf dist (kinship-distance f0 (father f1)))
    (when (< dist min) (setf min dist) (setf closest (father f1)))
    (setf dist (kinship-distance f0 (mother f1)))
    (when (< dist min) (setf min dist) (setf closest (mother f1)))
    (+ min (kinship-distance closest f1))))

;;;------------------------------------------------------------
;;; memo-ized!

(declaim (ftype (function (Pedigree-Node Pedigree-Node) Double-Float)
		kinship-distance))

(defun kinship-distance (a b)

  "Get the kinship-distance between two nodes in a pedigree,
computing it, if necessary, 
looking it up in a cache, if possible."

  (declare (optimize (safety 0) (speed 3)))
  (assert (eq (pedigree a) (pedigree b)))
  (let* ((kinship-distance-table (kinship-distance-table (pedigree a)))
	 (inner-table (az:lookup a kinship-distance-table)))
    (declare (type Hash-Table kinship-distance-table))
    (when (null inner-table)
      (setf inner-table (make-hash-table :test #'eq))
      (setf (az:lookup a kinship-distance-table) inner-table))
    (locally (declare (type Hash-Table inner-table))
      (let ((k (az:lookup b inner-table)))
	(when (null k)
	  (setf k (compute-kinship-distance a b))
	  (setf (az:lookup b inner-table) k))
	k))))

;;;------------------------------------------------------------
;;; a simple distance:
;;; 1.0 for every edge in the pedigree graph,
;;; -1.0 (meaning ignore this edge) for any other pair of nodes

(defmethod family-distance ((a T) (b T)) -1.0d0)

(defmethod family-distance ((a Individual) (b Family))
  (if (family-member? a b) 1.0d0 -1.0d0))

(defmethod family-distance ((b Family) (a Individual))
  (family-distance a b))

;;;------------------------------------------------------------
;;; a partial ordering

(defmethod family-order ((a T) (b T)) 0.0d0)

(defmethod family-order ((a Individual) (b Family))
  (cond ((parent? a b) 1.0d0)
	((child? a b) -1.0d0)
	(t 0.0)))

(defmethod family-order ((a Family) (b Individual))
  (cond ((parent? b a) -1.0d0)
	((child? b a) 1.0d0)
	(t 0.0d0)))

(defmethod family-order? ((a T) (b T)) nil)

(defmethod family-order? ((a Individual) (b Family))
  (parent? a b))

(defmethod family-order? ((a Family) (b Individual))
  (child? b a))

;;;------------------------------------------------------------

(defmethod kinship-order ((a Individual) (b Individual))
  (cond ((ancestor? a b) 1.0d0)
	((ancestor? b a) -1.0d0)
	(t 0.0d0)))

(defmethod kinship-order ((a Family) (b Individual))
  (cond ((or (member b (children a))
	     (find-if #'(lambda (c) (ancestor? c b)) (children a)))
	 1.0d0)
	((or (eql b (father a))
	     (eql b (mother a))
	     (ancestor? b (father a))
	     (ancestor? b (mother a)))
	 -1.0d0)
	(t
	 0.0d0)))

(defmethod kinship-order ((a Individual) (b Family))
  (- (kinship-order b a)))

(defmethod kinship-order ((a Family) (b Family)) 0.0d0)

