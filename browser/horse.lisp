;;;-*- Package: :Browser; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Browser)

;;;============================================================
;;;
;;; Now the Horses:
;;;

#||
From charlie@elk.stat Thu Nov 10 22:45:23 1988
Received: by entropy.ms.washington.edu (5.52.1/6.12)
	id AA25787; Thu, 10 Nov 88 22:45:18 PST
Received: by elk.stat.washington.edu (5.51/6.12)
	id AA05205; Thu, 10 Nov 88 22:45:07 PST
Date: Thu, 10 Nov 88 22:45:07 PST
From: charlie@elk.stat (Charlie Geyer)
Message-Id: <8811110645.AA05205@elk.stat.washington.edu>
To: jam@entropy.ms
Subject: Pedigree Example
Status: R

There is an example pedigree of horses in the file

  /mica/charlie/pedigree.example

on the RT's.  The pedigree has 47 horses, one per line of the
file.  Each line has 10 items of data on the horse.
  (1) name (actually a number, the international studbook number.  The
      horses have names, but not in the file I extracted)

  (2) father's name (same kind of number), NA if the horse is a
      founder

  (3) mother's name (same kind of number), NA if the horse is a
      founder

  (4) sex (MALE=1, FEMALE=2)

  (5) place of birth

  (6) country of birth

  (7) current location (as of sometime last year)

  (8) country of current location

  (9) date of birth (in the form yymmdd, where yy is the last two
      digits of the year, mm the number of the month, and dd is the
      day of the month.  Some of the early dates are approximate, the
      ones with zeros for month and day)

 (10) date of death (same format), NA if still alive.

You can construct the pedigree from knowing the parents of each
horse.  It may help to know that the pedigree is actually
ordered so that each horse follows his parents in the file.  This
helps in building a list with only links back to parents.

The best indicator of relationship is the kinship coefficient defined
as follows:

  k(A,B) = k(B,A)

so it is only necessary to know how to compute k(A,B) if A is not an
ancestor of B.  Assume this.  Then if A is not a founder

  k(A,B) = [ k(p(A),B) + k(m(A),B) ] / 2

if A is not B and

  k(A,A) = [ 1 + k(p(A),m(A)) ] / 2

where p(A) and m(A) denote the parents of A.  And if A is a founder
(recall A is not an ancestor of B), then

  k(A,B) = 0

if A is not B and

  k(A,A) = 1/2

A nice easy recursive function to program.  Note that "A is not an
ancestor of B" is true if A follows B in the file.
||#
;;;============================================================

(defparameter *horse-records*
    (list
     (vector 1 -1 -1 1 :bijsk :ssr :askania-nova :ssr 010000 150929)
     (vector 5 -1 -1 2 :bijsk :ssr :askania-nova :ssr 010000 150906)
     (vector 17 -1 -1 1 :bijsk :ssr :washington :usa 010000 211127)
     (vector 18 -1 -1 2 :bijsk :ssr :cincinnati :usa 010000 350000)
     (vector 39 -1 -1 1 :hagenbeck :brd :new-york :usa 020000 190619)
     (vector 40 -1 -1 2 :hagenbeck :brd :new-york :usa 020000 231129)
     (vector 52 -1 -1 2 :bijsk :ssr :askania-nova :ssr 020000 150515)
     (vector 100 39 40 2 :new-york :usa :new-york :usa 090502 260701)
     (vector 101 39 40 2 :new-york :usa :philadelphia :usa 110910 360131)
     (vector 103 39 40 1 :new-york :usa :sidney :aus 120906 450225)
     (vector 107 39 100 2 :new-york :usa :sidney :aus 160712 330626)
     (vector 113 17 18 1 :cincinnati :usa :philadelphia :usa 120215 430820)
     (vector 118 113 101 1 :philadelphia :usa :washington :usa 230608 500118)
     (vector 119 113 101 2 :philadelphia :usa :washington :usa 260713 590606)
     (vector 121 118 119 2 :washington :usa :munchen :brd 320425 490716)
     (vector 211 -1 -1 1 :woburn :eng :london :eng 010000 331218)
     (vector 212 -1 -1 2 :woburn :eng :london :eng 010000 391025)
     (vector 222 103 107 2 :sidney :aus :sidney :aus 231123 361031)
     (vector 224 103 222 2 :sidney :aus :munchen :brd 290522 510720)
     (vector 421 1 5 1 :askania-nova :ssr :askania-nova :ssr 050525 350114)
     (vector 429 1 52 2 :askania-nova :ssr :askania-nova :ssr 140427 351201)
     (vector 430 1 5 1 :askania-nova :ssr :leningrad :ssr 140510 330400)
     (vector 180 211 212 1 :london :eng :whipsnade :eng 310815 621217)
     (vector 182 180 212 1 :whipsnade :eng :munchen :brd 370509 530706)
     (vector 187 421 429 1 :askania-nova :ssr :schorfheide :ddr 200514 450000)
     (vector 189 430 429 2 :askania-nova :ssr :schorfheide :ddr 260616 450000)
     (vector 198 187 189 2 :berlin-west :ddr :schorfheide :ddr 310713 450000)
     (vector 140 187 198 2 :munchen :brd :munchen :brd 350530 450601)
     (vector 143 187 140 2 :munchen :brd :munchen :brd 380616 490713)
     (vector 149 182 140 2 :munchen :brd :catskill :usa 450427 721011)
     (vector 150 182 143 1 :munchen :brd :catskill :usa 460515 720808)
     (vector 151 182 121 2 :munchen :brd :catskill :usa 460515 640420)
     (vector 154 182 224 1 :munchen :brd :bern :swi 480517 750712)
     (vector 157 182 224 1 :munchen :brd :catskill :usa 500513 720913)
     (vector 163 182 149 2 :munchen :brd :catskill :usa 520711 770516)
     (vector 168 154 151 2 :munchen :brd :catskill :usa 550620 -1)
     (vector 171 154 149 1 :munchen :brd :catskill :usa 560711 750822)
     (vector 228 157 151 2 :catskill :usa :catskill :usa 580404 -1)
     (vector 277 150 151 2 :catskill :usa :catskill :usa 620627 -1)
     (vector 319 150 163 2 :catskill :usa :san-pasqual :usa 640806 810407)
     (vector 320 171 168 1 :catskill :usa :san-diego :usa 640822 770807)
     (vector 341 171 168 1 :catskill :usa :catskill :usa 650831 780911)
     (vector 458 320 319 2 :san-diego :usa :san-pasqual :usa 700210 -1)
     (vector 469 171 228 1 :catskill :usa :marwell :eng 700511 -1)
     (vector 568 341 277 1 :catskill :usa :san-pasqual :usa 730820 -1)
     (vector 961 469 458 2 :san-pasqual :usa :san-pasqual :usa 810505 -1)
     (vector 2100 568 961 2 :san-pasqual :usa :san-pasqual :usa 871201 -1)
     )) 

;;;============================================================

;(defconstant -month-days-alist- ;; $$$$ -jm
(defparameter -month-days-alist-
	     '((0 0 0) (1 31 0) (2 29 31) (3 31 60) (4 30 91) (5 31 121)
	       (6 30 152)
	       (7 31 182) (8 31 213) (9 30 244) (10 31 274) (11 30 305)
	       (12 31 335)))

(defun convert-date (packed-date)
  (declare (type Fixnum packed-date))
  (let* ((year (truncate packed-date 10000))
	 (month (truncate (- packed-date (* year 10000)) 100))
	 (days-so-far (third (assoc month -month-days-alist-)))
	 (month-day (- packed-date (* year 10000) (* month 100)))
	 (year-day (+ days-so-far month-day)))
    (+ year (/ year-day 366.0))))
  
;;;============================================================

(defclass Horse (Individual)
     ((record-number ;; provides a cheap way to test for not-ancestor,
	:type Fixnum ;; since it preserves the ancestry partial ordering.
	:accessor record-number 
	:initarg :record-number)
      (studbook-number
	:type Fixnum
	:accessor studbook-number
	:initarg :studbook-number)
      (birth-place
	:type Symbol
	:accessor birth-place
	:initarg :birth-place)
      (birth-country
	:type Symbol
	:accessor birth-country
	:initarg :birth-country)
      (current-place
	:type Symbol
	:accessor current-place
	:initarg :current-place)
      (current-country
	:type Symbol
	:accessor current-country
	:initarg :current-country)
      (birth-date ;; fractional years since 1900
	:type Fixnum
	:accessor birth-date
	:initarg :birth-date) 
      (death-date ;; fractional years since 1900
	:type Fixnum
	:accessor death-date
	:initarg :death-date)))

;;;============================================================

(defun find-horse (studbook-number pedigree)
  (if (minusp studbook-number)
      nil
      (find-if #'(lambda (horse) (= studbook-number (studbook-number horse)))
	       (individuals pedigree))))

;;;------------------------------------------------------------

;; fast test for <not-ancestor?> based on ordering of records:
(defmethod not-ancestor? ((h0 Horse) (h1 Horse))
    (>= (record-number h0) (record-number h1)))

(defmethod ancestor? ((a Horse) (b Horse))
  (unless (or (founder? b) (not-ancestor? a b))
    (or (eq a (father b))
	(eq a (mother b))
	(ancestor? a (father b))
	(ancestor? a (mother b)))))

;;;------------------------------------------------------------

(defmethod birth-order ((a T) (b T)) 0.0)

(defmethod birth-order ((a Horse) (b Horse))
  (let ((date-a (birth-date a))
	(date-b (birth-date b)))
    (cond
      ((or (minusp date-a)
	   (minusp date-b)
	   (= date-a date-b))
       0.0)
      ((< date-a date-b)
       1.0)
      ((> date-a date-b)
       -1.0))))

;;;------------------------------------------------------------

(defmethod approx-date ((a Horse))
  (if (plusp (death-date a))
      (* 0.5 (+ (birth-date a) (death-date a)))
      (az:fl (birth-date a))))

(defmethod approx-date ((a Family))
  (* 0.5 (+ (approx-date (father a))
	    (approx-date (mother a)))))

;;;============================================================

(defun make-horse-pedigree ()
  (let ((pedigree (make-instance 'pedigree))
	(record-number 0))
    (map nil
	 #'(lambda (hr)
	     (let* ((h (make-instance
			 'Horse
			 :record-number record-number
			 :studbook-number (aref hr 0)
			 :sex-code (aref hr 3)
			 :birth-place (aref hr 4)
			 :birth-country (aref hr 5)
			 :current-place (aref hr 6)
			 :current-country (aref hr 7)
			 :birth-date (convert-date (aref hr 8))
			 :death-date (convert-date (aref hr 9))
			 :pedigree pedigree))
		    (pa (find-horse (aref hr 1) pedigree))
		    (ma (find-horse (aref hr 2) pedigree)))
	       (unless (null pa) (setf (slot-value h 'father) pa))
	       (unless (null ma) (setf (slot-value h 'mother) ma))
	       (push h (individuals pedigree))
	       (incf record-number)))
	 *horse-records*)

    ;; fill in the family info
    (map nil #'(lambda (h)
		 (unless (founder? h)
		   (let ((family (get-family (father h) (mother h) pedigree)))
		     (pushnew h (children family))
		     (setf (birth-family h) family))))
	 (individuals pedigree))
    (map nil #'(lambda (f)
		 (pushnew f (families (father f)))
		 (pushnew f (families (mother f))))
	 (families pedigree))
    (setf (gra:nodes pedigree)
      (concatenate 'List (individuals pedigree) (families pedigree)))
    (setf (gra:edges pedigree) (collect-pedigree-edges pedigree))
    pedigree))



