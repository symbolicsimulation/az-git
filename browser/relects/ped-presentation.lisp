;;;-*- Package: :Browser; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Browser)

;;;============================================================


#||
;;;============================================================
;;; some pens

(defparameter *filling-style* (slate:make-fill-style :fill-style-type :solid))

(defparameter *red-pen* nil)
(defun red-pen ()
  (when (null *red-pen*)
    (setf *red-pen*
      (slate:make-pen
       :pen-operation boole-1
       :pen-font (slate:font :size 8)
       :pen-pixel-value (slate:color-name->pixel-value
			 (slate:screen-colormap (slate:default-screen))
			 :red))))
  *red-pen*)

(defparameter *red-fill-pen* nil)
(defun red-fill-pen ()
  (when (null *red-fill-pen*)
    (setf *red-fill-pen*
      (slate:make-pen
       :pen-operation boole-1
       :pen-fill-style *filling-style*
       :pen-font (slate:font :size 8)
       :pen-pixel-value (slate:color-name->pixel-value
			 (slate:screen-colormap (slate:default-screen))
			 :red))))
  *red-fill-pen*)

(defparameter *green-pen* nil)
(defun green-pen ()
  (when (null *green-pen*)
    (setf *green-pen*
      (slate:make-pen
       :pen-operation boole-1
       :pen-font (slate:font :size 8)
       :pen-pixel-value (slate:color-name->pixel-value
			 (slate:screen-colormap (slate:default-screen))
			 :green))))
  *green-pen*)

(defparameter *green-fill-pen* nil)
(defun green-fill-pen ()
  (when (null *green-fill-pen*)
    (setf *green-fill-pen*
      (slate:make-pen
       :pen-operation boole-1
       :pen-fill-style *filling-style*
       :pen-font (slate:font :size 8)
       :pen-pixel-value (slate:color-name->pixel-value
			 (slate:screen-colormap (slate:default-screen))
			 :green))))
  *green-fill-pen*)


(defparameter *blue-pen* nil)
(defun blue-pen ()
  (when (null *blue-pen*)
    (setf *blue-pen*
      (slate:make-pen
       :pen-operation boole-1
       :pen-font (slate:font :size 8)
       :pen-pixel-value (slate:color-name->pixel-value
			 (slate:screen-colormap (slate:default-screen))
			 :blue))))
  *blue-pen*)

(defparameter *blue-fill-pen* nil)
(defun blue-fill-pen ()
  (when (null *blue-fill-pen*)
    (setf *blue-fill-pen*
      (slate:make-pen
       :pen-operation boole-1
       :pen-fill-style *filling-style*
       :pen-font (slate:font :size 8)
       :pen-pixel-value (slate:color-name->pixel-value
			 (slate:screen-colormap (slate:default-screen))
			 :blue))))
  *blue-fill-pen*)

;;;============================================================

(defun draw-family-glyph (slate x y)
  (slate:draw-rect
   (blue-fill-pen) slate
   (g:make-screen-rect :left x :top y :width 4 :height 5)))

(defun draw-male-glyph (slate x y)
  (slate:draw-rect
   (red-pen) slate
   (g:make-screen-rect :left x :top y :width 10 :height 10)))

(defun draw-female-glyph (slate x y)
  (slate:draw-circle
   (green-pen) slate
   (g:make-screen-point :x x :y y) 5))

(defun draw-male-founder-glyph (slate x y)
  (draw-male-glyph slate x y)
  (slate:draw-rect
   (red-fill-pen) slate
   (g:make-screen-rect :left x :top y :width 10 :height 10)))

(defun draw-female-founder-glyph (slate x y)
  (draw-female-glyph slate x y)
  (slate:draw-circle
   (green-fill-pen) slate
   (g:make-screen-point :x x :y y) 5))

(defun draw-male-terminal-glyph (slate x y)
  (draw-male-glyph slate x y)
  (slate:draw-rect 
   (red-fill-pen) slate
   (g:make-screen-rect :left x :top y :width 10 :height 10)))

(defun draw-female-terminal-glyph (slate x y)
  (draw-female-glyph slate x y)
  (slate:draw-circle
   (green-fill-pen) slate
   (g:make-screen-point :x x :y y) 5))

;;;============================================================

(defclass Individual-Presentation (clay::Node-Presentation) ())

;;;============================================================

(defclass Family-Presentation (clay::Node-Presentation) ())

;;;============================================================

(defclass Pedigree-Presentation (Graph-Presentation) ())

;;;------------------------------------------------------------

(defmethod clay::build-children ((diagram Pedigree-Presentation))
  (let* ((subject (clay::subject diagram))
	 (slate (clay::diagram-slate diagram))
	 (inodes (individuals subject))
	 (fnodes (families subject))
	 (sedges (gr:edges subject))
	 (sn-table (clay::subject-node-table diagram))
	 (se-table (clay::subject-edge-table diagram)))
    ;; make presentations for the nodes in the pedigree
    (setf (gr:nodes diagram)
      (az:with-collection
       (dolist (inode inodes)
	 (let ((gnp (allocate-instance (find-class 'Individual-Presentation))))
	   (clay::build-diagram-local gnp (list :subject inode
						:slate slate
						:parent diagram))
	   (setf (gethash inode sn-table) gnp)
	   (az:collect gnp)))
       (dolist (fnode fnodes)
	 (let ((gnp (allocate-instance (find-class 'Family-Presentation))))
	   (clay::build-diagram-local gnp (list :subject fnode
						:slate slate
						:parent diagram))
	   (setf (gethash fnode sn-table) gnp)
	   (az:collect gnp)))))
    (clay::graph-presentation-index-nodes diagram)

    ;; make presentations for the edges in the graph
    ;; and recreate the graph structure
    ;; in the presentation nodes and edges
    (setf (gr:edges diagram)
      (az:with-collection
       (dolist (sedge sedges)
	 (let ((gep (allocate-instance
		     (find-class 'Edge-Presentation)))
	       (gnp0 (gethash (gr:edge-node0 sedge) sn-table))
	       (gnp1 (gethash (gr:edge-node1 sedge) sn-table)))
	   (clay::build-diagram-local gep (list :subject sedge
						:slate slate
						:parent diagram))
	   (setf (gethash sedge se-table) gep)
	   (push gep (clay::from-edges gnp0))
	   (push gep (clay::to-edges gnp1))
	   (setf (gr:edge-node0 gep) gnp0)
	   (setf (gr:edge-node1 gep) gnp1)
	   (az:collect gep)))))
    (clay::graph-presentation-index-edges diagram)
    ))
||#