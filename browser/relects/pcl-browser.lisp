;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
 
;;;============================================================
;;; Class-Graph
;;;============================================================
;;; The nodes in a Class-Graph are the classes themselves.

(defmethod node-name ((cl Class))
  (class-name cl))

;;;============================================================
;;; The edges in a Class-Graph are instances of Subclass-Pair.

(defclass Subclass-Pair (az:Arizona-Object)
     ((parent-class
	:type Class
	:accessor parent-class
	:accessor gr:edge-node0
	:initarg :parent-class)
      (child-class
	:type Class
	:accessor child-class
	:accessor gr:edge-node1
	:initarg :child-class)))

;;;------------------------------------------------------------

(defun make-subclass-pair (parent child)
  (let ((pair (allocate-instance (find-class 'Subclass-Pair))))
    (setf (parent-class pair) parent)
    (setf (child-class pair) child)
    pair))

;;;============================================================
;;; The purpose of having Class-Graph objects is to enable us
;;; to focus our attention on a subset of the inheritance relations
;;; in a set of classes.

(defclass Class-Graph (Dag) ())

;;;------------------------------------------------------------

(defun make-class-graph (classes)
  (let ((class-graph (allocate-instance (find-class 'Class-Graph)))
	(edges ()))
    (setf (gr:nodes class-graph) classes)
    (dolist (class0 classes)
      (dolist (class1 classes)
	(when (member class1 (pcl::class-direct-subclasses class0))
	  (push (make-subclass-pair class0 class1) edges))))
    (setf (gr:edges class-graph) edges)
    class-graph))

;;;============================================================

(defclass Class-Presentation (Node-Presentation) ())

;;;------------------------------------------------------------

(defmethod make-presentation-for ((class Class))
  (let ((p (allocate-instance (find-class 'Class-Presentation))))
    (setf (subject p) class)
    p))

;;;------------------------------------------------------------

(defmethod build-diagram-local ((diagram Class-Presentation) build-options)

  build-options

  ;; slots inherited from Diagram
  (setf (slot-value diagram 'redraw-timestamp) (az:make-timestamp))
  (setf (slate-rect diagram) (g:make-screen-rect))
  (setf (clipping-region diagram) nil)
  (setf (lit? diagram) nil)

  ;; slots inherited from Diagram-Leaf
  (setf (slot-value diagram 'diagram-pen) (subject-pen (subject diagram))) 

  ;; local slots
  (setf (name-string diagram)
    (string-capitalize (string (class-name (subject diagram))))))

;;;============================================================

(defclass Subclass-Pair-Presentation (Dedge-Presentation) ())

;;;------------------------------------------------------------

(defmethod make-presentation-for ((subclass-pair Subclass-Pair))
  (let ((p (allocate-instance (find-class 'Subclass-Pair-Presentation))))
    (setf (subject p) subclass-pair)
    p))

;;;============================================================

(defclass Class-Graph-Presentation (Dag-Presentation) ())

;;;============================================================
;;; Generic-Function-Graph
;;;============================================================
;;; The nodes in a Generic-Function-Graph are classes and methods.

(defmethod node-name ((method Standard-Method))
  (with-output-to-string (stream)
    (let ((generic-function (pcl::method-generic-function method)))
      (format stream "(~a~{ ~a~}~{ ~a~})"
	      (pcl::generic-function-name generic-function) 
	      (pcl::method-qualifiers method)
	      (pcl::unparse-specializers method))) ))

;;;============================================================
;;; The edges in a Generic-Function-Graph are instances of
;;; Class-Method-Pair.

(defclass Class-Method-Pair (az:Arizona-Object)
     ((class
	:type Class
	:accessor class
	:accessor gr:edge-node0
	:initarg :class)
      (method
	:type Standard-Method
	:accessor method
	:accessor gr:edge-node1
	:initarg :method)))

;;;------------------------------------------------------------

(defun make-class-method-pair (class method)
  (let ((pair (allocate-instance (find-class 'Class-Method-Pair))))
    (setf (class pair) class)
    (setf (method pair) method)
    pair))

;;;============================================================
;;; The purpose of having Generic-Function-Graph objects is to enable us
;;; to focus our attention on a subset of the inheritance and
;;; specialization relations in a set of classes and methods.

(defclass Generic-Function-Graph (Dag) ())

;;;------------------------------------------------------------

(defun make-generic-function-graph (gf)
  (let* ((dag (allocate-instance (find-class 'Generic-Function-Graph)))
	 (edges ())
	 (methods (copy-list (pcl::generic-function-methods gf)))
	 (specializing-classes
	   (delete-duplicates
	     (mapcan #'(lambda (method)
			 (copy-list
			   (pcl::method-specializers method)))
		     methods)))
	 (classes
	   (delete-duplicates
	     (mapcon #'(lambda (remainder)
			 (let ((c0 (first remainder)))
			   (delete-duplicates
			     (mapcan #'(lambda (c1)
					 (all-classes-between c0 c1))
				     (rest remainder)))))
		     specializing-classes))))
    
    (dolist (class0 classes)
      (dolist (class1 classes)
	(when (member class1 (pcl::class-direct-subclasses class0))
	  (push (make-subclass-pair class0 class1) edges)))
      (dolist (method methods)
	(when (member class0 (pcl::method-specializers method))
	  (push (make-class-method-pair class0 method) edges))))
    (setf (gr:nodes dag) (concatenate 'list classes methods))
    (setf (gr:edges dag) edges)
    dag))

;;;============================================================

(defclass Method-Presentation (Node-Presentation)
     ((strings
	:type List
	:accessor strings)
      (pens
	:type List
	:accessor pens)))

;;;------------------------------------------------------------

(defmethod make-presentation-for ((method Standard-Method))
  (let ((p (allocate-instance (find-class 'Method-Presentation))))
    (setf (subject p) method)
    p)) 

;;;------------------------------------------------------------

(defmethod build-diagram-local ((diagram Method-Presentation) build-options)
  build-options

  ;; slots inherited from Diagram
  (setf (slot-value diagram 'redraw-timestamp)
    (az:make-timestamp))
  (setf (slate-rect diagram) (g:make-screen-rect))
  (setf (clipping-region diagram) nil)
  (setf (lit? diagram) nil)

  ;; slots inherited from Presentaion
  (setf (slot-value diagram 'diagram-pen) (subject-pen (subject diagram))) 

  ;; local slots
  (setf (name-string diagram)
	(string-capitalize (string (class-name (subject diagram)))))
  (let ((m (subject diagram)))
    (setf (slot-value diagram 'diagram-pen) (subject-pen m))
    (setf (strings diagram)
	  (list (string-downcase
		  (pcl::generic-function-name
		    (pcl::method-generic-function m)))))
    (setf (pens diagram)
	  (list (diagram-pen diagram)))
    (mapc #'(lambda (q)
	      (push (string-downcase q) (strings diagram))
	      (push (small-font-pen) (pens diagram)))
	  (pcl::method-qualifiers m))
    (mapc #'(lambda (c)
	      (push (string-capitalize (class-name c)) (strings diagram))
	      (push (subject-pen c) (pens diagram)))
	  (pcl::method-specializers m)))
  (setf (strings diagram) (nreverse (strings diagram)))
  (setf (pens diagram) (nreverse (pens diagram)))
  diagram)

(defmethod layout-diagram ((p Method-Presentation) layout-options)
  (declare (ignore layout-options))
  (let* ((pens (pens p))
	 (strings (strings p))
	 (slate (diagram-slate p))
	 (widths (mapcar #'(lambda (pen str)
			     (slate:string-width pen slate str))
			 pens strings))
	 (heights (mapcar #'(lambda (pen str)
			      (slate:string-height pen slate str))
			  pens strings))
	 (descents (mapcar #'(lambda (pen str)
			       (slate:string-descent pen slate str))
			   pens strings))
	 (margin 4)
	 (width (+ margin (apply #'max widths)))
	 (height (+ margin (apply #'+ heights)))
	 (invisible-slate (slate:make-invisible-slate
			    :screen (slate:slate-screen slate)
			    :width width :height height)))
    (setf (invisible-slate p) invisible-slate)
    (g:with-borrowed-screen-rect (r :width width :height height)
      (slate:draw-rect (diagram-pen p) invisible-slate r))
    (g:with-borrowed-screen-point (screen-point :x 2 :y (- height margin))
      (mapc #'(lambda (pen s h d)
		(incf (g:screen-point-y screen-point) (- d h))
		(slate:draw-string pen invisible-slate s screen-point))
	    pens strings heights descents))
    (setf (allowed-rect p)
	  (possible-point-rect-xy
	    width height margin (slate:slate-rect slate)
	    :result (allowed-rect p)))
    ;; random initialization
    (setf (slate-rect p) (random-screen-rect-in-rect
			    (allowed-rect p)
			    :result (slate-rect p)))))

;;;============================================================

(defclass Class-Method-Pair-Presentation (Dedge-Presentation) ())

(defmethod make-presentation-for ((class-method-pair Class-Method-Pair))
  (let ((p (allocate-instance (find-class 'Class-Method-Pair-Presentation))))
    (setf (subject p) class-method-pair)
    p)) 

;;;============================================================

(defclass Generic-Function-Presentation (Dag-Presentation) ())

;;;------------------------------------------------------------

(defmethod build-children ((gp Generic-Function-Presentation))
  (let* ((subject (subject gp))
	 (snodes (gr:nodes subject))
	 (sedges (gr:edges subject))
	 (pnodes ())
	 (pedges ()))
    ;; make presentations for the nodes in the graph
    (dolist (snode snodes)
      (let ((gnp (make-presentation-for snode)))
	(setf (parent gnp) gp)
	(push gnp pnodes)))
    (setf (gr:nodes gp) (nreverse pnodes))

    ;; make presentations for the edges in the graph
    (dolist (sedge sedges)
      (let ((gep (make-presentation-for sedge)))
	(setf (parent gep) gp)
	(push gep pedges)))
    (setf (gr:edges gp) (nreverse pedges))

    ;; recreate the graph structure in the presentation nodes and edges
    (dolist (gep (gr:edges gp))
      (let* ((sedge (subject gep))
	     (snode0 (gr:edge-node0 sedge))
	     (gnp0 (find-presentation-of snode0 gp))
	     (snode1 (gr:edge-node1 sedge))
	     (gnp1 (find-presentation-of snode1 gp)))
	(push gep (from-edges gnp0))
	(push gep (to-edges gnp1))
	(setf (gr:edge-node0 gep) gnp0)
	(setf (gr:edge-node1 gep) gnp1)))))

;;;------------------------------------------------------------

(defun make-generic-function-presentation (Generic-Function slate)
  (let ((gp (allocate-instance
	      (find-class 'Generic-Function-Presentation)))
	(screen-rect (slate:slate-rect slate)))
    (setf (diagram-slate gp) slate)
    (setf (subject gp) Generic-Function)
    (g:copy-screen-rect screen-rect :result (slate-rect gp))
    (build-diagram gp nil)
    (layout-diagram gp nil)
    (slate:clear-slate (diagram-slate gp))
    (draw-diagram gp)
    gp)) 

