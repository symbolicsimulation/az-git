;;;-*- Package: :Browser; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Browser) 

;;;============================================================
;;; examiner-references:
;;;============================================================

(defgeneric examiner-references (object)
  (:documentation
   "A generic function for navigating among related objects."))


(defmethod examiner-references ((object T))
  "Default is no references."
  (declare (:returns ()))
  ())

(defmethod examiner-references ((object Null))
  "Nil has no references."
  (declare (:returns ()))
  ())

(defmethod examiner-references ((object Symbol))

  "For now, Symbols just return the value and function.  We might
later add package and plist items."
  (declare (:returns (type List)))

  (let ((refs ()))
    (when (boundp object)
      (pushnew (symbol-value object) refs :test #'eql))
    (when (fboundp object)
      (pushnew (symbol-function object) refs) :test #'eql)
    refs))


(defmethod examiner-references ((object Sequence))
  "Make a list of all the unique objects in the sequence."
  (declare (:returns (type List)))
  (delete-duplicates (concatenate 'List object)))

(defmethod examiner-references ((object String))
  "An examiner does not want to look at characters."
  (declare (:returns ()))
  ())

(defmethod examiner-references ((object Array))
  "Examiners skip references in Arrays for the present."
  (declare (:returns ()))
  ())

(defmethod examiner-references ((object Standard-Object))

  "Examiners look at all the references in the <az:interesting-slots>
of a CLOS object."

  (declare (:returns (type List)))
  (delete-duplicates (az:interesting-slot-values object)))

;;;============================================================
;;; examiner-reference?
;;;============================================================

(defgeneric examiner-reference? (object0 object1)
  (:documentation
   "Tests to see if object0 contains a reference to object1.
It's a generic function to allow for more efficient (less cons-y)
specializations of the default method."))

(defmethod examiner-reference? ((object0 T) (object1 T))
  "The default method tests if <object1> is a member of the examiner
references of <object1>."
  (member object1 (examiner-references object0) :test #'eql))

(defmethod examiner-reference? ((object0 Null) (object1 T))
  "Nil contains no references."
  (declare (:returns nil))
  nil)

(defmethod examiner-reference? ((object0 T) (object1 Null))
  "All references to Nil are ignored."
  (declare (:returns nil))
  nil)

(defmethod examiner-reference? ((object0 Number) (object1 T))
  "Numbers contain no references."
  (declare (:returns nil))
  nil)

(defmethod examiner-reference? ((object0 T) (object1 Number))
  "All references to Numbers are ignored."
  (declare (:returns nil))
  nil)

(defmethod examiner-reference? ((object0 Symbol) (object1 T))
  "Value and function references are considered; other references are
ignored."
  (or (and (boundp object0)
	   (eql (symbol-value object0) object1))
      (and (fboundp object0)
	   (eql (symbol-function object0) object1))))

(defmethod examiner-reference? ((object0 Sequence) (object1 T))
  "Is <object1> an element of the sequence <object0>?"
  (find object1 object0 :test #'eql))

;;; I don't think I want to look at characters
(defmethod examiner-reference? ((object0 String) (object1 T))
  "Strings are considered to contain no interesting references."
  (declare (:returns nil))
  nil)

(defmethod examiner-reference? ((object0 Standard-Object) (object1 T))
  (member object1 (az::interesting-slot-values object0) :test #'eql))

;;;============================================================
;;; Examiner-Graph's
;;;============================================================

(defclass Examiner-Graph (gra:Digraph) ()
  (:documentation
   "The nodes in an Examiner-Graph are random Lisp objects.
The edges represent references to a Lisp object that are
contained in another Lisp object."))

;;;------------------------------------------------------------

(defmethod gra:directed-graph? ((g Examiner-Graph))
  "Examiner-Graphs are directed."
  (declare (:returns t))
  t)

;;;------------------------------------------------------------

(defun make-examiner-graph (objects)

  "Makes an examiner graph for the list <objects>. The edges in the
graph are gotten by calculating which of the objects have
<examiner-references> to other objects in the list."

  (declare (type List objects)
	   (:returns (type Examiner-Graph)))
  (let ((graph (make-instance 'Examiner-Graph))
	(edges ()))
    (setf (gra:nodes graph) (copy-list objects))
    (dolist (object0 objects)
      (dolist (object1 objects)
	(when (member object1 (examiner-references object0))
	  (push (list object0 object1) edges))))
    (setf (gra:edges graph) edges)
    graph))

;;;============================================================

(defun add-examiner-references (node graph)

  "Adds <node> to <graph>, adding edges for any examiner reference
between <node> and the nodes already in the graph."

  (declare (type T node)
	   (type Examiner-Graph graph)
	   (:returns graph))
  (let* ((old (gra:nodes graph))
	 (new (set-difference (examiner-references node) old))
	 (new-edges
	  (az:with-collection
	   (dolist (new0 new)
	     (dolist (new1 new)
	       (when (examiner-reference? new1 new0)
		 (az:collect (list new1 new0)))
	       (when (examiner-reference? new0 new1)
		 (az:collect (list new0 new1))))
	     (dolist (old-object old)
	       (when (examiner-reference? old-object new0)
		 (az:collect (list old-object new0)))
	       (when (examiner-reference? new0 old-object)
		 (az:collect (list new0 old-object))))))))
    (declare (type List old new new-edges))
    (unless (null new) (gra:add-nodes-and-edges new new-edges graph))))

;;;------------------------------------------------------------

(defmethod lay:graph-node-menu-items append ((graph Examiner-Graph) (node T))
  "A menu for nodes in an examiner graph."
  (declare (:returns (type List menu-spec)))
  (when (member node (gra:nodes graph))
    (list (list (format nil "Add examiner references from ~a" node)
		#'add-examiner-references node graph))))

;;;============================================================

(defun make-examiner (objects &key (host ""))
  "Creates an examiner for <objects>."
  (declare (type List objects)
	   (:returns (type lay:Graph-Diagram)))
  (lay:make-graph-diagram :subject (make-examiner-graph objects)
			  :host host))

