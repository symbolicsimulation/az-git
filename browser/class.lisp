;;;-*- Package: :Browser; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Browser) 

;;;============================================================
;;; Class-Graph
;;;============================================================
;;;

(defclass Class-Graph (gra:Dag) ()
  (:documentation
   "The purpose of having Class-Graph objects is to enable us to focus
our attention on a subset of the inheritance relations in a set of
classes.  The nodes in a Class-Graph are the classes themselves.  The
edges represent direct-subclass relations."))

;;;------------------------------------------------------------

(defun make-class-graph (classes)
  "Build the class graph from a list of classes by making edges
for every direct subclass --- direct superclass pair."
  (declare (type List classes)
	   (:returns (type Class-Graph)))
  (let ((class-graph (make-instance 'Class-Graph))
	(edges ()))
    (setf (gra:nodes class-graph) classes)
    (dolist (class0 classes)
      (dolist (class1 classes)
	(when (member class1 (class-direct-subclasses class0))
	  (push (list class0 class1) edges))))
    (setf (gra:edges class-graph) edges)
    class-graph))

;;;------------------------------------------------------------

(defun direct-subclass? (class0 class1)
  (declare (type Class class0 class1))
  (member class0 (class-direct-subclasses class1)))

(defun direct-superclass? (class0 class1)
  (declare (type Class class0 class1))
  (member class0 (class-direct-superclasses class1)))

;;;============================================================
;;; Interactive operations on class graphs
;;;============================================================

(defun add-subclass-nodes (class class-graph)

  "Add nodes and corresponding edges for all the direct subclasses of
<class> to <class-graph>."

  (declare (type Class class)
	   (type Class-Graph class-graph)
	   (:returns class-graph))

  (let* ((old-nodes (gra:nodes class-graph))
	 (new-nodes (set-difference (class-direct-subclasses class) old-nodes))
	 (new-edges
	  (az:with-collection
	   (dolist (class0 new-nodes)
	     (dolist (class1 new-nodes)
	       (cond ((direct-superclass? class0 class1)
		      (az:collect (list class0 class1)))
		     ((direct-superclass? class1 class0)
		      (az:collect (list class1 class0)))))
	     (dolist (class1 old-nodes)
	       (cond ((direct-superclass? class0 class1)
		      (az:collect (list class0 class1)))
		     ((direct-superclass? class1 class0)
		      (az:collect (list class1 class0)))))))))
    (declare (type List old-nodes new-nodes new-edges))
    (unless (null new-nodes)
      (gra:add-nodes-and-edges new-nodes new-edges class-graph))))

(defun add-superclass-nodes (class class-graph)
 "Add nodes and corresponding edges for all the direct superclasses of
<class> to <class-graph>."

  (declare (type Class class)
	   (type Class-Graph class-graph)
	   (:returns class-graph)) 

  (let* ((old-nodes (gra:nodes class-graph))
	 (new-nodes (set-difference (class-direct-superclasses class)
				    old-nodes))
	 (new-edges
	  (az:with-collection
	   (dolist (class0 new-nodes)
	     (dolist (class1 new-nodes)
	       (cond ((direct-superclass? class0 class1)
		      (az:collect (list class0 class1)))
		     ((direct-superclass? class1 class0)
		      (az:collect (list class1 class0)))))
	     (dolist (class1 old-nodes)
	       (cond ((direct-superclass? class0 class1)
		      (az:collect (list class0 class1)))
		     ((direct-superclass? class1 class0)
		      (az:collect (list class1 class0)))))))))
    (declare (type List old-nodes new-nodes new-edges))
    (unless (null new-nodes)
      (gra:add-nodes-and-edges new-nodes new-edges class-graph))))

;;;------------------------------------------------------------

(defmethod lay:graph-node-menu-items append ((graph Class-Graph)
					      (node Class))
  "A menu for classes."
  (list (list (format nil "Add direct subclasses of ~a" node)
	      #'add-subclass-nodes node graph)
	(list (format nil "Add direct superclasses of ~a" node)
	      #'add-superclass-nodes node graph)))

;;;============================================================

(defun make-class-browser (classes
			   &key
			   (host (xlt:default-host))
			   (display (ac:host-display host))
			   (screen (xlib:display-default-screen display))
			   (left 0)
			   (top 0)
			   (width 256)
			   (height 256)
			   (x-order-f 'direct-superclass?)
			   (y-order-f nil)
			   (rest-length nil))
  
  "Make a graph browser for a set of classes."
  
  (declare (type List classes)
	   (:returns (type lay:graph-Diagram)))
  
  (lay:make-graph-diagram :screen screen
			   :subject (make-class-graph classes)
			   :name (gensym "Class Browser ")
			   :left left :top top :width width :height height
			   :x-order-f x-order-f
			   :y-order-f y-order-f
			   :rest-length rest-length))