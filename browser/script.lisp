;;; -*- Package: :Browser; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Browser) 

(setf *host* "fram")

(progn
(defparameter *spaces* (make-class-browser
			(cons (find-class 'g:Flat-Space)
			      (az:all-subclasses 'g:Flat-Space))
			:width 800 :height 600 :host *host*))

(defparameter *g* (br:make-class-browser
		   (delete-if #'(lambda (c) (subtypep c 'g:Flat-Map))
			      (cons
			       (find-class 'g:Geometric-Object)
			       (az:all-subclasses 'g:Geometric-Object)))
		   :width 740 :height 450 :left 400 :top 400  :host *host*))

(defparameter *maps* (br:make-class-browser
		     (cons (find-class 'g:Flat-Map)
			   (az:all-subclasses 'g:flat-Map))
		     :width 740 :height 450 :left 400 :top 400
		     :host *host*))
)

(xlt:ps-dump (clay:diagram-window *spaces*)
	     "/belgica-4g/jam/az/geometry/doc/spaces.ps"
	     :scale 1)

(xlt:ps-dump (clay:diagram-window *g*)
	     "/belgica-4g/jam/az/geometry/doc/geometric-objects.ps"
	     :scale 1)

(xlt:ps-dump (clay:diagram-window *maps)*
	     "/belgica-4g/jam/az/geometry/doc/maps.ps"
	     :scale 1)


(defparameter *g* (make-class-browser
		   (az:all-subclasses 'g::Geometric-Object)
		   :width 1200 :height 900 :host *host*))

(defparameter *diagrams* (make-class-browser
			  (az:all-subclasses 'clay:Diagram)
			  :width 900 :height 700 :left 100 :top 100))

(defparameter *diagrams* (make-class-browser
			  (cons (find-class 'clay:Diagram)
				(az:all-subclasses 'clay:Diagram))
			  :width 900 :height 700 :left 100 :top 100))

(defparameter *clay* (make-class-browser
		      (az:all-subclasses 'clay:Clay-Object)
		      :width 900 :height 700 :left 100 :top 100))

(defparameter *actors* (make-class-browser
			(cons (find-class 'ac:Actor)
			      (az:all-subclasses 'ac:Actor))
			:width 900 :height 700 :left 100 :top 100))

(defparameter *roles* (make-class-browser
		       (cons (find-class 'ac:Role)
			     (az:all-subclasses 'ac:Role))
		       :width 640 :height 480 :left 500 :top 300))

(defparameter *actors-roles* (make-class-browser
			      (concatenate 'List
				(cons (find-class 'ac:Actor)
				      (az:all-subclasses 'ac:Actor))
				(cons (find-class 'ac:Role)
				      (az:all-subclasses 'ac:Role)))
			      :width 1000 :height 700))

(defparameter *mus* (make-class-browser
		     (az:all-subclasses 'mu::Mu-Object)
		     :width 640 :height 480 :left 500 :top 300))

(defparameter *az* (make-class-browser
		     (az:all-subclasses 'az:Arizona-Object)
		     :width 1000 :height 800))

(defparameter *conditions* (az:all-subclasses 'Condition))
(make-class-browser *conditions*
		    :width 640 :height 480 :left 500 :top 300)


(defparameter *points* (br:make-class-browser
			(delete-if #'(lambda (c) (subtypep c 'ca::Mapping))
				   (cons
				    (find-class 'ca::Point)
				    (az:all-subclasses 'ca::Point)))
			:width 740 :height 450 :left 400 :top 400))

(xlt:ps-dump-window (clay:diagram-window *points*)
		     "/belgica-4g/jam/az/cactus/doc/points.ps"
		     :scale 1)

(defparameter *cactus* (make-class-browser
			 (az:all-subclasses 'ca:Cactus-Object)
			 :width 1000 :height 900))

(defparameter *spaces* (make-class-browser
			(cons (find-class 'ca:Space)
			      (az:all-subclasses 'ca:Space))
			:width 740 :height 450 :left 400 :top 400))





(time
 (defparameter *cactus0*
     (make-class-browser
      (cons (find-class 'ca:Point)
	    (az:all-subclasses 'ca:Point)))))

(time
 (defparameter *mappings*
     (make-class-browser
      (cons (find-class 'ca::Mapping)
	    (az:all-subclasses 'ca::Mapping))
      :x-order-f nil :y-order-f 'direct-superclass?
      :width 740 :height 450 :left 400 :top 400)))


(time
 (defparameter *v-maps*
     (make-class-browser
      (cons (find-class 'ca::Mapping)
	    (az:all-subclasses 'ca::Mapping)))))
(xlt:ps-dump-window (clay:diagram-window *mappings*)
		     "/belgica-4g/jam/az/cactus/doc/mappings.ps"
		     :scale 1)

(time
 (defparameter *linear-maps*
     (make-class-browser
      (cons (find-class 'ca::Linear-Map)
	    (az:all-subclasses 'ca::Linear-Map))
      :width 740 :height 450 :left 400 :top 400)))

(xlt:ps-dump-window (clay:diagram-window *linear-maps*)
		     "/belgica-4g/jam/az/cactus/doc/linear-maps.ps"
		     :scale 1)

(defparameter *optimizers*
     (make-class-browser
      (cons (find-class 'ca:Optimizer)
	    (az:all-subclasses 'ca:Optimizer))
      width 740 :height 450 :left 400 :top 400))

(xlt:ps-dump-window (clay:diagram-window *optimizers*)
		     "/belgica-4g/jam/az/cactus/doc/optimizers.ps")


(time
 (defparameter *cb2*
     (make-class-browser
      (cons (find-class 'Definition:Definition)
	    (az:all-subclasses 'Definition:Definition))
      )))


(time
 (defparameter *cb1*
     (make-class-browser
      (az:all-subclasses 'az:Arizona-Object)
      :width 820 :height 732 :left 320 :top 150)))

(time
 (defparameter *diagram-classes*
     (make-class-browser
      (cons (find-class 'clay:Diagram)
	    (az:all-subclasses 'clay:Diagram))
      :host *host*
      :width 740 :height 450 :left 400 :top 400)))
(xlt:ps-dump (clay:diagram-window *diagram-classes*)
	     "/belgica-4g/jam/az/browser/doc/diagram-classes.ps"
	     :scale 3)

(time (defparameter *diagram-examiner*
	  (make-examiner  *diagram-classes*
			 :host *host*)))
(xlt:ps-dump (clay:diagram-window *diagram-examiner*)
	     "/belgica-4g/jam/az/browser/doc/diagram-examiner.ps"
	     :scale 3)

;;;------------------------------------------------------------
;;; examiner example

(time (defparameter *ex* (make-examiner (list *cb0*))))

;;;------------------------------------------------------------
;;; pedigree example

(time (defparameter *horses* (make-horse-pedigree)))

(time (defparameter *horsep0*
	  (lay:make-graph-diagram
	   :subject *horses*
	   :y-order-f 'family-order?
	   :node-pair-distance-f 'kinship-distance
	   :host *host*)))

(time (defparameter *horsep1*
	  (lay:make-graph-diagram
	   :subject *horses*
	   :y-order-f 'family-order?
	   :host *host*)))

(time (defparameter *horsep2*
	  (lay:make-graph-diagram :subject *horses*
	   :host *host*)))

(progn
  (xlt:ps-dump (clay:diagram-window *horsep0*)
	       "/belgica-4g/jam/az/browser/doc/horsep0.ps"
	       :scale 2)
  (xlt:ps-dump (clay:diagram-window *horsep1*)
	       "/belgica-4g/jam/az/browser/doc/horsep1.ps"
	       :scale 2)
  (xlt:ps-dump (clay:diagram-window *horsep2*)
	       "/belgica-4g/jam/az/browser/doc/horsep2.ps"
	       :scale 2)
  )
(time
 (progn
   ;;(prof:start-profiler :space)
   (clay::layout-diagram *horsep0* nil)
   ;;(prof:stop-profiler :space)
   ))
(prof:show-call-graph)


(setf *solver* (clay::diagram-layout-solver *horsep*))
(setf (clay::y-order-f *solver*) nil)
(setf (clay::y-order-f *solver*) 'family-order?)
(setf (clay::node-pair-distance-f *solver*) 'kinship-distance)
(setf (clay::rest-length *solver*) 8.0d0)
(describe *solver*)
(wt:winspect *solver*)