;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Browser
  (:use :Common-Lisp #+:clos :CLOS)
  (:nicknames :br)
  (:export 
   Class-Graph
   make-class-browser

   Examiner-Graph
   examiner-references
   make-examiner-graph))

