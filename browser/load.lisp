;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================

(when (find-package :WT) (funcall (intern :composer :wt)))  

(load (truename
       (pathname
	(concatenate 'String
	  (namestring 
	   (make-pathname
	    :directory (pathname-directory excl::*source-pathname*)))
	  "../az.system"))))

(setf (sys:gsgc-switch :print) t)

(mk:load-system :Browser)

