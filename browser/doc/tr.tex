\documentstyle[code,twoside,epsf]{article}
\pagestyle{headings}
\newcommand{\ignorethis}[1]{}
\topmargin=-0.2in
\textwidth=6.0in
\textheight=8.0in
\oddsidemargin=0.25in
\evensidemargin=0.25in

\begin{document}

\title{A simple graph browser}

\author{{\sc John Alan McDonald}
\thanks{This work was supported in part 
the Dept. of Energy under contract FG0685-ER25006
and by NIH grant LM04174 from the National
Library of Medicine.}
\\
Dept. of Statistics, University of Washington}
%\date{October 1991}


\maketitle

\begin{abstract}
The purpose of this document is to describe a minimal interface
to a graph browser that is part of the Clay user interface toolkit,
a component of Arizona 
Clay is an object of current research and development;
the idea here is to provide a useful interface to its graph browser
which places as few constraints as possible on future changes.
\end{abstract}

\vfill
\pagebreak

\tableofcontents

\listoffigures

\vfill
\pagebreak

\section{Introduction}
\label{Introduction}

The purpose of this document is to describe a minimal interface
to a graph browser that is part of the Clay user interface toolkit,
a component of the Arizona system
\cite{McDo88b,McDo91g,McDo91h,McDo91j,McDo91k,McDo91l,McDo91m}.
Clay is an object of current research and development;
the idea here is to provide a useful interface to its graph browser
which places as few constraints as possible on future changes.

I've separated :Clay (the user interface package) into 3 packages
(and 3 modules or systems), 
to better expose the interface. 
The 3 packages are :Graph, :Clay, and :Browser.
The corresponding systems are defined in az/az.system 
(along with a number of other systems not discussed here).
The latest version of the three systems is on both 
belgica.stat.washington.edu and eowyn.rad.washington.edu,
under my home directory.
All three systems are compiled by loading
az/browser/make.lisp and 
loaded by loading az/browser/load.lisp.

\subsection{Declarative graph layout using nonlinear programming}

Automatic layout of graphs 
is problem that has a sizable literature;
for recent surveys see \cite{Tama88}.
Roughly speaking, methods for layout can be divided into
{\it procedural} and {\it declarative} 
(also known as {\it constraint-based} \cite{Kama88}).

By procedural layout, we mean simply that there is a procedure,
which, given a graph, computes its layout.
A typical layout procedure
(such as that given by Rowe et al. \cite{Rowe87})
traverses the graph,
assigning each node to an unoccupied site 
in a regular grid of allowed positions.
The order of traversal and the method of site assignment are 
chosen in the hope of producing a ``readable'' layout:
a minimal number of crossed edges,
short (but not too short) edges,
almost straight paths between important pairs of non-adjacent nodes,
node positions whose y (or x) coordinates are consistent with the
partial ordering of a directed graph,
and so on.
There are often additional passes over the graph
during which the nodes are re-arranged
in an attempt to improve the layout.

Declarative layout could be thought of as a special case of procedural
layout, where the procedure definition is based on
three basic abstractions:
\begin{itemize}
\item a domain of possible layouts (for a given graph),
\item a specification ``language'' that allows the user to express
	essential and desirable features of the layout, and
\item a generic solver, that takes a graph and a specification,
	and returns an element of the domain that meets
	or well approximates the specification.
\end{itemize}

For example, the domain might be all possible assignments of
nodes to positions in a discrete lattice in the plane.
The specification language might allow the user to ask to minimize
some loss function (eg. the number of edge crossings)
subject to constraints on the node positions (eg. no two nodes
can occupy the same position in the lattice).

The distinction between procedural and declarative layout
is, of course, very similar to the distinction between 
procedural and declarative programming languages.
In particular, our notion of declarative layout is 
intentionally close to constraint programming languages 
\cite{Born87,Hent89,Lele88,Levi84}.

The advantage of procedural layout is speed.
A procedural method makes one or a few passes over the graph;
the declarative example given above is 
an expensive integer programming problem,
whose solution would require the equivalent of many iterations
over the graph.

The advantage of the declarative approach is flexibility.
In general, the only way for a user to modify the result of a procedural
layout is to reprogram the procedure;
because of the ad hoc nature of most layout procedures,
it is difficult to know how to change the procedure to get the 
desired effect.
The three abstractions of the declarative approach --- the domain,
the specification, and the solver --- gives a user immediate access
to a whole class of layouts.
The key research issue is to design abstractions
to provide a large and useful class of layouts,
without giving up too much in performance compared
ad hoc procedural methods.

Our proposal for declarative layout is an extension of the 
spring model \cite{Eades84,Kama88}.
The spring model is based on a physical analogy:
Imagine a spring placed between each pair of nodes in the graph.
Let the rest length of the spring be the desired distance
between the nodes, eg., proportional to the length of the shortest
path in the graph connecting the two nodes.
Then simply minimize the energy.
(This is a minor variation
on multidimensional scaling \cite{Borg87,Youn87}.)

The domain of the layout is the space of node positions,
${\oplus^n} {\Re^2}$, that is, the n-fold direct sum of the plane. 
The specification language is the set of rest lengths 
(and spring constants).
The solver is whatever minimization routine is used.

Our extension to the spring model is to minimize the energy subject to affine
inequality (and equality) constraints on the node positions,
in other words, nonlinear programming \cite{Flet89,Gill81,Luen69,Luen84}.
For our solver, we are using standard packages for constrained minimization,
in particular MINOS and NPSOL \cite{Gill86a,Gill86b,Murt78,Murt82,Murt87}.
The possibility of affine constraints gives
us a much richer specification language,
making it easy, for example, 
to preserve the directedness of a graph in the vertical 
or horizontal ordering of the nodes, 
or to adjust a layout for the size and shape of the window in which
the graph is being displayed.

The use of modern optimization methods, as represented by MINOS and NPSOL,
gives us still greater flexibility.
We can experiment with other energy functions than the spring model
and eventually provide users with more general ways to specify the
energy than the rest lengths and spring constants.
For example, we are considering energy functions that
depend on the relationships of pairs of edges 
as well on pairs of nodes --- with the goal of minimizing
or at least controlling edge crossings.

\section{A protocol for Graph data structures}
\label{Protocol}

The Graph package is in directory az/graph. 
It defines a simple functional protocol for data structures
that are used to represent graphs 
and that are to be displayed by Clay's graph browser.

\subsection{Graph protocol}
\label{Graph-Protocol}

A graph object must have an immutable identity, 
for example,
you cannot represent a graph by a list and
cons new nodes or edges on the front.

A graph object must provide methods for two generic functions
\begin{itemize}

\item {\sf gr:nodes}
returns a list of the nodes in the graph.  
Nodes may be anything.
Nodes are considered identical if they are eql.

\item {\sf gr:edges}
returns a list of the edges in the graph.  
Edges must obey the edge protocol defined below.  
Edges are considered identical if they are eq.
\end{itemize}

If an object has methods for {\sf gr:nodes} and {\sf gr:edges}, 
then it is considered an instance of the abstract gr:Generic-Graph type
and satisfies \cd{\sf (typep {\it object} 'gr:Generic-Graph)}.

Graphs that want to be considered directed and/or acyclic
should define appropriate methods for the following two generic functions:
{\sf gr:directed-graph}, 
which returns T or Nil depending on whether the graph should be considered
directed,
and
{\sf gr:acyclic-graph?},
which returns T or Nil depending on whether the graph can be assumed to be
acyclic. 
The default method for both generic functions returns nil.

\subsection{Edge protocol}
An object representing a graph edge must have methods for
{\sf gr:edge-node0} and {\sf gr:edge-node1},
which return the nodes the edge connects.  
If the edge is considered
directed, then it goes from {\sf gr:edge-node0} to {\sf gr:edge-node1}.

If an object has methods for {\sf gr:edge-node0} and {\sf gr:edge-node1}, 
then it is considered an instance of the abstract 
{\sf gr:Generic-Edge} type
and satisfies
\cd{\sf (typep {\it object} 'gr:Generic-Edge)}.

\subsection{A sample implementation}
\label{Implementation}

The file implementation.lisp provides sample implementation classes
{\sf gr:Graph}, {\sf gr:Digraph}, 
and {\sf gr:Dag} that obey the graph protocol and may
be useful base classes for more specialized graphs.

\section{Clay}
\label{Clay}

The Clay package, in az/clay and az/layout, provides
a generic graph browser that can be used with any object that 
obeys the graph protocol. 

A graph browser is created by a call to {\sf clay:make-graph-diagram},
which has one required argument, the graph to be presented,
and keyword arguments:
:diagram-name, which is used for the label of the window in which
the graph is displayed,
and
:x-order-f, :y-order-f, :node-pair-distance-f,
and :rest-length, which are used to customize the layout and are discussed 
in section~\ref{Layout}.

\section{Customizing behavior}
\label{Customizing}

\subsection{Updating the display}
\label{Updating}

Currently, consistency between graphs and their presentations
is maintained using my implementation of Announcements \cite{McDo91m}
(in az/tools/announcements.lisp),
based on Mark Niehaus's implementation in Prism
of Sullivan-Notkin ideas about events/mediators \cite{Sull90a}.
A full discussion of events, mediators, announcements, etc.,
is beyond the scope of this document.

All a client of the graph browser needs to know is to
\cd{\sf (az:announce {\it graph} :graph-changed)}
when it wants displays of that graph to update in response to addition
or deletion of nodes or edges to or from {\it graph}.

\subsection{Menus}

The behavior of the graph browser can be
specialized by adding items to the menus
that are produced when the right (righthand) button is pressed.
If the mouse is within a node, then the node menu appears.
If the mouse button is not within a node,
and is within the rectangle enclosing an edge, 
then the edge menu appears. 
Otherwise the whole graph menu appears.

If your graph, nodes, and edges are instances of classes or structures
that you have defined,
then you can add menu items by defining new methods for the three
generic functions: 
{\sf clay:graph-node-menu},
{\sf clay:graph-edge-menu},
and
{\sf clay:graph-menu}.
Your methods must return a (newly constructed) list of menu items. 
A menu item is a list whose first item is the string
that is printed in the menu, and whose second item is a function that
is applied to the remaining items.  
Note that methods for these generic function must be defined with
{\sf append} methods combination.
For example, here is method for {\sf clay:graph-node-menu }
defined in az/browser/class.lisp:

\begin{code}
(defmethod clay:graph-node-menu append ((graph Class-Graph)
					(node Class))
  (list (list (format nil "Add direct subclasses of ~a" node)
	      #'add-subclass-nodes node graph)
	(list (format nil "Add direct superclasses of ~a" node)
	      #'add-superclass-nodes node graph)))
\end{code}

\subsection{Layout}
\label{Layout}

The function make-graph-presentation takes several keyword arguments
than can be used to customize the layout of the graph.
\begin{itemize}

\item {\sf :x-order-f, :y-order-f}

The graph layout is computed subject to the constraint that all nodes
remain in the window and optionally subject to constraints
that require node A to be above node B or node A to be to the left
of node B.

If supplied, {\sf :x-order-f} 
must be a function of two arguments that can be applied
to any pair of nodes in the graph.
It should return T if its first argument should be constrained to be to the
left of its second argument and Nil otherwise.

If supplied, {\sf :y-order-f}
 must be a function of two arguments that can be applied
to any pair of nodes in the graph.
It should return T if its first argument should be constrained to be above
its second argument and Nil otherwise.

It is better for performance reasons to use a minimal, non-transitive
ordering function.
For example,
if A is above B and B is above C,
it is not necessary to also constrain A to be above C.

If neither {\sf :x-order-f} or {\sf :y-order-f} 
are supplied then default constraints
will be imposed.
For graph objects that return T for both
{\sf gr:directed-graph?} and {\sf gr:acyclic-graph?},
the default constraints are that parent nodes must be left of their children.
Otherwise, there are no default constraints 
(other than the bounds constraints that all nodes must remian in the window).

\item {\sf :node-pair-distance-f}

If supplied, {\sf :node-pair-distance-f} must be a function of two arguments 
that can be applied to any pair of nodes in the graph.
It must return a positive Double-Float number.

The layout algorithm tries to place nodes so that the distance between
them on the screen is proportional to the value returned by
{\sf :node-pair-distance-f}.

If {\sf :node-pair-distance-f} is not supplied, 
then it defaults to the graph distance, that is,
the number of edges between the two nodes in the shortest
path in the graph, ignoring any directedness of the edges.

\item {\sf :rest-length}

{\sf :Rest-length} determines the average length of the displayed edges
in the graph, in a somewhat complicated fashion.
The distance between the centers of two nodes will be roughly
the rest-length times the geometric average of the widths or heights
of the nodes times a given desired distance (see below).
The overall scale of the layout will change linearly with {\sf :rest-length},
as long as all nodes remain in the interior of the window.
The exact result of a given {\sf :rest-length} with a particular
graph is hard to predict ahead of time.
10 seems to be a good value for many graphs.
\end{itemize}

\section{Examples}
\label{Examples}

The Browser module, in az/browser/, provides three examples of how
to use the graph browser.
Script.lisp has examples of how to create all three kinds of browsers.
 
The first two examples show how to use the graph browser
with minimal specialization --- essentially just adding menu items.
The first example is a CLOS class hierarchy browser, in class.lisp 
(see figure~\ref{diagram-classes} for an example).  
The second example is a Lisp object browser, in examiner.lisp
(see figure~\ref{diagram-examiner} for an example). 

\begin{figure}
\begin{center}
\mbox{\epsffile{diagram-classes.ps}}
\end{center}
\ignorethis{
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(5.0,3.0)
\put(0,0){\special{psfile=diagram-classes.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
}
\caption {A class inheritance graph for some classes related to graph browsers.
\label{diagram-classes}}
\end{figure}

\begin{figure}
\begin{center}
\mbox{\epsffile{diagram-examiner.ps}}
\end{center}
\ignorethis{
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(4.0,2.0)
\put(0,0){\special{psfile=diagram-examiner.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
}
\caption {A examiner graph for some objects used to make a class browser.
\label{diagram-examiner}}
\end{figure}


The third example is demonstrates alternate layouts,
using custom order and distance functions.
It is contained in two files, pedigree.lisp and horses.lisp.
These files create a small database for a small number
of Przewalski's horses \cite{Geye89}.
The followinf figures show 3 alternative layouts:
(figure~\ref{horse2}) treats the pedigree as an undirected graph,
(figure~\ref{horse1}) uses inheritance to place
parents above children,
and 
(figure~\ref{horse0}) uses  distance function based on kinship, 
the probable genetic similarity between two individuals.


\begin{figure}
\begin{center}
\mbox{\epsffile{horsep2.ps}}
\end{center}
\ignorethis{
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(2.5,3.0)
\put(0,0){\special{psfile=horsep2.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
}
\caption {Horse pedigree layout, treating the pedigree as an undirected graph. \label{horse2} }
\end{figure}

\begin{figure}
\begin{center}
\mbox{\epsffile{horsep1.ps}}
\end{center}
\ignorethis{
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(2.5,3.0)
\put(0,0){\special{psfile=horsep1.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
}
\caption {Horse pedigree layout using inheritance. 
\label{horse1} }
\end{figure}


\begin{figure}
\begin{center}
\mbox{\epsffile{horsep0.ps}}
\end{center}
\ignorethis{
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(5.0,2.5)
\put(0,0){\special{psfile=horsep0.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
}
\caption {Horse pedigree layout using kinship and inheritance. 
\label{horse0} }
\end{figure}

\bibliographystyle{plain} 

\bibliography{../../b//arizona,../../b//cs,../../b//cactus,../../b//constraint,../../b//database,../../b//layout,../../b//lisp,../../b//image,../../b//buja,../../b//mcdonald,../../b//friedman,../../b//wxs}

\vfill
\pagebreak

\section{Reference Manual}
\label{reference-manual}
\pagestyle{myheadings}

These reference manual entries were produced automatically,
using the Definitions module described in \cite{McDo91h}.

\vskip 1.5em


\input{reference-manual}

\pagestyle{headings}

\end{document}