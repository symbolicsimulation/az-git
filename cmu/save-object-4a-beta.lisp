;;; -*- Base: 10; Mode: LISP; Package: DATABASE; Syntax: Common-Lisp -*-

#| 

 SAVE OBJECT, Version 4A(BETA TEST), Effective Date: December, 1991.

New algorithm & code:  Copyright (c) Kerry V. Koitzsch, 1991.
Old algorithm & code: Copyright (c) ADS, 1990, 1991.

The views, opinions, and/or findings contained in this document are those
of the author, and should not be construed as an official position, policy,
or decision of any company or other individual, unless designated by other
documentation.

Permission is granted to any individual or institution to use, copy, 
modify and distribute this document, provided the copyright and permission
notice is maintained, intact, in all copies and supporting documentation.
The author makes no representations about the suitability of the software
described herein for any purpose. It is provided "as is" without express
or implied warranty.

Suggestions, bugs, criticism and questions to kerry@ads.com, or 
kerryk@corwin.ccs.northeastern.edu.

SAVE-OBJECT is a recursive function which writes an ASCII representation
of a LISP object to a designated file.

Objects which may be saved include:

--- symbols, keywords, characters, strings, and pathnames.
--- numbers, including integer, rational, complex, and floating point.
--- vectors and multi-dimensional arrays.
--- objects produced by defstruct.
--- CLOS (PCL) instances, and CLOS(PCL) classes.
--- hash tables.
--- compiled functions, represented as (FUNCTION <function name>),
    internally.
--- generic functions, method objects, and class objects.
--- conses and lists.
--- user defined methods may be defined for arbitrary objects, such
    as images.
--- readtables (a kludge for now)
--- CLIM objects (saved as ordinary CLOS instances)

Tested on:
------ --
Machines: Sun-4, Symbolics 3670, Mac IIfx and Mac Quadra.
Versions of Symbolics Common Lisp: Genera 8.1, Genera 7.2 w/rev 4b PCL.
Versions of PCL/CLOS: AAAI PCL, Victoria Day PCL, REV 4b PCL, Lucid 4.0
CLOS.

Versions of MCL: 2.0b3
Versions of Allegro Common Lisp: 4.0, 4.1beta.
Versions of Lucid (Sun) Common Lisp: 4.0.

Directions:
-----------

(1) Redefine the IN-PACKAGEs below to suit: they should USE CLOS or PCL,
though.
In particular, on the Symbolics, because of the attribute line, the 
DATABASE package must be constructed BEFORE loading the file.

Try this:

(make-package 'database :nicknames '(db) :use '(common-lisp))
(in-package 'database)
(shadowing-import '(setf documentation) 'database)
(use-package 'clos)
If at any point an error occurs about conflicting symbols, select the
proceed option which prefers the symbols in the common lisp package.

(2) After defining an appropriate package (only the symbolics package
needs to be created beforehand, the others are defined automatically
while loading) load the file, save-object.lisp.

(3) Enter package DATABASE with (in-package 'DATABASE). You are now
ready to save objects!

(4) To save an object to a file, invoke the save-object function:

(in-package 'database)
(save-object (list 20 30 19.6) "my-simple-filename.lisp")

to reload the saved-object file:

(load "my-simple-filename.lisp")

The result of the load is stored in the global variable *db-input*,
in the DATABASE package.

|#

#+lucid
(in-package 'DATABASE :nicknames '(DB) :use '(CLOS LISP))

;;;===============================================================================
;;; Package building for MCL.
;;;===============================================================================

#+:mcl
(eval-when (load eval compile)

(unless (find-package 'database)
(make-package 'database  :nicknames '(db) :use '(common-lisp)))

(in-package DATABASE) ;;;; ANSI definition of IN-PACKAGE

(pushnew :clos *features*) ;;; MCL has clos, but it isnt in the features list...

(unless (find-package 'clos)
(make-package 'clos :use '(ccl common-lisp))) ;;; hence no cl package, either.

(when (equal (machine-instance) "Quadra")
       (pushnew :quadra *features*) ;;; note that its a 68040 on features....
)

(when (equal (machine-type) "Macintosh IIfx")
       (pushnew :fx *features*) ;;; note that its an fx on features....
)
) ;;; end of MCL eval-when...

;;; Set up correct Lucid hash-table accessors....

;;; New, added this to keep Lucids compiler happy!

#+lucid
(shadowing-import '(lcl::hash-table-rehash-size
		    lcl::hash-table-size
		    lcl::hash-table-test
		    lcl::hash-table-rehash-threshold
) 'database)

#+lucid
(eval-when (load eval compile)
(setf (symbol-function 'hash-table-rehash-size) #'lcl::hash-table-rehash-size)
(setf (symbol-function 'hash-table-size) #'lcl::hash-table-size)
(setf (symbol-function 'hash-table-test) #'lcl::hash-table-test)
(setf (symbol-function 'hash-table-rehash-threshold) 
      #'lcl::hash-table-rehash-threshold))

;;; Set version info on *features* list for Symbolics. assumes its 7.2, 8.0, or 8.1.

#+lispm
(eval-when (load eval compile)

(multiple-value-bind (major minor status)
  (sct:get-release-version)
(cond ((and (equal major 7)(equal minor "2"))(pushnew :rel-7-2 *features*))
      ((and (equal major 8)(member minor '("0" "1") :test #'equal))
	    (if (equal minor "0")(pushnew :rel-8-0 *features*)
		(pushnew :rel-8-1 *features*)))
      (T (error "Can't deal with major release ~a, minor release ~a!" major minor))))

(when (find-package 'clos)
      (pushnew :clos *features*))

(unless (find-package 'database)
        (make-package 'database :nicknames '(db) :use '(clos)))

(shadowing-import '(setf documentation) 'database)

(unless (find-package 'clos)(format t "Couldnt find the CLOS package, trying to continue.~%"))

(in-package 'database :use '(LISP))

(defun UNLOCK-PKG (packagename)
"Changes read-only package status to read+write, if package exists."
(when (find-package packagename)
(setf (si:pkg-locked (find-package packagename)) NIL)))

) ;;; end of Symbolics eval-when. Worry about TI much later.

;;; Initial package-building eval-when for allegro on suns.

#+excl
(eval-when (load compile eval)

(setf excl:*cltl1-in-package-compatibility-p* T)

(in-package :Common-lisp-user)

(defpackage "database" (:nicknames "dbs") (:use :clos :excl :common-lisp-user))

(in-package DATABASE)

(in-package 'DATABASE)

#+allegro-v4.1
(defun UNLOCK-PKG (packagename)
"Changes read-only package status to read+write, if package exists."
(when (find-package packagename)
    (setf (excl::package-lock-fdefinitions
	   (find-package packagename)) NIL)))

#+allegro-v4.1
(unlock-pkg 'common-lisp)

) ;;; end of excl eval-when...

;;; NOTE: Change the package def below if it does not suit you:
;;; make sure you USE-PACKAGE your favorite brand of CLOS or PCL, though.

#+lucid
(in-package 'DATABASE :nicknames '(DB) :use '(CLOS LISP))

#+lispm
(in-package 'DATABASE :nicknames '(DB) :use '(CLOS LISP))

#+pcl
(in-package 'DATABASE :nicknames '(DB) :use '(PCL LISP))

;;; exported symbols....

#-excl
(in-package 'DATABASE)

(export '(stream-fasd-form
	   symbol-fasd-form
	   readtable-fasd-form
	   generic-function-fasd-form
	   method-fasd-form
	   class-fasd-form
	   complex-fasd-form
	   array-fasd-form
	   structure-fasd-form
	   vector-fasd-form
	   compiled-function-fasd-form
	   instance-fasd-form
	   htab-fasd-form
	   cons-fasd-form))

(export '(*db-input*
	  *global-unsavable-slots*
	  save-object
	  pseudo-quote-reader
	  reset-symbol-counter
	  hash-table-size
	  hash-table-rehash-size
	  hash-table-rehash-threshold
	  hash-table-test
	  make-keyword
	  string-append
	  all-slots
	  all-slotnames
	  copy-instance
	  all-slots-and-values
	  all-values
	  %allocate-instance
	  find-generic-function
	  methodp
	  instance-p
	  instance-name
	  structure-p
	  get-slot-values
	  pushsym
	  list-array
	  coerce-2d-array
	  make-defstruct-body
	  get-fasd-form
	  fasd-form
	  has-fasd-form-p
	  define-fasd-form

	  %make-array

	  describe-htab
	  cons-p

	  array-list-aux
	  set-defstruct-slot
	  get-defstruct-value
	  search-symbols
	  read-file
	  makesyms
	  *save-object-system-date* 
	  write-global-header))

;;; Globals.

(defvar *debug-instance-storage* nil "when this one is T, status messages are printed
by the CLOS instance saver to aid diagnosis of problems.")

(defvar *supress-standard-object* T "")
(defvar *save-contents-of-class-allocated-classes* NIL "")

(defvar *load-object-hash-table* (make-hash-table :size 50 :test #'eql)
"A hash table which is filled at load time with objects restored from a
file.")

(defvar *save-object-hash-table* (make-hash-table :size 50 :test #'eql)
"A hash table which is filled at save time by the invokation of the
save object function.")

(defvar *pco-types* '(structure hash-table array-type-t
		      class instance circular-list)
"A list of the type names returned by function %type-of, that
are potentially circular objects (PCOs).")

(setf *pco-types* '(structure hash-table array-type-t 
		      class instance circular-list))

(defvar *mode-for-set-object-var* nil "Either :load or :save, depending
on the context. Used by SET-OBJECT-VAR.")

(defvar *save-object-system-date* "December 1991 Save Object Experimental: VERSION 4A.")

#+lucid
(setf lcl::*print-structure* T) ;;;; "Prints the #S form of a defstruct when t."

(defvar *global-instance-count* 0)

(defvar *global-object-count* 0
 "count of varnames made for object hashtable objects, by makevar in cache-object invokations.")

(defvar *mode-for-object-var* :save)

(defvar *seen* nil)
(defvar *vars* nil)
(defvar *structs-seen* nil)
(defvar *struct-vars* nil)
(defvar *htabs-seen* nil)
(defvar *htab-vars* nil)
(defvar *vectors-seen* nil)
(defvar *vector-vars* nil)
(defvar *arrays-seen* nil)
(defvar *array-vars* nil)

(defvar *use-default-class-initargs* nil)
(defvar *global-unsavable-slots* nil)

(defvar *current-htab-size* nil)
(defvar *current-htab-rehash-size* nil)
(defvar *current-htab-rehash-threshold* nil)
(defvar *current-htab-test* nil)

;;; ROW MAJOR AREF --- ACL doesnt have it, Genera has it in package FCL....

;;; lucid has row-major-aref, no problem.

#+(or rel-8-0 rel-8-1)
(shadowing-import '(future-common-lisp:row-major-aref) 'database)

#-(or lispm rel-8-0 rel-8-1 lucid)
(when (not (fboundp 'row-major-aref))
(pushnew :need-row-major-aref *features*))

;;; lispm has it.

#-lispm
(eval-when (load eval compile)

#+need-row-major-aref 
(defun ROW-MAJOR-AREF (array index)
"We have to define this, as Franz does not implement RMA pg. 450 CLtL2.
 NOTE: Neither does Symbolics."
(aref (make-array (array-total-size array)
		  :displaced-to array
		  :element-type (array-element-type array))
      index))

#+need-row-major-aref
(defun ROW-MAJOR-SETA (array index newval)
"so we can defsetf row-major-aref!"
(setf (aref (make-array (array-total-size array)
			:displaced-to array
			:element-type (array-element-type array))
	    index) newval))

#+need-row-major-aref
(defsetf row-major-aref row-major-seta)

) ;;; eval-when....

(defun GET-INSTANCE-LABEL (instance)
""
(let* ((lists (case (%type-of instance)
                (INSTANCE (list *seen* *vars*))
                (STRUCTURE (list *structs-seen* *struct-vars*))
                (HASH-TABLE (list *htabs-seen* *htab-vars*))
                (otherwise (error "Couldnt parse ~a, of type ~a!"
                                  instance (type-of instance))))))
  (let* ((instance-list (first lists))
         (var-list (second lists))
         (where (position instance instance-list :test #'equal)))
    (when (null where)(error "~a was not on the seen list!" instance))
(nth where var-list))))
    
(defun DO-VAR-TYPE-CELLS (vars insts)
""
(mapcar #'(lambda (a b)(list a b))
vars insts))

#-allegro-v4.0
(defun MAKE-VAR-TYPE-CELLS (vars insts)
""
(loop with cells = (do-var-type-cells vars insts)
  for cell in cells collect `(,(first cell)
 (%ALLOCATE-INSTANCE ',(instance-name (second cell))))))

#+allegro-v4.0
(defun MAKE-VAR-TYPE-CELLS (vars insts)
""
(mapcar #'(lambda (c)
	    (list (first c)(list '%allocate-instance 
				(list 'quote (instance-name (second c))))))
(do-var-type-cells vars insts)))

(defun MAKE-LIST-VAR-TYPE-CELLS (to-be-saved-list)
"SEQUENCES need the whole ball of wax."
(declare (ignore to-be-saved-list))
(let* ((insts (make-var-type-cells *vars* *seen*))
       (structs (make-var-type-cells *struct-vars* *structs-seen*))
       (htabs (make-var-type-cells *htab-vars* *htabs-seen*))
       (end-result (append insts structs htabs)))
end-result))

(defun MAKE-LET-FORM (object &optional other-code)
"This functions constructs the lexical environment for the text representation of
LISP objects --- without this, there could be no self refererence!"
(cond ((equal (%type-of object) 'instance)
`(let* (,@(make-list-var-type-cells object))
        ,other-code))
      ((equal (%type-of object) 'structure)
       `(let* (,@(make-list-var-type-cells object))
        ,other-code))
      ((equal (%type-of object) 'hash-table)
       `(let* (,@(make-list-var-type-cells object))
          ,other-code))
      ((equal (%type-of object) 'circular-list)
       `(progn ,other-code))
      ((equal (%type-of object) 'vector)
       `(let* (,@(make-list-var-type-cells object)) ,other-code))
      ((equal (%type-of object) 'array)
       `(let* (,@(make-list-var-type-cells object)) ,other-code))
      ((listp object)`(let* (,@(make-list-var-type-cells object)) ,other-code))
(T (warn "FROM MAKE LET FORM: object was of bogus type: ~A!!!" (%type-of object))
   (if other-code `(progn ,other-code)
       (progn (warn "there was no code to enclose!") nil)))))

#-(or lispm lucid)
(defun FILL-ARRAY (array l)
"Fill n-dimensional array with values from list."
(let ((list (flatten l)))
(if (= 1 (length (array-dimensions array)))
    (loop for count from 0 to (1- (length array)) do
	  (setf (aref array count)(nth count list))
	  finally (return-from fill-array array))
(progn  (dotimes (i (array-total-size array) array)
	   (setf (row-major-aref array i)(nth i list)))
	(return-from fill-array array)))))

#+lispm
(defun FILL-ARRAY (array l)
"Fill n-dimensional array with values from list."
(let ((list (flatten l))(array array))
(declare (sys:array-register array))
(if (= 1 (length (array-dimensions array)))
    (loop for count from 0 to (1- (length array)) do
	  (setf (si:%1d-aref array count)(nth count list))
	  finally (return-from fill-array array))
(progn  (dotimes (i (array-total-size array) array)
	   (setf (row-major-aref array i)(nth i list)))
	(return-from fill-array array)))))

#-lucid
(defun MAPARRAY (function array)
"like mapcar, but maps a function over each element of an
n-dim array: the function to be applied is a function of two args,
the count and the element value at aref count in the array."
(let ((array array))
#+lispm (declare (sys:array-register array))
(if (= 1 (length (array-dimensions array)))
    (loop for count from 0 to (1- (length array)) do
#+lispm(setf (sys:%1d-aref array count)(funcall function count (sys:%1d-aref array count)))
#-lispm(setf (aref array count)(funcall function count (aref array count)))
	  finally (return-from maparray array))
(progn  (dotimes (i (array-total-size array) array)
	   (setf (row-major-aref array i)
		 (funcall function i (aref array i))))
	(return-from maparray array)))))

(defun ARRAY-TYPE-T-P (X)
"Predicate, checks type and element-type of x."
(and (arrayp x)(not (stringp x))(equal (array-element-type x) T)))

(defun %TYPE-OF (x)
"Special type-of operator, returning a more intellignet type  for object caching:"
(cond ((%classp x) 'class)
      ((instance-p x) 'instance)
      ((structure-p x) 'structure)
      ((hash-table-p x) 'hash-table)
      ((typep x 'vector) 'vector)
      ((array-type-t-p x) 'array-type-t)
      ((arrayp x) 'array)
      ((listp x)(if (circular-list-p x) 'circular-list 'list))
      (T (type-of x))))

(defun LOOKUP-OBJECT (X &key (mode :save))
"Accessor to the global object hashtable."
(rassoc x (gethash (%type-of x)(if (equal mode :save)
				 *save-object-hash-table*
			       *load-object-hash-table*))
 :test #'equalp))

(defun CACHE-OBJECT (x &key (mode :save))
"If the object is a structured object, cache the object in the object
hash table, if it isnt already there, along with its variable designation."
"If the object is a structured object, cache the object in the object
hash table, if it isnt already there, along with its variable designation."
(push (CONS (makevar) x) (gethash (%type-of x)
(if (equal mode :save) *save-object-hash-table*
  *load-object-hash-table*)))
x)

(defun LOOKUP-OBJECT-OR-CACHE (x)
""
(cond ((null (lookup-object (eval x) :mode :load))
       (cache-object (eval x) :mode :load))
      (T x)))

(defun %LIST-LENGTH (x)
"Differs from ClTl2 LIST-LENGTH in that a multiple value return of
NIL and counter value are returned if its a circular list."
(do (( n 0 (+ n 2))
     (fast x (cddr fast))
     (slow x (cdr slow)))
     (nil)
(when (endp fast)(return (values n nil)))
(when (endp (cdr fast))(return (values (1+ n) nil)))
(when (and (eq fast slow)(> n 0))(return (values nil (/ n 2))))))

(defun FIRSTN (n list)
"Return the first n elements of a list."
(loop for count from 1 to n collect (nth count list)))

(defun GET-CIRCULAR-LIST-ELEMENTS (circular-list)
"Given a circular list, get the repeating pattern."
(if (circular-list-p circular-list)
    (multiple-value-bind (status len) 
    (%list-length circular-list)
       status
      (firstn len circular-list))
    circular-list))

(defun MAKE-CIRCULAR-LIST (elts)
"Given non circular list elements elts, return a circular list of those elements."
 (rplacd (last elts) elts))

(defun CIRCULAR-LIST-FASD-FORM (clist)
""
(let ((ones (get-fasd-form (get-circular-list-elements clist))))
  `(make-circular-list ,ones)))

(defun CIRCULAR-LIST-LENGTH (clist)
"Given a circular list, returns the number of non-circular elements before cycle:
returns an error if this is not a circular list!"
(multiple-value-bind (status length)
(%list-length clist)
(when status (error "this is not a circular list!"))
length))

(defun CIRCULAR-LIST-EQUAL (a b)
""
(and (equal (circular-list-length a)(circular-list-length b))
     (equal (get-circular-list-elements a)(get-circular-list-elements b))))

(defun CIRCULAR-LIST-P (X)
"Predicate to determine if something is a circular list, uses
LIST-LENGTH, which, unlike LENGTH, terminates and returns NIL if
the list is circular: LIST-LENGTH may not be in all versions of
LISP, as it is CLtL2."
(and (listp x) (null (list-length x))))

(defun PCO-P (instance)
"A predicate to determine if a LISP object is a PCO."
(and (not (stringp instance))(member (%type-of instance) *pco-types*)))

(defun OBJECT-VAR (some-object &optional mode)
"The structure of the object htabs entries is (key . object),
 finding the cell with lookup-object, then the first element of the CONS!"
(if (null mode)(setf mode *mode-for-object-var*))
(let ((lo (lookup-object some-object :mode *mode-for-object-var*)))
(setf lo
(cond ((null lo)(warn "couldnt find ~a in object var!" some-object) NIL)
      ((listp lo)(first lo))
      (T lo)))))

(defun SET-OBJECT-VAR (object new-var)
"Given object and new var, and mode, set the appropriate hash table
key/value to the new-var."
(let* ((mode *mode-for-set-object-var*)
(there (lookup-object object :mode mode)))
(when (not there)(cache-object object :mode *mode-for-object-var*))
(rplaca (lookup-object object :mode *mode-for-object-var*)(object-var new-var))))

(defsetf object-var set-object-var)

(defun  LTCO (type values)
"Given an object type, andthe objects contained values, construct an
object with those values at load time with this form."
`(let ((tmp (allocate-object ',type)))
   (fill-object tmp ',values)
   (return-object tmp)))

(defun MAP-NONCIRCULAR-ELEMENTS-AND-COPY (function circ-list)
""
(let ((elts (mapcar function
   (copy-list (get-circular-list-elements circ-list)))))
(make-circular-list elts)))

(defun MAP-OBJECT (function object)
"Generalized iterator for PCOs."
(cond ((circular-list-p object)
       (map-noncircular-elements-and-copy function object))
      ((vectorp object)
       (loop for count from 0 to (1- (length object)) do
	     (setf (aref object count)
		   (funcall function (aref object count)))
	     finally (return object)))
      ((arrayp object)(maparray function object))
      ((structure-p object)(mapstruct function object))
      ((hash-table-p object)(maphash #'(lambda (key val)
			    (setf (gethash key object)
				  (funcall function val)))
				     object) object)
      ((instance-p object)(map-instance function object))
      (T (warn "Couldnt deal with object ~a, type: ~a.~%"
	       object (type-of object)))))

(defun FIND-MARKER-OBJECT (label)
(maphash #'(lambda (unu val)
             unu
	     (when (assoc label val :test #'equal)
	       (return-from find-marker-object (rest (assoc label val :test #'equal)))))
         *load-object-hash-table*))

(defun READ-FILE (pathname &key
			   (variable '*db-input*))
""
  (cond ((load pathname :if-does-not-exist nil)
	 (setf *vars* nil *seen* nil
	       *struct-vars* nil *structs-seen* nil)
	 (setf (symbol-value variable)
	       (search-symbols (symbol-value variable)))
	 (eval variable))
	(T (format t  "the pathname ~a does not exist." pathname)
	   NIL)))

(defun SEARCH-SYMBOLS (instance)
"This is the recursive reader which processes incoming expressions from the file, reconst
ructing a possibly recursive object as it goes --- like an inverse to GET-FASD-FORM."
(cond ((null instance) nil)
      ((equal instance T) T)
      ((numberp instance) instance)
      ((symbolp instance)
       (cond ((not (special-marker-p instance)) instance)
	     ((member instance *vars* :test #'equal)(eval instance))
	      ((member instance *struct-vars* :test #'equal)(EVAL INSTANCE))
;;;	       (nth (position instance *struct-vars* :test #'equal)
;;;		    *structs-seen*))
	     (T (if (instance-p instance)(push instance *vars*)(push instance *struct-vars*))
		(search-symbols (eval instance)))))
      ((functionp instance) instance)
      ((pathnamep instance) instance)
      ((instance-p instance)
       (unless (member instance *seen* :test #'equal)
	       (push instance *seen*)
	       (map-instance #'(lambda (slotname slotval)
				 (setf (slot-value instance
		slotname)(search-symbols  slotval)))
			     instance)))
      ((hash-table-p instance)
       (maphash #'(lambda (key val)
		(setf (gethash key instance)
		      (search-symbols val)))
		instance)
       instance)
((structure-p instance)
       (unless (member instance *structs-seen* :test #'equal)
	       (push instance *structs-seen*)
 (mapstruct #'(lambda (slotname val)
		(set-defstruct-value instance slotname
				     (search-symbols val)))
	    instance)
instance))
((or (characterp instance)(stringp instance)) instance)
((arrayp instance)
 (maparray #'(lambda (count val)(setf (aref instance count)
				      (search-symbols val)))
	   instance))
((cons-p instance)(cons (search-symbols (first instance))
			(search-symbols (rest instance))))
((listp instance)(mapcar #'search-symbols instance))
(T (format t "couldnt parse ~a, with type ~a."
	   instance (type-of instance)))))

(defun RETURN-OBJECT (object)
"Return an 'expanded' object."
(map-object #'search-symbols object))

;;;  *** Beginning of CLOS eval-when... ***

#+clos
(eval-when (compile load eval)

;;; *** Dont-care vendor CLOS definitions. ***

(defun GET-CLASS-METACLASS (class-object)
"Given a class object, returns the metaclass name to help build
 CLASS-FASD-FORM:  (NEW)."
(when (%classp class-object)
(let ((meta (%class-name (class-of (class-of class-object)))))
(if (not (equal meta 'clos::standard-class)) ;;; the default...
(list (list :metaclass meta))))))

(defun GET-CLASS-DOCUMENTATION (c)
""
(or (documentation c) ""))

(defmethod INSTANCE-NAME ((instance T))
  "returns the symbol naming the given class object.
   NOTE: on the slimbolical hash-tables are FLAVORS.
   Therefore one must use HASH-TABLE-P instead of TYPE-OF,
   and the type returned is a Common Lisp entity, NOT a FLAVOR!"
(cond ((hash-table-p instance) 'hash-table)
      ((equal (%type-of instance) 'structure)(type-of instance))
      (T (clos::class-name (clos::class-of instance)))))

;;; *** BEGINNING OF NON-MCL definitions! ***

#-:mcl
(eval-when (eval load compile)

(defun HAS-FASD-FORM-P (class-name)
  "Predicate, returns t if a class has a user-defined FASD FORM method."
  (get class-name 'user::%%FASD-FORM-METHOD%%))

(defmacro DEFINE-FASD-FORM (class-name arglist &body body)
  "Macro to define a user-defined fasd-form for a given class-name.
   You could do this as two discrete steps, programmatically where you need it."
  `(progn (setf (get ',class-name 'user::%%fasd-form-method%%) T)
	  (defmethod FASD-FORM ,arglist ,@body)
	  ',class-name))

(defun GET-CLASS-DEFAULT-INITARGS (class)
"Gets the default-initargs out of the class object."
(mapcan #'(lambda (l)(list (first l)(get-fasd-form (third l))))
(clos::class-direct-default-initargs class)))

(defmethod ALL-SLOTNAMES ((instance T) &optional (all-allocations T))
  "returns the names of the slots in instance, uses what MOP stuff is available."
(declare (ignore all-allocations))
(mapcar #'clos::slot-definition-name 
	(clos::class-DIRECT-slots (clos::class-of instance))))
                 ;;;;******

) ;;; end of non-MCL definitions eval-when...

;;; *** beginning of MCL common lisp definitions...***

#+:mcl
(eval-when (compile load eval)

(defun HAS-FASD-FORM-P (class-name)
  "Predicate, returns t if a class has a user-defined FASD FORM method."
  (get class-name '%%FASD-FORM-METHOD%%))

(defmacro DEFINE-FASD-FORM (class-name arglist &body body)
  "Macro to define a user-defined fasd-form for a given class-name.
   You could do this as two discrete steps, programmatically where you need it."
  `(progn (setf (get ',class-name '%%fasd-form-method%%) T)
	  (defmethod FASD-FORM ,arglist ,@body)
	  ',class-name))

(defun CLASS-SLOTNAMES (class-object)
"Calls the clos internal function to compute class slot names."
(remove nil (mapcar #'first (class-slots class-object))))

(defun CLASS-SLOTS (class)
#+supra (ccl::class-instance-slots class)
#+fx (ccl::class-slots class)
)

(defun CLASS-DIRECT-SLOTS (class)
(ccl::class-direct-slots class))

(defun GET-DEFSTRUCT-CONSTRUCTOR (name)
"default definition for now...."
(read-from-string (concatenate 'string "make-" (format nil "~a" name))))

(defun ALLOCATE-STRUCT (name)
"Function to allocate the empty husk of a defstruct."
(apply (get-defstruct-constructor name) nil))

(defun INSTANCE-P (X)
"Predicate to determine whether something is an INSTANCE."
(and (not (%classp x))(typep x 'standard-object)))

(defun STRUCTURE-P (X)
"Predicate to determine whether something is a structure INSTANCE."
(ccl:structurep x))

(defun GET-CLASS-DEFAULT-INITARGS (class)
"Gets the default-initargs out of the class object."
class
nil)

(defun %CLASSP (X)
"predicate to tell if something is a class object."
(typep x 'ccl::standard-class))

(defun %GENERIC-FUNCTION-DOCUMENTATION (f)
""
(or (documentation f) ""))

(defun GET-SLOT-TYPE (S)
""
(first (reverse s)))

(defun GET-DIRECT-SLOTS (class-object)
"Gets the immediately available 'new' non inheried slot OBJECTS."
(class-direct-slots class-object))

(defun GET-SLOT-DOCUMENTATION (s)
""
(or (documentation s) ""))

(defun GET-SLOT-NAME (S)
"Method to get the name from a standard slot."
(clos::slot-definition-name s))

(defun SLOT-HAS-AN-INITFORM-P (slot-object)
""
(second slot-object))

(defun GET-SLOT-READERS (s)
""
s
nil)

(defun GET-SLOT-WRITERS (s)
""
s
nil)

(defun %SLOT-DEFINITION-ALLOCATION (S)
""
s
NIL)

(defun GET-SLOT-NAMED (instance name)
""
(find-if #'(lambda (slot)
(equal (get-slot-name slot) name))
	 (all-slots instance)))

(defun GET-SLOT-ALLOCATION (S)
"Method to get the type of allocation from a standard slot: oneof :CLASS or :INSTANCE."
(let ((alloc (%slot-definition-allocation s)))
(cond ((%classp alloc) :CLASS)
      ((member alloc '(:INSTANCE :CLASS)) alloc) 
      (T :INSTANCE))))

(defmethod GET-SLOT-INITFORM (s)
""
(when (slot-has-an-initform-p s)
(first (second s))))

(defun %GET-SLOT-INITFORM (S)
"Method to create the iniform pair, if there is an initform value!"
(if  *save-contents-of-class-allocated-classes*
(when (and (equal (get-slot-allocation s) :CLASS)
	   (slot-has-an-initform-p s)) 	   
(list :initform (get-fasd-form (funcall (get-slot-reader s) s)))))
(when (slot-has-an-initform-p s)
(list :initform  (get-slot-initform s))))

(defun GET-SLOT-INITARGS (s)
""
(ccl::class-slot-initargs s))

(defun GET-SLOT-INITARG (s)
""
(first (ccl::class-slot-initargs s)))

(defmethod ALL-SLOTNAMES ((instance T) &optional (all-allocations T))
  "returns the names of the slots in instance, uses what MOP stuff is available."
(declare (ignore all-allocations))
(REMOVE NIL (mapcar #'clos::slot-definition-name 
	            (class-slots (clos::class-of instance)))))

(defun ALL-SLOTS (instance)
"Gets all the slots from the instances class, whether inherited or not."
(class-slots (clos::class-of instance)))

(defun %CLASS-NAME (x)
"If instance, gets the name of the class of the instance."
(if (instance-p x)(clos::class-name (clos::class-of x))
(clos::class-name x)))

(defun GET-SUPERCLASS-NAMES (class)
""
(mapcar #'clos::class-name (clos::class-direct-superclasses class)))

) ;;; *END OF MCL CCL CLOS eval-when! ****

;;; *** Non-LISP machine CLOS eval-when. ***

#-(or lispm :mcl)
(eval-when (load compile eval)

(defun %GENERIC-FUNCTION-DOCUMENTATION (f)
""
(or (documentation f) ""))

(defun GET-SLOT-TYPE (S)
"Method to get the type from a standard slot:
 this works for most things EXCEPT Genera 8x CLOS."
(clos::slotd-type s))

;;;;(defun GET-DIRECT-SLOTS (class-object)
;;;""
;;;(clos::class-class-direct-slots class-object))

(defun %GENERIC-FUNCTION-P (x)
"Predicate, returns t for generic functions. causes symbol conflict problem
 in genera 8.0."
(clos::generic-function-p x))

)

;;; *** END OF NON LISPM EVAL-WHEN ***

;;; *** Lisp Machine Genera 8.x CLOS eval-when. ***

#+lispm
(eval-when (load eval compile)

(defun %GENERIC-FUNCTION-P (x)
"Predicate, returns t for generic functions. causes symbol conflict problem
 in genera 8.0."
(clos-internals::generic-function-p x))

(defun BUILTIN-CLASS-P (class-object)
"Predicate to determine whether a class object (that which is returned by (FIND-CLASS <NAME>))
 is a BUILTIN class or not."
(typep class-object 'clos:built-in-class))

(defmethod CLASS-NAME ((object t))
"We use this in %classp. we already know its either an instance or a class.
 if its an instance, it has no name. CLASS-NAME on standard class takes care of real
 class objects."
nil)

(defun %CLASSP (X)
"The function CLASSP is not defined at all in Genera."
(and (instance-p x)(find-class (class-name x) nil)))

(defun %GENERIC-FUNCTION-DOCUMENTATION (f)
""
(or (documentation f) ""))

(defun INSTANCE-P (x)
"This will work in Genera 8x CLOSes: filters out entities that are flavor instances.
 Also filters out things that are defstruct instances."
(and (sys:instancep x)(not (flavor:find-flavor (type-of x) nil))))

(defun GET-SLOT-TYPE (S)
"This will work for Genera 8x CLOSses."
(clos:slot-definition-type s))

(defun GET-DIRECT-SLOTS (class-object)
""
(clos:class-direct-slots class-object))

(defun GET-SLOT-DOCUMENTATION (s)
""
(or (documentation s) ""))

(defun GET-SLOT-NAME (S)
"Method to get the name from a standard slot."
(clos::slot-definition-name s))

(defun SLOT-HAS-AN-INITFORM-P (slot-object)
  (clos::slot-definition-initform slot-object))

(defun GET-SLOT-READERS (slot-object)
  (clos::slot-definition-readers slot-object))

(defun GET-SLOT-WRITERS (slot-object)
  (clos::slot-definition-writers slot-object))

(defun GET-SLOT-NAMED (instance name)
(find-if #'(lambda (slot)(equal (get-slot-name slot) name))
	 (all-slots instance)))

(defun GET-SLOT-ALLOCATION (S)
"Method to get the type of allocation from a standard slot: oneof :CLASS or :INSTANCE."
(let ((alloc (clos::slot-definition-allocation s)))
(cond ((%classp alloc) :CLASS)
      ((member alloc '(:INSTANCE :CLASS)) alloc) 
      (T :INSTANCE))))

(defmethod GET-SLOT-INITFORM (s)
""
(when (slot-has-an-initform-p s)(clos::slot-definition-initform s)))

(defun %GET-SLOT-INITFORM (S)
"Method to create the iniform pair, if there is an initform value!"
(if  *save-contents-of-class-allocated-classes*
(when (and (equal (get-slot-allocation s) :CLASS)
	   (slot-has-an-initform-p s)) 	   
(list :initform (get-fasd-form (funcall (get-slot-reader s) s)))))
(when (slot-has-an-initform-p s)
(list :initform  (clos::slot-definition-initform s))))

(defun GET-SLOT-INITARGS (s)
(clos::slot-definition-initargs s))

(defun GET-SLOT-INITARG (s)
(first (clos::slot-definition-initargs s)))

) ;;; end of Genera 8x CLOS eval-when.

;;; Lucid CLOS eval when...

#+lucid
(eval-when (load eval compile)

(defun MAPARRAY (function array)
"like mapcar, but maps a function over each element of an
n-dim array: the function to be applied is a function of two args,
the count and the element value at aref count in the array."
(let* ((vec (sys:underlying-simple-vector array))
      (len (1- (length vec))))
(loop for count from 0 to len do (setf (aref vec count)
				       (funcall function count (aref vec count)))
  finally (return array))))				 

(defun FILL-ARRAY (array l)
"Fill n-dimensional array with values from list."
(let* ((vec (sys:underlying-simple-vector array))
       (len (1- (length vec)))
       (data (flatten l)))
(loop for index from 0 to len 
do (setf (aref vec index)(nth index data))
  finally (return array))))				 

(defun GET-SUPERCLASS-NAMES (class)
"Expects the object returned by FIND-CLASS."
(mapcar #'clos::class-name (clos::class-direct-superclasses class)))

(defun INSTANCE-P (x)
"Alternate def as a function for lucid 4.0."
(and (system:standard-object-p x)(not (system:classp x))))

(defun GET-SLOT-DOCUMENTATION (s)
""
(or (clos::slotd-documentation s) ""))

(defun GET-SLOT-NAME (S)
"Method to get the name from a standard slot."
(clos::slotd-name s))

(defun GET-SLOT-READERS (slot-object)
  (clos::slotd-readers slot-object))

(defun GET-SLOT-WRITERS (slot-object)
  (clos::slotd-writers slot-object))

(defun GET-SLOT-ALLOCATION (S)
"Method to get the type of allocation from a standard slot: oneof :CLASS or :INSTANCE."
(let ((alloc (clos::slotd-allocation s)))
(cond ((%classp alloc) :CLASS)
      ((member alloc '(:INSTANCE :CLASS)) alloc) 
      (T :INSTANCE))))

(defun %CLASSP (X)
"CLASSP is not exported in Lucid or EXCL, and is not defined at all in Genera!"
(clos::classp x))

(defmethod GET-SLOT-INITFORM (s)
""
(when (slot-boundp s 'clos::initform)
       (clos::slotd-initform s)))

(defun %GET-SLOT-INITFORM (S)
"Method to create the iniform pair, if there is an initform value!"
(if  *save-contents-of-class-allocated-classes*
(when (and (equal (get-slot-allocation s) :CLASS)
	   (slot-boundp s 'clos::initform))
(list :initform (get-fasd-form (funcall (get-slot-reader s) s))))
(when (slot-boundp s 'clos::initform)
      (list :initform  (clos::slotd-initform s)))))

(defun GET-SLOT-INITARGS (s)
(clos::slotd-initargs s))

(defun GET-SLOT-INITARG (s)
(first (clos::slotd-initargs s)))

(defun BUILTIN-CLASS-P (X)
  "Predicate to determine whether a class object is a builtin class. returns
   T if it is."
  (and (%classp x)(member (%class-name x)
  (mapcar #'first clos-system::built-in-classes) :test #'equal)))

) ;;; *** end of Lucid CLOS eval-when. ***

;;; *** Allegro non-MCL eval-when (e.g. on Suns.) ***

#+excl
(eval-when (load eval compile)

(defun GET-SLOT-DOCUMENTATION (s)
""
(or (documentation s) ""))

(defun GET-SLOT-READERS (slot-object)
  (clos::slotd-readers slot-object))

(defun GET-SLOT-WRITERS (slot-object)
  (clos::slotd-writers slot-object))

(defun GET-SLOT-ALLOCATION (S)
"Method to get the type of allocation from a standard slot: oneof :CLASS or :INSTANCE."
(let ((alloc (clos::slotd-allocation s)))
(cond ((%classp alloc) :CLASS)
      ((member alloc '(:INSTANCE :CLASS)) alloc) 
      (T :INSTANCE))))

(defun GET-SLOT-NAME (S)
"Method to get the name from a standard slot."
(clos::slotd-name s))

(defun %CLASSP (X)
""
(or (typep x 'clos::standard-class)(typep x 'clos::built-in-class)))

(defmethod GET-SLOT-INITFORM (s)
""
(when (slot-boundp s 'clos::initform)
       (clos::slotd-initform s)))

(defun %GET-SLOT-INITFORM (S)
"Method to create the iniform pair, if there is an initform value!"
(if  *save-contents-of-class-allocated-classes*
(when (and (equal (get-slot-allocation s) :CLASS)
	   (slot-boundp s 'clos::initform))
(list :initform (get-fasd-form (funcall (get-slot-reader s) s))))
(when (slot-boundp s 'clos::initform)
      (list :initform  (clos::slotd-initform s)))))

(defun GET-SLOT-INITARGS (s)
(clos::slotd-initargs s))

(defun GET-SLOT-INITARG (s)
(first (clos::slotd-initargs s)))

(defun BUILTIN-CLASS-P (X)
  "Predicate to determine whether a class object is a builtin class. returns
   T if it is."
(and (%classp x)(typep x 'clos::built-in-class)))

(defun INSTANCE-P (X)
"With the 4.0 series, structures are instances as well: exclude these."
(and (not (typep x 'clos::structure-class))
 (not (%classp x))(excl::standard-instance-p x)))

) ;;; *** end of non-MCL Allegro (like Sun Allegro) CLOS eval-when. ***

) ;;; end of clos eval-when...


;;; PCL Dependent functions & methods,,,

#+pcl
(eval-when (load eval compile)

(defun INSTANCE-NAME (instance)
  "returns the symbol naming the given class object."
  (pcl::class-name (pcl::class-of instance)))

(defun ALL-SLOTNAMES (instance &optional (all-allocations T))
  "returns the names of the slots in instance."
(let ((them (mapcar #'(lambda (slot)
			(pcl::slot-value slot 'pcl::name))
          (pcl::slots-to-inspect (pcl::class-of instance)
				 instance))))
(if all-allocations them 
	  (remove-if-not #'(lambda (slot)
          (equal (pcl::slotd-allocation slot) :instance))
			      them))))

(defun ALL-SLOTS (instance &optional (all-allocations T))
  "returns the names of the slots in instance."
(let ((them (pcl::slots-to-inspect (pcl::class-of instance)
				 instance)))
(if all-allocations them 
	  (remove-if-not #'(lambda (slot)
          (equal (pcl::slotd-allocation slot) :instance))
			      them))))

) ;;; *** END PCL EVAL-WHEN.... ***

;;; Independent.

(defun GET-CLASS-SUPERCLASSES (class)
"Returns a list of the NAMES (symbol list) of the direct superclasses of the class object."
(let ((the-ones (get-superclass-names class)))
  (if *supress-standard-object* (delete 'standard-object the-ones)
    the-ones)))

(defun GET-SLOT-READER (slot-object)
  (first (get-slot-readers slot-object)))

(defun GET-SLOT-WRITER (slot-object)
  (first (get-slot-writers slot-object)))

(defun ACCESSOR-EXISTS-P (S)
"Predicate: Returns T iff the slot has both a reader and a standard writer."
(let* ((readers (get-slot-readers s))
       (writers (get-slot-writers s))
       (accessors (some #'(lambda (writer)
			    (and (listp writer)
				 (equal (first writer) 'SETF)
				 (second writer)
				 (member (second writer) readers
					 :test #'equal)))
			writers)))
  accessors))

(defun GET-SLOT-ACCESSOR (s)
"Returns the first slot accessor alone."
(let ((val  (first (get-slot-readers s))))
(when (and val (accessor-exists-p s))
      val)))

(defun %GET-SLOT-NAME (S)
"Method to get the name from a standard slot."
(get-slot-name s))

(defun %GET-SLOT-ALLOCATION (S)
"Method to get the type of allocation from a standard slot: oneof :CLASS or :INSTANCE."
(let ((val  (get-slot-allocation s)))
(when val (list :allocation val))))

(defun %GET-SLOT-TYPE (S)
"Method to get the type from a standard slot.."
(list :type (get-slot-type s)))

(defun %GET-SLOT-INITARG (S)
"Method to get the first initarg found for the standard slot instance supplied."
(let ((val (or (first (get-slot-initargs s))
(if *use-default-class-initargs* (make-keyword (get-slot-name s))))))
(when val (list :initarg val))))

(defun %GET-SLOT-READER (slot)
"Method to determine whether to use an accessor or a reader. Does not splice
 into the fasd form if there is no reader defined."
(when (null (%get-slot-accessor slot))
  (let ((val  (GET-SLOT-reader slot)))
    (when val (list :reader val)))))

(defun %GET-SLOT-WRITER (slot)
"Method to determine whether to use an accessor or a writer. Does not splice
 into the fasd form if there is no writer defined."
(when (null (%get-slot-accessor slot))
  (let ((val (GET-SLOT-WRITER slot)))
      (when val (list :writer val)))))

(defun %GET-SLOT-DOCUMENTATION (S)
""
(list :documentation (or (GET-SLOT-DOCUMENTATION s) "")))

(defun %GET-SLOT-ACCESSOR (S)
""
(let ((val  (GET-SLOT-READER s)))
(when (and val (accessor-exists-p s))
      (list :accessor val))))

(defmethod METHODP ((thing null))
"NIL is not a method."
nil)

(defmethod METHODP ((thing t)) 
"Anything else is not a method."
  nil)

(defmethod MBOUNDP ((name symbol))
  "Predicate: returns t if this name is a method as opposed to a function/macro."
  (when (methodp name) T))

(defmethod MBOUNDP ((name null))
"vacuous case for NIL."
NIL)

(defmethod MBOUNDP ((name t))
  "Predicate: returns t if this name is a method as opposed to a function/macro."
  (when (methodp name) T))

(defun SLOT-DATA-AS-PLIST (slot)
"Generates the slot value pairs of the slot descriptor as a property list, 
 of course the name is stuck on the front."
(let ((name (get-slot-name slot))
      (initarg (get-slot-initarg slot))
      (accessor (get-slot-accessor slot))
      (initform (get-slot-initform slot))
      (type (get-slot-type slot))
      (documentation (get-slot-documentation slot))
      (allocation (get-slot-allocation slot)))
(if accessor
(list name :initarg initarg 
      :accessor accessor
      :initform initform
      :type type
      :documentation documentation
      :allocation allocation)
(list name :initarg initarg 
      :initform initform
      :type type
      :documentation documentation
      :allocation allocation))))

(defun CONSTRUCT-SLOT-SPEC (slot)
"The internal fasd-form constructor for slots."
(let ((name (%get-slot-name slot))
      (initarg-pair (%get-slot-initarg slot))
      (type-pair (%get-slot-type slot))
      (accessor-pair (%get-slot-accessor slot))
      (reader-pair (%get-slot-reader slot))
      (writer-pair (%get-slot-writer slot))
      (allocation-pair (%get-slot-allocation slot))
      (initform-pair (%get-slot-initform slot))
      (documentation-pair (%get-slot-documentation slot)))
  `(,name ,@initarg-pair
    ,@type-pair
    ,@accessor-pair
    ,@reader-pair
    ,@writer-pair
    ,@allocation-pair
    ,@initform-pair
    ,@documentation-pair)))

(defun GENERATE-CLASS-SLOT-FORM (slotd)
"Default method for rev4b --- seems to be defective...
 This one gets called by CLASS-FASD-FORM."
(construct-slot-spec slotd))

(defun SORT-ALLOCATED-SLOTS (class-object)
""
(let ((slots (class-slots class-object)))
(values
(remove-if-not #'(lambda (slot)(equal (get-slot-allocation slot)
				      :CLASS))
	       slots)
(remove-if-not #'(lambda (slot)(equal (get-slot-allocation slot)
				      :INSTANCE))
	       slots))))

;;;; Only allow class save, method save, and generic function save when this
;;; file is loaded: control this with :class-save on the features list.
;;; end of lucid eval-when.

;;; NOTE! Returned result from CLASS-DIRECT-SLOTS varies with the vendor!

#-(or :mcl lucid lispm)
(defun CLASS-SLOTS (class-object)
"Calls the clos internal function to compute class slot objects."
(clos::class-direct-slots class-object))

;;; CLOS/PCL independent class accessor methods.

(eval-when (load eval compile)

(defun DO-SPECIALIZER (spec)
"Map objects to class names."
(cond ((SYMBOLP SPEC) spec)
      ((%CLASSP SPEC)`(FIND-CLASS ',(%class-name spec)))
      (T SPEC)))

(defun DO-SPECIALIZERS (lst)
(loop for spec in lst collect (do-specializer spec)))

(defun FIND-GENERIC-FUNCTION (name)
"A function given the name of a supposed generic function,
 returns the function object if it exists, NIL otherwise."
(cond ((and (fboundp name)(%generic-function-p name))
       (symbol-function name))
      (T NIL)))

(defun GENERATE-CLASS-OPTIONS-FORM (class)
"Generates a fasd form for the default-initargs, metaclass,
 documentation components of a class object...."
(let ((default-initargs (get-class-default-initargs class))
      (metaclass (get-class-metaclass class)))
  (if default-initargs
      `((:default-initargs ,@default-initargs)
	,@metaclass
	(:documentation ,(or (get-class-documentation class) "")))
    `(,@metaclass
      (:documentation ,(or (get-class-documentation class) ""))))))

(defun GENERATE-CLASS-SLOT-FORMS (class)
"This generates fasd forms for all the slots in the class object."
(loop for slot in (class-slots class)
      collect (generate-class-slot-form slot)))

) ;;; end of class-save eval-when.


;;; Now, the Symbolics....

#+lispm
(defun HASH-TABLE-SIZE (x)
(scl:send x :size))

#+lispm
(defun HASH-TABLE-TEST (x)
(si:function-name (cli::test-function x)))

(defun PSEUDO-QUOTE-READER (stream subchar arg)
  "Reader to convert a function spec into a more parsable format."
  (declare (ignore subchar arg))
  (eval
   (list 'quote
	 (second (read-from-string 
		  (nsubstitute #\space #\#
       (concatenate 'string "(" 
	    (read-line stream t nil t) ")")
			       :test #'equal))))))

(defun MAKE-KEYWORD (x)
"Makes a keyword out of a symbol."
 (if (keywordp x) x (intern (symbol-name x) 'keyword)))

(defun NEWSYM (symbol)
  "Similar to GENSYM, but allows access to the gensym counter unlike pre-ANSI GENSYM."
  (if (null (get symbol 'namecounter))
      (setf (get symbol 'namecounter) 0))
  (read-from-string (concatenate 'string (string symbol)
  (format nil "~S" (incf (get symbol 'namecounter))))))

(defmethod COPY-INSTANCE ((instance T))
  "Provides shallow copying of any instance: returns a new copy of a given clos instance,
    writ as a method so youse gys can write ur own."
(let* ((copy (make-instance (instance-name instance)))
       (slots (all-slotnames instance)))
    (dolist (slot slots)
      (setf (slot-value copy slot)
	    (slot-value instance slot)))
    copy))

(defmethod ALL-SLOTS-AND-VALUES ((instance T))
"returns an alist of slot value pairs. NOTE: Each alist cell is a LIST, NOT a CONS!"
  (loop for slot in (all-slotnames instance) nconc
	(list slot (when (slot-boundp instance slot)
			 (slot-value instance slot)))))

(defun PRSLOT (key val &optional (stream *standard-output*))
"Simple function to be used by MAP-INSTANCE, printing out a slots key and value, ala
DESCRIBE."
(format stream "Key: ~a, Value: ~a~%" key val))

(defun MAP-INSTANCE (function instance &key (modify T)(concat nil))
"Iterator over the slots in an instance, ala MAPHASH. Takes a function of the
keyword/ value (2 arguments, not ONE!)."
(let* ((init (all-slots-and-values instance))
      (answer (loop with con = nil
      until (null init)
      as key = (pop init)
      as val = (pop init)
      as result = (funcall function key val)
      when concat do (setf con (append con (list result)))
      when modify do (setf (slot-value instance key) result)
      finally (return (if (null concat) instance (flatten1 con))))))
  answer))

(defun CLEAR-GLOBAL-VARS-AND-HTABS ()
"Initializes the SAVE-OBJECT enviroment for recording graph cycles."
	 (setf *structs-seen* nil *struct-vars* nil)
	 (setf *vectors-seen* nil *vector-vars* nil)
	 (setf *arrays-seen* nil *array-vars* nil)
	 (setf *htabs-seen* nil *htab-vars* nil)
	 (setf *seen* nil *vars* nil)
         (clrhash *save-object-hash-table*))

(defun CLEAR ()
"shorthand to clear the environment."
(clear-global-vars-and-htabs)
)

;;; The main routine, SAVE-OBJECT.

(defun SAVE-OBJECT (object-instance filename &key
				    (compile nil)
				    (variable '*db-input*)
				    (if-exists :append)
				    (print-pretty nil)
				    (max-print-level 10000000)
				    (package nil) 
				    (if-does-not-exist :create))
         (setf *global-instance-count* 0)
	 (setf *global-object-count* 0)
	 (clear-global-vars-and-htabs)
(let* ((*print-level*  max-print-level)
       (*print-circle* t)
       #+lispm (scl::*print-structure-contents* t)
       (*print-pretty* print-pretty)
       (*print-length* 50000000)
       (*package*      (or (and package (find-package package))
			   *package*))
	 (pathname       filename)
	 (form           (MAKE-LET-FORM object-instance 
					(get-fasd-form object-instance))))
    (setf (get '.%%SL%%. 'namecounter) 0)
(with-open-file (stream pathname :direction :output :if-exists if-exists
			:if-does-not-exist if-does-not-exist)
(format stream ";;;-*- Mode: Lisp; Base: 10; Syntax: Common-Lisp; Package: ~a -*-~%" (package-name *package*))
     (format stream "~%~s"
	     `(in-package ',(read-from-string (package-name *package*))))
     (write-global-header stream 
			  '.%%SL%%. 0
			  *global-instance-count*)
     (format stream "~%~s" `(setq ,variable ,form)))
    (format t "~& object saved to file: ~A" pathname)
    (when compile (format t "~% compiling file ~A" pathname)
	          (compile-file pathname)
		  (format t "~% done compiling file ~A" pathname))))

;;;================================
;;; ======= FASD forms. ===========
;;;================================

(defun STREAM-FASD-FORM (instance)
"Very machine dependent! for now, just recognize we got one, return NIL as FASD FORM."
(format t "Recognized a stream in save object: ~a.~%" instance)
NIL)

(defun GET-STRUCTURED-OBJECT-FASD-FORM (object)
"Routine which deals with any potentially circular objects (PCOS)."
(cond ((null object) NIL)
      ((%classp object)(class-fasd-form object))
       ((instance-p object)
	 (if (member object *seen* :test #'equal)
	     (symbol-fasd-form
	      (nth (position object *seen* :test #'equal) *vars*))
	   (progn (push object *seen*)
		  (setq *vars* (pushsym *vars*))
		  (instance-fasd-form object))))
      ((structure-p object)
       (if (member object *structs-seen* :test #'equal)
            (symbol-fasd-form (nth (position object *structs-seen*
						    :test #'equal)
					  *struct-vars*))
		   (progn (push object *structs-seen*)
		  (setf *struct-vars* (pushsym *struct-vars*))
		  (structure-fasd-form object))))
#|
      ((vectorp object)
       (if (member object *vectors-seen* :test #'equal)
            (progn (symbol-fasd-form (nth (position object *vectors-seen*
						    :test #'equal)
					  *vector-vars*)))
	   (progn (push object *vectors-seen*)
		  (setf *vector-vars* (pushsym *vector-vars*))
		  (vector-fasd-form object))))
      ((arrayp object)
       (if (member object *arrays-seen* :test #'equal)
            (progn (symbol-fasd-form (nth (position object *arrays-seen* :test #'equal)
				  *array-vars*)))
	   (progn (push object *arrays-seen*)
		  (setf *array-vars* (pushsym *array-vars*))
		  (array-fasd-form object))))
|#
      ((hash-table-p object)
       (if (member object *htabs-seen* :test #'equal)
	   (symbol-fasd-form  (nth (position object *htabs-seen* :test #'equal) *htab-vars*))
	   (progn (push object *htabs-seen*)
                  (setf *htab-vars* (pushsym *htab-vars*))
                  (setf *current-htab-size* (or (hash-table-size object) 5000))
                  (setf *current-htab-rehash-threshold* (or (hash-table-rehash-threshold object) 20))
                  (setf *current-htab-test* (hash-table-test object))
                  (setf *current-htab-rehash-size* (or (hash-table-rehash-size object) 67))
		  (htab-fasd-form object))))
      ((circular-list-p object)(circular-list-fasd-form object))
      (T (error "couldnt parse ~a as a structured object!" object))))

(defun %LOAD-HTAB  (htab &rest lst)
""
(loop while lst do (setf (gethash (pop lst) htab)(pop lst)) 
		   finally (return htab)))

(defun HTAB-FASD-FORM (htab)
"The dump form for hash tables."
  (let ((vals nil))
    (maphash #'(lambda (key value)
		 (push (GET-FASD-FORM value) vals)
                 (push (GET-FASD-FORM key) vals))
	     htab)
    `(%load-htab ,(get-instance-label htab) ,@vals)))

(defun GET-HTAB-VALUES (htab)
"Simple routine: makes an association list of ((key1 val1) .... (keyn valn))
where the vals are the FASD forms of the actual contents (values) of the hash table slot
contents."
(let ((values nil))
(maphash #'(lambda (key val)
	     (push (list `(QUOTE  ,key) (get-fasd-form val)) values))
	 htab)
values))

(defun SIMPLE-ARRAY-FASD-FORM (array)
"Numerical arrays are stored using this routine...."
`(make-array ,(get-fasd-form (array-dimensions array))
	     :element-type ',(array-element-type array)
	     :initial-contents ,(list-array array)))

(defun PACKAGE-FASD-FORM (package)
"assume its there in the environment, somewhere."
(let ((pn (get-fasd-form (package-name package))))
`(FIND-PACKAGE ,pn)))

(defun STREAM-P (x)
"Avoids problems with vendor-made type confusion."
(and (not (%classp x))(streamp x)))

#+ignore
(defun BYTE-SPECIFIER-FASD-FORM (instance)
""
`(BYTE ,(get-fasd-form instance)))

;;; the workhorse. NOTE: The case statement is very ORDER-DEPENDENT!
;;; If your version of CLOS supports specialization on ALL LISP types,
;;; you could write this as a set of FASD-FORM methods on the LISP types.
;;; This has not always been possible with PCL, thus the case statement.
;;; NOTE that a CONS is not necessarily a list! CONS-P distinguishes 
;;; between items such as (CONS 'A 'B) and (LIST 'A 'B).
;;;
;;; Notice that this version uses SAFE-CLASS-FASD-FORM to prevent class
;;; definition overwrite. Use CLASS-FASD-FORM below if you do not want this 
;;; behavior!

(defun INSURE-LIST (X)
(if (listp x) x (list x)))

(defun GET-FASD-FORM (instance)
"New incarnation of get-fasd-form: if the instance is a structured
object, construct a representation for it anticipating that it might
be a PCO. NOTE: in MCL Common Lisp, note that STREAMS are implemented as
CLASSES! This makes it possible to SAVE-OBJECT things like *TERMINAL-IO*!"
    (cond ((null instance) nil)
	  ((equal instance T) T)
	  ((numberp instance) instance)
	  ((or (pathnamep instance)
	       (stringp instance)
               (keywordp instance)
               (special-marker-p instance)
	       (characterp instance)) instance)
          ((packagep instance)(package-fasd-form instance))
	  ((quoted-symbol-p instance)(quoted-symbol-fasd-form instance))
	  ((symbolp instance)(symbol-fasd-form instance))
	  ((and (arrayp instance)(not (pco-p instance)))
                                 (simple-array-fasd-form instance))
	  ((vectorp instance)(vector-fasd-form instance))
	  ((pco-p instance)(get-structured-object-fasd-form instance))
	  ((arrayp instance)(array-fasd-form instance))
	  ((functionp instance)(compiled-function-fasd-form instance))
          ((stream-p instance)(stream-fasd-form instance))
          ((readtablep instance)(readtable-fasd-form instance))
	  ((quoted-list-p instance)(quoted-list-fasd-form instance))

          ((LISTP instance)
#|
	 `( ,(mapcar #'(lambda (thing)
		 (if (quoted-list-p thing)
		      (quoted-list-fasd-form thing)
		      (get-fasd-form thing)))
			  instance)))
|#

	 `(list ,@(mapcar #'(lambda (thing)
			      (get-fasd-form thing)) instance)))

	  (T (error "could not parse object ~a, of type ~a.~%"
		    instance (type-of instance)))))

(defun STRIP-PACKAGE (x)
"strip the package designator off the symbol, return the rest,
if keyword, return self.."
(if (keywordp x) x
  (intern (symbol-name x))))

(defun SLOT-EXISTS-P-ANY (instance name)
"returns t if the slotname exists with any package designator."
(let ((slots (mapcar #'strip-package (all-slotnames instance))))
  (member (strip-package name) slots :test #'equal)))

(defun QUOTED-SYMBOL-P (X)
"Predicate: returns t if the object is a quoted symbol."
(and (listp x)(equal (first x) 'quote)(symbolp (second x))))

(defun QUOTED-SYMBOL-FASD-FORM (instance)
"A FASD form for a quoted symbol."
`(QUOTE ,(second instance)))

(defun PAIR-SYMBOLS (instance)
(let ((slots (all-slotnames instance)))
 (pairlis (mapcar #'strip-package slots) slots)))

(defun FIND-PACKAGED-SLOTNAME (instance stripped)
"given the slotname without package, find the slotname WITH package."
  (let* ((choices (pair-symbols instance)))
    (rest (assoc stripped choices :test #'equal))))

(defun SLOT-VALUE-ANY (instance stripped)
"find the value of the real slot given the stripped name."
(setf stripped (strip-package stripped))
(let ((slotname (find-packaged-slotname instance stripped)))
  (when slotname (slot-value instance slotname))))

(defun GET-UNSAVEABLE-SLOTNAMES (instance)
  "Returns a list of the slotnames in instance, or the slotnames in the
   class of instance, which should not be saved."
  (slot-value-any instance 'unsaveable))

;;;; ===================================== TESTS ============================================

;;; *** NASTY TEST SUITE: A collection of self-referencing consolas that put this code to the test!

#+lispm
(defvar *nasty-path* "e:>kerry>")

#+:mcl
(defvar *nasty-path* "Macintosh HD:")

#+:mcl
(setf *nasty-path* "Macintosh HD:")

#+lucid
(defvar *nasty-path* "/users/kerry/save-object/tests/")

#+excl
(defvar *nasty-path* "/users/kerry/save-object/tests/")

(defun NASTY-PATH (filename)
(concatenate 'string *nasty-path* filename))

(defun LESS-NASTY-INSTANCE-TEST ()
"One instance with one self-reference."
(let ((a (make-instance 'test)))
  (setf (slot-value a 'a) a)
  (setf (slot-value a 'b) a)
  (save-object a (NASTY-PATH "little-instance.lisp"))))

(defvar *a)
(defvar *b)
(defvar *c)
(defvar *d)
(defvar *the-nasties* nil "stored here for later review.")

(defclass BOGON ()
((name :initarg :name
       :accessor bogon-name
       :documentation ""))
(:default-initargs :name "")
(:documentation ""))

(defmethod INITIALIZE-INSTANCE :AFTER ((self bogon) &rest plist)
(declare (ignore plist))
(push self *the-nasties*))

#-lispm
(defmethod PRINT-OBJECT ((self bogon) stream)
(with-slots (name) self
(format stream "#<Test Instance ~A>" name)))

#+lispm
(defmethod PRINT-OBJECT ((self bogon) stream)
(with-slots (name) self
(format stream "#<~A: ~A>" name (si:%pointer self))))

(defclass TEST (bogon)
((a :initarg :a)
 (b :initarg :b)
 (c :initarg :c)
 (name :initarg :name))
(:default-initargs :a nil
		   :b nil
		   :c nil
		   :name "")
(:documentation "Simple test class for the examples below."))

(defstruct foo a b c)

(defun NASTY-STRUCT-TEST ()
(let ((a (make-foo)))
(setf (foo-a a) a)
(save-object a (NASTY-PATH "nnn.lisp"))))

(defun GET-NASTY (filename)
(concatenate 'string *nasty-path* filename ".lisp"))

#+lispm
(defun NASTY-ARRAY-TEST ()
""
(let ((them! nil))
(tv:noting-progress ("Nasty Array Allocation!")
 (setf them! (LIST (make-array '(50 20 36) :element-type 'float
			 :initial-element PI)
		   (make-array 10 :initial-contents (make-list 10
						       :initial-element "STRINGS!"))
		   (make-array '(21 16 33 4) :element-type '(unsigned-byte 32)
			       :initial-element 1024)
		   (make-array '(20 20) :element-type 'character
			       :initial-element #\!)))
)
(tv:noting-progress ("Nasty Array Storage!")
(save-object them! (NASTY-PATH "horrid-arrays.lisp")))))

#-lispm
(defun NASTY-ARRAY-TEST ()
""
(let ((them! (LIST (make-array '(50 20 36) :element-type 'float
			 :initial-element pi)
		   (make-array 10 :initial-contents (make-list 10
					       :initial-element "STRINGS!"))
		   (make-array '(21 16 33 4) :element-type '(unsigned-byte 32)
			       :initial-element 666)
		   (make-array '(20 20) :element-type 'character
			       :initial-element #\!))))
(save-object them! (NASTY-PATH "horrid-arrays.lisp"))))

(defun NASTY-INSTANCE-TEST ()
  ""
(setf *a (make-instance 'test :name "A")
       *b (make-instance 'test :name "B")
       *c (make-instance 'test :name "C"))
(setf (slot-value *a 'a) *b)
(setf (slot-value *a 'b) *c)
(setf (slot-value *b 'a) *a)
(setf (slot-value *b 'b) *c)
(setf (slot-value *c 'a) *b)
(setf (slot-value *c 'b) *a)
(save-object *a (NASTY-PATH "nasty-inst.lisp")))

(defun BOBS-INSTANCE-TEST ()
  " A ----> B ----> C ----> D
    ^       ^       |       V
    |-------+--------       |
            |----------------   "

(setf *a (make-instance 'test :name "A")
       *b (make-instance 'test :name "B")
       *c (make-instance 'test :name "C")
       *d (make-instance 'test :name "D"))

(setf (slot-value *a 'a) *b)
(setf (slot-value *b 'a) *c)
(setf (slot-value *c 'a) *d)
(setf (slot-value *c 'b) *a)
(setf (slot-value *d 'a) *b)

(save-object *a (NASTY-PATH "bobtest.lisp")))

(defun BOBS-INSTANCE-TEST-2 ()

"BACKPOINTERS ON ALL THE PREVIOUS EXAMPLES
 (two links on each node: twice as many as before, ten.)

    A <---> B <---> C <---> D
    ^       ^       V       V
    V--<>---+-------^       |
            V----<>---------^   "		

(setf *a (make-instance 'test :name "A")
       *b (make-instance 'test :name "B")
       *c (make-instance 'test :name "C")
       *d (make-instance 'test :name "D"))

(setf (slot-value *a 'a) *b)
(setf (slot-value *a 'b) *c)

(setf (slot-value *b 'a) *c)
(setf (slot-value *b 'b) *d)
(setf (slot-value *b 'c) *a)

(setf (slot-value *c 'a) *d)
(setf (slot-value *c 'b) *a)
(setf (slot-value *c 'c) *d)

(setf (slot-value *d 'a) *b)
(setf (slot-value *d 'b) *c)
(save-object *a (NASTY-PATH "bobtest2.lisp")))

(defvar *nasty-hash-tables* nil)

(defun NASTY-HASH-CHAIN-TEST ()
" Makes a nested hash table net like this:

     a->b  b->c  c->d  d->e   e->a
    a-----b-----c-----d----->e----->|
    ^                               v
    |<---------------------<---------
      e->a      e->a       e->a   e->"

(setf *nasty-hash-tables* nil)
(let* ((a (make-hash-table))
       (b (make-hash-table))
       (c (make-hash-table))
       (d (make-hash-table))
       (e (make-hash-table)))
(pushnew a *nasty-hash-tables*)
(pushnew b *nasty-hash-tables*)
(pushnew c *nasty-hash-tables*)
(pushnew d *nasty-hash-tables*)
(pushnew e *nasty-hash-tables*)
(setf (gethash 'a->b b) a)
(setf (gethash 'b->a a) b)
(setf (gethash 'b->c c) b)
(setf (gethash 'c->b b) c)
(setf (gethash 'c->d d) c)
(setf (gethash 'd->e e) d)
(setf (gethash 'e->a a) e)
(save-object a (NASTY-PATH "qhash.lisp"))))

;;; END OF THE NASTIES.

(defun FLATTEN1 (cells)
(loop for cell in cells nconc cell))

(defun GET-SLOT-VALUES (clos-instance)
  "given a pcl/clos instance,constructs a plist of all the saveable
   slot/value pairs."
  (incf *global-instance-count*)
  (let ((unsaveable (get-unsaveable-slotnames clos-instance)))
(map-instance #'(lambda (key val)
		  (unless (or (member key unsaveable :test #'equal)
			      (member key *global-unsavable-slots* :test #'equal))
		  (list (make-keyword key)(get-fasd-form val))))
	      clos-instance
	      :modify nil
	      :concat t)))

(defun MAKEVAR (&optional (label '.%%SL%%.))
"makes a new variable for something in the global object hashtable."
(incf *global-object-count*)
(newsym label))

(defun PUSHSYM (list &optional (label '.%%SL%%.))
"label must match with special-marker-p, and must be upper-case."
  (push (newsym label) list))

(defun MAKESYMS (symbol min max &optional (pkg *package*))
(loop for count from min to max do (eval `(defvar
,(read-from-string (concatenate 'string (format nil "~A" symbol)
				(format nil "~A" count))
  pkg)))))

(defun WRITE-GLOBAL-HEADER (stream symbol min max
	   &optional (pkg-name (package-name *package*)))
(format stream (format nil "~%(EVAL-WHEN (COMPILE LOAD EVAL)
                       (DATABASE:MAKESYMS '~A ~A ~A ~s))~%"
		       symbol min max pkg-name)))

;;; Predicates....

#+ignore
(defun BYTE-SPECIFIER-P (X)
 ""
(typep x 'byte-specifier))

(defun NASSOC (key list &key (test #'equal))
"Given a key and a list, return the thing AFTER that key in the list.
 Similar to GETF."
  (let ((where (position key list :test test)))
    (when where (nth (1+ where) list))))

(defun CONS-P (x)
  "ingenious predicate for testing whether something is a cons cell vs. a list.
   note that this returns nil for (LIST 'A 'B) whereas it returns T for (CONS 'A 'B)."
  (and (listp x) (null (listp (rest x)))))

(defun CONS-FASD-FORM (item)
  `(CONS ,(get-fasd-form (first item))
	 ,(get-fasd-form (rest item))))

(defun LIST-ARRAY (array)
  (list-array-aux array 0 nil))

(defun LIST-ARRAY-AUX (array level subscript-list)
  (let ((new-level (1+ level))
	(dims (array-dimensions array)))
    (loop for i from 0 to (1- (nth level dims))
	  collect
	  (cond ((equal level (1- (length dims)))
		 (let* ((aref-arg-list
			 (cons array (append subscript-list
					     (list i))))
			(array-val (apply #'aref aref-arg-list)))
		   (if (numberp array-val) array-val
		     (get-fasd-form array-val))))
		(T (list-array-aux array new-level
				   (append subscript-list (list i)))))
	  into temp finally (return (append '(list) temp)))))

(defun COERCE-2D-ARRAY (2d-array)
  (let ((rows (array-dimension 2d-array 0))
	(cols (array-dimension 2d-array 1)))
    (loop for x from 0 to (1- rows) collect
	  (loop for y from 0 to (1- cols) collect
		(aref 2d-array x y)) into answers
	  finally (return answers))))

(defun ARRAY-FASD-FORM (array)
  "this function return a make-array form."  
  (setf *print-array* T)
   (let ((vals (list-array array)))
  `(let ((tmp (allocate-array ,(get-fasd-form (array-dimensions array))
	       :element-type ',(array-element-type array)
	       :adjustable ,(adjustable-array-p array)
	       :initial-contents ,(get-fasd-form vals))))
     TMP)))

(defun VECTOR-FASD-FORM (array)
  "this function return a make-array form."  
  (setf *print-array* T)
   (let ((vals (list-array array)))
  `(let ((tmp (allocate-array ,(get-fasd-form (array-dimensions array))
	       :element-type ',(array-element-type array)
	       :adjustable ,(adjustable-array-p array)
	       :initial-contents ,(get-fasd-form vals))))
     TMP)))

;;; HASH TABLES...

(defun CREATE-HASH-TABLE (&key (test #'eql)
			       (size 67)
			       (rehash-size nil)
			       (rehash-threshold nil))
(let ((args (remove nil `(:size ,(get-fasd-form size)
	 :test ,test
		,@(when rehash-size (list :rehash-size (get-fasd-form rehash-size)))
		,@(when rehash-threshold
		    (list :rehash-threshold (get-fasd-form rehash-threshold)))))))
      (cache-object (apply #'make-hash-table args) :mode :load)))

;;; Vendor independent struct functions...

(defun MAPSTRUCT (fun struct)
"Iterator for defstruct instances."
(loop for slotname in (get-struct-slotnames-from-struct struct)
      do (set-defstruct-value struct slotname
			      (funcall fun slotname
				       (get-defstruct-value struct slotname)))
      finally (return struct)))

#+lispm
(defun SET-DEFSTRUCT-VALUE (object slotname newval)
"SETF (SLOT-VALUE <structure instance> '<slotname>) newval apparently
works properly."
(setf (slot-value object (make-sym slotname)) newval))

(defun MAKE-SYM (x)
(if (keywordp x)
    (read-from-string (subseq (symbol-name x) 0))
  x))

(defun GET-EVENS (l)
  "Get the even elements out of a list, ccoerce them into a new list."
(loop for count from 0 to (1- (length l)) 
      when (evenp count) collect (nth count l) into evens
      finally (return evens)))

(defun GET-ODDS (l)
  "Get the odd  elements out of a list, ccoerce them into a new list."
(loop for count from 0 to (1- (length l))
when (oddp count) collect (nth count l) into odds
finally (return odds)))

(defun LOAD-HTAB (values &key (test #'eql)(size 67)
		  (rehash-size nil)
		  (rehash-threshold nil))
""
(let ((htab (create-hash-table :test test :size size :rehash-size rehash-size
			       :rehash-threshold rehash-threshold)))
(loop for cell in values as key = (first cell) as val = (eval (second cell))
do (setf (gethash key htab) val))))

(defvar *debug-struct-save* nil)

(defun FILL-OBJECT (object values)
""
(let ((*print-circle* nil)(*print-level* 1000000)
      (*print-pretty* nil)
#+lispm(scl::*print-structure-contents* nil))
(cond ((structure-p object)
       (loop with sns = (get-evens values)
	     with vals = (get-odds values)
	     while (and sns vals) 
	     as a = (first sns) as b = (first vals) do
(when *debug-struct-save* (format t "slotname = ~a, value = ~a~%" (first sns)(first vals)))
     (pop sns)(pop vals)
	     (when (and sns vals) (set-defstruct-value object a b))
	     finally (return object)))
      ((hash-table-p object)(load-htab values :size (hash-table-size object)
				       :rehash-threshold (hash-table-rehash-threshold
							   object)
				       :rehash-size (hash-table-rehash-size object)
				       :test (hash-table-test object)))
      ((vectorp object)
       (loop for count from 0 to (1- (array-dimension object 0))
	     do (setf (aref object count)(nth count values))
	     finally (return object)))
      ((arrayp object)
       (fill-array object values))
      (T object))))

;;;====================================ALLEGRO (version 4.1 beta)  DEFSTRUCT ==========================================

#+excl
(eval-when (load eval compile)

(defun STRUCTURE-FASD-FORM (instance)
"for EXCL: saves out a structure."
`(fill-struct ,(get-instance-label instance)
	      ',@(get-defstruct-values instance)))

(defun  GET-SLOT-NAMED (instance name)
""
(dolist (x (%get-defstruct-slots instance))
  (when (equal name (slot-value x 'excl::name))
        (return-from get-slot-named x))))

(defun GET-DEFSTRUCT-SLOT-ACCESSOR (instance slotname)
""
(when (null instance)(error "~a was not a structure instance!" instance))
(slot-value (get-slot-named instance slotname) 'excl::accessor))

(defun SET-DEFSTRUCT-VALUE (instance slotname value)
(let ((acc (get-defstruct-slot-accessor instance slotname)))
(when (null acc)(error "accessor cannot be NIL! for thing ~A, for slotname ~a.~%"
instance slotname))
 (eval `(setf (,acc ,instance) ,value))))

(defun %STRUCTURE-P (x)
""
(cond ((and (symbolp x)(find-class x nil)
	    (equal (class-name (find-class x)) 'clos:structure-class)) T)
      ((typep x 'clos:structure-class) T)
      (T NIL)))

(defun FILL-STRUCT (struct vals)
(loop for slotname in (%get-defstruct-slotnames struct) do
  (set-defstruct-value struct slotname (pop vals)))
 struct)

(defun GET-DEFSTRUCT-TYPE (s)
"given defstruct instance s, return type of s."
(excl::structure-ref s 0))

(defun GET-DEFSTRUCT-DESCRIPTION (defstruct-name)
"Given name, return description object."
(nassoc 'excl::%structure-definition (symbol-plist defstruct-name)))

(defun GET-DEFSTRUCT-SLOTS (description)
"Given defstruct description, return slot objects."
(excl::structure-ref description 3))

(defun GET-DEFSTRUCT-NAME (s)
(slot-value s 'excl::name))

(defun %GET-DEFSTRUCT-SLOTS (instance)
(get-defstruct-slots (get-defstruct-description (get-defstruct-type instance))))

(defuN GET-STRUCT-SLOTNAMES-FROM-STRUCT (struct)
(get-defstruct-slotnames (get-defstruct-slots
 (get-defstruct-description (get-defstruct-type struct)))))

(defun GET-DEFSTRUCT-SLOTNAMES (slot-list)
"Given list of slot objects, return slotnames as keywords."
(mapcar #'make-keyword (mapcar #'(lambda (x)(excl::structure-ref x 1))
			       slot-list)))

(defun %GET-DEFSTRUCT-SLOTNAMES (slot-list)
"Given list of slot objects, return slotnames as symbols."
(if (listp slot-list)
 (mapcar #'(lambda (x)(excl::structure-ref x 1)) slot-list)
(let ((the-ones	(get-defstruct-slots (get-defstruct-description
				       (get-defstruct-type slot-list)))))
(mapcar #'get-defstruct-name (if (listp the-ones) the-ones (list the-ones))))))

(defun DEFSTRUCT-SLOT-COUNT (instance)
""
(length (delete nil (get-defstruct-slotnames
		      (get-defstruct-slots 
			(get-defstruct-description
			  (get-defstruct-type instance)))))))

(defun GET-DEFSTRUCT-VALUES (instance)
(let ((length (defstruct-slot-count instance)))
(loop for c from 1 to length collect (excl::structure-ref instance c))))

(defun GET-DEFSTRUCT-SLOTS-AND-VALS (instance)
""
(let* ((type (get-defstruct-type instance))
       (desc (get-defstruct-description type))
       (slots (get-defstruct-slots desc))
       (slotnames (DELETE NIL (get-defstruct-slotnames slots)))
       (len (length slotnames))
       (values (get-defstruct-values instance)))
(loop for count from 0 to (1- len)
 append (list (nth count slotnames)
(get-fasd-form (nth count values))))))

(defun GET-DEFSTRUCT-VALUE (instance slotname)
(slot-value instance (make-sym slotname)))

(defun STRUCTUREP (x)
"Predicate to test whether some object is a structure instance."
(excl::structurep x))

(setf (symbol-function 'structure-p) #'structurep)

) ;;; end of acl4.1/acl4.0

;;;; eval-when...

;;;==================================== MCL ALLEGRO (MACINTOSH)  DEFSTRUCT ==========================================

;;; Beginning of the MAC 2.0 Beta: (MCL Common LISP?)

#+:mcl
(eval-when (eval compile load)

(defun GET-SYMBOLS-DEFSTRUCT-SPEC (symbol)
"If the symbol names a defstruct, it returns that defstructs spec."
(declare (ignore symbol))
nil)

(defun FILL-STRUCT (struct vals)
"Fills up the structure instance: invoked by STRUCTURE-FASD_FORM, GET-FASD-FORM"
(loop for slotname in (get-defstruct-slots struct) do
  (set-defstruct-value struct slotname (pop vals)))
 struct)

(defun ALLOCATE-STRUCT (name)
"Function to allocate the empty husk of a defstruct."
(apply (get-defstruct-constructor name) nil))

(defun GET-DEFSTRUCT-NAME (instance)
""
(type-of instance))
  
  (defun GET-DEFSTRUCT-TYPE (instance)
""
    (car (ccl::struct-ref instance 0)))
  
  (defun SET-DEFSTRUCT-VALUE (instance slotname value)
""
(when (null instance)(error "set-defstruct-value got NULL instead of an instance!"))
    (let* ((struct-slot-value-list (inspector::structure-slots instance))
           (slotname-position (1+ (position slotname 
                                            struct-slot-value-list 
                                            :key #'first))))
(when (null slotname-position)(error "couldnt find slotname ~a in ~a!" slotname instance))
      (ccl::struct-set instance slotname-position value)))

(defun GET-DEFSTRUCT-VALUES (instance)
"Return a supposedly ordered list of the defstruct instances values."
(get-odds (get-defstruct-slots-and-vals instance)))
  
  (defun GET-DEFSTRUCT-VALUE (instance slotname)
    "Given an instance of a defstruct, and the name of some slot, return the slots value."
    (let* ((struct-slot-value-list (inspector::structure-slots instance))
           (slotname-position (1+ (position slotname 
                                            struct-slot-value-list 
                                            :key #'first))))
      (ccl::struct-ref instance slotname-position)))
  
  (defsetf get-defstruct-value set-defstruct-value)

(defun GET-DEFSTRUCT-SLOT-ACCESSOR (instance slotname)
    ""
    (let* ((id (GET-DEFSTRUCT-TYPE instance)))
      (read-from-string (concatenate 'string (symbol-name id) "-"
				     (symbol-name slotname)))))
  
(defun GET-DEFSTRUCT-SLOTS-AND-VALS (instance)
  "Return a list of slots and values" ;Note that slots are not keyword names
  (labels ((interlock-lists (list1 list2 &optional interlocked-list)
                            (if (and list1 list2)
                              (cons (car list1) 
                                    (cons (car list2) 
                                          (interlock-lists (rest list1)
                                                           (rest list2)
                                                           interlocked-list)))
                              interlocked-list)))
    (let* ((struct-slot-value-list (inspector::structure-slots instance))
           (slot-list (mapcar #'first struct-slot-value-list))
           (vals-list '()))
      (dotimes (i (length slot-list))
        (push (ccl::struct-ref instance (1+ i)) vals-list))
      (setf vals-list (nreverse vals-list))
      (interlock-lists slot-list vals-list))))

(defun GET-DEFSTRUCT-SLOTS (instance)
  "Return a list of slots" ;Note that slots are not keyword names
  (let* ((struct-slot-value-list (inspector::structure-slots instance)))
    (mapcar #'first struct-slot-value-list)))

(setf (symbol-function 'get-struct-slotnames-from-struct) #'get-defstruct-slots)

(defun STRUCTURE-FASD-FORM (instance)
`(fill-struct ,(get-instance-label instance)
	      ',(get-defstruct-values instance)))

(defun BUILTIN-CLASS-p (X)
"Predicate to determine whether a class object is a built-in class: this should be the generic definition of this one."
(typep x 'built-in-class))

  
  ) ;;;; *** END OF THE MAC EVAL-WHEN.... ****

;;;  out to pasture....

#+ignore 
(eval-when (compile eval load)
#+clos
(defun STRUCTURE-TYPE-P (type)
  (let ((s-data (gethash type lucid::*defstructs*)))
 (and s-data (eq 'structure (system::structure-ref s-data 1 'defstruct)))))

(defun STRUCTURE-TYPE-INCLUDED-TYPE-NAME (type)
  (let ((s-data (gethash type lucid::*defstructs*)))
    (and s-data (system:structure-ref s-data 6 'defstruct))))

)

;;;====================================LUCID DEFSTRUCT ==========================================
;;; Beginning of the Lucid eval-when....

#+lucid
(eval-when (eval compile load)

(defun ALLOCATE-STRUCT (name)
"Function to allocate the empty husk of a defstruct."
(apply (get-defstruct-constructor name) nil))

(defun COMPILED-FUNCTION-FASD-FORM (X)
"fasd form for hashmark-quote e.g. (FUNCTION name) forms."
  `(function ,(get-compiled-function-name x)))

(defun FUNCTION-NAME (x)
"The 1th slot of the procedure struct is the function name in Lucid 4.0.
 i.e. SYS:PROCEDURE-SYMBOL <X>. SYS:PROCEDURE-SYMBOL is a constant, representing the
index to the function name within the procedures slots. (see wizard doc for 4.0 lucid."
(when (sys:procedurep x)(sys:procedure-ref x SYS:PROCEDURE-SYMBOL)))

(defun GET-COMPILED-FUNCTION-NAME (compiled-function)
""
(function-name compiled-function))

;;; Long-list functions for Lucid.

(defun LONG-LIST-FASD-FORM (instance)
`(nconc ,@(make-list-forms (partition-long-list instance))))

(defun MAKE-LIST-FORMS (lists)
  (loop for list in lists collect (get-fasd-form list)))

(defun PARTITION-LONG-LIST (long-list &optional (limit 512))
"Some LISPs have a limit on the number of list components: this function partitions a
 list longer than the supplied limit appropriately for saving to file."
(loop while long-list collect
      (loop for count from 0 to (- limit 2) while long-list
	    collect (pop long-list))))

(defun GET-DEFSTRUCT-CONSTRUCTOR (name)
"Extracts the name of the constructor function from the instance name."
(third (multiple-value-list (system::defstruct-info name))))

(defun STRUCTURE-P (x)
"Predicate to determine if something is a structure instance:
 NOTE: there is overlap of types in Lucid, in this case hash tables."
  (and (typep x 'structure)
       (NOT (VECTORP X))
       (not (hash-table-p x))
       (not (typep x 'simple-vector))
       (not (typep x 'simple-array))
        (not (and (arrayp x)(> (array-rank x) 1)))))

#+ignore
(defun STRUCTURE-P (X)
"Predicate to determine if something is a structure instance."
(and (structurep x)(not (hash-table-p x))(not (instance-p x))))

(setf (symbol-function 'structurep) #'structure-p)

#+pcl
(defun STRUCTURE-TYPE-P (type)
  (let ((s-data (gethash type lucid::*defstructs*)))
 (or (and s-data (eq 'structure (system::structure-ref s-data 1 'defstruct)))
	(eq pcl::*structure-type* type))))

(defun STRUCTURE-SLOTD-NAME (slotd)
  (first slotd))

(defun STRUCTURE-SLOTD-READER (slotd)
  (second slotd))

(defun STRUCTURE-SLOTD-WRITER (slotd)
  (third slotd))

(defun SET-DEFSTRUCT-VALUE (instance slotname value)
(EVAL `(setf (,(get-defstruct-slot-accessor instance slotname) ,instance) ,value)))

(defun GET-DEFSTRUCT-VALUE (instance slotname)
"Given an instance of a defstruct, and the name of some slot, return the slots value."
(apply (get-defstruct-slot-accessor instance slotname)(list instance)))

(defsetf get-defstruct-value set-defstruct-value)

(defun GET-DEFSTRUCT-SLOT-LOCATION (i name)
(position name (nreverse (get-defstruct-slotnames i))))

(defun GET-DEFSTRUCT-SLOT-ACCESSOR (instance slotname)
""
(let* ((id (type-of instance))
       (answer nil))
  (multiple-value-bind (a accessor b c d)
  (system:defstruct-slot-info id (get-defstruct-slot-location instance slotname))
  a b c d
 (setf answer accessor)
answer)))

(defun FILL-STRUCT (struct vals)
(loop for slotname in (get-defstruct-slotnames struct) do
  (set-defstruct-value struct slotname (pop vals)))
 struct)

(defun GET-DEFSTRUCT-SLOTS-AND-VALS (i)
(loop for name in (get-defstruct-slotnames i) collect
	       (cons name (get-defstruct-value i name))))

(defun GET-DEFSTRUCT-VALUES (s)
""
(loop for name in (get-defstruct-slotnames s) collect
 (get-defstruct-value s name)))

(defun GET-DEFSTRUCT-SLOTNAMES (i)
  (let ((id (type-of i)))
  (multiple-value-bind (indices a b c)
   (system:defstruct-info ID)
      (declare (ignore a b c))
      (let ((answers nil))
	(dotimes (count indices answers)
	  (multiple-value-bind (name d value e f)
	      (system:defstruct-slot-info ID count)
	      (declare (ignore value d e f))
	      (push name answers)
	      answers))))))

(setf (symbol-function 'get-struct-slotnames-from-struct) #'get-defstruct-slotnames)

(defun %STRUCTURE-P (symbol)
""
(system:defstruct-info symbol))

(defun %CLASS-NAME (class-object)
""
(clos::class-name class-object))

) ;;; *** END LUCID EVAL-WHEN ***

;;;====================================SYMBOLICS DEFSTRUCT ==========================================
;;; beginning of symbolics eval-when....

#+Lispm
(eval-when (load eval compile)

(defun ALLOCATE-STRUCT (name)
"Function to allocate the empty husk of a defstruct."
(apply (get-defstruct-constructor name) nil))

(defun FILL-STRUCT (struct vals)
(loop for slotname in (get-defstruct-slotnames struct) do
  (set-defstruct-value struct slotname (pop vals)))
 struct)

(defun ALL-SLOTS (instance)
"Gets all the slots from the instances class, whether inherited or not."
(clos::class-slots (clos::class-of instance)))

(defun %CLASS-NAME (x)
""
(if (instance-p x)(clos::class-name (clos::class-of x))
(clos::class-name x)))

(defun GET-SUPERCLASS-NAMES (class)
""
(mapcar #'clos::class-name (clos::class-direct-superclasses class)))

(defun GET-DEFSTRUCT-CONSTRUCTOR (defstruct-name)
"Derived from defstruct sources genera 8.1: file:sys:sys2;struct.lisp."
(si:get-defstruct-constructor-macro-name defstruct-name))

(defun GET-DEFSTRUCT-NAME (struct-instance)
""
(if (typep struct-instance 'list)
 (progn  (warn "~a has a type which is not parsable == ~a.~%"
  struct-instance (type-of struct-instance)) nil)
 (nth 10 (si:get-defstruct-description (type-of struct-instance)))))

(defun GET-DEFSTRUCT-NAME (struct-instance)
""
(type-of struct-instance))

(defun GET-DEFSTRUCT-SLOT (struct-instance slotname)
(let ((structure-name (get-defstruct-name struct-instance)))
		 (cdr (si:assq slotname (si:defstruct-description-slot-alist
	(si:get-defstruct-description structure-name))))))

(defun SET-DEFSTRUCT-VALUE (struct-instance slotname value)
(let ((slot (get-defstruct-slot struct-instance slotname)))
(EVAL `(setf (,(first (reverse slot)) ,struct-instance) ,value))))

(defun STRUCTURE-P (X)
(cli::structurep x))

(setf (symbol-function 'structurep) #'structure-p)

(defun STRUCTURE-INSTANCE-P (X)
"Predicate which returns t if the object is a STRUCTURE, NOT AN INSTANCE THEREOF!"
(and (structurep x)(not (instance-p x))))

(defun MAKE-DEFSTRUCT-BODY (slot-names slot-values)
  "makes a list of keyword value pairs appropriate for the body of a MAKE-x
   defstruct invokation. note the recursive call to fasd-form, which is where
   all the real work happens."
  (loop while (and slot-names slot-values) nconc
	(list (make-keyword (pop slot-names))
	      (get-fasd-form (pop slot-values)))
              into answers finally (return answers)))

(defun GET-DEFSTRUCT-DESCRIPTION (x)
(if (not (structurep x))
    (progn (format t "~A is not a structure! you lose...." x)
    NIL)
(si:get (or (AND (ARRAYP X) (si:NAMED-STRUCTURE-SYMBOL X))
			   (AND (LISTP X) (SYMBOLP (CAR X)) (CAR X)))
			       'si:DEFSTRUCT-DESCRIPTION)))

(defun QUOTEMEMBERS (lst)
(loop for cell in lst collect `(QUOTE ,cell)))

(defun CSV ()
"clear struct vars."
(setf *struct-vars* nil *structs-seen* nil))

;;; some things about this one are still bogus....

(defun GET-DEFSTRUCT-SLOTNAMES (x)
""
(mapcar #'first (fourth (get-defstruct-description x))))

(defun GET-DEFSTRUCT-SLOTS-AND-VALS (x)
  "given a defstruct instance, return a list of the slots and vals in that defstruct."
  (let* ((desc (get-defstruct-description x))
	 (slot-names (get-defstruct-slotnames x))
	 (accessor-functions (mapcar #'(lambda (slot)
					 (first (reverse slot)))
				     (fourth desc)))
	 (slot-values (loop for acc in accessor-functions collect 
			    (funcall acc x))))
(get-fasd-form (mapcar #'(lambda (e1 e2)(list e1 e2))
		 slot-names slot-values))))

(defun FORMATTED-DEFSTRUCT-SLOTS-AND-VALS (x)
  (let ((initial (get-defstruct-slots-and-vals x)))
    (loop for thang in initial nconc (list (make-keyword (first thang))
					   (get-fasd-form (rest thang))))))

(defun GET-DEFSTRUCT-SLOTS (x)
  "given a defstruct instance, return a list of the slots in that defstruct (no values)."
  (let* ((desc (get-defstruct-description x))
	 (slot-names (mapcar #'first (fourth desc))))
         slot-names))

(defun STRUCTURE-SLOTD-NAME (slotd)
  (first slotd))

(defun STRUCTURE-SLOTD-READER (slotd)
  (second slotd))

(defun STRUCTURE-SLOTD-WRITER (slotd)
  (third slotd))

(defun GET-DEFSTRUCT-SLOT-ACCESSOR (instance slotname)
""
(or (first (LAST (assoc slotname (fourth (get-defstruct-description instance)))))
    (read-from-string (concatenate 'string (format nil "~A" (type-of instance))
				   "-" (format nil "~A" slotname)))))

#+ignore
(defun SET-DEFSTRUCT-VALUE (instance slotname value)
(let ((acc (get-defstruct-slot-accessor instance slotname)))
(when (null acc)(error "accessor cannot be NIL! for thing ~A, for slotname ~a.~%"
instance slotname)
 (eval `(setf (,acc ,instance) ,value)))))

(defun GET-DEFSTRUCT-VALUE (instance slotname)
"Given an instance of a defstruct, and the name of some slot, return the slots value."
(apply (get-defstruct-slot-accessor instance slotname)(list instance)))

(defun GET-DEFSTRUCT-VALUES (instance)
""
(loop for slot in (get-defstruct-slots instance) collect
(get-defstruct-value instance slot)))

(defun GET-DEFSTRUCT-TYPE (struct)
  (type-of struct))

(defsetf get-defstruct-value set-defstruct-value)

(setf (symbol-function 'get-struct-slotnames-from-struct) #'get-defstruct-slotnames)

)  ;;; end OF THE SYMBOLICS STRUCTURE EVAL-WHEN.,...

;;;; Arrays & Vectors.

(defun ALLOCATE-ARRAY (dims &key (element-type t)
				 (adjustable nil)
				 (initial-contents nil))
"Function to allocate an array. No fill-pointer.
 suggested by kanderson@bbn.com."
(make-array dims :element-type element-type
	         :initial-contents initial-contents
		 :adjustable adjustable))


(defun ALLOCATE-VECTOR (dims &key (element-type t)
				 (adjustable nil)
				 (fill-pointer nil))
"Function to allocate an array. suggested by kanderson@bbn.com."
(make-array dims :element-type element-type
		  :adjustable adjustable
		  :fill-pointer fill-pointer))

;;; Compiled functions.

#+(or :mcl excl)
(defun GET-COMPILED-FUNCTION-NAME (compiled-function)
""
(let ((ans nil)
      (strname ""))
(setq *readtable* (copy-readtable))
(set-dispatch-macro-character #\# #\' (function pseudo-quote-reader))
(set-dispatch-macro-character #\# #\< (function pseudo-quote-reader))
(setf strname (format nil "~S" compiled-function))
(setq ans (read-from-string (SUBSEQ strname 0 (length strname))))
(setq *readtable* (copy-readtable nil))
ans))

;;; Massive kludge for encountering READTABLES!!!

(defun READTABLE-FASD-FORM (i)
"Doesnt seem to be a good way to probe the internals of readtables, even
machine specific ways!!!!"
(declare (ignore i))
`(copy-readtable *readtable*))

;;; Massive kludge for pre-ansi hash table specs!!!!

#+lispm
(defun PARSE-HASH-TABLE-SPEC (htab)
(let ((ans nil))
(setq *readtable* (copy-readtable))
(set-dispatch-macro-character #\# #\' (function pseudo-quote-reader))
(set-dispatch-macro-character #\# #\< (function pseudo-quote-reader))
(setq ans (rest (butlast (read-from-string 
			   (concatenate 'string "(" (subseq (format nil "~a" htab) 8) ")")))))
(setq *readtable* (copy-readtable nil))
ans))

#+lispm
(defun HASH-TABLE-REHASH-SIZE (x)
(let ((spec (parse-hash-table-spec x)))
  (getf spec :rehash-size)))

#+lispm
(defun HASH-TABLE-REHASH-THRESHOLD (x)
(let ((spec (parse-hash-table-spec x)))
  (getf spec :rehash-threshold)))

;;; Functions and Generic Functions.

#+:mcl
(eval-when (load eval compile)

(defun GENERIC-FUNCTION-NAME (instance)
(get-compiled-function-name instance))

(defun GENERIC-FUNCTION-LAMBDA-LIST (gf)
""
(function-lambda-expression gf))

(defun %GENERIC-FUNCTION-P (X)
""
(ccl::standard-generic-function-p x))

(defun COMPILED-FUNCTION-FASD-FORM (X)
"fasd form for hashmark-quote e.g. (FUNCTION name) forms."
  `(function ,(get-compiled-function-name x)))

(defun METHOD-SPECIALIZERS (method)
""
(ccl:specializer-direct-generic-functions method))

(defun METHOD-GENERIC-FUNCTION (gf)
""
(ccl:method-generic-function gf))

) ;;; end of MCL function & generic function eval-when!

(defun GENERIC-FUNCTION-FASD-FORM (instance)
"FASD Form for saving out generic functions..."
     (let ((name (generic-function-name instance))
	   (arglist (generic-function-lambda-list instance))
	   (documentation (%generic-function-documentation instance)))
       `(OR (FIND-GENERIC-FUNCTION ',name)
	    (DEFGENERIC ,name ,arglist (:DOCUMENTATION ,(or documentation ""))))))

(defun METHOD-FASD-FORM (instance)
"Fasd form for saving out method objects."
     (LET* ((name (generic-function-name (method-generic-function instance)))
	    (qualifiers (method-qualifiers instance))
	    (specializers (method-specializers instance)))
	   `(FIND-METHOD (FUNCTION ,name)
			 (LIST ,@qualifiers)
			 (LIST ,@(DO-SPECIALIZERS specializers))
			 NIL)))

#+excl
(defun COMPILED-FUNCTION-FASD-FORM (X)
"fasd form for hashmark-quote e.g. (FUNCTION name) forms."
  `(function ,(get-compiled-function-name x)))

#+lispm
(defun COMPILED-FUNCTION-FASD-FORM (X)
"fasd form for hashmark-quote e.g. (FUNCTION name) forms."
  (if (si:lexical-closure-p x) nil
  `(FUNCTION ,(si:compiled-function-name x))))

;;;; PCL/CLOS classes and instances.
;;;; NOTE: CLASS DEFINITIONS, WHEN READ IN, WILL OVERWRITE THE
;;;; CLASS DEFINITION PREVIOUSLY IN MEMORY. IF YOU DO NOT WANT THIS
;;;; TO HAPPEN, REPLACE 'DEFCLASS' BELOW WITH 'FIND CLASS' + the
;;;; APPROPRIATE ARGUMENTS!

(defun SAFE-CLASS-FASD-FORM (instance)
"This version of the class-fasd-form function WILL NOT overwrite 
 current class definitions with the same name. It is the one invoked
 by GET-FASD-FORM and SAVE-OBJECT."
  (let* ((name (%class-name instance))
       (supertypes (get-class-superclasses instance))
       (slots (generate-class-slot-forms instance))
       (options (generate-class-options-form instance)))
`(OR (FIND-CLASS ',name)
     (DEFCLASS ,name ,supertypes ,slots ,@options))))

(defun CLASS-FASD-FORM (instance)
"This version of the class-fasd-form function WILL OVERWRITE 
 CURRENT CLASS DEFINITIONS WITH THE SAME NAME. Sunstitute a call to
 this one in GET-FASD-FORM and SAVE-OBJECT."
 (let* ((name (%class-name instance))
       (supertypes (get-class-superclasses instance))
       (slots (generate-class-slot-forms instance))
       (options (generate-class-options-form instance)))
   (if (builtin-class-p instance) `(FIND-CLASS ',name)
     `(DEFCLASS ,name ,supertypes ,slots ,@options))))

(defun SYM< (a b)
"Predicate to see if symbol a is alphabetically before symbol b. T if a is."
(string< (format nil "~A" A)(format nil "~A" b)))

(defun SYMF< (a b)
"Predicate to see if symbol a is alphabetically before symbol b. T if a is."
(string< (format nil "~A" (FIRST A))(format nil "~A" (first b))))

(defun GET-ORDERED-SLOT-NAMES (I)
"Returns a list of the slot names of the instance, alphabetized."
(sort (all-slotnames i) #'sym<))

#+allegro-v4.0
(defun FLATTEN (l)
""
(mapcan #'identity l))

#-allegro-v4.0
(defun FLATTEN (l)
""
(loop for cell in l nconc cell into answers
      finally (return answers)))

(defun PAIR-UP (l)
""
(loop while l collect (list (pop l)(pop l)) into answers
      finally (return (reverse answers))))

(defun ALPHABETIZE-BY-KEYWORD (lst)
(let ((alpha-cells (sort (pair-up lst) #'symf<)))
  (mapcar #'second alpha-cells)))

(defun GET-ORDERED-SLOT-VALUES (i)
"Gets the fasd forms out of the instance slot values, then alphabetizes them."
 (alphabetize-by-keyword (get-slot-values i)))

(defun %FILL-INSTANCE (i ordered-slot-values)
"Fills in the slots alphabetically. Assumes the slot values come into the function
 alphabetically ordered already: Returns the instance object."
(loop with osv = (copy-list ordered-slot-values)
      for name in (get-ordered-slot-names i) 
      as thang = (pop osv) do
      (when (symbolp thang)(setf thang `(quote ,thang)))
      (setf (slot-value i name) thang)
      finally (return i)))

#+pcl
(defun %ALLOCATE-INSTANCE (class-object)
(pcl::allocate-instance class-object))

#+lispm
(defun GET-SYMBOLS-DEFSTRUCT-SPEC (x)
"for the slimebolical , this information is kept as a property."
(si:get x 'si:defstruct-description))

#+lispm
(defun %STRUCTURE-P (x)
"predicate, if symbol returns t if it names a struct."
(cond ((and (symbolp x)(get-symbols-defstruct-spec x)) T)
      ((structure-p x) T)
      (T NIL)))

#+:mcl
(defun %STRUCTURE-P (X)
"predicate, if symbol returns t if it names a struct."
(and (symbolp x)(equal (class-name (class-of (find-class x nil))) 'STRUCTURE-CLASS)))

(defun ALLOCATE-HTAB (htab &rest arglist)
"Allocates the empty husk of a hash table, getting its attributes from the object itself."
(declare (ignore htab))
(let ((size (getf arglist :size))
      (rehash-size (getf arglist :rehash-size))
                   (test (getf arglist :test))
                   (rehash-threshold (getf arglist :rehash-threshold)))
(make-hash-table :size size 
                 :rehash-size rehash-size 
                 :rehash-threshold rehash-threshold
                 :test test)))

#+clos
(defun %ALLOCATE-INSTANCE (class-object) 
  (cond ((equal class-object 'HASH-TABLE)
	 (allocate-htab class-object 
			:size *current-htab-size*
			:rehash-size *current-htab-rehash-size*
			:rehash-threshold *current-htab-rehash-threshold*
			:test *current-htab-test*))
        ((%structure-p class-object)(allocate-struct class-object))
        (T (when *debug-instance-storage*
	   (format t "now trying to allocate an instance for ~a!" class-object))
           (when (symbolp class-object)(setf class-object (find-class class-object nil)))
           (when class-object (clos::allocate-instance class-object)))))

#+clos
(defun FILL-INSTANCE (new &rest vals)
"New: allocates an instance given classname, the vals are the alphabetized list of
 slot values extracted from the target instance. returns the newly filled in instance."
  (%fill-instance new vals)
  new)

#+pcl
(defun FILL-INSTANCE (classname &rest vals)
"New: allocates an instance given classname, the vals are the alphabetized list of
 slot values extracted from the target instance. returns the newly filled in instance."
(let* ((new (pcl::allocate-instance (find-class classname))))
  (%fill-instance new vals)
  new))

;;; ========= user defined fasd forms ==========

(defun INSTANCE-FASD-FORM (instance)
"NEW VERSION. ATTEMPTS TO DEAL WITH SIRCULAR SLOT VALUE REFS,
Basic FASD form for clos/pcl instances. checks if the instance has a custom
 fasd form, binds it to a generated symbol name, recursively expands the
 instances contents."
(declare (special tmp))
  (if (has-fasd-form-p (instance-name instance))
      `(setq ,(get-instance-label instance) ,(funcall #'(lambda (x)
					   (get-fasd-form x))
				       instance))
 `(fill-instance ,(get-instance-label instance) ,@(get-ordered-slot-values instance))))

(defmacro MAKE-STRUCTURE (name &rest kwd-val-pairs)
`(funcall (get-defstruct-constructor ,name) ,@kwd-val-pairs))

(defun STRUCTURE-FASD-FORM (instance)
"Independent of vendor:"
`(fill-struct ,(get-instance-label instance)
	      ',(get-defstruct-values instance)))

#+ignore ;;;; for later.
(defun FIND-THINGS-IN-OBJECT (object type-to-find)
""
(mapappend-object #'(lambda (key slot)
                     (declare (ignore key))
		      (find-things-in-slot type-to-find slot))
		  object))

(defun FIND-THINGS-IN-SLOT (type slot)
(cond ((null slot) nil)
      ((symbolp slot) nil)
      ((typep slot type) slot)
      ((listp slot)(cons (find-things-in-slot type (first slot))
			 (find-things-in-slot type (rest slot))))))

(defun EVAL-SLOTS (i)
"Goes through the local slots, insuring the contents of these slots have been
evaled properly. called by return-instance."
(loop for name in (all-slotnames i) do
(setf (slot-value i name)(eval (slot-value i name)))
finally (return i)))

(defun RETURN-INSTANCE (i)
"Called by the new instance-fasd-form: takes an instances, maps eval over its
slots, then returns the instance after having reassigned the slot values to
the evaled contents!"
;;;; (eval-slots i)
i)

;;; symbols.

(defun SPECIAL-MARKER-P (X &optional (label ".%%SL%%."))
"label must match with pushsym, and must be upper-case."
  (search label (format nil "~A"  x) :test #'equal))

(defun SYMBOL-FASD-FORM (instance)
"Better bolder symbol saving formula which includes the package data implicitly."
(if (null instance) NIL
(if (special-marker-p instance) instance
(read-from-string (format nil "~a" (concatenate 'string "'"
				 (package-name (symbol-package instance)) "::"
			              (symbol-name instance)))))))

(defun REGULAR-FUNCTION-FASD-FORM (instance)
  `(FUNCTION ,instance))

(defun QUOTED-LIST-P (x)
"Predicate, if somethings a quoted list...."
(and (listp x)
     (not (circular-list-p x))
     (not (every #'null x))
     (every #'symbolp x)))

(defun QUOTED-LIST-FASD-FORM (instance)
"If something is a quoted list, put the quote at the right place."
`(QUOTE (,@instance)))

#-lucid
(eval-when (load eval compile)

(defun LONG-LIST-FASD-FORM (instance)
(list-fasd-form instance))

(defun LIST-FASD-FORM (instance)
`(list ,@(mapcar #'(lambda (thing)
  (get-fasd-form thing)) instance)))
)

;;; end of nonlucid eval-when.

(defun CONSTANT-FASD-FORM (i)
  "anything that evals to itself, e.g. keywords, etc. just return that thing."
  i)

(defun COMPLEX-FASD-FORM (i)
"anything which is a complex number."
`(COMPLEX ,(GET-FASD-FORM (REALpart i))
	  ,(GET-FASD-FORM (imagpart i))))

;;; eof.
