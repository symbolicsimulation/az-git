;;; This may be dumb, but keep from clobbering the guy's ops: search-list
;;; just in case.

(let ((old-ops (search-list "ops:")))
  (setf (search-list "ops:") '("/afs/cs/project/clisp/library/ops/"))
  (compile-file "ops:ops-util.lisp" :error-file nil)
  (compile-file "ops:ops-compile.lisp" :error-file nil)
  (compile-file "ops:ops-rhs.lisp" :error-file nil)
  (compile-file "ops:ops-match.lisp" :error-file nil)
  (compile-file "ops:ops-main.lisp" :error-file nil)
  (compile-file "ops:ops-backup.lisp" :error-file nil)
  (compile-file "ops:ops-io.lisp" :error-file nil)
  (compile-file "ops:ops.lisp" :error-file nil)
  (when old-ops (setf (search-list "ops:") old-ops)))
