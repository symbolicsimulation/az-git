;;; Sun Nov 18 13:39:25 1990 by Mark Kantrowitz <mkant@GLINDA.OZ.CS.CMU.EDU>
;;; c-lisp-interfaces.text

;;; ****************************************************************
;;; C to Lisp Interfaces *******************************************
;;; ****************************************************************
;;; This file describes linking together compiled LISP and C code.

ISSUES INVOLVED IN THE C TO LISP INTERFACE

Can C object code be dynamically linked in to a running LISP image? 
Can C's data types be converted to LISP's data types?  
Can you use a shared memory area to pass information between procedures
in C and LISP?


C TO LISP INTERFACES 

There is no standard on calling foreign functions in Lisp, and no
portable foreign function interface. (In fact, Lucid on the Vax and on
the Sun4 are incompatible in this respect.) In most you need to define
the function as a C-callable function (or load the C-function into lisp).

Franz' Allegro Common Lisp has a Foreign Function Call facility that allows you
to use C routines within lisp.  See the documentation, chapter 10.
Calling Lisp functions from C is treated in section 10.8.2.
(defun-c-callable and register-function to define a lisp function as
being C-callable, and then the C function call lisp_call() with
appropriate arguments.)

(Macintosh Allegro Common Lisp also has a similar FFI.)

Austin Kyoto Common Lisp is actually written in C and the source code is
available, making embellishments easy.  Kyoto is available free of
charge, but it's an experimental implementation. Use defCfun to create
a C-callable function in somewhat convoluted lisp syntax. See chapter
10 of the KCL Report. Load compiled C code into Lisp using si:faslink.

Sun Common Lisp's (aka Lucid Common Lisp) ADVANCED USER'S GUIDE has a
lengthy section on using foreign functions (Chapt. 5, "Working beyond
the Lisp environment").  (Note that the ADVANCED USER's GUIDE and the
USER's GUIDE are two different items.)  A set of demo code comes with
the Lucid; on our system it's under a directory called
foreign-code-examples.

DEC's VaxLisp is the best implementation for a hybrid C-LISP
environment.  It supports a very large repetoire of C data types,
including C structures.

;;; ********************************
;;; Run-Program ********************
;;; ********************************
;;; Running an External Unix Program

Lucid:

RUN-PROGRAM (name &key input output error-output (wait t) arguments 
		       (if-input-does-not-exist :error)
		       (if-output-exists :error)
		       (if-error-output-exists :error))

