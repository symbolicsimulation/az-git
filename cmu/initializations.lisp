;;; -*- Mode: LISP; Syntax: Common-lisp; Package: CL-LIB; Base: 10 -*-

;;; ****************************************************************
;;; Initializations ************************************************
;;; ****************************************************************
;;;
;;; This is the initializations package written March 1991 by
;;;   Bradford W. Miller
;;;   miller@cs.rochester.edu
;;;   University of Rochester, Department of Computer Science
;;;   610 CS Building, Comp Sci Dept., U. Rochester, Rochester NY 14627-0226
;;;   716-275-1118
;;; It runs in Allegro 4.0.1 & allegro 4.1 (not needed for lispms).
;;;
;;; This version was obtained from the directory
;;; /afs/cs.cmu.edu/user/mkant/Public/Lisp-Utilities/initializations.lisp
;;; via anonymous ftp from a.gp.cs.cmu.edu.
;;;
;;; Bug reports, improvements, and feature requests should be sent
;;; to miller@cs.rochester.edu. Ports to other lisps are also welcome.
;;; (It would be appreciated if you would also cc mkant@cs.cmu.edu.)
;;;
;;; Copyright (C) 1991 by the University of Rochester.
;;; Right of use & redistribution is granted as per the terms of the 
;;; GNU GENERAL PUBLIC LICENCE version 2 which is incorporated here by
;;; reference.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;

;;; ********************************
;;; Motivation *********************
;;; ********************************
;;;
;;; Since RHET and other projects have been using the initializations features
;;; of the explorer and symbolics, the following was inspired to allow a simple
;;; port to non-lispm lisps.
;;;
;;; Like the lisp machines, a number of lists are predefined, one to be run
;;; whenever the world is first booted (:WARM), one to force a single eval
;;; (:ONCE), one before any gc (:GC), after any gc (:AFTER-GC), before a
;;; non-checkpointed disk save (:BEFORE-COLD), and after a non-checkpointed
;;; world is first booted (:COLD). Lispms had a initialization-keywords 
;;; variable for associating new lists with keywords, mine is called
;;; *initialization-keywords*.
;;;
;;; One can also define when an initializtion is run, either :normal, which
;;; only places the form on the list (the default, unless :once is specified),
;;; :now which is to eval now and add it to the list, :first, which causes
;;; immediate evaluation if it hasn't been evaluated before (default for
;;; :once), and :redo which will not eval the form now, but will cause the
;;; initialization to be run the next time the list is processed, even if it
;;; has already been run.

;;; ********************************
;;; ********************************
;;; ********************************

;;; initializations are ordered as first added, first run.
(in-package "CL-LIB")
;;; first set up the default lists

(defvar *warm-initialization-list* nil
  "Initializations run just after booting any world")

(defvar *cold-initialization-list* nil
  "Initializations run just after booting a non-checkpointed world")

(defvar *before-cold-initialization-list* nil
  "Initializations run just before saving a non-checkpointed world")

(defvar *once-initialization-list* nil
  "Initializations to be run only once")

(defvar *gc-initialization-list* nil
  "Initializations to be run before a gc")

(defvar *after-gc-initialization-list* nil
  "Initializations to be run after a gc")

(defvar *initialization-keywords*
    (copy-list '((:warm *warm-initialization-list*)
                 (:cold *cold-initialization-list*)
                 (:before-cold *before-cold-initialization-list*)
                 (:once *once-initialization-list* :first)
                 (:gc *gc-initialization-list*)
                 (:after-gc *after-gc-initialization-list*)))
  "Alist of initialization-list keywords and the list itself. Third element,
if present, is the default run time, :normal if absent. This can be overridden
by the add-initialization function.")

(defstruct initialization
  (name "" :type string)
  (form nil :type list)
  (flag nil)
  (run nil :type list))

;;; TODO: make more robust by checking the input.

(defun reset-initializations (initialization-list-name)
  "Sets the FLAG of each initialization on the passed list to NIL so it
will be (re)run the next time initializations is called on it."
  (declare (type symbol initialization-list-name))

  (dolist (init (eval initialization-list-name))
    (setf (initialization-flag init) nil)))

(defun delete-initialization (name &optional keywords initialization-list-name)
  "Delete the initialization with name from either the list specified with the
keywords, or passed as initialization-list-name."
  (declare (type string name)
           (type list keywords)
           (symbol initialization-list-name))

  (unless initialization-list-name
    (setq initialization-list-name (cadr (assoc (car keywords)
                                                *initialization-keywords*))))

  (set initialization-list-name (delete name (eval initialization-list-name)
                                        :test #'equal
                                        :key #'(lambda (x) (initialization-name x)))))

(defun initializations (initialization-list-name &optional redo (flag t))
  "Run the initializations on the passed list. If redo is non-nil, then the
current value of the flag on the initialization is ignored, and the 
initialization is always run. Otherwise an initialization is only run if
the associated flag is NIL. Flag is the value stored into the initialization's
flag when it is run."
  (declare (type symbol initialization-list-name))
  
  (dolist (init (eval initialization-list-name))
    (when (or redo (not (initialization-flag init)))
      (eval (initialization-form init))
      (setf (initialization-flag init) flag))))

(defun add-initialization (name form &optional keywords initialization-list-name)
  "The initialization form is given name name and added to the initialization list
specified either by the keywords or passed directly. The keywords can also be used
to change the default time to evaluate the initialization."
  (declare (type string name)
           (type list form keywords)
           (type symbol initialization-list-name))

  (let ((type '(:normal))
        (list-name initialization-list-name)
        list-key)
    (cond
     (list-name
      (if keywords
          (setq type keywords)))
     (t
      (setq list-name (cdr (some #'(lambda (x)
                                     (setq list-key x) ; remember it
                                     (assoc x *initialization-keywords*))
                                 keywords)))
      (if (cdr list-name)
          (setq type (cdr list-name)))
      (setq list-name (car list-name))
      ;; now check if keywords override.
      (setq keywords (remove list-key keywords :test #'eq))
      (if keywords
          (setq type keywords))))

    ;; OK, now we can process the entry.... first, is it already there?
    (let ((entry (find name (eval list-name) 
                       :test #'equal
                       :key #'(lambda (x) (initialization-name x)))))
      (cond
       ((null entry)
        (set list-name
             (nconc (eval list-name)
                    (list (setq entry (make-initialization
                                       :name name
                                       :form form
                                       :run type))))))
       (t
        ;; update the entry
        (setf (initialization-form entry) form)
        (setf (initialization-run entry) type)))
      (cond
       ((or (member :now type)
            (and (member :first type)
                 (not (initialization-flag entry))))
        (eval form)
        (setf (initialization-flag entry) t)))
      (cond
       ((member :redo type)
        (setf (initialization-flag entry) nil))))))

;;; the rest of this stuff is just to make sure the various predefined lists are
;;; automatically processed at the appropriate time.

(defun reset-and-invoke-initializations (list-keyword)
  (declare (type keyword list-keyword))
  
  (let ((entry (assoc list-keyword *initialization-keywords*)))
    (reset-initializations (cadr entry))
    (initializations (cadr entry))))

;;; :once requires no processing. It's done only at add-initialization time.
;;;
;;; :gc / :after-gc surround invocations to gc

(excl:advise excl:gc :before handle-gc-initializations nil
             (reset-and-invoke-initializations :gc))

(excl:advise excl:gc :after handle-after-gc-initializations nil
             (reset-and-invoke-initializations :after-gc))

;;; :before-cold happens when we disk save, unless we specify :checkpoint
;;; while :cold happens after the world comes back (unless we specify :checkpoint)
;;; and :warm happens in any case.

(eval-when (compile load eval)
  (pushnew '(:eval initializations '*warm-initialization-list*) excl:*restart-actions* :test #'equalp)
  (pushnew '(:eval initializations '*cold-initialization-list*) excl:*restart-actions* :test #'equalp))

(excl:advise excl:dumplisp :before handle-initializations nil
             (reset-initializations '*warm-initialization-list*)
             (unless (extract-keyword :checkpoint excl:arglist)
               (reset-and-invoke-initializations :before-cold)))

(eval-when (load eval)
  (add-initialization "User Notification"
                      '(format *terminal-io* "~2&--> Running Before-Cold Initializations~2%")
                      '(:before-cold))
  (add-initialization "User Notification"
                      '(format *terminal-io* "~2&--> Running Cold Initializations~2%")
                      '(:cold))
  (add-initialization "User Notification"
                      '(format *terminal-io* "~2&--> Running Warm Initializations~2%")
                      '(:warm))
  (add-initialization "Reset Cold Initializations"
                      '(reset-initializations '*cold-initialization-list*)
                      '(:before-cold)))

;;; 
;;; Here is a short example of the above 
;;;
#||
(add-initialization "test1" '(print "should print once") (:once))

(add-initialization "test1" '(print "should print once") (:once))

;; note the first immediately executed the print, and the second supressed it.

(defvar *new-init-list* nil)

(add-initialization "a hack" '(print "One hack") () '*new-init-list*)

;; didn't print yet

(initializations '*new-init-list*)

;; now it did

(initializations '*new-init-list*)

;; but not again until

(reset-initializations '*new-init-list*)

(initializations '*new-init-list*)
||#

;;; Note in particular the :once list may be useful in init files, particularly
;;; where the result might be saved in a world and you don't want it redone
;;; e.g. in allegro checkpoint worlds.
;;; 
;;; The other sorts of lists are particularly useful for housekeeping

;;; *EOF*
