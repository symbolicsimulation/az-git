      SUBROUTINE ZGEESX( JOBVS, SORT, SELECT, SENSE, N, A, LDA, SDIM, W,
     $                   VS, LDVS, RCONDE, RCONDV, WORK, LWORK, RWORK,
     $                   BWORK, INFO )
*
*  -- LAPACK driver routine (version 1.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     February 29, 1992
*
*     .. Scalar Arguments ..
      CHARACTER          JOBVS, SENSE, SORT
      INTEGER            INFO, LDA, LDVS, LWORK, N, SDIM
      DOUBLE PRECISION   RCONDE, RCONDV
*     ..
*     .. Array Arguments ..
      LOGICAL            BWORK( * )
      DOUBLE PRECISION   RWORK( * )
      COMPLEX*16         A( LDA, * ), VS( LDVS, * ), W( * ), WORK( * )
*     ..
*     .. Function Arguments ..
      LOGICAL            SELECT
      EXTERNAL           SELECT
*     ..
*
*  Purpose
*  =======
*
*  For an N by N complex nonsymmetric matrix A,
*
*     compute the eigenvalues (W)
*     compute the Schur form (A)
*     compute the Schur vectors (VS)
*     order the eigenvalues on the diagonal of the Schur form so
*       that selected eigenvalues are at the top left (SELECT)
*     compute a reciprocal condition number for the average of the
*       selected eigenvalues (RCONDE)
*     compute a reciprocal condition number for the right invariant
*       subspace corresponding to the selected eigenvalues (RCONDV)
*
*  The last four actions are optional:
*
*     JOBVS determines whether the Schur vectors VS are computed.
*     SORT determines whether the eigenvalues are ordered by using
*       the logical function SELECT to choose eigenvalues to move
*       to the top left of the Schur form. SDIM is the number of
*       selected eigenvalues.
*     SENSE determines whether RCONDE and RCONDV are to be computed.
*
*  A matrix is in Schur form if it is upper triangular.
*
*  On return array W contains the (optionally ordered) eigenvalues.
*  A will be overwritten with its Schur form.  If the eigenvalues are
*  ordered the output Schur form is
*
*                     [  A11    A12  ]  SDIM
*                     [   0     A22  ]  N-SDIM
*                        SDIM  N-SDIM
*
*  where the eigenvalues lambda(1), ..., lambda(SDIM) of A11 satisfy
*  SELECT(lambda(i))=.TRUE. and the eigenvalues lambda(SDIM+1), ... ,
*  lambda(N) of A22 satisfy SELECT(lambda(i)) = .FALSE.
*
*  Optionally, VS contains the Schur vectors. This means VS is a
*  unitary matrix and
*
*     A (on input) = VS * (Schur form of A) * conjugate-transpose(VS)
*
*  The reciprocal of the condition number of the selected eigenvalues
*  is defined as follows. The reciprocal condition number RCONDE
*  measures the sensitivity of the average of the eigenvalues of A11:
*
*               ( lambda(1) + ... + lambda(SDIM) ) / SDIM
*
*  RCONDE is between 0 (very badly conditioned) and 1 (very well
*  conditioned). We compute RCONDE as follows. First we compute R so
*  that
*
*                      P = [  I       R   ] SDIM
*                          [  0       0   ] N-SDIM
*                            SDIM  N-SDIM
*
*  is the projector on the invariant subspace associated with A11.
*  R is the solution of the Sylvester equation
*
*                        A11*R - R*A22 = A12.
*
*  Let F-norm(M) denote the Frobenius-norm of M and 2-norm(M) denote
*  the two-norm of M. Then RCONDE is the lower bound
*
*                      (1 + F-norm(R)**2)**(-1/2)
*
*  on the reciprocal of 2-norm(P), the true reciprocal condition
*  number. RCONDE cannot underestimate 1 / 2-norm(P) by more than a
*  factor of sqrt(N).
*  An approximate error bound for the computed average of the
*  eigenvalues of A11 is
*
*                         EPS * norm(A) / RCONDE
*
*  where EPS = DLAMCH( 'P' ) is the machine precision.
*
*  The reciprocal of the condition number of the right invariant
*  subspace spanned by the selected eigenvalues is defined as follows.
*  This subspace is spanned by the first SDIM columns of VS. RCONDV
*  is defined as the separation of A11 and A22:
*
*                     sep (A11,A22) = sigma-min(K)
*
*  where sigma-min(K) is the smallest singular value of the
*  SDIM*(N-SDIM) by SDIM*(N-SDIM) matrix
*
*     K  = kprod( I(N-SDIM), A11 ) - kprod( transpose(A22), I(SDIM) )
*
*  I(m) is an m by m identity matrix, and kprod denotes the Kronecker
*  product. We estimate sigma-min(K) by the reciprocal of an estimate
*  of the 1-norm of inverse(K). The true reciprocal 1-norm of
*  inverse(K) cannot differ from sigma-min(K) by more than a factor
*  of sqrt(SDIM*(N-SDIM)).
*  When RCONDV is small, small changes in A can cause large changes in
*  the invariant subspace. An approximate bound on the maximum angular
*  error in the computed right invariant subspace is
*
*                      EPS * norm(A) / RCONDV
*
*  where EPS = DLAMCH( 'P' ) is the machine precision.
*
*  See section 4.9 of the LAPACK Users' Guide for a detailed discussion
*  of these condition numbers.
*
*  Arguments
*  =========
*
*  JOBVS   (input) CHARACTER*1
*          Specifies whether or not to compute the Schur vectors.
*          = 'N': Schur eigenvectors are not computed.
*          = 'V': Schur eigenvectors are computed.
*
*  SORT    (input) CHARACTER*1
*          Specifies whether or not to order the eigenvalues on the
*          diagonal of the Schur form.
*          = 'N': Eigenvalues are not ordered.
*          = 'S': Eigenvalues are ordered so that the eigenvalues W(j)
*                 for which the logical function SELECT(W(j)) is true
*                 will appear at the top left corner of the Schur form.
*
*  SELECT  (input) LOGICAL FUNCTION of one COMPLEX*16 variable
*          Used if SORT = 'S' to select eigenvalues to order to the top
*          left of the Schur form. The eigenvalue W(j) is selected if
*          SELECT(W(j)) is true.
*          SELECT must be declared EXTERNAL in the calling subroutine.
*          SELECT is not referenced if SORT = 'N'.
*
*  SENSE   (input) CHARACTER*1
*          Determines which reciprocal condition numbers are computed.
*          = 'N': None are computed.
*          = 'E': Computed for average of selected eigenvalues only
*          = 'V': Computed for selected right invariant subspace only
*          = 'B': Computed for both
*          If SENSE = 'E', 'V' or 'B', SORT must equal 'S'.
*
*  N       (input) INTEGER
*          The number of rows and columns of the input matrix A. N >= 0.
*
*  A       (input/output) COMPLEX*16 array, dimension (LDA, N)
*          On input, A is the matrix whose Schur form is desired.
*          On output, A has been overwritten by its Schur form.
*
*  LDA     (input) INTEGER
*          The leading dimension of the array A.  LDA >= max(1,N).
*
*  SDIM    (output) INTEGER
*          If SORT = 'N', SDIM = 0 on output.
*          If SORT = 'S', SDIM = number of eigenvalues for which
*                         SELECT is true.
*
*  W       (output) COMPLEX*16 array, dimension (N)
*          On exit, W contains the computed eigenvalues. The eigenvalues
*          will be in the same order that they appear on the diagonal
*          of the output Schur form.
*
*  VS      (output) COMPLEX*16 array, dimension (LDVS,N)
*          If JOBVS = 'V', VS will contain the Schur vectors of A.
*          This means that VS is a unitary matrix satisfying
*              A (on input) =
*                  VS * (Schur form of A) * conjugate-transpose(VS)
*          Not referenced if JOBVS = 'N'.
*
*  LDVS    (input) INTEGER
*          The leading dimension of the array VS.  LDVS >= 1, and if
*          JOBVS = 'V', LDVS >= N.
*
*  RCONDE  (output) DOUBLE PRECISION
*          On exit, if SENSE = 'E' or 'B', the reciprocal condition
*          number for the average of the selected eigenvalues.
*
*  RCONDV  (output) DOUBLE PRECISION
*          On exit, if SENSE = 'V' or 'B', the reciprocal condition
*          number for the selected right invariant subspace.
*
*  WORK    (workspace/output) COMPLEX*16 array, dimension (LWORK)
*          On exit, WORK(1) contains the optimal workspace size LWORK
*          for high performance.
*
*  LWORK   (input) INTEGER
*          The dimension of the array WORK.  LWORK >= max(1,2*N).
*          If SENSE = 'E' or 'V' or 'B', LWORK >= 2*SDIM*(N-SDIM),
*          where SDIM is the number of selected eigenvalues computed by
*          this routine.  Note that 2*SDIM*(N-SDIM) <= N*N/2.
*          For good performance, LWORK must generally be larger.
*          The optimum value of LWORK for high performance is
*          returned in WORK(1).
*
*  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
*
*  BWORK   (workspace) LOGICAL array, dimension (N)
*          Not referenced if SORT = 'N'.
*
*  INFO    (output) INTEGER
*          = 0: successful exit
*          < 0: if INFO = -i, the i-th argument had an illegal value.
*          > 0: the QR algorithm failed to compute all the eigenvalues;
*               if INFO = i, elements 1:ILO-1 and i+1:N of W contain
*               those eigenvalues which have converged, and VS, if
*               computed, contains the transformation which reduces A
*               to its partially converged Schur form.
*
*  =====================================================================
*
*     .. Parameters ..
      DOUBLE PRECISION   ZERO, ONE
      PARAMETER          ( ZERO = 0.0D0, ONE = 1.0D0 )
*     ..
*     .. Local Scalars ..
      LOGICAL            SCALEA, WANTSB, WANTSE, WANTSN, WANTST, WANTSV,
     $                   WANTVS
      INTEGER            HSWORK, I, IBAL, ICOND, IERR, IEVAL, IHI, ILO,
     $                   ITAU, IWRK, K, MAXB, MAXWRK, MINWRK
      DOUBLE PRECISION   ANRM, BIGNUM, CSCALE, EPS, SMLNUM
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION   DUM( 1 )
*     ..
*     .. External Subroutines ..
      EXTERNAL           DLABAD, DLASCL, XERBLA, ZCOPY, ZGEBAK, ZGEBAL,
     $                   ZGEHRD, ZHSEQR, ZLACPY, ZLASCL, ZTRSEN, ZUNGHR
*     ..
*     .. External Functions ..
      LOGICAL            LSAME
      INTEGER            ILAENV
      DOUBLE PRECISION   DLAMCH, ZLANGE
      EXTERNAL           LSAME, ILAENV, DLAMCH, ZLANGE
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          MAX, MIN, SQRT
*     ..
*     .. Executable Statements ..
*
*     Test the input arguments
*
      INFO = 0
      WANTVS = LSAME( JOBVS, 'V' )
      WANTST = LSAME( SORT, 'S' )
      WANTSN = LSAME( SENSE, 'N' )
      WANTSE = LSAME( SENSE, 'E' )
      WANTSV = LSAME( SENSE, 'V' )
      WANTSB = LSAME( SENSE, 'B' )
      IF( ( .NOT.WANTVS ) .AND. ( .NOT.LSAME( JOBVS, 'N' ) ) ) THEN
         INFO = -1
      ELSE IF( ( .NOT.WANTST ) .AND. ( .NOT.LSAME( SORT, 'N' ) ) ) THEN
         INFO = -2
      ELSE IF( .NOT.( WANTSN .OR. WANTSE .OR. WANTSV .OR. WANTSB ) .OR.
     $         ( .NOT.WANTST .AND. .NOT.WANTSN ) ) THEN
         INFO = -4
      ELSE IF( N.LT.0 ) THEN
         INFO = -5
      ELSE IF( LDA.LT.MAX( 1, N ) ) THEN
         INFO = -7
      ELSE IF( LDVS.LT.1 .OR. ( WANTVS .AND. LDVS.LT.N ) ) THEN
         INFO = -11
      END IF
*
*     Compute workspace
*      (Note: Comments in the code beginning "Workspace:" describe the
*       minimal amount of real workspace needed at that point in the
*       code, as well as the preferred amount for good performance.
*       CWorkspace refers to complex workspace, and RWorkspace to real
*       workspace. NB refers to the optimal block size for the
*       immediately following subroutine, as returned by ILAENV.
*       HSWORK refers to the workspace preferred by ZHSEQR, as
*       calculated below. HSWORK is computed assuming ILO=1 and IHI=N,
*       the worst case.
*       If SENSE = 'E', 'V' or 'B', then the amount of workspace needed
*       depends on SDIM, which is computed by the routine ZTRSEN later
*       in the code.)
*
      MINWRK = 1
      IF( INFO.EQ.0 .AND. LWORK.GE.1 ) THEN
         MAXWRK = N + N*ILAENV( 1, 'ZGEHRD', ' ', N, 1, N, 0 )
         MINWRK = MAX( 1, 2*N )
         IF( .NOT.WANTVS ) THEN
            MAXB = MAX( ILAENV( 8, 'ZHSEQR', 'SN', N, 1, N, -1 ), 2 )
            K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'ZHSEQR', 'SN', N, 1,
     $          N, -1 ) ) )
            HSWORK = MAX( K*( K+2 ), 2*N )
            MAXWRK = MAX( MAXWRK, HSWORK, 1 )
         ELSE
            MAXWRK = MAX( MAXWRK, N+( N-1 )*
     $               ILAENV( 1, 'ZUNGHR', ' ', N, 1, N, -1 ) )
            MAXB = MAX( ILAENV( 8, 'ZHSEQR', 'SV', N, 1, N, -1 ), 2 )
            K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'ZHSEQR', 'SV', N, 1,
     $          N, -1 ) ) )
            HSWORK = MAX( K*( K+2 ), 2*N )
            MAXWRK = MAX( MAXWRK, HSWORK, 1 )
         END IF
         WORK( 1 ) = MAXWRK
      END IF
      IF( LWORK.LT.MINWRK ) THEN
         INFO = -15
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'ZGEESX', -INFO )
         RETURN
      END IF
*
*     Quick return if possible
*
      IF( N.EQ.0 ) THEN
         SDIM = 0
         RETURN
      END IF
*
*     Get machine constants
*
      EPS = DLAMCH( 'P' )
      SMLNUM = DLAMCH( 'S' )
      BIGNUM = ONE / SMLNUM
      CALL DLABAD( SMLNUM, BIGNUM )
      SMLNUM = SQRT( SMLNUM ) / EPS
      BIGNUM = ONE / SMLNUM
*
*     Scale A if max entry outside range [SMLNUM,BIGNUM]
*
      ANRM = ZLANGE( 'M', N, N, A, LDA, DUM )
      SCALEA = .FALSE.
      IF( ANRM.GT.ZERO .AND. ANRM.LT.SMLNUM ) THEN
         SCALEA = .TRUE.
         CSCALE = SMLNUM
      ELSE IF( ANRM.GT.BIGNUM ) THEN
         SCALEA = .TRUE.
         CSCALE = BIGNUM
      END IF
      IF( SCALEA )
     $   CALL ZLASCL( 'G', 0, 0, ANRM, CSCALE, N, N, A, LDA, IERR )
*
*
*     Permute the matrix to make it more nearly triangular
*     (CWorkspace: none)
*     (RWorkspace: need N)
*
      IBAL = 1
      CALL ZGEBAL( 'P', N, A, LDA, ILO, IHI, RWORK( IBAL ), IERR )
*
*     Reduce to upper Hessenberg form
*     (CWorkspace: need 2*N, prefer N+N*NB)
*     (RWorkspace: none)
*
      ITAU = 1
      IWRK = N + ITAU
      CALL ZGEHRD( N, ILO, IHI, A, LDA, WORK( ITAU ), WORK( IWRK ),
     $             LWORK-IWRK+1, IERR )
*
      IF( WANTVS ) THEN
*
*        Copy Householder vectors to VS
*
         CALL ZLACPY( 'L', N, N, A, LDA, VS, LDVS )
*
*        Generate unitary matrix in VS
*        (CWorkspace: need 2*N-1, prefer N+(N-1)*NB)
*        (RWorkspace: none)
*
         CALL ZUNGHR( N, ILO, IHI, VS, LDVS, WORK( ITAU ), WORK( IWRK ),
     $                LWORK-IWRK+1, IERR )
      END IF
*
      SDIM = 0
*
*     Perform QR iteration, accumulating Schur vectors in VS if desired
*     (CWorkspace: need 1, prefer HSWORK (see comments) )
*     (RWorkspace: none)
*
      IWRK = ITAU
      CALL ZHSEQR( 'S', JOBVS, N, ILO, IHI, A, LDA, W, VS, LDVS,
     $             WORK( IWRK ), LWORK-IWRK+1, IEVAL )
      IF( IEVAL.GT.0 )
     $   INFO = IEVAL
*
*     Sort eigenvalues if desired
*
      IF( WANTST .AND. INFO.EQ.0 ) THEN
         IF( SCALEA )
     $      CALL ZLASCL( 'G', 0, 0, CSCALE, ANRM, N, 1, W, N, IERR )
         DO 10 I = 1, N
            BWORK( I ) = SELECT( W( I ) )
   10    CONTINUE
*
*        Reorder eigenvalues, transform Schur vectors, and compute
*        reciprocal condition numbers
*        (CWorkspace: if SENSE is not 'N', need 2*SDIM*(N-SDIM)
*                     otherwise, need none )
*        (RWorkspace: none)
*
         CALL ZTRSEN( SENSE, JOBVS, BWORK, N, A, LDA, VS, LDVS, W, SDIM,
     $                RCONDE, RCONDV, WORK( IWRK ), LWORK-IWRK+1,
     $                ICOND )
         IF( .NOT.WANTSN )
     $      MAXWRK = MAX( MAXWRK, 2*SDIM*( N-SDIM ) )
         IF( ICOND.EQ.-14 ) THEN
*
*           Not enough complex workspace
*
            INFO = -15
         END IF
      END IF
*
      IF( WANTVS ) THEN
*
*        Undo balancing
*        (CWorkspace: none)
*        (RWorkspace: need N)
*
         CALL ZGEBAK( 'P', 'R', N, ILO, IHI, RWORK( IBAL ), N, VS, LDVS,
     $                IERR )
      END IF
*
      IF( SCALEA ) THEN
*
*        Undo scaling for the Schur form of A
*
         CALL ZLASCL( 'U', 0, 0, CSCALE, ANRM, N, N, A, LDA, IERR )
         CALL ZCOPY( N, A, LDA+1, W, 1 )
         IF( ( WANTSV .OR. WANTSB ) .AND. INFO.EQ.0 ) THEN
            DUM( 1 ) = RCONDV
            CALL DLASCL( 'G', 0, 0, CSCALE, ANRM, 1, 1, DUM, 1, IERR )
            RCONDV = DUM( 1 )
         END IF
      END IF
*
      WORK( 1 ) = MAXWRK
      RETURN
*
*     End of ZGEESX
*
      END
