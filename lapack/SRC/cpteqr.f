      SUBROUTINE CPTEQR( COMPZ, N, D, E, Z, LDZ, WORK, INFO )
*
*  -- LAPACK routine (version 1.0a) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     June 30, 1992
*
*     .. Scalar Arguments ..
      CHARACTER          COMPZ
      INTEGER            INFO, LDZ, N
*     ..
*     .. Array Arguments ..
      REAL               D( * ), E( * ), WORK( * )
      COMPLEX            Z( LDZ, * )
*     ..
*
*  Purpose
*  =======
*
*  CPTEQR computes all eigenvalues and, optionally, eigenvectors of a
*  symmetric positive definite tridiagonal matrix by first factoring the
*  matrix using SPTTRF and then calling CBDSQR to compute the singular
*  values of the bidiagonal factor.  This routine computes the
*  eigenvalues of the positive definite tridiagonal matrix to high
*  relative accuracy.  This means that if the eigenvalues range over
*  many orders of magnitude in size, then the small eigenvalues and
*  corresponding eigenvectors will be computed more accurately than, for
*  example, with the standard QR method.  The eigenvectors of a full or
*  band complex Hermitian matrix can also be found if CHETRD or CHPTRD
*  or CHBTRD has been used to reduce this matrix to tridiagonal form.
*  (The reduction to tridiagonal form, however, may preclude the
*  possibility of obtaining high relative accuracy in the small
*  eigenvalues of the original matrix, if these eigenvalues range over
*  many orders of magnitude.)
*
*  Arguments
*  =========
*
*  COMPZ   (input) CHARACTER*1
*          Specifies whether eigenvectors are to be computed
*          as follows
*
*             COMPZ = 'N' or 'n'   Compute eigenvalues only.
*
*             COMPZ = 'V' or 'v'   Compute eigenvectors of original
*                                  symmetric matrix also.
*                                  Array Z contains the unitary
*                                  matrix used to reduce the original
*                                  matrix to tridiagonal form.
*
*             COMPZ = 'I' or 'i'   Compute eigenvectors of
*                                  tridiagonal matrix also.
*
*  N       (input) INTEGER
*          The number of rows and columns in the matrix.  N >= 0.
*
*  D       (input/output) REAL array, dimension (N)
*          On entry, D contains the diagonal elements of the
*          tridiagonal matrix.
*          On normal exit, D contains the eigenvalues, in descending
*          order.
*
*  E       (input/output) REAL array, dimension (N)
*          On entry, E contains the subdiagonal elements of the
*          tridiagonal matrix in positions 1 through N-1.
*          E(N) is arbitrary.
*          On exit, E has been destroyed.
*
*  Z       (input/output) COMPLEX array, dimension (LDZ, N)
*          If  COMPZ = 'V' or 'v', then:
*          On entry, Z contains the unitary matrix used in the
*          reduction to tridiagonal form.
*          If  COMPZ = 'V' or 'v' or 'I' or 'i', then:
*          On exit, Z contains the orthonormal eigenvectors of the
*          symmetric tridiagonal (or full) matrix.  If an error exit
*          is made, Z contains the eigenvectors associated with the
*          stored eigenvalues.
*
*          If  COMPZ = 'N' or 'n', then Z is not referenced.
*
*  LDZ     (input) INTEGER
*          The leading dimension of the array Z.  If eigenvectors are
*          desired, then  LDZ >= max( 1, N ).  In any case, LDZ >= 1.
*
*  WORK    (workspace) REAL array, dimension (max(1,4*N-4))
*          Workspace used in computing eigenvectors.
*          If  COMPZ = 'N' or 'n', then WORK is not referenced.
*
*  INFO    (output) INTEGER
*          = 0:  successful exit.
*          < 0:  if INFO = -i, the i-th argument had an illegal value.
*          > 0:  if INFO = +i, 1 <= i <= N, the Cholesky factorization
*                              of the matrix could not be performed
*                              because the i-th principal minor was not
*                              positive definite.
*                if INFO = N+i, 1 <= i <= N, the i-th singular value
*                              of the bidiagonal factor failed to
*                              converge.
*
*     .. Parameters ..
      COMPLEX            CZERO, CONE
      PARAMETER          ( CZERO = 0.0, CONE = 1.0 )
*     ..
*     .. External Functions ..
      LOGICAL            LSAME
      EXTERNAL           LSAME
*     ..
*     .. External Subroutines ..
      EXTERNAL           CBDSQR, CLAZRO, SPTTRF, XERBLA
*     ..
*     .. Local Arrays ..
      COMPLEX            C( 1, 1 ), VT( 1, 1 )
*     ..
*     .. Local Scalars ..
      INTEGER            I, ICOMPZ, NRU
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          MAX, SQRT
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
*
      IF( LSAME( COMPZ, 'N' ) ) THEN
         ICOMPZ = 0
      ELSE IF( LSAME( COMPZ, 'V' ) ) THEN
         ICOMPZ = 1
      ELSE IF( LSAME( COMPZ, 'I' ) ) THEN
         ICOMPZ = 2
      ELSE
         ICOMPZ = -1
      END IF
      IF( ICOMPZ.LT.0 ) THEN
         INFO = -1
      ELSE IF( N.LT.0 ) THEN
         INFO = -2
      ELSE IF( ( LDZ.LT.1 ) .OR. ( ICOMPZ.GT.0 .AND. LDZ.LT.MAX( 1,
     $         N ) ) ) THEN
         INFO = -6
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'CPTEQR', -INFO )
         RETURN
      END IF
*
*     Quick return if possible
*
      IF( N.EQ.0 )
     $   RETURN
*
      IF( N.EQ.1 ) THEN
         IF( ICOMPZ.GT.0 )
     $      Z( 1, 1 ) = CONE
         RETURN
      END IF
      IF( ICOMPZ.EQ.2 )
     $   CALL CLAZRO( N, N, CZERO, CONE, Z, LDZ )
*
*     Call SPTTRF to factor the matrix.
*
      CALL SPTTRF( N, D, E, INFO )
      IF( INFO.NE.0 )
     $   RETURN
      DO 10 I = 1, N
         D( I ) = SQRT( D( I ) )
   10 CONTINUE
      DO 20 I = 1, N - 1
         E( I ) = E( I )*D( I )
   20 CONTINUE
*
*     Call CBDSQR to compute the singular values/vectors of the
*     bidiagonal factor.
*
      IF( ICOMPZ.GT.0 ) THEN
         NRU = N
      ELSE
         NRU = 0
      END IF
      CALL CBDSQR( 'Lower', N, 0, NRU, 0, D, E, VT, 1, Z, LDZ, C, 1,
     $             WORK, INFO )
*
*     Square the singular values.
*
      IF( INFO.EQ.0 ) THEN
         DO 30 I = 1, N
            D( I ) = D( I )*D( I )
   30    CONTINUE
      ELSE
         INFO = N + INFO
      END IF
*
      RETURN
*
*     End of CPTEQR
*
      END
