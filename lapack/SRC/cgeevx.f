      SUBROUTINE CGEEVX( BALANC, JOBVL, JOBVR, SENSE, N, A, LDA, W, VL,
     $                   LDVL, VR, LDVR, ILO, IHI, SCALE, ABNRM, RCONDE,
     $                   RCONDV, WORK, LWORK, RWORK, INFO )
*
*  -- LAPACK driver routine (version 1.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     February 29, 1992
*
*     .. Scalar Arguments ..
      CHARACTER          BALANC, JOBVL, JOBVR, SENSE
      INTEGER            IHI, ILO, INFO, LDA, LDVL, LDVR, LWORK, N
      REAL               ABNRM
*     ..
*     .. Array Arguments ..
      REAL               RCONDE( * ), RCONDV( * ), RWORK( * ),
     $                   SCALE( * )
      COMPLEX            A( LDA, * ), VL( LDVL, * ), VR( LDVR, * ),
     $                   W( * ), WORK( * )
*     ..
*
*  Purpose
*  =======
*
*  For an N by N complex nonsymmetric matrix A, compute
*
*     the eigenvalues (W)
*     the left and/or right eigenvectors (VL and VR)
*     a balancing transformation to improve the conditioning of the
*        eigenvalues and eigenvectors (ILO, IHI, SCALE, and ABNRM)
*     reciprocal condition numbers for the eigenvalues (RCONDE)
*     reciprocal condition numbers for the right eigenvectors (RCONDV)
*
*  The last four outputs are optional:
*
*     JOBVL determines whether to compute left eigenvectors VL
*     JOBVR determines whether to compute right eigenvectors VR
*     BALANC determines how to balance the matrix (output in ILO, IHI,
*        SCALE, and ABNRM)
*     SENSE determines whether to compute reciprocal condition numbers
*        RCONDE and RCONDV
*
*  Balancing a matrix means permuting the rows and columns to make it
*  more nearly upper triangular, and computing a diagonal similarity
*  D * A * D**(-1), D a diagonal matrix, to make its rows and columns
*  closer in norm and the condition numbers of its eigenvalues and
*  eigenvectors smaller. These two steps, permuting and diagonal
*  scaling, may be applied independently as determined by BALANC.
*  The one-norm of the balanced matrix (the maximum of the sum of
*  absolute values of entries of any column) is returned in ABNRM.
*
*  The reciprocal condition numbers correspond to the balanced matrix.
*  Permuting rows and columns will not change the condition numbers
*  (in exact arithmetic) but diagonal scaling will.
*
*  The reciprocal of the condition number of an eigenvalue lambda
*  is defined as
*
*          RCONDE(lambda) = |v'*u| / norm(u) * norm(v)
*
*  where u and v are the right and left eigenvectors of T
*  corresponding to lambda (v' denotes the conjugate transpose
*  of v), and norm(u) denotes the Euclidean norm.
*  These reciprocal condition numbers always lie between zero
*  (very badly conditioned) and one (very well conditioned).
*  These reciprocal condition numbers are returned in the array RCONDE.
*  An approximate error bound for a computed eigenvalue W(i) is given by
*
*                      EPS * ABRNM / RCONDE(i)
*
*  where EPS = SLAMCH( 'P' ) is the machine precision.
*
*  The reciprocal condition number of the right eigenvector u
*  corresponding to lambda is defined as follows. Suppose
*
*              T = [ lambda  c  ]
*                  [   0    T22 ]
*
*  Then the reciprocal condition number is
*
*          RCONDV(lambda,T22) = sigma-min(T22 - lambda*I)
*
*  where sigma-min denotes the smallest singular value.
*  We approximate the smallest singular value by the reciprocal of
*  an estimate of the one-norm of the inverse of T22 - lambda*I.
*  When RCONDV is small, small changes in the matrix can cause large
*  changes in u. These reciprocal condition numbers are returned in
*  the array RCONDV.
*  If BALANC = 'N' or 'P', an approximate error bound for a computed
*  right eigenvector VR(i) is given by
*
*                      EPS * ABRNM / RCONDV(i)
*
*  where EPS = SLAMCH( 'P' ) is the machine precision.  When
*  BALANC = 'S' or 'B', the interpretation of RCONDV(i) is more complex.
*
*  See section 4.9 of the LAPACK Users' Guide for a detailed discussion
*  of these condition numbers.
*
*  Arguments
*  =========
*
*  BALANC  (input) CHARACTER*1
*          Indicates how the input matrix should be diagonally scaled
*          and/or permuted to improve the conditioning of its
*          eigenvalues.
*          = 'N': Do not diagonally scale or permute.
*          = 'P': Perform permutations to make the matrix more nearly
*                 upper triangular. Do not diagonally scale.
*          = 'S': Diagonally scale the matrix, ie. replace A by
*                 D*A*D**(-1), where D is a diagonal matrix chosen
*                 to make the rows and columns of A more equal in
*                 norm. Do not permute.
*          = 'B': Both diagonally scale and permute A.
*
*          Computed reciprocal condition numbers will be for the matrix
*          after balancing and/or permuting. Permuting does not change
*          condition numbers (in exact arithmetic), but balancing does.
*
*  JOBVL   (input) CHARACTER*1
*          Specifies whether or not to compute left eigenvectors of A.
*          = 'N': left eigenvectors are not computed.
*          = 'V': left eigenvectors are computed.
*
*          If SENSE = 'E' or 'B', JOBVL must = 'V'.
*
*  JOBVR   (input) CHARACTER*1
*          Specifies whether or not to compute right eigenvectors of A.
*          = 'N': right eigenvectors are not computed.
*          = 'V': right eigenvectors are computed.
*
*          If SENSE = 'E' or 'B', JOBVR must = 'V'.
*
*  SENSE   (input) CHARACTER*1
*          Determines which reciprocal condition numbers are computed.
*          = 'N': None are computed.
*          = 'E': Computed for eigenvalues only.
*          = 'V': Computed for right eigenvectors only.
*          = 'B': Computed for eigenvalues and right eigenvectors.
*
*          If SENSE = 'E' or 'B', both left and right eigenvectors
*          must also be computed (JOBVL = 'V' and JOBVR = 'V').
*
*  N       (input) INTEGER
*          The number of rows and columns of the input matrix A. N >= 0.
*
*  A       (input/output) COMPLEX array, dimension (LDA,N)
*          On entry, A is the matrix whose eigenvalues and eigenvectors
*          are desired.
*          On exit, A has been overwritten.  If JOBVL = 'V' or
*          JOBVR = 'V', the upper Hessenberg part of A has been
*          overwritten with the Schur form of the balanced version of A.
*
*  LDA     (input) INTEGER
*          The leading dimension of the array A.  LDA >= max(1,N).
*
*  W       (output) COMPLEX array, dimension (N)
*          On exit, W contains the computed eigenvalues.
*
*  VL      (output) COMPLEX array, dimension (LDVL,N)
*          The left eigenvectors will be stored one after another in
*          the columns of VL, in the same order as their eigenvalues.
*          The eigenvectors will be normalized to have Euclidean
*          norm equal to 1 and largest component real.
*          Specifically, VL(j)' * A = W(j) * VL(j)'  where ' means
*          conjugate-transpose.  Left eigenvectors of A are the same as
*          the right eigenvectors of conjugate-transpose(A).
*
*          If JOBVL = 'N', VL is not referenced.
*
*  LDVL    (input) INTEGER
*          The leading dimension of the array VL.  LDVL >= 1, and if
*          JOBVL = 'V', LDVL >= N.
*
*  VR      (output) COMPLEX array, dimension (LDVR,N)
*          The right eigenvectors will be stored one after another in
*          the columns of VR, in the same order as their eigenvalues.
*          The eigenvectors will be normalized to have Euclidean
*          norm equal to 1 and largest component real.
*          Specifically, A * VR(j) = W(j) * VR(j).
*
*          If JOBVR = 'N', VR is not referenced.
*
*  LDVR    (input) INTEGER
*          The leading dimension of the array VR.  LDVR >= 1, and if
*          JOBVR = 'V', LDVR >= N.
*
*  ILO,IHI (output) INTEGER
*          On exit, ILO, IHI and SCALE describe how A was balanced.
*          The balanced A(i,j) is equal to zero if I is greater than
*          J and J = 1,...,ILO-1 or I = IHI+1,...,N.
*
*  SCALE   (output) REAL array, dimension (N)
*          On exit, SCALE contains information determining the
*          permutations and diagonal scaling factors used in balancing.
*          Suppose that the principal submatrix in rows ILO through
*          IHI has been balanced, that P(J) denotes the index inter-
*          changed with J during the permutation step, and that the
*          elements of the diagonal matrix used in diagonal scaling
*          are denoted by D(I,J).  Then
*          SCALE(J) = P(J),    for J = 1,...,ILO-1
*                   = D(J,J),      J = ILO,...,IHI
*                   = P(J)         J = IHI+1,...,N.
*          the order in which the interchanges are made is N to IHI+1,
*          then 1 to ILO-1.
*
*  ABNRM   (output) REAL
*          On exit, the one-norm of the balanced matrix (the maximum
*          of the sum of absolute values of entries of any column)
*          is returned in ABNRM.
*
*  RCONDE  (output) REAL array, dimension (N)
*          RCONDE(i) is the reciprocal condition number of eigenvalue
*          W(i).
*
*  RCONDV  (output) REAL array, dimension (N)
*          RCONDV(i) is the reciprocal condition number of the i-th
*          right eigenvector in VR.
*
*  WORK    (workspace/output) COMPLEX array, dimension (LWORK)
*          On exit, WORK(1) contains the optimal workspace size LWORK
*          for high performance.
*
*  LWORK   (input) INTEGER
*          The dimension of the array WORK.  If SENSE = 'N' or 'E',
*          LWORK >= max(1,2*N), and if SENSE = 'V' or 'B',
*          LWORK >= N*N+2*N.
*          For good performance, LWORK must generally be larger.
*          The optimum value of LWORK for high performance is
*          returned in WORK(1).
*
*  RWORK   (workspace) REAL array, dimension (2*N)
*
*  INFO    (output) INTEGER
*          = 0: successful exit
*          < 0: if INFO = -i, the i-th argument had an illegal value.
*          > 0: the QR algorithm failed to compute all the eigenvalues;
*               if INFO = i, elements 1:ILO-1 and i+1:N of W contain
*               eigenvalues which have converged, and the eigenvectors
*               are not computed.
*
*  =====================================================================
*
*     .. Parameters ..
      REAL               ZERO, ONE
      PARAMETER          ( ZERO = 0.0E0, ONE = 1.0E0 )
*     ..
*     .. Local Scalars ..
      LOGICAL            SCALEA, WANTVL, WANTVR, WNTSNB, WNTSNE, WNTSNN,
     $                   WNTSNV
      CHARACTER          JOB, SIDE
      INTEGER            HSWORK, I, ICOND, IERR, ITAU, IWRK, K, MAXB,
     $                   MAXWRK, MINWRK, NOUT
      REAL               ANRM, BIGNUM, CSCALE, EPS, SCL, SMLNUM
      COMPLEX            TMP
*     ..
*     .. Local Arrays ..
      LOGICAL            SELECT( 1 )
      REAL               DUM( 1 )
*     ..
*     .. External Subroutines ..
      EXTERNAL           CGEBAK, CGEBAL, CGEHRD, CHSEQR, CLACPY, CLASCL,
     $                   CSCAL, CSSCAL, CTREVC, CTRSNA, CUNGHR, SLABAD,
     $                   SLASCL, XERBLA
*     ..
*     .. External Functions ..
      LOGICAL            LSAME
      INTEGER            ILAENV, ISAMAX
      REAL               CLANGE, SCNRM2, SLAMCH
      EXTERNAL           LSAME, ILAENV, ISAMAX, CLANGE, SCNRM2, SLAMCH
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          AIMAG, CMPLX, CONJG, MAX, MIN, REAL, SQRT
*     ..
*     .. Executable Statements ..
*
*     Test the input arguments
*
      INFO = 0
      WANTVL = LSAME( JOBVL, 'V' )
      WANTVR = LSAME( JOBVR, 'V' )
      WNTSNN = LSAME( SENSE, 'N' )
      WNTSNE = LSAME( SENSE, 'E' )
      WNTSNV = LSAME( SENSE, 'V' )
      WNTSNB = LSAME( SENSE, 'B' )
      IF( .NOT.( LSAME( BALANC, 'N' ) .OR. LSAME( BALANC, 'S' ) .OR.
     $    LSAME( BALANC, 'P' ) .OR. LSAME( BALANC, 'B' ) ) ) THEN
         INFO = -1
      ELSE IF( ( .NOT.WANTVL ) .AND. ( .NOT.LSAME( JOBVL, 'N' ) ) ) THEN
         INFO = -2
      ELSE IF( ( .NOT.WANTVR ) .AND. ( .NOT.LSAME( JOBVR, 'N' ) ) ) THEN
         INFO = -3
      ELSE IF( .NOT.( WNTSNN .OR. WNTSNE .OR. WNTSNB .OR. WNTSNV ) .OR.
     $         ( ( WNTSNE .OR. WNTSNB ) .AND. .NOT.( WANTVL .AND.
     $         WANTVR ) ) ) THEN
         INFO = -4
      ELSE IF( N.LT.0 ) THEN
         INFO = -5
      ELSE IF( LDA.LT.MAX( 1, N ) ) THEN
         INFO = -7
      ELSE IF( LDVL.LT.1 .OR. ( WANTVL .AND. LDVL.LT.N ) ) THEN
         INFO = -10
      ELSE IF( LDVR.LT.1 .OR. ( WANTVR .AND. LDVR.LT.N ) ) THEN
         INFO = -12
      END IF
*
*     Compute workspace
*      (Note: Comments in the code beginning "Workspace:" describe the
*       minimal amount of workspace needed at that point in the code,
*       as well as the preferred amount for good performance.
*       CWorkspace refers to complex workspace, and RWorkspace to real
*       workspace. NB refers to the optimal block size for the
*       immediately following subroutine, as returned by ILAENV.
*       HSWORK refers to the workspace preferred by CHSEQR, as
*       calculated below. HSWORK is computed assuming ILO=1 and IHI=N,
*       the worst case.)
*
      MINWRK = 1
      IF( INFO.EQ.0 .AND. LWORK.GE.1 ) THEN
         MAXWRK = N + N*ILAENV( 1, 'CGEHRD', ' ', N, 1, N, 0 )
         IF( ( .NOT.WANTVL ) .AND. ( .NOT.WANTVR ) ) THEN
            MINWRK = MAX( 1, 2*N )
            IF( .NOT.( WNTSNN .OR. WNTSNE ) )
     $         MINWRK = MAX( MINWRK, N*N+2*N )
            MAXB = MAX( ILAENV( 8, 'CHSEQR', 'SN', N, 1, N, -1 ), 2 )
            IF( WNTSNN ) THEN
               K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'CHSEQR', 'EN', N,
     $             1, N, -1 ) ) )
            ELSE
               K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'CHSEQR', 'SN', N,
     $             1, N, -1 ) ) )
            END IF
            HSWORK = MAX( K*( K+2 ), 2*N )
            MAXWRK = MAX( MAXWRK, 1, HSWORK )
            IF( .NOT.( WNTSNN .OR. WNTSNE ) )
     $         MAXWRK = MAX( MAXWRK, N*N+2*N )
         ELSE
            MINWRK = MAX( 1, 2*N )
            IF( .NOT.( WNTSNN .OR. WNTSNE ) )
     $         MINWRK = MAX( MINWRK, N*N+2*N )
            MAXB = MAX( ILAENV( 8, 'CHSEQR', 'SN', N, 1, N, -1 ), 2 )
            K = MIN( MAXB, N, MAX( 2, ILAENV( 4, 'CHSEQR', 'EN', N, 1,
     $          N, -1 ) ) )
            HSWORK = MAX( K*( K+2 ), 2*N )
            MAXWRK = MAX( MAXWRK, 1, HSWORK )
            MAXWRK = MAX( MAXWRK, N+( N-1 )*
     $               ILAENV( 1, 'CUNGHR', ' ', N, 1, N, -1 ) )
            IF( .NOT.( WNTSNN .OR. WNTSNE ) )
     $         MAXWRK = MAX( MAXWRK, N*N+2*N )
            MAXWRK = MAX( MAXWRK, 2*N, 1 )
         END IF
         WORK( 1 ) = MAXWRK
      END IF
      IF( LWORK.LT.MINWRK ) THEN
         INFO = -20
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'CGEEVX', -INFO )
         RETURN
      END IF
*
*     Quick return if possible
*
      IF( N.EQ.0 )
     $   RETURN
*
*     Get machine constants
*
      EPS = SLAMCH( 'P' )
      SMLNUM = SLAMCH( 'S' )
      BIGNUM = ONE / SMLNUM
      CALL SLABAD( SMLNUM, BIGNUM )
      SMLNUM = SQRT( SMLNUM ) / EPS
      BIGNUM = ONE / SMLNUM
*
*     Scale A if max entry outside range [SMLNUM,BIGNUM]
*
      ICOND = 0
      ANRM = CLANGE( 'M', N, N, A, LDA, DUM )
      SCALEA = .FALSE.
      IF( ANRM.GT.ZERO .AND. ANRM.LT.SMLNUM ) THEN
         SCALEA = .TRUE.
         CSCALE = SMLNUM
      ELSE IF( ANRM.GT.BIGNUM ) THEN
         SCALEA = .TRUE.
         CSCALE = BIGNUM
      END IF
      IF( SCALEA )
     $   CALL CLASCL( 'G', 0, 0, ANRM, CSCALE, N, N, A, LDA, IERR )
*
*     Balance the matrix and compute ABNRM
*
      CALL CGEBAL( BALANC, N, A, LDA, ILO, IHI, SCALE, IERR )
      ABNRM = CLANGE( '1', N, N, A, LDA, DUM )
      IF( SCALEA ) THEN
         DUM( 1 ) = ABNRM
         CALL SLASCL( 'G', 0, 0, CSCALE, ANRM, 1, 1, DUM, 1, IERR )
         ABNRM = DUM( 1 )
      END IF
*
*     Reduce to upper Hessenberg form
*     (CWorkspace: need 2*N, prefer N+N*NB)
*     (RWorkspace: none)
*
      ITAU = 1
      IWRK = ITAU + N
      CALL CGEHRD( N, ILO, IHI, A, LDA, WORK( ITAU ), WORK( IWRK ),
     $             LWORK-IWRK+1, IERR )
*
      IF( WANTVL ) THEN
*
*        Want left eigenvectors
*        Copy Householder vectors to VL
*
         SIDE = 'L'
         CALL CLACPY( 'L', N, N, A, LDA, VL, LDVL )
*
*        Generate unitary matrix in VL
*        (CWorkspace: need 2*N-1, prefer N+(N-1)*NB)
*        (RWorkspace: none)
*
         CALL CUNGHR( N, ILO, IHI, VL, LDVL, WORK( ITAU ), WORK( IWRK ),
     $                LWORK-IWRK+1, IERR )
*
*        Perform QR iteration, accumulating Schur vectors in VL
*        (CWorkspace: need 1, prefer HSWORK (see comments) )
*        (RWorkspace: none)
*
         IWRK = ITAU
         CALL CHSEQR( 'S', 'V', N, ILO, IHI, A, LDA, W, VL, LDVL,
     $                WORK( IWRK ), LWORK-IWRK+1, INFO )
*
         IF( WANTVR ) THEN
*
*           Want left and right eigenvectors
*           Copy Schur vectors to VR
*
            SIDE = 'B'
            CALL CLACPY( 'F', N, N, VL, LDVL, VR, LDVR )
         END IF
*
      ELSE IF( WANTVR ) THEN
*
*        Want right eigenvectors
*        Copy Householder vectors to VR
*
         SIDE = 'R'
         CALL CLACPY( 'L', N, N, A, LDA, VR, LDVR )
*
*        Generate unitary matrix in VR
*        (CWorkspace: need 2*N-1, prefer N+(N-1)*NB)
*        (RWorkspace: none)
*
         CALL CUNGHR( N, ILO, IHI, VR, LDVR, WORK( ITAU ), WORK( IWRK ),
     $                LWORK-IWRK+1, IERR )
*
*        Perform QR iteration, accumulating Schur vectors in VR
*        (CWorkspace: need 1, prefer HSWORK (see comments) )
*        (RWorkspace: none)
*
         IWRK = ITAU
         CALL CHSEQR( 'S', 'V', N, ILO, IHI, A, LDA, W, VR, LDVR,
     $                WORK( IWRK ), LWORK-IWRK+1, INFO )
*
      ELSE
*
*        Compute eigenvalues only
*        If condition numbers desired, compute Schur form
*
         IF( WNTSNN ) THEN
            JOB = 'E'
         ELSE
            JOB = 'S'
         END IF
*
*        (CWorkspace: need 1, prefer HSWORK (see comments) )
*        (RWorkspace: none)
*
         IWRK = ITAU
         CALL CHSEQR( JOB, 'N', N, ILO, IHI, A, LDA, W, VR, LDVR,
     $                WORK( IWRK ), LWORK-IWRK+1, INFO )
      END IF
*
*     If INFO > 0 from CHSEQR, then quit
*
      IF( INFO.GT.0 )
     $   GO TO 50
*
      IF( WANTVL .OR. WANTVR ) THEN
*
*        Compute left and/or right eigenvectors
*        (CWorkspace: need 2*N)
*        (RWorkspace: need N)
*
         CALL CTREVC( SIDE, 'O', SELECT, N, A, LDA, VL, LDVL, VR, LDVR,
     $                N, NOUT, WORK( IWRK ), RWORK, IERR )
      END IF
*
*     Compute condition numbers if desired
*     (CWorkspace: need N*N+2*N unless SENSE = 'E')
*     (RWorkspace: need 2*N unless SENSE = 'E')
*
      IF( .NOT.WNTSNN ) THEN
         CALL CTRSNA( SENSE, 'A', SELECT, N, A, LDA, VL, LDVL, VR, LDVR,
     $                RCONDE, RCONDV, N, NOUT, WORK( IWRK ), N, RWORK,
     $                ICOND )
      END IF
*
      IF( WANTVL ) THEN
*
*        Undo balancing of left eigenvectors
*
         CALL CGEBAK( BALANC, 'L', N, ILO, IHI, SCALE, N, VL, LDVL,
     $                IERR )
*
*        Normalize left eigenvectors and make largest component real
*
         DO 20 I = 1, N
            SCL = ONE / SCNRM2( N, VL( 1, I ), 1 )
            CALL CSSCAL( N, SCL, VL( 1, I ), 1 )
            DO 10 K = 1, N
               RWORK( K ) = REAL( VL( K, I ) )**2 +
     $                      AIMAG( VL( K, I ) )**2
   10       CONTINUE
            K = ISAMAX( N, RWORK, 1 )
            TMP = CONJG( VL( K, I ) ) / SQRT( RWORK( K ) )
            CALL CSCAL( N, TMP, VL( 1, I ), 1 )
            VL( K, I ) = CMPLX( REAL( VL( K, I ) ), ZERO )
   20    CONTINUE
      END IF
*
      IF( WANTVR ) THEN
*
*        Undo balancing of right eigenvectors
*
         CALL CGEBAK( BALANC, 'R', N, ILO, IHI, SCALE, N, VR, LDVR,
     $                IERR )
*
*        Normalize right eigenvectors and make largest component real
*
         DO 40 I = 1, N
            SCL = ONE / SCNRM2( N, VR( 1, I ), 1 )
            CALL CSSCAL( N, SCL, VR( 1, I ), 1 )
            DO 30 K = 1, N
               RWORK( K ) = REAL( VR( K, I ) )**2 +
     $                      AIMAG( VR( K, I ) )**2
   30       CONTINUE
            K = ISAMAX( N, RWORK, 1 )
            TMP = CONJG( VR( K, I ) ) / SQRT( RWORK( K ) )
            CALL CSCAL( N, TMP, VR( 1, I ), 1 )
            VR( K, I ) = CMPLX( REAL( VR( K, I ) ), ZERO )
   40    CONTINUE
      END IF
*
*     Undo scaling if necessary
*
   50 CONTINUE
      IF( SCALEA ) THEN
         CALL CLASCL( 'G', 0, 0, CSCALE, ANRM, N-INFO, 1, W( INFO+1 ),
     $                MAX( N-INFO, 1 ), IERR )
         IF( INFO.EQ.0 ) THEN
            IF( ( WNTSNV .OR. WNTSNB ) .AND. ICOND.EQ.0 )
     $         CALL SLASCL( 'G', 0, 0, CSCALE, ANRM, N, 1, RCONDV, N,
     $                      IERR )
         ELSE
            CALL CLASCL( 'G', 0, 0, CSCALE, ANRM, ILO-1, 1, W, N, IERR )
         END IF
      END IF
*
      WORK( 1 ) = MAXWRK
      RETURN
*
*     End of CGEEVX
*
      END
