      SUBROUTINE CTZRQF( M, N, A, LDA, TAU, INFO )
*
*  -- LAPACK routine (version 1.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     February 29, 1992
*
*     .. Scalar Arguments ..
      INTEGER            INFO, LDA, M, N
*     ..
*     .. Array Arguments ..
      COMPLEX            A( LDA, * ), TAU( * )
*     ..
*
*  Purpose
*  =======
*
*  CTZRQF reduces the m by n ( m.le.n ) upper trapezoidal matrix A
*  to upper triangular form by means of unitary transformations.
*
*  The m by n ( m.le.n ) upper trapezoidal matrix A given by
*
*     A = ( U  X ),
*
*  where U is an m by m upper triangular matrix, is factorized as
*
*     A = ( R  0 ) * P,
*
*  where P is an n by n unitary matrix and R is an m by m upper
*  triangular matrix.
*
*  The  factorization is obtained by Householder's method.  The kth
*  transformation matrix, P( k ), whose conjugate transpose is used to
*  introduce zeros into the (m - k + 1)th row of A, is given in the form
*
*     P( k ) = ( I     0   ),
*              ( 0  T( k ) )
*
*  where
*
*     T( k ) = I - tau*u( k )*u( k )',   u( k ) = (   1    ),
*                                                 (   0    )
*                                                 ( z( k ) )
*
*  tau is a scalar and z( k ) is an ( n - m ) element vector.
*  tau and z( k ) are chosen to annihilate the elements of the kth row
*  of X.
*
*  The scalar tau is returned in the kth element of TAU and the vector
*  u( k ) in the kth row of A, such that the elements of z( k ) are
*  in  a( k, m + 1 ), ..., a( k, n ). The elements of R are returned in
*  the upper triangular part of A.
*
*  P is given by
*
*     P =  P( 1 ) * P( 2 ) * ... * P( m ).
*
*  Arguments
*  =========
*
*  M       (input) INTEGER
*          The number of rows of the matrix A.  M >= 0.
*          If M = 0 then an immediate return is effected.
*
*  N       (input) INTEGER
*          The number of columns of the matrix A.  N >= M.
*
*  A       (input/output) COMPLEX array, dimension (LDA,max(1,N))
*
*          On entry, the leading M by N upper trapezoidal part of the
*          array A must contain the matrix to be factorized.
*
*          On exit, the M by M upper triangular part of A will contain
*          the upper triangular matrix R and the remaining M by (N - M)
*          upper trapezoidal part of A will contain details of the
*          factorization as described above.
*
*  LDA     (input) INTEGER
*          The leading dimension of the array A.  LDA >= max(1,M).
*
*  TAU     (output) COMPLEX array, dimension (max(1,M))
*          On exit, TAU( k ) contains the scalar tau( k ) for the
*          ( m - k + 1 )th transformation as described above.  If
*          P( k ) = I  then TAU( k ) = 0.0.
*
*  INFO    (output) INTEGER
*          =    0: successful exit
*          .lt. 0: INFO = -k, the k-th argument had an illegal value
*
*     .. Parameters ..
      COMPLEX            ONE, ZERO
      PARAMETER          ( ONE = 1.0E+0, ZERO = 0.0E+0 )
*     ..
*     .. Local Scalars ..
      INTEGER            I, K, M1
      COMPLEX            ALPHA
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          CONJG, MAX, MIN
*     ..
*     .. External Subroutines ..
      EXTERNAL           CAXPY, CCOPY, CGEMV, CGERC, CLACGV, CLARFG,
     $                   XERBLA
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF( M.LT.0 ) THEN
         INFO = -1
      ELSE IF( N.LT.M ) THEN
         INFO = -2
      ELSE IF( LDA.LT.MAX( 1, M ) ) THEN
         INFO = -4
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'CTZRQF', -INFO )
         RETURN
      END IF
*
*     Perform the factorization.
*
      IF( M.EQ.0 )
     $   RETURN
      IF( M.EQ.N ) THEN
         DO 10 I = 1, N
            TAU( I ) = ZERO
   10    CONTINUE
      ELSE
         M1 = MIN( M+1, N )
         DO 20 K = M, 1, -1
*
*           Use a Householder reflection to zero the kth row of A.
*           First set up the reflection.
*
            A( K, K ) = CONJG( A( K, K ) )
            CALL CLACGV( N-M, A( K, M1 ), LDA )
            ALPHA = A( K, K )
            CALL CLARFG( N-M+1, ALPHA, A( K, M1 ), LDA, TAU( K ) )
            A( K, K ) = ALPHA
            TAU( K ) = CONJG( TAU( K ) )
*
            IF( TAU( K ).NE.ZERO .AND. K.GT.1 ) THEN
*
*              We now perform the operation  A := A*P( k )'.
*
*              Use the first ( k - 1 ) elements of TAU to store  a( k ),
*              where  a( k ) consists of the first ( k - 1 ) elements of
*              the  kth column  of  A.  Also  let  B  denote  the  first
*              ( k - 1 ) rows of the last ( n - m ) columns of A.
*
               CALL CCOPY( K-1, A( 1, K ), 1, TAU, 1 )
*
*              Form   w = a( k ) + B*z( k )  in TAU.
*
               CALL CGEMV( 'No transpose', K-1, N-M, ONE, A( 1, M1 ),
     $                     LDA, A( K, M1 ), LDA, ONE, TAU, 1 )
*
*              Now form  a( k ) := a( k ) - conjg(tau)*w
*              and       B      := B      - conjg(tau)*w*z( k )'.
*
               CALL CAXPY( K-1, -CONJG( TAU( K ) ), TAU, 1, A( 1, K ),
     $                     1 )
               CALL CGERC( K-1, N-M, -CONJG( TAU( K ) ), TAU, 1,
     $                     A( K, M1 ), LDA, A( 1, M1 ), LDA )
            END IF
   20    CONTINUE
      END IF
*
      RETURN
*
*     End of CTZRQF
*
      END
