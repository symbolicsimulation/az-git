      SUBROUTINE ZSPTRI( UPLO, N, AP, IPIV, WORK, INFO )
*
*  -- LAPACK routine (version 1.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     February 29, 1992
*
*     .. Scalar Arguments ..
      CHARACTER          UPLO
      INTEGER            INFO, N
*     ..
*     .. Array Arguments ..
      INTEGER            IPIV( * )
      COMPLEX*16         AP( * ), WORK( * )
*     ..
*
*  Purpose
*  =======
*
*  ZSPTRI computes the inverse of a complex symmetric indefinite matrix
*  A in packed storage using the factorization A = U*D*U' or A = L*D*L'
*  computed by ZSPTRF.
*
*  Arguments
*  =========
*
*  UPLO    (input) CHARACTER*1
*          Specifies whether the details of the factorization are stored
*          as an upper or lower triangular matrix.
*          = 'U':  Upper triangular (form is A = U*D*U')
*          = 'L':  Lower triangular (form is A = L*D*L')
*
*  N       (input) INTEGER
*          The order of the matrix A.  N >= 0.
*
*  AP      (input/output) COMPLEX*16 array, dimension (N*(N+1)/2)
*          On entry, the block diagonal matrix D and the multipliers
*          used to obtain the factor U or L as computed by ZSPTRF,
*          stored as a packed triangular matrix.
*
*          On exit, if INFO = 0, the (symmetric) inverse of the original
*          matrix, stored as a packed triangular matrix. The j-th column
*          of inv(A) is stored in the array AP as follows:
*          if UPLO = 'U', AP(i + (j-1)*j/2) = inv(A)(i,j) for 1<=i<=j;
*          if UPLO = 'L',
*             AP(i + (j-1)*(2n-j)/2) = inv(A)(i,j) for j<=i<=n.
*
*  IPIV    (input) INTEGER array, dimension (N)
*          Details of the interchanges and the block structure of D
*          as determined by ZSPTRF.
*
*  WORK    (workspace) COMPLEX*16 array, dimension (N)
*
*  INFO    (output) INTEGER
*          = 0: successful exit
*          < 0: if INFO = -k, the k-th argument had an illegal value
*          > 0: if INFO = k, D(k,k) = 0; the matrix is singular and its
*               inverse could not be computed.
*
*  =====================================================================
*
*     .. Parameters ..
      COMPLEX*16         ONE, ZERO
      PARAMETER          ( ONE = 1.0D+0, ZERO = 0.0D+0 )
*     ..
*     .. Local Scalars ..
      LOGICAL            UPPER
      INTEGER            ITMP, J, K, KC, KCNEXT, KP, KPC, NALL
      COMPLEX*16         AK, AKKP1, AKP1, D, T, TEMP
*     ..
*     .. External Functions ..
      LOGICAL            LSAME
      COMPLEX*16         ZDOTU
      EXTERNAL           LSAME, ZDOTU
*     ..
*     .. External Subroutines ..
      EXTERNAL           XERBLA, ZCOPY, ZSPMV, ZSWAP
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      UPPER = LSAME( UPLO, 'U' )
      IF( .NOT.UPPER .AND. .NOT.LSAME( UPLO, 'L' ) ) THEN
         INFO = -1
      ELSE IF( N.LT.0 ) THEN
         INFO = -2
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'ZSPTRI', -INFO )
         RETURN
      END IF
*
*     Quick return if possible
*
      IF( N.EQ.0 )
     $   RETURN
*
*     Check that the diagonal matrix D is nonsingular.
*
      IF( UPPER ) THEN
*
*        Upper triangular storage: examine D from bottom to top
*
         KP = N*( N+1 ) / 2
         DO 10 INFO = N, 1, -1
            IF( IPIV( INFO ).GT.0 .AND. AP( KP ).EQ.ZERO )
     $         RETURN
            KP = KP - INFO
   10    CONTINUE
      ELSE
*
*        Lower triangular storage: examine D from top to bottom.
*
         KP = 1
         DO 20 INFO = 1, N
            IF( IPIV( INFO ).GT.0 .AND. AP( KP ).EQ.ZERO )
     $         RETURN
            KP = KP + N - INFO + 1
   20    CONTINUE
      END IF
      INFO = 0
*
      IF( UPPER ) THEN
*
*        Compute inv(A) from the factorization A = U*D*U'.
*
*        K is the main loop index, increasing from 1 to N in steps of
*        1 or 2, depending on the size of the diagonal blocks.
*
         K = 1
         KC = 1
   30    CONTINUE
*
*        If K > N, exit from loop.
*
         IF( K.GT.N )
     $      GO TO 60
*
         IF( IPIV( K ).GT.0 ) THEN
*
*           1 x 1 diagonal block
*
*           Invert the diagonal block.
*
            AP( KC+K-1 ) = ONE / AP( KC+K-1 )
*
*           Compute column K of the inverse.
*
            IF( K.GT.1 ) THEN
               CALL ZCOPY( K-1, AP( KC ), 1, WORK, 1 )
               CALL ZSPMV( UPLO, K-1, -ONE, AP, WORK, 1, ZERO, AP( KC ),
     $                     1 )
               AP( KC+K-1 ) = AP( KC+K-1 ) -
     $                        ZDOTU( K-1, WORK, 1, AP( KC ), 1 )
            END IF
*
*           Interchange rows and columns K and IPIV(K).
*
            KP = IPIV( K )
            IF( KP.NE.K ) THEN
               KPC = ( KP-1 )*KP / 2 + 1
               CALL ZSWAP( KP, AP( KPC ), 1, AP( KC ), 1 )
               KPC = KC + KP - 1
               DO 40 J = K, KP, -1
                  TEMP = AP( KC+J-1 )
                  AP( KC+J-1 ) = AP( KPC )
                  AP( KPC ) = TEMP
                  KPC = KPC - ( J-1 )
   40          CONTINUE
            END IF
            KC = KC + K
            K = K + 1
         ELSE
*
*           2 x 2 diagonal block
*
*           Invert the diagonal block.
*
            KCNEXT = KC + K
            T = AP( KCNEXT+K-1 )
            AK = AP( KC+K-1 ) / T
            AKP1 = AP( KCNEXT+K ) / T
            AKKP1 = AP( KCNEXT+K-1 ) / T
            D = T*( AK*AKP1-ONE )
            AP( KC+K-1 ) = AKP1 / D
            AP( KCNEXT+K ) = AK / D
            AP( KCNEXT+K-1 ) = -AKKP1 / D
*
*           Compute columns K and K+1 of the inverse.
*
            IF( K.GT.1 ) THEN
               CALL ZCOPY( K-1, AP( KC ), 1, WORK, 1 )
               CALL ZSPMV( UPLO, K-1, -ONE, AP, WORK, 1, ZERO, AP( KC ),
     $                     1 )
               AP( KC+K-1 ) = AP( KC+K-1 ) -
     $                        ZDOTU( K-1, WORK, 1, AP( KC ), 1 )
               AP( KCNEXT+K-1 ) = AP( KCNEXT+K-1 ) -
     $                            ZDOTU( K-1, AP( KC ), 1, AP( KCNEXT ),
     $                            1 )
               CALL ZCOPY( K-1, AP( KCNEXT ), 1, WORK, 1 )
               CALL ZSPMV( UPLO, K-1, -ONE, AP, WORK, 1, ZERO,
     $                     AP( KCNEXT ), 1 )
               AP( KCNEXT+K ) = AP( KCNEXT+K ) -
     $                          ZDOTU( K-1, WORK, 1, AP( KCNEXT ), 1 )
            END IF
*
*           Interchange rows and columns K and -IPIV(K).
*
            KP = -IPIV( K )
            IF( KP.NE.K ) THEN
               KPC = ( KP-1 )*KP / 2 + 1
               CALL ZSWAP( KP, AP( KPC ), 1, AP( KC ), 1 )
               KPC = KC + KP - 1
               DO 50 J = K, KP, -1
                  TEMP = AP( KC+J-1 )
                  AP( KC+J-1 ) = AP( KPC )
                  AP( KPC ) = TEMP
                  KPC = KPC - ( J-1 )
   50          CONTINUE
               TEMP = AP( KCNEXT+KP-1 )
               AP( KCNEXT+KP-1 ) = AP( KCNEXT+K-1 )
               AP( KCNEXT+K-1 ) = TEMP
            END IF
            KC = KCNEXT + K + 1
            K = K + 2
         END IF
*
         GO TO 30
   60    CONTINUE
*
      ELSE
*
*        Compute inv(A) from the factorization A = L*D*L'.
*
*        K is the main loop index, increasing from 1 to N in steps of
*        1 or 2, depending on the size of the diagonal blocks.
*
         NALL = N*( N+1 ) / 2
         K = N
         KC = NALL + 1
   70    CONTINUE
*
*        If K < 1, exit from loop.
*
         IF( K.LT.1 )
     $      GO TO 100
*
         KC = KC - ( N-K+1 )
         IF( IPIV( K ).GT.0 ) THEN
*
*           1 x 1 diagonal block
*
*           Invert the diagonal block.
*
            AP( KC ) = ONE / AP( KC )
*
*           Compute column K of the inverse.
*
            IF( K.LT.N ) THEN
               CALL ZCOPY( N-K, AP( KC+1 ), 1, WORK, 1 )
               CALL ZSPMV( UPLO, N-K, -ONE, AP( KC+N-K+1 ), WORK, 1,
     $                     ZERO, AP( KC+1 ), 1 )
               AP( KC ) = AP( KC ) - ZDOTU( N-K, WORK, 1, AP( KC+1 ),
     $                    1 )
            END IF
*
*           Interchange rows and columns K and IPIV(K).
*
            KP = IPIV( K )
            IF( KP.NE.K ) THEN
               KPC = NALL + 1 - ( N-KP+1 )*( N-KP+2 ) / 2
               ITMP = KPC
               DO 80 J = KP, K, -1
                  TEMP = AP( KC+J-K )
                  AP( KC+J-K ) = AP( KPC )
                  AP( KPC ) = TEMP
                  KPC = KPC - ( N-J+1 )
   80          CONTINUE
               KPC = ITMP
               CALL ZSWAP( N-KP+1, AP( KC+KP-K ), 1, AP( KPC ), 1 )
            END IF
            K = K - 1
         ELSE
*
*           2 x 2 diagonal block
*
*           Invert the diagonal block.
*
            KCNEXT = KC - ( N-K+2 )
            T = AP( KCNEXT+1 )
            AK = AP( KCNEXT ) / T
            AKP1 = AP( KC ) / T
            AKKP1 = AP( KCNEXT+1 ) / T
            D = T*( AK*AKP1-ONE )
            AP( KCNEXT ) = AKP1 / D
            AP( KC ) = AK / D
            AP( KCNEXT+1 ) = -AKKP1 / D
*
*           Compute columns K-1 and K of the inverse.
*
            IF( K.LT.N ) THEN
               CALL ZCOPY( N-K, AP( KC+1 ), 1, WORK, 1 )
               CALL ZSPMV( UPLO, N-K, -ONE, AP( KC+( N-K+1 ) ), WORK, 1,
     $                     ZERO, AP( KC+1 ), 1 )
               AP( KC ) = AP( KC ) - ZDOTU( N-K, WORK, 1, AP( KC+1 ),
     $                    1 )
               AP( KCNEXT+1 ) = AP( KCNEXT+1 ) -
     $                          ZDOTU( N-K, AP( KC+1 ), 1,
     $                          AP( KCNEXT+2 ), 1 )
               CALL ZCOPY( N-K, AP( KCNEXT+2 ), 1, WORK, 1 )
               CALL ZSPMV( UPLO, N-K, -ONE, AP( KC+( N-K+1 ) ), WORK, 1,
     $                     ZERO, AP( KCNEXT+2 ), 1 )
               AP( KCNEXT ) = AP( KCNEXT ) -
     $                        ZDOTU( N-K, WORK, 1, AP( KCNEXT+2 ), 1 )
            END IF
*
*           Interchange rows and columns K-1 and -IPIV(K).
*
            KP = -IPIV( K )
            IF( KP.NE.K ) THEN
               TEMP = AP( KCNEXT+1 )
               AP( KCNEXT+1 ) = AP( KCNEXT+KP-( K-1 ) )
               AP( KCNEXT+KP-( K-1 ) ) = TEMP
               KPC = NALL + 1 - ( N-KP+1 )*( N-KP+2 ) / 2
               ITMP = KPC
               DO 90 J = KP, K, -1
                  TEMP = AP( KC+J-K )
                  AP( KC+J-K ) = AP( KPC )
                  AP( KPC ) = TEMP
                  KPC = KPC - ( N-J+1 )
   90          CONTINUE
               KPC = ITMP
               CALL ZSWAP( N-KP+1, AP( KC+KP-K ), 1, AP( KPC ), 1 )
            END IF
            KC = KCNEXT
            K = K - 2
         END IF
*
         GO TO 70
  100    CONTINUE
      END IF
*
      RETURN
*
*     End of ZSPTRI
*
      END
