;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(deftype Hostname ()
  "A type for host names."
  'Simple-String)

(deftype Fill-Style ()
  "A type for gcontext fill styles."
  '(Member :opaque-stippled :solid :stippled :tiled))



 
