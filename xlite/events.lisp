;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. Chisheng Huang, John Alan McDonald, and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================
;;; X event handling
;;; For more details on X events see \cite{Nye88a,Nye88b,CLX89}.
;;;============================================================
;;; These events are always received by top-level-interactors

(defparameter *default-event-mask*
    '(
      ;; These are redundant if we select :pointer-motion
      ;; :button-1-motion
      ;; :button-2-motion
      ;; :button-3-motion
      ;; :button-4-motion
      ;; :button-5-motion
      ;; :button-motion

      ;; :button-press
      ;; :button-release

      ;; :colormap-change

      ;; :enter-window
      :exposure
      ;; :focus-change
      ;; :key-press
      ;; :key-release

      ;; :keymap-state

      ;; :leave-window

      ;; :owner-grab-button
      ;; :pointer-motion
      ;; :pointer-motion-hint

      ;; :property-change
      
      ;; :resize-redirect
      :structure-notify
      ;; :substructure-redirect
      
      :visibility-change
      ))


;;;============================================================

(defun event-mask (event)
 "This function translates a clx event key into the mask needed
to receive that event."
  (ecase event
    (:button-press :button-press)
    (:button-release :button-release)
    (:colormap-notify :colormap-change)
    (:enter-notify :enter-window)
    (:exposure :exposure)
    ((:focus-in :focus-out) :focus-change)
    (:key-press :key-press)
    (:key-release :key-release)
    (:keymap-notify :keymap-change)
    (:leave-notify :leave-window)
    (:motion-notify :pointer-motion)
    (:pointer-motion-hint :pointer-motion-hint)
    (:property-notify :property-change)
    (:resize-request :resize-notify)
    ((:circulate-notify :configure-notify :destroy-notify :gravity-notify
      :map-notify :reparent-notify :unmap-notify)
     :structure-notify)
    ((:circulate-request :configure-request :map-request)
     :substructure-redirect)
    (:visibility-notify :visibility-change)))


(defun event-list->event-mask (events)
     "This function takes a list of input event keywords
and returns a list of event mask keywords that make a CLX window
receive at least the events given."
  (let ((event-mask ()))
    (dolist (event events)
      (pushnew (event-mask (find-symbol (symbol-name event) :Keyword))
	       event-mask))
    (union event-mask *default-event-mask*)))






 
