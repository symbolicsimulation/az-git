;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================
;;; Paint colormap constants

(defconstant -paint-h-mask- #b01110000
  "Which bits determine the hue and saturation of a painted object?")

(defconstant -paint-s-mask- #b10000000
  "Which bit(s) determine(s) the saturation of a painted object?")

(defconstant -paint-hs-mask- (logior -paint-h-mask- -paint-s-mask-)
  "Which bits determine the hue and saturation of a painted object?")

(defconstant -paint-v-mask- (logxor #b11111111 -paint-hs-mask-)
  "Which bits determine the value (brightness) of a painted object?")

(defconstant -paint-hs-bits- (logcount -paint-hs-mask-)
  "How many bits are used to determine the hue and saturation
of a painted object?")

(defconstant -paint-h-bits- (logcount -paint-h-mask-)
  "How many bits are used to determine the hue
of a painted object?")

(defconstant -paint-s-bits- (logcount -paint-s-mask-)
  "How many bits are used to determine the saturation
of a painted object?")

(defconstant -paint-v-bits- (- 8 -paint-hs-bits-)
  "How many bits are used to determine the value (brightness)
 of a painted object?")

(defconstant -paint-hs-values- (ash 1 -paint-hs-bits-)
  "How many possible combinations of hue and saturation are provided?")

(defconstant -paint-h-values- (ash 1 -paint-h-bits-)
  "How many possible hues are provided?")

(defconstant -paint-s-values- (ash 1 -paint-s-bits-)
  "How many possible saturations are provided?")

(defconstant -paint-v-values- (ash 1 -paint-v-bits-)
  "How many possible levels of brightness are provided?")

; (defconstant -needed-pixels- ;; $$$$ -jm
(defparameter -needed-pixels-
    (nconc
     (loop 
	 for i from -paint-v-values-
	 below (* -paint-h-values- -paint-v-values-)
	 collect i)
     (loop 
	 for i from (+ -paint-v-values- (* -paint-h-values- -paint-v-values-))
	 below (* -paint-hs-values- -paint-v-values-)
	 collect i))
  "this is actually assuming -paint-v-values- = 2.")

;(defconstant -strong-names- ;; $$$$ -jm
(defparameter -strong-names-
    '(:red :green :blue :magenta :yellow :cyan))

;(defconstant -weak-names- ;; $$$$ -jm
(defparameter -weak-names-
    '(:weak-red :weak-green :weak-blue :weak-magenta :weak-yellow :weak-cyan))

; (defconstant -sat-names- '(:strong :weak)) ;; $$$$ -jm
(defparameter -sat-names- '(:strong :weak))

;;;------------------------------------------------------------
;;; Paint deftypes

(deftype Hsi () `(Mod ,-paint-hs-values-))

(deftype Hue-Sat-Name ()
  '(Member
    :gray
    :red :green :blue
    :yellow :magenta :cyan
    :weak-gray
    :weak-red :weak-green :weak-blue :weak-yellow :weak-magenta :weak-cyan))

(defun strong-colorname? (colorname)
  (declare (type Hue-Sat-Name colorname))
  (member colorname '(:gray :red :green :blue :yellow :magenta :cyan)))

(defun coerce-saturation (hs-name strength)
  (if (eq hs-name :gray)
      :gray
    ;; else
    (ecase strength
      (:strong (ecase hs-name
		 ((:gray :weak-gray) :gray)
		 ((:red :weak-red) :red)
		 ((:green :weak-green) :green)
		 ((:blue :weak-blue) :blue)
		 ((:yellow :weak-yellow) :yellow)
		 ((:magenta :weak-magenta) :magenta)
		 ((:cyan :weak-cyan) :cyan)))
      (:weak (ecase hs-name
	       ((:gray :weak-gray) :weak-gray)
	       ((:red :weak-red) :weak-red)
	       ((:green :weak-green) :weak-green)
	       ((:blue :weak-blue) :weak-blue)
	       ((:yellow :weak-yellow) :weak-yellow)
	       ((:magenta :weak-magenta) :weak-magenta)
	       ((:cyan :weak-cyan) :weak-cyan))))))

;;;------------------------------------------------------------
;;; Paint macros

(defmacro hs (p)
  "map the hue-saturation index to fully bright pixel value."
  `(logior (ash ,p -paint-v-bits-) -paint-v-mask-))

(defmacro hs+v (p b)
  "Overlay the -paint-hs-bits- most significant bits of the paint pixel <p>
on the band pixel <b> shifted right -paint-hs-bits-"
  `(logior (logand (the az:Card8 ,p) -paint-hs-mask-)
	   (ash (the az:Card8 ,b) (- -paint-hs-bits-))))

(defmacro hs+v-ref (p b &rest indexes)
  "Like hs+v, but looks up in simple pixel vectors."
  `(hs+v (aref ,p ,@indexes) (aref ,b ,@indexes)))

(defmacro paint-wash (pixel paint mask)
  "Combine the <paint> with the existing pixel value <pixel>,
changing only those bits that are on in <mask>."
  `(logior (logand ,paint ,mask) (logandc2 ,pixel ,mask)))

;;;------------------------------------------------------------

(defun hue-sat-name->hsi (colorname)
  (declare (type Hue-Sat-Name colorname))
  (ecase colorname
    (:gray 1)
    (:white 1)
    (:black 1)
    (:magenta 2)
    (:red 3)
    (:yellow 4)
    (:green 5)
    (:cyan 6)
    (:blue 7)
    (:weak-gray 9)
    (:weak-magenta 10)
    (:weak-red 11)
    (:weak-yellow 12)
    (:weak-green 13)
    (:weak-cyan 14)
    (:weak-blue 15)))

(defun hue-sat-name->hs (colorname)
  (hs (hue-sat-name->hsi colorname)))

;;;------------------------------------------------------------

(defparameter *paint-gamma* 0.75d0
  "Gamma correction exponent for paint colormap.")

(defun map-index (colorname v)
  (+ v (* -paint-v-values- (hue-sat-name->hsi colorname))))

(defun gamma-corrected-gray (i)
  (expt (/ i (az:fl (- -paint-v-values- 1))) *paint-gamma*))

(defun fill-paint-colors (colormap)
  (declare (type xlib:Colormap colormap))
  (let ((color (xlib:make-color :red 0.0d0 :blue 0.0d0 :green 0.0d0)))
    (declare (type xlib:Color color))
    (loop for i from 0 below -paint-v-values- do
	  (let* ((gray (gamma-corrected-gray i))
		 (rgb (+ 0.25d0 (* 0.75d0 gray)))
		 (rgb3/4 (* rgb 3/4))
		 (rgb/2 (/ rgb 2)))
	    ;; red part
	    (setf (xlib:color-red color) rgb)	
	    (setf (xlib:color-green color) 0.0d0)
	    (setf (xlib:color-blue color) 0.0d0)
	    (xlib:store-color colormap (map-index :red i) color)    
	    ;; yellow part
	    (setf (xlib:color-green color) rgb)
	    (xlib:store-color colormap (map-index :yellow i) color)    
	    ;; green part
	    (setf (xlib:color-red color) 0.0d0)
	    (xlib:store-color colormap (map-index :green i) color)
	    ;; cyan part
	    (setf (xlib:color-blue color) rgb)
	    (xlib:store-color colormap (map-index :cyan i) color)
	    ;; blue part
	    (setf (xlib:color-green color) 0.0d0)
	    (xlib:store-color colormap (map-index :blue i) color)     
	    ;; magenta part
	    (setf (xlib:color-red color) rgb)
	    (xlib:store-color colormap (map-index :magenta i) color)
	    ;; 2 gray parts are identical
	    (setf (xlib:color-red color) gray)
	    (setf (xlib:color-green color) gray)
	    (setf (xlib:color-blue color) gray)
	    (xlib:store-color colormap (map-index :gray i) color)
	    (xlib:store-color colormap (map-index :weak-gray i) color)

	    ;; desaturated parts
	    ;; red part
	    (setf (xlib:color-red color) rgb3/4)	
	    (setf (xlib:color-green color) rgb/2)
	    (setf (xlib:color-blue color) rgb/2)
	    (xlib:store-color colormap (map-index :weak-red i) color)    
	    ;; yellow part
	    (setf (xlib:color-green color) rgb3/4)
	    (xlib:store-color colormap (map-index :weak-yellow i) color)  
	    ;; green part
	    (setf (xlib:color-red color) rgb/2)
	    (xlib:store-color colormap (map-index :weak-green i) color)
	    ;; cyan part
	    (setf (xlib:color-blue color) rgb3/4)
	    (xlib:store-color colormap (map-index :weak-cyan i) color)
	    ;; blue part
	    (setf (xlib:color-green color) rgb/2)
	    (xlib:store-color colormap (map-index :weak-blue i) color)     
	    ;; magenta part
	    (setf (xlib:color-red color) rgb3/4)
	    (xlib:store-color colormap (map-index :weak-magenta i) color)))))

;;;------------------------------------------------------------

(defun allocate-all-available-pixels (colormap)
  (declare (type xlib:Colormap colormap))
  (dotimes (i 256)
    (let ((pixels (ignore-errors
		   (xlib:alloc-color-cells colormap (- 256 i)))))
      (when pixels (return pixels)))))

(defun colormap-usable? (colormap)
  "This assumes the default colormap is of the right visual type and depth.
It just checks to see if we can allocate the right pixels for a paint 
colormap."
  (declare (type xlib:Colormap colormap))
  (let* ((available (sort (allocate-all-available-pixels colormap) #'<))
	 (unneeded (sort (set-difference available -needed-pixels-) #'<))
	 (*print-length* nil))
    (declare (type List available unneeded)
	     #-sbcl (special *print-length*))
    ;; free the pixels we're sure not to need
    (xlib:free-colors colormap unneeded)
    ;; test to see we get everything we need
    (if (subsetp -needed-pixels- available)
	t
      ;; else free the rest of the allocated pixels and return nil
      (progn
	(xlib:free-colors colormap (intersection available -needed-pixels-))
	nil))))

;;;------------------------------------------------------------

(defun default-colors (colormap)
  (xlib:query-colors
   colormap
   (loop for i from 0 below -paint-v-values- collect i)
   :result-type 'Vector))

(defun copy-default-colors (default-colormap colormap)
  (declare (type xlib:Colormap default-colormap colormap)
	   (:returns (type xlib:Colormap colormap)))
  (let ((default-colors (default-colors default-colormap)))
    (declare (type Vector default-colors))
    (dotimes (i -paint-v-values-)
      (xlib:store-color colormap i (aref default-colors i)))
    colormap))

;;;------------------------------------------------------------

(defparameter *paint-colormaps*
    (az:make-alist-table :test #'xlib:window-equal)
  "A table (plist) to map root windows to colormaps")

(defun paint-colormap (window)
  (let* ((root (drawable-root window))
	 (default-colormap (xlt:default-colormap window))
	 (colormap (az:lookup root *paint-colormaps* nil)))
    (declare (type xlib:Window window)
	     (type xlib:Colormap default-colormap)
	     (type (or Null xlib:Colormap) colormap))
    (when (null colormap)
      (setf colormap
	(if (colormap-usable? default-colormap)
	    default-colormap
	  (progn
	    (warn "Default colormap too full. Creating new one.")
	    (copy-default-colors default-colormap
				 (xlt:make-colormap window)))))
      (fill-paint-colors colormap)
      (setf (az:lookup root *paint-colormaps*) colormap))
    colormap))

;;;------------------------------------------------------------

(defparameter *paint-gray-pixels*
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to pixels.")

(defun paint-gray-pixel (window)
  (declare (type xlib:Window window))
  (let* ((root (drawable-root window))
	 (p (az:lookup root *paint-gray-pixels* -1)))
    (declare (type xlib:Window root)
	     (type Fixnum p))
    (when (minusp p)
      (setf p (xlt:colorname->pixel (paint-colormap root) :gray45))
      (setf (az:lookup root *paint-gray-pixels*) p))
    p))
 

;;;============================================================
;;; Gcontexts for pseudo 3d drawing
;;;============================================================

(defparameter *3d-high-v* 230
  "A brightness index for highlights on 3d objects.")

(defparameter *3d-mid-v* 190
  "A brightness index for neutral regions on 3d objects.")

(defparameter *3d-low-v* 130
  "A brightness index for partially shaded regions on 3d objects.")

(defparameter *3d-shadow-v* 0
  "A brightness index for full shadows cast by floating objects.")

;;;============================================================

(defun 3d-high-gcontext (window hue-sat-name)
  "Get a gcontext that can be used to draw highlights on 3d objects
in the window."
  (declare (type xlib:Window window)
	   (type Hue-Sat-Name hue-sat-name)
	   (:returns (type xlib:Gcontext)))
  (pixel-default-gcontext 
   window 
   (hs+v (hue-sat-name->hs hue-sat-name) *3d-high-v*)))

(defun 3d-mid-gcontext (window hue-sat-name)
  "Get a gcontext that can be used to draw midlights on 3d objects
in the window."
  (declare (type xlib:Window window)
	   (type Hue-Sat-Name hue-sat-name)
	   (:returns (type xlib:Gcontext)))
  (pixel-default-gcontext 
   window
   (hs+v (hue-sat-name->hs hue-sat-name) *3d-mid-v*)))

(defun 3d-low-gcontext (window hue-sat-name)
  "Get a gcontext that can be used to draw lowlights on 3d objects
in the window."
  (declare (type xlib:Window window)
	   (type Hue-Sat-Name hue-sat-name)
	   (:returns (type xlib:Gcontext)))
  (pixel-default-gcontext 
   window 
   (hs+v (hue-sat-name->hs hue-sat-name) *3d-low-v*)))

(defun 3d-shadow-gcontext (window hue-sat-name)
  "Get a gcontext that can be used to draw shadows on 3d objects
in the window."
  (declare (type xlib:Window window)
	   (type Hue-Sat-Name hue-sat-name)
	   (:returns (type xlib:Gcontext)))
  (pixel-default-gcontext 
   window 
   (hs+v (hue-sat-name->hs hue-sat-name) *3d-shadow-v*)))



