;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; -*-
(in-package :cl-user)

(let* ((directory 
	(namestring (make-pathname
		     :directory (pathname-directory *load-truename*)))))
  (load (truename (pathname
		   (concatenate 'String directory "../az.system")))))

(cs :Xlite)
