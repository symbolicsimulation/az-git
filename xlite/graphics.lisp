;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================
;;; Absolute line drawing
;;;============================================================
;;; Argh!  The X line drawing is totally uncontrollable.  I think that
;;; the following will work for horizontal and vertical lines; for any
;;; other lines, there are undoubtedly off-by-one errors. (Sannella)

(defun %draw-line-xy (drawable gcontext x0 y0 x1 y1)
  (declare (type xlib:Drawable drawable)
	   (type xlib:GContext gcontext)
	   (type az:Int16 x0 y0 x1 y1))
  #||
  ;; the following ugly hack tries to simulate mac-syle line drawing,
  ;; by adjusting the endpoints by one to include final pixels
  (when (> x0 x1) (rotatef x0 x1) (rotatef y0 y1))
  (cond ((= x0 x1)
	 (cond ((<= y0 y1) (incf y1))
	       (t (decf y1))))
	((< x0 x1)
	 (cond ((< y0 y1) (incf x1) (incf y1))
	       ((= y0 y1) (incf x1))
	       ((> y0 y1) (decf y1))))
	(t (error "shouldn't be possible to get here")))
  ||#
  (xlib:draw-line drawable gcontext x0 y0 x1 y1))

(defun draw-line-xy (drawable gcontext x0 y0 x1 y1)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 x0 y0 x1 y1))
  (%draw-line-xy drawable gcontext x0 y0 x1 y1))

;;;------------------------------------------------------------

(defun %draw-line (drawable gcontext p0 p1)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point p0 p1))
  (xlib:draw-line drawable gcontext (g:x p0) (g:y p0) (g:x p1) (g:y p1)))

(defun draw-line (drawable gcontext p0 p1)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point p0 p1))
  (%draw-line drawable gcontext p0 p1))

;;;------------------------------------------------------------
;;; Draw several (unconnected) lines with a given gcontext on a given drawable.
;;; This is different from draw-polyline!

(defparameter *segment-coordinate-vector*
    (make-array 64
		:element-type 'az:Int16
		:initial-element 0))

(defun segment-coordinate-vector (n)
  (unless (= (length *segment-coordinate-vector*) n)
    (setf *segment-coordinate-vector*
      (make-array n
		  :element-type 'az:Int16
		  :initial-element 0)))
  *segment-coordinate-vector*)

(defun %draw-lines (drawable gcontext points0 points1)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type List points0 points1))
  (let* ((nsegments (min (length points0) (length points1)))
	 (n (* 4 nsegments))
	 (pxys (segment-coordinate-vector n))
	 (i 0))
    (declare (type az:Int16-Vector pxys)
	     (type az:Int16 nsegments n i))
    (loop for p0 in points0
	for p1 in points1 do
	  (setf (aref pxys i) (g:x p0)) (incf i)
	  (setf (aref pxys i) (g:y p0)) (incf i)
	  (setf (aref pxys i) (g:x p1)) (incf i)
	  (setf (aref pxys i) (g:y p1)) (incf i))
    (xlib:draw-segments drawable gcontext pxys)))

(defun draw-lines (drawable gcontext points0 points1) 
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List points0 points1))
  (%draw-lines drawable gcontext points0 points1))

;;;============================================================
;;; drawing points
;;;============================================================

(defun %draw-point-xy (drawable gcontext x y)
  (xlib:draw-point drawable gcontext x y))

(defun draw-point-xy (drawable gcontext x y)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 x y))
  (%draw-point-xy drawable gcontext x y))

;;;------------------------------------------------------------

(defun %draw-point (drawable gcontext p)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:point p))
  (xlib:draw-point drawable gcontext (g:x p) (g:y p)))

(defun draw-point (drawable gcontext point) 
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:point point))
  (%draw-point drawable gcontext point))

;;;------------------------------------------------------------

(defun %draw-points-xy (drawable gcontext xys)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16-List xys))
  (loop (when (null xys) (return))
    (%draw-point-xy drawable gcontext (pop xys) (pop xys))))

(defun draw-points-xy (drawable gcontext xy-list)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16-List xy-list))
  (%draw-points-xy drawable gcontext xy-list))

;;;------------------------------------------------------------

(defparameter *point-coordinate-vector*
    (make-array 64
		:element-type `az:Int16
		:initial-element 0))

(defun point-coordinate-vector (n)
  (unless (= (length *point-coordinate-vector*) n)
    (setf *point-coordinate-vector*
      (make-array n
		  :element-type 'az:Int16
		  :initial-element 0)))
  *point-coordinate-vector*)

(defun %draw-points (drawable gcontext points)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type List points))
  (let* ((n (* 2 (length points)))
	 (pxys (point-coordinate-vector n))
	 (i 0)) 
    (declare (type az:Int16 n)
	     (type az:Int16-Vector pxys)
	     (type az:Int16 i))
    (dolist (p points)
      (declare (type g:Point p))
      (setf (aref pxys i) (the az:Int16 (g:x p)))
      (incf i)
      (setf (aref pxys i) (the az:Int16 (g:y p)))
      (incf i))
    (xlib:draw-points drawable gcontext pxys)))

(defun draw-points (drawable gcontext point-list)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List point-list))
  (%draw-points drawable gcontext point-list))

;;;============================================================
;;; Arrows
;;;============================================================

(defparameter *arrowhead-angle* (/ pi 10.0d0))
(declaim (type Double-Float *arrowhead-angle*))

(defparameter *arrowhead-length* 10)
(declaim (type Fixnum *arrowhead-length*))

(defun %draw-arrow-xy (drawable gcontext x0 y0 x1 y1)

  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 x0 y0 x1 y1))

  (unless (and (= x0 x1) (= y0 y1))
    (let* ((theta (atan (az:fl (- y1 y0))
			(az:fl (- x1 x0))))
	   (alpha (- pi *arrowhead-angle*))
	   (beta0 (- theta alpha))
	   (beta1 (+ theta alpha))
	   (dl (az:fl *arrowhead-length*))
	   (x2  (+ x1 (the az:Int16
			(truncate
			 (the Double-Float
			   (* dl (the Double-Float (cos beta0))))))))
	   (y2  (+ y1 (the az:Int16
			(truncate 
			 (the Double-Float
			   (* dl (the Double-Float (sin beta0))))))))
	   (x3  (+ x1 (the az:Int16
			(truncate 
			 (the Double-Float
			   (* dl (the Double-Float (cos beta1))))))))
	   (y3  (+ y1 (the az:Int16
			(truncate 
			 (the Double-Float
			   (* dl (the Double-Float (sin beta1)))))))))

      (declare (type az:Int16 x2 y2 x3 y3)
	       (type Double-Float dl theta alpha beta0 beta1))

      (xlib:draw-segments drawable gcontext
			  (list x0 y0 x1 y1 x1 y1 x2 y2 x1 y1 x3 y3)))))



(defun draw-arrow-xy (drawable gcontext x0 y0 x1 y1)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 x0 y0 x1 y1))
  (%draw-arrow-xy drawable gcontext x0 y0 x1 y1))
			
;;;------------------------------------------------------------

(defparameter *p2* (make-array 2 :element-type 'Fixnum))
(defparameter *p3* (make-array 2 :element-type 'Fixnum))
(declaim (type (az:Fixnum-Vector 2) *p2* *p3*))

(defun %draw-arrow (drawable gcontext p0 p1)

  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type g:Point p0 p1)
	   (special *p2* *p3*))

  (unless (az:equal? p0 p1)
    (let* ((x0 (g:x p0))
	   (y0 (g:y p0))
	   (x1 (g:x p1))
	   (y1 (g:y p1))
	   (p2 *p2*)
	   (p3 *p3*))

      (declare (type az:Int16 x0 y0 x1 y1)
	       (type (az:Fixnum-Vector 2) p2 p3))

      (setf (aref p2 0) x0)
      (setf (aref p2 1) y0)
      (setf (aref p3 0) x1)
      (setf (aref p3 1) y1)
      (az:arrowhead_points p2 p3
			   *arrowhead-angle*
			   *arrowhead-length*)
      #||
      (xlib:draw-segments (drawable-drawable drawable) gcontext
			  (list x1 y1 x0 y0
				x1 y1 (aref p2 0) (aref p2 1)
				x1 y1 (aref p3 0) (aref p3 1))
			  )
      ||#
      (xlib:draw-lines drawable gcontext
		       (list x0 y0
			     x1 y1
			     (aref p2 0) (aref p2 1)
			     (aref p3 0) (aref p3 1)
			     x1 y1)))))

(defun draw-arrow (drawable gcontext p0 p1)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:point p0 p1))
  (%draw-arrow drawable gcontext p0 p1))

;;;------------------------------------------------------------

(defun %draw-arrows (drawable gcontext points0 points1)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List points0 points1))
  (mapc #'(lambda (p0 p1) (%draw-arrow drawable gcontext p0 p1))
	points0 points1))

(defun draw-arrows (drawable gcontext points0 points1) 
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List points0 points1))
  (%draw-arrows drawable gcontext points0 points1))

;;;============================================================
;;; Polylines (Piecewise Linear Curves)
;;;============================================================

(defun %draw-polyline-xy (drawable gcontext xy-list)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16-List xy-list))
  (let* ((xys xy-list)
	 (x0 (pop xys))
	 (y0 (pop xys)))
    (loop
      (when (null xys) (return))
      (let ((x1 (pop xys))
	    (y1 (pop xys)))
	(%draw-line-xy drawable gcontext x0 y0 x1 y1)
	(setf x0 x1)
	(setf y0 y1)))))

(defun draw-polyline-xy (drawable gcontext xy-list)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16-List xy-list))
  (%draw-polyline-xy drawable gcontext xy-list))

;;;------------------------------------------------------------

(defparameter *polyline-coordinate-vector*
    (make-array 64
		:element-type `az:Int16
		:initial-element 0))

(defun polyline-coordinate-vector (n)
  (unless (= (length *polyline-coordinate-vector*) n)
    (setf *polyline-coordinate-vector*
      (make-array n
		  :element-type 'az:Int16
		  :initial-element 0)))
  *polyline-coordinate-vector*)

(defun %draw-polyline (drawable gcontext points)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type g:Point-List points))
  (let* ((n (* 2 (length points)))
	 (pxys (polyline-coordinate-vector n))
	 (i 0)) 
    (declare (type az:Int16 n)
	     (type az:Int16-Vector pxys)
	     (type az:Int16 i))
    (dolist (p points)
      (declare (type g:Point p))
      (setf (aref pxys i) (the az:Int16 (g:x p)))
      (incf i)
      (setf (aref pxys i) (the az:Int16 (g:y p)))
      (incf i))
    (xlib:draw-lines drawable gcontext pxys)))

(defun draw-polyline (drawable gcontext point-list)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List point-list))
  (%draw-polyline drawable gcontext point-list))

;;;------------------------------------------------------------

(defun %draw-polylines (drawable gcontext point-list-list)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type List point-list-list))
  (dolist (point-list point-list-list)
    (draw-polyline drawable gcontext point-list)))

(defun draw-polylines (drawable gcontext point-list-list)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type List point-list-list))
  (%draw-polylines drawable gcontext point-list-list))

;;;============================================================ 
;;; copy-area (aka bitblt) operations
;;;============================================================

(defun %copy-area-xy (gcontext from-drawable from-left from-top width height
		      to-drawable to-left to-top)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable from-drawable to-drawable)
	   (type az:Int16
		 from-left from-top to-left to-top)
	   (type az:Card16 width height))
  (let* (;; Have to clip width, height to source drawable,
	 ;; so X won't just assume 0`s
	 (clipped-width
	  (+ 1 (g:iclip
		(- (xlib:drawable-width from-drawable) from-left) 0 width)))
	 (clipped-height
	  (+ 1 (g:iclip
		(- (xlib:drawable-height from-drawable) from-top) 0 height))))
    (xlib:copy-area from-drawable
		    gcontext from-left from-top clipped-width clipped-height
		    to-drawable to-left to-top )))

(defun copy-area-xy (gcontext from-drawable from-left from-top width height
		     to-drawable to-left to-top)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable from-drawable to-drawable)
	   (type az:Int16
		 from-left from-top to-left to-top)
	   (type az:Card16 width height))
  (%copy-area-xy
   gcontext from-drawable from-left from-top width height
   to-drawable to-left to-top))

;;;------------------------------------------------------------

(defun %copy-area (gcontext from-drawable from-rect to-drawable to-point)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable from-drawable to-drawable)
	   (type g:Rect from-rect)
	   (type g:Point to-point))
  (%copy-area-xy gcontext from-drawable
		 (g:x from-rect)
		 (g:y from-rect)
		 (g:w from-rect)
		 (g:h from-rect)
		 to-drawable
		 (g:x to-point)
		 (g:y to-point)))
  
(defun copy-area (gcontext from-drawable from-rect to-drawable to-point)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable from-drawable to-drawable)
	   (type g:Rect from-rect)
	   (type g:Point to-point))
  (%copy-area gcontext from-drawable from-rect to-drawable to-point))

;;;------------------------------------------------------------

(defun %copy-areas (gcontext from-drawable from-rects to-drawable to-points)
  (declare (optimize (safety 0) (speed 3))
           (type xlib:Gcontext gcontext)
           (type Xlib:Drawable from-drawable to-drawable))
  (let* ((from-width (xlib:drawable-width from-drawable))
	 (from-height (xlib:drawable-height from-drawable)))
    (declare (type az:Card16 from-width from-height))
    (loop for r in from-rects
	for p in to-points do
	  (let* ((r r)
		 (p p)
		 (r-left (g:x r))
		 (r-width (g:w r))
		 (r-top (g:y r))
		 (r-height (g:h r))
		 ;; Have to clip width, height to source drawable,
		 ;; so X won't just assume 0`s
		 (clipped-width
		  (+ 1 (g:iclip (- from-width r-left) 0 r-width)))
		 (clipped-height
		  (+ 1 (g:iclip (- from-height r-top) 0 r-height))))
	    (declare (type g:Rect r)
		     (type g:Point p)
		     (type az:Int16 r-left r-top)
		     (type az:Card16
			   r-width r-height clipped-width clipped-height))
	    (xlib:copy-area
	     from-drawable gcontext
	     r-left r-top clipped-width clipped-height
	     to-drawable (g:x p) (g:y p))))))
  
(defun copy-areas (gcontext from-drawable from-rects to-drawable to-points)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable from-drawable to-drawable)
	   (type g:Rect-List from-rects)
	   (type g:Point-List to-points))
  (%copy-areas gcontext from-drawable from-rects to-drawable to-points))

;;;============================================================
;;; drawing rects
;;;============================================================

(defun %draw-rect-xy (drawable gcontext left top width height filled?)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Drawable drawable)
	   (type xlib:GContext gcontext)
	   (type az:Int16 left top)
	   (type az:Card16 width height)
	   (type az:Boolean filled?))
  (let ((thickness (xlib:gcontext-line-width gcontext)))
    (declare (type az:Card16 thickness))
    (unless filled?
      (let ((delta (ash thickness -1)))
	(declare (type az:Card16 delta))
	(setf width (max 0 (- width delta 1)))
	(setf height (max 0 (- height delta 1)))
	(setf left (+ left delta))
	(setf top (+ top delta))))
    (xlib:draw-rectangle drawable gcontext left top width height filled?)))

(defun draw-rect-xy (drawable gcontext left top width height filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 left top)
	   (type az:Card16 width height)
	   (type az:Boolean filled?))
  (%draw-rect-xy drawable gcontext left top width height filled?))

;;;------------------------------------------------------------

(defun %draw-rect (drawable gcontext rect filled?)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type g:Rect rect)
	   (type az:Boolean filled?))
  (let* ((left (g:x rect))
	 (top (g:y rect))
	 (width (g:w rect))
	 (height (g:h rect))
	 (thickness (xlib:gcontext-line-width gcontext)))
    (declare (type az:Int16 left top)
	     (type az:Card16 width height thickness))
    (unless filled?
      (let ((delta (ash thickness -1)))
	(declare (type az:Card16 delta))
	(setf width (max 0 (- width 1 delta)))
	(setf height (max 0 (- height 1 delta)))
	(setf left (+ left delta))
	(setf top (+ top delta))))
    (xlib:draw-rectangle drawable gcontext left top width height filled?)))

(defun draw-rect (drawable gcontext rect filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Rect rect)
	   (type az:Boolean filled?))
  (%draw-rect drawable gcontext rect filled?)) 

;;;------------------------------------------------------------

(defparameter *rect-coordinate-vector*
    (make-array 64
		:element-type `az:Int16
		:initial-element 0))

(defun rect-coordinate-vector (n)
  (unless (= (length *rect-coordinate-vector*) n)
    (setf *rect-coordinate-vector*
      (make-array n
		  :element-type 'az:Int16
		  :initial-element 0)))
  *rect-coordinate-vector*)

(defun %draw-rects (drawable gcontext rects filled?)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type g:Rect-List rects))
  (let* ((n (* 4 (length rects)))
	 (i 0)
	 (rltwh (rect-coordinate-vector n))
	 (thickness (xlib:gcontext-line-width gcontext))) 
    (declare (type az:Int16 n i)
	     (type az:Int16-Vector rltwh))
    (dolist (r rects)
      (declare (type g:Rect r))
      (let ((left (g:x r))
	    (top (g:y r))
	    (width (g:w r))
	    (height (g:h r)))
	(declare (type az:Int16 left top)
		 (type az:Card16 width height))
	(unless filled?
	  (let ((delta (ash thickness -1)))
	    (declare (type az:Card16 delta))
	    (setf width (max 0 (- width 1 delta)))
	    (setf height (max 0 (- height 1 delta)))
	    (setf left (+ left delta))
	    (setf top (+ top delta))))
	(setf (aref rltwh i) left) (incf i)
	(setf (aref rltwh i) top) (incf i)
	(setf (aref rltwh i) width) (incf i)
	(setf (aref rltwh i) height) (incf i)))
    (xlib:draw-rectangles drawable gcontext rltwh filled?)))

(defun draw-rects (drawable gcontext rects filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Rect-List rects))
  (%draw-rects drawable gcontext rects filled?))

;;;------------------------------------------------------------

(defun %clear-drawable (drawable &key (rect nil))
  (declare (type xlib:Drawable drawable)
	   (type (or Null g:Rect) rect))
  (let ((x 0) (y 0) (w 0) (h 0))
    (if rect
	(progn
	  (setf x (g:x rect))
	  (setf y (g:y rect))
	  (setf w (g:w rect))
	  (setf h (g:h rect)))
      ;; else
      (progn
	(setf w (xlib:drawable-width drawable))
	(setf h (xlib:drawable-height drawable))))
    (etypecase drawable
      (xlib:Window
       (xlib:clear-area drawable :x x :y y :width w :height h))
      (xlib:Pixmap
       (xlib:draw-rectangle 
	drawable
	;; need to get the erasing gcontext from the root,
	;; when drawable is a pixmap
	(drawable-erasing-gcontext (drawable-root drawable))
	x y w h t)))))

(defun clear-drawable (drawable &key (rect nil))
  (declare (type xlib:Drawable drawable)
	   (type (or Null g:Rect) rect))
  (%clear-drawable drawable :rect rect))

;;;============================================================
;;; Ellipses
;;;============================================================

(defun %draw-ellipse-xy (drawable gcontext left top width height filled?)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Drawable drawable)
	   (type xlib:GContext gcontext)
	   (type az:Int16 left top)
	   (type az:Card16 width height))
  (cond ;; Note: X is inconsistent between filled and non-filled cases.
   (filled?
    (setf width (max 0 (1+ width)))
    (setf height (max 0 (1+ height)))
    (decf left)
    (decf top))
   (t
    (setf width (max 0 (1- width)))
    (setf height (max 0 (1- height)))))
  (xlib:draw-arc drawable gcontext left top width height 0 #.(* 2 pi) filled?))

(defun draw-ellipse-xy (drawable gcontext left top width height filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 left top)
	   (type az:Card16 width height))
  (%draw-ellipse-xy drawable gcontext left top width height filled?))

;;;------------------------------------------------------------

(defun %draw-ellipse (drawable gcontext rect filled?)
  (%draw-ellipse-xy drawable gcontext
		    (g:x rect) (g:y rect)
		    (g:w rect) (g:h rect)
		    filled?))

(defun draw-ellipse (drawable gcontext rect filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Rect rect))
  (%draw-ellipse drawable gcontext rect filled?))

;;;============================================================
;;; drawing ellipses
;;;============================================================

(defparameter *ellipse-coordinate-vector*
    (make-array 64
		:element-type 'az:Int16
		:initial-element 0))

(defun ellipse-coordinate-vector (n)
  (unless (= (length *ellipse-coordinate-vector*) n)
    (let ((v (make-array n
			 :element-type 'az:Int16
			 :initial-element 0)))
      (do ((i 3 (+ i 6)))
	  ((>= i n))
	(setf (aref v i) 0)
	(setf (aref v (+ i 1)) 7))
      (setf *ellipse-coordinate-vector* v)))  
  *ellipse-coordinate-vector*)

(defun %draw-ellipses (drawable gcontext rects filled?)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type g:Rect-List rects))
  (let* ((n (* 6 (length rects)))
	 (i 0)
	 (xywhaa (ellipse-coordinate-vector n)))
    (declare (type az:Int16 n i)
	     (type az:Int16-Vector xywhaa))
    (cond
     ;; Note: X is inconsistent between filled and non-filled cases.
     (filled?
      (dolist (r rects)
	(declare (type g:Rect r))
	(let ((left (g:x r))
	      (top (g:y r))
	      (width (g:w r))
	      (height (g:h r)))
	  (declare (type az:Int16 left top)
		   (type az:Card16 width height))
	  (setf (aref xywhaa i) (- left 1)) (incf i)
	  (setf (aref xywhaa i) (- top 1)) (incf i)
	  (setf (aref xywhaa i) (max 0 (+ width 1))) (incf i)
	  (setf (aref xywhaa i) (max 0 (+ height 1))) (incf i 3))))
     (t (dolist (r rects)
	  (declare (type g:Rect r))
	  (let ((left (g:x r))
		(top (g:y r))
		(width (g:w r))
		(height (g:h r)))
	    (declare (type az:Int16 left top)
		     (type az:Card16 width height))
	    (setf (aref xywhaa i) left) (incf i)
	    (setf (aref xywhaa i) top) (incf i)
	    (setf (aref xywhaa i) (max 0 (- width 1))) (incf i)
	    (setf (aref xywhaa i) (max 0 (- height 1))) (incf i 3)))))
    (xlib:draw-arcs drawable gcontext xywhaa filled?)))

(defun draw-ellipses (drawable gcontext rects filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Rect-List rects))
  (%draw-ellipse drawable gcontext rects filled?))

;;;============================================================
;;; Circles
;;;============================================================

(defun %draw-circle-xy (drawable gcontext x y r filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 x y)
	   (type az:Card16 r))
  (let ((d (* 2 r)))
    (%draw-ellipse-xy drawable gcontext (- x r) (- y r) d d filled?)))

(defun draw-circle-xy (drawable gcontext x y r filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16 x y)
	   (type az:Card16 r))
  (%draw-circle-xy drawable gcontext x y r filled?))

;;;------------------------------------------------------------

(defun %draw-circle (drawable gcontext point r filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type az:Card16 r))
  (%draw-circle-xy drawable gcontext
		   (g:x point) (g:y point) r
		   filled?))

(defun draw-circle (drawable gcontext point r filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type az:Card16 r))
  (%draw-circle drawable gcontext point r filled?))

;;;------------------------------------------------------------

(defparameter *circle-coordinate-vector*
    (make-array 64
		:element-type `az:Int16
		:initial-element 0))

(defun circle-coordinate-vector (n)
  (unless (= (length *circle-coordinate-vector*) n)
    (let ((v (make-array n
			 :element-type 'az:Int16
			 :initial-element 0)))
      (do ((i 3 (+ i 6)))
	  ((>= i n))
	(setf (aref v i) 0)
	(setf (aref v (+ i 1)) 7))
      (setf *circle-coordinate-vector* v)))
  *circle-coordinate-vector*)

(defun %draw-circles (drawable gcontext points radii filled?)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type g:Point-List points)
           (type az:Card16-List radii))
  (let* ((n (* 6 (min (length points) (length radii))))
	 (i 0)
	 (xywhaa (circle-coordinate-vector n)))
    (declare (type az:Int16 n i)
	     (type az:Int16-Vector xywhaa))
    (cond
     ;; Note: X is inconsistent between filled and non-filled cases.
     (filled?
      (loop for p in points
	  for radius Fixnum in radii do
	    (let* ((d (max 0 (+ (* 2 radius) 1)))
		   (x (g:x p))
		   (y (g:y p))
		   (left (- x radius))
		   (top (- y radius)))
	      (declare (type az:Int16 x y left top)
		       (type az:Card16 d))
	      (setf (aref xywhaa i) (- left 1)) (incf i)
	      (setf (aref xywhaa i) (- top 1)) (incf i)
	      (setf (aref xywhaa i) d) (incf i)
	      (setf (aref xywhaa i) d) (incf i 3))))
     (t
      (loop for p in points
	  for radius Fixnum in radii do
	    (let* ((d (max 0 (- (* 2 radius) 1)))
		   (x (g:x p))
		   (y (g:y p))
		   (left (- x radius))
		   (top (- y radius)))
	      (declare (type az:Int16 x y left top)
		       (type az:Card16 d))
	      (setf (aref xywhaa i) left) (incf i)
	      (setf (aref xywhaa i) top) (incf i)
	      (setf (aref xywhaa i) d) (incf i)
	      (setf (aref xywhaa i) d) (incf i 3)))))
    (xlib:draw-arcs drawable gcontext xywhaa filled?)))

(defun draw-circles (drawable gcontext points rs filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List points)
	   (type az:Card16-List rs))
  (%draw-circles drawable gcontext points rs filled?))

;;;============================================================
;;; Polygons
;;;============================================================

(defun %draw-polygon-xy (drawable gcontext xy-list filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16-List xy-list))
  (if filled?
      (xlib:draw-lines drawable gcontext xy-list :fill-p t)
    (xlib:draw-lines
     drawable gcontext
     (append xy-list (list (first xy-list) (second xy-list)))
     :fill-p nil)))

(defun draw-polygon-xy (drawable gcontext xy-list filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type az:Int16-List xy-list))
  (%draw-polygon-xy drawable gcontext xy-list filled?))

;;;------------------------------------------------------------ 

(defun point-list->xy-list (ps)
  (declare (type g:Point-List ps)
	   (:returns (type az:Int16-List)))
  (az:with-collection
      (dolist (p ps)
	(az:collect (g:x p))
	(az:collect (g:y p)))))

(defun %draw-polygon (drawable gcontext point-list filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:point-List point-list))
  (%draw-polygon-xy drawable gcontext (point-list->xy-list point-list)
		    filled?))

(defun draw-polygon (drawable gcontext point-list filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:point-List point-list))
  (%draw-polygon drawable gcontext point-list filled?))

;;;------------------------------------------------------------

(defun %draw-polygons (drawable gcontext point-list-list filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type List point-list-list))(dolist (p-list point-list-list)
	   (%draw-polygon drawable gcontext p-list filled?)))

(defun draw-polygons (drawable gcontext point-list-list filled?)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type List point-list-list))
  (%draw-polygons drawable gcontext point-list-list filled?))

;;;============================================================
;;; Drawing Characters
;;;============================================================

(defun %draw-character (drawable gcontext point char)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type Character char))
  (xlib:draw-glyph drawable gcontext (g:x point) (g:y point) char))

(defun draw-character (drawable gcontext p char)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point p)
	   (type Character char))
  (%draw-character drawable gcontext p char))

;;;------------------------------------------------------------

(defun Character-List? (l)
  (and (listp l) (every #'characterp l)))

(deftype Character-List () '(satisfies Character-List?))

(defun %draw-characters (drawable gcontext points chars)
  
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List points)
	   (type Character-List chars))
  (mapc #'(lambda (char point) (%draw-character drawable gcontext point char))
	chars points))

(defun draw-characters (drawable gcontext points chars)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point-List points)
	   (type Character-List chars))
  (%draw-characters drawable gcontext points chars))

;;;============================================================
;;; Drawing Strings
;;;============================================================

(defun %draw-string (drawable gcontext point string)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (xlib:draw-glyphs drawable gcontext (g:x point) (g:y point) string))

(defun draw-string (drawable gcontext point string)

  "The text is drawn with the left baseline starting at {\sf point}.
When the drawing is done, the {\sf drawable-point} of the drawable is at the
baseline and the first pixel to the right of the last character."

  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (%draw-string drawable gcontext point string))

;;;============================================================
;;; Drawing Centered Strings
;;;============================================================

(defun %draw-centered-string (drawable gcontext point string)
  (declare (optimize (safety 0) (speed 3))
           (type xlib:Gcontext gcontext)
           (type Xlib:Drawable drawable)
	   (type g:Point point)
	   (type Simple-String string))
  (multiple-value-bind
      (w a d) (%analyze-string-placement string (xlib:gcontext-font gcontext))
    (declare (type az:Int16 w a d))
    (let ((x (- (the az:Int16 (g:x point)) (ash w -1)))
	  (y (+ (the az:Int16 (g:y point)) (ash (- a d) -1))))
      (declare (type az:Int16 x y))
      (xlib:draw-glyphs drawable gcontext x y string :width w))))

(defun draw-centered-string (drawable gcontext point string)
  "The text is drawn with the center of the string's rect at <point>."
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (%draw-centered-string drawable gcontext point string))

;;;============================================================
;;; Drawing Strings relative to upper left corner
;;;============================================================

(defun %draw-cornered-string (drawable gcontext point string)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (g:with-borrowed-element (p (g:home point))
    (setf (g:x p) (g:x point))
    (setf (g:y p) (+ (g:y point)
		     (%string-ascent string (xlib:gcontext-font gcontext))))
    (%draw-string drawable gcontext p string)))

(defun draw-cornered-string (drawable gcontext point string)

  "The text is drawn with the upper left corner of the string's screen
rect at <point>."

  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (%draw-cornered-string drawable gcontext point string))

;;;============================================================
;;; Drawing Vertical Strings
;;;============================================================
;;; layout determined by global font characteristics

;;; per character layout
(defun %draw-vertical-string (drawable gcontext point string)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Gcontext gcontext)
	   (type Xlib:Drawable drawable)
	   (type g:Point point)
	   (type Simple-String string))
  (let* ((font (xlib:gcontext-font gcontext))
	 (str-width (%vertical-string-width string font))
	 (init-x (g:x point))
	 (x (+ init-x (ash (the Fixnum str-width) -1)))
	 (y (g:y point)))
    (declare (type xlib:Font font)
	     (type az:Int16 str-width init-x x y))
    (dotimes (i (length string))
      (declare (type Fixnum i))
      (let* ((char (char string i))
	     (font-index (char-code char))
	     (w (xlib:char-width font font-index))
	     (a (xlib:char-ascent font font-index))
	     (d (xlib:char-descent font font-index)))
	(declare (type Character char)
		 (type Fixnum font-index)
		 (type az:Int16 w a d))
	;; except for first char, move down by this char's ascent
	;; a+d turns out to be exact character height, with no
	;; extra room, so add a pixel space for readability
	;; We should probably be using the font line height,
	;; or some such, here. As it is this doesn't do the right thing
	;; with spaces.
	(incf y (+ 1 a)) 
	;; center char within vertical column
	(xlib:draw-glyph drawable gcontext (- x (ash w -1)) y char)
	;; move down by this char's descent
	(incf y d)))))

(defun draw-vertical-string (drawable gcontext point string)

  "The text is drawn with the point at the upper left hand corner, not
the left baseline, aligned with <point>."

  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (%draw-vertical-string drawable gcontext point string))

;;;============================================================
;;; Drawing Centered Vertical Strings
;;;============================================================

(defun %draw-centered-vertical-string (drawable gcontext point string)
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (multiple-value-bind
      (w a h)
      (%analyze-vertical-string-placement string (xlib:gcontext-font gcontext))
    (declare (ignore a))
    (g:with-borrowed-element (p (g:home point))
      (setf (g:x p) (- (g:x point) (truncate w 2)))
      (setf (g:y p) (- (g:y point) (truncate h 2)))
      (%draw-vertical-string drawable gcontext p string))))

(defun draw-centered-vertical-string (drawable gcontext point string)
  "The text is drawn with the center of the string's screen rect at <point>."
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable)
	   (type g:Point point)
	   (type String string))
  (%draw-centered-vertical-string drawable gcontext point string))

