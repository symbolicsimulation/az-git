;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald  All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(defun raw-dump (window file-name)

  "Dump the window (as an image) to a ``Adobe Photoshop Raw'' format file.
Assumes 8 bits per pixel and writes 24 bits rgb interleaved per pixel."

  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Window window)
	   (type String file-name))
  
  (multiple-value-bind (pixel-matrix width height) (window-array window nil)
    (declare (type az:Card8-Matrix pixel-matrix)
	     (type xlib:Card16 width height))
    (let ((rowlen (array-dimension pixel-matrix 1))
	  (pixel-vector (az:Array-data pixel-matrix))
	  (lut (xlib:query-colors (xlib:window-colormap window)
				  (loop for i Fixnum from 0 to 255 collect i)
				  :result-type 'Vector))
	  (rlut (az:make-card8-vector 256))
	  (glut (az:make-card8-vector 256))
	  (blut (az:make-card8-vector 256))
	  (i1 0))
      (declare (type xlib:Card16 rowlen)
	       (type Fixnum i1)
	       (type az:Card8-Vector pixel-vector rlut glut blut)
	       (type Simple-Vector lut))
      (dotimes (i 255)
	(declare (type xlib:Card16 i))
	(let ((color (aref lut i)))
	  (declare (type xlib:Color color))
	  (setf (aref rlut i)
	    (floor (* (the Single-Float (xlib:color-red color)) 255.0f0)))
	  (setf (aref glut i)
	    (floor (* (the Single-Float (xlib:color-green color)) 255.0f0)))
	  (setf (aref blut i)
	    (floor (* (the Single-Float (xlib:color-blue color)) 255.0f0)))))
      (print (cons width height))
      (with-open-file (stream file-name
		       :element-type '(Unsigned-Byte 8)
		       :direction :output
		       :if-exists :supersede
		       :if-does-not-exist :create)
	(declare (type Stream stream))
	(dotimes (i height)
	  (declare (type Fixnum i))
	  (setf i1 (* i rowlen))
	  (dotimes (j width)
	    (declare (type Fixnum j))
	    (let ((p (aref pixel-vector (+ i1 j))))
	      (declare (type (Unsigned-Byte 8) p))
	      (write-byte (aref rlut p) stream)
	      (write-byte (aref glut p) stream)
	      (write-byte (aref glut p) stream))))))))

(defun raw24-dump (window file-name)

  "Dump the window (as an image) to a ``Adobe Photoshop Raw'' format file.
Assumes 8 bits per pixel and writes 24 bits rgb interleaved per pixel."

  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Window window)
	   (type String file-name))
  
  (multiple-value-bind (pixel-matrix width height) (window-array window nil)
    (declare (type az:Card8-Matrix pixel-matrix)
	     (type xlib:Card16 width height))
    (let ((rowlen (array-dimension pixel-matrix 1))
	  (pixel-vector (az:Array-data pixel-matrix))
	  (lut (xlib:query-colors (xlib:window-colormap window)
				  (loop for i Fixnum from 0 to 255 collect i)
				  :result-type 'Vector))
	  (rgb (az:make-card24-vector 256))
	  (i1 0))
      (declare (type xlib:Card16 rowlen)
	       (type Fixnum i1)
	       (type az:Card8-Vector pixel-vector)
	       (type az:Card24-Vector rgb)
	       (type Simple-Vector lut))
      (dotimes (i 255)
	(declare (type xlib:Card16 i))
	(let* ((color (aref lut i))
	       (r (floor (* (the Single-Float (xlib:color-red color))
			    255.0f0)))
	       (g (floor (* (the Single-Float (xlib:color-green color))
			    255.0f0)))
	       (b (floor (* (the Single-Float (xlib:color-blue color))
			    255.0f0))))
	  (declare (type xlib:Color color)
		   (type az:Card8 r g b))
	  (setf (aref rgb i) (logior (ash r 16) (ash g 8) b))))
      (print (cons width height))
      (with-open-file (stream file-name
		       :element-type 'az:Card24
		       :direction :output
		       :if-exists :supersede
		       :if-does-not-exist :create)
	(declare (type Stream stream))
	(dotimes (i height)
	  (declare (type Fixnum i))
	  (setf i1 (* i rowlen))
	  (dotimes (j width)
	    (declare (type Fixnum j))
	    (let ((p (aref pixel-vector (+ i1 j))))
	      (declare (type (Unsigned-Byte 8) p))
	      (write-byte (aref rgb p) stream))))))))

