;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Xlite)

(xlib:close-display (host-default-display))

(setf w (make-window :width 256 :height 256))

(xlib:screen-white-pixel (drawable-default-screen w))
(xlib:screen-black-pixel (drawable-default-screen w))
(az:kill-object w)
(setf sp (drawable-space w))
(setf gc (make-gcontext w))
(setf gc (make-gcontext w :function boole-xor))
(setf gc (make-gcontext w :function boole-set))
(setf gc (make-gcontext w :function boole-clr))
(setf gc (make-gcontext w :function boole-ior))
(setf gc (make-gcontext w :function boole-nand))
(setf gc (make-gcontext w :function boole-nor))


(xlib:window-colormap w)
(xlt:drawable-colormap w)

(dotimes (i 16)
  (draw-rect w gc (g:make-rect sp :left (* i 4) :top i :width 16 :height 16) 
	     nil)
  (drawable-force-output w))

(dotimes (i 16)
  (draw-ellipse w gc (g:make-rect sp :left (* i 4) :top i :width 16 :height 16)
		nil)
  (drawable-force-output w))


(xlib:clear-area w)

(dotimes (i 128)
  (xlib:draw-rectangle w gc i i 16 16 nil)
  (drawable-force-output w))

(show-colormap
 (paint-colormap
  (xlib:screen-root
   (xlib:display-default-screen (host-default-display)))))

(setf c (xlib:screen-default-colormap
	 (xlib:display-default-screen (default-display))))

(show-colormap c)

;;;============================================================

(defparameter *image*
    (let ((*print-pretty* nil)
	  (*print-array* nil))
      (xlib:get-image *root*
		      :x 100 :y 100 :width 64 :height 64
		      :format :z-pixmap
		      :result-type 'xlib:Image-Z
		      )))

;;; ---------------------------------------------------------------
;;; Evaluating the following does not put the image into the window
;;; and eventually returns a MATCH-ERROR from the X server.
;;; ---------------------------------------------------------------

(progn

  (defparameter *display* (xlib:open-display ""))

  (defparameter *root*
      (xlib:screen-root (first (xlib:display-roots *display*))))

  (defparameter *window*
      (let ((w (xlib:create-window
		:parent *root*
		:background 0
		:x 500 :y 1 :width 128 :height 128)))
	(xlib:map-window w)
	(xlib:display-force-output *display*)
	w))

  (defparameter *gcontext*
      (xlib:create-gcontext
       :drawable *root*
       :function boole-ior
       :background 0
       :foreground #xffff))


  (defparameter *image*
      (xlib:create-image
       :data (make-array '(64 64)
			 :element-type '(Unsigned-Byte 8)
			 :initial-element 3)
       :format :z-pixmap
       :depth 8 
       :height 64
       :width 64))

  (progn
    (xlib:put-image *window* *gcontext* *image*
		    :x 17 :y 13 :src-x 0 :src-y 0 :width 64 :height 64)
    (xlib:display-force-output *display*)))


