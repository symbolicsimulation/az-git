;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite) 

;;;============================================================
;;; built-in menu facilities
;;;============================================================

(defparameter *menu-font-name*
    "-*-times-medium-r-normal-*-*-80-100-100-*-*-*-*")

(defparameter *menu-gcontexts* (make-hash-table :test #'eq)
  "Maps root windows to gcontexts.")

(defun menu-gcontext (window)
  (declare (type xlib:Window window))
  (let* ((root (drawable-root window))
	 (gcontext (gethash root *menu-gcontexts* nil)))
    (declare (type xlib:Window root)
	     (type (or Null xlib:Gcontext) gcontext))
    (when (null gcontext)
      (setf gcontext
	(xlt:make-gcontext
	 root
	 :font (xlib:open-font (xlib:window-display root) *menu-font-name*)))
      (setf (gethash root *menu-gcontexts*) gcontext))
    gcontext))

(defparameter *menu-xor-gcontexts* (make-hash-table :test #'eq)
  "Maps root windows to gcontexts.")

(defun menu-xor-gcontext (window)
  (declare (type xlib:Window window))
  (let* ((root (drawable-root window))
	 (gcontext (gethash root *menu-xor-gcontexts* nil)))
    (declare (type xlib:Window root)
	     (type (or Null xlib:Gcontext) gcontext))
    (when (null gcontext)
      (setf gcontext
	(xlt:make-gcontext
	 root
	 :function boole-xor
	 :font (xlib:open-font (xlib:window-display root) *menu-font-name*)))
      (setf (gethash root *menu-xor-gcontexts*) gcontext))
    gcontext))

;;;============================================================

(defun pointer-mask (window)
  (declare (type xlib:Window window))
  (multiple-value-bind (x y ssp child state-mask root-x root-y root)
      (xlib:query-pointer window)
    (declare (ignore x y ssp child root-x root-y root))
    (values state-mask)) )

(defun pointer-xy (window)
  (declare (type xlib:Window window))
  (multiple-value-bind (x y ssp child state-mask root-x root-y root)
      (xlib:query-pointer window)
    (declare (ignore ssp child state-mask root-x root-y root))
    (values x y)))

(defun menu-xy (window width height)
  (multiple-value-bind (x y) (pointer-xy window)
    ;; adjust the position of the menu so it fits in the window,
    ;; if possible
    (g::%keep-screen-rect-in-xy
     x y width height
     0 0 (xlib:drawable-width window) (xlib:drawable-height window))))

(defun menu-window (window width height &key (namestring nil))
  (multiple-value-bind (x y) (menu-xy window width height)
    (let ((w (xlib:create-window
	      :parent window
	      :border-width 1
	      :colormap (xlib:window-colormap window)
	      :bit-gravity :north-west
	      :do-not-propagate-mask 0
	      :x x :y y :width width :height height
	      :cursor (xlt:window-cursor window :hand1)
	      :backing-store :always
	      :background (xlib:window-backing-pixel window)
	      :backing-pixel (xlib:window-backing-pixel window)
	      :event-mask '(:exposure :button-press :button-release))))
      (when namestring
	(setf (xlib:wm-name w) namestring)
	(setf (xlib:wm-icon-name w) namestring))
      ;; set window hints, so menu will appear without prompting,
      ;; at current point of mouse
      (setf (xlib:wm-normal-hints w)
	(xlib:make-wm-size-hints
	 :user-specified-size-p t :user-specified-position-p t
	 :x x :y y :width width :height height))
      ;; map the window
      (xlib:map-window w)
      ;; wait until the window is actually on the screen before returning
      (loop (when (eq (xlib:window-map-state w) :viewable) (return)))
      w)))

(defun menu-specs (menu-list font)
  (let ((maxwidth 0)
	(last-y 0)
	(new-y 0)
	item-string
	menu-specs)
    (dolist (item menu-list)
      (setf item-string (format nil "~A" (first item)))
      (multiple-value-bind (width maxascent maxdescent)
	  (xlt:analyze-string-placement item-string font)
	(setf maxwidth (max maxwidth width))
	(setf new-y (+ maxascent maxdescent last-y))
	(setf menu-specs
	  (cons (list item-string last-y new-y (+ last-y maxascent) item)
		menu-specs))
	(setf last-y new-y)))
    (values (reverse menu-specs) (+ maxwidth 10) last-y)))

(defun menu-button-state (w button)
  (logtest (pointer-mask w) (xlib:make-state-mask button)))

(defun menu-button-1-down (w) (menu-button-state w :button-1))

(defun menu-button-down? (w)
  (logtest (pointer-mask w)
	   (xlib:make-state-mask :button-1 :button-2 :button-3)))


(defun menu-get-current-item (x y menu-specs menu-width menu-height)
  (when (and (>= x 0) (<= x menu-width)
	     (>= y 0) (<= y menu-height))
    (dolist (spec menu-specs)
      (when (and (>= y (nth 1 spec))
		 (<= y (nth 2 spec)))
	(return spec)))))

(defun menu-xor-item (item w width)
  (when item
    (xlib:draw-rectangle w (menu-xor-gcontext w)
			 0 (nth 1 item)
			 width
			 (- (nth 2 item) (nth 1 item))
			 t)
    (xlt:drawable-finish-output w)))

(defun menu-handle-exposure (w menu-specs)
  (xlib:clear-area w)
  (dolist (spec menu-specs)
    (xlib:draw-glyphs w (menu-gcontext w) 5 (nth 3 spec) (nth 0 spec)))
  (xlt:drawable-finish-output w)
  nil)

(defun menu-handle-button-press (code x y w menu-specs menu-w menu-h)
  (declare (ignore code))
  (let* ((current-item
	  (menu-get-current-item x y menu-specs menu-w menu-h))
	 (last-item nil))
    (loop (when (not (menu-button-down? w)) (return))
      (when (not (eql last-item current-item))
	(menu-xor-item last-item w menu-w)
	(menu-xor-item current-item w menu-w))	  	
      (multiple-value-bind
	  (xx xy same-screen-p) (xlib:pointer-position w)
	(declare (ignore same-screen-p))
	(setf last-item current-item)
	(setf current-item
	  (menu-get-current-item xx xy menu-specs menu-w menu-h))))
    (nth 4 last-item)))

;;;============================================================
;;; Takes a list of 'menu items', each of which is a list whose
;;; first item is a string, and whose second item is a function
;;; to be applied to the thrid item in the list if the item is selected.

(defun menu (window menu-list)
  (declare (type xlib:Window window)
	   (type List menu-list))
  (let* ((font (xlib:gcontext-font (menu-gcontext window)))
	 (selected-item nil))
    (declare (type xlib:Font font)
	     (type List selected-item))
    (multiple-value-bind (specs w h) (menu-specs menu-list font)
      (declare (type List specs)
	       (type g:Positive-Screen-Coordinate h w))
      (let* ((menu-window (menu-window window w h))
	     (display (xlib:window-display menu-window)))
	(menu-handle-exposure menu-window specs)
	(xlib:with-event-queue (display)
	  (unwind-protect
	      ;; protected form
	      (block event-loop
		(loop
		  (xlib:event-case
		   (display :force-output-p t :discard-p t)
		   (:exposure ;; handle exposure events
		    (count)
		    ;; Ignore all but the last exposure event
		    (when (= 0 (the xlib:Card16 count))
		      (menu-handle-exposure menu-window specs)))
		   (:button-press ;; handle mouse button down
		    (x y code)
		    (setf selected-item
		      (menu-handle-button-press
		       code x y menu-window specs w h))
		    (return-from event-loop nil))
		   (:button-release
		    ()
		    nil))))
	    ;; cleanup form
	    (progn (xlib:destroy-window menu-window)
		   (xlt:drawable-finish-output window)))
	  ;; execute the selected item
	  (when selected-item
	    #+:excl
	    (apply 'mp:process-run-function
		   (first selected-item)
		   (second selected-item)
		   (rest (rest selected-item)))
	    #+:cmu
	    (apply (first selected-item)
		   (second selected-item)
		   (rest (rest selected-item)))))))))