;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================
;;; Flushing output buffers 
;;;============================================================

(defun drawable-finish-output (drawable)
  (declare (type xlib:Drawable drawable))
  (xlib:display-finish-output (xlib:drawable-display drawable)))

(defun drawable-force-output (drawable)
  (declare (type xlib:Drawable drawable))
  (xlib:display-force-output (xlib:drawable-display drawable)))

;;;============================================================

(defun drawable-default-screen (drawable)
  (xlib:display-default-screen (xlib:drawable-display drawable)))
 
;;;============================================================

(defun window-children (w)
  "Returns the window's children in the X window hierarchy.
A convenient substitute to calling xlib:query-tree."
  (declare (type xlib:Window w)
	   (:returns (type List children)))
  (multiple-value-bind (children parent root) (xlib:query-tree w)
    (declare (ignore parent root))
    children))

(defun window-parent (w)
  "Returns the window's parent in the X window hierarchy.
A convenient substitute to calling xlib:query-tree."
  (declare (type xlib:Window w)
	   (:returns (type xlib:Window parent)))
  (multiple-value-bind (children parent root) (xlib:query-tree w)
    (declare (ignore children root))
    parent))

(defparameter *drawable-roots* (az:make-alist-table :test #'xlib:window-equal)
  "Cache root windows. We can do this safely because we never reroot windows?")

(defun drawable-root (w)
  "Returns the cached value of window's root in the X window hierarchy.
We cache because xlib:query-tree requires an expensive roundtrip to the
X server. A convenient substitute to calling xlib:query-tree."
  (declare (type xlib:Drawable w)
	   (:returns (type xlib:Window root)))
  (az:lazy-lookup w *drawable-roots* (xlib:drawable-root w)))

;;;============================================================

(defparameter *drawable-depths* (az:make-alist-table :test #'xlib:window-equal)
  "Cache window depths. We can do this safely because depths never change?")

(defun drawable-depth (w)
  "Returns the cached value of window's depth. I cache because 
xlib:drawable-depth requires an expensive roundtrip to the X server."
  (declare (type xlib:Drawable w)
	   (:returns (type xlib:Card8 depth)))
  (az:lazy-lookup w *drawable-depths* (xlib:drawable-depth w)))

;;;============================================================

(defun drawable-root-xy (window)

  "The upper left corner in the root window's coordinate system."

  (declare (type xlib:Window window)
	   (:returns (values (type xlib:Int16 root-x)
			     (type xlib:Int16 root-y))))
  
  (xlib:translate-coordinates
   (window-parent window) (xlib:drawable-x window) (xlib:drawable-y window)
   (drawable-root window)))

(defun drawable-root-x (window)

  "The left side in the root window's coordinate system."

  (declare (type xlib:Window window)
	   (:returns (type xlib:Int16 root-x)))
  
  (multiple-value-bind (root-x root-y) (drawable-root-xy window)
    (declare (type xlib:Int16 root-x root-y)
	     (ignore root-y))
    root-x))

(defun drawable-root-y (window)

  "The top in the root window's coordinate system."

  (declare (type xlib:Window window)
	   (:returns (type xlib:Int16 root-y)))
  
  (multiple-value-bind (root-x root-y) (drawable-root-xy window)
    (declare (type xlib:Int16 root-x root-y)
	     (ignore root-x))
    root-y))

;;;============================================================
;;; Spaces for locations and displacements in xlib:Drawables
;;; (see the Geometry package)
;;;============================================================

(defparameter *drawable-spaces* (az:make-alist-table :test #'xlib:window-equal)
  "A lazy table of g:Int-Grid spaces, one for each xlib:Drawable.")

(defun drawable-space (drawable)
  "Returns a unique g:Int-Grid space for points in the drawable."
  (az:declare-check (type xlib:Drawable drawable)
		    (:returns (type g:Int-Grid)))
  (az:lazy-lookup
   drawable  *drawable-spaces*
   (make-instance 'g:Int-Grid
     :dimension 2
     :name (format nil "~a" drawable)
     :translation-space (make-instance 'g:Int-Lattice
			  :dimension 2
			  :name (format nil "~a" drawable)))))

(defun drawable-translation-space (drawable)
  "Returns a unique g:Int-Lattice for displacements in the drawable.
[Actually, just short for (g:translation-space (drawbable-space drawable)).]"
  (az:declare-check (type xlib:Drawable drawable)
		    (:returns (type g:Int-Lattice)))

  (g:translation-space (drawable-space drawable)))

;;;============================================================
;;; Window manipulation
;;;============================================================

(defun expose-window (w)
  (declare (type xlib:Window w))
  ;; map the window
  (xlib:map-window w)
  ;; wait until the window is actually on the screen before returning
  (loop (when (eq (xlib:window-map-state w) :viewable) (return)))
  (setf (xlib:window-priority w) :top-if))

;;;============================================================

(defun window-manager-normal-hints (window)
  "Lazy evaluation of the normal hints."
  (declare (type xlib:Window window))
  (let ((hints (xlib:wm-normal-hints window)))
    (when (null hints)
      (setf hints (xlib:make-wm-size-hints))
      (setf (xlib:wm-normal-hints window) hints))
    hints))

(defsetf window-manager-normal-hints (window) (hints)
  `(setf (xlib:wm-normal-hints ,window) ,hints))

;;;============================================================

(defun place-window (window
		     &key
		     (left nil) 
		     (top nil)
		     (user-specified-position-p nil))

  "Set the wm hints so the window appears at the specified position, 
unless either of the position args is null, in which case put it at 
the mouse position."
  
  (declare (type xlib:Window window)
	   (type (or Null az:Int16) left top)
	   (type az:Boolean user-specified-position-p))
  (xlib:with-state (window)
    (let ((xx (or left 0))
	  (yy (or top 0))
	  (hints (window-manager-normal-hints window)))
      (declare (type az:Int16 xx yy))
      (setf (xlib:wm-size-hints-user-specified-position-p hints)
	user-specified-position-p)
      (setf (xlib:wm-size-hints-x hints) xx)
      (setf (xlib:wm-size-hints-y hints) yy)
      (setf (window-manager-normal-hints window) hints)
      (when left (setf (xlib:drawable-x window) xx))
      (when top (setf (xlib:drawable-y window) yy))
      (drawable-force-output window))))

(defun size-window (window
		    &key
		    (width nil)
		    (height nil)
		    (user-specified-size-p nil))

  "Set the wm hints so the window appears with the specified size, 
unless either of the size args in null, in which case, have the user size 
it with the mouse."
  
  (declare (type xlib:Window window)
	   (type (or Null az:Int16) width height))
  (xlib:with-state (window)
    (let ((ww (or width 64))
	  (hh (or height 64))
	  (hints (window-manager-normal-hints window)))
      (declare (type az:Int16 ww hh))
      (setf (xlib:wm-size-hints-user-specified-size-p hints)
	user-specified-size-p)
      (setf (xlib:wm-size-hints-width hints) ww)
      (setf (xlib:wm-size-hints-height hints) hh)
      (setf (window-manager-normal-hints window) hints)
      (when width (setf (xlib:drawable-width window) ww))
      (when height (setf (xlib:drawable-height window) hh))
      (drawable-force-output window))))

;;;============================================================

(defun make-window (&key
		    (left nil) (top nil)
		    (width nil) (height nil)
		    (name (gentemp "xlt:Window-"))
		    (parent (xlib:screen-root
			     (xlib:display-default-screen
			      (host-default-display
			       :host (default-host)))))
		    (display (when parent (xlib:drawable-display parent)))
		    (screen
		     (if display
		       (xlib:display-default-screen display)
		       (error "Need one of :parent, :display, or :screen.")))
		    (depth (xlib:drawable-depth parent))
		    (colormap (default-colormap (xlib:screen-root screen)))
		    (backing-store :not-useful)
		    (backing-pixel 
		     (if (= depth 1)
		       (xlib:screen-white-pixel screen)
		       (xlt:paint-gray-pixel (xlib:screen-root screen))))
		    (background backing-pixel)
		    (visual :copy)
		    (map? t))
  (declare (type (or Null az:Int16)
		 left top width height)
	   (type (or Symbol String) name)
	   (type (or Null xlib:Window) parent)
	   (type (or Null xlib:Display) display)
	   (type (or Null xlib:Screen) screen)
	   (type xlib:Colormap colormap)
	   (type (member :always :when-mapped :not-useful) backing-store)
	   (type xlib:Pixel backing-pixel)
	   (type (or xlib:Pixel xlib:Pixmap (member :none :parent-relative))
		 background)
	   (type (or (member :copy) xlib:Visual-Info) visual)
	   (type az:Boolean map?))
  (when (null parent)
    (if screen
      (setf parent (xlib:screen-root screen))
      (error ":parent, :display, or :screen must be supplied.")))
  (let ((w (xlib:create-window
	    :background background
	    :backing-pixel backing-pixel
	    :backing-store (ecase (xlib:screen-backing-stores screen)
			     (:always backing-store)
			     (:when-mapped (if (eq backing-store :when-mapped)
					     :when-mapped :not-useful))
			     (:never :not-useful))
	    :bit-gravity :north-west
	    :border (xlib:screen-black-pixel screen)
	    :border-width 0
	    :colormap colormap
	    :do-not-propagate-mask 0
	    :height (or height 64)
	    :parent parent
	    :width (or width 64)
	    :x (or left 0)
	    :y (or top 0)
	    :visual visual)))
    ;; Set the window's name and icon name
    (setf (xlib:wm-name w) name)
    (setf (xlib:wm-icon-name w) name)
    (setf (xlib:wm-protocols w) '(:wm_delete_window))
    (place-window w :left left :top top
		  :user-specified-position-p (if (and left top) t nil))
    (size-window w :width width :height height
		 :user-specified-size-p (if (and width height) t nil))
    ;;(setf (xlib:window-override-redirect w) :off)
    (when map? (expose-window w))
    w))

;;;============================================================

(defun window-array (window whole-window?)

  "Return an array holding an image of the contents of the window.  The
array may not have the same dimensions as the window. In particular, if
<whole-window?> is not <nil>, then borders, title bars, etc., will be
included as well as the strictly Window contents of the window.  Also
return the effective width and height of the window's image, which may
be smaller that the array size, due to padding."

  (declare (type xlib:Window window)
	   (type az:Boolean whole-window?)
	   (:returns
	    (values (type (Array Number (* *)) image)
		    (type xlib:Card16 width height))))
  
  (let* ((win (if whole-window?
		  ;; then get the parent window,
		  ;; which includes the title bar,, etc.
		  (window-parent window)
		;; else just the window itself
		window))
	 (w (xlib:drawable-width win))
	 (h (xlib:drawable-height win)))
    (values
     (xlib:image-z-pixarray
      (xlib:get-image win :x 0 :y 0 :width w :height h :format :z-pixmap))
     w h)))

;;;============================================================

(defmethod az:kill-object ((pixmap xlib:Pixmap)) 
  (xlib:free-pixmap pixmap)
  (drawable-force-output pixmap)
  (change-class pixmap 'az:Dead-Object))

(defmethod az:kill-object ((window xlib:Window)) 
  (xlib:destroy-window window)
  (drawable-force-output window)
  (change-class window 'az:Dead-Object))