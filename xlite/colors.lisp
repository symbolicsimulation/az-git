;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(defun default-screen (drawable)
  (xlib:display-default-screen (xlib:drawable-display drawable)))

(defun default-visual-info (window)
#|
  (xlib:visual-info (xlib:drawable-display window)
			   (xlib:window-visual window))
  |#
  ;; Find the first :direct-color visual and return it
  (let* ((display (xlib:drawable-display window))
	 (screen (xlib:display-default-screen display))
;	 (v (third (first (slot-value screen 'xlib::depths))))
	 )
    (dolist (list-of-numbits-and-visuals (slot-value screen 'xlib::depths))
      (let ((num-bits (first list-of-numbits-and-visuals))
	    (visuals (rest list-of-numbits-and-visuals)))
	(format *terminal-io* "num-bits ~D~%" num-bits)
	(dolist (visual visuals)
	  (if (eq :direct-color (slot-value visual 'xlib::class))
;	      (clouseau:inspector visual)
	      (return-from default-visual-info visual)
	    )
	  )
	)
      )
;    (clouseau:inspector (list display screen))
;    (clouseau:inspector v)
;    (clouseau:inspector (slot-value (glx:choose-visual screen '((:glx-class 3 =))) 'glx::id))
;    v
    )
  )

(defun default-visual-info-class (window)
  (xlib:visual-info-class (default-visual-info window)))

(defun default-screen-depth (window)
  (xlib:screen-root-depth (default-screen window)))

;;;============================================================

(defun %equal-colors? (c0 c1)
  (declare (type xlib:Color c0 c1))
  (or (eq c0 c1)
      (and (= (xlib:color-red   c0) (xlib:color-red   c1))
	   (= (xlib:color-green c0) (xlib:color-green c1))
	   (= (xlib:color-blue  c0) (xlib:color-blue  c1)))))

(defun equal-colors? (c0 c1)
  (declare (type xlib:Color c0 c1))
  (%equal-colors? c0 c1))

(defun %copy-color-to! (color result)
  (declare (type xlib:Color color result))
  (setf (xlib:color-red   result) (xlib:color-red   color))
  (setf (xlib:color-green result) (xlib:color-green color))
  (setf (xlib:color-blue  result) (xlib:color-blue  color))
  (values result))

(defmethod az:copy ((color xlib:Color) &key (result (xlib:make-color)))
  (declare (type xlib:Color color result))
  (%copy-color-to! color result)
  (values result))

;;;============================================================

(defun pixel->color (colormap pixel)
  "Find the color in <colormap> at <pixel>."
  (declare (type xlib:Colormap colormap)
	   (type xlib:Pixel pixel))
  (first (xlib:query-colors colormap (list pixel) :result-type 'List)))
 
;;;============================================================

(defparameter *all-pixels* `#.(loop for i from 0 to 255 collect i))

(defun color->pixel (colormap color)
  "Find the pixel in <colormap> whose color best approximates <color>
in the L-infinity norm in device RGB."
  (declare (type xlib:Colormap colormap)
	   (type xlib:Color color))
  (let ((lut (xlib:query-colors colormap *all-pixels* :result-type 'Vector))
	(r (xlib:color-red color))
	(g (xlib:color-green color))
	(b (xlib:color-blue color))
	(pixel 0)
	(min most-positive-double-float))
    (declare (type Vector lut)
	     (type xlib:RGB-Val r g b)
	     (type Float min)
	     (type xlib:Pixel pixel))
    (dotimes (i (length lut))
      (declare (type xlib:Pixel i))
      (let* ((c (aref lut i))
	     (d (max (abs (- r (xlib:color-red c)))
		     (abs (- g (xlib:color-green c)))
		     (abs (- b (xlib:color-blue c))))))
	(declare (type xlib:Color c)
		 (type Float d))
	(when (< d min) (setf pixel i) (setf min d))))
    pixel))
  
;;;============================================================
;;; based on X's rgb.txt color database

(defun read-colorname-database (database-file)
  (let ((line "")
	(index 0)
	(plist ()))
    (with-open-file (database database-file :direction :input)
      (loop 
	(setf line (read-line database nil nil))
	(when (null line) (return))
	(with-input-from-string (s line :index index)
	  (push (xlib:make-color :red (/ (read s) 255.0d0)
				 :green (/ (read s) 255.0d0)
				 :blue (/ (read s) 255.0d0))
		plist)
	  (push (intern (nsubstitute #\- #\space
				     (string-upcase
				      (string-left-trim '(#\space #\tab)
							(subseq line index))))
			:keyword)
		plist))))
    plist))

(defvar *colorname-plist* ())

(defparameter *important-colornames*
    '(:black
      :blue
      :aquamarine
      :navy
      :coral
      :cyan
      :firebrick
      :brown
      :gold
      :green
      :gray
      :khaki
      :magenta
      :maroon
      :orange
      :orchid
      :pink
      :plum
      :red
      :salmon
      :sienna
      :tan
      :thistle
      :turquoise
      :violet
      :wheat
      :white
      :yellow
      :MediumAquamarine
      :mediumblue
      :mediumslateblue
      :mediumorchid
      :mediumseagreen
      :mediumturquoise
      :mediumvioletred
      :CadetBlue
      :CornflowerBlue
      :DarkSlateBlue
      :LightBlue
      :LightSteelBlue
      :MediumBlue
      :MediumSlateBlue
      :MidnightBlue
      :NavyBlue
      :SkyBlue
      :SlateBlue
      :SteelBlue
      :sandybrown
      :goldenrod
      :DarkGreen
      :DarkOliveGreen
      :ForestGreen
      :LimeGreen
      :PaleGreen
      :SeaGreen
      :SpringGreen
      :YellowGreen
      :DarkSlateGray
      :DimGray
      :LightGray
      :DarkOrchid
      :IndianRed
      :OrangeRed
      :VioletRed
      :DarkTurquoise
      :BlueViolet
      :GreenYellow))

(defun colorname? (name)
  "The type predicate for <Colorname>s."
  (and (typep name 'Keyword)
       (member name *colorname-plist* :test #'eq)))

(deftype Colorname ()
  "A type for X color names."
  '(satisfies colorname?))

(defun colorname->color (name &key (result (xlib:make-color)))
  "Create or modify a color object to correspond to a given color name."
  (declare (type Colorname name)
	   (type xlib:Color result))
  (let ((color (getf *colorname-plist* name nil)))
    (if (null color)
	(error "~%No color named ~s" name)
	(az:copy color :result result)))
  result)

(defun colorname->pixel (colormap name)
  (declare (type xlib:Colormap colormap)
	   (type Colorname name))
  (color->pixel colormap (xlib:lookup-color colormap name)))

;;;============================================================

(defun color->lightness (color)

  "This function is intended to be used for approximating color displays
in gray scale."

  (declare (type xlib:Color color))

  (/ (+ (xlib:color-red color)
	(xlib:color-green color)
	(xlib:color-blue color))
     3.0d0))

(defun colormap-lit (window
		     &key
		     (colormap (xlib:window-colormap window))
		     (depth (drawable-depth window))
		     (length (ash 1 depth))
		     (result (make-array
			      length
			      :element-type `(Unsigned-Byte ,depth))))

  "Extract a vector of lightness values (the average of r, g, and b
scaled to a maximum of 2^pixel-depth) from a colormap."

  (declare (type xlib:Window window)
	   (type xlib:Colormap colormap)
	   (type Vector result)
	   (:returns result))
  
  (let* ((l-1 (- length 1))
	 (lut (xlib:query-colors colormap
				 (loop for i from 0 to l-1 collect i)
				 :result-type 'Vector)))
    (dotimes (i length)
      (setf (aref result i)
	(truncate (* (color->lightness (aref lut i)) l-1)))))
  result)

;;;============================================================

(defun make-colormap (window
		      &key
		      (visual (default-visual-info window))
		      (alloc-p t))
  (let ((colormap (xlib:create-colormap visual window alloc-p)))
    (declare (type xlib:Colormap colormap))
    ;;(xlib:install-colormap colormap)
    (drawable-force-output window)
    colormap))

(defmethod az:kill-object :before ((colormap xlib:Colormap))
  (xlib:uninstall-colormap colormap))

;;;============================================================

(defun default-colormap (window)
  (xlib:screen-default-colormap (default-screen window)))

(defparameter *grayscale-colormaps*
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to colormaps.")

(defun grayscale-colormap (window)
  (let* ((root (drawable-root window))
	 (colormap (az:lookup root *grayscale-colormaps* nil)))
    (declare (type xlib:Window root)
	     (type (or Null xlib:Colormap) colormap))
    (when (null colormap)
      (setf colormap (xlt:make-colormap root))
      (setf (az:lookup root *grayscale-colormaps*) colormap)
      (let ((color (xlib:make-color :red 0.0d0 :blue 0.0d0 :green 0.0d0)))
	(declare (type xlib:Color color))
	(dotimes (i 256)
	  (let ((x (/ i 255.0d0)))
	    ;; red part
	    (setf (xlib:color-red color) x)
	    (setf (xlib:color-green color) x)
	    (setf (xlib:color-blue color) x)
	    (xlib:store-color colormap i color)))))
    colormap))

(defparameter *colornames-colormaps*
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps screens to colormaps.")

(defun colornames-colormap (window)
  (let* ((root (drawable-root window))
	 (colormap (az:lookup root *grayscale-colormaps* nil)))
    (declare (type xlib:Window root)
	     (type (or Null xlib:Colormap) colormap))
    (when (null colormap)
      (setf colormap (xlt:make-colormap root))
      (setf (az:lookup root *colornames-colormaps*) colormap)
      (let ((pixel 0)
	    (pixel-colors ()))
	(declare (type xlib:Pixel pixel)
		 (type List pixel-colors))
	(dolist (c0 *colorname-plist*)
	  (when (> pixel 255)
	    (warn "More color names than colormap entries.")
	    (return))
	  ;; just skip over the names in the plist
	  (when (typep c0 'xlib:Color)
	    ;; only add colormap entries for distinct colors
	    (unless (find-if #'(lambda (c1) (equal-colors? c0 c1))
			     pixel-colors)
	      (push c0 pixel-colors)
	      (push pixel pixel-colors)
	      (incf pixel))))
	(xlib:store-colors colormap pixel-colors)))
    colormap))


