;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Xlite
  (:use :Common-Lisp #+:clos :CLOS)
  (:nicknames :xlt)
  (:export
   Hostname
   Fill-Style
   Gcontext-Vector
	    
   ;; display
   default-host
   host-default-display
   global-pointer-x
   global-pointer-y

   ;; drawables
   drawable-screen
   expose-window
   place-window
   size-window
   make-window
   window-children
   window-parent
   drawable-root
   drawable-root-x
   drawable-root-y
   drawable-depth
   drawable-space
   drawable-translation-space

   ;; gcontext
   make-gcontext
   copy-gcontext-for-drawable
   drawable-default-gcontext
   drawable-erasing-gcontext
   drawable-xor-gcontext
   3d-high-gcontext
   3d-mid-gcontext
   3d-low-gcontext
   3d-shadow-gcontext
   pixel-default-gcontext
   pure-hue-gcontext
   colorname->pure-hue-gcontext
   hue-sat-gcontext
   colorname->hue-sat-gcontext
   colorname->hue-gcontext
   colorname->sat-gcontext
	    
   ;; graphics
   ;; string and character measurements
   character-rect 
   character-width
   character-height
   character-ascent
   character-descent
   analyze-string-placement
   string-rect
   string-extent
   string-width
   string-height
   string-ascent
   string-descent
   maximum-string-width
   maximum-string-height
   vertical-string-rect
   vertical-string-extent
   vertical-string-width 
   vertical-string-height
   vertical-string-ascent
   vertical-string-descent
   maximum-vertical-string-width
   maximum-vertical-string-height
    
   ;; flushing output
   drawable-finish-output drawable-force-output
   ;; drawing points (pixels)
   draw-point draw-points

   ;; drawing lines
   draw-line draw-lines
   draw-polyline draw-polylines
   draw-polygon draw-polygons
   draw-arrow draw-arrows
   ;; copy-slate-rect (aka bitblt) operations
   copy-area

   ;; drawing rects
   clear-drawable
   draw-rect draw-rects
         
   ;; drawing ellipses
   draw-ellipse draw-ellipses
   draw-circle draw-circles
         
   ;; drawing strings and characters
   draw-character draw-characters
   draw-string draw-strings
   draw-centered-string draw-centered-strings
   draw-cornered-string 
   draw-vertical-string draw-vertical-strings
   draw-centered-vertical-string draw-centered-vertical-strings

   ;; images
   image-space
   image-translation-space
   image-pixel-vector
   cache-image
   magnify-byte-array
   magnify-image
   draw-magnified-image
   stretch-byte-array
   stretch-image
   draw-stretched-image
   draw-inverse-transformed-image
	    
   ;; color
   pixel->color
   color->pixel
   colorname->color
   colorname->pixel
   make-colormap
   default-colormap
   grayscale-colormap
   colornames-colormap
   -paint-hs-bits-
   -paint-hs-mask-
   -paint-hs-values-
   -paint-v-bits-
   -paint-v-mask-
   -paint-v-values-

   -strong-names-
   -weak-names-
   -sat-names-
	    
   Hue-Sat-Name
   strong-colorname?
   coerce-saturation
   Hsi
   hs
   hs+v
   hs+v-ref
   paint-wash
   hue-sat-name->hs
   paint-colormap
   paint-gray-pixel
   brightness-paint-colormap
	    
   ;; cursors
   window-cursor

   ;; events
   ;;*default-event-mask*
   event-mask
   event-list->event-mask

   ;; menu
   menu
	    
   ;; hardcopy
   raw-dump
   ps-dump
   ))

