;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================
;;; Spaces for locations and displacements in xlib:Images
;;; (see the Geometry package)
;;;============================================================

(defparameter *image-spaces* (az:make-alist-table :test #'xlib:window-equal)
  "A lazy table of g:Int-Grid spaces, one for each xlib:Image.")

(defun image-space (image)
  "Returns a unique g:Int-Grid space for points in the image."
  (az:declare-check (type xlib:Image image)
		    (:returns (type g:Int-Grid)))
  (az:lazy-lookup
   image *image-spaces*
   (make-instance 'g:Int-Grid
     :dimension 2
     :name (format nil "~a" image)
     :translation-space (make-instance 'g:Int-Lattice
			  :dimension 2
			  :name (format nil "~a" image)))))

(defun image-translation-space (image)
  "Returns a unique g:Int-Lattice for displacements in the image.
[Actually, just short for (g:translation-space (drawbable-space image)).]"
  (az:declare-check (type xlib:Image image)
		    (:returns (type g:Int-Lattice)))
  (g:translation-space (drawable-space image)))

;;;============================================================
;;; Image data access
;;;============================================================

(defun image-pixel-vector (image)
  (declare (type xlib:Image-Z image))
  (az:Array-data (xlib:image-z-pixarray image)))

;;;============================================================
;;; Drawing Images
;;;============================================================

(defparameter *cache-image* nil)

(declaim (inline cache-image))

(defun cache-image (w h)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Card16 w h))
  (when (or (null *cache-image*)
	    (< (the xlib:Card16 (xlib:image-width *cache-image*)) w)
	    (< (the xlib:Card16 (xlib:image-height *cache-image*)) h))
    (setf *cache-image*
      (xlib:create-image
       :data (az:make-card8-array (list h w))
       :format :z-pixmap :depth 8 :height h :width w)))
  (values *cache-image*))

;;;============================================================

(defgeneric draw-inverse-transformed-image (drawable gc image map)

  (:documentation
   "Draw <image> in <drawable>, transformed by the inverse of <map>.")
  
  #-sbcl (declare (type xlib:Drawable drawable)
		  (type xlib:Gcontext gc)
		  (type xlib:Image-Z image)
		  (type g:Int-Grid-Map map)))

;;;------------------------------------------------------------

(defmethod draw-inverse-transformed-image (drawable gc cod-image
					   (map g:Int-Grid-Map))

  "Draw <image> in <drawable>, transformed by the inverse of <map>."
  
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Drawable drawable)
	   (type xlib:Gcontext gc)
	   (type xlib:Image-Z cod-image)
	   (type g:Int-Grid-Map map))
  
  (let* ((x-cache (aref (g::caches map) 0))
	 (y-cache (aref (g::caches map) 1))
	 (xdom (g:x (g:dom-origin map)))
	 (ydom (g:y (g:dom-origin map)))
	 (wdom (g:x (g:dom-extent map)))
	 (hdom (g:y (g:dom-extent map)))
	 (dom-image (cache-image wdom hdom))
	 (dom-vector (az:array-data (xlib:image-z-pixarray dom-image)))
	 (dom-rowlen (array-dimension (xlib:image-z-pixarray dom-image) 1))
	 (cod-vector (az:array-data (xlib:image-z-pixarray cod-image)))
	 (cod-rowlen (array-dimension (xlib:image-z-pixarray cod-image) 1)))

    (declare (type az:Card16-Vector x-cache y-cache)
	     (type xlib:Image-Z dom-image)
	     (type az:Card8-Vector dom-vector cod-vector)
	     (type az:Int16 xdom ydom)
	     (type az:Card16 wdom hdom dom-rowlen cod-rowlen))
    
    (dotimes (i hdom)
      (declare (type az:Card16 i))
      (let ((jdom (+ xdom (* dom-rowlen (+ ydom i))))
	    (jcod (* cod-rowlen (the az:Card16 (aref y-cache i)))))
	(declare (type az:Card28 jdom jcod))
	(dotimes (j wdom)
	  (declare (type az:Card16 j))
	  (setf (aref dom-vector (the az:Card28 (+ jdom j)))
	    (aref cod-vector (the az:Card28 (+ jcod (aref x-cache j))))))))
    
    (xlib:put-image drawable gc dom-image 
		    :x xdom :y ydom 
		    :src-x xdom :src-y ydom 
		    :width wdom :height hdom))
  t)

