;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(defparameter *cursor-foreground-color*
    (xlib:make-color :red 0.0 :green 0.0 :blue 0.0)
  "Black")

(defparameter *cursor-background-color*
    (xlib:make-color :red 1.0 :green 1.0 :blue 1.0)
  "White")

(defparameter *cursor-fonts* (az:make-alist-table :test #'xlib:window-equal)
  "A table that maps root windows to the standard X cursor fonts.")

(defun window-cursor-font (window)
  "Lazy creation of cursor fonts for windows."
  (declare (type xlib:Window window))
  (let* ((root (drawable-root window))
	 (font (az:lookup root *cursor-fonts* nil)))
    (declare (type xlib:Window root)
	     (type (or Null xlib:Font) font))
    (when (null font)
      (setf font (xlib:open-font (xlib:drawable-display root) "cursor"))
      (setf (az:lookup root *cursor-fonts*) font))
    font))

(defun cursor-name->font-index (name)
  (ecase name
    (:X-cursor 0)
    (:arrow 2)
    (:based-arrow-down 4)
    (:based-arrow-up 6)
    (:boat 8)
    (:bogosity 10)
    (:bottom-left-corner 12)
    (:bottom-right-corner 14)
    (:bottom-side 16)
    (:box-spiral 18)
    (:center-ptr 20)
    (:circle 24)
    (:clock 26)
    (:coffee-mug 28)
    (:cross 30)
    (:cross-reverse 32)
    (:diamond-cross 34)
    (:dot 38)
    (:dotbox 40)
    (:double-arrow 42)
    (:draft-large 44)
    (:draft-small 46)
    (:draped-box 48)
    (:exchange 50)
    (:fleur 52)
    (:gobbler 54)
    (:gumby 56)
    (:hand1 58)
    (:hand2 60)
    (:heart 62)
    (:icon 64)
    (:iron-cross 66)
    (:left-ptr 68)
    (:left-side 70)
    (:left-tee 72)
    (:left-button 74)
    (:ll-angle 76)
    (:lr-angle 78)
    (:man 80)
    (:middle-button 82)
    (:mouse 84)
    (:pencil 86)
    (:pirate 88)
    (:plus 90)
    (:question-arrow 92)
    (:right-ptr 94)
    (:right-side 96)
    (:right-tee 98)
    (:rightbutton 100)
    (:rtl-logo 102)
    (:sailboat 104)
    (:sb-down-arrow 106)
    (:sb-h-double-arrow 108)
    (:sb-left-arrow 110)
    (:sb-right-arrow 112)
    (:sb-up-arrow 114)
    (:sb-v-double-arrow 116)
    (:shuttle 118)
    (:sizing 120)
    (:spider 122)
    (:spraycan 124)
    (:star 126)
    (:target 128)
    (:tcross 130)
    (:top-left-arrow 132)
    (:top-left-corner 134)
    (:top-right-corner 136)
    (:top-side 138)
    (:top-tee 140)
    (:trek 142)
    (:ul-angle 144)
    (:umbrella 146)
    (:ur-angle 148)
    (:watch 150)
    (:xterm 152)))

;;;---------------------------------------------------------------------

(defun make-glyph-cursor (window name)
  (declare (type xlib:Window window)
	   (type Keyword name))
  "Translate keyword to font index. Names based on X11/cursorfont.h"
  (let ((source-char (cursor-name->font-index name)))
    (declare (type (Integer 0 152) source-char))
    (xlib:create-glyph-cursor
     :source-char source-char
     :mask-char (+ 1 source-char)
     :source-font (window-cursor-font window)
     :mask-font (window-cursor-font window)
     :foreground *cursor-foreground-color*
     :background *cursor-background-color*)))

;;;---------------------------------------------------------------------

(defun make-bitmap-cursor-from-file (window path mask-path)
  "Define a cursor from a random bitmap file."
  (let* ((image  (xlib:read-bitmap-file path))
	 (mask-image (xlib:read-bitmap-file mask-path))
	 (x (xlib:image-x-hot image))
	 (y (xlib:image-y-hot image))
	 (w (xlib:image-width image))
	 (h (xlib:image-height image))
	 (p (xlib:create-pixmap :drawable window :width w :height h :depth 1))
	 (m (xlib:create-pixmap :drawable window :width w :height h :depth 1))
	 (f *cursor-foreground-color*)
	 (b *cursor-background-color*)
	 (gc (xlib:create-gcontext :drawable p :foreground 1 :background 0))
	 (cursor nil))

    (multiple-value-bind
	(maxw maxh) (xlib:query-best-cursor w h (xlib:drawable-display window))
      (when (or (> w maxw) (> h maxh))
	(error "~a at ~dx~d ~a exceeds the max cursor size ~dx~d."
	       path w h image maxw maxh))) ; $$$$ path seemed like good missing first arg -jm
    
    (xlib:put-image p gc image :x 0 :y 0 :width w :height h)
    (xlib:put-image m gc mask-image :x 0 :y 0 :width w :height h )
    (setf cursor
      (xlib:create-cursor
       :source p :mask m :x x :y y :foreground f :background b))
    (xlib:free-pixmap m)
    (xlib:free-pixmap p)
    cursor))

;;;---------------------------------------------------------------------

(defun make-bitmap-cursor (window name)
  (let ((namestring (string-downcase (symbol-name name))))
    (declare (type String namestring))
    (make-bitmap-cursor-from-file
     window
     (concatenate 'String 
       cl-user::*az-dir* "bitmaps/" namestring ".bitmap")
     (concatenate 'String 
       cl-user::*az-dir* "bitmaps/" namestring "-mask.bitmap"))))

;;;---------------------------------------------------------------------

(defparameter *window-cursors* (az:make-alist-table :test #'xlib:window-equal)
  "A table that maps root windows to cursor name tables")

(defun window-cursor (window name)
  "Lazy creation of cursor objects corresponding to keyword names."
  (az:lazy-lookup
   name
   (az:lazy-lookup (drawable-root window) *window-cursors*
		    (make-hash-table :test #'eq))
   (if (member name '(:cactus :magnifier :paint-brush :watercolor-brush))
       (make-bitmap-cursor window name)   
     (make-glyph-cursor window name))))
    
;;;---------------------------------------------------------------------
#||
(defparameter *cactus-cursor*
    (window-cursor
     (xlib:screen-root (xlib:display-default-screen (default-display)))
     :cactus))

(defparameter *paint-brush-cursor*
    (window-cursor
     (xlib:screen-root (xlib:display-default-screen (default-display)))
     :paint-brush))

(defparameter *watercolor-brush-cursor*
    (window-cursor
     (xlib:screen-root (xlib:display-default-screen (default-display)))
     :watercolor-brush))

(defparameter *magnifying-glass-cursor*
    (window-cursor
     (xlib:screen-root (xlib:display-default-screen (default-display)))
     :magnifier))

||#
