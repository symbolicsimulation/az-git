;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(defun show-colormap (colormap)
  (declare (type xlib:Colormap colormap))
  (let* ((window (make-window :display (xlib:colormap-display colormap)
			      :backing-store :always
			      :colormap colormap
			      :name (format nil "~s" colormap)))
	 (width (xlib:drawable-width window))
	 (height (xlib:drawable-height window))
	 (space (drawable-space window))
	 (xstep (truncate width 16))
	 (ystep (truncate height 16))
	 (dx (- xstep 2))
	 (dy (- ystep 2))
	 (x 0)
	 (y 0)
	 (gcontext (make-gcontext window)))
    (declare (type xlib:Window window)
	     (type xlib:Card16 width height xstep ystep dx dy x y)
	     (type xlib:Gcontext gcontext))
    (g:with-borrowed-rect (r space :width dx :height dy)
      (locally (declare (type g:Rect r))
	(dotimes (i 16)
	  (dotimes (j 16)
	    (setf (xlib:gcontext-foreground gcontext) (+ j (* i 16)))
	    (setf (g:x r) x)
	    (setf (g:y r) y)
	    (draw-rect window gcontext r t)
	    (incf x xstep))
	  (incf y ystep)
	  (setf x 0))))
    (drawable-finish-output window)
    window))