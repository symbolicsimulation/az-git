;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(defun gcontext-arrowhead-length (gcontext)
  (declare (type xlib:Gcontext gcontext)
	   (ignore gcontext))
  8)

;;;============================================================

(defun make-gcontext (drawable
		      &key
		      (background (xlib:screen-white-pixel
				   (drawable-default-screen drawable)))
		      (foreground (xlib:screen-black-pixel
				   (drawable-default-screen drawable)))
		      (fill-style :solid)
		      (plane-mask #b11111111)
		      (font (xlib::open-font (xlib:drawable-display drawable)
					     *default-font-name*))
		      (function boole-1)
		      (line-width 0))

  "Make a gcontext with my favorite defaults."
  
  (declare (type xlib:Drawable drawable)
	   (type xlib:Pixel background foreground)
	   (type xlt:Fill-Style fill-style)
	   (type xlib:Pixel plane-mask)
	   (type xlib:Font font)
	   (type xlib:Boole-Constant function)
	   (type xlib:Card16))
  
  (xlib:create-gcontext :drawable drawable
			:cache-p t
			:background background
			:foreground foreground
			:exposures :off
			:fill-style fill-style
			:font font
			:function function
			:line-width line-width
			:plane-mask plane-mask))

;;;============================================================

(defun copy-gcontext-for-drawable (drawable gcontext)

  "Make a copy of <gcontext> that can be used on <drawable>."
  
  (declare (type xlib:Gcontext gcontext)
	   (type xlib:Drawable drawable))

  (make-gcontext drawable
		 :function (xlib:gcontext-function gcontext)
		 :background (xlib:gcontext-background gcontext)
		 :foreground (xlib:gcontext-foreground gcontext)
		 :fill-style (xlib:gcontext-fill-style gcontext)
		 :line-width (xlib:gcontext-line-width gcontext)
		 :plane-mask (xlib:gcontext-plane-mask gcontext)
		 :font (xlib::open-font
			(xlib:drawable-display drawable)
			(xlib:font-name (xlib:gcontext-font gcontext)))))

;;;============================================================

(defparameter *drawable-default-gcontexts*
    (az:make-alist-table :test #'xlib:window-equal)
  "A table that maps root windows to default gcontexts.")

(defun drawable-default-gcontext (drawable)
  (declare (type xlib:Drawable drawable))
  (let ((root (drawable-root drawable)))
    (declare (type xlib:Window root))
    (az:lazy-lookup root *drawable-default-gcontexts* 
		     (xlt:make-gcontext root))))

(defparameter *drawable-erasing-gcontexts*
    (az:make-alist-table :test #'xlib:window-equal)
   "A table that maps windows to erasing gcontexts.")

(defun drawable-erasing-gcontext (drawable)
  (declare (type xlib:Drawable drawable))
  (az:lazy-lookup
   drawable *drawable-erasing-gcontexts*
   (xlt:make-gcontext
    drawable
    :foreground
    (etypecase drawable
      (xlib:Window (xlib:window-backing-pixel drawable))
      (xlib:Pixmap (xlib:window-backing-pixel (drawable-root drawable)))))))

(defparameter *drawable-xor-gcontexts*
    (az:make-alist-table :test #'xlib:window-equal)
  "A table that maps root windows to xor-ing gcontexts.")

(defun drawable-xor-gcontext (drawable)
  (declare (type xlib:Drawable drawable))
  (let ((root (drawable-root drawable)))
    (declare (type xlib:Window root))
    (az:lazy-lookup
     root *drawable-xor-gcontexts*
     (xlt:make-gcontext root :function boole-xor))))

;;;============================================================
;;; Gcontexts for image paint
;;;============================================================
;;; We can save the overhead of gcontext changes
;;; by pre-allocating a seperate gcontext for each screen (root window)
;;; each of the 256 possible pixel values we'd want to draw.

(deftype Gcontext-Vector (&optional (len nil))
  (when (null len) (setf len '*))
  #+:sbcl  `(Simple-Array (or Null xlib:Gcontext) (,len))
  #+:cmu  `(Simple-Array (or Null xlib:Gcontext) (,len))
  #+:excl `(Simple-Array T (,len)))

(defun window->gcontext-vector (window gcontext-table constructor)

  "Get a vector of gcontexts (one for each possible pixel value)
that can be used on <window>'s root. If there is not already a
gcontext vector corresponding to <window>'s root in <gcontext-table>
then make a new vector, use <constructor> to fill it, and save it 
in the table."

  (declare (type xlib:Window window)
	   (type az:Table gcontext-table)
	   (type az:Funcallable constructor)
	   (:returns (type Gcontext-Vector)))

  (let ((root (drawable-root window)))
    (declare (type xlib:Window root))
    (az:lazy-lookup
     root
     gcontext-table
     (let ((vec
	    (make-array '(256)
			:element-type #+:cmu '(or Null xlib:Gcontext) #+:sbcl '(or Null xlib:Gcontext) #+:excl T
			:initial-element nil)))
       (declare (type Gcontext-Vector vec))
       (dotimes (i 256) (setf (aref vec i) (funcall constructor root i)))
       vec))))

(defun pixel->gcontext (window pixel gcontext-table constructor)

  "Get a <gcontext> that can be used on <window>'s root window
for the given <pixel>. Get it from <gcontext-table> if it's there.
Otherwise apply  <constructor> to <window> and  <pixel> to create it.
Also save it in the table."
  
  (declare (type xlib:Window window)
	   (type az:Card8 pixel)
	   (type az:Table gcontext-table)
	   (type az:Funcallable constructor)
	   (:returns (type xlib:Gcontext)))

  (let ((gc-vector 
	  (window->gcontext-vector window gcontext-table constructor)))
    (declare (type Gcontext-Vector gc-vector))
    (aref gc-vector pixel)))

;;;------------------------------------------------------------

(defparameter *pixel-default-gcontexts*
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to Gcontext-Vector's.")

(defun make-default-gcontext (window p) (make-gcontext window :foreground p))

(defun window-default-gcontext-vector (window)
  (window->gcontext-vector
   window
   *pixel-default-gcontexts*
   #'make-default-gcontext))

(defun pixel-default-gcontext (window i)
  (declare (type xlib:Window window)
	   (type (Integer 0 255) i))
  (pixel->gcontext window i *pixel-default-gcontexts*
		   #'make-default-gcontext))

;;;------------------------------------------------------------

(defun pure-hue-gcontext (window i)
  (declare (type xlib:Window window)
	   (type Hsi i))
  (pixel-default-gcontext window (hs i)))

(defun colorname->pure-hue-gcontext (window colorname)
  (declare (type xlib:Window window)
	   (type Hue-Sat-Name colorname))
  (pure-hue-gcontext window (hue-sat-name->hsi colorname)))
 
;;;------------------------------------------------------------

(defparameter *hue-sat-gcontexts*
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to Gcontext-Vector's.")

(defun make-hue-sat-gcontext (window pixel)
  (declare (type xlib:Window window)
	   (type az:Card8 pixel))
  (xlt:make-gcontext window
		     :foreground pixel
		     :plane-mask -paint-hs-mask-
		     :background 0))

(defun hue-sat-gcontext (window i)
  (declare (type xlib:Window window)
	   (type Hsi i))
  (pixel->gcontext window (hs i) *hue-sat-gcontexts* #'make-hue-sat-gcontext))

(defun colorname->hue-sat-gcontext (window colorname)
  (declare (type xlib:Window window)
	   (type Hue-Sat-Name colorname))
  (hue-sat-gcontext window (hue-sat-name->hsi colorname)))

;;;------------------------------------------------------------

(defparameter *hue-gcontexts* (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to Gcontext-Vector's.")

(defun make-hue-gcontext (window pixel)
  (declare (type xlib:Window window)
	   (type az:Card8 pixel))
  (xlt:make-gcontext window
		     :foreground pixel
		     :plane-mask -paint-h-mask-
		     :background 0))

(defun hue-gcontext (window i)
  (declare (type xlib:Window window)
	   (type Hsi i))
  (pixel->gcontext window (hs i) *hue-gcontexts* #'make-hue-gcontext))

(defun colorname->hue-gcontext (window colorname)
  (declare (type xlib:Window window)
	   (type Hue-Sat-Name colorname))
  (hue-gcontext window (hue-sat-name->hsi colorname)))

;;;------------------------------------------------------------

(defparameter *strong-gcontexts* 
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to gcontexts that set the saturation to be strong.")

(defun make-strong-gcontext (window)
  (declare (type xlib:Window window))
  (xlt:make-gcontext window
		     :foreground (logxor #b11111111 -paint-s-mask-)
		     :plane-mask -paint-s-mask-
		     :background 0))

(defun strong-gcontext (window)
  (declare (type xlib:Window window))
  (let ((root (drawable-root window)))
    (az:lazy-lookup root *strong-gcontexts* (make-strong-gcontext root))))

;;;------------------------------------------------------------

(defparameter *weak-gcontexts* (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to gcontexts that set the saturation to be weak.")

(defun make-weak-gcontext (window)
  (declare (type xlib:Window window))
  (xlt:make-gcontext window
		     :foreground -paint-s-mask-
		     :plane-mask -paint-s-mask-
		     :background 0))

(defun weak-gcontext (window)
  (declare (type xlib:Window window))
  (let ((root (drawable-root window)))
    (az:lazy-lookup root *weak-gcontexts* (make-weak-gcontext root))))


;;;------------------------------------------------------------

(defun colorname->sat-gcontext (window colorname)
  (if (strong-colorname? colorname)
      (strong-gcontext window)
    (weak-gcontext window)))


 



