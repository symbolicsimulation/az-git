;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================
;;; Getting at some X defaults:
;;;============================================================

(defun default-host ()

  "Returns a string containing the name of the X host, as given in the
Unix DISPLAY environment variable, minus the colon and numbers."

  (declare (:returns (type (or Null String) host-name)))
  (let* ((display-var #+:excl (system:getenv "DISPLAY")
		      #+:sbcl (sb-posix:getenv "DISPLAY")
		      #+:cmu (cdr (assoc :display ext:*environment-list*)))
	 (colon-pos (position #\: display-var)))
    (when colon-pos (subseq display-var 0 colon-pos))))

(defparameter *host-default-display* (make-hash-table :test #'equal)
  "A table that maps host names to default display objects, which have
been opened.")

(defun host-default-display (&key (host (default-host)))

  "Returns a clx display object corresponding to the Unix DISPLAY
environment variable. <display> only calls <xlib:open-display> the
first time it is called, so all references based on <display> will
share the same connection to the X server and can therefore share
gcontexts, input event streams, etc."

  (declare (:returns (type xlib:Display)))
  (let ((display (az:lookup host *host-default-display* nil)))
    (when (null display)
      (setf display (xlib:open-display host))
      (setf (xlib:close-down-mode display) :destroy)
      (setf (az:lookup host *host-default-display*) display))
    display))

;;;============================================================

(defun global-pointer-x (display)
  "Current mouse x in root coordinates."
  (declare (type xlib:Display display)
	   (:returns (type xlib:Int16 root-x)))
  (multiple-value-bind (root-x root-y) (xlib:global-pointer-position display)
    (declare (type xlib:Int16 root-x root-y)
	     (ignore root-y))
    root-x))

(defun global-pointer-y (display)
  "Current mouse y in root coordinates."
  (declare (type xlib:Display display)
	   (:returns (type xlib:Int16 root-y)))
  (multiple-value-bind (root-x root-y) (xlib:global-pointer-position display)
    (declare (type xlib:Int16 root-x root-y)
	     (ignore root-x))
    root-y))
  



