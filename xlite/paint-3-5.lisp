;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================
;;; Paint constants

(defconstant -paint-hs-mask- #b11100000
  "Which bits determine the hue and saturation of a painted object?")

(defconstant -paint-v-mask- (logxor #b11111111 -paint-hs-mask-)
  "Which bits determine the hue and saturation of a painted object?")

(defconstant -paint-hs-bits- (logcount -paint-hs-mask-)
  "How many bits are used to determine the hue and saturation
of a painted object?")

(defconstant -paint-v-bits- (- 8 -paint-hs-bits-)
  "How many bits are used to determine the value (brightness)
 of a painted object?")

(defconstant -paint-hs-values- (ash 1 -paint-hs-bits-)
  "How many possible combinations of hue and saturation are provided?")

;;;------------------------------------------------------------
;;; Paint deftypes

(deftype Hsi () `(Mod ,-paint-hs-values-))

(deftype Hue-Sat-Name ()
  '(Member
    :red :green :blue
    :yellow :magenta :cyan
    :gray
    :strong-red :strong-green :strong-blue
    :strong-yellow :strong-magenta :strong-cyan
    :weak-red :weak-green :weak-blue :weak-yellow :weak-magenta :weak-cyan
    ))

;;;------------------------------------------------------------
;;; Paint macros

(defmacro hs (p)
  "map the hue-saturation index to fully bright pixel value."
  `(logior (ash ,p -paint-v-bits-) -paint-v-mask-))

(defmacro hs+v (p b)
  "Overlay the three most significant bits of the paint pixel <p>
on the band pixel <b> shifted right -paint-hs-bits-"
  `(logior (logand (the az:Card8 ,p) -paint-hs-mask-)
	   (ash (the az:Card8 ,b) (- -paint-hs-bits-))))

(defmacro hs+v-ref (p b i)
  "Like hs+v, but looks up in simple pixel vectors."
  (let ((ii (gensym)))
    `(let ((,ii ,i))
       (declare (type az:Array-Index ,ii))
       (hs+v (aref (the az:Card8-Vector ,p) ,ii)
	   (aref (the az:Card8-Vector ,b) ,ii)))))

;;;------------------------------------------------------------

(defun hue-sat-name->hsi (colorname)
  (declare (type Hue-Sat-Name colorname))
  (ecase colorname
    (:gray 1)
    (:white 1)
    (:black 1)
    (:magenta 2)
    (:red 3)
    (:yellow 4)
    (:green 5)
    (:cyan 6)
    (:blue 7)))

(defun hue-sat-name->hs (colorname)
  (hs (hue-sat-name->hsi colorname)))

;;;------------------------------------------------------------
;;; paint colormaps

(defparameter *paint-gamma* 0.75d0
  "Gamma correction exponent for paint colormap.")

(defparameter *paint-colormaps* (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to colormaps")

(defun paint-colormap (window)
  (let* ((root (drawable-root window))
	 (colormap (az:lookup root *paint-colormaps* nil)))
    (declare (type xlib:Window window)
	     (type (or Null xlib:Colormap) colormap))
    (when (null colormap)
      (setf colormap (xlt:make-colormap window))
      (setf (az:lookup root *paint-colormaps*) colormap)
      (let ((color (xlib:make-color :red 0.0d0 :blue 0.0d0 :green 0.0d0))
	    (default-colors (xlib:query-colors
			     (xlt:default-colormap window)
			     (loop for i from 0 to 31 collect i)
			     :result-type 'Vector)))
	(declare (type xlib:Color color)
		 (type Vector default-colors))
	(xlib:store-color colormap 0 (aref default-colors 0))
	;; do other color levels
	(loop for i from 1 below 32 do
	      (xlib:store-color colormap i (aref default-colors i))
	      (let* ((gray (expt (/ i 31.0d0) *paint-gamma*))
		     (rgb (+ 0.25d0 (* 0.75d0 gray))))
		;; red part
		(setf (xlib:color-red color) rgb)	
		(setf (xlib:color-green color) 0.0d0)
		(setf (xlib:color-blue color) 0.0d0)
		(xlib:store-color colormap (+ i 96) color)    
		;; yellow part
		(setf (xlib:color-green color) rgb)
		(xlib:store-color colormap (+ i 128) color)
		;; green part
		(setf (xlib:color-red color) 0.0d0)
		(xlib:store-color colormap (+ i 160) color)      
		;; cyan part
		(setf (xlib:color-blue color) rgb)
		(xlib:store-color colormap (+ i 192) color)
		;; blue part
		(setf (xlib:color-green color) 0.0d0)
		(xlib:store-color colormap (+ i 224) color)     
		;; magenta part
		(setf (xlib:color-red color) rgb)
		(xlib:store-color colormap (+ i 64) color)
		;; gray part
		(setf (xlib:color-red color) gray)
		(setf (xlib:color-green color) gray)
		(setf (xlib:color-blue color) gray)
		(xlib:store-color colormap (+ i 32) color)))))
    colormap))

;;;------------------------------------------------------------

(defparameter *paint-gray-pixels*
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps root windows to pixels.")

(defun paint-gray-pixel (window)
  (declare (type xlib:Window window))
  (let* ((root (drawable-root window))
	 (p (az:lookup root *paint-gray-pixels* -1)))
    (declare (type xlib:Window root)
	     (type Fixnum p))
    (when (minusp p)
      (setf p (xlt:colorname->pixel (paint-colormap root) :gray))
      (setf (az:lookup root *paint-gray-pixels*) p))
    p))
 
;;;------------------------------------------------------------

(defparameter *brightness-paint-colormaps*
    (az:make-alist-table :test #'xlib:window-equal)
  "Maps screens to colormaps")

(defun brightness-paint-colormap (window)

  "Set up a colormap for image paint with brightness,
rather than the usual color. This is intended primarily
for producing figures for publication in b&w media, rather
than for actual use, for which color is much more effective."

  (declare (type xlib:Window window))
  
  (let*  ((root (drawable-root window))
	  (colormap (az:lookup root *brightness-paint-colormaps* nil)))
    (declare (type xlib:Window root)
	     (type (or Null xlib:Colormap) colormap))
    (when (null colormap)
      (setf colormap (xlt:make-colormap root))
      (setf (az:lookup root *brightness-paint-colormaps*) colormap)
      (let ((color (xlib:make-color :red 0.0d0 :blue 0.0d0 :green 0.0d0))
	    (default-colors (xlib:query-colors
			     (xlt:default-colormap root)
			     (loop for i from 0 to 31 collect i)
			     :result-type 'Vector)))
	(declare (type xlib:Color color))
	(dotimes (i 32)
	  (xlib:store-color colormap i (aref default-colors i))
	  ;; bright parts
	  (let ((x (/ (+ 224 i) 255.0d0)))
	    (declare (type xlib:RGB-Val x))
	    (setf (xlib:color-red color) x)
	    (setf (xlib:color-green color) x)
	    (setf (xlib:color-blue color) x))
	  (xlib:store-color colormap (+ i 32) color)
	  (xlib:store-color colormap (+ i 64) color)
	  (xlib:store-color colormap (+ i 96) color)
	  (xlib:store-color colormap (+ i 128) color)
	  (xlib:store-color colormap (+ i 160) color)
	  (xlib:store-color colormap (+ i 192) color)
	  ;; gray part
	  (let ((x (/ (ash i 2) 255.0d0)))
	    (declare (type xlib:RGB-Val x))
	    (setf (xlib:color-red color) x)
	    (setf (xlib:color-green color) x)
	    (setf (xlib:color-blue color) x)
	    (xlib:store-color colormap (+ i 224) color)))))
    colormap))



