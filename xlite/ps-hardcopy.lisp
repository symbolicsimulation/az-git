;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald  All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(defun ps-dump-header (stream x y width height depth iscale)

  "Dump an image with lower left corner at current origin, scaling by
iscale (iscale=1 means 1/300 inch per pixel)."

  (let ((box-width (ceiling (* width iscale 72) 300))
	(box-height (ceiling (* height iscale 72) 300)))
    (format stream
	    "~
%!PS-Adobe-1.0
%%BoundingBox: 0 0 ~d ~d
%%EndComments
%%Pages: 0
%%EndProlog

gsave

% scale appropriately
~d ~d translate
~d ~d scale
% allocate space for one scanline of input
/picstr ~d string def

% read and dump the image
~d ~d ~d
[~d 0 0 ~d 0 ~d]
{ currentfile picstr readhexstring pop }
image
"
	    ;; bounding box:
	    box-width box-height
	    ;; translation
	    x y
	    ;; scaling
	    box-width box-height
	    ;; length of string  to allocate
	    (ceiling (* width depth) 8)

	    ;; first args to image operator
	    width height depth

	    ;; transformation mtx
	    width (- height) height)

    (fresh-line stream)
    (fresh-line stream)))

;;;============================================================

;(defconstant -hex-digits- "0123456789abcdef") ;; $$$$ -jm
(defparameter -hex-digits- "0123456789abcdef")
(declaim (type Simple-String -hex-digits-))

;;;-----------------------------------------------------------

(defparameter *ps-dump-buffer* "")

(defun ps-dump-buffer (size)
  (when (/= (length *ps-dump-buffer*) size)
    (setf *ps-dump-buffer* (make-string size :initial-element #\F)))
  *ps-dump-buffer*)

;;;-----------------------------------------------------------

(defun ps-dump-1bit-data (stream width height data lit)
  (declare (type Stream stream)
	   (type xlib:Card16 width height)
	   (type az:Bit-Matrix data)
	   (type Simple-Bit-Vector lit))
  (let* ((buflen (* (ceiling width 8) 2))
	 ;; We need to print an integer number of bytes for each scanline.
	 ;; Each byte is represented by 2 hex digit chars.
	 (buf (ps-dump-buffer buflen))
	 (nibble 0)
	 (ibuf 0)
	 (shift 3))
    (declare (type xlib:Card16 buflen nibble ibuf)
	     (type Simple-String buf)
	     (type (Integer -1 3) shift))
    (dotimes (j height)
      (declare (type xlib:Card16 j))
      (setf shift 3)
      (setf ibuf 0)
      (setf nibble 0)
      (dotimes (i width)
	(declare (type xlib:Card16 i))
	(setf nibble
	  (logior nibble (ash (the Bit (sbit lit (aref data j i))) shift)))
	(decf shift)
	(when (< shift 0)
	  (setf (schar buf ibuf) (schar -hex-digits- nibble))
	  (setf nibble 0)
	  (setf shift 3)
	  (incf ibuf)))
      (unless (= shift 3) (setf (schar buf ibuf) (schar -hex-digits- nibble)))
      (fresh-line stream)
      (write-string buf stream))
    (fresh-line stream)))

;;;-----------------------------------------------------------

(defun ps-dump-4bit-data (stream width height data lit)
  (declare (type Stream stream)
	   (type xlib:Card16 width height)
	   (type az:Card4-Matrix data)
	   (type az:Card4-Vector lit))
  (let* ((buflen (* 2 (ceiling width 2)))
	 ;; need to print an integer number of bytes,
	 ;; each represented by 2 hex digits.
	 (buf (ps-dump-buffer buflen)))
    (declare (type Simple-String buf))
    (dotimes (j height)
      (declare (type xlib:Card16 j))
      (dotimes (i width)
	(declare (type xlib:Card16 i))
	(setf (schar buf i) (schar -hex-digits- (aref lit (aref data j i)))))
      (fresh-line stream)
      (write-string buf stream))
    (fresh-line stream)))

;;;-----------------------------------------------------------

(defun ps-dump-8bit-data (stream width height data lit)
  (declare (type Stream stream)
	   (type xlib:Card16 width height)
	   (type az:Card8-Matrix data)
	   (type az:Card8-Vector lit))
  (dotimes (j height)
    (declare (type xlib:Card16 j))
    (let ((buf (ps-dump-buffer (* 2 width)))
	  (2i 0)
	  (byte 0))
      (declare (type xlib:Card16 2i byte)
	       (type Simple-String buf))
      (dotimes (i width)
	(declare (type xlib:Card16 i))
	(setf byte (aref lit (aref data j i)))
	(setf (schar buf 2i) (schar -hex-digits- (ash byte -4)))
	(incf 2i)
	(setf (schar buf 2i) (schar -hex-digits- (logand byte #x0f)))
	(incf 2i))
      (fresh-line stream)
      (write-string buf stream)))
  (fresh-line stream))

;;;============================================================

(defun ps-dump-trailer (stream showpage?)
  (fresh-line stream)
  (fresh-line stream)
  (when showpage? (format stream "showpage"))
  (fresh-line stream)
  (format stream "grestore")
  (fresh-line stream)
  (format stream "%%Trailer")
  (fresh-line stream)
  (fresh-line stream))

;;;============================================================

(defun ps-dump (window file-name
		&key
		(scale 4)
		(whole-window? nil)
		(showpage? nil)
		(screen (xlib:display-default-screen
			 (xlib:drawable-display window)))
		(depth (xlib:screen-root-depth screen)))

  "Dump the window (as an image) to a postscript file.

<scale> determines the magnification from window pixels to printer
pixels, eg., if <scale> is 1, then a 300x300 window will print as a 1
inch x 1 inch bitmap on a 300 dpi printer.

If <whole-window?> is not <nil>, then borders, title bars, etc., will
be dumped as well as the strictly Window contents of the window.

If <showpage?> is not <nil>, then a <showpage> instruction will be
appended to the file. <showpage?> should be <nil> for dumps that are
intended to be included in other documents as figures and should be
<t> if the dump is intended to be printed by itself.

<depth> determines the number of bits per pixel sent to the postscript
printer."

  (declare (type xlib:Window window)
	   (type String file-name)
	   (type (Integer 0 128) scale)
	   (type (Member 1 2 4 8) depth))
  (multiple-value-bind
      (data width height) (window-array window whole-window?)
    (let ((lit (colormap-lit window)))
      (with-open-file (stream file-name
		       :direction :output
		       :if-exists :supersede
		       :if-does-not-exist :create)
	(ps-dump-header stream 16 16 width height depth scale)
	(ecase depth
	  (1 (ps-dump-1bit-data stream width height data lit))
	  (4 (ps-dump-4bit-data stream width height data lit))
	  (8 (ps-dump-8bit-data stream width height data lit)))
	(ps-dump-trailer stream showpage?))))
  t)




