;;;-*- Package: :Xlite; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Xlite)

;;;============================================================

(defparameter *default-font-name*"-*-helvetica-medium-r-*-*-*-80-*-*-*-*-*-*")

;;;============================================================

(defun %analyze-character-placement (char font)
  (declare (optimize (safety 0) (speed 3))
	   (type Character char)
	   (type xlib:Font font))
  (let ((font-index (char-code char)))
    (declare (type Fixnum font-index))
    (values (xlib:char-width font font-index)
	    (xlib:char-ascent font font-index)
	    (xlib:char-descent font font-index))))

(defun analyze-character-placement (char font)
  "Returns width, ascent, and descent as multiple values."
  (declare (type Character char)
	   (type xlib:Font font))
  (%analyze-character-placement char font))

;;;------------------------------------------------------------

(defun %character-rect (char font origin result)
  (declare (optimize (safety 0) (speed 3))
	   (type Character char)
	   (type xlib:Font font)
	   (type g:Point origin)
	   (type g:Rect result))
  (let* ((font-index (char-code char))
	 (a (xlib:char-ascent font font-index)))
    (declare (type Fixnum font-index)
	     (type az:Int16 a))
    (setf (g:x result) (g:x origin))
    (setf (g:y result) (- (the az:Int16 (g:y origin)) a))
    (setf (g:w result) (xlib:char-width font font-index))
    (setf (g:h result) (+ a (xlib:char-descent font font-index))))
  result)

(defun character-rect (char font
		       &key
		       (home g:*screen-space*)
		       (position (g:make-element home))
		       (result (g:make-rect home)))
  (declare (type Character char)
	   (type xlib:Font font)
	   (type g:Point position)
	   (type g:Rect result))
  (%character-rect char font position result)
  result)

;;;------------------------------------------------------------

(defun %character-width (char font)
  (declare (optimize (safety 0) (speed 3))
	   (type Character char)
	   (type xlib:Font font))
  (xlib:char-width font (char-code char)))

(defun character-width (char font)
  (declare (type Character char)
	   (type xlib:Font font))
  (%character-width char font))

(defun %character-height (char font)
  (declare (optimize (safety 0) (speed 3))
	   (type Character char)
	   (type xlib:Font font))
  (let ((font-index (char-code char)))
    (declare (type Fixnum font-index))
    (the az:Int16
      (+ (the az:Int16 (xlib:char-ascent font font-index))
	 (the az:Int16 (xlib:char-descent font font-index))))))

(defun character-height ( char font)
  (declare (type Character char)
	   (type xlib:Font font))
  (%character-height char font))

;;;------------------------------------------------------------

(defun %character-ascent (char font)
  (declare (optimize (safety 0) (speed 3))
	   (type Character char)
	   (type xlib:Font font))
  (xlib:char-ascent font (char-code char)))

(defun character-ascent (char font)
  (declare (type Character char)
	   (type xlib:Font font))
  (%character-ascent  char font))

(defun %character-descent (char font)
  (declare (optimize (safety 0) (speed 3))
	   (type Character char)
	   (type xlib:Font font))
  (xlib:char-descent font (char-code char)))

(defun character-descent (char font)
  (declare (type Character char)
	   (type xlib:Font font))
  (%character-descent char font))

;;;------------------------------------------------------------

(defun %analyze-string-placement (string font)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String string)
	   (type xlib:Font font))
  (multiple-value-bind
      (width ascent descent left-bearing right-bearing
       overall-ascent overall-descent overall-direction next-start)
      (xlib:text-extents font string)
    (declare (ignore ascent descent left-bearing right-bearing
		     overall-direction next-start)
	     (type az:Int16 width overall-ascent overall-descent))
    (values width overall-ascent overall-descent)))

(defun analyze-string-placement (string font)
  "returns string width, ascent, and descent, by iterating over the chars."
  (declare (type String string)
	   (type xlib:Font font))
  (%analyze-string-placement string font))

;;;------------------------------------------------------------

(defun %string-rect ( string font position result)
  (declare (type String string)
           (type xlib:Font font)
           (type g:Point position)
           (type g:Rect result))
  (multiple-value-bind (w a d) (%analyze-string-placement string font)
    (setf (g:x result) (g:x position))
    (setf (g:y result) (- (g:y position) a))
    (setf (g:w result) w)
    (setf (g:h result) (+ a d))
    result))

(defun string-rect (string font
		    &key
		    (home g:*screen-space*)
		    (position (g:make-element home))
		    (result (g:make-rect home)))
  (declare (type String string)
	   (type xlib:Font font)
	   (type g:Point position)
	   (type g:Rect result))
  (%string-rect string font position result)
  result)

;;;------------------------------------------------------------

(defun %string-extent (string font result)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String string)
	   (type xlib:Font font)
	   (type g:Point result))
  (multiple-value-bind
      (width ascent descent left-bearing right-bearing
       overall-ascent overall-descent overall-direction next-start)
      (xlib:text-extents font string)
    (declare (ignore ascent descent left-bearing right-bearing
		     overall-direction next-start)
	     (type az:Int16 width overall-ascent overall-descent))
    (setf (g:x result) width)
    (setf (g:y result) (+ overall-ascent overall-descent))))

(defun string-extent (string font
		      &key
		      (home g:*screen-space*)
		      (result (g:make-element (g:translation-space home))))
  (declare (type String string)
	   (type xlib:Font font)
	   (type g:Point result))
  (%string-extent string font result)
  result)

;;;------------------------------------------------------------

(defun %string-width (string font)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String string)
	   (type xlib:Font font))
  (xlib:text-width font string))

(defun string-width (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%string-width string font))

(defun %string-height (string font)
  (declare (type String string)
           (type xlib:Font font))
  (multiple-value-bind (w a d) (%analyze-string-placement string font)
    (declare (ignore w))
    (+ a d)))

(defun string-height (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%string-height string font))

;;;------------------------------------------------------------

(defun maximum-string-height (strings font)
  (declare (type List strings)
	   (type xlib:Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%string-height string font))
      (when (> x max) (setf max x)))
    max))

(defun maximum-string-width (strings font)
  (declare (type List strings)
	   (type xlib:Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%string-width string font))
      (when (> x max) (setf max x)))
    max))

;;;------------------------------------------------------------

(defun %string-ascent (string font)
  (declare (type String string)
           (type xlib:Font font))
  (multiple-value-bind (w a d) (%analyze-string-placement string font)
    (declare (ignore w d))
    a))

(defun string-ascent (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%string-ascent string font))

(defun %string-descent (string font)
  (declare (type String string)
           (type xlib:Font font))
  (multiple-value-bind (w a d) (%analyze-string-placement string font)
    (declare (ignore w a))
    d))

(defun string-descent (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%string-descent string font))

;;;------------------------------------------------------------

(defun %analyze-vertical-string-placement (string font)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String string)
	   (type xlib:Font font))
  (let ((width 0)
	(height 0))
    (declare (type az:Card16 width height))
    (dotimes (i (length string))
      (let* ((font-index (char-code (char string i)))
	     (w (xlib:char-width font font-index))
	     (a (xlib:char-ascent font font-index))
	     (d (xlib:char-descent font font-index)))
	(declare (type Fixnum font-index)
		 (type az:Int16 w a d))
	(az:maxf width w)
	;; except for first char, move down by this char's ascent
	;; a+d turns out to be exact character height, with no
	;; extra room, so add a pixel space for readability
	;; We should probably be using the font line height,
	;; or some such, here. As it is this doesn't do the right thing
	;; with spaces.
	(incf height (+ a d 1))))
    (values width 0 height)))

(defun analyze-vertical-string-placement (string font)
  "Returns string width, ascent, and descent, by iterating over the chars."
  (declare (type String string)
	   (type xlib:Font font))
  (%analyze-vertical-string-placement string font))

;;;------------------------------------------------------------

(defun %vertical-string-rect (string font position result)
  (declare (type String string)
           (type xlib:Font font)
           (type g:Point position)
           (type g:Rect result))
  (multiple-value-bind (w a d) (%analyze-vertical-string-placement string font)
    (setf (g:x result) (g:x position))
    (setf (g:y result) (g:y position))
    (setf (g:w result) w)
    (setf (g:h result) (+ a d))
    result))

(defun vertical-string-rect (string font 
			     &key
			     (home g:*screen-space*)
			     (position (g:make-element home))
			     (result (g:make-rect home)))
  "A vertical string's region has the point at its upper left corner,
not the left baseline of the first character.  This is done to make it
easier to draw a sequence of vertical strings without overlapping."
  (declare (type String string)
	   (type xlib:Font font)
	   (type g:Point position)  
	   (type g:Rect result))
  (%vertical-string-rect string font position result)
  result)

;;;------------------------------------------------------------

(defun %vertical-string-extent (string font result)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String string)
	   (type xlib:Font font)
	   (type g:Point result))
  (let ((width 0)
	(height 0))
    (declare (type az:Card16 width height))
    (dotimes (i (length string))
      (let* ((font-index (char-code (char string i)))
	     (w (xlib:char-width font font-index))
	     (a (xlib:char-ascent font font-index))
	     (d (xlib:char-descent font font-index)))
	(declare (type Fixnum font-index)
		 (type az:Int16 w a d))
	(az:maxf width w)
	;; except for first char, move down by this char's ascent
	;; a+d turns out to be exact character height, with no
	;; extra room, so add a pixel space for readability
	;; We should probably be using the font line height,
	;; or some such, here. As it is this doesn't do the right thing
	;; with spaces.
	(incf height (+ a d 1))))
    (setf (g:x result) width)
    (setf (g:y result) height)))

(defun vertical-string-extent (string font
			       &key
			       (home g:*screen-space*)
			       (result
				(g:make-element (g:translation-space home))))
  (declare (type String string)
	   (type xlib:Font font)
	   (type g:Point result))
  (%vertical-string-extent string font result)
  result)

;;;------------------------------------------------------------

(defun %vertical-string-width (string font)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String string)
	   (type xlib:Font font))   
  (let ((width 0))
    (declare (type az:Int16 width))
    (dotimes (i (length string))
      (declare (type Fixnum i))
      (let ((w (xlib:char-width font (char-code (char string i)))))
	(declare (type az:Int16 w))
	(az:maxf width w)))
    (values width)))

(defun vertical-string-width (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%vertical-string-width string font))

;;;------------------------------------------------------------

(defun %vertical-string-height (string font)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String string)
	   (type xlib:Font font))
  (let ((height 0))
    (declare (type az:Int16 height))
    (dotimes (i (length string))
      (declare (type Fixnum i))
      (let ((index (char-code (char string i))))
	(declare (type Fixnum index))
	(incf height
	      (+ (the az:Int16 (xlib:char-ascent font index))
		 (the az:Int16 (xlib:char-descent font index))))))
    (values height)))

(defun vertical-string-height (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%vertical-string-height string font))

;;;------------------------------------------------------------

(defun maximum-vertical-string-width (strings font)
  (declare (type List strings)
	   (type xlib:Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%vertical-string-width string font))
      (when (> x max) (setf max x)))
    max))

(defun maximum-vertical-string-height (strings font)
  (declare (type List strings)
	   (type xlib:Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%vertical-string-height string font))
      (when (> x max) (setf max x)))
    max))

;;;------------------------------------------------------------

(defun %vertical-string-ascent (string font)
  (declare (type String string)
           (type xlib:Font font))
  (multiple-value-bind (w a d) (%analyze-vertical-string-placement string font)
    (declare (ignore w d))
    a))

(defun vertical-string-ascent (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%vertical-string-ascent string font))

(defun %vertical-string-descent (string font)
  (declare (type String string)
           (type xlib:Font font))
  (multiple-value-bind (w a d) (%analyze-vertical-string-placement string font)
    (declare (ignore w a))
    d))

(defun vertical-string-descent (string font)
  (declare (type String string)
	   (type xlib:Font font))
  (%vertical-string-descent string font)) 



