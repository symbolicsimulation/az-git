;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Basic-Math)

;;;=======================================================
;;;		Assorted array functions
;;;=======================================================

(defun diagonal-zeros? (a)
  (az:type-check az:Float-Matrix a)
  (dotimes (i (min (array-dimension a 0) (array-dimension a 1)))
    (when (zerop (aref a i i)) (return-from diagonal-zeros? t)))
  nil)
  
;;;-------------------------------------------------------

(defun add-arrays (a0 a1
		   &optional
		   (a2 (az:make-float-array (array-dimensions a0))))
  "Overwrites a2 with the sum of a0 and a1."
  (declare (type az:Float-Array a0 a1 a2)
	   (:returns a2))
  (assert (az:equal-array-dimensions? a0 a1 a2))
  (cond
    ((eq a2 a0) (add-arrays-left! a0 a1))
    ((eq a2 a1) (add-arrays-right! a0 a1))
    (t (ecase (array-rank a0)
	 (1 (let ((n (length a0)))
	      (v<-v! a2 a0 :end0 n :end1 n)
	      (v+v! a2 a1 :end0 n :end1 n)))
	 (2 (let ((m (array-dimension a0 0))
		  (n (array-dimension a0 1)))
	      (dotimes (i m)
		(v<-v! a2 a0
			:vtype0 :row :end0 n :on0 i
			:vtype1 :row :end1 n :on1 i)
		(v+v!  a2 a1
			:vtype0 :row :end0 n :on0 i
			:vtype1 :row :end1 n :on1 i)))))))
  a2)

(defun add-arrays-left! (a0 a1)
  "Overwrites a0 with the sum of a0 and a1."
  (declare (type az:Float-Array a0 a1)
	   (:returns a0))
  (assert (az:equal-array-dimensions? a0 a1))
  (ecase (array-rank a0)
    (1 (let ((n (length a0)))
	 (v+v! a0 a1 :end0 n :end1 n)))
    (2 (let ((m (array-dimension a0 0))
	     (n (array-dimension a0 1)))
	 (dotimes (i m)
	   (v+v! a0 a1
		  :vtype0 :row :end0 n :on0 i
		  :vtype1 :row :end1 n :on1 i))))) 
  a0)

(defun add-arrays-right! (a0 a1)
  "Overwrites a1 with the sum of a0 and a1."
  (declare (type az:Float-Array a0 a1)
	   (:returns a0))
  (assert (az:equal-array-dimensions? a0 a1))
  (ecase (array-rank a0)
    (1 (let ((n (length a0)))
	 (v+v! a1 a0 :end0 n :end1 n)))
    (2 (let ((m (array-dimension a0 0))
	     (n (array-dimension a0 1)))
	 (dotimes (i m)
	   (v+v! a1 a0
		  :vtype0 :row :end0 n :on0 i
		  :vtype1 :row :end1 n :on1 i)))))
  a1)

;;;=======================================================

(defun sub-arrays (a0 a1
			&optional
			(a2 (az:make-float-array (array-dimensions a0))))
  "Overwrites a2 with the sum of a0 and a1."
  (declare (type az:Float-Array a0 a1 a2)
	   (:returns a2))
  (assert (az:equal-array-dimensions? a0 a1 a2))
  (cond
   ((eq a2 a0) (sub-arrays-left! a0 a1))
   ((eq a2 a1) (sub-arrays-right! a0 a1))
   (t (ecase (array-rank a0)
	(1 (let ((n (length a0)))
	     (v<-v! a2 a0 :end0 n :end1 n)
	     (v-v! a2 a1 :end0 n :end1 n)))
	(2 (let ((m (array-dimension a0 0))
		 (n (array-dimension a0 1)))
	     (dotimes (i m)
	       (v<-v! a2 a0
		      :vtype0 :row :end0 n :on0 i
		      :vtype1 :row :end1 n :on1 i)
	       (v-v!  a2 a1
		      :vtype0 :row :end0 n :on0 i
		      :vtype1 :row :end1 n :on1 i)) )))))
  a2)

(defun sub-arrays-left! (a0 a1)
  "Overwrites a0 with the difference of a0 and a1."
  (declare (type az:Float-Array a0 a1)
	   (:returns a0))
  (assert (az:equal-array-dimensions? a0 a1))
  (ecase (array-rank a0)
    (1 (let ((n (length a0)))
	 (v-v! a0 a1 :end0 n :end1 n)))
    (2 (let ((m (array-dimension a0 0))
	     (n (array-dimension a0 1)))
	 (dotimes (i m)
	   (v-v! a0 a1
		 :vtype0 :row :end0 n :on0 i
		 :vtype1 :row :end1 n :on1 i))))) 
  a0)

(defun sub-arrays-right! (a0 a1)
  "Overwrites a1 with the difference of a0 and a1."
  (declare (type az:Float-Array a0 a1)
	   (:returns a1))
  (assert (az:equal-array-dimensions? a0 a1))
  (ecase (array-rank a0)
    (1 (let ((n (length a0)))
	 (v*x! a1 -1.0d0 :end n)
	 (v+v! a1 a0 :end0 n :end1 n)))
    (2 (let ((m (array-dimension a0 0))
	     (n (array-dimension a0 1)))
	 (dotimes (i m)
	   (v*x! a1 -1.0d0 :vtype :row :end n :on i)
	   (v+v! a1 a0
		 :vtype0 :row :end0 n :on0 i
		 :vtype1 :row :end1 n :on1 i))))) 
  a1)

;;;=======================================================

(defun scale-array (x a0
		    &optional
		    (a1 (az:copy-array-contents a0) a1-supplied?))
  (az:type-check Double-Float x)
  (when (and a1-supplied? (not (eq a0 a1)))
    (assert (az:equal-array-dimensions? a0 a1))
    (az:copy-array-contents a0 a1))
  (scale-array! x a1))

(defun scale-array! (x a0)
  (let ((end (array-dimension a0 1)))
    (dotimes (i (array-dimension a0 0))
      (v*x! a0 x :end end :vtype :row :on i))))

;;;=======================================================

(defun 2d-array-sup-norm (a)
  (let ((maximum 0.0d0)
	(end (array-dimension a 1)))
    (dotimes (i (array-dimension a 0))
      (let ((row-max (v-abs-max a :end end :vtype :row :on i)))
	(when (> row-max maximum)
	  (setf maximum row-max))))
    maximum))
  
;;;=======================================================

(defun vector-zero! (v) (v<-x! v 0.0d0))

(defun vector-scale! (a v)
  (cond ((zerop a) (vector-zero! v))
	((= a 1.0d0) v)
	(t (v*x! v a))))

(defun vector-add-to! (v0 v1 result)
  (assert (= (length v0) (length v1) (length result)))
  (cond ((eq v0 result)  (v+v! result v1))
	((eq v1 result) (v+v! result v0))
	(t (v<-v! result v0)
	   (v+v! result v1))))

(defun vector-sub-to! (v0 v1 result) 
  (assert (= (length v0) (length v1) (length result)))
  (cond ((eq v0 result)
	 (if (eq v0 v1)
	     (vector-zero! v0)
	     (v-v! v0 v1)))
	((eq v1 result)
	 (vector-scale! -1.0d0 v1)
	 (v+v! v1 v0))
	(t
	 (v<-v! result v0)
	 (v-v! result v1))))

;;; optimize common special cases:
(defun vector-linear-mix (a0 v0 a1 v1 &key result)
  (az:type-check Double-Float a0 a1)
  (az:type-check az:Float-Vector v0 v1 result)
  (assert (= (length v0) (length v1) (length result)))
  (cond ((eq v0 result)
	 (%vector-linear-mix-left! a0 v0 a1 v1))
	((eq v1 result)
	 (%vector-linear-mix-right! a0 v0 a1 v1))
	(t
	 (%vector-linear-mix-to! a0 v0 a1 v1 result)))) 

(defun %vector-linear-mix-left! (a0 v0 a1 v1)
  (cond ((eq v0 v1)
	 (vector-scale! (+ a0 a1) v0))
	((zerop a1)
	 (if (zerop a0)
	     (vector-zero! v0)
	     (vector-scale! a0 v0)))
	((zerop a0)
	 (if (= 1.0d0 a1)
	     (v<-v! v0 v1)
	     (v<-v*x! v0 v1 a1)))
	((= 1.0d0 a0)
	 (cond ((= 1.0d0 a1)
		(v+v! v0 v1))
	       ((= -1.0d0 a1)
		(v-v! v0 v1))
	       (t
		(v+v*x! v0 v1 a1))))
	((= 1.0d0 a1)
	 (v*x! v0 a0)
	 (v+v! v0 v1))
	((= -1.0d0 a1)
	 (v*x! v0 a0)
	 (v-v! v0 v1))
	(t
	 (vector-scale! a0 v0)
	 (v+v*x! v0 v1 a1)))) 

(defun %vector-linear-mix-right! (a0 v0 a1 v1)
  (cond ((eq v1 v0)
	 (vector-scale! (+ a1 a0) v1))
	((zerop a0)
	 (if (zerop a1)
	     (vector-zero! v1)
	     (vector-scale! a1 v1)))
	((zerop a1)
	 (if (= 1.0d0 a0)
	     (v<-v! v1 v0)
	     (v<-v*x! v1 v0 a0)))
	((= 1.0d0 a1)
	 (cond ((= 1.0d0 a0)
		(v+v! v1 v0))
	       ((= -1.0d0 a0)
		(v-v! v1 v0))
	       (t
		(v+v*x! v1 v0 a0))))
	((= 1.0d0 a0)
	 (v*x! v1 a1)
	 (v+v! v1 v0))
	((= -1.0d0 a0)
	 (v*x! v1 a1)
	 (v-v! v1 v0))
	(t
	 (vector-scale! a1 v1)
	 (v+v*x! v1 v0 a0)))) 

(defun %vector-linear-mix-to! (a0 v0 a1 v1 result)
  (cond ((zerop a0)
	 (if(= 1.0d0 a1)
	    (v<-v! result v1)
	    (v<-v*x! result v1 a1)))
	((zerop a1)
	 (if (= 1.0d0 a0)
	     (v<-v! result v0)
	     (v<-v*x! result v0 a0)))
	((= 1.0d0 a0)
	 (v<-v! result v0)
	 (cond ((= 1.0d0 a1)
		(v+v! result v1))
	       ((= -1.0d0 a1)
		(v-v! result v1))
	       (t
		(v+v*x! result v1 a1))))
	((=  1.0d0 a1)
	 (v<-v*x! result v0 a0)
	 (v+v! result v1))
	((= -1.0d0 a1)
	 (v<-v*x! result v0 a0)
	 (v-v! result v1))
	(t
	 (v<-v*x! result v0 a0)
	 (v+v*x! result v1 a1))))

;;;=======================================================

(defun matrix-multiply (a0 a1
			&optional
			(a2 (az:make-float-array
			     (list (array-dimension a0 0)
				   (array-dimension a1 1)))))

  "Overwrites <a2> with the matrix product of a0 and a1."

  (declare (type az:Float-Matrix a0 a1 a2)
	   (:returns a2))

  (assert (= 2 (array-rank a0) (array-rank a1) (array-rank a2)))
  (assert (= (array-dimension a0 1) (array-dimension a1 0))) 
  (assert (= (array-dimension a0 0) (array-dimension a2 0)))
  (assert (= (array-dimension a1 1) (array-dimension a2 1)))
  (assert (not (or (eq a0 a2) (eq a1 a2))))
  
  (let ((n (array-dimension a2 0))
	(p (array-dimension a2 1))
	(m (array-dimension a1 0)))
    (dotimes (i n)
      (dotimes (j p)
	(setf (aref a2 i j)
	  (v.v a0 a1
	       :end0 m :vtype0 :row :on0 i
	       :end1 m :vtype1 :col :on1 j)))))
  a2)

(defun multiply-matrices (a0 a1)
  (ecase (array-rank a1)
         (1 (matrix*vector a0 a1))
         (2 (matrix-multiply a0 a1))))

;;;-------------------------------------------------------
;;;     T
;;;   a0  * a1

(defun matrix-inner-product (a0 a1
			     &optional
			     (a2 (az:make-float-matrix
				  (array-dimension a0 1)
				  (array-dimension a1 1))))

  "Overwrites <a2> with the matrix inner product of a0 and a1."

  (declare (type az:Float-Matrix a0 a1 a2)
	   (:returns a2))

  (assert (= 2 (array-rank a0) (array-rank a1) (array-rank a2)))
  (assert (= (array-dimension a0 0) (array-dimension a1 0))) 
  (assert (= (array-dimension a0 1) (array-dimension a2 0)))
  (assert (= (array-dimension a1 1) (array-dimension a2 1)))
  (assert (not (or (eq a0 a2) (eq a1 a2))))
  
  (let ((n (array-dimension a2 0))
	(p (array-dimension a2 1))
	(m (array-dimension a1 0)))
    (dotimes (i n)
      (dotimes (j p)
	(setf (aref a2 i j) (v.v a0 a1
				 :end0 m :vtype0 :col :on0 i
				 :end1 m :vtype1 :col :on1 j)))))
  a2)

;;;-------------------------------------------------------
;;;          T
;;;   a0 * a1

(defun matrix-outer-product (a0 a1
			     &optional
			     (a2 (az:make-float-matrix
				  (array-dimension a0 0)
				  (array-dimension a1 0))))
  
  "Overwrites <a2> with the matrix inner product of a0 and a1."
  
  (declare (type az:Float-Matrix a0 a1 a2)
	   (:returns a2))
  
  (assert (= 2 (array-rank a0) (array-rank a1) (array-rank a2)))
  (assert (= (array-dimension a0 1) (array-dimension a1 1))) 
  (assert (= (array-dimension a0 0) (array-dimension a2 0)))
  (assert (= (array-dimension a1 0) (array-dimension a2 1)))
  (assert (not (or (eq a0 a2) (eq a1 a2))))
  
  (let ((n (array-dimension a2 0))
	(p (array-dimension a2 1))
	(m (array-dimension a1 1)))
    (dotimes (i n)
      (dotimes (j p)
	(setf (aref a2 i j) (v.v a0 a1
				 :end0 m :vtype0 :row :on0 i
				 :end1 m :vtype1 :row :on1 j))))) 
  a2)

;;;=======================================================

(defun matrix*vector (a v0
		      &optional
		      (v1 (az:make-float-vector (array-dimension a 0))))
  
  "Overwrites <v1> with the matrix multiply of a and v0."
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector v0 v1)
	   (:returns v1))
  
  (assert (= 2 (array-rank a)))
  (assert (= 1 (array-rank v0) (array-rank v1)))
  (assert (= (array-dimension a 1) (length v0)))
  (assert (= (array-dimension a 0) (length v1)))
  (assert (not (eq v0 v1)))

  (let ((len (length v0)))
    (dotimes (i (length v1))
      (setf (aref v1 i) (v.v a v0
			      :end0 len :vtype0 :row :on0 i
			      :end1 len))))
  v1)

;;;-------------------------------------------------------
;;;    T
;;;   a * v

(defun matrix^t*vector (a v0
			&optional
			(v1 (az:make-float-vector (array-dimension a 0))))
  
  "Overwrites <v1> with the matrix multiply of a and v0."
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector v0 v1)
	   (:returns v1))
  
  (assert (= 2 (array-rank a)))
  (assert (= 1 (array-rank v0) (array-rank v1)))
  (assert (= (array-dimension a 0) (length v0)))
  (assert (= (array-dimension a 1) (length v1)))
  (assert (not (eq v0 v1)))

  (let ((len (length v0)))
    (dotimes (i (length v1))
      (setf (aref v1 i) (v.v a v0
			     :end0 len :vtype0 :col :on0 i
			     :end1 len))))
  v1)

;;;-------------------------------------------------------
;;; Overwrites a1 with a0 * (d) where d is a vector standing
;;; for the diagonal of a 2d array (ith col of a gets multiplied
;;; by di).

(defun matrix*diagonal (a0 d
			&optional
			(a1 (az:make-float-array (array-dimensions a0))))
  
  (declare (type az:Float-Matrix a0 a1)
	   (type az:Float-Vector d))
  
  (assert (= 2 (array-rank a0) (array-rank a1)))
  (assert (= 1 (array-rank d)))
  (assert (= (array-dimension a0 1) (length d)))

  (if (eq a0 a1)
      (matrix*diagonal! a0 d)
      ;; else
      (let ((m (array-dimension a0 0)))
	(dotimes (i (length d))
	  (v<-v*x! a1 a0 (aref d i)
		    :end0 m :vtype0 :col :on0 i
		    :end1 m :vtype1 :col :on1 i))))
  a1)

;;;-------------------------------------------------------
;;; Overwrites a1 with (d) * a0 where d is a vector standing
;;; for the diagonal of a 2d array (ith row of a gets multiplied
;;; by di).

(defun diagonal*matrix (d a0
			&optional
			(a1 (az:make-float-array (array-dimensions a0))))
    
  (declare (type az:Float-Vector d)
	   (type az:Float-Matrix a0 a1))

  (assert (= 1 (array-rank d)))
  (assert (= 2 (array-rank a0) (array-rank a1)))
  (assert (= (length d) (array-dimension a0 0)))

  (if (eq a0 a1)
      (diagonal*matrix! d a0)
      ;; else
      (let ((n (array-dimension a0 1)))
	(dotimes (i (length d))
	  (v<-v*x! a1 a0 (aref d i)
		    :end0 n :vtype0 :row :on0 i
		    :end1 n :vtype1 :row :on1 i))))
  a1)

;;;-------------------------------------------------------
;;; Overwrites a with a * (d) where d is a vector standing
;;; for the diagonal of a 2d array (ith col of a gets multiplied
;;; by di).

(defun matrix*diagonal! (a d)
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector d))

  (assert (= 2 (array-rank a)))
  (assert (= 1 (array-rank d)))
  (assert (= (array-dimension a 1) (length d)))

  (let ((m (array-dimension a 0)))
    (dotimes (i (length d)) (v*x! a (aref d i) :end m :vtype :col :on i)))
  a)

;;;-------------------------------------------------------
;;; Overwrites a with (d) * a where d is a vector standing
;;; for the diagonal of a 2d array (ith row of a gets multiplied
;;; by di).

(defun diagonal*matrix! (d a)
  
  (declare (type az:Float-Vector d)
	   (type az:Float-Matrix a))

  (assert (= 1 (array-rank d)))
  (assert (= 2 (array-rank a)))
  (assert (= (length d) (array-dimension a 0)))

  (let ((n (array-dimension a 1)))
    (dotimes (i (length d)) (v*x! a (aref d i) :end n :vtype :row :on i)))
  a)

;;;=======================================================

(declaim (Inline determinant-2x2 determinant-3x3))

(defun determinant-2x2 (a00 a01
			a10 a11)

  (az:type-check Double-Float a00 a01 a10 a11)

  (- (* a00 a11) (* a01 a10)))

(defun determinant-3x3 (a00 a01 a02
			a10 a11 a12
			a20 a21 a22)

  (az:type-check Double-Float a00 a01 a02  a10 a11 a12 a20 a21 a22)

  (+ (- (* a00 (- (* a11 a22) (* a21 a12)))
	(* a01 (- (* a10 a22) (* a20 a12))))
     (* a02 (- (* a10 a21) (* a20 a11)))))

;;;=======================================================

(defun svd-nash (x
		 &optional
		 (u (az:make-float-array (array-dimensions x)))
		 (s (az:make-float-vector (array-dimension x 1)))
		 (v (az:make-float-array (list (array-dimension x 1)
					       (array-dimension x 1)))))

  "Nash Svd code from Steve Peters & Catherine Hurley.
Singular-value decomposition by means of orthogonalization by plane
rotations.  Taken from Nash and Shlien: -Partial Svd Algorithms.-

<x> (array float (m n)) is the array to be decomposed.
The optional arguments may pre-allocate storage.
<u> (array float (m n)) is overwritten with the left singular vectors.
<s> (array float (n)) is overwritten with the singular values.
<v> (array float (n n)) is overwritten with the right singular vectors.

<u> may be <eq> to <x>, in which case <x> will be overwritten."

  (declare (type az:Float-Matrix x u v)
	   (type az:Float-Vector s)
	   (:returns (values (type az:Float-Matrix u)
			     (type az:Float-Vector s)
			     (type az:Float-Matrix v))))
  
  (assert (= 2 (array-rank x) (array-rank u) (array-rank v)))
  (assert (= 1 (array-rank s)))
  (assert (equal (array-dimensions x) (array-dimensions u)))
  (assert (= (array-dimension x 1)
	     (array-dimension v 0)
	     (array-dimension v 1)
	     (length s)))
  (assert (not (eq x v)))
  
  (let* ((m (array-dimension x 0))
	 (n (array-dimension x 1))
	 (slimit (max (truncate n 4) 6))
	 (nt n)
	 (rcount n))
    ;; initialize v to identity
    (dotimes (i (array-dimension v 0))
      (dotimes (j (array-dimension v 1))
	(setf (aref v i j) (if (= i j) 1.0d0 0.0d0))))
    (az:copy x :result u)
    
    (when (= n 1)
      (setf (aref s 0) (v-l2-norm u :end m :vtype :col))
      (unless (zerop (aref s 0))
	(v*x! u (/ 1.0d0 (aref s 0)) :end m :vtype :col))
      (return-from svd-nash (values u s v)))
    
    ;; The main loop: Repeatedly sweep over all pairs of columns in u,
    ;; rotating as needed, until no rotations in a complete sweep are
    ;; effective. Check the opportunity for rank reduction at the
    ;; conclusion of each sweep.
    
    (loop with eps^2 = (sq -small-Double-Float-)
	for scount upfrom 1
	while (> rcount 0)
	when (> scount slimit) do (error "Too many sweeps.")
	do
	  (setf rcount (truncate (* nt (- nt 1)) 2))
	  (loop for j from 0 to (- nt 2) do
		(loop with cc and ss and val
		    for k from (+ j 1) below nt
		    for p = (v.v u u
				 :end0 m :vtype0 :col :on0 j
				 :end1 m :vtype1 :col :on1 k)
		    for q = (v-l2-norm2 u :end m :vtype :col :on j)
		    for r = (v-l2-norm2 u :end m :vtype :col :on k)
		    do (assert (not (zerop r)))
		       (setf (aref s j) q)
		       (setf (aref s k) r)
		       (cond
			((< q r)
			 (setf q (- (/ q r) 1.0d0))
			 (setf p (/ p r))
			 (setf val (sqrt (+ (* 4.0 p p) (* q q))))
			 (assert (not (zerop val)))
			 (setf ss (sqrt (* 0.5 (- 1.0d0 (/ q val)))))
			 (if (< p 0.0d0) (setf ss (- ss)))
			 (assert (not (zerop ss)))
			 (setf cc (/ p (* val ss)))
			 (v-rot-v! u u cc ss
				   :end0 m :vtype0 :col :on0 j
				   :end1 m :vtype1 :col :on1 k)
			 (v-rot-v! v v cc ss
				   :end0 n :vtype0 :col :on0 j
				   :end1 n :vtype1 :col :on1 k))
			((or (<= (* q r) eps^2)
			     (<= (* (/ p q) (/ p r)) -small-Double-Float-))
			 (decf rcount))
			(t
			 (assert (not (zerop q)))
			 (setf r (- 1.0d0 (/ r q)))
			 (setf p (/ p q))
			 (setf val (sqrt (+ (* 4.0 p p) (* r r))))
			 (assert (not (zerop val)))
			 (setf cc (sqrt (* 0.5 (+ 1.0d0 (/ r val)))))
			 (assert (not (zerop cc)))
			 (setf ss (/ p (* val cc)))
			 (v-rot-v! u u cc ss
				   :end0 m :vtype0 :col :on0 j
				   :end1 m :vtype1 :col :on1 k)
			 (v-rot-v! v v cc ss
				   :end0 n :vtype0 :col :on0 j
				   :end1 n :vtype1 :col :on1 k)))))	    
	  (loop while (and (>= nt 3)
			   (<= (/ (aref s (- nt 1))
				  (+ (aref s 0) -small-Double-Float-))
			       -small-Double-Float-))
	      do (decf nt)))       
    ;; finish the decomposition by returning all n singular values, 
    ;; and by normalizing those columns of u judged to be non-zero.
    (dotimes (j n)
      (let ((q (sqrt (aref s j))))
	(setf (aref s j) q)
	(if (<= j nt)
	    (assert (not (zerop q)))
	  (v*x! u (/ q) :end m :vtype :col :on j)))))
  (values u s v))

;;;-------------------------------------------------------
;;; Back substitution:
;;; Treats $A$ as upper triangular.
;;; Computes $A^{-1}v_d$ and puts the result in $v_r$.

(defun back-substitution (a vd
			  &optional
			  (vr (az:make-float-vector (array-dimension a 1))))

  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 0) (length vd)))
  (assert (= (array-dimension a 1) (length vr)))
  (assert (not (eq vd vr)))

  (let ((m (array-dimension a 0))
	(n (array-dimension a 1)))
    (do ((i+1 (min m n) (- i+1 1)))
	((<= i+1 0))
      (let* ((i (- i+1 1))
	     (sum (aref vd i)))
	(do ((j (- m 1) (- j 1)))
	    ((< j i+1))
	  (decf sum (* (aref a i j) (aref vr j))))
	(assert (not (zerop (aref a i i))))
	(setf (aref vr i) (/ sum (aref a i i))))))
  vr) 

;;;-------------------------------------------------------
;;; Transposed Back substitution:
;;; Treats $A$ as lower triangular.
;;; Computes $(A^{\dagger})^{-1} v_d$ and puts the result in $v_r$.

(defun transposed-back-substitution (a vd
				     &optional
				     (vr (az:make-float-vector
					  (array-dimension a 0))))
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 1) (length vd)))
  (assert (= (array-dimension a 0) (length vr)))
  (assert (not (eq vd vr)))

  (let ((m (array-dimension a 1))
	(n (array-dimension a 0)))
    (do ((i+1 (min m n) (- i+1 1)))
	((<= i+1 0))
      (let* ((i (- i+1 1))
	     (sum (aref vd i)))
	(do ((j (- m 1) (- j 1)))
	    ((< j i+1))
	  (decf sum (* (aref a j i) (aref vr j))))
	(assert (not (zerop (aref a i i))))
	(setf (aref vr i) (/ sum (aref a i i))))))
  vr) 

;;;-------------------------------------------------------
;;; Forward elimination:
;;; Treats $A$ as though it was lower triangular.
;;; Computes $A^{-1}v_d$ and puts the result in $v_r$.

(defun forward-elimination (a vd
			    &optional
			    (vr (az:make-float-vector (array-dimension a 1))))
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 0) (length vd)))
  (assert (= (array-dimension a 1) (length vr)))
  (assert (not (eq vd vr)))

  (dotimes (i (min (array-dimension a 0) (array-dimension a 1)))
    (assert (not (zerop (aref a i i))))
    (setf (aref vr i)
      (/ (- (aref vd i) (v.v a vr
			     :end0 i :vtype0 :row :on0 i 
			     :end1 i))
	 (aref a i i))))
  vr) 

;;;-------------------------------------------------------
;;; Transposed Forward elimination:
;;; Treats $A$ as upper triangular.
;;; Computes $(A^{\dagger})^{-1} v_d$ and puts the result in $v_r$.

(defun transposed-forward-elimination (a vd
				       &optional
				       (vr (az:make-float-vector
					    (array-dimension a 0))))
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 1) (length vd)))
  (assert (= (array-dimension a 0) (length vr)))
  (assert (not (eq vd vr)))

  (dotimes (i (min (array-dimension a 0) (array-dimension a 1)))
    (assert (not (zerop (aref a i i))))
    (setf (aref vr i)
      (/ (- (aref vd i) (v.v a vr
			     :end0 i :vtype0 :col :on0 i
			     :end1 i))
	 (aref a i i))))
  vr) 

;;;=======================================================
;;; Dennis and Schnabel A3.3.1
;;;
;;; Compute a lower bound on the $L_1$ condition of the tranformation
;;; represented by the upper triangle of $A$.

(defun estimate-condition-of-upper-triangle (a)
  (az:type-check az:Float-Matrix a)
  (dotimes (i (min (array-dimension a 0) (array-dimension a 1))))
  (assert (not (diagonal-zeros? a)))
  (let ((n (min (array-dimension a 0) (array-dimension a 1)))
	(condition 0.0d0)
	(x0 (/ 1.0d0 (aref a 0 0))))
    ;; initialize <condition> to max l1 norm of columns
    (dotimes (j n) (az:maxf condition
			 (v-l1-norm a :end (+ j 1) :vtype :col :on j)))
    ;; implicitly compute $x = (A^{\dagger})^{-1} e$,
    ;; where $e_i = \pm 1$ is chosen as we go.
    (az:with-borrowed-float-vectors ((x p pm) n)
      (setf (aref x 0) x0)
      (v<-v*x! p a x0
		:start0 1 :end0 n
		:start1 1 :end1 n :vtype1 :row)
      (do ((j 1 (+ j 1)))
	  ((>= j n))
	(let* ((ajj (aref a j j))
	       (pj (aref p j))
	       (xp (/ (- 1.0d0 pj) ajj))
	       (xm (/ (- -1.0d0 pj) ajj))
	       (temp (abs xp))
	       (tempm (abs xm)))
	  (do ((i (+ j 1) (+ i 1)))
	      ((>= i n))
	    (setf (aref pm i) (+ (aref p i) (* xm (aref a j i))))
	    (incf tempm (abs (/ (aref pm i) (aref a i i))))
	    (incf (aref p i) (* xp (aref a j i)))
	    (incf temp (abs (/ (aref p i) (aref a i i)))))
	  (cond ((>= temp tempm) ;; $e_j = 1$
		 (setf (aref x j) xp))
		(t ;; $e_j = -1$
		 (setf (aref x j) xm)
		 (v<-v! p pm
			 :start0 (+ j 1) :end0 n
			 :start1 (+ j 1) :end1 n)))))
      (az:divf condition (v-l1-norm x))
      (az:mulf condition (v-l1-norm (back-substitution a x p))))
    condition))

;;;-------------------------------------------------------
;;; modified from Dennis and Schnabel A3.3.1
;;;
;;; Compute a lower bound on the $L_1$ condition of the tranformation
;;; represented by the lower triangle of $M$. This was created
;;; by transposing all references to $M$ in the preceeding function.

(defun estimate-condition-of-lower-triangle (a)
  (az:type-check az:Float-Matrix a)
  (assert (not (diagonal-zeros? a)))
  (let ((n (min (array-dimension a 0) (array-dimension a 1)))
	(condition 0.0d0)
	(x0 (/ 1.0d0 (aref a 0 0))))
    ;; initialize <condition> to max l1 norm of rows
    (dotimes (j n) (az:maxf condition
			 (v-l1-norm a :end (+ j 1) :vtype :row :on j)))
    ;; implicitly compute $x = (A^{\dagger})^{-1} e$,
    ;; where $e_i = \pm 1$ is chosen as we go.
    (az:with-borrowed-float-vectors ((x p pm) n)
      (setf (aref x 0) x0)
      (v<-v*x! p a x0
		:start0 1 :end0 n
		:start1 1 :end1 n :vtype1 :col :on1 0)
      (do ((j 1 (+ j 1)))
	  ((>= j n))
	(let* ((ajj (aref a j j))
	       (pj (aref p j))
	       (xp (/ (- 1.0d0 pj) ajj))
	       (xm (/ (- -1.0d0 pj) ajj))
	       (temp (abs xp))
	       (tempm (abs xm)))
	  (do ((i (+ j 1) (+ i 1)))
	      ((>= i n))
	    (setf (aref pm i) (+ (aref p i) (* xm (aref a i j))))
	    (incf tempm (abs (/ (aref pm i) (aref a i i))))
	    (incf (aref p i) (* xp (aref a i j)))
	    (incf temp (abs (/ (aref p i) (aref a i i)))))
	  (cond ((>= temp tempm) ;; $e_j = 1$
		 (setf (aref x j) xp))
		(t ;; $e_j = -1$
		 (setf (aref x j) xm)
		 (v<-v! p pm
			 :start0 (+ j 1) :end0 n
			 :start1 (+ j 1) :end1 n)))))
      (az:divf condition (v-l1-norm x))
      (az:mulf condition (v-l1-norm (transposed-back-substitution a x p))))
    condition))

;;;=======================================================
;;; Golub & VanLoan Alg 5.2-1
;;;
;;; Given $A$ square, symmetric, and positive definite, computes lower
;;; triangular $L$ such that $A = LL^{\dagger}$, overwriting the
;;; optional argument, if provided.

(defun cholesky (a
		 &optional
		 (l (az:copy a) l-supplied?))

  (when (and l-supplied? (not (eq a l)))
    (assert (az:equal-array-dimensions? a l))
    (az:copy a :result l))

  (cholesky! l)) 

;;; Given $A$ square, symmetric, and positive definite, computes lower
;;; triangular $L$ such that $A = LL^{\dagger}$, overwriting $A$
;;; with $L$.

(defun cholesky! (a)
  (az:type-check az:Float-Matrix a)
  (assert (= (array-dimension a 0) (array-dimension a 1)))
  (let ((n (array-dimension a 0)))
    (declare (type az:Array-Index n))
    (dotimes (k n)
      (declare (type az:Array-Index k))
      (setf (aref a k k) (sqrt (- (aref a k k)
				  (v-l2-norm2 a :end k :vtype :row :on k))))
      (do ((i (+ k 1) (+ i 1)))
	  ((>= i n))
	(declare (type az:Array-Index i))
	(setf (aref a i k)
	      (/ (- (aref a i k) (v.v a a
				       :end0 k :vtype0 :row :on0 i
				       :end1 k :vtype1 :row :on1 k))
		 (aref a k k)))))
    (dotimes (i (- n 1))
      (declare (type az:Array-Index i))
      (v<-x! a 0.0d0 :start (+ i 1) :end n :vtype :row :on i)))
  a)

;;;-------------------------------------------------------
;;; Based on Golub & VanLoan Alg 5.2-1
;;; 
;;; Given 2-dimensional $A$ is square, symmetric, and positive definite.
;;; Computes unit lower triangular 2-dimensional $L$ and 1-dimensional
;;; $D$ such that $A = LDL^{\dagger}$, overwriting the optional
;;; arguments, if provided. (This saves $n$ square roots over {\tt
;;; cholesky}. See Gill-Murray-Wright Sec 2.2.5.2; Lawson-Hanson
;;; Exercise 19.40)

(defun cholesky-ldlt (a
		      &optional
		      (l (az:copy a) l-supplied?)
		      (d (az:make-float-vector (array-dimension a 0))))

  (when (and l-supplied? (not (eq a l)))
    (assert (az:equal-array-dimensions? a l))
    (az:copy a :result l))

  (cholesky-ldlt! l d))

;;; Given 2-dimensional $A$ is square, symmetric, and positive definite.
;;; Computes unit lower triangular 2-dimensional $L$ and 1-dimensional
;;; $D$ such that $A = LDL^{\dagger}$, overwriting $A$ with $L$.  A
;;; 1-dimensional array to hold $D$ is required.  (This saves $n$ square
;;; roots over {\tt cholesky}. See Gill-Murray-Wright Sec 2.2.5.2;
;;; Lawson-Hanson Exercise 19.40)

(defun cholesky-ldlt! (a d)
  (az:type-check az:Float-Matrix a)
  (assert (= (array-dimension a 0) (array-dimension a 1) (length d)))
  (let ((n (length d))
	(dk 0.0d0)
	(aik 0.0d0))
    (declare (type az:Array-Index n)
	     (type Double-Float dk aik))
    (dotimes (k n)
      (declare (type az:Array-Index k))
      (setf dk (aref a k k))
      (dotimes (i k)
	(declare (type az:Array-Index i))
	(decf dk (* (aref d i) (sq (aref a k i)))))
      (setf (aref d k) dk)
      (setf (aref a k k) 1.0d0)
      (do ((i (+ k 1) (+ i 1)))
	  ((>= i n))
	(declare (type az:Array-Index i))
	(setf aik (aref a i k))
	(dotimes (j k)
	  (declare (type az:Array-Index j))
	  (decf aik (* (aref d j) (aref a i j) (aref a k j))))
	(assert (not (zerop dk)))
	(setf (aref a i k) (/ aik dk))))
    ;; Enforce lower triangularity---not strictly necessary, but safe.
    (dotimes (i (- n 1))
      (declare (type az:Array-Index i))
      (v<-x! a 0.0d0 :start (+ i 1) :end n :vtype :row :on i)))
  (values a d))

;;;-------------------------------------------------------
;;; 2-dimensional, square, lower triangular $L_0$ and
;;; 1-dimensional $D_0$ are taken to be the factors of an
;;; $LDL_{\dagger}$ Cholesky decomposition. $v$ is a vector
;;; representing the elementary tensor product $v \otimes v$.
;;; This function the $LDL_{\dagger}$ decomposition of
;;; $(L_0 D_0 L_0^{\dagger}) + (v0 \otimes v0)$,
;;; using the optional arguments $L_1$ and $D_1$ to hold the result,
;;; and using $v_1$ as temporary storage.

(defun update-cholesky-ldlt (l0 d0 v0
			     &optional
			     (l1 (az:copy l0) l1-supplied?)
			     (d1 (az:copy d0) d1-supplied?)
			     (v1 (az:copy v0) v1-supplied?))

  (az:type-check az:Float-Matrix l0 l1)
  (az:type-check az:Float-Vector d0 v0 d1 v1)
  (assert (= (array-dimension l0 0)
	     (array-dimension l0 1)
	     (length d0)
	     (length v0)))

  (when (and l1-supplied? (not (eq l0 l1)))
    (assert (az:equal-array-dimensions? l0 l1))
    (az:copy l0 :result l1))

  (when (and d1-supplied? (not (eq d0 d1)))
    (assert (= (length d0) (length d1)))
    (v<-v! d1 d0))

  (when (and v1-supplied? (not (eq v0 v1)))
    (assert (= (length v0) (length v1)))
    (v<-v! v1 v0)) 

  (update-cholesky-ldlt! l1 d1 v1))

;;; 2-dimensional, square, lower triangular $L$ and 1-dimensional $D$
;;; are taken to be the factors of an $LDL_{\dagger}$ Cholesky
;;; decomposition. $v$ is a vector representing the elementary tensor
;;; product $v \otimes v$.  This function returns $L$ and $D$,
;;; overwritten with the $LDL_{\dagger}$ decomposition of
;;; $(LDL^{\dagger}) + (v \otimes v)$. It destroys the contents of $v$.
;;; See Gill-Murray-Wright p 42.

(defun update-cholesky-ldlt! (l d v)

  (az:type-check az:Float-Matrix l)
  (az:type-check az:Float-Vector d v)
  (assert (notany #'zerop d))
  (assert (= (array-dimension l 0)
	     (array-dimension l 1)
	     (length d)
	     (length v)))

  (let ((n (length d))
	(tj-1 1.0d0))	
    (declare (type az:Array-Index n)
	     (type Double-Float tj-1))
    (dotimes (j n)
      (declare (type az:Array-Index j))
      (let* ((dj (aref d j))
	     (vj (aref v j))
	     (tj (+ tj-1 (/ (sq vj) dj)))
	     (b (progn (assert (not (zerop tj)))
		       (/ vj (* dj tj)))))
	(assert (not (zerop tj-1)))
	(setf (aref d j) (/ (* dj tj) tj-1))
	(do ((k (+ j 1) (+ k 1)))
	    ((>= k n))
	  (declare (type az:Array-Index k))
	  (let ((v+1 (- (aref v k) (* vj (aref l k j)))))
	    (declare (type Double-Float v+1))
	    (setf (aref v k) v+1)
	    (incf (aref l k j) (* b v+1))))
	(setf tj-1 tj))))
  (values l d)) 

;;;-------------------------------------------------------
;;; 2-dimensional, square, lower triangular $L_0$ and
;;; 1-dimensional $D_0$ are taken to be the factors of an
;;; $LDL_{\dagger}$ Cholesky decomposition. $v$ is a vector
;;; representing the elementary tensor product $v \otimes v$.
;;; This function computes the $LDL_{\dagger}$ decomposition of
;;; $(L_0 D_0 L_0^{\dagger}) - (v0 \otimes v0)$,
;;; using the optional arguments $L_1$ and $D_1$ to hold the result,
;;; and using $v_1$ as temporary storage.
;;; 
;;; Note: this is trickier than updating, because there's no guarantee,
;;; in general, that the downdated matrix is still positive definite and
;;; that the $LDL_{\dagger}$ decomposition still exists.

(defun downdate-cholesky-ldlt (l0 d0 v0
			       &optional
			       (l1 (az:copy l0) l1-supplied?)
			       (d1 (az:copy d0) d1-supplied?)
			       (v1 (az:copy v0) v1-supplied?))

  (az:type-check az:Float-Matrix l0 l1)
  (az:type-check az:Float-Vector d0 v0 d1 v1)
  (assert (= (array-dimension l0 0)
	     (array-dimension l0 1)
	     (length d0)
	     (length v0)))
  
  (when (and l1-supplied? (not (eq l0 l1)))
    (assert (az:equal-array-dimensions? l0 l1))
    (az:copy l0 :result l1))

  (when (and d1-supplied? (not (eq d0 d1)))
    (assert (= (length d0) (length d1)))
    (v<-v! d1 d0))

  (when (and v1-supplied? (not (eq v0 v1)))
    (assert (= (length v0) (length v1)))
    (v<-v! v1 v0)) 

  (downdate-cholesky-ldlt! l1 d1 v1))

;;; 2-dimensional, square, lower triangular $L$ and 1-dimensional $D$
;;; are taken to be the factors of an $LDL_{\dagger}$ Cholesky
;;; decomposition. $v$ is a vector representing the elementary tensor
;;; product $v \otimes v$.  This function returns $L$ and $D$,
;;; overwritten with the $LDL_{\dagger}$ decomposition of
;;; $(LDL^{\dagger}) - (v \otimes v)$. It destroys the contents of $v$.
;;; See Gill-Murray-Wright p 43.
;;; 
;;; Note: this is trickier than updating, because there's no guarantee,
;;; in general, that the downdated matrix is still positive definite and
;;; that the $LDL_{\dagger}$ decomposition still exists.


(defun downdate-cholesky-ldlt! (l d v)
  (az:type-check az:Float-Matrix l)
  (az:type-check az:Float-Vector d v)
  (assert (= (array-dimension l 0)
	     (array-dimension l 1)
	     (length d)
	     (length v)))
  (warn "<downdate-cholesky-ldlt!> not implemented yet")
  (values l d)) 
