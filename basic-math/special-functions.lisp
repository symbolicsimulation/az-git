;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the; software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;================================================================

(in-package :Basic-Math)

;;;================================================================
;;; Special Functions
;;;================================================================
;;; based on Abramowitz and Stegun 26.2.17

(defun density-standard-gaussian (x)
  (declare (type Double-Float x)
	   (:returns (values (type Double-Float probability))))
  (/ (exp (- (* 0.5 x x))) -sqrt2pi-))
  
;;;----------------------------------------------------------------
;;; Reference?

;(defconstant -standard-gaussian-percentile-coefficients- ;; $$$$ -jm
(defparameter -standard-gaussian-percentile-coefficients-
    (vector 
     1.330274429d0
     -1.821255978d0
     1.781477937d0
     -0.356563782d0
     0.31938153d0
     0.0d0))

(defun percentile-standard-gaussian (x)
  (declare (type Double-Float x)
	   (:returns (type (Double-Float 0.0d0 1.0d0)
probability)))
  (flet ((phi/2 (x)
	   (assert (plusp x))
	   (when (integerp x) (setf x (az:fl x)))
	   (float ;; make sure the answer is the same precision as <x>
	    (if (< x 5.5d0)
		(- 1.0d0 (* (density-standard-gaussian x)
			  (evaluate-polynomial
			   -standard-gaussian-percentile-coefficients-
			   (/ 1.0d0 (+ 1.0d0 (* 0.2316419d0 x))))))
	      1.0d0)
	    x)))
    (cond ((> x 0.0d0) (phi/2 x))
	  ((= x 0.0d0) 0.5d0)
	  ((< x 0.0d0) (- 1.0d0 (phi/2 (- x)))))))

;;;=======================================================================
;;; log gamma function
;;; see Numerical Recipes, ch 6.1, p. 157, FUNCTION GAMMLN.
;;; What about complex x?

;(defconstant -log-gamma-coeficients- ;; $$$$ -jm
(defparameter -log-gamma-coeficients-
    (vector
     76.18009173d0
     -86.50532033d0
     24.01409822d0
     -1.231739516d0
     0.120858003d-2
     -0.536382d-5))

(declaim (ftype (Function (Double-Float) Double-Float) log-gamma))

(defun log-gamma (x)
  (declare (type Double-Float x))
  
  (flet ((%log-gamma (x) ;; good for x >= 1
	   (assert (>= x 1.0d0))
	   (loop for j from 0 to 5
	       for y from x by 1.0d0
	       sum (/ (aref -log-gamma-coeficients- j) y) into total
	       finally
		 (return
		   (let* ((x+4.5 (+ x 4.5d0))
			  (temp (- (* (- x 0.5d0) (log x+4.5)) x+4.5)))
		     (+ temp (log (* -sqrt2pi- (+ 1.0d0 total)))))))))
    (cond ((= x 0.0d0) 0.0d0)
	  ((>= x 1.0d0) (%log-gamma x))
	  (t (- (%log-gamma (+ 1.0d0 x)) (log x))))))

;;;----------------------------------------------------------------
;;; beta function
;;; see Numerical Recipes, ch 6.1, p. 159, FUNCTION BETA.

(declaim (ftype (Function (Double-Float Double-Float) Double-Float)
		log-beta beta))

(defun log-beta (a b)
  (declare (type Double-Float a b))
  (- (+ (log-gamma a) (log-gamma b))
     (log-gamma (+ a b))))

(defun beta (a b)
  (declare (type Double-Float a b))
  (exp (log-beta a b)))

;;;================================================================
;;; incomplete gamma function.
;;; Numerical Recipes, 6.2, FUNCTION GAMMP

(defun percentile-gamma (x a
			 &key
			 (max-iterations *max-iterations*))
  (declare (type Double-Float x a)
	   (:returns (type (Double-Float 0.0d0 1.0d0)
probability)))
  (assert (and (>= x 0.0d0) (> a 0.0d0)) (x a) "Invalid Arguments")
  
  (flet ((series-approximation (x a)
	   ;; Numerical Recipes, 6.2, FUNCTION GSER
	   (assert (plusp a))
	   (print "series approx.")
	   (loop for n from 0 to max-iterations
	       for ap from a by 1.0d0
	       for delta = (/ a) then (/ (* delta x) ap)
	       sum delta into total
	       do
		 (when (< (abs delta) (* (abs total) -small-double-float-))
		   (return total))
	       finally (error "No convergence by the ~:r iteration." n)))
	 ;; - - - - - - - - - - - - - - - - -
	 (continued-fraction-approximation (x a)
	   ;; Numerical Recipes, 6.2, FUNCTION GCF
	   (assert (plusp a))
	   (print "continued-fraction approx.")
	   (continued-fraction 
	    #'(lambda (n) (if (evenp n)
			      (- (truncate n 2) a)
			    (if (= n 1) 1.0d0 (* 0.5d0 (- n 1)))))
	    #'(lambda (n) (if (evenp n)
			      (if (zerop n) 0.0d0 1.0d0)
			    x)))))
    ;; - - - - - - - - - - - - - - - - -
    ;; check for domain of convergence and use appropriate expansion.
    (let ((factor (exp (- (* a (log x)) x (float (log-gamma a) x)))))
      (cond
       ((< x 0.0d0) (error "x must be >= 0.0d0"))
       ((= x 0.0d0) 0.0d0)
       ((< x (+ a 1)) (* factor (series-approximation x a)))
       (t (- 1.0d0 (* factor (continued-fraction-approximation x a))))))))

;;;================================================================
;;; percentile beta function
;;;================================================================
;;; based on the Fortran function:
;;;
;;;      function betai (x, pin, qin)
;;;c  april 1977 version.  w. fullerton, c3, los alamos scientific lab.
;;;c  based on bosten and battiste, remark on algorithm 179, comm. acm,
;;;c  v 17, p 153, (1974).
;;;c 
;;;c  x   value to which function is to be integrated. x must be in (0,1).
;;;c  p   input (1st) parameter (must be greater than 0)
;;;c  q   input (2nd) parameter (must be greater than 0)
;;;c  betai  incomplete beta function ratio, the probability that a random
;;;c         variable from a beta distribution having parameters p and q
;;;c         will be less than or equal to x.
;;;
;;; from Entropy.ms.washington.edu:/usr/src/local/lib/cmlib/fnlib/betai.f
;;;
;;; see also:
;;; 
;;; Ludwig, O.G.,
;;; "Algorithm 179: Incomplete Beta Ratio,"
;;; CACM 6 (6) 314, June 1963.
;;;
;;; Pike, M.C. and Hill, I.D.,
;;; "Remark on Algorithm 179: Incomplete Beta Ratio,"
;;; CACM 10 (6) 375-376, June, 1967.
;;;
;;; Bosten, N.E. and Battiste, E.L.,
;;; "Remark on Algorithm 179: Incomplete Beta Ratio,"
;;; CACM 17 (3) 156-157, March 1974.

(defun percentile-beta (x a b)
  
  (declare (type Double-Float x a b)
	   (:returns (type Double-Float a-probability)))
  
  (assert (<= 0.0d0 x 1.0d0))
  (assert (and (> a 0.0d0) (> b 0.0d0)))
  
  (labels
      ((reflect? (x a b) (or (< x 0.2d0) (and (<= b a) (< x 0.8d0))))
       ;; - - - - - - - - - -
       (%betai (x a b)
	 (declare (type Double-Float x a b))
	 (az:bound
	  0.0d0
	  (if (< (* (+ a b) (/ x (+ a 1.0d0))) -smallest-relative-spacing-)
	      (let* ((xm (max x -smallest-positive-magnitude-))
		     (xb (- (* a (log xm)) (log a) (log-beta a b))))
		(if (and (> xb -log-smallest-positive-magnitude-) (/= x 0.0d0))
		    (exp xb)
		  0.0d0))
	    ;; else
	    (+ (infinite-sum x a b) (finite-sum x a b)))
	  1.0d0))
       ;; - - - - - - - - - -
       (infinite-sum (x a b)
	 (declare (type Double-Float x a b))
	 (let* ((as1 (- b (ftruncate b)))
		(as (if (= as1 0.0d0) 1.0d0 as1))
		(xb (- (* a (log x)) (log-beta as a) (log a))))
	   (cond
	    ((< xb -log-smallest-positive-magnitude-) 0.0d0)
	    ((= as 1.0d0) (exp xb))
	    (t 
	     (loop with sum = (exp xb)
		 with term = (* sum a)
		 for i from 1
		 to (max 4 (truncate -log-smallest-relative-spacing- (log x)))
		    ;; term will equal x^a/beta(as,a) * (1-as)i * x^i / fac(i)
		 do (setf term (* (* term (- i as)) (/ x i)))
		    (incf sum (/ term (+ a i)))
		 finally (return sum))))))
       ;; - - - - - - - - - -
       (finite-sum (x a b)
	 (declare (type Double-Float x a b))
	 (cond
	  ((<= b 1.0d0) 0.0d0)
	  (t
	   (assert (/= x 1.0d0))
	   (loop
	       with xb = (+ (* a (log x))
			    (- (* b (log (- 1.0d0 x))) (log-beta a b)
			       (log b)))
	       with ib =
		 (max 0 (truncate xb -log-smallest-positive-magnitude-))
	       with term =
		 (exp (- xb (* ib -log-smallest-positive-magnitude-)))
	       with 1/1-x = (/ 1.0d0 (- 1.0d0 x))
	       with a1 = (* b (/ 1/1-x (+ a (- b 1.0d0))))
	       with sum = 0.0d0
	       with n = (truncate b)
	       for i from 1 to (if (= b n) (decf n) n)
	       while (or (> a1 1.0d0)
			 (<= sum (/ term -smallest-relative-spacing-)))
	       do (setf term (/ (* (- b i -1.0d0) 1/1-x term)
				(+ a (- b i))))
	       when (> term 1.0d0)
	       do (decf ib)
		  (setf term (* term -smallest-positive-magnitude-))
	       when (= ib 0) do (incf sum term)
	       finally (return sum))))))
    ;; - - - - - - - - - -
    (cond
     ((= x 0.0d0) 0.0d0)
     ((= x 1.0d0) 1.0d0)
     ((reflect? x a b) (- 1.0d0 (%betai (- 1.0d0 x) b a)))
     (t (%betai x a b)))))

;;;----------------------------------------------------------------
;;; Numerical Recipes, 6.3, FUNCTION BETAI
;;; this under/over flows for fairly small values of a and b,
;;; so we prefer the more complicated version above.

(defun numerical-recipes-percentile-beta (x a b)
  (declare (type Double-Float x a b)
	   (:returns (type (Double-Float 0.0d0 1.0d0)
probability)))
  (assert (<= 0.0d0 x 1.0d0) (x) "x=~a must be in [0,1]." x) 

  (flet ((continued-fraction-approximation (x a b)
	   (continued-fraction
	     #'(lambda (n)
		 (if (= 1 n)
		     (/ (* (expt x a) (expt (- 1.0d0 x) b))
			a
			(float (beta a b) x))
		     ;; else
		     (let* ((n-1 (- n 1))
			    (a+n-1 (+ a n-1))
			    (n-1/2 (truncate n-1 2))
			    (a+n-1/2 (+ a n-1/2))
			    (y (if (evenp n)
				   (- (* a+n-1/2 (+ a+n-1/2 b)))
				   (* n-1/2 (- b n-1/2)))))
		       (/ (* x y) (- a+n-1 1) a+n-1))))
	     #'(lambda (n) (if (= 0 n) 0.0d0 1.0d0)))))
    
    ;; Check for domain of convergence; use symmetry relation if necessary.
    (cond
      ((= x 0.0d0) 0.0d0)
      ((= x 1.0d0) 1.0d0)
      ((< x (/ (az:fl (+ a 1)) (+ a b 1)))
       (continued-fraction-approximation x a b))
      (t
       (- 1.0d0 (continued-fraction-approximation (- 1.0d0 x) b a))))))

;;;================================================================
;;; Numerical Recipes, 6.3, equation (6.3.11)

(defun percentile-F (x nu1 nu2)
  (declare (type Double-Float x nu1 nu2))
  (assert (plusp x))
  (assert (plusp nu1))
  (assert (plusp nu2))
  (- 1.0d0 (percentile-beta (/ nu2 (+ nu2 (* nu1 x)))
			  (* 0.5 nu2)
			  (* 0.5 nu1))))

;;;================================================================

(defun exact-factorial (n)
  (declare (type Integer n)
	   (:returns (type Integer n-factorial)))
  (if (or (= 0 n) (= 1 n))
      1
      ;; don't use recursion because we can run out of stack space
      ;; before the answer gets too large for bignums
      (loop for i downfrom (- n 1) to 2
	    for n! = (* i n) then (* i n!)
	    finally (return n!))))

;;;----------------------------------------------------------------
;;; this cuteness is an attempt to be portable to machines with
;;; fixnums that are not 32bits.

(defparameter *maximum-fixnum-factorial*
    (labels ((f (n) (if (= 0 n) 1 (* n (f (- n 1))))))
      (loop for i from 1 while (typep (f i) 'fixnum)
	  finally (return (- i 1)))))

;;;----------------------------------------------------------------

(defun exact-number-of-choices (n k)
  (declare (type Integer n k))
  (assert (<= 0 k n))
  
  (truncate (exact-factorial n)
	    (* (exact-factorial k) (exact-factorial (- n k)))))

;;;----------------------------------------------------------------
;;; a slow "exact" version for testing the faster (we hope) approximation

(defun exact-percentile-binomial (x n p)
  
  (declare (type Double-Float x)
	   (type Integer n)
	   (type Double-Float p)
	   (:returns (type Double-Float a-probability)))
  
  (assert (<= 0.0d0 p 1.0d0))
  (assert (<= 0.0d0 x n))

  (az:bound
   0.0d0
   (loop
       with rat-p = (rationalize p)
       for k from 0 to (truncate x)
       for log-magnitude = (+ (log-number-of-choices n k)
			      (* k (log p))
			      (* (- n k) (log (- 1.0d0 p))))
       when (or (>= log-magnitude -log-largest-magnitude-)
		(<= log-magnitude -log-smallest-positive-magnitude-))
       do (setf rat-p (float rat-p 1.0d0))
       sum (* (exact-number-of-choices n k)
	      (expt rat-p k)
	      (expt (- 1.0d0 rat-p) (- n k))))
   1.0d0))

;;;----------------------------------------------------------------

(defun number-of-choices (n k)
  (declare (type Integer n k)
	   (:returns (type Integer n-choose-k)))
  (when (> k n) (error "<n> (~a) must be < <k> (~a)." n k))
  
  (if (<= n *maximum-fixnum-factorial*)
      (exact-number-of-choices n k)
    ;; else
    (truncate
     (exp (- (log-gamma (az:fl (+ 1 n)))
	     (log-gamma (az:fl (+ 1 k)))
	     (log-gamma (az:fl (+ 1 (- n k)))))))))

;;;----------------------------------------------------------------

(defun log-number-of-choices (n k)
  (declare (type Integer n k)
	   (:returns (type Double-Float log-n-choose-k)))
  (when (> k n) (error "<n> (~a) must be < <k> (~a)." n k))
  
  (if (<= n *maximum-fixnum-factorial*)
      (log (az:fl (exact-number-of-choices n k)))
    ;; else
    (- (log-gamma (az:fl (+ 1 n)))
       (log-gamma (az:fl (+ 1 k)))
       (log-gamma (az:fl (+ 1 (- n k)))))))

;;;================================================================
;;; Numerical Recipes, 6.3, equation (6.3.12)

(defun percentile-binomial (x n p)
  (declare (type Double-Float x)
	   (type Integer n)
	   (type Double-Float p)
	   (:returns (type Double-Float probability)))
  (assert (<= 0.0d0 p 1.0d0) (p) "p=~a must be in [0,1]." p)
  (cond
   ((<  x 0.0d0)
    0.0d0)
   ((>= x (az:fl n))
    1.0d0)
   ((< n 12)
    (loop for k from 0 to (truncate x)
	sum (* (az:fl (number-of-choices n k))
	       (expt p k)
	       (expt (- 1.0d0 p) (- n k)))))
   (t (let ((ix (truncate x)))
	(- 1.0d0 (percentile-beta p (az:fl (+ ix 1)) (az:fl (- n ix))))))))

;;;================================================================
;;; Numerical Recipes, 6.2, equation (6.2.17).
;;; (could also try Abramowitz and Stegun 26.4)

(defun percentile-chi-square (x nu)
  (declare (type Double-Float x)
	   (type Integer nu)
	   (:returns (type Double-Float probability)))
  (assert (>= x 0.0d0) (x) "Negative x=~a" x)

  (percentile-gamma (* 0.5d0 x) (* 0.5d0 (az:fl nu))))

;;;----------------------------------------------------------------
;;; students t percentile 
;;; Referenc

(defun percentile-T (x nu)
  (declare (type Double-Float x)
	   (type Integer nu)
	   (:returns (type Double-Float probability)))
  (assert (plusp nu))  
  (flet ;; - - - - - - - - - - - - - - - - -
    ((small-n-mod-t (x n)
       (assert (plusp n))
       (let ((b (+ 1.0d0 (/ (* x x) n)))
	     (y (/ x (sqrt n)))
	     (c 1.0d0))
	 (when (= 1 n)
	   (setf c (loop for i from 0 below (truncate n 2)
			 for n-2i = (- n (* 2.0 i))
			 for c = 0.0d0 then (+ 1.0d0 (/ (* c (- n-2i 1))
						    (* b n-2i)))
			 finally (return c))))
	 (if (evenp n)
	     (+ (/ (* c y) (sqrt b) 2.0) 0.5) 
	     (+ (/ (* -pi/2-
		      (+ (atan y 1.0d0)
			 (/ (* c y) b))) 2.0) 0.5))))
     ;; - - - - - - - - - - - - - - - - -
     (large-n-mod-t (x n)
       (assert (plusp n))
       (let* ((n-0.5 (- n 0.5d0))
	      (d (* 48.0d0 (* n-0.5 n-0.5)))
	      (z (sqrt (* n-0.5 (log (+ 1.0d0 (/ (* x x) n)))))))
	 (percentile-standard-gaussian
	   (+ z
	      (/ (* z (+ (* z z) 3.0d0)) d)
	      (- (/ (+ (* 4.0d0 (expt z 7.0d0))
		       (* 33.0d0 (expt z 5.0d0))
		       (* 240.0d0 (* z z z))
		       (* 855.0d0 z))
		    (* 10.0d0 d (+ d (* 0.8d0 (expt z 4.0d0)) 100.0d0)))))))))
    ;; - - - - - - - - - - - - - - - - -
    (cond
      ((= x 0.0d0) 0.5d0)
      ((and (> nu 150) (< x 1.5d0)) (density-standard-gaussian x))
      (t
       (let ((abs-x (abs x))
	     p)
	 (when (>= abs-x 4.0d0) (setf abs-x (coerce abs-x 'double-float)))
	 (setf p (if (< nu 21)
		     (small-n-mod-t abs-x nu)
		     (large-n-mod-t abs-x nu)))
	 (if (> x 0.0d0) p (- 1.0d0 p)))))))
