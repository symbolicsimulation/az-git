;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :Basic-Math)

;;;=======================================================================
;;; Numerical constants
;;;=======================================================================

(declaim (type az:Positive-Fixnum -double-float-radix- -double-float-digits-))

(declaim (type Double-Float
	       -smallest-positive-magnitude-
	       -largest-magnitude-
	       -smallest-relative-spacing-
	       -largest-relative-spacing-
	       -log10-double-float-radix-
	       -log-smallest-positive-magnitude-
	       -log-largest-magnitude-
	       -log-smallest-relative-spacing-
	       -pi/2-
	       -2pi-
	       -lnpi-
	       -sqrt2pi-
	       -lnsqrt2pi-
	       -convergence-epsilon-
	       -large-double-float-
	       -small-double-float-))

;;;=======================================================================
;;;
;;; These correspond to the r1mach constants in many Fortran libraries:
;;;
;;;  double-precision machine constants
;;;
;;;  r1mach(1) = b**(emin-1), the smallest positive magnitude.
;;; is lisp:least-positive-double-float
;;;
;;;  r1mach(2) = b**emax*(1 - b**(-t)), the largest magnitude.
;;; is lisp:most-positive-double-float
;;;
;;;  r1mach(3) = b**(-t), the smallest relative spacing.
;;;
;;;  r1mach(4) = b**(1-t), the largest relative spacing.
;;;
;;;  r1mach(5) = log10(b)
;;;
;;;  to alter this function for a particular environment,
;;;  the desired set of data statements should be activated by
;;;  removing the c column 1.
;;;

(defconstant -double-float-radix- (float-radix 1.0d0))

(defconstant -double-float-digits- (float-digits 1.0d0))

(defconstant -smallest-positive-magnitude- least-positive-double-float
	     "r1mach(1)")

(defconstant -largest-magnitude-
	     (max most-positive-double-float (- most-negative-double-float))
	     "r1mach(2)")


(defconstant -smallest-relative-spacing-
	       (expt -double-float-radix- (- -double-float-digits-))
	     "r1mach(3)")

(defconstant -largest-relative-spacing-
	       (expt -double-float-radix- (- 1 -double-float-digits-))
	     "r1mach(4)")

(defconstant -log10-double-float-radix-
    (/ (log -double-float-radix-) (log 10.0d0))
	     "r1mach(5)")


(defconstant -log-smallest-positive-magnitude-
 	      (log -smallest-positive-magnitude-))

(defconstant -log-largest-magnitude- (log -largest-magnitude-))

(defconstant -log-smallest-relative-spacing-
	      (log -smallest-relative-spacing-))

;;;=======================================================================
 
(defconstant -pi/2- (/ pi 2.0d0))
(defconstant -2pi- (* 2.0d0 pi))
(defconstant -lnpi- (log pi))
(defconstant -sqrt2pi- (sqrt -2pi-))
(defconstant -lnsqrt2pi- (log -sqrt2pi-))

;;;------------------------------------------------------------------

(defconstant -large-double-float- (sqrt most-positive-double-float))
(defconstant -small-double-float- (* 100.0d0 -largest-relative-spacing-))

;;;=======================================================================
;;; handy, optimize-able, inline-able procedures

(declaim
 (ftype (Function (Double-Float) Double-Float) sign sq)
 (ftype (Function (Double-Float &optional Double-Float) az:Boolean) small?)
 (ftype (Function (Double-Float Double-Float) Double-Float)
	sq-diff abs-difference l2-norm2-xy l2-norm-xy)
 (ftype (Function (Double-Float Double-Float &optional Double-Float)
		  az:Boolean)
	large?)
 (ftype (Function (Double-Float Double-Float Double-Float Double-Float)
		  Double-Float)
	l2-dist2-xy l2-dist-xy)
 (Inline large? small? sign sq  abs-difference l2-norm2-xy
	 l2-norm-xy sq-diff l2-dist2-xy l2-dist-xy %row-major-xy))

;;; Is (abs x) extremely large compared to (abs y)?
(defun large? (x y &optional (big -large-double-float-))
  (> (abs x) (* (+ (abs y) 1.0d0) big))) 

;;; Is x negligibly small compared to y?
(defun small? (x &optional (y 1.0d0)) (= (+ x y) y))

;;; Like <signum>, but returns 1.0 if argument is zero.
(defun sign (a) (if (zerop a) 1.0d0 (signum a)))

(defun sq (a) (* a a))

;;; handy in some macros
(defun sq-diff (a b) (sq (- a b)))

;;; this often needs to be passed to other functions
(defun abs-difference (x1 x2) (abs (- x1 x2)))

(defun l2-norm2-xy (x y) (+ (sq x) (sq y)))

(defun l2-norm-xy (x y) (sqrt (l2-norm2-xy x y)))

(defun l2-dist2-xy (x0 y0 x1 y1) (+ (sq (- x0 x1)) (sq (- y0 y1))))
(defun l2-dist-xy (x0 y0 x1 y1) (sqrt (l2-dist2-xy x0 y0 x1 y1)))

;;;------------------------------------------------------------------

(defun integral-float? (x)
  (declare (type Double-Float x)
	   (:returns (type az:Boolean)))
  (= x (az:fl (truncate x))))

;;;------------------------------------------------------------------
;;; See Dennis and Schnabel A1.3.1.
;;; This is very crude, can be off by a factor of 2.

(defun compute-double-float-epsilon ()
  (let ((eps 1.0d0)
	(step 0.5d0))
    (loop
      (az:mulf eps step)
      (when (= 1.0d0 (+ 1.0d0 eps)) (return (/ eps step))))))
  
;;;------------------------------------------------------------------
;;; Numerical Recipes, Ch 5.2
;;; Test if <x> is getting close to limits on float numbers

(defun under/overflow-danger? (x &optional (scale 100.0d0))
  (not
    (etypecase x
      (Short-Float
	(or (< (/ scale most-negative-short-float)
	       x
	       (* scale least-negative-short-float))
	    (< (* scale least-positive-short-float)
	       x
	       (/ scale most-positive-short-float))))
      (Single-Float
	(or (< (/ scale most-negative-single-float)
	       x
	       (* scale least-negative-single-float))
	    (< (* scale least-positive-single-float)
	       x
	       (/ scale most-positive-single-float))))
      (Double-Float
	(or (< (/ scale most-negative-double-float)
	       x
	       (* scale least-negative-double-float))
	    (< (* scale least-positive-double-float)
	       x
	       (/ scale most-positive-double-float))))
      (Long-Float
	(or (< (/ scale most-negative-long-float)
	       x
	       (* scale least-negative-long-float))
	    (< (* scale least-positive-long-float)
	       x
	       (/ scale most-positive-long-float))))))) 



