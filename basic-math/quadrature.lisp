;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;================================================================
;;;		      One dimensional Integration
;;;
;;; Numerical Recipes, Ch. 4.
;;; Acton, Ch. 4
;;;================================================================

(in-package :Basic-Math)

;;;================================================================
;;; One dimensional Integration
;;; Numerical Recipes, Ch. 4. and Acton, Ch. 4
;;;================================================================

(defconstant -quadrature-epsilon- 1.0d-6)
(defconstant -quadrature-max-iterations- 20)

;;;================================================================
;;; Numerical Recipes, 4.2, TRAPZD, QTRAP

#||
(defun trapezoidal-rule (f a b
			 &key
			 (max-iterations -quadrature-max-iterations-)
			 (epsilon -quadrature-epsilon-))
  
  (let ((b-a (- b a)))
    (flet ((refine (npts)
	     (assert (plusp npts))
	     (loop
		 with delta = (/ b-a npts)
		 repeat npts
		 for x from (+ a (* 0.5d0 delta)) by delta
		 sum (funcall f x) into total
		 finally (return (/ (* b-a total) npts)))))
      (loop
	  repeat max-iterations
	  for npts = 1 then (* 2 npts)
	  for old-integral = 0.0d0 then integral
	  for integral = (* 0.5d0 b-a (+ (funcall f a) (funcall f b)))
	  then (* 0.5d0 (+ integral (refine npts)))
	  do (when (< (abs (- integral old-integral))
		      (* epsilon (abs old-integral)))
	       (return integral))
	  finally (error "<trapezoidal-rule> failed to converge.")))))
||#


(defun trapezoidal-rule (f a b
			 &key
			 (max-iterations -quadrature-max-iterations-)
			 (epsilon -quadrature-epsilon-))
  
  most-negative-double-float(let ((b-a (- b a)))
    (flet ((refine (npts)
	     (assert (plusp npts))
	     (let* ((delta (/ b-a npts))
		    (x (+ a (* 0.5d0 delta)))
		    (total 0.0d0))
	       (dotimes (i npts)
		 (incf total (funcall f x))
		 (incf x delta))
	       (values (/ (* b-a total) npts)))))
      (let* ((npts 1)
	     (old-integral 0.0d0)
	     (integral (* 0.5d0 b-a (+ (funcall f a) (funcall f b)))))
	(dotimes (i max-iterations
		   (error "<trapezoidal-rule> failed to converge."))
	  (when (< (abs (- integral old-integral))
		   (* epsilon (abs old-integral)))
	    (return integral))
	  (setf npts (* 2 npts))
	  (setf old-integral integral)
	  (setf integral (* 0.5d0 (+ integral (refine npts)))))))))

;;;================================================================
;;; See Numerical Recipes, 4.2, TRAPZD, QSIMP

#||
(defun simpsons-rule (f a b
		      &key
		      (max-iterations -quadrature-max-iterations-)
		      (epsilon -quadrature-epsilon-))
  
  (let ((b-a (- b a)))
    (flet ((refine (npts)
	     (assert (plusp npts))
	     (loop
		 with delta = (/ b-a npts)
		 repeat npts
		 for x from (+ a (* 0.5d0 delta)) by delta
		 sum (funcall f x) into total
		 finally (/ (* b-a total) npts))))
      (loop
	  repeat max-iterations
	  for npts = 1 then (* 2 npts)
	  for old-term = 0.0d0 then term
	  for term = (* 0.5d0 b-a (+ (funcall f a) (funcall f b)))
	  then (* 0.5d0 (+ term (refine npts)))
	  for old-integral = 0.0d0 then integral
	  for integral = (/ (- (* 4.0d0 term) old-term) 3.0d0)
	  do (when (< (abs (- integral old-integral))
		      (* epsilon (abs old-integral)))
	       (return integral))
	  finally (error "<simpsons-rule> failed to converge.")))))
||#

(defun simpsons-rule (f a b
		      &key
		      (max-iterations -quadrature-max-iterations-)
		      (epsilon -quadrature-epsilon-))
  
  (let ((b-a (- b a)))
    (flet ((refine (npts)
	     (assert (plusp npts))
	     (let* ((delta (/ b-a npts))
		    (x (+ a (* 0.5d0 delta)))
		    (total 0.0d0))
	       (dotimes (i npts)
		 (incf total (funcall f x))
		 (incf x delta))
	       (values (/ (* b-a total) npts)))))
      (let* ((npts 1)
	     (old-term 0.0d0)
	     (term (* 0.5d0 b-a (+ (funcall f a) (funcall f b))))
	     (old-integral 0.0d0)
	     (integral 0.0d0))
	(dotimes (i max-iterations
		   (error "<simpsons-rule> failed to converge."))
	  (setf integral (/ (- (* 4.0d0 term) old-term) 3.0d0))
	  (when (< (abs (- integral old-integral))
		   (* epsilon (abs old-integral)))
	    (return integral))
	  (setf npts (* 2 npts))
	  (setf old-term term)
	  (setf term (* 0.5d0 (+ term (refine npts))))
	  (setf old-integral integral))))))


