;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================
;;;
;;;		    Interpolation and Extrapolation
;;; 
;;; based on Numerical Recipes, Chapter 4.
;;;=======================================================================

(in-package :Basic-Math)

;;;================================================================
;;; Interpolation and Extrapolation
;;; based on Numerical Recipes, Chapter 4.
;;;================================================================
;;; based on subroutine POLINT, Numerical Recipes 3.1

#||
(defun make-polynomial-interpolator (xs ys)
  (declare (type az:Float-Vector xs ys)
	   (values interpolating-function))
  (assert (= (length xs) (length ys)))
  
  (let* ((n (length xs))
	 (c (make-array n))
	 (d (make-array n)))
    (declare (type az:Array-Index n)
	     (type az:Float-Vector c d))
    #'(lambda (x)
	(declare (type Double-Float x)
		 (values interpolated-value error-estimate))
	(block nil
	  ;; test to see if it's exactly one of the given values
	  (let ((pos (find x xs :test #'=)))
	    (when (not (null pos))
	      (return (values (aref ys pos) 0.0d0))))
	  (let* ((delta-y 0.0d0)
		 (index (index-of-nearest-neighbor x xs #'abs-difference))
		 (y (aref ys index)))
	    (declare (type Double-Float index y delta-y))
	    (loop for i from 0 below n do (setf (aref c i) (aref ys i))
					  (setf (aref d i) (aref ys i)))
	    (loop for m from 1 below n
		  for n-m = (- n m)
		  do (loop for i from 0 below n-m
			   for hc = (- (aref xs i) x)
			   for hd = (- (aref xs (+ i m)) x)
			   for numer = (- (aref c (+ i 1)) (aref d i))
			   for denom = (- hc hd)
			   for fac = (progn (assert (not (zerop denom)))
					    (/ numer denom))
			   do (setf (aref d i) (* hd fac))
			      (setf (aref c i) (* hc fac)))
		     (setf delta-y (if (<= (* 2 index) n-m)
				       (aref c index)
				       (aref d (decf index))))
		     (incf y delta-y))	  
	    (values y delta-y))))))
||#

(defun make-polynomial-interpolator (xs ys)
  (declare (type az:Float-Vector xs ys)
	   (:returns interpolating-function))
  (assert (= (length xs) (length ys)))
  
  (let* ((n (length xs))
	 (c (az:make-float-vector n))
	 (d (az:make-float-vector n)))
    (declare (type az:Array-Index n)
	     (type az:Float-Vector c d))
    #'(lambda (x)
	(declare (type Double-Float x)
		 (:returns interpolated-value error-estimate))
	(block nil
	  ;; test to see if it's exactly one of the given values
	  (let ((pos (find x xs :test #'=)))
	    (when (not (null pos))
	      (return (values (aref ys pos) 0.0d0))))
	  (let* ((index (index-of-nearest-neighbor x xs #'abs-difference))
		 (delta-y 0.0d0)
		 (y (aref ys index)))
	    (declare (type az:Array-Index index)
		     (type Double-Float y delta-y))
	    (dotimes (i n)
	      (setf (aref c i) (aref ys i))
	      (setf (aref d i) (aref ys i)))
	    (dotimes (m n)
	      (let ((n-m (- n m)))
		(dotimes (i n-m)
		  (let* ((hc (- (aref xs i) x))
			 (hd  (- (aref xs (+ i m)) x))
			 (numer (- (aref c (+ i 1)) (aref d i)))
			 (denom  (- hc hd))
			 (fac (progn (assert (not (zerop denom)))
				     (/ numer denom))))
		    (setf (aref d i) (* hd fac))
		    (setf (aref c i) (* hc fac))))
		(setf delta-y
		  (if (<= (* 2 index) n-m)
		      (aref c index)
		    (aref d (decf index)))))
	      (incf y delta-y))	  
	    (values y delta-y))))))

;;;-----------------------------------------------------------------------
;;; based on subroutine RATINT, Numerical Recipes 3.2

(defun make-rational-interpolator (xs ys)
  (declare (type az:Float-Vector xs ys)
	   (:returns interpolating-function))
  (assert (= (length xs) (length ys)))
  (let* ((n (length xs))
	 (c (az:make-float-vector n))
	 (d (az:make-float-vector n))
	 (tiny 1.0d-35))
    (declare (type integer n)
	     (type az:Float-Vector c d)
	     (type Double-Float tiny))
    #'(lambda (x)
	(declare (type Double-Float x)
		 (:returns interpolated-value error-estimate))
	(block nil
	  ;; test to see if it's exactly one of the given values
	  (let ((pos (find x xs :test #'=)))
	    (when (not (null pos))
	      (return (values (aref ys pos) 0.0d0))))
	  
	  (let* ((index (index-of-nearest-neighbor x xs #'abs-difference))
		 (delta-y 0.0d0)
		 (y (aref ys index)))
	    (declare (type Integer index)
		     (type Double-Float y delta-y))
	    (loop for i from 0 below n
		do (setf (aref c i) (aref ys i))
		   ;; the tiny part is needed to prevent rare 0/0 below
		   (setf (aref d i) (+ tiny (aref ys i))))
	    (loop for m from 1 below n
		for n-m = (- n m)
		do (loop for i from 0 below n-m
		       for w = (- (aref c (+ i 1)) (aref d i))
		       for h = (- (aref xs (+ i m)) x)
		       for tt = (progn (assert (not (zerop h)))
				       (* (- (aref xs i) x)
					  (/ (aref d i) h)))
		       for dd = (- tt (aref c (+ i 1)))
		       when (= dd 0) do (error "x is a singularity.")
		       do (setf dd (/ w dd))
			  (setf (aref d i) (* (aref c (+ i 1)) dd))
			  (setf (aref c i) (* tt dd)))
		   (setf delta-y (if (<= (* 2 index) n-m)
				     (aref c index)
				   (aref d (decf index))))
		   (incf y delta-y))	  
	    (values y delta-y))))))

;;;-----------------------------------------------------------------------

(defun make-linear-interpolator (xs ys)
  (declare (type az:Float-Vector xs ys)
	   (:returns interpolating-function))
  (assert (= (length xs) (length ys)))
  (assert (ordered? xs #'<))
  #'(lambda (x)
      (declare (type Double-Float x)
	       (:returns interpolated-value))
      (let ((i (locate x xs)))
	(when (null i) (error "x=~a not in range of xs=~a." x xs))
	(let* ((i+1 (+ i 1))
	       (x1 (aref xs i))
	       (x2 (aref xs i+1))
	       (x2-x1 (- x2 x1))
	       (y1 (aref ys i))
	       (y2 (aref ys i+1)))
	  (assert (not (zerop x2-x1)))
	  (+ (* y1 (/ (- x2 x) x2-x1))
	     (* y2 (/ (- x x1) x2-x1))))))) 

  