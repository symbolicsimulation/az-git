;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 
  
;;;============================================================
;;; Objects to handle input to Graph-Diagrams
;;;============================================================

(defclass Graph-Role (Clay:Diagram-Role)
	  ((curr-pos
	    :type g:Point
	    :reader curr-pos)
	   (last-pos
	    :type g:Point
	    :reader last-pos)))

;;;-----------------------------------------------------------

(defmethod initialize-instance :after ((role Graph-Role) &rest inits)
  (declare (ignore inits))
  (let ((space (xlt:drawable-space 
		(clay:diagram-window (ac:role-actor role)))))
    (setf (slot-value role 'curr-pos) (g:make-element space))
    (setf (slot-value role 'last-pos) (g:make-element space))))

;;;============================================================
;;; Methods for Graph-Role 
;;;============================================================

(defmethod ac:button-press ((role Graph-Role)
			    msg-name sender receiver id
			    code x y state time root root-x root-y child
			    same-screen-p)

  (declare (type Clay:Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Graph-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))

  (let ((selected (clay:find-diagram-under-xy receiver x y)))
    (declare (type clay:Diagram selected))
    (cond ((and (= code 3) (eq selected receiver))
	   ;; then put up our menu
	   (clay:diagram-show-menu receiver x y))
	  ((and (not (eq selected receiver))
		(typep selected 'Clay:Interactor-Diagram))
	   ;; then forward the event
	   (ac:send-msg 'ac:button-press receiver selected (ac:new-id)
			code x y state time root root-x root-y child
			same-screen-p))
	  ((= code 3)
	   ;; the selected is not itself an Interactor, (must be an edge)
	   ;; so put up a menu for it
	   (clay:diagram-show-menu receiver x y))))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:button-release ((role Graph-Role)
			      msg-name sender receiver id
			      code x y state time root root-x root-y child
			      same-screen-p)
  "This method returns control to the default role."
  (declare (type Clay:Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Graph-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))
  ;; pop up the button, if one is down
  (let ((selected (clay:find-diagram-under-xy receiver x y)))
    (declare (type clay:Diagram selected))
    (when (and (not (eq selected receiver))
	       (typep selected 'Clay:Interactor-Diagram))
      (ac:send-msg 'ac:button-release receiver selected (ac:new-id)
		   code x y state time root root-x root-y child
		   same-screen-p))
    ;; return control to the default role
    (setf (ac:actor-current-role receiver) (ac:actor-default-role receiver)))
  (values t))
 
;;;============================================================
;;; Role for node dragging
;;;============================================================

(defclass Node-Dragging-Role (Graph-Role)
	  ((node
	    :type Node-Diagram
	    :accessor node)
	   (start
	    :type g:Point
	    :accessor start)
	   (dp
	    :type g:Point
	    :accessor dp)))

;;;-----------------------------------------------------------

(defmethod initialize-instance :after ((role Node-Dragging-Role) &rest inits)
  (declare (ignore inits))
  (let ((space 
	 (xlt:drawable-space (clay:diagram-window (ac:role-actor role)))))
    (setf (slot-value role 'start) (g:make-element space))
    (setf (slot-value role 'dp) (g:make-element (g:translation-space space)))))

;;;============================================================
;;; Methods for Node-Dragging-Role 
;;;============================================================

(defmethod ac:interactor-role-cursor ((interactor Clay:Interactor-Diagram)
				      (role Node-Dragging-Role))
  "A Node dragger gets a hand cursor."
  (xlt:window-cursor (ac:interactor-window interactor) :hand2))

;;;============================================================

(defun give-control-to-node-dragger (node x y)

  (declare (optimize (safety 0) (speed 3))
	   (type Node-Diagram node)
	   (type az:Int16 x y))

  (let* ((diagram (clay:diagram-parent node))
	 (dragger (node-dragging-role diagram)))
    (declare (type Graph-Diagram diagram)
	     (type Node-Dragging-Role dragger))
    (setf (node dragger) node)
    (unpin-node node) ;; in case it's pinned
    (az:copy (g:origin (clay:diagram-window-rect (node dragger)))
	     :result (start dragger))
    (setf (g:x (curr-pos dragger)) x)
    (setf (g:y (curr-pos dragger)) y)
    (az:copy (curr-pos dragger) :result (last-pos dragger))
    (g:sub (start dragger) (curr-pos dragger) :result (dp dragger))
    (setf (ac:actor-current-role diagram) dragger)))

;;;-----------------------------------------------------------

(defmethod ac:button-release ((role Node-Dragging-Role)
			      msg-name sender receiver id
			      code x y state time root root-x root-y child
			      same-screen-p)
  "Return control to the default role and pop the dragged node up."
  (declare (type Clay:Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Graph-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   code x y state time root root-x root-y child same-screen-p))
  ;; pop up node
  (setf (clay:button-state (node role)) :up)
  ;; return control to the default role
  (setf (ac:actor-current-role receiver) (ac:actor-default-role receiver))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:motion-notify ((role Node-Dragging-Role)
			     msg-name sender receiver id
			     hint-p x y state time root root-x root-y child
			     same-screen-p)
 
  "This method moves the selected node."
  
  (declare (optimize (safety 0) (speed 3))
	   (type Node-Dragging-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Graph-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Boolean hint-p)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id 
		   hint-p state time root root-x root-y child same-screen-p))
  
  (let ((queue (ac:actor-msg-queue receiver))
	(node (node role))
	(curr-pos (curr-pos role))
	(last-pos (last-pos role))
	(start (start role))
	(dp (dp role)))

    (declare (type Graph-Diagram receiver)
	     (type ac:Queue queue)
	     (type Node-Diagram node)
	     (type g:Point curr-pos last-pos start)
	     (type g:Point dp))
    
    ;; jump to the last motion notify msg on the queue
    (loop (let ((next-msg (ac:dequeue queue)))
	    (unless (or (eq (first next-msg) 'ac:motion-notify)
			(eq (first next-msg) #'ac:motion-notify))
	      (unless (null next-msg) (ac:queue-push next-msg queue))
	      (return))
	    (setf x (sixth next-msg))
	    (setf y (seventh next-msg))))
    
    ;; follow mouse with current node, if any
    (unless (null node)
      (az:copy curr-pos :result last-pos)
      (setf (g:x curr-pos) x)
      (setf (g:y curr-pos) y)
      (unless (az:equal? curr-pos last-pos)
	(g:add curr-pos dp :result start)
	(move-diagram-to node start))))
  (values t))


(defmethod clay:find-diagram-under-xy ((diagram Graph-Diagram) x y)
  (declare (type Graph-Diagram diagram)
	   (type az:Int16 x y))
  (g:with-borrowed-element 
      (p (xlt:drawable-space (clay:diagram-window diagram)))
    (setf (g:x p) x)
    (setf (g:y p) y)
    (dolist (node (gra:nodes diagram))
      (when (g:intersect? p (clay:diagram-window-rect node))
	(return-from clay:find-diagram-under-xy node)))
    (dolist (edge (gra:edges diagram))
      (when (g:intersect? p (clay:diagram-window-rect edge))
	(return-from clay:find-diagram-under-xy edge))))
  ;; if we're not over a node or an edge, return the graph itself
  diagram)





