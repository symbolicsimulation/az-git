;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================
;;; Bounds constraints:
;;;============================================================
;;; Converting bounds constraints to NPSOL representation:

(defmethod bounds-constraints (diagram (solver Graph-Layout-Solver) bl bu)

  (declare (type Graph-Diagram diagram)
	   (type Graph-Layout-Solver solver)
	   (type (Array Double-Float (*)) bl bu))
 
  (let* ((bounding-r (clay:diagram-window-rect diagram))
	 (bxmin (az:fl (g:x bounding-r)))
	 (bxmax (az:fl (g:xmax bounding-r)))
	 (bymin (az:fl (g:y bounding-r)))
	 (bymax (az:fl (g:ymax bounding-r)))
	 (i 0))
    
    (declare (type g:Rect bounding-r)
	     (type Double-Float bxmin bxmax bymin bymax)
	     (type az:Array-Index i))
    
    (dolist (node (brk:bricks (clay:layout-domain solver)))
      (declare (type Node-Diagram node))
      (let ((bounds (node-bounds node)))
	(if bounds
	    (locally (declare (type g:Rect bounds))
	      (let ((xmin (g:x bounds))
		    (w (g:w bounds))
		    (ymin (g:y bounds))
		    (h (g:h bounds)))
		(declare (type Double-Float xmin w ymin h))
		(setf (aref bl i) xmin)
		(setf (aref bu i) (+ xmin w))
		(incf i)
		(setf (aref bl i) ymin)
		(setf (aref bu i) (+ ymin h))
		(incf i)))
	  ;; else the bounds are unspecified,
	  ;; so use the screen rect
	  (let ((r (clay:diagram-window-rect node)))
	    (declare (type g:Rect r))
	    (setf (aref bl i) bxmin)
	    (setf (aref bu i) (- bxmax (az:fl (g:w r))))
	    (incf i)
	    (setf (aref bl i) bymin)
	    (setf (aref bu i) (- bymax (az:fl (g:h r))))
	    (incf i)))))))

;;;============================================================
;;; User specified bounds constraints
;;;============================================================
;;; set the node bounds so the node can't move

(defun pin-node (node)
  (declare (type Node-Diagram node))
  (ac:atomic
   (setf (node-user-pinned? node) t)
   (let* ((bounds (node-bounds node))
	  (wr (clay:diagram-window-rect node))
	  (x (az:fl (g:x wr)))
	  (y (az:fl (g:y wr))))
     (declare (type g:Rect wr)
	      (type Double-Float x y))
     (cond
      ((null bounds) ;; no bounds were ever defined
       (setf bounds (let* ((r (g:make-rect (g:rn 2)))
			   (c (g:coords (g:origin r))))
		      (setf (aref c 0) x)
		      (setf (aref c 1) y)
		      r))
       (setf (node-bounds node) bounds))
      (t ;; bounds rect exists, fix its coordinates
       (locally (declare (type g:Rect bounds))
	 (setf (g:x bounds) x)
	 (setf (g:y bounds) y)
	 (setf (g:w bounds) 0.0d0)
	 (setf (g:h bounds) 0.0d0)))))))

;;; release the node to be anywhere in its window

(defun unpin-node (node)
  (declare (type Node-Diagram node))
  (ac:atomic
   (setf (node-user-pinned? node) nil)
   (when (node-bounds node) (setf (node-bounds node) nil))))

;;; toggle the pinning state. Assume that if the bounds
;;; aren't nil, then it's pinned.

(defun toggle-pin-node (node)
  (declare (type Node-Diagram node))
  (if (node-user-pinned? node)
      (unpin-node node)
    ;; else
    (pin-node node)))

;;;============================================================
;;; Affine constraints:
;;;============================================================

(defun translate-affine-constraint (constraint domain constraint-mtx
				    col-index bl bu b-index)

  "translate-affine-constraint replaces the references to :width and
:height by appropriately modifying lower-bound and upper-bound. It
also replaces the references to (left #<node xxx>) and (top #<node
xxx>) by the appropriate indexes into the corresponding row of the
constraint matrix passed to NPSOL."

  (let ((bound (brk:bound constraint))
	(relation (brk:relation constraint))
	(terms (brk:terms (brk:functional constraint))))
    (dolist (term terms)
      (let* ((coef (brk:term-coefficient term))
	     (param (brk:term-parameter term))
	     (node (brk:term-brick term))
	     (i (brk:brick-index node domain)))
	(declare (type Node-Diagram node)
		 (type brk:Parameter param)
		 (type Double-Float coef)
		 (type az:Array-Index i))
	(ecase param
	  (:left
	   (setf (aref constraint-mtx i col-index) coef))
	  (:width
	   (decf bound (* coef (brk:width node))))
	  (:top
	   (setf (aref constraint-mtx (+ 1 i) col-index) coef))
	  (:height
	   (decf bound (* coef (brk:height node))))))
      (ecase relation
	('<= (setf (aref bl b-index) bound)
	     (setf (aref bu b-index) *npsol-infinity*))
	('=  (setf (aref bl b-index) bound)
	     (setf (aref bu b-index) bound))
	('>= (setf (aref bl b-index) (- *npsol-infinity*))
	     (setf (aref bu b-index) bound))))))

(defun translate-affine-constraints (constraints domain
				     constraint-mtx bl bu)

  ;; make sure the constraint-mtx is zeroed to start
  (zero-mtx constraint-mtx)
  (let ((col-index 0)
	(b-index (brk:dimension domain)))
    (dolist (constraint constraints)
      (translate-affine-constraint
       constraint domain constraint-mtx col-index bl bu b-index)
      (incf col-index)
      (incf b-index))))

(defun node-constrained? (node constraint)
  (find node (cddr constraint) :key #'car))

(defun affine-constraint-term-value (term)
  (let ((node (brk:term-brick term))
	(param (brk:term-parameter term))
	(coef (brk:term-coefficient term)))
    (declare (type Node-Diagram node)
	     (type brk:Parameter param)
	     (type Double-Float coef))
    (* coef (az:fl (ecase param
		     (:left   (brk:left node))
		     (:width  (brk:width node))
		     (:top    (brk:top node))
		     (:height (brk:height node)))))))

;;;============================================================

(defun make-order-constraint (domain node0 node1 direction delta)
  (declare (type Node-Diagram node0 node1)
	   (type Double-Float delta)
	   (type (Member :x :y) direction))
  (ecase direction
    (:x (brk:to-left domain node0 delta node1))
    (:y (brk:above domain node0 delta node1))))

(defun make-order-constraints (diagram f direction delta)
  (declare (type Graph-Diagram diagram)
	   (type (or Function Symbol) f)
	   (type (Member :x :y) direction))
  (let ((nodes (gra:nodes diagram))
	(domain (clay:layout-domain (clay:diagram-layout-solver diagram)))
	(cs ())
	n0 s0 s1
	(cf (ecase direction (:x 'brk:to-left) (:y 'brk:above))))
    (loop
      (setf n0 (first nodes))
      (setf s0 (clay:diagram-subject n0))
      (setf nodes (rest nodes))
      (when (null nodes) (return))
      (dolist (n1 nodes)
	(setf s1 (clay:diagram-subject n1))
	(setf cs
	  (nconc
	   (cond ((funcall f s0 s1) (funcall cf domain n0 delta n1))
		 ((funcall f s1 s0) (funcall cf domain n1 delta n0))
		 (t ()))
	   cs))))
    cs))    

(defun make-=-constraint (domain node0 node1 direction)
  (declare (type Node-Diagram node0 node1)
	   (type (Member :x :y) direction))
  (let ((parameter (ecase direction (:x :left) (:y :top))))
    (brk:equal-parameter domain parameter node0 node1)))

(defun make-dag-constraints (gp
			     &key
			     (delta 16.0d0)
			     (direction :y))
  (declare (type Graph-Diagram gp)
	   (type Double-Float delta)
	   (type (Member :x :y) direction))
  (let ((cs ())
	(domain (clay:layout-domain (clay:diagram-layout-solver gp))))
    (dolist (node0 (gra:nodes gp))
      (dolist (edge (to-edges node0))
	(setf cs
	  (nconc
	   (make-order-constraint
	    domain (gra:edge-node0 edge) node0 direction delta) cs))))
    cs))

(defun make-sibling-constraints (gp &key
				    (delta 16.0d0)
				    (direction :y))
  (declare (type Graph-Diagram gp)
	   (type Double-Float delta)
	   (type (Member :x :y) direction))
  (let ((cs ())
	(domain (clay:layout-domain (clay:diagram-layout-solver gp))))
    (dolist (parent (gra:nodes gp))
      (let* ((children (node-children parent))
	     (first-child (first children)))
	(unless (null children)
	  (setf cs
	    (nconc
	     (make-order-constraint domain parent first-child direction delta)
		   cs))
	  (dolist (child (rest children))
	    (setf cs
	      (nconc
	       (make-order-constraint domain parent child direction delta)
		     cs))
	    (setf cs
	      (nconc (make-=-constraint domain first-child child direction)
		     cs))))))
    cs))

(defun roots (gp) (remove-if-not #'root-node? (gra:nodes gp)))

;;;============================================================
;;; Reduce Affine Constraints when only a subset of the nodes
;;; is allowed to move.
;;;============================================================

(defun reduce-affine-constraint (constraint domain)
  
  (declare (type brk:Inequality constraint)
	   (type brk:Domain domain))
  
  (flet ((variable? (term)
	   (declare (type brk:Term term))
	   (member (brk:term-brick term) (brk:bricks domain))))
    
    (let* ((bound (brk:bound constraint))
	   (terms (brk:terms (brk:functional constraint)))
	   (constant-terms (remove-if #'variable? terms))
	   (variable-terms (remove-if-not #'variable? terms)))
      (declare (type Double-Float bound)
	       (type List terms constant-terms variable-terms))
      (unless (null variable-terms)
	(dolist (term constant-terms)
	  (declare (type List term))
	  (let ((value (affine-constraint-term-value term)))
	    (declare (type Double-Float value))
	    (decf bound value)))
	;; return reduced constraint
	(brk:make-inequality bound
			     (brk:relation constraint)
			     (apply #'brk:make-functional
				    domain variable-terms))))))

(defun reduce-affine-constraints (constraints domain)
  (declare (type brk:Inequality-List constraints)
	   (type brk:Domain domain))
  (delete ()
	  (az:with-collection
	   (dolist (constraint constraints)
	     (az:collect
	      (reduce-affine-constraint constraint domain))))))

