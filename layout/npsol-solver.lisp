;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================

(defclass NPSOL-Graph-Layout-Solver (Graph-Layout-Solver)
	  (;; save arguments to npsol-minimize
	   (npsol-x
	    :type az:Float-Vector
	    :initform '#(0.0d0))
	   (npsol-bl
	    :type az:Float-Vector
	    :initform '#(0.0d0))
	   (npsol-bu
	    :type az:Float-Vector
	    :initform '#(0.0d0))
	   (npsol-a
	    :type az:Float-Matrix
	    :initform '#((0.0d0)))))

;;;------------------------------------------------------------

(defmethod clay:make-layout-cost ((diagram Graph-Diagram)
				  (solver NPSOL-Graph-Layout-Solver))
  'calculate-energy)
 
;;;------------------------------------------------------------
;;; caches for arguments to npsol

(defun npsol-x (solver n)
  (declare (type Graph-Layout-Solver solver)
	   (type az:Array-Index n))
  (setf (slot-value solver 'npsol-x)
    (ensure-large-enough-vector (slot-value solver 'npsol-x) n)))

(defun npsol-bl (solver n+nclin)
  (declare (type Graph-Layout-Solver solver)
	   (type az:Array-Index n+nclin))
  (setf (slot-value solver 'npsol-bl)
    (ensure-large-enough-vector (slot-value solver 'npsol-bl) n+nclin)))

(defun npsol-bu  (solver n+nclin)
  (declare (type Graph-Layout-Solver solver)
	   (type az:Array-Index n+nclin))
  (setf (slot-value solver 'npsol-bu)
    (ensure-large-enough-vector (slot-value solver 'npsol-bu) n+nclin)))

(defun npsol-a (solver n nclin)
  (declare (type Graph-Layout-Solver solver)
	   (type az:Array-Index n nclin))
  (setf (slot-value solver 'npsol-a)
    (ensure-large-enough-mtx (slot-value solver 'npsol-a) n nclin)))

;;;------------------------------------------------------------
;;; Energy calculation
;;;------------------------------------------------------------

(defgeneric calculate-energy (solver objf grad gradient? residual?)
  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Layout-Solver solver)
	   (type az:Float-Vector objf grad)
	   (type az:Boolean gradient? residual?)))

(defparameter *sum* (make-double-array '(1) :element-type 'Double-Float))
(defmethod calculate-energy ((solver Graph-Layout-Solver)
			     objf grad gradient? residual?)

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Layout-Solver solver)
	   (type az:Float-Vector objf grad)
	   (type az:Boolean gradient? residual?))

  (print (list 'calculate-energy solver objf grad gradient? residual?))
  (finish-output )
  (let* ((domain (clay:layout-domain solver))
	 (active-nodes (brk:bricks domain))
	 (inactive-nodes (inactive-nodes solver)))
    (declare (type brk:Domain domain)
	     (type List active-nodes inactive-nodes))

    (setf (aref *sum* 0) 0.0d0)
    (when gradient? (zero-node-gradients active-nodes))

    (dolist (node0 active-nodes)
      (declare (type Node-Diagram node0))
      (dolist (node1 active-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (print (list 'calculate-energy (node-pair-energy-f solver)
		   solver node0 node1 gradient? gradient?
		   residual? *sum*))
	  (finish-output)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? gradient?
		   residual? *sum*)))
      (dolist (node1 inactive-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? nil residual?
		   *sum*))))
    (setf (aref objf 0) (aref *sum* 0))
    (when gradient? (nodes->objgrd active-nodes grad))))

;;;------------------------------------------------------------

(defun zero-node-gradients (nodes)

  (declare (optimize (safety 0) (speed 3))
	   (type List nodes))

  (dolist (node nodes)
    (declare (type Node-Diagram node))
    (let ((grd (node-grd node)))
      (declare (type az:Float-Vector grd))
      (setf (aref grd 0) 0.0d0)
      (setf (aref grd 1) 0.0d0)
      (setf (aref grd 2) 0.0d0))))

(defun nodes->objgrd (nodes grad)

  (declare (optimize (safety 0) (speed 3))
	   (type List nodes)
	   (type az:Float-Vector grad))

  (let ((i -1))
    (declare (type Fixnum i))
    (dolist (node nodes)
      (declare (type Node-Diagram node))
      (let ((grd (node-grd node)))
	(declare (type az:Float-Vector grd))
	(setf (aref grad (incf i)) (aref grd 0))
	(setf (aref grad (incf i)) (aref grd 1))))))

;;;------------------------------------------------------------

(defun swap-nodes (gp node0 node1 &key (animate? t))

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram gp)
	   (type Node-Diagram node0 node1))

  ;;(when animate? (clay:erase-diagram gp nil))

  (let* ((solver (clay:diagram-layout-solver gp))
	 (r0 (clay:diagram-window-rect node0))
	 (r1 (clay:diagram-window-rect node1)))
    (declare (type clay:Layout-Solver solver)
	     (type g:Rect r0 r1))

    (rotatef (node-pnt node0) (node-pnt node1))
    (rotatef (g:x r0) (g:x r1))
    (rotatef (g:y r0) (g:y r1))

    (dolist (edge (the List (from-edges node0)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))
    (dolist (edge (the List (to-edges node0)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))

    (dolist (edge (the List (from-edges node1)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))
    (dolist (edge (the List (to-edges node1)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))

    (when animate? (clay:redraw-diagram gp))))

;;;------------------------------------------------------------

(defun swap-solve-layout (gp &key (animate? t))

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram gp))
  
  (let* ((solver (clay:diagram-layout-solver gp))
	 (the-nodes (gra:nodes gp))
	 (objf (az:make-float-vector 1))
	 (grad (az:make-float-vector 0))	 
	 (energy (az:make-float-vector
		  1 :initial-element most-positive-double-float)))
    (declare (type Graph-Layout-Solver solver)
	     (type List the-nodes)
	     (type az:Float-Vector objf grad energy))
    (do ((nodes the-nodes (rest nodes)))
	((null nodes))
      (declare (type List nodes))
      (let ((node0 (first nodes)))
	(declare (type Node-Diagram node0))
	(dolist (node1 (rest nodes))
	  (declare (type Node-Diagram node1))
	  (swap-nodes gp node0 node1 :animate? animate?)
	  (calculate-energy solver objf grad nil nil)

	  (if (< (aref objf 0) (aref energy 0))
	      (setf (aref energy 0) (aref objf 0))
	    ;; else
	    (swap-nodes gp node0 node1 :animate? animate?)))))
    energy))
 
;;;------------------------------------------------------------

(defmethod clay:solve-layout ((diagram Graph-Diagram)
			      (solver NPSOL-Graph-Layout-Solver))

  (declare (optimize (safety 0) (speed 3))
	   (type NPSOL-Graph-Layout-Solver solver))

  (let ((old-domain (clay:layout-domain solver))
	(value most-positive-double-float))
    (declare (type brk:Domain old-domain)
	     (type Double-Float value))
    (unwind-protect
	(progn
	  (setf (clay:layout-domain solver)
	    (brk:make-position-domain
	     (remove-if #'node-user-pinned? (brk:bricks old-domain))))
	  (setf (inactive-nodes solver)
	    (set-difference (gra:nodes diagram)
			    (brk:bricks (clay:layout-domain solver))))
	  (when (> (brk:dimension (clay:layout-domain solver)) 0)
	    (let* ((domain (clay:layout-domain solver))
		   (constraints
		    (reduce-affine-constraints
		     (clay:layout-constraints solver) domain))
		   (n (brk:dimension domain))
		   (nclin (length constraints))
		   (n+nclin (+ n nclin))
		   (x (npsol-x solver n))
		   (bl (npsol-bl solver n+nclin))
		   (bu (npsol-bu solver n+nclin))
		   (a (npsol-a solver n nclin)))
	      
	      (declare (type Graph-Diagram diagram)
		       (type az:Array-Index n nclin n+nclin)
		       (type az:Float-Vector x bl bu)
		       (type az:Float-Matrix a))

	      (initialize-node-float-coordinates diagram)
	      (nodes->npsol (brk:bricks domain) x)
	      (bounds-constraints diagram solver bl bu)
	      (translate-affine-constraints constraints domain a bl bu)

	      (multiple-value-bind (x0 objf objgrd inform iter)
		  (npsol:npsol-minimize
		   (npsol-default-options solver)
		   (npsol-objfn diagram solver)
		   (npsol-confn diagram solver)
		   n nclin 0 x a bl bu)
		(declare (ignore x0 objgrd inform iter))

		;; draw the final state, even if we not animating.
		(npsol->nodes x (brk:bricks domain))
		(attach-edges solver diagram)
		(setf value objf))))
	  ;; cleanup
	  (setf (clay:layout-domain solver) old-domain))
      (values value))))



