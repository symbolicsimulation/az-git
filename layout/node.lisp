;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 
  
;;;============================================================
#||
(defstruct (Node-Layout-Parameters
	    (:conc-name nil)
	    (:constructor %make-node-layout-parameters))  )
||#

(defclass Node-Diagram (clay:Button) 
	  ( ;; slots for the graph strucutre
	   (from-edges
	    :type List
	    :accessor from-edges
	    :initform ())
	   (to-edges
	    :type List
	    :accessor to-edges
	    :initform ())
	   ;; slots used by various graph analysis algorithms
	   #||
	   (node-layout-parameters
	    :type Node-Layout-Parameters
	    :reader node-layout-parameters)
	   ||#
	   (node-index
	    ;; index of this node in the set of all nodes
	    :type az:Array-Index
	    :accessor node-index)
	   (node-mark
	    :type (Member :red :green :blue)
	    :accessor node-mark)
	   (node-weight
	    :type Double-Float
	    :accessor node-weight)
	   (node-predecessor
	    :type T ;; (or Null Node-Diagram)
	    :accessor node-predecessor)
	   (loss
	    :type Double-Float
	    :accessor loss)
	   (node-pnt
	    :type az:Float-Vector
	    :accessor node-pnt
	    :initform (az:make-float-vector 3 :initial-element 0.0d0))
	   (node-rad
	    :type az:Float-Vector
	    :accessor node-rad
	    :initform (az:make-float-vector 3 :initial-element 0.0d0))
	   (node-1/rad
	    :type az:Float-Vector
	    :accessor node-1/rad
	    :initform (az:make-float-vector 3 :initial-element 0.0d0))
	   (node-grd
	    :type az:Float-Vector
	    :accessor node-grd
	    :initform (az:make-float-vector 3 :initial-element 0.0d0))
	   (node-bounds
	    ;; The upper left corner of the node is constrained
	    ;; to be in this rectangle. If null, the node is
	    ;; constrained to remain entriely within its window.
	    :type (or Null g:Rect)
	    :accessor node-bounds
	    :initform nil)
	   (node-user-pinned?
	    ;; were the bounds set by the user?
	    ;; The implication is that they should
	    ;; then not be changed by the program.
	    :type az:Boolean
	    :accessor node-user-pinned?
	    :initform nil)
	   ;; slots used for diagram and input
	   (allowed-rect
	    :type g:Rect
	    :reader allowed-rect)))

;;;------------------------------------------------------------

(defmethod initialize-instance :after ((node Node-Diagram) &rest inits)
  (declare (ignore inits))
  (setf (slot-value node 'allowed-rect)
    (g:make-rect (xlt:drawable-space (clay:diagram-window node)))))

;;;------------------------------------------------------------

(defmethod print-object ((node Node-Diagram) stream)
  (declare (type Stream stream))
  (az:printing-random-thing
   (node stream)
   (format stream "NodeP ~a" (node-name (clay:diagram-subject node)))))

;;;------------------------------------------------------------

(defmethod diagram-name ((node Node-Diagram))
  (format nil "NodeP ~a" (node-name (clay:diagram-subject node))))

;;;------------------------------------------------------------

(defgeneric node-name (object)
  #-sbcl (declare (type T object))
  (:documentation
   "Return a string or symbol used in graph displays of this object graph."))

(defmethod node-name ((node T))
  "Prints the node to a string of maximum length 32."
  (let* (#+:excl (excl:*print-nickname* t)
	 (s (format nil "~a" node)))
    (subseq s 0 (min 32 (length s)))))

(defmethod node-name ((node String))
  "Prints the node to a string of maximum length 32."
  (subseq node 0 (min 32 (length node))))

(defmethod node-name ((cl Class))
  (let (#+:excl (excl:*print-nickname* t))
    (format nil "~:(~s~)" (class-name cl))))

;;;------------------------------------------------------------

(defun node-children (node)
  (declare (type Node-Diagram node))
  (az:with-collection
   (dolist (edge (from-edges node))
     (az:collect (gra:edge-node1 edge)))))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Node-Diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Node-Role :role-actor diagram)))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod clay:layout-diagram :before ((diagram Node-Diagram) layout-options)

  (declare (type Node-Diagram diagram)
	   (type List layout-options)
	   (ignore layout-options))

  ;; initialize the button's diagram-window-rect
  (setf (brk:width diagram) (clay:diagram-minimum-width diagram))
  (setf (brk:height diagram) (clay:diagram-minimum-height diagram)))

(defmethod clay:layout-diagram :after ((diagram Node-Diagram) layout-options)
  (declare (type Node-Diagram diagram)
	   (type List layout-options)
	   (ignore layout-options))
  ;; set up possible region for the upper left corner of the node
  (g:possible-point-rect
   (clay:diagram-minimum-extent diagram)
   (clay:button-margin diagram)
   (clay:diagram-window-rect (clay:diagram-parent diagram))
   :result (allowed-rect diagram)))

;;;------------------------------------------------------------

(defun node-randomize-position (diagram)
  (declare (type Node-Diagram diagram))
  ;; random initialization
  (g:random-rect (allowed-rect diagram) 
		    :result (clay:diagram-window-rect diagram)))

;;;------------------------------------------------------------
;;; This assumes graph is directed from left to right

(defun node-position-from-neighbors (node)
  (declare (type Node-Diagram node))
  ;; initialization to the mean of parents's rights and children's lefts
  (let ((x 0)
	(y 0)
	(w (brk:width node))
	(h (brk:height node))
	(n (+ (length (to-edges node))
	      (length (from-edges node)))))
    (declare (type az:Int16 x y w h)
	     (ignore h))
    (unless (zerop n)
      (dolist (e (to-edges node))
	(incf x (+ (brk:left (gra:edge-node0 e))
		   (brk:width (gra:edge-node0 e))))
	(incf y (brk:top (gra:edge-node0 e))))
      (dolist (e (from-edges node))
	(incf x (+ w (brk:left (gra:edge-node1 e))))
	(incf y (brk:top (gra:edge-node1 e))))
      (setf (brk:left node) (truncate x n))
      (setf (brk:top node) (truncate y n)))))

;;;------------------------------------------------------------

(defmethod g:l2-dist2 ((diagram0 Node-Diagram) (diagram1 Node-Diagram))
  (g:l2-dist2 (clay:diagram-window-rect diagram0)
	      (clay:diagram-window-rect diagram1)))

(defmethod g:l2-dist ((diagram0 Node-Diagram) (diagram1 Node-Diagram))
  (g:l2-dist (clay:diagram-window-rect diagram0)
	     (clay:diagram-window-rect diagram1)))

;;;============================================================
;;; Drawing
;;;============================================================

(defun move-node (diagram gnp pos)

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram diagram)
	   (type Node-Diagram gnp)
	   (type g:Point pos))

  
  (g:with-borrowed-rect (damaged-rect
			 (xlt:drawable-space (clay:diagram-window diagram)))
   (locally (declare (type g:Rect damaged-rect))
     (let* ((gnp-rect (clay:diagram-window-rect gnp))
	    (from-edges (from-edges gnp))
	    (to-edges (to-edges gnp)))
       (declare (type g:Rect gnp-rect)
		(type List from-edges to-edges))

       ;; erase node, set new node coordinates, repair damage, and draw
       (clay:erase-diagram gnp damaged-rect)
       (setf (g:x gnp-rect) (g:x pos))
       (setf (g:y gnp-rect) (g:y pos))
       (clay:repair-diagram diagram damaged-rect)
       (clay:draw-diagram gnp)

       ;; erase edges, repair damage, re-attach, and draw
       (let ((solver (clay:diagram-layout-solver diagram)))
	 (dolist (edge from-edges)
	   (declare (type Clay:Line-Diagram #||Edge-Diagram||# edge))
	   (clay:erase-diagram edge damaged-rect)
	   (attach-edge solver edge)
	   (clay:repair-diagram diagram damaged-rect)
	   (clay:draw-diagram edge))
	 (dolist (edge to-edges)
	   (declare (type Clay:Line-Diagram #||Edge-Diagram||# edge))
	   (clay:erase-diagram edge damaged-rect)
	   (attach-edge solver edge)
	   (clay:repair-diagram diagram damaged-rect)
	   (clay:draw-diagram edge))))))
  (xlt:drawable-force-output (clay:diagram-window diagram)))

;;;============================================================

(defmethod move-diagram-to ((diagram Node-Diagram) pos)
  (check-type pos g:point)
  ;; because this requires maintaining constraints amoung
  ;; the siblings of <diagram>, it must be handled by the parent
  (move-node (clay:diagram-parent diagram) diagram pos))

;;;============================================================
;;; Generic graph traversal functions
;;;============================================================

(defgeneric root-node? (gnp) #-sbcl (declare (type Node-Diagram gnp)))

(defmethod root-node? ((gnp Node-Diagram)) (null (to-edges gnp)))

(defgeneric leaf-node? (gnp) #-sbcl (declare (type Node-Diagram gnp)))

(defmethod leaf-node? ((gnp Node-Diagram)) (null (from-edges gnp)))

;;;============================================================

(defclass Node-Role (clay:Button-Role) ()
  (:documentation
   "A Node-Role handles input forwarded by the Graph-Diagram."))

;;;-----------------------------------------------------------

(defmethod ac:button-press ((role Node-Role)
			    msg-name sender receiver id
			    code x y state time root root-x root-y child
			    same-screen-p)

  "Pressing button 1 causes the graph-diagram to start dragging
this node. Button 2 toggles the pined state of this node. Button 3
brings up a menu for the node."

  (declare (type Node-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Node-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   state time root root-x root-y child same-screen-p))
  (setf (clay:button-state receiver) :down)
  (ecase code
    (1 (give-control-to-node-dragger receiver x y))
    (2 (toggle-pin-node receiver))
    (3 (clay:diagram-show-menu receiver x y)
       ;; make sure the button is up after the menu is done
       (setf (clay:button-state receiver) :up)))
  (values t))

;;;-----------------------------------------------------------

(defclass Labeled-Node-Diagram (Node-Diagram clay:Labeled-Button) ())

(defmethod initialize-instance :after ((diagram Labeled-Node-Diagram)
				       &rest initargs)
  (declare (ignore initargs))
  (setf (clay:label-string diagram)
    (let ((name (node-name (clay:diagram-subject diagram))))
      (if (stringp name)
	  name
	;; else
	(string-downcase (string name))))))

(defun make-node-diagram (subject parent)
  (declare (type T subject)
	   (type Graph-Diagram parent))
  (let ((nodegram (make-instance 'Labeled-Node-Diagram
		    :diagram-subject subject
		    :diagram-parent parent)))
    (declare (type Node-Diagram nodegram))
    (ac:start-msg-handling nodegram)
    nodegram))
