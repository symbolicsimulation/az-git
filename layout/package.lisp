;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Layout
  (:use :Common-Lisp #+:clos :CLOS)
  (:nicknames :Lay)
  (:export 
   Graph-Diagram
   make-graph-diagram
   graph-diagram-index-nodes
   graph-diagram-index-edges
   Graph-Mediator
   graph-changed
   Node-Diagram
   node-name
   Edge-Diagram
   make-edge-diagram
   graph-menu-items
   graph-node-menu-items
   graph-edge-menu-items
   graph-control-panel-items
   graph-node-control-panel-items
   graph-edge-control-panel-items
   ))

