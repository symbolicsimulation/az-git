;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================

(defclass NLC-Graph-Layout-Solver (Graph-Layout-Solver)
	  ((;; default to simpler energy function than default sovler
	    node-pair-energy-f
	    :type (or Null Symbol Function)
	    :accessor node-pair-energy-f
	    :initform 'spring-energy)
	   (spacing
	    :type Double-Float
	    :accessor spacing
	    :initform 8.0d0))
  (:documentation
   "A graph layout solver that uses nonlinear constraints."))

;;;------------------------------------------------------------
;;; Initializations of the layout solver that happen at diagram layout
;;; time, before the solver is called the first time

(defmethod update-layout-solver ((diagram Graph-Diagram)
				 (solver NLC-Graph-Layout-Solver))
  (let* ((nodes (gra:nodes diagram))
	 (n (length nodes)))
    (setf (clay:layout-domain solver)
      (brk:make-position-domain
       (intersection (brk:bricks (clay:layout-domain solver)) nodes)))
    ;;(setf (active-node-pairs solver) (gra:all-unordered-pairs nodes))
    (setf (node-distance-mtx solver)
      (ensure-large-enough-mtx (node-distance-mtx solver) n n))
    (setf (node-residual-mtx solver)
      (ensure-large-enough-mtx (node-residual-mtx solver) n n))
    (setf (clay:layout-constraints solver) 
      (clay:make-layout-constraints diagram solver))
    (compute-node-pair-distances diagram solver)
    (1/d2-weights diagram solver)
    solver))

;;;------------------------------------------------------------

(defun nlc-row-index (i j)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Array-Index i j))
  (values
   (the az:Array-Index
     (+ j (ash (* i (- i 1)) -1)))))

(defun nlc-pair-indexes (k)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Array-Index k))
  (let ((i (truncate (* 0.5d0 (+ (sqrt (az:fl (+ 1 (ash k 3)))) 1.0d0)))))
    (declare (type az:Array-Index i))
    (values i (the az:Array-Index (- k (ash (* i (- i 1)) -1))))))

;;;------------------------------------------------------------

(defmethod solve-layout ((diagram Graph-Diagram)
			 (solver NLC-Graph-Layout-Solver))

  (declare (type NLC-Graph-Layout-Solver solver))

  (let ((old-domain (clay:layout-domain solver))
	(value most-positive-double-float))
    (declare (type brk:Domain old-domain)
	     (type Double-Float value))
    (unwind-protect
	(progn
	  (setf (clay:layout-domain solver)
	    (brk:make-position-domain
	     (remove-if #'node-user-pinned? (brk:bricks old-domain))))
	  (setf (inactive-nodes solver)
	    (set-difference (gra:nodes diagram)
			    (brk:bricks (clay:layout-domain solver))))
	  (when (> (brk:dimension (clay:layout-domain solver)) 0)
	    (let* ((domain (clay:layout-domain solver))
		   (constraints (reduce-affine-constraints
				 (clay:layout-constraints solver)
				 domain))
		   (n (brk:dimension domain))
		   (nclin (length constraints))
		   (nnodes (length (brk:bricks domain)))
		   (ncnln (ash (* nnodes (- nnodes 1)) -1))
		   (x (npsol-x solver n))
		   (n+nclin+ncnln (+ n nclin ncnln))
		   (bl (npsol-bl solver n+nclin+ncnln))
		   (bu (npsol-bu solver n+nclin+ncnln))
		   (a (npsol-a solver n nclin)))
	      
	      (declare (type Graph-Diagram diagram)
		       (type az:Array-Index n nclin ncnln n+nclin+ncnln)
		       (type az:Float-Vector x bl bu)
		       (type az:Float-Matrix a))

	      (initialize-node-float-coordinates diagram)
	      (nodes->npsol (brk:bricks domain) x)
	      (bounds-constraints diagram solver bl bu)
	      (translate-affine-constraints constraints domain a bl bu)
	      (loop for i from (+ n nclin) below (+ n nclin ncnln) do
		    (setf (aref bl i) (spacing solver))
		    (setf (aref bu i) *npsol-infinity*))

	      ;; first pass with no nonlinear constraints
	      (multiple-value-bind (x0 objf objgrd inform iter)
		  (npsol:npsol-minimize
		   (npsol-default-options solver)
		   (npsol-objfn diagram solver)
		   (npsol-confn diagram solver)
		   n nclin 0 x a bl bu)
		(declare (ignore x0 objgrd inform iter))

		;; draw the final state, even if we not animating.
		(npsol->nodes x (brk:bricks domain))
		(attach-edges solver diagram)
		(setf value objf))
	      	      ;; 2nd pass with nonlinear constraints
	      #||
	      (multiple-value-bind (x0 objf objgrd inform iter)
		  (npsol:npsol-minimize
		   (npsol-default-options solver)
		   (npsol-objfn diagram solver)
		   (npsol-confn diagram solver)
		   n nclin ncnln x a bl bu)
		(declare (ignore x0 objgrd inform iter))

		;; draw the final state, even if we not animating.
		(npsol->nodes x (brk:bricks domain))
		(attach-edges solver diagram)
		(setf value objf))
	      ||#
))
	  ;; cleanup
	  (setf (clay:layout-domain solver) old-domain))
      (values value))))

;;;------------------------------------------------------------
;;; Energy calculation
;;;------------------------------------------------------------

(defmethod calDculate-energy ((solver NLC-Graph-Layout-Solver)
			     objf grad gradient? residual?)

  (declare (optimize (safety 0) (speed 3))
	   (type NLC-Graph-Layout-Solver solver)
	   (type az:Float-Vector objf grad)
	   (type az:Boolean  gradient? residual?))

  (let ((active-nodes (brk:bricks (clay:layout-domain solver)))
	(inactive-nodes (inactive-nodes solver)))
    (declare (type List active-nodes inactive-nodes))

    (setf (aref objf 0) 0.0d0)
    (when gradient? (zero-node-gradients active-nodes))

    (dolist (node0 active-nodes)
      (declare (type Node-Diagram node0))
      (dolist (node1 active-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? gradient?
		   residual? objf)))
      (dolist (node1 inactive-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? nil residual?
		   objf))))

    (when gradient? (nodes->objgrd active-nodes grad))))

;;;------------------------------------------------------------
;;; Nonlinear Constraint calculation
;;;------------------------------------------------------------

(defmethod npsol-confn ((diagram Graph-Diagram)
			(solver NLC-Graph-Layout-Solver))

  #'(lambda (mode ncnln n nrowj needc x c cjac nstate)
      (declare (optimize (safety 0) (speed 3))
	       (type (az:Fixnum-Vector 1) mode ncnln n nrowj nstate)
	       (type az:Fixnum-Vector needc)
	       (type az:Float-Vector x c)
	       (type az:Float-Matrix cjac))
      (list mode ncnln n nrowj needc x c cjac nstate)
      ))