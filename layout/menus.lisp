;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 
  
;;;============================================================
;;; Menus for graph browsers
;;;============================================================

(defgeneric graph-node-menu-items (graph node)
  #-sbcl (declare (type T graph node)
		  (:returns (type List menu-spec)))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <node> viewed as
a node in <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod graph-node-menu-items append ((graph T) (node T))
  "Returns no items."
  (declare (:returns (type List menu-spec)))
  ())

(defmethod graph-node-menu-items append ((graph gra:Graph) (node T))
  "Contributes a menu item for deleting nodes."
  (declare (:returns (type List menu-spec)))
  (list (list (format nil "Delete ~a from graph" node) 
	      #'gra:delete-nodes-and-edges (list node) () graph)))

;;;============================================================

(defgeneric graph-edge-menu-items (graph edge)
  #-sbcl (declare (type T graph edge)
		  (:returns (type List menu-spec)))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <edge> viewed as
an edge in <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod graph-edge-menu-items append ((graph T) (edge T))
  "Returns no items"
  (declare (:returns (type List menu-spec)))
  ())

(defmethod graph-edge-menu-items append ((graph gra:Graph) (edge T))
  "Contributes a menu item for deleting edges."
  (declare (:returns (type List menu-spec)))
  (list (list (format nil "Delete ~a from graph" edge) 
	      #'gra:delete-nodes-and-edges () (list edge) graph)))

;;;============================================================

(defgeneric graph-menu-items (graph)
  #-sbcl (declare (type T graph))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))


(defmethod graph-menu-items append ((graph gra:Graph))
  "Contributes no menu items."
  (declare (:returns ()))
  ())

(defmethod graph-menu-items append ((graph T))
  "Contributes no menu items."
  (declare (:returns ()))
  ())

;;;============================================================

(defmethod clay:diagram-menu-items append ((graph Graph-Diagram))
  "Contributes a menu item for resolving the layout."
  (declare (:returns (type List menu-items)))
  (let ((solver (clay:diagram-layout-solver graph)))
    (append 
     (graph-menu-items (clay:diagram-subject graph))
     (list 
      (list 
       "Solve Layout"
       #'(lambda ()
	   (clay:layout-diagram graph nil)
	   (ac:send-msg 'clay:role-redraw-diagram graph graph (ac:new-id))))
      (list (format nil "Inspect ~a" solver)
	    #'az:examine solver)))))

(defmethod clay:diagram-menu-items append ((node Node-Diagram))
  "Contributes a menu item for Pinning and Unpinning nodes."
  (declare (:returns (type List menu-spec)))
  (append (graph-node-menu-items 
	   (clay:diagram-subject (clay:diagram-parent node))
	   (clay:diagram-subject node))
	  (list (list (format nil "Toggle pin ~a" node)
		      #'toggle-pin-node node)) ))

(defmethod clay:diagram-menu-items append ((edge Edge-Diagram))
  "Contributes no menu items."
  (declare (:returns (type List menu-items)))
  (graph-edge-menu-items (clay:diagram-subject (clay:diagram-parent edge))
			 (clay:diagram-subject edge)))

;;;============================================================
;;; Control-Panels for graph browsers
;;;============================================================

(defgeneric graph-node-control-panel-items (graph node)
  #-sbcl (declare (type T graph node)
	   (:returns (type List menu-spec)))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <node> viewed as
a node in <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod graph-node-control-panel-items append ((graph T) (node T))
  "Returns no items."
  (declare (:returns (type List menu-spec)))
  ())

(defmethod graph-node-control-panel-items append ((graph gra:Graph) (node T))
  "Contributes a menu item for deleting nodes."
  (declare (:returns (type List menu-spec)))
  (list
   (list "Delete node" #'gra:delete-nodes-and-edges (list node) () graph)))

;;;============================================================

(defgeneric graph-edge-control-panel-items (graph edge)
  #-sbcl (declare (type T graph edge)
	   (:returns (type List menu-spec)))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <edge> viewed as
an edge in <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod graph-edge-control-panel-items append ((graph T) (edge T))
  "Returns no items"
  (declare (:returns (type List menu-spec)))
  ())

(defmethod graph-edge-control-panel-items append ((graph gra:Graph) (edge T))
  "Contributes a menu item for deleting edges."
  (declare (:returns (type List menu-spec)))
  (list (list "Delete edge" 
	      #'gra:delete-nodes-and-edges () (list edge) graph)))

;;;============================================================

(defgeneric graph-control-panel-items (graph)
  #-sbcl (declare (type T graph))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))


(defmethod graph-control-panel-items append ((graph gra:Graph))
  "Contributes no menu items."
  (declare (:returns ()))
  ())

(defmethod graph-control-panel-items append ((graph T))
  "Contributes no menu items."
  (declare (:returns ()))
  ())

;;;============================================================

(defmethod clay:diagram-control-panel-items append ((graph Graph-Diagram))
  "Contributes a control-panel item for resolving the layout."
  (declare (:returns (type List control-panel-items)))
  (let ((solver (clay:diagram-layout-solver graph)))
    (append 
     (graph-control-panel-items (clay:diagram-subject graph))
     (list 
      (list 
       "Solve Layout"
       #'(lambda ()
	   (clay:layout-diagram graph nil)
	   (ac:send-msg 'clay:role-redraw-diagram graph graph (ac:new-id))))
      (list "Inspect solver" #'az:examine solver)))))

(defmethod diagram-control-panel-items append ((node Node-Diagram))
  "Contributes a control-panel item for Pinning and Unpinning nodes."
  (declare (:returns (type List control-panel-spec)))
  (append (graph-node-control-panel-items
	   (clay:diagram-subject (clay:diagram-parent node))
	   (clay:diagram-subject node))
	  (list (list "Toggle Node Pin" #'toggle-pin-node node))))

(defmethod diagram-control-panel-items append ((edge Edge-Diagram))
  "Contributes no control-panel items."
  (declare (:returns (type List control-panel-items)))
  (graph-edge-control-panel-items 
   (clay:diagram-subject (clay:diagram-parent edge))
   (clay:diagram-subject edge)))
