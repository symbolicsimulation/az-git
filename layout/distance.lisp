;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================
;;; compute the number of edges in the shortest path between
;;; all pairs of Node-Diagrams in a Graph-Diagram.

;;; breadth first search (see \cite{Corm90}, p 470).
;;; Use node pens to mark the "color" of the nodes, to make it easy
;;; to animate the search. Use red, green, blue to substitute for
;;; white, gray, black.

#||
(defun bf-search-from-node (node0 dist0 q origin-index d-mtx)

  (declare (optimize (safety 0) (speed 3))
	   (type List edges)
	   (type Double-Float dist0)
	   (type Queue q)
	   (type az:Array-Index origin-index)
	   (type az:Float-Matrix d-mtx))

  (setf dist0 (+ dist0 1.0d0))
  (dolist (edge (from-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node1 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (node-mark node1) :red)
	(setf (node-mark node1) :green)
	(setf (aref d-mtx i1 origin-index) dist0)
	(setf (aref d-mtx origin-index i1) dist0)
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q))))

  (dolist (edge (to-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node0 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (node-mark node1) :red)
	(setf (node-mark node1) :green)
	(setf (aref d-mtx i1 origin-index) dist0)
	(setf (aref d-mtx origin-index i1) dist0)
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q)))))

(defun undirected-breadth-first-search (graph origin d-mtx)

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram graph)
	   (type Node-Diagram origin)
	   (type az:Float-Matrix d-mtx))
  
  (let ((origin-index (node-index origin))
	(q (ac:make-queue)))
    (declare (type az:Array-Index origin-index)
	     (type ac:Queue q))
    ;; initialization
    (setf (aref d-mtx origin-index origin-index) 0.0d0)
    (dolist (node (the List (gra:nodes graph)))
      (declare (type Node-Diagram node))
      (setf (node-mark node) :red)
      (setf (node-predecessor node) nil))
    (setf (node-mark origin) :green)
    (ac:enqueue origin q)

    (loop (when (ac:queue-empty? q) (return))
      (let* ((node0 (ac:dequeue q))
	     (i0 (node-index node0))
	     (dist0 (aref d-mtx origin-index i0)))
	(declare (type Node-Diagram node0)
		 (type az:Array-Index i0)
		 (type Double-Float dist0))
	(bf-search-from-node node0 dist0 q origin-index d-mtx)
	(setf (node-mark node0) :blue)))))
||#

(defun undirected-breadth-first-search (graph origin d-mtx)

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram graph)
	   (type Node-Diagram origin)
	   (type az:Float-Matrix d-mtx))
  
  (let ((origin-index (node-index origin))
	(q (ac:make-queue)))
    (declare (type az:Array-Index origin-index)
	     (type ac:Queue q))
    ;; initialization
    (setf (aref d-mtx origin-index origin-index) 0.0d0)
    (dolist (node (the List (gra:nodes graph)))
      (declare (type Node-Diagram node))
      (setf (node-mark node) :red)
      (setf (node-predecessor node) nil))
    (setf (node-mark origin) :green)
    (ac:enqueue origin q)

    (loop (when (ac:queue-empty? q) (return))
      (let* ((node0 (ac:dequeue q))
	     (i0 (node-index node0))
	     (d (+ 1.0d0 (aref d-mtx origin-index i0))))
	(declare (type Node-Diagram node0)
		 (type az:Array-Index i0)
		 (type Double-Float d))
	(dolist (edge (from-edges node0))
	  (declare (type Edge-Diagram edge))
	  (let* ((node1 (gra:edge-node1 edge))
		 (i1 (node-index node1)))
	    (declare (type Node-Diagram node1)
		     (type az:Array-Index i1))
	    (when (eq (node-mark node1) :red)
	      (setf (node-mark node1) :green)
	      (setf (aref d-mtx i1 origin-index) d)
	      (setf (aref d-mtx origin-index i1) d)
	      (setf (node-predecessor node1) node0)
	      (ac:enqueue node1 q))))

	(dolist (edge (to-edges node0))
	  (declare (type Edge-Diagram edge))
	  (let* ((node1 (gra:edge-node0 edge))
		 (i1 (node-index node1)))
	    (declare (type Node-Diagram node1)
		     (type az:Array-Index i1))
	    (when (eq (node-mark node1) :red)
	      (setf (node-mark node1) :green)
	      (setf (aref d-mtx i1 origin-index) d)
	      (setf (aref d-mtx origin-index i1) d)
	      (setf (node-predecessor node1) node0)
	      (ac:enqueue node1 q))))

	(setf (node-mark node0) :blue)))))

;;;============================================================

(defun animated-bf-search-from-node (node0 dist0 q origin-index d-mtx)

  (declare (optimize (safety 0) (speed 3))
	   (type Double-Float dist0)
	   (type ac:Queue q)
	   (type az:Array-Index origin-index)
	   (type az:Float-Matrix d-mtx))
  
  (dolist (edge (from-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node1 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (clay:diagram-gcontext node1)
		(xlt:colorname->hue-sat-gcontext
		 (clay:diagram-window node1) :red))
	(setf (clay:diagram-gcontext node1)
	  (xlt:colorname->hue-sat-gcontext
		 (clay:diagram-window node1) :green))
	(setf (aref d-mtx i1 origin-index)
	  (setf (aref d-mtx origin-index i1) (+ dist0 1.0d0)))
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q))))

  (dolist (edge (to-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node0 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (clay:diagram-gcontext node1)
		(xlt:colorname->hue-sat-gcontext
		 (clay:diagram-window node1) :red))
	(setf (clay:diagram-gcontext node1)
	  (setf (clay:diagram-gcontext node1)
	  (xlt:colorname->hue-sat-gcontext
		 (clay:diagram-window node1) :green)))
	(setf (aref d-mtx i1 origin-index)
	  (setf (aref d-mtx origin-index i1) (+ dist0 1.0d0)))
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q)))))

(defun animated-undirected-breadth-first-search (graph origin d-mtx)

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram graph)
	   (type Node-Diagram origin)
	   (type az:Float-Matrix d-mtx))
  
  (let ((origin-index (node-index origin))
	(q (ac:make-queue)))
    (declare (type az:Array-Index origin-index)
	     (type ac:Queue q))
    ;; initialization
    (setf (aref d-mtx origin-index origin-index) 0.0d0)
    (dolist (node (the List (gra:nodes graph)))
      (declare (type Node-Diagram node))
      (setf (clay:diagram-gcontext node)
	(xlt:colorname->hue-sat-gcontext (clay:diagram-window node) :red))
      (setf (node-predecessor node) nil))
    (setf (clay:diagram-gcontext origin)
      (xlt:colorname->hue-sat-gcontext (clay:diagram-window origin) :blue))
    (ac:enqueue origin q)

    (loop (when (ac:queue-empty? q) (return))
      (let* ((node0 (ac:dequeue q))
	     (i0 (node-index node0))
	     (dist0 (aref d-mtx origin-index i0)))
	(declare (type Node-Diagram node0)
		 (type az:Array-Index i0)
		 (type Double-Float dist0))
	(animated-bf-search-from-node node0 dist0 q origin-index d-mtx)
	(setf (clay:diagram-gcontext node0)
	  (xlt:colorname->hue-sat-gcontext (clay:diagram-window node0) :blue))))))


;;;============================================================

(defun compute-graph-distances (diagram solver &key (animate? nil))

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram diagram)
	   (type Graph-Layout-Solver solver)
	   (type az:Boolean animate?))

  (let* ((nodes (gra:nodes diagram))
	 (nnodes (length nodes))
	 (d-mtx (node-distance-mtx solver))
	 (unconnected-distance (az:c_sqrt (az:fl nnodes))))
    (declare (type List nodes)
	     (type az:Array-Index nnodes)
	     (type az:Float-Matrix d-mtx)
	     (type Double-Float unconnected-distance))
    (dotimes (i nnodes)
      (declare (type az:Array-Index i))
      (dotimes (j nnodes)
	(declare (type az:Array-Index j))
	(setf (aref d-mtx i j) unconnected-distance)))
    (if animate?
	(dolist (origin nodes)
	  (animated-undirected-breadth-first-search diagram origin d-mtx))
      ;; else
      (dolist (origin nodes)
	(undirected-breadth-first-search diagram origin d-mtx)))))

;;;============================================================

(defun relax-node-distances (solver amount)

  (let ((d-mtx (node-distance-mtx solver))
	(r-mtx (node-residual-mtx solver))
	(rest-len (rest-length solver)))
    (dotimes (i (array-dimension d-mtx 0))
      (dotimes (j i)
	(let* ((r ;;(min 0.2d0
		(* amount (/ (aref r-mtx i j) rest-len))
		;;     )
		)
	       (d (aref d-mtx i j)))
	  (when (and (> r 0.0d0)  (> d 1.0d0))
	    (setf (aref d-mtx i j)
	      (setf (aref d-mtx j i)
		(+ d r)))))))))