;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================
;;; Weights for the node pairs
;;;------------------------------------------------------------

(defun unit-weights (diagram solver)
  
  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram diagram)
	   (type Graph-Layout-Solver solver))
    
  (let* ((n (length (gra:nodes diagram)))
	 (w-mtx (node-weight-mtx solver)))
    (declare (type az:Float-Matrix w-mtx)
	     (type az:Array-Index n))
    (cond ((>= (array-dimension w-mtx 0) n)
	   (dotimes (i n)
	     (declare (type az:Array-Index i))
	     (dotimes (j i)
	       (declare (type az:Array-Index j))
	       (setf (aref w-mtx i j)
		 (setf (aref w-mtx j i) 1.0d0)))))
	  (t
	   (setf (node-weight-mtx solver)
	     (az:make-float-matrix n n :initial-element 1.0d0))))))

(defun 1/d-weights (diagram solver)
  
  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram diagram)
	   (type Graph-Layout-Solver solver))
    
  (let* ((n (length (gra:nodes diagram)))
	 (d-mtx (node-distance-mtx solver))
	 (w-mtx (node-weight-mtx solver)))
    (declare (type az:Float-Matrix d-mtx w-mtx)
	     (type az:Array-Index n))
    (unless (>= (array-dimension w-mtx 0) n)
      (setf w-mtx (make-array (list n n) :element-type 'Double-Float))
      (setf (node-weight-mtx solver) w-mtx))
    (dotimes (i n)
      (declare (type az:Array-Index i))
      (dotimes (j i)
	(declare (type az:Array-Index j))
	(let ((d (aref d-mtx i j)))
	  (setf (aref w-mtx i j)
	    (setf (aref w-mtx j i) (/ 1.0d0 d))))))))

(defun 1/d2-weights (diagram solver)
  
  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram diagram)
	   (type Graph-Layout-Solver solver))
    
  (let* ((n (length (gra:nodes diagram)))
	 (d-mtx (node-distance-mtx solver))
	 (w-mtx (node-weight-mtx solver)))
    (declare (type az:Float-Matrix d-mtx w-mtx)
	     (type az:Array-Index n))
    (setf w-mtx (ensure-large-enough-mtx w-mtx n n))
    (setf (node-weight-mtx solver) w-mtx)
    (dotimes (i n)
      (declare (type az:Array-Index i))
      (dotimes (j i)
	(declare (type az:Array-Index j))
	(let* ((d (aref d-mtx i j))
	      (1/d2 (/ 1.0d0 d d)))
	  (declare (type Double-Float d 1/d2))
	  (setf (aref w-mtx i j) 1/d2)
	  (setf (aref w-mtx j i) 1/d2))))))

