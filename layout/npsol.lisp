;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================

(defun npsol-default-options (solver)
  (declare (type clay:Layout-Solver solver))
  (let ((n (brk:dimension (clay:layout-domain solver))))
    (nconc
     (list "Nolist"
	   "Major Print Level = 0"
	   "Minor Print Level = 0")
     #||
     (cond ((animate? solver)
	    (list "Major Print Level = 0"))
	   (t
	    (list "Nolist"
		  "Major Print Level = 0"
		  "Minor Print Level = 0")))
     ||#
     (list
      "Verify Level = 0"
      "Derivative Level = 3"
      ;; "Difference Interval = 1.0d0"
      ;; "Central Difference Interval = 1.0d0"
  
      "Linesearch Tolerance  = 0.45d0"
      (format nil "Minor Iteration Limit = ~d" n)
      (format nil "Major Iteration Limit = ~d" n)
      "Optimality Tolerance = 1.0d-6"
      (format nil "Infinite Bound = ~f" *npsol-infinity*)))))

;;;------------------------------------------------------------
;;; Creating the objective function that gets passed to NPSOL
;;;------------------------------------------------------------

(defgeneric npsol-objfn (diagram solver)
  (declare (type clay:Diagram diagram)
	   (type clay:Layout-Solver solver)))

(defmethod npsol-objfn ((diagram clay:Diagram) (solver clay:Layout-Solver))
  (declare (type clay:Diagram diagram)
	   (type clay:Layout-Solver solver))
  (let ((domain (clay:layout-domain solver)))
    (declare (type brk:Domain domain))

    #'(lambda (mode n x objf objgrd nstate)
	(declare (optimize (safety 0) (speed 3))
		 (type (az:Fixnum-Vector 1) mode)
		 (type (az:Fixnum-Vector 1) n)
		 (type az:Float-Vector x)
		 (type (az:Float-Vector 1) objf)
		 (type az:Float-Vector objgrd)
		 (type (az:Fixnum-Vector 1) nstate)	       
		 (ignore n nstate))
	
	(print (list 'npsol-objfn-internal mode n x objf objgrd nstate))
	(finish-output)
	(check-type mode (az:Fixnum-Vector 1))
	(check-type n (az:Fixnum-Vector 1))
	(check-type x az:Float-Vector)
	(check-type objf (az:Float-Vector 1))
	(check-type objgrd az:Float-Vector)
	(check-type nstate (az:Fixnum-Vector 1))	       

	;; first update the state of the graph diagram
	;;(when (animate? solver) (clay:erase-diagram diagram nil))
	(npsol->nodes x (brk:bricks domain))
	(when (animate? solver)
	  (attach-edges solver diagram)
	  (clay:redraw-diagram diagram))
	(ecase (aref mode 0)
	  (0 (calculate-energy solver objf objgrd nil nil))
	  (1 (calculate-energy solver objf objgrd t nil))
	  (2 (calculate-energy solver objf objgrd t nil))))))

(defgeneric npsol-confn (diagram solver)
  (declare (type clay:Diagram diagram)
	   (type clay:Layout-Solver solver)))

(defmethod npsol-confn ((diagram clay:Diagram) (solver clay:Layout-Solver))
  (declare (type clay:Diagram diagram)
	   (type clay:Layout-Solver solver))

  #'(lambda (mode ncnln n nrowj needc x c cjac nstate)
      (declare (optimize (safety 0) (speed 3))
	       (type (az:Fixnum-Vector 1) mode ncnln n nrowj nstate)
	       (type az:Fixnum-Vector needc)
	       (type az:Float-Vector x c)
	       (type az:Float-Matrix cjac))
  
      (format t "~%Lisp constraint function:")
      (map nil #'print (list mode ncnln n nrowj needc x c cjac nstate))
      (finish-output *standard-output*)))
