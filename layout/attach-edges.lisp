;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================
;;; Computing edge attachments
;;;------------------------------------------------------------
;;; different solvers may use different strategies for how
;;; to connect nodes by edges

(defun attach-edges (solver gp &rest options)

  (declare (type Graph-Layout-Solver solver)
	   (type Graph-Diagram gp)
	   (ignore options))

  (dolist (edge (the List (gra:edges gp)))
    (declare (type Edge-Diagram edge))
    (attach-edge solver edge)))
	    
;;;------------------------------------------------------------

(defun attach-edge (solver edge)

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Layout-Solver solver)
	   (type Edge-Diagram edge))

  (funcall (edge-attacher-f solver)
	   (clay:diagram-window-rect (gra:edge-node0 edge))
	   (clay:diagram-window-rect (gra:edge-node1 edge))
	   (clay:from-point edge)
	   (clay:to-point edge)
	   (clay:diagram-window-rect edge)))

;;;------------------------------------------------------------
;;; destructively modifies <edge-pos0>, <edge-pos1>,
;;; and  <edge-rect>.

(defun attach-shortest-edge (node-rect0 node-rect1
			     edge-pos0 edge-pos1 edge-rect)

  (declare (optimize (safety 0) (speed 3))
	   (type g:Rect node-rect0 node-rect1 edge-rect)
	   (type g:Point edge-pos0 edge-pos1))

  (let* ((x0 (g:x node-rect0))
	 (y0 (g:y node-rect0))
	 (w0 (g:w node-rect0))
	 (h0 (g:h node-rect0))
	 (x1 (g:x node-rect1))
	 (y1 (g:y node-rect1))
	 (w1 (g:w node-rect1))
	 (h1 (g:h node-rect1))
	 (xmin0 (- x0 1))
	 (xmax0 (+ xmin0 w0))
	 (ymin0 (- y0 1))
	 (ymax0 (+ ymin0 h0))
	 (xmin1 (- x1 1))
	 (xmax1 (+ xmin1 w1))
	 (ymin1 (- y1 1))
	 (ymax1 (+ ymin1 h1))	 
	 (midpoint 0))
    (declare (type az:Int16 x0 y0 w0 h0 x1 y1 w1 h1
		   xmin0 xmax0 ymin0 ymax0 xmin1 xmax1 ymin1 ymax1
		   midpoint))
    (cond ((< xmax0 xmin1) ;; 0 interval is left of 1 interval
	   (setf (g:x edge-pos0) xmax0)
	   (setf (g:x edge-pos1) xmin1)
	   (setf (g:x edge-rect) xmax0)
	   (setf (g:w edge-rect) (- xmin1 xmax0)))
	  ((< xmax1 xmin0) ;; 0 interval is right of 1 interval
	   (setf (g:x edge-pos0) xmin0)
	   (setf (g:x edge-pos1) xmax1)
	   (setf (g:x edge-rect) xmax1)
	   (setf (g:w edge-rect) (- xmin0 xmax1)))
	  (t ;; intervals intersect
	   (setf midpoint (ash (+ (max xmin0 xmin1) (min xmax0 xmax1)) -1))
	   (setf (g:x edge-pos0) midpoint)
	   (setf (g:x edge-pos1) midpoint)
	   (setf (g:x edge-rect) midpoint)
	   (setf (g:w edge-rect) 1)))
    (cond ((< ymax0 ymin1) ;; 0 interval is left of 1 interval
	   (setf (g:y edge-pos0) ymax0)
	   (setf (g:y edge-pos1) ymin1)
	   (setf (g:y edge-rect) ymax0)
	   (setf (g:h edge-rect) (- ymin1 ymax0)))
	  ((< ymax1 ymin0) ;; 0 interval is right of 1 interval
	   (setf (g:y edge-pos0) ymin0)
	   (setf (g:y edge-pos1) ymax1)
	   (setf (g:y edge-rect) ymax1)
	   (setf (g:h edge-rect) (- ymin0 ymax1)))
	  (t ;; intervals intersect
	   (setf midpoint (ash (+ (max ymin0 ymin1) (min ymax0 ymax1)) -1))
	   (setf (g:y edge-pos0) midpoint)
	   (setf (g:y edge-pos1) midpoint)
	   (setf (g:y edge-rect) midpoint)
	   (setf (g:h edge-rect) 1)))))

;;;------------------------------------------------------------
;;; destructively modifies <edge-pos0>, <edge-pos1>,
;;; and  <edge-rect>.

(defun attach-centers (node-rect0 node-rect1
		       edge-pos0 edge-pos1 edge-rect)

  (declare (optimize (safety 0) (speed 3))
	   (type g:Rect node-rect0 node-rect1 edge-rect)
	   (type g:Point edge-pos0 edge-pos1))

  (let* ((x0 (ash (+ (the az:Int16 (g:x node-rect0))
		     (the az:Int16 (g:xmax node-rect0)))
		  -1))
	 (y0 (ash (+ (the az:Int16 (g:y node-rect0))
		     (the az:Int16 (g:ymax node-rect0)))
		  -1))
	 (x1 (ash (+ (the az:Int16 (g:x node-rect1))
		     (the az:Int16 (g:xmax node-rect1)))
		  -1))
	 (y1 (ash (+ (the az:Int16 (g:y node-rect1))
		     (the az:Int16 (g:ymax node-rect1)))
		  -1)))

    (declare (type az:Int16 x0 y0 x1 y1))

    (setf (g:x edge-pos0) x0)
    (setf (g:y edge-pos0) y0)
    (setf (g:x edge-pos1) x1)
    (setf (g:y edge-pos1) y1)

    (cond ((< x0 x1)
	   (setf (g:x edge-rect) (+ 1 x0))
	   (setf (g:w edge-rect) (- x1 x0 1)))
	  (t	   
	   (setf (g:x edge-rect) (+ 1 x1))
	   (setf (g:w edge-rect) (- x0 x1 1))))

    (cond ((< y0 y1)
	   (setf (g:y edge-rect) (+ y0 1))
	   (setf (g:h edge-rect) (- y1 y0 1)))
	  (t	   
	   (setf (g:y edge-rect) (+ 1 y1))
	   (setf (g:h edge-rect) (- y0 y1 1))))))

;;;============================================================
;;; Clipping
;;;============================================================
;;; Clip line against clipping-rect.  Implements
;;; Cohen-Sutherland clipping.  From Foley and Van Dam, pg.146 

(declaim (inline clip-code))

(defun clip-code (x y xmin xmax ymax ymin)       
  ;; Returns Cohen-Sutherland clip code for point (x,y).
  ;; This version assumes that xmin < xmax, etc.
  (declare (optimize (safety 0) (speed 3))
	   (type az:Int16 x y xmin xmax ymax ymin))
  (the (Integer 0 8)
    (logior (the (Integer 0 8) (cond ((> y ymax) 8) ((> ymin y) 4) (t 0)))
	    (the (Integer 0 8) (cond ((> x xmax) 2) ((> xmin x) 1) (t 0))))))

#||
(defun clip-code (x y xmin xmax ymax ymin)       
  ;; returns Cohen-Sutherland clip code for point (x,y). 
  (let ((abovebit (if (> y ymax) 8 0))
	(belowbit (if (> ymin y) 4 0))
	(xmaxbit (if (> x xmax) 2 0))
	(xminbit (if (> xmin x) 1 0)))
    (logior abovebit belowbit xmaxbit xminbit)))
||#

;;;------------------------------------------------------------

(defun clip-line (x0 y0 x1 y1 clipping-rect)

  "Clip line against <clipping-rect>.  Implements Cohen-Sutherland
clipping.  From Foley and Van Dam, pg.146 returns four values: new
<x0>, <y0>, <x1>, <y1> of line clipped to <clipping-rect> if it
returns nil, the line is outside <clipping-rect> note: the direction
of the line is maintained (from <x0>,<y0> to <x1>,<y1>)"

  (declare (optimize (safety 0) (speed 3))
	   (type az:Int16 x0 y0 x1 y1)
	   (type (or Null g:Rect) clipping-rect)
	   (:returns (values (type (or Null az:Int16) x0)
			     (type (or Null az:Int16) y0)
			     (type (or Null az:Int16) x1)
			     (type (or Null az:Int16) y1))))
  (if (null clipping-rect)
      (values x0 y0 x1 y1)
    (let* ((rect clipping-rect) ;; to make sure decl takes hold
	   (clip-xmin (g:x rect))
	   (clip-ymin (g:y rect))
	   (clip-xmax  (+ -1 clip-xmin (the az:Int16 (g:w rect))))
	   (clip-ymax (+ -1 clip-ymin (the az:Int16 (g:h rect))))
	   (accept-p nil)
	   (done-p nil)
	   (reversed-p nil)
	   (code-1 -1)
	   (code-2 -1))
      (declare (type g:Rect rect)
	       (type az:Int16 clip-xmin clip-ymin clip-xmax clip-ymax)
	       (type az:Boolean accept-p done-p reversed-p)
	       (type Fixnum code-1 code-2))
      (loop
	(if done-p (return nil))
	(setf code-1 (clip-code x0 y0 clip-xmin clip-xmax clip-ymax clip-ymin))
	(setf code-2 (clip-code x1 y1 clip-xmin clip-xmax clip-ymax clip-ymin))
	(if (= 0 (logand code-1 code-2))
	    ;; Possible accept
	    (if (= 0 (logior code-1 code-2))
		;; then accept
		(progn (setf accept-p t)
		       (setf done-p t))
	      ;; else find intersections
	      (progn 
		(when (= 0 code-1)
		  ;; Swap points so (X0 Y0) is guaranteed to be outside 
		  (rotatef x0 x1)
		  (rotatef y0 y1)
		  (rotatef code-1 code-2)
		  (setf reversed-p t))
		(cond
		 ((/= 0 (logand code-1 8)) ;; divide line at ymax
		  (incf x0
			(the az:Int16
			  (truncate
			   (the Fixnum (* (the az:Int16 (- x1 x0))
					  (the az:Int16 (- clip-ymax y0))))
			   (the az:Int16 (- y1 y0)))))
		  (setf y0 clip-ymax))
		 ((not (= 0 (logand code-1 4)))
		  ;; divide line at ymin
		  (incf x0
			(the az:Int16
			  (truncate
			   (the Fixnum (* (the az:Int16 (- x1 x0))
					  (the az:Int16 (- clip-ymin y0))))
			   (the az:Int16 (- y1 y0)))))
		  (setf y0 clip-ymin))
		 ((/= 0 (logand code-1 2)) ;; divide line at xmax
		  (incf y0
			(the az:Int16
			  (truncate
			   (the Fixnum (* (the az:Int16 (- y1 y0))
					  (the az:Int16 (- clip-xmax x0))))
			   (the az:Int16 (- x1 x0)))))
		  (setf x0 clip-xmax))
		 (t ;; divide line at xmin
		  (incf y0
			(the az:Int16
			  (truncate
			   (the Fixnum (* (the az:Int16 (- y1 y0))
					  (the az:Int16 (- clip-xmin x0))))
			   (the az:Int16 (- x1 x0)))))
		  (setf x0 clip-xmin)))))
	  ;; Reject
	  (setf done-p t)))
      (when reversed-p ;; reverse it back
	(rotatef x0 x1)
	(rotatef y0 y1))
      (if accept-p (values x0 y0 x1 y1)	(values nil)))))
  
;;;------------------------------------------------------------
;;; destructively modifies <edge-pos0>, <edge-pos1>,
;;; and  <edge-rect>.

(defun attach-centers-clipped (node-rect0 node-rect1
			       edge-pos0 edge-pos1
			       edge-rect)

  (declare (optimize (safety 0) (speed 3))
	   (type g:Rect node-rect0 node-rect1 edge-rect)
	   (type g:Point edge-pos0 edge-pos1))

  (let* ((cx0 (ash (+ (the az:Int16 (g:x node-rect0))
		      (the az:Int16 (g:xmax node-rect0)))
		   -1))
	 (cy0 (ash (+ (the az:Int16 (g:y node-rect0))
		      (the az:Int16 (g:ymax node-rect0)))
		   -1))
	 (cx1 (ash (+ (the az:Int16 (g:x node-rect1))
		      (the az:Int16 (g:xmax node-rect1)))
		   -1))
	 (cy1 (ash (+ (the az:Int16 (g:y node-rect1))
		      (the az:Int16 (g:ymax node-rect1)))
		   -1)))

    ;; clip edge to outside of node rects
    (multiple-value-bind (x0 y0)
	(clip-line cx1 cy1 cx0 cy0 node-rect0)
      (declare (type az:Int16 x0 y0))
      (multiple-value-bind (x1 y1)
	  (clip-line cx0 cy0 cx1 cy1 node-rect1)
	(declare (type az:Int16 x1 y1))
	(let ((dx (- x1 x0))
	      (dy (- y1 y0)))
	  (declare (type az:Int16 dx dy))
	  (cond ((> dx  1) (incf x0 2) (decf x1 2))
		((> dx  0) (incf x0 1) (decf x1 1))
		((< dx -1) (decf x0 2) (incf x1 2))
		((< dx  0) (decf x0 1) (incf x1 1)))
	  (cond ((> dy  1) (incf y0 2) (decf y1 2))
		((> dy  0) (incf y0 1) (decf y1 1))
		((< dy -1) (decf y0 2) (incf y1 2))
		((< dy  0) (decf y0 1) (incf y1 1))))
	
	(setf (g:x edge-pos0) x0)
	(setf (g:y edge-pos0) y0)
	(setf (g:x edge-pos1) x1)
	(setf (g:y edge-pos1) y1)

	(cond ((< x0 x1)
	       (setf (g:x edge-rect) (+ 1 x0))
	       (setf (g:w edge-rect) (- x1 x0 1)))
	      ((= x0 x1)
	       (setf (g:x edge-rect) (+ 1 x0))
	       (setf (g:w edge-rect) 0))
	      (t	   
	       (setf (g:x edge-rect) (+ 1 x1))
	       (setf (g:w edge-rect) (- x0 x1 1))))

	(cond ((< y0 y1)
	       (setf (g:y edge-rect) (+ y0 1))
	       (setf (g:h edge-rect) (- y1 y0 1)))
	      ((= y0 y1)
	       (setf (g:y edge-rect) (+ y0 1))
	       (setf (g:h edge-rect) 0))
	      (t	   
	       (setf (g:y edge-rect) (+ 1 y1))
	       (setf (g:h edge-rect) (- y0 y1 1))))))))



