;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================

(defclass Graph-Layout-Solver (clay:Layout-Solver)
	  ((inactive-nodes
	    :type List
	    :accessor inactive-nodes
	    :initform ())
	   #||
	   (active-node-pairs
	    :type List
	    :accessor active-node-pairs
	    :initform ())
	   ||#

	   (node-pair-energy-f
	    :type (or Null Symbol Function)
	    :accessor node-pair-energy-f
	    :initform 'sphered-spring-energy)

	   (node-pair-distance-f
	    :type (or Null Symbol Function)
	    :accessor node-pair-distance-f
	    :initform ())
	   (node-distance-mtx
	    :type (or Null az:Float-Matrix)
	    :accessor node-distance-mtx
	    :initform nil)

	   (x-order-f
	    :type (or Null Symbol Function)
	    :accessor x-order-f
	    :initform nil)
	   (y-order-f
	    :type (or Null Symbol Function)
	    :accessor y-order-f
	    :initform nil)
	
	   (node-weight-mtx
	    :type (or Null az:Float-Matrix)
	    :accessor node-weight-mtx
	    :initform nil)
	   (node-residual-mtx
	    :type (or Null az:Float-Matrix)
	    :accessor node-residual-mtx
	    :initform nil)
	   (node-weight-f
	    :type az:Funcallable
	    :accessor node-weight-f
	    :initform '1/d2-weights)

	   (active-edge-pairs
	    :type List
	    :accessor active-edge-pairs
	    :initform ())
	   (edge-attacher-f
	    :type az:Funcallable
	    :accessor edge-attacher-f
	    :initform 'attach-centers-clipped)
	   (edge-distance-mtx
	    :type (or Null az:Float-Matrix)
	    :accessor edge-distance-mtx
	    :initform nil)
	   (edge-pair-energy-f
	    :type Function
	    :accessor edge-pair-energy-f
	    :initform 'edge-repulsion)

	   (rest-length
	    :type Double-Float
	    :accessor rest-length
	    :initform 10.0d0)
	   (spring-constant
	    :type Double-Float
	    :accessor spring-constant
	    :initform 1.0d0)
	   (sigma
	    :type Double-Float
	    :accessor sigma
	    :initform 1.0d0)

	   (animate?
	    :type az:Boolean 
	    :accessor animate?
	    :initform nil)))

;;;============================================================
;;; initialization

(defmethod clay:layout-solver-class ((diagram Graph-Diagram))
  #+:npsol 'NPSOL-Graph-Layout-Solver
  #-:npsol 'Graph-Layout-Solver)

;;;------------------------------------------------------------
;;; Initializations of the layout solver that happen at diagram layout
;;; time, before the solver is called the first time

(defmethod clay:make-layout-domain ((diagram Graph-Diagram)
				    (solver Graph-Layout-Solver))
  (brk:make-position-domain (gra:nodes diagram)))

(defmethod clay:make-layout-cost ((diagram Graph-Diagram)
				  (solver Graph-Layout-Solver))
  (brk:default-cost (clay:layout-domain solver)))

(defmethod clay:make-layout-constraints ((diagram Graph-Diagram)
					 (solver Graph-Layout-Solver))
  (nconc (when (x-order-f solver)
	   (make-order-constraints diagram (x-order-f solver) :x 16.0d0))
	 (when (y-order-f solver) 
	   (make-order-constraints diagram (y-order-f solver) :y 16.0d0))))

;;;------------------------------------------------------------

(defun compute-node-pair-distances (diagram solver)
  (declare (optimize (safety 0) (speed 3))
	   (type Graph-diagram diagram)
	   (type Graph-Layout-Solver solver))
  (if (null (node-pair-distance-f solver))
    (compute-graph-distances diagram solver)
    ;; else
    (let ((d-mtx (node-distance-mtx solver))
	  (d-f (node-pair-distance-f solver))
	  (inactive-nodes (inactive-nodes solver)))
      (declare (type az:Float-Matrix d-mtx)
	       (type az:Funcallable d-f)
	       (type List inactive-nodes))
      (do ((bricks (brk:bricks (clay:layout-domain solver)) (rest bricks)))
	  ((null bricks))
	(declare (type List bricks))
	(let ((node0 (first bricks)))
	  (declare (type Node-Diagram node0))
	  ;; distance to other active nodes
	  (dolist (node1 (rest bricks))
	    (let* ((i0 (node-index node0))
		   (i1 (node-index node1))
		   (d (funcall d-f 
			       (clay:diagram-subject node0) 
			       (clay:diagram-subject node1))))
	      (declare (type T node0 node1)
		       (type az:Array-Index i0 i1)
		       (type Double-Float d))
	      (setf (aref d-mtx i0 i1) d)
	      (setf (aref d-mtx i1 i0) d)))
	  ;; distance to inactive nodes
	  (dolist (node1 inactive-nodes)
	    (let* ((i0 (node-index node0))
		   (i1 (node-index node1))
		   (d (funcall d-f 
			       (clay:diagram-subject node0)
			       (clay:diagram-subject node1))))
	      (declare (type T node0 node1)
		       (type az:Array-Index i0 i1)
		       (type Double-Float d))
	      (setf (aref d-mtx i0 i1) d)
	      (setf (aref d-mtx i1 i0) d)))))))
  ;; scale the d-mtx so that a unit distance becomes the solver's rest-length
  (scale-mtx (rest-length solver) (node-distance-mtx solver)))

;;;------------------------------------------------------------

(defmethod clay:update-layout-solver ((diagram Graph-Diagram)
				      (solver Graph-Layout-Solver))
  (let* ((nodes (gra:nodes diagram))
	 (n (length nodes)))
    (setf (clay:layout-domain solver)
      (if (clay:layout-domain solver)
	(brk:make-position-domain
	 (intersection (brk:bricks (clay:layout-domain solver)) nodes))
	(clay:make-layout-domain diagram solver)))
    (setf (node-distance-mtx solver)
      (ensure-large-enough-mtx (node-distance-mtx solver) n n))
    (setf (node-residual-mtx solver)
      (ensure-large-enough-mtx (node-residual-mtx solver) n n))
    (setf (clay:layout-constraints solver) 
      (clay:make-layout-constraints diagram solver))
    (compute-node-pair-distances diagram solver)
    (1/d2-weights diagram solver)
    solver))

;;;----------------------------------------------------------

(defmethod clay:solve-layout ((diagram Graph-Diagram)
			      (solver Graph-Layout-Solver))

  (declare (type Graph-Layout-Solver solver))

  (let ((old-domain (clay:layout-domain solver))
	(value most-positive-double-float))
    (declare (type brk:Domain old-domain)
	     (type Double-Float value))
    (unwind-protect
	(progn
	  (setf (clay:layout-domain solver)
	    (brk:make-position-domain
	     (remove-if #'node-user-pinned? (brk:bricks old-domain))))
	  (setf (inactive-nodes solver)
	    (set-difference (gra:nodes diagram)
			    (brk:bricks (clay:layout-domain solver))))
	  (when (> (brk:dimension (clay:layout-domain solver)) 0)
	    (let* ((domain (clay:layout-domain solver))
		   (cost (brk:default-cost (clay:layout-domain solver)))
		   (constraints (reduce-affine-constraints
				 (clay:layout-constraints solver)
				 domain)))
	      (multiple-value-bind
		  (case objf) (brk:solve-constraints cost constraints domain)
		(declare (ignore case))
		(setf value objf))
	      ;; draw the final state, even if we not animating.
	      (attach-edges solver diagram))))
      ;; cleanup
      (setf (clay:layout-domain solver) old-domain))
    (values value)))
 


