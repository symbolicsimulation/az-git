;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 
  
;;;============================================================
;;; An example of a class that uses graph-diagram's

(defmethod default-diagram-class ((graph gra:Graph))
  (find-class 'Graph-Diagram))

;;;============================================================

(defclass Graph-Diagram (Clay:Root-Diagram)
	  ((gra:nodes
	    :type List
	    :accessor gra:nodes
	    :initform ())
	   (gra:edges
	    :type List
	    :accessor gra:edges
	    :initform ())
	   (initialize-node-positions?
	    :type az:Boolean
	    :accessor initialize-node-positions?
	    :initform t)
	   (initial-node-configuration
	    :type (or Null az:Int16-Vector)
	    :accessor initial-node-configuration)
	   ;; who controls the layout of this diagram?

	   ;; who handles the input to this diagram in node dragging mode?
	   (node-dragging-role
	    :type Node-Dragging-Role
	    :accessor node-dragging-role)))
 
;;;------------------------------------------------------------
;;; Traversing the diagram tree
;;;------------------------------------------------------------

(defmethod clay:map-over-children (result-type function 
				   (diagram Graph-Diagram))
  (cond
   ((null result-type)
    (dolist (node (gra:nodes diagram)) (funcall function node))
    (dolist (edge (gra:edges diagram)) (funcall function edge)))
   (t
    (concatenate result-type
      (map result-type function (gra:nodes diagram))
      (map result-type function (gra:edges diagram))))))

(defmethod clay:some-child? (function (diagram Graph-Diagram))
  (or (some function (gra:nodes diagram))
      (some function (gra:edges diagram))))

(defmethod clay:find-child-if (function (diagram Graph-Diagram))
  (let ((found (find-if function (gra:nodes diagram))))
    (when (null found)
      (setf found (find-if function (gra:edges diagram))))
    found))

;;;------------------------------------------------------------
;;; Initialization
;;;------------------------------------------------------------

;;;------------------------------------------------------------
;;; Building
;;;------------------------------------------------------------

(defgeneric edge-class (diagram)
  #-sbcl (declare (type Graph-Diagram diagram)))

(defmethod edge-class ((diagram Graph-Diagram))
  (if (gra:directed-graph? (clay:diagram-subject diagram))
      (find-class 'Dedge-Diagram)
    ;; else
    (find-class 'Edge-Diagram)))

;;;------------------------------------------------------------

(defun make-edge-diagram (subject parent)
  (declare (type T subject)
	   (type Graph-Diagram parent))
  (let* ((gep (make-instance (edge-class parent)
		:diagram-subject subject
		:diagram-parent parent))
	 (gnp0 (find (gra:edge-node0 subject)
		     (gra:nodes parent) :key #'clay:diagram-subject))
	 (gnp1 (find (gra:edge-node1 subject)
		     (gra:nodes parent) :key #'clay:diagram-subject)))
    (pushnew gep (from-edges gnp0))
    (pushnew gep (to-edges gnp1))
    (setf (gra:edge-node0 gep) gnp0)
    (setf (gra:edge-node1 gep) gnp1)
    gep))

;;;------------------------------------------------------------

(defmethod clay:build-children ((diagram Graph-Diagram))
  (let* ((diagram-subject (clay:diagram-subject diagram))
	 (snodes (gra:nodes diagram-subject))
	 (sedges (gra:edges diagram-subject)))
    ;; make diagrams for the nodes in the graph
    (setf (gra:nodes diagram)
      (az:with-collection
	  (dolist (snode snodes)
	    (az:collect (make-node-diagram snode diagram)))))
    (graph-diagram-index-nodes diagram)

    ;; make diagrams for the edges in the graph
    ;; and recreate the graph structure
    ;; in the diagram nodes and edges
    (setf (gra:edges diagram)
      (az:with-collection
	  (dolist (sedge sedges)
	    (az:collect (make-edge-diagram sedge diagram)))))
    (graph-diagram-index-edges diagram)))

;;;------------------------------------------------------------

(defmethod graph-diagram-index-nodes ((diagram Graph-Diagram))
  (let ((i -1))
    (dolist (node (gra:nodes diagram))
      (setf (node-index node) (incf i)))))

;;;------------------------------------------------------------

(defmethod graph-diagram-index-edges ((diagram Graph-Diagram))
  (let ((i -1))
    (dolist (edge (gra:edges diagram))
      (setf (edge-index edge) (incf i)))))

;;;------------------------------------------------------------

(defun initialize-node-float-coordinates (diagram)

  (declare (type Graph-Diagram diagram))

  (dolist (node (the List (gra:nodes diagram)))
    (let ((r (clay:diagram-window-rect node))
	  (pnt (node-pnt node))
	  (rad (node-rad node))
	  (1/rad (node-1/rad node)))
      (declare (type g:Rect r)
	       (type az:Float-Vector pnt rad 1/rad))
      (setf (aref pnt 0) (az:fl (g:x r)))
      (setf (aref pnt 1) (az:fl (g:y r)))
      (setf (aref rad 0) (* 0.5d0 (g:w r)))
      (setf (aref rad 1) (* 0.5d0 (g:h r)))
      (setf (aref 1/rad 0) (/ 1.0d0 (g:w r)))
      (setf (aref 1/rad 1) (/ 1.0d0 (g:h r))))))

;;;------------------------------------------------------------

(defmethod ac:build-roles ((diagram Graph-Diagram))
  (setf (clay:diagram-unmapped-role diagram)
    (make-instance 'clay:Unmapped-Role :role-actor diagram))
  (setf (node-dragging-role diagram)
    (make-instance 'Node-Dragging-Role :role-actor diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Graph-Role :role-actor diagram)))

;;;------------------------------------------------------------
;;; Layout
;;;------------------------------------------------------------

(defmethod clay:layout-diagram ((diagram Graph-Diagram) layout-options)
  (let ((rect (clay:diagram-window-rect diagram))
	(solver (clay:diagram-layout-solver diagram))
	(initial-node-configuration
	 (getf layout-options :initial-node-configuration)))
    (declare (type g:Rect rect))
    (setf (g:x rect) 0)
    (setf (g:y rect) 0)
    (setf (g:w rect)
      (xlib:drawable-width (clay:diagram-window diagram)))
    (setf (g:h rect)
      (xlib:drawable-height (clay:diagram-window diagram)))
    (unless (null initial-node-configuration)
      (assert (- (length initial-node-configuration)
		 (* 2 (length (gra:nodes diagram))))))
    (setf (initial-node-configuration diagram) initial-node-configuration)
    (clay:layout-children diagram)
    (setf (x-order-f solver)
      (getf layout-options :x-order-f (x-order-f solver)))
    (setf (y-order-f solver)
      (getf layout-options :y-order-f (y-order-f solver)))
    (setf (node-pair-distance-f solver)
      (getf layout-options :node-pair-distance-f
	    (node-pair-distance-f solver)))
    (when (getf layout-options :rest-length)
      (setf (rest-length solver)
	(az:fl (getf layout-options :rest-length (rest-length solver)))))
    (when (> (length (gra:nodes diagram)) 0)
      (clay:update-layout-solver diagram solver)
      (clay:solve-layout diagram solver)))
  (clay:update-internal diagram nil)
  diagram)

;;;------------------------------------------------------------
;;; Transforming between representations of graph layout
;;;------------------------------------------------------------
;;; Transforming from the state of Graph-Diagram
;;; to the alternate representation of that state used
;;; by NPSOL (a simple array of double floats)

(defun %nodes->npsol (nodes x)

  (declare (optimize (safety 0) (speed 3))
	   (type List nodes)
	   (type az:Float-Vector x))
  
  (let ((i -1))
    (declare (type Fixnum i))
    (dolist (node nodes)
      ;; (declare (type Node-Diagram node))
      (let ((pnt (node-pnt node)))
	(declare (type az:Float-Vector pnt))
	(setf (aref x (incf i)) (aref pnt 0))
	(setf (aref x (incf i)) (aref pnt 1)))))
  x)

(defun nodes->npsol (nodes x)
  (az:type-check List nodes)
  (az:type-check az:Float-Vector x)
  (assert (<= (* 2 (length nodes)) (length x)))
  (%nodes->npsol nodes x))

;;;------------------------------------------------------------
;;; Transforming from the state of Graph-Diagram
;;; to the alternate representation of that state used
;;; by NPSOL (a simple array of double floats)

(defparameter *p* (make-array 2 :element-type 'Double-Float))
(declaim (type (az:Float-Vector 2) *p*))

(defparameter *ip* (make-array 2 :element-type 'Fixnum))
(declaim (type (az:Fixnum-Vector 2) *ip*))

(defun %npsol->nodes (a nodes)

  (declare (optimize (safety 0) (speed 3))
	   (type az:Float-Vector a)
	   (type List nodes))

  ;; first set node point and rect
  (let ((i -1))
    (declare (type Fixnum i))
    (dolist (node nodes)
      (declare (type Node-Diagram node))
      (let ((r (clay:diagram-window-rect node))
	    (pnt (node-pnt node))
	    (rad (node-rad node)))
	(declare (type g:Rect r)
		 (type az:Float-Vector pnt rad))
	(setf (aref *p* 0) (aref a (incf i)))
	(setf (aref *p* 1) (aref a (incf i)))
	(az:c_trunc2 *p* *ip*)
	(setf (aref pnt 0) (+ (aref *p* 0) (aref rad 0)))
	(setf (aref pnt 1) (+ (aref *p* 1) (aref rad 1)))
	(setf (g:x r) (aref *ip* 0))
	(setf (g:y r) (aref *ip* 1)))))
  nodes)

(defun npsol->nodes (x nodes)
  (az:type-check Vector x)
  (az:type-check List nodes)
  (assert (<= (* 2 (length nodes)) (length x)))
  (%npsol->nodes x nodes))

;;;------------------------------------------------------------

(defmethod clay:layout-children ((diagram Graph-Diagram))

  (dolist (node (gra:nodes diagram)) (clay:layout-diagram node nil))

  (when (initialize-node-positions? diagram)
    (setf (initialize-node-positions? diagram) nil)
    (if (null (initial-node-configuration diagram))
	;; put the node is random places
	(dolist (node (gra:nodes diagram)) (node-randomize-position node))
      ;; else use the supplied starting position
      (npsol->nodes (initial-node-configuration diagram) (gra:nodes diagram))))
  
  (dolist (edge (gra:edges diagram)) (clay:layout-diagram edge nil)))

;;;------------------------------------------------------------
;;; Drawing and Erasing
;;;------------------------------------------------------------

(defmethod clay:draw-diagram ((diagram Graph-Diagram))
  (dolist (d (gra:edges diagram)) (clay:draw-diagram d))
  (dolist (d (gra:nodes diagram)) (clay:draw-diagram d)))

;;;------------------------------------------------------------

(defmethod clay:erase-diagram ((diagram Graph-Diagram) rect)
  (declare (type (or Null g:Rect) rect))
  (dolist (d (gra:nodes diagram)) (clay:erase-diagram d nil))
  (dolist (d (gra:edges diagram)) (clay:erase-diagram d nil))
  (unless (null rect)
    (az:copy (clay:diagram-window-rect diagram) :result rect)))

;;;------------------------------------------------------------

(defmethod clay:repair-diagram ((diagram Graph-Diagram) damaged-rect)

  (declare (optimize (safety 0) (speed 3))
	   (type Graph-Diagram diagram)
	   (type g:Rect damaged-rect))

  (when (clay:intersect-diagram? diagram damaged-rect)
    (dolist (edge (the List (gra:edges diagram)))
      (clay:repair-diagram edge damaged-rect))
    (dolist (node (the List (gra:nodes diagram)))
      (clay:repair-diagram node damaged-rect))))

;;;------------------------------------------------------------
;;; Input
;;;------------------------------------------------------------
;;; internal method used to find the diagram of a given diagram-subject

(defmethod %find-diagram-of (subj (diagram Graph-Diagram))
  (if (eql subj (clay:diagram-subject diagram))
      (throw :search-done diagram)
      ;; else
      (dolist (node (gra:nodes diagram) nil)
	(when (eql subj (clay:diagram-subject node))
	  (throw :search-done node)))))

;;;------------------------------------------------------------
;;; Updating in response to changes in the graph
;;;------------------------------------------------------------

(defmethod gra:add-nodes-and-edges (nodes edges (diagram Graph-Diagram))
  (declare (type List nodes edges))

  (let ((new-nodes (set-difference nodes (gra:nodes diagram)))
	(new-edges (set-difference edges (gra:edges diagram))))
     (setf (gra:nodes diagram)
       (nunion (gra::collect-nodes-from-edges new-edges)
	       (concatenate 'List new-nodes (gra:nodes diagram))))
    (setf (gra:edges diagram) (nconc new-edges (gra:edges diagram)))

    (dolist (edge new-edges)
      ;; make sure nodes know about edges
      (pushnew edge (from-edges (gra:edge-node0 edge)))
      (pushnew edge (to-edges (gra:edge-node1 edge))))

    (graph-diagram-index-nodes diagram)
    (graph-diagram-index-edges diagram))
  diagram)

(defmethod gra:delete-nodes-and-edges (nodes edges (diagram Graph-Diagram))
  "Delete the listed nodes and edges from the graph."
  (declare (type List nodes edges))

  (flet ((delete-edge? (edge)
	   (let ((delete? nil)
		 (node0 (gra:edge-node0 edge))
		 (node1 (gra:edge-node1 edge)))
	     (dolist (node nodes)
	       ;; fix the nodes here (a little sneaky)
	       (when (or (eql node node0) (eql node node1))
		 (setf (from-edges node0) (delete edge (from-edges node0)))
		 (setf (to-edges node1) (delete edge (to-edges node1)))
		 (setf delete? t)))
	     delete?)))
    (setf (gra:edges diagram)
      (nset-difference (delete-if #'delete-edge? (gra:edges diagram)) edges)))
  ;; fix the nodes in the other edges
  (dolist (edge edges)
    (let ((node0 (gra:edge-node0 edge))
	  (node1 (gra:edge-node1 edge)))
      (setf (from-edges node0) (delete edge (from-edges node0)))
      (setf (to-edges node1) (delete edge (to-edges node1)))))

  (setf (gra:nodes diagram) (nset-difference (gra:nodes diagram) nodes))

  (graph-diagram-index-nodes diagram)
  (graph-diagram-index-edges diagram)
  ;; kill the node and edge diagrams to free related data
  (map nil #'az:kill-object nodes)
  (map nil #'az:kill-object edges)
  diagram)

;;;============================================================

(defgeneric graph-changed (role msg-name sender receiver id)
  #-sbcl (declare (type (or clay:Diagram-Role clay:Mediator-Role clay:Diagram) role)
		  (type az:Funcallable msg-name)
		  (type T sender)
		  (type T receiver)
		  (type ac:Msg-Id id))
  (:documentation
   "A method for mediators should cause the diagrams to be updated
properly. A method for diagrams should analyze the diagram-subject to see how
it has changed and update the diagram accordingly.")) 

(defmethod graph-changed ((role clay:Diagram-Role) msg-name sender receiver id)
  (declare (type clay:Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type T receiver)
	   (type ac:Msg-Id id))
  (graph-changed (ac:role-actor role) msg-name sender receiver id))

(defmethod graph-changed ((diagram Graph-Diagram) msg-name sender receiver id)
  (declare (type Graph-Diagram diagram)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type T receiver)
	   (type ac:Msg-Id id)
	   (ignore msg-name sender receiver id))
  (let* ((graph (clay:diagram-subject diagram))
	 (nodes (gra:nodes graph))
	 (edges (gra:edges graph))
	 (presented-nodes (mapcar #'clay:diagram-subject (gra:nodes diagram)))
	 (presented-edges (mapcar #'clay:diagram-subject (gra:edges diagram)))
	 (added-nodes (set-difference nodes presented-nodes))
	 (added-edges (set-difference edges presented-edges))
	 (deleted-nodes (set-difference presented-nodes nodes))
	 (deleted-edges (set-difference presented-edges edges))
	 (added-gnps
	  (mapcar #'(lambda (node) (make-node-diagram node diagram))
		  added-nodes))
	 (added-geps ())
	 (deleted-gnps
	  (remove-if
	   #'(lambda (gnp)
	       (not (member (clay:diagram-subject gnp) deleted-nodes)))
	   (gra:nodes diagram)))
	 (deleted-geps
	  (remove-if
	   #'(lambda (gep)
	       (not (member (clay:diagram-subject gep) deleted-edges)))
	   (gra:edges diagram)))
	 
	 (old-domain 
	  (clay:layout-domain (clay:diagram-layout-solver diagram))))
    (declare (type List
		   nodes edges presented-nodes presented-edges
		   added-nodes added-edges deleted-nodes deleted-edges
		   added-gnps added-geps deleted-gnps deleted-geps)
	     (type brk:Domain old-domain))

    ;; first delete
    (gra:delete-nodes-and-edges deleted-gnps deleted-geps diagram)

    ;; Must add node diagrams before making the edge diagrams,
    ;; because <make-edge-diagram> expects a node-diagram
    ;; in the graph-diagram for both of its diagram-subject nodes.
    (gra:add-nodes-and-edges added-gnps () diagram)
    ;; now make and add the edge diagrams
    (setf added-geps
      (mapcar #'(lambda (edge) (make-edge-diagram edge diagram))
	      added-edges))
    (gra:add-nodes-and-edges () added-geps diagram)

    ;; simple layout and redraw
    (unless (and (null added-gnps) (null added-geps))
      (mapc #'node-position-from-neighbors added-gnps)
      (setf (clay:layout-domain (clay:diagram-layout-solver diagram))
	(brk:make-position-domain added-gnps))
      (clay:layout-diagram diagram nil))
    (setf (clay:layout-domain (clay:diagram-layout-solver diagram))
      (brk:make-position-domain
       (set-difference
	(concatenate 'List added-gnps (brk:bricks old-domain))
	deleted-gnps)))
    (ac:send-msg 'clay:role-redraw-diagram diagram diagram (ac:new-id))  
    diagram))



