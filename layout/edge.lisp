;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================

(defclass Edge-Diagram (Clay:Line-Diagram)
	  ((gra:edge-node0
	    :type Node-Diagram
	    :accessor gra:edge-node0)
	   (gra:edge-node1
	    :type Node-Diagram
	    :accessor gra:edge-node1)
	   (edge-index
	    :type az:Array-Index
	    :accessor edge-index)))

;;;============================================================

(defmethod clay:diagram-name ((edge Edge-Diagram))
  (format nil "EdgeP ~a -- ~a"
	  (gra:edge-node0 edge)
	  (gra:edge-node1 edge)))

(defmethod print-object ((edge Edge-Diagram) stream)
  (declare (type Stream stream))
  (az:printing-random-thing
   (edge stream)
   (format stream "EdgeP ~a -- ~a"
	   (gra:edge-node0 edge)
	   (gra:edge-node1 edge))))

;;;------------------------------------------------------------

(defmethod clay:diagram-layout-solver ((diagram Edge-Diagram))
  (clay:diagram-layout-solver (clay:diagram-parent diagram)))

;;;------------------------------------------------------------

(defmethod az:equal? ((e0 Edge-Diagram)
		      (e1 Edge-Diagram))
  (and (eq (gra:edge-node0 e0) (gra:edge-node0 e1))
       (eq (gra:edge-node1 e0) (gra:edge-node1 e1))))

;;;============================================================
;;; Building
;;;============================================================

;;;============================================================
;;; Layout
;;;============================================================

(defmethod clay:layout-diagram ((gep Edge-Diagram) layout-options)
  (declare (ignore layout-options))
  (attach-edge (clay:diagram-layout-solver (clay:diagram-parent gep)) gep)
  gep)

;;;------------------------------------------------------------

(defmethod edge-l2-norm2 ((gep Edge-Diagram))
  (g:l2-norm2 (g:extent (clay:diagram-window-rect gep))))

(defmethod edge-l2-norm ((gep Edge-Diagram))
  (g:l2-norm (g:extent (clay:diagram-window-rect gep))))

;;;============================================================
;;; For Directed Graphs:
;;;============================================================

(defclass Dedge-Diagram (Edge-Diagram clay:Arrow-Diagram) ())

;;;------------------------------------------------------------

(defmethod print-object ((edge Dedge-Diagram) stream)
  (declare (type Stream stream))
  (az:printing-random-thing
   (edge stream)
   (format stream "DedgeP ~a -> ~a"
	   (gra:edge-node0 edge)
	   (gra:edge-node1 edge))))