;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass NPSOL-Graph-Layout-Solver (Graph-Layout-Solver)
	  (;; save arguments to npsol-minimize
	   (npsol-x
	    :type (Simple-Array Double-Float (*))
	    :initform '#(0.0d0))
	   (npsol-bl
	    :type (Simple-Array Double-Float (*))
	    :initform '#(0.0d0))
	   (npsol-bu
	    :type (Simple-Array Double-Float (*))
	    :initform '#(0.0d0))
	   (npsol-a
	    :type (Simple-Array Double-Float (* *))
	    :initform '#((0.0d0)))))

;;;------------------------------------------------------------
;;; caches for arguments to npsol

(defun npsol-x (solver n)
  (declare (type NPSOL-Graph-Layout-Solver solver)
	   (type az:Array-Index n))
  (setf (slot-value solver 'npsol-x)
    (ensure-large-enough-vector (slot-value solver 'npsol-x) n)))

(defun npsol-bl (solver n+nclin)
  (declare (type NPSOL-Graph-Layout-Solver solver)
	   (type az:Array-Index n+nclin))
  (setf (slot-value solver 'npsol-bl)
    (ensure-large-enough-vector (slot-value solver 'npsol-bl) n+nclin)))

(defun npsol-bu  (solver n+nclin)
  (declare (type NPSOL-Graph-Layout-Solver solver)
	   (type az:Array-Index n+nclin))
  (setf (slot-value solver 'npsol-bu)
    (ensure-large-enough-vector (slot-value solver 'npsol-bu) n+nclin)))

(defun npsol-a (solver n nclin)
  (declare (type NPSOL-Graph-Layout-Solver solver)
	   (type az:Array-Index n nclin))
  (setf (slot-value solver 'npsol-a)
    (ensure-large-enough-mtx (slot-value solver 'npsol-a) n nclin)))

;;;------------------------------------------------------------

(defmethod solve-layout ((solver NPSOL-Graph-Layout-Solver))

  (declare (optimize (safety 3) (speed 0))
	   (type NPSOL-Graph-Layout-Solver solver))

  (let ((old-domain (brk:domain solver))
	(value most-positive-double-float))
    (declare (type brk:Domain old-domain)
	     (type Double-Float value))
    (unwind-protect
	(progn
	  (setf (brk:domain solver)
	    (brk:make-position-domain
	     (remove-if #'node-user-pinned? (brk:bricks old-domain))))
	  (setf (inactive-nodes solver)
	    (set-difference (gra:nodes (diagram solver))
			    (brk:bricks (brk:domain solver))))
	  (when (> (brk:dimension (brk:domain solver)) 0)
	    (let* ((diagram (diagram solver))
		   (constraints (reduce-affine-constraints
				 (constraints solver)
				 (brk:domain solver)))
		   (n (brk:dimension (brk:domain solver)))
		   (nclin (length constraints))
		   (n+nclin (+ n nclin))
		   (x (npsol-x solver n))
		   (bl (npsol-bl solver n+nclin))
		   (bu (npsol-bu solver n+nclin))
		   (a (npsol-a solver n nclin)))
	      
	      (declare (type Graph-Diagram diagram)
		       (type az:Array-Index n nclin n+nclin)
		       (type (Simple-Array Double-Float (*)) x bl bu)
		       (type (Simple-Array Double-Float (* *)) a))

	      (xlt:clear-drawable (diagram-window diagram))
	      (initialize-node-float-coordinates diagram)
	      (nodes->position-vector (brk:bricks (brk:domain solver)) x)
	      (bounds-constraints solver bl bu)
	      (translate-affine-constraints
	       constraints (brk:domain solver) a bl bu)

	      (multiple-value-bind (x0 objf objgrd inform iter)
		  (npsol:npsol-minimize
		   (npsol-default-options solver) (npsol-objfn solver)
		   (npsol-confn solver) n nclin 0 x a bl bu)
		(declare (ignore x0 objgrd inform iter))

		;; draw the final state, even if we not animating.
		(position-vector->nodes x (brk:bricks (brk:domain solver)))
		(attach-edges solver diagram)
		(xlt:clear-drawable (diagram-window diagram))
		(draw-diagram diagram)
		(xlt:drawable-force-output (diagram-window diagram))
		(setf value objf))))
	  ;; cleanup
	  (setf (brk:domain solver) old-domain))
      (values value))))

;;;------------------------------------------------------------

(defun npsol-default-options (solver)
  (declare (type NPSOL-Graph-Layout-Solver solver))
  (let ((n (brk:dimension (brk:domain solver))))
    (nconc
     #||
     (list "Nolist"
	   "Major Print Level = 0"
	   "Minor Print Level = 0")
     ||#
     (list 
	   "Major Print Level = 10"
	   "Minor Print Level = 10")
     (list
      "Verify Level = 3"
      "Derivative Level = 3"
      ;; "Difference Interval = 1.0d0"
      ;; "Central Difference Interval = 1.0d0"
  
      "Linesearch Tolerance  = 0.45d0"
      (format nil "Minor Iteration Limit = ~d" n)
      (format nil "Major Iteration Limit = ~d" n)
      "Optimality Tolerance = 1.0d-6"
      (format nil "Infinite Bound = ~f" *npsol-infinity*)))))

;;;------------------------------------------------------------
;;; Creating the objective function that gets passed to NPSOL
;;;------------------------------------------------------------

(defgeneric npsol-objfn (solver)
  (declare (type NPSOL-Graph-Layout-Solver solver)))

(defmethod npsol-objfn ((solver NPSOL-Graph-Layout-Solver))
  (declare (type NPSOL-Graph-Layout-Solver solver))
  (let ((diagram (diagram solver))
	(domain (brk:domain solver)))
    (declare (type Graph-Diagram diagram)
	     (type brk:Domain domain))

    #'(lambda (mode n x objf objgrd nstate)
	(declare (optimize (safety 3) (speed 0))
		 (type (Simple-Array Fixnum (1)) mode)
		 (type (Simple-Array Fixnum (1)) n)
		 (type (Simple-Array Double-Float (*)) x)
		 (type (Simple-Array Double-Float (1)) objf)
		 (type (Simple-Array Double-Float (*)) objgrd)
		 (type (Simple-Array Fixnum (1)) nstate)	       
		 (ignore n nstate))

	;; first update the state of the graph diagram
	(when (animate? solver) (erase-diagram diagram nil))
	(position-vector->nodes x (brk:bricks domain))
	(when (animate? solver)
	  (attach-edges solver diagram)
	  (draw-diagram diagram)
	  (xlt:drawable-force-output (diagram-window diagram)))
	(ecase (aref mode 0)
	  (0 (calculate-energy solver objf objgrd nil nil))
	  (1 (calculate-energy solver objf objgrd t nil))
	  (2 (calculate-energy solver objf objgrd t nil))))))

(defgeneric npsol-confn (solver)
  (declare (type NPSOL-Graph-Layout-Solver solver)))

(defmethod npsol-confn ((solver NPSOL-Graph-Layout-Solver))
  (declare (type NPSOL-Graph-Layout-Solver solver))

  #'(lambda (mode ncnln n nrowj needc x c cjac nstate)
      (declare (optimize (safety 3) (speed 0))
	       (type (Simple-Array Fixnum (1)) mode ncnln n nrowj nstate)
	       (type (Simple-Array Fixnum (*)) needc)
	       (type (Simple-Array Double-Float (*)) x c)
	       (type (Simple-Array Double-Float (* *)) cjac))
  
      (format t "~%Lisp constraint function:")
      (map nil #'print (list mode ncnln n nrowj needc x c cjac nstate))
      (finish-output *standard-output*)))

#||
;;;============================================================

(defclass NLC-Graph-Layout-Solver (Graph-Layout-Solver)
	  ((;; default to simpler energy function than default sovler
	    node-pair-energy-f
	    :type (or Null Symbol Function)
	    :accessor node-pair-energy-f
	    :initform 'spring-energy)
	   (spacing
	    :type Double-Float
	    :accessor spacing
	    :initform 8.0d0))
  (:documentation
   "A graph layout solver that uses nonlinear constraints."))

;;;------------------------------------------------------------
;;; Initializations of the layout solver that happen at diagram layout
;;; time, before the solver is called the first time

(defmethod initialize-layout-solver ((solver NLC-Graph-Layout-Solver)
				     layout-options)
  (declare (type List layout-options)
	   (ignore layout-options))
  (let* ((nodes (gra:nodes (diagram solver)))
	 (n (length nodes)))
    (setf (brk:domain solver)
      (brk:make-position-domain
       (if (null (brk:domain solver))
	   (copy-list nodes)
	 ;; else
	 (intersection (brk:bricks (brk:domain solver)) nodes))))
    ;;(setf (active-node-pairs solver) (gra:all-unordered-pairs nodes))
    (setf (node-distance-mtx solver)
      (ensure-large-enough-mtx (node-distance-mtx solver) n n))
    (setf (node-residual-mtx solver)
      (ensure-large-enough-mtx (node-residual-mtx solver) n n))
    (setf (constraints solver) (funcall (affine-constraint-f solver) solver))
    (compute-node-pair-distances solver)
    (1/d2-weights solver)
    solver))

;;;------------------------------------------------------------

(defun nlc-row-index (i j)
  (declare (optimize (safety 3) (speed 0))
	   (type az:Array-Index i j))
  (values
   (the az:Array-Index
     (+ j (ash (* i (- i 1)) -1)))))

(defun nlc-pair-indexes (k)
  (declare (optimize (safety 3) (speed 0))
	   (type az:Array-Index k))
  (let ((i (truncate (* 0.5d0 (+ (sqrt (az:fl (+ 1 (ash k 3)))) 1.0d0)))))
    (declare (type az:Array-Index i))
    (values i (the az:Array-Index (- k (ash (* i (- i 1)) -1))))))

;;;------------------------------------------------------------

(defmethod solve-layout ((solver NLC-Graph-Layout-Solver))

  (declare (type NLC-Graph-Layout-Solver solver))

  (let ((old-domain (brk:domain solver))
	(value most-positive-double-float))
    (declare (type brk:Domain old-domain)
	     (type Double-Float value))
    (unwind-protect
	(progn
	  (setf (brk:domain solver)
	    (brk:make-position-domain
	     (remove-if #'node-user-pinned? (brk:bricks old-domain))))
	  (setf (inactive-nodes solver)
	    (set-difference (gra:nodes (diagram solver))
			    (brk:bricks (brk:domain solver))))
	  (when (> (brk:dimension (brk:domain solver)) 0)
	    (let* ((domain (brk:domain solver))
		   (diagram (diagram solver))
		   (constraints (reduce-affine-constraints
				 (constraints solver)
				 domain))
		   (n (brk:dimension domain))
		   (nclin (length constraints))
		   (nnodes (length (brk:bricks domain)))
		   (ncnln (ash (* nnodes (- nnodes 1)) -1))
		   (x (npsol-x solver n))
		   (n+nclin+ncnln (+ n nclin ncnln))
		   (bl (npsol-bl solver n+nclin+ncnln))
		   (bu (npsol-bu solver n+nclin+ncnln))
		   (a (npsol-a solver n nclin)))
	      
	      (declare (type Graph-Diagram diagram)
		       (type az:Array-Index n nclin ncnln n+nclin+ncnln)
		       (type (Simple-Array Double-Float (*)) x bl bu)
		       (type (Simple-Array Double-Float (* *)) a))

	      (erase-diagram diagram nil)
	      (initialize-node-float-coordinates diagram)
	      (nodes->position-vector (brk:bricks domain) x)
	      (bounds-constraints solver bl bu)
	      (translate-affine-constraints constraints domain a bl bu)
	      (loop for i from (+ n nclin) below (+ n nclin ncnln) do
		(setf (aref bl i) (spacing solver))
		(setf (aref bu i) *npsol-infinity*))

	      ;; first pass with no nonlinear constraints
	      (multiple-value-bind (x0 objf objgrd inform iter)
		  (npsol:npsol-minimize
		   (npsol-default-options solver)
		   (npsol-objfn solver)
		   (npsol-confn solver)
		   n nclin 0 x a bl bu)
		(declare (ignore x0 objgrd inform iter))

		;; draw the final state, even if we not animating.
		(position-vector->nodes x (brk:bricks domain))
		(attach-edges solver diagram)
		(erase-diagram diagram nil)
		(draw-diagram diagram)
		(xlt:drawable-force-output (diagram-window diagram))
		(setf value objf))

	      ;; 2nd pass with nonlinear constraints
	      #||
	      (multiple-value-bind (x0 objf objgrd inform iter)
		  (npsol:npsol-minimize
		   (npsol-default-options solver)
		   (npsol-objfn solver)
		   (npsol-confn solver)
		   n nclin ncnln x a bl bu)
		(declare (ignore x0 objgrd inform iter))

		;; draw the final state, even if we not animating.
		(position-vector->nodes x (brk:bricks domain))
		(attach-edges solver diagram)
		(erase-diagram diagram nil)
		(draw-diagram diagram)
		(xlt:drawable-force-output (diagram-window diagram))
		(setf value objf))
	      ||#
	      ))
	  ;; cleanup
	  (setf (brk:domain solver) old-domain))
      (values value))))

;;;------------------------------------------------------------
;;; Energy calculation
;;;------------------------------------------------------------

(defmethod calculate-energy ((solver NLC-Graph-Layout-Solver)
			     objf grad gradient? residual?)

  (declare (optimize (safety 3) (speed 0))
	   (type NLC-Graph-Layout-Solver solver)
	   (type (Simple-Array Double-Float (*)) objf grad)
	   (type (Member t nil)  gradient? residual?))

  (let ((active-nodes (brk:bricks (brk:domain solver)))
	(inactive-nodes (inactive-nodes solver)))
    (declare (type List active-nodes inactive-nodes))

    (setf (aref objf 0) 0.0d0)
    (when gradient? (zero-node-gradients active-nodes))

    (dolist (node0 active-nodes)
      (declare (type Node-Diagram node0))
      (dolist (node1 active-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? gradient?
		   residual? objf)))
      (dolist (node1 inactive-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? nil residual?
		   objf))))

    (when gradient? (nodes-gradient-vector active-nodes grad))))

;;;------------------------------------------------------------
;;; Nonlinear Constraint calculation
;;;------------------------------------------------------------

(defmethod npsol-confn ((solver NLC-Graph-Layout-Solver))

  #'(lambda (mode ncnln n nrowj needc x c cjac nstate)
      (declare (optimize (safety 3) (speed 0))
	       (type (Simple-Array Fixnum (1)) mode ncnln n nrowj nstate)
	       (type (Simple-Array Fixnum (*)) needc)
	       (type (Simple-Array Double-Float (*)) x c)
	       (type (Simple-Array Double-Float (* *)) cjac))
      (list mode ncnln n nrowj needc x c cjac nstate)
      ))

||#