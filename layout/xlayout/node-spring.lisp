;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defun estimate-solver-rest-length (solver)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver))

  (let* ((window (diagram-window (diagram solver)))
	 (w (xlib:drawable-width window))
	 (h (xlib:drawable-height window)))
    (declare (type xlib:Window window)
	     (type g:Screen-Coordinate w h))
    (* 0.95d0
       (/
	;;(sqrt (az:fl (+ (* w w) (* h h))))
	;;(az:fl (max w h))
	;;(az:fl (min w h))
	(* (+ w h) 0.5d0)
	(mtx-max (node-distance-mtx solver))))))


;;;------------------------------------------------------------
;;; an arg to pass to a_sqrt, to save consing a double-float box

(declaim (type (Simple-Array Double-Float (1)) *d*))
(defparameter *d* (make-array 1 :element-type 'Double-Float))

;;;------------------------------------------------------------

(defun spring-energy (solver node0 node1 grad0? grad1? residual? objf)
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type Node-Diagram node0 node1)
	   (type (Member t nil) grad0? grad1? residual?)
	   (type (Simple-Array Double-Float (*)) objf))
  (let* ((pnt0 (node-pnt node0))
	 (pnt1 (node-pnt node1))
	 (d-mtx (node-distance-mtx solver))
	 (w-mtx (node-weight-mtx solver))
	 (r-mtx (node-residual-mtx solver))
	 (i0 (node-index node0))
	 (i1 (node-index node1))
	 (l (aref d-mtx i0 i1))
	 (dx (- (aref pnt0 0) (aref pnt1 0)))
	 (dy (- (aref pnt0 1) (aref pnt1 1)))
	 (d-l 0.0d0)
	 (w (aref w-mtx i0 i1)))
    (declare (type (Simple-Array Double-Float (*)) pnt0 pnt1)
	     (type (Simple-Array Double-Float (* *)) d-mtx w-mtx r-mtx)
	     (type az:Array-Index i0 i1)
	     (type (Double-Float 0.0d0 *) l)
	     (type Double-Float dx dy d-l w))
    (setf (aref *d* 0) (+ (* dx dx) (* dy dy)))
    (az:a_sqrt *d*)
    (setf d-l (- (aref *d* 0) l))

    (when residual? (setf (aref r-mtx i0 i1) (setf (aref r-mtx i1 i0) d-l)))
    (when (or grad0? grad1?)
      (if (plusp (aref *d* 0))
	  (let* ((alpha (/ (* 2.0d0 w d-l) (aref *d* 0)))
		 (gx (* alpha dx))
		 (gy (* alpha dy)))
	    (declare (type Double-Float alpha gx gy))
	    (when grad0?
	      (let ((grd0 (node-grd node0)))
		(declare (type (Simple-Array Double-Float (*)) grd0))
		(incf (aref grd0 0) gx)
		(incf (aref grd0 1) gy)))
	    (when grad1?
	      (let ((grd1 (node-grd node1)))
		(declare (type (Simple-Array Double-Float (*)) grd1))
		(decf (aref grd1 0) gx)
		(decf (aref grd1 1) gy))))
	;; else compute limit of grad as d -> 0
	(let* ((alpha (* 2.0d0 w d-l))
	       (gx (* alpha dx))
	       (gy (* alpha dy)))
	  (declare (type Double-Float alpha gx gy))
	  (when grad0?
	    (let ((grd0 (node-grd node0)))
	      (declare (type (Simple-Array Double-Float (*)) grd0))
	      (incf (aref grd0 0) gx)
	      (incf (aref grd0 1) gy)))
	  (when grad1?
	    (let ((grd1 (node-grd node1)))
	      (declare (type (Simple-Array Double-Float (*)) grd1))
	      (decf (aref grd1 0) gx)
	      (decf (aref grd1 1) gy))))))
    (incf (aref objf 0) (* w d-l d-l)))
  (values))

;;;------------------------------------------------------------

(defun sphered-spring-energy (solver node0 node1 grad0? grad1? residual? objf)
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type Node-Diagram node0 node1)
	   (type (Member t nil) grad0? grad1? residual?)
	   (type (Simple-Array Double-Float (*)) objf)
	   (inline sqrt #+:excl excl::fld_sqrt))
  (let* ((pnt0 (node-pnt node0))
	 (1/rad0 (node-1/rad node0))
	 (i0 (node-index node0))
	 (pnt1 (node-pnt node1))
	 (1/rad1 (node-1/rad node1))
	 (i1 (node-index node1))
	 (d-mtx (node-distance-mtx solver))
	 (w-mtx (node-weight-mtx solver))
	 (r-mtx (node-residual-mtx solver))
	 (w (aref w-mtx i0 i1))
	 (delta-x (+ 0.05d0 (aref 1/rad0 0) (aref 1/rad1 0)))
	 (delta-y (+ 0.05d0 (aref 1/rad0 1) (aref 1/rad1 1)))
	 (dx (* delta-x (- (aref pnt0 0) (aref pnt1 0))))
	 (dy (* delta-y (- (aref pnt0 1) (aref pnt1 1))))
	 (d (the Double-Float
	       #-:excl (sqrt (the (Double-Float 0.0d0)
			  (+ (* dx dx) (* dy dy))))
	       #+:excl (excl::fld_sqrt (+ (* dx dx) (* dy dy)))))
	 (l (aref d-mtx i0 i1))
	 (d-l 0.0d0))
    (declare (type (Simple-Array Double-Float (*)) pnt0 pnt1 1/rad0 1/rad1)
	     (type (Simple-Array Double-Float (* *)) d-mtx w-mtx r-mtx)
	     (type Double-Float delta-x delta-y w dx dy d l d-l)
	     (type az:Array-Index i0 i1))
    (setf d-l (- d l))
    (when residual? (setf (aref r-mtx i0 i1) (setf (aref r-mtx i1 i0) d-l)))
    (when (or grad0? grad1?)
      (if (plusp d)
	  (let* ((alpha (/ (* 2.0d0 w d-l) d))
		 (gx (* alpha dx delta-x))
		 (gy (* alpha dy delta-y)))
	    (declare (type Double-float alpha gx gy))
	    (when grad0?
	      (let ((grd0 (node-grd node0)))
		(declare (type (Simple-Array Double-Float (*)) grd0))
		(incf (aref grd0 0) gx)
		(incf (aref grd0 1) gy)))
	    (when grad1?
	      (let ((grd1 (node-grd node1)))
		(declare (type (Simple-Array Double-Float (*)) grd1))
		(decf (aref grd1 0) gx)
		(decf (aref grd1 1) gy))))
	;; distance is zero, compute limit of grad as d -> 0
	(let* ((alpha (* 2.0d0 w d-l))
	       (gx (* alpha delta-x))
	       (gy (* alpha delta-y)))
	  (declare (type Double-float alpha gx gy))
	  (when grad0?
	    (let ((grd0 (node-grd node0)))
	      (declare (type (Simple-Array Double-Float (*)) grd0))
	      (incf (aref grd0 0) gx)
	      (incf (aref grd0 1) gy)))
	  (when grad1?
	    (let ((grd1 (node-grd node1)))
	      (declare (type (Simple-Array Double-Float (*)) grd1))
	      (decf (aref grd1 0) gx)
	      (decf (aref grd1 1) gy))))))
    (incf (aref objf 0) (* w d-l d-l)))
  (values))

#||
(defun sphered-spring-energy (solver node0 node1 grad0? grad1? residual? objf)
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type Node-Diagram node0 node1)
	   (type (Member t nil) grad0? grad1? residual?)
	   (type (Simple-Array Double-float (*)) objf))
  (let* ((pnt0 (node-pnt node0))
	 (1/rad0 (node-1/rad node0))
	 (i0 (node-index node0))
	 (pnt1 (node-pnt node1))
	 (1/rad1 (node-1/rad node1))
	 (i1 (node-index node1))
	 (d-mtx (node-distance-mtx solver))
	 (w-mtx (node-weight-mtx solver))
	 (r-mtx (node-residual-mtx solver))
	 (w (aref w-mtx i0 i1))
	 (delta-x (+ 0.05d0 (aref 1/rad0 0) (aref 1/rad1 0)))
	 (delta-y (+ 0.05d0 (aref 1/rad0 1) (aref 1/rad1 1)))
	 (dx (* delta-x (- (aref pnt0 0) (aref pnt1 0))))
	 (dy (* delta-y (- (aref pnt0 1) (aref pnt1 1))))
	 (l (aref d-mtx i0 i1))
	 (d-l 0.0d0))
    (declare (type (Simple-Array Double-Float (*)) pnt0 pnt1 1/rad0 1/rad1)
	     (type (Simple-Array Double-Float (* *)) d-mtx w-mtx r-mtx)
	     (type Double-Float delta-x delta-y w dx dy l d-l)
	     (type az:Array-Index i0 i1))
    (setf (aref *d* 0) (+ (* dx dx) (* dy dy)))
    (az:a_sqrt *d*)
    (setf d-l (- (aref *d* 0) l))
    (when residual? (setf (aref r-mtx i0 i1) (setf (aref r-mtx i1 i0) d-l)))
    (when (or grad0? grad1?)
      (if (plusp (aref *d* 0))
	  (let* ((alpha (/ (* 2.0d0 w d-l) (aref *d* 0)))
		 (gx (* alpha dx delta-x))
		 (gy (* alpha dy delta-y)))
	    (declare (type Double-float alpha gx gy))
	    (when grad0?
	      (let ((grd0 (node-grd node0)))
		(declare (type (Simple-Array Double-Float (*)) grd0))
		(incf (aref grd0 0) gx)
		(incf (aref grd0 1) gy)))
	    (when grad1?
	      (let ((grd1 (node-grd node1)))
		(declare (type (Simple-Array Double-Float (*)) grd1))
		(decf (aref grd1 0) gx)
		(decf (aref grd1 1) gy))))
	;; distance is zero, compute limit of grad as d -> 0
	(let* ((alpha (* 2.0d0 w d-l))
	       (gx (* alpha delta-x))
	       (gy (* alpha delta-y)))
	  (declare (type Double-float alpha gx gy))
	  (when grad0?
	    (let ((grd0 (node-grd node0)))
	      (declare (type (Simple-Array Double-Float (*)) grd0))
	      (incf (aref grd0 0) gx)
	      (incf (aref grd0 1) gy)))
	  (when grad1?
	    (let ((grd1 (node-grd node1)))
	      (declare (type (Simple-Array Double-Float (*)) grd1))
	      (decf (aref grd1 0) gx)
	      (decf (aref grd1 1) gy))))))
    
    (incf (aref objf 0) (* w d-l d-l)))
  (values))
||#





