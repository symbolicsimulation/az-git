;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================
#||
(defstruct (Node-Layout-Parameters
	    (:conc-name nil)
	    (:constructor %make-node-layout-parameters))


  )
||#
(defclass Node-Diagram (Leaf-Diagram) 
	  ( ;; slots for the graph strucutre
	   (from-edges
	    :type List
	    :accessor from-edges
	    :initform ())
	   (to-edges
	    :type List
	    :accessor to-edges
	    :initform ())
	   ;; slots used by various graph analysis algorithms
	   #||
	   (node-layout-parameters
	    :type Node-Layout-Parameters
	    :reader node-layout-parameters)
	   ||#
	   (node-index
	    ;; index of this node in the set of all nodes
	    :type az:Array-Index
	    :accessor node-index)
	   (node-mark
	    :type (Member :red :green :blue)
	    :accessor node-mark)
	   (node-weight
	    :type Double-Float
	    :accessor node-weight)
	   (node-predecessor
	    :type (or Null Node-Diagram)
	    :accessor node-predecessor)
	   (loss
	    :type Double-Float
	    :accessor loss)
	   (node-pnt
	    :type (Simple-Array Double-Float (*))
	    :accessor node-pnt
	    :initform (az:make-float-array 3 :initial-element 0.0d0))
	   (node-rad
	    :type (Simple-Array Double-Float (*))
	    :accessor node-rad
	    :initform (az:make-float-array 3 :initial-element 0.0d0))
	   (node-1/rad
	    :type (Simple-Array Double-Float (*))
	    :accessor node-1/rad
	    :initform (az:make-float-array 3 :initial-element 0.0d0))
	   (node-grd
	    :type (Simple-Array Double-Float (*))
	    :accessor node-grd
	    :initform (az:make-float-array 3 :initial-element 0.0d0))
	   (node-bounds
	    ;; The upper left corner of the node is constrained
	    ;; to be in this rectangle. If null, the node is
	    ;; constrained to remain entriely within its window.
	    :type (or Null g:Chart-Rect)
	    :accessor node-bounds
	    :initform nil)
	   (node-user-pinned?
	    ;; were the bounds set by the user?
	    ;; The implication is that they should
	    ;; then not be changed by the program.
	    :type (Member T Nil)
	    :accessor node-user-pinned?
	    :initform nil)
	   ;; slots used for diagram and input
	   (string-point
	    :type g:Screen-Point
	    :accessor string-point
	    :initform  (g::%make-screen-point 0 0))
	   (diagram-pixmap
	    :type xlib:Pixmap
	    :accessor diagram-pixmap)
	   (node-margin
	    :type g:Positive-Screen-Coordinate
	    :accessor Node-margin)
	   (name-string
	    :type String
	    :accessor name-string
	    :initform "xxx")
	   (allowed-rect
	    :type g:Screen-Rect
	    :accessor allowed-rect
	    :initform (g::%make-screen-rect 0 0 0 0))))

;;;------------------------------------------------------------

(defmethod initialize-instance :after ((diagram Node-Diagram) &rest initargs)
  (declare (ignore initargs))
  (setf (name-string diagram)
    (let ((name (node-name (diagram-subject diagram))))
      (if (stringp name)
	  name
	;; else
	(string-downcase (string name))))))

;;;------------------------------------------------------------

(defmethod brk:left ((node Node-Diagram)) (aref (node-pnt node) 0))
(defmethod brk:width ((node Node-Diagram)) (* 2.0d0 (aref (node-rad node) 0)))
(defmethod brk:top ((node Node-Diagram)) (aref (node-pnt node) 1))
(defmethod brk:height ((node Node-Diagram)) (* 2.0d0 (aref (node-rad node) 1)))

(defmethod (setf brk:left) (v (node Node-Diagram))
  (setf (aref (node-pnt node) 0) v))
(defmethod (setf brk:width) (v (node Node-Diagram))
  (setf (aref (node-rad node) 0) (/ v 2.0d0)))
(defmethod (setf brk:top) (v (node Node-Diagram))
  (setf (aref (node-pnt node) 1) v))
(defmethod (setf brk:height) (v (node Node-Diagram))
  (setf (aref (node-rad node) 1) (/ v 2.0d0)))


#||
(defmethod brk:left ((node Node-Diagram))
  (g:screen-rect-left (diagram-rect node)))

(defmethod brk:width ((node Node-Diagram))
  (g:screen-rect-width (diagram-rect node)))

(defmethod brk:top ((node Node-Diagram))
  (g:screen-rect-top (diagram-rect node)))

(defmethod brk:height ((node Node-Diagram))
  (g:screen-rect-height (diagram-rect node)))

(defmethod (setf brk:left) (v (node Node-Diagram))
  (setf (g:screen-rect-left (diagram-rect node)) v))

(defmethod (setf brk:width) (v (node Node-Diagram))
  (setf (g:screen-rect-width (diagram-rect node)) v))

(defmethod (setf brk:top) (v (node Node-Diagram))
  (setf (g:screen-rect-top (diagram-rect node)) v))

(defmethod (setf brk:height) (v (node Node-Diagram))
  (setf (g:screen-rect-height (diagram-rect node)) v))
||#

;;;------------------------------------------------------------

(defmethod print-object ((node Node-Diagram) stream)
  (declare (type Stream stream))
  (az:printing-random-thing
   (node stream)
   (format stream "NodeP ~a" (node-name (diagram-subject node)))))

;;;------------------------------------------------------------

(defmethod diagram-name ((node Node-Diagram))
  (format nil "NodeP ~a" (node-name (diagram-subject node))))

;;;------------------------------------------------------------

(defgeneric node-name (object)
  (declare (type T object))
  (:documentation
   "Return a string or symbol used in graph displays of this object graph."))

(defmethod node-name ((node T))
  "Prints the node to a string of maximum length 32."
  (let* (#+:excl (excl:*print-nickname* t)
	 (s (format nil "~a" node)))
    (subseq s 0 (min 32 (length s)))))

(defmethod node-name ((node String))
  "Prints the node to a string of maximum length 32."
  (subseq node 0 (min 32 (length node))))

(defmethod node-name ((cl Class))
  (let (#+:excl (excl:*print-nickname* t))
    (format nil "~:(~s~)" (class-name cl))))

;;;------------------------------------------------------------

(defmethod layout-solver ((diagram Node-Diagram))
  (layout-solver (diagram-parent diagram)))

;;;------------------------------------------------------------

(defun node-children (node)
  (declare (type Node-Diagram node))
  (az:with-collection
   (dolist (edge (from-edges node))
     (az:collect (gra:edge-node1 edge)))))

;;;============================================================
;;; Building
;;;============================================================

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-diagram ((diagram Node-Diagram) layout-options)

  (declare (optimize (safety 3) (speed 0))
	   (type Node-Diagram diagram)
	   (type List layout-options)
	   (ignore layout-options))

  (setf (node-margin diagram) 4)

  (let* ((string-point (string-point diagram))
	 (rect (diagram-rect diagram))
	 (diagram-rect (diagram-rect (diagram-parent diagram)))
	 (allowed-rect (allowed-rect diagram))
	 (margin (node-margin diagram))
	 (margin/2 (ash margin -1))
	 (extent (diagram-minimum-extent diagram)))

    (declare (type g:Screen-Point string-point)
	     (type g:Screen-Rect rect diagram-rect allowed-rect)
	     (type g:Screen-Coordinate margin margin/2)
	     (type g:Screen-Vector extent))

    ;; initialize the node's diagram-rect
    (setf (g::%screen-rect-width rect) (g:screen-vector-x extent))
    (setf (g::%screen-rect-height rect) (g:screen-vector-y extent))

    ;; set up possible region for the upper left corner of the node
    (%possible-point-rect extent margin diagram-rect allowed-rect)

    ;; position the label with the node
    (setf (g::%screen-point-x string-point) margin/2)
    (setf (g::%screen-point-y string-point)
      (+ margin/2
	 (the g:Screen-Coordinate
	   (xlt::%string-ascent
	    (name-string diagram)
	    (xlib:gcontext-font (diagram-gcontext diagram))))))

    ;; draw on the pixmap
    (node-update-pixmap diagram)
    diagram))

;;;------------------------------------------------------------

(defmethod diagram-minimum-extent ((diagram Node-Diagram)
				   &key
				   (result (g:make-screen-vector)))
  (declare (type Node-Diagram diagram)
	   (type g:Screen-Vector result))
  (let ((margin (Node-margin diagram)))
    (declare (type g:Positive-Screen-Coordinate margin))
    (multiple-value-bind
	(w ascent descent)
	(xlt::%analyze-string-placement
	 (name-string diagram)
	 (xlib:gcontext-font (diagram-gcontext diagram)))
      (declare (type g:Screen-Coordinate w ascent descent))
      (setf (g:screen-vector-x result) (+ margin w))
      (setf (g:screen-vector-y result) (+ margin ascent descent))))
  result)

;;;------------------------------------------------------------

(defun node-randomize-position (diagram)
  (declare (type Node-Diagram diagram))
  ;; random initialization
  (%random-screen-rect-in-rect (allowed-rect diagram) (diagram-rect diagram)))

;;;------------------------------------------------------------
;;; 

(defun node-position-from-neighbors (node)

  "Initialization to the mean of parents's rights and children's lefts.
This assumes graph is directed from left to right."

  (declare (type Node-Diagram node))

  (let ((x 0)
	(y 0)
	(w (g:screen-rect-width (diagram-rect node)))
	(n (+ (length (to-edges node))
	      (length (from-edges node)))))
    (declare (type g:Screen-Coordinate x y w))
    (unless (zerop n)
      (dolist (e (to-edges node))
	(incf x (g:screen-rect-right (diagram-rect (gra:edge-node0 e))))
	(incf y (g:screen-rect-top (diagram-rect (gra:edge-node0 e)))))
      (dolist (e (from-edges node))
	(incf x (+ w (g:screen-rect-left (diagram-rect (gra:edge-node1 e)))))
	(incf y (g:screen-rect-top (diagram-rect (gra:edge-node1 e)))))
      (setf (g:screen-rect-left (diagram-rect node)) (truncate x n))
      (setf (g:screen-rect-top (diagram-rect node)) (truncate y n)))))


(defun vertical-node-position-from-neighbors (node)

  "Initialization to the mean of parents's rights and children's lefts.
This assumes graph is directed from top to bottom"

  (declare (type Node-Diagram node))

  (let ((x 0)
	(y 0)
	(h (g:screen-rect-width (diagram-rect node)))
	(n (+ (length (to-edges node))
	      (length (from-edges node)))))
    (declare (type g:Screen-Coordinate x y h))
    (unless (zerop n)
      (dolist (e (to-edges node))
	(incf x (g:screen-rect-right (diagram-rect (gra:edge-node0 e))))
	(incf y (g:screen-rect-top (diagram-rect (gra:edge-node0 e)))))
      (dolist (e (from-edges node))
	(incf x (g:screen-rect-left (diagram-rect (gra:edge-node1 e))))
	(incf y (+ h (g:screen-rect-top (diagram-rect (gra:edge-node1 e))))))
      (setf (g:screen-rect-left (diagram-rect node)) (truncate x n))
      (setf (g:screen-rect-top (diagram-rect node)) (truncate y n)))))

;;;------------------------------------------------------------

(defmethod node-l2-dist2 ((diagram0 Node-Diagram) (diagram1 Node-Diagram))
  (g:screen-rect-min-l2-dist2 (diagram-rect diagram0) (diagram-rect diagram1)))

(defmethod node-l2-dist ((diagram0 Node-Diagram) (diagram1 Node-Diagram))
  (g:screen-rect-min-l2-dist (diagram-rect diagram0) (diagram-rect diagram1)))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod draw-diagram ((diagram Node-Diagram))
  (declare (optimize (safety 3) (speed 0))
	   (type Node-Diagram diagram))
  (let ((r (diagram-rect diagram)))
    (declare (type g:Screen-Rect r))
    (xlib:copy-area
     (diagram-pixmap diagram) (diagram-gcontext diagram)
     0 0 (g::%screen-rect-width r) (g::%screen-rect-height r)
     (diagram-window diagram)
     (g::%screen-rect-xmin r) (g::%screen-rect-ymin r))))

;;;------------------------------------------------------------

(defmethod erase-diagram ((diagram Node-Diagram) rect)

  (declare (optimize (safety 3) (speed 0))
	   (type Node-Diagram diagram)
	   (type (or Null g:Screen-Rect) rect))
  (let ((r (diagram-rect diagram))
	(gcontext (diagram-erasing-gcontext diagram)))
    (declare (type g:Screen-Rect r)
	     (type xlib:Gcontext gcontext))
    (xlt:draw-rect gcontext (diagram-window diagram) r t)
    (unless (null rect) (g::%copy-screen-rect r rect))))
  
;;;------------------------------------------------------------

(defun node-update-pixmap (diagram)
  (declare (type Node-Diagram diagram))
  (let* ((window (diagram-window diagram))
	 (rect (diagram-rect diagram))
	 (w (g::%screen-rect-width rect))
	 (h (g::%screen-rect-height rect))
	 (gcontext (diagram-gcontext diagram))
	 (string (name-string diagram)))
    (when (not (slot-boundp diagram 'diagram-pixmap))
      ;; then we need to make a new pixmap
      (setf (diagram-pixmap diagram)
	(xlib:create-pixmap :width w :height h :drawable window
			    :depth (xlib:drawable-depth window))))
    (when (or (> w (xlib:drawable-width (diagram-pixmap diagram)))
	      (> h (xlib:drawable-height (diagram-pixmap diagram))))
      ;; then we need to make a new pixmap
      (let ((old-pixmap (diagram-pixmap diagram)))
	(setf (diagram-pixmap diagram)
	  (xlib:create-pixmap :width w :height h :drawable window
			      :depth (xlib:drawable-depth window)))
	(xlib:free-pixmap old-pixmap)))
    ;; redraw the contents of the pixmap
    (let ((pixmap (diagram-pixmap diagram)))
      (xlib:draw-rectangle pixmap (diagram-erasing-gcontext diagram) 0 0 w h t)
      (xlib:draw-rectangle pixmap gcontext 0 0 (- w 1) (- h 1) nil)
      (xlt:draw-string gcontext pixmap string (string-point diagram)))))

;;;------------------------------------------------------------
;; automate redrawing whenever gcontext is changed

(defmethod (setf diagram-gcontext) (gcontext (diagram Node-Diagram))
  (setf (slot-value diagram 'diagram-gcontext) gcontext)
  ;; animate
  ;; actually, we will need to erase if the diagram changes size
  ;;(erase-diagram diagram nil)
  (node-update-pixmap diagram)
  (draw-diagram diagram)
  (xlt:drawable-force-output (diagram-window diagram)))

;;;------------------------------------------------------------

(defmethod az:kill-object ((diagram Node-Diagram) &rest options)
  (declare (ignore options))
  (xlib:free-pixmap (diagram-pixmap diagram))
  (change-class diagram `az:Dead-Object))

;;;============================================================
;;; Generic graph traversal functions
;;;============================================================

(defgeneric root-node? (gnp) (declare (type Node-Diagram gnp)))

(defmethod root-node? ((gnp Node-Diagram)) (null (to-edges gnp)))

(defgeneric leaf-node? (gnp) (declare (type Node-Diagram gnp)))

(defmethod leaf-node? ((gnp Node-Diagram)) (null (from-edges gnp)))

;;;------------------------------------------------------------
;;; Transforming between representations of graph layout
;;;------------------------------------------------------------
;;; Transforming from the state of Graph-Diagram
;;; to the alternate representation of that state used
;;; by NPSOL (a simple array of double floats)

(defun %nodes->position-vector (nodes x)

  (declare (optimize (safety 3) (speed 0))
	   (type List nodes)
	   (type (Simple-Array Double-Float (*)) x))
  
  (let ((i -1))
    (declare (type Fixnum i))
    (dolist (node nodes)
      (declare (type Node-Diagram node))
      (let ((pnt (node-pnt node)))
	(declare (type (Simple-Array Double-Float (*)) pnt))
	(setf (aref x (incf i)) (aref pnt 0))
	(setf (aref x (incf i)) (aref pnt 1)))))
  x)

(defun nodes->position-vector (nodes x)
  (declare (optimize (safety 3) (speed 0))
	   (type List nodes)
	   (type (Simple-Array Double-Float (*)) x))
  (assert (<= (* 2 (length nodes)) (length x)))
  (%nodes->position-vector nodes x))

;;;------------------------------------------------------------
;;; Transforming from the state of Graph-Diagram
;;; to the alternate representation of that state used
;;; by NPSOL (a simple array of double floats)

(defparameter *p* (make-array 2 :element-type 'Double-Float))
(declaim (type (Simple-Array Double-Float (2)) *p*))

(defparameter *ip* (make-array 2 :element-type 'Fixnum))
(declaim (type (Simple-Array Fixnum (2)) *ip*))

(defun %position-vector->nodes (x nodes)

  (declare (optimize (safety 3) (speed 0))
	   (type (Simple-Array Double-Float (*)) x)
	   (type List nodes))

  ;; first set node point and rect
  (let ((i -1))
    (declare (type Fixnum i))
    (dolist (node nodes)
      (declare (type Node-Diagram node))
      (let ((r (diagram-rect node))
	    (pnt (node-pnt node))
	    (rad (node-rad node)))
	(declare (type g:Screen-Rect r)
		 (type (Simple-Array Double-Float (*)) pnt rad))
	(setf (aref *p* 0) (aref x (incf i)))
	(setf (aref *p* 1) (aref x (incf i)))
	(az:c_trunc2 *p* *ip*)
	(setf (aref pnt 0) (+ (aref *p* 0) (aref rad 0)))
	(setf (aref pnt 1) (+ (aref *p* 1) (aref rad 1)))
	(setf (g::%screen-rect-xmin r) (aref *ip* 0))
	(setf (g::%screen-rect-ymin r) (aref *ip* 1)))))
  nodes)

(defun position-vector->nodes (x nodes)
  (declare (type (Simple-Array Double-Float (*)) x)
	   (type List nodes))
  (assert (<= (* 2 (length nodes)) (length x)))
  (%position-vector->nodes x nodes))

;;;------------------------------------------------------------