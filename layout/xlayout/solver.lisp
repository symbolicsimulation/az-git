;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Graph-Layout-Solver (Layout-Solver)
	  ((diagram
	    :type Graph-Diagram
	    :reader diagram
	    :initarg :diagram)

	   (brk:domain
	    :type (or Null brk:Position-Domain)
	    :accessor brk:domain
	    :initform ())
	   (inactive-nodes
	    :type List
	    :accessor inactive-nodes
	    :initform ())
	   #||
	   (active-node-pairs
	    :type List
	    :accessor active-node-pairs
	    :initform ())
	   ||#

	   (node-pair-energy-f
	    :type (or Null Symbol Function)
	    :accessor node-pair-energy-f
	    :initform 'sphered-spring-energy)

	   (node-pair-distance-f
	    :type (or Null Symbol Function)
	    :accessor node-pair-distance-f
	    :initform ())
	   (node-distance-mtx
	    :type (Simple-Array Double-Float (* *))
	    :accessor node-distance-mtx)

	   (affine-constraint-f
	    :type (or Null Symbol Function)
	    :accessor affine-constraint-f
	    :initform 'make-affine-constraints)
	   (x-order-f
	    :type (or Null Symbol Function)
	    :accessor x-order-f
	    :initform nil)
	   (y-order-f
	    :type (or Null Symbol Function)
	    :accessor y-order-f
	    :initform nil)
	   (constraints
	    :type List
	    :accessor constraints)

	   (node-weight-mtx
	    :type (Simple-Array Double-Float (* *))
	    :accessor node-weight-mtx)
	   (node-residual-mtx
	    :type (Simple-Array Double-Float (* *))
	    :accessor node-residual-mtx)
	   (node-weight-f
	    :type (or Symbol Function)
	    :accessor node-weight-f
	    :initform '1/d2-weights)

	   (active-edge-pairs
	    :type List
	    :accessor active-edge-pairs
	    :initform ())
	   (edge-attacher-f
	    :type (or Symbol Function)
	    :accessor edge-attacher-f
	    :initform 'attach-centers-clipped)
	   (edge-distance-mtx
	    :type (Simple-Array Double-Float (* *))
	    :accessor edge-distance-mtx)
	   (edge-pair-energy-f
	    :type Function
	    :accessor edge-pair-energy-f
	    :initform 'edge-repulsion)

	   (rest-length
	    :type Double-Float
	    :accessor rest-length
	    :initform 10.0d0)
	   (spring-constant
	    :type Double-Float
	    :accessor spring-constant
	    :initform 1.0d0)
	   (sigma
	    :type Double-Float
	    :accessor sigma
	    :initform 1.0d0)

	   (animate?
	    :type (Member t nil) 
	    :accessor animate?
	    :initform t)))

;;;------------------------------------------------------------
;;; initialization

(defgeneric build-solver (solver))

(defmethod build-solver ((solver Graph-Layout-Solver))
  (let* ((diagram (diagram solver))
	 (nnodes (length (gra:nodes diagram)))
	 (nedges (length (gra:edges diagram))))
    (setf (node-distance-mtx solver)
      (make-array (list nnodes nnodes) :element-type 'Double-Float
		  :initial-element 0.0d0))
    (setf (node-weight-mtx solver)
      (make-array (list nnodes nnodes) :element-type 'Double-Float
		  :initial-element 0.0d0))
    (setf (node-residual-mtx solver)
      (make-array (list nnodes nnodes) :element-type 'Double-Float
		  :initial-element 0.0d0))
    (setf (edge-distance-mtx solver)
      (make-array (list nedges nedges) :element-type 'Double-Float
		  :initial-element 0.0d0))
    solver))

(defmethod build-layout-solver ((diagram Graph-Diagram)
				&rest options
				&key
				(class
				 #-:npsol 'Brick-Graph-Layout-Solver
				 #+:npsol 'NPSOL-Graph-Layout-Solver))
  (declare (ignore options))
  (build-solver (make-instance class :diagram diagram)))

;;;------------------------------------------------------------
;;; Initializations of the layout solver that happen at diagram layout
;;; time, before the solver is called the first time

(defmethod initialize-layout-solver :before ((solver Graph-Layout-Solver)
					     layout-options)
  (declare (type List layout-options))
  (let* ((nodes (gra:nodes (diagram solver)))
	 (n (length nodes)))
    (setf (brk:domain solver)
      (brk:make-position-domain
       (if (null (brk:domain solver))
	   (copy-list nodes)
	 ;; else
	 (intersection (brk:bricks (brk:domain solver)) nodes))))
    ;;(setf (active-node-pairs solver) (gra:all-unordered-pairs nodes))
    (setf (node-distance-mtx solver)
      (ensure-large-enough-mtx (node-distance-mtx solver) n n))
    (setf (node-residual-mtx solver)
      (ensure-large-enough-mtx (node-residual-mtx solver) n n))
    (setf (constraints solver) (funcall (affine-constraint-f solver) solver))
    (compute-node-pair-distances solver)
    (1/d2-weights solver)
    solver)(setf (affine-constraint-f solver)
      (getf layout-options :affine-constraint-f (affine-constraint-f solver)))
    (setf (x-order-f solver)
      (getf layout-options :x-order-f (x-order-f solver)))
    (setf (y-order-f solver)
      (getf layout-options :y-order-f (y-order-f solver)))
    (setf (node-pair-distance-f solver)
      (getf layout-options :node-pair-distance-f
	    (node-pair-distance-f solver)))
    (when (getf layout-options :rest-length)
      (setf (rest-length solver)
	(az:fl (getf layout-options :rest-length (rest-length solver))))))

(defmethod initialize-layout-solver ((solver Graph-Layout-Solver)
				     layout-options)
  (declare (type List layout-options)
	   (ignore layout-options))
  (let* ((nodes (gra:nodes (diagram solver)))
	 (n (length nodes)))
    (setf (brk:domain solver)
      (brk:make-position-domain
       (if (null (brk:domain solver))
	   (copy-list nodes)
	 ;; else
	 (intersection (brk:bricks (brk:domain solver)) nodes))))
    ;;(setf (active-node-pairs solver) (gra:all-unordered-pairs nodes))
    (setf (node-distance-mtx solver)
      (ensure-large-enough-mtx (node-distance-mtx solver) n n))
    (setf (node-residual-mtx solver)
      (ensure-large-enough-mtx (node-residual-mtx solver) n n))
    (setf (constraints solver) (funcall (affine-constraint-f solver) solver))
    (compute-node-pair-distances solver)
    (1/d2-weights solver)
    solver))

;;;------------------------------------------------------------

(defgeneric make-affine-constraints (solver)
  (declare (type Graph-Layout-Solver solver)))

(defmethod make-affine-constraints ((solver Graph-Layout-Solver))
  (nconc
   (when (x-order-f solver)
     (make-order-constraints
      (diagram solver) (x-order-f solver) :x 16.0d0))
   (when (y-order-f solver)
     (make-order-constraints
      (diagram solver) (y-order-f solver) :y 16.0d0))))

;;;----------------------------------------------------------

(defun compute-node-pair-distances (solver)
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver))
  (if (null (node-pair-distance-f solver))
      (compute-graph-distances solver)
    ;; else
    (let ((d-mtx (node-distance-mtx solver))
	  (d-f (node-pair-distance-f solver))
	  (inactive-nodes (inactive-nodes solver)))
      (declare (type (Simple-Array Double-Float (* *)) d-mtx)
	       (type (or Symbol Function) d-f)
	       (type List inactive-nodes))
      (do ((bricks (brk:bricks (brk:domain solver)) (rest bricks)))
	  ((null bricks))
	(declare (type List bricks))
	(let ((node0 (first bricks)))
	  (declare (type Node-Diagram node0))
	  ;; distance to other active nodes
	  (dolist (node1 (rest bricks))
	    (let* ((i0 (node-index node0))
		   (i1 (node-index node1))
		   (d (funcall
		       d-f (diagram-subject node0) (diagram-subject node1))))
	      (declare (type T node0 node1)
		       (type az:Array-Index i0 i1)
		       (type Double-Float d))
	      (setf (aref d-mtx i0 i1) d)
	      (setf (aref d-mtx i1 i0) d)))
	  ;; distance to inactive nodes
	  (dolist (node1 inactive-nodes)
	    (let* ((i0 (node-index node0))
		   (i1 (node-index node1))
		   (d (funcall d-f (diagram-subject node0)
			       (diagram-subject node1))))
	      (declare (type T node0 node1)
		       (type az:Array-Index i0 i1)
		       (type Double-Float d))
	      (setf (aref d-mtx i0 i1) d)
	      (setf (aref d-mtx i1 i0) d)))))))
  ;; scale the d-mtx so that a unit distance becomes the solver's rest-length
  (scale-mtx (rest-length solver) (node-distance-mtx solver)))

;;;------------------------------------------------------------

(defmethod solve-layout ((solver Graph-Layout-Solver))

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver))

  t)

;;;------------------------------------------------------------
;;; Energy calculation
;;;------------------------------------------------------------

(defgeneric calculate-energy (solver objf grad gradient? residual?)
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type (Simple-Array Double-Float (*)) objf grad)
	   (type (Member t nil)  gradient? residual?)))

(defmethod calculate-energy ((solver Graph-Layout-Solver)
			     objf grad gradient? residual?)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type (Simple-Array Double-Float (*)) objf grad)
	   (type (Member t nil)  gradient? residual?))

  (let* ((domain (brk:domain solver))
	 (active-nodes (brk:bricks domain))
	 (inactive-nodes (inactive-nodes solver)))
    (declare (type brk:Domain domain)
	     (type List active-nodes inactive-nodes))

    (setf (aref objf 0) 0.0d0)
    (when gradient? (zero-node-gradients active-nodes))

    (dolist (node0 active-nodes)
      (declare (type Node-Diagram node0))
      (dolist (node1 active-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? gradient?
		   residual? objf)))
      (dolist (node1 inactive-nodes)
	(declare (type Node-Diagram node1))
	(unless (eq node0 node1)
	  (funcall (node-pair-energy-f solver)
		   solver node0 node1 gradient? nil residual?
		   objf))))

    (when gradient? (nodes-gradient-vector active-nodes grad))))

;;;------------------------------------------------------------

(defun zero-node-gradients (nodes)

  (declare (optimize (safety 3) (speed 0))
	   (type List nodes))

  (dolist (node nodes)
    (declare (type Node-Diagram node))
    (let ((grd (node-grd node)))
      (declare (type (Simple-Array Double-Float (*)) grd))
      (setf (aref grd 0) 0.0d0)
      (setf (aref grd 1) 0.0d0)
      (setf (aref grd 2) 0.0d0))))

(defun nodes-gradient-vector (nodes grad)

  (declare (optimize (safety 3) (speed 0))
	   (type List nodes)
	   (type (Simple-Array Double-Float (*)) grad))

  (let ((i -1))
    (declare (type Fixnum i))
    (dolist (node nodes)
      (declare (type Node-Diagram node))
      (let ((grd (node-grd node)))
	(declare (type (Simple-Array Double-Float (*)) grd))
	(setf (aref grad (incf i)) (aref grd 0))
	(setf (aref grad (incf i)) (aref grd 1))))))



(defun swap-nodes (gp node0 node1 &key (animate? t))

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram gp)
	   (type Node-Diagram node0 node1))

  (when animate? (erase-diagram gp nil))

  (let* ((solver (layout-solver gp))
	 (r0 (diagram-rect node0))
	 (r1 (diagram-rect node1)))
    (declare (type g:Screen-Rect r0 r1))

    (rotatef (node-pnt node0) (node-pnt node1))
    (rotatef (g::%screen-rect-xmin r0) (g::%screen-rect-xmin r1))
    (rotatef (g::%screen-rect-ymin r0) (g::%screen-rect-ymin r1))

    (dolist (edge (the List (from-edges node0)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))
    (dolist (edge (the List (to-edges node0)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))

    (dolist (edge (the List (from-edges node1)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))
    (dolist (edge (the List (to-edges node1)))
      (declare (type Edge-Diagram edge))
      (attach-edge solver edge))

    (when animate?
      (draw-diagram gp)
      (xlt:drawable-force-output (diagram-window gp)))))

;;;------------------------------------------------------------

(defun swap-solve-layout (gp &key (animate? t))

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram gp))
  
  (let* ((solver (layout-solver gp))
	 (the-nodes (gra:nodes gp))
	 (energy (make-array '(1) :element-type 'Double-Float))
	 (test (make-array '(1) :element-type 'Double-Float)))
    (declare (type Graph-Layout-Solver solver)
	     (type List the-nodes)
	     (type (Simple-Array Double-Float (1)) energy test))

    (calculate-energy solver energy nil nil nil)
    (do ((nodes the-nodes (rest nodes)))
	((null nodes))
      (declare (type List nodes))
      (let ((node0 (first nodes)))
	(declare (type Node-Diagram node0))
	(dolist (node1 (rest nodes))
	  (declare (type Node-Diagram node1))
	  (swap-nodes gp node0 node1 :animate? animate?)
	  (setf test
	    (calculate-energy solver test nil nil nil))
	  (if (< (aref test 0) (aref energy 0))
	      (print (rotatef energy test))
	    ;; else
	    (swap-nodes gp node0 node1 :animate? animate?)))))
    (xlt:clear-drawable (diagram-window gp))
    (draw-diagram gp)
    (xlt:drawable-force-output (diagram-window gp))
    energy))


;;;============================================================
;;; Weights for the node pairs
;;;------------------------------------------------------------

(defun unit-weights (solver)
  
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver))
    
  (let* ((n (length (gra:nodes (diagram solver))))
	 (w-mtx (node-weight-mtx solver)))
    (declare (type (Simple-Array Double-Float (* *)) w-mtx)
	     (type az:Array-Index n))
    (cond ((>= (array-dimension w-mtx 0) n)
	   (dotimes (i n)
	     (declare (type az:Array-Index i))
	     (dotimes (j i)
	       (declare (type az:Array-Index j))
	       (setf (aref w-mtx i j)
		 (setf (aref w-mtx j i) 1.0d0)))))
	  (t
	   (setf (node-weight-mtx solver)
	     (make-array (list n n)
			 :element-type 'Double-Float
			 :initial-element 1.0d0))))))

(defun 1/d-weights (solver)
  
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver))
    
  (let* ((n (length (gra:nodes (diagram solver))))
	 (d-mtx (node-distance-mtx solver))
	 (w-mtx (node-weight-mtx solver)))
    (declare (type (Simple-Array Double-Float (* *)) d-mtx w-mtx)
	     (type az:Array-Index n))
    (unless (>= (array-dimension w-mtx 0) n)
      (setf w-mtx (make-array (list n n) :element-type 'Double-Float))
      (setf (node-weight-mtx solver) w-mtx))
    (dotimes (i n)
      (declare (type az:Array-Index i))
      (dotimes (j i)
	(declare (type az:Array-Index j))
	(let ((d (aref d-mtx i j)))
	  (setf (aref w-mtx i j)
	    (setf (aref w-mtx j i) (/ 1.0d0 d))))))))

(defun 1/d2-weights (solver)
  
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver))
    
  (let* ((n (length (gra:nodes (diagram solver))))
	 (d-mtx (node-distance-mtx solver))
	 (w-mtx (node-weight-mtx solver)))
    (declare (type (Simple-Array Double-Float (* *)) d-mtx w-mtx)
	     (type az:Array-Index n))
    (setf w-mtx (ensure-large-enough-mtx w-mtx n n))
    (setf (node-weight-mtx solver) w-mtx)
    (dotimes (i n)
      (declare (type az:Array-Index i))
      (dotimes (j i)
	(declare (type az:Array-Index j))
	(let* ((d (aref d-mtx i j))
	      (1/d2 (/ 1.0d0 d d)))
	  (declare (type Double-Float d 1/d2))
	  (setf (aref w-mtx i j) 1/d2)
	  (setf (aref w-mtx j i) 1/d2))))))