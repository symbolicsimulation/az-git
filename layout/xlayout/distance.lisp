;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================
;;; compute the number of edges in the shortest path between
;;; all pairs of Node-diagrams in a graph-diagram.

;;; breadth first search (see \cite{Corm90}, p 470).
;;; Use node pens to mark the "color" of the nodes, to make it easy
;;; to animate the search. Use red, green, blue to substitute for
;;; white, gray, black.

(defun red-gcontext (diagram)
  (xlt:colorname->pure-hue-gcontext (diagram-window diagram) :red))
(defun green-gcontext (diagram)
  (xlt:colorname->pure-hue-gcontext (diagram-window diagram) :green))
(defun blue-gcontext (diagram)
  (xlt:colorname->pure-hue-gcontext (diagram-window diagram) :blue))

;;;============================================================
#||
(defun bf-search-from-node (node0 dist0 q origin-index d-mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type List edges)
	   (type Double-Float dist0)
	   (type Queue q)
	   (type az:Array-Index origin-index)
	   (type (Simple-Array Double-Float (* *)) d-mtx))

  (setf dist0 (+ dist0 1.0d0))
  (dolist (edge (from-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node1 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (node-mark node1) :red)
	(setf (node-mark node1) :green)
	(setf (aref d-mtx i1 origin-index) dist0)
	(setf (aref d-mtx origin-index i1) dist0)
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q))))

  (dolist (edge (to-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node0 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (node-mark node1) :red)
	(setf (node-mark node1) :green)
	(setf (aref d-mtx i1 origin-index) dist0)
	(setf (aref d-mtx origin-index i1) dist0)
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q)))))

(defun undirected-breadth-first-search (graph origin d-mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram graph)
	   (type Node-Diagram origin)
	   (type (Simple-Array Double-Float (* *)) d-mtx))
  
  (let ((origin-index (node-index origin))
	(q (ac:make-queue)))
    (declare (type az:Array-Index origin-index)
	     (type ac:Queue q))
    ;; initialization
    (setf (aref d-mtx origin-index origin-index) 0.0d0)
    (dolist (node (the List (gra:nodes graph)))
      (declare (type Node-Diagram node))
      (setf (node-mark node) :red)
      (setf (node-predecessor node) nil))
    (setf (node-mark origin) :green)
    (ac:enqueue origin q)

    (loop (when (ac:empty-queue? q) (return))
      (let* ((node0 (ac:dequeue q))
	     (i0 (node-index node0))
	     (dist0 (aref d-mtx origin-index i0)))
	(declare (type Node-Diagram node0)
		 (type az:Array-Index i0)
		 (type Double-Float dist0))
	(bf-search-from-node node0 dist0 q origin-index d-mtx)
	(setf (node-mark node0) :blue)))))
||#

(defun undirected-breadth-first-search (graph origin d-mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram graph)
	   (type Node-Diagram origin)
	   (type (Simple-Array Double-Float (* *)) d-mtx))
  
  (let ((origin-index (node-index origin))
	(q (ac:make-queue)))
    (declare (type az:Array-Index origin-index)
	     (type ac:Queue q))
    ;; initialization
    (setf (aref d-mtx origin-index origin-index) 0.0d0)
    (dolist (node (the List (gra:nodes graph)))
      (declare (type Node-Diagram node))
      (setf (node-mark node) :red)
      (setf (node-predecessor node) nil))
    (setf (node-mark origin) :green)
    (ac:enqueue origin q)

    (loop (when (ac:empty-queue? q) (return))
      (let* ((node0 (ac:dequeue q))
	     (i0 (node-index node0))
	     (d (+ 1.0d0 (aref d-mtx origin-index i0))))
	(declare (type Node-Diagram node0)
		 (type az:Array-Index i0)
		 (type Double-Float d))
	(dolist (edge (from-edges node0))
	  (declare (type Edge-Diagram edge))
	  (let* ((node1 (gra:edge-node1 edge))
		 (i1 (node-index node1)))
	    (declare (type Node-Diagram node1)
		     (type az:Array-Index i1))
	    (when (eq (node-mark node1) :red)
	      (setf (node-mark node1) :green)
	      (setf (aref d-mtx i1 origin-index) d)
	      (setf (aref d-mtx origin-index i1) d)
	      (setf (node-predecessor node1) node0)
	      (ac:enqueue node1 q))))

	(dolist (edge (to-edges node0))
	  (declare (type Edge-Diagram edge))
	  (let* ((node1 (gra:edge-node0 edge))
		 (i1 (node-index node1)))
	    (declare (type Node-Diagram node1)
		     (type az:Array-Index i1))
	    (when (eq (node-mark node1) :red)
	      (setf (node-mark node1) :green)
	      (setf (aref d-mtx i1 origin-index) d)
	      (setf (aref d-mtx origin-index i1) d)
	      (setf (node-predecessor node1) node0)
	      (ac:enqueue node1 q))))

	(setf (node-mark node0) :blue)))))

;;;============================================================

(defun animated-bf-search-from-node (node0 dist0 q origin-index d-mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type Double-Float dist0)
	   (type ac:Queue q)
	   (type az:Array-Index origin-index)
	   (type (Simple-Array Double-Float (* *)) d-mtx))
  
  (dolist (edge (from-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node1 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (diagram-gcontext node1) (red-gcontext node1))
	(setf (diagram-gcontext node1) (green-gcontext node1))
	(setf (aref d-mtx i1 origin-index)
	  (setf (aref d-mtx origin-index i1) (+ dist0 1.0d0)))
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q))))

  (dolist (edge (to-edges node0))
    (declare (type Edge-Diagram edge))
    (let* ((node1 (gra:edge-node0 edge))
	   (i1 (node-index node1)))
      (declare (type Node-Diagram node1)
	       (type az:Array-Index i1))
      (when (eq (diagram-gcontext node1) (red-gcontext node1))
	(setf (diagram-gcontext node1) (green-gcontext node1))
	(setf (aref d-mtx i1 origin-index)
	  (setf (aref d-mtx origin-index i1) (+ dist0 1.0d0)))
	(setf (node-predecessor node1) node0)
	(ac:enqueue node1 q)))))

(defun animated-undirected-breadth-first-search (graph origin d-mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram graph)
	   (type Node-Diagram origin)
	   (type (Simple-Array Double-Float (* *)) d-mtx))
  
  (let ((origin-index (node-index origin))
	(q (ac:make-queue)))
    (declare (type az:Array-Index origin-index)
	     (type ac:Queue q))
    ;; initialization
    (setf (aref d-mtx origin-index origin-index) 0.0d0)
    (dolist (node (the List (gra:nodes graph)))
      (declare (type Node-Diagram node))
      (setf (diagram-gcontext node) (red-gcontext node))
      (setf (node-predecessor node) nil))
    (setf (diagram-gcontext origin) (green-gcontext origin))
    (ac:enqueue origin q)

    (loop (when (ac:empty-queue? q) (return))
      (let* ((node0 (ac:dequeue q))
	     (i0 (node-index node0))
	     (dist0 (aref d-mtx origin-index i0)))
	(declare (type Node-Diagram node0)
		 (type az:Array-Index i0)
		 (type Double-Float dist0))
	(animated-bf-search-from-node node0 dist0 q origin-index d-mtx)
	(setf (diagram-gcontext node0) (blue-gcontext node0))))))


;;;============================================================

(defun compute-node-graph-distances (solver &key (animate? nil))

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type (Member T Nil) animate?))

  (let* ((gp (diagram solver))
	 (nodes (gra:nodes gp))
	 (nnodes (length nodes))
	 (d-mtx (node-distance-mtx solver))
	 (unconnected-distance (az:c_sqrt (az:fl nnodes))))
    (declare (type Graph-Diagram gp)
	     (type List nodes)
	     (type az:Array-Index nnodes)
	     (type (Simple-Array Double-Float (* *)) d-mtx)
	     (type Double-Float unconnected-distance))
    (dotimes (i nnodes)
      (declare (type az:Array-Index i))
      (dotimes (j nnodes)
	(declare (type az:Array-Index j))
	(setf (aref d-mtx i j) unconnected-distance)))
    (if animate?
	(dolist (origin nodes)
	  (animated-undirected-breadth-first-search gp origin d-mtx))
      ;; else
      (dolist (origin nodes)
	(undirected-breadth-first-search gp origin d-mtx)))))

(defun compute-graph-distances (solver &key (animate? nil))
  
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type (Member T Nil) animate?))
  
  (compute-node-graph-distances solver :animate? animate?)
  #||
  (let* ((gp (diagram solver))
	 (node-d-mtx (node-distance-mtx solver))
	 (edge-d-mtx (edge-distance-mtx solver))
	 (gra:edges (gra:edges gp))
	 (nedges (length edges)))
    (unless (>= (array-dimension edge-d-mtx 0) nedges)
      (setf edge-d-mtx
	(make-array (list nedges nedges)
		    :element-type 'Double-Float
		    :initial-element most-positive-double-float))
      (setf (edge-distance-mtx solver) edge-d-mtx))
    (dotimes (i nedges)
      (dotimes (j nedges)
	(setf (aref edge-d-mtx i j) most-positive-double-float)))
    (dolist (edge0 edges)
      (let* ((i00 (node-index (gra:edge-node0 edge0)))
	     (i01 (node-index (gra:edge-node1 edge0)))
	     (j0 (edge-index edge0)))
	(dolist (edge1 edges)
	  (let ((j1 (edge-index edge1)))
	    (setf (aref edge-d-mtx j0 j1)
	      (setf (aref edge-d-mtx j1 j0)
		(if (eq edge0 edge1)
		    0.0d0
		  ;; else
		  (let ((i10 (node-index (gra:edge-node0 edge1)))
			(i11 (node-index (gra:edge-node1 edge1))))
		    (+ 1.0d0
		       (min (aref node-d-mtx i00 i10)
			    (aref node-d-mtx i00 i11)
			    (aref node-d-mtx i01 i10)
			    (aref node-d-mtx i01 i11))))))))))))
  ||#
  )

;;;============================================================

(defun relax-node-distances (solver amount)

  (let ((d-mtx (node-distance-mtx solver))
	(r-mtx (node-residual-mtx solver))
	(rest-len (rest-length solver)))
    (dotimes (i (array-dimension d-mtx 0))
      (dotimes (j i)
	(let* ((r ;;(min 0.2d0
		(* amount (/ (aref r-mtx i j) rest-len))
		;;     )
		)
	       (d (aref d-mtx i j)))
	  (when (and (> r 0.0d0)  (> d 1.0d0))
	    (setf (aref d-mtx i j)
	      (setf (aref d-mtx j i)
		(+ d r)))))))))