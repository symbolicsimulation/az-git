;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;------------------------------------------------------------
;;; What constraints (in additon to keeping the nodes within
;;; the window) should be imposed by default?

#||
(defgeneric default-constraints (solver)
  (declare (type Graph-Layout-Solver solver)))

(defmethod default-constraints ((solver Graph-Layout-Solver))
  (let* ((diagram (diagram solver))
	 (diagram-subject (diagram-subject diagram)))
    (if (and (gra:directed-graph? diagram-subject)
	     (gra:acyclic-graph? diagram-subject))
	(make-dag-constraints diagram :delta 2.0d0 :direction :x)
      ;; else no additional constraints are imposed!
      ())))

||#
;;;============================================================
;;; Bounds constraints:
;;;============================================================
;;; Converting bounds constraints to NPSOL representation:

(defmethod bounds-constraints ((solver Graph-Layout-Solver) bl bu)

  (declare (type Graph-Layout-Solver solver)
	   (type (Simple-Array Double-Float (*)) bl bu))
 
  (let* ((bounding-r (diagram-rect (diagram solver)))
	 (bxmin (az:fl (g:screen-rect-xmin bounding-r)))
	 (bxmax (az:fl (g:screen-rect-xmax bounding-r)))
	 (bymin (az:fl (g:screen-rect-ymin bounding-r)))
	 (bymax (az:fl (g:screen-rect-ymax bounding-r)))
	 (i 0))
    
    (declare (type g:Screen-Rect bounding-r)
	     (type Double-Float bxmin bxmax bymin bymax)
	     (type az:Array-Index i))
    
    (dolist (node (brk:bricks (brk:domain solver)))
      (declare (type Node-Diagram node))
      (let ((bounds (node-bounds node)))
	(if bounds
	    (locally (declare (type g:Chart-Rect bounds))
	      (let ((xmin (g::%chart-rect-xmin bounds))
		    (w (g::%chart-rect-width bounds))
		    (ymin (g::%chart-rect-ymin bounds))
		    (h (g::%chart-rect-height bounds)))
		(declare (type g:Chart-Coordinate xmin w ymin h))
		(setf (aref bl i) xmin)
		(setf (aref bu i) (+ xmin w))
		(incf i)
		(setf (aref bl i) ymin)
		(setf (aref bu i) (+ ymin h))
		(incf i)))
	  ;; else the bounds are unspecified,
	  ;; so use the screen rect
	  (let ((r (diagram-rect node)))
	    (declare (type g:Screen-Rect r))
	    (setf (aref bl i) bxmin)
	    (setf (aref bu i) (- bxmax (az:fl (g::%screen-rect-width r))))
	    (incf i)
	    (setf (aref bl i) bymin)
	    (setf (aref bu i) (- bymax (az:fl (g::%screen-rect-height r))))
	    (incf i)))))))

;;;============================================================
;;; User specified bounds constraints
;;;============================================================
;;; set the node bounds so the node can't move

(defun pin-node (node)
  (declare (type Node-Diagram node))
    (ac:atomic
     (setf (node-user-pinned? node) t)
  (let* ((bounds (node-bounds node))
	 (r (diagram-rect node))
	 (x (az:fl (g::%screen-rect-xmin r)))
	 (y (az:fl (g::%screen-rect-ymin r))))
    (declare (type g:Screen-Rect r)
	     (type Double-Float x y))
     (cond
      ((null bounds) ;; no bounds were ever defined
       (setf bounds (g::%borrow-chart-rect x y 0.0d0 0.0d0))
       (setf (node-bounds node) bounds))
      (t ;; bounds rect exists, fix its coordinates
       (locally (declare (type g:Chart-Rect bounds))
	 (setf (g::%chart-rect-xmin bounds) x)
	 (setf (g::%chart-rect-ymin bounds) y)
	 (setf (g::%chart-rect-width bounds) 0.0d0)
	 (setf (g::%chart-rect-height bounds) 0.0d0)))))))

;;; release the node to be anywhere in its window

(defun unpin-node (node)
  (declare (type Node-Diagram node))
  (ac:atomic
   (setf (node-user-pinned? node) nil)
   (let ((bounds (node-bounds node)))
     (when bounds
       (setf (node-bounds node) nil)
       (g::%return-chart-rect bounds)))))

;;; toggle the pinning state. Assume that if the bounds
;;; aren't nil, then it's pinned.

(defun toggle-pin-node (node)
  (declare (type Node-Diagram node))
  (if (node-user-pinned? node)
      (unpin-node node)
    ;; else
    (pin-node node)))

;;;============================================================
;;; Affine constraints:
;;;============================================================

(defun translate-affine-constraint (constraint domain constraint-mtx
				    col-index bl bu b-index)

  "translate-affine-constraint replaces the references to :width and
:height by appropriately modifying lower-bound and upper-bound. It
also replaces the references to (left #<node xxx>) and (top #<node
xxx>) by the appropriate indexes into the corresponding row of the
constraint matrix passed to NPSOL."

  (let ((bound (brk:bound constraint))
	(relation (brk:relation constraint))
	(terms (brk:terms (brk:functional constraint))))
    (dolist (term terms)
      (let* ((coef (brk:term-coefficient term))
	     (param (brk:term-parameter term))
	     (node (brk:term-brick term))
	     (i (brk:brick-index node domain)))
	(declare (type Node-Diagram node)
		 (type brk:Parameter param)
		 (type Double-Float coef)
		 (type az:Array-Index i))
	(ecase param
	  (:left
	   (setf (aref constraint-mtx i col-index) coef))
	  (:width
	   (decf bound (* coef (g:screen-rect-width (diagram-rect node)))))
	  (:top
	   (setf (aref constraint-mtx (+ 1 i) col-index) coef))
	  (:height
	   (decf bound (* coef (g:screen-rect-height (diagram-rect node)))))))
      (ecase relation
	('<= (setf (aref bl b-index) bound)
	     (setf (aref bu b-index) *npsol-infinity*))
	('= (setf (aref bl b-index) bound)
	     (setf (aref bu b-index) bound))
	('>= (setf (aref bl b-index) (- *npsol-infinity*))
	     (setf (aref bu b-index) bound))))))

;;;-------------------------------------------------------------------------

(defun translate-affine-constraints (constraints domain
				     constraint-mtx bl bu)

  ;; make sure the constraint-mtx is zeroed to start
  (zero-mtx constraint-mtx)
  (let ((col-index 0)
	(b-index (brk:dimension domain)))
    (dolist (constraint constraints)
      (translate-affine-constraint
       constraint domain constraint-mtx col-index bl bu b-index)
      (incf col-index)
      (incf b-index))))

(defun node-constrained? (node constraint)
  (find node (cddr constraint) :key #'car))

(defun affine-constraint-term-value (term)
  (let ((node (brk:term-brick term))
	(param (brk:term-parameter term))
	(coef (brk:term-coefficient term)))
    (declare (type Node-Diagram node)
	     (type brk:Parameter param)
	     (type Double-Float coef))
    (* coef
       (az:fl
	(ecase param
	  (:left   (g::%screen-rect-xmin (diagram-rect node)))
	  (:width  (g::%screen-rect-width (diagram-rect node)))
	  (:top    (g::%screen-rect-ymin (diagram-rect node)))
	  (:height (g::%screen-rect-height (diagram-rect node))))))))

;;;============================================================

(defun make-order-constraint (domain node0 node1 direction delta)
  (declare (type Node-Diagram node0 node1)
	   (type Double-Float delta)
	   (type (Member :x :y) direction))
  (ecase direction
    (:x (brk:to-left domain node0 delta node1))
    (:y (brk:above domain node0 delta node1))))

(defun make-order-constraints (diagram f direction delta)
  (declare (type Graph-Diagram diagram)
	   (type (or Function Symbol) f)
	   (type (Member :x :y) direction))
  (let ((nodes (gra:nodes diagram))
	(domain (brk:domain (layout-solver diagram)))
	(cs ())
	n0 s0 s1
	(cf (ecase direction (:x 'brk:to-left) (:y 'brk:above))))
    (loop
      (setf n0 (first nodes))
      (setf s0 (diagram-subject n0))
      (setf nodes (rest nodes))
      (when (null nodes) (return))
      (dolist (n1 nodes)
	(setf s1 (diagram-subject n1))
	(setf cs
	  (nconc
	   (cond ((funcall f s0 s1) (funcall cf domain n0 delta n1))
		 ((funcall f s1 s0) (funcall cf domain n1 delta n0))
		 (t ()))
	   cs))))
    cs))    

(defun make-=-constraint (domain node0 node1 direction)
  (declare (type Node-Diagram node0 node1)
	   (type (Member :x :y) direction))
  (let ((parameter (ecase direction (:x :left) (:y :top))))
    (brk:equal-parameter domain parameter node0 node1)))

(defun make-dag-constraints (gp
			     &key
			     (delta 16.0d0)
			     (direction :y))
  (declare (type Graph-Diagram gp)
	   (type Double-Float delta)
	   (type (Member :x :y) direction))
  (let ((cs ())
	(domain (brk:domain (layout-solver gp))))
    (dolist (node0 (gra:nodes gp))
      (dolist (edge (to-edges node0))
	(setf cs
	  (nconc
	   (make-order-constraint
	    domain (gra:edge-node0 edge) node0 direction delta) cs))))
    cs))

(defun make-sibling-constraints (gp &key
				    (delta 16.0d0)
				    (direction :y))
  (declare (type Graph-Diagram gp)
	   (type Double-Float delta)
	   (type (Member :x :y) direction))
  (let ((cs ())
	(domain (brk:domain (layout-solver gp))))
    (dolist (parent (gra:nodes gp))
      (let* ((children (node-children parent))
	     (first-child (first children)))
	(unless (null children)
	  (setf cs
	    (nconc
	     (make-order-constraint domain parent first-child direction delta)
		   cs))
	  (dolist (child (rest children))
	    (setf cs
	      (nconc
	       (make-order-constraint domain parent child direction delta)
		     cs))
	    (setf cs
	      (nconc (make-=-constraint domain first-child child direction)
		     cs))))))
    cs))

(defun roots (gp) (remove-if-not #'root-node? (gra:nodes gp)))

;;;============================================================
;;; These shouldn't work anymore,
;;; but we might want to fix them later

#||
(defun pin-roots-to-top (gp)
  (az:type-check Graph-Diagram gp)
  (let* ((sscreen-rect (diagram-rect gp))
	 (sleft (g:screen-rect-left sscreen-rect))
	 (sbottom (g:screen-rect-bottom sscreen-rect))
	 (swidth (g:screen-rect-width sscreen-rect)))
    (dolist (node (gra:nodes gp))
      (when (root-node? node)
	(let ((y (- sbottom (g:screen-rect-height (diagram-rect node)))))
	  (g:set-screen-rect-coords
	   (allowed-rect node)
	   :left sleft :top y :width swidth :height 0))))))

(defun pin-roots-to-left (gp)
  (az:type-check Graph-Diagram gp)
  (let* ((sscreen-rect (diagram-rect gp))
	 (sleft (g:screen-rect-left sscreen-rect))
	 (stop (g:screen-rect-top sscreen-rect))
	 (sheight (g:screen-rect-height sscreen-rect)))
    (dolist (node (gra:nodes gp))
      (when (root-node? node)
	(g:set-screen-rect-coords
	 (allowed-rect node)
	 :left sleft :top stop :width 0 :height sheight)))))
||#

;;;============================================================
;;; Reduce Affine Constraints when only a subset of the nodes
;;; is allowed to move.
;;;============================================================

(defun reduce-affine-constraint (constraint domain)
  
  (declare (type brk:Inequality constraint)
	   (type brk:Domain domain))
  
  (flet ((variable? (term)
	   (declare (type brk:Term term))
	   (member (brk:term-brick term) (brk:bricks domain))))
    
    (let* ((bound (brk:bound constraint))
	   (terms (brk:terms (brk:functional constraint)))
	   (constant-terms (remove-if #'variable? terms))
	   (variable-terms (remove-if-not #'variable? terms)))
      (declare (type Double-Float bound)
	       (type List terms constant-terms variable-terms))
      (unless (null variable-terms)
	(dolist (term constant-terms)
	  (declare (type List term))
	  (let ((value (affine-constraint-term-value term)))
	    (declare (type Double-Float value))
	    (decf bound value)))
	;; return reduced constraint
	(brk:make-inequality
	 bound
	 (brk:relation constraint)
	 (apply #'brk:make-functional domain variable-terms))))))

(defun reduce-affine-constraints (constraints domain)
  (declare (type List constraints)
	   (type brk:Domain domain))
  (delete ()
	  (az:with-collection
	   (dolist (constraint constraints)
	     (az:collect
	      (reduce-affine-constraint constraint domain))))))

