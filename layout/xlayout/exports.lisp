;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(eval-when (compile load eval)
 (export '(Graph-Diagram
	   make-graph-diagram
	   ;;Digraph-Diagram
	   ;;Dag-Diagram

	   node-name
	   
	   Graph-Mediator
	   Graph-Mediator-Role

	   graph-node-menu
	   graph-edge-menu
	   graph-menu	

	   animate?
	   )))
