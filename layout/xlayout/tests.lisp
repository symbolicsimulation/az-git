;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================
			     
(defun make-random-diagram-points (n diagram)
  (let* ((screen-rect (diagram-rect diagram))
	 (xmin (g:screen-rect-left screen-rect))
	 (xmax (g:screen-rect-right screen-rect))
	 (ymin (g:screen-rect-top screen-rect))
	 (ymax (g:screen-rect-bottom screen-rect))
	 (screen-points ()))
    (dotimes (i n)
      (push (make-random-screen-point
	     :xmin xmin :xmax xmax
	     :ymin ymin :ymax ymax)
	    screen-points))
    screen-points))




