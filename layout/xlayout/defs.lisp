;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defparameter *npsol-infinity* 1.0d15)

;;;============================================================

(defun ~= (x y) (<= (abs (- x y)) single-float-epsilon))

;;;============================================================

(defun ensure-large-enough-vector (v n)
  (declare (type (Simple-Array Double-Float (*)) v)
	   (type az:Array-Index n))
  (unless  (>= (array-dimension v 0) n))
    (setf v (make-array n :element-type 'Double-Float))
  v)

(defun ensure-large-enough-mtx (m nrows ncols)
  (declare (type (Simple-Array Double-Float (* *)) m)
	   (type az:Array-Index nrows ncols))
  (unless (and (>= (array-dimension m 0) nrows)
	       (>= (array-dimension m 1) ncols))
    (setf m (make-array (list nrows ncols) :element-type 'Double-Float)))
  m)

(defun zero-mtx (mtx)
  (declare (optimize (safety 3) (speed 0))
	   (type (Simple-Array Double-Float (* *)) mtx))
  (let ((ncols (array-dimension mtx 1)))
    (declare (type az:Array-Index ncols))
    (dotimes (i (array-dimension mtx 0))
      (declare (type az:Array-Index i))
      (dotimes (j ncols)
	(declare (type az:Array-Index j))
	(setf (aref mtx i j) 0.0d0)))))

;;;============================================================

(defun mtx-max (mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type (Simple-Array Double-Float (* *)) mtx))
  
  (let ((maximum most-negative-double-float))
    (declare (type Double-Float maximum))
    (dotimes (i (array-dimension mtx 0))
      (declare (type az:Array-Index i))
      (dotimes (j i)
	(declare (type az:Array-Index j))
	(let ((d (max (aref mtx i j) (aref mtx j i))))
	  (declare (type Double-Float d))
	  (when (< maximum d) (setf maximum d))))
      (let ((d  (aref mtx i i)))
	(declare (type Double-Float d))
	(when (< maximum d) (setf maximum d))))
    maximum))

(defun mtx-min (mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type (Simple-Array Double-Float (* *)) mtx))
  
  (let ((minimum most-positive-double-float))
    (declare (type Double-Float minimum))
    (dotimes (i (array-dimension mtx 0))
      (declare (type az:Array-Index i))
      (dotimes (j i)
	(declare (type az:Array-Index j))
	(let ((d (min (aref mtx i j) (aref mtx j i))))
	  (declare (type Double-Float d))
	  (when (> minimum d) (setf minimum d))))
      (let ((d  (aref mtx i i)))
	(declare (type Double-Float d))
	(when (> minimum d) (setf minimum d))))
    minimum))

(defun scale-mtx (a mtx)

  (declare (optimize (safety 3) (speed 0))
	   (type Double-Float a)
	   (type (Simple-Array Double-Float (* *)) mtx))
  
  (let ((m (array-dimension mtx 1)))
    (declare (type az:Array-Index m))
    (dotimes (i (array-dimension mtx 0))
      (declare (type az:Array-Index i))
      (dotimes (j m)
	(declare (type az:Array-Index j))
	(az:mulf (aref mtx i j) a))))
  mtx)

