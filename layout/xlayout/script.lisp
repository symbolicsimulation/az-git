;;; -*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-

(in-package :Clay) 

(defparameter *xpr-graph*
    (gra:make-graph
      `((|xwd| |xwud|) (|xwd| |xpr|) (|xwd| |X|) (|xpr| |xwd|) (|xpr| |xwud|)
	(|xpr| |X|) (|xwud| |xwd|) (|xwud| |xpr|) (|xwud| ||X||) (|xdpr| |xwd|)
	(|xdpr| |xpr|) (|xdpr| |lpr|) (|xdpr| |xwud|) (|xdpr| |X|)
	(|lpr| |lpq|) (|lpr| |lprm|) (|lpr| |plot|) (|lpr| |pr|)
	(|lpr| |screendump|) (|lpr| |troff|) (|lpr| |printcap|)
	(|lpr| |rasterfile|) (|lpr| |lpc|) (|lpr| |lpd|) (|lpq| |lpr|)
	(|lpq| |lprm|) (|lpq| |lpc|) (|lpq| |lpd|) (|lprm| |lpr|)
	(|lprm| |lpq|) (|lprm| |lpd|) (|screendump| |lpr|)
	(|screendump| |rastrepl|) (|screendump| |screenload|)
	(|screendump| |rasfilter8to1|) (|screenload| |rastrepl|)
	(|screenload| |screendump|) (|screenload| |rasfilter8to1|)
	;;(|troff| |checknr|) (|troff| |chmod|) (|troff| |eqn|)
	(|troff| |lpr|)
	;;(|troff| |nroff|) (|troff| |tbl|) (|troff| |col|)
	(|troff| |printcap|)
	;;(|troff| |man|) (|troff| |me|) (|troff| |ms|)
	(|troff| |lpd|)
	)))

(defparameter *xpr-diagram*
    (make-graph-diagram :subject *xpr-graph* :name "Man pages"
			:width 384 :height 384 :left 750 :top 490))

;;;============================================================

(progn
  (defparameter *branching* 3)
  (defparameter *depth* 2)
  (defparameter *tree* (gra::make-complete-nary-tree *branching* *depth*))
  
  (defparameter *diagram*
      (make-graph-diagram
       :subject *tree* :left 630 :top 340
       :affine-constraint-f
       #'(lambda (solver)
	   (make-dag-constraints (diagram solver) :delta 8.0d0 :direction :x))
       :name
       (format nil
	       "Complete ~d-ary Tree of depth ~d: ~d nodes"
	       *branching* *depth* (length (gra:nodes *tree*))))))


;;;============================================================

(defparameter *kamada-window*
    (make-window :width 512 :height 512
		    :left 650 :top 300
		    :backing-store :always))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a c) (a d) (b d) (b c) (c d)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.7a"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) t))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a d) (a e)
			    (b c) (b f)
			    (c d) (c g)
			    (d h)
			    (e f) (e h)
			    (f g)
			    (g h)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.7b"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a c) (a e) (a f)
			    (b c) (b d) (b e)
			    (c d) (c f)
			    (d e) (d f)
			    (e f)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.7c"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a e) (a f)
			    (b c) (b h)
			    (c d) (c j)
			    (d e) (d l)
			    (e n)
			    (f g) (f o)
			    (g h) (g p)
			    (h i)
			    (i j) (i q)
			    (j k)
			    (k l) (k r)
			    (l m)
			    (m n) (m s)
			    (n o)
			    (o t)
			    (p q) (p t)
			    (q r)
			    (r s)
			    (s t)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.7d"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a c) (a d) (a e) (a i)
			    (b c) (b e) (b f) (b g)
			    (c i) (c g) (c h)
			    (d e) (d i) (d j) (d k)
			    (e f) (e j)
			    (f g) (f j) (f l)
			    (g h) (g l)
			    (h i) (h k) (h l)
			    (i k)
			    (j k) (j l)
			    (k l)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.7e"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))


(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a d)
			    (b c) (b e)
			    (c f)
			    (d e) (d g)
			    (e f) (e h)
			    (f i)
			    (g h)
			    (h i)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.8a"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))
(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))


(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a c)
			    (b c) (b d) (b e)
			    (c e) (c f)
			    (d e) (d g) (d h)
			    (e f) (e h) (e i)
			    (f i) (f j)
			    (g h)
			    (h i)
			    (i j)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.8b"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))


(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a e) (a f)
			    (b e) (b f)
			    (c d) (c g) (c h)
			    (d g) (d h)
			    (e f)
			    (f g) (f j) (f k)
			    (g h) (g j) (g k)
			    (i j) (i m) (i n)
			    (j k) (j m) (j n)
			    (k l) (k o) (k p)
			    (l o) (l p)
			    (m n)
			    (o p)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.8c"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))


(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a j) (a k)
			    (b c) (b l)
			    (c d) (c m)
			    (d e) (d n)
			    (e f) (e o)
			    (f g) (f p)
			    (g h) (g q)
			    (h i) (h r)
			    (i j) (i s)
			    (j t)
			    (k n) (k r)
			    (l o) (l s)
			    (m p) (m t)
			    (n q)
			    (o r)
			    (p s)
			    (q t)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.8d"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))


(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a d)
			    (b c)
			    (c d)
			    (d e) (d f) (d g)
			    (f g)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.9a"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))


(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b)
			    (b c) (b e)
			    (c d) (c h) (c i)
			    (e f) (e g)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.9b"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))


(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b)
			    (b c) (b f) (b h)
			    (c f) (c i)
			    (d i) (d j)
			    (e f) (e j)
			    (f j)
			    (g j)
			    (h i)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.9c"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))


(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b)
			    (b c) (b g)
			    (c d) 
			    (d e) (d i)
			    (e f) (e k) (e l)
			    (g h) (g j)
			    (h i) (h j)
			    (k l)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.9d"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a j)
			    (b c) (b e) (b f) (b i)
			    (d g) (d t)
			    (e f)
			    (g h)
			    (h i) (h t)
			    (i j) (i r)
			    (j k) (j p) (j r)
			    (k m) (k n) (k o)
			    (l p)
			    (m n)
			    (p q)
			    (q s)
			    (s t)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.10a"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))


(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a c) (a s) (a t)
			    (b c)
			    (c e) (c o)
			    (d e) (d f)
			    (e g) (e h) (e j)
			    (f h)
			    (g j)
			    (i k)
			    (j n)
			    (k l) (k n)
			    (l m)
			    (m n)
			    (n o)
			    (o p)
			    (p q) (p r)
			    (s t)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.10b"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a e)
			    (b c) (b e)
			    (c e)
			    (d e) (d h) (d n)
			    (e g)
			    (f g) (f j)
			    (g h) (g k)
			    (h p)
			    (i k) (i p) (i s) (i t)
			    (k t)
			    (l m) (l q)
			    (m n) (m p) (m q)
			    (n o)
			    (p r)
			    (r s)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.10c"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(progn
  (defparameter *kamada-diagram*
      (make-diagram-for-graph
       (gra:make-graph `((a b) (a e)
			    (b c) (b d)
			    (c d)
			    (d e) (d k)
			    (e f) (e g) (e i)
			    (f g)
			    (h i) (h n) (h o) (h p) (h q)
			    (i j)
			    (j k) (j m) (j n)
			    (k s)
			    (l s) (l t)
			    (o r)
			    (q r)
			    (s t)))
       *kamada-window*
       :diagram-name "Kamada Fig 5.10d"))
  (defparameter *kamada-solver* (layout-solver *kamada-diagram*))
  (setf (animate? *kamada-solver*) nil))

(time 
 (progn
   (setf (active-node-pairs *kamada-solver*)
     (gra:all-unordered-pairs (gra:nodes *kamada-diagram*)))
   (setf (rest-length *kamada-solver*)
     (estimate-solver-rest-length *kamada-solver*))
   (compute-graph-distances *kamada-diagram*)
   (solve-layout *kamada-solver*)))

(time (swap-solve-layout *kamada-diagram*))

(setf (node-pair-energy-f *kamada-solver*) 'spring-energy-0)
(setf (node-pair-energy-f *kamada-solver*) 'spring-energy)
(setf (node-pair-energy-f *kamada-solver*) 'spring-energy-2)
(setf (node-pair-energy-f *kamada-solver*) 'spring-energy-3)
(setf (node-pair-energy-f *kamada-solver*) 'spring-energy-4)
(setf (node-pair-energy-f *kamada-solver*) 'spring-energy-5)

;;;============================================================

(defparameter *horse-pedigree* (make-pedigree))

(defparameter *horse-window*
    (make-window :width 512 :height 512
		    :left 630 :top 340
		    :backing-store :always))

(defparameter *horse-diagram*
    (make-diagram-for-pedigree
     *horse-pedigree* *horse-window*))

(defparameter *horse-solver* (layout-solver *horse-diagram*))

(progn
  (setf (active-node-pairs *horse-solver*)
    (gra:all-unordered-pairs (gra:nodes *horse-diagram*)))
  (setf (rest-length *horse-solver*) 28.0d0)
  (time (compute-graph-distances *horse-diagram*))
  (time (solve-layout *horse-solver*)))

(progn
  (setf (constraints *horse-solver*)
    (make-dag-constraints *horse-diagram*
			      :delta (* 0.2 (rest-length *horse-solver*))))
  (solve-layout *horse-solver*)

  (time (swap-solve-layout *horse-diagram*))
  (solve-layout *horse-solver*)
  )

(progn
  (setf (constraints *horse-solver*)
    (make-sibling-constraints *horse-diagram*
			      :delta (* 0.2 (rest-length *horse-solver*))))
  (solve-layout *horse-solver*)
 )

;;;============================================================

(defparameter *abcdefgh-window*
    (make-window :width 256 :height 256
		    :left 750 :top 4
		    :backing-store :always))

(defparameter *abcdefgh-diagram*
    (make-diagram-for-graph
     (gra:make-graph `((a b) (a e) (e b) (e d)
			  (b d) (b c) (b f) (b g) (b h) (c d)))
     *abcdefgh-window*
     :diagram-name "abcdefgh"))
(defparameter *abcdefgh-solver* (layout-solver *abcdefgh-diagram*))

(compute-graph-distances *abcdefgh-diagram*)
(solve-layout *abcdefgh-solver*)
(jitter-distances *abcdefgh-diagram*)

(time
 (setf vals
   (sort
    (az:with-collection
     (dotimes (i 100)
       (print i)
       (setf *abcdefgh-diagram*
	 (make-diagram-for-graph
	  (gra:make-graph `((a b) (a e) (e b) (e d)
			       (b d) (b c) (b f) (b g) (b h) (c d)))
	  *abcdefgh-window*
	  :diagram-name "abcdefgh"))
       (compute-graph-distances *abcdefgh-diagram*)
       (setf *abcdefgh-solver* (layout-solver *abcdefgh-diagram*))
       (setf (animate? *abcdefgh-solver*) nil)
       (solve-layout *abcdefgh-solver*)
       (swap-solve-layout *abcdefgh-diagram* :animate? nil)
       (az:collect (solve-layout *abcdefgh-solver*))))
    #'<)))

(count (first vals) vals
       :test #'(lambda (x y) (<= (abs (- x y)) (* (first vals) 2.0d-5))))

(nthcdr 74 vals )
(time (solve-layout *abcdefgh-solver*))
(time (swap-solve-layout *abcdefgh-diagram*))

(setf (node-pair-energy-f *abcdefgh-solver*) 'spring-energy-0)
(setf (node-pair-energy-f *abcdefgh-solver*) 'spring-energy)
(setf (node-pair-energy-f *abcdefgh-solver*) 'spring-energy-2)
(setf (node-pair-energy-f *abcdefgh-solver*) 'spring-energy-3)
(setf (node-pair-energy-f *abcdefgh-solver*) 'spring-energy-4)
(setf (node-pair-energy-f *abcdefgh-solver*) 'spring-energy-5)


(defparameter *abcdefgh-homotopy* (make-instance 'Homotopy-GL-Solver
			       :diagram *abcdefgh-diagram*))

(setf (layout-solver *abcdefgh-diagram*) *abcdefgh-homotopy*)
(setf (layout-solver *abcdefgh-diagram*) *abcdefgh-solver*)

(setf (rho *abcdefgh-homotopy*) 1.0d0)
(setf (rho *abcdefgh-homotopy*) 0.0d0)
(setf (animate? *abcdefgh-homotopy*) nil)

(loop
  (when (< (rho *abcdefgh-homotopy*) 1.0d-3) (return))
  (decf (rho *abcdefgh-homotopy*) 0.2d0)
  (solve-layout *abcdefgh-homotopy*))

;;;============================================================

(defparameter *tetra-window*
    (make-window :width 256 :height 256
		       :left 750 :top 490
		       :backing-store :always))

(defparameter *tetra-diagram*
    (make-diagram-for-graph
     (gra:make-graph `((a b) (a c) (a d) (b c) (b d) (c d)))
     *tetra-window*
     :diagram-name "Tetrahedron"))

(dolist (origin (gra:nodes *tetra-diagram*))
  (undirected-breadth-first-search *tetra-diagram* origin))
(dolist (node (gra:nodes *tetra-diagram*))
  (setf (aref (node-pnt node) 2) (random 100.0d0)))
(defparameter *tetra-solver* (layout-solver *tetra-diagram*))
(setf (layout-solver *tetra-diagram*) *tetra-solver*)

(time (solve-layout *tetra-solver*))

(defparameter *tetra-homotopy* (make-instance 'Homotopy-GL-Solver
			       :diagram *tetra-diagram*))

(setf (layout-solver *tetra-diagram*) *tetra-homotopy*)

(setf (rho *tetra-homotopy*) 1.0d0)
(setf (rho *tetra-homotopy*) 0.0d0)
(setf (animate? *tetra-homotopy*) nil)

(loop
  (when (< (rho *tetra-homotopy*) 1.0d-3) (return))
  (az:mulf (rho *tetra-homotopy*) 0.75d0)
  (solve-layout *tetra-homotopy*))

;;;============================================================
;;; from Andersen, Krebs, and Andersen
;;; "STENO: An expert system for medical diagnosis basaed on
;;; graphical models and model search."

(defparameter *aka-window* (make-window))

(defparameter *aka-nodes*
    '(sex
      angina
      previous-AMI
      Q-wave-in-ecg
      q-wave-informative
      st-segment-informative
      st-segment-shift
      maximum-workload
      left-ventricular-hypertrophy
      hypercholestrolemia
      smoker
      hereditary-predisposition
      congential-heart-disease
      coronary-artery-disease
      ))

(defparameter *aka-graph*
    (gra:make-graph
     '((sex angina)
       (sex Q-wave-in-ecg)
       (sex st-segment-informative)
       (angina Q-wave-in-ecg)
       (angina hypercholestrolemia)
       (angina maximum-workload)
       (angina left-ventricular-hypertrophy)
       (angina congential-heart-disease)
       (angina st-segment-informative)
       (angina st-segment-shift)
       (angina coronary-artery-disease)
       (angina previous-AMI)
       (previous-ami left-ventricular-hypertrophy)
       (previous-ami maximum-workload)
       (previous-ami congential-heart-disease)
       (previous-ami st-segment-shift)
       (previous-ami st-segment-informative)
       (previous-ami Q-wave-in-ecg)
       (Q-wave-in-ecg hereditary-predisposition)
       (Q-wave-in-ecg left-ventricular-hypertrophy)
       (Q-wave-in-ecg st-segment-informative)
       (Q-wave-in-ecg q-wave-informative)
       (Q-wave-in-ecg coronary-artery-disease)
       (q-wave-informative left-ventricular-hypertrophy)
       (q-wave-informative st-segment-informative)
       (st-segment-informative coronary-artery-disease)
       (st-segment-informative smoker)
       (st-segment-informative maximum-workload)
       (st-segment-informative congential-heart-disease)
       (st-segment-informative st-segment-shift)
       (st-segment-shift congential-heart-disease)
       (st-segment-shift coronary-artery-disease)
       (st-segment-shift left-ventricular-hypertrophy)
       (maximum-workload hypercholestrolemia)
       (maximum-workload left-ventricular-hypertrophy)
       (maximum-workload congential-heart-disease)
       (left-ventricular-hypertrophy congential-heart-disease)
       (hypercholestrolemia coronary-artery-disease)
       (smoker coronary-artery-disease)
       (hereditary-predisposition coronary-artery-disease)
       (congential-heart-disease coronary-artery-disease)

       )))

(defparameter *aka-diagram*
    (make-diagram-for-graph *aka-graph* *aka-window*))

(time 
 (dolist (origin (gra:nodes *aka-diagram*))
   (undirected-breadth-first-search
    *aka-diagram* origin)))


(describe (sixth (gra:nodes *aka-diagram*)))

(defparameter *small-aka-window* (make-window))
(defparameter *small-aka-graph*
    (gra:make-graph
     '((cath sex)
       (cath previous-AMI)
       (cath Q-wave-in-ecg)
       (cath q-wave-informative)
       (cath st-segment-shift)
       (cath maximum-workload)
       (cath hypercholestrolemia)
       (cath smoker)
       (cath hereditary-predisposition)
       (cath congential-heart-disease)
       (Q-wave-in-ecg hereditary-predisposition)
       (Q-wave-in-ecg sex)
       (Q-wave-in-ecg q-wave-informative)
       (Q-wave-in-ecg previous-AMI)
       (st-segment-shift previous-AMI)
       (st-segment-shift congential-heart-disease)
       (previous-AMI congential-heart-disease)
       )))

(defparameter *small-aka-diagram*
    (let ((p (make-diagram-for-graph
	     *small-aka-graph* *small-aka-window*
	     :diagram-name "AKA")))
      (time 
       (dolist (origin (gra:nodes p))
	 (undirected-breadth-first-search p origin)))
      p))



;;;============================================================
;;; from Lauritzen and Spiegalhalter,
;;; "Local Computations with Probabilities on Graphical Structures
;;; and their application to expert systems,"
;;; JRSSB (1988) 50, (2) 157-224.

(defparameter *ls-window* (make-window))
(defparameter *ls-graph*
    (make-dag '((visit-to-asia tuberculosis)
			   (tuberculosis tb-or-cancer)
			   (lung-cancer tb-or-cancer)
			   (tb-or-cancer positive-x-ray)
			   (smoking lung-cancer)
			   (smoking bronchitis)
			   (bronchitis dyspnea)
			   (tb-or-cancer dyspnea)
			   )))

(defparameter *ls-diagram*
    (make-diagram-for-dag *ls-graph* *ls-window*))



;;;============================================================

(defparameter *gs* (make-window))
(defparameter *g* (make-digraph '((a b) (a c) (a d) (a e)
					 (a f) (b z) (c z) (d z)
					 (e z) (f z)
					 (z g) (z h) (i z) (j z)
					 (k z) (l z)
					 )))

(defparameter *gp* (make-diagram-for-digraph *g* *gs*))


;;;============================================================

(defparameter *graph* (gra:make-graph '((alpha beta)
					   (beta gamma)
					   (gamma delta)
					   (delta alpha))))

(defparameter *gp* 
    (make-diagram-for-graph
     *graph* *window*))


;;;============================================================

(defparameter *triangles*
	      (gra:make-graph '((a b)(a d)(a e)
				   (b e)(b f)(b c)
				   (c f)(c g)
				   (d h)(d i)(d e)
				   (e i)(e j)(e f)
				   (f j)(f k)(f g)
				   (g k)(g l)
				   (h m)(h i)
				   (i m)(i n)(i j)
				   (j n)(j o)(j k)
				   (k o)(k p)(k l)
				   (l p)
				   (m q)(m n)
				   (n q)(n r)(n o)
				   (o r)(o s)(o p)
				   (p s)(q r)(r s)
				   )))
(length (gra:nodes *triangles*))
(defparameter *trianglep* 
	      (make-diagram-for-graph
		*triangles*
		(default-window)))

;;;============================================================
;;; this example is from Bohringer and Paulisch,
;;; "Using constraints to schieve stability in automatic
;;; graph layout algroithms", CHI'90 Proceedings

(defparameter *physics-window* (make-window))
(defparameter *physics-g*
	      (gra:make-graph
		'((mechanics general-relativity)
		  (mechanics kinetics)
		  (mechanics statics)
		  (mechanics wave-theory)
		  (mechanics quantum-mechanics)
		  (kinetic-thermodynamics thermodynamics)
		  (electricity electrodynamics)
		  (electricity electrostatics)
		  (kinetics special-relativity)
		  (kinetics atomic-physics)
		  (wave-theory acoustics)
		  (wave-theory optics)
		  (thermodynamics atomic-physics)
		  (electrodynamics atomic-physics)
		  (special-relativity general-relativity)
		  (optics atomic-physics)
		  (atomic-physics quantum-mechanics)
		  (atomic-physics nuclear-physics)
		  (atomic-physics physics-of-solids)
		  (quantum-mechanics elementary-particle-physics)
		  (quantum-mechanics nuclear-physics)
		  (quantum-mechanics physics-of-solids)
		  (nuclear-physics elementary-particle-physics)
		  )))

(defparameter *physics-p* 
	      (make-diagram-for-dag
		*physics-g*
		*physics-window*))






