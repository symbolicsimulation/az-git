;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Crusher-GL-Solver (Homotopy-3d-GL-Solver)
	  ((zmin
	   :type Double-Float
	   :accessor zmin
	   :initform 0.0d0)
	   (zmax
	   :type Double-Float
	   :accessor zmax
	   :initform 1.0d3)
	   (node-pair-energy-f
	    :type Function
	    :accessor node-pair-energy-f
	    :initform 'npe-3d-1)))

;;;------------------------------------------------------------
;;; Converting constraints to NPSOL representation
;;;------------------------------------------------------------

(defmethod bounds-constraints ((solver Crusher-GL-Solver) bl bu)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Crusher-GL-Solver solver)
	   (type (Simple-Array Double-Float (*)) bl bu))
 
  (let* ((diagram (diagram solver))
	 (bounding-rect (slate:slate-rect (diagram-slate diagram)))
	 (xmin (g:fl (g:screen-rect-xmin bounding-rect)))
	 (xmax (g:fl (g:screen-rect-xmax bounding-rect)))
	 (ymin (g:fl (g:screen-rect-ymin bounding-rect)))
	 (ymax (g:fl (g:screen-rect-ymax bounding-rect)))
	 (zmin (zmin solver))
	 (zmax (zmax solver))
	 (i 0))
    (declare (type Graph-Presentation diagram)
	     (type g:Screen-Rect bounding-rect)
	     (type Double-Float xmin xmax ymin ymax zmax)
	     (type tools:Array-Index i))
    (dolist (node (the List (gr:nodes diagram)))
      (declare (type Node-Presentation node))
      (let ((rect (slate-rect node)))
	(declare (type g:Screen-Rect rect))
	;; x bounds
	(setf (aref bl i) xmin)
	(setf (aref bu i) (- xmax (g:fl (g::%screen-rect-width rect))))
	(incf i)
	;; y bounds
	(setf (aref bl i) ymin)
	(setf (aref bu i) (- ymax (g:fl (g::%screen-rect-height rect))))
	(incf i)
	;; z bounds
	(setf (aref bl i) zmin) 
	(setf (aref bu i) zmax)
	(incf i)))))

