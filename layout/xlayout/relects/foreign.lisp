;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Clay -*-
;;;
;;; Copyright 1991. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Clay)

(eval-when (compile load eval)
  (proclaim '(optimize
	      (compilation-speed 0)
	      (safety 3)
	      (space 0)
	      (speed 1)))

  #-:excl (error "This file can only be used with Franz Allegro CL.")
  #+:allegro-v3.1 (require :Foreign)
  #+:allegro-v4.0 (cltl1:require :Foreign)

  (export '(c_spring_energy
	    c_sphered_spring_energy))

  (defun load-truename (&optional (errorp nil))
    (flet ((bad-time ()
	     (when errorp
	       (error "LOAD-TRUENAME called but a file isn't being loaded."))))
      #+Lispm  (or sys:fdefine-file-pathname (bad-time))
      #+excl   excl::*source-pathname*
      #+Xerox  (pathname (or (il:fullname *standard-input*) (bad-time)))
      #+(and dec vax common) (truename (sys::source-file #'load-truename))
      ;;
      ;; The following use of  `lucid::' is a kludge for 2.1 and 3.0
      ;; compatibility.  In 2.1 it was in the SYSTEM package, and i
      ;; 3.0 it's in the LUCID-COMMON-LISP package.
      ;;
      #+LUCID (or lucid::*source-pathname* (bad-time))))

  (defun load-directory ()
    (make-pathname :directory (pathname-directory (load-truename))))

  )

;;;=======================================================

;;(ff:reset-entry-point-table)

(load (merge-pathnames (load-directory) (pathname "c-tools.o"))
      :system-libraries '("m" "c"))

(ff:defforeign 'c_sphered_spring_energy
	       :entry-point (ff:convert-to-lang "c_sphered_spring_energy"
						:language :C)
	       :language :C
	       :arguments '((Simple-Array Double-Float (*)) ;; p0
			    (Simple-Array Double-Float (*)) ;; p1
			    (Simple-Array Double-Float (*)) ;; s0
			    (Simple-Array Double-Float (*)) ;; s1
			    (Simple-Array Double-Float (1)) ;; rest
			    (Simple-Array Double-Float (1)) ;; l
			    (Simple-Array Double-Float (1)) ;; w
			    (Simple-Array Double-Float (1)) ;; d_l
			    (Simple-Array Double-Float (1)) ;; value
			    )
	       :print t
	       :arg-checking nil
	       :return-type :void)

(ff:defforeign 'c_sphered_spring_energy01
	       :entry-point (ff:convert-to-lang "c_sphered_spring_energy01"
						:language :C)
	       :language :C
	       :arguments '((Simple-Array Double-Float (*)) ;; p0
			    (Simple-Array Double-Float (*)) ;; p1
			    (Simple-Array Double-Float (*)) ;; s0
			    (Simple-Array Double-Float (*)) ;; s1
			    (Simple-Array Double-Float (1)) ;; rest
			    (Simple-Array Double-Float (1)) ;; l
			    (Simple-Array Double-Float (1)) ;; w
			    (Simple-Array Double-Float (1)) ;; d_l
			    (Simple-Array Double-Float (1)) ;; value
			    (Simple-Array Double-Float (*)) ;; g0
			    (Simple-Array Double-Float (*)) ;; g1
			    )
	       :print t
	       :arg-checking nil
	       :return-type :void)

(ff:defforeign 'c_sphered_spring_energy0
	       :entry-point (ff:convert-to-lang "c_sphered_spring_energy01"
						:language :C)
	       :language :C
	       :arguments '((Simple-Array Double-Float (*)) ;; p0
			    (Simple-Array Double-Float (*)) ;; p1
			    (Simple-Array Double-Float (*)) ;; s0
			    (Simple-Array Double-Float (*)) ;; s1
			    (Simple-Array Double-Float (1)) ;; rest
			    (Simple-Array Double-Float (1)) ;; l
			    (Simple-Array Double-Float (1)) ;; w
			    (Simple-Array Double-Float (1)) ;; d_l
			    (Simple-Array Double-Float (1)) ;; value
			    (Simple-Array Double-Float (*)) ;; g0
			    )
	       :print t
	       :arg-checking nil
	       :return-type :void)

(ff:defforeign 'c_sphered_spring_energy1
	       :entry-point (ff:convert-to-lang "c_sphered_spring_energy01"
						:language :C)
	       :language :C
	       :arguments '((Simple-Array Double-Float (*)) ;; p0
			    (Simple-Array Double-Float (*)) ;; p1
			    (Simple-Array Double-Float (*)) ;; s0
			    (Simple-Array Double-Float (*)) ;; s1
			    (Simple-Array Double-Float (1)) ;; rest
			    (Simple-Array Double-Float (1)) ;; l
			    (Simple-Array Double-Float (1)) ;; w
			    (Simple-Array Double-Float (1)) ;; d_l
			    (Simple-Array Double-Float (1)) ;; value
			    (Simple-Array Double-Float (*)) ;; g1
			    )
	       :print t
	       :arg-checking nil
	       :return-type :void)


