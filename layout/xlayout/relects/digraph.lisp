;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================
;;; Directed Graphs:
;;;============================================================

(defclass Digraph-Presentation (Graph-Presentation) ())

;;;------------------------------------------------------------
;;; An example of a class that uses this presentation

(defmethod default-presentation-class ((graph gr:Digraph))
  (find-class 'Digraph-Presentation))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-children ((diagram DiGraph-Presentation))
  (let* ((subject (subject diagram))
	 (snodes (gr:nodes subject))
	 (sedges (gr:edges subject)))
    ;; make presentations for the nodes in the graph
    (setf (gr:nodes diagram)
      (tools:with-collection
       (dolist (snode snodes)
	 (tools:collect (make-node-presentation snode diagram)))))
    (graph-presentation-index-nodes diagram)

    ;; make presentations for the edges in the graph
    ;; and recreate the graph structure
    ;; in the presentation nodes and edges
    (setf (gr:edges diagram)
      (tools:with-collection
       (dolist (sedge sedges)
	 (tools:collect (make-edge-presentation sedge diagram)))))
    (graph-presentation-index-edges diagram)))

;;;----------------------------------------------------------------

(defmethod edge-class ((diagram Digraph-Presentation))
  (find-class 'Dedge-Presentation))




