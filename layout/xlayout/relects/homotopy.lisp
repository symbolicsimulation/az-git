;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Homotopy-GL-Solver (Graph-Layout-Solver)
	  ((rho
	   :type Double-Float
	   :accessor rho
	   :initform 1.0d0)
	   (node-pair-energy-f
	    :type Function
	    :accessor node-pair-energy-f
	    :initform 'npe-rho-1)))

;;;------------------------------------------------------------

(defun npe-rho-1 (solver node0 node1 gradient? residual?)

  (declare (optimize (compilation-speed 0)
		     (safety 0)
		     (space 0)
		     (speed 3))
	   (type Graph-Layout-Solver solver)
	   (type Node-Presentation node0 node1)
	   (type (Member t nil)  gradient? residual?))

  (let* ((rho (rho solver))
	 (pnt0 (node-pnt node0))
	 (pnt1 (node-pnt node1))
	 (d-mtx (node-distance-mtx solver))
	 (i0 (node-index node0))
	 (i1 (node-index node1))
	 (l (* (the Double-Float (rest-length solver))
	       (aref d-mtx i0 i1)))
	 (div (+ rho (* (- 1.0d0 rho) l)))
	 (dx (- (aref pnt0 0) (aref pnt1 0)))
	 (dy (- (aref pnt0 1) (aref pnt1 1)))
	 (dx2 (* dx dx))
	 (dy2 (* dy dy))
	 (dx2+dy2 (+ dx2 dy2))
	 (d (the (Double-Float 0.0 *) (sqrt dx2+dy2)))
	 (d-l (- d l))
	 (d-l/div (/ d-l div)))

    (declare (type (Simple-Array Double-Float (*)) pnt0 pnt1)
	     (type (Simple-Array Double-Float (* *)) d-mtx)
	     (type tools:Array-Index i0 i1)
	     (type (Double-Float 0.0 *) l d dx2 dy2 dx2+dy2)
	     (type Double-Float div dx dy d-l d-l/div))

    (unless (or (not gradient?) (zerop d))
      (let* ((alpha (/ (* 2.0d0 d-l/div) d l))
	     (gx (* alpha dx))
	     (gy (* alpha dy))
	     (grd0 (node-grd node0))
	     (grd1 (node-grd node1)))
	(declare (type Double-Float alpha gx gy)
		 (type (Simple-Array Double-Float (*)) grd0 grd1))
	(incf (aref grd0 0) gx)
	(incf (aref grd0 1) gy)
	(decf (aref grd1 0) gx)
	(decf (aref grd1 1) gy)))

    (* d-l/div d-l/div)))

