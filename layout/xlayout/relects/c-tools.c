#include 	<fcntl.h>
#include 	<stdio.h>
#include	<math.h>

/****************************************************************/
/* Energy Calculations for Graph Layout                         */
/****************************************************************/

#define len(x,y) (sqrt((x)*(x) + (y)*(y)))

/****************************************************************/

void c_sphered_spring_energy (p0, p1, s0, s1, rest, l, w, d_l, objf)

     double p0[2], p1[2]; /* current positions of nodes */
     double s0[2], s1[2]; /* scales for sphered metric */
     double *rest;        /* rest length for unit distance */
     double *l;           /* desired distance */
     double *w;           /* weight for this pair of nodes */
     double *d_l;           /* residual */
     double *objf;
       
{
  double delta_x, delta_y, dx, dy, d, alpha, gx, gy;

  delta_x = s0[0] + s1[0] + 0.05;
  delta_y = s0[1] + s1[1] + 0.05;

  dx = (p0[0] - p1[0]) * delta_x;
  dy = (p0[1] - p1[1]) * delta_y;

  d = len(dx,dy);

  *d_l = d - (*rest * (*l));

  *objf += (*w * *d_l * *d_l);
}

void c_sphered_spring_energy01 (p0, p1, s0, s1, rest, l, w, d_l, objf, g0, g1)

     double p0[2], p1[2]; /* current positions of nodes */
     double s0[2], s1[2]; /* scales for sphered metric */
     double *rest;        /* rest length for unit distance */
     double *l;           /* desired distance */
     double *w;           /* weight for this pair of nodes */
     double *d_l;           /* residual */
     double *objf;
     double g0[2], g1[2]; /* gradients */
       
{
  double delta_x, delta_y, dx, dy, d, alpha, gx, gy;

  delta_x = s0[0] + s1[0] + 0.05;
  delta_y = s0[1] + s1[1] + 0.05;

  dx = (p0[0] - p1[0]) * delta_x;
  dy = (p0[1] - p1[1]) * delta_y;

  d = len(dx,dy);

  *d_l = d - (*rest * (*l));

  alpha = (2.0 * (*w) * (*d_l)) / d ;
  
  gx = alpha * delta_x * dx;
  gy = alpha * delta_y * dy;

  g0[0] += gx;
  g0[1] += gy;
  g1[0] -= gx;
  g1[1] -= gy;
  
  *objf += (*w * *d_l * *d_l);
}

double c_sphered_spring_energy0 (p0, p1, s0, s1, rest, l, w, d_l, objf, g0)

     double p0[2], p1[2]; /* current positions of nodes */
     double s0[2], s1[2]; /* scales for sphered metric */
     double *rest;        /* rest length for unit distance */
     double *l;           /* desired distance */
     double *w;           /* weight for this pair of nodes */
     double *d_l;           /* residual */
     double *objf;
     double g0[2]; /* gradient */
       
{
  double delta_x, delta_y, dx, dy, d, alpha;

  delta_x = s0[0] + s1[0] + 0.05;
  delta_y = s0[1] + s1[1] + 0.05;

  dx = (p0[0] - p1[0]) * delta_x;
  dy = (p0[1] - p1[1]) * delta_y;

  d = len(dx,dy);

  *d_l = d - (*rest * (*l));

  alpha = (2.0 * (*w) * (*d_l)) / d ;
  
  g0[0] += alpha * delta_x * dx;
  g0[1] += alpha * delta_y * dy;
  
  *objf += (*w * *d_l * *d_l);
}

double c_sphered_spring_energy1 (p0, p1, s0, s1, rest, l, w, d_l, objf, g1)

     double p0[2], p1[2]; /* current positions of nodes */
     double s0[2], s1[2]; /* scales for sphered metric */
     double *rest;        /* rest length for unit distance */
     double *l;           /* desired distance */
     double *w;           /* weight for this pair of nodes */
     double *d_l;           /* residual */
     double *objf;
     double g1[2]; /* gradients */
       
{
  double delta_x, delta_y, dx, dy, d, alpha;

  delta_x = s0[0] + s1[0] + 0.05;
  delta_y = s0[1] + s1[1] + 0.05;

  dx = (p0[0] - p1[0]) * delta_x;
  dy = (p0[1] - p1[1]) * delta_y;

  d = len(dx,dy);

  *d_l = d - (*rest * (*l));

  alpha = (2.0 * (*w) * (*d_l)) / d ;
  
  g1[0] -= alpha * delta_x * dx;
  g1[1] -= alpha * delta_y * dy;
  
  *objf += (*w * *d_l * *d_l);
}
