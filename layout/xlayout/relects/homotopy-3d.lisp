;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Homotopy-3d-GL-Solver (Homotopy-GL-Solver)
	  ((node-pair-energy-f
	    :type Function
	    :accessor node-pair-energy-f
	    :initform 'npe-3d-1)))

;;;------------------------------------------------------------

(defmethod solve-layout ((solver Homotopy-3d-GL-Solver))

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Graph-Layout-Solver solver))

  (let* ((diagram (diagram solver))
	 (n (* 3 (length (active-nodes solver))))
	 (x (make-array n :element-type 'Double-Float))
	 (bl (make-array n :element-type 'Double-Float))
	 (bu (make-array n :element-type 'Double-Float))
	 (a (make-array (list n 0)
			:element-type 'Double-Float
			:initial-element 0.0d0)))
    (declare (type Graph-Presentation diagram)
	     (type tools:Array-Index n)
	     (type (Simple-Array Double-Float (*)) x bl bu)
	     (type (Simple-Array Double-Float (* *)) a))

    (initialize-node-float-coordinates diagram)
    (nodes->npsol3 (active-nodes solver) x)
    (bounds-constraints solver bl bu)

    (set-npsol-default-options solver)

    (npsol:npsol-minimize (make-npsol-objfn solver) x a bl bu)

    ;; draw the final state, even if we not animating.
    (npsol->nodes3 x (active-nodes solver))
    (attach-edges solver diagram)
    (slate:clear-slate (diagram-slate diagram))
    (draw-diagram diagram)
    (slate:force-drawing (diagram-slate diagram))))

;;;------------------------------------------------------------
;;; Energy calculation
;;;------------------------------------------------------------

(defmethod make-npsol-objfn ((solver Homotopy-3d-GL-Solver))

  (declare (optimize (compilation-speed 0)
		     (safety 3)
		     (space 0)
		     (speed 1))
	   (type Graph-Layout-Solver solver))

  (let ((diagram (diagram solver))
	(active-nodes (active-nodes diagram)))
    (declare (type Graph-Presentation diagram)
	     (type List active-nodes))

    #'(lambda (mode n x objf objgrd nstate)
	(declare (optimize (compilation-speed 0)
			   (safety 1)
			   (space 0)
			   (speed 3))
		 (type (Simple-Array Fixnum (1)) mode)
		 (type (Simple-Array Fixnum (1)) n)
		 (type (Simple-Array Double-Float (*)) x)
		 (type (Simple-Array Double-Float (1)) objf)
		 (type (Simple-Array Double-Float (*)) objgrd)
		 (type (Simple-Array Fixnum (1)) nstate)	       
		 (ignore n nstate))

	;; first update the state of the graph presentation
	(when (animate? solver) (erase-diagram diagram nil))
	(npsol->nodes3 x active-nodes)
	(when (animate? solver)
	  (attach-edges solver diagram)
	  (draw-diagram diagram)
	  (slate:force-drawing (diagram-slate diagram)))
	(ecase (aref mode 0)
	  (0 (calculate-energy solver objf objgrd nil nil))
	  (1 (calculate-energy solver objf objgrd t nil))
	  (2 (calculate-energy solver objf objgrd t nil))))))

;;;------------------------------------------------------------

(defmethod calculate-energy ((solver Homotopy-3d-GL-Solver)
			     objf grad gradient? residual?)

  (declare (optimize (compilation-speed 0)
		     (safety 0)
		     (space 0)
		     (speed 3))
	   (type Graph-Layout-Solver solver)
	   (type (Simple-Array Double-Float (*)) objf grad)
	   (type (Member t nil)  gradient? residual?))

  (let ((active-nodes (active-nodes solver))
	(all-nodes (gr:nodes (diagram solver))))
    (declare (type List active-nodes all-nodes))

    (setf (aref objf 0) 0.0d0)
    (when gradient? (zero-node-gradients active-nodes))

    (do ((gr:nodes all-nodes (rest nodes)))
	((null nodes))
      (declare (type List nodes))
      (let ((node0 (first nodes)))
	(declare (type Node-Presentation node0))
	(when (member node0 active-nodes)
	  (dolist (node1 (rest nodes))
	    (declare (type Node-Presentation node1))
	    (funcall (node-pair-energy-f solver)
		     solver node0 node1 gradient? residual? objf)))))

    (when gradient? (nodes->objgrd3 active-nodes grad))))

;;;------------------------------------------------------------

(defun npe-3d-1 (solver node0 node1 gradient? residual?)

    (declare (optimize (compilation-speed 0)
		       (safety 0)
		       (space 0)
		       (speed 3))
	     (type Graph-Layout-Solver solver)
	     (type Node-Presentation node0 node1)
	     (type (Member t nil)  gradient? residual?))
    residual?
    (let* ((pnt0 (node-pnt node0))
	   (pnt1 (node-pnt node1))
	   (d-mtx (node-distance-mtx solver))
	   (i0 (node-index node0))
	   (i1 (node-index node1))
	   (rho (rho solver))
	   (l (* (the Double-Float (rest-length solver))
		 (aref d-mtx i0 i1)))
	   (dx (- (aref pnt0 0) (aref pnt1 0)))
	   (dy (- (aref pnt0 1) (aref pnt1 1)))
	   (dz (* rho (- (aref pnt0 2) (aref pnt1 2))))
	   (dx2 (* dx dx))
	   (dy2 (* dy dy))
	   (dz2 (* dz dz))
	   (d2 (+ dx2 dy2 dz2))
	   (d (the (Double-Float 0.0 *) (sqrt d2)))
	   (d-l (- d l))
	   (d-l/l (/ d-l l)))

      (declare (type (Simple-Array Double-Float (*)) pnt0 pnt1)
	       (type (Simple-Array Double-Float (* *)) d-mtx)
	       (type tools:Array-Index i0 i1)
	       (type (Double-Float 0.0 *) rho l d dx2 dy2 dz2 d2)
	       (type Double-Float dx dy dz d-l d-l/l))

      (unless (or (not gradient?) (zerop d))
	(let* ((alpha (/ (* 2.0d0 d-l/l) d l))
	       (gx (* alpha dx))
	       (gy (* alpha dy))
	       (gz (* alpha rho dz))
	       (grd0 (node-grd node0))
	       (grd1 (node-grd node1)))
	  (declare (type Double-Float alpha gx gy gz)
		   (type (Simple-Array Double-Float (*)) grd0 grd1))
	  (incf (aref grd0 0) gx)
	  (incf (aref grd0 1) gy)
	  (incf (aref grd0 2) gz)
	  (decf (aref grd1 0) gx)
	  (decf (aref grd1 1) gy)
	  (decf (aref grd1 2) gz)))

      (* d-l/l d-l/l)))

;;;------------------------------------------------------------
;;; Transforming between representations of graph layout
;;;------------------------------------------------------------
;;; Transforming from the state of Graph-Presentation
;;; to the alternate representation of that state used
;;; by NPSOL (a simple array of double floats)

(defun %nodes->npsol3 (gr:nodes x)

    (declare (optimize (compilation-speed 0)
		       (safety 1)
		       (space 0)
		       (speed 3))
	     (type List nodes)
	     (type (Simple-Array Double-Float (*)) x))
  
    (let ((i -1))
      (declare (type Fixnum i))
      (dolist (node nodes)
	(declare (type Node-Presentation node))
	(let ((pnt (node-pnt node)))
	  (declare (type (Simple-Array Double-Float (*)) pnt))
	  (setf (aref x (incf i)) (aref pnt 0))
	  (setf (aref x (incf i)) (aref pnt 1))
	  (setf (aref x (incf i)) (aref pnt 2)))))
    x)

(defun nodes->npsol3 (gr:nodes x)
    (tools:type-check List nodes)
    (tools:type-check (Simple-Array Double-Float (*)) x)
    (assert (= (* 3 (length nodes)) (length x)))
    (%nodes->npsol3 nodes x))

;;;------------------------------------------------------------
;;; Transforming from the state of Graph-Presentation
;;; to the alternate representation of that state used
;;; by NPSOL (a simple array of double floats)

(defun %npsol->nodes3 (x nodes)

    (declare (optimize (compilation-speed 0)
		       (safety 1)
		       (space 0)
		       (speed 3))
	     (type (Simple-Array Double-Float (*)) x)
	     (type List nodes))

    ;; first set node point and rect
    (let ((i -1))
      (declare (type Fixnum i))
      (dolist (node nodes)
	(declare (type Node-Presentation node))
	(let ((r (slate-rect node))
	      (x (aref x (incf i)))
	      (y (aref x (incf i)))
	      (z (aref x (incf i)))
	      (pnt (node-pnt node))
	      (rad (node-rad node)))
	  (declare (type g:Screen-Rect r)
		   (type Double-Float x y)
		   (type (Simple-Array Double-Float (*)) pnt rad))
	  (setf (aref pnt 0) (+ x (aref rad 0)))
	  (setf (aref pnt 1) (+ y (aref rad 1)))
	  (setf (aref pnt 2) z)
	  (setf (g::%screen-rect-xmin r)
	    (the g:Screen-Coordinate (truncate x)))
	  (setf (g::%screen-rect-ymin r)
	    (the g:Screen-Coordinate (truncate y))))))
    nodes)

(defun npsol->nodes3 (x nodes)
    (tools:type-check Vector x)
    (tools:type-check List nodes)
    (assert (= (* 3 (length nodes)) (length x)))
    (%npsol->nodes3 x nodes))

;;;------------------------------------------------------------

(defun nodes->objgrd3 (gr:nodes grad)
    (declare (optimize (compilation-speed 0)
		       (safety 1)
		       (space 0)
		       (speed 3))
	     (type List nodes gp)
	     (type (Simple-Array Double-Float (*)) grad))
    (let ((i -1))
      (declare (type Fixnum i))
      (dolist (node nodes))
      (declare (type Node-Presentation node))
      (let ((grd (node-grd node)))
	(declare (type (Simple-Array Double-Float (*)) grd))
	(setf (aref grad (incf i)) (aref grd 0))
	(setf (aref grad (incf i)) (aref grd 1))
	(setf (aref grad (incf i)) (aref grd 2))))
    grad))

;;;------------------------------------------------------------
;;; Converting constraints to NPSOL representation
;;;------------------------------------------------------------

(defmethod bounds-constraints ((solver Homotopy-3d-GL-Solver) bl bu)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Homotopy-3d-GL-Solver solver)
	   (type (Simple-Array Double-Float (*)) bl bu))
 
  (let* ((diagram (diagram solver))
	 (bounding-rect (slate:slate-rect (diagram-slate diagram)))
	 (xmin (g:fl (g:screen-rect-xmin bounding-rect)))
	 (xmax (g:fl (g:screen-rect-xmax bounding-rect)))
	 (ymin (g:fl (g:screen-rect-ymin bounding-rect)))
	 (ymax (g:fl (g:screen-rect-ymax bounding-rect)))
	 (i 0))
    (declare (type Graph-Presentation diagram)
	     (type g:Screen-Rect bounding-rect)
	     (type Double-Float xmin xmax ymin ymax)
	     (type tools:Array-Index i))
    (dolist (node (the List (gr:nodes diagram)))
      (declare (type Node-Presentation node))
      (let ((rect (slate-rect node)))
	(declare (type g:Screen-Rect rect))
	;; x bounds
	(setf (aref bl i) xmin)
	(setf (aref bu i) (- xmax (g:fl (g::%screen-rect-width rect))))
	(incf i)
	;; y bounds
	(setf (aref bl i) ymin)
	(setf (aref bu i) (- ymax (g:fl (g::%screen-rect-height rect))))
	(incf i)
	;; z bounds
	(setf (aref bl i) 0.0d0)
	(setf (aref bu i) 500.0d0) 
	(incf i)))))

