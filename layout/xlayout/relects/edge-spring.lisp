;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defun edge-spring-energy (solver edge0 edge1 gradient?)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Graph-Layout-Solver solver)
	   (type Edge-Presentation edge0 edge1)
	   (type (Member t nil)  gradient?))

  (let* ((node00 (gr:edge-node0 edge0))
	 (node01 (gr:edge-node1 edge0))
	 (node10 (gr:edge-node0 edge1))
	 (node11 (gr:edge-node1 edge1))
	 (pnt00 (node-pnt node00))
	 (pnt01 (node-pnt node01))
	 (pnt10 (node-pnt node10))
	 (pnt11 (node-pnt node11))
	 (d-mtx (edge-distance-matrix solver))
	 (i0 (edge-index edge0))
	 (i1 (edge-index edge1))
	 (l (* (the Double-Float (rest-length solver))
	       (aref d-mtx i0 i1)))
	 (dx (/ (- (+ (aref pnt00 0) (aref pnt01 0))
		   (aref pnt10 0) (aref pnt11 0))
		2.0d0))
	 (dy (/ (- (+ (aref pnt00 1) (aref pnt01 1))
		   (aref pnt10 1) (aref pnt11 1))
		2.0d0))
	 (dx2 (* dx dx))
	 (dy2 (* dy dy))
	 (d2 (+ dx2 dy2))
	 (d (the (Double-Float 0.0d0 *) (sqrt d2)))
	 (d-l (- d l))
	 (d-l/l (/ d-l l)))

    (declare (type (Simple-Array Double-Float (*)) pnt00 pnt01 pnt10 pnt11)
	     (type (Double-Float 0.0d0 *) l d dx2 dy2 d2)
	     (type Double-Float dx dy d-l d-l/l))

    (unless (or (not gradient?) (zerop d))
      (let* ((alpha (/ (* #||2.0d0||# d-l/l) d l))
	     (gx (* alpha dx))
	     (gy (* alpha dy))
	     (grd00 (node-grd node00))
	     (grd01 (node-grd node01))
	     (grd10 (node-grd node10))
	     (grd11 (node-grd node11)))
	(declare (type Double-Float alpha gx gy)
		 (type (Simple-Array Double-Float (*)) grd0 grd1))
	(incf (aref grd00 0) gx)
	(incf (aref grd00 1) gy)
	(incf (aref grd01 0) gx)
	(incf (aref grd01 1) gy)
	(decf (aref grd10 0) gx)
	(decf (aref grd10 1) gy)
	(decf (aref grd11 0) gx)
	(decf (aref grd11 1) gy)))

    (* d-l/l d-l/l)))


