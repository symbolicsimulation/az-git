;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defun edge-repulsion (solver edge0 edge1 gradient?)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Graph-Layout-Solver solver)
	   (type Edge-Presentation edge0 edge1)
	   (type (Member t nil)  gradient?))

  (let* ((node00 (gr:edge-node0 edge0))
	 (node01 (gr:edge-node1 edge0))
	 (node10 (gr:edge-node0 edge1))
	 (node11 (gr:edge-node1 edge1))
	 (pnt00 (node-pnt node00))
	 (pnt01 (node-pnt node01))
	 (pnt10 (node-pnt node10))
	 (pnt11 (node-pnt node11))
	 (grd00 (node-grd node00))
	 (grd01 (node-grd node01))
	 (grd10 (node-grd node10))
	 (grd11 (node-grd node11))
	 (d-mtx (edge-distance-mtx solver))
	 (i0 (edge-index edge0))
	 (i1 (edge-index edge1))
	 (l0 (aref d-mtx i0 i1))
	 (rest-length (rest-length solver))
	 (spring-constant (spring-constant solver))
	 (sigma (sigma solver))
	 (l (* rest-length l0))
	 (dx (/ (- (+ (aref pnt00 0) (aref pnt01 0))
		   (aref pnt10 0) (aref pnt11 0))
		2.0d0))
	 (dy (/ (- (+ (aref pnt00 1) (aref pnt01 1))
		   (aref pnt10 1) (aref pnt11 1))
		2.0d0))
	 (dx2 (* dx dx))
	 (dy2 (* dy dy))
	 (d2 (+ dx2 dy2))
	 (d (the (Double-Float 0.0d0 *) (sqrt d2)))
	 (alpha 0.0d0)
	 (gx 0.0d0) (gy 0.0d0)
	 (loss 0.0d0))
    (declare (type (Simple-Array Double-Float (*))
		   pnt0 pnt1 grd0 grd1)
	     (type (Double-Float 0.0d0 *)
		   l0 rest-length spring-constant sigma
		   l d dx2 dy2 d2)
	     (type Double-Float dx dy alpha gx gy))
    (when (<= l0 2.1d0)
      (let* ((d-l (- d l))
	     (d-l/l (/ d-l l)))
	(declare (type Double-Float d-l d-l/l))
	(unless (or (not gradient?) (zerop d))
	  (setf alpha (/ (* spring-constant d-l/l) d l))
	  (setf gx (* alpha dx))
	  (setf gy (* alpha dy))
	  (incf (aref grd00 0) gx)
	  (incf (aref grd00 1) gy)
	  (incf (aref grd01 0) gx)
	  (incf (aref grd01 1) gy)
	  (decf (aref grd10 0) gx)
	  (decf (aref grd10 1) gy)	
	  (decf (aref grd11 0) gx)
	  (decf (aref grd11 1) gy))
	(incf loss (* spring-constant d-l/l d-l/l))))
    (let* ((sigma2 (* sigma sigma))
	   (exp[-d2/sigma2] (* l0 (exp (- (/ d2 sigma2))))))
      (declare (type (Double-Float 0.0d0 *) sigma2 exp[-d2/sigma2]))
      (unless (or (not gradient?) (zerop d))
	(setf alpha (/ (- exp[-d2/sigma2]) sigma2))
	(setf gx (* alpha dx))
	(setf gy (* alpha dy))
	(incf (aref grd00 0) gx)
	(incf (aref grd00 1) gy)
	(incf (aref grd01 0) gx)
	(incf (aref grd01 1) gy)
	(decf (aref grd10 0) gx)
	(decf (aref grd10 1) gy)
	(decf (aref grd11 0) gx)
	(decf (aref grd11 1) gy))
      (+ loss exp[-d2/sigma2]))))