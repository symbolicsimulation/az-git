;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(an:defannouncement :graph-changed ()
  (:documentation
  "This announcement should made by a graph when it wants
displays of itself to be updated."))

;;;============================================================

(defclass Graph-Mediator (Diagram-Mediator)
	  ((graph-diagrams
	    :type List
	    :accessor graph-diagrams
	    :initform ())))

;;;------------------------------------------------------------

(defclass Graph-Mediator-Role (Mediator-Role) ())

(defmethod ac:build-roles ((c Graph-Mediator))
  (setf (ac:actor-default-role c)
    (make-instance 'Graph-Mediator-Role :role-actor c)))
 
;;;------------------------------------------------------------

(defparameter *graph-mediators* ()
  "Mediators for graph editors.")

(defun make-graph-mediator (graph)
  (az:type-check gra:Generic-Graph graph)
  (let ((c (make-instance `Graph-Mediator :mediator-subject graph)))
    ;; let the mediator be notified of changes to the graph
    (an:join-audience graph :graph-changed c
		      #'(lambda (x) (ac:send-msg x #'subject-changed)))
    (ac:start-msg-handling c)
    c))

(defun graph-mediator (graph)
  (let ((m (first (clay:subject-mediators graph))))
    (when (null m) (setf m (make-graph-mediator graph)))
    m))

;;;------------------------------------------------------------

(defun make-graph-diagram (&key
			   (host (xlt:default-host))
			   (display (ac:host-default-display host))
			   (screen (xlib:display-default-screen display))
			   (subject (error "~&Subject must be supplied!"))
			   (mediator (graph-mediator subject))
			   (left 0) (top 0) (width 512) (height 512)
			   (name (gensym))
			   (initial-node-configuration nil)
			   (affine-constraint-f 'make-affine-constraints)
			   (x-order-f nil)
			   (y-order-f nil)
			   (node-pair-distance-f nil)
			   (rest-length nil))
  
  "Make an interactive diagram for browsing and editing the subject graph."

  (declare (type gra:Generic-Graph subject) 
	   (type clay:Graph-Mediator mediator)
	   (type (or String Symbol) name)
	   (type (or Null (Simple-Array g:Screen-Coordinate (*)))
		 initial-node-configuration)
	   (type (or Symbol Function)
		 affine-constraint-f x-order-f y-order-f
		 node-pair-distance-f)
	   (type (or Null Number) rest-length)
	   (:returns (type Graph-Diagram)))
  
  (let ((diagram (make-instance 'Graph-Diagram
		   :diagram-name name
		   :diagram-subject subject
		   :diagram-window (xlt:make-window
				    :screen screen :left left :top top
				    :width width :height height))))
    (declare (type Graph-Diagram diagram))
    (initialize-diagram
     diagram
     :layout-options (list
		      :initial-node-configuration initial-node-configuration
		      :affine-constraint-f affine-constraint-f
		      :x-order-f x-order-f
		      :y-order-f y-order-f
		      :node-pair-distance-f node-pair-distance-f
		      :rest-length rest-length))
    (initialize-painting-dependency diagram mediator)
    diagram))

;;;------------------------------------------------------------
;;; Updating in response to changes in the graph
;;;------------------------------------------------------------

(defmethod subject-changed ((role Graph-Mediator-Role))
  (dolist (gp (graph-diagrams (ac:role-actor role)))
    (ac:send-msg gp #'subject-changed)))








