;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================
;;; Objects to handle input to Graph-Diagrams
;;;============================================================

(defclass Graph-Role (Diagram-Role)
	  ((curr-pos
	    :type g:Screen-Point
	    :accessor curr-pos
	    :initform (g:make-screen-point))
	   (last-pos
	    :type g:Screen-Point
	    :accessor last-pos
	    :initform (g:make-screen-point))))

;;============================================================
;;; Role for node dragging
;;;============================================================

(defclass Node-Dragging-Role (Graph-Role)
	  ((node
	    :type Node-Diagram
	    :accessor node)
	   (origin
	    :type g:Screen-Point
	    :accessor origin
	    :initform (g:make-screen-point))
	   (dp
	    :type g:Screen-Vector
	    :accessor dp
	    :initform (g:make-screen-vector))))

;;;============================================================
;;; Methods for Graph-Role 
;;;============================================================

;;; left button hands control to node dragger
(defmethod button-1-press ((role Graph-Role) x y)
  (declare (type g:Screen-Coordinate x y))
  (ac:atomic
   (let ((selected (find-diagram-under-xy (ac:role-actor role) x y)))
     (when (typep selected 'Node-Diagram)
       (give-control-to-node-dragger selected x y)))))

;;; middle button pins or unpins node
(defmethod button-2-press ((role Graph-Role) x y)
  (declare (type g:Screen-Coordinate x y))
  (ac:atomic
   (let ((selected (find-diagram-under-xy (ac:role-actor role) x y)))
     (when (typep selected 'Node-Diagram) (toggle-pin-node selected)))))

;;; put up a menu for the node
(defmethod button-3-press ((role Graph-Role) x y)
  (declare (type g:Screen-Coordinate x y))
  (ac:atomic
   (let* ((diagram (ac:role-actor role))
	  (selected (find-diagram-under-xy diagram x y)))
     (when selected
       (let ((graph (diagram-subject diagram))
	     (diagram-subject (diagram-subject selected)))
	 (xlt:menu
	  (diagram-window selected)
	  (append (cond ((eq diagram-subject graph)
			 (graph-menu graph))
			((member diagram-subject (gra:nodes graph) :test #'eql)
			 (graph-node-menu graph diagram-subject))
			((member diagram-subject (gra:edges graph) :test #'eq)
			 (graph-edge-menu graph diagram-subject)))
		  (diagram-menu selected))))))))

;;;============================================================
;;; Methods for Node-Dragging-Role 
;;;============================================================

(defmethod ac:interactor-role-cursor ((interactor ac:Interactor)
				      (role Node-Dragging-Role))
  "A Node dragger gets a hand cursor."
  (xlt:window-cursor (ac:interactor-window interactor) :hand2))

;;;============================================================

(defun give-control-to-node-dragger (node x y)

  (declare (optimize (safety 3) (speed 0))
	   (type Node-Diagram node)
	   (type g:Screen-Coordinate x y))

  (let* ((diagram (diagram-parent node))
	 (dragger (node-dragging-role diagram)))
    (declare (type Graph-Diagram diagram)
	     (type Node-Dragging-Role dragger))
    (setf (node dragger) node)
    (unpin-node node) ;; in case it's pinned
    (g:screen-rect-origin (diagram-rect (node dragger))
			  :result (origin dragger))
    (g::set-screen-point-coords (curr-pos dragger) :x x :y y)
    (g:copy-screen-point (curr-pos dragger)
			 :result (last-pos dragger))
    (g:subtract-screen-points (origin dragger) (curr-pos dragger)
			      :result (dp dragger))
    (setf (ac:actor-current-role diagram) dragger)))

;;;============================================================

(defmethod ac:motion-notify ((role Node-Dragging-Role) hint-p x y state
			     time root root-x root-y child same-screen-p)
  "This method moves the selected node."
  (declare (optimize (safety 3) (speed 0))
	   (type Node-Dragging-Role role)
	   (type xlib:Boolean hint-p)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))
  hint-p state time root root-x root-y child same-screen-p
  (let* ((diagram (ac:role-actor role))
	 (queue (ac:actor-msg-queue diagram))
	 (node (node role))
	 (curr-pos (curr-pos role))
	 (last-pos (last-pos role))
	 (origin (origin role))
	 (dp (dp role)))

    (declare (type Graph-Diagram diagram)
	     (type ac:Queue queue)
	     (type Node-Diagram node)
	     (type g:Screen-Point curr-pos last-pos origin)
	     (type g:Screen-Vector dp))
    
    ;; jump to the last motion notify msg on the queue
    (loop (let ((next-msg (ac:dequeue queue)))
	    (unless (or (eq (first next-msg) 'ac:motion-notify)
			(eq (first next-msg) #'ac:motion-notify))
	      (unless (null next-msg) (ac:Queue-push next-msg queue))
	      (return))
	    (setf x (third next-msg))
	    (setf y (fourth next-msg))))
    
    ;; follow mouse with current node, if any
    (unless (null node)
      (g:copy-screen-point curr-pos :result last-pos)
      (setf (g:screen-point-x curr-pos) x)
      (setf (g:screen-point-y curr-pos) y)
      (unless (g:equal-screen-points? curr-pos last-pos)
	(g:move-screen-point curr-pos dp :result origin)
	(move-diagram-to node origin))))
  (values t))

;;;--------------------------------------------------------------

(defun find-diagram-under-xy (diagram x y)
  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram diagram)
	   (type g:Screen-Coordinate x y))
  (dolist (node (gra:nodes diagram))
    (when (g::%screen-point-in-rect-xy? x y (diagram-rect node))
      (return-from find-diagram-under-xy node)))
  (dolist (edge (gra:edges diagram))
    (when (g::%screen-point-in-rect-xy? x y (diagram-rect edge))
      (return-from find-diagram-under-xy edge)))
  ;; if we're not over a node or an edge, return the graph itself
  diagram)

(defun find-diagram-under (diagram point)
  (find-diagram-under-xy
   diagram (g:screen-point-x point) (g:screen-point-y point)))



