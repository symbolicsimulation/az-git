;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Brick-Graph-Layout-Solver (Graph-Layout-Solver) ())

;;;------------------------------------------------------------

(defmethod solve-layout ((solver Brick-Graph-Layout-Solver))

  (let ((old-domain (brk:domain solver))
	(value most-positive-double-float))
    (declare (type brk:Domain old-domain))
    (unwind-protect
	(progn
	  (setf (brk:domain solver)
	    (brk:make-position-domain
	     (remove-if #'node-user-pinned? (brk:bricks old-domain))))
	  (setf (inactive-nodes solver)
	    (set-difference (gra:nodes (diagram solver))
			    (brk:bricks (brk:domain solver))))
	  (when (> (brk:dimension (brk:domain solver)) 0)
	    (let* ((diagram (diagram solver))
		   (domain (brk:domain solver))
		   (constraints (reduce-affine-constraints
				 (constraints solver) domain))
		   (cost (brk:default-cost domain)))
	      (declare (type Graph-Diagram diagram))
	      (xlt:clear-drawable (diagram-window diagram))
	      (setf value (brk:solve-constraints cost constraints domain))
	      ;; draw the final state, even if we not animating.
	      (attach-edges solver diagram)
	      (xlt:clear-drawable (diagram-window diagram))
	      (draw-diagram diagram)
	      (xlt:drawable-force-output (diagram-window diagram)))))
      ;; cleanup
      (setf (brk:domain solver) old-domain))
    (values value)))

