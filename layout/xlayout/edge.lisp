;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Edge-Diagram (Line-Diagram)
	  ((gra:edge-node0
	    :type Node-Diagram
	    :accessor gra:edge-node0)
	   (gra:edge-node1
	    :type Node-Diagram
	    :accessor gra:edge-node1)
	   (edge-index
	    :type az:Array-Index
	    :accessor edge-index)))

;;;============================================================

(defmethod diagram-name ((edge Edge-Diagram))
  (format nil "EdgeP ~a -- ~a"
	  (gra:edge-node0 edge)
	  (gra:edge-node1 edge)))

(defmethod print-object ((edge Edge-Diagram) stream)
  (declare (type Stream stream))
  (az:printing-random-thing
   (edge stream)
   (format stream "EdgeP ~a -- ~a"
	   (gra:edge-node0 edge)
	   (gra:edge-node1 edge))))

;;;------------------------------------------------------------

(defmethod layout-solver ((diagram Edge-Diagram))
  (layout-solver (diagram-parent diagram)))

;;;------------------------------------------------------------

(defmethod equal-objects? ((e0 Edge-Diagram)
			   (e1 Edge-Diagram))
  (and (eq (gra:edge-node0 e0) (gra:edge-node0 e1))
       (eq (gra:edge-node1 e0) (gra:edge-node1 e1))))

;;;============================================================
;;; Building
;;;============================================================

;;;============================================================
;;; Layout
;;;============================================================

(defmethod edge-l2-norm2 ((gep Edge-Diagram))
  (let* ((screen-rect (diagram-rect gep))
	 (w (g:screen-rect-width screen-rect))
	 (h (g:screen-rect-height screen-rect)))
    (+ (* w w) (* h h))))

(defmethod edge-l2-norm ((gep Edge-Diagram))
  (let* ((screen-rect (diagram-rect gep))
	 (w (g:screen-rect-width screen-rect))
	 (h (g:screen-rect-height screen-rect)))
    (sqrt (+ (* w w) (* h h)))))

;;;============================================================
;;; For Directed Graphs:
;;;============================================================

(defclass Dedge-Diagram (Edge-Diagram Arrow-Diagram) ())

;;;------------------------------------------------------------

(defmethod print-object ((edge Dedge-Diagram) stream)
  (declare (type Stream stream))
  (az:printing-random-thing
   (edge stream)
   (format stream "DedgeP ~a -> ~a"
	   (gra:edge-node0 edge)
	   (gra:edge-node1 edge))))