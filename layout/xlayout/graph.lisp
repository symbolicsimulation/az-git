;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================

(defclass Graph-Diagram (Root-Diagram)
	  ((gra:nodes
	    :type List
	    :accessor gra:nodes
	    :initform ())
	   (gra:edges
	    :type List
	    :accessor gra:edges
	    :initform ())
	   (initialize-node-positions?
	    :type (Member T Nil)
	    :accessor initialize-node-positions?
	    :initform t)
	   (initial-node-configuration
	    :type (or Null (Simple-Array g:Screen-Coordinate (*)))
	    :accessor initial-node-configuration)
	   ;; who controls the layout of this diagram?
	   (layout-solver
	    :type (or Null Layout-Solver) ;; (or Null Graph-Layout-Solver)
	    :accessor layout-solver)

	   ;; who handles the input to this diagram in node dragging mode?
	   (node-dragging-role
	    :type Node-Dragging-Role
	    :accessor node-dragging-role)))
 
;;;============================================================

(defmethod initialize-instance :after ((diagram Graph-Diagram) &rest initargs)
  (declare (ignore initargs))
  (setf (slot-value diagram 'layout-solver) (build-layout-solver diagram))
  (build-children diagram))

;;;============================================================
;;; An example of a class that uses this diagram

(defmethod default-diagram-class ((graph gra:Graph))
  (find-class 'Graph-Diagram))

;;;============================================================
;;; Traversing the diagram tree
;;;============================================================

(defmethod map-over-children (result-type function (diagram Graph-Diagram))
  (cond
   ((null result-type)
    (dolist (node (gra:nodes diagram)) (funcall function node))
    (dolist (edge (gra:edges diagram)) (funcall function edge)))
   (t
    (concatenate result-type
      (map result-type function (gra:nodes diagram))
      (map result-type function (gra:edges diagram))))))

(defmethod some-child? (function (diagram Graph-Diagram))
  (or (some function (gra:nodes diagram))
      (some function (gra:edges diagram))))

(defmethod find-child-if (function (diagram Graph-Diagram))
  (let ((found (find-if function (gra:nodes diagram))))
    (when (null found)
      (setf found (find-if function (gra:edges diagram))))
    found))

;;;============================================================
;;; Building
;;;============================================================

(defgeneric edge-class (diagram)
  (declare (type Graph-Diagram diagram)))

(defmethod edge-class ((diagram Graph-Diagram))
  (if (gra:directed-graph? (diagram-subject diagram))
      (find-class 'Dedge-Diagram)
    ;; else
    (find-class 'Edge-Diagram)))

;;;------------------------------------------------------------

(defun make-edge-diagram (subject parent)
  (declare (type T subject)
	   (type Graph-Diagram parent))
  (let* ((gep (make-instance (edge-class parent)
		:diagram-subject subject
		:diagram-parent parent))
	 (gnp0 (find (gra:edge-node0 subject)
		     (gra:nodes parent) :key #'diagram-subject))
	 (gnp1 (find (gra:edge-node1 subject)
		     (gra:nodes parent) :key #'diagram-subject)))
    (pushnew gep (from-edges gnp0))
    (pushnew gep (to-edges gnp1))
    (setf (gra:edge-node0 gep) gnp0)
    (setf (gra:edge-node1 gep) gnp1)
    gep))

;;;------------------------------------------------------------

(defmethod build-children ((diagram Graph-Diagram))
  (let* ((diagram-subject (diagram-subject diagram))
	 (snodes (gra:nodes diagram-subject))
	 (sedges (gra:edges diagram-subject)))
    ;; make diagrams for the nodes in the graph
    (setf (gra:nodes diagram)
      (az:with-collection
       (dolist (snode snodes)
	 (az:collect (make-instance 'Node-Diagram
				     :diagram-subject snode
				     :diagram-parent diagram)))))
    (graph-diagram-index-nodes diagram)

    ;; make diagrams for the edges in the graph
    ;; and recreate the graph structure
    ;; in the diagram nodes and edges
    (setf (gra:edges diagram)
      (az:with-collection
       (dolist (sedge sedges)
	 (az:collect (make-edge-diagram sedge diagram)))))
    (graph-diagram-index-edges diagram)))

;;;------------------------------------------------------------

(defmethod graph-diagram-index-nodes ((diagram Graph-Diagram))
  (let ((i -1))
    (dolist (node (gra:nodes diagram))
      (setf (node-index node) (incf i)))))

;;;------------------------------------------------------------

(defmethod graph-diagram-index-edges ((diagram Graph-Diagram))
  (let ((i -1))
    (dolist (edge (gra:edges diagram))
      (setf (edge-index edge) (incf i)))))

;;;------------------------------------------------------------

(defun initialize-node-float-coordinates (diagram)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram diagram))

  (dolist (node (the List (gra:nodes diagram)))
    (let ((r (diagram-rect node))
	  (pnt (node-pnt node))
	  (rad (node-rad node))
	  (1/rad (node-1/rad node)))
      (declare (type g:Screen-Rect r)
	       (type (Simple-Array Double-Float (*)) pnt rad 1/rad))
      (setf (aref pnt 0) (az:fl (g::%screen-rect-xmin r)))
      (setf (aref pnt 1) (az:fl (g::%screen-rect-ymin r)))
      (setf (aref rad 0) (* 0.5d0 (g::%screen-rect-width r)))
      (setf (aref rad 1) (* 0.5d0 (g::%screen-rect-height r)))
      (setf (aref 1/rad 0) (/ 1.0d0 (g::%screen-rect-width r)))
      (setf (aref 1/rad 1) (/ 1.0d0 (g::%screen-rect-height r))))))

;;;------------------------------------------------------------

(defmethod ac:build-roles ((diagram Graph-Diagram))
  (setf (clay:diagram-unmapped-role diagram)
    (make-instance 'Unmapped-Role :role-actor diagram))
  (setf (node-dragging-role diagram)
    (make-instance 'Node-Dragging-Role :role-actor diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Graph-Role :role-actor diagram)))

;;;============================================================
;;; Layout
;;;============================================================

(defgeneric initialize-layout-solver (solver layout-options)
  (:method
   ((solver Layout-Solver) layout-options)
   (declare (ignore layout-options))
   t))

;;;------------------------------------------------------------

(defmethod layout-diagram ((diagram Graph-Diagram) layout-options)
  (let ((rect (diagram-rect diagram))
	(solver (layout-solver diagram))
	(initial-node-configuration
	 (getf layout-options :initial-node-configuration)))
    (declare (type g:Screen-Rect rect))
    (setf (g:screen-rect-xmin rect) 0)
    (setf (g:screen-rect-ymin rect) 0)
    (setf (g:screen-rect-width rect)
      (xlib:drawable-width (diagram-window diagram)))
    (setf (g:screen-rect-height rect)
      (xlib:drawable-height (diagram-window diagram)))
    (unless (null initial-node-configuration)
      (assert (= (length initial-node-configuration)
		 (* 2 (length (gra:nodes diagram))))))
    (setf (initial-node-configuration diagram) initial-node-configuration)
    (layout-children diagram)
    (when (> (length (gra:nodes diagram)) 0)
      (initialize-layout-solver solver layout-options)
      (solve-layout solver)))
  diagram)

;;;------------------------------------------------------------

(defmethod layout-children ((diagram Graph-Diagram))

  (dolist (node (gra:nodes diagram)) (layout-diagram node nil))

  (when (initialize-node-positions? diagram)
    (setf (initialize-node-positions? diagram) nil)
    (if (null (initial-node-configuration diagram))
	;; put the node is random places
	(dolist (node (gra:nodes diagram)) (node-randomize-position node))
      ;; else use the supplied starting position
      (position-vector->nodes (initial-node-configuration diagram)
			      (gra:nodes diagram))))
  
  (dolist (edge (gra:edges diagram)) (layout-diagram edge nil)))

;;;============================================================
;;; Drawing and Erasing
;;;============================================================

(defmethod draw-diagram ((diagram Graph-Diagram))
  (dolist (d (gra:edges diagram))
    (declare (type Node-Diagram d))
    (draw-diagram d))
  (dolist (d (gra:nodes diagram))
    (declare (type Edge-Diagram d))
    (draw-diagram d)))

;;;------------------------------------------------------------

(defmethod erase-diagram ((diagram Graph-Diagram) rect)
  (declare (type (or Null g:Screen-Rect) rect)
	   (:returns (or Null g:Screen-Rect)))
  (dolist (d (gra:nodes diagram))
    (declare (type Node-Diagram d))
    (erase-diagram d nil))
  (dolist (d (gra:edges diagram))
    (declare (type Edge-Diagram d))
    (erase-diagram d nil))
  (unless (null rect)
    (g::%copy-screen-rect (diagram-rect diagram) rect)))

;;;------------------------------------------------------------

(defmethod repair-damage ((diagram Graph-Diagram) damaged-rect)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram diagram)
	   (type g:Screen-Rect damaged-rect))

  (when (intersect-diagram? diagram damaged-rect)
    (dolist (edge (the List (gra:edges diagram)))
      (declare (type Edge-Diagram edge))
      (repair-damage edge damaged-rect))
    (dolist (node (the List (gra:nodes diagram)))
      (declare (type Node-Diagram node))
      (repair-damage node damaged-rect))))

;;;============================================================
;;; Handling :configure-notify msgs
;;;============================================================

(defmethod clay:reconfigure-diagram ((diagram Graph-Diagram) width height)
  (declare (ignore width height))
  (erase-diagram diagram nil)
  ;;(layout-diagram diagram nil)
  (draw-diagram diagram)
  (xlt:drawable-force-output (diagram-window diagram)))

;;;============================================================

(defun move-node (diagram gnp pos)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Diagram diagram)
	   (type Node-Diagram gnp)
	   (type g:Screen-Point pos))

  (g:with-borrowed-screen-rect
   (damaged-rect)
   (locally (declare (type g:Screen-Rect damaged-rect))
     (let* ((gnp-rect (diagram-rect gnp))
	    (from-edges (from-edges gnp))
	    (to-edges (to-edges gnp)))
       (declare (type g:Screen-Rect gnp-rect)
		(type List from-edges to-edges))

       ;; erase node, set new node coordinates, repair damage, and draw
       (erase-diagram gnp damaged-rect)
       (setf (g::%screen-rect-xmin gnp-rect) (g::%screen-point-x pos))
       (setf (g::%screen-rect-ymin gnp-rect) (g::%screen-point-y pos))
       (repair-damage diagram damaged-rect)
       (draw-diagram gnp)

       ;; erase edges, repair damage, re-attach, and draw
       (let ((solver (layout-solver diagram)))
	 (dolist (edge from-edges)
	   (declare (type Edge-Diagram edge))
	   (erase-diagram edge damaged-rect)
	   (attach-edge solver edge)
	   (repair-damage diagram damaged-rect)
	   (draw-diagram edge))
	 (dolist (edge to-edges)
	   (declare (type Edge-Diagram edge))
	   (erase-diagram edge damaged-rect)
	   (attach-edge solver edge)
	   (repair-damage diagram damaged-rect)
	   (draw-diagram edge))))))
  (xlt:drawable-force-output (diagram-window diagram)))

;;;------------------------------------------------------------

(defmethod move-diagram-to ((diagram Node-Diagram) pos)
  (check-type pos g:Screen-point)
  ;; because this requires maintaining constraints amoung
  ;; the siblings of <diagram>, it must be handled by the parent
  (move-node (diagram-parent diagram) diagram pos))


;;;------------------------------------------------------------
;;; internal method used to find the diagram of a given diagram-subject

(defmethod %find-diagram-of (subj (diagram Graph-Diagram))
  (if (eql subj (diagram-subject diagram))
      (throw :search-done diagram)
      ;; else
      (dolist (node (gra:nodes diagram) nil)
	(when (eql subj (diagram-subject node))
	  (throw :search-done node)))))

(defun select-node-with-mouse (diagram)
  (check-type diagram Graph-Diagram)
  (first (select-nodes-with-mouse diagram)))

(defun drag-node-with-mouse (diagram)
  ;;(check-type diagram Graph-Diagram)
  (let ((node (select-node-with-mouse diagram)))
    (unless (null node) (drag-diagram node))))

;;;------------------------------------------------------------
;;; Updating in response to changes in the graph
;;;------------------------------------------------------------

(defmethod gra:add-nodes-and-edges (nodes edges (diagram Graph-Diagram))
  (declare (type List nodes edges))

  (let ((new-nodes (set-difference nodes (gra:nodes diagram)))
	(new-edges (set-difference edges (gra:edges diagram))))
     (setf (gra:nodes diagram)
       (nunion (gra::collect-nodes-from-edges new-edges)
	       (concatenate 'List new-nodes (gra:nodes diagram))))
    (setf (gra:edges diagram) (nconc new-edges (gra:edges diagram)))

    (dolist (edge new-edges)
      ;; make sure nodes know about edges
      (pushnew edge (from-edges (gra:edge-node0 edge)))
      (pushnew edge (to-edges (gra:edge-node1 edge))))

    (graph-diagram-index-nodes diagram)
    (graph-diagram-index-edges diagram))
  diagram)

(defmethod gra:delete-nodes-and-edges (nodes edges (diagram Graph-Diagram))
  (declare (type List nodes edges))

  (flet ((delete-edge? (edge)
	   (let ((delete? nil)
		 (node0 (gra:edge-node0 edge))
		 (node1 (gra:edge-node1 edge)))
	     (dolist (node nodes)
	       ;; fix the nodes here (a little sneaky)
	       (when (or (eql node node0) (eql node node1))
		 (setf (from-edges node0) (delete edge (from-edges node0)))
		 (setf (to-edges node1) (delete edge (to-edges node1)))
		 (setf delete? t)))
	     delete?)))
    (setf (gra:edges diagram)
      (nset-difference (delete-if #'delete-edge? (gra:edges diagram))
		       edges)))
  ;; fix the nodes in the other edges
  (dolist (edge edges)
    (let ((node0 (gra:edge-node0 edge))
	  (node1 (gra:edge-node1 edge)))
      (setf (from-edges node0) (delete edge (from-edges node0)))
      (setf (to-edges node1) (delete edge (to-edges node1)))))

  (setf (gra:nodes diagram) (nset-difference (gra:nodes diagram) nodes))

  (graph-diagram-index-nodes diagram)
  (graph-diagram-index-edges diagram)
  diagram)

;;;------------------------------------------------------------

(defmethod subject-changed ((diagram Graph-Diagram))
  (let* ((graph (diagram-subject diagram))
	 (nodes (gra:nodes graph))
	 (edges (gra:edges graph))
	 (presented-nodes (mapcar #'diagram-subject (gra:nodes diagram)))
	 (presented-edges (mapcar #'diagram-subject (gra:edges diagram)))
	 (added-nodes (set-difference nodes presented-nodes))
	 (added-edges (set-difference edges presented-edges))
	 (deleted-nodes (set-difference presented-nodes nodes))
	 (deleted-edges (set-difference presented-edges edges))
	 (added-gnps
	  (mapcar #'(lambda (node) (make-instance 'Node-Diagram
				     :diagram-subject node
				     :diagram-parent diagram))
		  added-nodes))
	 (added-geps ())
	 (deleted-gnps
	  (remove-if #'(lambda (gnp)
			 (not (member (diagram-subject gnp) deleted-nodes)))
		     (gra:nodes diagram)))
	 (deleted-geps
	  (remove-if #'(lambda (gep)
			 (not (member (diagram-subject gep) deleted-edges)))
		     (gra:edges diagram)))
	 
	 (old-domain (brk:domain (layout-solver diagram))))
    (declare (type List
		   nodes edges presented-nodes presented-edges
		   added-nodes added-edges deleted-nodes deleted-edges
		   added-gnps added-geps deleted-gnps deleted-geps)
	     (type brk:Domain old-domain))

    ;; first delete
    (erase-diagram diagram nil)
    (gra:delete-nodes-and-edges deleted-gnps deleted-geps diagram)
    (draw-diagram diagram)

    ;; Must add node diagrams before making the edge diagrams,
    ;; because <make-edge-diagram> expects a node-diagram
    ;; in the graph-diagram for both of its diagram-subject nodes.
    (gra:add-nodes-and-edges added-gnps () diagram)
    ;; now make and add the edge diagrams
    (setf added-geps
      (mapcar #'(lambda (edge) (make-edge-diagram edge diagram))
	      added-edges))
    (gra:add-nodes-and-edges () added-geps diagram)

    ;; simple layout and redraw
    (unless (and (null added-gnps) (null added-geps))
      (mapc #'node-position-from-neighbors added-gnps)
      (setf (brk:domain (layout-solver diagram))
	(brk:make-position-domain added-gnps))
      (layout-diagram diagram nil))
    (setf (brk:domain (layout-solver diagram))
      (brk:make-position-domain
       (set-difference
	(concatenate 'List added-gnps (brk:bricks old-domain))
	deleted-gnps)))
    (draw-diagram diagram)
    (xlt:drawable-force-output (diagram-window diagram))  
    diagram))



