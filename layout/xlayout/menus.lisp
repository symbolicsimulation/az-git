;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================
;;; Menus for graph browsers
;;;============================================================

(defgeneric graph-node-menu (graph node)
  (declare (type T graph node)
	   (:returns (type List menu-spec)))
  (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <node> viewed as
a node in <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod graph-node-menu append ((graph gra:Graph) (node T))
  "Contributes a menu item for deleting nodes."
  (declare (:returns (type List menu-spec)))
  (list (list (format nil "Delete ~a from graph" node) 
	      #'gra:delete-nodes-and-edges (list node) () graph)))

;;;============================================================

(defgeneric graph-edge-menu (graph edge)
  (declare (type T graph edge)
	   (:returns (type List menu-spec)))
  (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <edge> viewed as
an edge in <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod graph-edge-menu append ((graph gra:Graph) (edge T))
  "Contributes a menu item for deleting edges."
  (declare (:returns (type List menu-spec)))
  (list (list (format nil "Delete ~a from graph" edge) 
	      #'gra:delete-nodes-and-edges () (list edge) graph)))

;;;============================================================

(defgeneric graph-menu (graph)
  (declare (type T graph))
  (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <graph>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))


(defmethod graph-menu append ((graph gra:Graph))
  "Contributes no menu items."
  (declare (:returns ()))
  ())

;;;============================================================

(defmethod diagram-menu append ((graph Graph-Diagram))
  "Contributes a menu item for resolving the layout."
  (declare (:returns (type List menu-spec)))
  (let ((solver (layout-solver graph)))
    (list (list "Solve Layout"
		#'(lambda (diagram)
		    (erase-diagram diagram nil)
		    (layout-diagram diagram nil)
		    (draw-diagram diagram)
		    (xlt:drawable-force-output (diagram-window diagram)))
		graph)
	  (list (format nil "Describe ~a" solver)
		#'describe solver)
	  (list (format nil "Inspect ~a" solver)
		#'winspect solver))))

(defmethod diagram-menu append ((node Node-Diagram))
  "Contributes a menu item for Pinning and Unpinning nodes."
  (declare (:returns (type List menu-spec)))
  (list (if (node-bounds node)
	    (list (format nil "Unpin ~a" node) #'unpin-node node)
	  (list (format nil "Pin ~a" node) #'pin-node node))))

(defmethod diagram-menu append ((edge Edge-Diagram))
  "Contributes no menu items."
  (declare (:returns ()))
  ())