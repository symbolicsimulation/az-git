;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================
;;; Computing edge attachments
;;;------------------------------------------------------------
;;; different solvers may use different strategies for how
;;; to connect nodes by edges

(defun attach-edges (solver gp &rest options)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type Graph-Diagram gp)
	   (ignore options))

  (dolist (edge (the List (gra:edges gp)))
    (declare (type Edge-Diagram edge))
    (attach-edge solver edge)))
	    
;;;------------------------------------------------------------

(defun attach-edge (solver edge)

  (declare (optimize (safety 3) (speed 0))
	   (type Graph-Layout-Solver solver)
	   (type Edge-Diagram edge))

  (funcall (edge-attacher-f solver)
	   (diagram-rect (gra:edge-node0 edge))
	   (diagram-rect (gra:edge-node1 edge))
	   (from-point edge)
	   (to-point edge)
	   (diagram-rect edge)))

;;;------------------------------------------------------------

(defmethod layout-diagram ((gep Edge-Diagram) layout-options)
  (declare (ignore layout-options))
  (attach-edge (layout-solver (diagram-parent gep)) gep)
  gep)

;;;------------------------------------------------------------
;;; destructively modifies <edge-pos0>, <edge-pos1>,
;;; and  <edge-screen-rect>.

(defun attach-shortest-edge (node-rect0 node-rect1
			     edge-pos0 edge-pos1 edge-rect)

  (declare (optimize (safety 3) (speed 0))
	   (type g:Screen-Rect node-rect0 node-rect1 edge-rect)
	   (type g:Screen-Point edge-pos0 edge-pos1))

  (let* ((xmin0 (- (g::%screen-rect-xmin node-rect0) 1))
	 (xmax0 (+ xmin0 (g::%screen-rect-width node-rect0)))
	 (ymin0 (- (g::%screen-rect-ymin node-rect0) 1))
	 (ymax0 (+ ymin0 (g::%screen-rect-height node-rect0)))
	 (xmin1 (- (g::%screen-rect-xmin node-rect1) 1))
	 (xmax1 (+ xmin1 (g::%screen-rect-width node-rect1)))
	 (ymin1 (- (g::%screen-rect-ymin node-rect1) 1))
	 (ymax1 (+ ymin1 (g::%screen-rect-height node-rect1)))	 
	 (midpoint 0))
    (declare (type g:Screen-Coordinate
		   xmin0 xmax0 ymin0 ymax0 xmin1 xmax1 ymin1 ymax1 midpoint))
    (cond ((< xmax0 xmin1) ;; 0 interval is left of 1 interval
	   (setf (g::%screen-point-x edge-pos0) xmax0)
	   (setf (g::%screen-point-x edge-pos1) xmin1)
	   (setf (g::%screen-rect-xmin edge-rect) xmax0)
	   (setf (g::%screen-rect-width edge-rect) (- xmin1 xmax0)))
	  ((< xmax1 xmin0) ;; 0 interval is right of 1 interval
	   (setf (g::%screen-point-x edge-pos0) xmin0)
	   (setf (g::%screen-point-x edge-pos1) xmax1)
	   (setf (g::%screen-rect-xmin edge-rect) xmax1)
	   (setf (g::%screen-rect-width edge-rect) (- xmin0 xmax1)))
	  (t ;; intervals intersect
	   (setf midpoint (ash (+ (max xmin0 xmin1) (min xmax0 xmax1)) -1))
	   (setf (g::%screen-point-x edge-pos0) midpoint)
	   (setf (g::%screen-point-x edge-pos1) midpoint)
	   (setf (g::%screen-rect-xmin edge-rect) midpoint)
	   (setf (g::%screen-rect-width edge-rect) 1)))
    (cond ((< ymax0 ymin1) ;; 0 interval is left of 1 interval
	   (setf (g::%screen-point-y edge-pos0) ymax0)
	   (setf (g::%screen-point-y edge-pos1) ymin1)
	   (setf (g::%screen-rect-ymin edge-rect) ymax0)
	   (setf (g::%screen-rect-height edge-rect) (- ymin1 ymax0)))
	  ((< ymax1 ymin0) ;; 0 interval is right of 1 interval
	   (setf (g::%screen-point-y edge-pos0) ymin0)
	   (setf (g::%screen-point-y edge-pos1) ymax1)
	   (setf (g::%screen-rect-ymin edge-rect) ymax1)
	   (setf (g::%screen-rect-height edge-rect) (- ymin0 ymax1)))
	  (t ;; intervals intersect
	   (setf midpoint (ash (+ (max ymin0 ymin1) (min ymax0 ymax1)) -1))
	   (setf (g::%screen-point-y edge-pos0) midpoint)
	   (setf (g::%screen-point-y edge-pos1) midpoint)
	   (setf (g::%screen-rect-ymin edge-rect) midpoint)
	   (setf (g::%screen-rect-height edge-rect) 1)))))

;;;------------------------------------------------------------
;;; destructively modifies <edge-pos0>, <edge-pos1>,
;;; and  <edge-screen-rect>.

(defun attach-centers (node-rect0 node-rect1
		       edge-pos0 edge-pos1 edge-rect)

  (declare (optimize (safety 3) (speed 0))
	   (type g:Screen-Rect node-rect0 node-rect1 edge-rect)
	   (type g:Screen-Point edge-pos0 edge-pos1))

  (let* ((x0 (ash (+ (g::%screen-rect-xmin node-rect0)
		     (g::%screen-rect-xmax node-rect0))
		  -1))
	 (y0 (ash (+ (g::%screen-rect-ymin node-rect0)
		     (g::%screen-rect-ymax node-rect0))
		  -1))
	 (x1 (ash (+ (g::%screen-rect-xmin node-rect1)
		     (g::%screen-rect-xmax node-rect1))
		  -1))
	 (y1 (ash (+ (g::%screen-rect-ymin node-rect1)
		     (g::%screen-rect-ymax node-rect1))
		  -1)))

    (declare (type g:Screen-Coordinate x0 y0 x1 y1))

    (setf (g::%screen-point-x edge-pos0) x0)
    (setf (g::%screen-point-y edge-pos0) y0)
    (setf (g::%screen-point-x edge-pos1) x1)
    (setf (g::%screen-point-y edge-pos1) y1)

    (cond ((< x0 x1)
	   (setf (g::%screen-rect-xmin edge-rect) (+ 1 x0))
	   (setf (g::%screen-rect-width edge-rect) (- x1 x0 1)))
	  (t	   
	   (setf (g::%screen-rect-xmin edge-rect) (+ 1 x1))
	   (setf (g::%screen-rect-width edge-rect) (- x0 x1 1))))

    (cond ((< y0 y1)
	   (setf (g::%screen-rect-ymin edge-rect) (+ y0 1))
	   (setf (g::%screen-rect-height edge-rect) (- y1 y0 1)))
	  (t	   
	   (setf (g::%screen-rect-ymin edge-rect) (+ 1 y1))
	   (setf (g::%screen-rect-height edge-rect) (- y0 y1 1))))))

;;;------------------------------------------------------------
;;; destructively modifies <edge-pos0>, <edge-pos1>,
;;; and  <edge-screen-rect>.

(defun attach-centers-clipped (node-rect0 node-rect1
			       edge-pos0 edge-pos1
			       edge-rect)

  (declare (optimize (safety 3) (speed 0))
	   (type g:Screen-Rect node-rect0 node-rect1 edge-rect)
	   (type g:Screen-Point edge-pos0 edge-pos1))

  (let* ((cx0 (ash (+ (g::%screen-rect-xmin node-rect0)
		      (g::%screen-rect-xmax node-rect0))
		   -1))
	 (cy0 (ash (+ (g::%screen-rect-ymin node-rect0)
		      (g::%screen-rect-ymax node-rect0))
		   -1))
	 (cx1 (ash (+ (g::%screen-rect-xmin node-rect1)
		      (g::%screen-rect-xmax node-rect1))
		   -1))
	 (cy1 (ash (+ (g::%screen-rect-ymin node-rect1)
		      (g::%screen-rect-ymax node-rect1))
		   -1)))

    ;; clip edge to outside of node rects
    (multiple-value-bind (x0 y0)
	(g:clip-screen-line cx1 cy1 cx0 cy0 node-rect0)
      (declare (type g:Screen-Coordinate x0 y0))
      (multiple-value-bind (x1 y1)
	  (g:clip-screen-line cx0 cy0 cx1 cy1 node-rect1)
	(declare (type g:Screen-Coordinate x1 y1))
	(let ((dx (- x1 x0))
	      (dy (- y1 y0)))
	  (declare (type g:Screen-Coordinate dx dy))
	  (cond ((> dx  1) (incf x0 2) (decf x1 2))
		((> dx  0) (incf x0 1) (decf x1 1))
		((< dx -1) (decf x0 2) (incf x1 2))
		((< dx  0) (decf x0 1) (incf x1 1)))
	  (cond ((> dy  1) (incf y0 2) (decf y1 2))
		((> dy  0) (incf y0 1) (decf y1 1))
		((< dy -1) (decf y0 2) (incf y1 2))
		((< dy  0) (decf y0 1) (incf y1 1))))
	
	(setf (g::%screen-point-x edge-pos0) x0)
	(setf (g::%screen-point-y edge-pos0) y0)
	(setf (g::%screen-point-x edge-pos1) x1)
	(setf (g::%screen-point-y edge-pos1) y1)

	(cond ((< x0 x1)
	       (setf (g::%screen-rect-xmin edge-rect) (+ 1 x0))
	       (setf (g::%screen-rect-width edge-rect) (- x1 x0 1)))
	      ((= x0 x1)
	       (setf (g::%screen-rect-xmin edge-rect) (+ 1 x0))
	       (setf (g::%screen-rect-width edge-rect) 0))
	      (t	   
	       (setf (g::%screen-rect-xmin edge-rect) (+ 1 x1))
	       (setf (g::%screen-rect-width edge-rect) (- x0 x1 1))))

	(cond ((< y0 y1)
	       (setf (g::%screen-rect-ymin edge-rect) (+ y0 1))
	       (setf (g::%screen-rect-height edge-rect) (- y1 y0 1)))
	      ((= y0 y1)
	       (setf (g::%screen-rect-ymin edge-rect) (+ y0 1))
	       (setf (g::%screen-rect-height edge-rect) 0))
	      (t	   
	       (setf (g::%screen-rect-ymin edge-rect) (+ 1 y1))
	       (setf (g::%screen-rect-height edge-rect) (- y0 y1 1))))))))



