;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================
			     
(defun random-diagram-points (n window)
  (let* ((xmin (xlib:drawable-x window))
	 (xmax (+ xmin (xlib:drawable-width window)))
	 (ymin (xlib:drawable-y window))
	 (ymax (+ ymin (xlib:drawable-height window)))
	 (screen-points ()))
    (dotimes (i n)
      (push (g:random-point-in-rect :xmin xmin :xmax xmax :ymin ymin :ymax ymax)
	    screen-points))
    screen-points))




