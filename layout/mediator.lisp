;;;-*- Package: :Layout; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout) 

;;;============================================================

(defclass Graph-Mediator (Clay:Diagram-Mediator) ())

;;;------------------------------------------------------------
 
(defun initialize-graph-dependency (diagram mediator)
  (declare (type Graph-Diagram diagram)
	   (type Graph-Mediator mediator))
  (an:join-audience mediator :changed diagram 'graph-changed))

(defmethod activate-children ((diagram Graph-Diagram) mediator)
  "Only need to initialize the nodes, to get their message handling started."
  (declare (ignore mediator))
  (map nil #'(lambda (child) (clay:activate-diagram child nil))
       (gra:nodes diagram)))

(defmethod clay:activate-diagram ((diagram Graph-Diagram)
				  (mediator Graph-Mediator))
  (clay:initialize-paint-dependency diagram mediator)
  (initialize-graph-dependency diagram mediator)
  (clay:activate-children diagram mediator))

;;;------------------------------------------------------------

(defclass Graph-Mediator-Role (Clay:Mediator-Role) ())

(defmethod ac:build-roles ((c Graph-Mediator))
  (setf (ac:actor-default-role c)
    (make-instance 'Graph-Mediator-Role :role-actor c)))
 
;;;------------------------------------------------------------

(defparameter *graph-mediators* ()
  "Mediators for graph editors.")

(defun make-graph-mediator (graph)
  (az:type-check gra:Generic-Graph graph)
  (let ((c (make-instance `Graph-Mediator :mediator-subject graph)))
    ;; let the mediator be notified of changes to the graph
    (an:join-audience graph :changed c 'graph-changed)
    (ac:start-msg-handling c)
    c))

(defun graph-mediator (graph)
  (let ((m (first (clay:subject-mediators graph))))
    (when (null m) (setf m (make-graph-mediator graph)))
    m))

;;;------------------------------------------------------------

(defun make-graph-diagram (&key
			   (host (xlt:default-host))
			   (display (ac:host-display host))
			   (screen (xlib:display-default-screen display))
			   (subject (error "~&Subject must be supplied!"))
			   (mediator (graph-mediator subject))
			   (left 0) (top 0) (width 512) (height 512)
			   (name (gensym))
			   (initial-node-configuration nil)
			   (x-order-f nil)
			   (y-order-f nil)
			   (node-pair-distance-f nil)
			   (rest-length nil))
  
  "Make an interactive diagram for browsing and editing the subject graph."

  (declare (type gra:Generic-Graph subject) 
	   (type Graph-Mediator mediator)
	   (type (or String Symbol) name)
	   (type (or Null az:Int16-Vector) initial-node-configuration)
	   (type az:Funcallable x-order-f y-order-f node-pair-distance-f)
	   (type (or Null Number) rest-length)
	   (:returns (type Graph-Diagram)))  
  (let* ((parent (xlib:screen-root screen))
	 (window (xlt:make-window
		  :parent parent
		  :colormap (xlt:paint-colormap parent)
		  :left left :top top :width width :height height))
	 (diagram (make-instance 'Graph-Diagram
		    :diagram-name name
		    :diagram-subject subject
		    :diagram-window window)))
    (declare (type xlib:Window parent window)
	     (type Graph-Diagram diagram))
    (clay:initialize-diagram
     diagram
     :layout-options
     (list :initial-node-configuration initial-node-configuration
	   :x-order-f x-order-f
	   :y-order-f y-order-f
	   :node-pair-distance-f node-pair-distance-f
	   :rest-length rest-length)
     :mediator mediator)))

;;;------------------------------------------------------------
;;; Updating in response to changes in the graph
;;;------------------------------------------------------------

(defmethod graph-changed ((role Graph-Mediator-Role) 
			  msg-name sender receiver id)
  (declare (type Graph-Mediator-Role diagram)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type T receiver)
	   (type ac:Msg-Id id)
	   (ignore msg-name sender receiver id))
  (an:announce (ac:role-actor role) :changed))









