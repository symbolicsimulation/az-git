;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Paintbrush (Clay-Object)
	  ((paintbrush-width
	    :type xlib:Card16
	    :accessor paintbrush-width
	    :initform 16
	    :documentation "The width of the painted region.")
	   (paintbrush-height
	    :type xlib:Card16
	    :accessor paintbrush-height
	    :initform 16
	    :documentation "The height of the painted region.")
	   (paintbrush-hue-sat
	    :type xlt:Hue-Sat-Name
	    :reader paintbrush-hue-sat
	    :initform :gray
	    :documentation "The color of the paint.")
	   (paintbrush-aspect
	    :type (Member :hue-sat :sat :hue)
	    :reader paintbrush-aspect
	    :initform :hue-sat
	    :documentation
	    "What aspect of the painted object's color are we changing,
             ie., the color itself or the sat of the color?"))
  (:documentation
   "A paintbrush is used in painting. It determines various aspects of 
painting, like the size of the painted region, how the color of a 
painted object changes, etc."))

;;;============================================================

(defmethod (setf paintbrush-hue-sat) (x (paintbrush Paintbrush))
  (setf (slot-value paintbrush 'paintbrush-hue-sat) x)
  (an:announce paintbrush :changed)
  x)

(defmethod (setf paintbrush-aspect) (x (paintbrush Paintbrush))
  (setf (slot-value paintbrush 'paintbrush-aspect) x)
  (an:announce paintbrush :changed)
  x)

;;;============================================================

(defun paintbrush-gcontext (paintbrush window)
  (declare (type Paintbrush paintbrush)
	   (type xlib:Window window))
  (ecase (paintbrush-aspect paintbrush)
    (:hue
     (xlt:colorname->hue-gcontext
      window (paintbrush-hue-sat paintbrush)))
    (:sat
     (xlt:colorname->sat-gcontext
      window (paintbrush-hue-sat paintbrush)))
    (:hue-sat
     (xlt:colorname->hue-sat-gcontext
      window (paintbrush-hue-sat paintbrush)))))

;;;============================================================

(defparameter *paintbrush* (make-instance 'Paintbrush)
  "The default Paintbrush.")
 
