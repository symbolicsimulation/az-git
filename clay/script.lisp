;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Clay -*-

(in-package :Clay)

;;;============================================================

(defparameter *palette* (make-palette))
(show-diagram *palette*)
(hide-diagram *palette*)
(wt:winspect *palette*)
(show-diagram (palette))
