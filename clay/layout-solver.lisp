;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

;;; (defclass Layout-Solver (Clay-Object) ...) is in diagram.lisp

;;;------------------------------------------------------------

(defgeneric make-layout-domain (diagram solver)
  #-sbcl (declare (type Diagram diagram)
		  (type Layout-Solver solver)
		  (:returns (type brk:Domain)))
  (:documentation
   "Make a domain which represents the space of possible layouts of
<diagram>."))

(defmethod make-layout-domain ((diagram Diagram) (solver Layout-Solver))
  (brk:make-domain (diagram-bricks diagram)))

;;;------------------------------------------------------------

(defgeneric make-layout-cost (diagram solver)
  #-sbcl (declare (type Diagram diagram)
		  (type Layout-Solver solver)
		  (:returns (type brk:Functional)))
  (:documentation
   "Make a cost function which is minimized to compute the layout of
<diagram>. Note that the domain must be defined first, because the cost
is a function on the domain."))

(defmethod make-layout-cost ((diagram Diagram) (solver Layout-Solver))
  (brk:default-cost (layout-domain solver)))

;;;------------------------------------------------------------

(defgeneric make-layout-constraints (diagram solver)
  #-sbcl (declare (type Diagram diagram)
		  (type Layout-Solver solver)
		  (:returns (type brk:Inequality-List)))
  (:documentation
   "Make a set of affine inequality constraints that the layout of
<diagram> must satisfy. Note that the domain must be defined first, because 
the constraints are defined in terms of functionals over the domain."))

(defmethod make-layout-constraints ((diagram Diagram)
				    (solver Layout-Solver))
  "Diagrams recurse over children by default."
  (collect-from-children
   #'(lambda (child) (make-layout-constraints child solver))
   diagram))

;;;------------------------------------------------------------

(defgeneric solve-layout (diagram solver)
  #-sbcl (declare (type Diagram diagram)
		  (type Layout-Solver solver)
		  (:returns
		   (values
		    (type (Member :solution :no-feasible-point :unbounded) case)
		    (type Double-Float cost))))
  (:documentation
   "Determine the layout of <diagram> using <solver.>"))

(defmethod solve-layout ((diagram Diagram)
			 (solver Layout-Solver))

  "An instance of the Layout-Solver class solves layouts specified
as linear programming problems, using the Brick package."
  
  (declare (type Layout-Solver solver)
	   (:returns
	    (values
	     (type (Member :solution :no-feasible-point :unbounded) case)
	     (type Double-Float cost))))

  (brk:solve-constraints (layout-cost solver) (layout-constraints solver)
			 (layout-domain solver)))
 
;;;============================================================

