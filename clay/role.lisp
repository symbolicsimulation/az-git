;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================
;;; Objects to handle input to Root-Diagrams
;;;============================================================

(defclass Diagram-Role (ac:Interactor-Role Clay-Object) ())

;;;============================================================
  
(defun role-draw-diagram (role msg-name sender receiver id)
  "Drawing is sometimes handled by sending msgs, in order
to avoid synchronization problems that may occur when more than
one process is telling a diagram to draw itself at the same time.
The msg is called role-draw-diagram, and takes one argument, because
the diagram we want to draw may not be the role's actor itself."
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Diagram receiver)
	   (type ac:Msg-Id id)
	   (ignore role msg-name sender id))
  (draw-diagram receiver))
  
(defun role-redraw-diagram (role msg-name sender receiver id)
  "Redrawing is sometimes handled by sending msgs, in order
to avoid synchronization problems that may occur when more than
one process is telling a diagram to redraw itself at the same time.
The msg is called role-redraw-diagram, and takes one argument, because
the diagram we want to reredraw may not be the role's actor itself."
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Diagram receiver)
	   (type ac:Msg-Id id)
	   (ignore role msg-name sender id))
  ;; pop off any redundant redraw msgs
  (let ((queue (ac:actor-msg-queue receiver)))
    (declare (type ac:Queue queue))
    (loop
      (let ((msg (ac:dequeue queue)))
	(declare (type List msg))
	(cond ((null msg) (return)) ;; queue is empty
	      ((not (and (eq (first msg) 'role-redraw-diagram)
			 (eq (third msg) receiver)))
	       ;; it's not redundant, so put it back and return
	       (ac:queue-push msg queue)
	       (return))))))
  (redraw-diagram receiver))

;;;-----------------------------------------------------------

(defun role-hide-diagram (role msg-name sender receiver id)
  "Handler function for role-hide-diagram messages."
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Diagram receiver)
	   (type ac:Msg-Id id)
	   (ignore role msg-name sender id))
  (hide-diagram receiver))
 
;;;============================================================
;;; Painting stuff
;;;============================================================

(defun role-paintbrush (role)
  (declare (type Diagram-Role role)
	   (ignore role)
	   (:returns (type Paintbrush)))
  *paintbrush*)

(defmethod paintbrush-hue-sat ((role Diagram-Role))
  (declare (type Diagram-Role role)
	   (:returns (type xlt:Hue-Sat-Name)))
  (paintbrush-hue-sat (role-paintbrush role)))

(defmethod (setf paintbrush-hue-sat) ((colorname Symbol)
					(role Diagram-Role))
  (declare (type xlt:Hue-Sat-Name colorname)
	   (type Diagram-Role role)
	   (:returns (type xlt:Hue-Sat-Name)))
  (setf (paintbrush-hue-sat (role-paintbrush role)) colorname))

(defmethod paintbrush-aspect ((role Diagram-Role))
  (declare (type Diagram-Role role)
	   (:returns (type (Member :hue-sat :hue :sat))))
  (paintbrush-aspect (role-paintbrush role)))

(defmethod (setf paintbrush-aspect) ((aspect Symbol)
				     (role Diagram-Role))
  (declare (type (Member :hue-sat :hue :sat) aspect)
	   (type Diagram-Role role)
	   (:returns (type (Member :hue-sat :hue :sat))))
  (setf (paintbrush-aspect (role-paintbrush role)) aspect))

(defun role-paint-gcontext (role)
  "Get a gcontext that's right for the role's diagram's window."
  (declare (type Diagram-Role role))
  (paintbrush-gcontext (role-paintbrush role)
		       (diagram-window (ac:role-actor role))))

(defun recolor-role-cursor (role)
  (let* ((colorname (paintbrush-hue-sat role))
	 (diagram (ac:role-actor role))
	 (window (diagram-window diagram))
	 (colormap (xlt:paint-colormap window)))
    (declare (type xlt:Hue-Sat-Name colorname)
	     (type Diagram diagram)
	     (type xlib:Window window))  
    (xlib:recolor-cursor
     (ac:interactor-role-cursor diagram role)
     (xlt:pixel->color colormap (xlt:hue-sat-name->hs colorname))
     (xlt:colorname->color :black))
    (xlt:drawable-force-output window)))


(defmethod (setf ac:actor-current-role) :after ((role Diagram-Role)
						(diagram Diagram))
  (recolor-role-cursor role))

;;;============================================================

(defgeneric button-1-press (role x y)
  (declare (type Diagram-Role role)
	   (type xlib:Int16 x y)))

(defmethod button-1-press ((role Diagram-Role) x y)

  "The default method for button-1-press takes us into paint mode."

  (declare (type Diagram-Role role)
	   (type xlib:Int16 x y)
	   (ignore x y))
   
  (let* ((diagram (ac:role-actor role))
	 (painter (diagram-painter diagram)))
    (unless (null painter)
      (initialize-painter painter)
      (setf (ac:actor-current-role diagram) painter)))
  (values t))

;;;-----------------------------------------------------------

(defgeneric button-2-press (role x y)
  (declare (type Diagram-Role role)
	   (type xlib:Int16 x y))
  (:method
   ((role Diagram-Role) x y)
   "The default method for button-2-press does nothing."
   (declare (type Diagram-Role role)
	    (type xlib:Int16 x y)
	    (ignore x y))
   (values t)))

;;;-----------------------------------------------------------

(defgeneric button-3-press (role x y)
  (declare (type Diagram-Role role)
	   (type xlib:Int16 x y))
  (:method
   ((role Diagram-Role) x y)
   "The default method for button-2-press puts up a menu."
   (declare (type Diagram-Role role)
	    (type xlib:Int16 x y))
   (diagram-show-menu (ac:role-actor role) x y)
   (values t)))

;;;-----------------------------------------------------------

(defmethod ac:button-press ((role Diagram-Role)
			    msg-name sender receiver id
			    code x y state time root root-x root-y child
			    same-screen-p)
  
  "See if any child wants the button-press. If so, forward it.
If not, handle it here."
  
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))

  (let* ((selected (find-diagram-under-xy receiver x y)))
    (declare (type (or Null Diagram) selected))
    (cond ((or (null selected) (eq selected receiver))
	   ;; then handle it here
	   (case code
	     (1 (button-1-press role x y))
	     (2 (button-2-press role x y))
	     (3 (button-3-press role x y))))
	  ((typep selected 'Diagram)
	   ;; then set the diagram-selection and forward the event
	   (setf (diagram-selection receiver) selected)
	   (ac:send-msg 'ac:button-press receiver selected (ac:new-id)
			code x y state time root root-x root-y child
			same-screen-p))))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:button-release ((role Diagram-Role)
			      msg-name sender receiver id
			      code x y state time root root-x root-y child
			      same-screen-p)
  
  "This method returns control to the default role. 
If there is a diagram-selection, it is notified as well."

  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))

  ;; notify the selection, if there is one
  (when (diagram-selection receiver)
    (ac:send-msg 
     'ac:button-release receiver (diagram-selection receiver) (ac:new-id)
     code x y state time root root-x root-y child same-screen-p))
  ;; return control to the default role
  (setf (ac:actor-current-role receiver) (ac:actor-default-role receiver))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:configure-notify ((role Diagram-Role)
				msg-name sender receiver id
				x y width height border-width
				above-sibling override-redirect-p)
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 width height border-width)
	   (type (Or Null xlib:Window) above-sibling)
	   (type xlib:Boolean override-redirect-p)
	   (ignore msg-name sender id
		   x y border-width above-sibling override-redirect-p))
  (reconfigure-diagram receiver width height)
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:exposure ((role Diagram-Role) 
			msg-name sender receiver id
			x y w h count)
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card16 x y w h count)
	   (ignore msg-name sender id count))
  (let ((window (diagram-window receiver)))
    (g:with-borrowed-rect (r (xlt:drawable-space window)
			     :left x :top y :width w :height h)
      (repair-diagram receiver r))
    (xlt:drawable-force-output window))
  (values t))
  
;;;------------------------------------------------------------

(defmethod handle-key-press ((role Diagram-Role)
			     code x y state time root root-x root-y child
			     same-screen-p)

  "This method exits the event loop for #\Q or #\q.  It discards all
pending messages and returns to the default role on receiving a
control-c.  It destroys the diagram and window on #\x or #\X.  It
spins the diagram (if that makes sense) on #\s or #\S.  It prints the
mouse position on #\. .  It changes the paint color for other
characters."

  (declare (type Diagram-Role role)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore time root-x root-y child same-screen-p))

  (let* ((diagram (ac:role-actor role))
	 (display (xlib:window-display root))
	 (char (xlib:keycode->character display code state))
	 (modifiers (xlib:make-state-keys state)))
	
    (declare (type Interactor-Diagram diagram)
	     (type xlib:Display display)
	     (type (or Symbol Character) char)
	     (type List modifiers))
    (if (member :control modifiers)
      (case char
	((#\c #\C #+:excl #\control-\c #+:excl #\control-\C) 
	 ;; then it's an interrupt, discard all other msgs
	 ;; and revert to the default role
	 (print "interrupt!") (force-output)
	 (ac:queue-clear (ac:actor-msg-queue diagram))
	 (setf (ac:actor-current-role diagram)
	   (ac:actor-default-role diagram)))
	(otherwise nil))
      ;; else process the character
      (case char
	((#\1) (setf (paintbrush-aspect role) :hue-sat))
	((#\2) (setf (paintbrush-aspect role) :hue))
	((#\3) (setf (paintbrush-aspect role) :sat))
	((#\b) (setf (paintbrush-hue-sat role) :blue))
	((#\c) (setf (paintbrush-hue-sat role) :cyan))
	((#\g) (setf (paintbrush-hue-sat role) :green))
	((#\x #\X)
	 (ac:send-msg 'ac:destroy-request diagram diagram (ac:new-id)
		      (diagram-window diagram)))
	((#\m) (setf (paintbrush-hue-sat role) :magenta))
	((#\q #\Q) (ac:exit-event-loop t))
	((#\r) (setf (paintbrush-hue-sat role) :red))
	((#\s #\S) (spin-diagram (ac:role-actor role)))
	((#\w) (setf (paintbrush-hue-sat role) :gray))
	((#\y) (setf (paintbrush-hue-sat role) :yellow))
	((#\B) (setf (paintbrush-hue-sat role) :weak-blue))
	((#\C) (setf (paintbrush-hue-sat role) :weak-cyan))
	((#\G) (setf (paintbrush-hue-sat role) :weak-green))
	((#\M) (setf (paintbrush-hue-sat role) :weak-magenta))
	((#\R) (setf (paintbrush-hue-sat role) :weak-red))
	((#\W) (setf (paintbrush-hue-sat role) :weak-gray))
	((#\Y) (setf (paintbrush-hue-sat role) :weak-yellow))
	((#\.) (format t "~&mouse position=(~d,~d)" x y))
	(otherwise nil))))
  (recolor-role-cursor role)
  (values t))

(defmethod ac:key-press ((role Diagram-Role)
			 msg-name sender receiver id
			 code x y state time root root-x root-y child
			 same-screen-p)

  "If there is a diagram-selection, the key-press msg is forwarded.
Otherwise the key-press is handled here and now by calling handle-key-press."

  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))

  (if (diagram-selection receiver)
    ;; then forward it to the selection
    (ac:send-msg 
     'ac:key-press receiver (diagram-selection receiver) (ac:new-id)
     code x y state time root root-x root-y child same-screen-p)
    ;; else handle it here
    (handle-key-press role code x y state time root root-x root-y child
		      same-screen-p))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:key-release ((role Diagram-Role) 
			   msg-name sender receiver id
			   code x y state time root root-x root-y child
			   same-screen-p)

  "Forward the msg to the diagram-selection, if there is one."

  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))

  (when (diagram-selection receiver)
    ;; then forward it to the selection
    (ac:send-msg 
     'ac:key-release receiver (diagram-selection receiver) (ac:new-id)
     code x y state time root root-x root-y child same-screen-p))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:motion-notify ((role Diagram-Role) 
			     msg-name sender receiver id
			     hint-p x y state time root root-x root-y child 
			     same-screen-p)

  "Forward the msg to the diagram-selection, if there is one."

  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Boolean hint-p)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))

    (when (diagram-selection receiver)
      ;; then forward it to the selection
      (ac:send-msg 
       'ac:motion-notify receiver (diagram-selection receiver) (ac:new-id)
       hint-p x y state time root root-x root-y child same-screen-p))
  (values t))
 
;;;-----------------------------------------------------------

(defmethod ac:map-notify ((role Diagram-Role) 
			  msg-name sender receiver id
			  mapped-window override-redirect-p)
  "This method updates the window and returns control to the default role."
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type ac:Top-Level-Interactor receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Window mapped-window)
	   (type xlib:Boolean override-redirect-p)
	   (ignore msg-name sender id
		   mapped-window override-redirect-p))
  ;; catch up with changes while unmapped
  ;; and return control to the default role
  (when (diagram-outdated? receiver)
    (update-internal receiver nil)
    (draw-diagram receiver)
    (setf (diagram-outdated? receiver) nil))
  (setf (ac:actor-current-role receiver) (ac:actor-default-role receiver))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:unmap-notify ((role Diagram-Role) 
			    msg-name sender receiver id
			    unmapped-window configure-p)
  "Hand control to the unmapped role."
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Window unmapped-window)
	   (type xlib:Boolean configure-p)
	   (ignore msg-name sender id
		   unmapped-window configure-p))
  (setf (ac:actor-current-role receiver) (diagram-unmapped-role receiver))
  (values t))
 
;;;-----------------------------------------------------------

#|| Mapped windows are kept up to date, so drawing only has to be done
    on map-notify, rather than visibility-notify.

(defmethod ac:visibility-notify  ((role Diagram-Role) 
				  msg-name sender receiver id
				  state)
  "The default method does nothing."
  (declare (type Diagram-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type (Member :unobscured :partially-obscured :fully-obscured)
		 state)
	   (ignore msg-name sender id))
  (when (eq state :unobscured) (draw-diagram receiver))
  (values t))

||# 

;;;============================================================

(defgeneric stroke-object (actor stroke)
   
  (declare (optimize (safety 0) (speed 3))
	   (type ac:Actor actor)
	   (type Stroke stroke))
  
  (:documentation
   "This is applied to actors other than the diagram in which the stroke
originated, to bring them up to date."))

(defmethod stroke-object ((diagram Interactor-Diagram) (stroke Stroke))
  (update-internal diagram nil)
  (ac:send-msg 'role-redraw-diagram diagram diagram (ac:new-id))
  (ac:atomic
   (az:deletef diagram (stroke-objects-to-be-updated stroke))
   (when (null (stroke-objects-to-be-updated stroke))
     (return-stroke stroke))))

;;;============================================================
;;; Diagram-Role for "unmapped" mode
;;;============================================================

(defclass Unmapped-Role (Diagram-Role) ()
  (:documentation "An Unmapped-Role ignores all events except
<map-notify>."))

(defmethod stroke-object :before ((actor ac:Top-Level-Interactor) (stroke Stroke))
  "Still need to erase recording when stroked, even if unmapped."
  (erase-record actor)
  (setf (diagram-outdated? actor) t)
  (when (null (stroke-objects-to-be-updated stroke))
    (return-stroke stroke))
  t)

;;;============================================================
;;; Diagram-Roles for "paint" mode
;;;============================================================

(defclass Painter (Diagram-Role)
	  ((painter-window-rect
	    :type g:Rect
	    :reader painter-window-rect
	    :documentation
	    "The painted region in window coordinates.")
	   (painter-diagram-rect
	    :type g:Rect
	    :reader painter-diagram-rect
	    :documentation
	    "The painted region in the painted diagram's coordinates.")
	   (painter-stroke
	    :type Stroke
	    :accessor painter-stroke)
	   (painter-moved?
	    :type az:Boolean
	    :accessor painter-moved?
	    :initform nil
	    :documentation
	    "Indicates if there were any motion-notify events while
the Painter was in control. If nil, then we probably don't need to
notify anyone about the paint."))

  (:documentation
   "A <Painter> is an <Diagram-Role> for paint mode."))

;;;============================================================

(defgeneric initialize-painter (Painter))

(defmethod initialize-painter ((painter Painter))
  (let* ((gcontext (role-paint-gcontext painter))
	 (pixel (xlib:gcontext-foreground gcontext))
	 (pmask (xlib:gcontext-plane-mask gcontext))
	 (diagram (ac:role-actor painter)))
    (declare (type xlib:Gcontext gcontext)
	     (type az:Card8 pixel pmask)
	     (type Diagram diagram))
    (setf (slot-value painter 'painter-window-rect)
      (g:make-rect (xlt:drawable-space (diagram-window diagram))))
    (setf (slot-value painter 'painter-diagram-rect)
      (g:make-rect (diagram-view-space diagram)))
    (setf (painter-moved? painter) nil)
    (setf (painter-stroke painter) (borrow-stroke painter))
    (setf (stroke-pixel (painter-stroke painter)) pixel)
    (setf (stroke-pmask (painter-stroke painter)) pmask)))

;;;------------------------------------------------------------

(defmethod ac:interactor-role-cursor ((interactor Interactor-Diagram)
				      (role Painter))
  "A Painter has a spray can for its cursor."
  (xlt:window-cursor
   (ac:interactor-window interactor)
   (ecase (paintbrush-aspect role)
     (:hue-sat :paint-brush)
     (:hue :spraycan)
     (:sat :watercolor-brush))))

;;;============================================================
;;; Painter methods
;;;============================================================

(defgeneric borrow-stroke (painter)
  (declare (type Painter painter))
  (:method ((painter Painter)) nil))

;;;------------------------------------------------------------
;;; msg handlers
;;;------------------------------------------------------------

(defmethod ac:button-release ((painter Painter)
			      msg-name sender receiver id
			      code x y state time root root-x root-y child
			      same-screen-p)
  "This method takes us out of paint mode, causing any other
Diagrams to be updated."
  (declare (type Painter painter)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   code x y state time root root-x root-y child same-screen-p))
  (let ((stroke (painter-stroke painter)))
    (declare (type Stroke stroke))
    (when (painter-moved? painter)
      (setf (painter-stroke painter) nil) ;; to be safe
      (an:announce receiver :painted stroke)) 
    ;; return control to the default role
    (ac:atomic
     (setf (ac:actor-current-role receiver) (ac:actor-default-role receiver))))
  (values t))

;;;------------------------------------------------------------

(defgeneric paint-feedback (diagram painter x y)
  (declare (type Diagram diagram)
	   (type Painter painter)
	   (type az:Int16 x y)
	   (:returns (type g:Rect)))
  (:documentation
   "Provide immediate feedback (in some quick and dirty way)
for painting in <diagram>. Return the painted rectangle in the diagram's
coordinates."))

(defmethod paint-feedback ((diagram Diagram) (painter Painter) x y)

  "Draw a filled <rect> in <diagram>'s window using <gcontext>."

  (declare (type Diagram diagram)
	   (type Painter painter)
	   (type az:Int16 x y)
	   (:returns (type g:Rect)))
  (let* ((gcontext (role-paint-gcontext painter))
	 (wrect (painter-window-rect painter))
	 (paintbrush (role-paintbrush painter))
	 (width (paintbrush-width paintbrush))
	 (height (paintbrush-height paintbrush)))
    (declare (type xlib:Gcontext gcontext)
	     (type g:Rect wrect) 
	     (type Paintbrush paintbrush)
	     (type az:Card16 width height))
    ;; paint whatever falls under the brush region
    (setf (g:x wrect) x)
    (setf (g:y wrect) y)
    (setf (g:w wrect) width)
    (setf (g:h wrect) height)
    (let ((prect (clip-to-diagram! diagram wrect)))
      (xlt:draw-rect (diagram-window diagram) gcontext wrect t)
      ;; get those bits out on the screen
      (xlt:drawable-force-output (diagram-window diagram))
      prect)))

;;;------------------------------------------------------------

(defmethod ac:motion-notify ((painter Painter)
			     msg-name sender receiver id
			     hint-p x y state time root root-x root-y child
			     same-screen-p)
  "This method is intended to provide immediate feedback while painting"
  (declare (type Painter painter)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor-Diagram receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Boolean hint-p)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id 
		   hint-p state time root root-x root-y child same-screen-p))
  (setf (painter-moved? painter) t)
  (mark-stroke (painter-stroke painter)
	       (paint-feedback receiver painter x y))
  (values t))
