;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================
;;; Terminal Nodes in the Diagram Tree
;;;============================================================

(defclass Leaf-Diagram (Diagram) ())
(defclass Interactor-Leaf (Interactor-Diagram Leaf-Diagram) ())

;;;============================================================
;;; Traversing the diagram tree:
;;;============================================================
;;; This should normally return a sequence holding the children
;;; of the diagram

(defmethod diagram-children ((diagram Leaf-Diagram)) ())

;;;------------------------------------------------------------
;;; These methods will work for any instantiable Diagram
;;; class that has a proper method for <diagram-children>.

(defmethod map-over-children (result-type function (diagram Leaf-Diagram))
  (declare (ignore result-type function))
  ())

(defmethod some-child? (function (diagram Leaf-Diagram))
  (declare (ignore function))
  nil)

(defmethod find-child-if (function (diagram Leaf-Diagram))
  (declare (ignore function))
  nil)

;;;============================================================
;;; Building
;;;============================================================

;;;============================================================
;;; Layout
;;;============================================================

(defmethod diagram-bricks ((diagram Leaf-Diagram))
  (list diagram))

;;;------------------------------------------------------------

(defgeneric ensure-big-enough-pixmaps (diagram))


(defmethod ensure-big-enough-pixmaps ((diagram Leaf-Diagram))

  (declare (type Leaf-Diagram diagram))

  (let* ((window (diagram-window diagram))
	 (w (brk:width diagram))
	 (h (brk:height diagram))
	 (pixmap (diagram-pixmap diagram)))
    (declare (type xlib:Window window)
	     (type az:Int16 w h)
	     (type (or Null xlib:Pixmap) pixmap))
    (when (or (null pixmap)
	      (> w (xlib:drawable-width pixmap))
	      (> h (xlib:drawable-height pixmap)))
      ;; then we need to make a -new- pixmap
      (setf (diagram-pixmap diagram)
	(xlib:create-pixmap :drawable window
			    :width w :height h
			    :depth (xlt:drawable-depth window)))
      (unless (null pixmap) (az:kill-object pixmap))))
  t)
    
;;;============================================================
;;; Drawing
;;;============================================================
;;; need to define a null method to intercept default
;;; inherited from Diagram

(defmethod diagram-update-pixmaps ((diagram Leaf-Diagram))
  (declare (type Leaf-Diagram diagram))
  (render-diagram diagram (diagram-pixmap diagram)))

;;;------------------------------------------------------------
;;; automate redrawing whenever gcontext is changed

(defmethod (setf diagram-gcontext) (gcontext (diagram Leaf-Diagram))
  (setf (slot-value diagram 'diagram-gcontext) gcontext)
  (layout-diagram diagram nil)
  (redraw-diagram diagram))

;;;------------------------------------------------------------

(defmethod draw-diagram ((diagram Leaf-Diagram))
  (declare (type Leaf-Diagram diagram))
  (let ((r (diagram-window-rect diagram)))
    (declare (type g:Rect r))
    (xlib:copy-area
     (diagram-pixmap diagram) (diagram-gcontext diagram)
     0 0 (g:w r) (g:h r)
     (diagram-window diagram)
     (g:x r) (g:y r))))

;;;------------------------------------------------------------

(defmethod erase-diagram ((diagram Leaf-Diagram) rect)
  (declare (type Leaf-Diagram diagram)
	   (type (or Null g:Rect) rect))
  (let ((r (diagram-window-rect diagram)))
    (declare (type g:Rect r))
    (xlt:draw-rect (diagram-window diagram)
		   (diagram-erasing-gcontext diagram) r t)
    (unless (null rect) (az:copy r :result rect))))

;;;------------------------------------------------------------

(defmethod repair-diagram ((diagram Leaf-Diagram) damaged-rect)
  (declare (type Leaf-Diagram diagram)
	   (type g:Rect damaged-rect))
  (when (g:intersect? (diagram-window-rect diagram) damaged-rect)
    ;;(intersect-diagram? diagram damaged-rect)
    ;; use the less general definition for now,
    ;; since PCL overhead is so high
    (draw-diagram diagram)))

;;;============================================================
;;; Input
;;;============================================================

