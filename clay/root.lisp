;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Root-Diagram (ac:Top-Level-Interactor Interactor-Diagram)
	  ((diagram-window
	    :type xlib:Window
	    :accessor diagram-window
	    :initarg :diagram-window)
	   (diagram-fits-in-window?
	    :type az:Boolean
	    :accessor diagram-fits-in-window?
	    :initform t
	    :documentation
	    "If true, then the diagram is laid out with constraints
             that ensure that it stays within the initial window size.
             If nil, the window is resized to be big enough for the 
             diagram to fit. Normally, a diagram will attempt to
             lay itself out within the given window. If this layout
             fails, it will set <diagram-fits-in-window?> to nil
             and retry.")
	   (diagram-unmapped-role
	    :type Unmapped-Role
	    :accessor diagram-unmapped-role))
 
  (:documentation
   "The root of a diagram tree receives all X input events to its window.
It is responsible for either handling or filtering, translating, and
dispatching them to the appropriate child diagram."))

;;;============================================================

(defmethod diagram-gcontext ((diagram Root-Diagram))
  (or (slot-value diagram 'diagram-gcontext)
      (xlt:drawable-default-gcontext (diagram-window diagram))))

(defmethod diagram-erasing-gcontext ((diagram Root-Diagram))
  (or (slot-value diagram 'diagram-erasing-gcontext)
      (xlt:drawable-erasing-gcontext (diagram-window diagram))))

;;;============================================================

(defmethod initialize-instance :after ((diagram Root-Diagram) &rest initargs)
  (declare (ignore initargs))
  (let ((w (diagram-window diagram)))
    (declare (type xlib:Window w))
    (setf (xlib:wm-name w) (diagram-name diagram))
    (setf (xlib:wm-icon-name w) (diagram-name diagram))))

;;;============================================================

(defmethod ac:interactor-window ((diagram Root-Diagram))
  (diagram-window diagram))

(defmethod (setf ac:interactor-window) (window (diagram Root-Diagram))
  (declare (type xlib:Window window))
  (setf (diagram-window diagram) window))

;;;============================================================
;;; Initialization
;;;============================================================

(defmethod initialize-diagram ((diagram Root-Diagram)
			       &key
			       (build-options ())
			       (layout-options ())
			       (mediator nil))
  "Root-Diagrams draw themselves after initialization."
  (declare (type Root-Diagram diagram)
	   (type List build-options layout-options)
	   (type (or Null Mediator) mediator)
	   (:returns (type Root-Diagram diagram)))
  (build-diagram diagram build-options)
  (layout-diagram diagram layout-options)
  (activate-diagram diagram mediator)
  (ac:send-msg 'role-redraw-diagram diagram diagram (ac:new-id))
  diagram)

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-diagram ((diagram Root-Diagram) build-options)
  (declare (type Root-Diagram diagram)
	   (type List build-options)
	   (ignore build-options))
  (build-children diagram)
  (setf (diagram-layout-solver diagram) (build-layout-solver diagram)))

(defmethod ac:build-roles :after ((diagram Root-Diagram))
  "Root-Diagram adds the unmapped role to the Interactor-Diagram roles"
  (setf (diagram-unmapped-role diagram)
    (make-instance 'Unmapped-Role :role-actor diagram)))

(defmethod build-children ((diagram Root-Diagram))
  "By default, a Root-Diagram hash no children."
  t)

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-diagram ((root Root-Diagram) layout-options)
  "Layout the root diagram"
  (declare (type Root-Diagram root)
	   (type List layout-options)
	   (ignore layout-options))
  (update-layout-solver root (diagram-layout-solver root))
  (solve-layout root (diagram-layout-solver root))
  (layout-children root)
  (update-internal root nil)
  root)

;;;============================================================
;;; Drawing
;;;============================================================
;;; showing and hiding (mapping and unmapping)

(defgeneric show-diagram (diagram &key x y)
  (declare (type Root-Diagram diagram)
	   (type az:Int16 x y))
  (:documentation
   "Make the diagram visible, essentially by mapping its window,
placing it so that it is approximately centered it over (x,y)."))

(defmethod show-diagram ((diagram Root-Diagram)
			 &key
			 (x (xlt:drawable-root-x (diagram-window diagram)))
			 (y (xlt:drawable-root-y (diagram-window diagram))))
  "Makes the diagram visible, placing it near the point (x,y) in the root 
window. If x or y is not supplied, then it is put near its last position, 
but may be moved so that it is entirely on the screen. "  
  (declare (type Root-Diagram diagram)
	   (type az:Int16 x y))
  (let ((window (diagram-window diagram)))
    (xlt:place-window window :left x :top y)
    (xlt:size-window window
		     :width (brk:width diagram)
		     :height (brk:height diagram))
    (xlt:expose-window window)))

(defgeneric hide-diagram (diagram)
  (declare (type Root-Diagram diagram))
  (:documentation
   "Make the diagram invisible, essentially by unmapping its window."))

(defmethod hide-diagram ((diagram Root-Diagram))
  "Makes the diagram invisible."
  (declare (type Root-Diagram diagram))
  (xlib:unmap-window (diagram-window diagram))
  (xlt:drawable-force-output (diagram-window diagram)))

;;;============================================================
;;; Input
;;;============================================================

(defmethod stroke-object :before ((diagram Root-Diagram) (stroke Stroke))
   "Any recording will be invalidated by stroking, so erase."
   (erase-record diagram))



