;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================
;;; Assorted classes for Terminal Nodes in the Diagram Tree
;;;============================================================

;;;============================================================
;;; Lines
;;;============================================================

(defclass Line-Diagram (Leaf-Diagram)
     ((from-point
	:type g:Point
	:reader from-point)
      (to-point
	:type g:Point
	:reader to-point)))

;;;------------------------------------------------------------

(defmethod initialize-instance :after ((diagram Line-Diagram) &rest inits)
  (declare (ignore inits))
  (setf (slot-value diagram 'from-point)
    (g:make-element (xlt:drawable-space (diagram-window diagram))))
  (setf (slot-value diagram 'to-point)
    (g:make-element (xlt:drawable-space (diagram-window diagram)))))

;;;------------------------------------------------------------

(defmethod draw-diagram ((diagram Line-Diagram))
  (declare (type Line-Diagram diagram))
  (xlt:draw-line (diagram-window diagram)
		 (diagram-gcontext diagram)
		 (from-point diagram)
		 (to-point diagram)))

(defmethod erase-diagram ((diagram Line-Diagram) rect)
  (declare (type (or Null g:Rect) rect))
  (xlt:draw-line (diagram-window diagram)
		 (diagram-erasing-gcontext diagram)
		 (from-point diagram)
		 (to-point diagram))
  (unless (null rect)
    (az:copy (diagram-window-rect diagram) :result rect)))

;;;============================================================

(defclass Arrow-Diagram (Line-Diagram) ())

;;;------------------------------------------------------------

(defmethod draw-diagram ((diagram Arrow-Diagram))
  (declare (optimize (safety 0) (speed 3))
	   (type Arrow-Diagram diagram))
  (xlt:draw-arrow (diagram-window diagram)
		  (diagram-gcontext diagram)
		  (from-point diagram)
		  (to-point diagram)))

(defmethod erase-diagram ((diagram Arrow-Diagram) rect)
  (declare (optimize (safety 0) (speed 3))
	   (type Arrow-Diagram diagram)
	   (type (or Null g:Rect) rect))
  (xlt:draw-arrow (diagram-window diagram)
		  (diagram-erasing-gcontext diagram)
		  (from-point diagram)
		  (to-point diagram))
  (unless (null rect) (az:copy (diagram-window-rect diagram) :result rect)))
