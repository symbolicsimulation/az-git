;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Palette (Root-Diagram)
	  ((palette-paintbrush
	    :type T ;;Paintbrush
	    :reader palette-paintbrush
	    :initform *paintbrush*
	    :documentation
	    "The paintbrush this palette modifies.")
	   (palette-gray-button
	    :type T ;;(or Null Button)
	    :accessor palette-gray-button
	    :initform ()
	    :documentation "A button for painting in gray.")
	   (palette-strong-buttons
	    :type List ;; of Button
	    :accessor palette-strong-buttons
	    :initform ()
	    :documentation "Buttons for painting in strong colors.")
	   (palette-weak-buttons
	    :type List ;; of Button
	    :accessor palette-weak-buttons
	    :initform ()
	    :documentation "Buttons for painting in weak colors.")
	   (palette-hue-buttons
	    :type List ;; of Button
	    :accessor palette-hue-buttons
	    :initform ()
	    :documentation "Buttons for painting in hue only.")
	   (palette-sat-buttons
	    :type List ;; of Button
	    :accessor palette-sat-buttons
	    :initform ()
	    :documentation "Button for painting in saturationo only."))

  (:documentation
   "Used for choosing the Paintbrush hue and/or saturation."))

;;;------------------------------------------------------------
;;; Tree traversal
;;;------------------------------------------------------------

(defmethod diagram-children ((palette Palette))
  (cons (palette-gray-button palette)
	(concatenate 'List
	  (palette-strong-buttons palette)
	  (palette-weak-buttons palette)
	  (palette-hue-buttons palette)
	  (palette-sat-buttons palette))))
  
;;;------------------------------------------------------------
;;; Creation
;;;------------------------------------------------------------

(defun make-palette (&key
		     (paintbrush *paintbrush*)
		     (host (xlt:default-host))
		     (display (ac:host-display host))
		     (screen (xlib:display-default-screen display))
		     (parent (xlib:screen-root screen)))

  "Create a palette that can be used to modify <paintbrush>."
  
  (declare (type Paintbrush paintbrush)
	   (type xlt:Hostname host)
	   (type xlib:Display display)
	   (type xlib:Screen screen)
	   (type xlib:Window parent)
	   (:returns (type Palette)))
  
  (let* ((window (xlt:make-window :parent parent
				  :colormap (xlt:paint-colormap parent)
				  :map? nil))
	 (palette (make-instance 'Palette
		    :diagram-name (format nil "Palette for ~a" paintbrush)
		    :palette-paintbrush paintbrush
		    :diagram-window window)))
    (declare (type xlib:Window window)
	     (type Palette palette)
	     (:returns (type Palette palette)))
    (initialize-diagram palette)))

;;;------------------------------------------------------------

(defparameter *paintbrush-palettes* (make-hash-table :test #'eq)
  "a table that maps paintbrushes to palettes used to change them.")

(defun palette (display &key (paintbrush *paintbrush*))
  "Get a palette for this <paintbrush>. Only create a palette
if one doesn't exist."
  (declare (type Paintbrush paintbrush)
	   (:returns (type Palette)))
  (let ((plt (gethash paintbrush *paintbrush-palettes* nil)))
    (when (or (null plt)
	      (typep plt 'az:Dead-Object))
      (setf plt (make-palette :display display :paintbrush paintbrush))
      (setf (gethash paintbrush *paintbrush-palettes*) plt))
    plt))

;;;------------------------------------------------------------
;;; Building
;;;------------------------------------------------------------

(defun make-hue-button (palette name)
  (declare (type Palette palette)
	   (type Symbol name))
  (let ((button (make-instance 'Labeled-Button :diagram-parent palette)))
    (declare (type Labeled-Button button))
    (setf (label-string button) (string-capitalize (symbol-name name)))
    (setf (button-hue-sat-name button) name)
    (ac:start-msg-handling button)
    button))

(defun make-sat-button (palette name)
  (declare (type Palette palette)
	   (type Symbol name))
  (let ((button (make-instance 'Labeled-Button :diagram-parent palette)))
    (declare (type Labeled-Button button))
    (setf (label-string button) (string-capitalize (symbol-name name)))
    (ac:start-msg-handling button)
    button))

(defun make-color-button (palette name)
  (declare (type Palette palette)
	   (type xlt:Hue-Sat-Name name))
  (let ((button (make-instance 'Button :diagram-parent palette)))
    (declare (type Button button))
    (setf (button-hue-sat-name button) name)
    (setf (button-minimum-width button) 16)
    (setf (button-minimum-height button) 16)
    (ac:start-msg-handling button)
    button))

;;;------------------------------------------------------------

(defmethod build-diagram ((palette Palette) build-options)
  "Make buttons for the palette items."
  (declare (type Palette palette)
	   (type List build-options)
	   (ignore build-options))

  (setf (palette-gray-button palette) (make-color-button palette :gray))

  (setf (palette-strong-buttons palette)
    (map 'List  #'(lambda (name) (make-color-button palette name))
	 xlt:-strong-names-))
  
  (setf (palette-weak-buttons palette)
    (map 'List  #'(lambda (name) (make-color-button palette name))
	 xlt:-weak-names-))
  
  (setf (palette-sat-buttons palette)
   (map 'List  #'(lambda (name) (make-sat-button palette name))
	 xlt:-sat-names-))
    
  (setf (palette-hue-buttons palette)
    (map 'List  #'(lambda (name) (make-hue-button palette name))
	 xlt:-strong-names-))

  (setf (diagram-layout-solver palette) (build-layout-solver palette)))

;;;------------------------------------------------------------

(defmethod ac:build-roles ((diagram Palette))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Palette-Role :role-actor diagram)))

;;;------------------------------------------------------------
;;; Layout
;;;------------------------------------------------------------

(defmethod reconfigure-diagram ((palette Palette) w h)
  (declare (ignore w h))
  t)

(defmethod make-layout-constraints ((palette Palette) (solver Layout-Solver))
 
  (let ((domain (layout-domain solver))
	(buttons (diagram-children palette))
	(gray (palette-gray-button palette))
	(strong (palette-strong-buttons palette))
	(weak (palette-weak-buttons palette))
	(hue (palette-hue-buttons palette))
	(sat (palette-sat-buttons palette)))
    (declare (type brk:Domain domain)
	     (type List buttons))
    (nconc (apply #'brk:surround domain palette 2 2 buttons)
	   (apply #'brk:equal-size domain buttons)
	   (apply #'brk:in-column domain 4 hue)
	   (apply #'brk:centered-column domain 4 (first sat)
		  (append strong (list gray)))
	   (apply #'brk:in-column domain 4 (second sat) weak)
	   (apply #'brk:in-row domain 4 sat)
	   (mapcan #'(lambda (h s w) (brk:centered-row domain 4 h s w))
		   hue strong weak)
	   (collect-from-children #'(lambda (child)
				      (make-layout-constraints child solver))
				  palette))))

;;;------------------------------------------------------------

(defun update-button-states (plt)
  "Examine the paintbrush to determine which button should be down
and then change the palette so that that is the only button down."
  (declare (type Palette plt))
  (let* ((paintbrush (palette-paintbrush plt))
	 (aspect (paintbrush-aspect paintbrush))
	 (hs (paintbrush-hue-sat paintbrush))
	 (old-button
	  (find :down (diagram-children plt) :key #'button-state))
	 (new-button
	  (ecase aspect
	    (:hue-sat
	     (or (when (eql hs (button-hue-sat-name (palette-gray-button plt)))
		   (palette-gray-button plt))
		 (find hs (palette-strong-buttons plt)
		       :key #'button-hue-sat-name)
		 (find hs (palette-weak-buttons plt)
		       :key #'button-hue-sat-name)))
	    (:hue
	     (find (xlt:coerce-saturation hs :strong)
		   (palette-hue-buttons plt) :key #'button-hue-sat-name))
	    (:sat
	     (find (if (xlt:strong-colorname? hs) "strong" "weak")
		   (palette-sat-buttons plt)
		   :key #'label-string :test #'string-equal)))))
    (declare (type Paintbrush paintbrush)
	     (type (Member :hue-sat :sat :hue) aspect)
	     (type xlt:Hue-Sat-Name hs)
	     (type Button old-button new-button))
    (unless (eq old-button new-button)
      (when old-button (setf (button-state old-button) :up))
      (setf (button-state new-button) :down))))

;;;------------------------------------------------------------
;;; Drawing and Erasing
;;;------------------------------------------------------------

(defmethod update-internal ((palette Palette) (stroke T))
  "Update the buttons"
  (update-button-states palette))

;;;------------------------------------------------------------
;;; Input
;;;------------------------------------------------------------

(defmethod activate-diagram ((palette Palette) (mediator Null))
  (an:join-audience (palette-paintbrush palette) :changed palette
		    'role-update-button-states))

;;;-----------------------------------------------------------

(defclass Palette-Role (Diagram-Role) ()
  (:documentation
   "The default role for palettes."))

;;;-----------------------------------------------------------

(defun role-update-button-states (role msg-name sender receiver id)
  (declare (type Palette-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Palette receiver)
	   (type ac:Msg-Id id)
	   (ignore role msg-name sender id))
  (update-button-states receiver))

;;;-----------------------------------------------------------

(defmethod ac:button-press ((role Palette-Role)
			    msg-name sender receiver id
			    code x y state time root root-x root-y child
			    same-screen-p)

  (declare (type Palette-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Palette receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id))

  (let ((selected (find-diagram-under-xy receiver x y)))
    (declare (type Button selected))
    (when (typep selected 'Button)
      ;; forward the event
      (dolist (button (diagram-children receiver))
	(when (eq :down (button-state button))
	  (setf (button-state button) :up)))
      (ac:send-msg 'ac:button-press receiver selected (ac:new-id)
		   code x y state time root root-x root-y child
		   same-screen-p)
      (let ((brush (palette-paintbrush receiver)))
	(cond ((or (member selected (palette-strong-buttons receiver))
		   (member selected (palette-weak-buttons receiver))
		   (eq selected (palette-gray-button receiver)))
	       (setf (paintbrush-hue-sat brush) (button-hue-sat-name selected))
	       (setf (paintbrush-aspect brush) :hue-sat))
	      ((member selected (palette-hue-buttons receiver))
	       (setf (paintbrush-hue-sat brush) (button-hue-sat-name selected))
	       (setf (paintbrush-aspect brush) :hue))
	      ((member selected (palette-sat-buttons receiver))
	       (setf (paintbrush-hue-sat brush)
		 (xlt:coerce-saturation 
		  (paintbrush-hue-sat brush)
		  (intern (string-upcase (label-string selected)) :Keyword)))
	       (setf (paintbrush-aspect brush) :sat))))))
  (values t))





