;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Mediator (ac:Actor Clay-Object)
	  ((mediator-subject
	    :type T
	    :accessor mediator-subject
	    :initarg :mediator-subject
	    :documentation
	    "All the mediator's diagrams are presentations of its subject.")))

(defclass Diagram-Mediator (Mediator) ())

(defclass Mediator-Role (ac:Role Clay-Object) ())

;;;============================================================

(defvar *mediators* ()
  "A list of all the mediators ever created.  Used to dispatch
announcements. Will eventually become a hashtable or something
similar.")

(defmethod initialize-instance :after ((mediator Mediator) &rest initargs)
  (declare (optimize (safety 0) (speed 3))
	   (ignore initargs)
	   (special *mediators*))
  (push mediator *mediators*))

(defun subject-mediators (mediator-subject)
  (declare (special *mediators*))
  (az:with-collection
   (dolist (mediator *mediators*)
     (when (eq mediator-subject (mediator-subject mediator))
       (az:collect mediator)))))

;;;------------------------------------------------------------

(defun role-stroke-object (role msg-name sender receiver id stroke)
  "Just stroke the receiver."
  (declare (type ac:Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type ac:Actor receiver)
	   (type ac:Msg-Id id)
	   (type Stroke stroke)
	   (ignore role msg-name sender id))
  (stroke-object receiver stroke))

;;;------------------------------------------------------------

(defun initialize-paint-dependency (diagram mediator)
  (declare (type Diagram diagram)
	   (type Mediator mediator))
  (an:join-audience diagram :painted mediator 'role-stroke-object)
  (an:join-audience mediator :painted diagram 'role-stroke-object))
 
;;;============================================================

(defmethod az:kill-object :before ((mediator Mediator))
  (setf *mediators* (delete mediator *mediators*))
  (values))



