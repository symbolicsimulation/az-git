;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Plot-Root (Root-Diagram)
	  ((plot-child
	    :type (or Null Interactor-Diagram)
	    :reader plot-child
	    :initform nil
	    :documentation
	    "This child is the actual plot.")
	   (plot-control-panel
	    :type (or Null Interactor-Diagram)
	    :accessor plot-control-panel
	    :initform nil
	    :documentation
	    "This child is a control panel of thing to do to the plot-child."))
  (:documentation
   "A Plot-Root is a top-level, root diagram used to hold plots.
One basic property that distinguishes Plot-Root's from other root diagrams
is the loss function used for layout, which, taking the window size as a
constraint, maximizes the size of the plot child at the expense of everything 
else."))

;;;------------------------------------------------------------
;;; Tree traversal
;;;------------------------------------------------------------

(defmethod diagram-children ((root Plot-Root))
  (delete nil
	  (list (plot-child root)
		(plot-control-panel root))))

;;;------------------------------------------------------------
;;; Creation
;;;------------------------------------------------------------

;;;------------------------------------------------------------
;;; Building
;;;------------------------------------------------------------

(defmethod build-diagram ((root Plot-Root) build-options)
  
  "Get the child and control panel from the build-options."
  
  (declare (type Plot-Root root)
	   (type List build-options))

  (let ((plot (getf build-options :plot-child)))
    (declare (type Diagram plot))
    (setf (slot-value root 'plot-child) plot)
    (setf (slot-value root 'plot-control-panel) (make-control-panel plot))
    (setf (diagram-layout-solver root) (build-layout-solver root))))

;;;------------------------------------------------------------
;;; Layout
;;;------------------------------------------------------------

(defmethod make-layout-domain ((root Plot-Root) (solver Layout-Solver))
  (brk:make-domain (cons root (copy-list (diagram-children root)))))


(defmethod make-layout-cost ((root Plot-Root) (solver Layout-Solver))

  "Make a functional that maximizes the width and height of the plot-child
and minimizes everything else."

  (declare (:returns (type brk:Functional)))

  (let* ((domain (layout-domain solver))
	 (plot (plot-child root))
	 (bricks (remove plot (brk:bricks domain))))
    (declare (type brk:Domain domain)
	     (type Interactor-Diagram plot)
	     (type brk:Brick-List bricks))
    (if (diagram-fits-in-window? root)
	(apply #'brk:make-functional domain
	       (brk:make-term 1.0d0 :left plot)
	       (brk:make-term 1.0d0 :top plot)
	       (brk:make-term -1000.0d0 :width plot)
	       (brk:make-term -1000.0d0 :height plot)
	       (az:with-collection
		   (dolist (brick bricks)
		     (declare (type brk:Brick brick))
		     (az:collect (brk:make-term 1.0d0 :left brick))
		     (az:collect (brk:make-term 1.0d0 :width brick))
		     (az:collect (brk:make-term 1.0d0 :top brick))
		     (az:collect (brk:make-term 1.0d0 :height brick)))))
      ;; else
      (apply #'brk:make-functional domain
	     (brk:make-term 1.0d0 :left plot)
	     (brk:make-term 1.0d0 :top plot)
	     (brk:make-term 1.0d0 :width plot)
	     (brk:make-term 1.0d0 :height plot)
	     (az:with-collection
		 (dolist (brick bricks)
		   (declare (type brk:Brick brick))
		   (az:collect (brk:make-term 1.0d0 :left brick))
		   (az:collect (brk:make-term 1.0d0 :width brick))
		   (az:collect (brk:make-term 1.0d0 :top brick))
		   (az:collect (brk:make-term 1.0d0 :height brick))))))))

(defmethod make-layout-constraints ((root Plot-Root) (solver Layout-Solver))
  (let* ((domain (layout-domain solver))
	 (plot (plot-child root))
	 (panel (plot-control-panel root))
	 (window (diagram-window root))
	 (w (xlib:drawable-width window))
	 (h (xlib:drawable-height window)))
    (declare (type brk:Domain domain)
	     (type Diagram plot)
	     (type Control-Panel panel)
	     (type xlib:Window window)
	     (type xlib:Card16 w h))
    ;; the control panel determines the minimum size
    (setf w (max w (brk:width panel)))
    (setf h (max h (brk:height panel)))
    (nconc
     (brk:surround domain root 0.0d0 0.0d0 plot)
     (brk:surround domain root 0.0d0 0.0d0 panel)
     (brk:fixed-size domain (brk:width panel) (brk:height panel) panel)
     ;; the following is redundant, but seems to be necessary --
     ;; maybe there's a bug in brk:solve-constraints
     (brk:minimum-size domain (brk:width panel) (brk:height panel) root)
     (brk:above domain plot 0.0d0 panel)
     (make-layout-constraints plot solver)
     (when (diagram-fits-in-window? root)
       (brk:maximum-size domain w h root)))))

;;;------------------------------------------------------------

(defmethod layout-diagram ((root Plot-Root) layout-options)
  "Layout the plot-root"
  (declare (type Plot-Root root)
	   (type List layout-options)
	   (ignore layout-options))
  (let* ((panel (plot-control-panel root))
	 (plot (plot-child root))
	 (window (diagram-window root))
	 (case :no-feasible-point))
    (declare (type Diagram plot)
	     (type Control-Panel panel)
	     (type xlib:Window window)
	     (type (Member :solution :no-feasible-point :unbounded) case))
    
    ;; The control panel has a fixed minimum size, which implies 
    ;; a minimum size for the winodw. This is determined by laying 
    ;; it out first.
    (layout-diagram panel nil)
    
    ;; Determine how big the root brick must to enclose
    ;; both the plot and the panel, and determine positions
    ;; for the plot and the panel. The plot size is maximized.
    
    ;; first try to layout in the given window
    (setf (diagram-fits-in-window? root) t)
    (update-layout-solver root (diagram-layout-solver root))
    (setf case (solve-layout root (diagram-layout-solver root)))
    ;; check first try
    (ecase case
      (:no-feasible-point
       ;; relax the window size constraint and try again
       (setf (diagram-fits-in-window? root) nil)
       (update-layout-solver root (diagram-layout-solver root))
       (setf case (solve-layout root (diagram-layout-solver root))))
      (:unbounded (error "Layout unbounded"))
      (:solution t))
    ;; check second try
    (ecase case
      ((:no-feasible-point :unbounded) (error "Layout unsolvable: ~a" case))
      (:solution t)) 

    ;; Now we can layout the plot. It takes its size as given.
    (layout-diagram plot nil)
    
    ;; Translate the components of the plot and the panel so 
    ;; their diagram-window-rects have the correct coordinates
    (translate-children plot)
    (translate-children panel)
    
    ;; resize the window so that everything fits
    (xlt:size-window window :width (brk:width root) :height (brk:height root))
    (update-internal root nil)
    root))

;;;--------------------------------------------------------------

(defmethod reconfigure-diagram ((diagram Plot-Root) width height)
  (declare (type az:Card16 width height))
  (unless (and (= width (brk:width diagram))
	       (= height (brk:height diagram)))
    ;; let the scale be determined by the window size
    (setf (scale (diagram-lens (plot-child diagram))) nil)
    (layout-diagram diagram nil)
    (ac:send-msg 'role-redraw-diagram diagram diagram (ac:new-id))))
 
;;;------------------------------------------------------------
;;; Drawing and Erasing
;;;------------------------------------------------------------
;;;------------------------------------------------------------ 
;;; Input 
;;;------------------------------------------------------------




