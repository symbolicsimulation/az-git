;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================

(defclass Radio-Button (Toggle) ()
  (:documentation
   "A Radio-Button goes down if up when pressed,
ignores button-press events if already down,
and ignores all button-release events.
It goes up when some other button in it's set is pressed,
causing the parent diagram to set it's state to :up explicitly."))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Radio-Button))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Radio-Button-Role :role-actor diagram)))

;;;============================================================
;;; Layout
;;;============================================================

;;;============================================================
;;; Drawing
;;;============================================================

;;;============================================================
;;; Input
;;;============================================================

(defclass Radio-Button-Role (Toggle-Role) ()
  (:documentation
   "A Radio-Button-Role handles input forwarded by the Radio-Button's 
parent diagram."))

;;;-----------------------------------------------------------

(defmethod ac:button-press ((role Radio-Button-Role) 
			    msg-name sender receiver id
			    code x y state time root root-x root-y child 
			    same-screen-p)
  "Any button-press event on a Radio-Button causes the button state
to be flipped if it's either :up or :down."
  (declare (type Button-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Radio-Button receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   code x y state time root root-x root-y child same-screen-p))
  (when (eql :up (button-state receiver)) (setf (button-state receiver) :down))
  (values t))


