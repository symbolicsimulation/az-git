;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================

(defclass Toggle (Button) ()
  
  (:documentation
   "A Toggle is a button that stays down unntil pressed a second time."))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Toggle))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Toggle-Role :role-actor diagram)))

;;;============================================================
;;; Layout
;;;============================================================

;;;============================================================
;;; Drawing
;;;============================================================

;;;============================================================
;;; Input
;;;============================================================

(defclass Toggle-Role (Diagram-Role) ()
  (:documentation
   "A Toggle-Role handles input forwarded by the Toggle's parent diagram."))

;;;-----------------------------------------------------------

(defmethod ac:button-press ((role Toggle-Role) 
			    msg-name sender receiver id
			    code x y state time root root-x root-y child 
			    same-screen-p)
  "Any button-press event on a Toggle causes the button state
to be flipped if it's either :up or :down."
  (declare (type Button-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Toggle receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   code x y state time root root-x root-y child same-screen-p))
  (case (button-state receiver)
    (:up (setf (button-state receiver) :down))
    (:down (setf (button-state receiver) :up)))
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:button-release ((role Toggle-Role)
			      msg-name sender receiver id
			      code x y state time root root-x root-y child
			      same-screen-p)
  "A button-release event on a Toggle is ignored."
  (declare (type Button-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Toggle receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender receiver id
		   code x y state time root root-x root-y child same-screen-p))
  (values t))

