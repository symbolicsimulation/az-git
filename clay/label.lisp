;;;-*- Package: :Clay; Syntax: Common-Lisp; M20767ode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================

(defclass Label (Leaf-Diagram) 
	  ((label-string
	    :type String
	    :accessor label-string
	    :initform ""
	    :documentation
	    "Text that is printed in the label")
	   (label-point
	    :type g:Point
	    :reader label-point
	    :documentation "Where the label-string is placed.")
	   (label-margin
	    :type az:Card16
	    :accessor label-margin
	    :initform 2
	    :documentation "How much blank space surrounds the string."))
  (:documentation
   "Labels are intended to be small diagram components that display a short
string (no line breaks)."))

;;;------------------------------------------------------------

(defmethod initialize-instance :after ((diagram Label) &rest inits)
  (declare (ignore inits))
  (setf (slot-value diagram 'label-point)
    (g:make-element (xlt:drawable-space (diagram-window diagram)))))

;;;------------------------------------------------------------

(defmethod diagram-name ((label Label))
  (format nil "Label ~a" (label-string label)))
 
;;;============================================================
;;; Layout
;;;============================================================

(defmethod label-minimum-width ((diagram Label))
  (+ (* 2 (label-margin diagram))
     (xlt:string-width (label-string diagram)
		       (xlib:gcontext-font (diagram-gcontext diagram)))))

(defmethod label-minimum-height ((diagram Label))
  (+ (* 2 (label-margin diagram))
     (xlt:string-height (label-string diagram)
			(xlib:gcontext-font (diagram-gcontext diagram)))))

(defmethod diagram-minimum-width ((diagram Label))
  (label-minimum-width diagram))

(defmethod diagram-minimum-height ((diagram Label))
  (label-minimum-height diagram))

;;;------------------------------------------------------------

(defmethod make-layout-constraints ((label Label) (solver Layout-Solver))
  (brk:minimum-size (layout-domain solver)
		    (diagram-minimum-width label)
		    (diagram-minimum-height label)
		    label))

;;;------------------------------------------------------------

(defmethod layout-diagram ((diagram Label) layout-options)

  "The label's rect is set by its parent. This method calculates
the string position and ensures that the pixmaps are the right size."
  
  (declare (type Label diagram)
	   (type List layout-options)
	   (ignore layout-options))

  (ensure-big-enough-pixmaps diagram)
  (let* ((string-point (label-point diagram))
	 (margin (label-margin diagram)))
    (declare (type g:Point string-point)
	     (type az:Int16 margin))
    ;; position the label with the label
    (setf (g:x string-point) margin)
    (setf (g:y string-point)
      (+ margin
	 (the az:Int16
	   (xlt:string-ascent
	    (label-string diagram)
	    (xlib:gcontext-font (diagram-gcontext diagram))))))
    (update-internal diagram nil)
    diagram))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod render-diagram ((diagram Label) drawable)
  (declare (type Label diagram)
	   (type xlib:Drawable drawable))
  (xlt:draw-string drawable (diagram-gcontext diagram)
		   (label-point diagram) (label-string diagram)))