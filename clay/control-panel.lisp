;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Control-Panel (Interactor-Diagram)
	  ((control-panel-1st-row
	    :type List ;; of Menu-Button
	    :accessor control-panel-1st-row
	    :initform ()
	    :documentation
	    "Buttons relating to the subject of the plot.")
	   (control-panel-2nd-row
	    :type List ;; of Menu-Button
	    :accessor control-panel-2nd-row
	    :initform ()
	    :documentation
	    "Buttons relating to the plot itself."))
  (:documentation "Used for control-panels."))

;;;------------------------------------------------------------
;;; Tree traversal
;;;------------------------------------------------------------

(defmethod diagram-children ((panel Control-Panel))
  (concatenate `List
    (control-panel-1st-row panel)
    (control-panel-2nd-row panel)))

;;;------------------------------------------------------------
;;; Creation
;;;------------------------------------------------------------

(defun make-control-panel (diagram)

  "Takes a list of 'menu items', each of which is a list whose
first item is a string, and whose second item is a function to be
applied to the third item in the list if the item is selected."
  
  (declare (type Diagram diagram)
	   (:returns (type Control-Panel)))
  
  (let* ((1st-row (subject-control-panel-items (diagram-subject diagram)))
	 (2nd-row (diagram-control-panel-items diagram))
	 (panel
	  (make-instance 'Control-Panel
	    :diagram-name (format nil "Control-Panel for ~a" diagram)
	    :diagram-parent (diagram-parent diagram))))
    (declare (type List 1st-row 2nd-row)
	     (type Control-Panel panel)
	     (:returns (type Control-Panel panel)))
    (build-diagram panel (list 1st-row 2nd-row))
    panel))

;;;------------------------------------------------------------
;;; Building
;;;------------------------------------------------------------

(defmethod build-diagram ((panel Control-Panel) rows)

  "Make buttons for the control-panel items."

  (declare (type Control-Panel panel)
	   (type List rows))

  (let* ((1st-row (first rows))
	 (2nd-row (second rows))
	 (buttons ()))
    (dolist (item 1st-row)
      (let ((button (make-labeled-button 'Menu-Button
					 panel
					 (menu-item-string item)
					 (menu-item-function item)
					 (menu-item-args item))))
	(push button buttons)))
    (setf (slot-value panel 'control-panel-1st-row) buttons)
    
    (setf buttons ())
    (dolist (item 2nd-row)
      (let ((button (make-labeled-button 'Menu-Button
					 panel
					 (menu-item-string item)
					 (menu-item-function item)
					 (menu-item-args item))))
	(push button buttons)))
    (setf (slot-value panel 'control-panel-2nd-row) buttons)

    (setf (diagram-layout-solver panel) (build-layout-solver panel))))

;;;------------------------------------------------------------
;;; Layout
;;;------------------------------------------------------------

(defmethod make-layout-constraints ((panel Control-Panel)
				    (solver Layout-Solver))
  (let ((domain (layout-domain solver))
	(1st-row (control-panel-1st-row panel))
	(2nd-row (control-panel-2nd-row panel)))
    (declare (type brk:Domain domain)
	     (type List 1st-row 2nd-row))
    (nconc (apply #'brk:surround domain panel 2 2 1st-row)
	   (apply #'brk:surround domain panel 2 2 2nd-row)
	   (apply #'brk:centered-row domain 4 1st-row)
	   (apply #'brk:centered-row domain 4 2nd-row)
	   (brk:above domain (first 1st-row) 2 (first 2nd-row))
	   (collect-from-children #'(lambda (child)
				      (make-layout-constraints child solver))
				  panel))))

(defmethod layout-diagram ((panel Control-Panel) layout-options)
  (declare (type List layout-options)
	   (ignore layout-options))
  (update-layout-solver panel (diagram-layout-solver panel))
  (solve-layout panel (diagram-layout-solver panel))
  (update-internal panel nil)
  (layout-children panel)
  panel)

;;;------------------------------------------------------------
;;; Drawing and Erasing
;;;------------------------------------------------------------
;;;------------------------------------------------------------
;;; Input
;;;------------------------------------------------------------






