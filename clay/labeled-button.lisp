;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================
;;; Labeled Buttons
;;;============================================================

(defclass Labeled-Button (Label Button) ()
  (:documentation
   "Labeled Buttons show the label string and also determine their size
so that the label fits."))

;;;------------------------------------------------------------

(defmethod diagram-name ((diagram Labeled-Button))
  (format nil "Labeled Button ~a" (label-string diagram)))
 
;;;-----------------------------------------------------------

(defun make-labeled-button (class parent string function args)
  (declare (type (or Symbol Class) class)
	   (type Diagram parent)
	   (type String string)
	   (type az:Funcallable function)
	   (type List args))
  
  (let ((button (make-instance class :diagram-parent parent)))
    (declare (type Labeled-Button button))
    (setf (label-string button) string)
    (setf (button-function button) function)
    (setf (button-args button) args)
    (ac:start-msg-handling button)
    button))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod label-margin ((diagram Labeled-Button))
  (+ (button-margin diagram) (slot-value diagram 'label-margin)))

(defmethod diagram-minimum-width ((diagram Labeled-Button))
  (max (button-minimum-width diagram)
       (label-minimum-width diagram)))

(defmethod diagram-minimum-height ((diagram Labeled-Button))
  (max (button-minimum-height diagram)
       (label-minimum-height diagram)))


(defmethod layout-diagram ((diagram Labeled-Button) layout-options)

  "The label's rect is set by its parent. This method calculates
the string position and ensures that the pixmaps are the right size."
  
  (declare (type Label diagram)
	   (type List layout-options)
	   (ignore layout-options))

  (ensure-big-enough-pixmaps diagram)
  (let* ((string-point (label-point diagram))
	 (margin (label-margin diagram)))
    (declare (type g:Point string-point)
	     (type az:Int16 margin))
    ;; position the label with the label
    (setf (g:x string-point) margin)
    (setf (g:y string-point)
      (+ margin
	 (the az:Int16
	   (xlt:string-ascent
	    (label-string diagram)
	    (xlib:gcontext-font (diagram-gcontext diagram))))))
    (update-internal diagram nil)
    diagram))
 
;;;============================================================
;;; Drawing
;;;============================================================

(defmethod draw-button-up-pixmap :after ((diagram Labeled-Button))
  (declare (type Labeled-Button diagram))
  (xlt:draw-string (button-up-pixmap diagram) (diagram-gcontext diagram)
		   (label-point diagram) (label-string diagram)))

;;;------------------------------------------------------------

(defmethod draw-button-down-pixmap :after ((diagram Labeled-Button))
  (declare (type Labeled-Button diagram))
  (xlt:draw-string (button-down-pixmap diagram) (diagram-gcontext diagram)
		   (label-point diagram) (label-string diagram)))

;;;------------------------------------------------------------

(defmethod draw-button-flat-pixmap :after ((diagram Labeled-Button))
  (declare (type Labeled-Button diagram))
  (xlt:draw-string (button-flat-pixmap diagram) (diagram-gcontext diagram)
		   (label-point diagram) (label-string diagram)))



