;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Interactor-Diagram (ac:Interactor Diagram)
	  ((diagram-painter
	    :type (or Null Painter)
	    :accessor diagram-painter
	    :initform nil))
 
  (:documentation
   "Interactor-Diagrams handle X input events."))

;;;------------------------------------------------------------

(defmethod ac:interactor-window ((diagram Interactor-Diagram))
  (diagram-window diagram))

;;;------------------------------------------------------------
;;; Initialization
;;;------------------------------------------------------------

(defmethod activate-diagram :after ((diagram Interactor-Diagram) mediator)  
  
  "After method makes sure msg handling gets started."

  (declare (type Interactor-Diagram diagram)
	   (type T #||(or Null Mediator)||# mediator)
	   (ignore mediator))
  
  (ac:start-msg-handling diagram))

;;;------------------------------------------------------------
;;; Building
;;;------------------------------------------------------------

(defmethod ac:build-roles ((diagram Interactor-Diagram))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Diagram-Role :role-actor diagram)))

;;;------------------------------------------------------------
;;; Layout
;;;------------------------------------------------------------

;;;------------------------------------------------------------
;;; Drawing
;;;------------------------------------------------------------

;;;------------------------------------------------------------
;;; Input
;;;------------------------------------------------------------

;;;------------------------------------------------------------
;;; Killing
;;;------------------------------------------------------------



