;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Layout-Solver (Clay-Object) 
	  ((layout-domain
	    :type (or Null brk:Domain)
	    :accessor layout-domain
	    :initform nil)
	   (layout-constraints
	    :type brk:Inequality-List
	    :accessor layout-constraints
	    :initform ())
	   (layout-cost
	    :type (or nil az:Funcallable brk:Functional)
	    :accessor layout-cost
	    :initform nil))
  (:documentation
   "The root Layout-Solver class solves diagram layouts specified 
as linear programming problems, using the Brick package."))

;;;============================================================

(defclass Diagram (Clay-Object)
	  ((diagram-name
	    :type (or String Symbol)
	    :reader diagram-name
	    :initarg :diagram-name)

	   (diagram-parent 
	    :type T ;; (or Null Diagram)
	    :accessor diagram-parent
	    :initarg :diagram-parent
	    :documentation
	    "This node's parent in the diagram tree.  A node's
children are represented by the <children> slots of a diagram object,
which may be different for different kinds of diagrams.")

	   ;; drawing surfaces
	   (diagram-image
	    :type (or Null xlib:Image-Z)
	    :accessor diagram-image
	    :initform nil
	    :documentation
	    "The diagram-image provides a place to render the diagram 
             off screen, and without talking to the X server.")

	   (diagram-pixmap
	    :type (or Null xlib:Pixmap)
	    :accessor diagram-pixmap
	    :initform nil
	    :documentation
	    "The diagram-pixmap provides a place to render the diagram 
             off screen, allowing it to be quickly bltted without flicker.")

	   (diagram-frames
	    :type List ;; of xlib:Pixmaps
	    :accessor diagram-frames
	    :initform ()
	    :documentation
	    "A list of pixmaps that can be used for recording and animation.")

	   (diagram-view-space
	    :type g:Flat-Space
	    :reader diagram-view-space
	    :initform (g:Rn 2)
	    :documentation
	    "The space in which the current view is described.")
	   (diagram-world-space
	    :type g:Flat-Space
	    :reader diagram-world-space
	    :initform (g:Rn 2)
	    :documentation
	    "The space in which the scene is described.")

	   ;; regions in various spaces
	   (diagram-view-rect ;; in view space
	    :type g:Rect
	    :accessor diagram-view-rect)
	   (diagram-image-rect ;; in image space
	    :type g:Rect
	    :reader diagram-image-rect)
	   (diagram-pixmap-rect ;; in window coords
	    :type g:Rect
	    :accessor diagram-pixmap-rect)
	   (diagram-window-rect ;; in window coords
	    :type g:Rect
	    :accessor diagram-window-rect)

	   ;; drawing styles
	   (diagram-gcontext 
	    :type (or Null xlib:Gcontext)
	    :initform nil)
	   (diagram-erasing-gcontext 
	    :type xlib:Gcontext
	    :initform nil)

	   (diagram-lens
	    :type Lens
	    :accessor diagram-lens
	    :initform (make-instance 'Lens)
	    :documentation
	    "The Lens represents the mapping from raw data to the diagram.")

	   ;; determining the layout
	   (diagram-layout-solver
	    :type (or Null Layout-Solver)
	    :accessor diagram-layout-solver
	    :initarg :diagram-layout-solver
	    :documentation "controls the layout of this diagram")

	   ;; interaction
	   (diagram-menu
	    :type T #||(or Null az:Dead-Object Menu)||#
	    ;; no-accessor, explicit caching reader method
	    :initform nil
	    :documentation
	    "This slot caches a menu of things to do to the diagram
             or its subject.")
	   (diagram-subject
	    :type T
	    :accessor diagram-subject
	    :initarg :diagram-subject
	    :documentation "what this is a diagram of")

	   (diagram-outdated?
	    :type az:Boolean
	    :accessor diagram-outdated?
	    :initform nil
	    :documentation
	    "If diagram-outdated? is t, then the diagram needs to be updated
             and redrawn.")

	   (diagram-selection
	    :type T ;; (or Null Interactor-Diagram)
	    :accessor diagram-selection
	    :initform nil
	    :documentation
	    "This is intended to hold one of the descendants of the diagram,
             the one that is currently selected, in the sense that
             certain input events ought to be forwarded to the selection.
             For example, a Diagram will by default set the selection
             on a ac:button-press event, and forward the ac:button-press
             and any subsequent ac:button-press, ac:motion-notify, and
             ac:key-press events to the selection. When receiving a 
             ac:button-release event, it will forward it to the selection,
             and set diagram-selection to nil.

             This could alternatively be handled by having a special role
             that forwards to a selection."))
  
  (:default-initargs
      :diagram-name (gentemp "DIAGRAM-")
    :diagram-parent nil
    :diagram-subject nil
    :diagram-layout-solver nil)
  (:documentation
   "A Diagram is an object that knows how to draw itself on a xlib:Window."))

;;;============================================================

(defmethod initialize-instance :after ((diagram Diagram) &rest inits)
  (declare (ignore inits))
  (setf (slot-value diagram 'diagram-window-rect)
    (g:make-rect (xlt:drawable-space (diagram-window diagram))))
  (setf (slot-value diagram 'diagram-view-rect)
    (g:make-rect (diagram-view-space diagram))))

;;;============================================================
;;; Generalized accessors
;;;============================================================

(defmethod (setf diagram-name) (name (diagram Diagram))
  (setf (slot-value diagram 'diagram-name) name)
  (let ((w (diagram-window diagram)))
    (declare (type xlib:Window w))
    (setf (xlib:wm-name w) name)
    (setf (xlib:wm-icon-name w) name)))

(defun diagram-display (diagram)
  "What xlib:Display is this diagram's window on?"
  (declare (type Diagram diagram))
  (xlib:drawable-display (diagram-window diagram)))

(defun diagram-root-window (diagram)
  "What is the root of this diagram's window?"
  (declare (type Diagram diagram))
  (xlt:drawable-root (diagram-window diagram)))

(defgeneric diagram-menu (diagram)
  #-sbcl (declare (type Diagram diagram)
	   (:returns (type Menu)))
  (:documentation
   "Returns the Menu associated with diagram. The menu is created
on the first call and cached for future calls."))

(defgeneric diagram-window (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "What window is the diagram drawn in?"))

(defmethod diagram-window ((diagram Diagram))
   "The default method asks the diagram's parent.
   Root-Diagram's have a diagram-window slot, which terminates the recursion."
   (diagram-window (diagram-parent diagram)))

(defgeneric diagram-gcontext (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "What gcontext is used for drawing the diagram?"))

(defmethod diagram-gcontext ((diagram Diagram))
   "If there's nothing in the local slot, try the diagram's parent.
Root-Diagram's have a diagram-window slot, which terminates the recursion."
   (or (slot-value diagram 'diagram-gcontext)
       (diagram-gcontext (diagram-parent diagram))))

(defgeneric (setf diagram-gcontext) (gcontext diagram)
  #-sbcl (declare (type xlib:Gcontext gcontext)
	   (type Diagram diagram))
  (:documentation
   "Set the gcontext used for drawing the diagram."))

(defmethod (setf diagram-gcontext) ((gcontext xlib:Gcontext) (diagram Diagram))
   (setf (slot-value diagram 'diagram-gcontext) gcontext))

(defgeneric diagram-erasing-gcontext (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "What gcontext is used for drawing the diagram?"))

(defmethod diagram-erasing-gcontext ((diagram Diagram))
  "If there's nothing in the local slot, try the diagram's parent.
   Root-Diagram's have a diagram-window slot, which terminates the recursion."
  (or (slot-value diagram 'diagram-erasing-gcontext)
      (diagram-erasing-gcontext (diagram-parent diagram))))

(defgeneric (setf diagram-erasing-gcontext) (gcontext diagram)
  #-sbcl (declare (type xlib:Gcontext gcontext)
	   (type Diagram diagram))
  (:documentation
   "Set the gcontext used for drawing the diagram."))

(defmethod (setf diagram-erasing-gcontext) ((gcontext xlib:Gcontext) 
					    (diagram Diagram))
  (setf (slot-value diagram 'diagram-erasing-gcontext) gcontext))

(defmethod (setf diagram-pixmap) :after ((pixmap xlib:Pixmap) 
					 (diagram Diagram))
  (let* ((r (g:make-rect (xlt:drawable-space pixmap))))
    (setf (slot-value diagram 'diagram-pixmap-rect) r)
    (setf (g:x r) 0)
    (setf (g:y r) 0)
    (setf (g:w r) (xlib:drawable-width pixmap))
    (setf (g:h r) (xlib:drawable-height pixmap))))

(defmethod (setf diagram-image) :after ((image xlib:Image) (diagram Diagram))
  (let* ((r (g:make-rect (xlt:image-space image))))
    (setf (slot-value diagram 'diagram-image-rect) r)
    (setf (g:x r) 0)
    (setf (g:y r) 0)
    (setf (g:w r) (xlib:image-width image))
    (setf (g:h r) (xlib:image-height image))))

;;;============================================================
;;; Traversing the diagram tree:
;;;============================================================

(defgeneric diagram-children (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "This should normally return a sequence holding the
children of the diagram"))

(defmethod diagram-children ((diagram Diagram))
  "Default is no children."
  ())

;;;------------------------------------------------------------
;;; Some "sequence" functions for a diagram's children:

(defgeneric map-over-children (result-type function diagram)
  #-sbcl (declare (type Symbol result-type)
	   (type az:Funcallable function)
	   ()(type Diagram diagram))
  (:documentation
   "Map the supplied <function> over the diagram's children
    returning the results in a sequence of <result-type>, 
    mapping for side-effect only if <result-type> is nil
    (just like the CL sequence functions).")
  (:method
   (result-type function (diagram Diagram))
   (if (null result-type)
       ;; then, assuming <dolist> is faster than <map>.
       (dolist (child (diagram-children diagram)) (funcall function child))
     ;; else
     (map result-type function (diagram-children diagram)))))

(defgeneric collect-from-children (function diagram)
  #-sbcl (declare (type az:Funcallable function)
	   (type Diagram diagram))
  (:documentation
   "This is to map-over-children as mapcan is to map.
    The <function> is applied to the diagram's children, and is expected
    to return a list as a result. The lists are concatenated together
    and returned as a single list.")
  (:method
   (function (diagram Diagram))
   (mapcan function (diagram-children diagram))))

(defgeneric some-child? (function diagram)
  #-sbcl (declare (type az:Funcallable function)
	   (type Diagram diagram))
  (:documentation
   "A version of <some> for a diagram's children.")
  (:method
   (function (diagram Diagram))
   (some function (diagram-children diagram))))

(defgeneric find-child-if (function diagram)
  #-sbcl (declare (type az:Funcallable function)
	   (type Diagram diagram))
  (:documentation
   "A version of <find-if> for a diagram's children.")
  (:method
   (function (diagram Diagram))
   (find-if function (diagram-children diagram))))

;;;------------------------------------------------------------

(defun diagram-root (diagram)
  "Return the root of the diagram tree containing <diagram>."
  #-sbcl (declare (type Diagram diagram))
  (if (diagram-parent diagram)
      (diagram-root (diagram-parent diagram))
    ;; else this must be the root
    diagram))

;;;============================================================
;;; Initializing
;;;============================================================

(defgeneric initialize-diagram (diagram
				&key 
				build-options
				layout-options
				mediator)

  #-sbcl (declare (type Diagram diagram)
	   (type List build-options layout-options)
	   (type T #||(or Null Mediator)||# mediator)
	   (:returns (type Diagram diagram)))

  (:documentation
   "<initialize-diagram> does everything necessary to put a
    diagram up on the screen, ready for action."))

(defmethod initialize-diagram ((diagram Diagram)
			       &key
			       build-options
			       layout-options
			       mediator)

  "Default method just initializes the children; assumes building
   and layout taken care of."

  #-sbcl (declare (type Diagram diagram)
	   (type List build-options layout-options)
	   (type T #||(or Null Mediator)||# mediator))

  (map-over-children nil #'(lambda (child)
			     (initialize-diagram
			      child
			      :build-options build-options
			      :layout-options layout-options
			      :mediator mediator))
		     diagram)
  diagram)

;;;============================================================
;;; Building
;;;============================================================

(defgeneric build-diagram (diagram build-options)
  #-sbcl (declare (type Diagram diagram)
	   (type List build-options))
  (:documentation
   "<build-diagram> allocates the diagram-children and other data
structures needed to represent the diagram, and sets pointers to
connect them appropriately, but, in general, doesn't otherwise
initialize the diagram structure. In particular, it leaves any
initialization related to position and appearance to <layout-diagram>
and any intitialization related to automatic maintainance of
dependencies, handling input, etc., to <activate-diagram> ???

The method for <build-diagram> is responsible for filling the 'local'
slots of the diagram.  It then calls <build-children>, which
recursively the diagram-children of this node in the diagram tree.
<build-children> does not take any additional arguments, which implies
that the 'local' slots of diagram contain all the information
necessary for building the children.  <build-children> is required to
set the <parent> slot in the children to <diagram> and set the various
'child' slots in <diagram> to the appropriate child diagram object. "))

(defmethod build-diagram ((diagram Diagram) build-options)
  #-sbcl (declare (ignore build-options))
  "Default is to do nothing."
  t)


(defgeneric build-children (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation 	 

   "<build-children> builds the diagram's descendents, recursively.
pointers to connect them.  <build-children> does not take any
additional arguments, which implies that the 'local' slots of diagram
contain all the information necessary for building the children.
<build-children> is required to set the <parent> slot in the children
to <diagram> and set the various child slots in <diagram> to the
appropriate child diagram object. "))

;;;============================================================
;;; Layout
;;;============================================================

(defgeneric layout-solver-class (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "The default layout solver class used by this type of diagram."))

(defmethod layout-solver-class ((diagram Diagram)) 'Layout-Solver)

(defgeneric build-layout-solver (diagram)
  #-sbcl (declare (type Diagram diagram)
	   (:returns (type (or Null Layout-Solver))))
  (:documentation
   "Construct the layout solver for this diagram."))

(defmethod build-layout-solver ((diagram Diagram))
  (declare (type Diagram diagram)
	   (:returns (type (or Null Layout-Solver))))
  (make-instance (layout-solver-class diagram)))

;;;============================================================

(defgeneric update-layout-solver (diagram solver)
  #-sbcl (declare (type Diagram diagram)
	   (type Layout-Solver solver))
  (:documentation
   "Make sure the layout solver is ready for computing a layout."))

(defmethod update-layout-solver ((diagram Diagram) (solver Layout-Solver))
  (declare (type Diagram diagram)
	   (type Layout-Solver solver))
  (setf (layout-domain solver) (make-layout-domain diagram solver))
  (setf (layout-cost solver) (make-layout-cost diagram solver))
  (setf (layout-constraints solver) (make-layout-constraints diagram solver)))

;;;============================================================
;;; Using Diagram's as brk:Brick's

(defmethod brk:left ((diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (g:x (diagram-window-rect diagram)))

(defmethod (setf brk:left) (v (diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (declare (type Real v))
  (setf (g:x (diagram-window-rect diagram)) (round v)))

(defmethod brk:width ((diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (g:w (diagram-window-rect diagram)))

(defmethod (setf brk:width) (v (diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (declare (type Real v))
  (setf (g:w (diagram-window-rect diagram)) (round v)))

(defmethod brk:top ((diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (g:y (diagram-window-rect diagram)))

(defmethod (setf brk:top) (v (diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (declare (type Real v))
  (setf (g:y (diagram-window-rect diagram)) (round v)))

(defmethod brk:height ((diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (g:h (diagram-window-rect diagram)))

(defmethod (setf brk:height) (v (diagram Diagram))
  "This method is necessary to let us use Diagrams as brk:Bricks,
so that we can use the Brick package for layout."
  (declare (type Real v))
  (setf (g:h (diagram-window-rect diagram)) (round v)))

;;;------------------------------------------------------------

(defgeneric diagram-bricks (diagram)
  #-sbcl (declare (type Diagram diagram)
	   (:returns (type brk:Brick-List)))
  (:documentation
   "This returns a list of the bricks which determine the layout
domain of a root diagram, and the contribution to the root's layout
domain from any of its components. The default method recurses of the
diagram-children.  the recursion terminates at leaf-diagrams, whose
default method is to jsut return a list containing the leaf itself.
The reason this is a generic function, rather than just the described
recursive function, is that some diagrams may want to add extra bricks
to their layout domain --- the extra bricks corresponding typically to
a group of diagrams rather than a single diagram, for example, a
column in some table."))

(defmethod diagram-bricks ((diagram Diagram))
  (cons diagram (mapcan #'diagram-bricks (diagram-children diagram))))

;;;------------------------------------------------------------

(defgeneric diagram-minimum-width (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation "What's the mimnimum width allowed for this diagram?"))

(defmethod diagram-minimum-width ((diagram Diagram)) 1)

(defgeneric diagram-minimum-height (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation "What's the mimnimum height allowed for this diagram?"))

(defmethod diagram-minimum-height ((diagram Diagram)) 1)

(defgeneric diagram-minimum-extent (diagram &key result)
  #-sbcl (declare (type Diagram diagram)
	   (type g:Point result))
  (:documentation
   "Fills result with the minimum width-height extent vector
required by this diagram."))

(defmethod diagram-minimum-extent ((diagram Diagram)
				   &key
				   (result
				    (g:make-element
				     (xlt:drawable-translation-space
				      (diagram-window diagram)))))
  (setf (g:x result) (diagram-minimum-width diagram))
  (setf (g:y result) (diagram-minimum-height diagram))
  result)

;;;------------------------------------------------------------

(defun diagram-root-xy (diagram x y)
  "Return a new x y so that would approximately center the diagram 
over the given x y in the root window,"
  (declare (type az:Int16 x y)
	   (type Diagram diagram)
	   (:returns (values (type az:Int16 root-x)
			     (type az:Int16 root-y))))
  (let* ((root (xlt:drawable-root (diagram-window diagram)))
	 (xmin (xlib:drawable-x root))
	 (xmax (- (+ xmin (xlib:drawable-width root))
		  (brk:width diagram)))
	 (ymin (xlib:drawable-y root))
	 (ymax (- (+ ymin (xlib:drawable-height root))
		  (brk:height diagram))))
    (declare (type az:Int16 xmin xmax ymin ymax))
    (values (az:bound xmin (- x (truncate (brk:width diagram) 2)) xmax)
	    (az:bound ymin (- y (truncate (brk:height diagram) 2)) ymax))))

;;;------------------------------------------------------------

(defgeneric layout-diagram (diagram layout-options)
  #-sbcl (declare (type Diagram diagram)
	   (type List layout-options))
  (:documentation
   "<layout-diagram> analyzes the diagram, its children, and possibly
external objects that are referenced by the local slots --- like the
diagram-subject of a diagram --- in order to compute positions, extents, pens,
and any other parameters that determine to appearance of the diagram.
After <layout-diagram> returns, the diagram must be ready to be drawn
with a call to <draw-diagram>.  It's up to the designer of the diagram
to determine exactly what is computed at layout time and what at draw
time.

The default method for <layout-diagram> calls <solve-layout>,
if there is a diagram-layout-solver.."))

(defmethod layout-diagram ((diagram Diagram) layout-options)
  (declare (ignore layout-options))
  (error "~&No layout-diagram method defined for ~a.~%" diagram))

;;;------------------------------------------------------------

(defgeneric layout-children (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "Tells diagram to layout it's children.")
  (:method
   ((diagram Diagram))
   "Default method simply applies layout-diagram to children."
   (map-over-children nil
		      #'(lambda (child) (layout-diagram child nil))
		      diagram)))

;;;------------------------------------------------------------
;;; Handling :configure-notify msgs
;;;------------------------------------------------------------

(defmethod reconfigure-diagram ((diagram Diagram) width height)
  (declare (type az:Card16 width height))
  (unless (and (= width (brk:width diagram))
	       (= height (brk:height diagram)))
    (layout-diagram diagram nil)
    (ac:send-msg 'role-redraw-diagram diagram diagram (ac:new-id))))

;;;============================================================
;;; Drawing
;;;============================================================

(defgeneric diagram-update-pixmaps (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "Redraw (resizing if necessary) the diagram's pixmaps, if any."))

(defmethod diagram-update-pixmaps ((diagram Diagram))
  "By default, do nothing."
  t)

(defgeneric update-internal (diagram stroke)
  #-sbcl (declare (type Diagram diagram)
	   (type T stroke))
  (:documentation
   "<update-internal> is supposed to ensure that a diagram's internal
data structures (as opposed to its appearance) are up to date. The 
<stroke> is an object that may contain information about what has changed."))

(defmethod update-internal :before ((diagram Diagram) (stroke T))
  "Any recording will be invalidated by updating."
  (declare (type Diagram diagram)
	   (type T stroke))
  (erase-record diagram))

(defmethod update-internal ((diagram Diagram) (stroke T))
   "The default method for <update-internal> does nothing."
   (declare (optimize (safety 0) (speed 3))
	    (type Diagram diagram)
	    (type T stroke))
   t)

(defmethod update-internal :after ((diagram Diagram) options)
  (declare (ignore options))
  (diagram-update-pixmaps diagram))

;;;============================================================

(defgeneric render-diagram (diagram drawable)
  #-sbcl (declare (type Diagram diagram)
	   (type xlib:Drawable drawable))
  (:documentation "Render <diagram> onto <drawable>"))

;;;============================================================

(defgeneric draw-diagram (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation "Make the diagram visible on its window."))

(defmethod draw-diagram ((diagram Diagram))
  (map-over-children nil #'draw-diagram diagram))

(defmethod draw-diagram :after ((diagram Diagram))
  (xlt:drawable-force-output (diagram-window diagram)))

;;;------------------------------------------------------------

(defgeneric erase-diagram (diagram rect)
  #-sbcl (declare (type Diagram diagram)
	   (type (or Null g:Rect) rect))
  (:documentation
   "Erase-diagram assumes the internal state of the diagram is
consistent with what's on the screen, so that erasing may be
implemented in a way thats more efficient than wiping the screen.
Erase-diagram now returns a list of damaged rects; we might
want to just return (diagram-window-rect diagram) as a cheap conservative
alternative." ))

(defmethod erase-diagram ((diagram Diagram) rect)
  (declare (type Diagram diagram)
	   (type (or Null g:Rect) rect))
  (map-over-children 'List
		     #'(lambda (child) (erase-diagram child nil))
		     diagram)
  (unless (null rect)
    (az:copy (diagram-window-rect diagram) :result rect)))

(defgeneric redraw-diagram (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "Redrawing means erasing and then drawing. We don't assume that 
the internal state reflects the current picture, so we erase by
completely wiping diagram-window-rect"))

(defmethod redraw-diagram ((diagram Diagram))
  (xlt:clear-drawable (diagram-window diagram)
		      :rect (diagram-window-rect diagram))
  (draw-diagram diagram))

;;;============================================================

(defgeneric spin-diagram (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "Do one circuit of rotation, for diagrams for which it makes sense.
Otherwise do nothing, but don't complain."))

(defmethod spin-diagram ((diagram Diagram))
  "Spinning does nothing by default."
  (declare (type Diagram diagram))
  t)

;;;------------------------------------------------------------

(defgeneric move-diagram-to-xy (diagram x y)
  #-sbcl (declare (type Diagram diagram)
	   (type az:Int16 x y)))


(defgeneric move-diagram-to (diagram pos)
  #-sbcl (declare (type Diagram diagram)
	   (type g:Point pos))
  (:method
   ((diagram Diagram) pos)
   (move-diagram-to-xy diagram (g:x pos) (g:y pos))))


;;;------------------------------------------------------------

(defmethod print-object (stream (diagram Diagram))
  (az:printing-random-thing
   (diagram stream)
   (format stream "~:(~a~) ~a"
	   (class-name (class-of diagram))
	   (diagram-name diagram))))

;;;============================================================

(defgeneric record-diagram (diagram)
  #-sbcl (declare (type Diagram diagram))
  
  (:method
   ((diagram Diagram))
   (let* ((win (diagram-window diagram))
	  (w (xlib:drawable-width win))
	  (h (xlib:drawable-height win))
	  (d (xlt:drawable-depth win))
	  (pixmap
	   (xlib:create-pixmap :drawable win :depth d :width w :height h)))
     (declare (type xlib:Window win)
	      (type xlib:Card16 w h)
	      (type xlib:Card8 d)
	      (type xlib:Pixmap pixmap))
     (xlib:copy-area win (diagram-gcontext diagram) 0 0 w h pixmap 0 0)
     (push pixmap (diagram-frames diagram)))))

(defgeneric playback-diagram (diagram)
  #-sbcl (declare (type Diagram diagram))
  
  (:method
   ((diagram Diagram))
   (let* ((win (diagram-window diagram))
	  (gc (diagram-gcontext diagram))
	  (w (xlib:drawable-width win))
	  (h (xlib:drawable-height win)))
     (declare (type xlib:Window win)
	      (type xlib:Card16 w h))
     (dolist (frame (diagram-frames diagram))
       (xlib:copy-area frame gc 0 0 w h win 0 0)
       (xlt:drawable-force-output win)))))

(defgeneric erase-record (diagram)
  #-sbcl (declare (type Diagram diagram))

  (:method
   ((diagram Diagram))
   (let ((frames (diagram-frames diagram)))
     (setf (diagram-frames diagram) ())
     (dolist (frame frames) (az:kill-object frame)))))

;;;============================================================
;;; Input
;;;============================================================

(defgeneric activate-diagram (diagram mediator)
  #-sbcl (declare (type Diagram diagram)
	   (type (or Null Mediator) mediator))
  (:documentation
   "Start the dynamic behavior of the diagram."))

(defmethod activate-diagram ((diagram Diagram) mediator)
  "By default just activate the children."
  (declare (type T #||(or Null Mediator)||# mediator))
  (activate-children diagram mediator))

;;;------------------------------------------------------------

(defgeneric activate-children (diagram mediator)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "Start the dynamic behavior of the children of the diagram."))

(defmethod activate-children ((diagram Diagram) mediator)
  "By default just activate the children."
  (map-over-children
   nil
   #'(lambda (child) (activate-diagram child mediator))
   diagram))

;;;------------------------------------------------------------

(defgeneric find-diagram-under-xy (diagram x y)
  #-sbcl (declare (type Diagram diagram)
	   (type az:Int16 x y))
  (:documentation
   "Find a component diagram that is under <x,y>."))

(defmethod find-diagram-under-xy ((diagram Diagram) x y)
  "The default method iterates over the children
and returns the first one under <x,y>. If no child is under xy
and the root is, then retrun the root. Else return nil."
  (g:with-borrowed-element (p (xlt:drawable-space (diagram-window diagram)))
    (setf (g:x p) x)
    (setf (g:y p) y)
    (block :find
      (dolist (child (diagram-children diagram))
	(when (g:intersect? p (diagram-window-rect child))
	  (return-from :find child)))
      ;; if we're not over a child, return the root itself
      (when (g:intersect? p (diagram-window-rect diagram))
	diagram))))
 
;;;============================================================
;;; Geometric calculations

(defgeneric inside-diagram? (diagram pos)
  #-sbcl (declare (type Diagram diagram)
	   (type g:Point pos)
	   (:returns (type az:Boolean))))

(defmethod inside-diagram? ((diagram Diagram) pos)
  (declare (type Diagram diagram)
	   (type g:Point pos)
	   (:returns (type az:Boolean)))
  (g:intersect? pos (diagram-window-rect diagram)))

(defgeneric outside-diagram? (diagram pos)
  #-sbcl (declare (type Diagram diagram)
	   (type g:Point pos)
	   (:returns (type az:Boolean))))

(defmethod outside-diagram? ((diagram Diagram) pos)
  (declare (type Diagram diagram)
	   (type g:Point pos)
	   (:returns (type az:Boolean)))
  (not (inside-diagram? pos diagram)))

(defgeneric intersect-diagram? (diagram r)
  #-sbcl (declare (type Diagram diagram)
	   (type g:Rect r)
	   (:returns (type az:Boolean))))

(defmethod intersect-diagram? ((diagram Diagram) r)
  (declare (type Diagram diagram)
	   (type g:Rect r)
	   (:returns (type az:Boolean)))
  (g:intersect? r (diagram-window-rect diagram)))

;;;------------------------------------------------------------

(defgeneric clip-to-diagram! (diagram wr)
  #-sbcl (declare (type Diagram diagram)
	   (type g:Rect wr)
	   (:returns (type g:Rect dr)))
  (:documentation
   "Destructively modifies the coordinates of <wr> 
and creates and returns <dr>.

<wr> is initially the brush region, in window coordinates.

<dr> defines what is actually painted.  <dr> is computed by
transforming <wr> to diagram coordinates and clipping appropriately.
The default is to clip by intersecting <dr> with the <diagram-image-rect>.
If <dr> does not interset the <diagram-image-rect>, then its width and
height are set to 0 and top and left are unspecified.

<wr> is modified to be the inverse image (in window coordinates) of
<dr>, so that drawing <wr> in the window will accurately reflect what
has been painted."))

(defmethod clip-to-diagram! ((diagram Diagram) wr)
  (declare (type Diagram diagram)
	   (type g:Rect wr)
	   (:returns (type g:Rect ir)))
  ;; <g:intersect2> returns nil for an empty intersection,
  ;; in which case we just set the extent of <r> to be <0,0>.
  ;; transform twice to make sure truncation is handled consistently
  (let* ((w->p (window->pixmap (diagram-lens diagram)))
	 (p->i (pixmap->image (diagram-lens diagram)))
	 (ir (g:make-rect (g:codomain p->i))))
    (g:transform p->i (g:transform w->p wr) :result ir)
    (unless (g:intersect2 (diagram-image-rect diagram) ir :result ir)
      (setf (g:w ir) 0)
      (setf (g:h ir) 0))
    (g:inverse-transform w->p (g:inverse-transform p->i ir) :result wr)
    ir))

;;;------------------------------------------------------------

(defgeneric translate-children (diagram)
  #-sbcl (declare (type Diagram diagram))
  (:documentation
   "Assuming the children's diagram-window-rects are expressed
in the diagram`s coordinate system, tranform them so they are in
window coordinates."))

(defmethod translate-children ((diagram Diagram))
  "This assumes that (diagram-window-rect diagram) is already 
in window coordinates."
  (let ((dx (brk:left diagram))
	(dy (brk:top diagram)))
    (declare (type az:Int16 dx dy))
    (map-over-children nil
		       #'(lambda (child)
			   (incf (brk:left child) dx)
			   (incf (brk:top child) dy))
		       diagram)))

;;;------------------------------------------------------------

(defgeneric repair-diagram (diagram r)
  #-sbcl (declare (type Diagram diagram)
	   (type g:Rect r)))

(defmethod repair-diagram ((diagram Diagram) damaged-rect)
  ;;(check-type damaged-rect g:Rect)
  (when (intersect-diagram? diagram damaged-rect)
    (map-over-children
     nil #'(lambda (child) (repair-diagram child damaged-rect)) diagram)))

;;;------------------------------------------------------------

(defun mouse-down? (window)
  (multiple-value-bind (x y same-screen-p child mask root-x root-y root)
	(xlib:query-pointer window)
      (declare (ignore x y same-screen-p child root-x root-y root))
      (logtest mask (xlib::make-state-mask :button-1))))

(defgeneric drag-diagram (diagram)
  #-sbcl (declare (type Diagram diagram)))

(defmethod drag-diagram ((diagram Diagram))
  (let ((window (diagram-window diagram))
	(xc 0) (yc 0)
	(xo 0) (yo 0)
	(xl 0) (yl 0)
	(dx 0) (dy 0))
    (declare (type az:Int16 xc yc xo yo xl yl dx dy))
    (multiple-value-setq (xc yc) (xlib:query-pointer window))
    (setf xo (brk:left diagram))
    (setf yo (brk:top diagram))
    (setf dx (- xo xc))
    (setf dy (- yo yc))
    ;; first wait until mouse goes down on the window
    (loop (when (mouse-down? window) (return)))
    (loop (unless (mouse-down? window) (return))
      (setf xl xc)
      (setf yl yc)
      (multiple-value-setq (xc yc) (xlib:query-pointer window))
      (unless (and (= xc xl) (= yc yl))
	(setf xo (+ xc dx))
	(setf yo (+ yc dy))
	(move-diagram-to-xy diagram xc yc)))))

;;;============================================================
;;; Presentation stuff
;;;============================================================

(defgeneric make-diagram-for (diagram-subject)
  ;; In general, diagram-subject can be anything.
  ;; Methods return a diagram.
  #-sbcl (declare (type T diagram-subject)))

(defgeneric default-diagram-class (diagram-subject)
  #-sbcl (declare (type T diagram-subject)))

;;;------------------------------------------------------------
;;; Find the descendent of <pres> whose diagram-subject is <subj>

(defgeneric find-diagram-of (subj root)
  #-sbcl (declare (type T subj)
	   (type Diagram root)))

(defmethod find-diagram-of (subj (root Diagram))
  (catch :search-done
    (%find-diagram-of subj root)))

(defgeneric %find-diagram-of (subj root)
  #-sbcl (declare (type T subj)
	   (type Diagram root)))

(defmethod %find-diagram-of (subj (root Diagram))
  (if (eql subj (diagram-subject root))
      (throw :search-done root)
      ;; else
      (map-over-children
	nil
	#'(lambda (child) (%find-diagram-of subj child))
	root)))

;;;------------------------------------------------------------

(defgeneric subject-diagrams (subject)
  #-sbcl (declare (type T subject))
  (:documentation
   "<subject-diagrams> returns a list of the active
 diagrams for <subject> and, with <setf>, modifies
 that list. The default method uses a hash table to map subjects
 to diagrams; this is intended for primitive Lisp types.
 More specialized diagram-subject types should keep track of their 
 diagrams in some more efficient way.

 <subject-diagrams> returns a list of the active
 diagrams for <subject> and, with <setf>, modifies
 that list. The default method uses a hash table to map subjects
 to diagrams; this is intended for primitive Lisp types.
 More specialized diagram-subject types should keep track of their 
 diagrams in some more efficient way."))

(defgeneric (setf subject-diagrams) (diagrams subject)
  #-sbcl (declare (type List diagrams)
	   ;; (type (List Diagram) diagrams)
	   (type T subject)))

(defparameter *subject-diagrams-table* (make-hash-table :test #'eq))

(defmethod subject-diagrams (subject)
  (az:lookup subject *subject-diagrams-table* ()))

(defmethod (setf subject-diagrams) (diagrams subject)
  (setf (az:lookup subject *subject-diagrams-table* nil) diagrams))

;;;------------------------------------------------------------

(defparameter *subject-gcontext-table* (make-hash-table :test #'eq))

(defun subject-gcontext (subject)

  "Return the gcontext associated with <subject>. The idea is that the
 appearance of any diagram should always reflect the state of
 its subject, normally by drawing itself with the subject's gcontext. The
 default method looks up <subject> in <*subject-gcontext-table*>,
 returning a default gcontext if it's not there.
 Return the gcontext associated with <subject>. The idea is that the
 appearance of any diagram should always reflect the state of
 its subject, normally by drawing itself with the subject's gcontext. The
 default method looks up <subject> in <*subject-gcontext-table*>,
 returning a default gcontext if it's not there."

  (az:lookup subject *subject-gcontext-table* nil))

;;;============================================================
;;; Destroying
;;;============================================================

(defmethod az:kill-object :before ((diagram Diagram))
  (let ((menu (slot-value diagram 'diagram-menu)))
    (when menu
      (setf (slot-value diagram 'diagram-menu) nil)
      (ac:send-msg 'ac:destroy-request diagram menu (ac:new-id) 
		   (diagram-window menu))))
  (az:kill-object (slot-value diagram 'diagram-pixmap))
  (values))

(defmethod az:kill-object ((diagram Diagram))
  (map-over-children nil #'az:kill-object diagram)
  (change-class diagram `az:Dead-Object)
  (values))

;;;============================================================
