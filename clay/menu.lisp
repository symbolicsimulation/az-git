;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defun do-nothing (&rest args)
  (declare (ignore args))
  t)

;;;============================================================

(defclass Menu-Button (Labeled-Button) ()
  (:documentation
   "Menu-Buttons do something when they receive an ac:button-press
event. They respond immediately and only to ac:button-press,
without waiting for ac:button-release."))

;;;-----------------------------------------------------------
;;; Building
;;;-----------------------------------------------------------

(defmethod ac:build-roles ((diagram Menu-Button))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Menu-Button-Role :role-actor diagram)))

;;;============================================================

(defclass Menu-Button-Role (Button-Role) ()
  (:documentation
   "A Menu-Button-Role handles button-press events forwarded 
by the Menu-Diagram."))

;;;-----------------------------------------------------------

(defmethod ac:button-press ((role Menu-Button-Role) 
			    msg-name sender receiver id
			    code x y state time root root-x root-y child 
			    same-screen-p)
  "An ac:button-press event on a Menu-Button causes (1) the button
to go :down, (2) the menu-button-action to be funcalled, and (3) the 
button  to go :up."
  (declare (type Button-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Menu-Button receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id 
		   code x y state time root root-x root-y child same-screen-p))
  (setf (button-state receiver) :down)
  (apply (button-function receiver) (button-args receiver))
  (setf (button-state receiver) :up)
  (an:announce receiver :pressed)
  (values t))

;;;============================================================

(defclass Menu (Root-Diagram)
	  ((menu-buttons
	    :type List ;; of Menu-Button
	    :reader menu-buttons
	    :initform ()))
  (:documentation
   "Used for menus."))

;;;------------------------------------------------------------
;;; Tree traversal
;;;------------------------------------------------------------

(defmethod diagram-children ((menu Menu)) (menu-buttons menu))

;;;------------------------------------------------------------
;;; Creation
;;;------------------------------------------------------------

(defun make-menu (diagram)

  "Takes a list of 'menu items', each of which is a list whose
first item is a string, and whose second item is a function to be
applied to the third item in the list if the item is selected.
The menu's is not automatically mapped. To make the menu visible
you need to call <show-diagram>."
  
  (declare (type Diagram diagram)
	   (:returns (type Menu)))
  (let* ((menu-items (diagram-menu-items diagram))
	 (root (xlt:drawable-root (diagram-window diagram)))
	 (window (xlt:make-window
		  :parent root
		  :colormap (xlib:window-colormap (diagram-window diagram))
		  :map? nil))
	 (menu (make-instance 'Menu
		 :diagram-name (format nil "Menu for ~a" diagram)
		 :diagram-window window)))
    (declare (type List menu-items)
	     (type xlib:Window window)
	     (type Menu menu)
	     (:returns (type Menu menu)))
    (initialize-diagram menu :build-options menu-items)
    menu))

(defmethod diagram-menu ((diagram Diagram))
  (declare (type Diagram diagram)
	   (:returns (type Menu)))
  (when (or (null (slot-value diagram 'diagram-menu))
	    (typep (slot-value diagram 'diagram-menu) `az:Dead-Object))
    (setf (slot-value diagram 'diagram-menu) (make-menu diagram)))
  (slot-value diagram 'diagram-menu))

(defun diagram-show-menu (Diagram x y)
  "Put up the diagram's menu close to the point x,y 
in the diagram's coordinate system."
  (declare (type Diagram diagram)
	   (type az:Int16 x y))
  (multiple-value-bind (root-x root-y)
      (xlib:translate-coordinates (diagram-window diagram) x y
				  (xlt:drawable-root (diagram-window diagram)))
    (show-diagram (diagram-menu diagram) :x root-x :y root-y)))

;;;------------------------------------------------------------
;;; Building
;;;------------------------------------------------------------

(defmethod build-diagram ((menu Menu) menu-items)
  "Make buttons for the menu items."
  (declare (type Menu menu)
	   (type List menu-items))
  (let ((buttons ()))
    (dolist (item menu-items)
      (let ((button (make-labeled-button 'Menu-Button
					 menu
					 (menu-item-string item)
					 (menu-item-function item)
					 (menu-item-args item))))
	(push button buttons)
	(an:join-audience button :pressed menu 'role-hide-diagram)))
    (setf (slot-value menu 'menu-buttons) (nreverse buttons)))
  (setf (diagram-layout-solver menu) (build-layout-solver menu)))

;;;------------------------------------------------------------
;;; Layout
;;;------------------------------------------------------------

(defmethod reconfigure-diagram ((menu Menu) w h)
  (declare (ignore w h))
  t)

(defmethod make-layout-constraints ((menu Menu) (solver Layout-Solver))
  (let ((domain (layout-domain solver))
	(buttons (diagram-children menu)))
    (declare (type brk:Domain domain)
	     (type List buttons))
    (nconc (apply #'brk:surround domain menu 2 2 buttons)
	   (apply #'brk:centered-column domain 4 buttons)
	   (apply #'brk:equal-size domain buttons)
	   (collect-from-children #'(lambda (child)
				      (make-layout-constraints child solver))
				  menu))))

;;;------------------------------------------------------------
;;; Drawing and Erasing
;;;------------------------------------------------------------
;;;------------------------------------------------------------
;;; Input
;;;------------------------------------------------------------






