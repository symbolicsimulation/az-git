;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Lens (Clay-Object)
	  (;; mappings and related parameters
	   (scale
	    :type (or Null az:Positive-Real)
	    :accessor scale
	    :initarg :scale
	    :documentation
	    "Used by scalable diagrams. 
             If a positive number, the diagram is drawn at this many
             screen pixels per image pixel and the window size is adjusted
             to fit. If null, the diagram is drawn at the largest magnification
             that will fit in the window.")
	   (window->pixmap
	    :type (or Null g:Int-Grid-Map)
	    :accessor window->pixmap
	    :initform nil
	    :documentation
	    "Transforms window coordinates to the diagram's pixmap coords
             (by translation only).")
	   (pixmap->image
	    :type (or Null g:Int-Grid-Map)
	    :accessor pixmap->image
	    :initform nil
	    :documentation
	    "Transforms pixmap coordinates to the diagram's image coords.
             Usually, this is only a scaling, but sometimes it will
             involve a translation.")
	   (pixmap->view
	    :type (or Null g:Int-Float-Flip)
	    :accessor pixmap->view
	    :initform nil
	    :documentation
	    "Transforms pixmap coordinates to the diagram's view coords.
             Usually, this is only a scaling, but sometimes it will
             involve a translation."))
  (:default-initargs :scale nil)
  (:documentation
   "The Lens represents the mapping from raw data to the diagram."))