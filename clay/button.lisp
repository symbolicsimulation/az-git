;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
;;;============================================================

(defclass Button (Interactor-Leaf) 
	  ((button-state
	    :type (Member :up :down :flat)
	    :reader button-state
	    :initform :up
	    :documentation
	    "Buttons are either up or down or flat (inactive).")
	   (button-hue-sat-name
	    :type xlt:Hue-Sat-Name
	    :accessor button-hue-sat-name
	    :initform :gray
	    :documentation
	    "The button is drawn using varying brightness levels of this 
             combination of hue and saturation.")
	   (button-minimum-width
	    :type az:Card16
	    :accessor button-minimum-width
	    :initform 16
	    :documentation
	    "The button will be at least this wide.")
	   (button-minimum-height
	    :type az:Card16
	    :accessor button-minimum-height
	    :initform 16
	    :documentation
	    "The button will be at least this high.")
	   (button-margin
	    :type az:Card16
	    :accessor button-margin
	    :initform 4
	    :documentation
	    "How much shaded (for pseudo 3d) space surrounds the string." )
	   (button-up-pixmap
	    :type (or Null xlib:Pixmap)
	    :initform nil
	    :accessor button-up-pixmap
	    :documentation
	    "A cached image of the button in the up position.
             This is the usual appearance of the button.")
	   (button-down-pixmap
	    :type (or Null xlib:Pixmap)
	    :initform nil
	    :accessor button-down-pixmap
	    :documentation
	    "A cached image of the button in the down position.
             This is blt to the screen when the button is pressed.")
	   (button-flat-pixmap
	    :type (or Null xlib:Pixmap)
	    :initform nil
	    :accessor button-flat-pixmap
	    :documentation
	    "A cached image of the button in the flat position.
             The button is shown in the flat position when it's
             inactive for some reason.")
	   (button-function
	    :type az:Funcallable
	    :accessor button-function
	    :initform 'do-nothing
	    :documentation
	    "Something to apply to the button-args when the button is
             pressed.")
	   (button-args
	    :type List
	    :accessor button-args
	    :initform ()
	    :documentation
	    "Something to apply the button-function to when the button is
             pressed."))
  (:documentation
   "Buttons are intended to be small diagram components that respond to mouse
clicks and possibly other input. The sensitive region for any
button should be displayed to the user by drawing it in a pseudo-3d style.
Temporarily inactive buttons can be drawn flat."))

;;;------------------------------------------------------------

(defmethod diagram-name ((button Button))
  (format nil "~a" button))
 
;;;------------------------------------------------------------

(defmethod diagram-pixmap ((button Button))
  (declare (type Button button))
  (ecase (button-state button)
    (:up (button-up-pixmap button))
    (:down (button-down-pixmap button))
    (:flat (button-flat-pixmap button))))

(defmethod (setf diagram-pixmap) (new-pixmap (button Button))
  (declare (ignore new-pixmap))
  (error "Can't set pixmap of ~a" button))

;;;------------------------------------------------------------

(defmethod (setf button-state) (state (button Button))
  (declare (type (Member :up :down :flat) state))
  (setf (slot-value button 'button-state) state)
  (draw-diagram button)
  (xlt:drawable-finish-output (diagram-window button)))

;;;============================================================
;;; Building
;;;============================================================

(defmethod ac:build-roles ((diagram Button))
  (setf (ac:actor-default-role diagram)
    (make-instance 'Button-Role :role-actor diagram)))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod diagram-minimum-width ((diagram Button))
  (button-minimum-width diagram))

(defmethod diagram-minimum-height ((diagram Button))
  (button-minimum-height diagram))

(defmethod make-layout-constraints ((button Button) (solver Layout-Solver))
  (brk:minimum-size (layout-domain solver)
		    (diagram-minimum-width button)
		    (diagram-minimum-height button)
		    button))
 
;;;------------------------------------------------------------

(defmethod ensure-big-enough-pixmaps ((diagram Button))
  (declare (type Button diagram))
  (let* ((window (diagram-window diagram))
	 (w (brk:width diagram))
	 (h (brk:height diagram)))
    (declare (type xlib:Window window)
	     (type az:Int16 w h))
    (dolist (slot '(button-up-pixmap
		    button-down-pixmap
		    button-flat-pixmap))
      (let ((pixmap (slot-value diagram slot)))
	(declare (type (or Null xlib:Pixmap) pixmap))
	(when (or (null pixmap)
		  (> w (xlib:drawable-width pixmap))
		  (> h (xlib:drawable-height pixmap)))
	  ;; then we need to make a -new- pixmap
	  (unless (null pixmap) (az:kill-object pixmap))
	  (setf pixmap
	    (xlib:create-pixmap :drawable window
				:width w :height h
				:depth (xlt:drawable-depth window)))
	  (setf (slot-value diagram slot) pixmap))))
    (setf (diagram-pixmap-rect diagram)
      (g:make-rect (xlt:drawable-space (button-up-pixmap diagram))
		   :left 0 :top 0 :width w :height h)))
  t)
 
;;;------------------------------------------------------------

(defmethod layout-diagram ((diagram Button) layout-options)

  (declare (type Button diagram)
	   (type List layout-options)
	   (ignore layout-options))
  (ensure-big-enough-pixmaps diagram)
  (update-internal diagram nil)
  diagram)

;;;============================================================
;;; Drawing
;;;============================================================

(defgeneric draw-button-up-pixmap ((diagram Button))
  (declare (type Button diagram)))

(defgeneric draw-button-down-pixmap ((diagram Button))
  (declare (type Button diagram)))

(defgeneric draw-button-flat-pixmap ((diagram Button))
  (declare (type Button diagram)))

(defmethod draw-button-up-pixmap ((diagram Button))
  (declare (type Button diagram))
  (let* ((w (brk:width diagram))
	 (h (brk:height diagram))
	 (m (button-margin diagram))
	 (pixmap (button-up-pixmap diagram))
	 (window (diagram-window diagram))
	 (gcontext (diagram-gcontext diagram))
	 (hs-name (button-hue-sat-name diagram))
	 (high-gcontext (xlt:3d-high-gcontext window hs-name))
	 (mid-gcontext (xlt:3d-mid-gcontext window hs-name))
	 (low-gcontext (xlt:3d-low-gcontext window hs-name)))
    (declare (type az:Int16 w h m)
	     (type xlib:Pixmap pixmap)
	     (type xlib:Window window)
	     (type xlt:Hue-Sat-Name hs-name)
	     (type xlib:Gcontext
		   gcontext high-gcontext mid-gcontext low-gcontext))
    ;; redraw the contents of the pixmap
    (xlib:draw-rectangle pixmap low-gcontext 0 0 w h t)
    (xlib:draw-lines pixmap high-gcontext
		     (list 0 0
			   0 (- h 1)
			   m (- h m 1)
			   m m
			   (- w m) m
			   w 0)
		     :fill-p t)
    (xlib:draw-rectangle pixmap mid-gcontext m m (- w m m) (- h m m) t)
    (xlib:draw-rectangle pixmap gcontext 0 0 (- w 1) (- h 1) nil)))

;;;------------------------------------------------------------

(defmethod draw-button-down-pixmap ((diagram Button))
  (declare (type Button diagram))
  (let* ((w (brk:width diagram))
	 (h (brk:height diagram))
	 (window (diagram-window diagram))
	 (gcontext (diagram-gcontext diagram))
	 (hs-name (button-hue-sat-name diagram))
	 (high-gcontext (xlt:3d-high-gcontext window hs-name))
	 (mid-gcontext (xlt:3d-mid-gcontext window hs-name))
	 (low-gcontext (xlt:3d-low-gcontext window hs-name))
	 (m (button-margin diagram))
	 (pixmap (button-down-pixmap diagram)))
    (declare (type az:Int16 w h m)
	     (type xlib:Pixmap pixmap)
	     (type xlib:Window window)
	     (type xlt:Hue-Sat-Name hs-name)
	     (type xlib:Gcontext
		   gcontext high-gcontext mid-gcontext low-gcontext))
    ;; redraw the contents of the pixmap
    (xlib:draw-rectangle pixmap gcontext 0 0 w h t)
    (xlib:draw-rectangle pixmap low-gcontext 3 3 (- w 3 3) (- h 3 3) t)
    (xlib:draw-lines pixmap high-gcontext
		     (list 3 3
			   3 (- h 4)
			   m (- h m 1)
			   m m
			   (- w m) m
			   (- w 3) 3)
		     :fill-p t)
    (xlib:draw-rectangle pixmap mid-gcontext m m (- w m m) (- h m m) t)))

;;;------------------------------------------------------------

(defmethod draw-button-flat-pixmap ((diagram Button))
  (declare (type Button diagram))
  (let* ((w (brk:width diagram))
	 (h (brk:height diagram))
	 (gcontext (diagram-gcontext diagram))
	 (window (diagram-window diagram))
	 (mid-gcontext
	  (xlt:3d-mid-gcontext window (button-hue-sat-name diagram))))
      ;; redraw the contents of the pixmap
    (let ((pixmap (button-flat-pixmap diagram)))
      (xlib:draw-rectangle pixmap mid-gcontext 0 0 w h t)
      (xlib:draw-rectangle pixmap gcontext 0 0 (- w 1) (- h 1) nil))))

;;;------------------------------------------------------------

(defmethod diagram-update-pixmaps ((button Button))
  (declare (type Button button))
  (draw-button-up-pixmap button)
  (draw-button-down-pixmap button)
  (draw-button-flat-pixmap button))

;;;------------------------------------------------------------
;;; automate redrawing whenever gcontext is changed

(defmethod (setf diagram-gcontext) (gcontext (diagram Button))
  (setf (slot-value diagram 'diagram-gcontext) gcontext)
  ;; actually, we will need to erase if the diagram changes size
  (layout-diagram diagram nil)
  (ac:send-msg 'role-draw-diagram diagram diagram (ac:new-id)))

;;;------------------------------------------------------------

(defmethod az:kill-object :before ((diagram Button))
  (az:kill-object (button-up-pixmap diagram))
  (az:kill-object (button-down-pixmap diagram))
  (az:kill-object (button-flat-pixmap diagram))
  (values))

;;;============================================================

(defclass Button-Role (Diagram-Role) ()
  (:documentation
   "A Button-Role handles input forwarded by the Button's parent diagram."))

;;;------------------------------------------------------------

(defmethod button-1-press ((role Button-Role) x y)
  "Any button-press event on a Button causes the button state
to be :down."
  (declare (type Button-Role role)
	   (type xlib:Int16 x y)
	   (ignore x y))
  (setf (button-state (ac:role-actor role)) :down)
  (values t))

;;;-----------------------------------------------------------

(defmethod button-2-press ((role Button-Role) x y)
  "Any button-press event on a Button causes the button state
to be :down."
  (declare (type Button-Role role)
	   (type xlib:Int16 x y)
	   (ignore x y))
  (setf (button-state (ac:role-actor role)) :down)
  (values t))

;;;-----------------------------------------------------------

(defmethod button-3-press ((role Button-Role) x y)
  "Any button-press event on a Button causes the button state
to be :down."
  (declare (type Button-Role role)
	   (type xlib:Int16 x y)
	   (ignore x y))
  (setf (button-state (ac:role-actor role)) :down)
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:button-press ((role Button-Role) 
			    msg-name sender receiver id
			    code x y state time root root-x root-y child 
			    same-screen-p)
  "Any button-press event on a Button causes the button state to be :down."
  (declare (type Button-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Button receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   code x y state time root root-x root-y child same-screen-p))
  (setf (button-state receiver) :down)
  (values t))

;;;-----------------------------------------------------------

(defmethod ac:button-release ((role Button-Role)
			      msg-name sender receiver id
			      code x y state time root root-x root-y child
			      same-screen-p)
  "Any button-release event on a Button causes the button state to be :up."
  (declare (type Button-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Button receiver)
	   (type ac:Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender id
		   code x y state time root root-x root-y child same-screen-p))
  (setf (button-state receiver) :up)
  (values t))

