;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(deftype Menu-Item ()

  "A Menu-Item is a list of length 3.  The first element is a string
that is printed on the item's button.  The second is a function. The
remainder is a list of args.  When the corresponding menu button is
pressed, the function is applied to the args."
  
  'List)

(defun menu-item-string (menu-item)
  (declare (type Menu-Item menu-item))
  (first menu-item))

(defun menu-item-function (menu-item)
  (declare (type Menu-Item menu-item))
  (second menu-item))

(defun menu-item-args (menu-item)
  (declare (type Menu-Item menu-item))
  (rest (rest menu-item)))

;;;============================================================
;;; Menus for direct action on the diagram
;;; and also on the diagram-subject of the diagram.
;;;============================================================

(defgeneric subject-menu-items (diagram-subject)
  (declare (type T diagram-subject))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu-items items relevant to <diagram-subject>, which
is being presented graphically somehow.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod subject-menu-items append ((diagram-subject T)) 
  (unless (null diagram-subject)
    (list (list (format nil "Examine ~a" diagram-subject)
		#'az:examine diagram-subject))))

;;;------------------------------------------------------------

(defgeneric subject-control-panel-items (diagram-subject)
  (declare (type T diagram-subject))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu-items items relevant to <diagram-subject>, which
is being presented graphically somehow.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod subject-control-panel-items append ((diagram-subject T)) 
  (unless (null diagram-subject)
    (list (list "Examine subject" #'az:examine diagram-subject))))

;;;------------------------------------------------------------

(defgeneric diagram-menu-items (diagram)
  (declare (type Diagram diagram))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <diagram>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod diagram-menu-items append ((diagram Diagram))
  (declare (type Diagram diagram))
  (append 
   (subject-menu-items (diagram-subject diagram))
   (list 
    (list 
     "Palette"
     #'(lambda ()
	 (let ((display (diagram-display diagram)))
	   (declare (type xlib:Display display))
	   (show-diagram (palette display)
			 :x (xlt:global-pointer-x display)
			 :y (xlt:global-pointer-y display)))))
    (list 
     (format nil "Redraw ~a" diagram)
     #'(lambda ()
	 (update-internal diagram nil)
	 (ac:send-msg 
	  'role-redraw-diagram diagram (diagram-root diagram) (ac:new-id))))
    (list 
     (format nil "Examine ~a" diagram) #'az:examine diagram)
    (list 
     (format nil "Kill ~a" diagram)
     #'(lambda ()
	 (ac:send-msg 
	  'ac:destroy-request diagram (diagram-root diagram) (ac:new-id)
	  (diagram-window diagram)))))))

;;;------------------------------------------------------------

(defgeneric diagram-control-panel-items (diagram)
  (declare (type Diagram diagram))
  #-:cmu17 (:method-combination append)
  (:documentation
   "Return a list of menu items relevant to <diagram>.

Each menu item must be a list whose first element is the string
printed in the menu and whose second element is a function that is
applied to the remainder of the list.

Append method combination is used to collect the menu items from all
super classes (types)."))

(defmethod diagram-control-panel-items append ((diagram Diagram))
  (declare (type Diagram diagram))
  (list 
   (list 
    "Kill"
    #'(lambda ()
	(ac:send-msg 
	 'ac:destroy-request diagram (diagram-root diagram) (ac:new-id)
	 (diagram-window diagram))))
   (list 
    "Palette"
    #'(lambda ()
	(let ((display (diagram-display diagram)))
	  (declare (type xlib:Display display))
	  (show-diagram (palette display)
			:x (xlt:global-pointer-x display)
			:y (xlt:global-pointer-y display)))))
   (list 
    "Redraw"
    #'(lambda ()
	(update-internal diagram nil)
	(ac:send-msg 
	 'role-redraw-diagram diagram (diagram-root diagram) (ac:new-id))))
   (list "Examine diagram" #'az:examine diagram)))
