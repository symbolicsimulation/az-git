;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(defclass Stroke (Clay-Object)
	  ((stroke-pixel
	    :type az:Card8
	    :accessor stroke-pixel
	    :documentation
	    "This is the pixel value with which we are painting.")
	   (stroke-pmask
	    :type az:Card8
	    :accessor stroke-pmask
	    :documentation
	    "Painting only affects the 1 bits in the pmask.")
	   (stroked-diagram
	    :type Clay-Object ;; Diagram
	    :accessor stroked-diagram)
	   (stroke-objects-to-be-updated
	    :type List
	    :accessor stroke-objects-to-be-updated
	    :initform ())))

;;;------------------------------------------------------------

(defgeneric return-stroke (stroke)
  #-sbcl (declare (type Stroke stroke))
  (:documentation
   "Return the stroke to the resource from which it was borrowed."))

(defgeneric mark-stroke (stroke rect)
  #-sbcl (declare (type Stroke stroke)
		  (type g:Rect rect))
  (:documentation
   "<Rect> is a region in diagram coordinates that's been marked
by the paintbrush. Update the <stroke> to reflect that fact.
<Rect> may be borrowed, so no pointer to it should be retained after
<mark-stroke> returns. If necessary, use <az:copy> to retain
a copy of <rect>."))



