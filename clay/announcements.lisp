;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

;;;============================================================

(an:defannouncement :painted (stroke)
  (:documentation
   "This announcement is made by an object that's been painted
interactively."))

(an:defannouncement :changed ()
  (:documentation
   "This announcement is made by an object that's been changed."))

(an:defannouncement :pressed ()
  (:documentation
   "This announcement is made by a button that's been pressed."))




