;;;-*- Package: :Message; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Message) 

;;;============================================================

(defclass Client-to-Server-Connection (Connection)
  ((server-host
    :type Host-Name
    :reader server-host
    :initarg :server-host
    :documentation "The host the message server running on.")
   (server-port
    :type Port
    :reader server-port
    :initarg :server-port
    :documentation
    "The port the message server uses for connections requests?")
   (server-identifier
    :type Symbol
    :reader server-identifier
    :documentation
    "The identifier the server uses to refer to itself."))

  (:default-initargs
   :server-host (message-server-host)
   :server-port (message-server-port))
  
  (:documentation
   "A Client-to-Server-Connection object is the interface between a
CL application and the Message Server. It represents a particular 
connection from a Lisp environment to the Message Server, which may
be running in the same Lisp environment or in a different one,
possibly on a physically different host. In fact, the Message Server 
need not be implemented in Lisp."))

;;;============================================================

(defmethod print-object ((connection Client-to-Server-Connection) stream)
  
  (declare (type Client-to-Server-Connection connection)
	   (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (connection stream)
   (format stream "C->S ~a" (identifier connection))))

;;;============================================================

(defmethod initialize-instance :after ((connection Client-to-Server-Connection)
				       &rest initargs)
  (declare (type Client-to-Server-Connection connection)
	   (ignore initargs))
  (connect connection))

;;;------------------------------------------------------------

(defun make-client-to-server-connection (&key
					 (host (message-server-host))
					 (port (message-server-port)))
  (make-instance 'Client-to-Server-Connection
		 :server-host host :server-port port))

;;;============================================================
;;; Registering for announcements
;;;============================================================

(defun test-announce (connection)
  "Make a dummy announcement."
  (declare (type Server-to-Client-Connection connection))
  (send-message connection
		(make-message
		 (list (intern-identifier "test")
		       (identifier connection)
		       (intern-identifier "*")
		       (make-identifier "announce")))))

(defun join-audience (connection template)
  "Send a message to the server asking to add <connection> to
the sudience for <template>."
  (declare (type Server-to-Client-Connection connection)
	   (type List template))
  (send-message connection
		(make-message
		 (list (intern-identifier "join-audience")
		       (identifier connection)
		       (server-identifier connection)
		       (make-identifier))
		 (map 'List #'string template))))


(defun leave-audience (connection template)
  "Send a message to the server asking to add <connection> to
the sudience for <template>."
  (declare (type Server-to-Client-Connection connection)
	   (type List template))
  (send-message connection
		(make-message
		 (list (intern-identifier "leave-audience")
		       (identifier connection)
		       (server-identifier connection)
		       (make-identifier))
		 (map 'List #'string template))))

;;;============================================================
;;; Connecting and Disconnectiong
;;;============================================================

(defmethod connect ((connection Client-to-Server-Connection))

  "This requests a connection from the Message Server and handles the
first, acknowledgement message sent by the server on this connection."

  (declare (type Client-to-Server-Connection connection))

  (let ((socket (connect-to-port
		 (server-host connection) (server-port connection))))
    (declare (type Socket socket))
    (setf (slot-value connection 'connection-socket) socket)
    (let ((acknowledgement (read-message connection)))
      (declare (type Message acknowledgement))
      (unless (eq (intern-identifier "connected")
		  (message-name acknowledgement))
	(error "First msg to connection ~s not a :connected msg:~s~%"
	       connection acknowledgement))
      (setf (slot-value connection 'server-identifier)
	(message-sender acknowledgement))
      (setf (slot-value connection 'identifier)
	(print (message-receiver acknowledgement)))
      (setf (slot-value connection 'connection-input-handler)
	(set-socket-input-handler socket
				  #'(lambda (fd)
				      (declare (ignore fd))
				      (catch :disconnected
					(handle-message
					 connection
					 (read-message connection)))))))))

;;;------------------------------------------------------------

(defmethod disconnect :before ((connection Client-to-Server-Connection))

  "The <connection> won't take any more multicast messages."

  (declare (type Client-to-Server-Connection connection))

  (leave-audience
   connection
   (list (intern-identifier "**")
	 (intern-identifier "**")
	 (intern-identifier "**")))

  (send-message connection
		(make-message (list (intern-identifier "disconnect")
				    (identifier connection)
				    (server-identifier connection)
				    (make-identifier "disconnect")))))

;;;============================================================
;;; Reading and writing
;;;============================================================

(defmethod read-message ((connection Client-to-Server-Connection)
			 &key (message (make-instance 'Message)))
  "Read the next message on <connection>, putting the data in <message>."
  (declare (type Connection connection)
	   (type Message message)
	   (:returns (type Message message)))
 
  (read-message-header connection message)
  (setf (message-body-chunk message) (read-chunk connection))
  ;; apply the reader to the message body
  (setf (message-body message) (read-chunk-data (message-body-chunk message)))
  ;; for debugging
  (format t "~&read-message ~%~s~%~s~%~s~%~s~%"
	connection
	message
	(message-header message)
	(message-body message))
  (force-output)

  (values message))











