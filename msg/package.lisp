;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Message; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :cl-user)

(defpackage :Message
  (:use :Common-Lisp #+:clos :CLOS #+:pcl :PCL)
  (:nicknames :MS :MSG)
  (:export 
   Identifier
   equal-identifiers?
   make-identifier
   intern-identifier

   Message-Name
   Message-Chunk
   make-chunk
   Message-Header
   Chunk-Data
   Message
   message-sender
   message-receiver
   message-body-length
   make-message


   Connection
   connect
   disconnect
   read-message
   send-message
   handle-message
	    
   Server
   make-server

   ;;Server-to-Client-Connection

   Client-to-Server-Connection
   make-client-to-server-connection
   server-identifier
   leave-audience
   join-audience

   make-eval-server
   make-eval-client
   send-eval-request))

(declaim (declaration :returns))

