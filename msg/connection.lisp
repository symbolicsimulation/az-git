;;;-*- Package: :Message; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Message) 

;;;============================================================

(defclass Connection (Standard-Object)

  ((identifier
    :type Identifier
    :reader identifier
    :initform (make-identifier)
    :documentation
    "A unique identifier symbol associated with each connection.
     Each connection is notified what identifier is assigned to it.
     Connections use identifiers to address messages to other connections.")
   (connection-socket
    :type Socket
    :reader connection-socket
    :initarg :connection-socket
    :documentation
    "The socket that the Message Server uses to send messages to
     and receive messages from the connection.")
   (connection-input-handler
    :type (or Null Socket-Input-Handler)
    :reader connection-input-handler
    :documentation
    "An input-handler for messages to the server on this connection."))

  (:documentation
   "A Connection is an object used by Message servers
and clients to send messages."))

;;;============================================================
;;; Connecting and disconnecting
;;;============================================================

(defgeneric connect (connection)
  (declare (type Connection connection))
  (:documentation
   "This is a generic function called to connect a client side connection
object to the corresponding server side connection object and vice versa.
It get called once one both sides for each connection."))

;;;------------------------------------------------------------

(defgeneric disconnect (connection)
  (declare (type Connection connection))
  (:documentation
   "This is a generic function called to break a connection.
It uses <disconnected> to do cleanup."))

(defmethod disconnect ((connection Connection))
  (declare (type Connection connection))
  (disconnected connection))

;;;------------------------------------------------------------

(defgeneric disconnected (connection)
  (declare (type Connection connection))
  (:documentation
   "This is a generic function called to clean up after
a connection is broken, or disconvered to have been broken.
Its methods should do as much cleanup as possible. For example,
on the server side, the method will remove the connection from the
server's connection table."))

(defmethod disconnected :before ((connection Connection))
  (declare (type Connection connection))
  (remove-socket-input-handler (connection-socket connection)
			       (connection-input-handler connection))
  (setf (slot-value connection 'connection-input-handler) nil))

(defmethod disconnected ((connection Connection))
  (declare (type Connection connection))
  (let ((socket (connection-socket connection)))
    (declare (type (or Null Socket) socket))
    (setf (slot-value connection 'connection-socket) nil)
    ;; test in case it's already closed or null
    (unless (null socket) (close-socket socket))))

;;;============================================================
;;; Reading and writing over connections
;;;============================================================

(defun read-from-connection (connection buffer n)
  "Read exactly <n> bytes from the <connection> and put it in <buffer>.
Signal an error if there's any read error or if the read completes but
returns fewer bytes than expected."

  (declare (type Connection connection)
	   (type Chunk-Data buffer)
	   (type Positive-Fixnum n)
	   (:returns (values (type Chunk-Data buffer)
			     (type Positive-Fixnum bytes-read))))

  (fill buffer 0)
  (assert (<= n (length buffer)))
  
  (if (= n 0)
      ;; then there's nothing to read
      (values buffer 0)
    ;; else
    (if (null (connection-socket connection))
	;; then something has gone wrong
	(progn
	  (warn "~&socket nil, probably disconnected~%")
	  (disconnected connection)
	  (throw :disconnected (values buffer 0)))
      ;; else try to read
      (let ((bytes-read (read-from-socket (connection-socket connection)
					  buffer n)))
	(declare (type (or Null Positive-Fixnum) bytes-read))
	(cond ((null bytes-read)
	       (warn-with-unix-error-msg
		"~&Error reading on ~s, ~%disconnecting~%" connection)
	       (disconnected connection)
	       (throw :disconnected (values buffer 0)))
	      ((< bytes-read n)
	       (warn "~&Only ~d bytes found on ~s, ~d wanted,~%disconnecting~%"
		     bytes-read connection n)
	       (disconnected connection)
	       (throw :disconnected (values buffer 0)))
	      (t (values buffer bytes-read)))))))

(defun write-to-connection (connection buffer n)
  "Write exactly <n> bytes from <buffer> on the <connection>.  Signal
an error if there's any read error or if the write completes but sends
fewer bytes than expected."
  (declare (type Connection connection)
	   (type Positive-Fixnum n)
	   (type Chunk-Data buffer)
	   (:returns (type Positive-Fixnum bytes-written)))
  (assert (<= n (length buffer)))
  (if (= n 0)
      (values 0)
    ;; else
    (if (null (connection-socket connection))
	;; something has gone wrong
	(progn
	  (warn "~&socket nil, probably disconnected~%")
	  (disconnected connection)
	  (throw :disconnected (values buffer 0)))
      (let ((bytes-written
	     (write-to-socket (connection-socket connection) buffer n)))
	(declare (type (or Null Fixnum) bytes-written))
	(cond
	 ((null bytes-written)
	  (warn-with-unix-error-msg
	   "~&Error writing ~d bytes of ~s on ~s, ~%disconnecting~%"
	   n buffer connection)
	  (disconnected connection)
	  (throw :disconnected (values buffer 0)))
	 ((< bytes-written n)
	  (warn "~&Only ~d of ~d bytes of ~s ~%written on ~s,~%disconnecting~%"
		bytes-written n buffer connection)
	  (disconnected connection)
	  (throw :disconnected (values buffer 0))))
	(values bytes-written)))))

;;;------------------------------------------------------------

(defparameter *chunk-length-buffer* (make-chunk-data $chunk-length-nbytes$))
(declaim (type Chunk-Data *chunk-length-buffer*))

(defun read-chunk-length (connection)
  (declare (type Connection connection)
	   (:returns (type Positive-Fixnum)))
  (read-from-connection connection *chunk-length-buffer* $chunk-length-nbytes$)
  ;; only use the low 4 bytes for now
  (let ((*print-array* t)
	(*print-pretty* t)
	(*print-length* nil))
    (format t "~&read length bytes = ~a~%" *chunk-length-buffer*)
    (force-output))
  (dotimes (i 4) (assert (zerop (aref *chunk-length-buffer* i))))
  (values
   (the Positive-Fixnum
	(logior (ash (aref *chunk-length-buffer* 4) 24)
		(ash (aref *chunk-length-buffer* 5) 16)
		(ash (aref *chunk-length-buffer* 6)  8)
		(ash (aref *chunk-length-buffer* 7)  0)))))

(defun write-chunk-length (connection n)
  (declare (type Connection connection)
	   (type Positive-Fixnum n)
	   (:returns (type Positive-Fixnum)))
  (fill *chunk-length-buffer* 0)
  (setf (aref *chunk-length-buffer* 4) (logand (ash n -24) #xFF))
  (setf (aref *chunk-length-buffer* 5) (logand (ash n -16) #xFF))
  (setf (aref *chunk-length-buffer* 6) (logand (ash n  -8) #xFF))
  (setf (aref *chunk-length-buffer* 7) (logand (ash n   0) #xFF))
  ;; for debugging
  (let ((*print-array* t)
	(*print-pretty* t)
	(*print-length* nil))
    (format t "~&write length bytes = ~a~%" *chunk-length-buffer*)
    (force-output))  
  (write-to-connection connection *chunk-length-buffer* $chunk-length-nbytes$)
  (values n))

;;;------------------------------------------------------------

(defun read-chunk (connection &key (chunk (make-instance 'Message-Chunk)))

  "Assuming the next incoming bytes on <connection> are a message
chunk, read it."

  (declare (type Connection connection)
	   (type Message-Chunk chunk)
	   (:returns (type Message-Chunk chunk)))

  (setf (chunk-length chunk) (read-chunk-length connection))
  (setf (chunk-data chunk)
    (read-from-connection connection (make-chunk-data (chunk-length chunk))
			  (chunk-length chunk)))
  (values chunk))

(defun read-chunk-data (chunk)

  "Applies <read-from-string> to the chunk-data.  Assumes that there
is only one item to read."

  (declare (type Message-Chunk chunk))

  (setf (chunk-string chunk) (chunk-data->string (chunk-data chunk)))
  (read-from-string (chunk-string chunk) nil nil))

(defun write-chunk (connection chunk)

  "Write the <chunk> on the <connection."

  (declare (type Connection connection)
	   (type Message-Chunk chunk)
	   (:returns (type Message-Chunk chunk)))

  (write-chunk-length connection (chunk-length chunk))
  (write-to-connection connection (chunk-data chunk) (chunk-length chunk))
  (values chunk))

;;;------------------------------------------------------------

(defun read-message-header (connection message)

  "Assuming the next incoming bytes on <connection> are a message
header, read it and install it as the mmessage-header of <message>."

  (declare (type Connection connection)
	   (:returns (type Message message)))

  (let* ((chunk (read-chunk connection))
	 (header (read-chunk-data chunk)))
    (declare (type Message-Chunk chunk)
	     (type (or Null Message-Header) header))
    
    (when (null header)
	  ;; then something's wrong --- disconnect without hanging up server
	  (warn "Apparent EOF on connection ~s, disconnecting.~%" connection)
	  (disconnected connection)
	  (throw :disconnected nil))

    (setf (message-header-chunk message) chunk)
    (setf (message-header message) header))
  (values message))

;;;------------------------------------------------------------

(defgeneric read-message (link &key message)
  (declare (type T link)
	   (type Message message)
	   (:returns (type Message message)))
  (:documentation
   "This is a generic function which is called to read messages
sent over link. Methods are provided for the case where <link> 
is a Connection."))

(defmethod read-message ((connection Connection)
			 &key (message (make-instance 'Message)))
  "Read the next message on <connection>, putting the data in <message>."
  (declare (type Connection connection)
	   (type Message message)
	   (:returns (type Message message)))
 
  (read-message-header connection message)
  (format t "~&header: ~s" (message-header message))
  (setf (message-body-chunk message) (read-chunk connection))
  ;; for debugging
  (format t "~&read-message ~%~s~%~s~%~s~%~s"
	  connection
	  message
	  (message-header message)
	  (message-body message))
  (force-output)

  (values message))

;;;============================================================
;;; Sending messages over a connection
;;;============================================================

(defgeneric send-message (link message)

  (declare (type T link)
	   (type Message message))

  (:documentation
   "This is a generic function which is called to send messages over a
<link>. Methods are provided for the case where the link is a Connection."))

(defmethod send-message ((connection Connection) (message Message))
  "Send <message> out on <connection>."

  (declare (type Connection connection)
	   (type Message message))
  (catch :disconnected
    (write-chunk connection (message-header-chunk message))
    (write-chunk connection (message-body-chunk message))
    ;; for debugging
    (format t "~&send-message ~%~s~%~s~%~s~%~s~%"
	    connection
	    message
	    (message-header message)
	    (message-body message)))
  (force-output))

;;;============================================================
;;; Handling messages
;;;============================================================

(defgeneric handle-message (handler message)
  (declare (type T handler)
	   (type Message message))
  (:documentation
   "Askes the handler to do something about the message.

The initial protocol for all potential message receivers is:

(describe sender receiver message-id)
---no body---

(disconnect sender receiver message-id)
(explanation)

(response sender receiver message-id)
(original-msg-header data)

(error sender receiver message-id)
(original-msg-header explanation)

Describe is a request for the receiver to send a response message
back to sender including a description string. It's up to the two
communicating clients to agree on what constitutes a meaningful
description, and most probably the responsibility of the requestor to
make sense of whatever the describer responds with.

Disconnect tells the receiver that it should shut down the
connection.  This would normally be sent either from the server to a
client or from a client to the server. It should not be sent from one
client to another. The purpose is to make it easier to break
connections gracefully. It's a little nicer letting the receiver just
discover a closed stream or socket, or hang waiting for messages
forever.  The <explanation> should be a string that can be printed for
debugging purposes.

Response messages are sent (typically by one client to another) as a
response to some previous message, whose header is provided in
<original-msg-header>. The contents of <data> are whatever the
responder wants them to be. It's up to the respondee to make sense of
the <data>. A typical example of a response is the reply to a
describe message.

Error messages are sent to notify a client that something was wrong
with a previous message it sent, whose header is supplied in
<original-msg-header>. It is possible that in special cases a client
will be able to extract additional useful information out of the
<explanation>, but, in general, the purpose of the <explanation>
is to provide a string that can be printed for debugging." ))

(defmethod handle-message ((connection Connection) (message Message))
  "Default is to just print it."

  (declare (type Connection connection)
	   (type Message message))

  (format t "~&handle-message ~%~s~%~s~%~s~%~s"
	connection
	message
	(message-header message)
	(message-body message)))

;;;============================================================

(defgeneric respond (connection message response)
  (declare (type T connection)
	   (type Message message)
	   (type T response))
  (:documentation
   "Generate a standard form response to <message>."))

(defmethod respond ((connection Connection) message response)
  (declare (type Connection connection)
	   (type Message message)
	   (type T response))
  (let ((header (list (intern-identifier "response")
		      (identifier connection)
		      (message-sender message)
		      (make-identifier "response"))))
    (send-message connection (make-message header response))))

;;;============================================================

(defgeneric make-describe-response (connection message)
  (declare (type T connection)
	   (type Message message)
	   (:returns (type List)))
  (:documentation
   "Generate a description of <connection> to be used
as the body of a response to a describe message."))

(defmethod make-describe-response ((connection Connection) message)
  (declare (type Connection connection)
	   (type Message message)
	   (:returns (type List)))
  (list (message-header message)
	:class (class-name (class-of connection))
	:identifier (identifier connection)))







