#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>

#define TRUE 1

/*-----------------------------------------------------------------*/
/*   Some socket tools                                             */
/*-----------------------------------------------------------------*/
 
int accept_connection (request_socket)
     int request_socket;

{ struct sockaddr_in connection_address;
  int address_len = sizeof(connection_address);
  int connection_socket;

  /* accept a connection, creating a new socket fd */
  connection_socket = accept(request_socket,
			     &connection_address,
			     &address_len);

  /* check for an error */
  if (connection_socket == -1) {
    perror("accept_tcp_connection -> accept");
    return -1; }
  else {
    /* if no error, return the new socket fd */
    return connection_socket; } }
 
/*-----------------------------------------------------------------*/
 
int connect_to_port (hostname, port)
     char *hostname;
     int port;

{ struct hostent *hostp;
  struct sockaddr_in address;
  int address_len = sizeof(address);
  int socket_fd;
  
  /* find the host */
  if ( (hostp = gethostbyname(hostname)) == NULL) {
    perror("connect_to_port -> gethostbyname");
    return -1; }
  else {
    /* Initialize the socket address object */
    bzero(&address, address_len);
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = ((struct in_addr *)
			       (hostp->h_addr))->s_addr;
    address.sin_port = htons(port);

    /* Allocate a socket file descriptor in the INTERNET domain. */
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if( socket_fd == -1 ) {
      perror("connect_to_port -> socket");
      return -1; }
    else {
      /* Connect the socket. */
      if (connect(socket_fd, &address, address_len) == -1)
	{ perror("connect_to_port -> connect");
	  return -1; }
      else {
	return socket_fd; } } } }
  
/*-----------------------------------------------------------------*/
 
int connection_requested_p (socket)
     int socket;
{ fd_set ready;
  struct timeval timer;

  FD_ZERO(&ready);
  FD_SET(socket,&ready);
  timer.tv_sec = 0;

  if ( select(socket+1, &ready, (fd_set *) 0, (fd_set *) 0, &timer)
      < 0 )
    { perror("Select failed."); }
  
  if (FD_ISSET(socket,&ready)) { return 1; }
  else                         { return 0; }}
  
/*-----------------------------------------------------------------*/
 
int create_connection_request_socket (port, socket_fd)
     int *port, *socket_fd;
{ struct sockaddr_in address;
  int address_len = sizeof(address);

  /* Allocate a socket file descriptor in the INTERNET domain. */
  *socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if( *socket_fd == -1 )
    { perror("create_connection_request_socket -> socket");
      return -1; }
  else {
    /* Initialize the socket address object */
    bzero(&address, address_len);
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(*port);

    /* Bind the socket to the port. */
    if ( bind(*socket_fd, &address, address_len) == -1 )
      { perror("create_connection_request_socket -> bind");
	return -1; }
    else {
      /* update the value of <port> in case bind changed it. */
      *port = socket_port(*socket_fd,&address,address_len);
      return 0; } } }
  
/*-----------------------------------------------------------------*/

int socket_port (socket, address, addrlen)
     int socket;
     struct sockaddr_in *address;
     int addrlen;
{ int port;
  int len = (sizeof address);
     
  if( getsockname( socket, (struct sockaddr *) address, &len)
     < 0 )
    { perror("Error getting socket name");
      return -1; }
  else {
    port = ntohs(address->sin_port);
    return port; } }
 
/*-----------------------------------------------------------------*/
