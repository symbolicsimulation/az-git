;;;-*- Package: :Message; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Message) 

;;;============================================================

(deftype Message-Header ()
  
  "Messages come in two parts: a fixed format message header and 
an optional (or length zero) free format message body.

The message header is a list of the form:

   (message-name sender-id receiver-id message-id body-length)

where <message-name> is a Message-Name, <sender-id>, <receiver-id>,
and <message-id> are Identifiers and <body-length> is a non-negative 
integer, the length of the body in bytes."

  'List)

;;;============================================================

(deftype Message-Name ()

  "This type is used to represent the name or (or types) of messages.
Message-names can be compared with #'eq."

  'Identifier)
 
;;;============================================================

(defconstant $chunk-length-nbytes$ 8
  "How many bytes are used to specify the length of the chunk?  We use
8 bytes = 64 bits to allow for future expansion, though 32 bits = 4
bytes would be plenty for now.")

(deftype Chunk-Data () 

  "Message Chunks come in two parts: a fixed format chunk length
specifier and a (possibly length zero) chunk body of raw bytes."

  '(Simple-Array (Unsigned-Byte 8) (*)))

;; this assumes that code-char and char-code use ascii.

(defun chunk-data->string (data
			   &key (string (make-string (length data))))
  (declare (type Chunk-Data data)
	   (type String string))

  (dotimes (i (length data))
	   (setf (aref string i) (code-char (aref data i))))
  string)

(defun chunk-string->data (string
			   &key (data (make-chunk-data (length string))))
  (declare (type String string)
	   (type Chunk-Data data))
  (dotimes (i (length string))
	   (setf (aref data i) (char-code (aref string i))))
  data)

(defun make-chunk-data (nbytes)
  (make-array nbytes :element-type '(Unsigned-Byte 8)))

(defclass Message-Chunk (Standard-Object)
  ((chunk-length
    :type Positive-Fixnum
    :accessor chunk-length
    :documentation "How many bytes in the message chunk?")
   (chunk-data
    :type Chunk-Data
    :accessor chunk-data
    :initform (make-chunk-data 0)
    :documentation "The raw bytes in the chunk.")
   (chunk-string
    :type Simple-String
    :accessor chunk-string
    :documentation
    "The raw bytes in the chunk interpreted as ASCII characters."))

  (:documentation
   "Message Chunks come in two parts: a fixed format chunk length
specifier and a (possibly length zero) chunk body of raw bytes."))

;;;------------------------------------------------------------

(defun make-chunk (object)
  (declare (type T object))
  (let ((string (if object
		    (format nil "~s" object)
		  ;; else if object is null, string is empty
		  ""))
	(chunk (make-instance 'Message-Chunk)))
    (declare (type String string)
	     (type Message-Chunk chunk))
    (setf (chunk-length chunk) (length string))
    (setf (chunk-string chunk) string)
    (setf (chunk-data chunk) (chunk-string->data string))
    (values chunk)))

;;;============================================================

(defclass Message (Standard-Object)
  ((message-header-chunk
    :type Message-Chunk
    :accessor message-header-chunk
    :initarg :message-header-chunk
    :documentation
    "The raw message header")
   (message-header
    :type Message-Header
    :accessor message-header
    :initarg :message-header
    :documentation
    "The result of applying read-from-string to the data in the
message-header-chunk.")
   (message-body-chunk
    :type Message-Chunk
    :accessor message-body-chunk
    :initarg :message-body-chunk
    :documentation "The raw message body")
   (message-body
    :type T
    :accessor message-body
    :initarg :message-body
    :documentation
    "The result of applying read-from-string to the data in the
message-body-chunk. This is only done to messages to the server."))

  (:documentation
   "Message objects are used to hold messages that have been received
or are to be sent by a CL process.

Messages come in two parts: a fixed format header and a possibly
empty free format body. Both parts are passed as Message-Chunks.")
  (:default-initargs
   :message-header ()
   :message-body ()
   :message-header-chunk (make-chunk "")
   :message-body-chunk (make-chunk "")))
  
;;;============================================================

(defun make-message (header &optional (body nil))
  (declare (type List header)
	   (type T body)
	   (:returns (type Message)))
  (setf header (map 'List #'string header))
  (make-instance 'Message
		 :message-header header
		 :message-body body
		 :message-header-chunk (make-chunk header)
		 :message-body-chunk (make-chunk body)))

;;;============================================================

(defun message-name (message)
  "Get the message's name or type."
  (declare (type Message message)
	   (:returns (type Message-Name)))
  (intern-identifier (first (message-header message))))

(defun message-sender (message)
  "Get the identifier of the message's sender (connection)."
  (declare (type Message message)
	   (:returns (type Identifier sender)))
  (intern-identifier (second (message-header message))))

(defun message-receiver (message)
  "Get the identifier of the message's intended receiver (connection)."
  (declare (type Message message)
	   (:returns (type Identifier receiver)))
  (intern-identifier (third (message-header message))))

(defmethod identifier ((message Message))

  "Get a possibly null identifier for the message itself.
This identifier is generated, if desired, by the message sender. 
Its purpose is to allow the receiver of the message to refer
to this message in responses to the sender. For example, when
replying with an error notification, it's useful to be able
to send back some information about which message had the error."

  (declare (type Message message)
	   (:returns (type (or Null Identifier) message-id)))

  (intern-identifier (fourth (message-header message))))

;;;------------------------------------------------------------

(defmethod print-object ((message Message) stream)
  
  (declare (type Message message)
	   (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (message stream)
   (format stream "Msg:~a ~d ~d"
	   (message-name message)
	   (chunk-length (message-header-chunk message))
	   (chunk-length (message-body-chunk message)))))

;;;============================================================
