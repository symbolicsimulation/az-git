;;;-*- Package: :Message; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Message) 

;;;============================================================

(defclass Server (Standard-Object)
  ((server-host
    :type Host-Name
    :reader server-host
    :initarg :server-host
    :documentation
    "The host on which the Server is running.")
   (server-port
    :type Port
    :reader server-port
    :initarg :server-port
    :documentation
    "The port used by clients to request connections.")
   (server-socket
    :type Socket
    :reader server-socket
    :documentation
    "The socket used by the message server to receive connection requests.")
   (server-input-handler
    :type Socket-Input-Handler
    :reader server-input-handler
    :documentation
    "This is the Socket-Input-Handler used to accept connection requests.")
   (identifier
    :type Identifier
    :reader identifier
    :documentation
    "An identifier that connections use to send messages to the server,
     as opposed to other connections.")
   (connections
    :type List ;; (of Server-to-Client-Connection's)
    :accessor connections
    :initform ())
   (message-name-table
    :type Hash-Table
    :reader message-name-table
    :initform (make-hash-table :test #'eq)
    :documentation
    "The <message-name-table> table is used to map a message 
(based on the message-name, sender id, and receiver id) to a list of audience 
member ids. For each message received by the message server, it will
check the audience table to see if anyone else wants to see a the message
and forward a copy if so.

The <message-name-table> maps message names (including :*)
to sender tables. A sender table maps sender ids (including :*)
to receiver tables. A receiver table maps receiver ids (including :*)
to audience id lists."))

  (:default-initargs
   :server-host (message-server-host)
   :server-port (message-server-port))

  (:documentation
   "This is a CL implementation of a multicast message server."))

;;;============================================================
;;; Server side connection objects
;;;============================================================

(defclass Server-to-Client-Connection (Connection)

  ((connection-server
    :type Server
    :reader connection-server
    :initarg :connection-server
    :documentation
    "The server that owns and uses this connection."))

  (:documentation
   "A Server-to-Client-Connection is an object used on the Message Server
side to identify Clients, to send messages to them, and to receive messages 
from them either for the Server itself or for forwarding to other Clients."))

;;;============================================================

(defun make-server-identifier (server)
  (make-identifier (string-upcase
		    (format nil "~a.~a.~a"
			    (server-host server)
			    (server-port server)
			    (server-socket server)))))

(defmethod initialize-instance :after ((server Server) &rest initargs)
  (declare (ignore initargs))
  (flet ((make-connection (fd)
	   (declare (ignore fd))
	   (make-server-to-client-connection server)))
    (multiple-value-bind
	(port socket) (make-connection-request-socket (server-port server))
      (setf (slot-value server 'server-port) port)
      (setf (slot-value server 'server-socket) socket)
      (setf (slot-value server 'identifier) (make-server-identifier server))
      (setf (slot-value server 'server-input-handler)
	(set-socket-input-handler socket #'make-connection))))

  (connect server)
  ;; debugging
  (describe server) (force-output)
  server)

;;;------------------------------------------------------------

(defun make-server () (make-instance 'Server))

;;;============================================================
;;; Connecting and Disconnecting
;;;============================================================

(defmethod connect ((server Server))
  "This is a little dangerous/misleading, since this might be
more precisely named something like <start-accepting-connections>."
  (declare (type Server server))
  (listen-for-connection-requests (server-socket server)))

;;;------------------------------------------------------------

(defmethod disconnect ((server Server))
  "This is a little dangerous/misleading, since disconnecting a Server
is not exactly analogous to disconnecting a connection."
  (declare (type Server server))
  (remove-socket-input-handler (server-socket server)
			       (server-input-handler server))
  (let ((connections (connections server)))
    (declare (type List connections))
    (setf (connections server) ())
    (dolist (connection connections)
     (declare (type Connection connection))
     (disconnect connection))))

;;;------------------------------------------------------------

(defun identifier->object (server identifier)

  (if (eq identifier (identifier server))
      server
    (find identifier (connections server) :key #'identifier)))

;;;============================================================
;;; Audience handling
;;;============================================================

(defun message-audience (server message)

  "Return a list of the ids of all connections that should
receive a coyp of this message."

  (declare (type Message message)
	   (type Server server)
	   (:returns (type List)))

  (let* ((n (message-name message))
	 (s (message-sender message))
	 (r (message-receiver message))
	 (n-table (gethash n (message-name-table server) nil))
	 (*-table (unless (eq n :*)
			  (gethash :* (message-name-table server) nil)))
	 (ns-table (unless (null n-table) (gethash s  n-table nil)))
	 (n*-table (unless (or (eq s :*) (null n-table))
			   (gethash :* n-table nil)))
	 (*s-table (unless (null *-table) (gethash s  *-table nil)))
	 (**-table (unless (or (eq r :*) (null *-table))
			   (gethash :* *-table nil))))
    (delete-duplicates
     (nconc
      (unless (null ns-table)
	      (concatenate
	       'List
	       (gethash r ns-table nil) (gethash :* ns-table nil)))
      (unless (null n*-table)
	      (concatenate
	       'List
	       (gethash r n*-table nil) (gethash :* n*-table nil)))
      (unless (null *s-table)
	      (concatenate
	       'List
	       (gethash r *s-table nil) (gethash :* *s-table nil)))
      (unless (null **-table)
	      (concatenate
	       'List
	       (gethash r **-table nil) (gethash :* **-table nil)))))))

;;;------------------------------------------------------------

(defun add-to-audience (member name sender receiver server)

  "Add <member> to the audience for messages that match the <name>,
<sender>, <receiver> triple. Matching means that <name>, <sender>, and
<receiver> are eq to the message name, message sender, and message
sender, respectively, unless any of <name>, <sender>, and <receiver>
are :*, which will match any identifier in the corresponding field of
the message."

  (declare (type Identifier member sender receiver)
	   (type Message-Name name)
	   (type Server server))

  (let* ((n-table (message-name-table server))
	 (s-table (gethash name n-table nil)))
    (declare (type Hash-Table n-table)
	     (type (or Null Hash-Table) s-table))
    (when (null s-table)
	  (setf s-table (make-hash-table :test #'eq))
	  (setf (gethash name n-table) s-table))
    (let ((r-table (gethash sender s-table nil)))
      (declare (type (or Null Hash-Table) r-table))
      (when (null r-table)
	    (setf r-table (make-hash-table :test #'eq))
	    (setf (gethash sender s-table) r-table))
      (let ((audience (gethash receiver r-table nil)))
	(declare (type List audience))
	(setf (gethash receiver r-table) (pushnew member audience))))))
 
;;;------------------------------------------------------------

(defun remove-from-receiver-table (member receiver-table receiver)

  (declare (type Identifier member receiver)
	   (type (or Null Hash-Table) receiver-table))

  (unless (null receiver-table)
   (if (eq receiver :**)
       ;; then recurse
       (maphash
	#'(lambda (key value)
	    (declare (ignore value))
	    (remove-from-receiver-table member receiver-table key))
	receiver-table)
     ;; else
     (let ((audience (gethash receiver receiver-table nil)))
       (declare (type List audience))
       (unless (null audience)
	(setf (gethash receiver receiver-table) (delete member audience)))))))

(defun remove-from-sender-table (member sender-table sender receiver)

  (declare (type Identifier member sender receiver)
	   (type (or Null Hash-Table) sender-table))

  (unless (null sender-table)
   (if (eq sender :**)
       ;; then recurse
       (maphash
	#'(lambda (key value)
	    (declare (ignore value))
	    (remove-from-sender-table member sender-table key receiver))
	sender-table)
     ;; else
     (remove-from-receiver-table
      member (gethash sender sender-table nil) receiver))))

(defun remove-from-audience (server member name sender receiver)

  "Remove <member> from the audience for messages that match the
<name>, <sender>, <receiver> triple. Matching means that <name>,
<sender>, and <receiver> are eq to the audience name, audience sender,
and audience sender, even if any of <name>, <sender>, or <receiver>
are eq to :*. If <name>, <sender>, or <receiver> are eq to :**, then
the member is removed from all audiences that match on the other
fields."

  (declare (type Server server)
	   (type Identifier member name sender receiver))

  (let ((name-table (message-name-table server)))
    (declare (type Hash-Table name-table))
    (if (eq name :**)
	;; then remove it for all names
	(maphash #'(lambda (key value)
		     ;; recurse on every message name that's in the table
		     (declare (ignore value))
		     (remove-from-audience server member key sender receiver))
		 name-table)
      ;; else
      (remove-from-sender-table
       member (gethash name name-table nil) sender receiver))))
 
;;;============================================================
;;; Message passing
;;;============================================================

(defun make-unknown-receiver-error-message (server message)
  (let ((header (list :error
		      (identifier server)
		      (message-sender message)
		      (make-identifier "unknown-receiver"))))
    (declare (type List header))
    (make-message header (list :unknown-receiver-id message))))

;;;------------------------------------------------------------
;;; This is dangerous --- it's exactly the kind of thing
;;; the screws up auto code analysis, eg type inference.
;;; Maybe should be replaced by looking up function in a table.

(defun server-function (message-name)
  (intern
   (string-upcase
    (concatenate 'String "server-" (string message-name)))
   :msg))

;;;------------------------------------------------------------

(defun print-hashtable (key value)
  (print (list key value))
  (when (typep value 'Hash-Table)
	(maphash #'print-hashtable value)))

(defun display-table (server)
  (print-hashtable t (message-name-table server)))

;;;------------------------------------------------------------

(defun server-disconnect (server message)
  "This is called to handle a disconnect message."

  (declare (type Server server)
	   (type Message message))

  (assert (eq (intern-identifier "disconnect") (message-name message)))
  (let ((disconnector (identifier->object server (message-sender message))))
    (declare (type (or Null Server-to-Client-Connection) disconnector))
    ;; for debugging
    (format t "~&~s disconnecting~%" disconnector)
    (force-output)
    (disconnected disconnector)))

;;;------------------------------------------------------------

(defun server-join-audience (server message)
  "This is called to handle a join-audience message."

  (declare (type Server server)
	   (type Message message))

  (assert (eq (intern-identifier "join-audience") (message-name message)))
  (let* ((member (message-sender message))
	 (join-template (message-body message))
	 (name (intern-identifier (first join-template)))
	 (sender (intern-identifier (second join-template)))
	 (receiver (intern-identifier (third join-template))))
    (declare (type Identifier member name sender receiver)
	     (type List join-template))
    ;; for debugging
    (format t "~&~s ~%joining audience for ~%~s~%"
	    member join-template)
    (force-output)
    (add-to-audience member name sender receiver server)))

;;;------------------------------------------------------------

(defun server-leave-audience (server message)
  "This is called to handle a leave-audience message."

  (declare (type Server server)
	   (type Message message))

  (assert (eq (intern-identifier "leave-audience") (message-name message)))
  (let* ((member (message-sender message))
	 (join-template (message-body message))
	 (name (intern-identifier (first join-template)))
	 (sender (intern-identifier (second join-template)))
	 (receiver (intern-identifier (third join-template))))
    (declare (type Identifier name member sender receiver)
	     (type List join-template))
    (remove-from-audience server member name sender receiver)))

;;;------------------------------------------------------------

(defmethod handle-message ((server Server) (message Message))
  
  "This method is called whenever a message is sent to the server itself.
The initial protocol specifically for servers is:

 (join-audience sender server message-id nbytes) join-template

 (leave-audience sender server message-id nbytes) leave-template

join-audience asks the <server> to send <sender> a copy of any
message that matches <join-template>. <join-template> has the
form: (message-name sender receiver), where <message-name> is either a
Message-Name or *, and <sender> and <receiver> are either Identifiers
or *. A message matches a template if the three fields match;
the fields match if they are eq or if the template's value is *.

leave-audience tells the <server> that <sender> should no longer
receive copies of messages matching one or more join-templates.  The
semantics of the server's handling of leave-audience can be modeled
as follows: Suppose that, for each sender of join-audience messages,
the server keeps a list of the join templates received. When a
leave-audience message is received, the server deletes all those join
templates belonging to <sender> that match the <leave-template>.  A
join template matches the <leave-template> if the three fields match.
A field of the <leave-template> matches a field of a join template if
they are eq or if the field of the <leave-template> is **, not the
single *!

The reason for the use of ** in leave templates is to allow a client
to differentiate between deleting specific join templates that contain
*'s and deleting sets of join templates specified with a wild cards.
"
  (declare (type Server server)
	   (type Message message))

  ;; apply the reader to the message body
  (setf (message-body message) (read-chunk-data (message-body-chunk message)))
  ;; debugging
  (print message)
  (message-body message)
  (force-output)

  (funcall (server-function (message-name message)) server message))

;;;------------------------------------------------------------

(defmethod send-message ((server Server) (message Message))
  (handle-message server message))

;;;------------------------------------------------------------

(defun process-message (server message)

  "First check the audiences, to see if anyone wants a copy of this
message.  

If the message is addressed to another client connection,
pass it on. If it is for the server itself, handle it. Otherwise
report an error to the sender.

Finally, send copies of the message to anyone in its audience."

  (declare (type Server server)
	   (type Message message))

  (let ((audience (message-audience server message))
	(receiver (identifier->object server (message-receiver message))))
    (declare (type List audience)
	     (type (or Null Server Server-to-Client-Connection) receiver))

    (when receiver (send-message receiver message))
    
    (if (or receiver (eq (message-receiver message) :*))
	;; send copies to members of the audience
	(dolist
	 (member-id audience)
	 (let ((member (identifier->object server member-id)))
	   (if member
	       (send-message member message)
	     ;; else the server doesn't recognize the member id
	     ;; so it shouldn't be in any audiences.
	     (progn
	       (warn "~s is unknown to ~s, removing it from all audiences.~%"
		     member-id server)
	       (remove-from-audience server member-id :** :** :**)))))
      
      ;; else the server doesn't recognize the receiver-id,
      ;; and should send an error msg back to the sender

      (let ((sender (identifier->object server (message-sender message))))
	(declare (type (or Null Server Server-to-Client-Connection) sender))
	(warn "~&Receiver is unknown:~% ~s~%" message)
	(if sender
	    ;; then return an error message 
	    (send-message sender
			  (make-unknown-receiver-error-message server message))
	  ;; else sender is unknown too! Just forget it.
	  (warn "~&Sender is also unknown! ~%~s~%" message))))))

;;;============================================================

(defun make-server-to-client-connection (server)
  (declare (type Server server))
  (let ((connection (make-instance 'Server-to-Client-connection
				   :connection-server server)))
    (declare (type Server-to-Client-Connection connection))
    (connect connection)
    connection))

;;;============================================================

(defmethod print-object ((connection Server-to-Client-Connection) stream)
  
  (declare (type Server-to-Client-Connection connection)
	   (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (connection stream)
   (format stream "S->C ~a" (identifier connection))))

;;;------------------------------------------------------------

(defun server-to-client-connection-input-handler (server connection)

  "Returns a function that gets called automatically whenever there is
input waiting on the file descriptor <fd> that corresponds to
<connection>."


  (declare (type Server server)
	   (type Server-to-Client-Connection connection))
  
  #'(lambda (fd)
      "Have the server process the next waiting message."
      (declare (type Socket fd)
	       (ignore fd))
      (catch :disconnected
	(process-message server (read-message connection)))))

;;;------------------------------------------------------------

(defmethod connect ((connection Server-to-Client-Connection))

  (declare (type Server-to-Client-Connection connection))
  
  (let* ((server (connection-server connection))
	 (socket (accept-connection (server-socket server))))
    (declare (type Server server)
	     (type Socket socket))
    (push connection (connections server))
    (setf (slot-value connection 'connection-socket) socket)
    (setf (slot-value connection 'connection-input-handler)
	  (set-socket-input-handler
	   (connection-socket connection)
	   (server-to-client-connection-input-handler server connection)))

    ;; debugging
    (format t "~&~s ~%accepted connection from ~%~s~%" server connection)
    (describe connection)
    (force-output)

    (send-message connection (make-message
			      (list (intern-identifier "connected")
				    (identifier server)
				    (identifier connection)
				    (make-identifier "connected"))))))

;;;------------------------------------------------------------

(defmethod disconnected :before ((connection Server-to-Client-Connection))
  (declare (type Server-to-Client-Connection connection))
  (let ((server (connection-server connection)))
    (declare (type Server server))
    (setf (connections server) (delete connection (connections server)))))

;;;============================================================
;;; Server event loop sketch for non-interrupt io CL's
;;;============================================================

#-(or :cmu :excl)
(defun server-handle-messages (server)
  (declare (type Server server))
  (dolist (in (connections server))
	  (declare (type Connection in))
	  (server-handle-message server in)))

#-(or :cmu :excl)
(defun server-connection-requested? (server)
  (declare (type Server server))
  (not (zerop (connection-requested? (server-socket server)))))

#-(or :cmu :excl)
(defun server-loop (server)
  (declare (type Server server))
  (describe server)
  (unwind-protect
      (catch :exit-server-loop
	(loop
	 (when (server-connection-requested? server)
	       (connect server)
	       (pprint (connections server)))
	 (server-handle-messages server)
	 (force-output)))
    (disconnect-server server)))
  
