;;;-*- Package: :cl-user; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package  :cl-user) 

;;;============================================================

(defmacro ld (path) `(load (string-downcase (string ',path))))

(defmacro cl (path) `(compile-file (string-downcase (string ',path)) :load t))

(cl sock)

;;;============================================================

(defparameter s (msg:make-server))

(defparameter es (msg:make-eval-server))

(defparameter ec (msg:make-eval-client))


(msg:send-eval-request ec '(* 2 pi))
(msg:send-eval-request ec '(type-of pi))
(msg:send-eval-request ec '(format nil "~s~%" es))


(let* ((id (msg:identifier c))
       (h (list :eval id :id18 (msg:make-identifier "eval")))
       (b '(+ 2 2)))
  (defparameter m (msg:make-message h b)))


(msg:send-message c m)


(dotimes
 (i 10)
 (let ((h (list :test :id2 :id18 (msg:make-identifier "test"))))
   (msg:send-message c (msg:make-message h))))

(dotimes
 (i 10)
 (let ((h (list :test :id18 :id2 (msg:make-identifier "test"))))
   (msg:send-message c (msg:make-message h))))

(msg:join-audience c (list :* :id18 :*))

(let ((h (list :test :id18 :* (msg:make-identifier "announce"))))
   (msg:send-message c (msg:make-message h)))
