;;;-*- Package: :Message; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Message) 

;;;============================================================

(defclass Eval-Server (Client-to-Server-Connection)
  ()
  (:documentation
   "A Eval-Server handles eval messages.

An eval message has the form:
  
header:  (eval sender-id eval-server-id msg-id)
body:    lisp-expression

where the body of the message is a string of ascii characters that can
be read into a lisp expression with a single call to
<read-from-string>.  The message is handled by calling <eval> on the
expression returned by <read-from-string> in the default environment.
Errors may or may not be reported back to the sender.  If the
expression evaluates successfully, a response message is sent back to
sender-id with a printed representation of the value returned from
the call to eval.

A Eval-Server will respond to any announcements of the form:

header: (cl-eval-service-request sender-id * msg-id)
body: ---empty---

by sending a response msg back to <sender-id> whose body
is the header of the search announcement."))

;;;============================================================

(defun make-eval-server () (make-instance 'Eval-Server))

;;;============================================================

(defmethod print-object ((connection Eval-Server) stream)
  
  (declare (type Eval-Server connection)
	   (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (connection stream)
   (format stream "Eval-Server ~a" (identifier connection))))

;;;============================================================
;;; Connecting and disconnecting
;;;============================================================

(defmethod connect :after ((connection Eval-Server))

  "The Eval Server wants to hear any requests for CL eval service."

  (declare (type Eval-Server connection))

  (join-audience connection
		 (list (intern-identifier "cl-eval-service-request")
		       (intern-identifier "*")
		       (intern-identifier "*"))))

;;;============================================================
;;; Message handling
;;;============================================================

(defgeneric make-eval-response (connection message)
  (declare (type Eval-Server connection)
	   (type Message message))
  (:documentation
   "Eval the expression in the body of <message> and write it to a
string, to be used as the body of the response to <message>."))

(defmethod make-eval-response ((connection Eval-Server) message)

  "Eval the expression in the body of <message> and write it to a
string, to be used as the body of the response to <message>."

  (declare (type Eval-Server connection)
	   (type Message message))

  ;; apply the reader to the message body
  (print
   (list (message-header message)
	 (eval (message-body message)))))

;;;============================================================

(defmethod handle-message ((connection Eval-Server) message)
  (declare (type Eval-Server connection)
	   (type Message message))

  (case
   (message-name message)
   ((:|describe| :|cl-eval-service-request|)
    (respond connection message (make-describe-response connection message)))
   (:|eval|
    (respond connection message (make-eval-response connection message)))
   (:|disconnect|
    (disconnect connection))
   ((:|response| :|error|)
    (warn "~&~s ~%doesn't know how to handle ~%~s, ~%ignoring it.~%"
	  connection message))
   (otherwise
    (warn "~&~s ~%doesn't know how to handle ~%~s, ~%ignoring it.~%"
	  connection message))))
     
;;;============================================================
;;; A test client for eval servers
;;;============================================================

(defclass Eval-Client (Client-to-Server-Connection)
  ((eval-server-id
    :type (or Null Identifier)
    :accessor eval-server-id
    :initform nil
    :documentation
    "The Identifier that is used to address messages to the eval server." ))
  (:documentation
   "A Eval-Client sends :eval messages. An eval message has the form:
  
header:  (eval eval-client-id eval-server-id msg-id)

body:    lisp-expression

where the body of the message is a string of ascii characters that can
be read into a lisp expression with a single call to
<read-from-string>.  The message is handled by calling <eval> on the
expression returned by <read-from-string> in the default environment.
Errors may or may not be reported back to the sender.  If the
expression evaluates successfully, a response message is sent back to
sender-id with a printed representation of the value returned from
the call to eval.

A Eval-Client finds a server by making an announcement of the form:

header: (cl-eval-service-request eval-client-id * msg-id)
body: ---empty---

by sending a response msg back to <sender-id> whose body
is the header of the search announcement."))

;;;============================================================

(defmethod print-object ((connection Eval-Client) stream)
  
  (declare (type Eval-Client connection)
	   (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (connection stream)
   (format stream "Eval-Client ~a" (identifier connection))))


;;;============================================================

(defun find-eval-server (connection)
  "Announce that we're looking for an eval server."
  (declare (type Eval-Client connection))
  (send-message connection
		(make-message
		 (list (intern-identifier "cl-eval-service-request")
		       (identifier connection)
		       (intern-identifier "*")
		       (make-identifier "cl-eval-service-request")))))

(defmethod connect :after ((connection Eval-Client))
  "Find an eval server."
  (declare (type Eval-Client connection))
  (find-eval-server connection))

;;;============================================================

(defun send-eval-request (connection expression)
  "Send an expression to be evaluated."
  (declare (type Eval-Client connection))
  (send-message connection
		(make-message
		 (list (intern-identifier "eval")
		       (identifier connection)
		       (eval-server-id connection)
		       (make-identifier "eval"))
		 expression)))

;;;============================================================


(defmethod handle-cl-eval-service-request-response ((connection Eval-Client)
						    message)
  (when (null (eval-server-id connection))
	(format t "~&setting server id: ~s~%" (message-sender message))
	(setf (eval-server-id connection) (message-sender message))))

(defmethod handle-eval-response ((connection Eval-Client) message)
  "Just print it."
  (format t "~&eval response: ~%~s~%~s~%~s~%~%~s~%"
	  message
	  (message-header message)
	  (message-body message)
	  (second (message-body message)))
  (force-output))

(defmethod handle-response ((connection Eval-Client) message)
  (declare (type Eval-Client connection)
	   (type Message message))
  (let* ((body (message-body message))
	 (original-header (first body))
	 (original-message-name (intern-identifier (first original-header))))
    (case
     original-message-name
     (:|cl-eval-service-request|
      (handle-cl-eval-service-request-response connection message))
     (:|eval|
      (handle-eval-response connection message))
     (otherwise
      (warn "~&don't understand response message: ~%~s~%~s~%~s,~%ignoring it~%"
	    message
	    (message-header message)
	    (message-body message))))))


(defmethod handle-message ((connection Eval-Client) message)
  (declare (type Eval-Client connection)
	   (type Message message))
  (case
   (message-name message)
   ((:|describe| :|cl-eval-service-request|)
    (respond connection message (make-describe-response connection message)))
   (:|disconnect| (disconnect connection))
   (:|response| (handle-response connection message))
   (:|error| (warn "~&~s ~%doesn't know how to handle ~%~s, ~%ignoring it.~%"
		   connection message))
   (otherwise (warn "~&~s ~%doesn't know how to handle ~%~s, ~%ignoring it.~%"
		    connection message))))

;;;============================================================

(defun make-eval-client (&key
			 (host (message-server-host))
			 (port (message-server-port)))
  (make-instance 'Eval-Client :server-host host :server-port port))



