;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-

(in-package :cl-user)

;;;============================================================

(when (find-package :WT) (funcall (intern :composer :wt)))  

#+:cmu
(alien:load-foreign
 (namestring
  (truename
   (pathname
    (concatenate 'String
		 (namestring 
		  (make-pathname
		   :directory (pathname-directory *load-truename*)))
		 "wrench.o")))))

(load (truename
       (pathname
	(concatenate 'String
		     (namestring 
		      (make-pathname
		       :directory (pathname-directory *load-truename*)))
		     "../az.system"))))

(mk:load-system :Msg)

