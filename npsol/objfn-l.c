#include "/usr/local/franz/lib/misc/lisp.h

/*-----------------------------------------------------------------------
** set_npsol_objective_index sets the global variable,
** npsol_objective_index, to the index of a registered lisp
** function meant to be the objective function passed to npsol
**-----------------------------------------------------------------------*/

int npsol_objective_index;

set_npsol_objective_index (index)
int index;
{
npsol_objective_index = index;
}

/*-----------------------------------------------------------------------
** set_npsol_objective_index sets the global variable,
** npsol_objective_index, to the index of a registered lisp
** function meant to be the objective function passed to npsol
**-----------------------------------------------------------------------*/

int npsol_constraint_index;

set_npsol_constraint_index (index)
int index;
{
npsol_constraint_index = index;
}

/*-----------------------------------------------------------------------
** objfnl_ calls the lisp function whose index is in npsol_objective_index.
**-----------------------------------------------------------------------*/

objfn1_ (mode, n, x, objf, objgrd, nstate)
int *mode;
int *n;
double *x, *objf, *objgrd;
int *nstate;
{
lisp_call(npsol_objective_index,
          mode, n, x, objf, objgrd, nstate);
} 


/*-----------------------------------------------------------------------
** confnl_ calls the lisp function whose index is in npsol_constraint_index.
**-----------------------------------------------------------------------*/


confn1_ (mode, ncnln, n, nrowj, needc, x, c, cjac, nstate)

int *mode;
int *ncnln, *n, *nrowj, *needc;
double *x, *c, *cjac;
int *nstate;
{
lisp_call(npsol_constraint_index,
          mode, ncnln, n, nrowj, needc, x, c, cjac, nstate);
}

/*-----------------------------------------------------------------------
** objfnl_ calls the lisp function whose index is in npsol_objective_index.
**-----------------------------------------------------------------------*/

objfn2_ (mode, n, x, objf, objgrd, nstate)
int *mode;
int *n;
double *x, *objf, *objgrd;
int *nstate;
{
lisp_call(npsol_objective_index,
          mode, n, x, objf, objgrd, nstate);
} 


/*-----------------------------------------------------------------------
** confnl_ calls the lisp function whose index is in npsol_constraint_index.
**-----------------------------------------------------------------------*/


confn2_ (mode, ncnln, n, nrowj, needc, x, c, cjac, nstate)

int *mode;
int *ncnln, *n, *nrowj, *needc;
double *x, *c, *cjac;
int *nstate;
{
lisp_call(npsol_constraint_index,
          mode, ncnln, n, nrowj, needc, x, c, cjac, nstate);
}