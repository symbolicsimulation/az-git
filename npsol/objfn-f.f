C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE OBJFN1( MODE, N, X, OBJF, OBJGRD, NSTATE )
      IMPLICIT           DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION   X(N), OBJGRD(N)

C-----------------------------------------------------------------------
C     OBJFN1  computes the value and first derivatives of the nonlinear
c     objective function.
C-----------------------------------------------------------------------
      OBJF   = - X(2)*X(6) + X(1)*X(7) - X(3)*X(7) - X(5)*X(8)
     $         + X(4)*X(9) + X(3)*X(8)

      OBJGRD(1) =   X(7)
      OBJGRD(2) = - X(6)
      OBJGRD(3) = - X(7) + X(8)
      OBJGRD(4) =   X(9)
      OBJGRD(5) = - X(8)
      OBJGRD(6) = - X(2)
      OBJGRD(7) = - X(3) + X(1)
      OBJGRD(8) = - X(5) + X(3)
      OBJGRD(9) =   X(4)

      RETURN

C     End of  OBJFN1.

      END
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CONFN1( MODE, NCNLN, N, NROWJ,
     $                   NEEDC, X, C, CJAC, NSTATE )

      IMPLICIT           DOUBLE PRECISION(A-H,O-Z)
      INTEGER            NEEDC(*)
      DOUBLE PRECISION   X(N), C(*), CJAC(NROWJ,*)

C-----------------------------------------------------------------------
C     CONFN1  computes the values and first derivatives of the nonlinear
c     constraints.
C
C     The zero elements of Jacobian matrix are set only once.  This
c     occurs during the first call to CONFN1  (NSTATE = 1).
C-----------------------------------------------------------------------
      PARAMETER         (ZERO = 0.0, TWO = 2.0)

      IF (NSTATE .EQ. 1) THEN

C        First call to CONFN1.  Set all Jacobian elements to zero.
C        N.B.  This will only work with `Derivative Level = 3'.

         DO 120 J = 1, N
            DO 110 I = 1, NCNLN
               CJAC(I,J) = ZERO
  110       CONTINUE
  120    CONTINUE

      ENDIF

      IF (NEEDC(1) .GT. 0) THEN
         C(1)       =   X(1)**2  +  X(6)**2
         CJAC(1,1)  =   TWO*X(1)
         CJAC(1,6)  =   TWO*X(6)
      ENDIF

      IF (NEEDC(2) .GT. 0) THEN
         C(2)       =   (X(2) - X(1))**2  +  (X(7) - X(6))**2
         CJAC(2,1)  = - TWO*(X(2) - X(1))
         CJAC(2,2)  =   TWO*(X(2) - X(1))
         CJAC(2,6)  = - TWO*(X(7) - X(6))
         CJAC(2,7)  =   TWO*(X(7) - X(6))
      ENDIF

      IF (NEEDC(3) .GT. 0) THEN
         C(3)       =   (X(3) - X(1))**2  +  X(6)**2
         CJAC(3,1)  = - TWO*(X(3) - X(1))
         CJAC(3,3)  =   TWO*(X(3) - X(1))
         CJAC(3,6)  =   TWO*X(6)
      ENDIF

      IF (NEEDC(4) .GT. 0) THEN
         C(4)       =   (X(1) - X(4))**2  +  (X(6) - X(8))**2
         CJAC(4,1)  =   TWO*(X(1) - X(4))
         CJAC(4,4)  = - TWO*(X(1) - X(4))
         CJAC(4,6)  =   TWO*(X(6) - X(8))
         CJAC(4,8)  = - TWO*(X(6) - X(8))
      ENDIF

      IF (NEEDC(5) .GT. 0) THEN
         C(5)       =   (X(1) - X(5))**2  +  (X(6) - X(9))**2
         CJAC(5,1)  =   TWO*(X(1) - X(5))
         CJAC(5,5)  = - TWO*(X(1) - X(5))
         CJAC(5,6)  =   TWO*(X(6) - X(9))
         CJAC(5,9)  = - TWO*(X(6) - X(9))
      ENDIF

      IF (NEEDC(6) .GT. 0) THEN
         C(6)       =   X(2)**2  +  X(7)**2
         CJAC(6,2)  =   TWO*X(2)
         CJAC(6,7)  =   TWO*X(7)
      ENDIF

      IF (NEEDC(7) .GT. 0) THEN
         C(7)       =   (X(3) - X(2))**2  +  X(7)**2
         CJAC(7,2)  = - TWO*(X(3) - X(2))
         CJAC(7,3)  =   TWO*(X(3) - X(2))
         CJAC(7,7)  =   TWO*X(7)
      ENDIF

      IF (NEEDC(8) .GT. 0) THEN
         C(8)       =   (X(4) - X(2))**2  +  (X(8) - X(7))**2
         CJAC(8,2)  = - TWO*(X(4) - X(2))
         CJAC(8,4)  =   TWO*(X(4) - X(2))
         CJAC(8,7)  = - TWO*(X(8) - X(7))
         CJAC(8,8)  =   TWO*(X(8) - X(7))
      ENDIF

      IF (NEEDC(9) .GT. 0) THEN
         C(9)       =   (X(2) - X(5))**2  +  (X(7) - X(9))**2
         CJAC(9,2)  =   TWO*(X(2) - X(5))
         CJAC(9,5)  = - TWO*(X(2) - X(5))
         CJAC(9,7)  =   TWO*(X(7) - X(9))
         CJAC(9,9)  = - TWO*(X(7) - X(9))
      ENDIF

      IF (NEEDC(10) .GT. 0) THEN
         C(10)      =   (X(4) - X(3))**2  +  X(8)**2
         CJAC(10,3) = - TWO*(X(4) - X(3))
         CJAC(10,4) =   TWO*(X(4) - X(3))
         CJAC(10,8) =   TWO*X(8)
      ENDIF

      IF (NEEDC(11) .GT. 0) THEN
         C(11)      =   (X(5) - X(3))**2  +  X(9)**2
         CJAC(11,3) = - TWO*(X(5) - X(3))
         CJAC(11,5) =   TWO*(X(5) - X(3))
         CJAC(11,9) =   TWO*X(9)
      ENDIF

      IF (NEEDC(12) .GT. 0) THEN
         C(12)      =   X(4)**2  +  X(8)**2
         CJAC(12,4) =   TWO*X(4)
         CJAC(12,8) =   TWO*X(8)
      ENDIF

      IF (NEEDC(13) .GT. 0) THEN
         C(13)      =   (X(4) - X(5))**2  +  (X(9) - X(8))**2
         CJAC(13,4) =   TWO*(X(4) - X(5))
         CJAC(13,5) = - TWO*(X(4) - X(5))
         CJAC(13,8) = - TWO*(X(9) - X(8))
         CJAC(13,9) =   TWO*(X(9) - X(8))
      ENDIF

      IF (NEEDC(14) .GT. 0) THEN
         C(14)      =   X(5)**2  +  X(9)**2
         CJAC(14,5) =   TWO*X(5)
         CJAC(14,9) =   TWO*X(9)
      ENDIF

      RETURN

C     End of  CONFN1.

      END
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE OBJFN2( MODE, N, X, OBJF, OBJGRD, NSTATE )
      IMPLICIT           DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION   X(N), OBJGRD(N)

C-----------------------------------------------------------------------
C     OBJFN2  computes the value and some first derivatives of the
c     nonlinear objective function.
C-----------------------------------------------------------------------

      OBJF   = - X(2)*X(6) + X(1)*X(7) - X(3)*X(7) - X(5)*X(8)
     $         + X(4)*X(9) + X(3)*X(8)

      OBJGRD(3) = - X(7) + X(8)
      OBJGRD(7) = - X(3) + X(1)
      OBJGRD(8) = - X(5) + X(3)

      RETURN

C     End of  OBJFN2.

      END
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CONFN2( MODE, NCNLN, N, NROWJ,
     $                   NEEDC, X, C, CJAC, NSTATE )

      IMPLICIT           DOUBLE PRECISION(A-H,O-Z)
      INTEGER            NEEDC(*)
      DOUBLE PRECISION   X(N), C(*), CJAC(NROWJ,*)

C-----------------------------------------------------------------------
C     CONFN2  computes the values and the non-constant derivatives of
c     the nonlinear constraints.
C-----------------------------------------------------------------------
      PARAMETER         (TWO = 2.0)

      IF (NEEDC(1) .GT. 0) THEN
         C(1)       =   X(1)**2  +  X(6)**2
         CJAC(1,1)  =   TWO*X(1)
         CJAC(1,6)  =   TWO*X(6)
      ENDIF

      IF (NEEDC(2) .GT. 0) THEN
         C(2)       =   (X(2) - X(1))**2  +  (X(7) - X(6))**2
         CJAC(2,1)  = - TWO*(X(2) - X(1))
         CJAC(2,2)  =   TWO*(X(2) - X(1))
         CJAC(2,6)  = - TWO*(X(7) - X(6))
         CJAC(2,7)  =   TWO*(X(7) - X(6))
      ENDIF

      IF (NEEDC(3) .GT. 0) THEN
         C(3)       =   (X(3) - X(1))**2  +  X(6)**2
         CJAC(3,1)  = - TWO*(X(3) - X(1))
         CJAC(3,3)  =   TWO*(X(3) - X(1))
         CJAC(3,6)  =   TWO*X(6)
      ENDIF

      IF (NEEDC(4) .GT. 0) THEN
         C(4)       =   (X(1) - X(4))**2  +  (X(6) - X(8))**2
         CJAC(4,1)  =   TWO*(X(1) - X(4))
         CJAC(4,4)  = - TWO*(X(1) - X(4))
         CJAC(4,6)  =   TWO*(X(6) - X(8))
         CJAC(4,8)  = - TWO*(X(6) - X(8))
      ENDIF

      IF (NEEDC(5) .GT. 0) THEN
         C(5)       =   (X(1) - X(5))**2  +  (X(6) - X(9))**2
         CJAC(5,1)  =   TWO*(X(1) - X(5))
         CJAC(5,5)  = - TWO*(X(1) - X(5))
         CJAC(5,6)  =   TWO*(X(6) - X(9))
         CJAC(5,9)  = - TWO*(X(6) - X(9))
      ENDIF

      IF (NEEDC(6) .GT. 0) THEN
         C(6)       =   X(2)**2  +  X(7)**2
         CJAC(6,2)  =   TWO*X(2)
         CJAC(6,7)  =   TWO*X(7)
      ENDIF

      IF (NEEDC(7) .GT. 0) THEN
         C(7)       =   (X(3) - X(2))**2  +  X(7)**2
         CJAC(7,2)  = - TWO*(X(3) - X(2))
         CJAC(7,3)  =   TWO*(X(3) - X(2))
         CJAC(7,7)  =   TWO*X(7)
      ENDIF

      IF (NEEDC(8) .GT. 0) THEN
         C(8)       =   (X(4) - X(2))**2  +  (X(8) - X(7))**2
         CJAC(8,2)  = - TWO*(X(4) - X(2))
         CJAC(8,4)  =   TWO*(X(4) - X(2))
         CJAC(8,7)  = - TWO*(X(8) - X(7))
         CJAC(8,8)  =   TWO*(X(8) - X(7))
      ENDIF

      IF (NEEDC(9) .GT. 0) THEN
         C(9)       =   (X(2) - X(5))**2  +  (X(7) - X(9))**2
         CJAC(9,2)  =   TWO*(X(2) - X(5))
         CJAC(9,5)  = - TWO*(X(2) - X(5))
         CJAC(9,7)  =   TWO*(X(7) - X(9))
         CJAC(9,9)  = - TWO*(X(7) - X(9))
      ENDIF

      IF (NEEDC(10) .GT. 0) THEN
         C(10)      =   (X(4) - X(3))**2  +  X(8)**2
         CJAC(10,3) = - TWO*(X(4) - X(3))
         CJAC(10,4) =   TWO*(X(4) - X(3))
         CJAC(10,8) =   TWO*X(8)
      ENDIF

      IF (NEEDC(11) .GT. 0) THEN
         C(11)      =   (X(5) - X(3))**2  +  X(9)**2
         CJAC(11,3) = - TWO*(X(5) - X(3))
         CJAC(11,5) =   TWO*(X(5) - X(3))
         CJAC(11,9) =   TWO*X(9)
      ENDIF

      IF (NEEDC(12) .GT. 0) THEN
         C(12)      =   X(4)**2  +  X(8)**2
         CJAC(12,4) =   TWO*X(4)
         CJAC(12,8) =   TWO*X(8)
      ENDIF

      IF (NEEDC(13) .GT. 0) THEN
         C(13)      =   (X(4) - X(5))**2  +  (X(9) - X(8))**2
         CJAC(13,4) =   TWO*(X(4) - X(5))
         CJAC(13,5) = - TWO*(X(4) - X(5))
         CJAC(13,8) = - TWO*(X(9) - X(8))
         CJAC(13,9) =   TWO*(X(9) - X(8))
      ENDIF

      IF (NEEDC(14) .GT. 0) THEN
         C(14)      =   X(5)**2  +  X(9)**2
         CJAC(14,5) =   TWO*X(5)
         CJAC(14,9) =   TWO*X(9)
      ENDIF

      RETURN

C     End of  CONFN2.

      END
