/*  -- translated by f2c (version of 26 July 1990  10:54:47).
   You must link the resulting object file with the libraries:
	-lF77 -lI77 -lm -lc   (in that order)
*/

#include "f2c.h"

/*-----------------------------------------------------------------------*/
/*     OBJFN1  computes the value and first derivatives of the nonlinear */
/*     objective function. */
/*-----------------------------------------------------------------------*/

int objfn1_(mode, n, x, objf, objgrd, nstate)
integer *mode;
integer *n;
doublereal *x, *objf, *objgrd;
integer *nstate;
{
    /* Parameter adjustments */
    --objgrd;
    --x;

    /* Function Body */
    *objf = -x[2] * x[6]
	   + x[1] * x[7]
	   - x[3] * x[7]
	   - x[5] * x[8]
	   + x[4] * x[9] 
           + x[3] * x[8];
    objgrd[1] = x[7];
    objgrd[2] = -x[6];
    objgrd[3] = -x[7] + x[8];
    objgrd[4] = x[9];
    objgrd[5] = -x[8];
    objgrd[6] = -x[2];
    objgrd[7] = -x[3] + x[1];
    objgrd[8] = -x[5] + x[3];
    objgrd[9] = x[4];
    return 0;

} /* objfn1_ */

/*-----------------------------------------------------------------------*/
/* Subroutine */ 

int confn1_(mode, ncnln, n, nrowj, needc, x, c, cjac, nstate)

integer *mode;
integer *ncnln, *n, *nrowj, *needc;
doublereal *x, *c, *cjac;
integer *nstate;
{
    /* System generated locals */
    integer cjac_dim1, cjac_offset, i_1, i_2;
    doublereal d_1, d_2;

    /* Local variables */ static integer i, j;

/* -----------------------------------------------------------------------
 */
/*     CONFN1  computes the values and first derivatives of the nonlinear 
*/
/*     constraints. */

/*     The zero elements of Jacobian matrix are set only once.  This */
/*     occurs during the first call to CONFN1  (NSTATE = 1). */
/* -----------------------------------------------------------------------
 */
    /* Parameter adjustments */
    cjac_dim1 = *nrowj;
    cjac_offset = cjac_dim1 + 1;
    cjac -= cjac_offset;
    --c;
    --x;
    --needc;

    /* Function Body */
    if (*nstate == 1) {
/*        First call to CONFN1.  Set all Jacobian elements to zero. */

/*        N.B.  This will only work with `Derivative Level = 3'. */
	i_1 = *n;
	for (j = 1; j <= i_1; ++j) {
	    i_2 = *ncnln;
	    for (i = 1; i <= i_2; ++i) {
		cjac[i + j * cjac_dim1] = 0.;
/* L110: */
	    }
/* L120: */
	}
    }
    if (needc[1] > 0) {
/* Computing 2nd power */
	d_1 = x[1];
/* Computing 2nd power */
	d_2 = x[6];
	c[1] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 1] = x[1] * 2.;
	cjac[cjac_dim1 * 6 + 1] = x[6] * 2.;
    }
    if (needc[2] > 0) {
/* Computing 2nd power */
	d_1 = x[2] - x[1];
/* Computing 2nd power */
	d_2 = x[7] - x[6];
	c[2] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 2] = (x[2] - x[1]) * -2.;
	cjac[(cjac_dim1 << 1) + 2] = (x[2] - x[1]) * 2.;
	cjac[cjac_dim1 * 6 + 2] = (x[7] - x[6]) * -2.;
	cjac[cjac_dim1 * 7 + 2] = (x[7] - x[6]) * 2.;
    }
    if (needc[3] > 0) {
/* Computing 2nd power */
	d_1 = x[3] - x[1];
/* Computing 2nd power */
	d_2 = x[6];
	c[3] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 3] = (x[3] - x[1]) * -2.;
	cjac[cjac_dim1 * 3 + 3] = (x[3] - x[1]) * 2.;
	cjac[cjac_dim1 * 6 + 3] = x[6] * 2.;
    }
    if (needc[4] > 0) {
/* Computing 2nd power */
	d_1 = x[1] - x[4];
/* Computing 2nd power */
	d_2 = x[6] - x[8];
	c[4] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 4] = (x[1] - x[4]) * 2.;
	cjac[(cjac_dim1 << 2) + 4] = (x[1] - x[4]) * -2.;
	cjac[cjac_dim1 * 6 + 4] = (x[6] - x[8]) * 2.;
	cjac[(cjac_dim1 << 3) + 4] = (x[6] - x[8]) * -2.;
    }
    if (needc[5] > 0) {
/* Computing 2nd power */
	d_1 = x[1] - x[5];
/* Computing 2nd power */
	d_2 = x[6] - x[9];
	c[5] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 5] = (x[1] - x[5]) * 2.;
	cjac[cjac_dim1 * 5 + 5] = (x[1] - x[5]) * -2.;
	cjac[cjac_dim1 * 6 + 5] = (x[6] - x[9]) * 2.;
	cjac[cjac_dim1 * 9 + 5] = (x[6] - x[9]) * -2.;
    }
    if (needc[6] > 0) {
/* Computing 2nd power */
	d_1 = x[2];
/* Computing 2nd power */
	d_2 = x[7];
	c[6] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 6] = x[2] * 2.;
	cjac[cjac_dim1 * 7 + 6] = x[7] * 2.;
    }
    if (needc[7] > 0) {
/* Computing 2nd power */
	d_1 = x[3] - x[2];
/* Computing 2nd power */
	d_2 = x[7];
	c[7] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 7] = (x[3] - x[2]) * -2.;
	cjac[cjac_dim1 * 3 + 7] = (x[3] - x[2]) * 2.;
	cjac[cjac_dim1 * 7 + 7] = x[7] * 2.;
    }
    if (needc[8] > 0) {
/* Computing 2nd power */
	d_1 = x[4] - x[2];
/* Computing 2nd power */
	d_2 = x[8] - x[7];
	c[8] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 8] = (x[4] - x[2]) * -2.;
	cjac[(cjac_dim1 << 2) + 8] = (x[4] - x[2]) * 2.;
	cjac[cjac_dim1 * 7 + 8] = (x[8] - x[7]) * -2.;
	cjac[(cjac_dim1 << 3) + 8] = (x[8] - x[7]) * 2.;
    }
    if (needc[9] > 0) {
/* Computing 2nd power */
	d_1 = x[2] - x[5];
/* Computing 2nd power */
	d_2 = x[7] - x[9];
	c[9] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 9] = (x[2] - x[5]) * 2.;
	cjac[cjac_dim1 * 5 + 9] = (x[2] - x[5]) * -2.;
	cjac[cjac_dim1 * 7 + 9] = (x[7] - x[9]) * 2.;
	cjac[cjac_dim1 * 9 + 9] = (x[7] - x[9]) * -2.;
    }
    if (needc[10] > 0) {
/* Computing 2nd power */
	d_1 = x[4] - x[3];
/* Computing 2nd power */
	d_2 = x[8];
	c[10] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 * 3 + 10] = (x[4] - x[3]) * -2.;
	cjac[(cjac_dim1 << 2) + 10] = (x[4] - x[3]) * 2.;
	cjac[(cjac_dim1 << 3) + 10] = x[8] * 2.;
    }
    if (needc[11] > 0) {
/* Computing 2nd power */
	d_1 = x[5] - x[3];
/* Computing 2nd power */
	d_2 = x[9];
	c[11] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 * 3 + 11] = (x[5] - x[3]) * -2.;
	cjac[cjac_dim1 * 5 + 11] = (x[5] - x[3]) * 2.;
	cjac[cjac_dim1 * 9 + 11] = x[9] * 2.;
    }
    if (needc[12] > 0) {
/* Computing 2nd power */
	d_1 = x[4];
/* Computing 2nd power */
	d_2 = x[8];
	c[12] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 2) + 12] = x[4] * 2.;
	cjac[(cjac_dim1 << 3) + 12] = x[8] * 2.;
    }
    if (needc[13] > 0) {
/* Computing 2nd power */
	d_1 = x[4] - x[5];
/* Computing 2nd power */
	d_2 = x[9] - x[8];
	c[13] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 2) + 13] = (x[4] - x[5]) * 2.;
	cjac[cjac_dim1 * 5 + 13] = (x[4] - x[5]) * -2.;
	cjac[(cjac_dim1 << 3) + 13] = (x[9] - x[8]) * -2.;
	cjac[cjac_dim1 * 9 + 13] = (x[9] - x[8]) * 2.;
    }
    if (needc[14] > 0) {
/* Computing 2nd power */
	d_1 = x[5];
/* Computing 2nd power */
	d_2 = x[9];
	c[14] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 * 5 + 14] = x[5] * 2.;
	cjac[cjac_dim1 * 9 + 14] = x[9] * 2.;
    }
    return 0;
/*     End of  CONFN1. */
} /* confn1_ */

/*-----------------------------------------------------------------------*/
/*     OBJFN2  computes the value and some first derivatives of the      */
/*     nonlinear objective function.                                     */
/*-----------------------------------------------------------------------*/

int objfn2_(mode, n, x, objf, objgrd, nstate)
integer *mode;
integer *n;
doublereal *x, *objf, *objgrd;
integer *nstate;
{
    /* Parameter adjustments */
    --objgrd;
    --x;

    /* Function Body */
    *objf = -x[2] * x[6] + x[1] * x[7] - x[3] * x[7] - x[5] * x[8] + x[4] * x[
	    9] + x[3] * x[8];
    objgrd[3] = -x[7] + x[8];
    objgrd[7] = -x[3] + x[1];
    objgrd[8] = -x[5] + x[3];
    return 0;
/*     End of  OBJFN2. */
} /* objfn2_ */


int confn2_(mode, ncnln, n, nrowj, needc, x, c, cjac, nstate)

integer *mode, *ncnln;
integer *n, *nrowj, *needc;
doublereal *x, *c, *cjac;
integer *nstate;
{
    /* System generated locals */
    integer cjac_dim1, cjac_offset;
    doublereal d_1, d_2;

/* -----------------------------------------------------------------------
 */
/*     CONFN2  computes the values and the non-constant derivatives of */
/*     the nonlinear constraints. */
/* -----------------------------------------------------------------------
 */
    /* Parameter adjustments */
    cjac_dim1 = *nrowj;
    cjac_offset = cjac_dim1 + 1;
    cjac -= cjac_offset;
    --c;
    --x;
    --needc;

    /* Function Body */
    if (needc[1] > 0) {
/* Computing 2nd power */
	d_1 = x[1];
/* Computing 2nd power */
	d_2 = x[6];
	c[1] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 1] = x[1] * 2.;
	cjac[cjac_dim1 * 6 + 1] = x[6] * 2.;
    }
    if (needc[2] > 0) {
/* Computing 2nd power */
	d_1 = x[2] - x[1];
/* Computing 2nd power */
	d_2 = x[7] - x[6];
	c[2] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 2] = (x[2] - x[1]) * -2.;
	cjac[(cjac_dim1 << 1) + 2] = (x[2] - x[1]) * 2.;
	cjac[cjac_dim1 * 6 + 2] = (x[7] - x[6]) * -2.;
	cjac[cjac_dim1 * 7 + 2] = (x[7] - x[6]) * 2.;
    }
    if (needc[3] > 0) {
/* Computing 2nd power */
	d_1 = x[3] - x[1];
/* Computing 2nd power */
	d_2 = x[6];
	c[3] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 3] = (x[3] - x[1]) * -2.;
	cjac[cjac_dim1 * 3 + 3] = (x[3] - x[1]) * 2.;
	cjac[cjac_dim1 * 6 + 3] = x[6] * 2.;
    }
    if (needc[4] > 0) {
/* Computing 2nd power */
	d_1 = x[1] - x[4];
/* Computing 2nd power */
	d_2 = x[6] - x[8];
	c[4] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 4] = (x[1] - x[4]) * 2.;
	cjac[(cjac_dim1 << 2) + 4] = (x[1] - x[4]) * -2.;
	cjac[cjac_dim1 * 6 + 4] = (x[6] - x[8]) * 2.;
	cjac[(cjac_dim1 << 3) + 4] = (x[6] - x[8]) * -2.;
    }
    if (needc[5] > 0) {
/* Computing 2nd power */
	d_1 = x[1] - x[5];
/* Computing 2nd power */
	d_2 = x[6] - x[9];
	c[5] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 + 5] = (x[1] - x[5]) * 2.;
	cjac[cjac_dim1 * 5 + 5] = (x[1] - x[5]) * -2.;
	cjac[cjac_dim1 * 6 + 5] = (x[6] - x[9]) * 2.;
	cjac[cjac_dim1 * 9 + 5] = (x[6] - x[9]) * -2.;
    }
    if (needc[6] > 0) {
/* Computing 2nd power */
	d_1 = x[2];
/* Computing 2nd power */
	d_2 = x[7];
	c[6] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 6] = x[2] * 2.;
	cjac[cjac_dim1 * 7 + 6] = x[7] * 2.;
    }
    if (needc[7] > 0) {
/* Computing 2nd power */
	d_1 = x[3] - x[2];
/* Computing 2nd power */
	d_2 = x[7];
	c[7] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 7] = (x[3] - x[2]) * -2.;
	cjac[cjac_dim1 * 3 + 7] = (x[3] - x[2]) * 2.;
	cjac[cjac_dim1 * 7 + 7] = x[7] * 2.;
    }
    if (needc[8] > 0) {
/* Computing 2nd power */
	d_1 = x[4] - x[2];
/* Computing 2nd power */
	d_2 = x[8] - x[7];
	c[8] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 8] = (x[4] - x[2]) * -2.;
	cjac[(cjac_dim1 << 2) + 8] = (x[4] - x[2]) * 2.;
	cjac[cjac_dim1 * 7 + 8] = (x[8] - x[7]) * -2.;
	cjac[(cjac_dim1 << 3) + 8] = (x[8] - x[7]) * 2.;
    }
    if (needc[9] > 0) {
/* Computing 2nd power */
	d_1 = x[2] - x[5];
/* Computing 2nd power */
	d_2 = x[7] - x[9];
	c[9] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 1) + 9] = (x[2] - x[5]) * 2.;
	cjac[cjac_dim1 * 5 + 9] = (x[2] - x[5]) * -2.;
	cjac[cjac_dim1 * 7 + 9] = (x[7] - x[9]) * 2.;
	cjac[cjac_dim1 * 9 + 9] = (x[7] - x[9]) * -2.;
    }
    if (needc[10] > 0) {
/* Computing 2nd power */
	d_1 = x[4] - x[3];
/* Computing 2nd power */
	d_2 = x[8];
	c[10] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 * 3 + 10] = (x[4] - x[3]) * -2.;
	cjac[(cjac_dim1 << 2) + 10] = (x[4] - x[3]) * 2.;
	cjac[(cjac_dim1 << 3) + 10] = x[8] * 2.;
    }
    if (needc[11] > 0) {
/* Computing 2nd power */
	d_1 = x[5] - x[3];
/* Computing 2nd power */
	d_2 = x[9];
	c[11] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 * 3 + 11] = (x[5] - x[3]) * -2.;
	cjac[cjac_dim1 * 5 + 11] = (x[5] - x[3]) * 2.;
	cjac[cjac_dim1 * 9 + 11] = x[9] * 2.;
    }
    if (needc[12] > 0) {
/* Computing 2nd power */
	d_1 = x[4];
/* Computing 2nd power */
	d_2 = x[8];
	c[12] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 2) + 12] = x[4] * 2.;
	cjac[(cjac_dim1 << 3) + 12] = x[8] * 2.;
    }
    if (needc[13] > 0) {
/* Computing 2nd power */
	d_1 = x[4] - x[5];
/* Computing 2nd power */
	d_2 = x[9] - x[8];
	c[13] = d_1 * d_1 + d_2 * d_2;
	cjac[(cjac_dim1 << 2) + 13] = (x[4] - x[5]) * 2.;
	cjac[cjac_dim1 * 5 + 13] = (x[4] - x[5]) * -2.;
	cjac[(cjac_dim1 << 3) + 13] = (x[9] - x[8]) * -2.;
	cjac[cjac_dim1 * 9 + 13] = (x[9] - x[8]) * 2.;
    }
    if (needc[14] > 0) {
/* Computing 2nd power */
	d_1 = x[5];
/* Computing 2nd power */
	d_2 = x[9];
	c[14] = d_1 * d_1 + d_2 * d_2;
	cjac[cjac_dim1 * 5 + 14] = x[5] * 2.;
	cjac[cjac_dim1 * 9 + 14] = x[9] * 2.;
    }
    return 0;
/*     End of  CONFN2. */
} /* confn2_ */

#ifdef uNdEfInEd
comments from the converter:  (stderr from f2c)
   objfn1:
   confn1:
   objfn2:
   confn2:
#endif

