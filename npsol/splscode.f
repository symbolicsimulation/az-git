*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*     File  BLAS FORTRAN
*
*                         Level 1  BLAS
*                         -------  ----
*     SAXPY    SCOPY    SDOT     SNRM2    SSCAL    SSWAP    ISAMAX
*
*                         Level 2  BLAS
*                         -------  ----
*     SGEMV    SGER     SSYMV    SSYR     STRMV    STRSV    LEMAS
*     XALBRE
*                         Others
*                         ------
*     SCOND*   SSDIV*   SDIV     SDSCL    SGRFG    SLOAD    SNORM
*     SROT3*   SROT3G*  SSSQ     IYPOC*   DAOLI    ISRANK+
*
*    *Not in the Nag Blas.
*    +Differs from the Nag Blas.
*
*                         QR Routines
*                         -- --------
*     SGEQR    SGEQRP   SGEAP    SGEAPQ
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SAXPY ( N, ALPHA, X, INCX, Y, INCY )
      INTEGER            N, INCX, INCY
      REAL               ALPHA
      REAL               X( * ), Y( * )

C  SAXPY  performs the operation
C
C     y := alpha*x + y
C
C
C  Nag Fortran 77 version of the Blas routine SAXPY .
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 3-September-1982.
C     Sven Hammarling, Nag Central Office.

      INTEGER            I     , IX    , IY
      REAL               ZERO
      PARAMETER        ( ZERO  = 0.0E+0 )

      IF( N    .LT.1    )RETURN
      IF( ALPHA.EQ.ZERO )RETURN

      IF( ( INCX.EQ.INCY ).AND.( INCX.GT.0 ) )THEN
         DO 10, IX = 1, 1 + ( N - 1 )*INCX, INCX
            Y( IX ) = ALPHA*X( IX ) + Y( IX )
   10    CONTINUE
      ELSE
         IF( INCY.GE.0 )THEN
            IY = 1
         ELSE
            IY = 1 - ( N - 1 )*INCY
         END IF
         IF( INCX.GT.0 )THEN
            DO 20, IX = 1, 1 + ( N - 1 )*INCX, INCX
               Y( IY ) = ALPHA*X( IX ) + Y( IY )
               IY      = IY + INCY
   20       CONTINUE
         ELSE
            IX = 1 - ( N - 1 )*INCX
            DO 30, I = 1, N
               Y( IY ) = ALPHA*X( IX ) + Y( IY )
               IX      = IX + INCX
               IY      = IY + INCY
   30       CONTINUE
         END IF
      END IF
      RETURN

*     End of SAXPY .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SCOPY ( N, X, INCX, Y, INCY )
      INTEGER            N, INCX, INCY
      REAL               X( * ), Y( * )

C  SCOPY  performs the operation
C
C     y := x
C
C
C  Nag Fortran 77 version of the Blas routine SCOPY .
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 26-November-1982.
C     Sven Hammarling, Nag Central Office.

      INTEGER            I     , IX    , IY

      IF( N.LT.1 )RETURN

      IF( ( INCX.EQ.INCY ).AND.( INCY.GT.0 ) )THEN
         DO 10, IY = 1, 1 + ( N - 1 )*INCY, INCY
            Y( IY ) = X( IY )
   10    CONTINUE
      ELSE
         IF( INCX.GE.0 )THEN
            IX = 1
         ELSE
            IX = 1 - ( N - 1 )*INCX
         END IF
         IF( INCY.GT.0 )THEN
            DO 20, IY = 1, 1 + ( N - 1 )*INCY, INCY
               Y( IY ) = X( IX )
               IX      = IX + INCX
   20       CONTINUE
         ELSE
            IY = 1 - ( N - 1 )*INCY
            DO 30, I = 1, N
               Y( IY ) = X( IX )
               IY      = IY + INCY
               IX      = IX + INCX
   30       CONTINUE
         END IF
      END IF

      RETURN

*     End of SCOPY .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      REAL             FUNCTION SDOT  ( N, X, INCX, Y, INCY )
      INTEGER                           N, INCX, INCY
      REAL                              X( * ), Y( * )

C  SDOT   returns the value
C
C     SDOT   = x'y
C
C
C  Nag Fortran 77 version of the Blas routine SDOT  .
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 21-September-1982.
C     Sven Hammarling, Nag Central Office.

      INTEGER             I     , IX    , IY
      REAL                SUM
      REAL                ZERO
      PARAMETER         ( ZERO  = 0.0E+0 )

      SUM = ZERO
      IF( N.GE.1 )THEN
         IF( ( INCX.EQ.INCY ).AND.( INCX.GT.0 ) )THEN
            DO 10, IX = 1, 1 + ( N - 1 )*INCX, INCX
               SUM = SUM + X( IX )*Y( IX )
   10       CONTINUE
         ELSE
            IF( INCY.GE.0 )THEN
               IY = 1
            ELSE
               IY = 1 - ( N - 1 )*INCY
            END IF
            IF( INCX.GT.0 )THEN
               DO 20, IX = 1, 1 + ( N - 1 )*INCX, INCX
                  SUM = SUM + X( IX )*Y( IY )
                  IY  = IY  + INCY
   20          CONTINUE
            ELSE
               IX = 1 - ( N - 1 )*INCX
               DO 30, I = 1, N
                  SUM = SUM + X( IX )*Y( IY )
                  IX  = IX  + INCX
                  IY  = IY  + INCY
   30          CONTINUE
            END IF
         END IF
      END IF

      SDOT   = SUM
      RETURN

*     End of SDOT  .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      REAL             FUNCTION SNRM2 ( N, X, INCX )
      INTEGER                           N, INCX
      REAL                              X( * )

C  SNRM2  returns the Euclidean norm of a vector via the function
C  name, so that
C
C     SNRM2  := sqrt( x'*x )
C
C
C  Nag Fortran 77 version of the Blas routine SNRM2 .
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 25-October-1982.
C     Sven Hammarling, Nag Central Office.

      EXTERNAL            SNORM , SSSQ
      INTRINSIC           ABS
      REAL                SCALE , SNORM , SSQ
      REAL                ONE   ,         ZERO
      PARAMETER         ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

      IF( N.LT.1 )THEN
         SNRM2  = ZERO
      ELSE IF( N.EQ.1 )THEN
         SNRM2  = ABS( X( 1 ) )
      ELSE
         SCALE  = ZERO
         SSQ    = ONE

         CALL SSSQ  ( N, X, INCX, SCALE, SSQ )

         SNRM2  = SNORM ( SCALE, SSQ )

      END IF
      RETURN

*     End of SNRM2 .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SSCAL ( N, ALPHA, X, INCX )
      INTEGER            N, INCX
      REAL               ALPHA
      REAL               X( * )

C  SSCAL  performs the operation
C
C     x := alpha*x
C
C
C  Nag Fortran 77 version of the Blas routine SSCAL .
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 26-November-1982.
C     Sven Hammarling, Nag Central Office.

      INTEGER            IX
      REAL               ONE   ,         ZERO
      PARAMETER        ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

      IF( N.GE.1 )THEN
         IF( ALPHA.EQ.ZERO )THEN
            DO 10, IX = 1, 1 + ( N - 1 )*INCX, INCX
               X( IX ) = ZERO
   10       CONTINUE
         ELSE IF( ALPHA.EQ.( -ONE ) )THEN
            DO 20, IX = 1, 1 + ( N - 1 )*INCX, INCX
               X( IX ) = -X( IX )
   20       CONTINUE
         ELSE IF( ALPHA.NE.ONE )THEN
            DO 30, IX = 1, 1 + ( N - 1 )*INCX, INCX
               X( IX ) = ALPHA*X( IX )
   30       CONTINUE
         END IF
      END IF

      RETURN

*     End of SSCAL .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SSWAP ( N, X, INCX, Y, INCY )
      INTEGER            N, INCX, INCY
      REAL               X( * ), Y( * )

C  SSWAP  performs the operations
C
C     temp := x,   x := y,   y := temp.
C
C
C  Nag Fortran 77 version of the Blas routine SSWAP .
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 26-November-1982.
C     Sven Hammarling, Nag Central Office.

      INTEGER            I     , IX    , IY
      REAL               TEMP

      IF( N.LT.1 )RETURN

      IF( ( INCX.EQ.INCY ).AND.( INCY.GT.0 ) )THEN
         DO 10, IY = 1, 1 + ( N - 1 )*INCY, INCY
            TEMP    = X( IY )
            X( IY ) = Y( IY )
            Y( IY ) = TEMP
   10    CONTINUE
      ELSE
         IF( INCX.GE.0 )THEN
            IX = 1
         ELSE
            IX = 1 - ( N - 1 )*INCX
         END IF
         IF( INCY.GT.0 )THEN
            DO 20, IY = 1, 1 + ( N - 1 )*INCY, INCY
               TEMP    = X( IX )
               X( IX ) = Y( IY )
               Y( IY ) = TEMP
               IX      = IX + INCX
   20       CONTINUE
         ELSE
            IY = 1 - ( N - 1 )*INCY
            DO 30, I = 1, N
               TEMP    = X( IX )
               X( IX ) = Y( IY )
               Y( IY ) = TEMP
               IY      = IY + INCY
               IX      = IX + INCX
   30       CONTINUE
         END IF
      END IF

      RETURN

*     End of SSWAP .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INTEGER FUNCTION ISAMAX( N, X, INCX )
      INTEGER                  N, INCX
      REAL                     X( * )

C  ISAMAX returns the smallest value of i such that
C
C     abs( x( i ) ) = max( abs( x( j ) ) )
C                      j
C
C  Nag Fortran 77 version of the Blas routine ISAMAX.
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 31-May-1983.
C     Sven Hammarling, Nag Central Office.

      INTRINSIC           ABS
      INTEGER             I     , IMAX  , IX
      REAL                XMAX

      IF( N.LT.1 )THEN
         ISAMAX = 0
         RETURN
      END IF

      IMAX = 1
      IF( N.GT.1 )THEN
         XMAX = ABS( X( 1 ) )
         IX   = 1
         DO 10, I = 2, N
            IX = IX + INCX
            IF( XMAX.LT.ABS( X( IX ) ) )THEN
               XMAX = ABS( X( IX ) )
               IMAX = I
            END IF
   10    CONTINUE
      END IF

      ISAMAX = IMAX
      RETURN

*     End of ISAMAX.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SGEMV ( TRANS, M, N, ALPHA, A, LDA, X, INCX,
     $                   BETA, Y, INCY )
*     .. Scalar Arguments ..
      REAL               ALPHA, BETA
      INTEGER            INCX, INCY, LDA, M, N
      CHARACTER*1        TRANS
*     .. Array Arguments ..
      REAL               A( LDA, * ), X( * ), Y( * )
*     ..
*
*  Purpose
*  =======
*
*  SGEMV  performs one of the matrix-vector operations
*
*     y := alpha*A*x + beta*y,   or   y := alpha*A'*x + beta*y,
*
*  where alpha and beta are scalars, x and y are vectors and A is an
*  m by n matrix.
*
*  Parameters
*  ==========
*
*  TRANS  - CHARACTER*1.
*           On entry, TRANS specifies the operation to be performed as
*           follows:
*
*              TRANS = 'N' or 'n'   y := alpha*A*x + beta*y.
*
*              TRANS = 'T' or 't'   y := alpha*A'*x + beta*y.
*
*              TRANS = 'C' or 'c'   y := alpha*A'*x + beta*y.
*
*           Unchanged on exit.
*
*  M      - INTEGER.
*           On entry, M specifies the number of rows of the matrix A.
*           M must be at least zero.
*           Unchanged on exit.
*
*  N      - INTEGER.
*           On entry, N specifies the number of columns of the matrix A.
*           N must be at least zero.
*           Unchanged on exit.
*
*  ALPHA  - REAL.
*           On entry, ALPHA specifies the scalar alpha.
*           Unchanged on exit.
*
*  A      - REAL array of DIMENSION ( LDA, n ).
*           Before entry, the leading m by n part of the array A must
*           contain the matrix of coefficients.
*           Unchanged on exit.
*
*  LDA    - INTEGER.
*           On entry, LDA specifies the first dimension of A as declared
*           in the calling (sub) program. LDA must be at least
*           max( 1, m ).
*           Unchanged on exit.
*
*  X      - REAL array of DIMENSION at least
*           ( 1 + ( n - 1 )*abs( INCX ) ) when TRANS = 'N' or 'n'
*           and at least
*           ( 1 + ( m - 1 )*abs( INCX ) ) otherwise.
*           Before entry, the incremented array X must contain the
*           vector x.
*           Unchanged on exit.
*
*  INCX   - INTEGER.
*           On entry, INCX specifies the increment for the elements of
*           X. INCX must not be zero.
*           Unchanged on exit.
*
*  BETA   - REAL.
*           On entry, BETA specifies the scalar beta. When BETA is
*           supplied as zero then Y need not be set on input.
*           Unchanged on exit.
*
*  Y      - REAL array of DIMENSION at least
*           ( 1 + ( m - 1 )*abs( INCY ) ) when TRANS = 'N' or 'n'
*           and at least
*           ( 1 + ( n - 1 )*abs( INCY ) ) otherwise.
*           Before entry with BETA non-zero, the incremented array Y
*           must contain the vector y. On exit, Y is overwritten by the
*           updated vector y.
*
*  INCY   - INTEGER.
*           On entry, INCY specifies the increment for the elements of
*           Y. INCY must not be zero.
*           Unchanged on exit.
*
*
*  Level 2 Blas routine.
*
*  -- Written on 20-July-1986.
*     Sven Hammarling, Nag Central Office.
*     Richard Hanson, Sandia National Labs.
*
*
*     .. Parameters ..
      REAL               ONE         , ZERO
      PARAMETER        ( ONE = 1.0E+0, ZERO = 0.0E+0 )
*     .. Local Scalars ..
      REAL               TEMP
      INTEGER            I, INFO, IX, IY, J, JX, JY, KX, KY, LENX, LENY
*     .. External Functions ..
      LOGICAL            LEMAS
      EXTERNAL           LEMAS
*     .. External Subroutines ..
      EXTERNAL           XALBRE
*     .. Intrinsic Functions ..
      INTRINSIC          MAX
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF      ( .NOT.LEMAS( TRANS, 'N' ).AND.
     $          .NOT.LEMAS( TRANS, 'T' ).AND.
     $          .NOT.LEMAS( TRANS, 'C' )      ) THEN
         INFO = 1
      ELSE IF ( M.LT.0 ) THEN
         INFO = 2
      ELSE IF ( N.LT.0 ) THEN
         INFO = 3
      ELSE IF ( LDA.LT.MAX(1,M) ) THEN
         INFO = 6
      ELSE IF ( INCX.EQ.0 ) THEN
         INFO = 8
      ELSE IF ( INCY.EQ.0 ) THEN
         INFO = 11
      END IF
      IF( INFO.NE.0 )THEN
         CALL XALBRE( 'SGEMV ', INFO )
         RETURN
      END IF
*
*     Quick return if possible.
*
      IF( ( M.EQ.0 ).OR.( N.EQ.0 ).OR.
     $    ( ( ALPHA.EQ.ZERO ).AND.( BETA.EQ.ONE ) ) )
     $   RETURN
*
*     Set LENX and LENY, the lengths of the vectors x and y.
*
      IF( LEMAS( TRANS, 'N' ) )THEN
         LENX = N
         LENY = M
      ELSE
         LENX = M
         LENY = N
      END IF
*
*     Start the operations. In this version the elements of A are
*     accessed sequentially with one pass through A.
*
*     First form  y := beta*y  and set up the start points in X and Y if
*     the increments are not both unity.
*
      IF( ( INCX.EQ.1 ).AND.( INCY.EQ.1 ) )THEN
         IF( BETA.NE.ONE )THEN
            IF( BETA.EQ.ZERO )THEN
               DO 10, I = 1, LENY
                  Y( I ) = ZERO
   10          CONTINUE
            ELSE
               DO 20, I = 1, LENY
                  Y( I ) = BETA*Y( I )
   20          CONTINUE
            END IF
         END IF
      ELSE
         IF( INCX.GT.0 )THEN
            KX = 1
         ELSE
            KX = 1 - ( LENX - 1 )*INCX
         END IF
         IF( INCY.GT.0 )THEN
            KY = 1
         ELSE
            KY = 1 - ( LENY - 1 )*INCY
         END IF
         IF( BETA.NE.ONE )THEN
            IY = KY
            IF( BETA.EQ.ZERO )THEN
               DO 30, I = 1, LENY
                  Y( IY ) = ZERO
                  IY      = IY   + INCY
   30          CONTINUE
            ELSE
               DO 40, I = 1, LENY
                  Y( IY ) = BETA*Y( IY )
                  IY      = IY           + INCY
   40          CONTINUE
            END IF
         END IF
      END IF
      IF( ALPHA.EQ.ZERO )
     $   RETURN
      IF( LEMAS( TRANS, 'N' ) )THEN
*
*        Form  y := alpha*A*x + y.
*
         IF( ( INCX.EQ.1 ).AND.( INCY.EQ.1 ) )THEN
            DO 60, J = 1, N
               IF( X( J ).NE.ZERO )THEN
                  TEMP = ALPHA*X( J )
                  DO 50, I = 1, M
                     Y( I ) = Y( I ) + TEMP*A( I, J )
   50             CONTINUE
               END IF
   60       CONTINUE
         ELSE
            JX = KX
            DO 80, J = 1, N
               IF( X( JX ).NE.ZERO )THEN
                  TEMP = ALPHA*X( JX )
                  IY   = KY
                  DO 70, I = 1, M
                     Y( IY ) = Y( IY ) + TEMP*A( I, J )
                     IY      = IY      + INCY
   70             CONTINUE
               END IF
               JX = JX + INCX
   80       CONTINUE
         END IF
      ELSE
*
*        Form  y := alpha*A'*x + y.
*
         IF( ( INCX.EQ.1 ).AND.( INCY.EQ.1 ) )THEN
            DO 100, J = 1, N
               TEMP = ZERO
               DO 90, I = 1, M
                  TEMP = TEMP + A( I, J )*X( I )
   90          CONTINUE
               Y( J ) = Y( J ) + ALPHA*TEMP
  100       CONTINUE
         ELSE
            JY = KY
            DO 120, J = 1, N
               TEMP = ZERO
               IX   = KX
               DO 110, I = 1, M
                  TEMP = TEMP + A( I, J )*X( IX )
                  IX   = IX   + INCX
  110          CONTINUE
               Y( JY ) = Y( JY ) + ALPHA*TEMP
               JY      = JY      + INCY
  120       CONTINUE
         END IF
      END IF
*
      RETURN
*
*     End of SGEMV .
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SGER  ( M, N, ALPHA, X, INCX, Y, INCY, A, LDA )
*     .. Scalar Arguments ..
      REAL               ALPHA
      INTEGER            INCX, INCY, LDA, M, N
*     .. Array Arguments ..
      REAL               A( LDA, * ), X( * ), Y( * )
*     ..
*
*  Purpose
*  =======
*
*  SGER   performs the rank 1 operation
*
*     A := alpha*x*y' + A,
*
*  where alpha is a scalar, x is an m element vector, y is an n element
*  vector and A is an m by n matrix.
*
*  Parameters
*  ==========
*
*  M      - INTEGER.
*           On entry, M specifies the number of rows of the matrix A.
*           M must be at least zero.
*           Unchanged on exit.
*
*  N      - INTEGER.
*           On entry, N specifies the number of columns of the matrix A.
*           N must be at least zero.
*           Unchanged on exit.
*
*  ALPHA  - REAL.
*           On entry, ALPHA specifies the scalar alpha.
*           Unchanged on exit.
*
*  X      - REAL array of dimension at least
*           ( 1 + ( m - 1 )*abs( INCX ) ).
*           Before entry, the incremented array X must contain the m
*           element vector x.
*           Unchanged on exit.
*
*  INCX   - INTEGER.
*           On entry, INCX specifies the increment for the elements of
*           X. INCX must not be zero.
*           Unchanged on exit.
*
*  Y      - REAL array of dimension at least
*           ( 1 + ( n - 1 )*abs( INCY ) ).
*           Before entry, the incremented array Y must contain the n
*           element vector y.
*           Unchanged on exit.
*
*  INCY   - INTEGER.
*           On entry, INCY specifies the increment for the elements of
*           Y. INCY must not be zero.
*           Unchanged on exit.
*
*  A      - REAL array of DIMENSION ( LDA, n ).
*           Before entry, the leading m by n part of the array A must
*           contain the matrix of coefficients. On exit, A is
*           overwritten by the updated matrix.
*
*  LDA    - INTEGER.
*           On entry, LDA specifies the first dimension of A as declared
*           in the calling (sub) program. LDA must be at least
*           max( 1, m ).
*           Unchanged on exit.
*
*
*  Level 2 Blas routine.
*
*  -- Written on 20-July-1986.
*     Sven Hammarling, Nag Central Office.
*     Richard Hanson, Sandia National Labs.
*
*
*     .. Parameters ..
      REAL               ZERO
      PARAMETER        ( ZERO = 0.0E+0 )
*     .. Local Scalars ..
      REAL               TEMP
      INTEGER            I, INFO, IX, J, JY, KX
*     .. External Functions ..
      LOGICAL            LEMAS
      EXTERNAL           LEMAS
*     .. External Subroutines ..
      EXTERNAL           XALBRE
*     .. Intrinsic Functions ..
      INTRINSIC          MAX
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF      ( M.LT.0 ) THEN
         INFO = 1
      ELSE IF ( N.LT.0 ) THEN
         INFO = 2
      ELSE IF ( INCX.EQ.0 ) THEN
         INFO = 5
      ELSE IF ( INCY.EQ.0 ) THEN
         INFO = 7
      ELSE IF ( LDA.LT.MAX(1,M) ) THEN
         INFO = 9
      END IF
      IF( INFO.NE.0 )THEN
         CALL XALBRE( 'SGER  ', INFO )
         RETURN
      END IF
*
*     Quick return if possible.
*
      IF( ( M.EQ.0 ).OR.( N.EQ.0 ).OR.( ALPHA.EQ.ZERO ) )
     $   RETURN
*
*     Start the operations. In this version the elements of A are
*     accessed sequentially with one pass through A.
*
      IF( ( INCX.EQ.1 ).AND.( INCY.EQ.1 ) )THEN
         DO 20, J = 1, N
            IF( Y( J ).NE.ZERO )THEN
               TEMP = ALPHA*Y( J )
               DO 10, I = 1, M
                  A( I, J ) = A( I, J ) + X( I )*TEMP
   10          CONTINUE
            END IF
   20    CONTINUE
      ELSE
         IF( INCX.GT.0 )THEN
            KX = 1
         ELSE
            KX = 1 - ( M - 1 )*INCX
         END IF
         IF( INCY.GT.0 )THEN
            JY = 1
         ELSE
            JY = 1 - ( N - 1 )*INCY
         END IF
         DO 40, J = 1, N
            IF( Y( JY ).NE.ZERO )THEN
               TEMP = ALPHA*Y( JY )
               IX   = KX
               DO 30, I = 1, M
                  A( I, J ) = A( I, J ) + X( IX )*TEMP
                  IX        = IX        + INCX
   30          CONTINUE
            END IF
            JY = JY + INCY
   40    CONTINUE
      END IF
*
      RETURN
*
*     End of SGER  .
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SSYMV ( UPLO, N, ALPHA, A, LDA, X, INCX,
     $                   BETA, Y, INCY )
*     .. Scalar Arguments ..
      REAL               ALPHA, BETA
      INTEGER            INCX, INCY, LDA, N
      CHARACTER*1        UPLO
*     .. Array Arguments ..
      REAL               A( LDA, * ), X( * ), Y( * )
*     ..
*
*  Purpose
*  =======
*
*  SSYMV  performs the matrix-vector  operation
*
*     y := alpha*A*x + beta*y,
*
*  where alpha and beta are scalars, x and y are n element vectors and
*  A is an n by n symmetric matrix.
*
*  Parameters
*  ==========
*
*  UPLO   - CHARACTER*1.
*           On entry, UPLO specifies whether the upper or lower
*           triangular part of the array A is to be referenced as
*           follows:
*
*              UPLO = 'U' or 'u'   Only the upper triangular part of A
*                                  is to be referenced.
*
*              UPLO = 'L' or 'l'   Only the lower triangular part of A
*                                  is to be referenced.
*
*           Unchanged on exit.
*
*  N      - INTEGER.
*           On entry, N specifies the order of the matrix A.
*           N must be at least zero.
*           Unchanged on exit.
*
*  ALPHA  - REAL.
*           On entry, ALPHA specifies the scalar alpha.
*           Unchanged on exit.
*
*  A      - REAL array of DIMENSION ( LDA, n ).
*           Before entry with  UPLO = 'U' or 'u', the leading n by n
*           upper triangular part of the array A must contain the upper
*           triangular part of the symmetric matrix and the strictly
*           lower triangular part of A is not referenced.
*           Before entry with UPLO = 'L' or 'l', the leading n by n
*           lower triangular part of the array A must contain the lower
*           triangular part of the symmetric matrix and the strictly
*           upper triangular part of A is not referenced.
*           Unchanged on exit.
*
*  LDA    - INTEGER.
*           On entry, LDA specifies the first dimension of A as declared
*           in the calling (sub) program. LDA must be at least
*           max( 1, n ).
*           Unchanged on exit.
*
*  X      - REAL array of dimension at least
*           ( 1 + ( n - 1 )*abs( INCX ) ).
*           Before entry, the incremented array X must contain the n
*           element vector x.
*           Unchanged on exit.
*
*  INCX   - INTEGER.
*           On entry, INCX specifies the increment for the elements of
*           X. INCX must not be zero.
*           Unchanged on exit.
*
*  BETA   - REAL.
*           On entry, BETA specifies the scalar beta. When BETA is
*           supplied as zero then Y need not be set on input.
*           Unchanged on exit.
*
*  Y      - REAL array of dimension at least
*           ( 1 + ( n - 1 )*abs( INCY ) ).
*           Before entry, the incremented array Y must contain the n
*           element vector y. On exit, Y is overwritten by the updated
*           vector y.
*
*  INCY   - INTEGER.
*           On entry, INCY specifies the increment for the elements of
*           Y. INCY must not be zero.
*           Unchanged on exit.
*
*
*  Level 2 Blas routine.
*
*  -- Written on 20-July-1986.
*     Sven Hammarling, Nag Central Office.
*     Richard Hanson, Sandia National Labs.
*
*
*     .. Parameters ..
      REAL               ONE         , ZERO
      PARAMETER        ( ONE = 1.0E+0, ZERO = 0.0E+0 )
*     .. Local Scalars ..
      REAL               TEMP1, TEMP2
      INTEGER            I, INFO, IX, IY, J, JX, JY, KX, KY
*     .. External Functions ..
      LOGICAL            LEMAS
      EXTERNAL           LEMAS
*     .. External Subroutines ..
      EXTERNAL           XALBRE
*     .. Intrinsic Functions ..
      INTRINSIC          MAX
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF      ( .NOT.LEMAS( UPLO, 'U' ).AND.
     $          .NOT.LEMAS( UPLO, 'L' )      ) THEN
         INFO = 1
      ELSE IF ( N.LT.0 ) THEN
         INFO = 2
      ELSE IF ( LDA.LT.MAX(1,N) ) THEN
         INFO = 5
      ELSE IF ( INCX.EQ.0 ) THEN
         INFO = 7
      ELSE IF ( INCY.EQ.0 ) THEN
         INFO = 10
      END IF
      IF( INFO.NE.0 )THEN
         CALL XALBRE( 'SSYMV ', INFO )
         RETURN
      END IF
*
*     Quick return if possible.
*
      IF( ( N.EQ.0 ).OR.( ( ALPHA.EQ.ZERO ).AND.( BETA.EQ.ONE ) ) )
     $   RETURN
*
*     Start the operations. In this version the elements of A are
*     accessed sequentially with one pass through the triangular part
*     of A.
*
*     First form  y := beta*y  and set up the start points in X and Y if
*     the increments are not both unity.
*
      IF( ( INCX.EQ.1 ).AND.( INCY.EQ.1 ) )THEN
         IF( BETA.NE.ONE )THEN
            IF( BETA.EQ.ZERO )THEN
               DO 10, I = 1, N
                  Y( I ) = ZERO
   10          CONTINUE
            ELSE
               DO 20, I = 1, N
                  Y( I ) = BETA*Y( I )
   20          CONTINUE
            END IF
         END IF
      ELSE
         IF( INCX.GT.0 )THEN
            KX = 1
         ELSE
            KX = 1 - ( N - 1 )*INCX
         END IF
         IF( INCY.GT.0 )THEN
            KY = 1
         ELSE
            KY = 1 - ( N - 1 )*INCY
         END IF
         IF( BETA.NE.ONE )THEN
            IY = KY
            IF( BETA.EQ.ZERO )THEN
               DO 30, I = 1, N
                  Y( IY ) = ZERO
                  IY      = IY   + INCY
   30          CONTINUE
            ELSE
               DO 40, I = 1, N
                  Y( IY ) = BETA*Y( IY )
                  IY      = IY           + INCY
   40          CONTINUE
            END IF
         END IF
      END IF
      IF( ALPHA.EQ.ZERO )
     $   RETURN
      IF( LEMAS( UPLO, 'U' ) )THEN
*
*        Form  y  when A is stored in upper triangle.
*
         IF( ( INCX.EQ.1 ).AND.( INCY.EQ.1 ) )THEN
            DO 60, J = 1, N
               TEMP1 = ALPHA*X( J )
               TEMP2 = ZERO
               DO 50, I = 1, J - 1
                  Y( I ) = Y( I ) + TEMP1*A( I, J )
                  TEMP2  = TEMP2  + A( I, J )*X( I )
   50          CONTINUE
               Y( J ) = Y( J ) + TEMP1*A( J, J ) + ALPHA*TEMP2
   60       CONTINUE
         ELSE
            IX = KX - INCX
            DO 80, J = 1, N
               TEMP1 = ALPHA*X( IX + INCX )
               TEMP2 = ZERO
               IX    = KX
               IY    = KY
               DO 70, I = 1, J - 1
                  Y( IY ) = Y( IY ) + TEMP1*A( I, J )
                  TEMP2   = TEMP2   + A( I, J )*X( IX )
                  IX      = IX      + INCX
                  IY      = IY      + INCY
   70          CONTINUE
               Y( IY ) = Y( IY ) + TEMP1*A( J, J ) + ALPHA*TEMP2
   80       CONTINUE
         END IF
      ELSE
*
*        Form  y  when A is stored in lower triangle.
*
         IF( ( INCX.EQ.1 ).AND.( INCY.EQ.1 ) )THEN
            DO 100, J = 1, N
               TEMP1  = ALPHA*X( J )
               TEMP2  = ZERO
               Y( J ) = Y( J )       + TEMP1*A( J, J )
               DO 90, I = J + 1, N
                  Y( I ) = Y( I ) + TEMP1*A( I, J )
                  TEMP2  = TEMP2  + A( I, J )*X( I )
   90          CONTINUE
               Y( J ) = Y( J ) + ALPHA*TEMP2
  100       CONTINUE
         ELSE
            JX = KX
            JY = KY
            DO 120, J = 1, N
               TEMP1   = ALPHA*X( JX )
               TEMP2   = ZERO
               Y( JY ) = Y( JY )       + TEMP1*A( J, J )
               IX      = JX
               IY      = JY
               DO 110, I = J + 1, N
                  IX      = IX      + INCX
                  IY      = IY      + INCY
                  Y( IY ) = Y( IY ) + TEMP1*A( I, J )
                  TEMP2   = TEMP2   + A( I, J )*X( IX )
  110          CONTINUE
               Y( JY ) = Y( JY ) + ALPHA*TEMP2
               JX      = JX      + INCX
               JY      = JY      + INCY
  120       CONTINUE
         END IF
      END IF
*
      RETURN
*
*     End of SSYMV .
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SSYR  ( UPLO, N, ALPHA, X, INCX, A, LDA )
*     .. Scalar Arguments ..
      REAL               ALPHA
      INTEGER            INCX, LDA, N
      CHARACTER*1        UPLO
*     .. Array Arguments ..
      REAL               A( LDA, * ), X( * )
*     ..
*
*  Purpose
*  =======
*
*  SSYR   performs the symmetric rank 1 operation
*
*     A := alpha*x*x' + A,
*
*  where alpha is a real scalar, x is an n element vector and A is an
*  n by n symmetric matrix.
*
*  Parameters
*  ==========
*
*  UPLO   - CHARACTER*1.
*           On entry, UPLO specifies whether the upper or lower
*           triangular part of the array A is to be referenced as
*           follows:
*
*              UPLO = 'U' or 'u'   Only the upper triangular part of A
*                                  is to be referenced.
*
*              UPLO = 'L' or 'l'   Only the lower triangular part of A
*                                  is to be referenced.
*
*           Unchanged on exit.
*
*  N      - INTEGER.
*           On entry, N specifies the order of the matrix A.
*           N must be at least zero.
*           Unchanged on exit.
*
*  ALPHA  - REAL.
*           On entry, ALPHA specifies the scalar alpha.
*           Unchanged on exit.
*
*  X      - REAL array of dimension at least
*           ( 1 + ( n - 1 )*abs( INCX ) ).
*           Before entry, the incremented array X must contain the n
*           element vector x.
*           Unchanged on exit.
*
*  INCX   - INTEGER.
*           On entry, INCX specifies the increment for the elements of
*           X. INCX must not be zero.
*           Unchanged on exit.
*
*  A      - REAL array of DIMENSION ( LDA, n ).
*           Before entry with  UPLO = 'U' or 'u', the leading n by n
*           upper triangular part of the array A must contain the upper
*           triangular part of the symmetric matrix and the strictly
*           lower triangular part of A is not referenced. On exit, the
*           upper triangular part of the array A is overwritten by the
*           upper triangular part of the updated matrix.
*           Before entry with UPLO = 'L' or 'l', the leading n by n
*           lower triangular part of the array A must contain the lower
*           triangular part of the symmetric matrix and the strictly
*           upper triangular part of A is not referenced. On exit, the
*           lower triangular part of the array A is overwritten by the
*           lower triangular part of the updated matrix.
*
*  LDA    - INTEGER.
*           On entry, LDA specifies the first dimension of A as declared
*           in the calling (sub) program. LDA must be at least
*           max( 1, n ).
*           Unchanged on exit.
*
*
*  Level 2 Blas routine.
*
*  -- Written on 20-July-1986.
*     Sven Hammarling, Nag Central Office.
*
*
*     .. Parameters ..
      REAL               ZERO
      PARAMETER        ( ZERO = 0.0E+0 )
*     .. Local Scalars ..
      REAL               TEMP
      INTEGER            I, INFO, IX, J, JX, KX
*     .. External Functions ..
      LOGICAL            LEMAS
      EXTERNAL           LEMAS
*     .. External Subroutines ..
      EXTERNAL           XALBRE
*     .. Intrinsic Functions ..
      INTRINSIC          MAX
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF      ( .NOT.LEMAS( UPLO, 'U' ).AND.
     $          .NOT.LEMAS( UPLO, 'L' )      ) THEN
         INFO = 1
      ELSE IF ( N.LT.0 ) THEN
         INFO = 2
      ELSE IF ( INCX.EQ.0 ) THEN
         INFO = 5
      ELSE IF ( LDA.LT.MAX(1,N) ) THEN
         INFO = 7
      END IF
      IF( INFO.NE.0 )THEN
         CALL XALBRE( 'SSYR  ', INFO )
         RETURN
      END IF
*
*     Quick return if possible.
*
      IF( ( N.EQ.0 ).OR.( ALPHA.EQ.ZERO ) )
     $   RETURN
*
*     Set the start point in X if the increment is not unity.
*
      IF( INCX.LE.0 )THEN
         KX = 1 - ( N - 1 )*INCX
      ELSE IF( INCX.NE.1 )THEN
         KX = 1
      END IF
*
*     Start the operations. In this version the elements of A are
*     accessed sequentially with one pass through the triangular part
*     of A.
*
      IF( LEMAS( UPLO, 'U' ) )THEN
*
*        Form  A  when A is stored in upper triangle.
*
         IF( INCX.EQ.1 )THEN
            DO 20, J = 1, N
               IF( X( J ).NE.ZERO )THEN
                  TEMP = ALPHA*X( J )
                  DO 10, I = 1, J
                     A( I, J ) = A( I, J ) + X( I )*TEMP
   10             CONTINUE
               END IF
   20       CONTINUE
         ELSE
            JX = KX
            DO 40, J = 1, N
               IF( X( JX ).NE.ZERO )THEN
                  TEMP = ALPHA*X( JX )
                  IX   = KX
                  DO 30, I = 1, J
                     A( I, J ) = A( I, J ) + X( IX )*TEMP
                     IX        = IX        + INCX
   30             CONTINUE
               END IF
               JX = JX + INCX
   40       CONTINUE
         END IF
      ELSE
*
*        Form  A  when A is stored in lower triangle.
*
         IF( INCX.EQ.1 )THEN
            DO 60, J = 1, N
               IF( X( J ).NE.ZERO )THEN
                  TEMP = ALPHA*X( J )
                  DO 50, I = J, N
                     A( I, J ) = A( I, J ) + X( I )*TEMP
   50             CONTINUE
               END IF
   60       CONTINUE
         ELSE
            JX = KX
            DO 80, J = 1, N
               IF( X( JX ).NE.ZERO )THEN
                  TEMP = ALPHA*X( JX )
                  IX   = JX
                  DO 70, I = J, N
                     A( I, J ) = A( I, J ) + X( IX )*TEMP
                     IX        = IX        + INCX
   70             CONTINUE
               END IF
               JX = JX + INCX
   80       CONTINUE
         END IF
      END IF
*
      RETURN
*
*     End of SSYR  .
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE STRMV ( UPLO, TRANS, DIAG, N, A, LDA, X, INCX )
*     .. Scalar Arguments ..
      INTEGER            INCX, LDA, N
      CHARACTER*1        DIAG, TRANS, UPLO
*     .. Array Arguments ..
      REAL               A( LDA, * ), X( * )
*     ..
*
*  Purpose
*  =======
*
*  STRMV  performs one of the matrix-vector operations
*
*     x := A*x,   or   x := A'*x,
*
*  where x is n element vector and A is an n by n unit, or non-unit,
*  upper or lower triangular matrix.
*
*  Parameters
*  ==========
*
*  UPLO   - CHARACTER*1.
*           On entry, UPLO specifies whether the matrix is an upper or
*           lower triangular matrix as follows:
*
*              UPLO = 'U' or 'u'   A is an upper triangular matrix.
*
*              UPLO = 'L' or 'l'   A is a lower triangular matrix.
*
*           Unchanged on exit.
*
*  TRANS  - CHARACTER*1.
*           On entry, TRANS specifies the operation to be performed as
*           follows:
*
*              TRANS = 'N' or 'n'   x := A*x.
*
*              TRANS = 'T' or 't'   x := A'*x.
*
*              TRANS = 'C' or 'c'   x := A'*x.
*
*           Unchanged on exit.
*
*  DIAG   - CHARACTER*1.
*           On entry, DIAG specifies whether or not A is unit
*           triangular as follows:
*
*              DIAG = 'U' or 'u'   A is assumed to be unit triangular.
*
*              DIAG = 'N' or 'n'   A is not assumed to be unit
*                                  triangular.
*
*           Unchanged on exit.
*
*  N      - INTEGER.
*           On entry, N specifies the order of the matrix A.
*           N must be at least zero.
*           Unchanged on exit.
*
*  A      - REAL array of DIMENSION ( LDA, n ).
*           Before entry with  UPLO = 'U' or 'u', the leading n by n
*           upper triangular part of the array A must contain the upper
*           triangular matrix and the strictly lower triangular part of
*           A is not referenced.
*           Before entry with UPLO = 'L' or 'l', the leading n by n
*           lower triangular part of the array A must contain the lower
*           triangular matrix and the strictly upper triangular part of
*           A is not referenced.
*           Note that when  DIAG = 'U' or 'u', the diagonal elements of
*           A are not referenced either, but are assumed to be unity.
*           Unchanged on exit.
*
*  LDA    - INTEGER.
*           On entry, LDA specifies the first dimension of A as declared
*           in the calling (sub) program. LDA must be at least
*           max( 1, n ).
*           Unchanged on exit.
*
*  X      - REAL array of dimension at least
*           ( 1 + ( n - 1 )*abs( INCX ) ).
*           Before entry, the incremented array X must contain the n
*           element vector x. On exit, X is overwritten with the
*           tranformed vector x.
*
*  INCX   - INTEGER.
*           On entry, INCX specifies the increment for the elements of
*           X. INCX must not be zero.
*           Unchanged on exit.
*
*
*  Level 2 Blas routine.
*
*  -- Written on 20-July-1986.
*     Sven Hammarling, Nag Central Office.
*     Richard Hanson, Sandia National Labs.
*
*
*     .. Parameters ..
      REAL               ZERO
      PARAMETER        ( ZERO = 0.0E+0 )
*     .. Local Scalars ..
      INTEGER            I, INFO, IX, J, JX, KX
      LOGICAL            NOUNIT
*     .. External Functions ..
      LOGICAL            LEMAS
      EXTERNAL           LEMAS
*     .. External Subroutines ..
      EXTERNAL           XALBRE
*     .. Intrinsic Functions ..
      INTRINSIC          MAX
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF      ( .NOT.LEMAS( UPLO, 'U' ).AND.
     $          .NOT.LEMAS( UPLO, 'L' )      ) THEN
         INFO = 1
      ELSE IF ( .NOT.LEMAS( TRANS, 'N' ).AND.
     $          .NOT.LEMAS( TRANS, 'T' ).AND.
     $          .NOT.LEMAS( TRANS, 'C' )      ) THEN
         INFO = 2
      ELSE IF ( .NOT.LEMAS( DIAG, 'U' ).AND.
     $          .NOT.LEMAS( DIAG, 'N' )      ) THEN
         INFO = 3
      ELSE IF ( N.LT.0 ) THEN
         INFO = 4
      ELSE IF ( LDA.LT.MAX(1,N) ) THEN
         INFO = 6
      ELSE IF ( INCX.EQ.0 ) THEN
         INFO = 8
      END IF
      IF( INFO.NE.0 )THEN
         CALL XALBRE( 'STRMV ', INFO )
         RETURN
      END IF
*
*     Quick return if possible.
*
      IF( N.EQ.0 )
     $   RETURN
*
      NOUNIT = LEMAS( DIAG, 'N' )
*
*     Set up the start point in X if the increment is not unity. This
*     will be  ( N - 1 )*INCX  too small for descending loops.
*
      IF( INCX.LE.0 )THEN
         KX = 1 - ( N - 1 )*INCX
      ELSE IF( INCX.NE.1 )THEN
         KX = 1
      END IF
*
*     Start the operations. In this version the elements of A are
*     accessed sequentially with one pass through A.
*
      IF( LEMAS( TRANS, 'N' ) )THEN
*
*        Form  x := A*x.
*
         IF( LEMAS( UPLO, 'U' ) )THEN
            IF( INCX.EQ.1 )THEN
               DO 20, J = 1, N
                  IF( X( J ).NE.ZERO )THEN
                     DO 10, I = 1, J - 1
                        X( I ) = X( I ) + X( J )*A( I, J )
   10                CONTINUE
                     IF( NOUNIT )
     $                  X( J ) = X( J )*A( J, J )
                  END IF
   20          CONTINUE
            ELSE
               JX = KX
               DO 40, J = 1, N
                  IF( X( JX ).NE.ZERO )THEN
                     IX = KX
                     DO 30, I = 1, J - 1
                        X( IX ) = X( IX ) + X( JX )*A( I, J )
                        IX      = IX      + INCX
   30                CONTINUE
                     IF( NOUNIT )
     $                  X( JX ) = X( JX )*A( J, J )
                  END IF
                  JX = JX + INCX
   40          CONTINUE
            END IF
         ELSE
            IF( INCX.EQ.1 )THEN
               DO 60, J = N, 1, -1
                  IF( X( J ).NE.ZERO )THEN
                     DO 50, I = N, J + 1, -1
                        X( I ) = X( I ) + X( J )*A( I, J )
   50                CONTINUE
                     IF( NOUNIT )
     $                  X( J ) = X( J )*A( J, J )
                  END IF
   60          CONTINUE
            ELSE
               KX = KX + ( N - 1 )*INCX
               JX = KX
               DO 80, J = N, 1, -1
                  IF( X( JX ).NE.ZERO )THEN
                     IX  = KX
                     DO 70, I = N, J + 1, -1
                        X( IX ) = X( IX ) + X( JX )*A( I, J )
                        IX      = IX      - INCX
   70                CONTINUE
                     IF( NOUNIT )
     $                  X( JX ) = X( JX )*A( J, J )
                  END IF
                  JX = JX - INCX
   80          CONTINUE
            END IF
         END IF
      ELSE
*
*        Form  x := A'*x.
*
         IF( LEMAS( UPLO, 'U' ) )THEN
            IF( INCX.EQ.1 )THEN
               DO 100, J = N, 1, -1
                  IF( NOUNIT )
     $               X( J ) = X( J )*A( J, J )
                  DO 90, I = J - 1, 1, -1
                     X( J ) = X( J ) + A( I, J )*X( I )
   90             CONTINUE
  100          CONTINUE
            ELSE
               JX = KX + ( N - 1 )*INCX
               DO 120, J = N, 1, -1
                  IX = JX
                  IF( NOUNIT )
     $               X( JX ) = X( JX )*A( J, J )
                  DO 110, I = J - 1, 1, -1
                     IX      = IX      - INCX
                     X( JX ) = X( JX ) + A( I, J )*X( IX )
  110             CONTINUE
                  JX = JX - INCX
  120          CONTINUE
            END IF
         ELSE
            IF( INCX.EQ.1 )THEN
               DO 140, J = 1, N
                  IF( NOUNIT )
     $               X( J ) = X( J )*A( J, J )
                  DO 130, I = J + 1, N
                     X( J ) = X( J ) + A( I, J )*X( I )
  130             CONTINUE
  140          CONTINUE
            ELSE
               JX = KX
               DO 160, J = 1, N
                  IX = JX
                  IF( NOUNIT )
     $               X( JX ) = X( JX )*A( J, J )
                  DO 150, I = J + 1, N
                     IX      = IX      + INCX
                     X( JX ) = X( JX ) + A( I, J )*X( IX )
  150             CONTINUE
                  JX = JX + INCX
  160          CONTINUE
            END IF
         END IF
      END IF
*
      RETURN
*
*     End of STRMV .
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE STRSV ( UPLO, TRANS, DIAG, N, A, LDA, X, INCX )
*     .. Scalar Arguments ..
      INTEGER            INCX, LDA, N
      CHARACTER*1        DIAG, TRANS, UPLO
*     .. Array Arguments ..
      REAL               A( LDA, * ), X( * )
*     ..
*
*  Purpose
*  =======
*
*  STRSV  solves one of the systems of equations
*
*     A*x = b,   or   A'*x = b,
*
*  where b and x are n element vectors and A is an n by n unit, or
*  non-unit, upper or lower triangular matrix.
*
*  No test for singularity or near-singularity is included in this
*  routine. Such tests must be performed before calling this routine.
*
*  Parameters
*  ==========
*
*  UPLO   - CHARACTER*1.
*           On entry, UPLO specifies whether the matrix is an upper or
*           lower triangular matrix as follows:
*
*              UPLO = 'U' or 'u'   A is an upper triangular matrix.
*
*              UPLO = 'L' or 'l'   A is a lower triangular matrix.
*
*           Unchanged on exit.
*
*  TRANS  - CHARACTER*1.
*           On entry, TRANS specifies the equations to be solved as
*           follows:
*
*              TRANS = 'N' or 'n'   A*x = b.
*
*              TRANS = 'T' or 't'   A'*x = b.
*
*              TRANS = 'C' or 'c'   A'*x = b.
*
*           Unchanged on exit.
*
*  DIAG   - CHARACTER*1.
*           On entry, DIAG specifies whether or not A is unit
*           triangular as follows:
*
*              DIAG = 'U' or 'u'   A is assumed to be unit triangular.
*
*              DIAG = 'N' or 'n'   A is not assumed to be unit
*                                  triangular.
*
*           Unchanged on exit.
*
*  N      - INTEGER.
*           On entry, N specifies the order of the matrix A.
*           N must be at least zero.
*           Unchanged on exit.
*
*  A      - REAL array of DIMENSION ( LDA, n ).
*           Before entry with  UPLO = 'U' or 'u', the leading n by n
*           upper triangular part of the array A must contain the upper
*           triangular matrix and the strictly lower triangular part of
*           A is not referenced.
*           Before entry with UPLO = 'L' or 'l', the leading n by n
*           lower triangular part of the array A must contain the lower
*           triangular matrix and the strictly upper triangular part of
*           A is not referenced.
*           Note that when  DIAG = 'U' or 'u', the diagonal elements of
*           A are not referenced either, but are assumed to be unity.
*           Unchanged on exit.
*
*  LDA    - INTEGER.
*           On entry, LDA specifies the first dimension of A as declared
*           in the calling (sub) program. LDA must be at least
*           max( 1, n ).
*           Unchanged on exit.
*
*  X      - REAL array of dimension at least
*           ( 1 + ( n - 1 )*abs( INCX ) ).
*           Before entry, the incremented array X must contain the n
*           element right-hand side vector b. On exit, X is overwritten
*           with the solution vector x.
*
*  INCX   - INTEGER.
*           On entry, INCX specifies the increment for the elements of
*           X. INCX must not be zero.
*           Unchanged on exit.
*
*
*  Level 2 Blas routine.
*
*  -- Written on 20-July-1986.
*     Sven Hammarling, Nag Central Office.
*     Richard Hanson, Sandia National Labs.
*
*
*     .. Parameters ..
      REAL               ZERO
      PARAMETER        ( ZERO = 0.0E+0 )
*     .. Local Scalars ..
      INTEGER            I, INFO, IX, J, JX, KX
      LOGICAL            NOUNIT
*     .. External Functions ..
      LOGICAL            LEMAS
      EXTERNAL           LEMAS
*     .. External Subroutines ..
      EXTERNAL           XALBRE
*     .. Intrinsic Functions ..
      INTRINSIC          MAX
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF      ( .NOT.LEMAS( UPLO, 'U' ).AND.
     $          .NOT.LEMAS( UPLO, 'L' )      ) THEN
         INFO = 1
      ELSE IF ( .NOT.LEMAS( TRANS, 'N' ).AND.
     $          .NOT.LEMAS( TRANS, 'T' ).AND.
     $          .NOT.LEMAS( TRANS, 'C' )      ) THEN
         INFO = 2
      ELSE IF ( .NOT.LEMAS( DIAG, 'U' ).AND.
     $          .NOT.LEMAS( DIAG, 'N' )      ) THEN
         INFO = 3
      ELSE IF ( N.LT.0 ) THEN
         INFO = 4
      ELSE IF ( LDA.LT.MAX(1,N) ) THEN
         INFO = 6
      ELSE IF ( INCX.EQ.0 ) THEN
         INFO = 8
      END IF
      IF( INFO.NE.0 )THEN
         CALL XALBRE( 'STRSV ', INFO )
         RETURN
      END IF
*
*     Quick return if possible.
*
      IF( N.EQ.0 )
     $   RETURN
*
      NOUNIT = LEMAS( DIAG, 'N' )
*
*     Set up the start point in X if the increment is not unity. This
*     will be  ( N - 1 )*INCX  too small for descending loops.
*
      IF( INCX.LE.0 )THEN
         KX = 1 - ( N - 1 )*INCX
      ELSE IF( INCX.NE.1 )THEN
         KX = 1
      END IF
*
*     Start the operations. In this version the elements of A are
*     accessed sequentially with one pass through A.
*
      IF( LEMAS( TRANS, 'N' ) )THEN
*
*        Form  x := inv( A )*x.
*
         IF( LEMAS( UPLO, 'U' ) )THEN
            IF( INCX.EQ.1 )THEN
               DO 20, J = N, 1, -1
                  IF( X( J ).NE.ZERO )THEN
                     IF( NOUNIT )
     $                  X( J ) = X( J )/A( J, J )
                     DO 10, I = J - 1, 1, -1
                        X( I ) = X( I ) - X( J )*A( I, J )
   10                CONTINUE
                  END IF
   20          CONTINUE
            ELSE
               JX = KX + ( N - 1 )*INCX
               DO 40, J = N, 1, -1
                  IF( X( JX ).NE.ZERO )THEN
                     IF( NOUNIT )
     $                  X( JX ) = X( JX )/A( J, J )
                     IX = JX
                     DO 30, I = J - 1, 1, -1
                        IX      = IX      - INCX
                        X( IX ) = X( IX ) - X( JX )*A( I, J )
   30                CONTINUE
                  END IF
                  JX = JX - INCX
   40          CONTINUE
            END IF
         ELSE
            IF( INCX.EQ.1 )THEN
               DO 60, J = 1, N
                  IF( X( J ).NE.ZERO )THEN
                     IF( NOUNIT )
     $                  X( J ) = X( J )/A( J, J )
                     DO 50, I = J + 1, N
                        X( I ) = X( I ) - X( J )*A( I, J )
   50                CONTINUE
                  END IF
   60          CONTINUE
            ELSE
               JX = KX
               DO 80, J = 1, N
                  IF( X( JX ).NE.ZERO )THEN
                     IF( NOUNIT )
     $                  X( JX ) = X( JX )/A( J, J )
                     IX = JX
                     DO 70, I = J + 1, N
                        IX      = IX      + INCX
                        X( IX ) = X( IX ) - X( JX )*A( I, J )
   70                CONTINUE
                  END IF
                  JX = JX + INCX
   80          CONTINUE
            END IF
         END IF
      ELSE
*
*        Form  x := inv( A' )*x.
*
         IF( LEMAS( UPLO, 'U' ) )THEN
            IF( INCX.EQ.1 )THEN
               DO 100, J = 1, N
                  DO 90, I = 1, J - 1
                     X( J ) = X( J ) - A( I, J )*X( I )
   90             CONTINUE
                  IF( NOUNIT )
     $               X( J ) = X( J )/A( J, J )
  100          CONTINUE
            ELSE
               JX = KX
               DO 120, J = 1, N
                  IX = KX
                  DO 110, I = 1, J - 1
                     X( JX ) = X( JX ) - A( I, J )*X( IX )
                     IX      = IX      + INCX
  110             CONTINUE
                  IF( NOUNIT )
     $               X( JX ) = X( JX )/A( J, J )
                  JX = JX + INCX
  120          CONTINUE
            END IF
         ELSE
            IF( INCX.EQ.1 )THEN
               DO 140, J = N, 1, -1
                  DO 130, I = N, J + 1, -1
                     X( J ) = X( J ) - A( I, J )*X( I )
  130             CONTINUE
                  IF( NOUNIT )
     $               X( J ) = X( J )/A( J, J )
  140          CONTINUE
            ELSE
               KX = KX + ( N - 1 )*INCX
               JX = KX
               DO 160, J = N, 1, -1
                  IX = KX
                  DO 150, I = N, J + 1, -1
                     X( JX ) = X( JX ) - A( I, J )*X( IX )
                     IX      = IX      - INCX
  150             CONTINUE
                  IF( NOUNIT )
     $               X( JX ) = X( JX )/A( J, J )
                  JX = JX - INCX
  160          CONTINUE
            END IF
         END IF
      END IF
*
      RETURN
*
*     End of STRSV .
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      LOGICAL FUNCTION LEMAS ( CA, CB )
*     .. Scalar Arguments ..
      CHARACTER*1              CA, CB
*     ..
*
*  Purpose
*  =======
*
*  LEMAS  tests if CA is the same letter as CB regardless of case.
*  CB is assumed to be an upper case letter. LEMAS returns .TRUE. if
*  CA is either the same as CB or the equivalent lower case letter.
*
*  N.B. This version of the routine is only correct for ASCII code.
*       Installers must modify the routine for other character-codes.
*
*       For EBCDIC systems the constant IOFF must be changed to -64.
*       For CDC systems using 6-12 bit representations, the system-
*       specific code in comments must be activated.
*
*  Parameters
*  ==========
*
*  CA     - CHARACTER*1
*  CB     - CHARACTER*1
*           On entry, CA and CB specify characters to be compared.
*           Unchanged on exit.
*
*
*  Auxiliary routine for Level 2 Blas.
*
*  -- Written on 20-July-1986
*     Richard Hanson, Sandia National Labs.
*     Jeremy Du Croz, Nag Central Office.
*
*     .. Parameters ..
      INTEGER                IOFF
      PARAMETER            ( IOFF=32 )
*     .. Intrinsic Functions ..
      INTRINSIC              ICHAR
*     .. Executable Statements ..
*
*     Test if the characters are equal
*
      LEMAS = CA .EQ. CB
*
*     Now test for equivalence
*
      IF ( .NOT.LEMAS ) THEN
         LEMAS = ICHAR(CA) - IOFF .EQ. ICHAR(CB)
      END IF
*
      RETURN
*
*  The following comments contain code for CDC systems using 6-12 bit
*  representations.
*
*     .. Parameters ..
*     INTEGER                ICIRFX
*     PARAMETER            ( ICIRFX=62 )
*     .. Scalar Arguments ..
*     CHARACTER*1            CB
*     .. Array Arguments ..
*     CHARACTER*1            CA(*)
*     .. Local Scalars ..
*     INTEGER                IVAL
*     .. Intrinsic Functions ..
*     INTRINSIC              ICHAR, CHAR
*     .. Executable Statements ..
*
*     See if the first character in string CA equals string CB.
*
*     LEMAS = CA(1) .EQ. CB .AND. CA(1) .NE. CHAR(ICIRFX)
*
*     IF (LEMAS) RETURN
*
*     The characters are not identical. Now check them for equivalence.
*     Look for the 'escape' character, circumflex, followed by the
*     letter.
*
*     IVAL = ICHAR(CA(2))
*     IF (IVAL.GE.ICHAR('A') .AND. IVAL.LE.ICHAR('Z')) THEN
*        LEMAS = CA(1) .EQ. CHAR(ICIRFX) .AND. CA(2) .EQ. CB
*     END IF
*
*     RETURN
*
*     End of LEMAS.
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE XALBRE( SRNAME, INFO )
      INTEGER            INFO
      CHARACTER*6        SRNAME
*
*     This is a special version of XALBRE to be used only as part of
*     the test program for testing error exits from the Level 2 BLAS
*     routines.
*
*     XALBRE  is an error handler for the Level 2 BLAS routines.
*
*     It is called by the Level 2 BLAS routines if an input parameter is
*     invalid.
*
      LOGICAL            OK, LERR
      CHARACTER*6        SRNAMT
      COMMON    /INFOC / INFOT, NOUT, OK, LERR
      COMMON    /SRNAMC/ SRNAMT

      LERR = .TRUE.
      IF (INFO.NE.INFOT) THEN
         WRITE (NOUT,99998) INFO, INFOT
         OK = .FALSE.
      ENDIF
      IF (SRNAME.NE.SRNAMT) THEN
         WRITE (NOUT,99997) SRNAME, SRNAMT
         OK = .FALSE.
      END IF
      RETURN
*
99998 FORMAT (' XXXX    XALBRE was called with INFO = ', I6,
     $        ' instead of ',I2,'   XXXX')
99997 FORMAT (' XXXX    XALBRE was called with SRNAME = ', A6,
     $        ' instead of ',A6,'   XXXX')
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SCOND ( N, X, INCX, AXMAX, AXMIN )

      INTEGER            N, INCX
      REAL               AXMAX, AXMIN
      REAL               X( (N-1)*INCX+1 )
C
C     SCOND   finds the elements in  x  that are largest and smallest
C     in magnitude.
C
      REAL               ZERO
      PARAMETER        ( ZERO = 0.0E+0 )
      INTEGER            I, IX
      INTRINSIC          ABS, MAX, MIN

      IF (N .EQ. 0) THEN
         AXMAX = ZERO
         AXMIN = ZERO
      ELSE
         AXMAX = ABS( X(1) )
         AXMIN = AXMAX
         IX    = 1
         DO 100 I = 2, N
            IX    = IX + INCX
            AXMAX = MAX( AXMAX, ABS( X(IX) ) )
            AXMIN = MIN( AXMIN, ABS( X(IX) ) )
  100    CONTINUE
      END IF

      RETURN

*     End of  SCOND

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      REAL             FUNCTION SDIV  ( A, B, FAIL )
      REAL                              A, B
      LOGICAL                           FAIL
C
C  SDIV   returns the value div given by
C
C     div = ( a/b                 if a/b does not overflow,
C           (
C           ( 0.0                 if a .eq. 0.0,
C           (
C           ( sign( a/b )*flmax   if a .ne. 0.0 and a/b would overflow,
C
C  where flmax is a large value, via the function name. In addition if
C  a/b would overflow then fail is returned as true, otherwise fail is
C  returned as false.
C
C  Note that when a and b are both zero, fail is returned as true,
C  but div is returned as 0.0. in all other cases of overflow div is
C  such that abs( div ) = flmax.
C
C
C  Nag Fortran 77 O( 1 ) basic linear algebra routine.
C
C  -- Written on 26-October-1982.
C     Sven Hammarling, Nag Central Office.
C
      INTRINSIC           ABS   , SIGN
      LOGICAL             FIRST
      REAL                ABSB  , FLMAX , FLMIN
      REAL                ONE   ,         ZERO
      PARAMETER         ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/

      SAVE                FIRST , FLMIN , FLMAX
      DATA                FIRST / .TRUE. /

      IF( A.EQ.ZERO )THEN
         SDIV   = ZERO
         IF( B.EQ.ZERO )THEN
            FAIL = .TRUE.
         ELSE
            FAIL = .FALSE.
         END IF
         RETURN
      END IF

      IF( FIRST )THEN
         FIRST  = .FALSE.
         FLMIN  = WMACH( 5 )
         FLMAX  = WMACH( 7 )
      END IF

      IF( B.EQ.ZERO )THEN
         SDIV   = SIGN( FLMAX, A )
         FAIL   = .TRUE.
      ELSE
         ABSB   = ABS( B )
         IF( ABSB.GE.ONE )THEN
            FAIL = .FALSE.
            IF( ABS( A ).GE.ABSB*FLMIN )THEN
               SDIV   = A/B
            ELSE
               SDIV   = ZERO
            END IF
         ELSE
            IF( ABS( A ).LE.ABSB*FLMAX )THEN
               FAIL   = .FALSE.
               SDIV   = A/B
            ELSE
               FAIL   = .TRUE.
               SDIV   = FLMAX
               IF( ( ( A.LT.ZERO ).AND.( B.GT.ZERO ) ).OR.
     $             ( ( A.GT.ZERO ).AND.( B.LT.ZERO ) )     )
     $            SDIV   = -SDIV
            END IF
         END IF
      END IF

      RETURN

*     End of SDIV  .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SSDIV ( N, D, INCD, X, INCX )
      INTEGER            N, INCD, INCX
      REAL               D( * ), X( * )
C
C     SSDIV  performs the operation
C
C     x := diag( d )(inverse)*x
C
      PARAMETER        ( ONE = 1.0 )
      EXTERNAL           SSCAL
      INTEGER            I     , ID    , IX

      IF( N.GE.1 )THEN
         IF( INCD.EQ.0 )THEN

            CALL SSCAL ( N, (ONE/D( 1 )), X, INCX )

         ELSE IF( ( INCD.EQ.INCX ).AND.( INCD.GT.0 ) )THEN
            DO 10, ID = 1, 1 + ( N - 1 )*INCD, INCD
               X( ID ) = X( ID )/D( ID )
   10       CONTINUE
         ELSE
            IF( INCX.GE.0 )THEN
               IX = 1
            ELSE
               IX = 1 - ( N - 1 )*INCX
            END IF
            IF( INCD.GT.0 )THEN
               DO 20, ID = 1, 1 + ( N - 1 )*INCD, INCD
                  X( IX ) = X( IX )/D( ID )
                  IX      = IX + INCX
   20          CONTINUE
            ELSE
               ID = 1 - ( N - 1 )*INCD
               DO 30, I = 1, N
                  X( IX ) = X( IX )/D( ID )
                  ID      = ID + INCD
                  IX      = IX + INCX
   30          CONTINUE
            END IF
         END IF
      END IF

      RETURN

*     End of SSDIV .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SDSCL ( N, D, INCD, X, INCX )
      INTEGER            N, INCD, INCX
      REAL               D( * ), X( * )
C
C  SDSCL  performs the operation
C
C     x := diag( d )*x
C
C
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 22-September-1983.
C     Sven Hammarling, Nag Central Office.
C
      EXTERNAL           SSCAL
      INTEGER            I     , ID    , IX

      IF( N.GE.1 )THEN
         IF( INCD.EQ.0 )THEN

            CALL SSCAL ( N, D( 1 ), X, INCX )

         ELSE IF( ( INCD.EQ.INCX ).AND.( INCD.GT.0 ) )THEN
            DO 10, ID = 1, 1 + ( N - 1 )*INCD, INCD
               X( ID ) = D( ID )*X( ID )
   10       CONTINUE
         ELSE
            IF( INCX.GE.0 )THEN
               IX = 1
            ELSE
               IX = 1 - ( N - 1 )*INCX
            END IF
            IF( INCD.GT.0 )THEN
               DO 20, ID = 1, 1 + ( N - 1 )*INCD, INCD
                  X( IX ) = D( ID )*X( IX )
                  IX      = IX + INCX
   20          CONTINUE
            ELSE
               ID = 1 - ( N - 1 )*INCD
               DO 30, I = 1, N
                  X( IX ) = D( ID )*X( IX )
                  ID      = ID + INCD
                  IX      = IX + INCX
   30          CONTINUE
            END IF
         END IF
      END IF

      RETURN

*     End of SDSCL .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SGRFG ( N, ALPHA, X, INCX, TOL, ZETA )
      INTEGER            N, INCX
      REAL               ALPHA, X( * ), TOL, ZETA
C
C  SGRFG  generates details of a generalized Householder reflection such
C  that
C
C     P*( alpha ) = ( beta ),   P'*P = I.
C       (   x   )   (   0  )
C
C  P is given in the form
C
C     P = I - ( zeta )*( zeta  z' ),
C             (   z  )
C
C  where z is an n element vector and zeta is a scalar that satisfies
C
C     1.0 .le. zeta .le. sqrt( 2.0 ).
C
C  zeta is returned in ZETA unless x is such that
C
C     max( abs( x( i ) ) ) .le. max( eps*abs( alpha ), tol )
C
C  where eps is the relative machine precision and tol is the user
C  supplied value TOL, in which case ZETA is returned as 0.0 and P can
C  be taken to be the unit matrix.
C
C  beta is overwritten on alpha and z is overwritten on x.
C  the routine may be called with  n = 0  and advantage is taken of the
C  case where  n = 1.
C
C
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 30-August-1984.
C     Sven Hammarling, Nag Central Office.
C     This version dated 28-September-1984.
C
      EXTERNAL           SSSQ  , SSCAL
      INTRINSIC          ABS   , MAX   , SIGN  , SQRT
      LOGICAL            FIRST
      REAL               BETA  , EPS   , SCALE , SSQ
      REAL               ONE   ,         ZERO
      PARAMETER        ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/

      IF( N.LT.1 )THEN
         ZETA = ZERO
      ELSE IF( ( N.EQ.1 ).AND.( X( 1 ).EQ.ZERO ) )THEN
         ZETA = ZERO
      ELSE

         EPS    =  WMACH( 3 )

*        Treat case where P is a 2 by 2 matrix specially.

         IF( N.EQ.1 )THEN

*           Deal with cases where  ALPHA = zero  and
*           abs( X( 1 ) ) .le. max( EPS*abs( ALPHA ), TOL )  first.

            IF( ALPHA.EQ.ZERO )THEN
               ZETA   =  ONE
               ALPHA  =  ABS( X( 1 ) )
               X( 1 ) = -SIGN( ONE, X( 1 ) )
            ELSE IF( ABS( X( 1 ) ).LE.MAX( EPS*ABS( ALPHA ),
     $                                     TOL ) )THEN
               ZETA   =  ZERO
            ELSE
               IF( ABS( ALPHA ).GE.ABS( X( 1 ) ) )THEN
                  BETA = ABS ( ALPHA  )*
     $                   SQRT( ONE + ( X( 1 )/ALPHA )**2 )
               ELSE
                  BETA = ABS ( X( 1 ) )*
     $                   SQRT( ONE + ( ALPHA/X( 1 ) )**2 )
               END IF
               ZETA   =  SQRT( ( ABS( ALPHA ) + BETA )/BETA )
               IF( ALPHA.GE.ZERO )BETA = -BETA
               X( 1 ) = -X( 1 )/( ZETA*BETA )
               ALPHA  =  BETA
            END IF
         ELSE

*           Now P is larger than 2 by 2.

            SSQ   = ONE
            SCALE = ZERO

            CALL SSSQ  ( N, X, INCX, SCALE, SSQ )

*           Treat cases where  SCALE = zero,
*           SCALE .le. max( EPS*abs( ALPHA ), TOL )  and
*           ALPHA = zero  specially.
*           Note that  SCALE = max( abs( X( i ) ) ).

            IF( ( SCALE.EQ.ZERO ).OR.
     $          ( SCALE.LE.MAX( EPS*ABS( ALPHA ), TOL ) ) )THEN
               ZETA  = ZERO
            ELSE IF( ALPHA.EQ.ZERO )THEN
               ZETA  = ONE
               ALPHA = SCALE*SQRT( SSQ )

               CALL SSCAL ( N, -ONE/ALPHA, X, INCX )

            ELSE
               IF( SCALE.LT.ABS( ALPHA ) )THEN
                  BETA = ABS ( ALPHA )*
     $                   SQRT( ONE + SSQ*( SCALE/ALPHA )**2 )
               ELSE
                  BETA = SCALE*
     $                   SQRT( SSQ +     ( ALPHA/SCALE )**2 )
               END IF
               ZETA = SQRT( ( BETA + ABS( ALPHA ) )/BETA )
               IF( ALPHA.GT.ZERO )BETA = -BETA

               CALL SSCAL( N, -ONE/( ZETA*BETA ), X, INCX )

               ALPHA = BETA
            END IF
         END IF
      END IF
      RETURN

*     End of SGRFG .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SLOAD ( N, CONST, X, INCX )
      INTEGER            N, INCX
      REAL               CONST
      REAL               X( * )
C
C  SLOAD  performs the operation
C
C     x = const*e,   e' = ( 1  1 ... 1 ).
C
C
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 22-September-1983.
C     Sven Hammarling, Nag Central Office.
C
      INTEGER            IX
      REAL               ZERO
      PARAMETER        ( ZERO = 0.0E+0 )

      IF( N.LT.1 )RETURN

      IF( CONST.NE.ZERO )THEN
         DO 10, IX = 1, 1 + ( N - 1 )*INCX, INCX
            X( IX ) = CONST
   10    CONTINUE
      ELSE
         DO 20, IX = 1, 1 + ( N - 1 )*INCX, INCX
            X( IX ) = ZERO
   20    CONTINUE
      END IF

      RETURN

*     End of SLOAD .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      REAL             FUNCTION SNORM ( SCALE, SSQ )
      REAL                              SCALE, SSQ
C
C  SNORM  returns the value norm given by
C
C     norm = ( scale*sqrt( ssq ), scale*sqrt( ssq ) .lt. flmax
C            (
C            ( flmax,             scale*sqrt( ssq ) .ge. flmax
C
C  via the function name.
C
C
C  Nag Fortran 77 O( 1 ) basic linear algebra routine.
C
C  -- Written on 22-October-1982.
C     Sven Hammarling, Nag Central Office.
C
      INTRINSIC           SQRT
      LOGICAL             FIRST
      REAL                FLMAX , SQT
      REAL                ONE
      PARAMETER         ( ONE   = 1.0E+0 )

      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/

      SAVE                FIRST , FLMAX
      DATA                FIRST / .TRUE. /

      IF( FIRST )THEN
         FIRST = .FALSE.
         FLMAX = WMACH( 7 )
      END IF

      SQT = SQRT( SSQ )
      IF( SCALE.LT.FLMAX/SQT )THEN
         SNORM  = SCALE*SQT
      ELSE
         SNORM  = FLMAX
      END IF

      RETURN

*     End of SNORM .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SROT3 ( N, X, INCX, Y, INCY, CS, SN )

      INTEGER            N, INCX, INCY
      REAL               CS, SN
      REAL               X(*), Y(*)

      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/

C
C  SROT3   applies the plane rotation defined by CS and SN to the
C  columns of a 2 by N matrix held in X and Y.  The method used requires
C  3 multiplications and 3 additions per column, as described in Gill,
C  Golub, Murray and Saunders, Mathematics of Computation 28 (1974) 505-
C  -535 (see page 508).
C
C  SROT3   guards against underflow, and overflow is extremely unlikely.
C  It is assumed that CS and SN have been generated by SROT3G, ensuring
C  that CS lies in the closed interval (0, 1),  and that the absolute
C  value of CS and SN (if nonzero) is no less than the machine precision
C  EPS.  It is also assumed that  RTMIN .lt. EPS.  Note that the magic
C  number Z is therefore no less than 0.5*EPS in absolute value, so it
C  is safe to use TOL = 2*RTMIN in the underflow test involving Z*A.
C  For efficiency we use the same TOL in the previous two tests.
C
C  Systems Optimization Laboratory, Stanford University.
C  Original version dated January 1982.
C  F77 version dated 28-June-1986.
C  This version of SROT3 dated 28-June-1986.
C
      INTEGER            I, IX, IY
      REAL               A, B, ONE, RTMIN, TOL, W, Z, ZERO
      INTRINSIC          ABS
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      IF (N .LT. 1  .OR.  SN .EQ. ZERO) RETURN
      IX = 1
      IY = 1
      IF (CS .EQ. ZERO) THEN

*        Just swap  x  and  y.

         DO 10 I = 1, N
            A     = X(IX)
            X(IX) = Y(IY)
            Y(IY) = A
            IX    = IX + INCX
            IY    = IY + INCY
   10    CONTINUE

      ELSE

         RTMIN  = WMACH(6)
         TOL    = RTMIN + RTMIN
         Z      = SN/(ONE + CS)

         DO 20 I = 1, N
            A     = X(IX)
            B     = Y(IY)
            W     = ZERO
            IF (ABS(A) .GT. TOL) W = CS*A
            IF (ABS(B) .GT. TOL) W = W + SN*B
            X(IX) = W
            A     = A + W
            IF (ABS(A) .GT. TOL) B = B - Z*A
            Y(IY) = - B
            IX    =   IX + INCX
            IY    =   IY + INCY
   20    CONTINUE

      END IF

      RETURN

*     End of  SROT3

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SROT3G( X, Y, CS, SN )

      REAL               X, Y, CS, SN

      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/

C
C  SROT3G  generates a plane rotation that reduces the vector (X, Y) to
C  the vector (A, 0),  where A is defined as follows...
C
C     If both X and Y are negligibly small, or
C     if Y is negligible relative to Y,
C     then  A = X,  and the identity rotation is returned.
C
C     If X is negligible relative to Y,
C     then  A = Y,  and the swap rotation is returned.
C
C     Otherwise,  A = sign(X) * sqrt( X**2 + Y**2 ).
C
C  In all cases,  X and Y are overwritten by A and 0,  and CS will lie
C  in the closed interval (0, 1).  Also,  the absolute value of CS and
C  SN (if nonzero) will be no less than the machine precision,  EPS.
C
C  SROT3G  guards against overflow and underflow.
C  It is assumed that  FLMIN .lt. EPS**2  (i.e.  RTMIN .lt. EPS).
C
C  Systems Optimization Laboratory, Stanford University.
C  Original version dated January 1982.
C  F77 version dated 28-June-1986.
C  This version of SROT3G dated 28-June-1986.
C
      REAL               A, B, EPS, ONE, RTMIN, ZERO
      LOGICAL            FIRST
      INTRINSIC          ABS, MAX, SQRT
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      SAVE               FIRST , EPS   , RTMIN
      DATA               FIRST / .TRUE. /

      IF( FIRST )THEN
         FIRST = .FALSE.
         EPS    = WMACH(3)
         RTMIN  = WMACH(6)
      END IF

      IF (Y .EQ. ZERO) THEN

         CS = ONE
         SN = ZERO

      ELSE IF (X .EQ. ZERO) THEN

         CS = ZERO
         SN = ONE
         X  = Y

      ELSE

         A      = ABS(X)
         B      = ABS(Y)
         IF (MAX(A,B) .LE. RTMIN) THEN
            CS = ONE
            SN = ZERO
         ELSE
            IF (A .GE. B) THEN
               IF (B .LE. EPS*A) THEN
                  CS = ONE
                  SN = ZERO
                  GO TO 900
               ELSE
                  A  = A * SQRT( ONE + (B/A)**2 )
               END IF
            ELSE
               IF (A .LE. EPS*B) THEN
                  CS = ZERO
                  SN = ONE
                  X  = Y
                  GO TO 900
               ELSE
                  A  = B * SQRT( ONE + (A/B)**2 )
               END IF
            END IF
            IF (X .LT. ZERO) A = - A
            CS = X/A
            SN = Y/A
            X  = A
         END IF
      END IF

  900 Y  = ZERO

      RETURN

*     End of  SROT3G

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SSSQ  ( N, X, INCX, SCALE, SUMSQ )
      INTEGER            N, INCX
      REAL               X( * )
      REAL               SCALE, SUMSQ
C
C  SSSQ   returns the values scl and smsq such that
C
C     ( scl**2 )*smsq = y( 1 )**2 +...+ y( n )**2 + ( scale**2 )*sumsq,
C
C  where y( i ) = X( 1 + ( i - 1 )*INCX ). The value of sumsq is assumed
C  to be at least unity and the value of smsq will then satisfy
C
C     1.0 .le. smsq .le. ( sumsq + n ) .
C
C  scale is assumed to be non-negative and scl returns the value
C
C     scl = max( scale, abs( x( i ) ) ) .
C
C  scale and sumsq must be supplied in SCALE and SUMSQ respectively.
C  scl and smsq are overwritten on SCALE and SUMSQ respectively.
C
C  The routine makes only one pass through the vector X.
C
C
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 22-October-1982.
C     Sven Hammarling, Nag Central Office.
C
      INTRINSIC          ABS
      INTEGER            IX
      REAL               ABSXI
      REAL               ONE   ,         ZERO
      PARAMETER        ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

      IF( N.GE.1 )THEN
         DO 10, IX = 1, 1 + ( N - 1 )*INCX, INCX
            IF( X( IX ).NE.ZERO )THEN
               ABSXI = ABS( X( IX ) )
               IF( SCALE.LT.ABSXI )THEN
                  SUMSQ = ONE   + SUMSQ*( SCALE/ABSXI )**2
                  SCALE = ABSXI
               ELSE
                  SUMSQ = SUMSQ +       ( ABSXI/SCALE )**2
               END IF
            END IF
   10    CONTINUE
      END IF
      RETURN

*     End of SSSQ  .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE IYPOC ( N, IX, INCIX, IY, INCIY )

      INTEGER            N, INCIX, INCIY
      INTEGER            IX(*), IY(*)

C
C  Copy the first N elements of IX into IY.
C

      INTEGER            J, JX, JY

      IF (N .GE. 1) THEN
         IF (INCIX .EQ. 1  .AND.  INCIY .EQ. 1) THEN

            DO 10 J = 1, N
               IY(J) = IX(J)
   10       CONTINUE

         ELSE

            JX = 1
            JY = 1
            DO 20 J = 1, N
               IY(JY) = IX(JX)
               JX = JX + INCIX
               JY = JY + INCIY
   20       CONTINUE

         END IF
      END IF

      RETURN

*     End of  IYPOC

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE DAOLI ( N, ICONST, IX, INCIX )
      INTEGER            N, INCIX
      INTEGER            ICONST
      INTEGER            IX( * )
C
C  DAOLI   performs the operation
C
C     ix = iconst*e,   e' = ( 1  1 ... 1 ).
C
C
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 22-September-1983.
C     Sven Hammarling, Nag Central Office.
C
      INTEGER            JX

      IF( N.LT.1 )RETURN

      IF( ICONST.NE.0 )THEN
         DO 10, JX = 1, 1 + ( N - 1 )*INCIX, INCIX
            IX( JX ) = ICONST
   10    CONTINUE
      ELSE
         DO 20, JX = 1, 1 + ( N - 1 )*INCIX, INCIX
            IX( JX ) = 0
   20    CONTINUE
      END IF

      RETURN

*     End of DAOLI .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INTEGER           FUNCTION ISRANK( N, X, INCX, TOL )
      INTEGER                            N, INCX
      REAL                               X( * ), TOL

C  ISRANK finds the first element of the n element vector x for which
C
C     abs( x( k ) ).le.( tol*max ( abs(x(1)), ..., abs(x(k-1)) )
C
C  and returns the value ( k - 1 ) in the function name ISRANK. If no
C  such k exists then ISRANK is returned as n.
C
C  If TOL is supplied as less than zero then the value EPSMCH, where
C  EPSMCH is the relative machine precision, is used in place of TOL.
C
C
C  Nag Fortran 77 O( n ) basic linear algebra routine.
C
C  -- Written on 21-January-1985.
C     Sven Hammarling, Nag Central Office.
C     Modified by PEG, 19-December-1985.

      INTRINSIC                          ABS   , MAX
      INTEGER                            IX    , K
      REAL                               TOLRNK, XMAX  , ZERO
      PARAMETER                        ( ZERO  = 0.0E+0 )

      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/

      K = 0
      IF (N .GE. 1) THEN
         TOLRNK = TOL
         IF (TOL .LT. ZERO) TOLRNK = WMACH(3)

         IF( INCX .GT. 0 )THEN
            IX = 1
         ELSE
            IX = 1 - ( N - 1 )*INCX
         END IF

         XMAX = ABS( X(IX) )

*+       WHILE (K .LT. N) LOOP
   10    IF    (K .LT. N) THEN
            IF (ABS( X(IX) ) .LE. XMAX*TOLRNK) GO TO 20
            XMAX = MAX( XMAX, ABS( X(IX) ) )
            K    = K  + 1
            IX   = IX + INCX
            GO TO 10
         END IF
*+       END WHILE

      END IF
   20 ISRANK = K
      RETURN

*     End of ISRANK.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SGEQR ( M, N, A, LDA, ZETA, INFORM )
      INTEGER            M, N, LDA, INFORM
      REAL               A( LDA, * ), ZETA( * )
C
C  1. Purpose
C     =======
C
C  SGEQR  reduces the  m by n, m.ge.n, matrix A to upper triangular form
C  by means of orthogonal transformations.
C
C  2. Description
C     ===========
C
C  The m by n matrix A is factorized as
C
C     A = Q*( R )   when   m.gt.n,
C           ( 0 )
C
C     A = Q*R       when   m = n,
C
C  where  Q  is an  m by m  orthogonal matrix and  R  is an n by n upper
C  triangular matrix.
C
C  The  factorization  is  obtained  by  Householder's  method. The  kth
C  transformation matrix, Q( k ), which is used to introduce zeros  into
C  the kth column of A is given in the form
C
C     Q( k ) = ( I     0   ),
C              ( 0  T( k ) )
C
C  where
C
C     T( k ) = I - u( k )*u( k )',   u( k ) = ( zeta( k ) ),
C                                             (    z( k ) )
C
C  zeta( k )  is a scalar and  z( k )  is an  ( m - k )  element vector.
C  zeta( k )  and  z( k ) are chosen to annhilate the elements below the
C  triangular part of  A.
C
C  The vector  u( k )  is returned in the kth element of ZETA and in the
C  kth column of A, such that zeta( k ) is in ZETA( k ) and the elements
C  of z( k ) are in a( k + 1, k ), ..., a( m, k ). The elements of R are
C  returned in the upper triangular part of  A.
C
C  Q is given by
C
C     Q = ( Q( p )*Q( p - 1 )*...*Q( 1 ) )',
C
C  where p = min( n, m - 1 ).
C
C  3. Parameters
C     ==========
C
C  M      - INTEGER.
C
C           On entry, M must specify the number of rows of  A. M must be
C           at least  n.
C
C           Unchanged on exit.
C
C  N      - INTEGER.
C
C           On entry, N must specify the number of columns of  A. N must
C           be  at  least zero. When  N = 0  then an immediate return is
C           effected.
C
C           Unchanged on exit.
C
C  A      - 'real' array of DIMENSION ( LDA, n ).
C
C           Before entry, the leading  M by N  part of the array  A must
C           contain the matrix to be factorized.
C
C           On exit, the  N by N upper triangular part of A will contain
C           the  upper  triangular  matrix  R  and the  M by N  strictly
C           lower triangular part of  A  will  contain  details  of  the
C           factorization as described above.
C
C  LDA    - INTEGER.
C
C           On entry, LDA  must  specify  the  leading dimension of  the
C           array  A  as declared in the calling (sub) program. LDA must
C           be at least  m.
C
C           Unchanged on exit.
C
C  ZETA   - 'real' array of DIMENSION at least ( n ).
C
C           On  exit, ZETA( k )  contains the scalar  zeta( k )  for the
C           kth  transformation.  If  T( k ) = I  then   ZETA( k ) = 0.0
C           otherwise  ZETA( k )  contains  zeta( k ) as described above
C           and is always in the range ( 1.0, sqrt( 2.0 ) ).
C
C  INFORM - INTEGER.
C
C           On successful  exit  INFORM  will be zero, otherwise  INFORM
C           will  be set to unity indicating that an input parameter has
C           been  incorrectly  set. See  the  next section  for  further
C           details.
C
C  4. Diagnostic Information
C     ======================
C
C  INFORM = 1
C
C     One or more of the following conditions holds:
C
C        M   .lt. N
C        N   .lt. 0
C        LDA .lt. M
C
C  5. Further information
C     ===================
C
C  Following the use of this routine the operations
C
C     B := Q'*B   and   B := Q*B,
C
C  where  B  is an  m by k  matrix, can  be  performed  by calls to  the
C  auxiliary  linear  algebra routine  SGEAPQ. The  operation  B := Q'*B
C  can be obtained by the call:
C
C     INFORM = 0
C     CALL SGEAPQ( 'Transpose', 'Separate', M, N, A, LDA, ZETA,
C    $             K, B, LDB, WORK, INFORM )
C
C  and  B := Q*B  can be obtained by the call:
C
C     INFORM = 0
C     CALL SGEAPQ( 'No transpose', 'Separate', M, N, A, LDA, ZETA,
C    $             K, B, LDB, WORK, INFORM )
C
C  In  both  cases  WORK  must be a  k  element array  that  is used  as
C  workspace. If  B  is a one-dimensional array (single column) then the
C  parameter  LDB  can be replaced by  M. See routine SGEAPQ for further
C  details.
C
C  Operations involving the matrix  R  are performed by  the
C  Level 2 BLAS  routines  STRMV  and STRSV . Note that no test for near
C  singularity of R is incorporated in this routine or in routine  STRSV
C  and  so it is  strongly recommended that the auxiliary linear algebra
C  routine  SUTCO  be called, prior to solving equations involving R, in
C  order  to determine whether  or not  R  is nearly singular. If  R  is
C  nearly  singular  then  the  auxiliary linear algebra  routine  SUTSV
C  can  be used to  determine  the  singular value decomposition  of  R.
C
C
C  Nag Fortran 77 Auxiliary linear algebra routine.
C
C  -- Written on 13-December-1984.
C     Sven Hammarling, Nag Central Office.
C
      EXTERNAL           SGEMV , SGER  , SGRFG
      INTRINSIC          MIN
      INTEGER            J     , K     , LA
      REAL               TEMP
      REAL               ONE   ,         ZERO
      PARAMETER        ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

*     Check the input parameters.

      IF( N.EQ.0 )THEN
         INFORM = 0
         RETURN
      END IF
      IF( ( M.LT.N ).OR.( N.LT.0 ).OR.( LDA.LT.M ) )THEN
         INFORM = 1
         RETURN
      END IF

*     Perform the factorization.

      LA = LDA
      DO 20, K = 1, MIN( M - 1, N )

*        Use a Householder reflection to zero the kth column of A.
*        First set up the reflection.

         CALL SGRFG ( M - K, A( K, K ), A( K + 1, K ), 1, ZERO,
     $                ZETA( K ) )
         IF( ( ZETA( K ).GT.ZERO ).AND.( K.LT.N ) )THEN
            IF( ( K + 1 ).EQ.N )
     $         LA = M - K + 1
            TEMP      = A( K, K )
            A( K, K ) = ZETA( K )

*           We now perform the operation  A := Q( k )*A.

*           Let B denote the bottom ( m - k + 1 ) by ( n - k ) part
*           of A.

*           First form  work = B'*u. ( work is stored in the elements
*           ZETA( k + 1 ), ..., ZETA( n ). )

            CALL SGEMV ( 'Transpose', M - K + 1, N - K,
     $                   ONE, A( K, K + 1 ), LA, A( K, K ), 1,
     $                   ZERO, ZETA( K + 1 ), 1 )

*           Now form  B := B - u*work'.

            CALL SGER  ( M - K + 1, N - K, -ONE, A( K, K ), 1,
     $                   ZETA( K + 1 ), 1, A( K, K + 1 ), LA )

*           Restore beta.

            A( K, K ) = TEMP
         END IF
   20 CONTINUE

*     Store the final zeta when m.eq.n.

      IF( M.EQ.N )
     $   ZETA( N ) = ZERO

      INFORM = 0
      RETURN

*     End of SGEQR .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SGEQRP( PIVOT, M, N, A, LDA, ZETA, PERM, WORK, INFORM )
      CHARACTER*1        PIVOT
      INTEGER            M, N, LDA, INFORM
      INTEGER            PERM( * )
      REAL               A( LDA, * ), ZETA( * ), WORK( * )

      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/

C  1. Purpose
C     =======
C
C  SGEQRP reduces the  m by n matrix A to upper triangular form by means
C  of orthogonal transformations and column permutations.
C
C  2. Description
C     ===========
C
C  The m by n matrix A is factorized as
C
C     A = Q*( R )*P'      when   m.gt.n,
C           ( 0 )
C
C     A = Q*R*P'          when   m = n,
C
C     A = Q*( R  X )*P'   when   m.lt.n,
C
C  where  Q  is  an  m by m  orthogonal matrix, R  is a  min( m, n )  by
C  min( m, n )  upper triangular matrix and  P is an  n by n permutation
C  matrix.
C
C  The  factorization  is  obtained  by  Householder's  method. The  kth
C  transformation matrix, Q( k ),  which is used to introduce zeros into
C  the kth column of A is given in the form
C
C     Q( k ) = ( I     0   ),
C              ( 0  T( k ) )
C
C  where
C
C     T( k ) = I - u( k )*u( k )',   u( k ) = ( zeta( k ) ),
C                                             (    z( k ) )
C
C  zeta( k )  is a scalar and  z( k )  is an  ( m - k )  element vector.
C  zeta( k )  and  z( k ) are chosen to annhilate the elements below the
C  triangular part of  A.
C
C  The vector  u( k )  is returned in the kth element of ZETA and in the
C  kth column of A, such that zeta( k ) is in ZETA( k ) and the elements
C  of z( k ) are in a( k + 1, k ), ..., a( m, k ). The elements of R are
C  returned in the upper triangular part of A.
C
C  Q is given by
C
C     Q = ( Q( p )*Q( p - 1 )*...*Q( 1 ) )',
C
C  where p = min( m - 1, n ).
C
C  Two options are available for the column permutations. In either case
C  the column for which the  sub-diagonal elements are to be annihilated
C  at the  kth step is chosen from the remaining ( n - k + 1 )  columns.
C  The  particular column chosen as the pivot column is either that  for
C  which  the  unreduced  part  ( elements k onwards )  has the  largest
C  Euclidean  length, or  is that for  which the ratio of the  Euclidean
C  length  of the  unreduced part  to the  Euclidean length of the whole
C  column is a maximum.
C
C  3. Parameters
C     ==========
C
C  PIVOT  - CHARACTER*1.
C
C           On  entry, PIVOT  specifies  the  pivoting  strategy  to  be
C           performed as follows.
C
C           PIVOT = 'C' or 'c'
C
C              Column  interchanges  are  to be  incorporated  into  the
C              factorization, such that the  column whose unreduced part
C              has  maximum  Euclidean  length  is chosen  as the  pivot
C              column at each step.
C
C           PIVOT = 'S' or 's'
C
C              Scaled  column interchanges  are to be  incorporated into
C              the  factorization, such  that the  column for which  the
C              ratio  of the  Euclidean  length of the unreduced part of
C              the column to the original Euclidean length of the column
C              is a maximum is chosen as the  pivot column at each step.
C
C           Unchanged on exit.
C
C  M      - INTEGER.
C
C           On entry, M  must specify the number of rows of A. M must be
C           at  least  zero. When  M = 0  then  an  immediate return  is
C           effected.
C
C           Unchanged on exit.
C
C  N      - INTEGER.
C
C           On entry, N  must specify the number of columns of A. N must
C           be  at least zero. When  N = 0  then an immediate return  is
C           effected.
C
C           Unchanged on exit.
C
C  A      - 'real' array of DIMENSION ( LDA, n ).
C
C           Before entry, the leading  M by N  part of the array  A must
C           contain the matrix to be factorized.
C
C           On  exit, the  min( M, N ) by min( M, N )  upper  triangular
C           part of A will contain the upper triangular matrix R and the
C           M by min( M, N )  strictly lower triangular part of  A  will
C           contain details  of the  factorization  as  described above.
C           When m.lt.n then the remaining M by ( N - M ) part of A will
C           contain the matrix X.
C
C  LDA    - INTEGER.
C
C           On  entry, LDA  must  specify  the leading dimension of  the
C           array  A  as declared in the calling (sub) program. LDA must
C           be at least  m.
C
C           Unchanged on exit.
C
C  ZETA   - 'real' array of DIMENSION at least ( n ).
C
C           On exit, ZETA( k )  contains the scalar  zeta  for  the  kth
C           transformation. If T( k ) = I then ZETA( k) = 0.0, otherwise
C           ZETA( k )  contains the scalar  zeta( k ) as described above
C           and  is  always  in  the  range  ( 1.0, sqrt( 2.0 ) ).  When
C           n .gt. m  the  elements  ZETA( m + 1 ),  ZETA( m + 2 ), ...,
C           ZETA( n )  are used as internal workspace.
C
C  PERM   - INTEGER array of DIMENSION at least min( m, n ).
C
C           On exit, PERM  contains details of the permutation matrix P,
C           such  that  PERM( k ) = k  if no  column interchange occured
C           at  the  kth  step  and  PERM( k ) = j, ( k .lt. j .le. n ),
C           if  columns  k and j  were  interchanged at  the  kth  step.
C           Note  that, although  there are  min( m - 1, n )  orthogonal
C           transformations, there are min( m, n ) permutations.
C
C  WORK   - 'real' array of DIMENSION at least ( 2*n ).
C
C           Used as internal workspace.
C
C           On exit, WORK( j ), j = 1, 2, ..., n, contains the Euclidean
C           length  of the  jth  column  of the  permuted  matrix  A*P'.
C
C  INFORM - INTEGER.
C
C           On  successful exit, INFORM  will be zero, otherwise  INFORM
C           will  be set to unity indicating that an input parameter has
C           been  incorrectly supplied. See the next section for further
C           details.
C
C  4. Diagnostic Information
C     ======================
C
C  INFORM = 1
C
C     One or more of the following conditions holds:
C
C        PIVOT .ne. 'C' or 'c' or 'S' or 's'
C        M     .lt. 0
C        N     .lt. 0
C        LDA   .lt. M
C
C  5. Further information
C     ===================
C
C  Following the use of this routine the operations
C
C     B := Q'*B   and   B := Q*B,
C
C  where  B  is an  m by k  matrix, can  be  performed  by calls to  the
C  auxiliary  linear algebra  routine  SGEAPQ. The  operation  B := Q'*B
C  can be obtained by the call:
C
C     INFORM = 0
C     CALL SGEAPQ( 'Transpose', 'Separate', M, N, A, LDA, ZETA,
C    $             K, B, LDB, WORK, INFORM )
C
C  and  B := Q*B  can be obtained by the call:
C
C     INFORM = 0
C     CALL SGEAPQ( 'No transpose', 'Separate', M, N, A, LDA, ZETA,
C    $             K, B, LDB, WORK, INFORM )
C
C  In  both  cases  WORK  must be  a  k  element array  that is used  as
C  workspace. If B is a one-dimensional array ( single column ) then the
C  parameter  LDB  can be replaced by  M. See routine SGEAPQ for further
C  details.
C
C  Also following the use of this routine the operations
C
C     B := P'*B   and   B := P*B,
C
C  where B is an n by k matrix, and the operations
C
C     B := B*P    and   B := B*P',
C
C  where  B is a k by n  matrix, can  be performed by calls to the basic
C  linear  algebra  routine  SGEAP .  The  operation  B := P'*B  can  be
C  obtained by the call:
C
C     CALL SGEAP ( 'Left', 'Transpose', N, MIN( M, N ), PERM,
C    $             K, B, LDB )
C
C  the operation  B := P*B  can be obtained by the call:
C
C     CALL SGEAP ( 'Left', 'No transpose', N, MIN( M, N ), PERM,
C    $             K, B, LDB )
C
C  If  B is a one-dimensional array ( single column ) then the parameter
C  LDB  can be replaced by  N  in the above two calls.
C  The operation  B := B*P  can be obtained by the call:
C
C     CALL SGEAP ( 'Right', 'No transpose', K, MIN( M, N ), PERM,
C    $             M, B, LDB )
C
C  and  B := B*P'  can be obtained by the call:
C
C     CALL SGEAP ( 'Right', 'Transpose', K, MIN( M, N ), PERM,
C    $             M, B, LDB )
C
C  If  B is a one-dimensional array ( single column ) then the parameter
C  LDB  can be replaced by  K  in the above two calls.
C  See routine SGEAP for further details.
C
C  Operations involving  the matrix  R  are performed by  the
C  Level 2 BLAS  routines  STRSV  and STRMV.  Note that no test for near
C  singularity of  R is incorporated in this routine or in routine STRSV
C  and  so it is  strongly recommended that the auxiliary linear algebra
C  routine  SUTCO  be called, prior to solving equations involving R, in
C  order  to determine whether  or not  R  is nearly singular. If  R  is
C  nearly  singular then  the  auxiliary  linear algebra  routine  SUTSV
C  can  be  used  to  determine  the  singular value decomposition of R.
C  Operations  involving  the  matrix  X  can also be  performed  by the
C  Level 2  BLAS  routines.  Matrices  of  the  form   ( R  X )  can  be
C  factorized as
C
C     ( R  X ) = ( T  0 )*S',
C
C  where  T is upper triangular and S is orthogonal, using the auxiliary
C  linear algebra routine  DUTRQ .
C
C
C  Nag Fortran 77 Auxiliary linear algebra routine.
C
C  -- Written on 13-December-1984.
C     Sven Hammarling, Nag Central Office.
C
      EXTERNAL           MCHPAR, SGEMV , SGER  , SGRFG , SNRM2 , SSWAP
      INTRINSIC          ABS   , MAX   , MIN   , SQRT
      INTEGER            J     , JMAX  , K     , LA
      REAL               EPS   , MAXNRM, NORM  , SNRM2 , TEMP  , TOL
      REAL               LAMDA
      PARAMETER        ( LAMDA = 1.0E-2 )
      REAL               ONE   ,         ZERO
      PARAMETER        ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

*     Check the input parameters.

      IF( MIN( M, N ).EQ.0 )THEN
         INFORM = 0
         RETURN
      END IF
      IF( ( ( PIVOT.NE.'C' ).AND.( PIVOT.NE.'c' ).AND.
     $      ( PIVOT.NE.'S' ).AND.( PIVOT.NE.'s' )      ).OR.
     $    ( M.LT.0 ).OR.( N.LT.0 ).OR.( LDA.LT.M )           )THEN
         INFORM = 1
         RETURN
      END IF

*     Compute eps and the initial column norms.

      CALL MCHPAR()
      EPS = WMACH( 3 )
      DO 10, J = 1, N
         WORK( J )     = SNRM2 ( M, A( 1, J ), 1 )
         WORK( J + N ) = WORK( J )
   10 CONTINUE

*     Perform the factorization. TOL is the tolerance for SGRFG .

      LA = LDA
      DO 50, K = 1, MIN( M, N )

*        Find the pivot column.

         MAXNRM = ZERO
         JMAX   = K
         DO 20, J = K, N
            IF( ( PIVOT.EQ.'C' ).OR.( PIVOT.EQ.'c' ) )THEN
               IF( WORK( J + N  ).GT.MAXNRM )THEN
                  MAXNRM = WORK( J + N )
                  JMAX   = J
               END IF
            ELSE IF( WORK( J ).GT.ZERO )THEN
               IF( ( WORK( J + N )/WORK( J ) ).GT.MAXNRM )THEN
                  MAXNRM = WORK( J + N )/WORK( J )
                  JMAX   = J
               END IF
            END IF
   20    CONTINUE
         PERM( K ) = JMAX
         IF( JMAX.GT.K )THEN
            CALL SSWAP ( M, A( 1, K ), 1, A( 1, JMAX ), 1 )
            TEMP             = WORK( K )
            WORK( K )        = WORK( JMAX )
            WORK( JMAX )     = TEMP
            WORK( JMAX + N ) = WORK( K + N )
            PERM( K )        = JMAX
         END IF
         TOL = EPS*WORK( K )
         IF( K.LT.M )THEN

*           Use a Householder reflection to zero the kth column of A.
*           First set up the reflection.

            CALL SGRFG ( M - K, A( K, K ), A( K + 1, K ), 1, TOL,
     $                   ZETA( K ) )
            IF( K.LT.N )THEN
               IF( ZETA( K ).GT.ZERO )THEN
                  IF( ( K + 1 ).EQ.N )
     $               LA = M - K + 1
                  TEMP      = A( K, K )
                  A( K, K ) = ZETA( K )

*                 We now perform the operation  A := Q( k )*A.

*                 Let B denote the bottom ( m - k + 1 ) by ( n - k )
*                 part of A.

*                 First form  work = B'*u. ( work is stored in the
*                 elements ZETA( k + 1 ), ..., ZETA( n ). )

                  CALL SGEMV ( 'Transpose', M - K + 1, N - K,
     $                         ONE, A( K, K + 1 ), LA, A( K, K ), 1,
     $                         ZERO, ZETA( K + 1 ), 1 )

*                 Now form  B := B - u*work'.

                  CALL SGER  ( M - K + 1, N - K, -ONE, A( K, K ), 1,
     $                         ZETA( K + 1 ), 1, A( K, K + 1 ), LA )

*                 Restore beta.

                  A( K, K ) = TEMP
               END IF

*              Update the unreduced column norms. Use the Linpack
*              criterion for when to recompute the norms, except that
*              we retain the original column lengths throughout and use
*              a smaller lamda.

               DO 40, J = K + 1, N
                  IF( WORK( J + N ).GT.ZERO )THEN
                     TEMP = ABS( A( K, J ) )/WORK( J + N )
                     TEMP = MAX( ( ONE + TEMP )*( ONE - TEMP ), ZERO )
                     NORM = TEMP
                     TEMP = ONE +
     $                      LAMDA*TEMP*( WORK( J + N )/WORK( J ) )**2
                     IF( TEMP.GT.ONE )THEN
                        WORK( J + N ) = WORK( J + N )*SQRT( NORM )
                     ELSE
                        WORK( J + N ) = SNRM2 ( M - K,
     $                                          A( K + 1, J ), 1 )
                     END IF
                  END IF
   40          CONTINUE
            END IF
         END IF
   50 CONTINUE

*     Store the final zeta when m.le.n.

      IF( M.LE.N )
     $   ZETA( M ) = ZERO

      INFORM = 0
      RETURN

*     End of SGEQRP.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SGEAP ( SIDE, TRANS, M, N, PERM, K, B, LDB )
*     .. Scalar Arguments ..
      INTEGER            K, LDB, M, N
      CHARACTER*1        SIDE, TRANS
*     .. Array Arguments ..
      REAL               B( LDB, * )
      INTEGER            PERM( * )
*     ..
*
*  Purpose
*  =======
*
*  SGEAP  performs one of the transformations
*
*     B := P'*B   or   B := P*B,   where B is an m by k matrix,
*
*  or
*
*     B := B*P'   or   B := B*P,   where B is a k by m matrix,
*
*  P being an m by m permutation matrix of the form
*
*     P = P( 1, index( 1 ) )*P( 2, index( 2 ) )*...*P( n, index( n ) ),
*
*  where  P( i, index( i ) ) is the permutation matrix that interchanges
*  items i and index( i ). That is P( i, index( i ) ) is the unit matrix
*  with rows and columns  i and index( i )  interchanged.  Of course, if
*  index( i ) = i  then  P( i, index( i ) ) = I.
*
*  This routine  is intended for use in  conjunction with  Nag auxiliary
*  routines that  perform  interchange  operations,  such  as  pivoting.
*
*  Parameters
*  ==========
*
*  SIDE   - CHARACTER*1.
*  TRANS
*           On entry,  SIDE  ( Left-hand side, or Right-hand side )  and
*           TRANS  ( Transpose, or No transpose )  specify the operation
*           to be performed as follows.
*
*           SIDE = 'L' or 'l'   and   TRANS = 'T' or 't'
*
*              Perform the operation   B := P'*B.
*
*           SIDE = 'L' or 'l'   and   TRANS = 'N' or 'n'
*
*              Perform the operation   B := P*B.
*
*           SIDE = 'R' or 'r'   and   TRANS = 'T' or 't'
*
*              Perform the operation   B := B*P'.
*
*           SIDE = 'R' or 'r'   and   TRANS = 'N' or 'n'
*
*              Perform the operation   B := B*P.
*
*           Unchanged on exit.
*
*  M      - INTEGER.
*
*           On entry, M must specify the order of the permutation matrix
*           P.  M must be at least zero.  When  M = 0  then an immediate
*           return is effected.
*
*           Unchanged on exit.
*
*  N      - INTEGER.
*
*           On entry,  N must specify the value of n. N must be at least
*           zero.  When  N = 0  then an  immediate  return is  effected.
*
*           Unchanged on exit.
*
*  PERM   - INTEGER array of DIMENSION at least ( n ).
*
*           Before  entry,  PERM  must  contain the  n  indices  for the
*           permutation matrices. index( i ) must satisfy
*
*              1 .le. index( i ) .le. m.
*
*           It is usual for index( i ) to be at least i, but this is not
*           necessary for this routine.
*
*           Unchanged on exit.
*
*  K      - INTEGER.
*
*           On entry with  SIDE = 'L' or 'l',  K must specify the number
*           of columns of B and on entry with  SIDE = 'R' or 'r', K must
*           specify the  number of rows of B.  K must be at least  zero.
*           When  K = 0  then an immediate return is effected.
*
*           Unchanged on exit.
*
*  B      - REAL array of  DIMENSION  ( LDB, ncolb ),  where
*           ncolb = k   when   SIDE = 'L' or 'l'  and   ncolb = m   when
*           SIDE = 'R' or 'r'.
*
*           Before entry  with  SIDE = 'L' or 'l',  the  leading  M by K
*           part  of  the  array   B  must  contain  the  matrix  to  be
*           transformed  and  before entry with  SIDE = 'R' or 'r',  the
*           leading  K by M part of the array  B must contain the matrix
*           to  be  transformed.  On  exit,  B  is  overwritten  by  the
*           transformed matrix.
*
*  LDB    - INTEGER.
*
*           On entry,  LDB  must specify  the  leading dimension  of the
*           array  B  as declared  in the  calling  (sub) program.  When
*           SIDE = 'L' or 'l'   then  LDB  must  be  at  least  m,  when
*           SIDE = 'R' or 'r'   then  LDB  must  be  at  least  k.
*           Unchanged on exit.
*
*
*  Nag Fortran 77 O( n**2 ) basic linear algebra routine.
*
*  -- Written on 13-January-1986.
*     Sven Hammarling, Nag Central Office.
*
*
*     .. Local Scalars ..
      REAL               TEMP
      INTEGER            I, J, L
      LOGICAL            LEFT, NULL, RIGHT, TRNSP
*     .. Intrinsic Functions ..
      INTRINSIC          MIN
*     ..
*     .. Executable Statements ..
      IF( MIN( M, N, K ).EQ.0 )
     $   RETURN
      LEFT  = ( SIDE .EQ.'L' ).OR.( SIDE .EQ.'l' )
      RIGHT = ( SIDE .EQ.'R' ).OR.( SIDE .EQ.'r' )
      NULL  = ( TRANS.EQ.'N' ).OR.( TRANS.EQ.'n' )
      TRNSP = ( TRANS.EQ.'T' ).OR.( TRANS.EQ.'t' )
      IF( LEFT )THEN
         IF( TRNSP )THEN
            DO 20, I = 1, N
               IF( PERM( I ).NE.I )THEN
                  L = PERM( I )
                  DO 10, J = 1, K
                     TEMP      = B( I, J )
                     B( I, J ) = B( L, J )
                     B( L, J ) = TEMP
   10             CONTINUE
               END IF
   20       CONTINUE
         ELSE IF( NULL )THEN
            DO 40, I = N, 1, -1
               IF( PERM( I ).NE.I )THEN
                  L = PERM( I )
                  DO 30, J = 1, K
                     TEMP      = B( L, J )
                     B( L, J ) = B( I, J )
                     B( I, J ) = TEMP
   30             CONTINUE
               END IF
   40       CONTINUE
         END IF
      ELSE IF( RIGHT )THEN
         IF( TRNSP )THEN
            DO 60, J = 1, N
               IF( PERM( J ).NE.J )THEN
                  L = PERM( J )
                  DO 50, I = 1, K
                     TEMP      = B( I, J )
                     B( I, J ) = B( L, J )
                     B( L, J ) = TEMP
   50             CONTINUE
               END IF
   60       CONTINUE
         ELSE IF( NULL )THEN
            DO 80, J = N, 1, -1
               IF( PERM( J ).NE.J )THEN
                  L = PERM( J )
                  DO 70, I = 1, K
                     TEMP      = B( L, J )
                     B( L, J ) = B( I, J )
                     B( I, J ) = TEMP
   70             CONTINUE
               END IF
   80       CONTINUE
         END IF
      END IF
*
      RETURN
*
*     End of SGEAP . ( F06QJF )
*
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE SGEAPQ( TRANS, WHEREZ, M, N, A, LDA, ZETA,
     $                   NCOLB, B, LDB, WORK, INFORM )
      CHARACTER*1        TRANS, WHEREZ
      INTEGER            M, N, LDA, NCOLB, LDB, INFORM
      REAL               A( LDA, * ), ZETA( * ), B( LDB, * ), WORK( * )
C
C  1. Purpose
C     =======
C
C  SGEAPQ performs one of the transformations
C
C     B := Q'*B   or   B := Q*B,
C
C  where B is an m by ncolb matrix and Q is an m by m orthogonal matrix,
C  given as the product of  Householder transformation matrices, details
C  of  which are stored in the  m by n ( m.ge.n )  array  A  and, if the
C  parameter  WHEREZ = 'S' or 's', in the array ZETA.
C
C  This  routine is  intended for use following auxiliary linear algebra
C  routines such as  SGEQR , SGEHES and SSLTRI. ( See those routines for
C  example calls. )
C
C  2. Description
C     ===========
C
C  Q is assumed to be given by
C
C     Q = ( Q( p )*Q( p - 1 )*...*Q( 1 ) )',
C
C  Q( k ) being given in the form
C
C     Q( k ) = ( I     0   ),
C              ( 0  T( k ) )
C
C  where
C
C     T( k ) = I - u( k )*u( k )',   u( k ) = ( zeta( k ) ),
C                                             (    z( k ) )
C
C  zeta( k )  is a scalar and  z( k )  is an  ( m - k )  element vector.
C
C  z( k )  must  be  supplied  in  the  kth  column  of  A  in  elements
C  a( k + 1, k ), ..., a( m, k )  and  zeta( k ) must be supplied either
C  in  a( k, k )  or in  zeta( k ), depending upon the parameter WHEREZ.
C
C  To obtain Q explicitly B may be set to I and premultiplied by Q. This
C  is more efficient than obtaining Q'.
C
C  3. Parameters
C     ==========
C
C  TRANS  - CHARACTER*1.
C
C           On entry, TRANS  specifies the operation to be performed  as
C           follows.
C
C           TRANS = ' ' or 'N' or 'n'
C
C              Perform the operation  B := Q*B.
C
C           TRANS = 'T' or 't' or 'C' or 'c'
C
C              Perform the operation  B := Q'*B.
C
C           Unchanged on exit.
C
C  WHEREZ - CHARACTER*1.
C
C           On entry, WHEREZ specifies where the elements of zeta are to
C           be found as follows.
C
C           WHEREZ = 'I' or 'i'
C
C              The elements of zeta are in A.
C
C           WHEREZ = 'S' or 's'
C
C              The elements of zeta are separate from A, in ZETA.
C
C           Unchanged on exit.
C
C  M      - INTEGER.
C
C           On entry, M  must specify the number of rows of A. M must be
C           at least n.
C
C           Unchanged on exit.
C
C  N      - INTEGER.
C
C           On entry, N  must specify the number of columns of A. N must
C           be  at least zero. When  N = 0  then an immediate return  is
C           effected.
C
C           Unchanged on exit.
C
C  A      - 'real' array of DIMENSION ( LDA, n ).
C
C           Before entry, the leading  M by N  stricly lower  triangular
C           part of the array  A  must contain details of the matrix  Q.
C           In  addition, when  WHEREZ = 'I' or 'i'  then  the  diagonal
C           elements of A must contain the elements of zeta.
C
C           Unchanged on exit.
C
C  LDA    - INTEGER.
C
C           On  entry, LDA  must specify  the leading dimension  of  the
C           array  A  as declared in the calling (sub) program. LDA must
C           be at least m.
C
C           Unchanged on exit.
C
C  ZETA   - 'real' array of DIMENSION at least min( m - 1, n ).
C
C           Before entry with  WHEREZ = 'S' or 's', the array  ZETA must
C           contain the elements of the vector  zeta.
C
C           When  WHEREZ = 'I' or 'i', the array ZETA is not referenced.
C
C           Unchanged on exit.
C
C  NCOLB  - INTEGER.
C
C           On  entry, NCOLB  must specify  the number of columns of  B.
C           NCOLB  must  be  at  least  zero.  When  NCOLB = 0  then  an
C           immediate return is effected.
C
C           Unchanged on exit.
C
C  B      - 'real' array of DIMENSION ( LDB, ncolb ).
C
C           Before entry, the leading  M by NCOLB  part of  the array  B
C           must  contain  the matrix to be  transformed.
C
C           On  exit,  B  is  overwritten  by  the  transformed  matrix.
C
C  LDB    - INTEGER.
C
C           On  entry, LDB  must specify  the  leading dimension of  the
C           array  B as declared in the calling (sub) program. LDB  must
C           be at least m.
C
C           Unchanged on exit.
C
C  WORK   - 'real' array of DIMENSION at least ( ncolb ).
C
C           Used as internal workspace.
C
C  INFORM - INTEGER.
C
C           On  successful exit  INFORM  will be zero, otherwise  INFORM
C           will  be set to unity indicating that an input parameter has
C           been  incorrectly  set. See  the  next  section  for further
C           details.
C
C  4. Diagnostic Information
C     ======================
C
C  INFORM = 1
C
C     One or more of the following conditions holds:
C
C        TRANS  .ne. ' ' or 'N' or 'n' or 'T' or 't' or 'C' or 'c'
C        WHEREZ .ne. 'I' or 'i' or 'S' or 's'
C        M      .lt. N
C        N      .lt. 0
C        LDA    .lt. M
C        NCOLB  .lt. 0
C        LDB    .lt. M
C
C
C  Nag Fortran 77 Auxiliary linear algebra routine.
C
C  -- Written on 15-November-1984.
C     Sven Hammarling, Nag Central Office.
C
      EXTERNAL           SGEMV , SGER
      INTRINSIC          MIN
      INTEGER            J     , K     , KK    , LB
      REAL               TEMP
      REAL               ONE   ,         ZERO
      PARAMETER        ( ONE   = 1.0E+0, ZERO  = 0.0E+0 )

*     Check the input parameters.

      IF( MIN( N, NCOLB ).EQ.0 )THEN
         INFORM = 0
         RETURN
      END IF
      IF( ( ( TRANS .NE.' ' ).AND.
     $      ( TRANS .NE.'N' ).AND.( TRANS .NE.'n' ).AND.
     $      ( TRANS .NE.'T' ).AND.( TRANS .NE.'t' ).AND.
     $      ( TRANS .NE.'C' ).AND.( TRANS .NE.'c' )      ).OR.
     $    ( ( WHEREZ.NE.'I' ).AND.( WHEREZ.NE.'i' ).AND.
     $      ( WHEREZ.NE.'S' ).AND.( WHEREZ.NE.'s' )      ).OR.
     $    ( M.LT.N ).OR.( N.LT.0 ).OR.( LDA.LT.M ).OR.
     $    ( NCOLB.LT.0 ).OR.( LDB.LT.M )                      )THEN
         INFORM = 1
         RETURN
      END IF

*     Perform the transformation.

      LB = LDB
      DO 20, KK = 1, MIN( M - 1, N )
         IF( ( TRANS.EQ.'T' ).OR.( TRANS.EQ.'t' ).OR.
     $       ( TRANS.EQ.'C' ).OR.( TRANS.EQ.'c' )     )THEN

*           Q'*B = Q( p )*...*Q( 2 )*Q( 1 )*B,     p = min( m - 1, n ).

            K = KK
         ELSE

*           Q*B  = Q( 1 )'*Q( 2 )'*...*Q( p )'*B,  p = min( m - 1, n ).
*           Note that  Q( k )' = Q( k ).

            K = MIN( N, M - 1 ) + 1 - KK
         END IF
         IF( ( WHEREZ.EQ.'S' ).OR.( WHEREZ.EQ.'s' ) )THEN
            TEMP      = A( K, K )
            A( K, K ) = ZETA( K )
         END IF

*        If ZETA( k ) is zero then Q( k ) = I and we can skip the kth
*        transformation.

         IF( A( K, K ).GT.ZERO )THEN
            IF( NCOLB.EQ.1 )
     $         LB = M - K + 1

*           Let C denote the bottom ( m - k + 1 ) by ncolb part of B.

*           First form  work = C'*u.

            DO 10, J = 1, NCOLB
               WORK( J ) = ZERO
   10       CONTINUE
            CALL SGEMV ( 'Transpose', M - K + 1, NCOLB,
     $                   ONE, B( K, 1 ), LB, A( K, K ), 1,
     $                   ZERO, WORK, 1 )

*           Now form  C := C - u*work'.

            CALL SGER  ( M - K + 1, NCOLB, -ONE, A( K, K ), 1,
     $                   WORK, 1, B( K, 1 ), LB )
         END IF

*        Restore the diagonal element of A.

         IF( ( WHEREZ.EQ.'S' ).OR.( WHEREZ.EQ.'s' ) )
     $      A( K, K ) = TEMP
   20 CONTINUE

      INFORM = 0
      RETURN

*     End of SGEAPQ.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*     File  CMSUBS FORTRAN
*
*     CMALF1   CMALF    CMCHK    CMPERM   CMPRT    CMQMUL   CMR1MD
*     CMRSWP   CMTSOL
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMALF1( FIRSTV, NEGSTP, BIGALF, BIGBND, PNORM,
     $                   JADD1 , JADD2 , PALFA1, PALFA2,
     $                   ISTATE, N, NROWA, NCTOTL,
     $                   ANORM, AP, AX, BL, BU, FEATOL, P, X )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            FIRSTV, NEGSTP
      INTEGER            ISTATE(NCTOTL)
      REAL               ANORM(*), AP(*), AX(*)
      REAL               BL(NCTOTL), BU(NCTOTL), FEATOL(NCTOTL),
     $                   P(N), X(N)

      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL4CM/ EPSPT3, EPSPT5, EPSPT8, EPSPT9

      LOGICAL            CMDBG
      INTEGER            LCMDBG
      PARAMETER         (LCMDBG = 5)
      COMMON    /CMDEBG/ ICMDBG(LCMDBG), CMDBG

************************************************************************
*     CMALF1  finds steps PALFA1, PALFA2 such that
*        X + PALFA1*P  reaches a linear constraint that is currently not
*                      in the working set but is satisfied.
*        X + PALFA2*P  reaches a linear constraint that is currently not
*                      in the working set but is violated.
*     The constraints are perturbed by an amount FEATOL, so that PALFA1
*     is slightly larger than it should be,  and PALFA2 is slightly
*     smaller than it should be.  This gives some leeway later when the
*     exact steps are computed by CMALF.
*
*     Constraints in the working set are ignored  (ISTATE(j) .GE. 1).
*
*     If NEGSTP is true, the search direction will be taken to be  - P.
*
*
*     Values of ISTATE(j)....
*
*        - 2         - 1         0           1          2         3
*     a'x lt bl   a'x gt bu   a'x free   a'x = bl   a'x = bu   bl = bu
*
*     The values  -2  and  -1  do not occur once a feasible point has
*     been found.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original Fortran 66 version written  May 1980.
*     This version of CMALF1 dated 26-June-1986.
************************************************************************
      LOGICAL            LASTV
      INTRINSIC          ABS
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      IF (CMDBG  .AND.  ICMDBG(3) .GT. 0) WRITE (NOUT, 1100)
      LASTV  = .NOT. FIRSTV
      JADD1  = 0
      JADD2  = 0
      PALFA1 = BIGALF

      PALFA2 = ZERO
      IF (FIRSTV) PALFA2 = BIGALF

      DO 200 J = 1, NCTOTL
         JS = ISTATE(J)
         IF (JS .LE. 0) THEN
            IF (J .LE. N) THEN
               ATX    = X(J)
               ATP    = P(J)
               ROWNRM = ONE
            ELSE
               I      = J - N
               ATX    = AX(I)
               ATP    = AP(I)
               ROWNRM = ONE  +  ANORM(I)
            END IF
            IF (NEGSTP) ATP = - ATP

            IF ( ABS( ATP ) .LE. EPSPT9*ROWNRM*PNORM) THEN

*              This constraint appears to be constant along P.  It is
*              not used to compute the step.  Give the residual a value
*              that can be spotted in the debug output.

               RES = - ONE
            ELSE IF (ATP .LE. ZERO  .AND.  JS .NE. -2) THEN
*              ---------------------------------------------------------
*              a'x  is decreasing and the lower bound is not violated.
*              ---------------------------------------------------------
*              First test for smaller PALFA1.

               ABSATP = - ATP
               IF (BL(J) .GT. (-BIGBND)) THEN
                  RES    = ATX - BL(J) + FEATOL(J)
                  IF (BIGALF*ABSATP .GT. ABS( RES )) THEN
                     IF (PALFA1*ABSATP .GT. RES)  THEN
                        PALFA1 = RES / ABSATP
                        JADD1  = J
                     END IF
                  END IF
               END IF

               IF (JS .EQ. -1) THEN

*                 The upper bound is violated.  Test for either larger
*                 or smaller PALFA2, depending on the value of FIRSTV.

                  RES    = ATX - BU(J) - FEATOL(J)
                  IF (BIGALF*ABSATP .GT. ABS( RES )) THEN
                     IF (FIRSTV  .AND.  PALFA2*ABSATP .GT. RES  .OR.
     $                    LASTV  .AND.  PALFA2*ABSATP .LT. RES) THEN
                        PALFA2 = RES / ABSATP
                        JADD2  = J
                     END IF
                  END IF
               END IF
            ELSE IF (ATP .GT. ZERO  .AND.  JS .NE. -1) THEN
*              ---------------------------------------------------------
*              a'x  is increasing and the upper bound is not violated.
*              ---------------------------------------------------------
*              Test for smaller PALFA1.

               IF (BU(J) .LT. BIGBND) THEN
                  RES = BU(J) - ATX + FEATOL(J)
                  IF (BIGALF*ATP .GT. ABS( RES )) THEN
                     IF (PALFA1*ATP .GT. RES) THEN
                        PALFA1 = RES / ATP
                        JADD1  = J
                     END IF
                  END IF
               END IF

               IF (JS .EQ. -2) THEN

*                 The lower bound is violated.  Test for a new PALFA2.

                  RES  = BL(J) - ATX - FEATOL(J)
                  IF (BIGALF*ATP .GT. ABS( RES )) THEN
                     IF (FIRSTV  .AND.  PALFA2*ATP .GT. RES  .OR.
     $                    LASTV  .AND.  PALFA2*ATP .LT. RES) THEN
                        PALFA2 = RES / ATP
                        JADD2  = J
                     END IF
                  END IF
               END IF
            END IF

            IF (CMDBG  .AND.  ICMDBG(3) .GT. 0)
     $         WRITE (NOUT, 1200) J, JS, FEATOL(J), RES,
     $                            ATP, JADD1, PALFA1, JADD2, PALFA2
         END IF
  200 CONTINUE

      RETURN

 1100 FORMAT(/ '    J  JS         FEATOL        RES             AP',
     $         '     JADD1       PALFA1     JADD2       PALFA2' /)
 1200 FORMAT(I5, I4, 3G15.5, 2(I6, G17.7))

*     End of  CMALF1.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMALF ( FIRSTV, HITLOW, ISTATE, INFORM, JADD,
     $                   N, NROWA, NCLIN, NCTOTL, NUMINF,
     $                   ALFA, PALFA, ATPHIT, BIGALF, BIGBND, PNORM,
     $                   ANORM, AP, AX, BL, BU, FEATOL, P, X )

C     IMPLICIT           REAL(A-H,O-Z)
      INTEGER            ISTATE(NCTOTL)
      REAL               ANORM(*), AP(*), AX(*),
     $                   BL(NCTOTL), BU(NCTOTL), FEATOL(NCTOTL),
     $                   P(N), X(N)
      LOGICAL            FIRSTV, HITLOW

      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL4CM/ EPSPT3, EPSPT5, EPSPT8, EPSPT9

      LOGICAL            CMDBG
      INTEGER            LCMDBG
      PARAMETER         (LCMDBG = 5)
      COMMON    /CMDEBG/ ICMDBG(LCMDBG), CMDBG

************************************************************************
*  CMALF   finds a step ALFA such that the point x + ALFA*P reaches one
*  of the linear constraints (including bounds).  Two possible steps are
*  defined as follows...
*
*  ALFA1   is the maximum step that can be taken without violating
*          one of the linear constraints that is currently satisfied.
*  ALFA2   reaches a linear constraint that is currently violated.
*          Usually this will be the furthest such constraint along P,
*          but if FIRSTV = .TRUE. it will be the first one along P.
*          This is used only when the problem has been determined to be
*          infeasible, and the sum of infeasibilities are being
*          minimized.  (ALFA2  is not defined if NUMINF = 0.)
*
*  ALFA will usually be the minimum of ALFA1 and ALFA2.
*  ALFA could be negative (since we allow inactive constraints
*  to be violated by as much as FEATOL).  In such cases, a
*  third possible step is computed, to find the nearest satisfied
*  constraint (perturbed by FEATOL) along the direction  - P.
*  ALFA  will be reset to this step if it is shorter.  This is the
*  only case for which the final step  ALFA  does not move X exactly
*  onto a constraint (the one denoted by JADD).
*
*  Constraints in the working set are ignored  (ISTATE(j) ge 1).
*
*  JADD    denotes which linear constraint is reached.
*
*  HITLOW  indicates whether it is the lower or upper bound that
*          has restricted ALFA.
*
*  Values of ISTATE(j)....
*
*     - 2         - 1         0           1          2         3
*  a'x lt bl   a'x gt bu   a'x free   a'x = bl   a'x = bu   bl = bu
*
*  The values -2 and -1 do not occur once a feasible point has been
*  found.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original Fortran 66 version written  May 1980.
*  This version of  CMALF  dated  10-June-1986.
************************************************************************
      LOGICAL            HLOW1, HLOW2, LASTV, NEGSTP, STEP2
      INTRINSIC          ABS, MIN
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      INFORM = 0

*     ------------------------------------------------------------------
*     First pass -- find steps to perturbed constraints, so that
*     PALFA1 will be slightly larger than the true step, and
*     PALFA2 will be slightly smaller than it should be.
*     In degenerate cases, this strategy gives us some freedom in the
*     second pass.  The general idea follows that described by P.M.J.
*     Harris, p.21 of Mathematical Programming 5, 1 (1973), 1--28.
*     ------------------------------------------------------------------

      NEGSTP = .FALSE.
      CALL CMALF1( FIRSTV, NEGSTP, BIGALF, BIGBND, PNORM,
     $             JADD1, JADD2, PALFA1, PALFA2,
     $             ISTATE, N, NROWA, NCTOTL,
     $             ANORM, AP, AX, BL, BU, FEATOL, P, X )

      JSAVE1 = JADD1
      JSAVE2 = JADD2

*     ------------------------------------------------------------------
*     Second pass -- recompute step-lengths without perturbation.
*     Amongst constraints that are less than the perturbed steps,
*     choose the one (of each type) that makes the largest angle
*     with the search direction.
*     ------------------------------------------------------------------
      IF (CMDBG  .AND.  ICMDBG(3) .GT. 0) WRITE (NOUT, 1000)
      ALFA1  = BIGALF
      ALFA2  = ZERO
      IF (FIRSTV) ALFA2 = BIGALF

      APMAX1 = ZERO
      APMAX2 = ZERO
      ATP1   = ZERO
      ATP2   = ZERO
      HLOW1  = .FALSE.
      HLOW2  = .FALSE.
      LASTV  = .NOT. FIRSTV

      DO 400 J = 1, NCTOTL
         JS = ISTATE(J)
         IF (JS .LE. 0) THEN
            IF (J  .LE. N)  THEN
               ATX    = X(J)
               ATP    = P(J)
               ROWNRM = ONE
            ELSE
               I      = J - N
               ATX    = AX(I)
               ATP    = AP(I)
               ROWNRM = ANORM(I) + ONE
            END IF

            IF ( ABS( ATP ) .LE. EPSPT9*ROWNRM*PNORM) THEN

*              This constraint appears to be constant along P.  It is
*              not used to compute the step.  Give the residual a value
*              that can be spotted in the debug output.

               RES = - ONE
            ELSE IF (ATP .LE. ZERO  .AND.  JS .NE. -2) THEN
*              ---------------------------------------------------------
*              a'x  is decreasing.
*              ---------------------------------------------------------
*              The lower bound is satisfied.  Test for smaller ALFA1.

               ABSATP = - ATP
               IF (BL(J) .GT. (-BIGBND)) THEN
                  RES    = ATX - BL(J)
                  IF (PALFA1*ABSATP .GE. RES  .OR.  J .EQ. JSAVE1) THEN
                     IF (APMAX1*ROWNRM*PNORM .LT. ABSATP) THEN
                        APMAX1 = ABSATP / (ROWNRM*PNORM)
                        ALFA1  = RES / ABSATP
                        JADD1  = J
                        ATP1   = ATP
                        HLOW1  = .TRUE.
                     END IF
                  END IF
               END IF

               IF (JS. EQ. -1)  THEN

*                 The upper bound is violated.  Test for either a bigger
*                 or smaller ALFA2,  depending on the value of FIRSTV.

                  RES    = ATX - BU(J)
                  IF (     (FIRSTV  .AND.  PALFA2*ABSATP .GE. RES
     $                 .OR.  LASTV  .AND.  PALFA2*ABSATP .LE. RES)
     $                 .OR.  J .EQ.  JSAVE2) THEN
                     IF (APMAX2*ROWNRM*PNORM .LT. ABSATP) THEN
                        APMAX2 = ABSATP / (ROWNRM*PNORM)
                        IF      (ABSATP .GE. ONE          ) THEN
                           ALFA2 = RES / ABSATP
                        ELSE IF (RES    .LT. BIGALF*ABSATP) THEN
                           ALFA2 = RES / ABSATP
                        ELSE
                           ALFA2 = BIGALF
                        END IF
                        JADD2  = J
                        ATP2   = ATP
                        HLOW2  = .FALSE.
                     END IF
                  END IF
               END IF
            ELSE IF (ATP .GT. ZERO  .AND.  JS .NE.  -1)  THEN
*              ---------------------------------------------------------
*              a'x  is increasing and the upper bound is not violated.
*              ---------------------------------------------------------
*              Test for smaller ALFA1.

               IF (BU(J) .LT. BIGBND) THEN
                  RES = BU(J) - ATX
                  IF (PALFA1*ATP .GE. RES  .OR.  J .EQ. JSAVE1) THEN
                     IF (APMAX1*ROWNRM*PNORM .LT. ATP) THEN
                        APMAX1 = ATP / (ROWNRM*PNORM)
                        ALFA1  = RES / ATP
                        JADD1  = J
                        ATP1   = ATP
                        HLOW1  = .FALSE.
                     END IF
                  END IF
               END IF

               IF (JS .EQ. -2)  THEN

*                 The lower bound is violated.  Test for a new ALFA2.

                  RES    = BL(J) - ATX
                  IF (     (FIRSTV  .AND.  PALFA2*ATP .GE. RES
     $                 .OR.  LASTV  .AND.  PALFA2*ATP .LE. RES)
     $                 .OR.  J .EQ.  JSAVE2) THEN
                     IF (APMAX2*ROWNRM*PNORM .LT. ATP) THEN
                        APMAX2 = ATP / (ROWNRM*PNORM)
                        IF      (ATP .GE. ONE       ) THEN
                           ALFA2 = RES / ATP
                        ELSE IF (RES .LT. BIGALF*ATP) THEN
                           ALFA2 = RES / ATP
                        ELSE
                           ALFA2 = BIGALF
                        END IF
                        JADD2  = J
                        ATP2   = ATP
                        HLOW2  = .TRUE.
                     END IF
                  END IF
               END IF
            END IF

            IF (CMDBG  .AND.  ICMDBG(3) .GT. 0)
     $      WRITE (NOUT, 1200) J, JS, FEATOL(J), RES, ATP, JADD1,
     $                         ALFA1, JADD2, ALFA2
         END IF
  400 CONTINUE

*     ==================================================================
*     Determine ALFA, the step to be taken.
*     ==================================================================
*     In the infeasible case, check whether to take the step ALFA2
*     rather than ALFA1...

      STEP2 = NUMINF .GT. 0  .AND.  JADD2 .GT. 0

*     We do so if ALFA2 is less than ALFA1 or (if FIRSTV is false)
*     lies in the range  (ALFA1, PALFA1)  and has a smaller value of
*     ATP.

      STEP2 = STEP2 .AND. (ALFA2 .LT. ALFA1   .OR.   LASTV  .AND.
     $                     ALFA2 .LE. PALFA1  .AND.  APMAX2 .GE. APMAX1)

      IF (STEP2) THEN
         ALFA   = ALFA2
         PALFA  = PALFA2
         JADD   = JADD2
         ATPHIT = ATP2
         HITLOW = HLOW2
      ELSE
         ALFA   = ALFA1
         PALFA  = PALFA1
         JADD   = JADD1
         ATPHIT = ATP1
         HITLOW = HLOW1

*        If ALFA1 is negative, the constraint to be added (JADD)
*        remains unchanged, but ALFA may be shortened to the step
*        to the nearest perturbed satisfied constraint along  - P.

         NEGSTP = ALFA .LT. ZERO
         IF (NEGSTP) THEN
            CALL CMALF1( FIRSTV, NEGSTP, BIGALF, BIGBND, PNORM,
     $                   JADD1, JADD2, PALFA1, PALFA2,
     $                   ISTATE, N, NROWA, NCTOTL,
     $                   ANORM, AP, AX, BL, BU, FEATOL, P, X )

            IF (CMDBG  .AND.  ICMDBG(1) .GT. 0)
     $         WRITE (NOUT, 9000) ALFA, PALFA1

            ALFA = - MIN( ABS( ALFA ), PALFA1 )
         END IF
      END IF

*     Test for undefined or infinite step.

      IF (JADD .EQ. 0) THEN
         ALFA   = BIGALF
         PALFA  = BIGALF
      END IF

      IF (ALFA .GE. BIGALF) INFORM = 3
      IF (CMDBG  .AND.  ICMDBG(1) .GT. 0  .AND.  INFORM .GT. 0)
     $   WRITE (NOUT, 9010) JADD, ALFA
      RETURN

 1000 FORMAT(/ ' CMALF  entered'
     $       / '    J  JS         FEATOL        RES             AP',
     $         '     JADD1        ALFA1     JADD2        ALFA2 '/)
 1200 FORMAT( I5, I4, 3G15.5, 2(I6, G17.7) )
 9000 FORMAT(/ ' //CMALF //  Negative step',
     $       / ' //CMALF //           ALFA          PALFA'
     $       / ' //CMALF //', 2G15.4 )
 9010 FORMAT(/ ' //CMALF //  Unbounded step.'
     $       / ' //CMALF //  JADD           ALFA'
     $       / ' //CMALF //  ', I4, G15.4 )

*     End of  CMALF .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMCHK ( NERROR, MSGLVL, COLD, USERKX,
     $                   LIWORK, LWORK, LITOTL, LWTOTL,
     $                   N, NCLIN, NCNLN,
     $                   ISTATE, KX, NAMED, NAMES, LENNAM,
     $                   BL, BU, X )

C     IMPLICIT           REAL(A-H,O-Z)
      CHARACTER*8        NAMES(*)
      LOGICAL            COLD, NAMED, USERKX
      INTEGER            ISTATE(N+NCLIN+NCNLN), KX(N)
      REAL               BL(N+NCLIN+NCNLN), BU(N+NCLIN+NCNLN), X(N)

      COMMON    /SOL1CM/ NOUT

************************************************************************
*  CMCHK   checks the data input to various optimizers.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original Fortran 66 version written 10-May-1980.
*  Fortran 77 version written  5-October-1984.
*  This version of CMCHK dated  23-January-1987.
************************************************************************
      LOGICAL            OK
      INTRINSIC          ABS
      PARAMETER        ( ZERO   =  0.0E+0 , ONE    =  1.0E+0 )

      CHARACTER*5        ID(3)
      DATA                ID(1)   ,  ID(2)   ,  ID(3)
     $                 / 'VARBL'  , 'LNCON'  , 'NLCON'   /

      NERROR = 0

*     ------------------------------------------------------------------
*     Check that there is enough workspace to solve the problem.
*     ------------------------------------------------------------------
      OK     = LITOTL .LE. LIWORK  .AND.  LWTOTL .LE. LWORK
      IF (.NOT. OK)  THEN
         WRITE (NOUT, 1100) LIWORK, LWORK, LITOTL, LWTOTL
         NERROR = NERROR + 1
         WRITE (NOUT, 1110)
      ELSE IF (MSGLVL .GT. 0)  THEN
         WRITE (NOUT, 1100) LIWORK, LWORK, LITOTL, LWTOTL
      END IF

      IF (USERKX) THEN
*        ---------------------------------------------------------------
*        Check for a valid KX.
*        ---------------------------------------------------------------
         IFAIL = 1
         CALL CMPERM( KX, 1, N, IFAIL )
         IF (IFAIL .NE. 0) THEN
            WRITE (NOUT, 1300)
            NERROR = NERROR + 1
         END IF
      END IF

*     ------------------------------------------------------------------
*     Check the bounds on all variables and constraints.
*     ------------------------------------------------------------------
      DO 200 J = 1, N+NCLIN+NCNLN
         B1     = BL(J)
         B2     = BU(J)
         OK     = B1 .LE. B2
         IF (.NOT. OK)  THEN
            NERROR = NERROR + 1
            IF (J .GT. N+NCLIN)  THEN
               K  = J - N - NCLIN
               L  = 3
            ELSE IF (J .GT. N)  THEN
               K  = J - N
               L  = 2
            ELSE
               K = J
               L = 1
            END IF
            IF (.NOT. NAMED) WRITE (NOUT, 1200) ID(L), K, B1, B2
            IF (      NAMED) WRITE (NOUT, 1210) NAMES(J), B1, B2
         END IF
  200 CONTINUE

*     ------------------------------------------------------------------
*     If warm start, check  ISTATE.
*     ------------------------------------------------------------------
      IF (.NOT. COLD) THEN
         DO 420 J = 1, N+NCLIN+NCNLN
            IS     = ISTATE(J)
            OK     = IS .GE. (- 2)   .AND.   IS .LE. 4
            IF (.NOT. OK)  THEN
               NERROR = NERROR + 1
               WRITE (NOUT, 1500) J, IS
            END IF
  420    CONTINUE
      END IF

      RETURN

 1100 FORMAT(/ ' Workspace provided is     IW(', I6,
     $         '),  W(', I6, ').' /
     $         ' To solve problem we need  IW(', I6,
     $         '),  W(', I6, ').')
 1110 FORMAT(/ ' XXX  Not enough workspace to solve problem.')
 1200 FORMAT(/ ' XXX  The bounds on  ', A5, I3,
     $         '  are inconsistent.   BL =', G16.7, '   BU =', G16.7)
 1210 FORMAT(/ ' XXX  The bounds on  ', A8,
     $         '  are inconsistent.   BL =', G16.7, '   BU =', G16.7)
 1300 FORMAT(/ ' XXX  KX has not been supplied as a valid',
     $         '  permutation.' )
 1500 FORMAT(/ ' XXX  Component', I5, '  of  ISTATE  is out of',
     $         ' range...', I10)

*     End of  CMCHK .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMPERM( KX, M1, M2, IFAIL )

      INTEGER            IFAIL, M1, M2
      INTEGER            KX(M2)

      COMMON    /SOL1CM/ NOUT

************************************************************************
*     CMPERM checks that elements M1 to M2 of KX contain a valid
*     permutation of the integers M1 to M2. The contents of KX are
*     unchanged on exit.
*
*     SOL version of NAG Library routine M01ZBF.
*     Written by N.N.Maclaren, University of Cambridge.
*     This version of CMPERM dated 18-June-1986.
************************************************************************

      LOGICAL            CMDBG
      INTEGER            LCMDBG
      PARAMETER         (LCMDBG = 5)
      COMMON    /CMDEBG/ ICMDBG(LCMDBG), CMDBG

      INTEGER            I, IERR, J, K
      INTRINSIC          ABS

*     Check the parameters.

      IF (M2 .LT. 1  .OR.  M1 .LT. 1  .OR.  M1 .GT. M2) THEN
         IERR = 1
         IF (CMDBG  .AND.  ICMDBG(3) .GT. 0)
     $      WRITE (NOUT, FMT=1100) M1, M2
      ELSE
         IERR = 0

*        Check that KX is within range.

         DO 20 I = M1, M2
            J = KX(I)
            IF ((J .LT. M1) .OR. (J .GT. M2)) GO TO 100
            IF (I .NE. J) KX(I) = -J
   20    CONTINUE

*        Check that no value is repeated.

         DO 60 I = M1, M2
            K = - KX(I)
            IF (K .GE. 0) THEN
               J     = I
   40          KX(J) = K
               J     = K
               K     = - KX(J)
               IF (K .GT. 0) GO TO 40
               IF (J .NE. I) GO TO 120
            END IF
   60    CONTINUE
      END IF

*     Return

   80 IF (IERR .NE. 0) THEN
         IFAIL = IERR
      ELSE
         IFAIL = 0
      END IF
      RETURN
  100 IERR = 2
      WRITE (NOUT, FMT=1200) I, J
      GO TO 140
  120 IERR = 3
      WRITE (NOUT, FMT=1300) J

*     Restore KX.

  140 DO 160 I = M1, M2
         KX(I) = ABS(KX(I))
  160 CONTINUE
      GO TO 80

 1100 FORMAT(/ ' //CMPERM//  Illegal parameter values,'
     $       / ' //CMPERM//    M1    M1'
     $       / ' //CMPERM//', 2I6 )
 1200 FORMAT(/ ' XXX  KX(',I6,') contains an out-of-range value =', I16)
 1300 FORMAT(/ ' XXX  KX contains a duplicate value =',             I16)

*     End of CMPERM.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMPRT ( MSGLVL, NFREE, NROWA,
     $                   N, NCLIN, NCNLN, NCTOTL, BIGBND,
     $                   NAMED, NAMES, LENNAM,
     $                   NACTIV, ISTATE, KACTIV, KX,
     $                   A, BL, BU, C, CLAMDA, RLAMDA, X )

C     IMPLICIT           REAL(A-H,O-Z)
      CHARACTER*8        NAMES(*)
      LOGICAL            NAMED
      INTEGER            ISTATE(NCTOTL), KACTIV(N), KX(N)
      REAL               A(NROWA,*), BL(NCTOTL), BU(NCTOTL), C(*),
     $                   CLAMDA(NCTOTL), RLAMDA(N), X(N)

      COMMON    /SOL1CM/ NOUT

      LOGICAL            CMDBG
      INTEGER            LCMDBG
      PARAMETER         (LCMDBG = 5)
      COMMON    /CMDEBG/ ICMDBG(LCMDBG), CMDBG

***********************************************************************
*  CMPRT   creates the expanded Lagrange multiplier vector CLAMDA.
*  If MSGLVL .EQ 1 or MSGLVL .GE. 10,  CMPRT prints  x,  A*x,
*  c(x),  their bounds, the multipliers, and the residuals (distance
*  to the nearer bound).
*  CMPRT is called by LSCORE and NPCORE just before exiting.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original Fortran 77 version written  October 1984.
*  This version of  CMPRT  dated  10-June-1986.
***********************************************************************
      CHARACTER*2        LS, LSTATE(7)
      CHARACTER*5        ID(3), ID3
      CHARACTER*8        ID4
      EXTERNAL           SDOT
      INTRINSIC          ABS

      PARAMETER        ( ZERO  = 0.0E+0 )
      DATA               ID(1) / 'VARBL' /
      DATA               ID(2) / 'LNCON' /
      DATA               ID(3) / 'NLCON' /
      DATA               LSTATE(1) / '--' /, LSTATE(2) / '++' /
      DATA               LSTATE(3) / 'FR' /, LSTATE(4) / 'LL' /
      DATA               LSTATE(5) / 'UL' /, LSTATE(6) / 'EQ' /
      DATA               LSTATE(7) / 'TB' /


      NPLIN  = N     + NCLIN
      NZ     = NFREE - NACTIV

*     Expand multipliers for bounds, linear and nonlinear constraints
*     into the  CLAMDA  array.

      CALL SLOAD ( NCTOTL, ZERO, CLAMDA, 1 )
      NFIXED = N - NFREE
      DO 150 K = 1, NACTIV+NFIXED
         IF (K .LE. NACTIV) J = KACTIV(K) + N
         IF (K .GT. NACTIV) J = KX(NZ+K)
         CLAMDA(J) = RLAMDA(K)
  150 CONTINUE

      IF (MSGLVL .LT. 10  .AND.  MSGLVL .NE. 1) RETURN

      WRITE (NOUT, 1100)
      ID3 = ID(1)

      DO 500 J = 1, NCTOTL
         B1     = BL(J)
         B2     = BU(J)
         WLAM   = CLAMDA(J)
         IS     = ISTATE(J)
         LS     = LSTATE(IS + 3)
         IF (J .LE. N) THEN

*           Section 1 -- the variables  x.
*           ------------------------------
            K      = J
            V      = X(J)

         ELSE IF (J .LE. NPLIN) THEN

*           Section 2 -- the linear constraints  A*x.
*           -----------------------------------------
            IF (J .EQ. N + 1) THEN
               WRITE (NOUT, 1200)
               ID3 = ID(2)
            END IF

            K      = J - N
            V      = SDOT  ( N, A(K,1), NROWA, X, 1 )
         ELSE

*           Section 3 -- the nonlinear constraints  c(x).
*           ---------------------------------------------

            IF (J .EQ. NPLIN + 1) THEN
               WRITE (NOUT, 1300)
               ID3 = ID(3)
            END IF

            K      = J - NPLIN
            V      = C(K)
         END IF

*        Print a line for the j-th variable or constraint.
*        -------------------------------------------------
         RES    = V - B1
         RES2   = B2 - V
         IF (ABS(RES) .GT. ABS(RES2)) RES = RES2
         IP     = 1
         IF (B1 .LE. ( - BIGBND )) IP = 2
         IF (B2 .GE.     BIGBND  ) IP = IP + 2
         IF (NAMED) THEN

            ID4 = NAMES(J)
            IF (IP .EQ. 1) THEN
               WRITE (NOUT, 2100) ID4,    LS, V, B1, B2, WLAM, RES
            ELSE IF (IP .EQ. 2) THEN
               WRITE (NOUT, 2200) ID4,    LS, V,     B2, WLAM, RES
            ELSE IF (IP .EQ. 3) THEN
               WRITE (NOUT, 2300) ID4,    LS, V, B1,     WLAM, RES
            ELSE
               WRITE (NOUT, 2400) ID4,    LS, V,         WLAM, RES
           END IF

         ELSE

            IF (IP .EQ. 1) THEN
               WRITE (NOUT, 3100) ID3, K, LS, V, B1, B2, WLAM, RES
            ELSE IF (IP .EQ. 2) THEN
               WRITE (NOUT, 3200) ID3, K, LS, V,     B2, WLAM, RES
            ELSE IF (IP .EQ. 3) THEN
               WRITE (NOUT, 3300) ID3, K, LS, V, B1,     WLAM, RES
            ELSE
               WRITE (NOUT, 3400) ID3, K, LS, V,         WLAM, RES
           END IF
         END IF
  500 CONTINUE
      RETURN

 1100 FORMAT(// ' Variable        State', 5X, ' Value',
     $   6X, ' Lower bound', 4X, ' Upper bound',
     $   '  Lagr multiplier', '     Residual' /)
 1200 FORMAT(// ' Linear constr   State', 5X, ' Value',
     $   6X, ' Lower bound', 4X, ' Upper bound',
     $   '  Lagr multiplier', '     Residual' /)
 1300 FORMAT(// ' Nonlnr constr   State', 5X, ' Value',
     $   6X, ' Lower bound', 4X, ' Upper bound',
     $   '  Lagr multiplier', '     Residual' /)
 2100 FORMAT(1X, A8, 10X, A2, 3G16.7, G16.7, G16.4)
 2200 FORMAT(1X, A8, 10X, A2, G16.7, 5X, ' None', 6X, G16.7,
     $   G16.7, G16.4)
 2300 FORMAT(1X, A8, 10X, A2, 2G16.7, 5X, ' None', 6X, G16.7, G16.4)
 2400 FORMAT(1X, A8, 10X, A2,  G16.7, 5X, ' None', 11X, ' None',
     $   6X, G16.7, G16.4)
 3100 FORMAT(1X, A5, I3, 10X, A2, 3G16.7, G16.7, G16.4)
 3200 FORMAT(1X, A5, I3, 10X, A2,  G16.7,
     $   5X, ' None', 6X, G16.7, G16.7, G16.4)
 3300 FORMAT(1X, A5, I3, 10X, A2, 2G16.7, 5X, ' None', 6X,
     $   G16.7, G16.4)
 3400 FORMAT(1X, A5, I3, 10X, A2,  G16.7,
     $   5X, ' None', 11X, ' None', 6X, G16.7, G16.4)

*     End of  CMPRT

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMR1MD( N, NU, NRANK, NROWR, LENV, LENW,
     $                   R, U, V, W )

C     IMPLICIT           REAL(A-H,O-Z)
      INTEGER            N, NU, NRANK, NROWR, LENV, LENW
      REAL               R(NROWR,*), U(N,*), V(N), W(N)
************************************************************************
*     CMR1MD  modifies the  nrank*n  upper-triangular matrix  R  so that
*     Q*(R + v*w')  is upper triangular,  where  Q  is orthogonal,
*     v  and  w  are vectors, and the modified  R  overwrites the old.
*     Q  is the product of two sweeps of plane rotations (not stored).
*     If required,  the rotations are applied to the NU columns of
*     the matrix  U.
*
*     The matrix V*W' is an (LENV) by (LENW) matrix.
*     The vector V is overwritten.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version   October  1984.
*     This version of  CMR1MD  dated 18-September-1985.
************************************************************************
      INTRINSIC          MIN

      J = MIN( LENV, NRANK )
      IF (NRANK .GT. 0) THEN

*        ===============================================================
*        Reduce components  1  thru  (J-1)  of  V  to zero,  using a
*        backward sweep of rotations.  The rotations create a horizontal
*        spike in the  j-th  row of  R.  This row is stored in  V.
*        (Note that  SROT3G  sets  V(K) = 0  below as required.)
*        ===============================================================
         LROWJ  = N - J + 1
         VJ     = V(J)
         CALL SCOPY ( LROWJ, R(J,J), NROWR, V(J), 1 )
         LROWK  = LROWJ
         DO 400 K = J-1, 1, -1
            LROWK  = LROWK + 1
            CALL SROT3G( VJ, V(K), CS, SN )
            CALL SROT3 ( LROWK, V(K)  , 1, R(K,K), NROWR, CS, SN )

            IF (NU .GT. 0)
     $      CALL SROT3 ( NU   , U(J,1), N, U(K,1), N    , CS, SN )
  400    CONTINUE

*        ===============================================================
*        Add a multiple of elements  1  thru  LENW  of  W  to the row
*        spike of  R  (stored in elements  1  thru  N  of  V).
*        ===============================================================
         CALL SAXPY ( LENW, VJ, W, 1, V, 1 )

*        ===============================================================
*        Eliminate the row spike  (held in  V)  using a forward sweep
*        of rotations.
*        ===============================================================
         DO 600 K = 1, J-1
            LROWK  = LROWK - 1
            L      = K     + 1
            CALL SROT3G( R(K,K), V(K), CS, SN )
            CALL SROT3 ( LROWK, R(K,L), NROWR, V(L)  , 1, CS, SN )

            IF (NU .GT. 0)
     $      CALL SROT3 ( NU   , U(K,1), N    , U(J,1), N, CS, SN )
  600    CONTINUE
         CALL SCOPY ( LROWJ, V(J), 1, R(J,J), NROWR )
      END IF

      RETURN

*     End of  CMR1MD

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMRSWP( N, NU, NRANK, NROWR, I, J, R, U, V )

C     IMPLICIT           REAL(A-H,O-Z)
      INTEGER            N, NU, NRANK, NROWR, I, J
      REAL               R(NROWR,*), U(N,*), V(N)

************************************************************************
*     CMRSWP  interchanges the  I-th  and  J-th  (I .LT. J)  columns of
*     an  NRANK*N  upper-triangular matrix  R   and restores the
*     resulting matrix to upper-triangular form.  The final matrix  R
*     is equal to Q(R + VW')  where  V  and  W  are defined as
*         V   =  Rj  -  Ri      and    W  =  Ei  -  Ej
*     with  Ri  and  Rj  the Ith and Jth columns of  R,  Ei  and  Ej
*     unit vectors.
*
*     The vector V is used as workspace.  R is overwritten.  Q is the
*     product of two sweeps of plane rotations (not stored).
*     If required,  the rotations are applied to the  nu  columns of
*     the matrix  U.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written 31-October-1984.
*     This version of  CMRSWP  dated  18-September-1985.
************************************************************************
      INTRINSIC          MIN
      INTEGER            K, L, LENI1, LENJ, LROWJ, LROWK
      REAL               CS, SN, VJ

      LENJ   = MIN( J, NRANK )
      IF (LENJ .GT. 0) THEN
         CALL SCOPY ( LENJ, R(1,J), 1, V, 1 )
         IF (I .LE. NRANK) V(I)  = V(I) - R(I,I)
         LENI1 = MIN( I-1, NRANK )
         IF (LENI1 .GT. 0) THEN
            CALL SCOPY ( LENI1, R(1,I), 1, R(1,J), 1 )
            CALL SCOPY ( LENI1, V     , 1, R(1,I), 1 )
         END IF
      END IF
      IF (I .LE. NRANK) THEN

*        ===============================================================
*        Reduce components I thru  (LENJ-1) of V to zero,  using a
*        backward sweep of rotations.  The rotations create a horizontal
*        spike in the LENJ-th row of  R.  This row is stored in V.
*        (Note that  SROT3G  sets  V(K) = 0  below as required.)
*        ===============================================================
         LROWJ  = N - LENJ + 1
         VJ     = V(LENJ)
         CALL SCOPY ( LROWJ, R(LENJ,LENJ), NROWR, V(LENJ), 1 )
         LROWK  = LROWJ
         DO 400 K = LENJ-1, I, -1
            LROWK  = LROWK + 1
            CALL SROT3G( VJ, V(K), CS, SN )
            CALL SROT3 ( LROWK, V(K)     , 1, R(K,K), NROWR, CS, SN )

            IF (NU .GT. 0)
     $      CALL SROT3 ( NU   , U(LENJ,1), N, U(K,1), N    , CS, SN )
  400    CONTINUE

*        ===============================================================
*        Add a multiple of elements I thru J of W to the
*        horizontal spike of  R  (held in elements I thru J of V).
*        ===============================================================
         V(I) = V(I) + VJ
         V(J) = V(J) - VJ

*        ===============================================================
*        Eliminate the row spike  (held in V)  using a forward sweep
*        of rotations.
*        ===============================================================
         DO 600 K = I, LENJ-1
            LROWK  = LROWK - 1
            L      = K     + 1
            CALL SROT3G( R(K,K), V(K), CS, SN )
            CALL SROT3 ( LROWK, R(K,L), NROWR, V(L)     , 1, CS, SN )

            IF (NU .GT. 0)
     $      CALL SROT3 ( NU   , U(K,1), N    , U(LENJ,1), N, CS, SN )
  600    CONTINUE
         CALL SCOPY ( LROWJ, V(LENJ), 1, R(LENJ,LENJ), NROWR )
      END IF

      RETURN

*     End of  CMRSWP

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMTSOL( MODE, NROWT, N, T, Y )

C     IMPLICIT           REAL(A-H,O-Z)
      INTEGER            MODE, NROWT, N
      REAL               T(NROWT,*), Y(N)

************************************************************************
*     CMTSOL  solves equations involving a reverse-triangular matrix  T
*     and a right-hand-side vector  y,  returning the solution in  y.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original Fortran 77 version written February-1985.
************************************************************************
      PARAMETER        ( ZERO = 0.0E+0 )

      N1 = N + 1
      IF (MODE .EQ. 1) THEN

*        Mode = 1  ---  Solve  T * y(new) = y(old).

         DO 100 J = 1, N
            JJ = N1 - J
            YJ = Y(J)/T(J,JJ)
            Y(J) = YJ
            L  = JJ - 1
            IF (L .GT. 0  .AND.  YJ .NE. ZERO)
     $      CALL SAXPY( L, (-YJ), T(J+1,JJ), 1, Y(J+1), 1 )
  100    CONTINUE
      ELSE

*        Mode = 2  ---  Solve  T' y(new) = y(old).

         DO 500 J = 1, N
            JJ = N1 - J
            YJ = Y(J)/T(JJ,J)
            Y(J) = YJ
            L  = JJ - 1
            IF (L .GT. 0  .AND.  YJ .NE. ZERO)
     $      CALL SAXPY( L, (-YJ), T(JJ,J+1), NROWT, Y(J+1), 1 )
  500    CONTINUE
      END IF

*     Reverse the solution vector.

      IF (N .GT. 1) THEN
         L = N/2
         DO 800 J = 1, L
            JJ    = N1 - J
            YJ    = Y(J)
            Y(J)  = Y(JJ)
            Y(JJ) = YJ
  800    CONTINUE
      END IF

      RETURN

*     End of  CMTSOL.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE CMQMUL( MODE, N, NZ, NFREE, NQ, UNITQ,
     $                   KX, V, ZY, WRK )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            UNITQ
      INTEGER            KX(N)
      REAL               V(N), ZY(NQ,*), WRK(N)

************************************************************************
*     CMQMUL  transforms the vector  v  in various ways using the
*     matrix  Q = ( Z  Y )  defined by the input parameters.
*
*        MODE               result
*        ----               ------
*
*          1                v = Z v
*          2                v = Y v
*          3                v = Q v
*
*     On input,  v  is assumed to be ordered as  ( v(free)  v(fixed) ).
*     on output, v  is a full n-vector.
*
*
*          4                v = Z'v
*          5                v = Y'v
*          6                v = Q'v
*
*     On input,  v  is a full n-vector.
*     On output, v  is ordered as  ( v(free)  v(fixed) ).
*
*          7                v = Y'v
*          8                v = Q'v
*
*     On input,  v  is a full n-vector.
*     On output, v  is as in modes 5 and 6 except that v(fixed) is not
*     set.
*
*     Modes  1, 4, 7 and 8  do not involve  v(fixed).
*     Original F66 version  April 1983.
*     Fortran 77 version written  9-February-1985.
*     Level 2 BLAS added 10-June-1986.
*     This version of CMQMUL dated 10-June-1986.
************************************************************************
      EXTERNAL           SDOT
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      NFIXED = N - NFREE
      J1     = 1
      J2     = NFREE
      IF (MODE .EQ. 1  .OR.  MODE .EQ. 4) J2 = NZ
      IF (MODE .EQ. 2  .OR.  MODE .EQ. 5  .OR.  MODE .EQ. 7) J1 = NZ + 1
      LENV   = J2 - J1 + 1
      IF (MODE .LE. 3) THEN
*        ===============================================================
*        Mode = 1, 2  or  3.
*        ===============================================================

         IF (NFREE .GT. 0) CALL SLOAD ( NFREE, ZERO, WRK, 1 )

*        Copy  v(fixed)  into the end of  wrk.

         IF (MODE .GE. 2  .AND.  NFIXED .GT. 0)
     $      CALL SCOPY ( NFIXED, V(NFREE+1), 1, WRK(NFREE+1), 1 )

*        Set  WRK  =  relevant part of  ZY * V.

         IF (LENV .GT. 0)  THEN
            IF (UNITQ) THEN
               CALL SCOPY ( LENV, V(J1), 1, WRK(J1), 1 )
            ELSE
               CALL SGEMV ( 'N', NFREE, J2-J1+1, ONE, ZY(1,J1), NQ,
     $                      V(J1), 1, ONE, WRK, 1 )
            END IF
         END IF

*        Expand  WRK  into  V  as a full n-vector.

         CALL SLOAD ( N, ZERO, V, 1 )
         DO 220 K = 1, NFREE
            J    = KX(K)
            V(J) = WRK(K)
  220    CONTINUE

*        Copy  WRK(fixed)  into the appropriate parts of  V.

         IF (MODE .GT. 1)  THEN
            DO 320 L = 1, NFIXED
               J       = KX(NFREE+L)
               V(J)    = WRK(NFREE+L)
  320       CONTINUE
         END IF

      ELSE
*        ===============================================================
*        Mode = 4, 5, 6, 7  or  8.
*        ===============================================================
*        Put the fixed components of  V  into the end of  WRK.

         IF (MODE .EQ. 5  .OR.  MODE .EQ. 6)  THEN
            DO 420 L = 1, NFIXED
               J            = KX(NFREE+L)
               WRK(NFREE+L) = V(J)
  420       CONTINUE
         END IF

*        Put the free  components of  V  into the beginning of  WRK.

         IF (NFREE .GT. 0)  THEN
            DO 520 K = 1, NFREE
               J      = KX(K)
               WRK(K) = V(J)
  520       CONTINUE

*           Set  V  =  relevant part of  ZY' * WRK.

            IF (LENV .GT. 0)  THEN
               IF (UNITQ) THEN
                  CALL SCOPY ( LENV, WRK(J1), 1, V(J1), 1 )
               ELSE
                  CALL SGEMV ( 'T', NFREE, J2-J1+1, ONE, ZY(1,J1), NQ,
     $                         WRK, 1, ZERO, V(J1), 1 )
               END IF
            END IF
         END IF

*        Copy the fixed components of  WRK  into the end of  V.

         IF (NFIXED .GT. 0  .AND.  (MODE .EQ. 5  .OR.  MODE .EQ. 6))
     $      CALL SCOPY ( NFIXED, WRK(NFREE+1), 1, V(NFREE+1), 1 )
      END IF

      RETURN

*     End of  CMQMUL.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*     File  LSSUBS FORTRAN
*
*     LSADD    LSADDS   LSBNDS   LSCHOL   LSCORE   LSCRSH   LSDEL
*     LSDFLT   LSFEAS   LSFILE   LSGETP   LSGSET   LSKEY    LSLOC
*     LSMOVE   LSMULS   LSOPTN   LSPRT    LSSETX   LSSOL
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSADD ( UNITQ,
     $                   INFORM, IFIX, IADD, JADD,
     $                   NACTIV, NZ, NFREE, NRANK, NRES, NGQ,
     $                   N, NROWA, NQ, NROWR, NROWT,
     $                   KX, CONDMX,
     $                   A, R, T, RES, GQ, ZY, WRK1, WRK2 )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            UNITQ
      INTEGER            KX(N)
      REAL               A(NROWA,*), R(NROWR,*), T(NROWT,*),
     $                   RES(N,*), GQ(N,*), ZY(NQ,*)
      REAL               WRK1(N), WRK2(N)
************************************************************************
*  LSADD   updates the factorization,  A(free) * (Z Y) = (0 T),  when a
*  constraint is added to the working set.  If  NRANK .gt. 0, the
*  factorization  ( R ) = PWQ  is also updated,  where  W  is the
*                 ( 0 )
*  least squares matrix,  R  is upper-triangular,  and  P  is an
*  orthogonal matrix.  The matrices  W  and  P  are not stored.
*
*  There are three separate cases to consider (although each case
*  shares code with another)...
*
*  (1) A free variable becomes fixed on one of its bounds when there
*      are already some general constraints in the working set.
*
*  (2) A free variable becomes fixed on one of its bounds when there
*      are only bound constraints in the working set.
*
*  (3) A general constraint (corresponding to row  IADD  of  A) is
*      added to the working set.
*
*  In cases (1) and (2), we assume that  KX(IFIX) = JADD.
*  In all cases,  JADD  is the index of the constraint being added.
*
*  If there are no general constraints in the working set,  the
*  matrix  Q = (Z Y)  is the identity and will not be touched.
*
*  If  NRES .GT. 0,  the row transformations are applied to the rows of
*  the  (N by NRES)  matrix  RES.
*  If  NGQ .GT. 0,  the column transformations are applied to the
*  columns of the  (NGQ by N)  matrix  GQ'.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original version written 31-October--1984.
*  This version of LSADD dated 29-December-1985.
************************************************************************
      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL4CM/ EPSPT3, EPSPT5, EPSPT8, EPSPT9
      COMMON    /SOL5CM/ ASIZE, DTMAX, DTMIN

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      LOGICAL            BOUND , OVERFL
      EXTERNAL           SDOT  , SDIV  , SNRM2
      INTRINSIC          MAX   , MIN
      PARAMETER         (ZERO = 0.0E+0, ONE = 1.0E+0)

*     If the condition estimator of the updated factors is greater than
*     CONDBD,  a warning message is printed.

      CONDBD = ONE / EPSPT9

      OVERFL = .FALSE.
      BOUND  = JADD .LE. N
      IF (BOUND) THEN
*        ===============================================================
*        A simple bound has entered the working set.  IADD  is not used.
*        ===============================================================
         IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $      WRITE (NOUT, 1010) NACTIV, NZ, NFREE, IFIX, JADD, UNITQ
         NANEW = NACTIV

         IF (UNITQ) THEN

*           Q  is not stored, but KX defines an ordering of the columns
*           of the identity matrix that implicitly define  Q.
*           Reorder KX so that variable IFIX is moved to position
*           NFREE+1 and variables IFIX+1,...,NFREE+1 are moved one
*           position to the left.

            CALL SLOAD ( NFREE, (ZERO), WRK1, 1 )
            WRK1(IFIX) = ONE

            DO 100 I = IFIX, NFREE-1
               KX(I) = KX(I+1)
  100       CONTINUE
         ELSE
*           ------------------------------------------------------------
*           Q  is stored explicitly.
*           ------------------------------------------------------------
*           Set  WRK1 = the  (IFIX)-th  row of  Q.
*           Move the  (NFREE)-th  row of  Q  to position  IFIX.

            CALL SCOPY ( NFREE, ZY(IFIX,1), NQ, WRK1, 1 )
            IF (IFIX .LT. NFREE) THEN
               CALL SCOPY ( NFREE, ZY(NFREE,1), NQ, ZY(IFIX,1), NQ )
               KX(IFIX) = KX(NFREE)
            END IF
         END IF
         KX(NFREE) = JADD
      ELSE
*        ===============================================================
*        A general constraint has entered the working set.
*        IFIX  is not used.
*        ===============================================================
         IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $      WRITE (NOUT, 1020) NACTIV, NZ, NFREE, IADD, JADD, UNITQ

         NANEW  = NACTIV + 1

*        Transform the incoming row of  A  by  Q'.

         CALL SCOPY ( N, A(IADD,1), NROWA, WRK1, 1 )
         CALL CMQMUL( 8, N, NZ, NFREE, NQ, UNITQ, KX, WRK1, ZY, WRK2)

*        Check that the incoming row is not dependent upon those
*        already in the working set.

         DTNEW  = SNRM2 ( NZ, WRK1, 1 )
         IF (NACTIV .EQ. 0) THEN

*           This is the only general constraint in the working set.

            COND   = SDIV  ( ASIZE, DTNEW, OVERFL )
            TDTMAX = DTNEW
            TDTMIN = DTNEW
         ELSE

*           There are already some general constraints in the working
*           set. Update the estimate of the condition number.

            TDTMAX = MAX( DTNEW, DTMAX )
            TDTMIN = MIN( DTNEW, DTMIN )
            COND   = SDIV  ( TDTMAX, TDTMIN, OVERFL )
         END IF

         IF (COND .GT. CONDMX  .OR.  OVERFL) GO TO 900

         IF (UNITQ) THEN

*           This is the first general constraint to be added.
*           Set  Q = I.

            DO 200 J = 1, NFREE
               CALL SLOAD ( NFREE, (ZERO), ZY(1,J), 1 )
               ZY(J,J) = ONE
  200       CONTINUE
            UNITQ  = .FALSE.
         END IF
      END IF

      NZERO  = NZ - 1
      IF (BOUND) NZERO = NFREE - 1

*     ------------------------------------------------------------------
*     Use a sequence of 2*2 column transformations to reduce the
*     first NZERO elements of WRK1 to zero.  This affects ZY, except
*     when UNITQ is true.  The transformations may also be applied
*     to R, T and GQ'.
*     ------------------------------------------------------------------
      LROWR  = N
      NELM   = 1
      IROWT  = NACTIV

      DO 300 K = 1, NZERO

*        Compute the transformation that reduces WRK1(K) to zero,
*        then apply it to the relevant columns of  Z  and  GQ'.

         CALL SROT3G( WRK1(K+1), WRK1(K), CS, SN )
         IF (.NOT. UNITQ)
     $      CALL SROT3 ( NFREE, ZY(1,K+1), 1, ZY(1,K), 1, CS, SN )
         IF (NGQ .GT. 0)
     $      CALL SROT3 ( NGQ  , GQ(K+1,1), N, GQ(K,1), N, CS, SN )

         IF (K .GE. NZ  .AND.  NACTIV .GT. 0) THEN

*           Apply the rotation to  T.

            T(IROWT,K) = ZERO
            CALL SROT3 ( NELM, T(IROWT,K+1), 1, T(IROWT,K), 1, CS, SN )
            NELM  = NELM  + 1
            IROWT = IROWT - 1
         END IF

         IF (NRANK .GT. 0) THEN

*           Apply the same transformation to the columns of R.
*           This generates a subdiagonal element in R that must be
*           eliminated by a row rotation.

            IF (K .LT. NRANK) R(K+1,K) = ZERO
            LCOL   = MIN( K+1, NRANK )

            CALL SROT3 ( LCOL, R(1,K+1), 1, R(1,K), 1, CS, SN )
            IF (K .LT. NRANK) THEN
               CALL SROT3G( R(K,K), R(K+1,K), CS, SN )
               LROWR  = LROWR - 1
               CALL SROT3 ( LROWR,   R(K,K+1)  , NROWR,
     $                               R(K+1,K+1), NROWR, CS, SN )

               IF (NRES .GT. 0)
     $            CALL SROT3 ( NRES, RES(K,1)  , N    ,
     $                               RES(K+1,1), N    , CS, SN )
            END IF
         END IF
  300 CONTINUE

      IF (BOUND) THEN

*        The last row and column of ZY has been transformed to plus
*        or minus the unit vector E(NFREE).  We can reconstitute the
*        columns of GQ and R corresponding to the new fixed variable.

         IF (WRK1(NFREE) .LT. ZERO) THEN
            NFMIN = MIN( NRANK, NFREE )
            IF (NFMIN .GT. 0) CALL SSCAL ( NFMIN, -ONE, R(1,NFREE) , 1 )
            IF (NGQ   .GT. 0) CALL SSCAL ( NGQ  , -ONE, GQ(NFREE,1), N )
         END IF

*        ---------------------------------------------------------------
*        The diagonals of T have been altered.  Recompute the
*        largest and smallest values.
*        ---------------------------------------------------------------
         IF (NACTIV .GT. 0) THEN
            CALL SCOND( NACTIV, T(NACTIV,NZ), NROWT-1, TDTMAX, TDTMIN )
            COND   = SDIV  ( TDTMAX, TDTMIN, OVERFL )
         END IF
      ELSE
*        ---------------------------------------------------------------
*        General constraint.  Install the new row of T.
*        ---------------------------------------------------------------
         CALL SCOPY ( NANEW, WRK1(NZ), 1, T(NANEW,NZ), NROWT )
      END IF

*     ==================================================================
*     Prepare to exit.  Check the magnitude of the condition estimator.
*     ==================================================================
  900 IF (NANEW .GT. 0) THEN
         IF (COND .LT. CONDMX  .AND.  .NOT. OVERFL) THEN

*           The factorization has been successfully updated.

            INFORM = 0
            DTMAX  = TDTMAX
            DTMIN  = TDTMIN
            IF (COND .GE. CONDBD) WRITE (NOUT, 2000) JADD
         ELSE

*           The proposed working set appears to be linearly dependent.

            INFORM = 1
            IF (LSDBG  .AND.  ILSDBG(1) .GT. 0) THEN
               WRITE( NOUT, 3000 )
               IF (BOUND) THEN
                  WRITE (NOUT, 3010) ASIZE, DTMAX, DTMIN
               ELSE
                  IF (NACTIV .GT. 0) THEN
                     WRITE (NOUT, 3020) ASIZE, DTMAX, DTMIN, DTNEW
                  ELSE
                     WRITE (NOUT, 3030) ASIZE, DTNEW
                  END IF
               END IF
            END IF
         END IF
      END IF

      RETURN

 1010 FORMAT(/ ' //LSADD //  Simple bound added.'
     $       / ' //LSADD //  NACTIV    NZ NFREE  IFIX  JADD UNITQ'
     $       / ' //LSADD //  ', 5I6, L6 )
 1020 FORMAT(/ ' //LSADD //  General constraint added.           '
     $       / ' //LSADD //  NACTIV    NZ NFREE  IADD  JADD UNITQ'
     $       / ' //LSADD //  ', 5I6, L6 )
 2000 FORMAT(/ ' XXX  Serious ill-conditioning in the working set',
     $         ' after adding constraint ',  I5
     $       / ' XXX  Overflow may occur in subsequent iterations.'//)
 3000 FORMAT(/ ' //LSADD //  Dependent constraint rejected.' )
 3010 FORMAT(/ ' //LSADD //     ASIZE     DTMAX     DTMIN        '
     $       / ' //LSADD //', 1P3E10.2 )
 3020 FORMAT(/ ' //LSADD //     ASIZE     DTMAX     DTMIN     DTNEW'
     $       / ' //LSADD //', 1P4E10.2 )
 3030 FORMAT(/ ' //LSADD //     ASIZE     DTNEW'
     $       / ' //LSADD //', 1P2E10.2 )

*     End of  LSADD .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSADDS( UNITQ, VERTEX,
     $                   INFORM, K1, K2, NACTIV, NARTIF, NZ, NFREE,
     $                   NRANK, NREJTD, NRES, NGQ,
     $                   N, NQ, NROWA, NROWR, NROWT,
     $                   ISTATE, KACTIV, KX,
     $                   CONDMX,
     $                   A, R, T, RES, GQ,
     $                   ZY, WRK1, WRK2 )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            UNITQ, VERTEX
      INTEGER            ISTATE(*), KACTIV(N), KX(N)
      REAL               CONDMX
      REAL               A(NROWA,*), R(NROWR,*),
     $                   T(NROWT,*), RES(N,*), GQ(N,*), ZY(NQ,*)
      REAL               WRK1(N), WRK2(N)

************************************************************************
*     LSADDS  includes general constraints K1 thru K2 as new rows of
*     the TQ factorization stored in T, ZY.  If NRANK is nonzero, the
*     changes in Q are reflected in NRANK by N triangular factor R such
*     that
*                         W  =  P ( R ) Q,
*                                 ( 0 )
*     where  P  is orthogonal.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written  October-31-1984.
*     This version of LSADDS dated 30-December-1985.
************************************************************************
      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/
      COMMON    /SOL5CM/ ASIZE, DTMAX, DTMIN

      EXTERNAL           SNRM2
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      RTMAX  = WMACH(8)

*     Estimate the condition number of the constraints that are not
*     to be refactorized.

      IF (NACTIV .EQ. 0) THEN
         DTMAX = ZERO
         DTMIN = ONE
      ELSE
         CALL SCOND ( NACTIV, T(NACTIV,NZ+1), NROWT-1, DTMAX, DTMIN )
      END IF

      DO 200 K = K1, K2
         IADD = KACTIV(K)
         JADD = N + IADD
         IF (NACTIV .LT. NFREE) THEN

            CALL LSADD ( UNITQ,
     $                   INFORM, IFIX, IADD, JADD,
     $                   NACTIV, NZ, NFREE, NRANK, NRES, NGQ,
     $                   N, NROWA, NQ, NROWR, NROWT,
     $                   KX, CONDMX,
     $                   A, R, T, RES, GQ, ZY,
     $                   WRK1, WRK2 )

            IF (INFORM .EQ. 0) THEN
               NACTIV = NACTIV + 1
               NZ     = NZ     - 1
            ELSE
               ISTATE(JADD) =   0
               KACTIV(K)    = - KACTIV(K)
            END IF
         END IF
  200 CONTINUE

      IF (NACTIV .LT. K2) THEN

*        Some of the constraints were classed as dependent and not
*        included in the factorization.  Re-order the part of  KACTIV
*        that holds the indices of the general constraints in the
*        working set.  Move accepted indices to the front and shift
*        rejected indices (with negative values) to the end.

         L      = K1 - 1
         DO 300 K = K1, K2
            I         = KACTIV(K)
            IF (I .GE. 0) THEN
               L      = L + 1
               IF (L .NE. K) THEN
                  ISWAP     = KACTIV(L)
                  KACTIV(L) = I
                  KACTIV(K) = ISWAP
               END IF
            END IF
  300    CONTINUE

*        If a vertex is required, add some temporary bounds.
*        We must accept the resulting condition number of the working
*        set.

         IF (VERTEX) THEN
            CNDMAX = RTMAX
            NZADD  = NZ
            DO 320 IARTIF = 1, NZADD
               ROWMAX = ZERO
               DO 310 I = 1, NFREE
                  RNORM = SNRM2 ( NZ, ZY(I,1), NQ )
                  IF (ROWMAX .LT. RNORM) THEN
                     ROWMAX = RNORM
                     IFIX   = I
                  END IF
  310          CONTINUE
               JADD = KX(IFIX)

               CALL LSADD ( UNITQ,
     $                      INFORM, IFIX, IADD, JADD,
     $                      NACTIV, NZ, NFREE, NRANK, NRES, NGQ,
     $                      N, NROWA, NQ, NROWR, NROWT,
     $                      KX, CNDMAX,
     $                      A, R, T, RES, GQ, ZY,
     $                      WRK1, WRK2 )

               NFREE  = NFREE  - 1
               NZ     = NZ     - 1
               NARTIF = NARTIF + 1
               ISTATE(JADD) = 4
  320       CONTINUE
         END IF
      END IF

      NREJTD = K2 - NACTIV

      RETURN

*     End of  LSADDS.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSBNDS( UNITQ,
     $                   INFORM, NZ, NFREE, NRANK, NRES, NGQ,
     $                   N, NQ, NROWA, NROWR, NROWT,
     $                   ISTATE, KX,
     $                   CONDMX,
     $                   A, R, T, RES, GQ,
     $                   ZY, WRK1, WRK2 )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            UNITQ
      INTEGER            ISTATE(*), KX(N)
      REAL               CONDMX
      REAL               A(NROWA,*), R(NROWR,*),
     $                   T(NROWT,*), RES(N,*), GQ(N,*), ZY(NQ,*)
      REAL               WRK1(N), WRK2(N)

************************************************************************
*     LSBNDS updates the factor R as KX is reordered to reflect the
*     status of the bound constraints given by ISTATE.  KX is reordered
*     so that the fixed variables come last.  One of two alternative
*     are used to reorder KX. One method needs fewer accesses to KX, the
*     other gives a matrix Rz with more rows and columns.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written  30-December-1985.
*     This version dated 30-December-1985.
************************************************************************

      NFIXED = N - NFREE

      IF (NRANK .LT. N  .AND.  NRANK .GT. 0) THEN
*        ---------------------------------------------------------------
*        R is specified but singular.  Try and keep the dimension of Rz
*        as large as possible.
*        ---------------------------------------------------------------
         NACTV = 0
         NFREE = N
         NZ    = N

         J     = N
*+       WHILE (J .GT. 0  .AND.  N-NFREE .LT. NFIXED) DO
  100    IF    (J .GT. 0  .AND.  N-NFREE .LT. NFIXED) THEN
            IF (ISTATE(J) .GT. 0) THEN
               JADD = J
               DO 110 IFIX = NFREE, 1, -1
                  IF (KX(IFIX) .EQ. JADD) GO TO 120
  110          CONTINUE

*              Add bound JADD.

  120          CALL LSADD ( UNITQ,
     $                      INFORM, IFIX, IADD, JADD,
     $                      NACTV, NZ, NFREE, NRANK, NRES, NGQ,
     $                      N, NROWA, NQ, NROWR, NROWT,
     $                      KX, CONDMX,
     $                      A, R, T, RES, GQ, ZY,
     $                      WRK1, WRK2 )

               NFREE = NFREE - 1
               NZ    = NZ    - 1
            END IF
            J = J - 1
            GO TO 100
*+       END WHILE
         END IF
      ELSE
*        ---------------------------------------------------------------
*        R is of full rank,  or is not specified.
*        ---------------------------------------------------------------
         IF (NFIXED .GT. 0) THEN

*           Order KX so that the free variables come first.

            LSTART = NFREE + 1
            DO 250 K = 1, NFREE
               J = KX(K)
               IF (ISTATE(J) .GT. 0) THEN
                  DO 220 L = LSTART, N
                     J2 = KX(L)
                     IF (ISTATE(J2) .EQ. 0) GO TO 230
  220             CONTINUE

  230             KX(K)  = J2
                  KX(L)  = J
                  LSTART = L + 1

                  IF (NRANK .GT. 0)
     $               CALL CMRSWP( N, NRES, NRANK, NROWR, K, L,
     $                            R, RES, WRK1 )
               END IF
  250       CONTINUE

         END IF
         NZ = NFREE
      END IF

      RETURN

*     End of  LSBNDS.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSCHOL( NROWH, N, NRANK, TOLRNK, KX, H, INFORM )

C     IMPLICIT           REAL (A-H,O-Z)
      INTEGER            KX(*)
      REAL               H(NROWH,*)

************************************************************************
*     LSCHOL  forms the Cholesky factorization of the positive
*     semi-definite matrix H such that
*                   PHP'  =  R'R
*     where  P  is a permutation matrix and  R  is upper triangular.
*     The permutation P is chosen to maximize the diagonal of R at each
*     stage.  Only the diagonal and super-diagonal elements of H are
*     used.
*
*     Output:
*
*         INFORM = 0   the factorization was computed successfully,
*                      with the Cholesky factor written in the upper
*                      triangular part of H and P stored in KX.
*                  1   the matrix H was indefinite.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version of LSCHOL dated  2-February-1981.
*     Level 2 Blas added 29-June-1986.
*     This version of LSCHOL dated  30-June-1986.
************************************************************************

      COMMON    /SOL1CM/ NOUT
      INTRINSIC          ABS   , MAX   , SQRT
      EXTERNAL           ISAMAX
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      INFORM = 0
      NRANK  = 0

*     Main loop for computing rows of  R.

      DO 200 J = 1, N

*        Find maximum available diagonal.

         KMAX = J - 1 + ISAMAX( N-J+1, H(J,J), NROWH+1 )
         DMAX = H(KMAX,KMAX)

         IF (DMAX .LE. TOLRNK*ABS(H(1,1))) GO TO 300

*        Perform a symmetric interchange if necessary.

         IF (KMAX .NE. J) THEN
            K        = KX(KMAX)
            KX(KMAX) = KX(J)
            KX(J)    = K

            CALL SSWAP ( J       , H(1,J)   , 1, H(1,KMAX), 1     )
            CALL SSWAP ( KMAX-J+1, H(J,KMAX), 1, H(J,J)   , NROWH )
            CALL SSWAP ( N-KMAX+1, H(KMAX,KMAX), NROWH,
     $                             H(J,KMAX)   , NROWH )

            H(KMAX,KMAX) = H(J,J)
         END IF

*        Set the diagonal of  R.

         D      = SQRT( DMAX )
         H(J,J) = D
         NRANK  = NRANK + 1

         IF (J .LT. N) THEN

*           Set the super-diagonal elements of this row of R and update
*           the elements of the block that is yet to be factorized.

            CALL SSCAL ( N-J,   (ONE/D), H(J  ,J+1), NROWH )
            CALL SSYR  ( 'U', N-J, -ONE, H(J  ,J+1), NROWH,
     $                                   H(J+1,J+1), NROWH )
         END IF

  200 CONTINUE
*     ------------------------------------------------------------------
*     Check for the semi-definite case.
*     ------------------------------------------------------------------
  300 IF (NRANK .LT. N) THEN

*        Find the largest element in the unfactorized block.

         SUPMAX = ZERO
         DO 310 I = J, N-1
            K      = I + ISAMAX( N-I, H(I,I+1), NROWH )
            SUPMAX = MAX( SUPMAX, ABS(H(I,K)) )
  310    CONTINUE

         IF (SUPMAX .GT. TOLRNK*ABS(H(1,1))) THEN
            WRITE (NOUT, 1000) DMAX, SUPMAX
            INFORM = 1
         END IF
      END IF

      RETURN

 1000 FORMAT(' XXX  Hessian appears to be indefinite.'
     $      /' XXX  Maximum diagonal and off-diagonal ignored',
     $             ' in the Cholesky factorization:', 1P2E22.14 )

*     End of LSCHOL.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSCORE( PRBTYP, NAMED, NAMES, LINOBJ, UNITQ,
     $                   INFORM, ITER, JINF, NCLIN, NCTOTL,
     $                   NACTIV, NFREE, NRANK, NZ, NZ1,
     $                   N, NROWA, NROWR,
     $                   ISTATE, KACTIV, KX,
     $                   CTX, SSQ, SSQ1, SUMINF, NUMINF, XNORM,
     $                   BL, BU, A, CLAMDA, AX,
     $                   FEATOL, R, X, IW, W )

C     IMPLICIT           REAL(A-H,O-Z)
      CHARACTER*2        PRBTYP
      CHARACTER*8        NAMES(*)
      INTEGER            ISTATE(NCTOTL), KACTIV(N), KX(N)
      INTEGER            IW(*)
      REAL               BL(NCTOTL), BU(NCTOTL), A(NROWA,*),
     $                   CLAMDA(NCTOTL), AX(*),
     $                   FEATOL(NCTOTL), R(NROWR,*), X(N)
      REAL               W(*)
      LOGICAL            NAMED, LINOBJ, UNITQ

************************************************************************
*     LSCORE  is a subroutine for linearly constrained linear-least
*     squares.  On entry, it is assumed that an initial working set of
*     linear constraints and bounds is available.
*     The arrays ISTATE, KACTIV and KX will have been set accordingly
*     and the arrays T and ZY will contain the TQ factorization of
*     the matrix whose rows are the gradients of the active linear
*     constraints with the columns corresponding to the active bounds
*     removed.  the TQ factorization of the resulting (NACTIV by NFREE)
*     matrix is  A(free)*Q = (0 T),  where Q is (NFREE by NFREE) and T
*     is reverse-triangular.
*
*     Values of ISTATE(J) for the linear constraints.......
*
*     ISTATE(J)
*     ---------
*          0    constraint J is not in the working set.
*          1    constraint J is in the working set at its lower bound.
*          2    constraint J is in the working set at its upper bound.
*          3    constraint J is in the working set as an equality.
*
*     Constraint J may be violated by as much as FEATOL(J).
*
*     Systems Optimization Laboratory, Stanford University.
*     This version of  LSCORE  dated  1-August-1986.
*
*     Copyright  1984  Stanford University.
*
*  This material may be reproduced by or for the U.S. Government pursu-
*  ant to the copyright license under DAR clause 7-104.9(a) (1979 Mar).
*
*  This material is based upon work partially supported by the National
*  Science Foundation under grants MCS-7926009 and ECS-8012974; the
*  Department of Energy Contract AM03-76SF00326, PA No. DE-AT03-
*  76ER72018; and the Army Research Office Contract DAA29-79-C-0110.
************************************************************************
      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/
      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL3CM/ LENNAM, NROWT, NCOLT, NQ
      COMMON    /SOL4CM/ EPSPT3, EPSPT5, EPSPT8, EPSPT9
      COMMON    /SOL5CM/ ASIZE, DTMAX, DTMIN

      INTEGER            LOCLS
      PARAMETER         (LENLS = 20)
      COMMON    /SOL1LS/ LOCLS(LENLS)

      LOGICAL            CMDBG, LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG
      COMMON    /CMDEBG/ ICMDBG(LDBG), CMDBG
*-----------------------------------------------------------------------
      PARAMETER         (MXPARM = 30)
      INTEGER            IPRMLS(MXPARM), IPSVLS
      REAL               RPRMLS(MXPARM), RPSVLS

      COMMON    /LSPAR1/ IPSVLS(MXPARM),
     $                   IDBGLS, ITMAX1, ITMAX2, LCRASH, LDBGLS, LPROB ,
     $                   MSGLS , NN    , NNCLIN, NPROB , IPADLS(20)

      COMMON    /LSPAR2/ RPSVLS(MXPARM),
     $                   BIGBND, BIGDX , BNDLOW, BNDUPP, TOLACT, TOLFEA,
     $                   TOLRNK, RPADLS(23)

      EQUIVALENCE       (IPRMLS(1), IDBGLS), (RPRMLS(1), BIGBND)

      SAVE      /LSPAR1/, /LSPAR2/
*-----------------------------------------------------------------------
      EQUIVALENCE   (MSGLS , MSGLVL), (IDBGLS, IDBG), (LDBGLS, MSGDBG)

      EXTERNAL           SDIV  , SDOT  , SNRM2
      INTRINSIC          ABS   , MAX   , SQRT
      LOGICAL            CONVRG, CYCLIN, ERROR , FIRSTV, HITCON,
     $                   HITLOW, NEEDFG, OVERFL, PRNT  , PRNT1 , ROWERR
      LOGICAL            SINGLR, STALL , STATPT, UNBNDD, UNCON , UNITGZ,
     $                   WEAK
      PARAMETER        ( ZERO   =0.0E+0, HALF   =0.5E+0, ONE   =1.0E+0 )
      PARAMETER        ( MREFN  =1     , MSTALL =50                    )

*     Specify the machine-dependent parameters.

      EPSMCH = WMACH(3)
      FLMAX  = WMACH(7)
      RTMAX  = WMACH(8)

      LANORM = LOCLS( 2)
      LAP    = LOCLS( 3)
      LPX    = LOCLS( 4)
      LRES   = LOCLS( 5)
      LRES0  = LOCLS( 6)
      LHZ    = LOCLS( 7)
      LGQ    = LOCLS( 8)
      LCQ    = LOCLS( 9)
      LRLAM  = LOCLS(10)
      LT     = LOCLS(11)
      LZY    = LOCLS(12)
      LWTINF = LOCLS(13)
      LWRK   = LOCLS(14)

*     Set up the adresses of the contiguous arrays  ( RES0, RES )
*     and  ( GQ, CQ ).

      NRES   = 0
      IF (NRANK .GT. 0) NRES = 2
      NGQ    = 1
      IF (LINOBJ) NGQ = 2

*     Initialize.

      IREFN  =   0
      ITER   =   0
      ITMAX  =   ITMAX1
      JADD   =   0
      JDEL   =   0
      NCNLN  =   0
      NPHASE =   1
      NSTALL =   0
      NUMINF = - 1
      NZ1    =   0

      ALFA   = ZERO
      CONDMX = FLMAX
      DRZMAX = ONE
      DRZMIN = ONE
      SSQ    = ZERO

      CYCLIN = .FALSE.
      ERROR  = .FALSE.
      FIRSTV = .FALSE.
      PRNT   = .TRUE.
      PRNT1  = .TRUE.
      NEEDFG = .TRUE.
      STALL  = .TRUE.
      UNCON  = .FALSE.
      UNBNDD = .FALSE.

*     If debug output is required,  print nothing until iteration IDBG.

      MSGSVD = MSGLVL
      IF (IDBG .GT. 0  .AND.  IDBG .LE. ITMAX) THEN
         MSGLVL = 0
      END IF

*======================== start of the main loop =======================
*
*      cyclin = false
*      unbndd = false
*      error  = false
*      k      = 0
*
*      repeat
*            repeat
*                  compute Z'g,  print details of this iteration
*                  stat pt = (Z'g .eq. 0)
*                  if (not stat pt) then
*                     error =  k .ge. itmax
*                     if (not error) then
*                        compute p, alfa
*                        error = unbndd  or  cyclin
*                        if (not error) then
*                           k = k + 1
*                           x = x + alfa p
*                           if (feasible) update Z'g
*                           if necessary, add a constraint
*                        end if
*                     end if
*                  end if
*            until  stat pt  or  error
*
*            compute lam1, lam2, smllst
*            optmul =  smllst .gt. 0
*            if ( not (optmul .or. error) ) then
*                  delete an artificial or regular constraint
*            end if
*      until optmul  or  error
*
*=======================================================================

*     REPEAT
*        REPEAT
  100       IF (NEEDFG) THEN
               IF (NRANK .GT. 0) THEN
                  RESNRM = SNRM2 ( NRANK, W(LRES), 1 )
                  SSQ    = HALF*(SSQ1**2 + RESNRM**2 )
               END IF

               IF (NUMINF .NE. 0) THEN

*                 Compute the transformed gradient of either the sum of
*                 of infeasibilities or the objective.  Initialize
*                 SINGLR and UNITGZ.

                  CALL LSGSET( PRBTYP, LINOBJ, SINGLR, UNITGZ, UNITQ,
     $                         N, NCLIN, NFREE,
     $                         NROWA, NQ, NROWR, NRANK, NZ, NZ1,
     $                         ISTATE, KX,
     $                         BIGBND, TOLRNK, NUMINF, SUMINF,
     $                         BL, BU, A, W(LRES), FEATOL,
     $                         W(LGQ), W(LCQ), R, X, W(LWTINF),
     $                         W(LZY), W(LWRK) )

                  IF (PRBTYP .NE. 'FP'  .AND.  NUMINF .EQ. 0
     $                                  .AND.  NPHASE .EQ. 1) THEN
                     ITMAX  = ITER + ITMAX2
                     NPHASE = 2
                  END IF
               END IF
            END IF

            GZNORM = ZERO
            IF (NZ  .GT. 0 ) GZNORM = SNRM2 ( NZ, W(LGQ), 1 )

            IF (NZ1 .EQ. NZ) THEN
               GZ1NRM = GZNORM
            ELSE
               GZ1NRM = ZERO
               IF (NZ1 .GT. 0) GZ1NRM = SNRM2 ( NZ1, W(LGQ), 1 )
            END IF

            GFNORM = GZNORM
            IF (NFREE .GT. 0  .AND.  NACTIV .GT. 0)
     $         GFNORM = SNRM2 ( NFREE, W(LGQ), 1 )

*           ------------------------------------------------------------
*           Print the details of this iteration.
*           ------------------------------------------------------------
*           Define small quantities that reflect the size of X, R and
*           the constraints in the working set.  If feasible,  estimate
*           the rank and condition number of Rz1.
*           Note that NZ1 .LE. NRANK + 1.

            IF (NZ1 .EQ. 0) THEN
               SINGLR = .FALSE.
            ELSE
               IF (NUMINF .GT. 0  .OR.  NZ1 .GT. NRANK) THEN
                  ABSRZZ = ZERO
               ELSE
                  CALL SCOND ( NZ1, R, NROWR+1, DRZMAX, DRZMIN )
                  ABSRZZ = ABS( R(NZ1,NZ1) )
               END IF
               SINGLR = ABSRZZ .LE. DRZMAX*TOLRNK

               IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $            WRITE (NOUT, 9100) SINGLR, ABSRZZ, DRZMAX, DRZMIN

            END IF

            CONDRZ = SDIV  ( DRZMAX, DRZMIN, OVERFL )
            CONDT  = ONE
            IF (NACTIV .GT. 0)
     $         CONDT  = SDIV  ( DTMAX , DTMIN , OVERFL )

            IF (PRNT) THEN
               CALL LSPRT ( PRBTYP, PRNT1, ISDEL, ITER, JADD, JDEL,
     $                      MSGLVL, NACTIV, NFREE, N, NCLIN,
     $                      NRANK, NROWR, NROWT, NZ, NZ1,
     $                      ISTATE,
     $                      ALFA, CONDRZ, CONDT, GFNORM, GZNORM, GZ1NRM,
     $                      NUMINF, SUMINF, CTX, SSQ,
     $                      AX, R, W(LT), X, W(LWRK) )

               JDEL  = 0
               JADD  = 0
               ALFA  = ZERO
            END IF

            IF (NUMINF .GT. 0) THEN
               DINKY  = ZERO
            ELSE
               OBJSIZ = ONE  + ABS( SSQ + CTX )
               WSSIZE = ZERO
               IF (NACTIV .GT. 0) WSSIZE = DTMAX
               DINKY  = EPSPT8 * MAX( WSSIZE, OBJSIZ, GFNORM )
               IF (UNCON) THEN
                  UNITGZ = GZ1NRM .LE. DINKY
               END IF
            END IF

            IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $         WRITE (NOUT, 9000) UNITGZ, IREFN, GZ1NRM, DINKY

*           If the projected gradient  Z'g  is small and Rz is of full
*           rank, X is a minimum on the working set.  An additional
*           refinement step is allowed to take care of an inaccurate
*           value of DINKY.

            STATPT = .NOT. SINGLR  .AND.  GZ1NRM .LE. DINKY
     $                             .OR.   IREFN  .GT. MREFN

            IF (.NOT. STATPT) THEN
*              ---------------------------------------------------------
*              Compute a search direction.
*              ---------------------------------------------------------
               PRNT  = .TRUE.

               ERROR = ITER .GE. ITMAX
               IF (.NOT. ERROR) THEN

                  IREFN = IREFN + 1
                  ITER  = ITER  + 1

                  IF (ITER .EQ. IDBG) THEN
                     LSDBG  = .TRUE.
                     CMDBG  =  LSDBG
                     MSGLVL =  MSGSVD
                  END IF

                  CALL LSGETP( LINOBJ, SINGLR, UNITGZ, UNITQ,
     $                         N, NCLIN, NFREE,
     $                         NROWA, NQ, NROWR, NRANK, NUMINF, NZ1,
     $                         ISTATE, KX, CTP, PNORM,
     $                         A, W(LAP), W(LRES), W(LHZ), W(LPX),
     $                         W(LGQ), W(LCQ), R, W(LZY), W(LWRK) )

*                 ------------------------------------------------------
*                 Find the constraint we bump into along P.
*                 Update X and AX if the step ALFA is nonzero.
*                 ------------------------------------------------------
*                 ALFHIT is initialized to BIGALF.  If it remains
*                 that way after the call to CMALF, it will be
*                 regarded as infinite.

                  BIGALF = SDIV  ( BIGDX, PNORM, OVERFL )

                  CALL CMALF ( FIRSTV, HITLOW,
     $                         ISTATE, INFORM, JADD, N, NROWA,
     $                         NCLIN, NCTOTL, NUMINF,
     $                         ALFHIT, PALFA, ATPHIT,
     $                         BIGALF, BIGBND, PNORM,
     $                         W(LANORM), W(LAP), AX,
     $                         BL, BU, FEATOL, W(LPX), X )

*                 If  Rz1  is nonsingular,  ALFA = 1.0  will be the
*                 step to the least-squares minimizer on the
*                 current subspace. If the unit step does not violate
*                 the nearest constraint by more than FEATOL,  the
*                 constraint is not added to the working set.

                  HITCON = SINGLR  .OR.  PALFA  .LE. ONE
                  UNCON  = .NOT. HITCON

                  IF (HITCON) THEN
                     ALFA = ALFHIT
                  ELSE
                     JADD   = 0
                     ALFA   = ONE
                  END IF

*                 Check for an unbounded solution or negligible step.

                  UNBNDD =  ALFA .GE. BIGALF
                  STALL  = ABS( ALFA*PNORM ) .LE. EPSPT9*XNORM
                  IF (STALL) THEN
                     NSTALL = NSTALL + 1
                     CYCLIN = NSTALL .GT. MSTALL
                  ELSE
                     NSTALL = 0
                  END IF

                  ERROR = UNBNDD  .OR.  CYCLIN
                  IF (.NOT.  ERROR) THEN
*                    ---------------------------------------------------
*                    Set X = X + ALFA*P.  Update AX, GQ, RES and CTX.
*                    ---------------------------------------------------
                     IF (ALFA .NE. ZERO)
     $                  CALL LSMOVE( HITCON, HITLOW, LINOBJ, UNITGZ,
     $                               NCLIN, NRANK, NZ1,
     $                               N, NROWR, JADD, NUMINF,
     $                               ALFA, CTP, CTX, XNORM,
     $                               W(LAP), AX, BL, BU, W(LGQ),
     $                               W(LHZ), W(LPX), W(LRES),
     $                               R, X, W(LWRK) )

                     IF (HITCON) THEN
*                       ------------------------------------------------
*                       Add a constraint to the working set.
*                       Update the TQ factors of the working set.
*                       Use P as temporary work space.
*                       ------------------------------------------------
*                       Update  ISTATE.

                        IF (BL(JADD) .EQ. BU(JADD)) THEN
                           ISTATE(JADD) = 3
                        ELSE IF (HITLOW) THEN
                           ISTATE(JADD) = 1
                        ELSE
                           ISTATE(JADD) = 2
                        END IF
                        IADD = JADD - N
                        IF (JADD .LE. N) THEN

                           DO 510 IFIX = 1, NFREE
                              IF (KX(IFIX) .EQ. JADD) GO TO 520
  510                      CONTINUE
  520                   END IF

                        CALL LSADD ( UNITQ,
     $                               INFORM, IFIX, IADD, JADD,
     $                               NACTIV, NZ, NFREE, NRANK, NRES,NGQ,
     $                               N, NROWA, NQ, NROWR, NROWT,
     $                               KX, CONDMX,
     $                               A, R, W(LT), W(LRES), W(LGQ),
     $                               W(LZY), W(LWRK), W(LRLAM) )

                        NZ1    = NZ1 - 1
                        NZ     = NZ  - 1

                        IF (JADD .LE. N) THEN

*                          A simple bound has been added.

                           NFREE  = NFREE  - 1
                        ELSE

*                          A general constraint has been added.

                           NACTIV = NACTIV + 1
                           KACTIV(NACTIV) = IADD
                        END IF

                        IREFN  = 0

                     END IF

*                    ---------------------------------------------------
*                    Check the feasibility of constraints with non-
*                    negative ISTATE values.  If some violations have
*                    occurred.  Refine the current X and set INFORM so
*                    that feasibility is checked in LSGSET.
*                    ---------------------------------------------------
                     CALL LSFEAS( N, NCLIN, ISTATE,
     $                            BIGBND, CNORM, ERR1, JMAX1, NVIOL,
     $                            AX, BL, BU, FEATOL, X, W(LWRK) )

                     IF (ERR1 .GT. FEATOL(JMAX1)) THEN
                        CALL LSSETX( LINOBJ, ROWERR, UNITQ,
     $                               NCLIN, NACTIV, NFREE, NRANK, NZ,
     $                               N, NCTOTL, NQ, NROWA, NROWR, NROWT,
     $                               ISTATE, KACTIV, KX,
     $                               JMAX1, ERR2, CTX, XNORM,
     $                               A, AX, BL, BU, W(LCQ),
     $                               W(LRES), W(LRES0), FEATOL, R,
     $                               W(LT), X, W(LZY), W(LPX), W(LWRK) )

                        IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $                     WRITE (NOUT, 2100) ERR1, ERR2
                        IF (ROWERR)       WRITE (NOUT, 2200)

                        UNCON  =   .FALSE.
                        IREFN  =   0
                        NUMINF = - 1
                     END IF
                     NEEDFG = ALFA .NE. ZERO
                  END IF
               END IF
            END IF

*        UNTIL      STATPT  .OR.  ERROR
         IF (.NOT. (STATPT  .OR.  ERROR) ) GO TO 100

*        ===============================================================
*        Try and find the index JDEL of a constraint to drop from
*        the working set.
*        ===============================================================
         JDEL   = 0

         IF (NUMINF .EQ. 0  .AND.  PRBTYP .EQ. 'FP') THEN
            IF (N .GT. NZ)
     $         CALL SLOAD ( N-NZ, (ZERO), W(LRLAM), 1 )
            JTINY  = 0
            JSMLST = 0
            JBIGST = 0
         ELSE

            CALL LSMULS( PRBTYP,
     $                   MSGLVL, N, NACTIV, NFREE,
     $                   NROWA, NROWT, NUMINF, NZ, NZ1,
     $                   ISTATE, KACTIV, KX, DINKY,
     $                   JSMLST, KSMLST, JINF, JTINY,
     $                   JBIGST, KBIGST, TRULAM,
     $                   A, W(LANORM), W(LGQ), W(LRLAM),
     $                   W(LT), W(LWTINF) )

         END IF

         IF (.NOT. ERROR) THEN
            IF (     JSMLST .GT. 0) THEN

*              LSMULS found a regular constraint with multiplier less
*              than (-DINKY).

               JDEL   = JSMLST
               KDEL   = KSMLST
               ISDEL  = ISTATE(JDEL)
               ISTATE(JDEL) = 0

            ELSE IF (JSMLST .LT. 0) THEN

               JDEL   = JSMLST

            ELSE IF (NUMINF .GT. 0  .AND.  JBIGST .GT. 0) THEN

*              No feasible point exists for the constraints but the
*              sum of the constraint violations may be reduced by
*              moving off constraints with multipliers greater than 1.

               JDEL   = JBIGST
               KDEL   = KBIGST
               ISDEL  = ISTATE(JDEL)
               IF (TRULAM .LE. ZERO) IS = - 1
               IF (TRULAM .GT. ZERO) IS = - 2
               ISTATE(JDEL) = IS
               FIRSTV = .TRUE.
               NUMINF = NUMINF + 1
            END IF

            IF      (JDEL .NE. 0  .AND.  SINGLR) THEN

*              Cannot delete a constraint when Rz is singular.
*              Probably a weak minimum.

               JDEL = 0
            ELSE IF (JDEL .NE. 0               ) THEN

*              Constraint JDEL has been deleted.
*              Update the matrix factorizations.

               CALL LSDEL ( UNITQ,
     $                      N, NACTIV, NFREE, NRES, NGQ, NZ, NZ1,
     $                      NROWA, NQ, NROWR, NROWT, NRANK,
     $                      JDEL, KDEL, KACTIV, KX,
     $                      A, W(LRES), R, W(LT), W(LGQ),W(LZY),W(LWRK))

            END IF
         END IF

         IREFN  =  0
         CONVRG =  JDEL .EQ. 0
         PRNT   = .FALSE.
         UNCON  = .FALSE.
         NEEDFG = .FALSE.

*     until       convrg  .or.  error
      IF (.NOT.  (CONVRG  .OR.  ERROR)) GO TO 100

*  .........................End of main loop............................

      WEAK = JTINY .GT. 0  .OR.  SINGLR

      IF (ERROR) THEN
         IF (UNBNDD) THEN
            INFORM = 2
            IF (NUMINF .GT. 0) INFORM = 3
         ELSE IF (ITER .GE. ITMAX) THEN
            INFORM = 4
         ELSE IF (CYCLIN) THEN
            INFORM = 5
         END IF
      ELSE IF (CONVRG) THEN
         INFORM = 0
         IF (NUMINF .GT. 0) THEN
            INFORM = 3
         ELSE IF (PRBTYP .NE. 'FP'  .AND.  WEAK) THEN
            INFORM = 1
         END IF
      END IF

*     ------------------------------------------------------------------
*     Set   CLAMDA.  Print the full solution.
*     ------------------------------------------------------------------
      MSGLVL = MSGSVD
      IF (MSGLVL .GT. 0) WRITE (NOUT, 2000) PRBTYP, ITER, INFORM

      CALL CMPRT ( MSGLVL, NFREE, NROWA,
     $             N, NCLIN, NCNLN, NCTOTL, BIGBND,
     $             NAMED, NAMES, LENNAM,
     $             NACTIV, ISTATE, KACTIV, KX,
     $             A, BL, BU, X, CLAMDA, W(LRLAM), X )

      RETURN

 2000 FORMAT(/ ' Exit from ', A2, ' problem after ', I4, ' iterations.',
     $         '  INFORM =', I3 )
 2100 FORMAT(  ' XXX  Iterative refinement.  Maximum errors before and',
     $         ' after refinement are ',  1P2E14.2 )
 2200 FORMAT(  ' XXX  Warning.  Cannot satisfy the constraints to the',
     $         ' accuracy requested.')
 9000 FORMAT(/ ' //LSCORE//  UNITGZ IREFN     GZ1NRM      DINKY'
     $       / ' //LSCORE//  ', L6, I6, 1P2E11.2 )
 9100 FORMAT(/ ' //LSCORE//  SINGLR   ABS(RZZ1)      DRZMAX      DRZMIN'
     $       / ' //LSCORE//  ', L6,     1P3E12.4 )

*     End of  LSCORE.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSCRSH( COLD, VERTEX,
     $                   NCLIN, NCTOTL, NACTIV, NARTIF,
     $                   NFREE, N, NROWA,
     $                   ISTATE, KACTIV,
     $                   BIGBND, TOLACT,
     $                   A, AX, BL, BU, X, WX, WORK )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            COLD, VERTEX
      INTEGER            ISTATE(NCTOTL), KACTIV(N)
      REAL               A(NROWA,*), AX(*), BL(NCTOTL), BU(NCTOTL),
     $                   X(N), WX(N), WORK(N)

************************************************************************
*     LSCRSH  computes the quantities  ISTATE (optionally), KACTIV,
*     NACTIV, NZ and NFREE  associated with the working set at X.
*     The computation depends upon the value of the input parameter
*     COLD,  as follows...
*
*     COLD = TRUE.  An initial working set will be selected. First,
*                   nearly-satisfied or violated bounds are added.
*                   Next,  general linear constraints are added that
*                   have small residuals.
*
*     COLD = FALSE. The quantities KACTIV, NACTIV, NZ and NFREE are
*                   computed from ISTATE,  specified by the user.
*
*     Values of ISTATE(j)....
*
*        - 2         - 1         0           1          2         3
*     a'x lt bl   a'x gt bu   a'x free   a'x = bl   a'x = bu   bl = bu
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written 31-October-1984.
*     This version of LSCRSH dated 27-December-1985.
************************************************************************
      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/
      COMMON    /SOL1CM/ NOUT

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      EXTERNAL           SDOT
      INTRINSIC          ABS, MIN
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      FLMAX  = WMACH(7)
      CALL SCOPY ( N, X, 1, WX, 1 )

      IF (LSDBG) THEN
         IF (ILSDBG(1) .GT. 0)
     $      WRITE (NOUT, 1000) COLD, NCLIN, NCTOTL
         IF (ILSDBG(2) .GT. 0)
     $      WRITE (NOUT, 1100) (WX(J), J = 1, N)
      END IF

      NFIXED = 0
      NACTIV = 0
      NARTIF = 0

*     If a cold start is being made, initialize  ISTATE.
*     If  BL(j) = BU(j),  set  ISTATE(j)=3  for all variables and linear
*     constraints.

      IF (COLD) THEN
         DO 100 J = 1, NCTOTL
            ISTATE(J) = 0
            IF (BL(J) .EQ. BU(J)) ISTATE(J) = 3
  100    CONTINUE
      ELSE
         DO 110 J = 1, NCTOTL
            IF (ISTATE(J) .GT. 3  .OR.  ISTATE(J) .LT. 0) ISTATE(J) = 0
  110    CONTINUE
      END IF

*     Initialize NFIXED, NFREE and KACTIV.
*     Ensure that the number of bounds and general constraints in the
*     working set does not exceed N.

      DO 200 J = 1, NCTOTL
         IF (NFIXED + NACTIV .EQ. N) ISTATE(J) = 0
         IF (ISTATE(J) .GT. 0) THEN
            IF (J .LE. N) THEN
               NFIXED = NFIXED + 1
               IF (ISTATE(J) .EQ. 1) WX(J) = BL(J)
               IF (ISTATE(J) .GE. 2) WX(J) = BU(J)
            ELSE
               NACTIV = NACTIV + 1
               KACTIV(NACTIV) = J - N
            END IF
         END IF
  200 CONTINUE

*     ------------------------------------------------------------------
*     If a cold start is required,  attempt to add as many
*     constraints as possible to the working set.
*     ------------------------------------------------------------------
      IF (COLD) THEN
         BIGLOW = - BIGBND
         BIGUPP =   BIGBND

*        See if any bounds are violated or nearly satisfied.
*        If so,  add these bounds to the working set and set the
*        variables exactly on their bounds.

         J = N
*+       WHILE (J .GE. 1  .AND.  NFIXED + NACTIV .LT. N) DO
  300    IF    (J .GE. 1  .AND.  NFIXED + NACTIV .LT. N) THEN
            IF (ISTATE(J) .EQ. 0) THEN
               B1     = BL(J)
               B2     = BU(J)
               IS     = 0
               IF (B1 .GT. BIGLOW) THEN
                  IF (WX(J) - B1 .LE. (ONE + ABS( B1 ))*TOLACT) IS = 1
               END IF
               IF (B2 .LT. BIGUPP) THEN
                  IF (B2 - WX(J) .LE. (ONE + ABS( B2 ))*TOLACT) IS = 2
               END IF
               IF (IS .GT. 0) THEN
                  ISTATE(J) = IS
                  IF (IS .EQ. 1) WX(J) = B1
                  IF (IS .EQ. 2) WX(J) = B2
                  NFIXED = NFIXED + 1
               END IF
            END IF
            J = J - 1
            GO TO 300
*+       END WHILE
         END IF

*        ---------------------------------------------------------------
*        The following loop finds the linear constraint (if any) with
*        smallest residual less than or equal to TOLACT  and adds it
*        to the working set.  This is repeated until the working set
*        is complete or all the remaining residuals are too large.
*        ---------------------------------------------------------------
*        First, compute the residuals for all the constraints not in the
*        working set.

         IF (NCLIN .GT. 0  .AND.  NACTIV+NFIXED .LT. N) THEN
            DO 410 I = 1, NCLIN
               IF (ISTATE(N+I) .LE. 0)
     $         AX(I) = SDOT  (N, A(I,1), NROWA, WX, 1 )
  410       CONTINUE

            IS     = 1
            TOOBIG = TOLACT + TOLACT

*+          WHILE (IS .GT. 0  .AND.  NFIXED + NACTIV .LT. N) DO
  500       IF    (IS .GT. 0  .AND.  NFIXED + NACTIV .LT. N) THEN
               IS     = 0
               RESMIN = TOLACT

               DO 520 I = 1, NCLIN
                  J      = N + I
                  IF (ISTATE(J) .EQ. 0) THEN
                     B1     = BL(J)
                     B2     = BU(J)
                     RESL   = TOOBIG
                     RESU   = TOOBIG
                     IF (B1 .GT. BIGLOW)
     $                  RESL  = ABS( AX(I) - B1 ) / (ONE + ABS( B1 ))
                     IF (B2 .LT. BIGUPP)
     $                  RESU  = ABS( AX(I) - B2 ) / (ONE + ABS( B2 ))
                     RESIDL   = MIN( RESL, RESU )
                     IF(RESIDL .LT. RESMIN) THEN
                        RESMIN = RESIDL
                        IMIN   = I
                        IS     = 1
                        IF (RESL .GT. RESU) IS = 2
                     END IF
                  END IF
  520          CONTINUE

               IF (IS .GT. 0) THEN
                  NACTIV = NACTIV + 1
                  KACTIV(NACTIV) = IMIN
                  J         = N + IMIN
                  ISTATE(J) = IS
               END IF
               GO TO 500
*+          END WHILE
            END IF
         END IF

*        ---------------------------------------------------------------
*        If required, add temporary bounds to make a vertex.
*        ---------------------------------------------------------------
         IF (VERTEX  .AND.  NACTIV+NFIXED .LT. N) THEN

*           Compute lengths of columns of selected linear constraints
*           (just the ones corresponding to free variables).

            DO 630 J = 1, N
               IF (ISTATE(J) .EQ. 0) THEN
                  COLSIZ = ZERO
                  DO 620 K = 1, NCLIN
                     IF (ISTATE(N+K) .GT. 0)
     $               COLSIZ = COLSIZ + ABS( A(K,J) )
  620             CONTINUE
                  WORK(J) = COLSIZ
               END IF
  630       CONTINUE

*           Find the  NARTIF  smallest such columns.
*           This is an expensive loop.  Later we can replace it by a
*           4-pass process (say), accepting the first col that is within
*           T  of  COLMIN, where  T = 0.0, 0.001, 0.01, 0.1 (say).
*           (This comment written in 1980).

*+          WHILE (NFIXED + NACTIV .LT. N) DO
  640       IF    (NFIXED + NACTIV .LT. N) THEN
               COLMIN = FLMAX
               DO 650 J = 1, N
                  IF (ISTATE(J) .EQ. 0) THEN
                     IF (NCLIN .EQ. 0) GO TO 660
                     COLSIZ = WORK(J)
                     IF (COLMIN .GT. COLSIZ) THEN
                        COLMIN = COLSIZ
                        JMIN   = J
                     END IF
                  END IF
  650          CONTINUE
               J      = JMIN
  660          ISTATE(J) = 4
               NARTIF = NARTIF + 1
               NFIXED = NFIXED + 1
               GO TO 640
*+          END WHILE
            END IF
         END IF
      END IF

      NFREE = N - NFIXED

      IF (LSDBG) THEN
         IF (ILSDBG(1) .GT. 0)
     $       WRITE (NOUT, 1300) NFIXED, NACTIV, NARTIF
         IF (ILSDBG(2) .GT. 0)
     $       WRITE (NOUT, 1200) (WX(J), J = 1, N)
      END IF

      RETURN

 1000 FORMAT(/ ' //LSCRSH// COLD NCLIN NCTOTL'
     $       / ' //LSCRSH// ', L4, I6, I7 )
 1100 FORMAT(/ ' //LSCRSH// Variables before crash... '/ (5G12.3))
 1200 FORMAT(/ ' //LSCRSH// Variables after  crash... '/ (5G12.3))
 1300 FORMAT(/ ' //LSCRSH// Working set selected ...             '
     $       / ' //LSCRSH// NFIXED NACTIV NARTIF      '
     $       / ' //LSCRSH// ', I6, 2I7 )

*     End of  LSCRSH.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSDEL ( UNITQ,
     $                   N, NACTIV, NFREE, NRES, NGQ, NZ, NZ1,
     $                   NROWA, NQ, NROWR, NROWT, NRANK,
     $                   JDEL, KDEL, KACTIV, KX,
     $                   A, RES, R, T, GQ, ZY, WORK )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            UNITQ
      INTEGER            KACTIV(N), KX(N)
      REAL               A(NROWA,*), RES(N,*), R(NROWR,*), T(NROWT,*),
     $                   GQ(N,*), ZY(NQ,*)
      REAL               WORK(N)

************************************************************************
*     LSDEL   updates the least-squares factor R and the factorization
*     A(free) (Z Y) = (0 T) when a regular, temporary or artificial
*     constraint is deleted from the working set.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written 31-October-1984.
*     This version of LSDEL dated 10-June-1986.
************************************************************************
      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL5CM/ ASIZE, DTMAX, DTMIN

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      EXTERNAL           ISAMAX
      PARAMETER        ( ZERO = 0.0E+0, ONE = 1.0E+0 )

      IF (JDEL .GT. 0) THEN

*        Regular constraint or temporary bound deleted.

         IF (JDEL .LE. N) THEN

*           Case 1.  A simple bound has been deleted.
*           =======  Columns NFREE+1 and IR of R must be swapped.

            IR     = NZ    + KDEL
            IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $         WRITE (NOUT, 1100) NACTIV, NZ, NFREE, IR, JDEL, UNITQ

            IBEGIN = 1
            NFREE  = NFREE + 1
            IF (NFREE .LT. IR) THEN
               KX(IR)    = KX(NFREE)
               KX(NFREE) = JDEL
               IF (NRANK .GT. 0)
     $            CALL CMRSWP( N, NRES, NRANK, NROWR, NFREE, IR,
     $                         R, RES, WORK )
               CALL SSWAP ( NGQ, GQ(NFREE,1), N, GQ(IR,1), N )
            END IF

            IF (.NOT. UNITQ) THEN

*              Copy the incoming column of  A(free)  into the end of T.

               DO 130 KA = 1, NACTIV
                  I = KACTIV(KA)
                  T(KA,NFREE) = A(I,JDEL)
  130          CONTINUE

*              Expand Q by adding a unit row and column.

               IF (NFREE .GT. 1) THEN
                  CALL SLOAD ( NFREE-1, ZERO, ZY(NFREE,1), NQ )
                  CALL SLOAD ( NFREE-1, ZERO, ZY(1,NFREE), 1  )
               END IF
               ZY(NFREE,NFREE) = ONE
            END IF
         ELSE

*           Case 2.  A general constraint has been deleted.
*           =======

            IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $         WRITE (NOUT, 1200) NACTIV, NZ, NFREE, KDEL, JDEL, UNITQ

            IBEGIN = KDEL
            NACTIV = NACTIV - 1

*           Delete a row of T and move the ones below it up.

            DO 220 I = KDEL, NACTIV
               KACTIV(I) = KACTIV(I+1)
               LD        = NFREE - I
               CALL SCOPY ( I+1, T(I+1,LD), NROWT, T(I,LD), NROWT )
  220       CONTINUE
         END IF

*        ---------------------------------------------------------------
*        Eliminate the super-diagonal elements of  T,
*        using a backward sweep of 2*2 transformations.
*        ---------------------------------------------------------------
         K     = NFREE  - IBEGIN
         L     = NACTIV - IBEGIN
         LROWR = N      - K

         DO 420 I = IBEGIN, NACTIV
            CALL SROT3G( T(I,K+1), T(I,K), CS, SN )

            IF (L .GT. 0)
     $      CALL SROT3 ( L    , T(I+1,K+1), 1, T(I+1,K ), 1, CS, SN )
            CALL SROT3 ( NFREE, ZY(1,K+1) , 1, ZY(1,K  ), 1, CS, SN )
            CALL SROT3 ( NGQ  , GQ(K+1,1) , N, GQ(K,1)  , N, CS, SN )

*           Apply the column transformations to  R.  The non-zero
*           sub-diagonal that is generated must be eliminated by a row
*           rotation.

            IF (K .LT. NRANK) R(K+1,K) = ZERO
            LCOL   = MIN( K+1, NRANK )
            IF (LCOL .GT. 0)
     $         CALL SROT3 ( LCOL, R(1,K+1), 1, R(1,K), 1, CS, SN )

            IF (K .LT. NRANK) THEN
               CALL SROT3G( R(K,K), R(K+1,K), CS, SN )

               CALL SROT3 ( LROWR, R(K,K+1)    , NROWR,
     $                             R(K+1,K+1)  , NROWR, CS, SN )
               CALL SROT3 ( NRES , RES(K,1)    , N    ,
     $                             RES(K+1,1)  , N    , CS, SN )
            END IF
            K     = K     - 1
            L     = L     - 1
            LROWR = LROWR + 1
  420    CONTINUE

         NZ  = NZ  + 1

*        ---------------------------------------------------------------
*        Estimate the condition number of  T.
*        ---------------------------------------------------------------
         IF (NACTIV .EQ. 0) THEN
            DTMAX = ONE
            DTMIN = ONE
         ELSE
            CALL SCOND ( NACTIV, T(NACTIV,NZ+1), NROWT-1, DTMAX, DTMIN )
         END IF

      END IF

      NZ1 = NZ1 + 1

      IF (NZ .GT. NZ1) THEN
         IF (JDEL .GT. 0) THEN
            JART =   NZ1 - 1 + ISAMAX( NZ-NZ1+1, GQ(NZ1,1), 1 )
         ELSE
            JART = - JDEL
         END IF

         IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $      WRITE( NOUT, 1000 ) NZ, NZ1, JART

         IF (JART .GT. NZ1) THEN

*           Swap columns NZ1 and JART of R.

            IF (UNITQ) THEN
               K        = KX(NZ1)
               KX(NZ1)  = KX(JART)
               KX(JART) = K
            ELSE
               CALL SSWAP ( NFREE, ZY(1,NZ1), 1, ZY(1,JART), 1 )
            END IF

            CALL SSWAP ( NGQ, GQ(NZ1,1), N, GQ(JART,1), N )
            IF (NRANK .GT. 0)
     $         CALL CMRSWP( N, NRES, NRANK, NROWR, NZ1, JART,
     $                      R, RES, WORK )
         END IF
      END IF

      RETURN

 1000 FORMAT(/ ' //LSDEL //  Artificial constraint deleted.      '
     $       / ' //LSDEL //      NZ   NZ1   JART                 '
     $       / ' //LSDEL //  ', 3I6 )
 1100 FORMAT(/ ' //LSDEL //  Simple bound deleted.               '
     $       / ' //LSDEL //  NACTIV    NZ NFREE    IR  JDEL UNITQ'
     $       / ' //LSDEL //  ', 5I6, L6 )
 1200 FORMAT(/ ' //LSDEL //  General constraint deleted.         '
     $       / ' //LSDEL //  NACTIV    NZ NFREE  KDEL  JDEL UNITQ'
     $       / ' //LSDEL //  ', 5I6, L6 )

*     End of  LSDEL .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSDFLT( M, N, NCLIN, TITLE )

C     IMPLICIT           REAL(A-H,O-Z)

      CHARACTER*(*)      TITLE

************************************************************************
*  LSDFLT  loads the default values of parameters not set by the user.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original Fortran 77 version written 17-September-1985.
*  This version of LSDFLT dated   9-September-1986.
************************************************************************
      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/
      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL4CM/ EPSPT3, EPSPT5, EPSPT8, EPSPT9

      LOGICAL            CMDBG, LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG
      COMMON    /CMDEBG/ ICMDBG(LDBG), CMDBG

      LOGICAL            NEWOPT
      COMMON    /SOL3LS/ NEWOPT
      SAVE      /SOL3LS/

*-----------------------------------------------------------------------
      PARAMETER         (MXPARM = 30)
      INTEGER            IPRMLS(MXPARM), IPSVLS
      REAL               RPRMLS(MXPARM), RPSVLS

      COMMON    /LSPAR1/ IPSVLS(MXPARM),
     $                   IDBGLS, ITMAX1, ITMAX2, LCRASH, LDBGLS, LPROB ,
     $                   MSGLS , NN    , NNCLIN, NPROB , IPADLS(20)

      COMMON    /LSPAR2/ RPSVLS(MXPARM),
     $                   BIGBND, BIGDX , BNDLOW, BNDUPP, TOLACT, TOLFEA,
     $                   TOLRNK, RPADLS(23)

      EQUIVALENCE       (IPRMLS(1), IDBGLS), (RPRMLS(1), BIGBND)

      SAVE      /LSPAR1/, /LSPAR2/
*-----------------------------------------------------------------------
      EQUIVALENCE   (MSGLS , MSGLVL), (IDBGLS, IDBG), (LDBGLS, MSGDBG)

      LOGICAL            CDEFND
      CHARACTER*4        ICRSH(0:2)
      CHARACTER*3        LSTYPE(1:10)
      CHARACTER*16       KEY
      INTRINSIC          LEN    ,  MAX   , MOD
      PARAMETER        ( ZERO   =  0.0E+0, TEN    = 10.0E+0)
      PARAMETER        ( RDUMMY = -11111., IDUMMY = -11111 )
      PARAMETER        ( GIGANT = 1.0E+10*.99999           )
      PARAMETER        ( WRKTOL = 1.0E-2                   )
      DATA               ICRSH(0), ICRSH(1), ICRSH(2)
     $                 /'COLD'   ,'WARM'   ,'HOT '   /
      DATA               LSTYPE(1), LSTYPE(2)
     $                 /' FP'     ,' LP'     /
      DATA               LSTYPE(3), LSTYPE(4), LSTYPE(5), LSTYPE(6)
     $                 /'QP1'     ,'QP2'     ,'QP3'     ,'QP4'     /
      DATA               LSTYPE(7), LSTYPE(8), LSTYPE(9), LSTYPE(10)
     $                 /'LS1'     ,'LS2'     ,'LS3'     ,'LS4'     /

      EPSMCH = WMACH( 3)

*     Make a dummy call to LSKEY to ensure that the defaults are set.

      CALL LSKEY ( NOUT, '*', KEY )
      NEWOPT = .TRUE.

*     Save the optional parameters set by the user.  The values in
*     RPRMLS and IPRMLS may be changed to their default values.

      CALL IYPOC ( MXPARM, IPRMLS, 1, IPSVLS, 1 )
      CALL SCOPY ( MXPARM, RPRMLS, 1, RPSVLS, 1 )

      IF (       LPROB  .LT. 0      )  LPROB   = 7
                                       CDEFND  = LPROB .EQ. 2*(LPROB/2)
      IF (       LCRASH .LT. 0
     $    .OR.   LCRASH .GT. 2      )  LCRASH  = 0
      IF (       ITMAX1 .LT. 0      )  ITMAX1  = MAX(50, 5*(N+NCLIN))
      IF (       ITMAX2 .LT. 0      )  ITMAX2  = MAX(50, 5*(N+NCLIN))
      IF (       MSGLVL .EQ. IDUMMY )  MSGLVL  = 10
      IF (       IDBG   .LT. 0
     $    .OR.   IDBG   .GT. ITMAX1 + ITMAX2
     $                              )  IDBG    = 0
      IF (       MSGDBG .LT. 0      )  MSGDBG  = 0
      IF (       MSGDBG .EQ. 0      )  IDBG    = ITMAX1 + ITMAX2 + 1
      IF (       TOLACT .LT. ZERO   )  TOLACT  = WRKTOL
      IF (       TOLFEA .EQ. RDUMMY
     $    .OR.  (TOLFEA .GE. ZERO
     $    .AND.  TOLFEA .LT. EPSMCH))  TOLFEA  = EPSPT5
      IF (       TOLRNK .LE. ZERO
     $    .AND.  CDEFND             )  TOLRNK  = EPSPT5
      IF (       TOLRNK .LE. ZERO   )  TOLRNK  = TEN*EPSMCH
      IF (       BIGBND .LE. ZERO   )  BIGBND  = GIGANT
      IF (       BIGDX  .LE. ZERO   )  BIGDX   = MAX(GIGANT, BIGBND)

      LSDBG = IDBG .EQ. 0
      CMDBG = LSDBG
      K     = 1
      MSG   = MSGDBG
      DO 200 I = 1, LDBG
         ILSDBG(I) = MOD( MSG/K, 10 )
         ICMDBG(I) = ILSDBG(I)
         K = K*10
  200 CONTINUE

      IF (MSGLVL .GT. 0) THEN

*        Print the title.

         LENT = LEN( TITLE )
         IF (LENT .GT. 0) THEN
            NSPACE = (81 - LENT)/2 + 1
            WRITE (NOUT, '(///// (80A1) )')
     $         (' ', J=1, NSPACE), (TITLE(J:J), J=1,LENT)
            WRITE (NOUT, '(80A1 //)')
     $         (' ', J=1, NSPACE), ('='       , J=1,LENT)
         END IF

         WRITE (NOUT, 2000)
         WRITE (NOUT, 2100) LSTYPE(LPROB),
     $                      NCLIN , TOLFEA, ICRSH(LCRASH),
     $                      N     , BIGBND, TOLACT,
     $                      M     , BIGDX , TOLRNK
         WRITE (NOUT, 2200) EPSMCH, ITMAX1, MSGLVL,
     $                              ITMAX2
      END IF

      RETURN

 2000 FORMAT(
     $//' Parameters'
     $/ ' ----------' )
 2100 FORMAT(
     $/ ' Problem type...........', 7X, A3
     $/ ' Linear constraints.....', I10,     6X,
     $  ' Feasibility tolerance..', 1PE10.2, 6X,
     $  1X, A4, ' start.............'
     $/ ' Variables..............', I10,     6X,
     $  ' Infinite bound size....', 1PE10.2, 6X,
     $  ' Crash tolerance........', 1PE10.2
     $/ ' Objective matrix rows..', I10,     6X,
     $  ' Infinite step size.....', 1PE10.2, 6X,
     $  ' Rank tolerance.........', 1PE10.2 )
 2200 FORMAT(
     $/ ' EPS (machine precision)', 1PE10.2, 6X,
     $  ' Feasibility phase itns.', I10, 6X,
     $  ' Print level............', I10
     $/ 40X,
     $  ' Optimality  phase itns.', I10 )

*     End of  LSDFLT.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSFEAS( N, NCLIN, ISTATE,
     $                   BIGBND, CVNORM, ERRMAX, JMAX, NVIOL,
     $                   AX, BL, BU, FEATOL, X, WORK )

C     IMPLICIT           REAL(A-H,O-Z)
      INTEGER            ISTATE(N+NCLIN)
      REAL               AX(*), BL(N+NCLIN), BU(N+NCLIN)
      REAL               FEATOL(N+NCLIN), X(N)
      REAL               WORK(N+NCLIN)

************************************************************************
*  LSFEAS  computes the following...
*  (1)  The number of constraints that are violated by more
*       than  FEATOL  and the 2-norm of the constraint violations.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original version      April    1984.
*  This version of  LSFEAS  dated  17-October-1985.
************************************************************************
      COMMON    /SOL1CM/ NOUT

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      EXTERNAL           ISAMAX, SNRM2
      INTRINSIC          ABS
      PARAMETER        ( ZERO = 0.0E+0 )

      BIGLOW = - BIGBND
      BIGUPP =   BIGBND

*     ==================================================================
*     Compute NVIOL,  the number of constraints violated by more than
*     FEATOL,  and CVNORM,  the 2-norm of the constraint violations and
*     residuals of the constraints in the working set.
*     ==================================================================
      NVIOL  = 0

      DO 200 J = 1, N+NCLIN
         FEASJ  = FEATOL(J)
         IS     = ISTATE(J)
         RES    = ZERO

         IF (IS .GE. 0  .AND.  IS .LT. 4) THEN
            IF (J .LE. N) THEN
               CON =  X(J)
            ELSE
               I   = J - N
               CON = AX(I)
            END IF

            TOLJ   = FEASJ

*           Check for constraint violations.

            IF (BL(J) .GT. BIGLOW) THEN
               RES    = BL(J) - CON
               IF (RES .GT.   FEASJ ) NVIOL = NVIOL + 1
               IF (RES .GT.    TOLJ ) GO TO 190
            END IF

            IF (BU(J) .LT. BIGUPP) THEN
               RES    = BU(J) - CON
               IF (RES .LT. (-FEASJ)) NVIOL = NVIOL + 1
               IF (RES .LT.  (-TOLJ)) GO TO 190
            END IF

*           This constraint is satisfied,  but count the residual as a
*           violation if the constraint is in the working set.

            IF (IS .LE. 0) RES = ZERO
            IF (IS .EQ. 1) RES = BL(J) - CON
            IF (IS .GE. 2) RES = BU(J) - CON
            IF (ABS( RES ) .GT. FEASJ) NVIOL = NVIOL + 1
         END IF
  190    WORK(J) = RES
  200 CONTINUE

      JMAX   = ISAMAX( N+NCLIN, WORK, 1 )
      ERRMAX = ABS ( WORK(JMAX) )

      IF (LSDBG  .AND.  ILSDBG(1) .GT. 0)
     $   WRITE (NOUT, 1000) ERRMAX, JMAX

      CVNORM  = SNRM2 ( N+NCLIN, WORK, 1 )

      RETURN

 1000 FORMAT(/ ' //LSFEAS//  The maximum violation is ', 1PE14.2,
     $                     ' in constraint', I5 )

*     End of  LSFEAS.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSFILE( IOPTNS, INFORM )
      INTEGER            IOPTNS, INFORM

************************************************************************
*     LSFILE  reads the options file from unit  IOPTNS  and loads the
*     options into the relevant elements of  IPRMLS  and  RPRMLS.
*
*     If  IOPTNS .lt. 0  or  IOPTNS .gt. 99  then no file is read,
*     otherwise the file associated with unit  IOPTNS  is read.
*
*     Output:
*
*         INFORM = 0  if a complete  OPTIONS  file was found
*                     (starting with  BEGIN  and ending with  END);
*                  1  if  IOPTNS .lt. 0  or  IOPTNS .gt. 99;
*                  2  if  BEGIN  was found, but end-of-file
*                     occurred before  END  was found;
*                  3  if end-of-file occurred before  BEGIN  or
*                     ENDRUN  were found;
*                  4  if  ENDRUN  was found before  BEGIN.
************************************************************************
      LOGICAL             NEWOPT
      COMMON     /SOL3LS/ NEWOPT
      SAVE       /SOL3LS/

      REAL                WMACH(15)
      COMMON     /SOLMCH/ WMACH
      SAVE       /SOLMCH/

      EXTERNAL            MCHPAR, LSKEY
      LOGICAL             FIRST
      SAVE                FIRST , NOUT
      DATA                FIRST /.TRUE./

*     If first time in, set NOUT.
*     NEWOPT is true first time into LSFILE or LSOPTN
*     and just after a call to LSSOL.

      IF (FIRST) THEN
         FIRST  = .FALSE.
         NEWOPT = .TRUE.
         CALL MCHPAR()
         NOUT = WMACH(11)
      END IF

      CALL OPFILE( IOPTNS, NOUT, INFORM, LSKEY )

      RETURN

*     End of  LSFILE.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSGETP( LINOBJ, SINGLR, UNITGZ, UNITQ,
     $                   N, NCLIN, NFREE,
     $                   NROWA, NQ, NROWR, NRANK, NUMINF, NZ1,
     $                   ISTATE, KX, CTP, PNORM,
     $                   A, AP, RES, HZ, P,
     $                   GQ, CQ, R, ZY, WORK )

C     IMPLICIT           REAL(A-H,O-Z)
      LOGICAL            LINOBJ, SINGLR, UNITGZ, UNITQ
      INTEGER            ISTATE(N+NCLIN), KX(N)
      REAL               A(NROWA,*), AP(*), RES(*), HZ(*), P(N),
     $                   GQ(N), CQ(*), R(NROWR,*), ZY(NQ,*)
      REAL               WORK(N)

************************************************************************
*     LSGETP  computes the following quantities for  LSCORE.
*     (1) The vector  (hz1) = (Rz1)(pz1).
*         If X is not yet feasible,  the product is computed directly.
*         If  Rz1 is singular,  hz1  is zero.  Otherwise  hz1  satisfies
*         the equations
*                        Rz1'hz1 = -gz1,
*         where  g  is the total gradient.  If there is no linear term
*         in the objective,  hz1  is set to  dz1  directly.
*     (2) The search direction P (and its 2-norm).  The vector P is
*         defined as  Z*(pz1), where  (pz1)  depends upon whether or
*         not X is feasible and the nonsingularity of  (Rz1).
*         If  NUMINF .GT. 0,  (pz1)  is the steepest-descent direction.
*         Otherwise,  x  is the solution of the  NZ1*NZ1  triangular
*         system   (Rz1)*(pz1) = (hz1).
*     (3) The vector Ap,  where A is the matrix of linear constraints.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written 31-October-1984.
*     Level 2 Blas added 11-June-1986.
*     This version of LSGETP dated 11-June-1986.
************************************************************************
      COMMON    /SOL1CM/ NOUT

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      EXTERNAL           SDOT  , SNRM2
      INTRINSIC          MIN
      PARAMETER        ( ZERO = 0.0E+0, ONE  = 1.0E+0 )

      IF (SINGLR) THEN
*        ---------------------------------------------------------------
*        The triangular factor for the current objective function is
*        singular,  i.e., the objective is linear along the last column
*        of Z1.  This can only occur when UNITGZ is TRUE.
*        ---------------------------------------------------------------
         IF (NZ1 .GT. 1) THEN
            CALL SCOPY ( NZ1-1, R(1,NZ1), 1, P, 1 )
            CALL STRSV ( 'U', 'N', 'N', NZ1-1, R, NROWR, P, 1 )
         END IF
         P(NZ1) = - ONE

         GTP = SDOT  ( NZ1, GQ, 1, P, 1 )
         IF (GTP .GT. ZERO) CALL SSCAL ( NZ1, (-ONE), P, 1 )

         IF (NZ1 .LE. NRANK) THEN
            IF (NUMINF .EQ. 0) THEN
               IF (UNITGZ) THEN
                  HZ(NZ1) = R(NZ1,NZ1)*P(NZ1)
               ELSE
                  CALL SLOAD ( NZ1, (ZERO), HZ, 1 )
               END IF
            ELSE
               HZ(1)   = R(1,1)*P(1)
            END IF
         END IF
      ELSE
*        ---------------------------------------------------------------
*        The objective is quadratic in the space spanned by Z1.
*        ---------------------------------------------------------------
         IF (LINOBJ) THEN
            IF (UNITGZ) THEN
               IF (NZ1 .GT. 1)
     $            CALL SLOAD ( NZ1-1, (ZERO), HZ, 1 )
               HZ(NZ1) = - GQ(NZ1)/R(NZ1,NZ1)
            ELSE
               CALL SCOPY ( NZ1, GQ  , 1, HZ, 1 )
               CALL SSCAL ( NZ1, (-ONE), HZ, 1 )
               CALL STRSV ( 'U', 'T', 'N', NZ1, R, NROWR, HZ, 1 )
            END IF
         ELSE
            CALL SCOPY ( NZ1, RES, 1, HZ, 1 )
         END IF

*        Solve  Rz1*pz1 = hz1.

         CALL SCOPY ( NZ1, HZ, 1, P, 1 )
         CALL STRSV ( 'U', 'N', 'N', NZ1, R, NROWR, P, 1 )
      END IF

*     Compute  p = Z1*pz1  and its norm.

      IF (LINOBJ)
     $   CTP = SDOT  ( NZ1, CQ, 1, P, 1 )
      PNORM  = SNRM2 ( NZ1, P, 1 )

      CALL CMQMUL( 1, N, NZ1, NFREE, NQ, UNITQ, KX, P, ZY, WORK )

      IF (LSDBG  .AND.  ILSDBG(2) .GT. 0)
     $   WRITE (NOUT, 1000) (P(J), J = 1, N)

*     Compute  Ap.

      IF (NCLIN .GT. 0) THEN
         CALL SLOAD ( NCLIN, ZERO, AP, 1 )
         DO 410 J = 1, N
            IF (ISTATE(J) .LE. 0)
     $         CALL SAXPY( NCLIN, P(J), A(1,J), 1, AP, 1 )
  410    CONTINUE
         IF (LSDBG  .AND.  ILSDBG(2) .GT. 0)
     $   WRITE (NOUT, 1100) (AP(I), I = 1, NCLIN)
      END IF

      RETURN

 1000 FORMAT(/ ' //LSGETP//   P ... ' / (1P5E15.5))
 1100 FORMAT(/ ' //LSGETP//  AP ... ' / (1P5E15.5))

*     End of  LSGETP.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSGSET( PRBTYP, LINOBJ, SINGLR, UNITGZ, UNITQ,
     $                   N, NCLIN, NFREE,
     $                   NROWA, NQ, NROWR, NRANK, NZ, NZ1,
     $                   ISTATE, KX,
     $                   BIGBND, TOLRNK, NUMINF, SUMINF,
     $                   BL, BU, A, RES, FEATOL,
     $                   GQ, CQ, R, X, WTINF, ZY, WRK )

C     IMPLICIT           REAL(A-H,O-Z)
      CHARACTER*2        PRBTYP
      LOGICAL            LINOBJ, SINGLR, UNITGZ, UNITQ
      INTEGER            ISTATE(*), KX(N)
      REAL               BL(*), BU(*), A(NROWA,*),
     $                   RES(*), FEATOL(*)
      REAL               GQ(N), CQ(*), R(NROWR,*), X(N), WTINF(*),
     $                   ZY(NQ,*)
      REAL               WRK(N)

************************************************************************
*     LSGSET  finds the number and weighted sum of infeasibilities for
*     the bounds and linear constraints.   An appropriate transformed
*     gradient vector is returned in  GQ.
*
*     Positive values of  ISTATE(j)  will not be altered.  These mean
*     the following...
*
*               1             2           3
*           a'x = bl      a'x = bu     bl = bu
*
*     Other values of  ISTATE(j)  will be reset as follows...
*           a'x lt bl     a'x gt bu     a'x free
*              - 2           - 1           0
*
*     If  x  is feasible,  LSGSET computes the vector Q(free)'g(free),
*     where  g  is the gradient of the the sum of squares plus the
*     linear term.  The matrix Q is of the form
*                    ( Q(free)  0       ),
*                    (   0      I(fixed))
*     where  Q(free)  is the orthogonal factor of  A(free)  and  A  is
*     the matrix of constraints in the working set.  The transformed
*     gradients are stored in GQ.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written 31-October-1984.
*     Level 2 Blas added 11-June-1986.
*     This version of LSGSET dated 24-June-1986.
************************************************************************
      EXTERNAL           SDOT  , ISRANK
      INTRINSIC          ABS   , MAX   , MIN
      PARAMETER        ( ZERO = 0.0E+0, HALF = 0.5E+0, ONE = 1.0E+0 )

      BIGUPP =   BIGBND
      BIGLOW = - BIGBND

      NUMINF =   0
      SUMINF =   ZERO
      CALL SLOAD ( N, ZERO, GQ, 1 )

      DO 200 J = 1, N+NCLIN
         IF (ISTATE(J) .LE. 0) THEN
            FEASJ  = FEATOL(J)
            IF (J .LE. N) THEN
               CTX = X(J)
            ELSE
               K   = J - N
               CTX = SDOT  ( N, A(K,1), NROWA, X, 1 )
            END IF
            ISTATE(J) = 0

*           See if the lower bound is violated.

            IF (BL(J) .GT. BIGLOW) THEN
               S = BL(J) - CTX
               IF (S     .GT. FEASJ ) THEN
                  ISTATE(J) = - 2
                  WEIGHT    = - WTINF(J)
                  GO TO 160
               END IF
            END IF

*           See if the upper bound is violated.

            IF (BU(J) .GE. BIGUPP) GO TO 200
            S = CTX - BU(J)
            IF (S     .LE. FEASJ ) GO TO 200
            ISTATE(J) = - 1
            WEIGHT    =   WTINF(J)

*           Add the infeasibility.

  160       NUMINF = NUMINF + 1
            SUMINF = SUMINF + ABS( WEIGHT ) * S
            IF (J .LE. N) THEN
               GQ(J) = WEIGHT
            ELSE
               CALL SAXPY ( N, WEIGHT, A(K,1), NROWA, GQ, 1 )
            END IF
         END IF
  200 CONTINUE

*     ------------------------------------------------------------------
*     Install  GQ,  the transformed gradient.
*     ------------------------------------------------------------------
      SINGLR = .FALSE.
      UNITGZ = .TRUE.

      IF (NUMINF .GT. 0) THEN
         CALL CMQMUL( 6, N, NZ, NFREE, NQ, UNITQ, KX, GQ, ZY, WRK )
      ELSE IF (NUMINF .EQ. 0  .AND.  PRBTYP .EQ. 'FP') THEN
         CALL SLOAD ( N, ZERO, GQ, 1 )
      ELSE

*        Ready for the Optimality Phase.
*        Set NZ1 so that Rz1 is nonsingular.

         IF (NRANK .EQ. 0) THEN
            IF (LINOBJ) THEN
               CALL SCOPY ( N, CQ, 1, GQ, 1 )
            ELSE
               CALL SLOAD ( N, ZERO, GQ, 1 )
            END IF
            NZ1    = 0
         ELSE

*           Compute  GQ = - R' * (transformed residual)

            CALL SCOPY ( NRANK, RES, 1, GQ, 1 )
            CALL SSCAL ( NRANK, (-ONE), GQ, 1 )
            CALL STRMV ( 'U', 'T', 'N', NRANK, R, NROWR, GQ, 1 )
            IF (NRANK .LT. N)
     $         CALL SGEMV( 'T', NRANK, N-NRANK, -ONE,R(1,NRANK+1),NROWR,
     $                      RES, 1, ZERO, GQ(NRANK+1), 1 )

            IF (LINOBJ) CALL SAXPY ( N, ONE, CQ, 1, GQ, 1 )
            UNITGZ = .FALSE.
            NZ1    = ISRANK( MIN(NRANK, NZ), R, NROWR+1, TOLRNK )
         END IF
      END IF

      RETURN

*     End of  LSGSET.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSKEY ( NOUT, BUFFER, KEY )

C     IMPLICIT           REAL(A-H,O-Z)
      CHARACTER*(*)      BUFFER

************************************************************************
*     LSKEY   decodes the option contained in  BUFFER  in order to set
*     a parameter value in the relevant element of  IPRMLS  or  RPRMLS.
*
*
*     Input:
*
*     NOUT   A unit number for printing error messages.
*            NOUT  must be a valid unit.
*
*     Output:
*
*     KEY    The first keyword contained in BUFFER.
*
*
*     LSKEY  calls OPNUMB and the subprograms
*                 LOOKUP, SCANNR, TOKENS, UPCASE
*     (now called OPLOOK, OPSCAN, OPTOKN, OPUPPR)
*     supplied by Informatics General, Inc., Palo Alto, California.
*
*     Systems Optimization Laboratory, Stanford University.
*     This version dated Jan 22, 1986.
************************************************************************
*-----------------------------------------------------------------------
      PARAMETER         (MXPARM = 30)
      INTEGER            IPRMLS(MXPARM), IPSVLS
      REAL               RPRMLS(MXPARM), RPSVLS

      COMMON    /LSPAR1/ IPSVLS(MXPARM),
     $                   IDBGLS, ITMAX1, ITMAX2, LCRASH, LDBGLS, LPROB ,
     $                   MSGLS , NN    , NNCLIN, NPROB , IPADLS(20)

      COMMON    /LSPAR2/ RPSVLS(MXPARM),
     $                   BIGBND, BIGDX , BNDLOW, BNDUPP, TOLACT, TOLFEA,
     $                   TOLRNK, RPADLS(23)

      EQUIVALENCE       (IPRMLS(1), IDBGLS), (RPRMLS(1), BIGBND)

      SAVE      /LSPAR1/, /LSPAR2/
*-----------------------------------------------------------------------

      EXTERNAL           OPNUMB
      LOGICAL            FIRST , MORE  , NUMBER, OPNUMB, SORTED
      SAVE               FIRST

      PARAMETER         (     MAXKEY = 27,  MAXTIE = 10,   MAXTOK = 10,
     $                        MAXTYP = 16)
      CHARACTER*16       KEYS(MAXKEY), TIES(MAXTIE), TOKEN(MAXTOK),
     $                   TYPE(MAXTYP)
      CHARACTER*16       KEY, KEY2, KEY3, VALUE

      PARAMETER         (IDUMMY = -11111,  RDUMMY = -11111.0,
     $                   SORTED = .TRUE.,  ZERO   =  0.0     )

      DATA                FIRST
     $                  /.TRUE./
      DATA   KEYS
     $ / 'BEGIN           ',
     $   'COLD            ', 'CONSTRAINTS     ', 'CRASH           ',
     $   'DEBUG           ', 'DEFAULTS        ', 'END             ',
     $   'FEASIBILITY     ', 'HOT             ', 'INFINITE        ',
     $   'IPRMLS          ', 'ITERATIONS      ', 'ITERS:ITERATIONS',
     $   'ITNS :ITERATIONS', 'LINEAR          ', 'LIST            ',
     $   'LOWER           ', 'NOLIST          ', 'OPTIMALITY      ',
     $   'PRINT           ', 'PROBLEM         ', 'RANK            ',
     $   'RPRMLS          ', 'START           ', 'UPPER           ',
     $   'VARIABLES       ', 'WARM            '/

      DATA   TIES
     $ / 'BOUND           ', 'CONSTRAINTS     ',
     $   'NO              ', 'NO.      :NUMBER', 'NUMBER          ',
     $   'PHASE           ', 'STEP            ',
     $   'TOLERANCE       ', 'TYPE            ', 'YES             '/

      DATA   TYPE
     $ / 'FP              ',
     $   'LEAST       :LS1', 'LINEAR       :LP', 'LP              ',
     $   'LS          :LS1', 'LS1             ', 'LS2             ',
     $   'LS3             ', 'LS4             ', 'LSQ         :LS1',
     $   'QP          :QP2', 'QP1             ', 'QP2             ',
     $   'QP3             ', 'QP4             ', 'QUADRATIC   :QP2'/
*-----------------------------------------------------------------------

      IF (FIRST) THEN
         FIRST  = .FALSE.
         DO 10 I = 1, MXPARM
            IPRMLS(I) = IDUMMY
            RPRMLS(I) = RDUMMY
   10    CONTINUE
      END IF

*     Eliminate comments and empty lines.
*     A '*' appearing anywhere in BUFFER terminates the string.

      I      = INDEX( BUFFER, '*' )
      IF (I .EQ. 0) THEN
         LENBUF = LEN( BUFFER )
      ELSE
         LENBUF = I - 1
      END IF
      IF (LENBUF .LE. 0) THEN
         KEY = '*'
         GO TO 900
      END IF

*     ------------------------------------------------------------------
*     Extract up to MAXTOK tokens from the record.
*     NTOKEN returns how many were actually found.
*     KEY, KEY2, KEY3 are the first tokens if any, otherwise blank.
*     ------------------------------------------------------------------
      NTOKEN = MAXTOK
      CALL OPTOKN( BUFFER(1:LENBUF), NTOKEN, TOKEN )
      KEY    = TOKEN(1)
      KEY2   = TOKEN(2)
      KEY3   = TOKEN(3)

*     Certain keywords require no action.

      IF (KEY .EQ. ' '     .OR.  KEY .EQ. 'BEGIN' ) GO TO 900
      IF (KEY .EQ. 'LIST'  .OR.  KEY .EQ. 'NOLIST') GO TO 900
      IF (KEY .EQ. 'END'                          ) GO TO 900

*     Most keywords will have an associated integer or real value,
*     so look for it no matter what the keyword.

      I      = 1
      NUMBER = .FALSE.

   50 IF (I .LT. NTOKEN  .AND.  .NOT. NUMBER) THEN
         I      = I + 1
         VALUE  = TOKEN(I)
         NUMBER = OPNUMB( VALUE )
         GO TO 50
      END IF

      IF (NUMBER) THEN
         READ (VALUE, '(BN, E16.0)') RVALUE
      ELSE
         RVALUE = ZERO
      END IF

*     Convert the keywords to their most fundamental form
*     (upper case, no abbreviations).
*     SORTED says whether the dictionaries are in alphabetic order.
*     LOCi   says where the keywords are in the dictionaries.
*     LOCi = 0 signals that the keyword wasn't there.

      CALL OPLOOK( MAXKEY, KEYS, SORTED, KEY , LOC1 )
      CALL OPLOOK( MAXTIE, TIES, SORTED, KEY2, LOC2 )

*     ------------------------------------------------------------------
*     Decide what to do about each keyword.
*     The second keyword (if any) might be needed to break ties.
*     Some seemingly redundant testing of MORE is used
*     to avoid compiler limits on the number of consecutive ELSE IFs.
*     ------------------------------------------------------------------
      MORE   = .TRUE.
      IF (MORE) THEN
         MORE   = .FALSE.
         IF (KEY .EQ. 'COLD        ') THEN
            LCRASH = 0
         ELSE IF (KEY .EQ. 'CONSTRAINTS ') THEN
            NNCLIN = RVALUE
         ELSE IF (KEY .EQ. 'CRASH       ') THEN
            TOLACT = RVALUE
         ELSE IF (KEY .EQ. 'DEBUG       ') THEN
            LDBGLS = RVALUE
         ELSE IF (KEY .EQ. 'DEFAULTS    ') THEN
            DO 20 I = 1, MXPARM
               IPRMLS(I) = IDUMMY
               RPRMLS(I) = RDUMMY
   20       CONTINUE
         ELSE IF (KEY .EQ. 'FEASIBILITY ') THEN
              IF (KEY2.EQ. 'PHASE       ') ITMAX1 = RVALUE
              IF (KEY2.EQ. 'TOLERANCE   ') TOLFEA = RVALUE
              IF (LOC2.EQ.  0            ) WRITE(NOUT, 2320) KEY2
         ELSE
            MORE   = .TRUE.
         END IF
      END IF

      IF (MORE) THEN
         MORE   = .FALSE.
         IF (KEY .EQ. 'HOT         ') THEN
            LCRASH = 2
         ELSE IF (KEY .EQ. 'INFINITE    ') THEN
              IF (KEY2.EQ. 'BOUND       ') BIGBND = RVALUE * 0.99999
              IF (KEY2.EQ. 'STEP        ') BIGDX  = RVALUE
              IF (LOC2.EQ.  0            ) WRITE(NOUT, 2320) KEY2
         ELSE IF (KEY .EQ. 'IPRMLS      ') THEN
*           Allow things like  IPRMLS 21 = 100  to set IPRMLS(21) = 100
            IVALUE = RVALUE
            IF (IVALUE .GE. 1  .AND. IVALUE .LE. MXPARM) THEN
               READ (KEY3, '(BN, I16)') IPRMLS(IVALUE)
            ELSE
               WRITE(NOUT, 2400) IVALUE
            END IF
         ELSE IF (KEY .EQ. 'ITERATIONS  ') THEN
            ITMAX2 = RVALUE
         ELSE IF (KEY .EQ. 'LINEAR      ') THEN
            NNCLIN = RVALUE
         ELSE IF (KEY .EQ. 'LOWER       ') THEN
            BNDLOW = RVALUE
         ELSE
            MORE   = .TRUE.
         END IF
      END IF

      IF (MORE) THEN
         MORE   = .FALSE.
         IF      (KEY .EQ. 'OPTIMALITY  ') THEN
            ITMAX2 = RVALUE
         ELSE IF (KEY .EQ. 'PROBLEM     ') THEN
            IF      (KEY2 .EQ. 'NUMBER') THEN
               NPROB  = RVALUE
            ELSE IF (KEY2 .EQ. 'TYPE  ') THEN

*              Recognize     Problem type = LP     etc.

               CALL OPLOOK( MAXTYP, TYPE, SORTED, KEY3, LOC3 )
               IF (KEY3 .EQ. 'FP' ) LPROB = 1
               IF (KEY3 .EQ. 'LP' ) LPROB = 2
               IF (KEY3 .EQ. 'QP1') LPROB = 3
               IF (KEY3 .EQ. 'QP2') LPROB = 4
               IF (KEY3 .EQ. 'QP3') LPROB = 5
               IF (KEY3 .EQ. 'QP4') LPROB = 6
               IF (KEY3 .EQ. 'LS1') LPROB = 7
               IF (KEY3 .EQ. 'LS2') LPROB = 8
               IF (KEY3 .EQ. 'LS3') LPROB = 9
               IF (KEY3 .EQ. 'LS4') LPROB = 10
               IF (LOC3 .EQ.  0  ) WRITE(NOUT, 2330) KEY3
            ELSE
               WRITE(NOUT, 2320) KEY2
            END IF
         ELSE
            MORE   = .TRUE.
         END IF
      END IF

      IF (MORE) THEN
         MORE   = .FALSE.
         IF      (KEY .EQ. 'PRINT       ') THEN
            MSGLS  = RVALUE
         ELSE IF (KEY .EQ. 'RANK        ') THEN
            TOLRNK = RVALUE
         ELSE IF (KEY .EQ. 'RPRMLS      ') THEN
*           Allow things like  RPRMLS 21 = 2  to set RPRMLS(21) = 2.0
            IVALUE = RVALUE
            IF (IVALUE .GE. 1  .AND. IVALUE .LE. MXPARM) THEN
               READ (KEY3, '(BN, E16.0)') RPRMLS(IVALUE)
            ELSE
               WRITE(NOUT, 2400) IVALUE
            END IF
         ELSE IF (KEY .EQ. 'START       ') THEN
            IDBGLS = RVALUE
         ELSE IF (KEY .EQ. 'UPPER       ') THEN
            BNDUPP = RVALUE
         ELSE IF (KEY .EQ. 'VARIABLES   ') THEN
            NN     = RVALUE
         ELSE IF (KEY .EQ. 'WARM        ') THEN
            LCRASH = 1
         ELSE
            WRITE(NOUT, 2300) KEY
         END IF
      END IF

  900 RETURN

 2300 FORMAT(' XXX  Keyword not recognized:         ', A)
 2320 FORMAT(' XXX  Second keyword not recognized:  ', A)
 2330 FORMAT(' XXX  Third  keyword not recognized:  ', A)
 2400 FORMAT(' XXX  The PARM subscript is out of range:', I10)

*     End of LSKEY

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSLOC ( LPROB, N, NCLIN, LITOTL, LWTOTL )

C     IMPLICIT           REAL(A-H,O-Z)

************************************************************************
*     LSLOC   allocates the addresses of the work arrays for  LSCORE.
*
*     Note that the arrays  ( GQ, CQ )  and  ( RES, RES0, HZ )  lie in
*     contiguous areas of workspace.
*     RES, RES0 and HZ are not needed for LP.
*     CQ is defined when the objective has an explicit linear term.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written  29-October-1984.
*     This version of LSLOC dated 16-February-1986.
************************************************************************
      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL3CM/ LENNAM, NROWT, NCOLT, NQ

      PARAMETER        ( LENLS = 20 )
      COMMON    /SOL1LS/ LOCLS(LENLS)

      LOGICAL            LSDBG
      PARAMETER        ( LDBG = 5 )
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      MINIW     = LITOTL + 1
      MINW      = LWTOTL + 1


*     Assign array lengths that depend upon the problem dimensions.

      IF (NCLIN .EQ. 0) THEN
         LENT      = 0
         LENZY     = 0
      ELSE
         LENT  = NROWT*NCOLT
         LENZY = NQ   *NQ
      END IF

      LENCQ  = 0
      IF (LPROB .EQ. 2*(LPROB/2)) LENCQ  = N
      LENRES = 0
      IF (LPROB .GT. 2          ) LENRES = N

      LKACTV    = MINIW
      MINIW     = LKACTV + N

      LANORM    = MINW
      LAP       = LANORM + NCLIN
      LPX       = LAP    + NCLIN
      LGQ       = LPX    + N
      LCQ       = LGQ    + N
      LRES      = LCQ    + LENCQ
      LRES0     = LRES   + LENRES
      LHZ       = LRES0  + LENRES
      LRLAM     = LHZ    + LENRES
      LT        = LRLAM  + N
      LZY       = LT     + LENT
      LWTINF    = LZY    + LENZY
      LWRK      = LWTINF + N  + NCLIN
      LFEATL    = LWRK   + N  + NCLIN
      MINW      = LFEATL + N  + NCLIN

      LOCLS( 1) = LKACTV
      LOCLS( 2) = LANORM
      LOCLS( 3) = LAP
      LOCLS( 4) = LPX
      LOCLS( 5) = LRES
      LOCLS( 6) = LRES0
      LOCLS( 7) = LHZ
      LOCLS( 8) = LGQ
      LOCLS( 9) = LCQ
      LOCLS(10) = LRLAM
      LOCLS(11) = LT
      LOCLS(12) = LZY
      LOCLS(13) = LWTINF
      LOCLS(14) = LWRK
      LOCLS(15) = LFEATL

      LITOTL    = MINIW - 1
      LWTOTL    = MINW  - 1

      RETURN

*     End of  LSLOC .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSMOVE( HITCON, HITLOW, LINOBJ, UNITGZ,
     $                   NCLIN, NRANK, NZ1,
     $                   N, NROWR, JADD, NUMINF,
     $                   ALFA, CTP, CTX, XNORM,
     $                   AP, AX, BL, BU, GQ, HZ, P, RES,
     $                   R, X, WORK )

C     IMPLICIT           REAL (A-H,O-Z)
      LOGICAL            HITCON, HITLOW, LINOBJ, UNITGZ
      REAL               AP(*), AX(*), BL(*), BU(*), GQ(*), HZ(*),
     $                   P(N), RES(*), R(NROWR,*), X(N)
      REAL               WORK(*)

************************************************************************
*     LSMOVE  changes X to X + ALFA*P and updates CTX, AX, RES and GQ
*     accordingly.
*
*     If a bound was added to the working set,  move X exactly on to it,
*     except when a negative step was taken (CMALF may have had to move
*     to some other closer constraint.)
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written 27-December-1985.
*     Level 2 BLAS added 11-June-1986.
*     This version of LSMOVE dated 11-June-1986.
************************************************************************
      COMMON    /SOL1CM/ NOUT

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      EXTERNAL           SDOT  , SNRM2
      INTRINSIC          ABS   , MIN
      PARAMETER        ( ZERO  = 0.0E+0, ONE = 1.0E+0 )

      CALL SAXPY ( N, ALFA, P, 1, X, 1 )
      IF (LINOBJ) CTX = CTX + ALFA*CTP

      IF (HITCON  .AND.  JADD .LE. N) THEN
         BND = BU(JADD)
         IF (HITLOW) BND = BL(JADD)
         IF (ALFA .GE. ZERO) X(JADD) = BND
      END IF
      XNORM  = SNRM2 ( N, X, 1 )

      IF (NCLIN .GT. 0)
     $   CALL SAXPY ( NCLIN, ALFA, AP, 1, AX, 1 )

      IF (NZ1 .LE. NRANK) THEN
         IF (UNITGZ) THEN
            RES(NZ1) = RES(NZ1) - ALFA*HZ(NZ1)
         ELSE
            CALL SAXPY ( NZ1, (-ALFA), HZ, 1, RES, 1  )
         END IF

         IF (NUMINF .EQ. 0) THEN

*           Update the transformed gradient GQ so that
*           GQ = GQ + ALFA*R'( HZ ).
*                            ( 0  )

            IF (UNITGZ) THEN
               CALL SAXPY ( N-NZ1+1, ALFA*HZ(NZ1), R(NZ1,NZ1), NROWR,
     $                                             GQ(NZ1)   , 1      )
            ELSE
               CALL SCOPY ( NZ1, HZ, 1, WORK, 1 )
               CALL STRMV ( 'U', 'T', 'N', NZ1, R, NROWR, WORK, 1 )
               IF (NZ1 .LT. N)
     $            CALL SGEMV ( 'T', NZ1, N-NZ1, ONE, R(1,NZ1+1), NROWR,
     $                         HZ, 1, ZERO, WORK(NZ1+1), 1 )
               CALL SAXPY ( N, ALFA, WORK, 1, GQ, 1 )
            END IF
         END IF
      END IF

      RETURN

*     End of  LSMOVE.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSMULS( PRBTYP,
     $                   MSGLVL, N, NACTIV, NFREE,
     $                   NROWA, NROWT, NUMINF, NZ, NZ1,
     $                   ISTATE, KACTIV, KX, DINKY,
     $                   JSMLST, KSMLST, JINF, JTINY,
     $                   JBIGST, KBIGST, TRULAM,
     $                   A, ANORMS, GQ, RLAMDA, T, WTINF )

C     IMPLICIT           REAL(A-H,O-Z)
      CHARACTER*2        PRBTYP
      INTEGER            ISTATE(*), KACTIV(N), KX(N)
      REAL               A(NROWA,*), ANORMS(*),
     $                   GQ(N), RLAMDA(N), T(NROWT,*), WTINF(*)

************************************************************************
*     LSMULS  first computes the Lagrange multiplier estimates for the
*     given working set.  It then determines the values and indices of
*     certain significant multipliers.  In this process, the multipliers
*     for inequalities at their upper bounds are adjusted so that a
*     negative multiplier for an inequality constraint indicates non-
*     optimality.  All adjusted multipliers are scaled by the 2-norm
*     of the associated constraint row.  In the following, the term
*     minimum refers to the ordering of numbers on the real line,  and
*     not to their magnitude.
*
*     JSMLST  is the index of the minimum of the set of adjusted
*             multipliers with values less than  - DINKY.  A negative
*             JSMLST defines the index in Q'g of the artificial
*             constraint to be deleted.
*     KSMLST  marks the position of general constraint JSMLST in KACTIV.
*
*     JBIGST  is the index of the largest of the set of adjusted
*             multipliers with values greater than (1 + DINKY).
*     KBIGST  marks its position in KACTIV.
*
*     On exit,  elements 1 thru NACTIV of RLAMDA contain the unadjusted
*     multipliers for the general constraints.  Elements NACTIV onwards
*     of RLAMDA contain the unadjusted multipliers for the bounds.
*
*     Systems Optimization Laboratory, Stanford University.
*     Original version written 31-October-1984.
*     This version of LSMULS dated  30-June-1986.
************************************************************************
      COMMON    /SOL1CM/ NOUT

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      INTRINSIC          ABS, MIN
      PARAMETER        ( ZERO   =0.0E+0,ONE    =1.0E+0 )

      NFIXED =   N - NFREE

      JSMLST =   0
      KSMLST =   0
      SMLLST = - DINKY

      TINYLM =   DINKY
      JTINY  =   0

      JBIGST =   0
      KBIGST =   0
      BIGGST =   ONE + DINKY

      IF (NZ1 .LT. NZ) THEN
*        ---------------------------------------------------------------
*        Compute JSMLST for the artificial constraints.
*        ---------------------------------------------------------------
         DO 100 J = NZ1+1, NZ
            RLAM = - ABS( GQ(J) )
            IF (RLAM .LT. SMLLST) THEN
               SMLLST =   RLAM
               JSMLST = - J
            ELSE IF (RLAM .LT. TINYLM) THEN
               TINYLM =   RLAM
               JTINY  =   J
            END IF
  100    CONTINUE

         IF (MSGLVL .GE. 20)
     $      WRITE (NOUT, 1000) (GQ(K), K=NZ1+1,NZ)

      END IF

*     ------------------------------------------------------------------
*     Compute JSMLST for regular constraints and temporary bounds.
*     ------------------------------------------------------------------
*     First, compute the Lagrange multipliers for the general
*     constraints in the working set, by solving  T'*lamda = Y'g.

      IF (N .GT. NZ)
     $   CALL SCOPY ( N-NZ, GQ(NZ+1), 1, RLAMDA, 1 )
      IF (NACTIV .GT. 0)
     $   CALL CMTSOL( 2, NROWT, NACTIV, T(1,NZ+1), RLAMDA )

*     -----------------------------------------------------------------
*     Now set elements NACTIV, NACTIV+1,... of  RLAMDA  equal to
*     the multipliers for the bound constraints.
*     -----------------------------------------------------------------
      DO 190 L = 1, NFIXED
         J     = KX(NFREE+L)
         BLAM  = RLAMDA(NACTIV+L)
         DO 170 K = 1, NACTIV
            I    = KACTIV(K)
            BLAM = BLAM - A(I,J)*RLAMDA(K)
  170    CONTINUE
         RLAMDA(NACTIV+L) = BLAM
  190 CONTINUE

*     -----------------------------------------------------------------
*     Find JSMLST and KSMLST.
*     -----------------------------------------------------------------
      DO 330 K = 1, N - NZ
         IF (K .GT. NACTIV) THEN
            J = KX(NZ+K)
         ELSE
            J = KACTIV(K) + N
         END IF

         IS   = ISTATE(J)

         I    = J - N
         IF (J .LE. N) ANORMJ = ONE
         IF (J .GT. N) ANORMJ = ANORMS(I)

         RLAM = RLAMDA(K)

*        Change the sign of the estimate if the constraint is in
*        the working set at its upper bound.

         IF (IS .EQ. 2) RLAM =      - RLAM
         IF (IS .EQ. 3) RLAM =   ABS( RLAM )
         IF (IS .EQ. 4) RLAM = - ABS( RLAM )

         IF (IS .NE. 3) THEN
            SCDLAM = RLAM * ANORMJ
            IF      (SCDLAM .LT. SMLLST) THEN
               SMLLST = SCDLAM
               JSMLST = J
               KSMLST = K
            ELSE IF (SCDLAM .LT. TINYLM) THEN
               TINYLM = SCDLAM
               JTINY  = J
            END IF
         END IF

         IF (NUMINF .GT. 0  .AND.  J .GT. JINF) THEN
            SCDLAM = RLAM/WTINF(J)
            IF (SCDLAM .GT. BIGGST) THEN
               BIGGST = SCDLAM
               TRULAM = RLAMDA(K)
               JBIGST = J
               KBIGST = K
            END IF
         END IF
  330 CONTINUE

*     -----------------------------------------------------------------
*     If required, print the multipliers.
*     -----------------------------------------------------------------
      IF (MSGLVL .GE. 20) THEN
         IF (NFIXED .GT. 0)
     $      WRITE (NOUT, 1100) PRBTYP, (KX(NFREE+K),
     $                         RLAMDA(NACTIV+K), K=1,NFIXED)
         IF (NACTIV .GT. 0)
     $      WRITE (NOUT, 1200) PRBTYP, (KACTIV(K),
     $                         RLAMDA(K), K=1,NACTIV)
      END IF

      IF (LSDBG  .AND.  ILSDBG(1) .GT. 0) THEN
         WRITE (NOUT, 9000) JSMLST, SMLLST, KSMLST
         WRITE (NOUT, 9100) JBIGST, BIGGST, KBIGST
         WRITE (NOUT, 9200) JTINY , TINYLM
      END IF

      RETURN

 1000 FORMAT(/ ' Multipliers for the artificial constraints        '
     $       / 4(5X, 1PE11.2))
 1100 FORMAT(/ ' Multipliers for the ', A2, ' bound  constraints   '
     $       / 4(I5, 1PE11.2))
 1200 FORMAT(/ ' Multipliers for the ', A2, ' linear constraints   '
     $       / 4(I5, 1PE11.2))
 9000 FORMAT(/ ' //LSMULS//  JSMLST     SMLLST     KSMLST (Scaled) '
     $       / ' //LSMULS//  ', I6, 1PE11.2, 5X, I6 )
 9100 FORMAT(  ' //LSMULS//  JBIGST     BIGGST     KBIGST (Scaled) '
     $       / ' //LSMULS//  ', I6, 1PE11.2, 5X, I6 )
 9200 FORMAT(  ' //LSMULS//   JTINY     TINYLM                     '
     $       / ' //LSMULS//  ', I6, 1PE11.2)

*     End of  LSMULS.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSOPTN( STRING )
      CHARACTER*(*)      STRING

************************************************************************
*     LSOPTN  loads the option supplied in  STRING  into the relevant
*     element of  IPRMLS  or  RPRMLS.
************************************************************************

      LOGICAL             NEWOPT
      COMMON     /SOL3LS/ NEWOPT
      SAVE       /SOL3LS/

      REAL                WMACH(15)
      COMMON     /SOLMCH/ WMACH
      SAVE       /SOLMCH/

      EXTERNAL            MCHPAR
      CHARACTER*16        KEY
      CHARACTER*72        BUFFER
      LOGICAL             FIRST , PRNT
      SAVE                FIRST , NOUT  , PRNT
      DATA                FIRST /.TRUE./

*     If first time in, set  NOUT.
*     NEWOPT  is true first time into  LSFILE  or  LSOPTN
*     and just after a call to  LSSOL.
*     PRNT    is set to true whenever  NEWOPT  is true.

      IF (FIRST) THEN
         FIRST  = .FALSE.
         NEWOPT = .TRUE.
         CALL MCHPAR()
         NOUT   =  WMACH(11)
      END IF
      BUFFER = STRING

*     Call  LSKEY   to decode the option and set the parameter value.
*     If NEWOPT is true, reset PRNT and test specially for NOLIST.

      IF (NEWOPT) THEN
         NEWOPT = .FALSE.
         PRNT   = .TRUE.
         CALL LSKEY ( NOUT, BUFFER, KEY )

         IF (KEY .EQ. 'NOLIST') THEN
            PRNT   = .FALSE.
         ELSE
            WRITE (NOUT, '(// A / A /)')
     $         ' Calls to LSOPTN',
     $         ' ---------------'
            WRITE (NOUT, '( 6X, A )') BUFFER
         END IF
      ELSE
         IF (PRNT)
     $      WRITE (NOUT, '( 6X, A )') BUFFER
         CALL LSKEY ( NOUT, BUFFER, KEY )

         IF (KEY .EQ.   'LIST') PRNT = .TRUE.
         IF (KEY .EQ. 'NOLIST') PRNT = .FALSE.
      END IF

      RETURN

*     End of  LSOPTN.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSPRT ( PRBTYP, PRNT1, ISDEL, ITER, JADD, JDEL,
     $                   MSGLVL, NACTIV, NFREE, N, NCLIN,
     $                   NRANK, NROWR, NROWT, NZ, NZ1, ISTATE,
     $                   ALFA, CONDRZ, CONDT, GFNORM, GZNORM, GZ1NRM,
     $                   NUMINF, SUMINF, CTX, SSQ,
     $                   AX, R, T, X, WORK )

C     IMPLICIT           REAL(A-H,O-Z)
      CHARACTER*2        PRBTYP
      LOGICAL            PRNT1
      INTEGER            ISTATE(*)
      REAL               AX(*), R(NROWR,*), T(NROWT,*), X(N)
      REAL               WORK(N)

************************************************************************
*  LSPRT  prints various levels of output for  LSCORE.
*
*           Msg        Cumulative result
*           ---        -----------------
*
*        le   0        no output.
*
*        eq   1        nothing now (but full output later).
*
*        eq   5        one terse line of output.
*
*        ge  10        same as 5 (but full output later).
*
*        ge  20        constraint status,  x  and  Ax.
*
*        ge  30        diagonals of  T  and  R.
*
*
*  Debug printing is performed depending on the logical variable  LSDBG.
*  LSDBG  is set true when  IDBG  major iterations have been performed.
*  At this point,  printing is done according to a string of binary
*  digits of the form  SVT  (stored in the integer array  ILSDBG).
*
*  S  set 'on'  gives information from the maximum step routine  CMALF.
*  V  set 'on'  gives various vectors in  LSCORE  and its auxiliaries.
*  T  set 'on'  gives a trace of which routine was called and an
*               indication of the progress of the run.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original version written 31-October-1984.
*  This version of LSPRT dated 14-January-1985.
************************************************************************
      COMMON    /SOL1CM/ NOUT

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      CHARACTER*2        LADD, LDEL
      CHARACTER*2        LSTATE(0:5)
      DATA               LSTATE(0), LSTATE(1), LSTATE(2)
     $                  /'  '     , 'L '     , 'U '     /
      DATA               LSTATE(3), LSTATE(4), LSTATE(5)
     $                  /'E '     , 'T '     , 'Z '     /

      IF (MSGLVL .GE. 15) WRITE (NOUT, 1000) PRBTYP, ITER

      IF (MSGLVL .GE. 5) THEN
         IF      (JDEL .GT. 0) THEN
            KDEL =   ISDEL
         ELSE IF (JDEL .LT. 0) THEN
            JDEL = - JDEL
            KDEL =   5
         ELSE
            KDEL =   0
         END IF

         IF (JADD .GT. 0) THEN
            KADD = ISTATE(JADD)
         ELSE
            KADD = 0
         END IF

         LDEL   = LSTATE(KDEL)
         LADD   = LSTATE(KADD)

         IF (NUMINF .GT. 0) THEN
            OBJ    = SUMINF
         ELSE
            OBJ    = SSQ + CTX
         END IF

*        ---------------------------------------------------------------
*        Print the terse line.
*        ---------------------------------------------------------------
         IF (NRANK .EQ. 0) THEN
            IF (PRNT1  .OR.  MSGLVL .GE. 15) WRITE (NOUT, 1100)
            WRITE (NOUT, 1200) ITER, JDEL, LDEL, JADD, LADD,
     $                         ALFA, NUMINF, OBJ, N-NFREE, NACTIV,
     $                         NZ, NZ1, GFNORM, GZ1NRM, CONDT
         ELSE
            IF (PRNT1  .OR.  MSGLVL .GE. 15) WRITE (NOUT, 1110)
            WRITE (NOUT, 1200) ITER, JDEL, LDEL, JADD, LADD,
     $                         ALFA, NUMINF, OBJ, N-NFREE, NACTIV,
     $                         NZ, NZ1, GFNORM, GZ1NRM, CONDT, CONDRZ
         END IF

         IF (MSGLVL .GE. 20) THEN
            WRITE (NOUT, 2000) PRBTYP
            WRITE (NOUT, 2100) (X(J) , ISTATE(J)  ,  J=1,N)
            IF (NCLIN .GT. 0)
     $      WRITE (NOUT, 2200) (AX(K), ISTATE(N+K), K=1,NCLIN )

            IF (MSGLVL .GE. 30) THEN
*              ---------------------------------------------------------
*              Print the diagonals of  T  and  R.
*              ---------------------------------------------------------
               IF (NACTIV .GT. 0) THEN
                  CALL SCOPY ( NACTIV, T(NACTIV,NZ+1), NROWT-1, WORK,1 )
                  WRITE (NOUT, 3000) PRBTYP, (WORK(J), J=1,NACTIV)
               END IF
               IF (NRANK  .GT. 0)
     $            WRITE (NOUT, 3100) PRBTYP, (R(J,J) , J=1,NRANK )
            END IF
            WRITE (NOUT, 5000)
         END IF
      END IF

      PRNT1 = .FALSE.

      RETURN

 1000 FORMAT(/// ' ', A2, ' iteration', I5
     $         / ' =================' )
 1100 FORMAT(// '  Itn Jdel  Jadd      Step',
     $          ' Ninf  Sinf/Objective', '  Bnd', '  Lin', '    Nz',
     $          '   Nz1   Norm Gf  Norm Gz1   Cond T' )
 1110 FORMAT(// '  Itn Jdel  Jadd      Step',
     $          ' Ninf  Sinf/Objective', '  Bnd', '  Lin', '    Nz',
     $          '   Nz1   Norm Gf  Norm Gz1   Cond T Cond Rz1' )
 1200 FORMAT(I5, I5, A1, I5, A1, 1PE9.1, I5, 1X, 1PE15.6, 2I5,
     $       2I6, 1P2E10.2, 1P2E9.1 )
 2000 FORMAT(/ ' Values and status of the ', A2, ' constraints'
     $       / ' ---------------------------------------' )
 2100 FORMAT(/ ' Variables...'                 /   (1X, 5(1PE15.6, I5)))
 2200 FORMAT(/ ' General linear constraints...'/   (1X, 5(1PE15.6, I5)))
 3000 FORMAT(/ ' Diagonals of ' , A2,' working set factor T'/(1P5E15.6))
 3100 FORMAT(/ ' Diagonals of ' , A2, ' triangle R         '/(1P5E15.6))
 5000 FORMAT(/// ' ---------------------------------------------------',
     $           '--------------------------------------------' )

*     End of  LSPRT .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSSETX( LINOBJ, ROWERR, UNITQ,
     $                   NCLIN, NACTIV, NFREE, NRANK, NZ,
     $                   N, NCTOTL, NQ, NROWA, NROWR, NROWT,
     $                   ISTATE, KACTIV, KX,
     $                   JMAX, ERRMAX, CTX, XNORM,
     $                   A, AX, BL, BU, CQ, RES, RES0, FEATOL,
     $                   R, T, X, ZY, P, WORK )

C     IMPLICIT           REAL (A-H,O-Z)
      LOGICAL            LINOBJ, ROWERR, UNITQ
      INTEGER            ISTATE(NCTOTL), KACTIV(N), KX(N)
      REAL               A(NROWA,*), AX(*), BL(NCTOTL), BU(NCTOTL),
     $                   CQ(*), RES(*), RES0(*), FEATOL(NCTOTL), P(N),
     $                   R(NROWR,*), T(NROWT,*), ZY(NQ,*), X(N)
      REAL               WORK(NCTOTL)

************************************************************************
*  LSSETX  computes the point on a working set that is closest to the
*  input vector  x  (in the least-squares sense).  The norm of  x, the
*  transformed residual vector  Pr - RQ'x,  and the constraint values
*  Ax  are also initialized.
*
*  If the computed point gives a row error of more than the feasibility
*  tolerance, an extra step of iterative refinement is used.  If  x  is
*  still infeasible,  the logical variable  ROWERR  is set.
*
*  Systems Optimization Laboratory, Stanford University.
*  Original version written 31-October-1984.
*  This version dated 29-December-1985.
************************************************************************
      COMMON    /SOL1CM/ NOUT

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG

      EXTERNAL           ISAMAX, SDOT
      INTRINSIC          ABS, MIN
      PARAMETER        ( NTRY  = 2 )
      PARAMETER        ( ZERO  = 0.0E+0, ONE = 1.0E+0 )

*     ------------------------------------------------------------------
*     Move  x  onto the simple bounds in the working set.
*     ------------------------------------------------------------------
      DO 100 K = NFREE+1, N
          J   = KX(K)
          IS  = ISTATE(J)
          BND = BL(J)
          IF (IS .GE. 2) BND  = BU(J)
          IF (IS .NE. 4) X(J) = BND
  100 CONTINUE

*     ------------------------------------------------------------------
*     Move  x  onto the general constraints in the working set.
*     We shall make  ntry  tries at getting acceptable row errors.
*     ------------------------------------------------------------------
      KTRY   = 1
      JMAX   = 1
      ERRMAX = ZERO

*     REPEAT
  200    IF (NACTIV .GT. 0) THEN

*           Set  work = residuals for constraints in the working set.
*           Solve for p, the smallest correction to x that gives a point
*           on the constraints in the working set.  Define  p = Y*(py),
*           where  py  solves the triangular system  T*(py) = residuals.

            DO 220 I = 1, NACTIV
               K   = KACTIV(I)
               J   = N + K
               BND = BL(J)
               IF (ISTATE(J) .EQ. 2) BND = BU(J)
               WORK(I) = BND - SDOT  ( N, A(K,1), NROWA, X, 1 )
  220       CONTINUE

            CALL CMTSOL( 1, NROWT, NACTIV, T(1,NZ+1), WORK )
            CALL SLOAD ( N, ZERO, P, 1 )
            CALL SCOPY ( NACTIV, WORK, 1, P(NZ+1), 1 )

            CALL CMQMUL( 2, N, NZ, NFREE, NQ, UNITQ, KX, P, ZY, WORK )
            CALL SAXPY ( N, ONE, P, 1, X, 1 )
         END IF

*        ---------------------------------------------------------------
*        Compute the 2-norm of  x.
*        Initialize  Ax  for all the general constraints.
*        ---------------------------------------------------------------
         XNORM  = SNRM2 ( N, X, 1 )
         IF (NCLIN .GT. 0)
     $      CALL SGEMV ( 'N', NCLIN, N, ONE, A, NROWA,
     $                   X, 1, ZERO, AX, 1 )

*        ---------------------------------------------------------------
*        Check the row residuals.
*        ---------------------------------------------------------------
         IF (NACTIV .GT. 0) THEN
            DO 300 K = 1, NACTIV
               I   = KACTIV(K)
               J   = N + I
               IS  = ISTATE(J)
               IF (IS .EQ. 1) WORK(K) = BL(J) - AX(I)
               IF (IS .GE. 2) WORK(K) = BU(J) - AX(I)
  300       CONTINUE

            JMAX   = ISAMAX( NACTIV, WORK, 1 )
            ERRMAX = ABS( WORK(JMAX) )
         END IF

         KTRY = KTRY + 1
*     UNTIL    (ERRMAX .LE. FEATOL(JMAX) .OR. KTRY .GT. NTRY
      IF (.NOT.(ERRMAX .LE. FEATOL(JMAX) .OR. KTRY .GT. NTRY)) GO TO 200

      ROWERR = ERRMAX .GT. FEATOL(JMAX)

*     ==================================================================
*     Compute the linear objective value  c'x  and the transformed
*     residual  Pr  -  RQ'x = RES0  -  RQ'x.
*     ==================================================================
      IF (NRANK .GT. 0  .OR.  LINOBJ) THEN
         CALL SCOPY ( N, X, 1, P, 1 )
         CALL CMQMUL( 6, N, NZ, NFREE, NQ, UNITQ, KX, P, ZY, WORK )
      END IF

      CTX = ZERO
      IF (LINOBJ)
     $   CTX = SDOT  ( N, CQ, 1, P, 1 )

      IF (NRANK .GT. 0) THEN

         CALL STRMV ( 'U', 'N', 'N', NRANK, R, NROWR, P, 1 )
         IF (NRANK .LT. N)
     $      CALL SGEMV ( 'N', NRANK, N-NRANK, ONE, R(1,NRANK+1), NROWR,
     $                   P(NRANK+1), 1, ONE, P, 1 )

         CALL SCOPY ( NRANK,       RES0, 1, RES, 1 )
         CALL SAXPY ( NRANK, -ONE, P   , 1, RES, 1 )

      END IF

      IF (LSDBG  .AND.  ILSDBG(2) .GT. 0)
     $   WRITE (NOUT, 2200) (X(J), J = 1, N)

      RETURN

 2200 FORMAT(/ ' //LSSETX// Variables after refinement ... '/ (5G12.3))

*     End of  LSSETX.

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE LSSOL ( MM, N,
     $                   NCLIN, NROWA, NROWR,
     $                   A, BL, BU, CVEC,
     $                   ISTATE, KX, X, R, B,
     $                   INFORM, ITER, OBJ, CLAMDA,
     $                   IW, LENIW, W, LENW )

C     IMPLICIT           REAL(A-H,O-Z)
      INTEGER            LENIW, LENW
      INTEGER            ISTATE(N+NCLIN), KX(N)
      INTEGER            IW(LENIW)
      REAL               BL(N+NCLIN), BU(N+NCLIN), A(NROWA,*)
      REAL               CLAMDA(N+NCLIN), CVEC(*)
      REAL               R(NROWR,*), X(N), B(*)
      REAL               W(LENW)

************************************************************************
*  LSSOL  solves problems of the form
*
*           Minimize               F(x)
*              x
*                                 (  x )
*           subject to    bl  .le.(    ).ge.  bu,
*                                 ( Ax )
*
*  where  '  denotes the transpose of a column vector,  x  denotes the
*  n-vector of parameters and  F(x) is one of the following functions..
*
*  FP =  None                         (find a feasible point).
*  LP =  c'x
*  QP1=        1/2 x'Rx                R  n times n, symmetric pos. def.
*  QP2=  c'x + 1/2 x'Rx                .  .   ..        ..       ..  ..
*  QP3=        1/2 x'R'Rx              R  m times n, upper triangular.
*  QP4=  c'x + 1/2 x'R'Rx              .  .   ..  .   ..      ...
*  LS1=        1/2 (b - Rx)'(b - Rx)   R  m times n, rectangular.
*  LS2=  c'x + 1/2 (b - Rx)'(b - Rx)   .  .   ..  .     ...
*  LS3=        1/2 (b - Rx)'(b - Rx)   R  m times n, upper triangular.
*  LS4=  c'x + 1/2 (b - Rx)'(b - Rx)   .  .   ..  .   ..      ...
*
*  The matrix  R  is entered as the two-dimensional array  R  (of row
*  dimension  NROWR).  If  NROWR = 0,  R  is not accessed.
*
*  The vector  c  is entered in the one-dimensional array  CVEC.
*
*  NCLIN  is the number of general linear constraints (rows of  A).
*  (NCLIN may be zero.)
*
*  The first  N  components of  BL  and   BU  are lower and upper
*  bounds on the variables.  The next  NCLIN  components are
*  lower and upper bounds on the general linear constraints.
*
*  The matrix  A  of coefficients in the general linear constraints
*  is entered as the two-dimensional array  A  (of dimension
*  NROWA by N).  If NCLIN = 0, A is not accessed.
*
*  The vector  x  must contain an initial estimate of the solution,
*  and will contain the computed solution on output.
*
*
*  Complete documentation for  LSSOL  is contained in Report SOL 86-1,
*  Users Guide for LSSOL (Version 1.0), by P.E. Gill, S. J. Hammarling,
*  W. Murray, M.A. Saunders and M.H. Wright, Department of
*  Operations Research, Stanford University, Stanford, California 94305.
*
*  Systems Optimization Laboratory, Stanford University.
*  Version 1.01 Dated  30-June-1986.
*
*  Copyright  1984  Stanford University.
*
*  This material may be reproduced by or for the U.S. Government pursu-
*  ant to the copyright license under DAR clause 7-104.9(a) (1979 Mar).
*
*  This material is based upon work partially supported by the National
*  Science Foundation under Grants MCS-7926009 and ECS-8312142; the
*  Department of Energy Contract AM03-76SF00326, PA No. DE-AT03-
*  76ER72018; the Army Research Office Contract DAA29-84-K-0156;
*  and the Office of Naval Research Grant N00014-75-C-0267.
************************************************************************
      REAL               WMACH
      COMMON    /SOLMCH/ WMACH(15)
      SAVE      /SOLMCH/
      COMMON    /SOL1CM/ NOUT
      COMMON    /SOL3CM/ LENNAM, NROWT, NCOLT, NQ
      COMMON    /SOL4CM/ EPSPT3, EPSPT5, EPSPT8, EPSPT9
      COMMON    /SOL5CM/ ASIZE, DTMAX, DTMIN

      PARAMETER         (LENLS = 20)
      COMMON    /SOL1LS/ LOCLS(LENLS)

      LOGICAL            LSDBG
      PARAMETER         (LDBG = 5)
      COMMON    /LSDEBG/ ILSDBG(LDBG), LSDBG
*-----------------------------------------------------------------------
      PARAMETER         (MXPARM = 30)
      INTEGER            IPRMLS(MXPARM), IPSVLS
      REAL               RPRMLS(MXPARM), RPSVLS

      COMMON    /LSPAR1/ IPSVLS(MXPARM),
     $                   IDBGLS, ITMAX1, ITMAX2, LCRASH, LDBGLS, LPROB ,
     $                   MSGLS , NN    , NNCLIN, NPROB , IPADLS(20)

      COMMON    /LSPAR2/ RPSVLS(MXPARM),
     $                   BIGBND, BIGDX , BNDLOW, BNDUPP, TOLACT, TOLFEA,
     $                   TOLRNK, RPADLS(23)

      EQUIVALENCE       (IPRMLS(1), IDBGLS), (RPRMLS(1), BIGBND)

      SAVE      /LSPAR1/, /LSPAR2/
*-----------------------------------------------------------------------
      EQUIVALENCE   (MSGLS , MSGLVL), (IDBGLS, IDBG), (LDBGLS, MSGDBG)

      INTRINSIC          MAX, MIN

*     Local variables.

      LOGICAL            COLD  , FACTRZ, LINOBJ, NAMED , ROWERR,
     $                   UNITQ , VERTEX
      CHARACTER*2        PRBTYP
      CHARACTER*8        NAMES(1)
      PARAMETER        ( ZERO   =0.0E+0, POINT1 =0.1E+0, POINT3 =3.3E-1)
      PARAMETER        ( POINT8 =0.8E+0, POINT9 =0.9E+0, ONE    =1.0E+0)

      CHARACTER*40       TITLE
      DATA               TITLE
     $                 / 'SOL/LSSOL  ---  Version 1.01   June 1986' /

*     Set the machine-dependent constants.

      CALL MCHPAR()

      EPSMCH = WMACH( 3)
      RTEPS  = WMACH( 4)
      NOUT   = WMACH(11)

      EPSPT3 = EPSMCH**POINT3
      EPSPT5 = RTEPS
      EPSPT8 = EPSMCH**POINT8
      EPSPT9 = EPSMCH**POINT9

      NAMED  = .FALSE.

      INFORM = 0
      ITER   = 0

      CONDMX = ONE / EPSPT5

      NCTOTL = N + NCLIN

*     Set the default values of the parameters.

      CALL LSDFLT( MM, N, NCLIN, TITLE )

*     Set all parameters determined by the problem type.

      IF      (LPROB .EQ. 1 ) THEN
         PRBTYP    = 'FP'
         M      = 0
         LINOBJ = .FALSE.
         FACTRZ = .TRUE.
      ELSE IF (LPROB .EQ. 2 ) THEN
         PRBTYP    = 'LP'
         M      = 0
         LINOBJ = .TRUE.
         FACTRZ = .TRUE.
      ELSE IF (LPROB .EQ. 3 ) THEN
         PRBTYP    = 'QP'
         M      = MM
         LINOBJ = .FALSE.
         FACTRZ = .TRUE.
      ELSE IF (LPROB .EQ. 4 ) THEN
         PRBTYP    = 'QP'
         M      = MM
         LINOBJ = .TRUE.
         FACTRZ = .TRUE.
      ELSE IF (LPROB .EQ. 5 ) THEN
         PRBTYP    = 'QP'
         M      = MM
         LINOBJ = .FALSE.
         FACTRZ = .FALSE.
      ELSE IF (LPROB .EQ. 6 ) THEN
         PRBTYP    = 'QP'
         M      = MM
         LINOBJ = .TRUE.
         FACTRZ = .FALSE.
      ELSE IF (LPROB .EQ. 7 ) THEN
         PRBTYP    = 'LS'
         M      = MM
         LINOBJ = .FALSE.
         FACTRZ = .TRUE.
      ELSE IF (LPROB .EQ. 8 ) THEN
         PRBTYP    = 'LS'
         M      = MM
         LINOBJ = .TRUE.
         FACTRZ = .TRUE.
      ELSE IF (LPROB .EQ. 9 ) THEN
         PRBTYP    = 'LS'
         M      = MM
         LINOBJ = .FALSE.
         FACTRZ = .FALSE.
      ELSE IF (LPROB .EQ. 10) THEN
         PRBTYP    = 'LS'
         M      = MM
         LINOBJ = .TRUE.
         FACTRZ = .FALSE.
      END IF

*     Assign the dimensions of arrays in the parameter list of LSCORE.
*     Economies of storage are possible if the minimum number of active
*     constraints and the minimum number of fixed variables are known in
*     advance.  The expert user should alter MINACT and MINFXD
*     accordingly.
*     If a linear program is being solved and the matrix of general
*     constraints is fat,  i.e.,  NCLIN .LT. N,  a non-zero value is
*     known for MINFXD.  Note that in this case, VERTEX must be
*     set  .TRUE..

      MINACT = 0
      MINFXD = 0

      VERTEX = .FALSE.
      IF (      (PRBTYP .EQ. 'LP'  .OR.  PRBTYP .EQ. 'FP')
     $    .AND.  NCLIN  .LT. N   ) THEN
         MINFXD = N - NCLIN - 1
         VERTEX = .TRUE.
      END IF

      MXFREE = N - MINFXD
      MAXACT = MAX( 1, MIN( N, NCLIN ) )
      MAXNZ  = N - ( MINFXD + MINACT )

      IF (NCLIN .EQ. 0) THEN
         NQ     = 1
         NROWT  = 1
         NCOLT  = 1
         VERTEX = .FALSE.
      ELSE
         NQ     = MAX( 1, MXFREE )
         NROWT  = MAX( MAXNZ, MAXACT )
         NCOLT  = MXFREE
      END IF

      NCNLN  = 0
      LENNAM = 1

*     Allocate certain arrays that are not done in LSLOC.

      LITOTL = 0

      LAX    = 1
      LWTOTL = LAX + NCLIN  - 1

*     Allocate remaining work arrays.

      CALL LSLOC ( LPROB, N, NCLIN, LITOTL, LWTOTL )

      COLD  = LCRASH .EQ. 0

*     Check input parameters and storage limits.

      CALL CMCHK ( NERROR, MSGLVL, COLD, (.NOT.FACTRZ),
     $             LENIW, LENW, LITOTL, LWTOTL,
     $             N, NCLIN, NCNLN,
     $             ISTATE, KX, NAMED, NAMES, LENNAM,
     $             BL, BU, X )

      IF (NERROR .GT. 0) THEN
         INFORM = 6
         GO TO 800
      END IF

      LKACTV = LOCLS( 1)

      LANORM = LOCLS( 2)
      LPX    = LOCLS( 4)
      LRES   = LOCLS( 5)
      LRES0  = LOCLS( 6)
      LGQ    = LOCLS( 8)
      LCQ    = LOCLS( 9)
      LRLAM  = LOCLS(10)
      LT     = LOCLS(11)
      LZY    = LOCLS(12)
      LWTINF = LOCLS(13)
      LWRK   = LOCLS(14)
      LFEATL = LOCLS(15)

      IF (TOLFEA .GT. ZERO)
     $   CALL SLOAD ( N+NCLIN, (TOLFEA), W(LFEATL), 1 )

      IANRMJ = LANORM
      DO 200 J = 1, NCLIN
         W(IANRMJ) = SNRM2 ( N, A(J,1), NROWA )
         IANRMJ    = IANRMJ + 1
  200 CONTINUE
      IF (NCLIN .GT. 0)
     $   CALL SCOND ( NCLIN, W(LANORM), 1, ASIZE, AMIN )

      CALL SCOND ( NCTOTL, W(LFEATL), 1, FEAMAX, FEAMIN )
      CALL SCOPY ( NCTOTL, W(LFEATL), 1, W(LWTINF), 1 )
      CALL SSCAL ( NCTOTL, (ONE/FEAMIN), W(LWTINF), 1 )

      SSQ1   = ZERO

      IF (FACTRZ) THEN
*        ===============================================================
*        Factorize R using QR or Cholesky.  KX must be initialized.
*        ===============================================================
         DO 210 I = 1, N
            KX(I) = I
  210    CONTINUE

         IF      (PRBTYP .EQ. 'LP'  .OR.  PRBTYP .EQ. 'FP') THEN
            NRANK = 0
         ELSE IF (PRBTYP .EQ. 'QP') THEN
*           ------------------------------------------------------------
*           Compute the Cholesky factorization of R.  The Hessian is
*           M times M and resides in the upper left-hand corner of R.
*           ------------------------------------------------------------
            DO 220 J = M+1, N
               CALL SLOAD ( M, (ZERO), R(1,J), 1 )
  220       CONTINUE

            CALL LSCHOL( NROWR, M, NRANK, TOLRNK, KX, R, INFO )

            IF (NRANK .GT. 0)
     $         CALL SLOAD ( NRANK, (ZERO), W(LRES0), 1 )

         ELSE IF (PRBTYP .EQ. 'LS') THEN
*           ------------------------------------------------------------
*           Compute the orthogonal factorization PRQ = ( U ),  where P
*                                                      ( 0 )
*           is an orthogonal matrix and Q is a permutation matrix.
*           Overwrite R with the upper-triangle U.  The orthogonal
*           matrix P is applied to the residual and discarded.  The
*           permutation is stored in the array KX.  Once U has been
*           computed we need only work with vectors of length N within
*           LSCORE.  However, it is necessary to store the sum of
*           squares of the terms  B(NRANK+1),...,B(M),  where B = Pr.
*           ------------------------------------------------------------
            CALL SGEQRP( 'Column iterchanges', M, N, R, NROWR,
     $                   W(LWRK), IW(LKACTV), W(LGQ), INFO )

            LJ  = LKACTV
            DO 230 J = 1, N
               JMAX = IW(LJ)
               IF (JMAX .GT. J) THEN
                  JSAVE    = KX(JMAX)
                  KX(JMAX) = KX(J)
                  KX(J)    = JSAVE
               END IF
               LJ = LJ + 1
  230       CONTINUE

            CALL SGEAPQ( 'Transpose', 'Separate', M, N, R, NROWR,
     $                   W(LWRK), 1, B, M, W(LGQ), INFO )

            NRANK = ISRANK( MIN(N, M), R, NROWR+1, TOLRNK )

            IF (M .GT. NRANK) SSQ1 = SNRM2 ( M-NRANK, B(NRANK+1), 1 )

            IF (NRANK .GT. 0)
     $         CALL SCOPY ( NRANK, B, 1, W(LRES0), 1 )
         END IF
      ELSE
*        ===============================================================
*        R is input as an upper-triangular matrix with M rows.
*        ===============================================================
         NRANK = M
         IF (NRANK .GT. 0) THEN
            IF      (PRBTYP .EQ. 'QP') THEN
               CALL SLOAD ( NRANK, (ZERO), W(LRES0), 1 )
            ELSE IF (PRBTYP .EQ. 'LS') THEN
               CALL SCOPY ( NRANK, B, 1, W(LRES0), 1 )
            END IF
         END IF
      END IF

      IF (       MSGLVL .GT. 0     .AND.  NRANK  .LT. N
     $    .AND.  PRBTYP .NE. 'LP'  .AND.  PRBTYP .NE. 'FP')
     $   WRITE (NOUT, 9000) NRANK

*     ------------------------------------------------------------------
*     Find an initial working set.
*     ------------------------------------------------------------------
      CALL LSCRSH( COLD, VERTEX,
     $             NCLIN, NCTOTL, NACTIV, NARTIF,
     $             NFREE, N, NROWA,
     $             ISTATE, IW(LKACTV),
     $             BIGBND, TOLACT,
     $             A, W(LAX), BL, BU, X, W(LGQ), W(LWRK) )

*     ------------------------------------------------------------------
*     Compute the TQ factorization of the constraints while keeping R in
*     upper-triangular form.  Transformations associated with Q are
*     applied to CQ.  Transformations associated with P are applied to
*     RES0.  If some simple bounds are in the working set,  KX is
*     re-ordered so that the free variables come first.
*     ------------------------------------------------------------------
*     First, add the bounds. To save a bit of work, CQ is not loaded
*     until after KX has been re-ordered.

      NGQ   = 0
      NRES  = 0
      IF (NRANK .GT. 0) NRES = 1
      UNITQ = .TRUE.

      CALL LSBNDS( UNITQ,
     $             INFORM, NZ, NFREE, NRANK, NRES, NGQ,
     $             N, NQ, NROWA, NROWR, NROWT,
     $             ISTATE, KX,
     $             CONDMX,
     $             A, R, W(LT), W(LRES0), W(LCQ),
     $             W(LZY), W(LGQ), W(LWRK) )

      IF (LINOBJ) THEN

*        Install the transformed linear term in CQ.
*        CMQMUL applies the permutations in KX to CVEC.

         NGQ = 1
         CALL SCOPY ( N, CVEC, 1, W(LCQ), 1 )
         CALL CMQMUL( 6, N, NZ, NFREE, NQ, UNITQ,
     $                KX, W(LCQ), W(LZY), W(LWRK) )
      END IF

      IF (NACTIV .GT. 0) THEN
         NACT1  = NACTIV
         NACTIV = 0

         CALL LSADDS( UNITQ, VERTEX,
     $                INFORM, 1, NACT1, NACTIV, NARTIF, NZ, NFREE,
     $                NRANK, NREJTD, NRES, NGQ,
     $                N, NQ, NROWA, NROWR, NROWT,
     $                ISTATE, IW(LKACTV), KX,
     $                CONDMX,
     $                A, R, W(LT), W(LRES0), W(LCQ),
     $                W(LZY), W(LGQ), W(LWRK) )
      END IF

*     ------------------------------------------------------------------
*     Move the initial  x  onto the constraints in the working set.
*     Compute the transformed residual vector  Pr = Pb - RQ'x.
*     ------------------------------------------------------------------
      CALL LSSETX( LINOBJ, ROWERR, UNITQ,
     $             NCLIN, NACTIV, NFREE, NRANK, NZ,
     $             N, NCTOTL, NQ, NROWA, NROWR, NROWT,
     $             ISTATE, IW(LKACTV), KX,
     $             JMAX, ERRMAX, CTX, XNORM,
     $             A, W(LAX), BL, BU, W(LCQ), W(LRES), W(LRES0),
     $             W(LFEATL), R, W(LT), X, W(LZY), W(LPX), W(LWRK) )

      JINF = 0

      CALL LSCORE( PRBTYP, NAMED, NAMES, LINOBJ, UNITQ,
     $             INFORM, ITER, JINF, NCLIN, NCTOTL,
     $             NACTIV, NFREE, NRANK, NZ, NZ1,
     $             N, NROWA, NROWR,
     $             ISTATE, IW(LKACTV), KX,
     $             CTX, OBJ, SSQ1,
     $             SUMINF, NUMINF, XNORM,
     $             BL, BU, A, CLAMDA, W(LAX),
     $             W(LFEATL), R, X, IW, W )

      OBJ    = OBJ    + CTX
      IF (PRBTYP .EQ. 'LS'  .AND.  NRANK .GT. 0)
     $   CALL SCOPY ( NRANK, W(LRES), 1, B, 1 )

*     ==================================================================
*     Print messages if required.
*     ==================================================================
  800 IF (MSGLVL .GT.   0) THEN
         IF (INFORM .EQ.   0) THEN
            IF (PRBTYP .EQ. 'FP') THEN
               WRITE (NOUT, 2001)
            ELSE
               WRITE (NOUT, 2002) PRBTYP
            END IF
         END IF
         IF (INFORM .EQ.   1) WRITE (NOUT, 2010) PRBTYP
         IF (INFORM .EQ.   2) WRITE (NOUT, 2020) PRBTYP
         IF (INFORM .EQ.   3) WRITE (NOUT, 2030)
         IF (INFORM .EQ.   4) WRITE (NOUT, 2040)
         IF (INFORM .EQ.   5) WRITE (NOUT, 2050)
         IF (INFORM .EQ.   6) WRITE (NOUT, 2060) NERROR

         IF (INFORM .LT.   6) THEN
            IF      (NUMINF .EQ. 0) THEN
                IF (PRBTYP .NE. 'FP') WRITE (NOUT, 3000) PRBTYP, OBJ
            ELSE IF (INFORM .EQ. 3) THEN
               WRITE (NOUT, 3010) SUMINF
            ELSE
               WRITE (NOUT, 3020) SUMINF
            END IF
            IF (NUMINF .GT. 0) OBJ = SUMINF
         END IF
      END IF

*     Recover the optional parameters set by the user.

      CALL IYPOC ( MXPARM, IPSVLS, 1, IPRMLS, 1 )
      CALL SCOPY ( MXPARM, RPSVLS, 1, RPRMLS, 1 )

      RETURN

 2001 FORMAT(/ ' Exit LSSOL - Feasible point found.     ')
 2002 FORMAT(/ ' Exit LSSOL - Optimal ', A2, ' solution.')
 2010 FORMAT(/ ' Exit LSSOL - Weak ',    A2, ' solution.')
 2020 FORMAT(/ ' Exit LSSOL - ', A2,         ' solution is unbounded.' )
 2030 FORMAT(/ ' Exit LSSOL - Cannot satisfy the linear constraints. ' )
 2040 FORMAT(/ ' Exit LSSOL - Too many iterations.')
 2050 FORMAT(/ ' Exit LSSOL - Too many iterations without changing X.' )
 2060 FORMAT(/ ' Exit LSSOL - ', I10, ' errors found in the input',
     $         ' parameters.  Problem abandoned.'         )
 3000 FORMAT(/ ' Final ', A2, ' objective value =', G16.7 )
 3010 FORMAT(/ ' Minimum sum of infeasibilities =', G16.7 )
 3020 FORMAT(/ ' Final sum of infeasibilities =',   G16.7 )

 9000 FORMAT(/ ' Rank of the objective function data matrix = ', I5 )

*     End of  LSSOL .

      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*     File  OPSUBS FORTRAN
*
*     OPFILE   OPLOOK   OPNUMB   OPSCAN   OPTOKN   OPUPPR
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SUBROUTINE OPFILE( IOPTNS, NOUT, INFORM, OPKEY )
      INTEGER            IOPTNS, NOUT, INFORM
      EXTERNAL           OPKEY

************************************************************************
*     OPFILE  reads the options file from unit  IOPTNS  and loads the
*     options into the relevant elements of the integer and real
*     parameter arrays.
*
*     Systems Optimization Laboratory, Stanford University.
*     This version dated December 18, 1985.
************************************************************************
      LOGICAL             PRNT
      CHARACTER*16        KEY   , TOKEN(1)
      CHARACTER*72        BUFFER, OLDBUF

      PRNT   = .TRUE.

*     Return if the unit number is out of range.

      IF (IOPTNS .LT. 0  .OR.  IOPTNS .GT. 99) THEN
         INFORM = 1
         RETURN
      END IF

*     ------------------------------------------------------------------
*     Look for  BEGIN, ENDRUN  or  SKIP.
*     ------------------------------------------------------------------
      NREAD  = 0
   50    READ (IOPTNS, '(A)', END = 930) BUFFER
         NREAD = NREAD + 1
         NKEY  = 1
         CALL OPTOKN( BUFFER, NKEY, TOKEN )
         KEY   = TOKEN(1)
         IF (KEY .EQ. 'ENDRUN') GO TO 940
         IF (KEY .NE. 'BEGIN' ) THEN
            IF (NREAD .EQ. 1  .AND.  KEY .NE. 'SKIP') THEN
               WRITE (NOUT, 2000) IOPTNS, BUFFER
            END IF
            GO TO 50
         END IF

*     ------------------------------------------------------------------
*     BEGIN found.
*     This is taken to be the first line of an OPTIONS file.
*     Read the second line to see if it is NOLIST.
*     ------------------------------------------------------------------
      OLDBUF = BUFFER
      READ (IOPTNS, '(A)', END = 920) BUFFER

      CALL OPKEY ( NOUT, BUFFER, KEY )

      IF (KEY .EQ. 'NOLIST') THEN
         PRNT   = .FALSE.
      END IF

      IF (PRNT) THEN
         WRITE (NOUT, '(// A / A /)')
     $      ' OPTIONS file',
     $      ' ------------'
         WRITE (NOUT, '(6X, A )') OLDBUF, BUFFER
      END IF

*     ------------------------------------------------------------------
*     Read the rest of the file.
*     ------------------------------------------------------------------
*+    while (key .ne. 'end') loop
  100 IF    (KEY .NE. 'END') THEN
         READ (IOPTNS, '(A)', END = 920) BUFFER
         IF (PRNT)
     $      WRITE (NOUT, '( 6X, A )') BUFFER

         CALL OPKEY ( NOUT, BUFFER, KEY )

         IF (KEY .EQ.   'LIST') PRNT = .TRUE.
         IF (KEY .EQ. 'NOLIST') PRNT = .FALSE.
         GO TO 100
      END IF
*+    end while

      INFORM =  0
      RETURN

  920 WRITE (NOUT, 2200) IOPTNS
      INFORM = 2
      RETURN

  930 WRITE (NOUT, 2300) IOPTNS
      INFORM = 3
      RETURN

  940 WRITE (NOUT, '(// 6X, A)') BUFFER
      INFORM = 4
      RETURN

 2000 FORMAT(
     $ //' XXX  Error while looking for an OPTIONS file on unit', I7
     $ / ' XXX  The file should start with BEGIN, SKIP or ENDRUN'
     $ / ' XXX  but the first record found was the following:'
     $ //' ---->', A
     $ //' XXX  Continuing to look for OPTIONS file...')
 2200 FORMAT(//' XXX  End-of-file encountered while processing',
     $         ' an OPTIONS file on unit', I6)
 2300 FORMAT(//' XXX  End-of-file encountered while looking for',
     $         ' an OPTIONS file on unit', I6)

*     End of  OPFILE.

      END
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
      SUBROUTINE OPLOOK (NDICT, DICTRY, ALPHA, KEY, ENTRY)
C
C
C Description and usage:
C
C       Performs dictionary lookups.  A pointer is returned if a
C    match is found between the input key and the corresponding
C    initial characters of one of the elements of the dictionary.
C    If a "synonym" has been provided for an entry, the search is
C    continued until a match to a primary dictionary entry is found.
C    Cases of no match, or multiple matches, are also provided for.
C
C     Dictionary entries must be left-justified, and may be alphabetized
C    for faster searches.  Secondary entries, if any, are composed of
C    two words separated by one or more characters such as blank, tab,
C    comma, colon, or equal sign which are treated as non-significant
C    by OPSCAN.  The first entry of each such pair serves as a synonym
C    for the second, more fundamental keyword.
C
C       The ordered search stops after the section of the dictionary
C    having the same first letters as the key has been checked, or
C    after a specified number of entries have been examined.  A special
C    dictionary entry, the vertical bar '|', will also terminate the
C    search.  This will speed things up if an appropriate dictionary
C    length parameter cannot be determined.  Both types of search are
C    sequential.  See "Notes" below for some suggestions if efficiency
C    is an issue.
C
C
C Parameters:
C
C    Name    Dimension  Type  I/O/S  Description
C    NDICT               I    I      Number of dictionary entries to be
C                                    examined.
C    DICTRY  NDICT       C    I      Array of dictionary entries,
C                                    left-justified in their fields.
C                                    May be alphabetized for efficiency,
C                                    in which case ALPHA should be
C                                    .TRUE.  Entries with synonyms are
C                                    of the form
C                                    'ENTRY : SYNONYM', where 'SYNONYM'
C                                    is a more fundamental entry in the
C                                    same dictionary.  NOTE: Don't build
C                                    "circular" dictionaries!
C    ALPHA               L    I      Indicates whether the dictionary
C                                    is in alphabetical order, in which
C                                    case the search can be terminated
C                                    sooner.
C    KEY                 C    I/O    String to be compared against the
C                                    dictionary.  Abbreviations are OK
C                                    if they correspond to a unique
C                                    entry in the dictionary.  KEY is
C                                    replaced on termination by its most
C                                    fundamental equivalent dictionary
C                                    entry (uppercase, left-justified)
C                                    if a match was found.
C    ENTRY               I      O    Dictionary pointer.  If > 0, it
C                                    indicates which entry matched KEY.
C                                    In case of trouble, a negative
C                                    value means that a UNIQUE match
C                                    was not found - the absolute value
C                                    of ENTRY points to the second
C                                    dictionary entry that matched KEY.
C                                    Zero means that NO match could be
C                                    found.  ENTRY always refers to the
C                                    last search performed -
C                                    in searching a chain of synonyms,
C                                    a non-positive value will be
C                                    returned if there is any break,
C                                    even if the original input key
C                                    was found.
C
C
C External references:
C
C    Name    Description
C    OPSCAN  Finds first and last significant characters.
C
C
C Environment:  Digital VAX-11/780 VMS FORTRAN (FORTRAN 77).
C               Appears to satisfy the ANSI Fortran 77 standard.
C
C
C Notes:
C
C    (1)  IMPLICIT NONE is non-standard.  (Has been commented out.)
C
C    (2)  We have assumed that the dictionary is not too big.  If
C         many searches are to be done or if the dictionary has more
C         than a dozen or so entries, it may be advantageous to build
C         an index array of pointers to the beginning of the section
C         of the dictionary containing each letter, then pass in the
C         portion of the dictionary beginning with DICTRY (INDEX).
C         (This won't generally work for dictionaries with synonyms.)
C         For very large problems, a completely different approach may
C         be advisable, e.g. a binary search for ordered dictionaries.
C
C    (3)  OPLOOK is case sensitive.  In most applications it will be
C         necessary to use an uppercase dictionary, and to convert the
C         input key to uppercase before calling OPLOOK.  Companion
C         routines OPTOKN and PAIRS, available from the author, already
C         take care of this.
C
C    (4)  The key need not be left-justified.  Any leading (or
C         trailing) characters which are "non-significant" to OPSCAN
C         will be ignored.  These include blanks, horizontal tabs,
C         commas, colons, and equal signs.  See OPSCAN for details.
C
C    (5)  The ASCII collating sequence for character data is assumed.
C         (N.B. This means the numerals precede the alphabet, unlike
C         common practice!)  This should not cause trouble on EBCDIC
C         machines if DICTRY just contains alphabetic keywords.
C         Otherwise it may be necessary to use the FORTRAN lexical
C         library routines to force use of the ASCII sequence.
C
C    (6)  Parameter NUMSIG sets a limit on the length of significant
C         dictionary entries.  Special applications may require that
C         this be increased.  (It is 16 in the present version.)
C
C    (7)  No protection against "circular" dictionaries is provided:
C         don't claim that A is B, and that B is A.  All synonym chains
C         must terminate!  Other potential errors not checked for
C         include duplicate or mis-ordered entries.
C
C    (8)  The handling of ambiguities introduces some ambiguity:
C
C            ALPHA = .TRUE.  A potential problem, when one entry
C                            looks like an abbreviation for another
C                            (eg. does 'A' match 'A' or 'AB'?) was
C                            resolved by dropping out of the search
C                            immediately when an "exact" match is found.
C
C            ALPHA = .FALSE. The programmer must ensure that the above
C                            situation does not arise: each dictionary
C                            entry must be recognizable, at least when
C                            specified to full length.  Otherwise, the
C                            result of a search will depend on the
C                            order of entries.
C
C
C Author:  Robert Kennelly, Informatics General Corporation.
C
C
C Development history:
C
C    24 Feb. 1984  RAK/DAS  Initial design and coding.
C    25 Feb. 1984    RAK    Combined the two searches by suitable
C                           choice of terminator FLAG.
C    28 Feb. 1984    RAK    Optional synonyms in dictionary, no
C                           longer update KEY.
C    29 Mar. 1984    RAK    Put back replacement of KEY by its
C                           corresponding entry.
C    21 June 1984    RAK    Corrected bug in error handling for cases
C                           where no match was found.
C    23 Apr. 1985    RAK    Introduced test for exact matches, which
C                           permits use of dictionary entries which
C                           would appear to be ambiguous (for ordered
C                           case).  Return -I to point to the entry
C                           which appeared ambiguous (had been -1).
C                           Repaired loop termination - had to use
C                           equal length strings or risk quitting too
C                           soon when one entry is an abbreviation
C                           for another.  Eliminated HIT, reduced
C                           NUMSIG to 16.
C    15 Nov. 1985    MAS    Loop 20 now tests .LT. FLAG, not .LE. FLAG.
C                           If ALPHA is false, FLAG is now '|', not '{'.
C    26 Jan. 1986    PEG    Declaration of FLAG and TARGET modified to
C                           conform to ANSI-77 standard.
C-----------------------------------------------------------------------


C     Variable declarations.
C     ----------------------

*     IMPLICIT NONE

C     Parameters.

      INTEGER
     $   NUMSIG
      CHARACTER
     $   BLANK, VBAR
      PARAMETER
     $   (BLANK = ' ', VBAR = '|', NUMSIG = 16)

C     Variables.

      LOGICAL
     $   ALPHA
      INTEGER
     $   ENTRY, FIRST, I, LAST, LENGTH, MARK, NDICT
*     CHARACTER
*    $   DICTRY (NDICT) * (*), FLAG * (NUMSIG),
*    $   KEY * (*), TARGET * (NUMSIG)
      CHARACTER
     $   DICTRY (NDICT) * (*), FLAG * 16,
     $   KEY * (*), TARGET * 16

C     Procedures.

      EXTERNAL
     $   OPSCAN


C     Executable statements.
C     ----------------------

      ENTRY = 0

C     Isolate the significant portion of the input key (if any).

      FIRST = 1
      LAST  = MIN( LEN(KEY), NUMSIG )
      CALL OPSCAN (KEY, FIRST, LAST, MARK)

      IF (MARK .GT. 0) THEN
         TARGET = KEY (FIRST:MARK)

C        Look up TARGET in the dictionary.

   10    CONTINUE
            LENGTH = MARK - FIRST + 1

C           Select search strategy by cunning choice of termination test
C           flag.  The vertical bar is just about last in both the
C           ASCII and EBCDIC collating sequences.

            IF (ALPHA) THEN
               FLAG = TARGET
            ELSE
               FLAG = VBAR
            END IF


C           Perform search.
C           ---------------

            I = 0
   20       CONTINUE
               I = I + 1
               IF (TARGET (1:LENGTH) .EQ. DICTRY (I) (1:LENGTH)) THEN
                  IF (ENTRY .EQ. 0) THEN

C                    First "hit" - must still guard against ambiguities
C                    by searching until we've gone beyond the key
C                    (ordered dictionary) or until the end-of-dictionary
C                    mark is reached (exhaustive search).

                     ENTRY = I

C                    Special handling if match is exact - terminate
C                    search.  We thus avoid confusion if one dictionary
C                    entry looks like an abbreviation of another.
C                    This fix won't generally work for un-ordered
C                    dictionaries!

                     FIRST = 1
                     LAST = NUMSIG
                     CALL OPSCAN (DICTRY (ENTRY), FIRST, LAST, MARK)
                     IF (MARK .EQ. LENGTH) I = NDICT
                  ELSE


C                    Oops - two hits!  Abnormal termination.
C                    ---------------------------------------

                     ENTRY = -I
                     RETURN
                  END IF
               END IF

C           Check whether we've gone past the appropriate section of the
C           dictionary.  The test on the index provides insurance and an
C           optional means for limiting the extent of the search.

            IF (DICTRY (I) (1:LENGTH) .LT. FLAG  .AND.  I .LT. NDICT)
     $         GO TO 20


C           Check for a synonym.
C           --------------------

            IF (ENTRY .GT. 0) THEN

C              Look for a second entry "behind" the first entry.  FIRST
C              and MARK were determined above when the hit was detected.

               FIRST = MARK + 2
               CALL OPSCAN (DICTRY (ENTRY), FIRST, LAST, MARK)
               IF (MARK .GT. 0) THEN

C                 Re-set target and dictionary pointer, then repeat the
C                 search for the synonym instead of the original key.

                  TARGET = DICTRY (ENTRY) (FIRST:MARK)
                  ENTRY = 0
                  GO TO 10

               END IF
            END IF

      END IF
      IF (ENTRY .GT. 0) KEY = DICTRY (ENTRY)


C     Normal termination.
C     -------------------

      RETURN

C     End of OPLOOK
      END
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FUNCTION OPNUMB( STRING )

      LOGICAL          OPNUMB
      CHARACTER*(*)    STRING

************************************************************************
*     Description and usage:
*
*        A simple(-minded) test for numeric data is implemented by
*        searching an input string for legitimate characters:
*                digits 0 to 9, D, E, -, + and .
*        Insurance is provided by requiring that a numeric string
*        have at least one digit, at most one D, E or .
*        and at most two -s or +s.  Note that a few ambiguities remain:
*
*           (a)  A string might have the form of numeric data but be
*                intended as text.  No general test can hope to detect
*                such cases.
*
*           (b)  There is no check for correctness of the data format.
*                For example a meaningless string such as 'E1.+2-'
*                will be accepted as numeric.
*
*        Despite these weaknesses, the method should work in the
*        majority of cases.
*
*
*     Parameters:
*
*        Name    Dimension  Type  I/O/S  Description
*        OPNUMB              L      O    Set .TRUE. if STRING appears
*                                        to be numerical data.
*        STRING              C    I      Input data to be tested.
*
*
*     Environment:  ANSI FORTRAN 77.
*
*
*     Notes:
*
*        (1)  It is assumed that STRING is a token extracted by
*             OPTOKN, which will have converted any lower-case
*             characters to upper-case.
*
*        (2)  OPTOKN pads STRING with blanks, so that a genuine
*             number is of the form  '1234        '.
*             Hence, the scan of STRING stops at the first blank.
*
*        (3)  COMPLEX data with parentheses will not look numeric.
*
*
*     Systems Optimization Laboratory, Stanford University.
*     12 Nov  1985    Initial design and coding, starting from the
*                     routine ALPHA from Informatics General, Inc.
************************************************************************

      LOGICAL         NUMBER
      INTEGER         J, LENGTH, NDIGIT, NEXP, NMINUS, NPLUS, NPOINT
      CHARACTER*1     ATOM

      NDIGIT = 0
      NEXP   = 0
      NMINUS = 0
      NPLUS  = 0
      NPOINT = 0
      NUMBER = .TRUE.
      LENGTH = LEN (STRING)
      J      = 0

   10    J    = J + 1
         ATOM = STRING (J:J)
         IF      (ATOM .GE. '0'  .AND.  ATOM .LE. '9') THEN
            NDIGIT = NDIGIT + 1
         ELSE IF (ATOM .EQ. 'D'  .OR.   ATOM .EQ. 'E') THEN
            NEXP   = NEXP   + 1
         ELSE IF (ATOM .EQ. '-') THEN
            NMINUS = NMINUS + 1
         ELSE IF (ATOM .EQ. '+') THEN
            NPLUS  = NPLUS  + 1
         ELSE IF (ATOM .EQ. '.') THEN
            NPOINT = NPOINT + 1
         ELSE IF (ATOM .EQ. ' ') THEN
            J      = LENGTH
         ELSE
            NUMBER = .FALSE.
         END IF

         IF (NUMBER  .AND.  J .LT. LENGTH) GO TO 10

      OPNUMB = NUMBER
     $         .AND.  NDIGIT .GE. 1
     $         .AND.  NEXP   .LE. 1
     $         .AND.  NMINUS .LE. 2
     $         .AND.  NPLUS  .LE. 2
     $         .AND.  NPOINT .LE. 1

      RETURN

*     End of OPNUMB
      END
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
      SUBROUTINE OPSCAN (STRING, FIRST, LAST, MARK)
C
C
C Description and usage:
C
C       Looks for non-blank fields ("tokens") in a string, where the
C    fields are of arbitrary length, separated by blanks, tabs, commas,
C    colons, or equal signs.  The position of the end of the 1st token
C    is also returned, so this routine may be conveniently used within
C    a loop to process an entire line of text.
C
C       The procedure examines a substring, STRING (FIRST : LAST), which
C    may of course be the entire string (in which case just call OPSCAN
C    with FIRST <= 1 and LAST >= LEN (STRING) ).  The indices returned
C    are relative to STRING itself, not the substring.
C
C
C Parameters:
C
C    Name    Dimension  Type  I/O/S  Description
C    STRING              C    I      Text string containing data to be
C                                    scanned.
C    FIRST               I    I/O    Index of beginning of substring.
C                                    If <= 1, the search begins with 1.
C                                    Output is index of beginning of
C                                    first non-blank field, or 0 if no
C                                    token was found.
C    LAST                I    I/O    Index of end of substring.
C                                    If >= LEN (STRING), the search
C                                    begins with LEN (STRING).  Output
C                                    is index of end of last non-blank
C                                    field, or 0 if no token was found.
C    MARK                I      O    Points to end of first non-blank
C                                    field in the specified substring.
C                                    Set to 0 if no token was found.
C
C
C Environment:  Digital VAX-11/780 VMS FORTRAN (FORTRAN 77).
C               ANSI Fortran 77, except for the tab character HT.
C
C Notes:
C
C    (1)  IMPLICIT NONE is non-standard.  Constant HT (Tab) is defined
C         in a non-standard way:  the CHAR function is not permitted
C         in a PARAMETER declaration (OK on VAX, though).  For Absoft
C         FORTRAN 77 on 68000 machines, use HT = 9.  In other cases, it
C         may be best to declare HT as a variable and assign
C         HT = CHAR(9) on ASCII machines, or CHAR(5) for EBCDIC.
C
C    (2)  The pseudo-recursive structure was chosen for fun.  It is
C         equivalent to three DO loops with embedded GO TOs in sequence.
C
C    (3)  The variety of separators recognized limits the usefulness of
C         this routine somewhat.  The intent is to facilitate handling
C         such tokens as keywords or numerical values.  In other
C         applications, it may be necessary for ALL printing characters
C         to be significant.  A simple modification to statement
C         function SOLID will do the trick.
C
C
C Author:  Robert Kennelly, Informatics General Corporation.
C
C
C Development history:
C
C    29 Dec. 1984    RAK    Initial design and coding, (very) loosely
C                           based on SCAN_STRING by Ralph Carmichael.
C    25 Feb. 1984    RAK    Added ':' and '=' to list of separators.
C    16 Apr. 1985    RAK    Defined SOLID in terms of variable DUMMY
C                           (previous re-use of STRING was ambiguous).
C
C-----------------------------------------------------------------------


C     Variable declarations.
C     ----------------------

*     IMPLICIT NONE

C     Parameters.

      CHARACTER
     $   BLANK, EQUAL, COLON, COMMA, HT
      PARAMETER
     $   (BLANK = ' ', EQUAL = '=', COLON = ':', COMMA = ',')

C     Variables.

      LOGICAL
     $   SOLID
      INTEGER
     $   BEGIN, END, FIRST, LAST, LENGTH, MARK
      CHARACTER
     $   DUMMY, STRING * (*)

C     Statement functions.

      SOLID (DUMMY) = (DUMMY .NE. BLANK) .AND.
     $                (DUMMY .NE. COLON) .AND.
     $                (DUMMY .NE. COMMA) .AND.
     $                (DUMMY .NE. EQUAL) .AND.
     $                (DUMMY .NE. HT)


C     Executable statements.
C     ----------------------

****  HT     = CHAR(9) for ASCII machines, CHAR(5) for EBCDIC.
      HT     = CHAR(9)
      MARK   = 0
      LENGTH = LEN (STRING)
      BEGIN  = MAX (FIRST, 1)
      END    = MIN (LENGTH, LAST)

C     Find the first significant character ...

      DO 30 FIRST = BEGIN, END, +1
         IF (SOLID (STRING (FIRST : FIRST))) THEN

C           ... then the end of the first token ...

            DO 20 MARK = FIRST, END - 1, +1
               IF (.NOT.SOLID (STRING (MARK + 1 : MARK + 1))) THEN

C                 ... and finally the last significant character.

                  DO 10 LAST = END, MARK, -1
                     IF (SOLID (STRING (LAST : LAST))) THEN
                        RETURN
                     END IF
   10             CONTINUE

C                 Everything past the first token was a separator.

                  LAST = LAST + 1
                  RETURN
               END IF
   20       CONTINUE

C           There was nothing past the first token.

            LAST = MARK
            RETURN
         END IF
   30 CONTINUE

C     Whoops - the entire substring STRING (BEGIN : END) was composed of
C     separators !

      FIRST = 0
      MARK = 0
      LAST = 0
      RETURN

C     End of OPSCAN
      END
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
      SUBROUTINE OPTOKN (STRING, NUMBER, LIST)
C
C
C Description and usage:
C
C       An aid to parsing input data.  The individual "tokens" in a
C    character string are isolated, converted to uppercase, and stored
C    in an array.  Here, a token is a group of significant, contiguous
C    characters.  The following are NON-significant, and hence may
C    serve as separators:  blanks, horizontal tabs, commas, colons,
C    and equal signs.  See OPSCAN for details.  Processing continues
C    until the requested number of tokens have been found or the end
C    of the input string is reached.
C
C
C Parameters:
C
C    Name    Dimension  Type  I/O/S  Description
C    STRING              C    I      Input string to be analyzed.
C    NUMBER              I    I/O    Number of tokens requested (input)
C                                    and found (output).
C    LIST    NUMBER      C      O    Array of tokens, changed to upper
C                                    case.
C
C
C External references:
C
C    Name    Description
C    OPSCAN  Finds positions of first and last significant characters.
C    OPUPPR  Converts a string to uppercase.
C
C
C Environment:  Digital VAX-11/780 VMS FORTRAN (FORTRAN 77).
C               Appears to satisfy the ANSI Fortran 77 standard.
C
C
C Notes:
C
C    (1)  IMPLICIT NONE is non-standard.  (Has been commented out.)
C
C
C Author:  Robert Kennelly, Informatics General Corporation.
C
C
C Development history:
C
C    16 Jan. 1984    RAK    Initial design and coding.
C    16 Mar. 1984    RAK    Revised header to reflect full list of
C                           separators, repaired faulty WHILE clause
C                           in "10" loop.
C    18 Sep. 1984    RAK    Change elements of LIST to uppercase one
C                           at a time, leaving STRING unchanged.
C
C-----------------------------------------------------------------------


C     Variable declarations.
C     ----------------------

*     IMPLICIT NONE

C     Parameters.

      CHARACTER
     $   BLANK
      PARAMETER
     $   (BLANK = ' ')

C     Variables.

      INTEGER
     $   COUNT, FIRST, I, LAST, MARK, NUMBER
      CHARACTER
     $   STRING * (*), LIST (NUMBER) * (*)

C     Procedures.

      EXTERNAL
     $   OPUPPR, OPSCAN


C     Executable statements.
C     ----------------------

C     WHILE there are tokens to find, loop UNTIL enough have been found.

      FIRST = 1
      LAST = LEN (STRING)

      COUNT = 0
   10 CONTINUE

C        Get delimiting indices of next token, if any.

         CALL OPSCAN (STRING, FIRST, LAST, MARK)
         IF (LAST .GT. 0) THEN
            COUNT = COUNT + 1

C           Pass token to output string array, then change case.

            LIST (COUNT) = STRING (FIRST : MARK)
            CALL OPUPPR (LIST (COUNT))
            FIRST = MARK + 2
            IF (COUNT .LT. NUMBER) GO TO 10

         END IF


C     Fill the rest of LIST with blanks and set NUMBER for output.

      DO 20 I = COUNT + 1, NUMBER
         LIST (I) = BLANK
   20 CONTINUE

      NUMBER = COUNT


C     Termination.
C     ------------

      RETURN

C     End of OPTOKN
      END
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
      SUBROUTINE OPUPPR(STRING)
C
C ACRONYM:  UPper CASE
C
C PURPOSE:  This subroutine changes all lower case letters in the
C           character string to upper case.
C
C METHOD:   Each character in STRING is treated in turn.  The intrinsic
C           function INDEX effectively allows a table lookup, with
C           the local strings LOW and UPP acting as two tables.
C           This method avoids the use of CHAR and ICHAR, which appear
C           be different on ASCII and EBCDIC machines.
C
C ARGUMENTS
C    ARG       DIM     TYPE I/O/S DESCRIPTION
C  STRING       *       C   I/O   Character string possibly containing
C                                 some lower-case letters on input;
C                                 strictly upper-case letters on output
C                                 with no change to any non-alphabetic
C                                 characters.
C
C EXTERNAL REFERENCES:
C  LEN    - Returns the declared length of a CHARACTER variable.
C  INDEX  - Returns the position of second string within first.
C
C ENVIRONMENT:  ANSI FORTRAN 77
C
C DEVELOPMENT HISTORY:
C     DATE  INITIALS  DESCRIPTION
C   06/28/83   CLH    Initial design.
C   01/03/84   RAK    Eliminated NCHAR input.
C   06/14/84   RAK    Used integer PARAMETERs in comparison.
C   04/21/85   RAK    Eliminated DO/END DO in favor of standard code.
C   09/10/85   MAS    Eliminated CHAR,ICHAR in favor of LOW, UPP, INDEX.
C
C AUTHOR: Charles Hooper, Informatics General, Palo Alto, CA.
C
C-----------------------------------------------------------------------

      CHARACTER      STRING * (*)
      INTEGER        I, J
      CHARACTER      C*1, LOW*26, UPP*26
      DATA           LOW /'abcdefghijklmnopqrstuvwxyz'/,
     $               UPP /'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/

      DO 10 J = 1, LEN(STRING)
         C    = STRING(J:J)
         IF (C .GE. 'a'  .AND.  C .LE. 'z') THEN
            I           = INDEX( LOW, C )
            IF (I .GT. 0) STRING(J:J) = UPP(I:I)
         END IF
   10 CONTINUE
      RETURN

*     End of OPUPPR
      END
