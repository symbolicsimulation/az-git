;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: az.asd; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2013.
;;; 
;;; License "Golden Rule License"
;;;
;;; First   (load "az.asd")
;;; then    (asdf:oos 'asdf:load-op :az)
;;; or
;;; checkout az into ~/quickload/local-projects
;;; and then (ql:quickload :az)
;;;

#+NIL
(proclaim
 '(optimize
   (speed 0)
   (safety 1)
   (debug 3)))

(defparameter cl-user::*xcolorname-database-file*
  (or
   (probe-file "/usr/openwin/lib/rgb.txt")
   (probe-file "/usr/lib/X11/rgb.txt")
   (probe-file "/usr/share/X11/rgb.txt"))) ; $$ on modern-ish linux distros like FC1x -jm
(when (null cl-user::*xcolorname-database-file*)
  (error "Define *xcolorname-database-file* in az.system!"))

(defparameter cl-user::*initially-do*
    '(progn
       #-sbcl (unix-make (az-subdir "foreign/"))
      #-:cmu (require :clx)
      #+sbcl (ql:quickload :clx)
      ;; #+:cmu (load "library:subsystems/clx-library")

 
      (setf *print-array* nil)
      (setf *read-default-float-format* 'Double-Float)
      #+:cmu (setf ext:*gc-verbose* t)
      #+:cmu (setf ext:*bytes-consed-between-gcs* 6000000)
      #+:excl (setf (sys:gsgc-switch :print) t)
      #+:excl (setf excl:*tenured-bytes-limit* 20000000)
      #-:allegro-v4.0 (require :loop)
      #-:allegro-v4.0 (require :clx)
      #+:allegro-v4.0 (cltl1:require :loop)
      #+:allegro-v4.0 (cltl1:require :clx)))

;(rename-package 'common-lisp-user 'common-lisp-user '(cl-user user))
;(rename-package 'common-lisp 'common-lisp '(lisp cl))
;(rename-package 'sb-mop 'sb-mop '(clos))

(defpackage :clos
  #+sbcl (:use :sb-mop)
  (:export
   #:built-in-class
   #:class-direct-slots
   #:class-direct-superclasses
   #:slot-definition-name
   #:slot-definition-type
   #:structure-class
   )
  )

(defpackage #:az.asd
  (:use :cl :asdf))

(in-package :az.asd)

(defsystem az
  :name "Arizona"
  :version "0.0.1"
  :author "John Alan McDonald"
  :maintainer "jm@symbolic-simulation.com"
  :description "The Arizona System"
;  :depends-on (:clouseau)
  :depends-on (:clx :clouseau)
  :components
  ((:module :definitions
	    :components
	    ((:file "package")
	     (:file "defs" :depends-on ("package"))
	     (:file "definition" :depends-on ("package" "defs"))
	     (:file "pack" :depends-on ("package" "defs"))
	     (:file "global" :depends-on ("package" "defs"))
	     (:file "fun")
	     (:file "type" :depends-on ("package"))
	     (:file "build" :depends-on ("package" "defs"))
	     (:file "read" :depends-on ("package" "defs"))
	     (:file "filter" :depends-on ("package" "defs"))
	     (:file "sort" :depends-on ("package" "defs"))
	     (:file "print" :depends-on ("package" "defs" "type"))))
   (:module :az
	    :depends-on ("definitions")
	    :components
	    ((:file "package")
	     (:file "types" :depends-on ("package"))
	     (:file "macros")
	     (:file "defs" :depends-on ("package"))
	     (:file "clos-additions" :depends-on ("package" "defs"))
	     (:file "tools" :depends-on ("package" "defs"))
	     (:file "equal" :depends-on ("package" "defs"))
	     (:file "copy" :depends-on ("package" "defs"))
	     (:file "kill")
	     (:file "arrays" :depends-on ("types"))
	     (:file "tables")
	     #+:accelerators (:file "foreign-load")
	     (:file "foreign")
	     (:file "atomic")
	     (:file "timestamp")
	     (:file "resources")))
   (:module :actors
	    :depends-on ("az" "xlite")
	    :components
	    ((:file "package")
	      (:file "defs")
	      (:file "atomic")
	      (:file "queues")
	      (:file "actors" :depends-on ("package"))
	      (:file "interactor" :depends-on ("package"))
	      (:file "dispatch" :depends-on ("package"))
	      (:file "event-loop")
	      (:file "initializations" :depends-on ("package"))))
   (:module :announcements
	    :depends-on ("az" "actors")
	    :components
	    ((:file "package")
	     (:file "defs" :depends-on ("package"))
	     (:file "announcements" :depends-on ("package"))))
   (:module :basic-math
	    :depends-on ("az")
	    :components
	    ((:file "package")
	     (:file "defs" :depends-on ("package"))
	     (:file "sequences" :depends-on ("package"))
	     (:file "minimize-1d" :depends-on ("package"))
	     (:file "interpolation" :depends-on ("package"))
	     (:file "evaluation" :depends-on ("package"))
	     (:file "quadrature" :depends-on ("package"))
	     (:file "special-functions" :depends-on ("package"))
	     (:file "v-ops" :depends-on ("package"))
	     (:file "arrays" :depends-on ("package" "v-ops"))))
   (:module :brick
	    :depends-on ("az")
	    :components
	    ((:file "package")
	     (:file "defs" :depends-on ("package"))
	     (:file "brick" :depends-on ("package"))
	     (:file "domain" :depends-on ("package"))
	     (:file "terms" :depends-on ("package"))
	     (:file "functionals" :depends-on ("package"))
	     (:file "inequalities" :depends-on ("package"))
	     (:file "tableau" :depends-on ("package"))
	     (:file "constraints" :depends-on ("package"))
	     (:file "simplex" :depends-on ("package"))
	     (:file "solve" :depends-on ("package"))))
   (:module :browser
	    :depends-on ("graph" "layout")
	    :serial t
	    :components
	    ((:file "package")
	     (:file "class")
	     (:file "examiner")
	     (:file "pedigree")
	     (:file "horse")))
   (:module :cactus
	    :depends-on ("az" "basic-math")
	    :serial t
	    :components (
			 (:file "package")
			 (:file "cactus-object")
			 (:file "spaces")
			 (:file "lvectors")
			 (:file "lpoints")
			 (:file "direct-sum")
			 (:file "mappings")
			 (:file "matrix")
			 (:file "modifiers")
			 (:file "compound")
			 (:file "mix")
			 (:file "compose")
			 (:file "decompose")
			 (:file "pseudo-inverse")
			 (:file "determinant")
			 (:file "metrics")
			 ;; (:file "strip-charts")
			 ;; (:file "objective-functions")
			 ;; (:file "minimize")
			 ;; (:file "optimizers")
			 #-sbcl (:file "test-linear-algebra") ; Calls functions defined only in relects
			 ;; (:file "test-minimize")
			 ;; (:file "test-optimizers")
			 ))
   (:module :clay
	    :depends-on ("az" "announcements" "brick" "geometry")
	    :serial t
	    :components
	    ((:file "package")
	     (:file "defs")
	     (:file "announcements")
	     (:file "lens")
	     (:file "paint")
	     (:file "stroke")
	     (:file "diagram")
	     (:file "layout-solver")
	     (:file "interactive")
;	     (:file "role")
	     (:file "mediator")
;	     (:file "root")
;	     (:file "leaf")
;	     (:file "leaves")
;	     (:file "label")
;	     (:file "button")
	     (:file "toggle")
	     (:file "radio-button")
;	     (:file "labeled-button")
;	     (:file "menu-items")
;	     (:file "menu")
	     (:file "control-panel")
;	     (:file "palette")
;	     (:file "plot")
	     (:file "initializations")
	     ))
   (:module :geometry
	    :depends-on ("az")
	    :serial t
	    :components
	    ((:file "package")
	     (:file "v-ops")
	     (:file "defs")
	     (:file "geometric-objects" )
	     (:file "spaces")
	     (:file "points")
	     (:file "arrays")
	     (:file "point-set")
	     (:file "point-cloud")
	     (:file "rects")
	     (:file "mappings")
	     (:file "matrix")
	     (:file "modifiers")
	     (:file "compound")
	     (:file "mix")
	     (:file "compose")
	     (:file "transform")
	     (:file "lapack")
	     (:file "decompose")
	     (:file "pseudo-inverse")
	     (:file "determinant")
	     (:file "metrics")
	     (:file "tics")))
   (:module :graph
	    :depends-on ("az" "announcements")
	    :serial t
	    :components
	    ((:file "package")
	     (:file "protocol")
	     (:file "implementation")))
   (:module :layout
	   :depends-on ("az" "brick" "clay" "graph")
	   :serial t
	   :components
	   ((:file "package")
	    (:file "defs")
	    (:file "graph")
	    (:file "node")
	    (:file "edge")
	    (:file "solver")
	    #+:npsol (:file "npsol")
	    #+:npsol (:file "npsol-solver")
	    ;; #+:npsol (:file "nlc-solver")
	    (:file "constraints")
	    (:file "attach-edges")
	    (:file "distance")
	    (:file "weights")
	    (:file "node-spring")
	    ;; (:file "node-repulsion")
	    ;; (:file "edge-spring")
	    ;; (:file "edge-repulsion")
	    ;; (:file "homotopy")
	    ;; (:file "homotopy-3d")
	    ;; (:file "crusher")
	    ;; (:file "digraph")
	    ;; (:file "dag")
	    (:file "mediator")
	    (:file "input")
	    (:file "menus")
	    (:file "tests")))
   #+NIL-JM (:module :msg
	    :depends-on ()
	    :serial t
	    :initially-do (unix-make (az-subdir "msg/"))
	    :components ("package"
			 "foreign"
			 "defs"
			 "socket"
			 "message"
			 "connection"
			 "server"
			 "client"
			 "eval"))
   
   (:module :xlite
	    :depends-on ("az" "geometry")
	    :serial t
	    :components
	    ((:file "package")
	     (:file "defs")
	     (:file "displays")
	     (:file "colors")
	     (:file "paint-4-4")
	     (:file "drawables")
	     (:file "fonts")
	     (:file "gcontexts")
	     (:file "graphics")
	     (:file "images")
	     (:file "cursors")
	     (:file "events")
	     ;; (:file "menu")
	     (:file "hardcopy")
	     (:file "ps-hardcopy")
	     (:file "tests")
	     (:file "initializations")))
  )
  )

#|
(asdf:oos 'asdf:load-op :az)
(trace xlib:create-colormap)
(trace xlib:store-colors)
;(trace xlib:store-color)
(trace xlt::make-colormap)
(trace xlt::drawable-force-output)
(trace xlt::fill-paint-colors)
(trace xlt::copy-default-colors)
;(xlite:make-window :width 100 :height 100)
|#
