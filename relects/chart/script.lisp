;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart)

;;;============================================================

(defparameter *strip-chart* (make-2d-strip-chart
                             :name (string (gentemp "Spiral-"))
                             :x-label "Dollars"
                             :y-label "Sense"))
(clear-chart *strip-chart*)
(time
 (dotimes (i 200)
   (let* ((theta (* 0.1d0 i))
	  (r (sqrt theta)))
     (strip-crawl *strip-chart*
		  (g:make-chart-point :x (* r (cos theta))
				      :y (* r (sin theta)))))))

(slate:finish-drawing (chart-slate *strip-chart*))
(chart-rect *strip-chart*)

;;;============================================================

(defparameter *s-chart* (make-s-chart
                         :chart-rect (g:make-chart-rect
				      :left -1.3d0 :right 1.1d0
				      :bottom -2.1d0 :top 1.7d0)))

(progn
  (with-clipping-region (*s-chart* nil)
    (slate:clear-slate (chart-slate *s-chart*)))
  (layout-s-chart *s-chart*)
  (draw-outer-s-chart *s-chart*)
  (draw-line slate::*xor-red-pen* *s-chart*
             (g:make-chart-point :x -1.0d0 :y 0.0d0)
             (g:make-chart-point :x 1.0d0 :y 0.0d0))
  (draw-line slate::*xor-red-pen* *s-chart*
             (g:make-chart-point :x 0.0d0 :y -1.0d0)
             (g:make-chart-point :x 0.0d0 :y 1.0d0))
  (finish-drawing *s-chart*))

;;;============================================================

(progn
  (slate:draw-rect
   slate::*gray-tile-pen* (chart-slate *s-chart*) (drawing-room *s-chart*))

  (with-clipping-region (*s-chart* nil)
			(loop for z from -1.0d0 to 1.0d0 by 0.0d01
			 do (draw-point slate::*xor-red-pen* *s-chart*
					(g:make-chart-point :x z :y z))))
  (finish-drawing *s-chart*))

;;;============================================================

(setf *chart* (b+w-chart t))
(setf *chart* (rgb-chart))
(setf *chart* (rgb-chart))

(setf *cr1* (one-pixel-larger-chart-rect *chart* *cr* *cr1*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-xor-red-pen* *chart* *unit-r*)
  (draw-line slate::*fast-xor-red-pen* *chart*
             (g:make-chart-point :x 0.0d0 :y 0.0d0)
             (g:make-chart-point :x 0.0d0 :y 1.0d0))
  (draw-line slate::*fast-xor-red-pen* *chart*
             (g:make-chart-point :x 0.0d0 :y 0.0d0)
             (g:make-chart-point :x 1.0d0 :y 0.0d0))
  (draw-line slate::*fast-xor-red-pen* *chart*
             (g:make-chart-point :x 1.0d0 :y 1.0d0)
             (g:make-chart-point :x 1.0d0 :y 0.0d0))
  (draw-line slate::*fast-xor-red-pen* *chart*
             (g:make-chart-point :x 0.0d0 :y 1.0d0)
             (g:make-chart-point :x 1.0d0 :y 1.0d0))
  (finish-drawing *chart*))

(progn
  (draw-rect slate::*fast-xor-red-fill-pen* *chart* *cr*)
  (slate:draw-rect
   slate::*fast-xor-red-fill-pen*
   (chart-slate *chart*)
   (g:transform (chart-transform *chart*) *cr*))
  (finish-drawing *chart*))


(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (with-clipping-region (*chart* *cr*)
    (draw-rect slate::*fast-xor-red-fill-pen* *chart* *cr*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (with-clipping-region (*chart* *cr*)
    (draw-polygon slate::*fast-xor-red-pen* *chart* *poly*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (with-clipping-region (*chart* *cr*)
    (draw-polygon slate::*fast-xor-red-fill-pen* *chart* *poly*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (with-clipping-region (*chart* *cr*)
    (draw-polygon slate::*xor-gray-tile-pen* *chart* *poly*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (draw-rect slate::*fast-green-pen* *chart* *r1*)
  (with-clipping-region (*chart* *cr*)
    (draw-rect slate::*fast-xor-red-pen* *chart* *r1*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (draw-rect slate::*fast-green-pen* *chart* *r1*)
  (with-clipping-region (*chart* *cr*)
    (draw-rect slate::*fast-xor-red-fill-pen* *chart* *r1*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (draw-rect slate::*fast-green-pen* *chart* *r1*)
  (with-clipping-region (*chart* *cr*)
    (draw-rect slate::*xor-gray-tile-pen* *chart* *r1*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (draw-rect slate::*fast-green-pen* *chart* *r1*)
  (with-clipping-region (*chart* *cr*)
    (draw-ellipse slate::*fast-xor-red-pen* *chart* *r1*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (draw-rect slate::*fast-green-pen* *chart* *r1*)
  (with-clipping-region (*chart* *cr*)
    (draw-ellipse slate::*gray-fill-pen* *chart* *r1*))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* *cr1*)
  (draw-rect slate::*fast-yellow-pen* *chart* *cr*)
  (draw-rect slate::*fast-green-pen* *chart* *r1*)
  (with-clipping-region (*chart* *cr*)
    (draw-ellipse slate::*xor-gray-tile-pen* *chart* *r1*))
  (finish-drawing *chart*))

(let* ((dx 1.0d0)
       (dy 1.0d0)
       (chart-rect (chart-rect *chart*))
       (left (+ (g:chart-rect-left chart-rect) dx))
       (bottom (+ (g:chart-rect-bottom chart-rect) dy))
       (width (- (g:chart-rect-width chart-rect) dx dx))
       (height (- (g:chart-rect-height chart-rect) dy dy))
       (r0 (g:make-chart-rect :left left :bottom bottom
                              :width width :height height))
       (sr0 (g:transform (chart-transform *chart*) r0))
       (sr1 (g:make-screen-rect :left (+ (g:screen-rect-left sr0) 1)
                                :top (+ (g:screen-rect-top sr0) 1)
                                :width (- (g:screen-rect-width sr0) 2)
                                :height (- (g:screen-rect-height sr0) 2)))
       (r1 (g:transform (chart-inverse-transform *chart*) sr1)))
  (clear-chart *chart*)
  (draw-rect slate::*fast-green-pen* *chart* r0)
  (draw-rect slate::*fast-yellow-pen* *chart* r1)
  (with-clipping-region (*chart* r1)
    (test-all-drawing-ops slate::*fast-xor-brown-pen* *chart*
                          :npoints 3
                          :seconds 1))
  (finish-drawing *chart*))

(test-all-drawing-ops
 slate::*fast-xor-red-pen* (default-chart)
 :npoints 8 :seconds 0 :print? nil)


(progn
  (clear-chart *chart*)
  (draw-line slate::*xor-white-pen* *chart*
             (g:make-chart-point)
             (g:make-chart-point :x 5.0d0 :y 5.0d0))
  (draw-line slate::*white-pen* *chart*
             (g:make-chart-point :x 5.0d0 :y 5.0d0)
             (g:make-chart-point :x 10.0d0))
  (draw-line slate::*white-pen* *chart*
             (g:make-chart-point :x 1.0d0 :y 1.0d0)
             (g:make-chart-point :x 3.0d0 :y 3.0d0))
  (draw-line slate::*white-pen* *chart*
             (g:make-chart-point :x 1.0d0 :y 1.0d0)
             (g:make-chart-point :x 5.0d0 :y 5.0d0))
  (draw-line slate::*white-pen* *chart*
             (g:make-chart-point :x 5.0d0 :y 1.0d0)
             (g:make-chart-point :x 1.0d0 :y 5.0d0))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-line slate::*white-pen* *chart*
             (g:make-chart-point :x 1.0d0 :y 1.0d0)
             (g:make-chart-point :x 3.0d0 :y 3.0d0))
  (draw-line slate::*white-pen* *chart*
             (g:make-chart-point :x 1.0d0 :y 1.0d0)
             (g:make-chart-point :x 1.0d0 :y 5.0d0))
  (draw-line slate::*white-pen* *chart*
             (g:make-chart-point :x 1.0d0 :y 1.0d0)
             (g:make-chart-point :x 5.0d0 :y 1.0d0))
  (draw-rect slate::*white-fill-pen* *chart*
             (g:make-chart-rect
	      :left 5.0d0 :bottom 5.0d0 :width 3.0d0 :height 4.0d0))
  (draw-rect slate::*white-pen* *chart*
             (g:make-chart-rect
	      :left 9.0d0 :top 9.0d0 :width 3.0d0 :height 4.0d0))
  (draw-rect slate::*xor-white-pen* *chart*
             (g:make-chart-rect
	      :left 13.0d0 :top 13.0d0 :width 3.0d0 :height 4.0d0))
  (let ((x 17.0d0))
    (dotimes (i 10)
      (draw-rect slate::*white-pen* *chart*
                 (g:make-chart-rect
                   :left x :top 2.0d0
		   :width (az:fl i)
		   :height (az:fl i)))
      (draw-rect slate::*white-pen* *chart*
                 (g:make-chart-rect
                   :left x :top 12.0d0
		   :width (az:fl i)
		   :height (az:fl (+ 1 i))))
      (draw-rect slate::*white-fill-pen* *chart*
                 (g:make-chart-rect
                   :left x :top 22.0d0
		   :width (az:fl i)
		   :height (az:fl i)))
      (draw-rect slate::*white-fill-pen* *chart*
                 (g:make-chart-rect
                   :left x :top 32.0d0
		   :width (az:fl i)
		   :height (az:fl (+ 1 i))))
      (incf x (+ i 2))))
  (finish-drawing *chart*))

(progn
  (clear-chart *chart*)
  (draw-line
    slate::*xor-white-pen* *chart*
    (g:make-chart-point :x 0.0d0 :y (chart-height *chart*))
    (g:make-chart-point :x (chart-width *chart*) :y 0.0d0))
  (draw-line
    slate::*xor-white-pen* *chart*
    (g:make-chart-point :x 0.0d0 :y 0.0d0)
    (g:make-chart-point :x (chart-width *chart*) :y (chart-height *chart*)))
  (finish-drawing *chart*))


(clear-chart *chart*)
(progn
  (time
   (test-hv-lines slate::*fast-xor-brown-pen* *chart*))
  (time (test-hv-lines slate::*xor-brown-pen* *chart*)))

(clear-chart *chart*)
(progn
  (time (test-slanted-lines slate::*fast-xor-brown-pen* *chart*))
  (time (test-slanted-lines slate::*xor-brown-pen* *chart*)))


(let ((p (g:make-chart-point
	  :x (chart-left *chart*)
			   :y (* 0.5d0 (+ (chart-top *chart*)
					  (chart-bottom *chart*))))))
      
  (clear-chart *chart*)
  (draw-string slate::*white-pen* *chart* "a" p)
  (draw-string slate::*white-pen* *chart* "b" p)
  (draw-character slate::*white-pen* *chart* #\a p)
  (draw-character slate::*white-pen* *chart* #\b p)
  (finish-drawing *chart*))








