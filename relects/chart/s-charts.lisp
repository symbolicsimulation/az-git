;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart)

(eval-when (compile load eval)
  (export '(S-Chart
	    make-s-chart)))

;;;============================================================
;;; Default Pens for S-Chart's
;;;============================================================

(defparameter *tic-pen* ())

(defun default-tic-pen ()
  (when (null *tic-pen*)
    (setf *tic-pen*
      (slate:make-pen :pen-pixel-value
		      (slate:colorname->pixel-value
		       (slate:screen-colormap
			(slate:default-screen))
		       :wheat)
		      :pen-font (slate:font :family :times
					    :face :roman
					    :size 6))))
  *tic-pen*)

(defparameter *label-pen* ())

(defun default-label-pen ()
  (when (null *label-pen*)
    (setf *label-pen*
      (slate:make-pen :pen-pixel-value
		      (slate:colorname->pixel-value
		       (slate:screen-colormap
			(slate:default-screen))
		       :wheat)
		      :pen-font (slate:font :family :times
					    :face :roman
					    :size 12))))
  *label-pen*)

(defparameter *title-pen* ())

(defun default-title-pen ()
  (when (null *title-pen*)
    (setf *title-pen*
      (slate:make-pen :pen-pixel-value
		      (slate:colorname->pixel-value
		       (slate:screen-colormap
			(slate:default-screen))
		       :wheat)
		      :pen-font (slate:font :family :times
					    :face :roman
					    :size 14))))
  *title-pen*)

;;;============================================================
;;; S-Chart's
;;;============================================================

(defclass S-Chart (Chart)
	  ((spacing :type g:Positive-Screen-Coordinate
		    :accessor spacing
		    :initarg :spacing)
	   (title :type String
		  :reader title
		  :initarg :title)
	   (title-pen :type slate:Pen
		      :accessor title-pen
		      :initarg :title-pen)
	   (x-label :type String
		    :accessor x-label
		    :initarg :x-label)
	   (y-label :type String
		    :accessor y-label
		    :initarg :y-label)
	   (x-label-pen :type slate:Pen
			:accessor x-label-pen
			:initarg :x-label-pen)
	   (y-label-pen :type slate:Pen
			:accessor y-label-pen
			:initarg :y-label-pen)
	   (xtics :type g:Tics
		  :accessor xtics
		  :initarg xtics)
	   (ytics :type g:Tics
		  :accessor ytics
		  :initarg ytics)
	   (tic-length :type g:Positive-Screen-Coordinate
		       :accessor tic-length
		       :initarg :tic-length)
	   (tic-pen :type slate:Pen
		    :accessor tic-pen
		    :initarg :tic-pen)
	   (tic-label-pen :type slate:Pen
			  :accessor tic-label-pen
			  :initarg :tic-label-pen))
	  (:default-initargs
	   :mouse-down-eventfn #'(lambda () nil)))

;;;============================================================
;;; make slate and window names the same as the title

(defgeneric (setf title) (title s-chart)
  (declare (type (or Symbol String) title)
	   (type S-Chart s-chart)))

(defmethod (setf title) (title (s-chart S-Chart))
  (setf (slot-value s-chart 'title) title)
  (setf (chart-name s-chart) title))

;;;============================================================

(defun available-slate-rect (slate s-chart &key (result (g:make-screen-rect)))
  (az:declare-check (type slate:Slate slate)
		    (type S-Chart s-chart))
  (let* ((slate-rect (slate:slate-rect slate))
	 (spacing (spacing s-chart))
	 (tic-length (tic-length s-chart))
	 (screen (slate:slate-screen slate))
	 (xtic-labels (g:tic-labels (xtics s-chart)))
	 (ytic-labels (g:tic-labels (ytics s-chart)))
	 (tic-font (slate:pen-font (tic-label-pen s-chart)))
	 (title-height
	  (slate:string-height
	   screen (title s-chart) (slate:pen-font (title-pen s-chart))))
	 (top-margin
	  (+ spacing
	     (max (+ spacing title-height tic-length)
		  (ceiling (slate:maximum-vertical-string-height
			    screen ytic-labels tic-font)
			   2))))
	 (right-margin
	  (+ spacing
	     (max tic-length
		  (ceiling (slate:maximum-string-width
			    screen xtic-labels tic-font)
			   2))))
	 (bottom-margin
	  (+ spacing spacing spacing tic-length
	     (slate:string-height
	      screen (x-label s-chart)
	      (slate:pen-font (x-label-pen s-chart)))
	     (slate:maximum-string-height screen xtic-labels tic-font)))
	 (left-margin
	  (+ spacing spacing spacing tic-length
	     (slate:vertical-string-width
	      screen (y-label s-chart)
	      (slate:pen-font (y-label-pen s-chart)))
	     (slate:maximum-string-width
	      screen ytic-labels tic-font)))
	 (left (+ (g:screen-rect-left slate-rect) left-margin 1))
	 (right (- (g:screen-rect-right slate-rect) right-margin 1))
	 (top (+ (g:screen-rect-top slate-rect) top-margin 1))
	 (bottom (- (g:screen-rect-bottom slate-rect) bottom-margin 1)))
    
    (setf (g:screen-rect-left result) left)
    (setf (g:screen-rect-width result) (- right left))
    (setf (g:screen-rect-top result) top)
    (setf (g:screen-rect-height result) (- bottom top))
    result))

;;;============================================================

(defun layout-s-chart (s-chart)
  (az:declare-check (type S-Chart s-chart))
  (multiple-value-bind
    (s-chart-rect xtics ytics)
      (g:nice-chart-rect (chart-rect s-chart) :result (chart-rect s-chart))
    (setf (xtics s-chart) xtics)
    (setf (ytics s-chart) ytics)
    (setf (slot-value s-chart 'chart-rect) s-chart-rect)
    (let* ((slate (chart-slate s-chart))
	   (drawing-room (available-slate-rect slate s-chart)))
      (setf (slot-value s-chart 'drawing-room) drawing-room)
      (setf (slate:slate-clipping-region slate) drawing-room)
      (setf (chart-transform s-chart)
	    (g:make-affine-map-between s-chart-rect drawing-room))
      ))
  s-chart)

;;;============================================================

(defun draw-outer-s-chart (s-chart)
  (az:declare-check (type S-Chart s-chart))
  (let* ((drawing-room (drawing-room s-chart))
	 (map (chart-transform s-chart))
	 (slate (chart-slate s-chart))
	 (screen (slate:slate-screen slate))
	 (spacing (spacing s-chart))
	 (title (title s-chart))
	 (title-pen (title-pen s-chart))
	 (x-label (x-label s-chart))
	 (x-label-pen (x-label-pen s-chart))
	 (y-label (y-label s-chart))
	 (y-label-pen (y-label-pen s-chart))
	 (tic-pen (tic-pen s-chart))
	 (tic-font (slate:pen-font tic-pen))
	 (tic-length (tic-length s-chart))
	 (xtics (xtics s-chart))
	 (xtic-values (g:tic-values xtics))
	 (xtic-labels (g:tic-labels xtics))
	 (ytics (ytics s-chart))
	 (ytic-values (g:tic-values ytics))
	 (ytic-labels (g:tic-labels ytics))
	 (border-left (- (g::%screen-rect-left drawing-room) 1))
	 (border-right (+ (g::%screen-rect-right drawing-room) 1))
	 (border-top (- (g::%screen-rect-top drawing-room) 1))
	 (border-bottom (+ (g::%screen-rect-bottom drawing-room) 1))
	 (tic-left (- border-left tic-length))
	 (tic-right (+ border-right tic-length))
	 (tic-top (- border-top tic-length))
	 (tic-bottom (+ border-bottom tic-length)))
    (slate:with-clipping-region
     (slate nil)
     (g:with-borrowed-screen-point
      (p0)
      (g:with-borrowed-screen-point
       (p1)
       (g:with-borrowed-screen-rect

	(r :left border-left :right border-right 
	   :top border-top :bottom border-bottom)
	;;(draw-rect tic-pen s-chart (chart-rect s-chart))
	(slate::%draw-rect tic-pen slate r)
	(g::%set-screen-point-coords
	 p0
	 (round (+ border-left border-right) 2)
	 (+ spacing
	    (round (- tic-top (slate:slate-top slate) spacing) 2)))
	(slate::%draw-centered-string title-pen slate title p0)
	(setf (g::%screen-point-y p0)
	  (+ spacing spacing tic-bottom
	     (slate:maximum-string-height screen xtic-labels tic-font)
	     (round
	      (slate::%string-height
	       screen x-label (slate:pen-font x-label-pen)) 2)))
	(slate::%draw-centered-string x-label-pen slate x-label p0)
	(g::%set-screen-point-coords
	 p0
	 (+ spacing
	    (truncate (slate::%vertical-string-width
		       screen y-label (slate:pen-font y-label-pen))
		      2))
	 (round (+ border-top border-bottom) 2))
	(slate::%draw-centered-vertical-string y-label-pen slate y-label p0)
	(dolist (x xtic-values)
	  (let* ((label (pop xtic-labels))
		 (sx (g::%transform-c->s-x-coordinate map x))
		 (h/2 (ceiling (slate::%string-height screen label tic-font)
			       2)))
	    (g::%set-screen-point-coords p0 sx border-bottom)
	    (g::%set-screen-point-coords p1 sx tic-bottom)
	    (slate::%draw-line tic-pen slate p0 p1)
	    (setf (g::%screen-point-y p0) border-top)
	    (setf (g::%screen-point-y p1) tic-top)
	    (slate::%draw-line tic-pen slate p0 p1)
	    (setf (g::%screen-point-y p0) (+ tic-bottom h/2 spacing))
	    (slate::%draw-centered-string tic-pen slate label p0) 
	    ))
	(dolist (y ytic-values)
	  (let* ((label (pop ytic-labels))
		 (sy (g::%transform-c->s-y-coordinate map y))
		 (w/2 (ceiling (slate::%string-width screen label tic-font)
			       2)))
	    (g::%set-screen-point-coords p0 border-left sy)
	    (g::%set-screen-point-coords p1 tic-left sy)
	    (slate::%draw-line tic-pen slate p0 p1)
	    (setf (g::%screen-point-x p0) border-right)
	    (setf (g::%screen-point-x p1) tic-right)
	    (slate::%draw-line tic-pen slate p0 p1)
	    (setf (g::%screen-point-x p0) (- tic-left w/2 spacing))
	    (slate::%draw-centered-string tic-pen slate label p0))))
       )))))

;;;============================================================

(defclass S-Chart-Interactor-Role (Chart-Interactor-Role) ())

(defmethod slate:configure-notify ((role S-Chart-Interactor-Role)
				   x y width height)
  x y width height
  (let ((s-chart (chart (ac:actor role))))
    (layout-s-chart s-chart)
    (draw-outer-s-chart s-chart)
    (finish-drawing s-chart)))

(defmethod slate:exposure ((role S-Chart-Interactor-Role)
			   x y w h count)
  x y w h count
  (let ((s-chart (chart (ac:actor role))))
    (draw-outer-s-chart s-chart)
    (finish-drawing s-chart)))

;;;------------------------------------------------------------

(defclass S-Chart-Interactor (Chart-Interactor) ())

(defmethod ac:build-roles ((interactor S-Chart-Interactor))
  (setf (ac:default-role interactor)
    (make-instance 'S-Chart-Interactor-Role :actor interactor)))

;;;============================================================

(defparameter *s-charts* ()) ;; for debugging

(defun make-s-chart (&key
		     (class 'S-Chart)
		     (chart-rect (g:make-chart-rect
				  :width 1.0d0 :height 1.0d0))
		     (screen (slate:default-screen))
		     (name (gentemp "S-CHART-"))
		     (slate-options ())
		     (spacing 4)
		     (title (string name))
		     (title-pen (default-title-pen))
		     (x-label "X Axis")
		     (y-label "Y Axis")
		     (x-label-pen (default-label-pen))
		     (y-label-pen (default-label-pen))
		     (tic-length 10)
		     (tic-pen (default-tic-pen))
		     (tic-label-pen (slate:writing-pen))
		     (chart-slate (apply #'slate:make-slate
					 :screen screen
					 :slate-name name
					 slate-options))
		     (chart-interactor (make-instance 'S-Chart-Interactor
					 :slate chart-slate))
		     &allow-other-keys)
  (az:declare-check (type slate:Screen screen)
		    (type g:Chart-Rect chart-rect)
		    (type slate:Screen screen)
		    (type (or String Symbol) name)
		    (type List slate-options)
		    (type Fixnum spacing tic-length)
		    (type String title x-label y-label)
		    (type slate:Pen title-pen x-label-pen y-label-pen tic-pen
			  tic-label-pen)
		    (type slate:Slate chart-slate)
		    (type S-Chart-Interactor chart-interactor))
  (let* ((s-chart (make-instance class
		    :chart-name name
		    :chart-slate chart-slate
		    :chart-rect chart-rect
		    :chart-interactor chart-interactor
		    :spacing spacing :title title :title-pen title-pen
		    :x-label x-label :x-label-pen x-label-pen 
		    :y-label y-label :y-label-pen y-label-pen
		    :tic-length tic-length
		    :tic-pen tic-pen :tic-label-pen tic-label-pen)))
    (setf (chart chart-interactor) s-chart)
    (layout-s-chart s-chart)
    (draw-outer-s-chart s-chart)
    (finish-drawing s-chart) 
    (push s-chart *s-charts*)
    (ac:start-msg-handling chart-interactor)
    s-chart))

