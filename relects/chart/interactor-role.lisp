;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart) 

;;;============================================================
;;; Modes for chart input handling
;;;============================================================

(defclass Chart-Interactor-Role (ac:Interactor-Role Chart-Object) ())

;;;============================================================

(defun kill-role-actor-chart-slate (role actor chart slate
				    &key (destroy-window t))
    (az:kill-object actor) 
    (az:kill-object chart)
    (az:kill-object slate :destroy-window destroy-window)
    (az:kill-object role))



