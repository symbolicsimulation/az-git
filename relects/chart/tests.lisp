;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart)

(eval-when (compile load eval)
  (export '(b+w-chart rgb-chart)))

;;;============================================================
;;; test charts:

(defparameter *chart* nil)
(defparameter *chart-rect* (g:make-chart-rect
			    :left (az:fl -2.0d0)
			    :bottom (az:fl -2.0d0)
			    :right (az:fl 2.0d0)
			    :top (az:fl 2.0d0)))

(defparameter *default-chart* nil)

(defun default-chart (&optional (new? nil))
  (if (null (slate::default-screen))
      (error "No default screen!")
      (when (or new? (null *default-chart*))
	(setf *default-chart*
	      (make-chart
		:screen (slate::default-screen)
		:chart-rect *chart-rect*))))
  *default-chart*)

(defparameter *b+w-chart* ())

(defun b+w-chart (&optional (new? nil))
  (if (null (slate::b+w-screen))
      (error "No b+w screen!")
      (when (or new? (null *b+w-chart*))
	(setf *b+w-chart*
	      (make-chart
		:screen (slate::b+w-screen)
	       :chart-rect *chart-rect*))))
  *b+w-chart*)

(defparameter *rgb-chart* ())

(defun rgb-chart (&optional (new? nil))
  (if (null (slate::rgb-screen))
      (error "No rgb screen!")
      (when (or new? (null *rgb-chart*))
	(setf *rgb-chart*
	      (make-chart
		:screen (slate::rgb-screen)
		:chart-rect *chart-rect*))))
  *rgb-chart*)

;;;============================================================

(defparameter *unit-r* (g:make-chart-rect
			:left 0.0d0
			:right 1.0d0
			:bottom 0.0d0
			:top 1.0d0))

(defparameter *cr* (let ((w (g:chart-rect-width *chart-rect*))
			 (h (g:chart-rect-height *chart-rect*))
			 (l (g:chart-rect-left *chart-rect*))
			 (b (g:chart-rect-bottom *chart-rect*)))
		     (g:make-chart-rect
		       :left (+ l (* 0.0d05 w)) :bottom (+ b (* 0.0d05 h))
		       :width (* 0.50 w) :height (* 0.50 h))))
(defparameter *cr1* (g:make-chart-rect))
(defparameter *poly*
	      (let ((l (g:chart-rect-left *cr*))
		    (b (g:chart-rect-bottom *cr*))
		    (r (g:chart-rect-right *cr*))
		    (tt (g:chart-rect-top *cr*)))
		(list (g:make-chart-point :x l :y b)
		      (g:make-chart-point :x r :y tt)
		      (g:make-chart-point :x (/ (+ l r r) 3.0d0) :y b))))

(defparameter *r1* (let ((w (g:chart-rect-width *chart-rect*))
			 (h (g:chart-rect-height *chart-rect*))
			 (l (g:chart-rect-left *chart-rect*))
			 (b (g:chart-rect-bottom *chart-rect*)))
		     (g:make-chart-rect
		       :left (+ l (* 0.1 w))
		       :bottom (+ b (* 0.1 h))
		       :width (* 0.50 w) :height (* 0.50 h))))

;;;============================================================

(defun one-pixel-larger-chart-rect (chart r result)
  (let ((sr (g:transform (chart-transform chart) r)))
    (g:transform (chart-inverse-transform chart)
		 (g:make-screen-rect :left (- (g:screen-rect-left sr) 1)
				     :top (- (g:screen-rect-top sr) 1)
				     :width (+ (g:screen-rect-width sr) 2)
				     :height (+ (g:screen-rect-height sr) 2))
		 :result result)))

(defun one-pixel-smaller-chart-rect (chart r result)
  (let ((sr (g:transform (chart-transform chart) r)))
    (g:transform (chart-inverse-transform chart)
		 (g:make-screen-rect :left (+ (g:screen-rect-left sr) 1)
				     :top (+ (g:screen-rect-top sr) 1)
				     :width (- (g:screen-rect-width sr) 2)
				     :height (- (g:screen-rect-height sr) 2))
		 :result result)))

;;;============================================================

(defun test-all-drawing-ops (pen chart
			     &key
			     (npoints 10)
			     (seconds 0.1)
			     (print? t))

  (let* ((slate (chart-slate chart))
	 (chart-rect (chart-rect chart))
	 (chart-rect chart-rect)
	 (r (random (/ (+ (g:chart-rect-width chart-rect)
			  (g:chart-rect-height chart-rect))
		       4.0d0)))
	 (r0 (random r))
	 (r1 (random r))
	 (p0 (g::make-random-chart-point chart-rect))
	 (p1 (g::make-random-chart-point chart-rect))
	 (vec0 (g:subtract-chart-points p1 p0))
	 (ps (g::make-random-chart-points chart-rect npoints))
	 (chart-rect0 (g::make-random-chart-rect chart-rect))
	 (chart-rect1 (g::make-random-chart-rect chart-rect))
	 (fill-pen (slate:copy-pen pen))
	 (tile-pen (slate:copy-pen pen)))
    (setf (slate:pen-fill-style fill-pen)
      (slate:make-fill-style :fill-style-type :solid))
    (setf (slate:pen-fill-style tile-pen)
      (slate:make-fill-style :fill-style-type :tiled
			     :fill-style-tile (slate:gray-tile)))
    (when print? (print "draw-character"))
    (draw-character pen chart #\0 p0)
    (draw-character pen chart #\1 p1)
    (let ((code (char-code #\A)))
      (dolist (p ps)
	(draw-character pen chart (code-char code) p)
	(setf code (mod (+ code 1) 128))))
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-vertical-string"))
    (dolist (p ps)
      (draw-vertical-string
	pen chart
	(format nil "~s" (cons (g:chart-point-x p) (g:chart-point-y p)))
	p))
    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-string"))
    (dolist (p ps)
      (draw-point pen chart p)
      (draw-string
	pen chart
	(format nil "~s" (cons (g:chart-point-x p) (g:chart-point-y p)))
	p))
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-point"))
    (dolist (p ps) (draw-point pen chart p))
    (finish-drawing chart)

    (sleep seconds)
    (when print? (print "draw-line"))
    (draw-line pen chart p0 p1)
    (when print? (print "draw-polyline"))
    (draw-polyline pen chart ps)
    (when print? (print "draw-polygon"))
    (draw-polygon pen chart ps)
    (when print? (print "draw-filled-polygon"))
    (draw-polygon fill-pen chart ps)
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-tiled-polygon"))
    (draw-polygon tile-pen chart ps)
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-arrow"))
    (dolist (p ps) (draw-arrow pen chart p0 p))
    (when print? (print "draw-rect"))
    (draw-rect pen chart chart-rect0)
    (draw-rect pen chart chart-rect1)
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-filled-rect"))
    (draw-rect pen chart chart-rect0)
    (draw-rect pen chart chart-rect1)
    (draw-rect fill-pen chart chart-rect0)
    (draw-rect fill-pen chart chart-rect1)
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-tiled-rect"))
    (draw-rect pen chart chart-rect0)
    (draw-rect pen chart chart-rect1)
    (draw-rect tile-pen chart chart-rect0)
    (draw-rect tile-pen chart chart-rect1)
    (finish-drawing chart)
         
    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-ellipse"))
    (draw-rect pen chart chart-rect0)
    (draw-rect pen chart chart-rect1)
    (draw-ellipse pen chart chart-rect0)
    (draw-ellipse pen chart chart-rect1)
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-filled-ellipse"))
    (draw-rect pen chart chart-rect0)
    (draw-rect pen chart chart-rect1)
    (draw-ellipse fill-pen chart chart-rect0)
    (draw-ellipse fill-pen chart chart-rect1)
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-tiled-ellipse"))
    (draw-rect pen chart chart-rect0)
    (draw-rect pen chart chart-rect1)
    (draw-ellipse pen chart chart-rect0)
    (draw-ellipse pen chart chart-rect1)
    (draw-ellipse tile-pen chart chart-rect0)
    (draw-ellipse tile-pen chart chart-rect1)
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-circle"))
    (draw-circle pen chart p0 r0) 
    (draw-circle pen chart p1 r1) 
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-filled-circle"))
    (draw-circle pen chart p0 r0) 
    (draw-circle pen chart p1 r1) 
    (draw-circle fill-pen chart p0 r0) 
    (draw-circle fill-pen chart p1 r1) 
    (finish-drawing chart)

    (sleep seconds)
    (slate:clear-slate slate :region (slate:slate-clipping-region slate))
    (when print? (print "draw-tiled-circle"))
    (draw-circle pen chart p0 r0) 
    (draw-circle pen chart p1 r1) 
    (draw-circle tile-pen chart p0 r0) 
    (draw-circle tile-pen chart p1 r1)
    (finish-drawing chart)
    ))

  
;;;============================================================
;; test horizontal and vertical lines.  Inscribed chart-rects are
;; drawn with xor so there are holes at the corners.

(defun test-hv-lines (pen chart)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (let* ((width (chart-width chart))
	 (height (chart-height chart))
	 (xmax (/ width 2.0d0))
	 (ymax (/ height 2.0d0))
	 (x0 (chart-left chart))
	 (y0 (chart-bottom chart))
	 (dx (/ (- xmax x0) 50.0d0))
	 (dy (/ (- ymax y0) 50.0d0))
	 (p0 (g:make-chart-point :x 0.0d0 :y 0.0d0))
	 (p1 (g:make-chart-point :x 0.0d0 :y 0.0d0))
	 (p2 (g:make-chart-point :x 0.0d0 :y 0.0d0))
	 (p3 (g:make-chart-point :x 0.0d0 :y 0.0d0)))
    (loop (when (or (> x0 xmax) (> y0 ymax)) (return))
      (let ((x1 (- width x0))
	    (y1 (- height y0)))
	(g::set-chart-point-coords p0 :x x0 :y y0)
	(g::set-chart-point-coords p1 :x x0 :y y1)
	(g::set-chart-point-coords p2 :x x1 :y y1)
	(g::set-chart-point-coords p3 :x x1 :y y0)
	(draw-line pen chart p0 p1)
	(draw-line pen chart p1 p2)
	(draw-line pen chart p2 p3)
	(draw-line pen chart p3 p0))
      (incf x0 dx) (incf y0 dy))
    (finish-drawing chart)))

;;;============================================================
;; test non-vertical or horizontal lines

(defun test-slanted-lines (pen chart)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (let* ((left (chart-left chart))
	 (bottom (chart-bottom chart))
	 (right (chart-right chart))
	 (top (chart-top chart))
	 (x0 left)
	 (y0 bottom)
	 (x1 right)
	 (y1 top)
	 (width (chart-width chart))
	 (height (chart-height chart))
	 (max-x (+ left (/ width 2.0d0)))
	 (max-y (+ bottom (/ height 2.0d0)))
	 (dx (/ width 200.0d0))
	 (dy (/ height 200.0d0))
	 (p0 (g:make-chart-point :x x0 :y y0))
	 (p1 (g:make-chart-point :x x1 :y y1)))
    (loop (when (> x0 max-x) (return))
      (setf (g:chart-point-x p0) x0)
      (setf (g:chart-point-x p1) x1)
      (draw-line pen chart p0 p1)
      (rotatef (g:chart-point-x p0) (g:chart-point-x p1))
      (draw-line pen chart p0 p1)
      (incf x0 dx)
      (decf x1 dx))
    (setf (g:chart-point-x p0) left)
    (setf (g:chart-point-x p1) right)
    (loop (when (> y0 max-y) (return))
      (setf (g:chart-point-y p0) y0)
      (setf (g:chart-point-y p1) y1)
      (draw-line pen chart p0 p1)
      (rotatef (g:chart-point-y p0) (g:chart-point-y p1))
      (draw-line pen chart p0 p1)
      (incf y0 dy)
      (decf y1 dy))
    (finish-drawing chart)))

