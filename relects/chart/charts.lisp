;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart)

(eval-when (compile load eval)
  (export '(Chart
	    make-chart
	    drawing-room
	    chart-name
	    chart-rect
	    chart-left chart-right chart-bottom chart-top
	    chart-width chart-height)))


;;;============================================================

(defgeneric chart-rect (chart)
  (declare (type T #||Chart||# chart)
           (values g:Chart-Rect)))

(defgeneric (setf chart-rect) (rect chart)
  (declare (type g:Chart-Rect rect)
           (type T #||Chart||# chart)
           (values rect)))

(defgeneric drawing-room (chart)
  (declare (type T #||Chart||# chart)
           (values g:Screen-Rect)))

(defgeneric (setf drawing-room) (rect chart)
  (declare (type g:Screen-Rect rect)
           (type T #||Chart||# chart)
           (values rect)))

(defgeneric chart-name (chart)
  (declare (type T #||Chart||# chart)
           (values (or Symbol String))))

(defgeneric (setf chart-name) (name chart)
  (declare (type (or Symbol String) name)
           (type T #||Chart||# chart)
           (values name)))

(defgeneric (setf chart-transform) (transform chart)
  (declare (type g:Diagonal-C->S-Map transform)
           (type T #||Chart||# chart)))

(defgeneric (setf chart-inverse-transform) (transform chart)
  (declare (type g:Diagonal-S->C-Map transform)
           (type T #||Chart||# chart)))

;;;============================================================
;;; Charts
;;;============================================================

(defclass Chart-Object (az:Arizona-Object) ())

(defclass Chart (Chart-Object)
     ((chart-slate
        :type slate:Slate
        :reader chart-slate
        :initarg :chart-slate)
      (chart-interactor
       :type Chart-Interactor
       :reader chart-interactor
       :initarg :chart-interactor)
      (chart-name
        :type (or Symbol String)
        :reader chart-name
        :initarg :chart-name)
      (chart-rect
        :type g:Chart-Rect
        :accessor chart-rect
        :initarg :chart-rect)
      (drawing-room
        :type g:Screen-Rect
        :accessor drawing-room
        :initarg :drawing-room)
      (chart-transform
        :type g:Diagonal-C->S-Map
        :reader chart-transform
        :initarg :chart-transform)
      (chart-inverse-transform
        :type (or Nil g:Diagonal-S->C-Map)
        :reader chart-inverse-transform
        :initarg :chart-inverse-transform))
  (:default-initargs
    :chart-rect (g:make-chart-rect
                  :left 0.0d0 :top 0.0d0 :width 1.0d0 :height 1.0d0)
    :chart-name (gentemp "CHART-")
    :chart-interactor (make-instance 'Chart-Interactor)))

;;;------------------------------------------------------------

(defmethod (setf chart-name) (name chart)
  (az:declare-check (type (or Symbol String) name)
		    (type Chart chart))
  (setf (slot-value chart 'chart-name) name)
  (setf (slate:slate-name (chart-slate chart)) name))

;;;------------------------------------------------------------


(defmethod (setf chart-transform) (transform chart)
  (az:declare-check (type g:Diagonal-C->S-Map transform)
		    (type Chart chart))
  (setf (slot-value chart 'chart-inverse-transform) (g:inverse transform))
  (setf (slot-value chart 'chart-transform) transform))

(defmethod (setf chart-inverse-transform) (transform chart)
  (az:declare-check (type g:Diagonal-S->C-Map transform)
		    (type Chart chart))
  (setf (slot-value chart 'chart-transform) (g:inverse transform))
  (setf (slot-value chart 'chart-inverse-transform) transform))

;;;============================================================

(defparameter *charts* ()) ;; for debugging

(defun make-chart (&rest options
                   &key
                   (screen (slate:default-screen))
                   (chart-rect (g:make-chart-rect :width 1.0d0 :height 1.0d0))
		   (name (gentemp "CHART-"))
		   (slate (apply #'slate:make-slate
				 :screen screen
				 :slate-name name
				 options))
		   (interactor (make-instance 'Chart-Interactor
				 :slate slate))
                   &allow-other-keys)
  (az:declare-check (type slate:Screen screen)
		    (type g:Chart-Rect chart-rect)
		    (type Symbol name)
		    (type slate:Slate slate)
		    (type Chart-Interactor interactor))
  (setf options (copy-list options)) ;; be conservative with &rest
  (remf options :chart-rect) ;; so they can be passed to make-slate
  (let ((chart (make-instance 'Chart
		 :chart-name name
		 :chart-slate slate
		 :chart-rect chart-rect
		 :chart-interactor interactor)))
    (setf (drawing-room chart) (slate:slate-rect slate))
    (setf (chart-transform chart)
      (g:make-affine-map-between (chart-rect chart) (drawing-room chart)))
    (setf (chart interactor) chart)
    (push chart *charts*)
    (ac:start-msg-handling (chart-interactor chart))
    chart))

;;;============================================================

(defun chart-left (chart)
  (az:declare-check (type Chart chart))
  (g:chart-rect-left (chart-rect chart)))

(defun chart-right (chart)
  (az:declare-check (type Chart chart))
  (g:chart-rect-right (chart-rect chart)))

(defun chart-bottom (chart)
  (az:declare-check (type Chart chart))
  (g:chart-rect-bottom (chart-rect chart)))

(defun chart-top (chart)
  (az:declare-check (type Chart chart))
  (g:chart-rect-top (chart-rect chart)))
                                            
(defun chart-width (chart)
  (az:declare-check (type Chart chart))
  (g:chart-rect-width (chart-rect chart)))

(defun chart-height (chart)
  (az:declare-check (type Chart chart))
  (g:chart-rect-height (chart-rect chart)))

;;;============================================================

#||
(defun transform-for-chart (chart x
                            &key
                            (result (g:c<->s-make-matching-object x)))
  (az:declare-check (type Chart chart)
		    (type (or g:Chart-Vector g:Chart-Point g:Chart-Rect) x))
  (g:c<->s-check-type-match x result) 
  (g:transform (chart-transform chart) x :result result))

(defun inverse-transform-for-chart (chart x
                                    &key
                                    (result (g:c<->s-make-matching-object x)))
  (az:declare-check (type Chart chart)
		    (type (or g:Screen-Vector g:Screen-Point g:Screen-Rect) x))
  (g:c<->s-check-type-match x result) 
  (g:transform (chart-inverse-transform chart) x :result result))
||#

;;;============================================================

(defun chart-clipping-region (chart &key (result (g:make-chart-rect)))
  (az:declare-check (type Chart chart)
		    (type g:Chart-Rect result))
  
  (g:transform (chart-inverse-transform chart) 
	       (slate:slate-clipping-region (chart-slate chart)
					    :result result)))

(defun set-chart-clipping-region (chart region)
  (az:declare-check (type Chart chart)
		    (type g:Chart-Rect region))  
  
  (setf (slate:slate-clipping-region (chart-slate chart))
    (g:transform (chart-transform chart) region)))

(defsetf chart-clipping-region (chart) (region)
  `(set-chart-clipping-region ,chart ,region))

(defmacro with-clipping-region ((chart clipr) &body body)
  (let ((screen-clipr (gensym "screen-clipr-")))
    (gensym "G")
    (az:once-only
     (chart clipr)
     `(g:with-borrowed-screen-rect
       (,screen-clipr)
       (slate:with-clipping-region
	((chart-slate ,chart)
	 (unless (null ,clipr)
	   (g:transform (chart-transform ,chart)
			,clipr :result ,screen-clipr)))
	,@body)))))