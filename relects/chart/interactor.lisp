;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart) 

;;;============================================================
;;; Objects to handle input to Charts
;;;============================================================

(defclass Chart-Interactor (ac:Interactor Chart-Object)
	  ((chart
	    :type Chart
	    :accessor chart
	    :initarg :chart)))

;;;------------------------------------------------------------

(defmethod slate:slate ((actor Chart-Interactor))
  (chart-slate (chart actor)))

;;;------------------------------------------------------------

(defmethod ac:build-roles ((actor Chart-Interactor))
  (setf (ac:actor-default-role actor)
    (make-instance 'Chart-Interactor-Role :role-actor actor)))



