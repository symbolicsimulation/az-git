;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart)

(eval-when (compile load eval)
  (export '(2d-Strip-Chart
	    make-2d-strip-chart
	    strip-points
	    strip-crawl
	    draw-function)))

;;;============================================================
;;; Strip Chart Pen's
;;;============================================================

(defparameter *point-drawing-pen* ())

(defun point-drawing-pen ()
  (when (null *point-drawing-pen*)
    (setf *point-drawing-pen* (slate:make-pen :pen-pixel-value 
					      (slate:colorname->pixel-value
					       (slate:screen-colormap
						(slate:default-screen))
					       :wheat)
					      )))
  *point-drawing-pen*)

(defparameter *point-erasing-pen* ())

(defun point-erasing-pen ()
  (when (null *point-erasing-pen*)
    (setf *point-erasing-pen* (slate:make-pen :pen-pixel-value 0)))
  *point-erasing-pen*)

;;;============================================================
;;; Strip Chart's
;;;============================================================

(defclass 2D-Strip-Chart (S-Chart)
	  ((strip-points
	    :type g:Chart-Point-List
	    :accessor strip-points
	    :initarg :strip-points)
	   (strip-drawing-pen
	    :type slate:Pen
	    :accessor strip-drawing-pen
	    :initarg :strip-drawing-pen)
	   (strip-erasing-pen
	    :type slate:Pen
	    :accessor strip-erasing-pen
	    :initarg :strip-erasing-pen))
  (:default-initargs
   :strip-points ()
   :strip-drawing-pen (point-drawing-pen) 
   :strip-erasing-pen (point-erasing-pen)))

;;;============================================================

(defun draw-inner-strip-chart (pen strip-chart)
  (az:declare-check (type slate:Pen pen)
		    (type 2D-Strip-Chart strip-chart))
  (let ((strip-points (strip-points strip-chart)))
    (when (rest strip-points)
      (%draw-polyline pen strip-chart strip-points))))

;;;============================================================

(defun draw-strip-chart (strip-chart)
  (az:declare-check (type 2D-Strip-Chart strip-chart))
  (with-clipping-region (strip-chart nil)
			(slate:clear-slate (chart-slate strip-chart)))
  (draw-outer-s-chart strip-chart)
  (draw-inner-strip-chart (strip-drawing-pen strip-chart) strip-chart)
  (finish-drawing strip-chart))

;;;============================================================

(defclass Strip-Chart-Interactor-Role (S-Chart-Interactor-Role) ())

(defmethod slate:configure-notify ((role Strip-Chart-Interactor-Role)
				   x y width height)
  x y width height
  (let ((strip-chart (chart (ac:actor role))))
    (layout-s-chart strip-chart)
    (draw-strip-chart strip-chart)
    (finish-drawing strip-chart)))

(defmethod slate:exposure ((role Strip-Chart-Interactor-Role)
			   x y w h count)
  x y w h count
  (let ((strip-chart (chart (ac:actor role))))
    (draw-strip-chart strip-chart)
    (finish-drawing strip-chart)))

;;;------------------------------------------------------------

(defclass Strip-Chart-Interactor (S-Chart-Interactor) ())

(defmethod ac:build-roles ((interactor Strip-Chart-Interactor))
  (setf (ac:default-role interactor)
    (make-instance 'Strip-Chart-Interactor-Role :actor interactor)))

;;;============================================================

(defparameter *strip-charts* ()) ;; for debugging

(defun make-2d-strip-chart (&rest options
			    &key
			    (strip-points ())
			    (screen (slate:default-screen))
			    (name (gentemp "S-CHART-"))
			    (slate-options ())
			    (chart-slate (apply #'slate:make-slate
						:screen screen
						:slate-name name
						slate-options))
			    (chart-interactor
			     (make-instance 'Strip-Chart-Interactor
			       :slate chart-slate))
			    &allow-other-keys)
  (az:declare-check (type g:Chart-Point-List strip-points)
		    (type slate:Screen screen)
		    (type (or String Symbol) name)
		    (type List slate-options)
		    (type slate:Slate chart-slate)
		    (type Strip-Chart-Interactor chart-interactor))
  (let* ((chart-rect
	  (if (null strip-points)
	      (g:make-chart-rect :width 1.0d0 :height 1.0d0)
	    (g::chart-rect-covering-points strip-points)))
	 (strip-chart
	  (apply #'make-s-chart
		 :class '2D-Strip-Chart
		 :chart-slate chart-slate
		 :chart-rect chart-rect
		 :chart-interactor chart-interactor
		 options)))
    (push strip-chart *strip-charts*)
    strip-chart))

(defun layout-strip-chart (strip-chart)
  (let ((strip-points (strip-points strip-chart)))
    (setf (chart-rect strip-chart)
      (cond
       ((null strip-points)
	(g:make-chart-rect :width 1.0d0 :height 1.0d0))
       ((null (rest strip-points))
	(let ((center (first strip-points)))
	  (g:make-chart-rect :left (- (g:chart-point-x center) 
				      (az:fl 0.5d0))
			     :bottom (- (g:chart-point-y center)
					(az:fl 0.5d0))
			     :width 1.0d0
			     :height 1.0d0)))
       
       (t
	(let ((r (g:chart-rect-covering-points strip-points)))
	  (when (minusp (g:chart-rect-height r))
	    (break))
	  r))))
    (layout-s-chart strip-chart)))
  

;;;------------------------------------------------------------

;; conservative version:
;(defun strip-crawl (strip-chart p)
;  (az:declare-check (type 2D-Strip-Chart strip-chart)
;  (type g:Chart-Point p))
;  (let ((points (cons p (strip-points strip-chart))))
;    (cond
;      ((and (rest points)
;	    (g:chart-points-in-rect? points (chart-rect strip-chart)))
;       (draw-inner-strip-chart (strip-erasing-pen strip-chart) strip-chart)
;       (setf (strip-points strip-chart) points)
;       (draw-inner-strip-chart (strip-drawing-pen strip-chart) strip-chart))
;      (t
;       (setf (strip-points strip-chart) points)
;       (layout-strip-chart strip-chart)
;       (draw-strip-chart strip-chart)))))

(defun strip-crawl (strip-chart p)
  (az:declare-check (type 2D-Strip-Chart strip-chart)
		    (type g:Chart-Point p))
  (let ((points (push p (strip-points strip-chart))))
    (cond
     ((and (rest points)
	   (g::%chart-points-in-rect? points (chart-rect strip-chart)))
      (draw-line (strip-drawing-pen strip-chart) strip-chart
		 (second points) (first points)))
     (t
      (layout-strip-chart strip-chart)
      (draw-strip-chart strip-chart)
      ;;(break)
      ))
    (finish-drawing strip-chart)))

;;;------------------------------------------------------------

(defparameter *function-chart* ())

(defun function-chart (&optional (new-chart? nil))
  (when (or (null *function-chart*) new-chart?)
    (setf *function-chart* (make-2d-strip-chart)))
  *function-chart*)

(defun draw-function (f a b
		      &key
		      (nsteps 50)
		      (new-chart? nil)
		      (title (gentemp "Function-Chart-"))
		      (x-label nil)
		      (y-label nil))
  (let ((chart (function-chart new-chart?))
	(dx (/ (- b a) (az:fl nsteps)))
	(x a))
    (when title (setf (title chart) title))
    (when x-label (setf (x-label chart) x-label))
    (when y-label (setf (y-label chart) y-label))
    (setf (strip-points chart)
      (az:with-collection
       (dotimes (i (+ nsteps 1))
	 (az:collect (g:make-chart-point :x x :y (funcall f x)))
	 (incf x dx))))
    (layout-strip-chart chart)
    (draw-strip-chart chart)))

