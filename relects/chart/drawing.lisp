;;;-*- Package: :Chart; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Chart)

(eval-when (compile load eval)
  (export '(finish-drawing force-drawing

	    ;; drawing points (pixels)
	    draw-point
	    draw-points

	    ;; drawing lines
	    draw-line draw-lines 
	    draw-polyline
	    draw-polygon 
	    draw-arrow draw-arrows 

	    ;; drawing chart-rects
	    clear-chart

	    draw-rect draw-rects
         
	    ;; drawing ellipses
	    draw-ellipse draw-ellipses 
	    draw-circle draw-circles 
         
	    ;; drawing strings and characters
	    draw-character draw-characters
	    draw-string draw-strings
	    draw-centered-string draw-centered-strings
	    draw-vertical-string draw-vertical-strings
	    draw-centered-vertical-string draw-centered-vertical-strings
	    )))
;;;============================================================
;;; flushing output
;;;============================================================

(defun %finish-drawing (chart) (slate:finish-drawing (chart-slate chart)))

(defun finish-drawing (chart)
  (az:type-check Chart chart)
  (%finish-drawing chart))

(defun %force-drawing (chart) (slate:force-drawing (chart-slate chart)))

(defun force-drawing (chart)
  (az:type-check Chart chart)
  (%force-drawing chart))

;;;============================================================
;;; drawing points
;;;============================================================

(defgeneric %draw-point (pen chart p) 
  (declare (type Pen pen)
           (type Chart chart)
           (type g:Chart-Point p)))

(defmethod %draw-point (pen chart p)
  (g:with-borrowed-screen-point (sp)
    (slate:draw-point pen (chart-slate chart)
                      (g:transform (chart-transform chart) p :result sp))))

(defun draw-point (pen chart p) 
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point p)
  (%draw-point pen chart p))

(defun draw-points (pen chart points)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point-List points)
  (dolist (p points) (%draw-point pen chart p)))

;;;============================================================
;;; Absolute line drawing
;;;============================================================

(defgeneric %draw-line (pen chart p0 p1)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type g:Chart-Point p0 p1)))

(defmethod %draw-line (pen chart p0 p1)
  (g:with-borrowed-screen-point
   (s0)
   (g:with-borrowed-screen-point
    (s1)
    (slate:draw-line
     pen (chart-slate chart)
     (g::%transform-c->s-point (chart-transform chart) p0 s0)
     (g::%transform-c->s-point (chart-transform chart) p1 s1)))))

(defun draw-line (pen chart p0 p1)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point p0 p1)
  (%draw-line pen chart p0 p1))

(defun draw-lines (pen chart points0 points1)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point-List points0 points1)
  (loop
   for p0 in points0
   for p1 in points1 do
    (%draw-line pen chart p0 p1)))

;;;============================================================
;;; Arrows
;;;============================================================

(defgeneric %draw-arrow (pen chart p0 p1)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type g:Chart-Point p0 p1))) 

(defmethod %draw-arrow (pen chart p0 p1)
  (g:with-borrowed-screen-point
   (s0)
   (g:with-borrowed-screen-point
    (s1)
    (slate:draw-arrow pen (chart-slate chart)
                      (g:transform (chart-transform chart) p0 :result s0)
                      (g:transform (chart-transform chart) p1 :result s1)))))

(defun draw-arrow (pen chart p0 p1)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point p0 p1)
  (%draw-arrow pen chart p0 p1))

(defun draw-arrows (pen chart points0 points1)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point-List points0 points1)
  (loop for p0 in points0
   for p1 in points1 do
    (%draw-arrow pen chart p0 p1)))

;;;============================================================
;;; Polylines (Piecewise Linear Curves)
;;;============================================================

(defgeneric %draw-polyline (pen chart p-list)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type g:Chart-Point-List p-list)))

(defmethod %draw-polyline (pen chart p-list)
  (g::%with-borrowed-screen-point-list
   (sp-list (length p-list))
   (loop for p in p-list
    for sp in sp-list do
     (g::%transform-c->s-point (chart-transform chart) p sp))
   (slate::%draw-polyline pen (chart-slate chart) sp-list)))

(defun draw-polyline (pen chart p-list)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point-List p-list)
  (%draw-polyline pen chart p-list))

;;;============================================================
;;; drawing chart-rects
;;;============================================================

(defgeneric %draw-rect (pen chart rect)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type g:Chart-rect rect)))

(defmethod %draw-rect (pen chart r)
  (g:with-borrowed-screen-rect (sr)
    (slate:draw-rect pen (chart-slate chart)
                     (g:transform (chart-transform chart) r :result sr))))

(defun draw-rect (pen chart rect)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-rect rect)
  (%draw-rect pen chart rect))

(defun draw-rects (pen chart rects)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Rect-List rects)
  (dolist (rect rects) (%draw-rect pen chart rect))) 

;;;============================================================

(defun clear-chart (chart &key (region (chart-rect chart)))
  (az:type-check Chart chart)
  (g:with-borrowed-screen-rect (sr)
    (slate:clear-slate
      (chart-slate chart) 
      :region (g:transform (chart-transform chart) region :result sr))))

;;;============================================================
;;; Ellipses
;;;============================================================

(defgeneric %draw-ellipse (pen chart rect)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (g:Chart-rect rect)))

(defmethod %draw-ellipse (pen chart r)
  (g:with-borrowed-screen-rect (sr)
    (slate:draw-ellipse pen (chart-slate chart)
                        (g:transform (chart-transform chart) r :result sr))))

(defun draw-ellipse (pen chart rect)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Rect rect)
  (%draw-ellipse pen chart rect))

(defun draw-ellipses (pen chart rects)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Rect-List rects)
  (dolist (rect rects) (%draw-ellipse pen chart rect)))

;;;============================================================
;;; Circles
;;;============================================================

(defgeneric %draw-circle (pen chart p r)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type g:Chart-Point p)
           (type g:Positive-Chart-coordinate r)))

(defmethod %draw-circle (pen chart p r)
  (g:with-borrowed-chart-rect (wr)
    (draw-ellipse pen chart (g:chart-rect-enclosing-circle p r :result wr))))

(defun draw-circle (pen chart p r)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point p)
  (az:type-check g:Positive-Chart-coordinate r)
  (%draw-circle pen chart p r))

(defun draw-circles (pen chart points radii)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point-List points)
  (az:type-check g:Positive-Chart-Coordinate-List radii)
  (loop for p in points
   for r in radii do
    (%draw-circle pen chart p r)))

;;;============================================================
;;; Polygons
;;;============================================================

(defgeneric %draw-polygon (pen chart p-list)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type g:Chart-Point-List p-list)))

(defmethod %draw-polygon (pen chart p-list)
  (g::with-borrowed-screen-point-list
   (sp-list (length p-list))
   (slate:draw-polygon
    pen (chart-slate chart)
    (mapcar #'(lambda (p sp)
		(g:transform (chart-transform chart) p :result sp))
	    p-list sp-list))))

(defun draw-polygon (pen chart p-list)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check g:Chart-Point-List p-list)
  (%draw-polygon pen chart p-list))

;;;============================================================
;;; Drawing Characters
;;;============================================================

(defgeneric %draw-character (pen chart char p)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type Character char)
           (type g:Chart-Point p)))

(defmethod %draw-character (pen chart char p)
  (g:with-borrowed-screen-point (sp)
    (draw-character pen (chart-slate chart) char
		    (g:transform (chart-transform chart) p :result sp))))

(defun draw-character (pen chart char p)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check Character char)
  (az:type-check g:Chart-Point p)
  (%draw-character pen chart char p))

(defun draw-characters (pen chart chars points)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (assert (every #'characterp chars))
  (az:type-check g:Chart-Point-List points)
  (loop for char in chars
   for p in points do
    (%draw-character pen chart char p)))

;;;============================================================
;;; Drawing Strings
;;;============================================================

(defgeneric %draw-string (pen chart string p)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type String string)
           (type g:Chart-Point p))) 

(defmethod %draw-string (pen chart string p)
  (g:with-borrowed-screen-point (sp)
    (slate:draw-string pen (chart-slate chart) string
                          (g:transform (chart-transform chart) p :result sp))))

(defun draw-string (pen chart string p)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check String string)
  (az:type-check g:Chart-Point p)
  (%draw-string pen chart string p))

(defun draw-strings (pen chart strings points)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (assert (every #'stringp strings))
  (az:type-check g:Chart-Point-List points)
  (loop for string in strings
   for p in points do
    (%draw-string pen chart string p)))

;;;============================================================
;;; Drawing Vertical Strings
;;;============================================================

(defgeneric %draw-vertical-string (pen chart string p)
  (declare (type slate:Pen pen)
           (type Chart chart)
           (type String string)
           (type g:Chart-Point p)))

(defmethod %draw-vertical-string (pen chart string p)
  (g:with-borrowed-screen-point (sp)
    (slate:draw-vertical-string pen (chart-slate chart) string
                          (g:transform (chart-transform chart) p :result sp))))

(defun draw-vertical-string (pen chart string p)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (az:type-check String string)
  (az:type-check g:Chart-Point p)
  (%draw-vertical-string pen chart string p))

(defun draw-vertical-strings (pen chart strings points)
  (az:type-check slate:Pen pen)
  (az:type-check Chart chart)
  (assert (every #'stringp strings))
  (az:type-check g:Chart-Point-List points)
  (loop for string in strings
   for p in points do
    (%draw-vertical-string pen chart string p)))



