;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: User; -*-
;;;
;;; Copyright 1987. John Alan McDonald. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(export '(polish-fortran))

;;;=================================================================
;;;			   The "Card Reader"
;;;=================================================================

(defconstant *white-chars*
	     '(#\space #\newline #\return #\linefeed #\tab #\page))

(defun white-space? (char) (member char *white-chars*))

(defun blank? (line) (every #'white-space? line))

;;;=================================================================

(defparameter *lisp-readtable* (copy-readtable nil))
  
;;;-----------------------------------------------------------------

(defmacro with-readtable ((readtable) &body body)

  `(let ((*readtable* ,readtable))
     (declare (special *readtable*))
     ,@body
     )
  )

;;;-----------------------------------------------------------------

(defun read-concatenated (&rest parts)
  (with-readtable (*lisp-readtable*)
    (values (read-from-string (format nil "~{~a~}" parts)))
    )
  )

;;;=================================================================

#-(or xerox ti)
(defun fortran-op-reader (stream char) 
  (declare (ignore stream))
  (values (intern (string char) 'user))
  )

;;;-----------------------------------------------------------------

;;; (1-87) these are needed because of a bug in xerox's (and ti's)
;;; implementation of reader macros. should be fixed eventually...

#+(or xerox ti)
(defun fortran-comma-reader (stream char) 
  (declare (ignore stream char))
  (values (intern (string #\,) 'user))
  )

#+(or xerox ti)
(defun fortran-+-reader (stream char) 
  (declare (ignore stream char))
  (values (intern (string #\+) 'user))
  )

#+(or xerox ti)
(defun fortran---reader (stream char) 
  (declare (ignore stream char))
  (values (intern (string #\-) 'user))
  )

#+(or xerox ti)
(defun fortran-/-reader (stream char) 
  (declare (ignore stream char))
  (values (intern (string #\/) 'user))
  )

#+(or xerox ti)
(defun fortran-=-reader (stream char) 
  (declare (ignore stream char)) 
  (values (intern (string #\=) 'user))
  )

;;;-----------------------------------------------------------------

(defun fortran-*-reader (stream char)
  (declare (ignore char)) 
  (if (char-equal #\* (peek-char nil stream))
      ;; then it's exponentiation
      (progn (read-char stream) '**)
      ;; else it's multiplication
      '*					
      )
  )

;;;-----------------------------------------------------------------

(defvar *fortran-ops* '(#\+ #\- #\* #\/ #\= #\, #\( #\)))

(defvar *fortran-logicals*
	'(.true .false .not .and .or .lt .le .eq .ne .ge .gt))

(defun fortran-.-reader (stream char) 
  (declare (ignore char)) 
  (do* ((c #\. (read-char stream nil nil t))
	(chars (list c) (nconc chars (list c)))
	(pc (peek-char nil stream nil nil t) (peek-char nil stream nil nil t))
	)
       (nil)
    (cond
      ((or (null pc) (white-space? pc) (member pc *fortran-ops*))
       (if (= (length chars) 1) ;; '(#\.)
	   (return '|.|) 
	   ;; else
	   (return (apply #'read-concatenated chars))
	   )
       )
      ((char-equal pc #\.)
       (let ((sym (apply #'read-concatenated chars)))
	 (if (member sym *fortran-logicals*)
	     (setf sym
		   (apply #'read-concatenated 
			  (nconc chars (list (read-char stream nil nil t))))))
	 (return sym)
	 )
       )
      )
    )
  )
  
;;;-----------------------------------------------------------------
  
(defun fortran-quote-reader (stream char)
  "Read a fortran string constant"
  (declare (ignore char)) 
  (do ((c (read-char stream nil nil t) (read-char stream nil nil t))
	(chars '())
	)
       (nil)
    (if (char-equal #\' c)
      (if (char-equal #\' (peek-char nil stream nil nil t))
	  ;; then it's an escaped quote within the string
	    (push (read-char stream nil nil t) chars)
	  ;; else we're done
	    (return (concatenate 'string (nreverse chars)))
	    )
      ;; else
      (push c chars)
      )
    )
  )

;;;-----------------------------------------------------------------
;;;
;;; Note that the logical operators, .eq., .and., etc.,
;;; will need to be surrounded by spaces.
;;;

(defun make-fortran-readtable ()
  
  #-(or xerox ti)
  (let ((readtable (copy-readtable nil)))
    (set-macro-character #\+ #'fortran-op-reader nil readtable)
    (set-macro-character #\- #'fortran-op-reader nil readtable)
    (set-macro-character #\/ #'fortran-op-reader nil readtable)
    (set-macro-character #\= #'fortran-op-reader nil readtable)
    (set-macro-character #\. #'fortran-.-reader nil readtable)
    (set-macro-character #\* #'fortran-*-reader nil readtable)
    (set-macro-character #\, #'fortran-op-reader nil readtable)
    (set-macro-character #\' #'fortran-quote-reader nil readtable)
    readtable
    )
  #+(or xerox ti)
  (let ((readtable (copy-readtable nil)))
    (set-macro-character #\+ #'fortran-+-reader nil readtable)
    (set-macro-character #\- #'fortran---reader nil readtable)
    (set-macro-character #\/ #'fortran-/-reader nil readtable)
    (set-macro-character #\= #'fortran-=-reader nil readtable)
    (set-macro-character #\. #'fortran-.-reader nil readtable)
    (set-macro-character #\* #'fortran-*-reader nil readtable)
    (set-macro-character #\, #'fortran-comma-reader nil readtable)
    (set-macro-character #\' #'fortran-quote-reader nil readtable)
    readtable
    )
  )
  
;;-----------------------------------------------------------------

(defparameter *fortran-readtable* (make-fortran-readtable))

;;;=================================================================

(defun complete-exponent? (sym)
  (and (symbolp sym)
       (char-equal #\. (char (symbol-name sym) 0))
       (> (length (symbol-name sym)) 2)
       (or (char-equal #\e (char (symbol-name sym) 1))
	   (char-equal #\d (char (symbol-name sym) 1))
	   )
       (numberp (with-readtable (*lisp-readtable*)
		  (read-from-string (subseq (symbol-name sym) 2)) 
		  )
		)
       )
  )

;;;-----------------------------------------------------------------

(defun broken-exponent? (sym)
  (and (symbolp sym)
       (char-equal #\. (char (symbol-name sym) 0))
       (> (length (symbol-name sym)) 1)
       (let* ((pname (symbol-name sym))
	      (l (- (length pname) 1))
	      (last-char (char pname l))
	      (dec (if (= 2 (length pname))
		       0
		       (with-readtable (*lisp-readtable*)
			 (read-from-string (subseq pname 0 l)) 
			 )
		       )
		   )
	      )
	 (and (or (char-equal #\e last-char) (char-equal #\d last-char))
	      (or (null dec) (numberp dec))
	      )
	 )
       )
  )

;;;-----------------------------------------------------------------

(defun fix-tokens (tokens)
  (do ((in tokens) (out nil))
      ((null in) (nreverse out))
    (labels ((fix-token (token)
	       (cond
		 ((listp token) (fix-tokens token))
		 ((eql token 't)       'fortran-t)
		 ((eql token 'nil)     'fortran-nil)
		 ((eql token '.true.)  t)
		 ((eql token '.false.) nil)
		 ;; split floating point numbers, like 1 .2e3:
		 ((and (numberp (first out)) (numberp token))
		  (+ token (pop out))
		  )
		 ((and (eql token '|.|) (numberp (first out)))
		  (float (pop out) 1.0)
		  )
		 ;; like 1 .e3
		 ((complete-exponent? token)
		  (if (numberp (first out))
		      (read-concatenated (pop out) token)
		      (error "exponent without a base!")
		      )
		  )
		 ;; like 1 .2e - 3
		 ((broken-exponent? token)
		  (if (numberp (first out))
		      (read-concatenated (pop out) token (pop in) (pop in))
		      (read-concatenated token (pop in) (pop in))
		      )
		  )
		 (t token)
		 )
	       ))
      (push (fix-token (pop in)) out)
      )
    )
  )

;;;-----------------------------------------------------------------

(defun comment-line? (line)
  (and (not (null line)) (char-equal #\c (char line 0))))
  
(defun continuation-line? (line)
  (and (not (comment-line? line))
       (>= (length line) 6)
       (not (find #\tab line :start 0 :end 5 :test #'char-equal))
       (not (char-equal #\space (char line 5)))))

;;;-----------------------------------------------------------------

(defun read-fortran-line (line)
  (if (comment-line? line)
      `(comment ,(subseq line 1))
      ;; else
      (with-input-from-string (in line)
	(with-readtable (*fortran-readtable*)
	  ;; end of file returns '.eol.,
	  ;; because there might be a fortran variable called <nil>.
	  (do* ((token (read in nil '.eol.) (read in nil '.eol.)) 
		(tokens nil)
		)
	       ((eql '.eol. token)
		(fix-tokens (nreverse tokens))
		)
	    (push token tokens)
	    ) 
	  )
	)
      )
  )

;;;-----------------------------------------------------------------

(defun list-lines (fortran-file)

  "Read a file into a list of strings, one for each line in the file."
  
  (with-open-file (input fortran-file)
    (do ((line (read-line input nil nil) (read-line input nil nil))
	 (lines nil)
	 )
	((null line)
	 (nreverse lines)
	 )
      (if (> (length line) 72) (setf line (subseq line 0 72)))
      (push line lines)
      )
    )
  )

;;;=================================================================

(defun read-cards (lines &optional &key (keep-fortran? nil))
  
  (do ((statements nil)
       (line (pop lines) (pop lines))
       ) 
      ((null line)
       (nreverse statements)
       )
    ;; concatenate continuation lines
    (unless (comment-line? line)
      (setf line
	    (apply #'concatenate 'string
		   line
		   (let ((cs nil))
		     (loop (if (continuation-line? (first lines))
			       (push (subseq (pop lines) 6) cs)
			       ;; else
			       (return (nreverse cs))
			       )
			   )
		     )
		   )
	    )
      )
    ;; keep the fortran as a comment?
    (when (and keep-fortran? (not (comment-line? line)))
      (push `(comment ,line) statements)
      )
    (push (read-fortran-line line) statements)
    )
  )


;;;======================================================================
;;;		      Infix to prefix translation
;;;======================================================================

(defvar *operators*
	'(.not. .or. .and. .gt. .ge. .eq. .le. .lt. .ne.
		+ - * /	^ ** =
		#+ti global:/ ;; something screwy in the ti reader.
		)
  )

;;;-----------------------------------------------------------------

(defun operator? (term) (member term *operators*))

;;;-----------------------------------------------------------------

(defun operator-weight (operator)

  (ecase operator
    (=      0)
    ((.eqv. .neqv.) 1)
    (.or.   2)
    (.and.  3)
    (.not.  4)
    ((.gt. .ge. .eq. .le. .lt. .ne.) 5)
    (+      6)
    (-      7)
    (*      8)
    ((/ #+ti global:/)     9)
    (**    10)
    )
  )

(defun operator> (operator1 operator2)
  (> (operator-weight operator1)
     (operator-weight operator2)
     )
  )
       
;;;-----------------------------------------------------------------

(defun lisp-function (operator)

  (ecase operator
    (=      'setf)
    ((+ - *)  operator)
    ((/ #+ti global:/)  'fortran-/) ;; fortran / is different from common lisp
    (**     'expt)
    (.or.   'or)
    (.and.  'and)
    (.gt.   '>)
    (.ge.   '>=)
    (.eq.   '=)
    (.le.   '<=)
    (.lt.   '<)
    (.ne.   '/=)
    (.not.  'not)
    )
  )

;;;-----------------------------------------------------------------
      
(defun merge-operator-operand-stacks (operators operands)

  (if (null operators)
      (first operands)
      ;; else
      (let ((fun (lisp-function (pop operators)))
	    (arg2 (pop operands))
	    (arg1 (pop operands))
	    )
	(push `(,fun ,arg1 ,arg2) operands)
	(merge-operator-operand-stacks operators operands)
	)
      )
  )

;;;-----------------------------------------------------------------

(defun infix-to-prefix (terms)
  
  (if (member '|,| terms)
      (parse-comma-list terms)
      ;; else
      (let ((operators nil) (operands nil))
	;; - - - - - - - - - - - - - - - - - - - - - - - - - -
	(labels
	  ((collect-operand! ()
	     (let ((term (pop terms)))
	       (cond
		 ;; collect a unary operator's operand and push
		 ;; the lisp expression on the operand stack.
		 ((operator? term)
		  (collect-operand!)
		  (push (list (lisp-function term) (pop operands))
			operands)
		  )
		 ;; a sub-expression to be parsed.
		 ((consp term) (push (infix-to-prefix term) operands))
		 ;; must be function or array, if arg list follows.
		 ((consp (first terms))
		  (push (cons term (parse-comma-list (pop terms)))
			operands)
		  )
		 ;; else must be a constant or simple variable
		 (t (push term operands)) 
		 )
	       )
	     )
	   ;; - - - - - - - - - - - - - - - - - - - - - - - - - -
	   (collect-operator! () 
	     (if (and (not (null operators)) 
		      (operator> (first operators) (first terms))
		      )
		 ;; if the top of the operator stack has precedence,
		 ;; it can gobble up the top two operands and go on the
		 ;; operand stack.
		 (let ((function (lisp-function (pop operators)))
		       (arg2 (pop operands))
		       (arg1 (pop operands))
		       )
		   (push `(,function ,arg1 ,arg2) operands)
		   (collect-operator!) ;; try again
		   )
		 ;; else
		 (push (pop terms) operators)
		 )
	     )
	   )
	  ;; - - - - - - - - - - - - - - - - - - - - - - - - - -
	  (if (atom terms)
	      ;; then there's nothing to do.
	      terms
	      ;; else alternate between operands and operators
	      (loop
		 (collect-operand!)
		 (when (null terms)
		   (return (merge-operator-operand-stacks operators operands))
		   )
		 (collect-operator!)
		 )
	      )
	  )
	)
      )
  )

;;;-----------------------------------------------------------------

(defun parse-comma-list (comma-list)
  
  ;; takes a list of infix expressions seperated by |,|'s
  ;; and returns a list of prefix expressions
  
  (let ((tokens nil))
    (if (atom comma-list)
	comma-list
	(cons
	  (infix-to-prefix (loop (let ((term (pop comma-list)))
				       (when (or (null term) (eql term '|,|))
					 (return (nreverse tokens))
					 )
				       (push term tokens)
				       )
			      )
			   )
	  (parse-comma-list comma-list)
	  )
	)
    )
  )

;;;=================================================================
;;; Complex numbers:
;;;=================================================================

(defun minus-number? (form)
  (and (consp form) (eql '- (first form)) (numberp (second form)))
  )

(defun fortran-number? (form)
  (or (numberp form) (minus-number? form))
  )

(defun complex-constant? (form)
  
  (and (consp form)
       (= 2 (length form))
       (fortran-number? (first form))
       (fortran-number? (second form))
       )
  )
       
(defun fix-complex-constants (form)

  (cond
    ((atom form) form)
    ((complex-constant? form)
     (complex (eval (first form)) (eval (second form))))
    ;; eval in case it's something like ((- 1) 0)
    (t (mapcar #'fix-complex-constants form))
    )      
  )


;;;=================================================================
;;;		     Single Statement Translations
;;;=================================================================
;;; Statement classification:

(defun statement-type? (statement-type s)
  
  (and (consp s)
       (if (numberp (first s)) ;; in case there's a label
	   (eql statement-type (second s))
	   (eql statement-type (first s)))))

(defun end?        (s) (statement-type? 'end       s))
(defun stop?       (s) (statement-type? 'stop      s))
(defun data?       (s) (statement-type? 'data      s))
(defun save?       (s) (statement-type? 'save      s))
(defun do?         (s) (statement-type? 'do        s))
(defun continue?   (s) (statement-type? 'continue  s))
(defun comment?    (s) (statement-type? 'comment   s))
(defun implicit?   (s) (statement-type? 'implicit  s))
(defun external?   (s) (statement-type? 'external  s))
(defun intrinsic?  (s) (statement-type? 'intrinsic  s))
(defun dimension?  (s) (statement-type? 'dimension s))
(defun assign?     (s) (statement-type? 'assign    s))
(defun return?     (s) (statement-type? 'return    s))
(defun if?         (s) (statement-type? 'if        s))
(defun else?       (s) (statement-type? 'else      s))
(defun endif?      (s) (statement-type? 'endif     s))
(defun call?       (s) (statement-type? 'call      s))
(defun subroutine? (s) (statement-type? 'subroutine s))
(defun program?    (s) (statement-type? 'program s))

(defun function?   (s) (or (eql 'function (first  s))
			   (eql 'function (second s))
			   (eql 'function (third  s))))

(defun type-decl?  (s) (and (consp s)
			    (member (first s)
				    '( integer real double
				      complex logical character))))

(defun declaration? (f) (and (consp f)
			     (member (first f)
				     '( integer real double
				      complex logical character
				      implicit data save
				      dimension external intrinsic))))

(defun setf? (s) (or (eql (second s) '=)
		     (eql (third  s) '=)
		     (eql (fourth s) '=)))

(defun goto? (s) (or (statement-type? 'goto s)
		     (statement-type? 'go s)))


;;;=================================================================
;;;
;;; Statement transformation:
;;;
;;;-----------------------------------------------------------------

(defun transform-single-statements (statements)
  
  (mapcan #'transform-single-statement statements)
  )

;;;-----------------------------------------------------------------

(defun transform-single-statement (s)
  
  ;; This returns a list of lisp forms approximating the translation of
  ;; a single fortran statement.  This is just a first pass.  It
  ;; transforms the fortran into a very rough approximation to the
  ;; desired lisp form, working on one line at a time, with out using
  ;; any contextual information.
  
  (cond ;; order matters!
    ((program?    s) (transform-program s))
    ((subroutine? s) (transform-subroutine s))
    ((function?   s) (transform-function   s))
    ((comment?    s) (transform-comment    s))
    ((type-decl?  s) (transform-type-decl  s))
    ((continue?   s) (transform-continue   s))
    ((implicit?   s) (transform-implicit   s))
    ((dimension?  s) (transform-dimension  s))
    ((external?   s) (transform-external   s))
    ((intrinsic?  s) (transform-intrinsic  s))
    ((return?     s) (transform-return     s))
    ((assign?     s) (transform-assign     s))
    ((call?       s) (transform-call       s))
    ((goto?       s) (transform-goto       s))
    ((if?         s) (transform-if         s))
    ((else?       s) (transform-else       s))
    ((endif?      s) (transform-endif      s))
    ((do?         s) (transform-do         s))
    ((stop?       s) (transform-stop       s))
    ((data?       s) (transform-data       s))
    ((save?       s) (transform-save       s))
    ((end?        s) (transform-end        s))
    ((setf?       s) (transform-setf       s))
    (t (untransformed-warning s))
    )
  )


;;;-----------------------------------------------------------------
;;;
;;; Stuff for carrying along the labels in the right way:
;;;
;;;-----------------------------------------------------------------

(defun make-label (i) (intern (format nil "LABEL-~d" i) 'keyword))
      
;;;
;;; This is a macro so it can have the side effect of poping the label
;;; off <s>.  Maybe this is bad style, but I can't think of anything
;;; cleaner.
;;; 

(defmacro with-label ((s) &body forms)
  
  `(let* ((.label. (if (integerp (first ,s)) (make-label (pop ,s)))))
     (if (null .label.)
	 (list ,@forms)
	 (list .label. ,@forms)
	 )
     )
  )


;;;-----------------------------------------------------------------

(defun untransformed-warning (s)
  (warn "Unrecognized form: ~a" s)
  `(comment ,(format nil "Untranslated: ~a" s)))

;;;-----------------------------------------------------------------

(defun transform-program (s)
  
  `((defsubroutine ,(second s) ,(parse-comma-list (third s)))
    (logical ,(second s)) ;; even programs will return a value.
    )
  )

;;;-----------------------------------------------------------------

(defun transform-subroutine (s)
  
  `((defsubroutine ,(second s) ,(parse-comma-list (third s)))
    (logical ,(second s)) ;; even subroutines will return a value.
    )
  )

;;;-----------------------------------------------------------------

(defun transform-function (s)
  
  ;; in case the type is the two word "double precision":
  (delete 'precision s)
  (if (eql 'function (first s))
      ;; then no type is declared
     `((defsubroutine ,(second s) ,(parse-comma-list (third s))))
     ;; else
      `((defsubroutine ,(third s) ,(parse-comma-list (fourth s)))
	(,(first s) ,(third s))
	)
      )
  )

;;;-----------------------------------------------------------------
;;; Simple un-label-able statements:
;;;-----------------------------------------------------------------

(defun transform-comment (s)
  ;; a comment statement looks like: (comment comment-string)
  (let ((comment-string (second s)))
    (if (blank? comment-string)
	'()
	;; else
	(list s)
	)
    )
  )

(defun transform-end (s)

  (declare (ignore s)) 

  (list 'end)
  )


;;;-----------------------------------------------------------------
;;; Data statements:
;;;-----------------------------------------------------------------
;;;
;;; a data statement looks like:
;;;
;;; data <names> / <constants> / , <names> / <constants> / ...
;;;

(defun transform-data (s)
  
  (labels ((place-name (place)
	     ;; a place is either a variable (atom), and array element,
	     ;; an implied do, or a nested implied do.
	     ;; In each case, the name is the first atom that comes up
	     ;; in a left-first, depth-first search of the expression.
	     (if (atom place)
		 place
		 (place-name (first place))
		 )
	     )
	   )
    (do* ((rs (rest (nsubst '|,| '= s)))
	  (places '())
	  (constants '())
	  )
	 ((null rs)
	  `((data ,places ,(mapcar #'fix-complex-constants constants))
	    (save ,@(delete-duplicates (mapcar #'place-name places)))
	    )
	  )
      (let* ((1st/ (position '/ rs))
	     (1st/+1 (+ 1 1st/))
	     (2nd/ (position '/ rs :start 1st/+1))
	     )
	(setf places (nconc places
			    (parse-comma-list (subseq rs 0 1st/))))
	(setf constants (nconc constants
			       (parse-comma-list (subseq rs 1st/+1 2nd/))))
	(setf rs (nthcdr (+ 2 2nd/) rs))
	)
      )
    )
  )


;;;-----------------------------------------------------------------
;;; Declarations:
;;;-----------------------------------------------------------------

(defun transform-dimension (s) `((dimension ,@(parse-comma-list (rest s)))))
(defun transform-external (s) `((external  ,@(parse-comma-list (rest s)))))
(defun transform-intrinsic (s) `((intrinsic  ,@(parse-comma-list (rest s)))))
(defun transform-save (s) `((save ,@(parse-comma-list (rest s)))))

;;;-----------------------------------------------------------------

(defun transform-type-decl (s)
  
  (delete 'precision s)
  (let* ((the-type (first s))
	 (var-specs (parse-comma-list (rest s)))
	 )
    (labels ((make-decl (var-spec)
	       (if (consp var-spec) ;; an array declaration:
		   `((dimension ,var-spec) (,the-type ,(first var-spec)))
		   ;; else a simple declaration
		   `((,the-type ,var-spec))
		   )
	       ))
      (mapcan #'make-decl var-specs)
      )
    )
  )

;;;-----------------------------------------------------------------
;;; Implicit type statements:
;;;-----------------------------------------------------------------

(defvar *alphabet*
	     '(#\A #\B #\C #\D #\E #\F #\G #\H
	       #\I #\J #\K #\L #\M
	       #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z))

(defun implicit-range-to-char-list (range)
  
  (if (atom range)
      (list (coerce range 'character))
      ;; else it's a list like (- a z)
      (subseq *alphabet*
	      (position (coerce (second range) 'character) *alphabet*)
	      (+ 1 (position (coerce (third range) 'character) *alphabet*))
	      )
      )
  )
    
(defun transform-implicit-spec (spec)
  
  (cons (first spec)
	(sort (mapcan #'implicit-range-to-char-list (rest spec)) #'char-lessp)
	)
  )

(defun transform-implicit (s)
  
  `((implicit
     ,@(mapcar #'transform-implicit-spec (parse-comma-list (rest s)))
     ))
  )


;;;-----------------------------------------------------------------
;;; Simple label-able statements:
;;;-----------------------------------------------------------------

(defun transform-continue (s)
  (with-label (s) (list 'continue)))
(defun transform-return (s) (with-label (s) :return))
(defun transform-stop   (s) (with-label (s) :return))
(defun transform-setf   (s)
  (with-label (s) (infix-to-prefix s)))
(defun transform-assign (s)
  (with-label (s) (list 'setf (fourth s) (make-label (second s)))))

;;;-----------------------------------------------------------------

(defun transform-call (s)
  (with-label (s)
    (if (= 2 (length s))
	;; then no arg list to parse
	(rest s)
	;; else
	(infix-to-prefix (rest s))
	)
    )
  )
      
;;;-----------------------------------------------------------------
;;; More complex label-able statements:
;;;-----------------------------------------------------------------

(defun transform-goto (s)
  
  (with-label (s)
    (progn
      (delete 'to s) ;; allow "goto" and "go to"
      
      (let* ((dest (rest s))
	     (1st (parse-comma-list (first dest)))
	     (2nd (parse-comma-list (third dest))) ;; 2nd elt of dest is |,|
	     )
	(cond
	  ;; simple goto
	  ((numberp 1st) `(go ,(make-label 1st)))
	  ;; assigned goto: looks like (branch-var (labels))
	  ((symbolp 1st) `(goto-assigned ,1st ,(mapcar #'make-label 2nd)))
	  ;; computed goto: looks like (labels destination-exp)
	  ((listp 1st) `(goto-computed ,(mapcar #'make-label 1st) ,2nd))
	  )
	)
      )
    )
  )


;;;-----------------------------------------------------------------

(defun transform-if (s)
  
  (with-label (s)
    (let* ((pred (infix-to-prefix (second s)))
	   (then (nthcdr 2 s))
	   )
      (cond
	((eq (first then) 'then) ;; a block if
	 `(cond (else-if ,pred))
	 )
	((or (member 'if then) (not (member '|,| then))) ;; ordinary if 
	 `(when ,pred ,@(transform-single-statement then))
	 )
	(t ;; else arithmetic if
	 `(arithmetic-if ,pred ,(mapcar #'make-label (parse-comma-list then)))
	 )
	)
      )
    )
  )

;;;-----------------------------------------------------------------

(defun transform-else (s)
  
  (with-label (s)
    (if (eql (second s) 'if) ;; else if
	`(else-if ,(infix-to-prefix (third s)))
	'(else-if t)
	)
    )
  )

(defun transform-endif (s)

  (declare (ignore s)) 

  (list 'endif)
  )
	
;;;-----------------------------------------------------------------

(defun transform-do (s)
  
  (with-label (s)
    (let* ((label (make-label (second s)))
	   (var   (third s))
	   (steps (parse-comma-list (nthcdr 4 s)))
	   (start (first steps))
	   (stop (second steps))
	   (inc  (if (= 2 (length steps)) `() (list (third steps))))
	   )
      `(fortran-do (,var ,start ,stop ,@inc) ,label)
      )
    )
  )
      
;;;=================================================================
;;;			  Fix a subroutine body
;;;=================================================================
;;;
;;; comment statements look like: (comment string1 string2 ...)
;;; 

(defun concatenate-comments (statement list-of-statements)
  
  ;; this function is meant to be called by <reduce>
  
  (let ((1st-of-list (first list-of-statements)))
    (if (and (comment? statement) (comment? 1st-of-list))
	(setf (rest 1st-of-list) (cons (second statement) (rest 1st-of-list)))
	;; else no comments to concatenate
	(push statement list-of-statements)
	)
    list-of-statements
    )
  )
		     
;;;-----------------------------------------------------------------

(defun fix-subroutine (subr)
  
  ;; escape now if we can
  (if (comment? subr) (return-from fix-subroutine (values subr)))
  
  (let* ((sub-name (second subr))
	 (arg-list (third subr))
	 (sub-body (nthcdr 3 subr))
	 (declarations (remove-if-not #'declaration? sub-body))
	 (executables (fix-complex-constants
			(collect-compound-statements
			  (reduce #'concatenate-comments
				  (nsubst
				    `(return-from ,sub-name ,sub-name)
				    :return 
				    (remove-if #'declaration? sub-body)
				    )
				  :from-end t
				  :initial-value '()
				  )
			  )
			)
		      )
	 )
    `(defsubroutine ,sub-name ,arg-list ,declarations 
       ,@executables
       )
    )
  )
  
;;;-----------------------------------------------------------------

(defun collect-do (forms)
  
  (let* ((head (first forms))
	 (label (first (last head)))
	 (end-do (+ 2 (position label forms)))
	 ;; index of statement after label +1 for nthcdr and subseq
	 (do-body (subseq forms 1 end-do))
	 (remaining-forms (nthcdr end-do forms))
	 )
    (cons (append (butlast head) (collect-compound-statements do-body))
	  (collect-compound-statements remaining-forms)
	  )
    )
  )

(defun collect-cond-clauses (forms)
  
  ;; <forms> is a list of the form:
  ;; ((else-if pred1) c11 c12 ... (else-if pred2) c21 c22 ...)
  ;; which is to be collected into:
  ;; ((pred1 c11 c12 ...) (pred2 c21 c22 ...) ...)
  
  (unless (null forms)
    (let ((pred (second (first forms)))
	  (pos (position-if
		 #'(lambda (f) (and (consp f) (eq 'else-if (first f))))
		 forms
		 :start 1
		 )
	       )
	  )
      (if (null pos)
	  (list (cons pred (rest forms)))
	  ;; else
	  (let ((clause (cons pred (subseq forms 1 pos)))
		(remaining-forms (nthcdr pos forms))
		)
	    (cons clause (collect-cond-clauses remaining-forms))
	    )
	  )
      )
    )
  )
	 
(defun collect-cond (forms)
  (let* ((head (first forms))
	 (tail (collect-compound-statements (rest forms)))
	 (end-if (position 'endif tail))
	 (if-body (subseq tail 0 end-if))
	 (remaining-forms (nthcdr (+ 1 end-if) tail))
	 (loose-clauses (cons (second head) if-body))
	 (cond-form `(cond ,@(collect-cond-clauses loose-clauses)))
	 )
    (cons cond-form remaining-forms)
    )
  )

(defun collect-compound-statements (forms)
  
  (unless (null forms)
    (let ((head (first forms)))
      (cond
	((and (listp head) (eql (first head) 'fortran-do))
	 (collect-do forms)
	 )
	((and (listp head) (eql (first head) 'cond))
	 (collect-cond forms)
	 )
	(t
	 (cons head (collect-compound-statements (rest forms)))
	 )
	)
      )
    )
  )
  
;;;-----------------------------------------------------------------
;;;
;;; These intrinsic fortran functions need definitions that are
;;; different from the lisp functions with the same name.
;;; 

(defvar *renamed-functions-alist*
	'((float . fortran-float) (char . fortran-char) (mod . fortran-mod)))



;;;=================================================================
;;;			     The Top Level
;;;=================================================================

(defun polish-fortran (input &optional &key (output nil) (keep-fortran? nil))
  
  #-xerox
  (setf input
	(merge-pathnames input "*.fortran")
	)
  
  (if (null output)
      (setf output (make-pathname :defaults "*.lisp"
				  :host (pathname-host input)
				  :device (pathname-device input)
				  :directory (pathname-directory input)
				  :name (pathname-name input)
				  )
	    )
      #-xerox
      (setf output
	    (merge-pathnames
	      output
	      (make-pathname :defaults "*.lisp"
			     :host (pathname-host input)
			     :device (pathname-device input)
			     :directory (pathname-directory input)
			     :name (pathname-name input)
			     )
	      )
	    )
      )
  (print input)
  (print output)
  
  (with-open-file (output-stream output :direction :output)
    (let ((*print-length* nil)
	  (*print-level*  nil)
	  (*standard-output* output-stream)
	  )
      (declare (special *print-length* *print-level* *standard-output*))
      (print-mode-line)
      #-symbolics '(in-package 'user)
      #+symbolics '(in-package 'cl-user)
      (mapc #+xerox# #'print
	    #-xerox #'pprint
	    (mapcar #'fix-subroutine
		    (get-subroutines input
				     :keep-fortran? keep-fortran?
				     )
		    )
	    )
      )
    output
    )
  )

;;;-----------------------------------------------------------------

(defun print-mode-line (&optional (output *standard-output*)
				  &key (package 'user))
  
  (format
    output
    "~&;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: ~a; -*-~%~%"
    package
    )
  )

;;;-----------------------------------------------------------------

(defun get-subroutines (input &optional &key (keep-fortran? nil))
  
  (collect-subroutines
    (transform-single-statements
      (nsublis *renamed-functions-alist* 
	       (read-cards (delete-if #'blank?
				      (list-lines input)
				      )
			   :keep-fortran? keep-fortran?
			   )
	       )
      )
    )
  )

;;;-----------------------------------------------------------------
;;;
;;; <collect-subroutines> takes a sequence of forms that look like:
;;; 
;;; ((defsubroutine fee (args)) (fee-form1) (fee-form2) ... end
;;;  (defsubroutine fii (args)) (fii-form1) (fii-form2) ... end
;;;  ...)
;;;
;;; and produces a result like:
;;; 
;;; ((defsubroutine fee (args) (fee-form1) (fee-form2) ... )
;;;  (defsubroutine fii (args) (fii-form1) (fii-form2) ... )
;;;  ...)
;;;

(defun collect-subroutines (forms)

  (unless (null forms)
    (if (statement-type? 'defsubroutine (first forms))
	(let* ((end-pos (position 'end forms))
	       (this-subr (append (first forms) (subseq forms 1 end-pos)))
	       )
	  (cons this-subr
		(collect-subroutines (nthcdr (+ end-pos 1) forms))
		)
	  )
	;; else must be a comment
	(cons (first forms)
	      (collect-subroutines (rest forms))
	      )
	)
    )
  )

