From dbp@anchor.apl.washington.edu Mon Sep 16 14:34:08 1991
Received: from lisbon.stat.washington.edu by belgica.stat.washington.edu
	(4.1/UW-NDC Revision: 2.13 ) id AA24316; Mon, 16 Sep 91 14:34:07 PDT
Received: from milton.u.washington.edu by lisbon.stat.washington.edu
	(4.1/UW-NDC Revision: 2.1 ) id AA03402; Mon, 16 Sep 91 14:46:05 PDT
Received: from anchor.apl.washington.edu by milton.u.washington.edu
	(5.65/UW-NDC Revision: 2.1 ) id AA19605; Mon, 16 Sep 91 14:43:10 -0700
Received: by apl.washington.edu (4.1/SMI-4.1)
	id AA06921; Mon, 16 Sep 91 14:43:18 PDT
Date: Mon, 16 Sep 91 14:43:18 PDT
From: dbp@apl.washington.edu (Don Percival)
Message-Id: <9109162143.AA06921@apl.washington.edu>
To: jam@stat.washington.edu
Subject: Fortran to Lisp translator
Cc: dbp@anchor.apl.washington.edu
Status: R

Hi, John.
   I have run into a small problem with your FORTRAN translator (or
perhaps I have forgotten how to use it properly!).  Here is what I
get when I run the following three subroutines through polish::parse-file.
The results for test1 and test2 are in error, whereas that for test3
is ok.

----------------------------------------------------------------------
       SUBROUTINE test1(delh, b, d, x)
       x=delh*(b*d - 1)
       return
       end

==>

(polish::subroutine test1 (delh b d x) nil
 ((setf x (* delh (* b d-1))) (return-from :bessik nil))) 
----------------------------------------------------------------------

       SUBROUTINE test2(delh, b, d, x)
       one = 1
       x=delh*(b*d - one)
       return
       end

==>

(polish::subroutine test2 (delh b d x) nil
 ((setf one 1) (setf x (* delh (* b d-one))) (return-from :test nil))) 
----------------------------------------------------------------------

       SUBROUTINE test3(delh, b, d, x)
       x=delh*(b*(d) - 1.0)
       return
       end

==>

(polish::subroutine test3 (delh b d x) nil
 ((setf x (* delh (- (* b d) 1.0))) (return-from :test3 nil)))

--- Don

