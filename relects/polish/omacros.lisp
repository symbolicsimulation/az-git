;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: User; -*-
;;;
;;; Copyright 1987. John Alan McDonald. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

#-symbolics (in-package 'user)

(defun affix (s1 s2) (intern (concatenate 'string (string s1) (string s2))))

;;;=================================================================
;;; Global constants:
;;;=================================================================

(defconstant *fortran-types*
	     '( integer integer*4 integer*8 real real*4 real*8
	       double complex logical character))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defconstant *polish-intrinsic-functions*
	     '( aref setf
	       + - * expt fortran-/
	       < <= = >= > /= not and or
	       aimag conjg 
	       sqrt dsqrt csqrt 
	       exp dexp cexp log alog dlog clog log10 alog10 dlog10
	       sin dsin csin cos dcos ccos tan dtan
	       asin dasin acos dacos atan datan atan2 datan2
	       sinh dsinh cosh dcosh tanh dtanh 
	       ;; These can't be passed because they're implemented
	       ;; as macros (which is easily changed, if needed):
	       int ifix idint real fortran-float sngl dble cmplx
	       fortran-char ichar fortran-char
	       aint dint anint dnint nint idnint
	       abs iabs dabs cabs fortran-mod amod dmod isign sign dsign
	       idim dim ddim dprod
	       max max0 amax1 dmax1 amax0 max1
	       min min0 amin1 dmin1 amin0 min1
	       )
  )

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defconstant *polish-control-forms*
	     '( comment continue the
	       return-from go goto-assigned goto-computed
	       arithmetic-if when cond
	       fortran-do 
	       )
  )

(defparameter *polish-reserved-words*
	     (concatenate 'list
			  *polish-intrinsic-functions*
			  *polish-control-forms*))

;;;=================================================================
;;; Form classification predicates:
;;;=================================================================

(defun variable? (v) (and (atom v) (not (constantp v))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun form-type? (f type) (and (consp f) (eql (first f) type)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun do-form?        (f) (form-type? f 'fortran-do))
(defun cond-form?      (f) (form-type? f 'cond))
(defun return-form?    (f) (form-type? f 'return-from))
(defun arithmetic-if-form? (f) (form-type? f 'arithmetic-if))
(defun goto-computed-form? (f) (form-type? f 'goto-computed))
(defun goto-assigned-form? (f) (form-type? f 'goto-assigned))
(defun comment-form?   (f) (form-type? f 'comment))
(defun continue-form?  (f) (form-type? f 'continue))
(defun aref-form?      (f) (form-type? f 'aref))
(defun setf-form?      (f) (form-type? f 'setf))
(defun the-form?  (f) (form-type? f 'the))

(defun implicit-form?  (f) (form-type? f 'implicit))
(defun external-form?  (f) (form-type? f 'external))
(defun intrinsic-form? (f) (form-type? f 'intrinsic))
(defun dimension-form? (f) (form-type? f 'dimension))
(defun data-form? (f) (form-type? f 'data))
(defun save-form? (f) (form-type? f 'save))
(defun type-form? (f) (and (consp f) (member (first f) *fortran-types*)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun declaration-form? (f) (or (implicit-form? f)
				 (external-form? f)
				 (intrinsic-form? f)
				 (dimension-form? f)
				 (data-form? f)
				 (type-form? f)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun subroutine-call? (form)
  ;; the subroutine must be loaded for this to work;
  ;; eval-ing a (defsubroutine foo ...) gives foo
  ;; a non-nil :subroutine property 
  (and (consp form)
       (atom (first form))
       (null (declared-dimensions (first form)))
       (not (member (first form) *polish-reserved-words*))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun array-reference? (form)
  (and (consp form)
       (atom (first form))
       (not (null (declared-dimensions (first form))))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun statement-function? (form)
  ;; a statement function definition looks like:
  ;; (setf (fun arg1 arg2) <body-form>)
  (and (setf-form? form)
       (consp (second form))
       (not (array-reference? (second form)))))

;;;=================================================================

(defun get-declared-subroutines (declarations)
  (mapcan #'rest (remove-if-not
		   #'(lambda (f) (or (external-form? f) (intrinsic-form? f)))
		   declarations)))
  
;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun get-saved-names (declarations)
  (mapcan #'rest (remove-if-not #'save-form? declarations)))
  

;;;=================================================================
;;; Global tables:
;;;=================================================================

(defvar *dimension-table* (make-hash-table))
(defvar *implicit-table* (make-hash-table))
(defvar *type-table* (make-hash-table))
(defvar *name-table* (make-hash-table))

;;;-----------------------------------------------------------------

(eval-when (compile load eval)
  (defun declared-dimensions (v) (gethash v *dimension-table* nil))
  (defun set-declared-dimensions (v newd)
    (setf (gethash v *dimension-table*) newd))
  (defsetf declared-dimensions set-declared-dimensions)
  )

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun build-dimension-table (declarations)
  (clrhash *dimension-table*)
  (labels ((do-dim-spec (s) (setf (declared-dimensions (first s)) (rest s)))
	   (do-dim-form (f) (mapc  #'do-dim-spec (rest f))))
    (mapc #'do-dim-form (remove-if-not #'dimension-form? declarations))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun get-declared-arrays ()
  (let ((declared-arrays nil))
    (maphash #'(lambda (key val) val (push key declared-arrays))
	     *dimension-table*)
    declared-arrays))

;;;-----------------------------------------------------------------

(defun fortran-type->lisp-type (ty)
  (ecase ty
    (integer         'fixnum)
    ((real real*4)   'single-float)
    ((double real*8) 'double-float)
    (complex 'complex)
    (logical '(member t nil))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(eval-when (compile load eval)
  (defun implicit-type (v)
    (etypecase v
      (character (gethash v *implicit-table*))
      (symbol (gethash (elt (string v) 0) *implicit-table*))))
  
  (defun set-implicit-type (v newt)
    (etypecase v
      (character (setf (gethash v *implicit-table*) newt))
      (symbol (setf (gethash (elt (string v) 0) *implicit-table*) newt))))
  
  (defsetf implicit-type set-implicit-type)
  
  (defun set-implicit-types (vars a-type)
    (mapc #'(lambda (v) (setf (implicit-type v) a-type)) vars))
  )

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defconstant *default-implicit-real-chars*
	     '(#\A #\B #\C #\D #\E #\F #\G #\H
	       #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z
	       #\a #\b #\c #\d #\e #\f #\g #\h
	       #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z))

(defconstant *default-implicit-integer-chars*
	     '(#\I #\J #\K #\L #\M #\N #\i #\j #\k #\l #\m #\n) )

(defun initialize-implicit-table ()

  (set-implicit-types *default-implicit-real-chars* 'single-float)
  (set-implicit-types *default-implicit-integer-chars* 'fixnum)
  )

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun build-implicit-table (declarations)
  (initialize-implicit-table)
  (labels ((do-implicit-spec (spec)
	     (set-implicit-types (rest spec)
				 (fortran-type->lisp-type (first spec))))
	   (do-implicit-form (form) (mapc #'do-implicit-spec (rest form)))
	   )
    (mapc #'do-implicit-form (remove-if-not #'implicit-form? declarations))))

;;;-----------------------------------------------------------------

(eval-when (compile load eval)
  (defun declared-type (v) (gethash v *type-table*))
  (defun set-declared-type (v newt) (setf (gethash v *type-table*) newt))
  (defsetf declared-type set-declared-type)
  
  (defun set-declared-types (vars a-type)
    (mapc #'(lambda (v) (setf (declared-type v) a-type)) vars))
  )
  
;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun build-type-table (declarations vars)
  (build-implicit-table declarations)
  (clrhash *type-table*)
  (mapc #'(lambda (v) (setf (declared-type v) (implicit-type v))) vars)
  (mapc #'(lambda (type-form)
	    (set-declared-types (rest type-form)
				(fortran-type->lisp-type (first type-form))))
	(remove-if-not #'type-form? declarations))
  )


;;;=================================================================
;;; 
;;; Types: Each name is a reference to a scalar, an array, or a subroutine.
;;;
;;; Scope: Each name is established locally, passed as an arg,
;;;        or global (only subroutines are global at present, because
;;;        I do not support common blocks).
;;; 
;;; Extent: Local names can be dynamic---the value evaporates after
;;;         return---or lexical---the value is preserved on next call of
;;;         subroutine.  
;;;
;;; Simulating Fortran parameter passing semantics requires an
;;; additional distinction for local scalars---between those that are
;;; purely local and those that are passed to other subroutines. Purely
;;; local scalars can be represented efficiently by lisp variables.  To
;;; simulate call by reference, local scalars that are passed out need a
;;; level of indirection; they are represented by local lisp variables
;;; whose value is a symbol whose <indirect-value> is the value that
;;; corresponds to the fortran name.
;;;
;;; Global and arg subroutines can either be intrinsic functions or
;;; explicitly defined subroutines and functions. Local subroutines must
;;; be statement functions. Local subroutines and functions are
;;; represented directly by lisp functions (or macros). Explicitly
;;; defined subroutines and functions are represented by defsubroutines.
;;; Global subroutine calls can be "linked" at compile time.  Arg
;;; subroutine calls must be "linked" at runtime.
;;;
;;; So we have the following possibilities:
;;;
;;; global-subroutine
;;; arg-subroutine
;;; local-subroutine (a statement function)
;;; arg-array
;;; dynamic-array
;;; lexical-array
;;; arg-scalar
;;; dynamic-scalar
;;; lexical-scalar
;;; indirect-dynamic-scalar (a dynamic-scalar that's passed to a subroutine)
;;; indirect-lexical-scalar (a lexical-scalar that's passed to a subroutine)
;;;
;;; At present, all names occuring in <data> statements are treated
;;; as lexical, though strictly speaking they shouldn't be.
;;; 
;;;=================================================================

(eval-when (compile load eval)
  (defun name-flags (n) (gethash n *name-table* '())) 
  (defun set-name-flags (n flags) (setf (gethash n *name-table* '()) flags))
  (defun add-name-flag (n flag) (pushnew flag (gethash n *name-table* '())))
  (defsetf name-flags set-name-flags)
  
  (defun add-flag-for-names (flag name-list)
    (mapc #'(lambda (n) (add-name-flag n flag)) name-list))
  )

;;;-----------------------------------------------------------------

(defun collect-functions-and-arguments (forms)
  
  (let ((funs '())
	(args '()))
    (labels ((recurse (f)
	       (cond
		 ((or (atom f)
		      (comment-form? f)
		      (continue-form? f)
		      (goto-assigned-form? f))
		  nil)
		 ((arithmetic-if-form? f) (mapc #'recurse (second f)))
		 ((goto-computed-form? f) (recurse (third f)))
		 ((do-form? f)
		  (mapc #'recurse (second f))
		  (mapc #'recurse (nthcdr 2 f)))
		 ((or (array-reference? f)
		      (member (first f) *polish-reserved-words*))
		  (mapc #'recurse (rest f)))
		 ((member (first f) *polish-control-forms*)
		  (mapc #'recurse (rest f)))
		 ;; names are only found here!
		 ((subroutine-call? f)
		  (push (first f) funs)
		  (setf args
			(nconc (copy-seq (remove-if-not #'variable? (rest f)))
			       args)))
		 (t (error "Unrecognized form: ~a" f))
		 )
	       ))
      (mapc #'recurse forms)
      (print funs)
      (print args)
      (values (delete-duplicates funs) (delete-duplicates args)) )))

;;;-----------------------------------------------------------------

(defun collect-names (forms)
  
  (labels ((recurse (f) (if (atom f)
			    (if (constantp f)
				nil
				(list f))
			    (mapcan #'recurse f))))
    
    (set-difference (delete-duplicates (recurse forms))
		    *polish-reserved-words*)))

    
  
;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun build-name-table (args declarations body)
  
  (build-dimension-table declarations)
  (clrhash *name-table*)
  
  (let* ((1st-executable (position-if-not #'statement-function? body))
	 (executables (nthcdr 1st-executable body))
	 (statement-functions (convert-statement-functions
				(subseq body 0 1st-executable)))
	 (all-names (collect-names executables))
	 (data-initializations (process-data-forms declarations)) )
    (build-type-table declarations all-names)
    (add-flag-for-names :arg args)
    (add-flag-for-names :array args) ;; all args are passed as arrays
    (add-flag-for-names :array (get-declared-arrays))
    (add-flag-for-names :subroutine (get-declared-subroutines declarations))
    (add-flag-for-names :lexical (get-saved-names declarations))
    (add-flag-for-names :lexical (collect-names data-initializations))
    (multiple-value-bind (subs pars) (collect-functions-and-arguments body)
      (add-flag-for-names :array pars)
      (add-flag-for-names :subroutine subs)
      )
    (finish-name-flags all-names)
    
    (values data-initializations statement-functions executables) ))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun finish-name-flags (names)
  
  (labels ((fill-in-any-missing (n)
	     (let ((flags (name-flags n)))
	       (unless (or (member :array flags)
			   (member :subroutine flags))
		 (add-name-flag n :scalar))
	       (unless (or (member :lexical flags)
			   (member :arg flags)) 
		 (add-name-flag n :dynamic))))
	   (find-unmarked-scalars (n flags)
	     (when (not (or (member :array flags)
			    (member :subroutine flags)))
	       (add-name-flag n :scalar)))
	   (pseudo-arrays (n flags)
	     (when (and (member :array flags)
			(null (declared-dimensions n)))
	       (add-name-flag n :scalar)
	       (setf (declared-dimensions n) '(1)))))
    (mapc #'fill-in-any-missing names)
    (maphash #'find-unmarked-scalars *name-table*)
    (maphash #'pseudo-arrays *name-table*) ))


;;;=================================================================

(eval-when (compile load eval)
  (defun subroutine (sub-name) (get sub-name :subroutine))
  (defun set-subroutine (sub-name function-object)
    (setf (get sub-name :subroutine) function-object))
  (defsetf subroutine set-subroutine)
  )

;;;=================================================================

(defmacro defsubroutine (sub-name args declarations &body body)
  
  (format *terminal-io* "~&Defining subroutine: ~s~%" sub-name)
  (setf body (delete-if #'continue-form? (delete-if #'comment-form? body)))
  (let ((make-name (affix 'make-subroutine- sub-name)))
    (multiple-value-bind
      (data-initializations statement-functions executables)
 	(build-name-table args declarations body)
      `(eval-when (compile load eval)
	 (defun ,make-name ()
	   (let ,(make-bindings :lexical)
	     (declare ,@(make-declarations :lexical))
	     ,@data-initializations 
	     #'(lambda ,(offset-args args) 
		 (declare ,@(make-declarations :arg))
		 (let* (,@(make-bindings :dynamic)
			#+symbolics ,@(make-array-rebindings)
			)
		   (declare ,@(make-declarations :dynamic)
			    #+symbolics ,(make-array-register-declarations)
			    )
		   (labels ,statement-functions 
		     (block ,sub-name
		       (tagbody ,@(fix-calls (fix-arefs executables))) ))))))
	 (setf (subroutine ',sub-name) (,make-name)) ))))

;;;-----------------------------------------------------------------

(defun make-bindings (flag)
  (let ((bindings '()))
    (labels
      ((make-binding (name flags)
	 (when (and (member flag flags) (not (member :subroutine flags)))
	   (if (member :array flags)
	       (push (array-binding name) bindings)
	       (push name bindings) ))))
      (maphash #'make-binding *name-table*)
      (values bindings) )))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun make-declarations (flag) 
  (let ((decls '()))
    (labels
      ((make-declaration (name flags)
	 (when (member flag flags)
	   (cond
	     ((member :array flags)
	      (push (array-declaration name) decls)
	      (when (member :arg flags)
		(push (array-offset-declaration name) decls)))
	     ((member :subroutine flags)
	      (push (subroutine-declaration name) decls))		    
	     (t (push (scalar-declaration name) decls)) ))))
      (maphash #'make-declaration *name-table*) )
    (values decls) ))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

#+symbolics
(defun make-array-register-declarations ()
  `(sys:array-register ,@(get-declared-arrays)))

#+symbolics
(defun make-array-rebindings ()
  (mapcar #'(lambda (a) (list a a)) (get-declared-arrays)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun scalar-declaration (v) `(type ,(declared-type v) ,v))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun subroutine-declaration (v)
  `(type (function ,v (&optional &rest list) ,(declared-type v))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun array-offset-declaration (v) `(type fixnum ,(offset v)))
(defun array-declaration (v) `(type (simple-array ,(declared-type v) 1) ,v))


;;;=================================================================
;;; Subroutine calls:
;;;=================================================================

(defmacro fortran-call (sub-name &rest arg-list)
  (expand-fortran-call sub-name arg-list))

(defun expand-fortran-call (sub-name arg-list)
  
  (let ((assignments '()))
    (labels ((fix-arg (param)
	       (cond
		 ((and (atom param) (member :subroutine (name-flags param)))
		  (list param))
		 ((aref-form? param) (copy-seq (rest param)))
		 (t (let ((slot (make-array '(1))))
		      (push `(setf (aref ',slot 0)
				   ,(if (atom param) param (copy-seq param)) )
			    assignments)
		      (copy-seq `((quote ,slot) 0)) )))))
      (let ((new-params (mapcan #'fix-arg arg-list)))
	;;(zl:dbg)
	`(progn
	   ,@assignments
	   (funcall (subroutine ',sub-name) ,@new-params) )))))


;;;=================================================================
;;;
;;; Data statements:
;;; 
;;; a data form looks like: (data names constants)
;;; 
	       
(defun process-data-forms (declarations)
  (mapcan #'expand-data-form
	  (mapcar #'rest (remove-if-not #'data-form? declarations))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun expand-data-form (nc)

  (let ((names     (mapcan #'expand-data-name     (first  nc)))
	(constants (mapcan #'expand-data-constant (second nc))) )
    (mapcar #'(lambda (n c) `(setf ,n ,c)) names constants) ))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun expand-data-name (name-spec)
  
  (if (atom name-spec)
      ;; then it's a variable name
      (let ((dims (declared-dimensions name-spec)))
	(if (null dims)
	    ;; then a scalar
	    (list name-spec)
	    ;; else it's an array, which will init each element.
	    (list-arefs name-spec (apply #'* dims)) ))
      ;; else it's a reference to array element(s)
      (if (atom (first name-spec))
	  ;; then a single array element
	  (let* ((array-name (first name-spec))
		 (dims (declared-dimensions array-name))
		 (index (1d-index (rest name-spec) dims))
		 )
	    `((aref ,array-name ,index))
	    )
	  ;; else its an implied do list
	  (expand-implied-do-list name-spec) )))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun list-arefs (arr len)
  (do* ((i len (- i 1))
	(arefs '() (cons `(aref ,arr ,(- i 1)) arefs)) )
       ((zerop i) arefs)
    ;; null do* body
    ))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun expand-implied-do-list (spec)
  
  (let ((body (first spec)))
    (cond
      ((atom body) ;; then all nested do's have been expanded
       (let ((dims (declared-dimensions body)))
	 (list `(aref ,body ,(1d-index (rest spec) dims))) ))
      (t  ;; else expand the outermost do and recurse
       (let ((var (second spec))
	     (min (third spec))
	     (max (fourth spec))
	     (stp (or (fifth spec) 1)) )
	 (do ((i min (+ i stp))
	      (expansion '()) )
	     ((> i max)
	      (mapcan #'expand-implied-do-list (nreverse expansion)) )
	   (push (subst i var body) expansion) ))))))
  
;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun expand-data-constant (constant-spec)

  (cond
    ((atom constant-spec) (list constant-spec))
    ((eql '* (first constant-spec))
     (make-sequence 'list (second constant-spec)
		    :initial-element (third constant-spec))) ))
  
;;;=================================================================
;;; Statement functions:
;;; a statement function definition looks like:
;;; (setf (fun arg1 arg2) <body-form>)

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun convert-statement-functions (funs)
  
  (let ((lisp-funs '()))
    (dolist (fun funs)
      (let ((fname (first (second fun)))
	    (lambda-list (rest (second fun)))
	    (fbody (nthcdr 2 fun)) )
	(push `(,fname ,lambda-list ,@fbody) lisp-funs) ))
    (nreverse lisp-funs) ))


;;;=================================================================

(defun offset (v) (affix v '-offset))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun fix-arefs (f)
  (cond
    ((atom f)
     (if (member :array (name-flags f))
	 (if (member :arg (name-flags f))
	     `(aref ,f ,(offset f))
	     `(aref ,f 0))
	 f))
    ((member (first f) *polish-intrinsic-functions*)
     (mapcar #'fix-arefs f))
    ((arithmetic-if-form? f)
     `(arithmetic-if ,(fix-arefs (second f)) ,(third f)))
    ((goto-assigned-form? f)
     `(goto-assigned ,(fix-arefs (second f)) ,(third f)))
    ((goto-computed-form? f)
     `(goto-computed ,(second f) ,(fix-arefs (third f))))
    ((do-form? f)
     `(fortran-do ,(mapcar #'fix-arefs (second f))
	,@(mapcar #'fix-arefs (nthcdr 2 f))))
    ((or (continue-form? f) (comment-form? f)) f)
    ((array-reference? f)
     (let* ((v (first f))
	    (dims (declared-dimensions v))
	    )
       (if (member :arg (name-flags (first f)) )
	   `(aref ,v (+ ,(fix-arefs (1d-index (rest f) dims)) ,(offset v)))
	   `(aref ,v ,(fix-arefs (1d-index (rest f) dims)))))
     )
    (t (mapcar #'fix-arefs f)) )
  )

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun fix-calls (f)
  (cond ((atom f) f)
	((member (first f) *polish-intrinsic-functions*)
	 (mapcar #'fix-calls f))
	((arithmetic-if-form? f)
	 `(arithmetic-if ,(fix-calls (second f)) ,(third f)))
	((goto-assigned-form? f)
	 `(goto-assigned ,(fix-calls (second f)) ,(third f)))
	((goto-computed-form? f)
	 `(goto-computed ,(second f) ,(fix-calls (third f))))
	((do-form? f)
	 `(fortran-do ,(mapcar #'fix-calls (second f))
	    ,@(mapcar #'fix-calls (nthcdr 2 f))))
	((subroutine-call? f)
	 (expand-fortran-call (first f) (mapcar #'fix-calls (rest f))))
	(t (mapcar #'fix-calls f)) ))

;;;=================================================================

(defun 1d-index (inds dims)
  
  ;; Computes a zero-based 1d index from a list of 1-based indexes and
  ;; dimensions.
  
  (if (= (length inds) 1)
      `(+ -1 ,(first inds))
      ;; else
      `(+ -1 ,(first inds)
	  (* ,(first dims) ,(1d-index (rest inds) (rest dims))) )))

;;;=================================================================

(defun array-binding (v)
  
    `(,v ',(make-array (apply #'* (declared-dimensions v))
		      :element-type (declared-type v) 
		      #+franz-inc :fill-pointer #+franz-inc t)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun offset-args (args)
  (mapcan #'(lambda (arg)
	      (if (member :subroutine (name-flags arg))
		  (list arg)
		  (list arg (offset arg))
		  )
	      )
	  args ))
    

;;;=================================================================
;;; Fortran control flow macros:
;;;=================================================================

(defmacro fortran-do ((index from to &optional (by 1)) &body body)
  
  `(prog ()
     (setf ,index ,from)
     :begin-do
     (when (> ,index ,to) (go :end-do))
     ,@body
     (incf ,index ,by)
     (go :begin-do)
     :end-do) )

;;;-----------------------------------------------------------------

(defmacro arithmetic-if (pred then)

  `(cond ((< ,pred 0) (go ,(first  then)))
	 ((= ,pred 0) (go ,(second then)))
	 ((> ,pred 0) (go ,(third  then)))) )
 
;;;-----------------------------------------------------------------

(defmacro goto-assigned (1st 2nd)
  
  `(ecase ,1st
     ,@(mapcar #'(lambda (lbl) `(,lbl (go ,lbl))) 2nd)) )

;;;-----------------------------------------------------------------

(defmacro goto-computed (1st 2nd)
  
  (let ((i 0))
    `(ecase ,2nd
       ,@(mapcar #'(lambda (lbl) `(,(incf i) (go ,lbl))) 1st)) ))

;;;=================================================================
;;; Ignorable Fortran statements:
;;;=================================================================

(defmacro comment (&rest comments) comments nil)
(defmacro continue () nil)


;;;=================================================================
;;; Intrinsic functions:
;;;=================================================================

(defmacro fortran-/ (a1 a2)
  ;; so that integer division works properly
  #+symbolics
  `(zl:/ ,a1 ,a2)
  #-symbolics
  `(if (and (integerp ,a1) (integerp ,a2))
       (truncate ,a1 ,a2)
       (/ ,a1 ,a2)) )

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defmacro int    (a1) `(truncate ,a1))
(defmacro ifix   (a1) `(truncate ,a1))
(defmacro idint  (a1) `(truncate ,a1))
(defmacro real   (a1) `(float (realpart ,a1) 1.0f0))
(defmacro fortran-float (a1) `(float (realpart ,a1) 1.0f0))
(defmacro sngl   (a1) `(float ,a1 1.0f0))
(defmacro dble   (a1) `(float ,a1 1.0d0))
(defmacro cmplx  (&rest args) `(complex ,@args))
(defmacro ichar  (a1) `(char-code ,a1))
(defmacro fortran-char (a1) `(code-char ,a1))
(defmacro aint   (a1) `(ftruncate ,a1))
(defmacro dint   (a1) `(ftruncate ,a1))
(defmacro anint  (a1) `(fround ,a1)) ;; this is not exactly right
(defmacro dnint  (a1) `(fround ,a1)) ;; this is not exactly right
(defmacro nint   (a1) `(round ,a1))
(defmacro idnint (a1) `(round ,a1))

(defun iabs (a1) (abs a1))
(defun dabs (a1) (abs a1))
(defun cabs (a1) (abs a1))

(defmacro fortran-mod (a1 a2) `(rem ,a1 ,a2))
(defmacro amod  (a1 a2) `(rem ,a1 ,a2))
(defmacro dmod  (a1 a2) `(rem ,a1 ,a2))
(defmacro isign (a1 a2) `(* (abs ,a1) (signum ,a2)))
(defmacro sign  (a1 a2) `(* (abs ,a1) (signum ,a2)))
(defmacro dsign (a1 a2) `(* (abs ,a1) (signum ,a2)))
(defmacro idim  (a1 a2) `(max 0 (- ,a1 ,a2)))
(defmacro dim   (a1 a2) `(max 0 (- ,a1 ,a2)))
(defmacro ddim  (a1 a2) `(max 0 (- ,a1 ,a2)))
(defmacro dprod (a1 a2) `(* (float ,a1 1.0d0) ,a2))

(defmacro max0  (&rest args) `(max ,@args))
(defmacro amax1 (&rest args) `(max ,@args))
(defmacro dmax1 (&rest args) `(max ,@args))
(defmacro amax0 (&rest args) `(float    (max ,@args)))
(defmacro max1  (&rest args) `(truncate (max ,@args)))
(defmacro min0  (&rest args) `(min ,@args))
(defmacro amin1 (&rest args) `(min ,@args))
(defmacro dmin1 (&rest args) `(min ,@args))
(defmacro amin0 (&rest args) `(float    (min ,@args)))
(defmacro min1  (&rest args) `(truncate (min ,@args)))

;;(defmacro len   (a1 a2))
;;(defmacro index (a1 a2))

;;(defmacro lge (a1 a2) `(char>= ,a1 ,a2))
;;(defmacro lgt (a1 a2) `(char>  ,a1 ,a2))
;;(defmacro lle (a1 a2) `(char-lessp= ,a1 ,a2))
;;(defmacro llt (a1 a2) `(char-lessp  ,a1 ,a2))

(defun aimag  (a1) (imagpart a1))
(defun conjg  (a1) (conjugate a1))
(defun dsqrt  (a1) (sqrt a1))
(defun csqrt  (a1) (sqrt a1))
(defun dexp   (a1) (exp a1))
(defun cexp   (a1) (exp a1))
(defun alog   (a1) (log a1))
(defun dlog   (a1) (log a1))
(defun clog   (a1) (log a1))
(defun log10  (a1) (/ (log a1) (log 10)))
(defun alog10 (a1) (/ (log a1) (log 10)))
(defun dlog10 (a1) (/ (log a1) (log 10)))
(defun dsin   (a1) (sin a1))
(defun csin   (a1) (sin a1))
(defun dcos   (a1) (cos a1))
(defun ccos   (a1) (cos a1))
(defun dtan   (a1) (tan a1))
(defun dasin  (a1) (asin a1))
(defun dacos  (a1) (acos a1))
(defun datan  (a1) (atan a1))
(defun atan2  (a1 a2) (atan (/ a1 a2)))
(defun datan2 (a1 a2) (atan (/ a1 a2)))
(defun dsinh  (a1) (sinh a1))
(defun dcosh  (a1) (cosh a1))
(defun dtanh  (a1) (tanh a1)) 

