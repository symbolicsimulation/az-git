;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: (:Polish :Lisp); -*-
;;;
;;; Copyright 1990. John Alan McDonald. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(eval-when (compile load eval)
  (unless (find-package :Polish)
    (make-package :Polish :use `(:Lisp) :nicknames '(:po))))

(in-package :Polish)

(eval-when (compile load eval)
  (export '(polish-fortran))
  (import '(polish-fortran) 'user)

  (proclaim '(declaration values))
  (proclaim '(declaration side-effect-on))) 

(deftype Boolean () '(or T Null))

;;;=================================================================
;;;
;;; Some lint-like stuff, maybe it will help in portability?
;;;

#||
(shadow '(defun))

(defmacro defun (name arglist &body body)
  
  (labels
    ((declare-form? (form) (and (consp form) (eql (first form) 'declare)))
     (type-form? (form) (and (consp form) (eql (first form) 'type)))
     (the-form? (form) (and (consp form) (eql (first form) 'the)))
     (ignore-form? (form) (and (consp form) (eql (first form) 'ignore)))
     (make-assertions (typed)
       (let ((type (second typed)))
	 (mapcar #'(lambda (var) `(check-type ,var ,type))
		 (nthcdr 2 typed))))
     (recurse (form)
       (cond
	 ((atom form)
	  (list form))
	 ((declare-form? form)
	  `((declare ,@(remove-if #'ignore-form? (rest form)))
	    ,@(mapcan #'make-assertions
		      (remove-if-not #'type-form? (rest form)))))
	 ((the-form? form)
	  (let ((place-name (gensym)))
	    `((let ((,place-name ,(third form)
		     (check-type ,place-name ,(second form))
		     (the ,(second form) ,place-name)))))))
	 (t
	  (list (mapcan #'recurse form)))
	 )))
    `(lisp:defun ,name ,arglist ,@(mapcan #'recurse body))))

||#
     

;;;=================================================================
;;;=================================================================
;;;		      Reading and lexical analysis
;;;=================================================================
;;;=================================================================

(defun sequence-of? (type sequence)
  (and (typep sequence 'sequence)
       (every #'(lambda (x) (typep x type)) sequence)))

(defun list-of? (type list)
  (and (typep list 'list)
       (every #'(lambda (x) (typep x type)) list)))

(defun vector-of? (type vector)
  (and (typep vector 'vector)
       (every #'(lambda (x) (typep x type)) vector)))

(deftype sequence-of (type)
  (let ((pred #'(lambda (e) (sequence-of? type e))))
  `(satisfies ,pred)))

(deftype list-of (type)
  (let ((pred #'(lambda (e) (list-of? type e))))
  `(satisfies ,pred)))

(deftype vector-of (type)
  (let ((pred #'(lambda (e) (vector-of? type e))))
  `(satisfies ,pred)))

;;;-----------------------------------------------------------------

(defun token?      (x) (or  (null x) (symbolp x) (numberp x) (stringp x)))
(defun variable?   (x) (and (symbolp x) (not (constantp x))))
(defun expression? (x) (or (token? x) (listp x)))

(deftype constant   () "A constant."           '(satisfies constantp))
(deftype token      () "A Polish token."       '(satisfies token?))
(deftype variable   () "A non-constant token." '(satisfies variable?))
(deftype expression () "A token or a list."    '(satisfies expression?))

;;;-----------------------------------------------------------------

(defun token= (token1 token2)
  (declare (type t token1 token2)
	   (values boolean))
  (if (and (symbolp token1) (symbolp token2))
      ;; then want to compare the <symbol-name>s,
      ;; ignoring the packages
       (string= (symbol-name token1) (symbol-name token2))
       ;; else
       (eql token1 token2)))

;;;=================================================================
;;; Read in a line from file, appending all continuation lines and
;;; discarding comments and blank lines.
;;;=================================================================

(defun white-space? (ch)
  (declare (type character ch)
	   (values boolean))
  (member ch '(#\Tab #\Page #\Space #\Return #\Newline #\Linefeed)
	  :test #'char-equal))

(defun blank-line? (line)
  (declare (type (or null string) line)
	   (values boolean))
  (and (not (null line))
       (every #'white-space? line)))

(defun comment-line? (line)
  (declare (type (or null string) line)
	   (values boolean))
  (and (not (null line))
       (> (length line) 0)
       (char-equal #\c (char line 0))))

(defun continuation-line? (line)
  (declare (type (or null string) line)
	   (values boolean))
  (and (not (comment-line? line))
       (>= (length line) 6)
       (not (find #\tab line :start 0 :end 6 :test #'char-equal))
       (not (char-equal #\space (char line 5)))))

;;;-----------------------------------------------------------------

(defun read-72-character-line (in-stream)

  (declare (type stream in-stream)
	   (values a-string))

  (let ((line (read-line in-stream nil nil)))
    (declare (type (or null string) line))
    (if (> (length line) 72)
	(subseq line 0 72)
	line)))
	
;;;-----------------------------------------------------------------

(defun read-next-non-blank-or-comment-line (in-stream)
  
  (declare (type stream in-stream)
	   (values a-string))
  
  (lisp:loop
       (let ((line (read-72-character-line in-stream)))
	 (declare (type (or null string) line))
	 (when (not (or (comment-line? line) (blank-line? line)))
	   (return line)))))
	
;;;-----------------------------------------------------------------

(defun read-continued-line (in-stream start-of-line)
  
  (declare (type stream in-stream)
	   (type (or null string) start-of-line)
	   (values complete-line next-part))
  
  (let ((next-part (read-next-non-blank-or-comment-line in-stream))
	(parts '()))
    (declare (type (or null string) next-part)
	     (type list parts))
    (lisp:loop
	 (cond
	   ((null start-of-line)
	    (return (values nil nil))
	    )
	   ((null next-part)
	    (setf start-of-line
		  (apply #'concatenate 'string start-of-line parts))
	    (return (values start-of-line nil))
	    )
	   ((continuation-line? next-part)
	    (setf parts (nconc parts (list (subseq next-part 6))))
	    (setf next-part (read-next-non-blank-or-comment-line in-stream))
	    )
	   (t 
	    (setf start-of-line
		  (apply #'concatenate 'string start-of-line parts))
	    (return (values start-of-line next-part)) 
	    )
	   )
	 )
    )
  )

;;;=================================================================
;;; Reader macros used to convert a fortran string into tokens:
;;;=================================================================

(defun op-reader (in-stream ch) 

  (declare (type stream in-stream)
	   (type character ch)
	   (ignore in-stream)
	   (values token))

  (values (intern (string ch)))
  )

;;;-----------------------------------------------------------------

(defun **-reader (in-stream ch)

  (declare (type stream in-stream)
	   (type character ch) 
	   (ignore ch)
	   (values token))

  (if (char-equal #\* (peek-char nil in-stream))
      ;; then it's exponentiation
      (progn (read-char in-stream) '**)
      ;; else it's multiplication
      '*					
      )
  )

;;;-----------------------------------------------------------------
  
(defun quote-reader (in-stream ch)

  "Read a fortran string constant."

  (declare (stream in-stream)
	   (character ch)
	   (values token))

  (let ((chars '()))
    (declare (type list chars))
    (lisp:loop
	 (setf ch (read-char in-stream nil #\Newline t))
	 ;; <ch> must be a character; so choose <eof-value> to be #\Newline.
	 (if (char-equal #\' ch)
	     (if (char-equal #\' (peek-char nil in-stream nil #\Newline t))
		 ;; then it's an escaped quote within the string
		 (push (read-char in-stream nil #\' t) chars)
		 ;; else we're done
		 (return (concatenate 'string (nreverse chars)))
		 )
	     ;; else
	     (push ch chars)
	     )
	 )
    )
  )

;;;=================================================================
;;; Reading dots (#\.) is more complicated.
;;;=================================================================

(defconstant *logicals*
	     '( .true. .false. .not. .and. .or. .lt. .le. .eq. .ne.
	       .ge. .gt. .eqv. .neqv.))

(defconstant *seperating-characters* '(#\+ #\- #\* #\/ #\= #\, #\( #\))) 

(eval-when (compile load eval)
  (proclaim '(type list *logicals* *seperating-characters*)))

;;;-----------------------------------------------------------------

(defun concatenate&intern (&rest char-seqs)

  (declare (type list char-seqs)
	   (values symbol))
	   
  (values (intern (string-upcase (apply #'concatenate 'string char-seqs)))))

;;;-----------------------------------------------------------------

(defun dot-reader (in-stream ch)
  
  "Read a logical token [eg. .eq.] or the second half of a decimal [eg .0e3].
   Floating point numbers are broken by this reader macro; they are repaired
   by <repair-tokens>."
  
  (declare (type stream in-stream)
	   (type character ch)
	   (ignore ch)
	   (values token))
  
  (let ((chars '()))
    (declare (type list chars))
    (lisp:loop
	 (let ((pc (peek-char nil in-stream nil nil t)))
	   (declare (type (or null character)  pc))

	   (when (or (null pc)
		     (white-space? pc)
		     (member pc *seperating-characters* :test #'char-equal))
	     (return (concatenate&intern "." chars))) 

	   (when (char-equal pc #\.)
	     (let ((sym (concatenate&intern "." chars ".")))
	       (declare (type symbol sym))
	       (if (member sym *logicals* :test #'token=)
		   (progn (read-char in-stream nil nil t) (return sym))
		   ;; else
		   (return (concatenate&intern "." chars)))))

	   (setf chars (nconc chars (list (read-char in-stream nil nil t))))
	   ))))

;;;=================================================================

(defparameter *fortran-readtable*
	      (let ((a-readtable (copy-readtable nil)))
		(declare (type readtable a-readtable))
		(set-macro-character #\+ #'op-reader nil a-readtable)
		(set-macro-character #\- #'op-reader nil a-readtable)
		(set-macro-character #\/ #'op-reader nil a-readtable)
		(set-macro-character #\= #'op-reader nil a-readtable)
		(set-macro-character #\, #'op-reader nil a-readtable)
		(set-macro-character #\. #'dot-reader nil a-readtable)
		(set-macro-character #\* #'**-reader nil a-readtable)
		(set-macro-character #\' #'quote-reader
				     nil a-readtable)
		a-readtable
		)
  "Readtable used for a crude 1st scan of a Fortran file."
  )

(eval-when (compile load eval)
  (proclaim '(type readtable *fortran-readtable*)))

;;;=================================================================
;;; Repair the tokens that have been broken by the over enthusiastic
;;; polish readtable---for example, "1.0e+10.eq.x" will be read as
;;; the list of tokens: (1 |.| 0e + 10 |.| eq |.| x). 
;;;=================================================================

(defun post-decimal? (token)
  (declare (type token token)
	   (values boolean))
  (and (symbolp token)
       (char-equal #\. (char (string token) 0))
       (not (member token *logicals* :test #'token=))))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

(defun plus-or-minus? (token)
  (declare (type token token)
	   (values boolean))
  (or (token= token '+) (token= token '-)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

(defun dot-or-digit? (ch)
  (declare (type character ch)
	   (values boolean))
  (or (char-equal ch #\.) (digit-char-p ch)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

(defun broken-exponent? (expr)
  (declare (type expression expr)
	   (values boolean))
  (when (variable? expr)
    (let* ((str (string expr))
	   (len-1 (- (length str) 1))
	   (ch (char str len-1)))
      (declare (type string str)
	       (type fixnum len-1)
	       (type character ch)
	       )
      (and (or (char-equal ch #\d) (char-equal ch #\e))
	   (every #'dot-or-digit? (subseq str 0 len-1))))))
  
;;;-----------------------------------------------------------------

(defun read-concatenated (&rest parts)
  (declare (type list parts)
	   (values token))
  (values (read-from-string (format nil "~{~a~}" parts))))

;;;-----------------------------------------------------------------

(defun repair-tokens (tokens)

  (declare (type list tokens)
	   (values list-of-repaired-tokens))

  (let ((out '()))
    (declare (type list out))
    (labels ((repair-token (token)
	       (cond
		 ((listp token) (repair-tokens token))
		 ;; certain tokens need substitutions
		 ((token= token 't)       '.t.)
		 ((token= token 'nil)     '.nil.)
		 ((token= token '.true.)  t)
		 ((token= token '.false.) nil)
		 ((token= token 'float)   'polish-float)
		 ((token= token 'mod)     'polish-mod)
		 ((token= token '|.|)
		  (if (numberp (first out))
		      (read-concatenated (pop out) ".0")
		      ;; else
		      (error "Read error: isolated |.|")))
		 ((post-decimal? token)
		  (if (numberp (first out))
		      (read-concatenated (pop out) token)
		      ;; else
		      (read-concatenated token)))
		 ((plus-or-minus? token)
		  (let* ((last (first out)))
		    (if (broken-exponent? last)
			(read-concatenated (pop out) token (pop tokens))
			;; else it's an arithmetic operator
			token)))
		 (t token))))
      (lisp:loop
	   (when (null tokens) (return (nreverse out)))
	   (push (repair-token (pop tokens)) out)))))

;;;=================================================================
;;; Use the polish readtable to convert a string into a statement.
;;; A statement is a nested list of tokens.
;;;=================================================================

(defun read-fortran-line (line)
  
;  (declare (type string line)
;	   (values list-of-tokens))
  
  (repair-tokens
    (let ((*readtable* *fortran-readtable*)
	  (tokens '())
	  (token nil))
;	(declare (type readtable *readtable*)
;		 (type list tokens)
;		 (type expression token))
      (with-input-from-string (in-stream line)
	;;(declare (type stream in-stream))
	(lisp:loop
	  ;; <end of file> returns <:eof>, in case there's a
	  ;; fortran variable called <nil>.
	  (setf token (read in-stream nil :eof nil))
	  (when (eql token :eof) (return (nreverse tokens)))
	  (push token tokens))))))

;;;=================================================================
;;; Read a file into a list of "statements". 
;;;=================================================================

(defun read-fortran-file (in-stream)

  (declare (type stream in-stream)
	   (values list-of-statements))  

  (let ((statements '())
	(complete-line "")
	(next-part (read-next-non-blank-or-comment-line in-stream)))
    (declare (type list statements)
	     (type (or null string) complete-line)
	     (type (or null string) next-part))
    (lisp:loop
	 (multiple-value-setq
	   (complete-line next-part)
	   (read-continued-line in-stream next-part))
	 (when (null complete-line)
	   (return (nreverse statements)))
	 (push (read-fortran-line complete-line)
	       statements))))


;;;=================================================================
;;;=================================================================
;;;				Parsing
;;;=================================================================
;;;=================================================================
;;;=================================================================
;;;		      Infix to prefix translation
;;;=================================================================

(defconstant *operators*
	     '( .not. .or. .and. .gt. .ge. .eq. .le. .lt. .ne. .eqv. .neqv.
	       + - * /	^ ** =))

(eval-when (compile load eval)
  (proclaim '(type list *operators*)))

;;;-----------------------------------------------------------------

(defun operator? (term)

  (declare (type expression term)
	   (values boolean))

  (member term *operators* :test #'token=))

;;;-----------------------------------------------------------------

(defun operator-weight (operator)

  (declare (type token operator)
	   (values a-fixnum))

  (cond
    ((token= operator '=) 0)
    ((member operator '(.eqv. .neqv.) :test #'token=) 1)
    ((token= operator '.or.) 2)
    ((token= operator '.and.) 3)
    ((token= operator '.not.) 4)
    ((member operator '(.gt. .ge. .eq. .le. .lt. .ne.) :test #'token=) 5)
    ((token= operator '+) 6)
    ((token= operator '-) 7)
    ((token= operator '*) 8)
    ((token= operator '/) 9)
    ((token= operator '**) 10)
    (t (error "Unknown operator:~a" operator))
    )
  )

(defun operator>= (operator1 operator2)

  (declare (type token operator1)
	   (type token operator2)
	   (values boolean))

  (>= (operator-weight operator1)
      (operator-weight operator2)))
       
;;;-----------------------------------------------------------------

(defun lisp-function (operator)

  (declare (type token operator)
	   (values token))

  (cond
    ((token= operator '=)      'setf)
    ((token= operator '.eqv.)  'eqv)
    ((token= operator '.neqv.) 'neqv)
    ((token= operator '.or.)   'or)
    ((token= operator '.and.)  'and)
    ((token= operator '.not.)  'not)
    ((token= operator '.gt.)   '>)
    ((token= operator '.ge.)   '>=)
    ((token= operator '.eq.)   '=)
    ((token= operator '.le.)   '<=)
    ((token= operator '.lt.)   '<)
    ((token= operator '.ne.)   '/=)
    ((token= operator '+)      '+)
    ((token= operator '-)      '-)
    ((token= operator '*)      '*)
    ((token= operator '/)      'polish-/)
    ((token= operator '**)     'expt)
    (t (error "Unknown operator:~a" operator))
    ) 
  )

;;;-----------------------------------------------------------------
      
(defun get-operand (terms operators operands)

  (declare (type list terms operators operands)
	   (values terms operators operands))

  (let ((term (pop terms)))
    (declare (type expression term))

    (cond
      ((operator? term)
       ;; collect a unary operator's operand and push
       ;; the lisp expression on the operand stack.
       (multiple-value-setq
	   (terms operators operands)
	   (get-operand terms operators operands)) 
       (push (list (lisp-function term) (pop operands)) operands))
      ((consp term)
       ;; a sub-expression to be parsed.
       (push (infix-to-prefix term) operands))
      ((consp (first terms))
       ;; must be function or array, if arg list follows.
       (push (cons term (translate-comma-list (pop terms))) operands))
      (t
       ;; else must be a constant or simple variable
       (push term operands))))

  (values terms operators operands))

;;;-----------------------------------------------------------------
      
(defun get-operator (terms operators operands)
  
  (declare (type list terms operators operands)
	   (values terms operators operands))
  
  (cond
    ((and (not (null operators)) (operator>= (first operators) (first terms)))
     ;; If the top of the operator stack has precedence, it can gobble
     ;; up the top two operands and go on the operand stack.
     (let ((arg2 (pop operands))
	   (arg1 (pop operands))
	   (fun (lisp-function (pop operators))))
       (declare (type expression arg2 arg1)
		(type token fun))
       (push (list fun arg1 arg2) operands)
       )
     ;; and then we try again...
     (multiple-value-setq
       (terms operators operands)
       (get-operator terms operators operands))) 
    (t ;; otherwise we're done...
     (push (pop terms) operators)))
  
  (values terms operators operands))

;;;-----------------------------------------------------------------
      
(defun merge-operator-operand-stacks (operators operands)

  (declare (type list operators)
	   (type list operands)
	   (values prefix-expression))

  (if (null operators)
      (first operands)
      ;; else
      (let ((fun (lisp-function (pop operators)))
	    (arg2 (pop operands))
	    (arg1 (pop operands)))
	(declare (type token fun)
		 (type expression arg2 arg1))
	(push `(,fun ,arg1 ,arg2) operands)
	(merge-operator-operand-stacks operators operands))))

;;;-----------------------------------------------------------------

(defun infix-to-prefix (terms)
  
  (declare (type list terms)
	   (values prefix-expression))
  
  ;; see if we can quit now
  (when (token? terms) (return-from infix-to-prefix terms))
  (when (member '|,| terms :test #'token=)
    (return-from infix-to-prefix (translate-comma-list terms))) 
  
  (let ((operators nil)
	(operands nil))
    (declare (type list operators operands))
    (lisp:loop	;; alternate between operands and operators
	 (multiple-value-setq
	   (terms operators operands)
	   (get-operand terms operators operands))
	 (when (null terms)
	   (return-from infix-to-prefix
	     (merge-operator-operand-stacks operators operands)))
	 (multiple-value-setq
	   (terms operators operands)
	   (get-operator terms operators operands))
	 )))

;;;-----------------------------------------------------------------

(defun translate-comma-list-one-level (comma-list)
  
  (declare (type expression comma-list)
	   (values list-of-infix-expressions))
  
  ;; takes a list of infix expressions seperated by |,|'s
  ;; and returns a list of infix(!) expressions
  
  (let ((tokens nil))
    (declare (type list tokens))
    (if (token? comma-list)
	comma-list
	(cons
	  (lisp:loop
	       (let ((term (pop comma-list)))
		 (declare (type expression term))
		 (when (or (null term) (token= term '|,|))
		   (return (nreverse tokens))
		   )
		 (push term tokens)
		 )
	       )
	  (translate-comma-list comma-list)
	  )
	)
    )
  )

;;;-----------------------------------------------------------------

(defun translate-comma-list (comma-list)
  
  (declare (type expression comma-list)
	   (values list-prefix-expressions))
  
  ;; takes a list of infix expressions seperated by |,|'s
  ;; and returns a list of prefix expressions
  
  (let ((tokens nil))
    (declare (type list tokens))
    (if (token? comma-list)
	comma-list
	(cons
	  (infix-to-prefix
	    (lisp:loop
		 (let ((term (pop comma-list)))
		   (declare (type expression term)) 
		   (when (or (null term) (token= term '|,|))
		     (return (nreverse tokens))
		     )
		   (push term tokens)
		   )
		 )
	    )
	  (translate-comma-list comma-list)
	  )
	)
    )
  )

;;;=================================================================
;;;  Complex numbers:
;;;=================================================================
;;; Fortran complex number constants will be read as a list of 2 numbers.
;;; They need to be converted to Common Lisp complex constant #c syntax.
;;; This has to be done after infix-to-prefix translation and with enough
;;; knowledge of the context so that we can assume that a list of 2 numbers
;;; is a complex constant rather than an array index list, etc.

(defun minus-number? (expr)

  (declare (type expression expr)
	   (values boolean))
  
  (and (consp expr)
       (token= '- (first expr))
       (numberp (second expr))))

;;;-----------------------------------------------------------------

(defun fortran-number? (expr)
  
  (declare (type expression expr)
	   (values boolean))
  
  (or (numberp expr) (minus-number? expr)))

;;;-----------------------------------------------------------------

(defun complex-constant? (expr)

  (declare (type expression expr)
	   (values boolean))

  (and (consp expr)
       (= 2 (length expr))
       (fortran-number? (first expr))
       (fortran-number? (second expr))))
       
;;;-----------------------------------------------------------------

(defun fix-complex-constants (expr)

  (declare (type expression expr)
	   (values fixed-expression))

  (cond
    ((token? expr) expr)
    ((complex-constant? expr)
     (complex (eval (first expr)) (eval (second expr))))
    ;; eval in case it's something like ((- 1) 0)
    (t (mapcar #'fix-complex-constants expr))
    )      
  )

;;;=================================================================
;;; Expression classification:
;;;=================================================================


(defconstant *intrinsic-functions*
	     '( aref setf error
	       + - * expt polish-/
	       < <= = >= > /= not and or eqv neqv
	       aimag conjg 
	       sqrt dsqrt csqrt 
	       exp dexp cexp log alog dlog clog log10 alog10 dlog10
	       sin dsin csin cos dcos ccos tan dtan
	       asin dasin acos dacos atan datan atan2 datan2
	       sinh dsinh cosh dcosh tanh dtanh 
	       ;; These can't be passed as arguments because they're
	       ;; implemented as macros (which is easily changed, if
	       ;; needed):
	       int ifix idint real polish-float sngl dble cmplx
	       polish-char ichar
	       aint dint anint dnint nint idnint
	       abs iabs dabs cabs polish-mod amod dmod isign sign dsign
	       idim dim ddim dprod
	       max max0 amax1 dmax1 amax0 max1
	       min min0 amin1 dmin1 amin0 min1
	       )
  )

(eval-when (compile load eval)
  (proclaim '(type list *intrinsic-functions*)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defconstant *control-expressions*
	     '(continue the return-from go when cond ecase tagbody
			arithmetic-if))

(defparameter *reserved-words*
	     (concatenate 'list *intrinsic-functions* *control-expressions*))

(defconstant *fortran-types*
	     '( integer integer*4 integer*8 real real*4 real*8 double
	       complex logical character))

(defconstant *fortran-declarations*
	     '( integer integer*4 integer*8 real real*4 real*8 double
	       complex logical character
	       implicit external intrinsic dimension parameter
	       common data save))
  
(eval-when (compile load eval)
  (proclaim '(type list
	      *control-expressions*
	      *reserved-words*
	      *fortran-types*
	      *fortran-declarations*)))

;;;-----------------------------------------------------------------

(defun declaration? (expr)
  (declare (type expression expr)
	   (values boolean))
  (and (consp expr)
       (token? (first expr))
       (member (first expr) *fortran-declarations* :test #'token=)))

;;;-----------------------------------------------------------------

(defun type-declaration? (expr)
  (declare (type expression expr)
	   (values boolean))
  (and (consp expr)
       (token? (first expr))
       (member (first expr) *fortran-types* :test #'token=)))

;;;-----------------------------------------------------------------

(defun expression-type? (expr expr-type)
  
  (declare (type expression expr)
	   (type token expr-type)
	   (values boolean))	   
  
  (unless (consp expr) (return-from expression-type? nil))
  (when (numberp (first expr)) (setf expr (rest expr)))
  (let ((1st (first expr)))
    (declare (type expression 1st))
    (cond
      ((listp 1st) nil)
      ((token= expr-type '=)
       (or (token= '= (second expr))
	   (token= '= (third expr))
	   (token= '= (fourth expr))))
      ((token= expr-type 'type-declaration)
       (type-declaration? expr))
      ((token= expr-type 'goto)
       (or (token= 1st 'goto) (token= 1st 'go)))
      ((token= expr-type 'return)
       (or (token= 1st 'return) (token= 1st 'return-from)))
      ((token= expr-type 'function)
       (or (token= 'function 1st)
	   (token= 'function (second expr))
	   (token= 'function (third expr))))
      (t ;; otherwise
       (token= 1st expr-type)))))

;;;=================================================================
;;; Statement parsing:
;;;=================================================================

(defun parse-single-statements (statements)
  
  (declare (type list statements)
	   (values list-of-statements-parsed-in-isolation))
  
  (mapcan #'parse-single-statement statements)
  )

;;;-----------------------------------------------------------------

(defun parse-single-statement (statement)

  (declare (type list statement)
	   (values a-statement-parsed-in-isolation))
  
  ;; This returns a list of lisp exprs approximating the translation of
  ;; a single fortran statement.  This is just a first pass.  It
  ;; parses the fortran into a very rough approximation to the
  ;; desired lisp expression, working on one line at a time, with out using
  ;; any contextual information.
  
  (cond ;; order matters!
    ((expression-type? statement 'parameter)
     (parse-parameter statement))
    ((expression-type? statement 'do)
     (parse-do-header statement))
    ((expression-type? statement 'if)
     (parse-if statement))
    ((expression-type? statement '=)
     (parse-=  statement))
    ((expression-type? statement 'program)
     (parse-program-header statement))
    ((expression-type? statement 'subroutine)
     (parse-subroutine-header statement))
    ((expression-type? statement 'function)
     (parse-function-header statement))
    ((expression-type? statement 'type-declaration)
     (parse-type statement))
    ((expression-type? statement 'continue)
     (parse-continue statement))
    ((expression-type? statement 'implicit)
     (parse-implicit statement))
    ((expression-type? statement 'dimension)
     (parse-dimension statement))
    ((expression-type? statement 'common)
     (parse-common statement))
    ((expression-type? statement 'external)
     (parse-external statement))
    ((expression-type? statement 'intrinsic)
     (parse-intrinsic statement))
    ((expression-type? statement :return)
     (parse-return statement))
    ((expression-type? statement 'assign)
     (parse-assign statement))
    ((expression-type? statement 'call)
     (parse-call statement))
    ((expression-type? statement 'goto)
     (parse-goto statement))
    ((expression-type? statement 'else) 
     (parse-else statement))
    ((expression-type? statement 'endif)
     (parse-endif statement))
    ((expression-type? statement 'stop)
     (parse-stop statement))
    ((expression-type? statement 'pause)
     (parse-pause statement))
    ((expression-type? statement 'data)
     (parse-data statement))
    ((expression-type? statement 'save)
     (parse-save statement))
    ((expression-type? statement 'end)
     (parse-end statement))
    (t (parse-unrecognized statement))
    )
  )

;;;-----------------------------------------------------------------
;;; Stuff for carrying along the labels in the right way:
;;;-----------------------------------------------------------------

(defun make-label (a-fixnum)

  (declare (type fixnum a-fixnum)
	   (values keyword-version-of-a-fixnum))
	   
  (intern (format nil "LABEL-~d" a-fixnum) 'keyword))

;;;-----------------------------------------------------------------
      
(defun extract-label (statement)

  (declare (type list statement)
	   (values keyword-label labelless-statement))
  
  (let ((label (if (integerp (first statement))
		   (make-label (pop statement)))))
    (declare (type (or null keyword) label))
    (values label statement)
    )
  )
  
;;;-----------------------------------------------------------------
      
(defun with-label (label &rest exprs)

  (declare (type (or null keyword) label)
	   (type list exprs)
	   (values non-null-label-consed-on-exprs))
  
  (let ((exprs (copy-seq exprs))) ;; &rest is dangerous!
    (declare (type list exprs))
    (if (null label)
	exprs
	(cons label exprs)
	)
    )
  )

;;;-----------------------------------------------------------------

(defun parse-program-header (statement)
  
  (declare (type list statement)
	   (values subroutine-expression))
  
  `((subroutine ,(second statement)
		,(translate-comma-list (third statement))
		;; The translation of a program returns <nil>.
		nil)))

;;;-----------------------------------------------------------------

(defun parse-subroutine-header (statement)

  (declare (type list statement)
	   (values subroutine-expression))

  `((subroutine ,(second statement)
		   ,(translate-comma-list (third statement))
		   ;; The translation of a subroutine returns <nil>.
		   nil)))

;;;-----------------------------------------------------------------

(defun parse-function-header (statement)

  (declare (type list statement)
	   (values subroutine-expression))

  ;; in case the type is the two token "double precision":
  (delete 'precision statement :test #'token=)

  (if (token= 'function (first statement))
      ;; then no type is declared
      `((subroutine ,(second statement)
		    ,(translate-comma-list (third statement))
		    ;; The translation of a function returns the value
		    ;; of variable with the same name as the function.
		    ,(second statement)))
      ;; else
      `((subroutine ,(third statement)
		    ,(translate-comma-list (fourth statement))
		    ;; The translation of a function returns the value
		    ;; of variable with the same name as the function.
		    ,(third statement))
	(,(first statement) ,(third statement)))))

;;;-----------------------------------------------------------------

(defun parse-end (statement)
  (declare (type list statement)
	   (ignore statement)
	   (values end-expr))
  (list :end))

;;;-----------------------------------------------------------------
;;; Data statements look like:
;;; data <names> / <constants> / , <names> / <constants> / ...

;;; A place is either a variable (token?), an array element reference,
;;; an implied do, or a nested implied do.  In each case, the place-name
;;; is the first atom that comes up in a left-first, depth-first search
;;; of the expression.

(defun place-name (place)
  (declare (type expression place)
	   (values token))
  (if (token? place) place (place-name (first place))))

(defun parse-data (statement)
  (declare (type list statement)
	   (values parsed-data-expression))
  (let ((rstatement (rest (nsubst '|,| '= statement :test #'token=)))
	(places '())
	(constants '()))
    (declare (type list rstatement places constants))
    (lisp:loop 
	 (when (null rstatement)
	   (return
	     `((data ,places ,(mapcar #'fix-complex-constants constants))
	       (save ,@(delete-duplicates (mapcar #'place-name places)
					  :test #'token=)))))
	 (let* ((1st/ (position '/ rstatement :test #'token=))
		(1st/+1 (+ 1 1st/))
		(2nd/ (position '/ rstatement :start 1st/+1 :test #'token=)))
	   (declare #-explorer (type fixnum 1st/ 1st/+1 2nd/)
		    #+explorer (type integer 1st/ 1st/+1 2nd/))
	   (setf places
		 (nconc places
			(translate-comma-list (subseq rstatement 0 1st/))))
	   (setf constants
		 (nconc constants
			(translate-comma-list
			  (subseq rstatement 1st/+1 2nd/))))
	   (setf rstatement (nthcdr (+ 2 2nd/) rstatement))))))

;;;-----------------------------------------------------------------

(defun parse-parameter (statement)
  
  (declare (type list statement)
	   (values parameter-expression))

  ;; <infix-to-prefix> produces either one or a list of exprs like
  ;; (setf zero 0.0).  We need to turn this into an association list for
  ;; a later call to <sublis>.
  (let ((setfs (infix-to-prefix (second statement))))
    (declare (type list setfs))
    (if (token? (first setfs))
	;; then only one parameter
	`((parameter ,(cons (second setfs)
			    (fix-complex-constants (third setfs)))))
	;; else it's a list of setfs
	`((parameter ,@(mapcar
			 #'(lambda (f)
			     (cons (second f)
				   (fix-complex-constants (third f))))
			       setfs)))
	)
    )
  )

;;;-----------------------------------------------------------------

(defun parse-common (statement)
  (declare (type list statement)
	   (values common-expression))
  ;; make sure all commons look like /blk1/ v1, v2 /blk2/ v3, v4...
  ;; unnamed commons get named "common" for consistancy.
  (if (token= '/ (second statement))
      (setf statement (rest statement))
      (setf statement (append '(/ common / ) (rest statement))))
  (let ((results '()))
    (declare (type list results))
    (lisp:loop
       (let* ((next/ (position '/ statement :start 4))
	      (block-name (second statement))
	      (var-specs (translate-comma-list (subseq statement 3 next/ )))
	      (dim-specs (mapcan #'(lambda (v) (unless (token? v) (list v)))
				 var-specs))
	      (vars (mapcar #'(lambda (v) (if (token? v) v (first v)))
			    var-specs)))
	 (declare (type (or null fixnum) next/)
		  (type variable block-name)
		  (type list var-specs)
		  (type list dim-specs)
		  (type list vars))
	 (push `(common ,block-name ,@vars) results)
	 (push `(dimension ,@dim-specs) results)
	 (when (null next/) (return (nreverse results)))
	 (setf statement (subseq statement next/))
	 )
       )
    )
  )
  
;;;-----------------------------------------------------------------
;;; Simple Declarations:
;;;-----------------------------------------------------------------

(defun parse-dimension (statement)
  (declare (type list statement)
	   (values dimension-expression))
  `((dimension ,@(translate-comma-list (rest statement)))))

(defun parse-external  (statement)
  (declare (type list statement)
	   (values external-expression))
  `((external  ,@(translate-comma-list (rest statement)))))

(defun parse-intrinsic (statement)
  (declare (type list statement)
	   (values intrinsic-expression))
  `((intrinsic ,@(translate-comma-list (rest statement)))))

(defun parse-save      (statement)
  (declare (type list statement)
	   (values save-expression))
  `((save      ,@(translate-comma-list (rest statement)))))

;;;-----------------------------------------------------------------

(defun parse-type (statement)
  
  (declare (type list statement)
	   (values type-declaration-expression[s]))

  (delete 'precision statement :test #'token=)
  (let ((the-type nil)
	(var-specs '()))
    (declare (type token the-type)
	     (type list var-specs))	     
    (cond
      ((and (token= (second statement) '*) (integerp (third statement)))
       ;; then it's something like character*2
       (setf the-type
	     (intern (string-upcase
		       (format nil "~a*~d"
			       (first statement) (third statement)))))
       (setf var-specs (translate-comma-list
			 (subst 1 '* (nthcdr 3 statement)
				:test #'token=)))
       )
      (t
       ;; else it's just a plain type
       (setf the-type (first statement))
       (setf var-specs (translate-comma-list (subst 1 '* (rest statement)
						    :test #'token=)))
       )
      )
    (labels ((make-decl (var-spec)
	       (declare (type expression var-spec)
			(values type-and-dimension-declaration-expressions))
	       (if (consp var-spec) ;; an array declaration:
		   `((dimension ,var-spec) (,the-type ,(first var-spec)))
		   ;; else a simple declaration
		   `((,the-type ,var-spec))
		   )
	       ))
      (mapcan #'make-decl var-specs)
      )
    )
  )

;;;=================================================================
;;; Implicit type statements:
;;;=================================================================

(defconstant *alphabet*
	     '(#\A #\B #\C #\D #\E #\F #\G #\H
	       #\I #\J #\K #\L #\M
	       #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z))

(eval-when (compile load eval)
  (proclaim '(type list *alphabet*)))

;;;-----------------------------------------------------------------

(defun implicit-range-to-character-list (range)
  (declare (type expression range)
	   (values list-of-characters))
  (if (token? range)
      (list (coerce range 'character))
      ;; else it's a list like (- a z)
      (subseq *alphabet*
	      (position (coerce (second range) 'character) *alphabet*)
	      (+ 1 (position (coerce (third range) 'character) *alphabet*))
	      )
      )
  )

;;;-----------------------------------------------------------------

(defun parse-implicit-spec (spec)
  (declare (type expression spec)
	   (values list-of-characters))
  (let ((the-type nil)
	(char-ranges '()))
    (declare (type token the-type)
	     (type list char-ranges))
    (cond
      ((and (token= (second spec) '*) (integerp (third spec)))
       ;; then it's something like real*8
       (setf the-type (intern (string-upcase
			    (format nil "~a*~d" (first spec) (third spec)))))
       (setf char-ranges (infix-to-prefix (nthcdr 3 spec)))
       )
      (t
       (setf the-type (first spec))
       (setf char-ranges (infix-to-prefix (rest spec)))
       )
      )
    (cons the-type
	  (sort (mapcan #'implicit-range-to-character-list char-ranges)
		#'char-lessp)
	  )
    )
  )

;;;-----------------------------------------------------------------

(defun parse-implicit (statement)

  (declare (type list statement)
	   (values implicit-expr))
  
  `((implicit
      ,@(mapcar #'parse-implicit-spec
		(translate-comma-list-one-level
		  (delete 'precision (rest statement) :test #'token=))))))

;;;=================================================================
;;; Simple label-able statements:
;;;=================================================================

(defun parse-continue (statement)

  (declare (type list statement)
	   (values continue-expression))

  ;; The (continue) that's returned is really only used to parse
  ;; do loops correctly. 

  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr)
	     (ignore expr))
    (with-label label (list 'continue))))

;;;-----------------------------------------------------------------

(defun parse-return (statement)

  (declare (type list statement)
	   (values return-expression))

  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr)
	     (ignore expr))
    (with-label label :return)
    )
  )

;;;-----------------------------------------------------------------

(defun parse-stop (statement)
  (declare (type list statement)
	   (values return-expression))
  (parse-return statement))

;;;-----------------------------------------------------------------

(defun parse-pause (statement)
  (declare (type list statement)
	   (values error-expression))
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (with-label label `(error ,(second expr)))
    )
  )

;;;=================================================================
;;; More complex label=able statements:
;;;=================================================================

(defun parse-= (statement)
  (declare (type list statement)
	   (values setf-expression))
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (with-label label (infix-to-prefix expr))))

;;;-----------------------------------------------------------------

(defun parse-assign (statement)
  (declare (type list statement)
	   (values setf-expression))
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (with-label label `(setf ,(fourth expr) ,(make-label (second expr))))))

;;;-----------------------------------------------------------------

(defun parse-call (statement)
  (declare (type list statement)
	   (values function-call-expression))
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (with-label label
		(if (= 2 (length expr))
		    ;; then no arg list to parse
		    (rest expr)
		    ;; else
		    (infix-to-prefix (rest expr))))))
      
;;;-----------------------------------------------------------------

(defun parse-goto (statement)
  (declare (type list statement) (values parsed-go-expression))
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label) (type list expr))
    (delete 'to expr :test #'token=) ;; allow "goto" and "go to"
    (let* ((dest (rest expr))
	   (1st (translate-comma-list (first dest)))
	   (2nd (translate-comma-list (third dest))) ;; 2nd elt of dest is |,|
	   )
      (declare (type list dest)
	       (type expression 1st 2nd))	       
      (cond
	;; simple goto
	((numberp 1st)
	 (with-label label `(go ,(make-label 1st))))
	;; assigned goto: looks like (branch-var (labelstatement))
	((symbolp 1st)
	 (let* ((labels (mapcar #'make-label 2nd))
		(cases (mapcar #'(lambda (lbl) `(,lbl (go ,lbl))) labels)))
	   (declare (type list labels cases))
	   (with-label label `(ecase ,1st ,@cases))))
	;; computed goto: looks like (labels destination-exp)
	((listp 1st)
	 (let* ((i 0)
		(labels (mapcar #'make-label 1st) )
		(cases (mapcar #'(lambda (lbl) `(,(incf i) (go ,lbl)))
			       labels)))
	   (declare (type integer i)
		    (type list labels cases))
	   (with-label label `(ecase ,2nd ,@cases))))
	))))

;;;-----------------------------------------------------------------

(defun parse-if (statement)

  (declare (type list statement)
	   (values arithmetic-if-when-or-cond-expression))
  
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (let* ((pred (infix-to-prefix (second expr)))
	   (then (nthcdr 2 expr)))
      (declare (type expression pred)
	       (type list then))
      (cond
	((token= (first then) 'then) ;; a block if
	 (with-label label `(cond (elseif ,pred)))
	 )
	((or (member 'if then :test #'token=)
	     (not (member '|,| then :test #'token=))) ;; ordinary if 
	 (with-label label `(when ,pred ,@(parse-single-statement then)))
	 )
	(t ;; else arithmetic if
	 (with-label label
		     `(arithmetic-if ,pred
				     ,@(mapcar #'make-label
					       (translate-comma-list then)))
		     )
	 )
	)
      )
    )
  )

;;;-----------------------------------------------------------------

(defun parse-else (statement)
  (declare (type list statement)
	   (values elseif-expression))
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (if (token= (second expr) 'if) ;; else if
	(with-label label `(elseif ,(infix-to-prefix (third expr))))
	(with-label label '(elseif t))
	)
    )
  )

;;;-----------------------------------------------------------------

(defun parse-endif (statement)
  (declare (type list statement)
	   (values endif-expression)
	   (ignore statement))
  (list 'endif))
	
;;;-----------------------------------------------------------------

(defun parse-do-header (statement)
  (declare (type list statement)
	   (values do-header-expression))
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (let* ((end-label (make-label (second expr)))
	   (var   (third expr))
	   (steps (translate-comma-list (nthcdr 4 expr)))
	   (start (first steps))
	   (stop (second steps))
	   (inc  (if (= 2 (length steps)) 1 (third steps)))
	   )
      (declare (type symbol end-label)
	       (type variable var)
	       (type list steps)
	       (type expression start stop inc))
      (with-label label `(do (,var ,start ,stop ,inc) ,end-label))
      )
    )
  )

;;;-----------------------------------------------------------------

(defun parse-unrecognized (statement)
  (declare (type list statement))
  (warn "Unrecognized expr: ~a" statement)
  ;; have to put something out so that the label is available for goto's
  (multiple-value-bind (label expr) (extract-label statement)
    (declare (type symbol label)
	     (type list expr))
    (with-label label `(continue ,expr))))

;;;-----------------------------------------------------------------

(defun parse-do-loop (exprs)

  (declare (type list exprs)
	   (values parsed-do-loop))
  (let* ((head (first exprs))
	 (spec (second head))
	 (label (third head))
	 (var (first spec))
	 (start (second spec))
	 (stop (third spec))
	 (inc (fourth spec))
	 (end-do (+ 2 (position label exprs)))
	 (do-body (subseq exprs 1 end-do))
	 (remaining-exprs (nthcdr end-do exprs))
	 )
    (declare (type list     head spec)
	     (type keyword  label)
	     (type variable var)
	     (type expression     start stop inc)
	     (type integer   end-do)
	     (type list     do-body remaining-exprs))
    (cons `(tagbody (setf ,var ,start)
		    :begin-do
		    (when (> ,var ,stop) (go :end-do))
		    ,@(parse-compound-statements do-body) 
		    (setf ,var (+ ,var ,inc))
		    (go :begin-do)
		    :end-do
		    )
	  (parse-compound-statements remaining-exprs)
	  )
    )
  )

;;;-----------------------------------------------------------------

(defun parse-cond-clauses (exprs)

  (declare (type list exprs)
	   (values parsed-cond-clauses))

  ;; <exprs> is a list like:
  ;; ((elseif pred1) c11 c12 ... (elseif pred2) c21 c22 ...)
  ;; which is to be collected into:
  ;; ((pred1 (tagbody c11 c12 ...)) (pred2 (tagbody c21 c22 ...)) ...)
  
  (unless (null exprs)
    (let ((pred (second (first exprs)))
	  (pos (position-if
		 #'(lambda (expr)
		     (declare (type expression expr))
		     (and (consp expr)
			  (token= 'elseif (first expr))))
		 exprs
		 :start 1
		 )
	       )
	  )
      (declare (type expression pred)
	       (type (or null integer) pos))	       
      (if (null pos)
	  (list `(,pred (tagbody ,@(rest exprs))))
	  ;; else
	  (let ((clause `(,pred (tagbody ,@(subseq exprs 1 pos))))
		(remaining-exprs (nthcdr pos exprs))
		)
	    (declare (type list clause)
		     (type list remaining-exprs))
	    (cons clause (parse-cond-clauses remaining-exprs))
	    )
	  )
      )
    )
  )
	 
;;;-----------------------------------------------------------------

(defun parse-cond (exprs)

  (declare (type list exprs)
	   (values parsed-cond-expr))
  (let* ((head (first exprs))
	 (tail (parse-compound-statements (rest exprs)))
	 (end-if (position 'endif tail))
	 (if-body (subseq tail 0 end-if))
	 (remaining-exprs (nthcdr (+ 1 end-if) tail))
	 (loose-clauses (cons (second head) if-body))
	 (cond-expr `(cond ,@(parse-cond-clauses loose-clauses)))
	 )
    (declare (type expression head)
	     (type list tail)
	     (type integer end-if)
	     (type list if-body remaining-exprs loose-clauses cond-expr))
    (cons cond-expr remaining-exprs)
    )
  )

;;;-----------------------------------------------------------------

(defun parse-compound-statements (exprs)

  "Takes a list of statements and returns a tree with the proper nesting
   of do-loop's and if-then-else's."

  (declare (type list exprs)
	   (values parsed-do-loops-and-conds))
  (unless (null exprs)
    (let ((head (first exprs)))
      (declare (type expression head))
      (cond
	((expression-type? head 'do) (parse-do-loop exprs))
	((expression-type? head 'cond) (parse-cond exprs))
	(t (cons head (parse-compound-statements (rest exprs))))
	)
      )
    )
  )
  
;;;-----------------------------------------------------------------

(defun block-name (name) (intern (string name) 'keyword))

;;;-----------------------------------------------------------------
;;;
;;; <parse-subroutines> takes a sequence of exprs that look like:
;;; 
;;; ((subroutine fee (args) return-value)
;;;   (fee-decl1) (fee-decl2) ... (fee-expr1) (fee-expr2) ... end
;;;  (subroutine fii (args) return-value)
;;;  (fii-decl1) (fii-decl2) ... (fii-expr1) (fii-expr2) ... end
;;;  ...)
;;;
;;; and produces a result like:
;;; 
;;; ((subroutine fee (args)
;;;  ((fee-decl1) (fee-decl2) ...)
;;;  ((fee-expr1) (fee-expr2) ...) )
;;;  (subroutine fii (args)
;;;  ((fii-decl1) (fii-decl2) ...)
;;;  ((fii-expr1) (fii-expr2) ...) )
;;;  ...)
;;;

(defun parse-subroutines (exprs)
  
  (declare (type list exprs)
	   (values list-of-subroutine-definitions))
  
  (when (null exprs) (return-from parse-subroutines nil))
  (when (not (expression-type? (first exprs) 'subroutine))
    (error "Not the first statement of a subroutine: ~s" (first exprs)))
  (let* ((end-pos (position :end exprs))
	 (subr-head (first exprs))
	 (sub-name (second subr-head))
	 (arg-list (third subr-head))
	 (return-value (fourth subr-head))
	 (sub-body (subseq exprs 1 end-pos))
	 (declarations (remove-if-not #'declaration? sub-body))
	 (executables (fix-complex-constants
			(parse-compound-statements
			  (nsubst `(return-from ,(block-name sub-name)
				     ,return-value)
				  :return 
				  (remove-if #'declaration? sub-body))))))
    (declare (type list subr-head)
	     (type symbol sub-name return-value)
	     (type list arg-list sub-body declarations executables))	     
    (cons `(subroutine ,sub-name ,arg-list ,declarations ,executables)
	  (parse-subroutines (nthcdr (+ end-pos 1) exprs)))))


;;;=================================================================
;;;=================================================================
;;;				Analysis 
;;;=================================================================
;;;=================================================================

(defvar *dimension-table* (make-hash-table))
(defvar *implicit-table* (make-hash-table))
(defvar *type-table* (make-hash-table))
(defvar *name-table* (make-hash-table))

(eval-when (compile load eval)
  (proclaim '(type hash-table
	      *dimension-table*
	      *implicit-table*
	      *type-table*
	      *name-table*)))

;;;=================================================================

(eval-when (compile load eval)
  
  (defun name-flags (n)
    (declare (type variable n)
	     (values name-flag-list))
    (gethash n *name-table* '()))
  
  (defun set-name-flags (n flags)
    (declare (type variable n)
	     (type list flags)
	     (side-effect-on *name-table*))
    (setf (gethash n *name-table* '()) flags))
  
  (defun add-name-flag (n flag)
    (declare (type variable n)
	     (type keyword flag)
	     (side-effect-on *name-table*))
    (pushnew flag (gethash n *name-table* '())))
  
  (defsetf name-flags set-name-flags)
  
  (defun add-flag-for-names (flag name-list)
    (declare (type keyword flag)
	     (type list name-list)
	     (side-effect-on *name-table*))
    (mapc #'(lambda (n) (add-name-flag n flag)) name-list))
  )

;;;=================================================================
;;; Expr analysis predicates:
;;;=================================================================

(defun intrinsic-or-statement-function-call? (expr)
  ;; as opposed to a call to an externally defined subroutine
  (declare (type expression expr)
	   (values boolean))
  (and (consp expr)
       (token? (first expr))
       (member :intrinsic (name-flags (first expr)))))

;;;-----------------------------------------------------------------

(defun subroutine-call? (expr)
  (declare (type expression expr)
	   (values boolean))
  (and (consp expr)
       (token? (first expr))
       (not (member (first expr) *reserved-words* :test #'token=))
       (not (member :intrinsic (name-flags (first expr))))
       (or (null (declared-dimensions (first expr)))
	   (member :subroutine (name-flags (first expr)))) 
       ))

;;;-----------------------------------------------------------------

(defun array-reference? (expr)
  (declare (type expression expr)
	   (values boolean))
  (and (consp expr)
       (token? (first expr))
       (not (member :subroutine (name-flags (first expr))))
       (not (member :intrinsic (name-flags (first expr))))
       (or (not (null (declared-dimensions (first expr))))
	   (member :array (name-flags (first expr)))
	   )
       )
  )

;;;-----------------------------------------------------------------

(defun statement-function-definition? (expr)
  (declare (type expression expr)
	   (values boolean))
  ;; a statement function definition looks like:
  ;; (setf (fun arg1 arg@) <body-expr>)
  (and (expression-type? expr 'setf)
       (consp (second expr))
       (not (array-reference? (second expr)))))

;;;=================================================================
;;; Declared names:
;;;=================================================================

(defun get-declared-subroutines (declarations)
  (declare (type list declarations)
	   (values names-that-appear-in-intrinsic-or-external-exprs))
  (mapcan #'rest (remove-if-not
		   #'(lambda (expr)
		       (declare (type expression expr)) 
		       (or (expression-type? expr 'external)
			   (expression-type? expr 'intrinsic)))
		   declarations)))
  
;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun get-declared-intrinsics (declarations)
  (declare (type list declarations)
	   (values names-that-appear-in-intrinsic-exprs ))
  (mapcan #'rest
	  (remove-if-not #'(lambda (d)
			     (declare (type expression d))
			     (expression-type? d 'intrinsic))
	  declarations)))
  
;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defun get-saved-names (declarations)
  (declare (type list declarations)
	   (values names-that-appear-in-save-exprs ))
  (mapcan #'rest
	  (remove-if-not #'(lambda (d)
			     (declare (type expression d))
			     (expression-type? d 'save))
			 declarations)))

;;;-----------------------------------------------------------------

(eval-when (compile load eval)
  (defun declared-dimensions (v)
    (declare (type variable v)
	     (values list-of-dimensions))
    (gethash v *dimension-table* nil))
  
  (defun set-declared-dimensions (v newd)
    (declare (type variable v)
	     (type list newd)
	     (side-effect-on *dimension-table*))
    (setf (gethash v *dimension-table*) newd)
    nil)
  
  (defsetf declared-dimensions set-declared-dimensions)
  )

;;;--------------------------------------------------------

(defun build-dimension-table (declarations)
  (declare (type list declarations)
	   (side-effect-on *dimension-table*))
  (clrhash *dimension-table*)
  (labels ((do-dim-spec (s)
	     (declare (type expression s))
	     (setf (declared-dimensions (first s)) (rest s)))
	   (do-dim-expr (expr)
	     (declare (type expression expr))
	     (mapc  #'do-dim-spec (rest expr))))
    (mapc #'do-dim-expr
	  (remove-if-not #'(lambda (d)
			     (declare (type expression d))
			     (expression-type? d 'dimension))
			 declarations))))

;;;--------------------------------------------------------

(defun get-declared-arrays ()
  (declare (values names-that-appear-in-*dimension-table*))
  (let ((declared-arrays nil))
    (declare (type list declared-arrays))
    (maphash #'(lambda (key val)
		 (declare (type token key)
			  (type list val)
			  (ignore val))
		 (push key declared-arrays))
	     *dimension-table*)
    declared-arrays))

;;;-----------------------------------------------------------------

(defun fortran-type->lisp-type (ty)
  (declare (type symbol ty)
	   (values lisp-type-specifier))
  (cond 
    ((token= ty 'integer)   'fixnum)
    ((token= ty 'real)      'single-float)
    ((token= ty 'real*4)    'single-float)
    ((token= ty 'double)    'double-float)
    ((token= ty 'real*8)    'double-float)
    ((token= ty 'complex)   'complex)
    ((token= ty 'logical)   'boolean)
    ((token= ty 'character) 'character)
    )
  )

;;;--------------------------------------------------------

(eval-when (compile load eval)
  (defun implicit-type (v)
    (declare (type (or character symbol) v)
	     (values lisp-type-specifier-implied-by-the-1st-char))
    (etypecase v
      (character (gethash v *implicit-table*))
      (symbol (gethash (elt (string v) 0) *implicit-table*))))
  
  (defun set-implicit-type (v newt)
    (declare (type (or character symbol) v)
	     (side-effect-on *implicit-table*))
    (etypecase v
      (character (setf (gethash v *implicit-table*) newt))
      (symbol (setf (gethash (elt (string v) 0) *implicit-table*) newt))))
  
  (defsetf implicit-type set-implicit-type)
  
  (defun set-implicit-types (vars a-type)
    (declare (type list vars)
	     (side-effect-on *implicit-table*))
    (mapc #'(lambda (v) (setf (implicit-type v) a-type)) vars))
  )

;;;--------------------------------------------------------

(defconstant *default-implicit-real-characters*
	     '(#\A #\B #\C #\D #\E #\F #\G #\H
	       #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z
	       #\a #\b #\c #\d #\e #\f #\g #\h
	       #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z))

(defconstant *default-implicit-integer-characters*
	     '(#\I #\J #\K #\L #\M #\N #\i #\j #\k #\l #\m #\n) )

(eval-when (compile load eval)
  (proclaim '(type list
	      *default-implicit-real-characters*
	      *default-implicit-integer-characters*)))

;;;--------------------------------------------------------

(defun initialize-implicit-table ()
  (declare (side-effect-on *implicit-table*))
  (set-implicit-types *default-implicit-real-characters* 'single-float)
  (set-implicit-types *default-implicit-integer-characters* 'fixnum)
  )

;;;--------------------------------------------------------

(defun build-implicit-table (declarations)
  (declare (type list declarations)
	   (side-effect-on *implicit-table*))
  (initialize-implicit-table)
  (labels ((do-implicit-spec (spec)
	     (declare (type list spec))
	     (set-implicit-types (rest spec)
				 (fortran-type->lisp-type (first spec))))
	   (do-implicit-expr (expr)
	     (declare (type expression expr))
	     (mapc #'do-implicit-spec (rest expr)))
	   )
    (mapc #'do-implicit-expr
	  (remove-if-not #'(lambda (d)
			     (declare (type expression d))
			     (expression-type? d 'implicit))
			 declarations))))

;;;-----------------------------------------------------------------

(eval-when (compile load eval)
  
  (defun declared-type (v)
    (declare (type variable v)
	     (values declared-lisp-type))
    (gethash v *type-table*))
  
  (defun set-declared-type (v newt)
    (declare (type variable v)
	     (type symbol newt)
	     (side-effect-on *type-table*))
    (setf (gethash v *type-table*) newt))
  
  (defsetf declared-type set-declared-type)
  
  (defun set-declared-types (vars a-type)
    (declare (type list vars)
	     (type symbol a-type)
	     (side-effect-on *type-table*))
    (mapc #'(lambda (v)
	      (declare (type token v))
	      (setf (declared-type v) a-type))
	  vars))
  
  )
  
;;;--------------------------------------------------------

(defun build-type-table (declarations vars)
  (declare (type list declarations)
	   (type list vars)
	   (side-effect-on *type-table*))
  (build-implicit-table declarations)
  (clrhash *type-table*)
  (mapc #'(lambda (v) (setf (declared-type v) (implicit-type v))) vars)
  ;; make sure all arrays have types defined, even if they aren't
  ;; referenced in the executables
  (maphash #'(lambda (key val)
	       (declare (type list val)
			(ignore val))
	       (setf (declared-type key) (implicit-type key)))
	   *dimension-table*)
  (mapc #'(lambda (type-expr)
	    (declare (type expression type-expr))
	    (set-declared-types
	      (rest type-expr)
	      (fortran-type->lisp-type (first type-expr))))
	(remove-if-not #'type-declaration? declarations)))

;;;=================================================================

(defun collect-functions-and-arguments (exprs)

  (declare (type list exprs)
	   (values list-of-subroutines-called variables-passed-as-arguments))
  
  (let ((funs '())
	(args '()))
    (declare (type list funs)
	     (type list args))
    (labels
      ((recurse (expr)
	 (declare (type expression expr)
		  (side-effect-on funs args))
	 (cond
	   ((token? expr) nil)
	   ((expression-type? expr 'cond)
	    (mapc #'(lambda (f1) (mapc #'recurse f1)) (rest expr)))
	   ((expression-type? expr 'ecase)
	    (mapc #'(lambda (f1) (mapc #'recurse (rest f1))) (nthcdr 2 expr)))
	   ((expression-type? expr 'do)
	    (mapc #'recurse (second expr))
	    (mapc #'recurse (nthcdr 2 expr)))
	   ((or (array-reference? expr)
		(member (first expr) *reserved-words* :test #'token=)
		(intrinsic-or-statement-function-call? expr))
	    (mapc #'recurse (rest expr)))
	   ;; names are only found here!
	   ((subroutine-call? expr)
	    (push (first expr) funs)
	    (setf args
		  (nconc (copy-seq (remove-if-not #'variable? (rest expr)))
			 args)))
	   (t (error "Unrecognized expr: ~a" expr))
	   )
	 ))
      (mapc #'recurse exprs)
      (values (delete-duplicates funs)
	      (delete-duplicates args))
      )))

;;;-----------------------------------------------------------------

(defun collect-names (exprs)

  (declare (type list exprs)
	   (values all-non-constant-names-in-exprs))

  (labels ((recurse (expr)
	     (declare (type expression expr)
		      (values all-non-constant-names-in-expr))
	     (if (token? expr)
		 (unless (constantp expr) (list expr))
		 (mapcan #'recurse expr))))
    (set-difference (delete-duplicates (recurse exprs) :test #'token=)
		    *reserved-words*
		    :test #'token=)))

;;;-----------------------------------------------------------------

(defun affix (&rest names)
  (declare (type list names) ;; each name is a symbol or string
	   (values names-concatenated-into-one-interned-symbol))
  (intern (apply #'concatenate 'string (mapcar #'string names))))

;;;--------------------------------------------------------

(defun substitute-parameters (declarations body)
  
  ;; substitute constant values for parameters

  (declare (type list declarations body)
	   (values new-declarations new-body))
  
  (let ((parameter-alist
	  (mapcan #'rest (remove-if-not #'(lambda (d)
					    (declare (type expression d))
					    (expression-type? d 'parameter))
					declarations))))
    (declare (type list parameter-alist))
    ;; parameter values may be exprs in previous parameters
    (mapl #'(lambda (al)
	      (declare (type cons al))
	      (nsublis (list (first al)) (rest al)))
	  parameter-alist)
    (setf parameter-alist
	  (mapcar #'(lambda (a)
		      (declare (type cons a))
		      (cons (first a) (eval (rest a))))
		  parameter-alist))
    (setf declarations
	  (nsublis parameter-alist
		   (remove-if #'(lambda (d)
				  (declare (type expression d))
				  (expression-type? d 'parameter))
			      declarations)))
    (setf body (nsublis parameter-alist body))
    )
  (values declarations body)
  )

;;;--------------------------------------------------------

(defun rename-common-variables (declarations body)
  
  ;; Rename all variables that appear in commons.
  ;; "v" becomes "blk-v" for a common named "block" (eg. "common /blk/ v")
  ;; Then substitute the new names throughout the declarations and body.
  
  (declare (type list declarations body)
	   (values new-declarations new-body))
  
  (let* ((common-blks 
	   (mapcar #'rest
		   (remove-if-not
		     #'(lambda (d)
			 (declare (type expression d))
			 (expression-type? d 'common))
		     declarations)))
	 (common-alist
	   (mapcan #'(lambda (blk)
		       (declare (type list blk))
		       (let ((blk-name (first blk))
			     (blk-vars (rest blk)))
			 (mapcar
			   #'(lambda (v)
			       (declare (type token v))
			       (cons v (affix blk-name '- v)))
			   blk-vars)))
		   common-blks))
	 
	 )
    (declare (type list common-blks)
	     (type list common-alist))
    (setf declarations (nsublis common-alist declarations))
    (setf body (nsublis common-alist body))
    )
  (values declarations body)
  )

;;;--------------------------------------------------------

(defun finish-name-flags (names)
  (declare (type list names)
	   (side-effect-on *name-table*))
  
  (labels
    ((fill-in-missing-flags (n)
       (declare (type variable n)
		(side-effect-on *name-table*))       
       (let ((flags (name-flags n)))
	 (unless (or (member :array flags)
		     (member :subroutine flags))
	   (add-name-flag n :scalar)
	   )
	 (unless (or (member :lexical flags)
		     (member :arg flags)) 
	   (add-name-flag n :dynamic))))
     
     (fix-entry (n flags)
       ;; make the name flags consistant
       (declare (type variable n)
		(type list flags)
		(side-effect-on *name-table*))
       (unless (or (member :array flags)
		   (member :subroutine flags))
	 (add-name-flag n :scalar)
	 )
       (when (and (member :array flags)
		  (null (declared-dimensions n)))
	 (add-name-flag n :scalar)
	 (setf (declared-dimensions n) '(1))
	 )
       (when (member :subroutine flags)
	 (setf (name-flags n) (delete :array (name-flags n)))
	 )
       (when (member :common flags)
	 (setf (name-flags n) (delete :dynamic (name-flags n)))
	 (setf (name-flags n) (delete :lexical (name-flags n))))))
    
    (mapc #'fill-in-missing-flags names)
    (maphash #'fix-entry *name-table*)
    ))

;;;--------------------------------------------------------

(defun build-name-table (args decls data-inits statement-funs executables) 
  (declare (type list args)
	   (type list decls)
	   (type list data-inits)
	   (type list statement-funs)
	   (type list executables)
	   (side-effect-on *name-table*))
  (clrhash *name-table*)
  (let ((all-names (collect-names executables)))
    (declare (type list all-names))
    (build-type-table decls all-names)
    (add-flag-for-names
      :common (mapcan #'(lambda (d)
			  (declare (type expression d))
			  (nthcdr 2 d))
		       (remove-if-not #'(lambda (d)
					  (declare (type expression d))
					  (expression-type? d 'common))
			 decls)))
    (add-flag-for-names :arg args)
    (add-flag-for-names :array args) ;; all args are passed as arrays
    (add-flag-for-names :array (get-declared-arrays))
    (add-flag-for-names :intrinsic (mapcar #'first statement-funs))
    (add-flag-for-names :intrinsic (get-declared-intrinsics decls) )
    (add-flag-for-names :subroutine (get-declared-subroutines decls))
    (add-flag-for-names :lexical (get-saved-names decls))
    (add-flag-for-names :lexical (collect-names data-inits))
    (multiple-value-bind
      (subs pars) (collect-functions-and-arguments executables)
      (declare (type list subs pars))
      (add-flag-for-names :array pars)
      (add-flag-for-names :subroutine subs)
      )
    (finish-name-flags all-names)
    )
  )

;;;-----------------------------------------------------------------
;;; Statement functions:
;;; a statement function definition looks like:
;;; (setf (fun arg1 arg2) <body-expr>)

(defun convert-statement-functions (funs)
  
  (declare (type list funs)
	   (values converted-statement-function-definitions))
  
  (mapcar #'(lambda (fun)
		 (declare (type list fun))
		 (let ((fname (first (second fun)))
		       (lambda-list (rest (second fun)))
		       (fbody (nthcdr 2 fun)) )
		   (declare (type token fname)
			    (type list lambda-list fbody))
		   `(,fname ,lambda-list ,@fbody)))
       funs))


;;;=================================================================
;;; Data statements:
;;;=================================================================
;;; 
;;; a data expr looks like: (data names constants)
;;; 

(defun make-list-of-arefs (arr len)
  (declare (type token arr)
	   (type integer len)
	   (values list-of-arefs))
  (do* ((i len (- i 1))
	(arefs '() (cons `(aref ,arr ,i) arefs)) )
       ((zerop i) arefs)
    (declare (type integer i)
	     (type list arefs))
    ;; null do* body
    ))

;;;--------------------------------------------------------

(defun expand-implied-do-list (spec)
  (declare (type list spec)
	   (values list-of-arefs))
  (let ((body (first spec)))
    (declare (type expression body))
    (cond
      ((token? body) ;; then all nested do's have been expanded
       (let ((dims (declared-dimensions body)))
	 (list `(aref ,body ,(1d-index (rest spec) dims))) ))
      (t  ;; else expand the outermost do and recurse
       (let ((var (second spec))
	     (min (third spec))
	     (max (fourth spec))
	     (stp (or (fifth spec) 1)) )
	 (declare (type token var)
		  (type integer min max stp))
	 (do ((i min (+ i stp))
	      (expansion '()) )
	     ((> i max)
	      (mapcan #'expand-implied-do-list (nreverse expansion)) )
	   (declare (type integer i)
		    (type list expansion))
	   (push (subst i var body) expansion) ))))))

;;;--------------------------------------------------------

(defun expand-data-name (name-spec)
  
  (declare (type expression name-spec)
	   (values expanded-list-of-place-references))
    (if (token? name-spec)
	;; then it's a variable name
	(let ((dims (declared-dimensions name-spec)))
	  (declare (type list dims))
	  (if (null dims)
	      ;; then a scalar
	      (list name-spec)
	      ;; else it's an array, which will init each element.
	      (make-list-of-arefs name-spec (apply #'* dims)) ))
	;; else it's a reference to array element(s)
	(if (token? (first name-spec))
	    ;; then a single array element
	    (let* ((array-name (first name-spec))
		   (dims (declared-dimensions array-name))
		   (index (1d-index (rest name-spec) dims))
		   )
	      (declare (type token array-name)
		       (type list dims)
		       (type integer index))
	      `((aref ,array-name ,index))
	      )
	    ;; else its an implied do list
	    (expand-implied-do-list name-spec) )))

;;;--------------------------------------------------------

(defun expand-data-constant (constant-spec)
  (declare (type expression constant-spec)
	   (values expanded-data-constant))
  (cond
    ((token? constant-spec) (list constant-spec))
    ((token= '* (first constant-spec))
     (make-sequence 'list (second constant-spec)
		    :initial-element (third constant-spec))) ))
  
;;;--------------------------------------------------------

(defun expand-data-expr (nc)
  (declare (type list nc)
	   (values list-of-assignments))

  (let ((names     (mapcan #'expand-data-name     (first  nc)))
	(constants (mapcan #'expand-data-constant (second nc))) )
    (declare (type list names constants))
    (mapcar #'(lambda (n c)
		(declare (type atom n c))
		`(setf ,n ,c)) names constants) ))

;;;--------------------------------------------------------
	       
(defun process-data-exprs (declarations)
  (declare (type list declarations)
	   (values data-initializations))
  (mapcan #'expand-data-expr
	  (mapcar #'rest
		  (remove-if-not
		    #'(lambda (d)
			(declare (type expression d))
			(expression-type? d 'data))
		    declarations))))

;;;=================================================================

(defun offset-name (v)
  (declare (type token v)
	   (values token-with-offset-suffix))
  (affix v '-o))

;;;-----------------------------------------------------------------

(defun 1d-index (inds dims)
  (declare (type list inds dims)
	   (values expression-that-computes-column-major-1d-index))
  
  ;; Computes a zero-based 1d index from a list of 1-based indexes and
  ;; dimensions.
  
  (if (= (length inds) 1)
      `(+ -1 ,(first inds))
      ;; else
      `(+ -1 ,(first inds)
	  (* ,(first dims) ,(1d-index (rest inds) (rest dims))) )))

;;;--------------------------------------------------------

(defun translate-arefs (expr)
  (declare (type expression expr)
	   (values expression-with-all-arefs-converted-to-offset-1d-indexes))
  (cond
    ((constantp expr) expr)
    ((variable? expr)
     (let ((flags (name-flags expr)))
       (declare (type list flags))
       (if (and (member :array flags)
		(not (member :subroutine flags)))
	   (if (member :arg flags)
	       `(aref ,expr ,(offset-name expr))
	       `(aref ,expr 0))
	   expr)))
    ((member (first expr) *intrinsic-functions* :test #'token=)
     (mapcar #'translate-arefs expr))
    ((expression-type? expr 'cond)
     `(cond ,@(mapcar #'(lambda (f1)
			  (declare (type expression f1))
			  (mapcar #'translate-arefs f1))
		      (rest expr))))
    ((expression-type? expr 'ecase)
     `(ecase ,(translate-arefs (second expr))
	,@(mapcar #'(lambda (f1)
		      (declare (type expression f1))
		      (mapcar #'translate-arefs f1))
		  (nthcdr 2 expr))))
    ((array-reference? expr)
     (let* ((v (first expr))
	    (dims (declared-dimensions v)))
       (declare (type token v)
		(type list dims))
       (if (member :arg (name-flags (first expr)) )
	   `(aref ,v (+ ,(translate-arefs (1d-index (rest expr) dims))
			,(offset-name v)))
	   `(aref ,v ,(translate-arefs (1d-index (rest expr) dims)))))
     )
    (t (mapcar #'translate-arefs expr)) )
  )

;;;========================================================

(defvar *arg-slots* '())
(eval-when (compile load eval)
  (proclaim '(type list *arg-slots*)))

;;;========================================================

(defun expand-fortran-call (sub-name arg-list)
  (declare (type token sub-name)
	   (type list arg-list)
	   (values translated-subroutine-call))
  (let ((assignments '()))
    (declare (type list assignments))
    (labels
      ((fix-arg (param)
	 (declare (type expression param))
	 (cond
	   ((constantp param)
	    (let ((arg-slot (gentemp)))
	      (declare (type symbol arg-slot))
	      (push arg-slot *arg-slots*)
	      (push `(setf (aref ,arg-slot 0) ,param) assignments)
	      (copy-seq `(,arg-slot 0))))
	   ((and (token? param) (member :subroutine (name-flags param)))
	    (cond ((or (member param *intrinsic-functions* :test #'token=)
		       (member :intrinsic (name-flags param)))
		   (list `#',param))
		  ((member :arg (name-flags sub-name)) (list param))
		  (t (list `(subroutine ',param)))))
	   ((expression-type? param 'aref) (copy-seq (rest param)))
	   (t (let ((arg-slot (gentemp))
		    (val (if (token? param) param (copy-seq param))))
		(declare (type symbol arg-slot) (type expression val))
		(push arg-slot *arg-slots*)
		(push `(setf (aref ,arg-slot 0) ,val) assignments)
		(copy-seq `(,arg-slot 0)))))))

      (let* ((new-params (mapcan #'fix-arg arg-list))
	     (fun-call
	       (cond
		 ((member :arg (name-flags sub-name))
		  `(funcall ,sub-name ,@new-params))
		 ((or (member sub-name *intrinsic-functions* :test #'token=)
		      (member :intrinsic (name-flags sub-name)))
		  `(,sub-name ,@arg-list))
		 (t `(funcall (subroutine ',sub-name) ,@new-params)))))
	(declare (type list new-params fun-call))
	(if (null assignments)
	    fun-call
	    `(progn ,@assignments ,fun-call))
	))))

;;;--------------------------------------------------------

(defun translate-calls (expr)
  (declare (type expression expr)
	   (values expression-with-translated-subroutine-calls))
  (cond
    ((token? expr) expr)
    ((member (first expr) *intrinsic-functions* :test #'token=)
     (mapcar #'translate-calls expr))
    ((expression-type? expr 'cond)
     `(cond ,@(mapcar #'(lambda (f1)
			  (declare (type expression f1))
			  (mapcar #'translate-calls f1))
		      (rest expr))))
    ((expression-type? expr 'ecase)
     `(ecase ,(translate-calls (second expr))
	,@(mapcar #'(lambda (f1)
		      (declare (type expression f1))
		      (mapcar #'translate-calls f1))
		  (nthcdr 2 expr))))
    ((subroutine-call? expr)
     (expand-fortran-call (first expr) (mapcar #'translate-calls (rest expr))))
    (t (mapcar #'translate-calls expr))
    )
  )

;;;========================================================

(defun translate-statement-functions (funs)
  (declare (type list funs)
	   (values statement-function-definition-with-translated-body))

  (mapcar #'(lambda (expr)
	      (let ((fname (first expr))
		    (fargs (second expr))
		    (body  (nthcdr 2 expr)))
		(declare (type token fname)
			 (type list fargs body))
		`(,fname ,fargs ,@(mapcar #'translate-calls
					  (translate-arefs body)))))
	  funs))

;;;========================================================

(defun analyze-names (args decls body)
  (declare (type list args)
	   (type list decls)
	   (type list body)
	   (values data-initializations
		   statement-function-defs
		   translateed-executable-exprs))
  (multiple-value-bind
    (decls body) (multiple-value-call #'rename-common-variables
				      (substitute-parameters decls body))
    (declare (type list decls body))
    (build-dimension-table decls)
    (let* ((1st-executable
	     (position-if-not #'(lambda (expr)
				  (declare (type expression expr))
				  (statement-function-definition? expr))
	       body))
	   (executables (nthcdr 1st-executable body))
	   (statement-functions
	     (convert-statement-functions (subseq body 0 1st-executable)))
	   (data-inits (process-data-exprs decls)))
      (declare (type integer 1st-executable)
	       (type list executables statement-functions data-inits))
      (build-name-table args decls data-inits statement-functions executables)
      (values
	data-inits
	(translate-statement-functions statement-functions)
	(mapcar #'translate-calls (translate-arefs executables))
	)
      )
    )
  )


;;;=================================================================
;;;=================================================================
;;;			Code emitting routines:
;;;=================================================================
;;;=================================================================

(defun make-bindings (flag)
  (declare (type keyword flag)
	   (values list-of-bindings))
  (let ((bindings '()))
    (declare (type list bindings))
    (labels ((make-binding (name flags)
	       (declare (type token name)
			(type list flags))
	       (when (and (member flag flags)
			  (not (or (member :subroutine flags)
				   (member :intrinsic flags))))
		 (if (member :array flags)
		     (push (array-binding  name) bindings)
		     (push (scalar-binding name) bindings) ))))
      (maphash #'make-binding *name-table*)
      (values bindings) )))

;;;-----------------------------------------------------------------

(defun make-arg-slot-bindings ()
  (declare (values bindings-for-temp-vars-used-as-arg-slots))
  (mapcar #'(lambda (n)
	      (declare (type token n))
	      `(,n (make-array 1)))
	  *arg-slots*))

;;;--------------------------------------------------------

(defun make-declarations (flag)
  (declare (type keyword flag)
	   (values list-of-declarations))
  (let ((decls '()))
    (declare (type list decls))
    (labels
      ((make-declaration (name flags)
	 (declare (type token name)
		  (type list flags))
	 (when (member flag flags)
	   (cond
	     ((member :intrinsic flags)
	      nil ;; do nothing
	      )
	     ((member :subroutine flags)
	      (push (subroutine-declaration name) decls))
	     ((member :array flags)
	      (push (array-declaration name) decls)
	      (when (member :arg flags)
		(push (array-offset-declaration name) decls)))		    
	     (t (push (scalar-declaration name) decls)) ))))
      (maphash #'make-declaration *name-table*) )
    (values decls) ))

;;;--------------------------------------------------------

#+symbolics
(defun make-array-register-declarations ()
  (declare (values list-of-array-register-declarations))
  `(sys:array-register
     ,@(delete-if #'(lambda (a)
		      (declare (type token a))
		      (or (member :subroutine (name-flags a))
			  (member :common (name-flags a))))
		  (get-declared-arrays)) ))

#+symbolics
(defun make-array-rebindings ()
  (declare (values list-of-bindings-for-array-registers))
  (mapcar #'(lambda (a)
	      (declare (type token a))
	      (list a a))
	  (delete-if #'(lambda (a)
			 (declare (type token a))
			 (or (member :subroutine (name-flags a))
			     (member :common (name-flags a))))
		     (get-declared-arrays))))

;;;--------------------------------------------------------

(defun scalar-declaration (v)
  (declare (type variable v)
	   (values type-declaration))
  `(type ,(declared-type v) ,v))

(defun subroutine-declaration (v)
  (declare (type variable v)
	   (values function-declaration))
  `(function ,v (&optional &rest list) ,(declared-type v)))

(defun array-declaration (v)
  (declare (type variable v)
	   (values array-declaration))
  #+symbolics `(type (array * 1) ,v)
  #-symbolics `(type (simple-array ,(declared-type v) 1) ,v)
  )

(defun array-offset-declaration (v)
  (declare (type variable v)
	   (values type-declaration))
  `(type integer ,(offset-name v)))

;;;=================================================================

(defun array-binding (v)
  (declare (type variable v)
	   (values array-allocation))
  `(,v (make-array ,(apply #'* (declared-dimensions v))
		   #-symbolics :element-type #-symbolics ',(declared-type v) 
		   #+franz-inc :fill-pointer #+franz-inc t))
  )

;;;--------------------------------------------------------

(defun scalar-binding (v)
  (declare (type variable v)
	   (values initial-binding-for-scalar))
  
  `(,v ,(case (declared-type v)
	  (fixnum 0)
	  (single-float 0.0)
	  (double-float 0.0d0)
	  (complex #c(0.0 0.0))
	  (character "")
	  (t nil))))

;;;--------------------------------------------------------

(defun offset-arg-list (arg-list)
  (declare (type list arg-list)
	   (values arg-list-with-array-offset-inserted))
  (mapcan #'(lambda (arg)
	      (declare (type variable arg))
	      (if (member :subroutine (name-flags arg))
		  (list arg)
		  (list arg (offset-name arg))))
	  arg-list ))
    
;;;=================================================================

(defun translate-message (name)
  (declare (type variable name))
  (format *terminal-io* "~&Translating Subroutine: ~s~%" name))

(defun compile&load-messages (name)
  (declare (type variable name)
	   (values eval-when-forms-to-print-compile&load-messages))
  `((eval-when (compile)
      (format *terminal-io* "~&Compiling Subroutine: ~s~%" ',name))
    (eval-when (load)
      (format *terminal-io* "~&Loading Subroutine: ~s~%" ',name))))

;;;=================================================================

(defun defs-for-commons ()
  (declare (values bindings&declarations-for-common-variables))
  (nconc
    (mapcar #'(lambda (b)
		(declare (type expression b))
		(if (token? b) (list 'defvar b) (cons 'defvar b))) 
	    (make-bindings :common))
    (mapcar #'(lambda (d)
		(declare (type expression d))
		`(proclaim ',d))
	    (make-declarations :common))))

;;;=================================================================

(defun emit-subroutine (subr)
  
  (declare (type list subr)
	   (values lisp-code-for-translated-subroutine))  
  (let* ((sub-name (second subr))
	 (make-name (affix sub-name '-maker))
	 (args (third subr))
	 (decls (fourth subr))
	 (body (fifth subr)))
    (declare (type symbol sub-name make-name)
	     (type list args decls body))
    (translate-message sub-name)
    (setf *arg-slots* '())
    (multiple-value-bind
      (data-inits statement-funs executables) (analyze-names args decls body)
      `(eval-when (compile load eval)
	 ,@(compile&load-messages sub-name)
	 (export '(,sub-name))
	 ,@(defs-for-commons)
	 (defun ,make-name ()
	   (let (,@(make-bindings :lexical)
		 ,@(make-arg-slot-bindings))
	     (declare ,@(make-declarations :lexical))
	     ,@data-inits 
	     #'(lambda ,(offset-arg-list args) 
		 (declare ,@(make-declarations :arg))
		 (let* (,@(make-bindings :dynamic)
			#+symbolics ,@(make-array-rebindings))
		   (declare ,@(make-declarations :dynamic)
			    #+symbolics ,(make-array-register-declarations))
		   (labels ,statement-funs 
		     (block ,(block-name sub-name)
		       (tagbody ,@executables)))))))
	 (setf (subroutine ',sub-name) (,make-name))))))


;;;=================================================================
;;;=================================================================
;;;			     The Top Level
;;;=================================================================
;;;=================================================================

(defun print-package (the-package lisp-stream)

  (declare (type (or symbol package) the-package)
	   (type stream lisp-stream))	   

  #+(or symbolics ti)
  (format
    lisp-stream
    "~&;;; -*-Mode: Lisp; Syntax: Common-Lisp; Package: ~a; -*-~%~%"
    the-package
    )

  (print `(in-package ',the-package) lisp-stream)
  )

;;;-----------------------------------------------------------------
;;; <merge-pathnames> is unpredictable across common lisps.

(defun make-pathnames (in out)
  (declare (type (or string pathname) in out)
	   (values completed-in-pathname completed-out-pathname))

  #+symbolics (setf in (merge-pathnames in "*.fortran"))
  
  (if (= 0 (length out)) 
      (setf out (make-pathname :defaults "*.lisp"
				  :host (pathname-host in)
				  :device (pathname-device in)
				  :directory (pathname-directory in)
				  :name (pathname-name in)))
      ;; else
      #+symbolics
      (setf out (merge-pathnames
		  out
		  (make-pathname :defaults "*.lisp"
				 :host (pathname-host in)
				 :device (pathname-device in)
				 :directory (pathname-directory in)
				 :name (pathname-name in))))
      )
  (values in out)
  )

;;;-----------------------------------------------------------------

(defun polish-fortran (input &key (output "") (a-package 'Polish))

  (declare (type (or string pathname) input output)
	   (type (or symbol package) a-package))

  (multiple-value-setq (input output) (make-pathnames input output))
  
  (with-open-file (lisp-stream output :direction :output)
    (let* ((*print-length* nil)
	   (*print-level*  nil)
	   (*print-pretty* nil)
	   (*package* (find-package a-package))
	   (statements
	     (with-open-file (in-stream input) (read-fortran-file in-stream)))
	   (parsed-subroutines
	     (parse-subroutines (parse-single-statements statements))))
      (declare (type (or null fixnum) *print-length* *print-level*)
	       (type boolean *print-pretty*)
	       (type package *package*)
	       (type list statements parsed-subroutines))
      (print-package a-package lisp-stream)
      (map nil #'(lambda (sub)
		   (declare (type list sub))
		   (print (emit-subroutine sub) lisp-stream))
	   parsed-subroutines))
    output))

;;;-----------------------------------------------------------------

(defun parse-file (input &key (output ""))

  (declare (type (or string pathname) input output))

  (multiple-value-setq (input output) (make-pathnames input output))
  
  (with-open-file (lisp-stream output
		   :direction :output
		   :if-exists :overwrite)
    (let* ((*print-length* nil)
	   (*print-level*  nil)
	   (*print-pretty* t)
	   (statements
	     (with-open-file (in-stream input) (read-fortran-file in-stream)))
	   (parsed-subroutines
	     (parse-subroutines (parse-single-statements statements))))
      (declare (type (or null fixnum) *print-length* *print-level*)
	       (type boolean *print-pretty*)
	       (type list statements parsed-subroutines))
      (break)
      (map nil #'(lambda (s) (print s lisp-stream)) parsed-subroutines)
    output)))

