;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: (Polish Lisp); -*-
;;;
;;; Copyright 1987. John Alan McDonald. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package (or (find-package 'Polish)
		(make-package 'Polish
			      :nicknames '(po)
			      :use '(lisp)))
	    )

(export '( subroutine set-subroutine subroutine-call
	  continue arithmetic-if polish-/ eqv neqv int ifix idint real
	  polish-float sngl dble cmplx ichar polish-char
	  aint dint anint dnint nint idnint iabs dabs cabs
	  polish-mod amod dmod isign sign dsign idim dim ddim dprod
	  max0 amax1 dmax1 amax0 max1 min0 amin1 dmin1 amin0 min1
	  len index lge lgt lle llt aimag conjg dsqrt csqrt dexp
	  cexp alog dlog clog log10 alog10 dlog10 dsin csin dcos
	  ccos dtan dasin dacos atan2 datan2 dsinh dcosh dtanh))

(import '(subroutine subroutine-call set-subroutine) 'user)

;;;=================================================================

(eval-when (compile load eval)
  (defun subroutine (sub-name) (get sub-name :subroutine))
  (defun set-subroutine (sub-name function-object)
    (setf (get sub-name :subroutine) function-object))
  (defsetf subroutine set-subroutine)
  (deftype boolean () '(member t nil))
  )

;;;=================================================================
;;;
;;; This assumes that each argument is a variable whose value is a one
;;; dimensional array to be passed to the fortran routine. It might be
;;; worth writing a more sophisticated version of this: Automatically
;;; creating displaced arrays for n-dim lisp arrays, allowing fortran
;;; style array element arguments (pointers into the displaced array),
;;; etc.

(defmacro subroutine-call (name &body args)

  `(funcall (subroutine ',name) ,@(mapcan #'(lambda (x) (list x 0)) args))) 

;;;=================================================================

(defmacro continue (&optional &rest junk) (declare (ignore junk)) nil) 

(defmacro arithmetic-if (pred then<0 then=0 then>0)
  `(cond ((< ,pred 0) (go ,then<0))
	 ((= ,pred 0) (go ,then=0))
	 ((> ,pred 0) (go ,then>0))))

;;;=================================================================
;;; Intrinsic functions:
;;;=================================================================

(defmacro polish-/ (a1 a2)
  (declare (type number a1 a2))
  ;; so that integer division works properly
  #+symbolics
  `(zl:/ ,a1 ,a2)
  #-symbolics
  `(if (and (integerp ,a1) (integerp ,a2))
       (truncate ,a1 ,a2)
       (/ ,a1 ,a2)) )

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defmacro eqv (a b) `(eql (null ,a) (null ,b)) )
(defmacro neqv (a b) `(not (eqv ,a ,b)))

;;;- - - - - - - - - - - - - - - - - - - - - - - - - - - - 

(defmacro int    (a1) `(truncate ,a1))
(defmacro ifix   (a1) `(truncate ,a1))
(defmacro idint  (a1) `(truncate ,a1))
(defmacro real   (a1) `(float (realpart ,a1) 1.0f0))
(defmacro polish-float (a1) `(float (realpart ,a1) 1.0f0))
(defmacro sngl   (a1) `(float ,a1 1.0f0))
(defmacro dble   (a1) `(float ,a1 1.0d0))
(defmacro cmplx  (&rest args) `(complex ,@args))
(defmacro ichar  (a1) `(char-code ,a1))
(defmacro polish-char (a1) `(code-char ,a1))
(defmacro aint   (a1) `(ftruncate ,a1))
(defmacro dint   (a1) `(ftruncate ,a1))
(defmacro anint  (a1) `(fround ,a1)) ;; this is not exactly right
(defmacro dnint  (a1) `(fround ,a1)) ;; this is not exactly right
(defmacro nint   (a1) `(round ,a1))
(defmacro idnint (a1) `(round ,a1))

(defun iabs (a1) (abs a1))
(defun dabs (a1) (abs a1))
(defun cabs (a1) (abs a1))

(defmacro polish-mod (a1 a2) `(rem ,a1 ,a2))
(defmacro amod  (a1 a2) `(rem ,a1 ,a2))
(defmacro dmod  (a1 a2) `(rem ,a1 ,a2))
(defmacro isign (a1 a2) `(* (abs ,a1) (signum ,a2)))
(defmacro sign  (a1 a2) `(* (abs ,a1) (signum ,a2)))
(defmacro dsign (a1 a2) `(* (abs ,a1) (signum ,a2)))
(defmacro idim  (a1 a2) `(max 0 (- ,a1 ,a2)))
(defmacro dim   (a1 a2) `(max 0 (- ,a1 ,a2)))
(defmacro ddim  (a1 a2) `(max 0 (- ,a1 ,a2)))
(defmacro dprod (a1 a2) `(* (float ,a1 1.0d0) ,a2))

(defmacro max0  (&rest args) `(max ,@args))
(defmacro amax1 (&rest args) `(max ,@args))
(defmacro dmax1 (&rest args) `(max ,@args))
(defmacro amax0 (&rest args) `(float    (max ,@args)))
(defmacro max1  (&rest args) `(truncate (max ,@args)))
(defmacro min0  (&rest args) `(min ,@args))
(defmacro amin1 (&rest args) `(min ,@args))
(defmacro dmin1 (&rest args) `(min ,@args))
(defmacro amin0 (&rest args) `(float    (min ,@args)))
(defmacro min1  (&rest args) `(truncate (min ,@args)))

;;(defmacro len   (a1 a2))
;;(defmacro index (a1 a2))

;;(defmacro lge (a1 a2) `(char>= ,a1 ,a2))
;;(defmacro lgt (a1 a2) `(char>  ,a1 ,a2))
;;(defmacro lle (a1 a2) `(char-lessp= ,a1 ,a2))
;;(defmacro llt (a1 a2) `(char-lessp  ,a1 ,a2))

(defun aimag  (a1) (imagpart a1))
(defun conjg  (a1) (conjugate a1))
(defun dsqrt  (a1) (sqrt a1))
(defun csqrt  (a1) (sqrt a1))
(defun dexp   (a1) (exp a1))
(defun cexp   (a1) (exp a1))
(defun alog   (a1) (log a1))
(defun dlog   (a1) (log a1))
(defun clog   (a1) (log a1))
(defun log10  (a1) (/ (log a1) (log 10)))
(defun alog10 (a1) (/ (log a1) (log 10)))
(defun dlog10 (a1) (/ (log a1) (log 10)))
(defun dsin   (a1) (sin a1))
(defun csin   (a1) (sin a1))
(defun dcos   (a1) (cos a1))
(defun ccos   (a1) (cos a1))
(defun dtan   (a1) (tan a1))
(defun dasin  (a1) (asin a1))
(defun dacos  (a1) (acos a1))
(defun datan  (a1) (atan a1))
(defun atan2  (a1 a2) (atan (/ a1 a2)))
(defun datan2 (a1 a2) (atan (/ a1 a2)))
(defun dsinh  (a1) (sinh a1))
(defun dcosh  (a1) (cosh a1))
(defun dtanh  (a1) (tanh a1)) 

