c  -*- mode: fortran; package: user -*-
c - - - - - - - - - - - - - - - - - - - - - - - - - - 
      real function dummy(i,x,y,f)
      integer i
      real x,y 
      real a,b
      real v,w
      complex z
      external f
      real f
      dimension r(3,3)
      dimension s(3,3)
      data info,r/12,3*1.3e10,3*2.0,3*3.0/
      data v/1.0/,w/2.0/
      data z/(1.0,1.0)/
      data ((s(i,j),j=1,2),i=1,3)/3*1.0,3*2.0,3*3.0/
      data s(1,3),s(2,3),s(3,3)/1.0,2.0,3.0/
      save a,b
      conj(z) = cmplx(aimag(z),real(z))
 150  if (x.eq.1.d-2.and..not.1e+3.le.y) 
     $ y=b*.2+a*1.e-10

      if (r(info,i) .eq. 0.0e0) go to 150
      z = conj(z) + cmplx(v,w) + (1.0,1.0)
      if (x.gt.1e-2) then
        z = f(x)
      else if (x.eq.1.5e3) then
        if (x.ge.y) then
          if (z.eq. x + y) z = f(y)
        else  
	  do 100 i = 1,3
            do 100 j = 1,3
100	      s(i,j) = f(float(i*j))
         endif
      else if (x.lt.100) then
	 z = f(x*y)
         a = z
         b = s(1,1)
      endif
	dummy = f(x)
      return
      end
c - - - - - - - - - - - - - - - - - - - - - - - - - - 
	subroutine fee
	external dummy
	call foo(1.0,2.0,dummy)
	return
	stop
	end
c - - - - - - - - - - - - - - - - - - - - - - - - - - 
	subroutine foo (x,y,extern)
	dimension x(1),y(1)
	external extern
	intrinsic sin
c
	a = extern(1,x(1),y(1),sin)
        return
	end
