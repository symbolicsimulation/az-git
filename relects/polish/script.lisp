;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Polish; -*-
;;;=======================================================

(in-package :Polish)

;;;=======================================================

(parse-file "/usr/local/cmlib/eispack/tred2.f"
	    :output "tred2.lisp")

(parse-file "/usr/local/cmlib/eispack/tql2.f"
	    :output "tql2.lisp")

(parse-file "dbp.f" :output "out.lisp")

(setf statements
  (with-open-file (in-stream "dbp.f")
    (read-fortran-file in-stream)))
