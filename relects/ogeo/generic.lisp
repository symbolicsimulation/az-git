;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)
	
;;;============================================================

(defstruct (%Diagonal-C<->S-Map
	    (:conc-name "")
	    (:copier nil))

  "A structure with slots for mapping either way Chart and Screen space."

  (%diagonal-c<->s-chart-origin (make-chart-point)
				:type Chart-Point)
  (%diagonal-c<->s-dlinear-map (make-chart-vector :x 1.0d0 :y 1.0d0)
			       :type Chart-Vector)
  (%diagonal-c<->s-screen-origin (make-screen-point)
				 :type Screen-Point))

;;;------------------------------------------------------------

(defstruct (Diagonal-C->S-Map
	    (:include %Diagonal-C<->S-Map)
	    (:constructor %make-diagonal-c->s-map))
  "Maps from Chart to Screen space.")

;;; a "safe" constructor

(defun make-diagonal-c->s-map (&key
			       (chart-origin (make-chart-point))
			       (dlinear-map
				(make-chart-vector :x 1.0d0 :y 1.0d0))
			       (screen-origin (make-screen-point)))
  (declare (type Chart-Point chart-origin)
	   (type Chart-Vector dlinear-map)
	   (type Screen-Point screen-origin))
  (%make-diagonal-c->s-map :%diagonal-c<->s-chart-origin chart-origin
			   :%diagonal-c<->s-dlinear-map dlinear-map
			   :%diagonal-c<->s-screen-origin screen-origin))

;;;------------------------------------------------------------

(defstruct (Diagonal-S->C-Map
	    (:include %Diagonal-C<->S-Map)
	    (:constructor %make-diagonal-s->c-map))
  "Inverse maps from Screen to Chart space.")

;;; a "safe" constructor

(defun make-diagonal-s->c-map (&key
			       (chart-origin (make-chart-point))
			       (dlinear-map
				(make-chart-vector :x 1.0d0 :y 1.0d0))
			       (screen-origin (make-screen-point)))
  (declare (type Chart-Point chart-origin)
	   (type Chart-Vector dlinear-map)
	   (type Screen-Point screen-origin))
  (%make-diagonal-s->c-map :%diagonal-c<->s-chart-origin chart-origin
			   :%diagonal-c<->s-dlinear-map dlinear-map
			   :%diagonal-c<->s-screen-origin screen-origin))

;;;------------------------------------------------------------
;;; home cooked type inheritance:

(deftype Diagonal-C<->S-Map ()
  "A map between Screen and Chart space, in one direction or the other."
  '(or Diagonal-C->S-Map Diagonal-S->C-Map))

;;;------------------------------------------------------------
;;; "safe" accessors

(defun chart-origin (map)
  (declare (type Diagonal-C<->S-Map map)
	   (:returns (type Chart-Point)))
  (%diagonal-c<->s-chart-origin map))

(defun set-chart-origin (map new-origin)
  (declare (type Diagonal-C<->S-Map map)
	   (type Chart-Point new-origin)
	   (:returns (type Chart-Point)))
  (setf (%diagonal-c<->s-chart-origin map) new-origin))

(defsetf chart-origin (map) (new-origin)
  `(set-chart-origin ,map ,new-origin))

(defun dlinear-map (map)
  (declare (type Diagonal-C<->S-Map map)
	   (:returns (type Chart-Vector)))
  (%diagonal-c<->s-dlinear-map map))

(defun set-dlinear-map (map new-origin)
  (declare (type Diagonal-C<->S-Map map)
	   (type Chart-Vector new-origin))
  (setf (%diagonal-c<->s-dlinear-map map) new-origin))

(defsetf dlinear-map (map) (new-origin)
  `(set-dlinear-map ,map ,new-origin))

(defun screen-origin (map)
  (declare (type Diagonal-C<->S-Map map)
	   (:returns (type Screen-Point)))
  (%diagonal-c<->s-screen-origin map))

(defun set-screen-origin (map new-origin)
  (declare (type Diagonal-C<->S-Map map)
	   (type Screen-Point new-origin))
  (setf (%diagonal-c<->s-screen-origin map) new-origin))

(defsetf screen-origin (map) (new-origin)
  `(set-screen-origin ,map ,new-origin))

;;;------------------------------------------------------------

(defun make-affine-map-between (c s)

  "Make the affine map that takes the Chart-Rect <c> to the
Screen-Rect <s>."

  (declare (type Chart-Rect c)
	   (type Screen-Rect s)
	   (:returns (type Diagonal-C->S-Map)))

  (let ((cl (chart-rect-left c))
	(cr (chart-rect-right c))
	(ct (chart-rect-top c))
	(cb (chart-rect-bottom c))
	(sl (screen-rect-left s))
	(sr (screen-rect-right s))
	(st (screen-rect-top s))
	(sb (screen-rect-bottom s)))
    (make-diagonal-c->s-map
     :chart-origin (make-chart-point :x cl :y ct)
     :screen-origin (make-screen-point :x sl :y st)
     :dlinear-map (make-chart-vector
		   :x (/ (az:fl (- sr sl)) (- cr cl))
		   :y (/ (az:fl (- st sb)) (- ct cb))))))


;;;============================================================
;;; Transform "methods"
;;;============================================================

(defun %transform-c->s-x-coordinate (map x)
  (declare (optimize (safety 0) (speed 3))
	   (type Diagonal-C<->S-Map map)
	   (type Chart-Coordinate x))
  (+ (%screen-point-x (%diagonal-c<->s-screen-origin map))
     (the Screen-Coordinate
       (truncate
	(the Chart-Coordinate
	  (* (%chart-vector-x (%diagonal-c<->s-dlinear-map map))
	     (- x (%chart-point-x (%diagonal-c<->s-chart-origin map)))))))))

(defun transform-c->s-x-coordinate (map x)
  (declare (type Diagonal-C->S-Map map)
	   (type Chart-Coordinate x)
	   (:returns (type Screen-Coordinate)))
  (%transform-c->s-x-coordinate map x))

(defun %transform-c->s-y-coordinate (map y)
  (declare (optimize (safety 0) (speed 3))
	   (type Diagonal-C<->S-Map map)
	   (type Chart-Coordinate y))
  (+ (%screen-point-y (%diagonal-c<->s-screen-origin map))
     (the Screen-Coordinate
       (truncate
	(the Chart-Coordinate
	  (* (%chart-vector-y (%diagonal-c<->s-dlinear-map map))
	     (- y (%chart-point-y (%diagonal-c<->s-chart-origin map)))))))))

(defun transform-c->s-y-coordinate (map y)
  (declare (type Diagonal-C->S-Map map)
	   (type Chart-Coordinate y)
	   (:returns (type Screen-Coordinate)))
  (%transform-c->s-y-coordinate map y))

;;;------------------------------------------------------------

(defun %transform-c->s-vector (map wv result)
  (declare (optimize (safety 0) (speed 3))
	   (type Diagonal-C<->S-Map map)
	   (type Chart-Vector wv)
	   (type Screen-Vector result))
  (let ((lv (%diagonal-c<->s-dlinear-map map)))
    (declare (type Chart-Vector lv))
    (setf (%screen-vector-x result)
      (the Screen-Coordinate
	(truncate
	 (the Chart-Coordinate
	   (* (%chart-vector-x lv) (%chart-vector-x wv))))))
    (setf (%screen-vector-y result)
      (the Screen-Coordinate
	(truncate
	 (the Chart-Coordinate
	   (* (%chart-vector-y lv) (%chart-vector-y wv))))))
    result))

(defun transform-c->s-vector (map wv &key (result (make-screen-vector)))
  (declare (type Diagonal-C->S-Map map)
	   (type Chart-Vector wv)
	   (type Screen-Vector result)
	   (:returns result))
  (%transform-c->s-vector map wv result))

;;;------------------------------------------------------------

(defun %transform-c->s-point (map cp result)
  (declare (optimize (safety 0) (speed 3))
	   (type Diagonal-C->S-Map map)
	   (type Chart-Point cp)
	   (type Screen-Point result))
  (let ((co (%diagonal-c<->s-chart-origin map))
	(lv (%diagonal-c<->s-dlinear-map map))
	(so (%diagonal-c<->s-screen-origin map)))
    (declare (type Chart-Point co)
	     (type Chart-Vector lv)
	     (type Screen-Point so))
    (setf (%screen-point-x result)
      (+ (%screen-point-x so)
	 (the Screen-Coordinate
	   (truncate
	    (the Chart-Coordinate
	      (* (%chart-vector-x lv)
		 (the Chart-Coordinate
		   (- (%chart-point-x cp)
		      (%chart-point-x co)))))))))
    (setf (%screen-point-y result)
      (+ (%screen-point-y so)
	 (the Screen-Coordinate
	   (truncate
	    (the Chart-Coordinate
	      (* (%chart-vector-y lv)
		 (the Chart-Coordinate
		   (- (%chart-point-y cp)
		      (%chart-point-y co)))))))))
    result))

(defun transform-c->s-point (map cp &key (result (make-screen-point)))
  (declare (type Diagonal-C->S-Map map)
	   (type Chart-Point cp)
	   (type Screen-Point result)
	   (:returns result))
  (%transform-c->s-point map cp result))

;;;------------------------------------------------------------

(defun %transform-c->s-rect (map cr result)
  (declare (optimize (safety 0) (speed 3))
	   (type Diagonal-C->S-Map map)
	   (type Chart-Rect cr)
	   (type Screen-Rect result))
  (let* ((co (%diagonal-c<->s-chart-origin map))
	 (lv (%diagonal-c<->s-dlinear-map map))
	 (so (%diagonal-c<->s-screen-origin map))
	 (cox (%chart-point-x co))
	 (coy (%chart-point-y co))
	 (lvx (%chart-vector-x lv))
	 (lvy (%chart-vector-y lv))
	 (sox (%screen-point-x so))
	 (soy (%screen-point-y so)))
    (declare (type Chart-Point co)
	     (type Chart-Vector lv)
	     (type Screen-Point so)
	     (type Chart-Coordinate cox coy lvx lvy)
	     (type Screen-Coordinate sox soy))
    (%set-screen-rect-coords
     result
     (+ sox
	(the Screen-Coordinate
	  (truncate
	   (the Chart-Coordinate
	     (* lvx (- (%chart-rect-xmin cr) cox))))))
     (+ soy
	(the Screen-Coordinate
	  (truncate
	   (the Chart-Coordinate
	     (* lvy (- (%chart-rect-ymax cr) coy))))))
     (truncate (* lvx (%chart-rect-width cr)))
     (truncate (* (- lvy) (%chart-rect-height cr))))
    #|| safe slow version 
    (set-screen-rect-coords
     result
     :xmin 
     (+ sox
	(the Screen-Coordinate
	  (truncate
	   (the Chart-Coordinate
	     (* lvx (- (%chart-rect-xmin cr) cox))))))
     :xmax
     (+ 1 sox
	(the Screen-Coordinate
	  (truncate
	   (the Chart-Coordinate
	     (* lvx (- (%chart-rect-xmax cr) cox))))))
     :ymin
     (+ soy
	(the Screen-Coordinate
	  (truncate
	   (the Chart-Coordinate
	     (* lvy (- (%chart-rect-ymax cr) coy))))))
     :ymax
     (+ 1 soy
	(the Screen-Coordinate
	  (truncate
	   (the Chart-Coordinate
	     (* lvy (- (%chart-rect-ymin cr) coy)))))))
    ||#
    ))

(defun transform-c->s-rect (map cr &key (result (make-screen-rect)))
  (declare (type Diagonal-C->S-Map map)
	   (type Chart-Rect cr)
	   (type Screen-Rect result)
	   (:returns result))
  (%transform-c->s-rect map cr result))

;;;------------------------------------------------------------

(defun %transform-s->c-vector (map sv result)
  (let ((lv (dlinear-map map)))
    (setf (chart-vector-x result)
      (* (chart-vector-x lv)
	 (az:fl (screen-vector-x sv))))
    (setf (chart-vector-y result)
      (* (chart-vector-y lv)
	 (az:fl (screen-vector-y sv))))
    result))

(defun transform-s->c-vector (map sv &key (result (make-chart-vector)))
  (declare (type Diagonal-S->C-Map map)
	   (type Screen-Vector sv)
	   (type Chart-Vector result)
	   (:returns result))
  (%transform-s->c-vector map sv result))

;;;------------------------------------------------------------

(defun %transform-s->c-point (map sp result)
  (let ((so (screen-origin map))
	(lv (dlinear-map map))
	(co (chart-origin map)))
    (setf (chart-point-x result)
      (+ (chart-point-x co)
	 (* (chart-vector-x lv)
	    (az:fl (- (screen-point-x sp) (screen-point-x so))))))
    (setf (chart-point-y result)
      (+ (chart-point-y co)
	 (* (chart-vector-y lv)
	    (az:fl (- (screen-point-y sp) (screen-point-y so))))))
    result))

(defun transform-s->c-point (map sp &key (result (make-chart-point)))
  (declare (type Diagonal-S->C-Map map)
	   (type Screen-Point sp)
	   (type Chart-Point result)
	   (:returns result))
  (%transform-s->c-point map sp result))

;;;------------------------------------------------------------

(defun %transform-s->c-rect (map sr result)
  (declare (optimize (safety 0) (speed 3))
	   (type Diagonal-S->C-Map map)
	   (type Screen-Rect sr)
	   (type Chart-Rect result))
  (let* ((co (%diagonal-c<->s-chart-origin map))
	 (lv (%diagonal-c<->s-dlinear-map map))
	 (so (%diagonal-c<->s-screen-origin map))
	 (cox (%chart-point-x co))
	 (coy (%chart-point-y co))
	 (lvx (%chart-vector-x lv))
	 (lvy (%chart-vector-y lv))
	 (sox (%screen-point-x so))
	 (soy (%screen-point-y so)))
    (declare (type Chart-Point co)
	     (type Chart-Vector lv)
	     (type Screen-Point so)
	     (type Chart-Coordinate cox coy lvx lvy)
	     (type Screen-Coordinate sox soy))
    (%set-chart-rect-coords
     result
     (+ cox (* lvx (the Chart-Coordinate
		     (az:fl (the Screen-Coordinate
			      (- (%screen-rect-left sr) sox))))))
     (+ coy (* lvy (the Chart-Coordinate
		     (az:fl (the Screen-Coordinate
			      (- (%screen-rect-top sr) soy))))))
     (* (chart-vector-x lv)
	(az:fl (%screen-rect-width sr)))
     (* (- (chart-vector-y lv))
	(az:fl (%screen-rect-height sr))))
    #|| safe slow version
    (set-chart-rect-coords
     result
     :xmin (+ cox (* lvx (the Chart-Coordinate
			   (az:fl (the Screen-Coordinate
				    (- (%screen-rect-left sr) sox))))))
     :xmax (+ cox (* lvx (the Chart-Coordinate
			   (az:fl (the Screen-Coordinate
				    (- (%screen-rect-right sr) sox))))))
     :ymin (+ coy (* lvy (the Chart-Coordinate
			   (az:fl (the Screen-Coordinate
				    (- (%screen-rect-top sr) soy))))))
     :ymax (+ coy (* lvy (the Chart-Coordinate
			   (az:fl (the Screen-Coordinate
				    (- (%screen-rect-bottom sr) soy)))))))
    ||#
    ))

(defun transform-s->c-rect (map sr &key (result (make-screen-rect)))
  (declare (type Diagonal-S->C-Map map)
	   (type Screen-Rect sr)
	   (type Chart-Rect result)
	   (:returns result))
  (%transform-s->c-rect map sr result))

;;;------------------------------------------------------------

(defun c<->s-check-type-match (a b)
  (declare (type (or Screen-Object Chart-Object Diagonal-C<->S-Map) a b)
	   (:returns (type (member T Nil))))
  (etypecase a
    (Chart-Vector (az:declare-check (type Screen-Vector b)))
    (Chart-Point (az:declare-check (type Screen-Point b)))
    (Chart-Rect (az:declare-check (type Screen-Rect b)))
    (Screen-Vector (az:declare-check (type Chart-Vector b)))
    (Screen-Point (az:declare-check (type Chart-Point b)))
    (Screen-Rect (az:declare-check (type Chart-Rect b)))
    (Diagonal-C->S-Map (az:declare-check (type Diagonal-S->C-Map b)))
    (Diagonal-S->C-Map (az:declare-check (type Diagonal-C->S-Map b)))))

(defun c<->s-make-matching-object (a)

  "Make an object suitable to be the result of applying <transform> 
-- or perhaps <inverse> -- to <a>, eg., if passed a Screen-Vector, 
make a Chart-Vector."

  (declare (type (or Screen-Object Chart-Object Diagonal-C<->S-Map) a)
	   (:returns (type (or Chart-Object
			       Screen-Object Diagonal-C<->S-Map))))

  (etypecase a
    (Chart-Point (make-screen-point))
    (Chart-Rect (make-screen-rect))
    (Chart-Vector (make-screen-vector))
    (Screen-Point (make-chart-point))
    (Screen-Rect (make-chart-rect))
    (Screen-Vector (make-chart-vector))
    (Diagonal-C->S-Map (make-diagonal-s->c-map))
    (Diagonal-S->C-Map (make-diagonal-c->s-map))))

;;;------------------------------------------------------------
;;; home cooked method lookup

(defun transform (map x &key (result (c<->s-make-matching-object x)))
  "Apply <map> to <x> and put the answer in <result>."
  (declare (type Diagonal-C<->S-Map map)
	   (type (or Chart-Object Screen-Object) x result)
	   (:returns result))
  (c<->s-check-type-match x result)
  (etypecase map
    (Diagonal-C->S-Map
     (etypecase x
       (Chart-Vector (%transform-c->s-vector map x result))
       (Chart-Point (%transform-c->s-point map x result))
       (Chart-Rect (%transform-c->s-rect map x result))))
    (Diagonal-S->C-Map
     (etypecase x
       (Screen-Vector (%transform-s->c-vector map x result))
       (Screen-Point (%transform-s->c-point map x result))
       (Screen-Rect (%transform-s->c-rect map x result))))))

;;;------------------------------------------------------------

(defun inverse (map &key (result (c<->s-make-matching-object map)))
  "Put the (pseudo) inverse of <map> in <result>."
  (declare (type Diagonal-C<->S-Map map result)
	   (:returns result))
  (c<->s-check-type-match map result)
  (copy-screen-point (screen-origin map) :result (screen-origin result))
  (copy-chart-point (chart-origin map) :result (chart-origin result))
  (let* ((v0 (dlinear-map map))
	 (x0 (chart-vector-x v0))
	 (y0 (chart-vector-y v0))
	 (v1 (dlinear-map result)))
    (setf (chart-vector-x v1) (if (zerop x0) 0.0d0 (/ x0)))
    (setf (chart-vector-y v1) (if (zerop y0) 0.0d0 (/ y0))))
  result)



