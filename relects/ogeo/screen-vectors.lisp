;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================
;;; Screen-Vectors
;;;============================================================

(defstruct (Screen-Vector
	    (:conc-name nil)
	    (:constructor %%make-screen-vector)
	    (:copier %screen-vector-copier)
	    (:print-function print-screen-vector))

  "Screen-Vectors are two-dimensional vectors. They are used to represent
differences between screen points (eg. translations of screen
points, screen rect sizes, etc.).

We use a coordinate system where x increases from left to right,
and, unfortunately, like most window systems, y from top to bottom."

  (%screen-vector-x 0 :type Screen-Coordinate)
  (%screen-vector-y 0 :type Screen-Coordinate))

(defun %screen-vector? (v) (typep v 'Screen-Vector))

(defun %positive-screen-vector? (v)
  (and (typep v 'Screen-Vector)
       (>= (%screen-vector-x v) 0)
       (>= (%screen-vector-y v) 0)))

(deftype Positive-Screen-Vector ()

  "A Screen-Vector whose coordinates are positive, ie., which lies in
the lower right quadrant (note contrast with Chart-Vectors)."

  '(satisfies %positive-screen-vector?))

;;;------------------------------------------------------------

(defun print-screen-vector (v stream depth)
  "Print screen rects with their coords."
  (declare (type Screen-Vector v)
	   (type Stream stream)
	   (type (or Null Integer) depth)
	   (ignore depth))
  (az:printing-random-thing
   (v stream)
   (format stream "V ~d ~d"
	   (%screen-vector-x v)
	   (%screen-vector-y v))))

;;;-------------------------------------------------------

(defun %make-screen-vector (x y)
  (declare (type Screen-Coordinate x y))
  (%%make-screen-vector :%screen-vector-x x :%screen-vector-y y))

(defun make-screen-vector (&key (x 0) (y 0))
  "The constructor function for Screen-Vector.

The vector is optionally initialized with the given <x> and <y>
coordinates."

  (declare (type Screen-Coordinate x y)
		    (:returns (type Screen-Vector)))
  (%make-screen-vector x y))

;;;-------------------------------------------------------

(defun screen-vector-x (v)
  "Accessor function for x coordinate."
  (declare (type Screen-Vector v)
		    (:returns (type Screen-Coordinate)))
  (%screen-vector-x v))

(defun screen-vector-y (v)
  "Accessor function for y coordinate."
  (declare (type Screen-Vector v)
		    (:returns (type Screen-Coordinate)))
  (%screen-vector-y v))

(defun set-screen-vector-x (v x)
  (declare (type Screen-Vector v)
		    (type Screen-Coordinate x))
  (setf (%screen-vector-x v) x))

(defun set-screen-vector-y (v y)
  (declare (type Screen-Vector v)
		    (type Screen-Coordinate y))
  (setf (%screen-vector-y v) y))

(defsetf screen-vector-x (v) (x)
   "Sets the vector's x coordinate."
   `(set-screen-vector-x ,v ,x))

(defsetf screen-vector-y (v) (y)
   "Sets the vector's y coordinate."
   `(set-screen-vector-y ,v ,y))

;;;-------------------------------------------------------

(defun %set-screen-vector-coords (v x y)
  (declare (type Screen-Vector v)
	   (type Screen-Coordinate x y))
  (setf (%screen-vector-x v) x)
  (setf (%screen-vector-y v) y)
  v)

(defun set-screen-vector-coords (v &key (x 0) (y 0))
  (declare (type Screen-Vector v)
		    (type Screen-Coordinate x y))
  (%set-screen-vector-coords v x y))

;;;-------------------------------------------------------

(defun %copy-screen-vector (v0 v1)
  (declare (type Screen-Vector v0 v1))
  (setf (%screen-vector-x v1) (%screen-vector-x v0))
  (setf (%screen-vector-y v1) (%screen-vector-y v0))
  v1)

(defun copy-screen-vector (v &key (result (make-screen-vector)))
  "The copier function for Screen-Vectors."
  (declare (type Screen-Vector v result)
		    (:returns result))
  (%copy-screen-vector v result))

;;;-------------------------------------------------------

(defun %equal-screen-vectors? (v0 v1)
  (declare (type Screen-Vector v0 v1))
  (and (= (%screen-vector-x v0) (%screen-vector-x v1))
       (= (%screen-vector-y v0) (%screen-vector-y v1))))

(defun equal-screen-vectors? (v0 v1)
  "The equality predicate for Screen-Vectors."
  (declare (type Screen-Vector v0 v1)
		    (:returns (type (Member T Nil))))
  (%equal-screen-vectors? v0 v1))

;;;-------------------------------------------------------

(defun %screen-vector-list? (vs)
  (every #'%screen-vector? vs))

(deftype Screen-Vector-List ()
  "A list of Screen-Vectors."
  '(satisfies %screen-vector-list?))

(defun %positive-screen-vector-list? (vs)
  (every #'%positive-screen-vector? vs))

(deftype Positive-Screen-Vector-List ()
  "A list of Positive-Screen-Vectors."
  '(satisfies %positive-screen-vector-list?))

;;;-------------------------------------------------------
;;; beginnings of Screen-Vector algebra:
;;;-------------------------------------------------------
;;; Note that it's ok for <result> to be <eq> to either
;;; <v0> or <v1>

(eval-when (compile load eval)
  (proclaim '(Inline
	      %linear-mix-screen-vectors
	      %add-screen-vectors
	      %subtract-screen-vectors)))

(defun %linear-mix-screen-vectors (a0 v0 a1 v1 result)
  (declare (type Number a0 a1)
	   (type Screen-Vector v0 v1 result))
  (setf (%screen-vector-x result) (round (+ (* a0 (%screen-vector-x v0))
					    (* a1 (%screen-vector-x v1)))))
  (setf (%screen-vector-y result) (round (+ (* a0 (%screen-vector-y v0))
					    (* a1 (%screen-vector-y v1)))))
  result)

(defun linear-mix-screen-vectors (a0 v0 a1 v1
				  &key (result (make-screen-vector)))
  "Computes a linear combination of <v0> and <v1>. Note that it's ok
for <result> to be eq to either <v0> or <v1> (or both)."
  (declare (type Number a0 a1)
		    (type Screen-Vector v0 v1 result)
		    (:returns result))
  (%linear-mix-screen-vectors a0 v0 a1 v1 result))

(defun %add-screen-vectors (v0 v1 result)
  (declare (type Screen-Vector v0 v1 result))
  (setf (%screen-vector-x result) (+ (%screen-vector-x v0)
				     (%screen-vector-x v1)))
  (setf (%screen-vector-y result) (+ (%screen-vector-y v0)
				     (%screen-vector-y v1)))
  result) 

(defun add-screen-vectors (v0 v1 &key (result (%make-screen-vector 0 0)))
  "Add <v0> and <v1>. Note that it's ok for <result> to be eq to
either <v0> or <v1> (or both)."
  (declare (type Screen-Vector v0 v1 result)
		    (:returns result))
  (%add-screen-vectors v0 v1 result))

(defun %subtract-screen-vectors (v0 v1 result)
  (declare (type Screen-Vector v0 v1 result))
  (setf (%screen-vector-x result) (- (%screen-vector-x v0)
				     (%screen-vector-x v1)))
  (setf (%screen-vector-y result) (- (%screen-vector-y v0)
				     (%screen-vector-y v1)))
  result)

(defun subtract-screen-vectors (v0 v1 &key (result (%make-screen-vector 0 0)))
  
  "Subtract <v1> from <v0>. Note that it's ok for <result> to be eq
to either <v0> or <v1> (or both)."
  (declare (type Screen-Vector v0 v1 result)
		    (:returns result))
  (%subtract-screen-vectors v0 v1 result))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline
	      %screen-vector-l2-norm2
	      %screen-vector-l1-norm)))

(defun %screen-vector-l2-norm2 (v)
  (declare (type Screen-Vector v))
  (let ((x (%screen-vector-x v))
	(y (%screen-vector-y v)))
    (declare (type Screen-Coordinate x y))
    (+ (* x x) (* y y))))

(defun screen-vector-l2-norm2 (v)
  "Computes the squared L2 norm of <v>."
  (declare (type Screen-Vector v)
		    (:returns (type Positive-Screen-Coordinate)))
  (%screen-vector-l2-norm2 v))

(defun screen-vector-l2-norm (v)
  "Computes the L2 norm of <v>."
  (declare (type Screen-Vector v)
		    (:returns (type Positive-Chart-Coordinate)))
  (sqrt (screen-vector-l2-norm2 v)))

(defun %screen-vector-l1-norm (v)
  (declare (type Screen-Vector v))
  (+ (abs (the Screen-Coordinate (%screen-vector-x v)))
     (abs (the Screen-Coordinate (%screen-vector-y v)))))

(defun screen-vector-l1-norm (v)
  "Computes the L1 norm of <v>."
  (declare (type Screen-Vector v)
		    (:returns (type Positive-Screen-Coordinate)))
  (%screen-vector-l1-norm v))
  
;;;=======================================================
;;; A resource for borrowing temporary Screen-Vector objects:
;;;=======================================================

(defparameter *screen-vector-resource* ())

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %borrow-screen-vector)))

(defun %borrow-screen-vector (x y)
  (declare (special *screen-vector-resource*)
	   (type Screen-Coordinate x y)
	   (:returns (type Screen-Vector)))
  (let ((v (pop *screen-vector-resource*)))
    (cond ((null v)
	   (%make-screen-vector x y))
	  (t
	   (setf (%screen-vector-x v) x)
	   (setf (%screen-vector-y v) y)
	   v))))
 
(defun borrow-screen-vector (&key (x 0) (y 0))
  "Get a vector out of the resource, making a new one if necessary."
  (declare (type Screen-Coordinate x y)
		    (:returns (type Screen-Vector)))
  (%borrow-screen-vector x y))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %return-screen-vector)))

(defun %return-screen-vector (v)
  (declare (special *screen-vector-resource*)
	   (type Screen-Vector v))
  (push v *screen-vector-resource*)
  t)

(defun return-screen-vector (v)
  
  "Return <v> to the resource. This must be done with care, since any
further reference to <v> will be an error."

  (declare (type Screen-Vector v)
		    (:returns t))
  (%return-screen-vector v))

;;;-------------------------------------------------------
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro %with-borrowed-screen-vector ((vname &key (x 0) (y 0)) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (%borrow-screen-vector ,x ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (%return-screen-vector ,return-name)))))

(defmacro with-borrowed-screen-vector ((vname &key (x 0) (y 0)) &body body)

  
  "This macro binds <vname> to a vector borrowed from the resource,
optionally initialized to the supplied <x> and <y> coordinates, and
returns the point to the resource on exit.  The user should be careful
not to create references to the vector that survive the dynamic extent
of the macro body."

  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name (borrow-screen-vector :x ,x :y ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-screen-vector ,return-name))))) 
 
