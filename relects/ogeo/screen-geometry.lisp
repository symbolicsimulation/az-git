;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;=======================================================

(deftype Screen-Object ()
  "A Screen-Object is one of Screen-Vector, Screen-Point, or Screen-Rect."
  '(or Screen-Vector Screen-Point Screen-Rect))

;;;=======================================================

(defun %keep-screen-rect-in-xy (x0 y0 w0 h0 x1 y1 w1 h1)
  
  "Modify <smaller>'s position so it is within <larger>.
If <smaller> won't fit, keep upper left corner within <larger>."

  (declare (type Screen-Coordinate x0 y0 w0 h0 x0 y1 w1 h1))
  (values (max (min x0 (- (+ x1 w1) w0)) x1)
	  (max (min y0 (- (+ y1 h1) h0)) y1)))

(defun keep-screen-rect-in (smaller larger)
  
  "Modify <smaller>'s position so it is within <larger>.
If <smaller> won't fit, keep upper left corner within <larger>."

  (declare (type Screen-Rect smaller larger))
  (locally (declare (type Screen-Rect smaller larger))
    (let ((x (screen-rect-xmin smaller))
	  (y (screen-rect-ymin smaller))
	  (w (screen-rect-width smaller))
	  (h (screen-rect-height smaller))
	  (xmin (screen-rect-xmin larger))
	  (ymin (screen-rect-ymin larger))
	  (xmax (screen-rect-xmax larger))
	  (ymax (screen-rect-ymax larger)))
      (declare (type Screen-Coordinate x y w h xmin ymin xmax ymax))
      (setf (screen-rect-xmin smaller) (max (min x (- xmax w)) xmin))
      (setf (screen-rect-ymin smaller) (max (min y (- ymax h)) ymin))
      smaller)))

;;;=======================================================

(defun %sub-screen-rect? (smaller larger)
  (declare (type Screen-Rect smaller larger))
  (and (<= (the Screen-Coordinate (%screen-rect-xmin larger))
	   (the Screen-Coordinate (%screen-rect-xmin smaller))
	   (the Screen-Coordinate (%screen-rect-xmax smaller))
	   (the Screen-Coordinate (%screen-rect-xmax larger)))
       (<= (the Screen-Coordinate (%screen-rect-ymin larger))
	   (the Screen-Coordinate (%screen-rect-ymin smaller))
	   (the Screen-Coordinate (%screen-rect-ymax smaller))
	   (the Screen-Coordinate (%screen-rect-ymax larger)))))

(defun sub-screen-rect? (smaller larger)
  "Test if <smaller> is inside <larger>."
  (declare (type Screen-Rect smaller larger)
	   (:returns (type (Member t nil))))
  (%sub-screen-rect? smaller larger))

(eval-when (compile load eval)
  (proclaim '(Inline %screen-point-in-rect-xy?)))

(defun %screen-point-in-rect-xy? (x y r)
  (declare (optimize (safety 0) (speed 3))
	   (type Screen-Coordinate x y)
	   (type Screen-Rect r))
  (let ((xmin (%screen-rect-xmin r))
	(ymin (%screen-rect-ymin r)))
    (declare (type Screen-Coordinate xmin ymin))
    (and
     (<= xmin x)
     (< x (+ xmin (the Positive-Screen-Coordinate (%screen-rect-width r))))
     (<= ymin y)
     (< y (+ ymin (the Positive-Screen-Coordinate (%screen-rect-height r)))))))

(defun screen-point-in-rect-xy? (x y r)
  "Is <point> in <rect>?"
  (declare (type Screen-Coordinate x y)
	   (type Screen-Rect r))
  (%screen-point-in-rect-xy? x y r))

(defun %screen-point-in-rect? (p r)
  (declare (optimize (safety 0) (speed 3))
	   (inline %screen-point-in-rect-xy?)
	   (type Screen-Point p)
	   (type Screen-Rect r))
  (%screen-point-in-rect-xy?
    (%screen-point-x p)
    (%screen-point-y p)
    r))

(defun screen-point-in-rect? (p r)
  "Is <p> in <r>?"
  (declare (type Screen-Point p)
	   (type Screen-Rect r)
	   (:returns (type (Member t nil))))
  (%screen-point-in-rect? p r))

(defun screen-points-in-rect? (ps r)
  "Are all the points in the list <ps> in the rect <r>?"
  (declare (type Screen-Point-List ps)
	   (type Screen-Rect r)
	   (:returns (type (Member t nil))))
  (every #'(lambda (p) (%screen-point-in-rect? p r)) ps))

;;;-----------------------------------------------------------------

(declaim (Inline %screen-rect-intersect?))

(defun %screen-rect-intersect? (r0 r1)
  (declare (optimize (safety 0) (speed 3))
	   (type Screen-Rect r0 r1))
  (not (or
	(let ((dx (- (the Screen-Coordinate (%screen-rect-xmin r1))
		     (the Screen-Coordinate (%screen-rect-xmin r0)))))
	  (declare (type Screen-Coordinate dx))
	  (or (>= dx (the Positive-Screen-Coordinate
		       (%screen-rect-width r0)))
	      (<= dx (- (the Positive-Screen-Coordinate
			  (%screen-rect-width r1))))))

	(let ((dy (- (the Screen-Coordinate (%screen-rect-ymin r1))
		     (the Screen-Coordinate (%screen-rect-ymin r0)))))
	  (declare (type Screen-Coordinate dy))
	  (or (>= dy (the Positive-Screen-Coordinate
		       (%screen-rect-height r0)))
	      (<= dy (- (the Positive-Screen-Coordinate
			  (%screen-rect-height r1)))))))))

(defun screen-rect-intersect? (r0 r1)
  "Test whether two rects intersect."
  (declare (type Screen-Rect r0 r1)
		    (:returns (type (Member t nil))))
  (%screen-rect-intersect? r0 r1))

;;;------------------------------------------------------------
;;; note that it's ok for <result> to be <r0> or r1>

(defun %intersect-2-screen-rects (r0 r1 result)
  (let* ((x0 (%screen-rect-xmin r0))
	 (y0 (%screen-rect-ymin r0))
	 (x1 (%screen-rect-xmin r1))
	 (y1 (%screen-rect-ymin r1))
	 (x (max x0 x1))
	 (y (max y0 y1))
	 (w (- (min (+ (%screen-rect-width r0) x0)
		    (+ (%screen-rect-width r1) x1))
	       x))
	 (h (- (min (+ (%screen-rect-height r0) y0)
		    (+ (%screen-rect-height r1) y1))
	       y)))
    (declare (type Screen-Coordinate x0 y0 x1 y1 x y w h))
    (if (or (minusp w) (minusp h))
	;; then no intersection
	nil
	;; else return the intersection in <result>
	(%set-screen-rect-coords result x y w h))))

(defun intersect-2-screen-rects (r0 r1
				 &key
				 (result (%make-screen-rect 0 0 0 0)))
  "Returns the rect that is the intersection.
Note that it's ok for <result> to be <r0> or r1>."
  (declare (type Screen-Rect r0 r1 result)
	   (:returns result))
  (%intersect-2-screen-rects r0 r1 result))

;;;------------------------------------------------------------

(defun intersect-screen-rects (rs
			       &key
			       (result (%make-screen-rect 0 0 0 0)))
    "Returns the rect that is the intersection.
Note that it's ok for <result> to be a member of <rs>."
  (declare (type Screen-Rect-List rs)
		    (type Screen-Rect result)
		    (:returns result))
  (let* ((r0 (car rs))
         (xmin (%screen-rect-xmin r0))
         (ymin (%screen-rect-ymin r0))
         (xmax (%screen-rect-xmax r0))
         (ymax (%screen-rect-ymax r0)))
    (declare (type Screen-Rect r0)
	     (type Screen-Coordinate xmin ymin xmax ymax))
    (dolist (r (cdr rs))
      (declare (type Screen-Rect r))
      (let* ((r-xmin (%screen-rect-xmin r))
             (r-ymin (%screen-rect-ymin r))
             (r-xmax (%screen-rect-xmax r))
             (r-ymax (%screen-rect-ymax r)))
	(declare (type Screen-Coordinate r-xmin r-ymin r-xmax r-ymax))
	(az:maxf xmin xmin r-xmin)
        (az:minf xmax r-xmax)
	(when (< xmax xmin) ;; < allows empty rects with position
	  ;; intersection is empty and has no location!!
	  (return-from intersect-screen-rects nil))
        (az:maxf ymin r-ymin)
	(az:minf ymax r-ymax)
	(when (< ymax ymin) ;; < allows empty rects with position
	  ;; intersection is empty and has no location!
	  (return-from intersect-screen-rects nil))))
    ;; Intersection has at least a location, even if it covers no
    ;; pixels.
    (%set-screen-rect-coords result xmin ymin (- xmax xmin) (- ymax ymin))
    result))

;;;------------------------------------------------------------

(defun screen-rect-covering-rects (rs
				   &key
				   (result (%make-screen-rect 0 0 0 0)))
  "Returns the smallest rect containing all rects in a list."
  (declare (type Screen-Rect-List rs)
		    (type Screen-Rect result)
		    (:returns result))

  (let* ((r0 (car rs))
         (xmin (%screen-rect-xmin r0))
         (ymin (%screen-rect-ymin r0))
         (xmax (%screen-rect-xmax r0))
         (ymax (%screen-rect-ymax r0)))
    (declare (type Screen-Rect r0)
	     (type Screen-Coordinate xmin ymin xmax ymax))
    (dolist (r (cdr rs))
      (declare (type Screen-Rect r))
      (let* ((r-xmin (%screen-rect-xmin r))
             (r-ymin (%screen-rect-ymin r))
             (r-xmax (%screen-rect-xmax r))
             (r-ymax (%screen-rect-ymax r)))
	(declare (type Screen-Coordinate r-xmin r-ymin r-xmax r-ymax))
	(setf xmin (min xmin r-xmin))
        (setf ymin (min ymin r-ymin))
        (setf xmax (max xmax r-xmax))
        (setf ymax (max ymax r-ymax))))
    (%set-screen-rect-coords result xmin ymin (- xmax xmin) (- ymax ymin))
    result))

;;;------------------------------------------------------------
;;; given a list of screen-points, return the smallest enclosing screen-rect

(defun screen-rect-covering-points (ps
				    &key
				    (result (%make-screen-rect 0 0 0 0)))
  "Returns the smallest rect containing all points in a list."
  (declare (type Screen-Point-List ps)
		    (type Screen-Rect result)
		    (:returns result))

  (let* ((p0 (car ps))
	 (min-x (%screen-point-x p0))
	 (max-x min-x)
	 (min-y (%screen-point-y p0))
	 (max-y min-y))
    (declare (type Screen-Point p0)
	     (type Screen-Coordinate min-x max-x min-y max-y))
    (dolist (p (cdr ps))
      (declare (type Screen-Point p))
      (let ((x (%screen-point-x p))
	    (y (%screen-point-y p)))
	(declare (type Screen-Coordinate x y))
	(if (< x min-x)
	    (setq min-x x)
	  (when (> x max-x) (setq max-x x)))
	(if (< y min-y)
	    (setq min-y y)
	  (when (> y max-y) (setq max-y y)))))
    (%set-screen-rect-coords
     result min-x min-y (- max-x min-x) (- max-y min-y))
    result))

;;;------------------------------------------------------------
;;; destructively modifies Screen-Rect to include screen-point

(defun extend-screen-rect-to-point-xy! (r x y)
  (declare (type Screen-Rect r)
		    (type Screen-Coordinate x y))
  (locally (declare (type Screen-Rect r)
		    (type Screen-Coordinate x y))
    (let* ((xmin (%screen-rect-xmin r))
	   (ymin (%screen-rect-ymin r))
	   (xmax (%screen-rect-xmax r))
	   (ymax (%screen-rect-ymax r)))
      (declare (type Screen-Coordinate xmin ymin xmax ymax))
      ;; this is using the fact that screen-rects are represented
      ;; by xmin, ymin, width, height
      (cond ((< x xmin)
	     (setf (%screen-rect-xmin r) x)
	     (setf (%screen-rect-width r) (- xmax x)))
	    ((> x xmax)
	     (setf (%screen-rect-width r) (- x xmin))))
      (cond ((< y ymin)
	     (setf (%screen-rect-ymin r) y)
	     (setf (%screen-rect-height r) (- ymax y)))
	    ((> y ymax)
	     (setf (%screen-rect-height r) (- y ymin))))
      r)))

(defun extend-screen-rect-to-point (r p &key (result (make-screen-rect)))

  "Computes the smallest rect that includes both <r> and <p>.
Note that it's ok for <r> to be eq to <result>."

  (declare (type Screen-Rect r result)
		    (type Screen-Point p)
		    (:returns result))
  (when (not (eq r result)) (copy-screen-rect r :result result))
  (extend-screen-rect-to-point-xy!
   result (screen-point-x p) (screen-point-y p))
  result)

;;;------------------------------------------------------------

(defun extend-screen-rect-to-rect (r0 r1 &key (result (make-screen-rect)))
  "Computes the smallest rect that includes both <r0> and <r1>.
Note that it's ok for <r0> and/or <r1> to be eq to <result>."
  (declare (type Screen-Rect r0 r1 result)
		    (:returns result))
  (when (not (eq r0 result)) (copy-screen-rect r0 :result result))
  (extend-screen-rect-to-point-xy!
   result (%screen-rect-xmin r1) (%screen-rect-ymin r1))
  (extend-screen-rect-to-point-xy!
   result
   (+ (%screen-rect-xmin r1) (%screen-rect-width r1))
   (+ (%screen-rect-ymin r1) (%screen-rect-height r1)))
  result)

;;;-----------------------------------------------------------------

(defun screen-rect-min-l2-dist2 (r0 r1)
  "The L2 distance (squared) between the closest two points of the two rects."
  (declare (type Screen-Rect r0 r1)
		    (:returns (type Positive-Screen-Coordinate)))
  (let ((xmin0 (%screen-rect-xmin r0))
	(xmax0 (%screen-rect-xmax r0))
	(ymin0 (%screen-rect-ymin r0))
	(ymax0 (%screen-rect-ymax r0))
	(xmin1 (%screen-rect-xmin r1))
	(xmax1 (%screen-rect-xmax r1))
	(ymin1 (%screen-rect-ymin r1))
	(ymax1 (%screen-rect-ymax r1))
	(dist 0))
    (declare (type Screen-Coordinate
		   xmin0 xmax0 ymin0 ymax0 xmin1 xmax1 ymin1 ymax1)
	     (type Positive-Screen-Coordinate dist))
    ;; perform reflections as needed so that r0 is below and xmin of r1
    (when (> xmin0 xmin1)
      (psetf xmin0 (- xmax0)
	     xmax0 (- xmin0))
      (psetf xmin1 (- xmax1)
	     xmax1 (- xmin1)))
    (when (> ymin0 ymin1)
      (psetf ymin0 (- ymax0)
	     ymax0 (- ymin0))
      (psetf ymin1 (- ymax1)
	     ymax1 (- ymin1)))
    ;; calculate distance
    (when (< xmax0 xmin1)
      (let ((dx (- xmin1 xmax0)))
	(declare (type Screen-Coordinate dx))
	(incf dist (* dx dx))))
    (when (< ymax0 ymin1)
      (let ((dy (- ymin1 ymax0)))
	(declare (type Screen-Coordinate dy))
	(incf dist (* dy dy))))
    dist))

(defun screen-rect-min-l2-dist (r0 r1)
  "The L2 distance between the closest two points of the two rects."
  (declare (type Screen-Rect r0 r1)
		    (:returns (type Positive-Chart-Coordinate)))
  (sqrt (screen-rect-min-l2-dist2 r0 r1))) 
 
;;;============================================================
;;; Useful Utilities
;;;============================================================

(defun screen-distance-to-line (p lp0 lp1)

  "Given a point, <p>, and the two endpoints of a line segment, return
the distance from the point to the line segement.  If the two
endpoints are close together, return the distance to the first one.
The code tries to avoid expensive operations."

  (declare (type Screen-Point p lp0 lp1)
		    (:returns (type Positive-Chart-Coordinate))) 

 (let* ((x (screen-point-x p))
         (y (screen-point-y p))
         (x1 (screen-point-x lp0))
         (y1 (screen-point-y lp0))
         (x2 (screen-point-x lp1))
         (y2 (screen-point-y lp1))
         (px1 (- x x1))
         (py1 (- y y1))
         (px2 (- x x2))
         (py2 (- y y2))
         (dx (- x2 x1))
         (dy (- y2 y1))
         (line-length-squared (+ (* dx dx) (* dy dy)))
         (dist-to-lp0-squared (+ (* px1 px1) (* py1 py1))))
    (sqrt (if (< line-length-squared 0.0d01)
	      dist-to-lp0-squared
	      (let* ((line-length (sqrt line-length-squared))
		     (dx-normalized (/ dx line-length))
		     (dy-normalized (/ dy line-length))
		     (proj-length (+ (* dx-normalized px1)
				     (* dy-normalized py1)))
		     (proj-length-squared (* proj-length proj-length))
		     (dist-to-line-squared
		       (max 0 (- dist-to-lp0-squared
				 proj-length-squared)))
		     (dist-to-lp1-squared (+ (* px2 px2) (* py2 py2))))
		(cond ((< proj-length 0) dist-to-lp0-squared)
		      ((> proj-length line-length) dist-to-lp1-squared)
		      (t dist-to-line-squared)))))))

;;;============================================================
;;; Clipping
;;;============================================================

;;; Clip line against clipping-rect.  Implements
;;; Cohen-Sutherland clipping.  From Foley and Van Dam, pg.146 

(declaim (inline clip-code))

(defun clip-code (x y xmin xmax ymax ymin)       
  ;; Returns Cohen-Sutherland clip code for point (x,y).
  ;; This version assumes that xmin < xmax, etc.
  (declare (optimize (safety 0) (speed 3))
	   (type Screen-Coordinate x y xmin xmax ymax ymin))
  (logior (cond ((> y ymax) 8) ((> ymin y) 4) (t 0))
	  (cond ((> x xmax) 2) ((> xmin x) 1) (t 0))))

;(defun clip-code (x y xmin xmax ymax ymin)       
;  ;; returns Cohen-Sutherland clip code for point (x,y). 
;  (let ((abovebit (if (> y ymax) 8 0))
;	(belowbit (if (> ymin y) 4 0))
;	(xmaxbit (if (> x xmax) 2 0))
;	(xminbit (if (> xmin x) 1 0)))
;    (logior abovebit belowbit xmaxbit xminbit)))

;;;------------------------------------------------------------

(defun clip-screen-line (x0 y0 x1 y1 clipping-rect)

  "Clip line against <clipping-rect>.  Implements Cohen-Sutherland
clipping.  From Foley and Van Dam, pg.146 returns four values: new
<x0>, <y0>, <x1>, <y1> of line clipped to <clipping-rect> if it
returns nil, the line is outside <clipping-rect> note: the direction
of the line is maintained (from <x0>,<y0> to <x1>,<y1>)"

  (declare (optimize (safety 0) (speed 3))
	   (type Screen-Coordinate x0 y0 x1 y1)
	   (type (or Null Screen-Rect) clipping-rect)
	   (:returns (values (type (or Null Screen-Coordinate) x0)
			     (type (or Null Screen-Coordinate) y0)
			     (type (or Null Screen-Coordinate) x1)
			     (type (or Null Screen-Coordinate) y1))))
  (if (null clipping-rect)
      (values x0 y0 x1 y1)
    (let* ((rect clipping-rect) ;; to make sure decl takes hold
	   (clip-xmin (%screen-rect-xmin rect))
	   (clip-ymin (%screen-rect-ymin rect))
	   (clip-xmax  (+ -1 clip-xmin (%screen-rect-width rect)))
	   (clip-ymax (+ -1 clip-ymin (%screen-rect-height rect)))
	   (accept-p nil)
	   (done-p nil)
	   (reversed-p nil)
	   (code-1 -1)
	   (code-2 -1))
      (declare (type Screen-Rect rect)
	       (type Screen-Coordinate clip-xmin clip-ymin clip-xmax clip-ymax)
	       (type (Member t nil accept-p done-p reversed-p))
	       (type Fixnum code-1 code-2))
      (loop
	(if done-p (return nil))
	(setf code-1 (clip-code x0 y0 clip-xmin clip-xmax clip-ymax clip-ymin))
	(setf code-2 (clip-code x1 y1 clip-xmin clip-xmax clip-ymax clip-ymin))
	(if (= 0 (logand code-1 code-2))
	    ;; Possible accept
	    (if (= 0 (logior code-1 code-2))
		;; then accept
		(progn (setf accept-p t)
		       (setf done-p t))
	      ;; else find intersections
	      (progn 
		(when (= 0 code-1)
		  ;; Swap points so (X0 Y0) is guaranteed to be outside 
		  (rotatef x0 x1)
		  (rotatef y0 y1)
		  (rotatef code-1 code-2)
		  (setf reversed-p t))
		(cond
		 ((/= 0 (logand code-1 8)) ;; divide line at ymax
		  (incf x0 (truncate
			    (the Fixnum (* (- x1 x0) (- clip-ymax y0)))
			    (- y1 y0)))
		  (setf y0 clip-ymax))
		 ((not (= 0 (logand code-1 4)))
		  ;; divide line at ymin
		  (incf x0 (truncate
			    (the Fixnum (* (- x1 x0) (- clip-ymin y0)))
			    (- y1 y0)))
		  (setf y0 clip-ymin))
		 ((/= 0 (logand code-1 2)) ;; divide line at xmax
		  (incf y0 (truncate
			    (the Fixnum (* (- y1 y0) (- clip-xmax x0)))
			    (- x1 x0)))
		  (setf x0 clip-xmax))
		 (t ;; divide line at xmin
		  (incf y0 (truncate
			    (the Fixnum (* (- y1 y0) (- clip-xmin x0)))
			    (- x1 x0)))
		  (setf x0 clip-xmin)))))
	  ;; Reject
	  (setf done-p t)))
      (when reversed-p ;; reverse it back
	(rotatef x0 x1)
	(rotatef y0 y1))
      (if accept-p (values x0 y0 x1 y1)	(values nil)))))
  
;;;------------------------------------------------------------

(defun clip-screen-rect-specs (xmin ymin width height clipping-screen-rect)
  ;; clip Screen-Rect specs to clipping screen-rect
  ;; (null clipping-screen-rect means no clipping)
  (when clipping-screen-rect
    (let ((clip-xmin (%screen-rect-xmin clipping-screen-rect))
	  (clip-ymin (%screen-rect-ymin clipping-screen-rect))
	  (clip-width (%screen-rect-width clipping-screen-rect))
	  (clip-height (%screen-rect-height clipping-screen-rect)))
      (when (> clip-xmin xmin)
	(setf width (- width (- clip-xmin xmin)))
	(setf xmin clip-xmin))
      (when (> clip-ymin ymin)
	(setf height (- height (- clip-ymin ymin)))
	(setf ymin clip-ymin))
      (when (> (+ xmin width) (+ clip-xmin clip-width))
	(setf width (- (+ clip-xmin clip-width) xmin)))
      (when (> (+ ymin height) (+ clip-ymin clip-height))
	(setf height (- (+ clip-ymin clip-height) ymin)))))
  (values xmin ymin width height))

;;;============================================================

(defun make-random-screen-vector (enclosing-rect)
  "Creates a (uniform) random vector in <enclosing-rect>."
  (declare (type Screen-Rect enclosing-rect)
		    (:returns (type Screen-Vector)))
  (%make-screen-vector (random (%screen-rect-width enclosing-rect))
		       (random (%screen-rect-height enclosing-rect))))

(defun make-random-screen-point (enclosing-rect)
  "Creates a (uniform) random point in <enclosing-rect>."
  (declare (type Screen-Rect enclosing-rect)
		    (:returns (type Screen-Point)))
  (%make-screen-point
   (+ (%screen-rect-xmin enclosing-rect)
      (random (%screen-rect-width enclosing-rect)))
   (+ (%screen-rect-ymin enclosing-rect)
      (random (%screen-rect-height enclosing-rect)))))

(defun make-random-screen-points (enclosing-rect n)
  "Creates a list of <n> (uniform) random points in <enclosing-rect>."
  (declare (type Screen-Rect enclosing-rect)
		    (:returns (type Screen-Point-List))) 
  (let ((ps ()))
    (dotimes (i n) (push (make-random-screen-point enclosing-rect) ps))
    ps))

(defun make-random-screen-rect (enclosing-rect)
  "Creates a (uniform) random rect in <enclosing-rect>."
  (declare (type Screen-Rect enclosing-rect)
		    (:returns (type Screen-Rect)))
  (let ((ps (make-random-screen-points enclosing-rect 2)))
    (screen-rect-covering-points ps)))

(defun make-random-screen-rects (enclosing-rect n)
  "Creates a list of <n> (uniform) random rects in <enclosing-rect>."
  (declare (type Screen-Rect enclosing-rect)
		    (:returns (type Screen-Rect-List))) 
  (let ((rects ()))
    (dotimes (i n) (push (make-random-screen-rect enclosing-rect) rects))
    rects))