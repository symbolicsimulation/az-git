%%% -*- Mode:Lisp; Syntax: Common-Lisp; -*-

\section{Screen Geometry}
\label{screen-geometry}

The Screen Geometry submodule is intended to support geometric calculation
using high-level mathematical abstractions,
similar to those used in 
\cite{DeRo88,DeRo89a,DeRo89b,McDo91t,McDo89b,Sega89},
but restricted to and optimized for the common special case 
of a discrete two-dimensional coordinate system,
ie. a bitmapped display.
In addition to performance, requiring the coordinates to be integers
makes it easier to deal reliably with the off-by-one errors that are the curse
of many bitmap graphics systems.

Screen Geometry provides an affine space of screen points and vectors
and an algebra of operations on screen points and vectors.
This affine space is the natural range of the last mapping 
in a viewing pipeline.
It also provides objects representing simple rectangular regions
and functions for standard calculations with these regions (eg. intersection).
It may be extended in the future to include objects representing
other geometric shapes.

Screen Geometry was developed to support the Slate package \cite{McDo91k}.

\subsection{Screen Objects}

A {\sf g:Screen-Object} is one of 
{\sf g:Screen-Vector}, {\sf g:Screen-Point}, or {\sf g:Screen-Rect},
which are described below.

\subsection{Screen Coordinates}

{\sf g:Screen-Coordinate} is a datatype
for the possible values of a point
on a {\sf slate:Screen} or {\sf slate:Slate} 
(see \cite{McDo91k}).
It will be a subtype of {\sf Integer} and usually a subtype of {\sf Fixnum}.

{\sf g:Positive-Screen-Coordinate} 
is a data type for the possible values of the width, height, etc.,
of a screen object.
{\sf g:Positive-Screen-Coordinate} really means non-negative;
it will be a subtype of {\sf (Integer 0 *)}.

{\sf g:Screen-Coordinate-List} and 
{\sf g:Positive-Screen-Coordinate-List} are types
supplied to make it convenient
to check if all the coordinates in a list are legal.

\subsection{Screen Vectors}

The abstract type {\sf g:Screen-Vector} represents displacements on a screen.
{\sf g:Screen-Vector} has constructor {\sf g:make-screen-vector}, 
equality predicate {\sf g:equal-screen-vectors?},
and copier {\sf g:copy-screen-vector}.
The coordinates of a screen vector can be read (and set with {\sf setf})
using {\sf g:screen-vector-x} and {\sf g:screen-vector-y}

The coordinates of a {\sf g:Positive-Screen-Vector} 
are both {\sf g:Positive-Screen-Coordinate}s. 

{\sf g:Screen-Vector-List} and {\sf g:Positive-Screen-Vector-List}
are types provided for convenience.

Screen vectors are often used to hold intermediate results;
to avoid the overhead of cons-ing and gc-ing many small objects,
we provide a resource of temporary vectors.
Vectors can be explicitly borrowed from and returned to the resource
using {\sf g:borrow-screen-vector} and {\sf g:return-screen-vector}.
However, it is recommended (and safer) to locally bind a variable
to a temporary vector using the macro {\sf g:with-borrowed-screen-vector}.

The Geometry package provides the following algebraic operations
on screen vectors: 
{\sf g:linear-mix-screen-vectors},
{\sf g:add-screen-vectors},
{\sf g:subtract-screen-vectors},
{\sf g:screen-vector-l2-norm2},
{\sf g:screen-vector-l2-norm},
 and {\sf g:screen-vector-l1-norm}.

\subsection{Screen Points}

The abstract type {\sf g:Screen-Point} represents locations on a screen.
{\sf g:Screen-Point} has constructor {\sf g:make-screen-point}, 
equality predicate {\sf g:equal-screen-points?},
and copier {\sf g:copy-screen-point}.
The coordinates of a screen point can be read (and set with {\sf setf})
using {\sf g:screen-point-x} and {\sf g:screen-point-y}

The coordinates of a {\sf g:Positive-Screen-Point} 
are both {\sf g:Positive-Screen-Coordinate}s. 

{\sf g:Screen-Point-List} and {\sf g:Positive-Screen-Point-List}
are types provided for convenience.

Screen points are often used to hold intermediate results;
to avoid the overhead of cons-ing and gc-ing many small objects,
we provide a resource of temporary points.
Points can be explicitly borrowed from and returned to the resource
using {\sf g:borrow-screen-point} and {\sf g:return-screen-point}.
However, it is recommended (and safer) to locally bind a variable
to a temporary point using the macro {\sf g:with-borrowed-screen-point}.

The Geometry package provides the following algebraic operations
on screen points: 
{\sf g:move-screen-point} and {\sf g:subtract-screen-points}.

\subsection{Screen Rects}

A {\sf Screen-Rect} is a screen rectangle whose sides are parallel to
the coordinate axes.
Other names are commonly used for this, including {\it rectangle}
 and {\it region}.
We use {\sf Screen-Rect} 
because we want to reserve {\it rectangle} for general rectangles 
and {\it region} for more general specifications of sets of pixels.
We welcome any
suggestions for names that are better than {\sf Screen-Rect}.
(One we have considered is 2D-Interval, but it seems too verbose.)

We use a coordinate system where x increases from left to right,
and, unfortunately, like most window systems, y from top to bottom.

{\sf Screen-Rect}s have the following generalized accessors: 
\\
{\sf screen-rect-xmin}, {\sf screen-rect-xmax}, {\sf screen-rect-ymin},
{\sf screen-rect-ymax}, 
\\
{\sf screen-rect-left}, {\sf screen-rect-top},
{\sf screen-rect-right}, {\sf screen-rect-bottom}, 
\\
{\sf screen-rect-width},
and {\sf screen-rect-height}.

At present, all rects are represented internally by {\sf xmin}, {\sf width},
{\sf ymin}, and {\sf height} slots. 
For {\sf Screen-Rect}s, {\sf xmin} is equivalent
to {\sf left}, and {\sf ymin} to {\sf top}.
The internal representation may be changed at any time.

The external interface for making {\sf Screen-Rect}'s
{\sf make-screen-rect} takes any two of {\sf :left}, {\sf :right}, 
and {\sf :height} and
any two of {\sf :top}, {\sf :bottom}, and {\sf :width}.  

Because of the pernicious danger of fence post errors, we give a
careful description of which pixels the various coordinates refer
to, and also give names to common alternative specifications:

The ({\sf screen-rect-left},{\sf screen-rect-right}) and
\\
({\sf screen-rect-top}, {\sf screen-rect-bottom}) pairs are specifications
of integer intervals like the (start, end) parameters to the CL
sequence functions.  
{\sf Screen-rect-left} is the coordinate of the
leftmost column that intersects the Screen-Rect and
{\sf screen-rect-top} the coordinate of the topmost row that intersects
the screen-rect.  
{\sf Screen-rect-right} is the coordinate of the first
column not intersecting the Screen-Rect to the right and
{\sf screen-rect-bottom} is the coordinate of the first row below the
Screen-Rect not intersecting it.  
(See figure~ref{screen-rect-coordinates}.)
This means
that {\sf screen-rect-width} = {\sf screen-rect-right} - {\sf screen-rect-left}
is the number of columns that intersect the screen-rect,
{\sf screen-rect-height} = {\sf screen-rect-bottom} - {\sf screen-rect-top} is
the number of rows that intersect the screen-rect, and
\\ 
{\sf screen-rect-width} * {\sf screen-rect-height} 
is the number of pixels in
the screen-rect.

\begin{figure}
\begin{center}
\begin{minipage}[t]{3.0in}
\begin{verbatim}
  screen-rect-left
  |    screen-rect-right
  |    |
  v    v
 ....... 
 .xxxxx. <--screen-rect-top
 .xxxxx.
 .xxxxx. 
 ....... <--screen-rect-bottom
\end{verbatim}
\end{minipage}
\end{center}
\caption{Screen Rect coordinates}
\label{screen-rect-coordinates}
\end{figure}

Implementation of drawing operations will require interfacing with
other parameterizations; to make this a little easier, we give
standard names to commonly occurring coordinates,
shown in figure~\ref{other-screen-rect-coordinates}.

\begin{figure}
\begin{center}
\begin{minipage}{3.0in}
\begin{verbatim}
  outer-left
  |inner-left
  ||   inner-right
  ||   |outer-right
  ||   ||
  vv   vv
  ....... <-outer-top
  .xxxxx. <-inner-top
  .xxxxx.
  .xxxxx. <-inner-bottom
  ....... <-outer-bottom
\end{verbatim}
\end{minipage}
\end{center}
\caption{Other Screen Rect coordinates}
\label{other-screen-rect-coordinates}
\end{figure}

For example, implementations of the drawing operations will often
need to transform to the analogous parameterization in top-to-bottom
coordinates. 
This means converting our (inner-left, inner-bottom,
width, height) representation to the coordinates of the (inner-left,
inner-top, width, height) in the top-to-bottom coordinates.

There's a natural ambiguity about what's supposed to happen
when we change the coordinates of a Rect.
When we change the right coordinate, 
does that mean translating the Rect rigidly so its width remains fixed,
or does it mean leaving the left coordinate fixed and 
changing the width.

We have adopted the convention that
setting any of the bounds (left, right, top, bottom, outer-left, etc.)
of the Rect is interpreted as changing
the origin, that is, a translation of the Rect without changing
its extent (width and height).
Setting the width and height changes the size or shape of the Rect,
holding the origin fixed.
This convention applies to both Screen and Chart Rects.

Width and height must be non-negative.  
Zero width and zero height are taken to imply an empty screen-rect. 
A width and height of 1 means the {\sf Screen-Rect} covers one pixel. 
Empty screen-rects still have a location.

\subsubsection{The Abstract Type}

The constructor for {\sf g:Screen-Rect} is {\sf g:make-screen-rect};
the equality predicate is {\sf g:equal-screen-rects?};
and the copier is {\sf g:copy-screen-rect}.

In addition to the coordinate accessor functions discussed above,
screen rects have accessors that take and return vectors and points.

{\sf g:screen-rect-origin} gets (and sets with {\sf setf})
the upper left corner of the rect.
{\sf g:screen-rect-extent} gets (and sets with {\sf setf})
the vector corresponding to the diagonal
(width, height) of the rect.

Screen rects are often used to hold intermediate results;
to avoid the overhead of cons-ing and gc-ing many small objects,
we provide a resource of temporary rects.
Rects can be explicitly borrowed from and returned to the resource
using {\sf g:borrow-screen-rect} and {\sf g:return-screen-rect}.
However, it is recommended (and safer) to locally bind a variable
to a temporary rect using the macro {\sf g:with-borrowed-screen-rect}.

\subsection{Geometric Calculations}

Geometry provides functions for a number of common geometric calculations.

{\sf g:sub-screen-rect?} tests whether one rect is inside another.

{\sf g:screen-point-in-rect?} and
{\sf g:screen-points-in-rect?}
test whether points are inside a rect.

{\sf g:screen-rect-intersect?} tests whether two rects intersect;
{\sf g:intersect-screen-rects} returns the rect that is the intersection.

{\sf g:screen-rect-covering-rects} 
returns the smallest rect containing all rects in a list.
{\sf g:screen-rect-covering-points}
returns the smallest rect containing all points in a list.
{\sf g:extend-screen-rect-to-point} 
returns the smallest rect covering a rect and a point.
{\sf g:extend-screen-rect-to-rect}
returns the smallest rect covering two rects.

{\sf g:screen-rect-min-l2-dist2}
and
{\sf g:screen-rect-min-l2-dist}
compute the minimum $L^2$ distance between two rects.

{\sf g:screen-distance-to-line} computes the $L^2$ distance
between a point and a line segment.


