%%% -*- Mode:Lisp; Syntax: Common-Lisp; -*-

\section{Chart Geometry}
\label{chart-geometry}

The Chart Geometry submodule is intended to support geometric calculation
using high-level mathematical abstractions,
similar to those used in 
\cite{DeRo88,DeRo89a,DeRo89b,McDo91t,McDo89b,Sega89},
but restricted to and optimized for the common special case 
of a two-dimensional coordinate system.
Specializing for the two-dimensional case gives us some small performance
benefits,
but the main reason for implementing Chart Geometry was 
as a quick and dirty world space 
for the scientifc diagrams provided by the Chart module \cite{McDo91l}.

Like the Screen Geometry module, 
Chart Geometry provides an affine space of chart points and vectors
and an algebra of operations on those points and vectors.
It also provides objects representing simple rectangular regions
and functions for standard calculations with these regions (eg. intersection).
It may be extended in the future to include objects representing
other geometric shapes.

\subsection{Chart Objects}

A {\sf g:Chart-Object} is one of 
{\sf g:Chart-Vector}, {\sf g:Chart-Point}, or {\sf g:Chart-Rect},
which are described below.

\subsection{Chart Coordinates}

{\sf g:Chart-Coordinate} is a datatype
for the possible values of a coordinate
on a {\sf slate:Chart} (see \cite{McDo91l}).
It's safe to assume it is a subtype of {\sf Float}.
{\sf g:Positive-Chart-Coordinate} is a data type for the possible values of the width, height, etc.,
of a chart object.
``Positive'' really means non-negative;
it's safe to assume it is a subtype of {\sf (Float 0.0 *)}.
{\sf g:Chart-Coordinate-List} and 
{\sf g:Positive-Chart-Coordinate-List} are types
supplied to make it convenient
to check if all the coordinates in a list are legal.

\subsection{Chart Vectors}

The abstract type {\sf g:Chart-Vector} represents displacements on a chart.
{\sf g:Chart-Vector} has constructor {\sf g:make-chart-vector}, 
equality predicate {\sf g:equal-chart-vectors?},
and copier {\sf g:copy-chart-vector}.
The coordinates of a chart vector can be read (and set with {\sf setf})
using {\sf g:chart-vector-x} and {\sf g:chart-vector-y}

The coordinates of a {\sf g:Positive-Chart-Vector} 
are both {\sf g:Positive-Chart-Coordinate}s. 

{\sf g:Chart-Vector-List} and {\sf g:Positive-Chart-Vector-List}
are types provided for convenience.

Chart vectors are often used to hold intermediate results;
to avoid the overhead of cons-ing and gc-ing many small objects,
we provide a resource of temporary vectors.
Vectors can be explicitly borrowed from and returned to the resource
using {\sf g:borrow-chart-vector} and {\sf g:return-chart-vector}.
However, it is recommended (and safer) to locally bind a variable
to a temporary vector using the macro {\sf g:with-borrowed-chart-vector}.

The Geometry package supports a chart vector algebra: 
{\sf g:linear-mix-chart-vectors},
(and two special cases:
{\sf g:add-chart-vectors} and
{\sf g:subtract-chart-vectors})
It also provides some simple metric functions:
{\sf g:chart-vector-l2-norm2},
{\sf g:chart-vector-l2-norm},
 and {\sf g:chart-vector-l1-norm}.

\subsection{Chart Points}

The abstract type {\sf g:Chart-Point} represents locations on a chart.
{\sf g:Chart-Point} has constructor {\sf g:make-chart-point}, 
equality predicate {\sf g:equal-chart-points?},
and copier {\sf g:copy-chart-point}.
The coordinates of a chart point can be read (and set with {\sf setf})
using {\sf g:chart-point-x} and {\sf g:chart-point-y}

The coordinates of a {\sf g:Positive-Chart-Point} 
are both {\sf g:Positive-Chart-Coordinate}s. 

{\sf g:Chart-Point-List} and {\sf g:Positive-Chart-Point-List}
are types provided for convenience.

Chart points are often used to hold intermediate results;
to avoid the overhead of cons-ing and gc-ing many small objects,
we provide a resource of temporary points.
Points can be explicitly borrowed from and returned to the resource
using {\sf g:borrow-chart-point} and {\sf g:return-chart-point}.
However, it is recommended (and safer) to locally bind a variable
to a temporary point using the macro {\sf g:with-borrowed-chart-point}.

The Geometry package provides the following algebraic operations
on chart points: 
{\sf g:move-chart-point} and {\sf g:subtract-chart-points}.

\subsection{Chart Rects}

A {\sf Chart-Rect} is a chart rectangle whose sides are parallel to
the coordinate axes.
Other names are commonly used for this,
including {\sf rectangle} and {\sf region}.
We use {\sf Chart-Rect} because we
want to reserve {\sf rectangle} for general rectangles 
and {\sf region} for more general sets in the plane.
We welcome any
suggestions for names that are better than {\sf Chart-Rect}.
(One we have considered is 2D-Interval, but it seems too verbose.)

We use a coordinate system where x increases from left to right,
and, 
in the natural way for quantitative graphics,
but unlike most window systems, y from bottom to top.

{\sf Chart-Rect}s have the following generalized accessors:
\\
{\sf chart-rect-xmin},
{\sf chart-rect-xmax},
{\sf chart-rect-ymin},
{\sf chart-rect-ymax},
\\
{\sf chart-rect-left},
{\sf chart-rect-top},
{\sf chart-rect-right},
{\sf chart-rect-bottom},
\\
{\sf chart-rect-width},
and
{\sf chart-rect-height}.
At present, all rects are represented internally by
{\sf xmin},
{\sf width},
{\sf ymin},
and
{\sf height}
slots. 
For {\sf Chart-Rect}s, {\sf xmin} is equivalent
to {\sf left}, and {\sf ymin} to {\sf bottom}.

The external interface for making {\sf Chart-Rect}'s
{\sf make-chart-rect} takes any two of {\sf :left}, {\sf :right}, 
and {\sf :width} and
any two of {\sf :top}, {\sf :bottom}, and {\sf :height}.  
The internal representation may be changed at any time.

There's a natural ambiguity about what's supposed to happen
when we change the coordinates of a Rect.
When we change the right coordinate, 
does that mean translating the Rect rigidly so its width remains fixed,
or does it mean leaving the left coordinate fixed and 
changing the width?

We have adopted the convention that
setting any of the bounds (left, right, top, bottom, outer-left, etc.)
of the Rect is interpreted as changing
the origin, that is, a translation of the Rect without changing
its extent (width and height).
Setting the width and height changes the size or shape of the Rect,
holding the origin fixed.
This convention applies to both Screen and Chart Rects.

Zero width and zero height are taken to imply an empty chart-rect. 
Empty chart-rects still have a location.

\subsubsection{The Abstract Type}

The constructor for {\sf g:Chart-Rect} is {\sf g:make-chart-rect};
the equality predicate is {\sf g:equal-chart-rects?};
and the copier is {\sf g:copy-chart-rect}.

In addition to the coordinate accessor functions discussed above,
chart rects have accessors that take and return vectors and points.

{\sf g:chart-rect-origin} gets (and sets with {\sf setf})
the upper left corner of the rect.
{\sf g:chart-rect-extent} gets (and sets with {\sf setf})
the vector corresponding to the diagonal
(width, height) of the rect.

Chart rects are often used to hold intermediate results;
to avoid the overhead of cons-ing and gc-ing many small objects,
we provide a resource of temporary rects.
Rects can be explicitly borrowed from and returned to the resource
using {\sf g:borrow-chart-rect} and {\sf g:return-chart-rect}.
However, it is recommended (and safer) to locally bind a variable
to a temporary rect using the macro {\sf g:with-borrowed-chart-rect}.

\subsection{Geometric Calculations}

Geometry provides functions for a number of common geometric calculations.

{\sf g:sub-chart-rect?} tests whether one rect is inside another.

{\sf g:chart-point-in-rect?} and
{\sf g:chart-points-in-rect?}
test whether points are inside a rect.

{\sf g:chart-rect-intersect?} tests whether two rects intersect;
{\sf g:intersect-chart-rects} returns the rect that is the intersection.

{\sf g:chart-rect-covering-rects} 
returns the smallest rect containing all rects in a list.
{\sf g:chart-rect-covering-points}
returns the smallest rect containing all points in a list.
{\sf g:extend-chart-rect-to-point} 
returns the smallest rect covering a rect and a point.
{\sf g:extend-chart-rect-to-rect}
returns the smallest rect covering two rects.

{\sf g:chart-rect-min-l2-dist2}
and
{\sf g:chart-rect-min-l2-dist}
compute the minimum $L^2$ distance between two rects.

{\sf g:chart-distance-to-line} computes the $L^2$ distance
between a point and a line segment.
