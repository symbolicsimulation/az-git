;;;-*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================
;;; Chart-Rects
;;;============================================================
;;; defstruct representation:

(defstruct (Chart-Rect
	    (:conc-name nil)
	    (:constructor %%make-chart-rect)
	    (:copier %chart-rect-copier)
	    (:predicate %chart-rect?))

  "A <Chart-Rect> is a screen rectangle whose sides are parallel to
the coordinate axes. Other names are commonly used for this, including
``rectangle'' and ``region''. We use <Chart-Rect> because we want to
reserve ``rectangle'' for general rectangles and ``region'' for more
general specifications of sets of pixels. We welcome any suggestions
for names that are better than <Chart-Rect>. One we have considered is
2D-Interval, but it seems too verbose.)

<Chart-Rect>s have the following generalized accessors:
<chart-rect-xmin>, <chart-rect-xmax>, <chart-rect-ymin>,
<chart-rect-ymax>, <chart-rect-left>, <chart-rect-top>,
<chart-rect-right>, <chart-rect-bottom>, <chart-rect-width>, and
<chart-rect-height>.  At present, all rects are represented internally
by <xmin>, <width>, <ymin>, and <height> ``slots''. For <Chart-Rect>s,
<xmin> is equivalent to <left>, and <ymin> to <bottom>.

(make-chart-rect) takes any two of :left, :right, and :height and any
two of :top, :bottom, and :width.  The internal representation may be
changed at any time."

  (%chart-rect-xmin 0.0d0 :type Chart-Coordinate)
  (%chart-rect-ymin 0.0d0 :type Chart-Coordinate)
  (%chart-rect-width 0.0d0 :type Positive-Chart-Coordinate)
  (%chart-rect-height 0.0d0 :type Positive-Chart-Coordinate))

;;;------------------------------------------------------------

(defun %chart-rect-list? (rs)
  (and (not (null rs))
       (every #'%chart-rect? rs)))

(deftype Chart-Rect-List ()
  "A list of Chart-Rects."
  '(satisfies %chart-rect-list?))

;;;-------------------------------------------------------

(defun %make-chart-rect (x y w h)
  (declare (type Chart-Coordinate x y w h))
  (%%make-chart-rect :%chart-rect-xmin x
		     :%chart-rect-ymin y
		     :%chart-rect-width w
		     :%chart-rect-height h))

(defun parse-chart-interval-specs (xmin xmax dx)
  (cond
   ((null xmax)
    (when (null xmin) (setf xmin 0.0d0))
    (when (null dx) (setf dx 0.0d0))
    (locally 
	(declare (type Chart-Coordinate xmin)
		 (type Positive-Chart-Coordinate dx))
      (setf xmax (+ xmin dx))))
   ((null xmin)
    (when (null dx) (setf dx 0.0d0))
    (locally 
	(declare (type Chart-Coordinate xmax)
		 (type Positive-Chart-Coordinate dx))
      (setf xmin (- xmax dx))))
   ((null dx)
    (locally 
	(declare (type Chart-Coordinate xmin xmax))
      (assert (>= xmax xmin))
      (setf dx (- xmax xmin))))
   (t
    (locally 
	(declare (type Chart-Coordinate xmin xmax)
		 (type Positive-Chart-Coordinate dx))
      (assert (= dx (- xmax xmin))))))
  (values xmin xmax dx))

(defun make-chart-rect (&key (width nil) (right nil) (left nil)
			     (height nil) (bottom nil) (top nil))

  "The constructor function for Chart-Rect.

The Rect is optionally initialized by specifying any two of <:left>,
<:right>, and <:width>, and any two of <:top>, <:bottom>, and
<:height>."

  (declare (type (or Null Chart-Coordinate) left right top bottom)
	   (type (or Null Positive-Chart-Coordinate) width height)
	   (:returns (type Chart-Rect)))

  (multiple-value-bind
      (left right width) (parse-chart-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(bottom top height) (parse-chart-interval-specs bottom top height)
      (declare (ignore top))
      (%make-chart-rect left bottom width height)))) 

;;;=======================================================
;;; A resource for borrowing temporary Chart-Rect objects:
;;;=======================================================

(defparameter *chart-rect-resource* ())

;;;-------------------------------------------------------

(defun %borrow-chart-rect (xmin ymin width height)
  (declare (special *chart-rect-resource*)
	   (type Chart-Coordinate xmin ymin)
	   (type Positive-Chart-Coordinate width height)
	   (:returns (type Chart-Rect)))
  (let ((r (pop *chart-rect-resource*)))
    (cond ((null r)
	   (%make-chart-rect xmin ymin width height))
	  (t
	   (setf (%chart-rect-xmin r) xmin)
	   (setf (%chart-rect-ymin r) ymin)
	   (setf (%chart-rect-width r) width)
	   (setf (%chart-rect-height r) height)
	   r))))

(defun borrow-chart-rect (&key (width nil) (right nil) (left nil)
			       (height nil) (bottom nil) (top nil))

  "Borrow a rect from the resource, specifying any two of <:left>,
<:right>, and <:width>, and any two of <:top>, <:bottom>, and <:height>."

  (declare (special *chart-rect-resource*)
	   (type (or Null Chart-Coordinate) left right bottom top)
	   (type (or Null Positive-Chart-Coordinate) width height)
	   (:returns (type Chart-Rect)))

  (multiple-value-bind
      (left right width) (parse-chart-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(bottom top height) (parse-chart-interval-specs bottom top height)
      (declare (ignore top))
      (%borrow-chart-rect left bottom width height))))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %return-chart-rect)))

(defun %return-chart-rect (r)
  (declare (special *chart-rect-resource*)
	   (type Chart-Rect r))
  (push r *chart-rect-resource*))

(defun return-chart-rect (r)

  "Return a rect to the resource. This must be done with care, since
any further reference to the rect will be an error."

  (declare (type Chart-Rect r)
	   (:returns t))
  (%return-chart-rect r))

;;;-------------------------------------------------------
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro %with-borrowed-chart-rect ((vname
				      &key
				      (xmin 0.0d0)
				      (ymin 0.0d0)
				      (width 0.0d0)
				      (height 0.0d0))
				     &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name (%borrow-chart-rect ,xmin ,ymin ,width ,height))
	    (,vname ,return-name))
       (multiple-value-prog1
	   (progn ,@body)
	 (%return-chart-rect ,return-name)))))

(defmacro with-borrowed-chart-rect ((vname
				     &key
				     (width nil) (right nil) (left nil)
				     (height nil) (bottom nil) (top nil))
				    &body body)

  "This macro binds <vname> to a rect borrowed from the resource,
optionally initialized by specifying any two of <:left>, <:right>, and
<:width>, and any two of <:top>, <:bottom>, and <:height>, and returns
the rect to the resource on exit.  The user should be careful not to
create references to the point that survive the dynamic extent of the
macro body."

  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name
	     (borrow-chart-rect :left ,left :right ,right :width ,width
				:top ,top :bottom ,bottom :height ,height))
	    (,vname ,return-name))
       (multiple-value-prog1
	   (progn ,@body)
	 (return-chart-rect ,return-name))))) 

;;;------------------------------------------------------------
;;; updating coordinates

(declaim (inline %set-chart-rect-coords))

(defun %set-chart-rect-coords (r x y w h)
  (declare (optimize (safety 0) (speed 3))
	   (type Chart-Rect r)
	   (type Chart-Coordinate x y)
	   (type Positive-Chart-Coordinate w h))
  (setf (%chart-rect-xmin r) x)
  (setf (%chart-rect-ymin r) y)
  (setf (%chart-rect-width  r) w)
  (setf (%chart-rect-height r) h)
  r)

(defun set-chart-rect-coords (r
			      &key
			      (width nil) (right nil) (left nil)
			      (height nil) (bottom nil) (top nil))
  (declare (type Chart-Rect r))
  (multiple-value-bind
      (left right width) (parse-chart-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(bottom top height) (parse-chart-interval-specs bottom top height)
      (declare (ignore top))
      (%set-chart-rect-coords r left bottom width height)))
  r)

;;;-------------------------------------------------------
;;; "safe" accessors for "slots"
;;;-------------------------------------------------------

(defun chart-rect-xmin (r)
  "The left side of <r>."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-xmin r))

(defun set-chart-rect-xmin (r x)
  (declare (type Chart-Rect r)
	   (type Chart-Coordinate x))
  (setf (%chart-rect-xmin r) x))

(defsetf chart-rect-xmin (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(set-chart-rect-xmin ,r ,x))

;;;-------------------------------------------------------

(defun chart-rect-ymin (r)
  "The bottom of <r>."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-ymin r))

(defun set-chart-rect-ymin (r x)
  (declare (type Chart-Rect r)
	   (type Chart-Coordinate x))
  (setf (%chart-rect-ymin r) x))

(defsetf chart-rect-ymin (r) (y)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(set-chart-rect-ymin ,r ,y))

;;;-------------------------------------------------------

(defun chart-rect-width (r)
  "The width of <r>."
  (declare (type Chart-Rect r)
	   (:returns (type Positive-Chart-Coordinate)))
  (%chart-rect-width r))

(defun set-chart-rect-width (r x)
  (declare (type Chart-Rect r)
	   (type Positive-Chart-Coordinate x))
  (setf (%chart-rect-width r) x))

(defsetf chart-rect-width (r) (x)
  "Changing a rect's width or height is interpreted as a scaling."
  `(set-chart-rect-width ,r ,x))

;;;-------------------------------------------------------

(defun chart-rect-height (r)
  "The height of <r>."
  (declare (type Chart-Rect r)
	   (:returns (type Positive-Chart-Coordinate)))
  (%chart-rect-height r))

(defun set-chart-rect-height (r x)
  (declare (type Chart-Rect r)
	   (type Positive-Chart-Coordinate x))
  (setf (%chart-rect-height r) x))

(defsetf chart-rect-height (r) (x)
  "Changing a rect's width or height is interpreted as a scaling."
  `(set-chart-rect-height ,r ,x))

;;;------------------------------------------------------------
;;; Setting the xmax of a rect is interpreted as a
;;; translation, rather than a stretching, to make xmax 
;;; behave like xmin.

(defun %chart-rect-xmax (r)
  (declare (type Chart-Rect r))
  (+ (the Chart-Coordinate (%chart-rect-xmin r))
     (the Positive-Chart-Coordinate (%chart-rect-width r))))

(defun chart-rect-xmax (r)
  "The right side of <r>."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-xmax r))

(defun %set-chart-rect-xmax  (r x)
  (declare (type Chart-Rect r)
	   (type Chart-Coordinate x))
  (setf (%chart-rect-xmin r)
    (- x (the Chart-Coordinate (%chart-rect-width r))))
  x)

(defun set-chart-rect-xmax (r x)
  (declare (type Chart-Rect r)
	   (type Chart-Coordinate x))
  (assert (>= x (%chart-rect-xmin r)))
  (%set-chart-rect-xmax r x))

(defsetf %chart-rect-xmax (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(%set-chart-rect-xmax ,r ,x))

(defsetf chart-rect-xmax (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  

  `(set-chart-rect-xmax ,r ,x))

;;;------------------------------------------------------------
;;; Setting the ymax of a rect is interpreted as a
;;; translation, rather than a stretching, to make ymax
;;; behave like ymin.

(defun %chart-rect-ymax (r)
  (declare (type Chart-Rect r))
  (+ (the Chart-Coordinate (%chart-rect-ymin r))
     (the Positive-Chart-Coordinate (%chart-rect-height r))))

(defun chart-rect-ymax (r)
  "The top of <r>."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-ymax r))

(defun %set-chart-rect-ymax  (r x)
  (declare (type Chart-Rect r)
	   (type Chart-Coordinate x))
  (setf (%chart-rect-ymin r)
    (- x (the Chart-Coordinate (%chart-rect-height r))))
  x)

(defun set-chart-rect-ymax (r x)
  (declare (type Chart-Rect r)
	   (type Chart-Coordinate x))
  (assert (>= x (%chart-rect-ymin r)))
  (%set-chart-rect-ymax r x))

(defsetf %chart-rect-ymax (r) (x) `(%set-chart-rect-ymax ,r ,x))

(defsetf chart-rect-ymax (r) (y)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(set-chart-rect-ymax ,r ,y))

;;;------------------------------------------------------------

(defun %chart-rect-left (r)
  (declare (type Chart-Rect r))
  (%chart-rect-xmin r))

(defun chart-rect-left (r)
  "``Left'' is an alias for ``xmin''."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-xmin r))

(defsetf %chart-rect-left (r) (x) `(setf (%chart-rect-xmin ,r) ,x))
(defsetf chart-rect-left (r) (x)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(setf (chart-rect-xmin ,r) ,x))

;;;------------------------------------------------------------

(defun %chart-rect-right (r)
  (declare (type Chart-Rect r))
  (%chart-rect-xmax r))

(defun chart-rect-right (r)
  "``Right'' is an alias for ``xmax''."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-xmax r))

(defsetf %chart-rect-right (r) (x)
  `(setf (%chart-rect-xmax ,r) ,x))

(defsetf chart-rect-right (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (chart-rect-xmax ,r) ,x))

;;;------------------------------------------------------------

(defun %chart-rect-top (r)
  (declare (type Chart-Rect r))
  (%chart-rect-ymax r))

(defun chart-rect-top (r)

  "``Top'' is an alias for ``ymax''."

  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-ymax r))

(defsetf %chart-rect-top (r) (x) `(setf (%chart-rect-ymax ,r) ,x))

(defsetf chart-rect-top (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (chart-rect-ymax ,r) ,y))

;;;------------------------------------------------------------

(defun %chart-rect-bottom (r)
  (declare (type Chart-Rect r))
  (%chart-rect-ymin r))

(defun chart-rect-bottom (r)
  "``Bottom'' is an alias for ``ymin''."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Coordinate)))
  (%chart-rect-ymin r))

(defsetf %chart-rect-bottom (r) (x) `(setf (%chart-rect-ymin ,r) ,x))

(defsetf chart-rect-bottom (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  
  `(setf (chart-rect-ymin ,r) ,y))

;;;------------------------------------------------------------
;;; Chart-Point based Chart-Rect specification:
;;; 

(defun %chart-rect-origin (r p)
  (declare (type Chart-Rect r)
	   (type Chart-Point p))
  (setf (%chart-point-x p) (the Chart-Coordinate (%chart-rect-xmin r)))
  (setf (%chart-point-y p) (the Chart-Coordinate (%chart-rect-ymin r)))
  p)

(defun %chart-rect-extent (r v)
  (declare (type Chart-Rect r)
	   (type Chart-Vector v))
  (setf (%chart-vector-x v)
    (the Positive-Chart-Coordinate (%chart-rect-width r)))
  (setf (%chart-vector-y v)
    (the Positive-Chart-Coordinate (%chart-rect-height r)))
  v)

(defun %chart-rect-origin-extent (r p v)
  (declare (type Chart-Rect r)
	   (type Chart-Point p)
	   (type Chart-Vector v))
  (setf (%chart-point-x p)
    (the Chart-Coordinate (%chart-rect-xmin r)))
  (setf (%chart-point-y p)
    (the Chart-Coordinate (%chart-rect-ymin r)))
  (setf (%chart-vector-x v)
    (the Positive-Chart-Coordinate (%chart-rect-width r)))
  (setf (%chart-vector-y v)
    (the Positive-Chart-Coordinate (%chart-rect-height r)))
  (values p v))

(defun %set-chart-rect-origin (r p)
  (declare (type Chart-Rect r)
	   (type Chart-Point p))
  (setf (%chart-rect-xmin r) (the Chart-Coordinate (%chart-point-x p)))
  (setf (%chart-rect-ymin r) (the Chart-Coordinate (%chart-point-y p)))
  p)

(defun %set-chart-rect-extent (r v)
  (declare (type Chart-Rect r)
	   (type Positive-chart-vector v))
  (setf (%chart-rect-width r)
    (the Positive-Chart-Coordinate (%chart-vector-x v)))
  (setf (%chart-rect-height r)
    (the Positive-Chart-Coordinate (%chart-vector-y v)))
  v)

(defun %set-chart-rect-origin-extent (r p v)
  (declare (type Chart-Rect r)
	   (type Chart-Point p)
	   (type Positive-chart-vector v))
  (setf (%chart-rect-xmin r)
    (the Chart-Coordinate (%chart-point-x p)))
  (setf (%chart-rect-ymin r)
    (the Chart-Coordinate (%chart-point-y p)))
  (setf (%chart-rect-width r)
    (the Positive-Chart-Coordinate (%chart-vector-x v)))
  (setf (%chart-rect-height r)
    (the Positive-Chart-Coordinate (%chart-vector-y v)))
  r)

(defun chart-rect-origin (r &key (result (%make-chart-point 0.0d0 0.0d0)))
 
  "The origin is the point at the lower left.

The extent of a rect is a vector whose coordinates are its width and
height.  The origin of a rect is a corner point.  Which corner point
we use is determined by the coordinate system and the desire to have
the sum of the origin and the extent be the opposite corner of the
rect. Therefore, Screen-Rect's, the origin is the xmin-ymin corner;
for Chart-Rect's, it's the xmin-ymax corner."

  (declare (type Chart-Rect r)
	   (type Chart-Point result)
	   (:returns result))
  (%chart-rect-origin r result))

(defun chart-rect-extent (r &key (result (%make-chart-vector 0.0d0 0.0d0)))
  
  "The extent is the vector from the lower left to the upper right.

The extent of a rect is a vector whose coordinates are its width and
height.  The origin of a rect is a corner point.  Which corner point
we use is determined by the coordinate system and the desire to have
the sum of the origin and the extent be the opposite corner of the
rect. Therefore, Screen-Rect's, the origin is the xmin-ymin corner;
for Chart-Rect's, it's the xmin-ymax corner."

  (declare (type Chart-Rect r)
	   (type Chart-Vector result)
	   (:returns result))
  (%chart-rect-extent r result))

(defun chart-rect-origin-extent (r
				 &key
				 (origin-result (make-chart-point))
				 (extent-result (make-chart-vector)))
  (declare (type Chart-Rect r)
	   (type Chart-Point origin-result)
	   (type Chart-Vector extent-result)
	   (:returns (values origin-result extent-result)))
  
  (%chart-rect-origin-extent r origin-result extent-result))

(defun set-chart-rect-origin (r p)
  (declare (type Chart-Rect r)
	   (type Chart-Point p))
  (%set-chart-rect-origin r p))

(defun set-chart-rect-extent (r v)
  (declare (type Chart-Rect r)
	   (type Positive-Chart-Vector v))
  (%set-chart-rect-extent r v))

(defun set-chart-rect-origin-extent (r p v)
  (declare (type Chart-Rect r)
	   (type Chart-Point p)
	   (type Positive-chart-vector v))
  (%set-chart-rect-origin-extent r p v))

(defsetf chart-rect-origin (r) (p)

  "Changing the origin of a rect is interpreted as a translation."

  `(set-chart-rect-origin ,r ,p))

(defsetf chart-rect-extent (r) (v)

  "Changing the extent of a rect is interpreted as a scaling."

  `(set-chart-rect-extent ,r ,v))

;;;------------------------------------------------------------

(defun chart-rect-center (r)
  "Return a new point at the center of <r>."
  (declare (type Chart-Rect r)
	   (:returns (type Chart-Point)))
  (make-chart-point
   :x (+ (chart-rect-xmin r) (/ (chart-rect-width r) 2.0d0))
   :y (+ (chart-rect-ymin r) (/ (chart-rect-height r) 2.0d0))))

;;;------------------------------------------------------------

(defun %copy-chart-rect (r0 r1)
  (declare (type Chart-Rect r0 r1))
  (setf (the Chart-Coordinate (%chart-rect-xmin r1))
    (the Chart-Coordinate (%chart-rect-xmin r0)))
  (setf (the Chart-Coordinate (%chart-rect-ymin r1))
    (the Chart-Coordinate (%chart-rect-ymin r0)))
  (setf (the Positive-Chart-Coordinate (%chart-rect-width r1))
    (the Positive-Chart-Coordinate (%chart-rect-width r0)))
  (setf (the Positive-Chart-Coordinate (%chart-rect-height r1))
    (the Positive-Chart-Coordinate (%chart-rect-height r0)))
  r1)

(defun copy-chart-rect (r &key (result (make-chart-rect)))
  "The copier function for Chart-Rects."
  (declare (type Chart-Rect r)
	   (type Chart-Rect result)
	   (:returns result))
  (%copy-chart-rect r result))

;;;------------------------------------------------------------
;;; equivalence tester

(defun %equal-chart-rects? (r0 r1)
  (declare (type Chart-Rect r0 r1))
  (and (= (the Chart-Coordinate (%chart-rect-xmin r0))
	  (the Chart-Coordinate (%chart-rect-xmin r1)))
       (= (the Chart-Coordinate (%chart-rect-ymin r0))
	  (the Chart-Coordinate (%chart-rect-ymin r1)))
       (= (the Positive-Chart-Coordinate (%chart-rect-width r0))
	  (the Positive-Chart-Coordinate (%chart-rect-width r1)))
       (= (the Positive-Chart-Coordinate (%chart-rect-height r0))
	  (the Positive-Chart-Coordinate (%chart-rect-height r1)))))

(defun equal-chart-rects? (r0 r1)
  "The equality predicate for Chart-Rects."
  (declare (type Chart-Rect r0 r1)
	   (:returns (type (Member t nil))))
  (%equal-chart-rects? r0 r1))

 

