;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================
;;; Screen-rects
;;;============================================================
;;;
;;; <Screen-rect>s have the following generalized accessors:
;;; <screen-rect-xmin>, <screen-rect-xmax>, <screen-rect-ymin>,
;;; <screen-rect-ymax>, <screen-rect-left>, <screen-rect-top>,
;;; <screen-rect-right>, <screen-rect-bottom>, <screen-rect-width>,
;;; and <screen-rect-height>.
;;; At present, all rects are represented internally by <xmin>, <width>,
;;; <ymin>, and <height> "slots". For <Screen-Rect>s, <xmin> is equivalent
;;; to <left>, and <ymin> to <top>.
;;;
;;; The external interface for making <Screen-Rect>'s
;;; (make-screen-rect) takes any two of :left, :right, and :height and
;;; any two of :top, :bottom, and :width.  The internal representation
;;; may be changed at any time.
;;; 
;;; Because of the pernicious danger of fence post errors, we give a
;;; careful description of which pixels the various coordinates refer
;;; to, and also give name to common alternative specifications:
;;;
;;; The (<screen-rect-left>,<screen-rect-right>) and
;;; (<screen-rect-top>, <screen-rect-bottom>) pairs are specifications
;;; of integer intervals like the (start, end) parameters to the CL
;;; sequence functions.  <screen-rect-left> is the coordinate of the
;;; leftmost column that intersects the Screen-Rect and
;;; <screen-rect-top> the coordinate of the topmost row that intersects
;;; the screen-rect.  <screen-rect-right> is the coordinate of the first
;;; column not intersecting the Screen-Rect to the right and
;;; <screen-rect-bottom> is the coordinate of the first row below the
;;; Screen-Rect not intersecting it.  (See figure below.)  This means
;;; that <screen-rect-width> = <screen-rect-right> - <screen-rect-left>
;;; is the number of columns that intersect the screen-rect,
;;; <screen-rect-height> = <screen-rect-bottom> - <screen-rect-top> is
;;; the number of rows that intersect the screen-rect, and
;;; <screen-rect-width>*<screen-rect-height> is the number of pixels in
;;; the screen-rect.
;;; 
;;;    <screen-rect-left>
;;;    |    <screen-rect-right>
;;;    |    |
;;;    v    v
;;;   ooooooo 
;;;   oxxxxxo <- <screen-rect-top>
;;;   oxxxxxo
;;;   oxxxxxo 
;;;   ooooooo <- <screen-rect-bottom>
;;;
;;; Implementation of drawing operations will require interfacing with
;;; other parameterizations; to make this a little easier, we give
;;; standard names to commonly occurring coordinates:
;;;
;;;   outer-left
;;;   |inner-left
;;;   ||   inner-right
;;;   ||   |outer-right
;;;   ||   ||
;;;   vv   vv
;;;   ooooooo <-outer-top
;;;   oxxxxxo <-inner-top
;;;   oxxxxxo
;;;   oxxxxxo <-inner-bottom
;;;   ooooooo <-outer-bottom
;;;
;;; For example, implementations of the drawing operations will often
;;; need to transform to the analogous parameterization in top-to-bottom
;;; coordinates. This means converting our (inner-left, inner-bottom,
;;; width, height) representation to the coordinates of the (inner-left,
;;; inner-top, width, height) in the top-to-bottom coordinates.

;;; Width and height must be non-negative.  Zero width and zero height
;;; are taken to imply an empty screen-rect. A width and height of 1
;;; means the Screen-Rect covers one pixel. Empty screen-rects still
;;; have a location.
;;;=======================================================
;;; defstruct representation:

(defstruct (Screen-Rect
	    (:conc-name nil)
	    (:constructor %%make-screen-rect)
	    (:copier %screen-rect-copier)
	    (:predicate %screen-rect?)
	    (:print-function print-screen-rect))

  "A <Screen-Rect> is a screen rectangle whose sides are parallel to
the coordinate axes. Other names are commonly used for this,
including ``rectangle'' and ``region''. We use <Screen-Rect> because we
want to reserve ``rectangle'' for general rectangles and ``region'' for
more general specifications of sets of pixels. We welcome any
suggestions for names that are better than <Screen-Rect>. (One we
have considered is 2D-Interval, but it seems too verbose.)

We use a coordinate system where x increases from left to right,
and, unfortrunately, like most window systems, y from top to bottom.

Width and height must be non-negative.  Zero width and zero height
are taken to imply an empty screen-rect. A width and height of 1
means the Screen-Rect covers one pixel. Empty screen-rects still
have a location."

  (%screen-rect-xmin 0 :type Screen-Coordinate)
  (%screen-rect-ymin 0 :type Screen-Coordinate)
  (%screen-rect-width 0 :type Positive-Screen-Coordinate)
  (%screen-rect-height 0 :type Positive-Screen-Coordinate))

;;;------------------------------------------------------------

(defun print-screen-rect (r stream depth)
  "Print screen rects with their coords."
  (declare (type Screen-Rect r)
	   (type Stream stream)
	   (type (or Null Integer) depth)
	   (ignore depth))
  (az:printing-random-thing
   (r stream)
   (format stream "R ~d ~d ~d ~d"
	   (%screen-rect-xmin r)
	   (%screen-rect-ymin r)
	   (%screen-rect-width r)
	   (%screen-rect-height r))))

;;;------------------------------------------------------------

(defun %screen-rect-list? (rlist)
  (and (not (null rlist))
       (every #'%screen-rect? rlist)))

(deftype Screen-Rect-List ()
  "A list of Screen-Rects."
  '(satisfies %screen-rect-list?))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %make-screen-rect)))

(defun %make-screen-rect (x y w h)
  (declare (type Screen-Coordinate x y w h))
  (%%make-screen-rect :%screen-rect-xmin x
		      :%screen-rect-ymin y
		      :%screen-rect-width w
		      :%screen-rect-height h))

(defun parse-screen-interval-specs (xmin xmax dx)
  (cond
   ((null xmax)
    (when (null xmin) (setf xmin 0))
    (when (null dx) (setf dx 0))
    (locally
	(declare (type Screen-Coordinate xmin)
		 (type Positive-Screen-Coordinate dx))
      (setf xmax (+ xmin dx))))
   ((null xmin)
    (when (null dx) (setf dx 0))
    (locally 
	(declare (type Screen-Coordinate xmax)
		 (type Positive-Screen-Coordinate dx))
      (setf xmin (- xmax dx))))
   ((null dx)
    (locally 
	(declare (type Screen-Coordinate xmin xmax))
      (assert (>= xmax xmin))
      (setf dx (- xmax xmin))))
   (t
    (locally
	(declare (type Screen-Coordinate xmin xmax)
		 (type Positive-Screen-Coordinate dx))
      (assert (= dx (- xmax xmin))))))
  (values xmin xmax dx))

(defun make-screen-rect (&key
			 (width nil) (right nil) (left nil)
			 (height nil) (bottom nil) (top nil))
  			
  "The constructor function for Screen-Rect.

The Rect is optionally initialized by specifying any two of <:left>,
<:right>, and <:width>, and any two of <:top>, <:bottom>, and
<:height>."

  (declare (type (or Null Screen-Coordinate) left right top bottom)
	   (type (or Null Positive-Screen-Coordinate) width height)
	   (:returns (type Screen-Rect)))

  (multiple-value-bind
      (left right width) (parse-screen-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(top bottom height) (parse-screen-interval-specs top bottom height)
      (declare (ignore bottom))
      (%make-screen-rect left top width height))))


;;;=======================================================
;;; A resource for borrowing temporary Screen-Rect objects:
;;;=======================================================

(defparameter *screen-rect-resource* ())

;;;-------------------------------------------------------

(defun %borrow-screen-rect (xmin ymin width height)
  (declare (special *screen-rect-resource*)
	   (type Screen-Coordinate xmin ymin)
	   (type Positive-Screen-Coordinate width height)
	   (:returns (type Screen-Rect)))
  (let ((r (pop *screen-rect-resource*)))
    (cond ((null r)
	   (%make-screen-rect xmin ymin width height))
	  (t
	   (setf (%screen-rect-xmin r) xmin)
	   (setf (%screen-rect-ymin r) ymin)
	   (setf (%screen-rect-width r) width)
	   (setf (%screen-rect-height r) height)
	   r))))

(defun borrow-screen-rect (&key (width nil) (right nil) (left nil)
				(height nil) (bottom nil) (top nil))
  "Borrow a rect from the resource, specifying any two of <:left>,
<:right>, and <:width>, and any two of <:top>, <:bottom>, and <:height>."
  (declare (type (or Null Screen-Coordinate) left right top bottom)
	   (type (or Null Positive-Screen-Coordinate) width height)
	   (:returns (type Screen-Rect)))
  (multiple-value-bind
      (left right width) (parse-screen-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(top bottom height) (parse-screen-interval-specs top bottom height)
      (declare (ignore bottom))
      (%borrow-screen-rect left top width height))))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %return-screen-rect)))

(defun %return-screen-rect (r)
  (declare (special *screen-rect-resource*)
	   (type Screen-Rect r))
  (push r *screen-rect-resource*))

(defun return-screen-rect (r)
  "Return a rect to the resource. This must be done with care, since
any further reference to the rect will be an error."

  (declare (type Screen-Rect r)
	   (:returns t))
  (%return-screen-rect r))

;;;-------------------------------------------------------
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro %with-borrowed-screen-rect ((vname
				       &key
				       (xmin 0)
				       (ymin 0)
				       (width 0)
				       (height 0))
				      &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name (%borrow-screen-rect ,xmin ,ymin ,width ,height))
	    (,vname ,return-name))
       (multiple-value-prog1
	   (progn ,@body)
	 (%return-screen-rect ,return-name)))))

(defmacro with-borrowed-screen-rect ((vname
				      &key
				      (width nil) (right nil) (left nil)
				      (height nil) (bottom nil) (top nil))
				     &body body)

  "This macro binds <vname> to a rect borrowed from the resource,
optionally initialized by specifying any two of <:left>, <:right>, and
<:width>, and any two of <:top>, <:bottom>, and <:height>, and returns
the rect to the resource on exit.  The user should be careful not to
create references to the point that survive the dynamic extent of the
macro body."

  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name
	     (borrow-screen-rect :left ,left :right ,right :width ,width
				 :top ,top :bottom ,bottom :height ,height))
	    (,vname ,return-name))
       (multiple-value-prog1
	   (progn ,@body)
	 (return-screen-rect ,return-name))))) 

;;;------------------------------------------------------------
;;; updating coordinates

(defun %set-screen-rect-coords (r x y w h)
  (declare (type Screen-Rect r)
	   (type Screen-Coordinate x y)
	   (type Positive-Screen-Coordinate w h))
  (setf (%screen-rect-xmin r) x)
  (setf (%screen-rect-ymin r) y)
  (setf (%screen-rect-width  r) w)
  (setf (%screen-rect-height r) h)
  r)

(defun set-screen-rect-coords (r
			       &key
			       (width nil) (right nil) (left nil)
			       (height nil) (bottom nil) (top nil))
  (declare (type Screen-Rect r))
  (multiple-value-bind
      (left right width) (parse-screen-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(top bottom height) (parse-screen-interval-specs top bottom height)
      (declare (ignore bottom))
      (%set-screen-rect-coords r left top width height)))
  r) 

;;;-------------------------------------------------------
;;; "safe" accessors for "slots"

(defun screen-rect-xmin (r)
  "The left side of <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-xmin r))

(defun set-screen-rect-xmin (r x)
  (declare (type Screen-Rect r)
	   (type Screen-Coordinate x))
  (setf (%screen-rect-xmin r) x))

(defsetf screen-rect-xmin (r) (x)
  
  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(set-screen-rect-xmin ,r ,x))

;;;------------------------------------------------------------

(defun screen-rect-ymin (r)
  "The top of <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-ymin r))

(defun set-screen-rect-ymin (r x)
  (declare (type Screen-Rect r)
	   (type Screen-Coordinate x))
  (setf (%screen-rect-ymin r) x))

(defsetf screen-rect-ymin (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(set-screen-rect-ymin ,r ,y))

;;;------------------------------------------------------------

(defun screen-rect-width (r)
  "The width of <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Positive-Screen-Coordinate)))
  (%screen-rect-width r))

(defun set-screen-rect-width (r x)
  (declare (type Screen-Rect r)
	   (type Positive-Screen-Coordinate x))
  (setf (%screen-rect-width r) x))

(defsetf screen-rect-width (r) (x)
  "Changing a rect's width or height is interpreted as a scaling."
  `(set-screen-rect-width ,r ,x))

;;;------------------------------------------------------------

(defun screen-rect-height (r)
  "The height of <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Positive-Screen-Coordinate)))
  (%screen-rect-height r))

(defun set-screen-rect-height (r x)
  (declare (type Screen-Rect r)
	   (type Positive-Screen-Coordinate x))
  (setf (%screen-rect-height r) x))

(defsetf screen-rect-height (r) (x)
  "Changing a rect's width or height is interpreted as a scaling."
  `(set-screen-rect-height ,r ,x))

;;;------------------------------------------------------------

(defun %screen-rect-xmax (r)
  (declare (type Screen-Rect r))
  (+ (the Screen-Coordinate (%screen-rect-xmin r))
     (the Positive-Screen-Coordinate (%screen-rect-width r))))

(defun screen-rect-xmax (r)
  "The right side of <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-xmax r))

(defun %set-screen-rect-xmax  (r x)
  (declare (type Screen-Rect r)
	   (type Screen-Coordinate x))
  (setf (%screen-rect-xmin r)
    (- x (the Screen-Coordinate (%screen-rect-width r))))
  x)

(defsetf %screen-rect-xmax (r) (x) `(%set-screen-rect-xmax ,r ,x))

(defun set-screen-rect-xmax (r x)
  (declare (type Screen-Rect r)
	   (type Screen-Coordinate x))
  (assert (>= x (%screen-rect-xmin r)))
  (setf (%screen-rect-xmax r) x))

(defsetf screen-rect-xmax (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(set-screen-rect-xmax ,r ,x))

;;;------------------------------------------------------------
;;; Setting the ymax of a rect is interpreted as a
;;; translation, rather than a stretching, to make ymax
;;; behave like ymin.

(defun %screen-rect-ymax (r)
  (declare (type Screen-Rect r))
  (+ (the Screen-Coordinate (%screen-rect-ymin r))
     (the Positive-Screen-Coordinate (%screen-rect-height r))))

(defun screen-rect-ymax (r)
  "The bottom of <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-ymax r))

(defun %set-screen-rect-ymax  (r x)
  (declare (type Screen-Rect r)
	   (type Screen-Coordinate x))
  (setf (%screen-rect-ymin r)
    (- x (the Screen-Coordinate (%screen-rect-height r))))
  x)

(defsetf %screen-rect-ymax (r) (x) `(%set-screen-rect-ymax ,r ,x))

(defun set-screen-rect-ymax (r x)
  (declare (type Screen-Rect r)
	   (type Screen-Coordinate x))
  (assert (>= x (%screen-rect-ymin r)))
  (setf (%screen-rect-ymax r) x))

(defsetf screen-rect-ymax (r) (y)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(set-screen-rect-ymax ,r ,y))

;;;------------------------------------------------------------
;;; "left" alias for "xmin"

(defun %screen-rect-left (r)
  (declare (type Screen-Rect r))
  (%screen-rect-xmin r))

(defun screen-rect-left (r)
  "``Left'' is an alias for ``xmin''."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-xmin r))

(defsetf %screen-rect-left (r) (x) `(setf (%screen-rect-xmin ,r) ,x))

(defsetf screen-rect-left (r) (x)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(setf (screen-rect-xmin ,r) ,x))

(defun %screen-rect-inner-left (r)
  (declare (type Screen-Rect r))
  (%screen-rect-xmin r))

(defun screen-rect-inner-left (r)
  "Returns the x coordinate of the left most pixel inside <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-xmin r))

(defsetf %screen-rect-inner-left (r) (x) `(setf (%screen-rect-xmin ,r) ,x))
(defsetf screen-rect-inner-left (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (screen-rect-xmin ,r) ,x))

(defun %screen-rect-outer-left (r)
  (declare (type Screen-Rect r))
  (- (the Screen-Coordinate (%screen-rect-xmin r)) 1))

(defun screen-rect-outer-left (r)

  "Returns the x coordinate of the first pixel outside <r> on the
left."

  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-outer-left r))

(defsetf %screen-rect-outer-left (r) (x)
  (az:once-only (x)
		`(progn (setf (%screen-rect-xmin ,r) (+ 1 ,x))
			,x)))

(defsetf screen-rect-outer-left (r) (x)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  (az:once-only (x)
		`(progn (setf (screen-rect-xmin ,r) (+ 1 ,x))
			,x)))

;;;------------------------------------------------------------
;;; "right" alias for "xmax"

(defun %screen-rect-right (r)
  (declare (type Screen-Rect r))
  (%screen-rect-xmax r))

(defun screen-rect-right (r)
  "``Right'' is an alias for ``xmax''."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-xmax r))

(defsetf %screen-rect-right (r) (x) `(setf (%screen-rect-xmax ,r) ,x))

(defsetf screen-rect-right (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (screen-rect-xmax ,r) ,x))

(defun %screen-rect-outer-right (r)
  (declare (type Screen-Rect r))
  (%screen-rect-xmax r))

(defun screen-rect-outer-right (r)

  
  "Returns the x coordinate of the first pixel outside <r> on the
right."
  
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-xmax r))

(defsetf %screen-rect-outer-right (r) (x) `(setf (%screen-rect-xmax ,r) ,x))
(defsetf screen-rect-outer-right (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (screen-rect-xmax ,r) ,x))

(defun %screen-rect-inner-right (r)
  (declare (type Screen-Rect r))
  (- (the Screen-Coordinate (%screen-rect-right r)) 1))

(defun screen-rect-inner-right (r)
  "Returns the x coordinate of the right most pixel inside <r>."

  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-inner-right r))

(defsetf %screen-rect-inner-right (r) (x)
  (az:once-only (x)
		`(progn (setf (%screen-rect-xmax ,r) (- ,x 1))
			,x)))

(defsetf screen-rect-inner-right (r) (x)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."
    
  (az:once-only (x)
		`(progn (setf (screen-rect-xmax ,r) (- ,x 1))
			,x)))

;;;------------------------------------------------------------
;;; "top" alias for "ymin"

(defun %screen-rect-top (r)
  (declare (type Screen-Rect r))
  (%screen-rect-ymin r))

(defun screen-rect-top (r)
  
  "``Top'' is an alias for ``ymin''."

  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (declare (type Screen-Rect r))
  (%screen-rect-ymin r))

(defsetf %screen-rect-top (r) (x) `(setf (%screen-rect-ymin ,r) ,x))
(defsetf screen-rect-top (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (screen-rect-ymin ,r) ,y))

(defun %screen-rect-inner-top (r)
  (declare (type Screen-Rect r))
  (%screen-rect-ymin r))

(defun screen-rect-inner-top (r)
  "Returns the y coordinate of the top most pixel inside <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-ymin r))

(defsetf %screen-rect-inner-top (r) (x) `(setf (%screen-rect-ymin ,r) ,x))

(defsetf screen-rect-inner-top (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (screen-rect-ymin ,r) ,y))

(defun %screen-rect-outer-top (r)
  (declare (type Screen-Rect r))
  (- (the Screen-Coordinate (%screen-rect-ymin r)) 1))

(defun screen-rect-outer-top (r)
  
  "Returns the y coordinate of the first pixel outside <r> on the
top."

  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-outer-top r))

(defsetf %screen-rect-outer-top (r) (x)
  (az:once-only (x)
		`(progn (setf (%screen-rect-ymin ,r) (+ 1 ,x))
			,x)))

(defsetf screen-rect-outer-top (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."
    
  (az:once-only (y)
		`(progn (setf (screen-rect-ymin ,r) (+ 1 ,y))
			,y)))

;;;------------------------------------------------------------
;;; "bottom" alias for "ymax"

(defun %screen-rect-bottom (r)
  (declare (type Screen-Rect r))
  (%screen-rect-ymax r))

(defun screen-rect-bottom (r)
  "``Bottom'' is an alias for ``ymax''."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-ymax r))

(defsetf %screen-rect-bottom (r) (x) `(setf (%screen-rect-ymax ,r) ,x))
(defsetf screen-rect-bottom (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (screen-rect-ymax ,r) ,y))

(defun %screen-rect-outer-bottom (r)
  (declare (type Screen-Rect r))
  (%screen-rect-ymax r))

(defun screen-rect-outer-bottom (r)
  "Returns the y coordinate of the first pixel outside <r> on the
bottom."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-ymax r))

(defsetf %screen-rect-outer-bottom (r) (x) `(setf (%screen-rect-ymax ,r) ,x))
(defsetf screen-rect-outer-bottom (r) (y)

  "Changing any of the border coordinates of a rect is interpreted as
a translation."

  `(setf (screen-rect-ymax ,r) ,y))

(defun %screen-rect-inner-bottom (r)
  (- (the Screen-Coordinate (%screen-rect-bottom r)) 1))

(defun screen-rect-inner-bottom (r)
  "Returns the y coordinate of the bottom most pixel inside <r>."
  (declare (type Screen-Rect r)
	   (:returns (type Screen-Coordinate)))
  (%screen-rect-inner-bottom r))

(defsetf %screen-rect-inner-bottom (r) (x)
  (az:once-only (x)
		`(progn (setf (%screen-rect-ymax ,r) (- ,x 1))
			,x)))

(defsetf screen-rect-inner-bottom (r) (y)
  
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
    
  (az:once-only (y)
		`(progn (setf (screen-rect-ymax ,r) (- ,y 1))
			,y)))

;;;------------------------------------------------------------
;;; Screen-Point based Screen-Rect specification:
;;;
;;; The extent of a rect is a vector whose coordinates are its width and
;;; height.  The origin of a rect is a corner point.  Which corner point
;;; we use is determined by the coordinate system and the desire to have
;;; the sum of the origin and the extent be the opposite corner of the
;;; rect. Therefore, for Screen-Rect's, the origin is the left-top
;;; corner; for Chart-Rect's, it's the left-bottom corner.

(eval-when (compile load eval)
  (proclaim '(Inline
	      %screen-rect-origin
	      %screen-rect-extent
	      %screen-rect-origin-extent
	      %set-screen-rect-origin
	      %set-screen-rect-extent
	      %set-screen-rect-origin-extent)))

(defun %screen-rect-origin (r p)
  (declare (type Screen-Rect r)
	   (type Screen-Point p))
  (setf (%screen-point-x p) (the Screen-Coordinate (%screen-rect-xmin r)))
  (setf (%screen-point-y p) (the Screen-Coordinate (%screen-rect-ymin r)))
  p)

(defun %screen-rect-extent (r v)
  (declare (type Screen-Rect r)
	   (type Screen-Vector v))
  (setf (%screen-vector-x v)
    (the Positive-Screen-Coordinate (%screen-rect-width r)))
  (setf (%screen-vector-y v)
    (the Positive-Screen-Coordinate (%screen-rect-height r)))
  v)

(defun %screen-rect-origin-extent (r p v)
  (declare (type Screen-Rect r)
	   (type Screen-Point p)
	   (type Screen-Vector v))
  (setf (%screen-point-x p)
    (the Screen-Coordinate (%screen-rect-xmin r)))
  (setf (%screen-point-y p)
    (the Screen-Coordinate (%screen-rect-ymin r)))
  (setf (%screen-vector-x v)
    (the Positive-Screen-Coordinate (%screen-rect-width r)))
  (setf (%screen-vector-y v)
    (the Positive-Screen-Coordinate (%screen-rect-height r)))
  (values p v))

(defun %set-screen-rect-origin (r p)
  (declare (type Screen-Rect r)
	   (type Screen-Point p))
  (setf (%screen-rect-xmin r) (the Screen-Coordinate (%screen-point-x p)))
  (setf (%screen-rect-ymin r) (the Screen-Coordinate (%screen-point-y p)))
  p)

(defun %set-screen-rect-extent (r v)
  (declare (type Screen-Rect r)
	   (type Positive-Screen-Vector v))
  (setf (%screen-rect-width r)
    (the Positive-Screen-Coordinate (%screen-vector-x v)))
  (setf (%screen-rect-height r)
    (the Positive-Screen-Coordinate (%screen-vector-y v)))
  v)

(defun %set-screen-rect-origin-extent (r p v)
  (declare (type Screen-Rect r)
	   (type Screen-Point p)
	   (type Positive-Screen-Vector v))
  (setf (%screen-rect-xmin r)
    (the Screen-Coordinate (%screen-point-x p)))
  (setf (%screen-rect-ymin r)
    (the Screen-Coordinate (%screen-point-y p)))
  (setf (%screen-rect-width r)
    (the Positive-Screen-Coordinate (%screen-vector-x v)))
  (setf (%screen-rect-height r)
    (the Positive-Screen-Coordinate (%screen-vector-y v)))
  r)

(defun screen-rect-origin (r &key (result (%make-screen-point 0 0)))

  "The origin is the point at the upper left.

The extent of a rect is a vector whose coordinates are its width and
height.  The origin of a rect is a corner point.  Which corner point
we use is determined by the coordinate system and the desire to have
the sum of the origin and the extent be the opposite corner of the
rect. Therefore, Screen-Rect's, the origin is the xmin-ymin corner;
for Chart-Rect's, it's the xmin-ymax corner."

  (declare (type Screen-Rect r)
	   (type Screen-Point result)
	   (:returns result))
  (%screen-rect-origin r result))

(defun screen-rect-extent (r &key (result (%make-screen-vector 0 0)))

  "The extent is the vector from the upper left to the lower right.

The extent of a rect is a vector whose coordinates are its width and
height.  The origin of a rect is a corner point.  Which corner point
we use is determined by the coordinate system and the desire to have
the sum of the origin and the extent be the opposite corner of the
rect. Therefore, Screen-Rect's, the origin is the xmin-ymin corner;
for Chart-Rect's, it's the xmin-ymax corner."

  (declare (type Screen-Rect r)
	   (type Screen-Vector result)
	   (:returns result))
  (%screen-rect-extent r result))

(defun screen-rect-origin-extent (r
				  &key
				  (origin-result (%make-screen-point 0 0))
				  (extent-result (%make-screen-vector 0 0)))
  (declare (type Screen-Rect r)
	   (type Screen-Point origin-result)
	   (type Screen-Vector extent-result))
  (%screen-rect-origin-extent r origin-result extent-result))

(defun set-screen-rect-origin (r p)
  (declare (type Screen-Rect r)
	   (type Screen-Point p))
  (%set-screen-rect-origin r p))

(defun set-screen-rect-extent (r v)
  (declare (type Screen-Rect r)
	   (type Positive-Screen-Vector v))
  (%set-screen-rect-extent r v))

(defun set-screen-rect-origin-extent (r p v)
  (declare (type Screen-Rect r)
	   (type Screen-Point p)
	   (type Positive-Screen-Vector v))
  (%set-screen-rect-origin-extent r p v))

(defsetf screen-rect-origin (r) (p)

  "Changing the origin of a rect is interpreted as a translation."

  `(set-screen-rect-origin ,r ,p))

(defsetf screen-rect-extent (r) (v)

  "Changing the extent of a rect is interpreted as a scaling."

  `(set-screen-rect-extent ,r ,v))

;;;------------------------------------------------------------

(defun %copy-screen-rect (r0 r1)
  (declare (type Screen-Rect r0 r1))
  (setf (the Screen-Coordinate (%screen-rect-xmin r1))
    (the Screen-Coordinate (%screen-rect-xmin r0)))
  (setf (the Screen-Coordinate (%screen-rect-ymin r1))
    (the Screen-Coordinate (%screen-rect-ymin r0)))
  (setf (the Positive-Screen-Coordinate (%screen-rect-width r1))
    (the Positive-Screen-Coordinate (%screen-rect-width r0)))
  (setf (the Positive-Screen-Coordinate (%screen-rect-height r1))
    (the Positive-Screen-Coordinate (%screen-rect-height r0)))
  r1)

(defun copy-screen-rect (r &key (result (make-screen-rect)))
  "The copier function for Screen-Rects."
  (declare (type Screen-Rect r result)
	   (:returns result))
  (%copy-screen-rect r result))

;;;------------------------------------------------------------
;;; equivalence tester

(defun %equal-screen-rects? (r0 r1)
  (declare (type Screen-Rect r0 r1))
  (and (= (the Screen-Coordinate (%screen-rect-xmin r0))
	  (the Screen-Coordinate (%screen-rect-xmin r1)))
       (= (the Screen-Coordinate (%screen-rect-ymin r0))
	  (the Screen-Coordinate (%screen-rect-ymin r1)))
       (= (the Positive-Screen-Coordinate (%screen-rect-width r0))
	  (the Positive-Screen-Coordinate (%screen-rect-width r1)))
       (= (the Positive-Screen-Coordinate (%screen-rect-height r0))
	  (the Positive-Screen-Coordinate (%screen-rect-height r1)))))

(defun equal-screen-rects? (r0 r1)
  "The equality predicate for Screen-Rects."
  (declare (type Screen-Rect r0 r1)
	   (:returns (type (Member t nil))))
  (%equal-screen-rects? r0 r1))

 