;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================
;;; Chart-Vectors (two-dimensional)
;;;============================================================
;;; defstruct representation:

(defstruct (Chart-Vector
	    (:conc-name nil)
	    (:constructor %%make-chart-vector)
	    (:copier %Chart-Vector-copier))

  "A Chart-Vector represents displacements on a chart."

  (%chart-vector-x 0.0d0 :type Chart-Coordinate)
  (%chart-vector-y 0.0d0 :type Chart-Coordinate))

(defun %chart-vector? (v) (typep v 'Chart-Vector))

(defun positive-chart-vector? (v)
  (and (typep v 'Chart-Vector)
       (>= (%chart-vector-x v) 0.0d0)
       (>= (%chart-vector-y v) 0.0d0)))

(deftype Positive-Chart-Vector ()

  "A Chart-Vector whose coordinates are positive; it lies in the upper
right quadrant (note contrast with Screen-Vectors)."

  '(satisfies positive-chart-vector?))

;;;-------------------------------------------------------

(defun %make-chart-vector (x y)
  (declare (type Chart-coordinate x y))
  (%%make-chart-vector :%chart-vector-x x :%chart-vector-y y))

(defun make-chart-vector (&key (x 0.0d0) (y 0.0d0))

  "The constructor function for Chart-Vector.

The vector is optionally initialized with the given <x> and <y>
coordinates."

  (declare (type Chart-Coordinate x y)
		    (:returns (type Chart-Vector)))

  (%make-chart-vector x y))

;;;-------------------------------------------------------

(defun chart-vector-x (v)
  "Accessor function for x coordinate."
  (declare (type Chart-Vector v)
		    (:returns (type Chart-Coordinate)))
  (%chart-vector-x v))

(defun chart-vector-y (v)
  "Accessor function for y coordinate."
  (declare (type Chart-Vector v)
		    (:returns (type Chart-Coordinate)))
  (%chart-vector-y v))

(defun set-chart-vector-x (v x)
  (declare (type Chart-Vector v)
		    (type Chart-Coordinate x))
  (setf (%chart-vector-x v) x))

(defun set-chart-vector-y (v y)
  (declare (type Chart-Vector v)
		    (type Chart-Coordinate y))
  (setf (%chart-vector-y v) y))

(defsetf chart-vector-x (v) (x)
  "Sets the vector's x coordinate."
  `(set-chart-vector-x ,v ,x))

(defsetf chart-vector-y (v) (y)
  "Sets the vector's y coordinate."
  `(set-chart-vector-y ,v ,y))

;;;-------------------------------------------------------

(defun %set-chart-vector-coords (v x y)
  (declare (type Chart-Vector v)
	   (type Chart-Coordinate x y))
  (setf (%chart-vector-x v) x)
  (setf (%chart-vector-y v) y)
  v)

(defun set-chart-vector-coords (v &key (x 0.0d0) (y 0.0d0))
  (declare (type Chart-Vector v)
	   (type Chart-Coordinate x y))
  (%set-chart-vector-coords v x y))

;;;-------------------------------------------------------

(defun %copy-chart-vector (v0 v1)
  (declare (type Chart-Vector v0 v1))
  (setf (%chart-vector-x v1) (%chart-vector-x v0))
  (setf (%chart-vector-y v1) (%chart-vector-y v0))
  v1)

(defun copy-chart-vector (v &key (result (make-chart-vector)))
  "The copier function for Chart-Vectors."
  (declare (type Chart-Vector v result)
		    (:returns result))
  (%copy-chart-vector v result))

;;;-------------------------------------------------------

(defun %equal-chart-vectors? (v0 v1)
  (declare (type Chart-Vector v0 v1))
  (and (= (%chart-vector-x v0) (%chart-vector-x v1))
       (= (%chart-vector-y v0) (%chart-vector-y v1))))

(defun equal-chart-vectors? (v0 v1)
  "The equality predicate for Chart-Vectors."
  (declare (type Chart-Vector v0 v1)
		    (:returns (type (Member T Nil))))
  (%equal-chart-vectors? v0 v1))

;;;-------------------------------------------------------

(defun %chart-vector-list? (plist)
  (every #'%chart-vector? plist))

(deftype Chart-Vector-List ()
  "A list of Chart-Vectors."
  '(satisfies %chart-vector-list?))

;;;-------------------------------------------------------
;;; beginnings of Chart-Vector algebra:
;;;-------------------------------------------------------
;;; Note that it's ok for <result> to be <eq> to either
;;; <v0> or <v1>

(defun %linear-mix-chart-vectors (a0 v0 a1 v1 result)
  (declare (type Chart-Coordinate a0 a1)
	   (type Chart-Vector v0 v1 result))
  (setf (%chart-vector-x result) (+ (* a0 (%chart-vector-x v0))
				    (* a1 (%chart-vector-x v1))))
  (setf (%chart-vector-y result) (+ (* a0 (%chart-vector-y v0))
				    (* a1 (%chart-vector-y v1))))
  result)

(defun linear-mix-chart-vectors (a0 v0 a1 v1 &key (result (make-chart-vector)))

  "Computes a linear combination of <v0> and <v1>. Note that it's ok
for <result> to be eq to either <v0> or <v1> (or both)."

  (declare (type Chart-Coordinate a0 a1)
		    (type Chart-Vector v0 v1 result)
		    (:returns result)) 

  (%linear-mix-chart-vectors a0 v0 a1 v1 result))

(defun %add-chart-vectors (v0 v1 result)
  (declare (type Chart-Vector v0 v1 result))
  (setf (%chart-vector-x result) (+ (%chart-vector-x v0) (%chart-vector-x v1)))
  (setf (%chart-vector-y result) (+ (%chart-vector-y v0) (%chart-vector-y v1)))
  result) 

(defun add-chart-vectors (v0 v1 &key (result (make-chart-vector)))

  "Add <v0> and <v1>. Note that it's ok for <result> to be eq to
either <v0> or <v1> (or both)."

  (declare (type Chart-Vector v0 v1 result)
		    (:returns result))
  (%add-chart-vectors v0 v1 result))

(defun %subtract-chart-vectors (v0 v1 result)
  (declare (type Chart-Vector v0 v1 result))
  (setf (%chart-vector-x result) (- (%chart-vector-x v0) (%chart-vector-x v1)))
  (setf (%chart-vector-y result) (- (%chart-vector-y v0) (%chart-vector-y v1)))
  result)

(defun subtract-chart-vectors (v0 v1
			       &key
			       (result (make-chart-vector)))

    "Subtract <v1> from <v0>. Note that it's ok for <result> to be eq
to either <v0> or <v1> (or both)."

  (declare (type Chart-Vector v0 v1 result)
		    (:returns result))
  (%subtract-chart-vectors v0 v1 result))

;;;-------------------------------------------------------

(defun %multiply-chart-vectors (v0 v1 result)
  (declare (type Chart-Vector v0 v1 result))
  (setf (%chart-vector-x result) (* (%chart-vector-x v0) (%chart-vector-x v1)))
  (setf (%chart-vector-y result) (* (%chart-vector-y v0) (%chart-vector-y v1)))
  result)

(defun multiply-chart-vectors (v0 v1
			       &key
			       (result (make-chart-vector)))

  "Computes the element-wise produce of <v0> and <v1>.  Note that it's
ok for <result> to be eq to either <v0> or <v1> (or both). (Why is
this operation defined?)"

  (declare (type Chart-Vector v0 v1 result)
		    (:returns result))
  (%multiply-chart-vectors v0 v1 result))

;;;-------------------------------------------------------

(defun %chart-vector-l2-norm2 (v)
  (declare (type Chart-Vector v))
  (let ((x (%chart-vector-x v))
	(y (%chart-vector-y v)))
    (declare (type Chart-Coordinate x y))
    (+ (* x x) (* y y))))

(defun chart-vector-l2-norm2 (v)
  "Computes the squared L2 norm of <v>."
  (declare (type Chart-Vector v)
		    (:returns (type Positive-Chart-Coordinate)))
  (%chart-vector-l2-norm2 v))

(defun chart-vector-l2-norm (v)
  "Computes the L2 norm of <v>."
  (declare (type Chart-Vector v)
		    (:returns (type Positive-Chart-Coordinate)))
  (sqrt (chart-vector-l2-norm2 v)))

(defun %chart-vector-l1-norm (v)
  (declare (type Chart-Vector v))
  (+ (abs (the Chart-Coordinate (%chart-vector-x v)))
     (abs (the Chart-Coordinate (%chart-vector-y v)))))

(defun chart-vector-l1-norm (v)
  "Computes the L2 norm of <v>."
  (declare (type Chart-Vector v)
		    (:returns (type Positive-Chart-Coordinate)))
  (%chart-vector-l1-norm v))
  
;;;=======================================================
;;; A resource for borrowing temporary Chart-Vector objects:
;;;=======================================================

(defparameter *chart-vector-resource* ())

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %borrow-chart-vector)))

(defun %borrow-chart-vector (x y)
  (declare (special *chart-vector-resource*)
	   (type Chart-Coordinate x y)
	   (:returns (type Chart-Vector)))
  (let ((v (pop *chart-vector-resource*)))
    (cond ((null v)
	   (%make-chart-vector x y))
	  (t
	   (setf (%chart-vector-x v) x)
	   (setf (%chart-vector-y v) y)
	   v))))
 
(defun borrow-chart-vector (&key (x 0.0d0) (y 0.0d0))

  "Get a vector out of the resource, making a new one if necessary."

  (declare (type Chart-Coordinate x y)
		    (:returns (type Chart-Vector)))
  (%borrow-chart-vector x y))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %return-chart-vector)))

(defun %return-chart-vector (v)
  (declare (special *chart-vector-resource*)
	   (type Chart-Vector v))
  (push v *chart-vector-resource*)
  t)

(defun return-chart-vector (v)

  "Return <v> to the resource. This must be done with care, since any
further reference to <v> will be an error."

  (declare (type Chart-Vector v)
		    (:returns t))
  (%return-chart-vector v))

;;;-------------------------------------------------------
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro %with-borrowed-chart-vector ((vname &key (x 0.0d0) (y 0.0d0))
				       &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (%borrow-chart-vector ,x ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (%return-chart-vector ,return-name)))))

(defmacro with-borrowed-chart-vector ((vname
				       &key (x 0.0d0) (y 0.0d0))
				      &body body)


  "This macro binds <vname> to a vector borrowed from the resource,
optionally initialized to the supplied <x> and <y> coordinates, and
returns the point to the resource on exit.  The user should be careful
not to create references to the vector that survive the dynamic extent
of the macro body."

  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name (borrow-chart-vector :x ,x :y ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-chart-vector ,return-name))))) 
 
	
