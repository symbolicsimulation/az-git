;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)
	
;;;============================================================
;;; Screen-Coordinate
;;;============================================================

(deftype Screen-Coordinate ()

  "The Screen-Coordinate type determines the allowed values for
the coordinates of Screen-Vectors, Screen-Points, and Screen-Rects."

  #+:excl `(Signed-Byte 16)
  #-:excl `(Integer #.most-negative-fixnum #.most-positive-fixnum))


(eval-when (compile load eval)
  (proclaim '(Inline
	      %Screen-Coordinate?
	      %Screen-Coordinate-list?
	      %iclip)))

(defun screen-coordinate? (x) (typep x 'Screen-Coordinate))

(defun screen-coordinate-list? (l)
  (and (listp l) (every #'screen-coordinate? l)))

(deftype Screen-Coordinate-List ()
  "A list of Screen-Coordinates."
  `(satisfies screen-coordinate-list?))

(defun screen-coordinate-vector? (l)
  (and (vectorp l) (every #'screen-coordinate? l)))

(deftype Screen-Coordinate-Vector ()
  "A vector (1d Lisp array) of Screen-Coordinates."
  `(satisfies screen-coordinate-vector?))

(deftype Screen-Coordinate-Sequence ()
  "A sequence of Screen-Coordinates."
  `(or Screen-Coordinate-List
       Screen-Coordinate-Vector))

(deftype Positive-Screen-Coordinate ()
  "A non-negative screen coordinate."
  #+:excl `(Unsigned-Byte 16)
  #-:excl `(Integer 0 #.most-positive-fixnum))

(defun positive-screen-coordinate? (x)
  (typep x 'Positive-Screen-Coordinate))

(defun positive-screen-coordinate-list? (l)
  (and (listp l) (every #'Screen-Coordinate? l)))

(deftype positive-screen-coordinate-list ()
  "A list of Positive-Screen-Coordinates."
  `(satisfies Positive-Screen-Coordinate-list?))

;;;=======================================================================

(eval-when (compile load eval)
  (proclaim '(Inline %bound-screen-coordinate)))

(defun %bound-screen-coordinate (xmin x xmax)
  (declare (type Screen-Coordinate xmin x xmax))
  (max xmin (min x xmax)))

(defun bound-screen-coordinate (xmin x xmax)
  "Clip <x> to the closed interval [xmin,xmax]."
  (declare (type Screen-Coordinate xmin x xmax)
		    (:returns (type Screen-Coordinate)))
  (%bound-screen-coordinate xmin x xmax))

;;;=======================================================================
;;; Clip x to the integer interval specified by [start,end),
;;; half open as in the CL sequence functions:

(eval-when (compile load eval)
  (proclaim '(Inline %iclip)))

(defun %iclip (x start end)
  (declare (type Screen-Coordinate x start end))
  (min (max start x) (- end 1)))

(defun iclip (x start end)
  (declare (type Screen-Coordinate x start end))
  (%iclip x start end))

;;; Convert <y> to corresponding index for a coordinate system whose
;;; zero is at <height> - 1  and increases in the reverse direction.
;;; This may sound peculiar, but its the usual case.

#||
(eval-when (compile load eval)
  (proclaim '(Inline %invert-coordinate)))

(defun %invert-coordinate (y height)
  (declare (type Screen-Coordinate y height))
  (- height y 1))

(defun invert-coordinate (y height)
  (declare (type Screen-Coordinate y height))
  (%invert-coordinate y height))
||#