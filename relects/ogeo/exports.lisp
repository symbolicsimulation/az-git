;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)


(eval-when (compile load eval)
  (declaim (declaration :returns))
  (export '( ;; discrete, integer, screen geometry entities
	    Screen-Coordinate
	    Screen-Coordinate-List
	    Positive-Screen-Coordinate
	    Positive-Screen-Coordinate-List
	    bound-screen-coordinate
	    ;; iclip

	    ;; screen-vectors: Linear Vectors, elements of R2, displacements
	    Screen-Vector
	    Positive-Screen-Vector
	    Screen-Vector-List
	    Positive-Screen-Vector-List
	    make-screen-vector
	    copy-screen-vector
	    equal-screen-vectors?
	    screen-vector-x screen-vector-y
	    ;; set-screen-vector-coords
	    borrow-screen-vector
	    return-screen-vector
	    with-borrowed-screen-vector
	    linear-mixscreen-vectors
	    add-screen-vectors
	    subtract-screen-vectors
	    screen-vector-l2-norm
	    screen-vector-l2-norm2
	    screen-vector-l1-norm

	    ;; screen-points: Affine Points, elements of E2, locations
	    Screen-Point
	    Screen-Point-List
	    make-screen-point
	    copy-screen-point
	    equal-screen-points?
	    screen-point-x screen-point-y
	    ;; set-screen-point-coords
	    borrow-screen-point
	    return-screen-point
	    with-borrowed-screen-point
	    ;;borrow-screen-point-list
	    ;;return-screen-point-list
	    ;;with-borrowed-screen-point-list
	    ;;with-borrowed-screen-points
	    ;;point-list->xy-list
	    ;;xy-list->point-list
	    affine-mix-screen-points
	    move-screen-point
	    subtract-screen-points
	    affine-mix-screen-points
	
	    ;; screen-rects: axis parallel reactangles
	    Screen-Rect
	    Screen-Rect-List 
	    make-screen-rect
	    copy-screen-rect
	    equal-screen-rects?
	    screen-rect-xmin
	    screen-rect-xmax
	    screen-rect-ymin
	    screen-rect-ymax
	    screen-rect-width
	    screen-rect-height
	    screen-rect-left
	    screen-rect-inner-left
	    screen-rect-outer-left
	    screen-rect-right
	    screen-rect-inner-right
	    screen-rect-outer-right
	    screen-rect-top
	    screen-rect-inner-top
	    screen-rect-outer-top
	    screen-rect-bottom
	    screen-rect-inner-bottom 
	    screen-rect-outer-bottom
	    screen-rect-origin
	    screen-rect-extent
	    ;; screen-rect-origin-extent
	    ;; set-screen-rect-coords
	    borrow-screen-rect
	    return-screen-rect
	    with-borrowed-screen-rect

	    Screen-Object
	    keep-screen-rect-in
	    sub-screen-rect?
	    screen-rect-covering-rects
	    screen-rect-intersect?
	    intersect-2-screen-rects
	    intersect-screen-rects 
	    screen-point-in-rect?
	    screen-point-in-rect-xy?
	    screen-points-in-rect?
	    extend-screen-rect-to-rect
	    extend-screen-rect-to-point
	    screen-rect-min-l2-dist2
	    screen-rect-min-l2-dist
	    screen-distance-to-line
	    screen-rect-covering-points
	    clip-screen-line
	    ;; clip-screen-rect-specs

	    ;; discrete, integer, screen geometry entities
	    Chart-Coordinate
	    Chart-Coordinate-List
	    Positive-Chart-Coordinate
	    Positive-Chart-Coordinate-List
	    Chart-Object
	    sub-chart-rect?
	    chart-rect-covering-rects
	    chart-rect-intersect?
	    intersect-2-chart-rects
	    intersect-chart-rects 
	    chart-point-in-rect?
	    ;;chart-point-in-rect-xy?
	    chart-points-in-rect?
	    chart-rect-covering-points
	    extend-chart-rect-to-rect
	    extend-chart-rect-to-point
	    chart-rect-enclosing-circle
	    chart-rect-min-l2-dist2
	    chart-rect-min-l2-dist
	    distance-to-line 
	    clip-line
	    ;; clip-chart-rect-specs
	    ;; sine-wave

	    ;; Chart-Points: Affine Points, elements of E2, locations
	    Chart-Point
	    Chart-Point-List
	    make-chart-point
	    copy-chart-point
	    equal-chart-points?
	    chart-point-x chart-point-y
	    ;; set-chart-point-coords
	    borrow-chart-point
	    return-chart-point
	    with-borrowed-chart-point
	    ;;with-borrowed-chart-points
	    ;;point-list->xy-list
	    ;;xy-list->point-list
	    move-chart-point
	    affine-mix-chart-points
	    subtract-chart-points

	    ;; Chart-Vectors: Linear Vectors, elements of R2, displacements
	    Chart-Vector
	    Positive-Chart-Vector
	    Chart-Vector-List
	    make-chart-vector
	    copy-chart-vector
	    equal-chart-vectors?
	    chart-vector-x chart-vector-y
	    ;;set-chart-vector-coords
	    borrow-chart-vector
	    return-chart-vector
	    with-borrowed-chart-vector
	    linear-mix-chart-vectors
	    add-chart-vectors
	    subtract-chart-vectors
	    chart-vector-l2-norm
	    chart-vector-l2-norm2
	    chart-vector-l1-norm

	    ;; Chart-rects: axis parallel reactangles
	    Chart-Rect
	    Chart-Rect-List 
	    make-chart-rect
	    copy-chart-rect
	    equal-chart-rects?
	    chart-rect-xmin
	    chart-rect-xmax
	    chart-rect-ymin
	    chart-rect-ymax
	    chart-rect-left
	    chart-rect-top
	    chart-rect-width
	    chart-rect-height
	    chart-rect-right
	    chart-rect-bottom
	    chart-rect-origin
	    chart-rect-extent
	    ;; chart-rect-origin-extent
	    ;; set-chart-rect-origin-extent
	    chart-rect-center
	    ;; set-chart-rect-coords
	    borrow-chart-rect
	    return-chart-rect
	    with-borrowed-chart-rect

	    Tics
	    tic-min
	    tic-max
	    tic-inc
	    tic-n
	    tic-values
	    tic-labels
	    tic-length
	    get-nice-tics
	    nice-chart-rect

	    Diagonal-C<->S-Map
	    Diagonal-C->S-Map
	    ;; make-diagonal-c->s-map
	    Diagonal-S->C-Map 
	    ;; make-diagonal-s->c-map
	    make-affine-map-between
	    ;; chart-origin dlinear-map screen-origin
	    ;; c<->s-check-type-match
	    ;; c<->s-make-matching-object
	    ;; transform-c->s-x-coordinate
	    ;; transform-c->s-y-coordinate
	    transform
	    inverse
	    )))

