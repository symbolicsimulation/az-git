;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================

(deftype Chart-Object ()
  "A Chart-Object is one of Chart-Vector, Chart-Point, or Chart-Rect."
  '(or Chart-Vector Chart-Point Chart-Rect))

;;;------------------------------------------------------------

(defun %sub-chart-rect? (smaller larger)
  (declare (type Chart-Rect smaller larger))
  (and (<= (the Chart-Coordinate (%chart-rect-xmin larger))
	   (the Chart-Coordinate (%chart-rect-xmin smaller))
	   (the Chart-Coordinate (%chart-rect-xmax smaller))
	   (the Chart-Coordinate (%chart-rect-xmax larger)))
       (<= (the Chart-Coordinate (%chart-rect-ymin larger))
	   (the Chart-Coordinate (%chart-rect-ymin smaller))
	   (the Chart-Coordinate (%chart-rect-ymax smaller))
	   (the Chart-Coordinate (%chart-rect-ymax larger)))))

(defun sub-chart-rect? (smaller larger)
  "Test if <smaller> is inside <larger>."
  (declare (type Chart-Rect smaller larger)
		    (:returns (type (Member t nil))))
  (%sub-chart-rect? smaller larger))

(eval-when (compile load eval)
  (proclaim '(Inline %chart-point-in-rect-xy?)))

(defun %chart-point-in-rect-xy? (x y r)
  (declare (optimize (safety 0) (speed 3))
	   (type Chart-Coordinate x y)
	   (type Chart-Rect r))
  (and (<= (the Chart-Coordinate (%chart-rect-xmin r))
	   x
	   (the Chart-Coordinate (%chart-rect-xmax r)))
       (<= (the Chart-Coordinate (%chart-rect-ymin r))
	   y
	   (the Chart-Coordinate (%chart-rect-ymax r)))))

(defun chart-point-in-rect-xy? (x y r)
  (declare (type Chart-Coordinate x y)
		    (type Chart-Rect r)
		    (:returns (type (Member t nil))))
  (%chart-point-in-rect-xy? x y r))

(defun %chart-point-in-rect? (p r)
  (declare (optimize (safety 0) (speed 3))
	   (inline %chart-point-in-rect-xy?)
	   (type Chart-Point p)
	   (type Chart-Rect r))
  (%chart-point-in-rect-xy?
    (%chart-point-x p)
    (%chart-point-y p)
    r))

(defun chart-point-in-rect? (p r)
  "Is <p> in <r>?"
  (declare (type Chart-Point p)
		    (type Chart-Rect r)
		    (:returns (type (Member t nil))))
  (%chart-point-in-rect? p r))

(defun %chart-points-in-rect? (ps r)
  (declare (optimize (safety 0) (speed 3))
	   (inline %chart-point-in-rect?)
	   (type Chart-Point-List ps)
	   (type Chart-Rect r))
  (let* ((xmin (%chart-rect-xmin r))
	 (xmax (+ xmin (the Chart-Coordinate (%chart-rect-width r))))
	 (ymin (%chart-rect-ymin r))
	 (ymax (+ ymin (the Chart-Coordinate (%chart-rect-height r)))))
    (declare (type Chart-Coordinate xmin xmax ymin ymax))
    (dolist (p ps)
      (declare (type Chart-Point p))
      (let ((x (%chart-point-x p))
	    (y (%chart-point-y p)))
	(declare (type Chart-Coordinate x y))
	(unless (and (<= xmin x)
		     (<= x xmax)
		     (<= ymin y)
		     (<= y ymax))
	  (return-from %chart-points-in-rect? nil)))))
  t)
    
(defun chart-points-in-rect? (ps r)
  "Are all the points in the list <ps> in the rect <r>?"
  (declare (type Chart-Point-List ps)
		    (type Chart-Rect r)
		    (:returns (type (Member t nil))))
  (%chart-points-in-rect? ps r))

;;;-----------------------------------------------------------------

(defun %chart-rect-intersect? (r0 r1)
  (declare (type Chart-Rect r0 r1))
  (not
    (or
      (let ((dl (- (the Chart-Coordinate (%chart-rect-xmin r1))
		   (the Chart-Coordinate (%chart-rect-xmin r0)))))
	(declare (type Chart-Coordinate dl))
	(or
	  (>= dl (the Positive-Chart-Coordinate (%chart-rect-width r0)))
	  (<= dl (- (the Positive-Chart-Coordinate (%chart-rect-width r1))))))
      (let ((dt (- (the Chart-Coordinate (%chart-rect-ymin r1))
		   (the Chart-Coordinate (%chart-rect-ymin r0)))))
	(declare (type Chart-Coordinate dt))
	(or
	  (>= dt (the Positive-Chart-Coordinate (%chart-rect-height r0)))
	  (<= dt (- (the Positive-Chart-Coordinate (%chart-rect-height r1))))
	  )))))

(defun chart-rect-intersect? (r0 r1)

  "Test whether two rects intersect."

  (declare (type Chart-Rect r0 r1)
		    (:returns (type (Member t nil))))
  (%chart-rect-intersect? r0 r1))

;;;------------------------------------------------------------

(defun %intersect-2-chart-rects (r0 r1 result)
  
  "Note that it's ok for <result> to be <r0> or r1>."
  
  (let* ((x0 (%chart-rect-xmin r0))
	 (y0 (%chart-rect-ymin r0))
	 (x1 (%chart-rect-xmin r1))
	 (y1 (%chart-rect-ymin r1))
	 (x (max x0 x1))
	 (y (min y0 y1))
	 (w (- (min (+ (%chart-rect-width r0) x0)
		    (+ (%chart-rect-width r1) x1))
	       x))
	 (h (+ (max (- y0 (%chart-rect-height r0))
		    (- y1 (%chart-rect-height r1)))
	       y)))
    (declare (type Chart-Coordinate x0 y0 x1 y1 x y w h))
    (if (or (minusp w) (minusp h))
	;; then no intersection
	nil
	;; else return the intersection in <result>
	(%set-chart-rect-coords result x y w h))))

(defun intersect-2-chart-rects (r0 r1
				&key
				(result (make-chart-rect)))

  "Returns the rect that is the intersection.
Note that it's ok for <result> to be <r0> or r1>."

  (declare (type Chart-Rect r0 r1 result)
		    (:returns result))
  (%intersect-2-chart-rects r0 r1 result))

;;;------------------------------------------------------------

(defun intersect-chart-rects (rs
			      &key
			      (result (make-chart-rect)))

  "Returns the rect that is the intersection.
Note that it's ok for <result> to be a member of <rs>."

  (declare (type Chart-Rect-List rs)
		    (type Chart-Rect result)
		    (:returns result))
  (let* ((r0 (car rs))
         (xmin (%chart-rect-xmin r0))
         (ymin (%chart-rect-ymin r0))
         (xmax (%chart-rect-xmax r0))
         (ymax (%chart-rect-ymax r0)))
    (declare (type Chart-Rect r0)
	     (type Chart-Coordinate xmin ymin xmax ymax))
    (dolist (r (cdr rs))
      (declare (type Chart-Rect r))
      (let* ((r-xmin (%chart-rect-xmin r))
             (r-ymin (%chart-rect-ymin r))
             (r-xmax (%chart-rect-xmax r))
             (r-ymax (%chart-rect-ymax r)))
	(declare (type Chart-Coordinate r-xmin r-ymin r-xmax r-ymax))
	(az:maxf xmin xmin r-xmin)
        (az:minf xmax r-xmax)
	(when (< xmax xmin) ;; < allows empty rects with position
	  ;; intersection is empty and has no location!!
	  (return-from intersect-chart-rects nil))
        (az:maxf ymin r-ymin)
	(az:minf ymax r-ymax)
	(when (< ymax ymin) ;; < allows empty rects with position
	  ;; intersection is empty and has no location!
	  (return-from intersect-chart-rects nil))))
    ;; Intersection has at least a location, even if it covers no
    ;; pixels.
    (%set-chart-rect-coords result xmin ymin (- xmax xmin) (- ymax ymin))
    result))

;;;------------------------------------------------------------

(defun chart-rect-covering-rects (rs
				   &key
				   (result (make-chart-rect)))

  "Returns the smallest rect containing all rects in a list."

  (declare (type Chart-Rect-List rs)
		    (type Chart-Rect result)
		    (:returns result))

  (let* ((r0 (car rs))
         (xmin (%chart-rect-xmin r0))
         (ymin (%chart-rect-ymin r0))
         (xmax (%chart-rect-xmax r0))
         (ymax (%chart-rect-ymax r0)))
    (declare (type Chart-Rect r0)
	     (type Chart-Coordinate xmin ymin xmax ymax))
    (dolist (r (cdr rs))
      (declare (type Chart-Rect r))
      (let* ((r-xmin (%chart-rect-xmin r))
             (r-ymin (%chart-rect-ymin r))
             (r-xmax (%chart-rect-xmax r))
             (r-ymax (%chart-rect-ymax r)))
	(declare (type Chart-Coordinate r-xmin r-ymin r-xmax r-ymax))
	(setf xmin (min xmin r-xmin))
        (setf ymin (min ymin r-ymin))
        (setf xmax (max xmax r-xmax))
        (setf ymax (max ymax r-ymax))))
    (%set-chart-rect-coords result xmin ymin (- xmax xmin) (- ymax ymin))
    result))

;;;------------------------------------------------------------

(defun %chart-rect-covering-points (ps result)
  (declare (optimize (safety 0) (speed 3))
	   (type Chart-Point-List ps)
	   (type Chart-Rect result)
	   (inline %set-chart-rect-coords))
  (let* ((p0 (car ps))
	 (min-x (%chart-point-x p0))
	 (max-x min-x)
	 (min-y (%chart-point-y p0))
	 (max-y min-y))
    (declare (type Chart-Point p0)
	     (type Chart-Coordinate min-x max-x min-y max-y))
    (dolist (p (cdr ps))
      (declare (type Chart-Point p))
      (let ((x (%chart-point-x p))
	    (y (%chart-point-y p)))
	(declare (type Chart-Coordinate x y))
	(if (< x min-x)
	    (setf min-x x)
	    (when (> x max-x) (setf max-x x)))
	(if (< y min-y)
	    (setf min-y y)
	    (when (> y max-y) (setf max-y y)))))
    (%set-chart-rect-coords
     result min-x min-y
     (the Chart-Coordinate (- max-x min-x))
     (the Chart-Coordinate (- max-y min-y)))
    result))

(defun chart-rect-covering-points (ps &key (result (make-chart-rect)))

  "Returns the smallest rect containing all points in a list."

  (declare (type Chart-Point-List ps)
		    (type Chart-Rect result)
		    (:returns result))

  (%chart-rect-covering-points ps result))

;;;------------------------------------------------------------

(defun extend-chart-rect-to-point-xy (r x y)

  "Destructively modifies <r> to include the point at <x>,<y>."

  (declare (type Chart-Rect r)
		    (type Chart-Coordinate x y)
		    (:returns r))

  (let* (;; some CL's need this for the declaration to work
	 (r r)
	 (xmin (%chart-rect-xmin r))
         (ymin (%chart-rect-ymin r))
	 (xmax (%chart-rect-xmax r))
	 (ymax (%chart-rect-ymax r)))
    (declare (type Chart-Rect r)
	     (type Chart-Coordinate xmin ymin xmax ymax))
    ;; This uses the fact that Chart-Rects are represented by xmin, ymin,
    ;; width, height
    (cond ((< x xmin)
           (setf (%chart-rect-xmin r) x)
           (setf (%chart-rect-width r) (- xmax x)))
          ((> x xmax)
           (setf (%chart-rect-width r) (- x xmin))))
    (cond ((< y ymin)
	   (setf (%chart-rect-ymin r) y)
	   (setf (%chart-rect-height r) (- ymax y)))
	  ((> y ymax)
	   (setf (%chart-rect-height r) (- y ymin))))
    r))

(defun extend-chart-rect-to-point (r p &key (result (make-chart-rect)))

  "Computes the smallest rect that includes both <r> and <p>.
Note that it's ok for <r> to be eq to <result>."

  (declare (type Chart-Rect r result)
		    (type Chart-Point p)
		    (:returns result))
  (when (not (eq r result))
    (copy-chart-rect r :result result))
  (extend-chart-rect-to-point-xy
    result (chart-point-x p) (chart-point-y p))
  result)

;;;------------------------------------------------------------

(defun extend-chart-rect-to-rect (r0 r1 &key (result (make-chart-rect)))
  "Computes the smallest rect that includes both <r0> and <r1>.
Note that it's ok for <r0> and/or <r1> to be eq to <result>."
  (declare (type Chart-Rect r0 r1 result)
		    (:returns result))
  (when (not (eq r0 result))
    (copy-chart-rect r0 :result result))
  (extend-chart-rect-to-point-xy
   result (%chart-rect-xmin r1) (%chart-rect-ymin r1))
  (extend-chart-rect-to-point-xy
   result
   (+ (%chart-rect-xmin r1) (%chart-rect-width r1))
   (+ (%chart-rect-ymin r1) (%chart-rect-height r1)))
  result)

;;;-----------------------------------------------------------------
;;; 

(defun chart-rect-min-l2-dist2 (r0 r1)
  "The L2 distance (squared) between the closest two points of the two rects."
  (declare (type Chart-Rect r0 r1)
		    (:returns (type Positive-Chart-Coordinate)))
  (let ((xmin0 (%chart-rect-xmin r0))
	(xmax0 (%chart-rect-xmax r0))
	(ymin0 (%chart-rect-ymin r0))
	(ymax0 (%chart-rect-ymax r0))
	(xmin1 (%chart-rect-xmin r1))
	(xmax1 (%chart-rect-xmax r1))
	(ymin1 (%chart-rect-ymin r1))
	(ymax1 (%chart-rect-ymax r1))
	(dist 0.0d0))
    (declare (type Chart-coordinate
		   xmin0 xmax0 ymin0 ymax0 xmin1 xmax1 ymin1 ymax1)
	     (type Positive-Chart-Coordinate dist))
    ;; perform reflections as needed so that r0 is below and xmin of r1
    (when (> xmin0 xmin1)
      (psetf xmin0 (- xmax0)
	     xmax0 (- xmin0))
      (psetf xmin1 (- xmax1)
	     xmax1 (- xmin1)))
    (when (> ymin0 ymin1)
      (psetf ymin0 (- ymax0)
	     ymax0 (- ymin0))
      (psetf ymin1 (- ymax1)
	     ymax1 (- ymin1)))
    ;; calculate distance
    (when (< xmax0 xmin1)
      (let ((dx (- xmin1 xmax0)))
	(declare (type Chart-Coordinate dx))
	(incf dist (* dx dx))))
    (when (< ymax0 ymin1)
      (let ((dy (- ymin1 ymax0)))
	(declare (type Chart-Coordinate dy))
	(incf dist (* dy dy))))
    dist))

(defun chart-rect-min-l2-dist (r0 r1)
  "The L2 distance between the closest two points of the two rects."
  (declare (type Chart-Rect r0 r1)
		    (:returns (type Positive-Chart-Coordinate)))
  (sqrt (chart-rect-min-l2-dist2 r0 r1))) 

;;;------------------------------------------------------- 

(defun %chart-rect-enclosing-circle (center radius result)
  (let ((diameter (* 2.0d0 radius)))
    (%set-chart-rect-coords result
			    (- (%chart-point-x center) radius)
			    (- (%chart-point-y center) radius)
			    diameter diameter)))

(defun chart-rect-enclosing-circle (center radius
				    &key
				    (result
				     (%make-chart-rect
				      0.0d0 0.0d0 0.0d0 0.0d0)))
  "The smallest rect containing the circle defined by <center> and <radius>."
  (declare (type Chart-Point center)
		    (type Chart-Coordinate radius)
		    (type Chart-Rect result)
		    (:returns result))
  (%chart-rect-enclosing-circle center radius result))

;;;============================================================
;;; Useful Utilities
;;;============================================================

(defun distance-to-line (p lp0 lp1)

  "Given a point, <p>, and the two endpoints of a line segment, return
the distance from the point to the line segement.  If the two
endpoints are close together, return the distance to the first one.
The code tries to avoid expensive operations."

  (declare (type Chart-Point p lp0 lp1)
		    (:returns (type Positive-Chart-Coordinate)))

  (let* ((x (chart-point-x p))
         (y (chart-point-y p))
         (x1 (chart-point-x lp0))
         (y1 (chart-point-y lp0))
         (x2 (chart-point-x lp1))
         (y2 (chart-point-y lp1))
         (px1 (- x x1))
         (py1 (- y y1))
         (px2 (- x x2))
         (py2 (- y y2))
         (dx (- x2 x1))
         (dy (- y2 y1))
         (line-length-squared (+ (* dx dx) (* dy dy)))
         (dist-to-lp0-squared (+ (* px1 px1) (* py1 py1))))
    (sqrt (if (< line-length-squared 0.01d0)
	      dist-to-lp0-squared
	    (let* ((line-length (sqrt line-length-squared))
		   (dx-normalized (/ dx line-length))
		   (dy-normalized (/ dy line-length))
		   (proj-length (+ (* dx-normalized px1)
				   (* dy-normalized py1)))
		   (proj-length-squared (* proj-length proj-length))
		   (dist-to-line-squared
		    (max 0.0d0 (- dist-to-lp0-squared
				  proj-length-squared)))
		   (dist-to-lp1-squared (+ (* px2 px2) (* py2 py2))))
	      (cond ((< proj-length 0.0d0) dist-to-lp0-squared)
		    ((> proj-length line-length) dist-to-lp1-squared)
		    (t dist-to-line-squared)))))))

;;;============================================================
;;; Clipping
;;;============================================================

(defun chart-clip-code (x y xmin xmax ymin ymax)       
  "Returns Cohen-Sutherland clip code for point (x,y).
   This version assumes that xmin < xmax, etc."
  (declare (type Chart-Coordinate x y xmin xmax ymin ymax)
	   (:returns (type (Integer 0 15))))
  (logior (cond ((> y ymax) 8) ((> ymin y) 4) (t 0))
	  (cond ((> x xmax) 2) ((> xmin x) 1) (t 0))))

(defun chart-clip-line (x0 y0 x1 y1 clipping-rect)

  "Clip line against <clipping-rect>.  Implements Cohen-Sutherland
clipping.  From Foley and Van Dam, pg.146 returns four values: new
<x0>, <y0>, <x1>, <y1> of line clipped to <clipping-rect> if it
returns nil, the line is outside <clipping-rect> note: the direction
of the line is maintained (from <x0>,<y0> to <x1>,<y1>)"

  (declare (type Chart-Coordinate x0 y0 x1 y1)
	   (type Chart-Rect clipping-rect)
	   (:returns (values (type (or Null Chart-Coordinate) x0)
			     (type (or Null Chart-Coordinate) y0)
			     (type (or Null Chart-Coordinate) x1)
			     (type (or Null Chart-Coordinate) y1))))

  (let* ((xmin (%chart-rect-xmin clipping-rect))
	 (ymin (%chart-rect-ymin clipping-rect))
	 (xmax (%chart-rect-xmax clipping-rect))
	 (ymax (%chart-rect-ymax clipping-rect))
	 (accept-p nil) (done-p nil) (reversed-p nil)
	 (code0 0) (code1 0))
    (declare (type Chart-Coordinate xmin xmax ymin ymax)
	     (type (Member t nil) accept-p done-p reversed-p)
	     (type (Integer 0 8) code0 code1))
    (loop
      (when done-p (return nil))
      (setf code0 (chart-clip-code x0 y0 xmin xmax ymin ymax))
      (setf code1 (chart-clip-code x1 y1 xmin xmax ymin ymax))
      (if (= 0 (logand code0 code1))
	  ;; Possible accept
	  (if (setf accept-p (= 0 (logior code0 code1)))
	      (setf done-p t) ;; accept
	    (progn ;; Find intersections
	      (when (= 0 code0)
		;; Swap points so (X0 Y0) is guaranteed to be outside 
		(rotatef x0 x1) (rotatef y0 y1)
		(rotatef code0 code1) (setf reversed-p t))
	      (cond
	       ((/= 0 (logand code0 8)) ;; divide line at ymax
		(incf x0 (/ (* (- x1 x0) (- ymax y0)) (- y1 y0)))
		(setf y0 ymax))
	       ((not (= 0 (logand code0 4)))
		;; divide line at ymin
		(incf x0 (/ (* (- x1 x0) (- ymin y0)) (- y1 y0)))
		(setf y0 ymin))
	       ((/= 0 (logand code0 2)) ;; divide line at xmax
		(incf y0 (/ (* (- y1 y0) (- xmax x0)) (- x1 x0)))
		(setf x0 xmax))
	       (t ;; divide line at xmin
		(incf y0 (/ (* (- y1 y0) (- xmin x0)) (- x1 x0)))
		(setf x0 xmin)))))
	;; Reject
	(setf done-p t)))
    (when reversed-p ;; reverse it back
      (rotatef x0 x1)
      (rotatef y0 y1))
    (if accept-p
	(values x0 y0 x1 y1)
      (values nil))))
  
;;;------------------------------------------------------------

(defun clip-chart-rect-specs (xmin ymin width height clipping-chart-rect)
  ;; clip Chart-Rect specs to clipping Chart-Rect
  ;; (null clipping-chart-rect means no clipping)
  (when clipping-chart-rect
    (let ((clip-xmin (%chart-rect-xmin clipping-chart-rect))
	  (clip-ymin (%chart-rect-ymin clipping-chart-rect))
	  (clip-width (%chart-rect-width clipping-chart-rect))
	  (clip-height (%chart-rect-height clipping-chart-rect)))
      (when (> clip-xmin xmin)
	(setf width (- width (- clip-xmin xmin)))
	(setf xmin clip-xmin))
      (when (> clip-ymin ymin)
	(setf height (- height (- clip-ymin ymin)))
	(setf ymin clip-ymin))
      (when (> (+ xmin width) (+ clip-xmin clip-width))
	(setf width (- (+ clip-xmin clip-width) xmin)))
      (when (> (+ ymin height) (+ clip-ymin clip-height))
	(setf height (- (+ clip-ymin clip-height) ymin)))))
  (values xmin ymin width height))

;;;============================================================

(defun make-random-chart-vector (enclosing-rect)
  "Creates a (uniform) random vector in <enclosing-rect>."
  (declare (type Chart-Rect enclosing-rect)
		    (:returns (type Chart-Vector)))
  (%make-chart-vector (random (%chart-rect-width enclosing-rect))
	     (random (%chart-rect-height enclosing-rect))))

(defun make-random-chart-point (enclosing-rect)
  "Creates a (uniform) random point in <enclosing-rect>."
  (declare (type Chart-Rect enclosing-rect)
		    (:returns (type Chart-Point)))
  (%make-chart-point
    (+ (%chart-rect-xmin enclosing-rect)
       (random (%chart-rect-width enclosing-rect)))
    (+ (%chart-rect-ymin enclosing-rect)
       (random (%chart-rect-height enclosing-rect)))))

(defun make-random-chart-points (enclosing-rect n)
  "Creates a list of <n> (uniform) random points in <enclosing-rect>."
  (declare (type Chart-Rect enclosing-rect)
		    (:returns (type Chart-Point-List)))
  (let ((ps ()))
    (dotimes (i n) (push (make-random-chart-point enclosing-rect) ps))
    ps))

(defun make-random-chart-rect (enclosing-rect)
  "Creates a (uniform) random rect in <enclosing-rect>."
  (declare (type Chart-Rect enclosing-rect)
		    (:returns (type Chart-Rect)))
  (let ((ps (make-random-chart-points enclosing-rect 2)))
    (chart-rect-covering-points ps)))

(defun make-random-chart-rects (enclosing-rect n)
  "Creates a list of <n> (uniform) random rects in <enclosing-rect>."
  (declare (type Chart-Rect enclosing-rect)
		    (:returns (type Chart-Rect-List))) 
  (let ((rects ()))
    (dotimes (i n) (push (make-random-chart-rect enclosing-rect) rects))
    rects))