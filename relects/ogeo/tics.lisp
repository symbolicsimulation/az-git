;;;-*- Package: :Geometry; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

(deftype Positive-Fixnum ()
  "A convenient type to have around."
  '(Integer 0 #.most-positive-fixnum))

;;;============================================================
;;; Implementation of the Tics abstract type:

(defstruct (Tics (:conc-name ""))

  "A data structure describing the placement of tic marks on a chart."

  (%tic-min 0.0d0 :type Chart-Coordinate)
  (%tic-max 1.0d0 :type Chart-Coordinate)
  (%tic-inc 0.1d0 :type  Positive-Chart-Coordinate)
  (%tic-n  0 :type Positive-Fixnum)
  (%tic-values () :type List)
  (%tic-labels () :type List))

(defun tics? (x) (typep x 'Tics))

(defun equal-tics? (t0 t1)
  "The equality predicate for Tics."
  (declare (type Tics t0 t1)
		    (:returns (type (Member t nil))))
  (and (= (%tic-min t0) (%tic-min t1))
       (= (%tic-max t0) (%tic-max t1))
       (= (%tic-inc t0) (%tic-inc t1))
       (= (%tic-n t0) (%tic-n t1))
       (every #'= (%tic-values t0) (%tic-values t1))
       (every #'string-equal (%tic-labels t0) (%tic-labels t1))))

;;;------------------------------------------------------------
;;; Tics are immutable

(defun tic-min (tics)
  "The value of the minimum tic mark."
  (declare (type Tics tics)
		    (:returns (type Chart-Coordinate)))
  (%tic-min Tics))

#||
(defsetf tic-min (tics) (x)
  "Tics are immutable, so using this setf is an error."
  tics x
  (error "Tics are immutable."))
||#

(defun tic-max (tics)
  "The value of the maximum tic mark."
  (declare (type Tics tics)
		    (:returns (type Chart-Coordinate)))
  (%tic-max Tics)) 

#||
(defsetf tic-max (tics) (x)
  "Tics are immutable, so using this setf is an error."
  tics x
  (error "Tics are immutable."))
||#

(defun tic-inc (tics)
  "The increment between tics."
  (declare (type Tics tics)
		    (:returns (type Positive-Chart-Coordinate)))
  (%tic-inc Tics)) 

#||
(defsetf tic-inc (tics) (x)
  "Tics are immutable, so using this setf is an error."
  tics x
  (error "Tics are immutable."))
||#

(defun tic-n (tics)
  "The number of tics."
  (declare (type Tics tics)
		    (:returns (type Positive-Fixnum)))
  (%tic-n Tics)) 

#||
(defsetf tic-n (tics) (x)
  "Tics are immutable, so using this setf is an error."
  tics x
  (error "Tics are immutable."))
||#

(defun tic-values (tics)
  "Returns a list of the values of the tic marks."
  (declare (type Tics tics)
		    (:returns (type Chart-Coordinate-List)))
  (%tic-values Tics)) 

#||
(defsetf tic-values (tics) (x)
  "Tics are immutable, so using this setf is an error."
  tics x
  (error "Tics are immutable."))
||#

(defun tic-labels (tics)
  "Returns a list of strings holding the printed representation of the
corresponding <tic-values>. This is defined for performance reasons,
so the user does not need to repeatedly call format on the same floating
point numbers."
  (declare (type Tics tics)
		    (:returns (type (List String))))
  (%tic-labels Tics)) 

#||
(defsetf tic-labels (tics) (x)
  "Tics are immutable, so using this setf is an error."
  tics x
  (error "Tics are immutable."))
||#


;;;------------------------------------------------------------

(declaim (inline %tic-length))
	   
(defun %tic-length (ti)
  (declare (optimize (safety 0) (speed 3))
	   (type Tics ti))
  (- (%tic-max ti) (%tic-min ti)))

(defun tic-length (ti)
  "Returns the length of the interval covered by the tics."
  (declare (type Tics ti)
		    (:returns (type Positive-Chart-Coordinate)))
  (%tic-length ti))

;;;============================================================

(defun get-nice-tics (min max &key (tics '(3 4 5 6 7 8)))

  "Compute a set of tic marks to cover the interval <min>, <max>."

  (declare (type Number min max)
		    (type List tics)
		    (:returns (type Tics)))

  (assert (<= min max))
  (assert (every #'(lambda (x) (typep x 'Fixnum)) tics))
  (locally
      (declare (optimize (safety 0) (speed 3))
	       (type Number min max)
	       (type List tics))
    (let ((dmin (coerce min 'Chart-Coordinate))
	  (dmax (coerce max 'Chart-Coordinate)))
      (declare (type Chart-Coordinate dmin dmax))
      (when (<= (- dmax dmin) #.(sqrt Double-Float-epsilon))
	(decf dmin #.(* 0.5d0 (sqrt Double-Float-epsilon)))
	(incf dmax #.(* 0.5d0 (sqrt Double-Float-epsilon))))
      (let* ((shortest (scale-tics dmin dmax (first tics)))
	     (shortest-length (%tic-length shortest)))
	(declare (type Tics shortest)
		 (type Chart-Coordinate shortest-length))
	(dolist (n-tics (rest tics))
	  (declare (type Fixnum n-tics))
	  (let* ((current (scale-tics dmin max n-tics))
		 (current-length (%tic-length current)))
	    (declare (type Tics current)
		     (type Chart-Coordinate current-length))
	    (when (< current-length shortest-length)
	      (setf shortest-length current-length)
	      (setf shortest current))))
	(setf (%tic-values shortest)
	  (let ((inc (%tic-inc shortest))
		(smax (%tic-max shortest))
		(x (%tic-min shortest)))
	    (declare (type Chart-Coordinate inc smax x))
	    (nconc (az:with-collection
		       (dotimes (i (- (%tic-n shortest) 1))
			 (declare (type Fixnum i))
			 (az:collect x)
			 (incf x inc)))
		   (list smax))))
	(setf (%tic-labels shortest)
	  (mapcar #'float-to-string (%tic-values shortest)))
	shortest))))

;;;============================================================
;;; We seem to be spending a lot of time in <float-to-string>,
;;; so I memo-ized it. It needs to be a little more complicated
;;; if we want to allow for multiple strings (with fonts) associated
;;; with each number.

(defun %float-to-string (x)
  (setf x (az:fl x))
  (string-left-trim
    '(#\Space)
    (if (let ((abs-x (abs x)))
	  (and (not (zerop abs-x))
	       (or (< abs-x 0.001d0)
		   (>= abs-x 1.0d7))))
	(format nil "~10e" x)
	(if (zerop x) "0.0" (format nil "~7f" x)))))

;;; this isn't quite right, maybe test should really be #'= ?
(defparameter *float-to-string-table* (make-hash-table :test  #'eql))

(defun float-to-string (x)
  (let ((string (gethash x *float-to-string-table* nil)))
    (when (null string)
      (setf string (%float-to-string x))
      (setf (gethash x *float-to-string-table*) string))
    string))

;;;============================================================
;; note:  assumes that min<max.  If min=max, will break in log
;; kludge:  if min=max, arbitrarily move min to min-inc and max to max+inc,
;; to provide non-zero interval.  inc is chosen to have reasonable size.
;; >>>changed (log min) to (log (abs min)), jam, 9-11-89.
;  (when (= min max)
;    (let ((inc (if (zerop min)
;		   1.0d0
;		   (expt 10.0d0 (or power (floor (log (abs min) 10.0d0)))))))
;      (setq min (- min inc))
;      (setq max (+ max inc))))

;;; Scaling algorithm for plots.  NTICS is the desired number of tics.
;;; Round is a list of acceptable scaling factors.  POWER is the power
;;; of ten to use.  Returns a Tics structure including NEWMAX, NEWMIN,
;;; INC, and NTICS

;; Rounding Constants.  Notice that they are in decreasing order and
;; end with 1.0d0

(deftype Truncated-Double-Float ()
  "Integers that are within the range of Double-Floats."
  '(Integer
    #.(truncate most-negative-double-float)
    #.(truncate most-positive-double-float)))
    
(defun scale-tics (min max n-tics
		   &optional
		   (round '(5.0d0 2.5d0 2.0d0 1.5d0 1.0d0))
		   initial)
  (declare (optimize (safety 0) (speed 3))
	   (type Chart-Coordinate min max)
	   (type Fixnum n-tics)
	   (type List round))
  (let* ((n-inc (- n-tics 1))
	 (raw-inc (/ (the Chart-Coordinate (- max min))
		     (the Chart-Coordinate
		       (az:fl n-inc))))
	 (init-power (or initial (floor (/ (log raw-inc) #.(log 10.0d0)))))
	 (power (expt 10.0d0 init-power)) ;; POWER is the power of ten
	 (mantissa (/ raw-inc power)) ;; MANTISSA is the scale factor
	 (index ()))
    (declare (type Fixnum n-inc init-power)
	     (type List index)
	     (type Chart-Coordinate raw-inc power mantissa))
    (if (> mantissa (the Chart-Coordinate (first round)))
	(progn (setf power (the Chart-Coordinate (* 10.0d0 power)))
	       (setf index (last round)))
      (setf index (do ((mark round (cdr mark)))
		      ((null (cdr mark)) mark)
		    (declare (type List mark))
		    (when (> mantissa (the Chart-Coordinate (second mark)))
		      (return mark)))))
    ;; Find new max and new min
    (let ((new-max min)
	  (new-min 0.0d0)
	  (inc 0.0d0)
	  (factor 0.0d0)
	  (lower-mult 0)
	  (upper-mult 0))
      (declare (type Chart-Coordinate new-max new-min inc factor)
	       (type Fixnum lower-mult upper-mult))
      (loop (when (<= max new-max)
	      (return (make-tics :%tic-min new-min :%tic-max new-max
				 :%tic-inc inc :%tic-n n-tics)))
	(setf inc (* (the Chart-Coordinate (first index)) power))
	(setf factor (/ (the Chart-Coordinate (- (+ max min) (* n-inc inc)))
			(the Chart-Coordinate (* 2.0d0 inc))))
	(setf lower-mult (ceiling factor))
	(setf new-min (* inc lower-mult))
	(when (> new-min min)
	  (decf lower-mult)
	  (setf new-min (* inc (the Chart-Coordinate
				 (az:fl lower-mult)))))
	(when (and (>= min 0.0d0) (minusp new-min))
	  (setf lower-mult 0)
	  (setf new-min 0.0d0))
	(setf upper-mult (+ lower-mult n-inc))
	(setf new-max (* inc (the Chart-Coordinate
			       (az:fl upper-mult))))
	(when (and (<= max 0.0d0) (> new-max 0.0d0))
	  (setf upper-mult 0)
	  (setf new-max 0.0d0)
	  (setf lower-mult (- n-inc))
	  (setf new-min (* inc (the Chart-Coordinate
				 (az:fl lower-mult)))))
	(if (eq round index)
	    (progn (setf index (last round))
		   (setf power (* 10.0d0 power)))
	  (setf index (do ((mark round (cdr mark)))
			  ((eq (cdr mark) index)
			   mark)
			(declare (type List mark)))))))))

;;;============================================================

(defun nice-chart-rect (r &key (result (make-chart-rect)))

  "This is a minor convenience for a common case;
it saves us having to call <get-nice-tics> twice
and then modifiy a rect to be consistent with the resulting intervals."

  (declare (type Chart-Rect r result)
		    (:returns (values (type Chart-Rect result)
				      (type Tics xtics)
				      (type Tics ytics))))
  
  (let* ((xtics (get-nice-tics (%chart-rect-xmin r) (%chart-rect-xmax r)))
	 (ytics (get-nice-tics (%chart-rect-ymin r) (%chart-rect-ymax r)))
	 (xmin (%tic-min xtics))
	 (width (- (%tic-max xtics) xmin))
	 (ymin (%tic-min ytics))
	 (height (- (%tic-max ytics) ymin)))
    (%set-chart-rect-coords result xmin ymin width height)
    (values result xtics ytics)))