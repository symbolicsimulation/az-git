;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)
	
;;;============================================================
;;; Chart-Coordinate
;;;============================================================

(deftype Chart-Coordinate ()
  "The Chart-Coordinate type determines the allowed values for
the coordinates of Chart-Vectors, Chart-Points, and Chart-Rects."
  'Double-Float)

(defun chart-coordinate? (x) (typep x 'Chart-Coordinate))

(defun chart-coordinate-list? (l)
  (and (listp l) (every #'Chart-Coordinate? l)))

(deftype Chart-Coordinate-List ()
  "A list of Chart-Coordinates."
  `(satisfies chart-coordinate-list?))

(deftype Positive-Chart-Coordinate ()
  "A non-negative Chart-Coordinate."
  `(Double-Float 0.0d0 *))

(defun positive-chart-coordinate? (x)
  (typep x 'Positive-Chart-Coordinate))

(defun positive-chart-coordinate-list? (l)
  (and (listp l) (every #'Chart-coordinate? l)))

(deftype Positive-Chart-Coordinate-List ()
  "A list of Positive-Chart-Coordinates."
  `(satisfies positive-chart-coordinate-list?))
