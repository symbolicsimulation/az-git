;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)


;;;============================================================
;;; Chart-Points (two-dimensional)
;;;============================================================
;;; defstruct representation:

(defstruct (Chart-Point
	    (:conc-name nil)
	    (:constructor %%make-chart-point)
	    (:copier %chart-point-copier))

  "Chart-Points represent locations on a chart."

  (%chart-point-x 0.0d0 :type Chart-Coordinate)
  (%chart-point-y 0.0d0 :type Chart-Coordinate))

(defun %chart-point? (p) (typep p 'Chart-Point))

;;;-------------------------------------------------------

(defun %make-chart-point (x y)
  (declare (type Chart-Coordinate x y))
  (%%make-chart-point :%chart-point-x x :%chart-point-y y))

(defun make-chart-point (&key (x 0.0d0) (y 0.0d0))
  "The constructor function for Chart-Point.

The point is optionally initialized with the given <x> and <y>
coordinates."

  (declare (type Chart-Coordinate x y)
	   (:returns (type Chart-Point)))

  (%make-chart-point x y))

;;;-------------------------------------------------------

(defun chart-point-x (p)
  "Accessor function for x coordinate."
  (declare (type Chart-Point p)
	   (:returns (type Chart-Coordinate)))
  (%chart-point-x p))

(defun chart-point-y (p)
  "Accessor function for y coordinate."
  (declare (type Chart-Point p)
	   (:returns (type Chart-Coordinate)))
  (%chart-point-y p))

(defun set-chart-point-x (p x)
  (declare (type Chart-Point p)
	   (type Chart-Coordinate x))
  (setf (%chart-point-x p) x))

(defun set-chart-point-y (p y)
  (declare (type Chart-Point p)
	   (type Chart-Coordinate y))
  (setf (%chart-point-y p) y))

(defsetf chart-point-x (p) (x)
  "Sets the point's x coordinate."
  `(set-chart-point-x ,p ,x))

(defsetf chart-point-y (p) (y)
  "Sets the point's y coordinate."
  `(set-chart-point-y ,p ,y))

;;;-------------------------------------------------------

(defun %set-chart-point-coords (p x y)
  (declare (type Chart-Point p)
	   (type Chart-Coordinate x y))
  (setf (%chart-point-x p) x)
  (setf (%chart-point-y p) y)
  p)

(defun set-chart-point-coords (p &key (x 0.0d0) (y 0.0d0))
  (declare (type Chart-Point p)
	   (type Chart-Coordinate x y)
	   (:returns p))
  (%set-chart-point-coords p x y))

;;;-------------------------------------------------------

(defun %copy-chart-point (p0 p1)
  (declare (type Chart-Point p0 p1))
  (setf (%chart-point-x p1) (%chart-point-x p0))
  (setf (%chart-point-y p1) (%chart-point-y p0))
  p1)

(defun copy-chart-point (p &key (result (make-chart-point)))
  "The copier function for Chart-Points."
  (declare (type Chart-Point p)
		    (type Chart-Point result)
		    (:returns result))
  (%copy-chart-point p result))

;;;-------------------------------------------------------

(defun %equal-chart-points? (p0 p1)
  (declare (type Chart-Point p0 p1))
  (and (= (%chart-point-x p0) (%chart-point-x p1))
       (= (%chart-point-y p0) (%chart-point-y p1))))

(defun equal-chart-points? (p0 p1)
  "The equality predicate for Chart-Points."
  (declare (type Chart-Point p0 p1)
		    (:returns (type (Member T Nil))))
  (%equal-chart-points? p0 p1))

;;;-------------------------------------------------------

(defun %chart-point-List? (plist)
  (every #'%chart-point? plist))

(deftype Chart-Point-List ()
  "A list of Chart-Points."
  '(satisfies %chart-point-List?))

;;;-------------------------------------------------------
;;; beginnings of Chart-Point algebra:
;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %move-chart-point)))

(defun %move-chart-point (p dp result)
  (declare (type Chart-Point p result)
	   (type Chart-Vector dp))
  (setf (%chart-point-x result)
	(+ (the Chart-Coordinate (%chart-point-x p))
	   (the Chart-Coordinate (%chart-vector-x dp))))
  (setf (%chart-point-y result)
	(+ (the Chart-Coordinate (%chart-point-y p))
	   (the Chart-Coordinate (%chart-vector-y dp))))
  result)

(defun move-chart-point (p dp &key (result (%make-chart-point 0.0d0 0.0d0)))

  "Displace a point <p> by adding a vector <dp>.  Note that it's ok
for <p> to be eq to <result>."

  (declare (type Chart-Point p result)
		    (type Chart-Vector dp)
		    (:returns result))
  (%move-chart-point p dp result))

;;;-------------------------------------------------------

(defun %affine-mix-chart-points (a0 p0 a1 p1 result)
  (declare (type Chart-Coordinate a0 a1)
	   (type Chart-Point p0 p1 result)
	   (:returns result))
  (setf (%chart-point-x result)
	 (+ (* a0 (the Chart-Coordinate (%chart-point-x p0)))
	    (* a1 (the Chart-Coordinate (%chart-point-x p1)))))
  (setf (%chart-point-y result)
	 (+ (* a0 (the Chart-Coordinate (%chart-point-y p0)))
	    (* a1 (the Chart-Coordinate (%chart-point-y p1)))))
  result)

(defun affine-mix-chart-points (a0 p0 a1 p1 &key (result (make-chart-point))) 

  "Computes an affine combination of <p0> and <p1>.  It's ok for
<result> to be eq to either <p0> or <p1>.  Since it's an affine
combination, <a0> + <a1> must be equal to 1, at least to some
reasonable precision."

  (declare (type Chart-Coordinate a0 a1)
		    (type Chart-Point p0 p1 result)
		    (:returns result)) 

  (assert (<= (abs (+ a0 a1 -1.0d0)) (* 10.0d0 double-float-epsilon)))

  (%affine-mix-chart-points a0 p0 a1 p1 result))

;;;-------------------------------------------------------

(defun %subtract-chart-points (p0 p1 result)
  (declare (type Chart-Point p0 p1)
	   (type Chart-Vector result))
  (setf (%chart-vector-x result)
	(- (the Chart-Coordinate (%chart-point-x p0))
	   (the Chart-Coordinate (%chart-point-x p1))))
  (setf (%chart-vector-y result)
	(- (the Chart-Coordinate (%chart-point-x p0))
	   (the Chart-Coordinate (%chart-point-x p1))))
  result)

(defun subtract-chart-points (p0 p1
			      &key
			      (result (%make-chart-vector 0.0d0 0.0d0)))

  "Computes the difference vector of <p0> and <p1>.
Note that it's ok for <p0> or <p1> to be eq to <result>."

  (declare (type Chart-Point p0 p1)
		    (type Chart-Vector result)
		    (:returns result))

  (%subtract-chart-points p0 p1 result))
  
;;;=======================================================
;;; A resource for borrowing temporary Chart-Point objects:
;;;=======================================================

(defparameter *chart-point-resource* ()
  "A resource for chart points.")

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %borrow-chart-point)))

(defun %borrow-chart-point (x y)
  (declare (special *chart-point-resource*)
	   (type Chart-Coordinate x y)
	   (:returns (type Chart-Point)))
  (let ((p (pop *chart-point-resource*)))
    (cond ((null p)
	   (%make-chart-point x y))
	  (t
	   (setf (%chart-point-x p) x)
	   (setf (%chart-point-y p) y)
	   p))))
 
(defun borrow-chart-point (&key (x 0.0d0) (y 0.0d0))

  "Get a point out of the resource, making a new one if necessary."

  (declare (type Chart-Coordinate x y)
		    (:returns (type Chart-Point)))

  (%borrow-chart-point x y))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %return-chart-point)))

(defun %return-chart-point (p)
  (declare (special *chart-point-resource*))
  (push p *chart-point-resource*)
  t)

(defun return-chart-point (p)

  "Return <p> to the resource. This must be done with care, since any
further reference to <p> will be an error."

  (declare (type Chart-Point p)
		    (:returns t))

  (%return-chart-point p))

;;;-------------------------------------------------------
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro %with-borrowed-chart-point ((vname
				       &key (x 0.0d0) (y 0.0d0))
				      &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (%borrow-chart-point ,x ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (%return-chart-point ,return-name)))))

(defmacro with-borrowed-chart-point ((vname
				      &key (x 0.0d0) (y 0.0d0))
				     &body body)

  "This macro binds <vname> to a point borrowed from the resource,
optionally initialized to the supplied <x> and <y> coordinates, and
returns the point to the resource on exit.  The user should be careful
not to create references to the point that survive the dynamic extent
of the macro body."

  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name (borrow-chart-point :x ,x :y ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-chart-point ,return-name))))) 

(defmacro with-borrowed-chart-points (names &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let* ((return-names (mapcar #'(lambda (name) (gensym (string name)))
			       names))
	 (borrowings (mapcar #'(lambda (return-name)
				 `(,return-name
				   (%borrow-chart-point 0.0d0 0.0d0)))
			     return-names))
	 (returnings (mapcar #'(lambda (return-name)
				 `(%return-chart-point ,return-name))
			     return-names))
	 (bindings (mapcar #'(lambda (name return-name) `(,name ,return-name))
			   names return-names)))
    `(let* (,@borrowings ,@bindings)
       (multiple-value-prog1
	 (progn ,@body)
	 ,@returnings))))
