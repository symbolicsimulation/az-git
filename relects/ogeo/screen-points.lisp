;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================
;;; Screen-points
;;;============================================================
;;; defstruct representation:

(defstruct (Screen-Point
	    (:conc-name nil)
	    (:constructor %%make-screen-point)
	    (:copier %screen-point-copier)
	    (:print-function print-screen-point))

  "Screen-points are locations on a 2d screen (in some abstract sense).

We use a coordinate system where x increases from left to right,
and, unfortunately, like most window systems, y from top to bottom."

  (%screen-point-x 0 :type Screen-Coordinate)
  (%screen-point-y 0 :type Screen-Coordinate))

(defun %screen-point? (p) (typep p 'Screen-point))

;;;------------------------------------------------------------

(defun print-screen-point (p stream depth)
  "Print points with their coords showing."
  (declare (type Screen-Point p)
	   (type Stream stream)
	   (type (or Null Integer) depth)
	   (ignore depth))
  (az:printing-random-thing
   (p stream)
   (format stream "P ~d ~d" (%screen-point-x p) (%screen-point-y p))))

;;;-------------------------------------------------------

(defun %make-screen-point (x y)
  (declare (type Screen-Coordinate x y))
  (%%make-screen-point :%screen-point-x x :%screen-point-y y))

(defun make-screen-point (&key (x 0) (y 0))
  "The constructor function for Screen-Point.

The point is optionally initialized with the given <x> and <y>
coordinates."

  (declare (type Screen-Coordinate x y)
		    (:returns (type Screen-Point)))
  (%make-screen-point x y)) 

;;;-------------------------------------------------------

(defun screen-point-x (p)
  "Accessor function for x coordinate."
  (declare (type Screen-Point p)
		     (:returns (type Screen-Coordinate)))
  (%screen-point-x p))

(defun screen-point-y (p)
   "Accessor function for y coordinate."
 (declare (type Screen-Point p)
		   (:returns (type Screen-Coordinate)))
  (%screen-point-y p))

(defun set-screen-point-x (p x)
  (declare (type Screen-Point p)
		    (type Screen-Coordinate x))
  (setf (%screen-point-x p) x))

(defun set-screen-point-y (p y)
  (declare (type Screen-Point p)
		    (type Screen-Coordinate y))
  (setf (%screen-point-y p) y))

(defsetf screen-point-x (p) (x)
  "Sets the point's x coordinate."
  `(set-screen-point-x ,p ,x))

(defsetf screen-point-y (p) (y)
  "Sets the point's y coordinate."
  `(set-screen-point-y ,p ,y))

;;;-------------------------------------------------------

(defun %set-screen-point-coords (p x y)
  (declare (type Screen-Point p)
	   (type Screen-Coordinate x y))
  (setf (%screen-point-x p) x)
  (setf (%screen-point-y p) y)
  p)

(defun set-screen-point-coords (p &key (x 0) (y 0))
  (declare (type Screen-Point p)
		    (type Screen-Coordinate x y))
  (%set-screen-point-coords p x y))

;;;-------------------------------------------------------

(defun %copy-screen-point (p0 p1)
  (declare (type Screen-Point p0 p1))
  (setf (%screen-point-x p1) (%screen-point-x p0))
  (setf (%screen-point-y p1) (%screen-point-y p0))
  p1)

(defun copy-screen-point (p &key (result (make-screen-point)))
  "The copier function for Screen-Points."
  (declare (type Screen-Point p)
		    (type Screen-Point result)
		    (:returns result))
  (%copy-screen-point p result))

;;;-------------------------------------------------------

(defun %equal-screen-points? (p0 p1)
  (declare (type Screen-Point p0 p1))
  (and (= (%screen-point-x p0) (%screen-point-x p1))
       (= (%screen-point-y p0) (%screen-point-y p1))))

(defun equal-screen-points? (p0 p1)
  "The equality predicate for Screen-Pointsd."
  (declare (type Screen-Point p0 p1)
		    (:returns (type (Member t nil))))
  (%equal-screen-points? p0 p1))

;;;-------------------------------------------------------

(defun %screen-point-list? (ps)
  (every #'%screen-point? ps))

(deftype Screen-Point-List ()
  "A list of Screen-Points."
  '(satisfies %Screen-Point-List?))

(defun %point-list->xy-list (ps)
  (az:with-collection
    (dolist (p ps)
      (az:collect (%screen-point-x p))
      (az:collect (%screen-point-y p)))))

(defun point-list->xy-list (ps)
  (declare (type Screen-Point-List ps)
		    (:returns (type Screen-Coordinate-List)))
  (%point-list->xy-list ps))

;;;-------------------------------------------------------
;;; beginnings of Screen-Point algebra:
;;;-------------------------------------------------------
;;; Note that it's ok for <result> to be <eq> to either
;;; <p0> or <p1>

(eval-when (compile load eval)
  (proclaim '(Inline %move-screen-point
	      %affine-mix-screen-points
	      %subtract-screen-points)))

(defun %move-screen-point (p dp result)
  (declare (type Screen-Point p result)
	   (type Screen-Vector dp))
  (setf (%screen-point-x result)
	(+ (%screen-point-x p)
	   (%screen-vector-x dp)))
  (setf (%screen-point-y result)
	(+ (%screen-point-y p)
	   (%screen-vector-y dp)))
  result)

(defun move-screen-point (p dp &key (result (%make-screen-point 0 0)))
  "Displace a point <p> by adding a vector <dp>.  Note that it's ok
for <p> to be eq to <result>."
  (declare (type Screen-Point p result)
		    (type Screen-Vector dp)
		    (:returns result))
  (%move-screen-point p dp result))

(defun %affine-mix-screen-points (a0 p0 a1 p1 result)
  (declare (type Number a0 a1)
	   (type Screen-Point p0 p1 result)
	   (:returns result))
  (setf (%screen-point-x result)
	(round (+ (* a0 (%screen-point-x p0))
		  (* a1 (%screen-point-x p1)))))
  (setf (%screen-point-y result)
	(round (+ (* a0 (%screen-point-y p0))
		  (* a1 (%screen-point-y p1)))))
  result)

(defun affine-mix-screen-points (a0 p0 a1 p1 &key (result (make-screen-point)))

  "Computes an affine combination of <p0> and <p1>.  It's ok for
<result> to be eq to either <p0> or <p1>.  Since it's an affine
combination, <a0> + <a1> must be equal to 1, at least to some
reasonable precision."

  (declare (type Number a0 a1)
		    (type Screen-Point p0 p1 result)
		    (:returns result))
  ;; Must be an affine combination, at least to some reasonable
  ;; precision.
  (assert (<= (abs (+ a0 a1 -1.0d0)) (* 10.0d0 Double-Float-epsilon)))
  (%affine-mix-screen-points a0 p0 a1 p1 result))

(defun %subtract-screen-points (p0 p1 result)
  (declare (type Screen-Point p0 p1)
	   (type Screen-Vector result)
	   (:returns result))
  (setf (%screen-vector-x result)
    (- (%screen-point-x p0) (%screen-point-x p1)))
  (setf (%screen-vector-y result)
    (- (%screen-point-y p0) (%screen-point-y p1)))
  result)

(defun subtract-screen-points (p0 p1 &key (result (%make-screen-vector 0 0)))
  "Computes the difference vector of <p0> and <p1>.
Note that it's ok for <p0> or <p1> to be eq to <result>."
  (declare (type Screen-Point p0 p1)
		    (type Screen-Vector result)
		    (:returns result))
  (%subtract-screen-points p0 p1 result))
  
;;;=======================================================
;;; A resource for borrowing temporary Screen-Point objects:
;;;=======================================================

(defparameter *screen-point-resource* ())

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %borrow-screen-point)))

(defun %borrow-screen-point (x y)
  (declare (special *screen-point-resource*)
	   (type Screen-Coordinate x y)
	   (:returns (type Screen-Point)))
  (let ((p (pop *screen-point-resource*)))
    (cond ((null p)
	   (%make-screen-point x y))
	  (t
	   (setf (%screen-point-x p) x)
	   (setf (%screen-point-y p) y)
	   p))))
 
(defun borrow-screen-point (&key (x 0) (y 0))
  "Get a point out of the resource, making a new one if necessary."

  (declare (type Screen-Coordinate x y)
		    (:returns (type Screen-Point)))
  (%borrow-screen-point x y))

;;;-------------------------------------------------------

(eval-when (compile load eval)
  (proclaim '(Inline %return-screen-point)))

(defun %return-screen-point (p)
  (declare (type Screen-Point p)
	   (special *screen-point-resource*))
  (push p *screen-point-resource*)
  t)

(defun return-screen-point (p)
  "Return <p> to the resource. This must be done with care, since any
further reference to <p> will be an error."
  (declare (type Screen-Point p)
		    (:returns t))
  (%return-screen-point p)
  t)

;;;-------------------------------------------------------
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro %with-borrowed-screen-point ((vname &key (x 0) (y 0)) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (%borrow-screen-point ,x ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (%return-screen-point ,return-name)))))

(defmacro with-borrowed-screen-point ((vname
				       &key (x 0) (y 0))
				      &body body)

  "This macro binds <vname> to a point borrowed from the resource,
optionally initialized to the supplied <x> and <y> coordinates, and
returns the point to the resource on exit.  The user should be careful
not to create references to the point that survive the dynamic extent
of the macro body."

  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    (gensym "G")
    `(let* ((,return-name (borrow-screen-point :x ,x :y ,y))
	    (,vname ,return-name))
       (multiple-value-prog1
	   (progn ,@body)
	 (return-screen-point ,return-name))))) 

;;;-------------------------------------------------------

(defmacro with-borrowed-screen-points (names &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let* ((return-names (mapcar #'(lambda (name) (gensym (string name)))
			       names))
	 (borrowings (mapcar #'(lambda (return-name)
				 `(,return-name (%borrow-screen-point 0 0)))
			     return-names))
	 (returnings (mapcar #'(lambda (return-name)
				 `(%return-screen-point ,return-name))
			     return-names))
	 (bindings (mapcar #'(lambda (name return-name) `(,name ,return-name))
			   names return-names)))
    `(let* (,@borrowings ,@bindings)
       (multiple-value-prog1
	 (progn ,@body)
	 ,@returnings))))

;;;-------------------------------------------------------

(defun %borrow-screen-point-list (npoints)
  (declare (type Fixnum npoints)
		    (:returns (type Screen-Point-List)))
  (let ((the-list ()))
    (dotimes (i npoints)
      (push (%borrow-screen-point 0 0) the-list))
    the-list))

(defun borrow-screen-point-list (npoints)
  (declare (type Fixnum npoints)
		    (:returns (type Screen-Point-List)))
  (%borrow-screen-point-list npoints))

(defun %return-screen-point-list (ps)
  (declare (type Screen-Point-List ps)
	   (:returns t))
  (dolist (p ps) (%return-screen-point p))
  t)

(defun return-screen-point-list (ps)
  (declare (type Screen-Point-List ps)
	   (:returns t))
  (dolist (p ps) (return-screen-point p))
  t)

(defmacro with-borrowed-screen-point-list ((name npoints) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    (gensym "G")
    `(let* ((,return-name (borrow-screen-point-list ,npoints))
	    (,name ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-screen-point-list ,return-name)))))

(defmacro %with-borrowed-screen-point-list ((name npoints) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym "BORROW-")))
    (gensym "G")
    `(let* ((,return-name (%borrow-screen-point-list ,npoints))
	    (,name ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (%return-screen-point-list ,return-name)))))
