/*
** Sgp device driver for lib457 library.
**
** Copyright (c) 1987, 1988, Tony DeRose
*/
#include <stdio.h>
#include "framebuffer.h"
#include "geometry.h"
#include "device.h"

/* Imports */
extern Space Screen;

/* Local names */
static Frame XwindowFrame;
static int   drawline();

/* Forward decls */

/* Local variables */

/*
** Create an X window and initialize the XwindowFrame.
*/
void DeviceInitialize()
{
	Vector PixX, PixY;
	Point Org;
	int xmin, ymin, xmax, ymax;
	int Xres, Yres;

	fb_open( &xmin, &ymin, &xmax, &ymax);

	Xres = xmax-xmin;
	Yres = ymax-ymin;

	/* PixX and PixY are the length of a pixel */
	Org  = PCreate( StdFrame(Screen), 0.0, 1.0);
	PixX = SVMult( 1.0/((Scalar) Xres), FV(StdFrame(Screen), 0));
	PixY = SVMult( -1.0/((Scalar) Yres), FV(StdFrame(Screen), 1));

	XwindowFrame = FCreate( "Xwindow", Org, PixX, PixY);
}

/*
** Terminate the device.
*/
void DeviceTerminate()
{
  fb_close();
}

/*
** Clear the screen to the current color.
*/
void DeviceClear()
{
  fb_clear();
}

/*
** Draw a line segment on the device in the current color.
*/
void DeviceDrawLine( p1, p2)
Point p1, p2;
{
	Scalar tx1, ty1, tx2, ty2;
	
	/* Extract coordinates relative to TargaFrame */
	PCoords( p1, XwindowFrame, &tx1, &ty1);
	PCoords( p2, XwindowFrame, &tx2, &ty2);

#ifdef DEBUG
	fprintf(stderr, "Line from (%lg %lg) to (%lg %lg)\n",
							tx1, ty1, tx2, ty2);
#endif
	/* Draw a line */
	drawline( (int) tx1, (int) ty1, (int) tx2, (int) ty2);
}


/*
** Write a pixel.
*/
void DeviceWritePix( x, y)
int x,y;
{
  fb_writePixel( x, y, BLACK);
}


static int drawline(x1, y1, x2, y2)
int x1, y1, x2, y2;
{
  fb_drawLine( x1, y1, x2, y2, BLACK);
}

