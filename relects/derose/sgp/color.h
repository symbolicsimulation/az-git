/*
** Typedefs and defines for device independent color.
**
** Copyright (c) 1987, 1988 Tony D. DeRose
*/

typedef struct {
	double r, g, b;		/* Each in the range [0,1] */
} Color;


