/*
** A program to do simple wireframing.  The program reads the world
** coordinates of line segments endpoints from a file, then displays
** them on a raster device.  The purpose of the program is to experiment
** with the geometry package.
**
** Copyright (c) 1987, 1988, Tony D. DeRose
*/
#include <stdio.h>
#include "sgp.h"

/* Imports */
extern double atof();

main(argc, argv)
int argc;
char **argv;
{
	Initialize( argc, argv);
	Scene();
	printf("Type ^C in this window to quit.\n");
	while (1) {};
	/* SgpExit(); */
}
	

/*
** Initialize the viewing transformations and the device.
*/
Initialize( argc, argv)
int argc;
char **argv;
{
	SgpInit();

	/*-------------------------------------*/
	/* Set defaults for viewing parameters */
	/*-------------------------------------*/
	SgpSetEye( PCreate(WorldFrame, 1.0, 0.5, 0.5));
	SgpSetViewDirection( VCreate(WorldFrame,-1.0, -0.5, -0.5));
	SgpSetUpVector( VCreate(WorldFrame, 0.0, 0.0, 1.0));

	SgpSetWindow( 3.0, 3.0);
	SgpSetViewPort( 0.0, 1.0, 0.0, 1.0);

	/* Parse the command line */
	ParseCommandLine( argc, argv);
}


ParseCommandLine( argc, argv)
int argc;
char **argv;
{
	Scalar c0, c1, c2;
	Scalar vpleft, vpright, vpbottom, vptop, winhsize, winvsize;
	
	/* Jump over the command name */
	argc--; argv++;

	while (argc) {
		if (argv[0][0] != '-') break; /* out of while loop */
		switch (argv[0][1]) {
		case 'e':
			/* Set the eye point */
			c0 = atof( argv[1]);
			c1 = atof( argv[2]);
			c2 = atof( argv[3]);
			SgpSetEye( PCreate( WorldFrame, c0, c1, c2));
			argc -= 3; argv += 3;			
			break;
		case 'v':
		    	/* Set the viewing direction */
		    	c0 = atof(argv[1]);
			c1 = atof( argv[2]);
			c2 = atof( argv[3]);
			SgpSetViewDirection( VCreate( WorldFrame, c0, c1, c2));
			argc -= 3; argv += 3;			
			break;
		case 'u':
		    	/* Set the up vector */
		    	c0 = atof(argv[1]);
			c1 = atof( argv[2]);
			c2 = atof( argv[3]);
			SgpSetUpVector( VCreate( WorldFrame, c0, c1, c2));
			argc -= 3; argv += 3;			
			break;
		case 'V':
			/* Set the components of viewport frame */
			vpleft  = atof( argv[1]);
			vpright = atof( argv[2]);
			vpbottom= atof( argv[3]);
			vptop   = atof( argv[4]);
			SgpSetViewPort( vpleft, vpright, vpbottom, vptop);
			argc -= 4; argv += 4;
			break;
		case 'w':
			/* Set the size of the window. */
			winhsize = atof( argv[1]);
			winvsize = atof( argv[2]);
			SgpSetWindow( winhsize, winvsize);
			argc -= 2; argv += 2;
			break;
		default:
			fprintf(stderr, "Unknown switch -%c\n", argv[0][1]);
			exit(1);
		}
		argc--; argv++;
	}
}
			
			
/*
** Read line segments whose endpoints are given in world
** coordinates, then display the line segments on the screen.
**
** File format:
**    x1 y1 z1 x2 y2 z2\n
**    ...
**    EOF
*/
Scene()
{
	Scalar x1, y1, z1, x2, y2, z2;
	Point p1, p2;
	
	while (scanf("%lf %lf %lf %lf %lf %lf\n", &x1,&y1,&z1, &x2,&y2,&z2) == 6) {
		p1 = PCreate( WorldFrame, x1, y1, z1);
		p2 = PCreate( WorldFrame, x2, y2, z2);
		SgpDrawLine( p1, p2);
	}
}

