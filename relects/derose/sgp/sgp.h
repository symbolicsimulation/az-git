/*
** sgp.h : Header file for users of sgp.c
**
** Copyright (c) 1987, 1988 Tony D. DeRose
*/
#include "geometry.h"
#include "color.h"

/* Type definitions */

typedef struct {			/* A geometric Hyperplane.    */
	Point b;			/* A point on the Hyperplane. */
	Normal n;			/* Normal to the Hyperplane.  */
} Hyperplane;

typedef struct {                        /* A vertex structure for polygons */
        Point b;
	Normal n;
	Color c;
} Vertex;

/* Exported definitions */
#define WorldFrame	StdFrame(World)

/* Exported Variables */
extern Space World, Screen;

/* Export Operations */
extern void
	 SgpDrawLine(),		/* Draw a line given world coords of ends */
	 SgpExit(),		/* Terminate the package */
	 SgpInit(),		/* Initialize the package */
	 SgpSetEye(),		/* Set eye point. */
	 SgpSetViewDirection(), /* Set viewing direction */
	 SgpSetViewPort(),	/* What it says...*/
	 SgpSetUpVector(),	/* Set up vector */
	 SgpSetWindow()		/* Set the window on proj plane */
;




