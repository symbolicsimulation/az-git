/*
** geometry.c: Routines to implement 2 and 3 dimensional affine
**           geometry.
**
** $Header: geometry.c,v 1.7 89/04/09 15:53:45 derose Exp $
**
** Copyright (c) 1989, Graphics and AI Laboratory, University of Washington
** Copying, use and development for non-commercial purposes permitted.
**                  All rights for commercial use reserved.
**		    This software is unsupported.
**
**
** This package exports the coordinate-free abstractions of affine 
** spaces, coordinate frames, points, vectors, normal vectors, and 
** affine transformations. When created, an affine space X comes
** ``pre-equipped'' with a cartesian coordinate frame called X.f0.
**
** A frame is created by specifying the origin (a point), and a basis
** for the vector space associated with the frame's space (2 or three
** linearly independent vectors).  Rather than storing the origin and
** basis separately as points and vectors, a frame F is represented by
** storing two matrices: tof0 and fromf0.  If X is the space in which
** the frame F lives, the matrix tof0 is the change of basis matrix 
** that converts coordinates relative to F into coordinates relative 
** to X.f0.  The matrix fromf0 is tof0's inverse; thus, fromf0 is the 
** change of basis matrix that converts coordinates relative to X.f0
** into coordinates relative to F.
** 
** Points, vectors, and normals are created by specifying their coordinates
** relative to any given frame.  These objects are represented by storing 
** their ``standard coordinates'', that is, their coordinates relative to 
** their space's f0 frame (using the tof0 matrix of the frame in which the
** object is specified). Coordinates for these objects are represented as
** row vectors (or, more precisely, row matrices).
**
** There are three basic ways in which affine transformations can be
** created. The first method is the most general. Let X and Y be two
** affine spaces, n = dim(X), and let Fx be a frame for X.  The routine
** ACreate(Fx, Py, Vy1,..., Vyn) returns the unique affine  transformation
** that carries the origin of Fx onto Py, the first basis vector of F
** onto Vy1, the second basis vector of F onto Yyn, and so forth.
** Such a transformation is (in fact, all transformation are) represented 
** by storing its matrix representation relative to the coordinate frame 
** X.f0 and Y.f0.  For efficiency in transforming normal vectors, if 
** dim(X) = dim(Y) the inverse transpose of this matrix is also stored.
**
** The second method is slightly less general than the first, and
** is appropriate when dim(X) = dim(Y).   Let Fx and Fy be coordinate
** frames for X and Y, respectively.  The routine ACreateF(Fx,Fy)
** returns the unique affine transformation that carries Fx onto Fy.  
**
** The third method of creating affine transformations is appropriate
** for the case when X = Y.  In this case, the notions of identity,
** rotations, translations and scalings make sense, so the routines
** AIdenity(), ARotate(), ATranslate(), and AScale() are provided.
**
** Various operations for combining and manipulating geometric objects
** are also provided.
*/
#include <stdio.h>
#include "geometry.h"

/* Convenient macros */
/* Fill in a row of a matrix */
#define SetRow(mat,row,rowvec)	{ME(mat,row,0) = ME(rowvec,0,0); \
			        ME(mat,row,1) = ME(rowvec,0,1); \
			        ME(mat,row,2) = ME(rowvec,0,2); \
			        ME(mat,row,3) = ME(rowvec,0,3);}

#define GetRow(rowvec,mat,row)  {ME(rowvec,0,0) = ME(mat,row,0); \
			         ME(rowvec,0,1) = ME(mat,row,1); \
			         ME(rowvec,0,2) = ME(mat,row,2); \
			         ME(rowvec,0,3) = ME(mat,row,3);}
			
/* Imports */
extern char *malloc();
extern double fabs();

/* Forward declarations */
extern int DefAffineError();


/* Local Variables */
static int (*AffineError)() = DefAffineError;    /* Error handler */


/*--------------------------------------------------------------*/
/*			Error Handling 				*/
/*--------------------------------------------------------------*/

/*
** Set the affine error handling routine.
*/
void SetAffineError( ErrorFunc)
int (*ErrorFunc)();
{
	AffineError = ErrorFunc;
}

/*
** Default error handling routine.
*/
DefAffineError( errmesg)
char *errmesg;
{
	fprintf(stderr, "Affine error: %s...dumping core.\n", errmesg);
	abort();
}

/*--------------------------------------------------------------*/
/*			Creation Routines			*/
/*--------------------------------------------------------------*/

/*
** Create a new affine space of the specified dimension.
*/
Space SCreate(name, dim)
char *name;
int dim;
{
	Space NewSpace;
	
	NewSpace = (Space) malloc(sizeof(struct euclideanSpace));
	
	NewSpace->dim = dim;
	NewSpace->name = name;
	NewSpace->f0.s = NewSpace;
	NewSpace->f0.tof0 = MatrixIdentity( dim+1);
	NewSpace->f0.fromf0 = MatrixIdentity( dim+1);

	return NewSpace;
}


/*
** Define a new coordinate frame.
*/
Frame FCreate( name, origin, v0, v1, v2)
char *name;
Point origin;
Vector v0, v1, v2;
{
	Frame NewFrame;
	int isValid = TRUE;
	
#ifdef DEBUG
	PPrintf( stderr, origin);
	VPrintf( stderr, v0);
	Vprintf( stderr, v1);
	if (Dim(SpaceOf(origin)) == 3) VPrintf( stderr, v2);
#endif

	isValid = (SpaceOf(origin) == SpaceOf(v0)) && (SpaceOf(origin) == SpaceOf(v1));
	if (Dim(SpaceOf(origin)) == 3) {
		isValid = isValid && (SpaceOf(origin) == SpaceOf(v2));
	}
	if (isValid) {
	    	
	    SpaceOf(NewFrame) = SpaceOf(origin);
	    NewFrame.name = name;
	    
	    switch (Dim(SpaceOf(NewFrame))) {
	    case 2:
		/* Create a frame for an affine 2-space */
	    	NewFrame.tof0 = MatrixZero( 3);

	    	/* The ith row of tof0 consists stdcoords of vi */
	    	SetRow(NewFrame.tof0, 0, v0.v);
	    	SetRow(NewFrame.tof0, 1, v1.v);

	    	/* The last row consists of stdcoords of origin */
	    	SetRow(NewFrame.tof0, 2, origin.p);
		break;
	    case 3:
		/* Create a frame for an affine 3-space */
	    	NewFrame.tof0 = MatrixZero( 4);

	    	/* The ith row of tof0 consists stdcoords of vi */
	    	SetRow(NewFrame.tof0, 0, v0.v);
	    	SetRow(NewFrame.tof0, 1, v1.v);
	    	SetRow(NewFrame.tof0, 2, v2.v);

	    	/* The last row consists of stdcoords of origin */
	    	SetRow(NewFrame.tof0, 3, origin.p);
		break;
	    }

	    /* Check to make sure a valid frame has been specified */
	    if (fabs(MatrixDet( NewFrame.tof0)) < EPSILON) {
#ifdef DEBUG
		MatrixPrint( NewFrame.tof0, stderr);
#endif
	    	(*AffineError)("FCreate: Vectors not linearly independent.");
	    } else {
		/* A valid frame, so compute fromf0 */
	    	NewFrame.fromf0 = MatrixInverse( NewFrame.tof0);
	    }
	    
	} else {
	    (*AffineError)("FCreate: elements not from same space.");
	}
	return NewFrame;
}
	
/*
** Create and return a new point.  The coordinates of the
** point are (c0,c1,[c2,]1) relative to the frame F.  The
** coordinate c2 is only used if F spans a 3-space.
*/
Point PCreate( F, c0, c1, c2)
Frame F;
Scalar c0, c1, c2;
{
	Point NewPoint;
	
	SpaceOf(NewPoint) = SpaceOf(F);
	switch (Dim(SpaceOf(NewPoint))) {
	case 2:
	    NewPoint.p = MatrixCreate( 1, 3);

	    /* Compute standard coords */
	    ME(NewPoint.p, 0, 0) = c0;
	    ME(NewPoint.p, 0, 1) = c1;
	    ME(NewPoint.p, 0, 2) = 1.0;
	    break;
	case 3:
	    NewPoint.p = MatrixCreate( 1, 4);

	    /* Compute standard coords */
	    ME(NewPoint.p, 0, 0) = c0;
	    ME(NewPoint.p, 0, 1) = c1;
	    ME(NewPoint.p, 0, 2) = c2;
	    ME(NewPoint.p, 0, 3) = 1.0;
	    break;
	}

	NewPoint.p = MatrixMult( NewPoint.p, F.tof0);
	return NewPoint;
}

	
/*
** Create and return a new vector.  The coordinates of the
** vector are (c0,c1,[c2,]1) relative to the frame F.  The
** coordinate c2 is only used if F spans a 3-space.
*/
Vector VCreate( F, c0, c1, c2)
Frame F;
Scalar c0, c1, c2;
{
	Vector NewVector;
	
	SpaceOf(NewVector) = SpaceOf(F);
	switch (Dim(SpaceOf(NewVector))) {
	case 2:
	    NewVector.v = MatrixCreate( 1, 3);

	    /* Compute standard coords */
	    ME(NewVector.v, 0, 0) = c0;
	    ME(NewVector.v, 0, 1) = c1;
	    ME(NewVector.v, 0, 2) = 0.0;
	    break;
	case 3:
	    NewVector.v = MatrixCreate( 1, 4);

	    /* Compute standard coords */
	    ME(NewVector.v, 0, 0) = c0;
	    ME(NewVector.v, 0, 1) = c1;
	    ME(NewVector.v, 0, 2) = c2;
	    ME(NewVector.v, 0, 3) = 0.0;
	    break;
	}

	NewVector.v = MatrixMult( NewVector.v, F.tof0);
	return NewVector;
}

/*
** Create and return a new normal vector.
*/
Normal NCreate( f, c0, c1, c2)
Frame f;
Scalar c0, c1, c2;
{
	return VDual( VCreate( f, c0, c1, c2));
}

/*
** Return the zero vector in space S.
*/
Vector VZero(S)
Space S;
{
	Vector ZeroVec;
	
	SpaceOf(ZeroVec) = S;
	ZeroVec.v = MatrixCreate(1, Dim(SpaceOf(ZeroVec)) + 1);
	
	return ZeroVec;
}

/*
** Return the transformation that carries the origin of F onto
** Oprime, and the basis in F onto v0prime, v1prime, [and v2prime.]
*/
AffineMap ACreate( F, Oprime, v0prime, v1prime, v2prime)
Frame F;
Point Oprime;
Vector v0prime, v1prime, v2prime;
{
	AffineMap T;			/* The returned transform */
	Matrix Tprime;			/* Rows built from primed objects */
	int isValid = 1;
	
	isValid = (SpaceOf(Oprime) == SpaceOf(v0prime)) &&
	    	  (SpaceOf(Oprime) == SpaceOf(v1prime));
	if (Dim(SpaceOf(Oprime)) == 3) {
		isValid = isValid && (SpaceOf(Oprime) == SpaceOf(v2prime));
	}
	if (isValid) {
		T.domain = SpaceOf(F);
		T.range  = SpaceOf(Oprime);

		/* Assume for now that the map isn't invertible */
		T.invertible = FALSE;
		
		/* Build Tprime */
		Tprime = MatrixCreate( Dim(SpaceOf(F))+1, Dim(SpaceOf(Oprime))+1);

		switch (Dim(SpaceOf(F))) {
		case 2:
		    SetRow(Tprime, 0, v0prime.v);
		    SetRow(Tprime, 1, v1prime.v);
		    SetRow(Tprime, 2, Oprime.p);
		    break;
		case 3:
		    SetRow(Tprime, 0, v0prime.v);
		    SetRow(Tprime, 1, v1prime.v);
		    SetRow(Tprime, 2, v2prime.v);
		    SetRow(Tprime, 3, Oprime.p);
		    break;
		}
		    
		/*-----------------------------------------*/
		/* T.t satisfies: F.tof0 * T.t = Tprime,   */
		/* So T.t = F.fromf0 * Tprime.             */
		/*-----------------------------------------*/
		T.t = MatrixMult( F.fromf0, Tprime);

		/*-------------------------------------------------------*/
		/* If T's domain and range have the same dimension, then */
		/* the inverse matrix may also be stored.                */
		/*-------------------------------------------------------*/
		if (Dim(SpaceOf(F)) == Dim(SpaceOf(Oprime))) {
		    /* See if T.t is invertible */
		    if (fabs(MatrixDet(T.t)) > EPSILON) {
			T.invt = MatrixInverse( T.t);
			T.invertible = TRUE;
		    }
		}
	} else {
		(*AffineError)("ACreate: image objects not in common space.");
	}
#ifdef DEBUG
	fprintf( stderr, "ACreated...\n");
	TPrintf( stderr, T);
#endif
	return T;
}


/*
** Return the affine transformation that carries the frame
** F1 onto the frame F2.  This could be done by calling ACreate,
** but the routine provided is a bit more efficient.
*/
AffineMap ACreateF( F1, F2)
Frame F1, F2;
{
	AffineMap T;

	if (Dim(SpaceOf(F1)) != Dim(SpaceOf(F2))) {
		(*AffineError)("ACreate: dim(F1) != dim(F2).");
	} else {
		T.range = SpaceOf(F1);
		T.domain = SpaceOf(F2);
		T.invertible = TRUE;
		T.t = MatrixMult( F1.fromf0, F2.tof0);
		T.invt = MatrixInverse(T.t);
	}
	return T;
}


/*
** Return the identity transformation from space S onto itself.
*/
AffineMap TIdentity(S)
Space S;
{
	AffineMap Identity;
	
	Identity.domain = S;
	Identity.range = S;
	Identity.invertible = TRUE;

	Identity.t = MatrixIdentity( Dim(S) + 1);
	Identity.invt = Identity.t;
	return Identity;
}




/*--------------------------------------------------------------*/
/*			Coordinate Routines			*/
/*--------------------------------------------------------------*/


/*
** Return the coordinates of the point p relative to the frame F.
*/
void PCoords( p, F, c0, c1, c2)
Point p;
Frame F;
Scalar *c0, *c1, *c2;
{
	Matrix Coords;
	
	Coords = MatrixMult( p.p, F.fromf0);
	*c0 = ME(Coords,0,0);
	*c1 = ME(Coords,0,1);

	if (Dim(SpaceOf(p)) == 3) {
		*c2 = ME(Coords,0,2);
	}
}


/*
** Return the coordinates of the vector v relative to the frame F.
*/
void VCoords( v, F, c0, c1, c2)
Vector v;
Frame F;
Scalar *c0, *c1, *c2;
{
	Matrix Coords;
	
	Coords = MatrixMult( v.v, F.fromf0);
	*c0 = ME(Coords,0,0);
	*c1 = ME(Coords,0,1);

	if (Dim(SpaceOf(v)) == 3) {
		*c2 = ME(Coords,0,2);
	}
}

/*
** Return the coordinates of the normal vector n relative to the frame F.
*/
void NCoords( n, F, c0, c1, c2)
Normal n;
Frame F;
Scalar *c0, *c1, *c2;
{
	Matrix Coords;
	
	Coords = MatrixMult( MatrixTranspose(n.n), F.fromf0);
	*c0 = ME(Coords,0,0);
	*c1 = ME(Coords,0,1);

	if (Dim(SpaceOf(n)) == 3) {
		*c2 = ME(Coords,0,2);
	}
}

/*
** Return the origin of the given frame.
*/
Point FOrg(F)
Frame F;
{
	Point Origin;
	
	SpaceOf(Origin) = SpaceOf(F);
	Origin.p = MatrixCreate( 1, Dim(SpaceOf(Origin)) + 1);

	if (Dim(SpaceOf(Origin)) == 2) {
		GetRow(Origin.p, F.tof0, 2);
	} else {
		GetRow(Origin.p, F.tof0, 3);
	}

	return Origin;
}

/*
** Return the ith basis vector in F (numbered starting at 0).
*/
Vector FV(F,i)
Frame F;
int i;
{
	Vector Bi;
	
	SpaceOf(Bi) = SpaceOf(F);
	Bi.v = MatrixCreate( 1, Dim(SpaceOf(Bi)) + 1);
	GetRow(Bi.v, F.tof0, i);

	return Bi;
}


/*--------------------------------------------------------------*/
/*			Affine Space Operators			*/
/*--------------------------------------------------------------*/

/*
** Return the vector difference of two points.  Returned
** is p-q, the vector from q to p.
*/
Vector PPDiff( p, q)
Point p, q;
{
	Vector v;
	
	if (SpaceOf(p) != SpaceOf(q)) {
		(*AffineError)("PPDiff: points not in same space.");
	}
	SpaceOf(v) = SpaceOf(p);
	v.v = MatrixCreate( 1, Dim(SpaceOf(v))+1);
	ME(v.v,0,0) = ME(p.p,0,0) - ME(q.p,0,0);
	ME(v.v,0,1) = ME(p.p,0,1) - ME(q.p,0,1);
	ME(v.v,0,2) = ME(p.p,0,2) - ME(q.p,0,2);
	
	return v;
}

/*
** Return the point q = p + v.
*/
Point PVAdd( p, v)
Point p;
Vector v;
{
	if (SpaceOf(p) != SpaceOf(v)) {
		(*AffineError)("PVAdd: point and vector from different spaces.");
	}
	ME(p.p,0,0) += ME(v.v,0,0);
	ME(p.p,0,1) += ME(v.v,0,1);
	ME(p.p,0,2) += ME(v.v,0,2);
	return p;
}

	
/*
** Return the point that breaks line segment p1,p2 into
** relative rations r1 and r2.
*/
Point PPrr( p1, p2, r1, r2)
Point p1, p2;
Scalar r1, r2;
{
	return PPac( p1, p2, r2/(r2+r1));
}

/*
** Return the point a1*p1 + (1-a1)*p2.
**
** This point can also be written as p2 + a1*(p1-p2), which
** is how its actually calculated.
*/
Point PPac( p1, p2, a1)
Point p1, p2;
Scalar a1;
{
	return PVAdd( p2, SVMult( a1, PPDiff( p1, p2)));
}


/*
** Return the point a1*p1 + a2*p2 + a3*p3, where
** a1 + a2 + a3 = 1.
*/
Point PPac3( p1, p2, p3, a1, a2, a3)
Point p1, p2, p3;
Scalar a1, a2, a3;
{
    Point p[3];
    Scalar a[3];

    p[0] = p1;  p[1] = p2;  p[2] = p3;
    a[0] = a1;  a[1] = a2;  a[2] = a3;
    return PPacN( 3, p, a);
}

    
/*
** Do an affine combination of n points. Returned is the point 
**
**       sum_{i=0}^{n-1} a[i] * p[i]
**
** where the a's sum to one.
*/
Point PPacN( n, p, a)
int n;
Point p[];
Scalar a[];
{
    Scalar x, y, z, sum;
    Space S = SpaceOf(p[0]);
    int i, dim = Dim(SpaceOf(p[0]));

    /* For efficiency, appeal to coordinates */
    x = 0.0;  y = 0.0;  z = 0.0;  sum = 0.0;
    for (i = 0; i < n; i++) {
	if (SpaceOf(p[i]) != S) {
	    (*AffineError)("PPacN: points not from a common space.\n");
	}
	sum += a[i];
	x += a[i] * ME(p[i].p,0,0);
	y += a[i] * ME(p[i].p,0,1);
	if (dim == 3) {
	    z += a[i] * ME(p[i].p,0,2);
	}
    }
    if (fabs(sum - 1.0) > EPSILON) {
	(*AffineError)("PPacN: coefficients don't sum to one.\n");
    }
    return PCreate( StdFrame(S), x, y, z);
}


    
/*
** Do a "vector combination" of n points. Returned is the vector
**
**       sum_{i=0}^{n-1} a[i] * p[i]
**
** where the a's sum to zero.
*/
Vector PPvcN( n, p, a)
int n;
Point p[];
Scalar a[];
{
    Scalar x, y, z, sum;
    Space S = SpaceOf(p[0]);
    int i, dim = Dim(SpaceOf(p[0]));

    /* For efficiency, appeal to coordinates */
    x = 0.0;  y = 0.0;  z = 0.0;  sum = 0.0;
    for (i = 0; i < n; i++) {
	if (SpaceOf(p[i]) != S) {
	    (*AffineError)("PPacN: points not from a common space.");
	}
	sum += a[i];
	x += a[i] * ME(p[i].p,0,0);
	y += a[i] * ME(p[i].p,0,1);
	if (dim == 3) {
	    z += a[i] * ME(p[i].p,0,2);
	}
    }
    if (fabs(sum) > EPSILON) {
	(*AffineError)("PPvcN: coefficients don't sum to zero.");
    }
    return VCreate( StdFrame(S), x, y, z);
}

	
/*
** Return the outward pointing normal vector to the plane defined
** by p1, p2 and p3.  The right hand rule is used to define outward
** pointing.
*/
Normal PPPNormal( p1, p2, p3)
Point p1, p2, p3;
{
    return VDual(VVCross( PPDiff( p2, p1), PPDiff( p3, p1)));
}


/*--------------------------------------------------------------*/
/*			Vector Space Operators			*/
/*--------------------------------------------------------------*/

/*
** Scalar-Vector Multiplication.
*/
Vector SVMult( scalar, vector)
Scalar scalar;
Vector vector;
{
	Vector Vprime;
	
	SpaceOf(Vprime) = SpaceOf(vector);
	Vprime.v = MatrixCreate( 1, Dim(SpaceOf(Vprime)) + 1);
	ME(Vprime.v,0,0) = ME(vector.v,0,0) * scalar;
	ME(Vprime.v,0,1) = ME(vector.v,0,1) * scalar;
	ME(Vprime.v,0,2) = ME(vector.v,0,2) * scalar;

	return Vprime;
}

/*
** Return the sum of the two vectors.
*/
Vector VVAdd( v1, v2)
Vector v1, v2;
{
    Vector v;

    if (SpaceOf(v1) != SpaceOf(v2)) {
	(*AffineError)("VVAdd: vectors from different spaces.");
    }
    SpaceOf(v) = SpaceOf(v1);
    v.v = MatrixCreate( 1, Dim(SpaceOf(v1)) + 1);
    ME(v.v,0,0) = ME(v1.v,0,0)+ME(v2.v,0,0);
    ME(v.v,0,1) = ME(v1.v,0,1)+ME(v2.v,0,1);
    if (Dim(SpaceOf(v)) == 3) {
	ME(v.v,0,2) = ME(v1.v,0,2)+ME(v2.v,0,2);
    }
    return v;
}

    
/*
** Do a linear combination of n vectors. Returned is the vector
**
**       sum_{i=0}^{n-1} a[i] * v[i]
**
*/
Vector VVlcN( n, v, a)
int n;
Vector v[];
Scalar a[];
{
    Scalar x, y, z;
    Space S = SpaceOf(v[0]);
    int i, dim = Dim(SpaceOf(v[0]));

    /* For efficiency, appeal to coordinates */
    x = 0.0;  y = 0.0;  z = 0.0;
    for (i = 0; i < n; i++) {
	if (SpaceOf(v[i]) != S) {
	    (*AffineError)("VVlcN: vectors not from a common space.\n");
	}
	x += a[i] * ME(v[i].v,0,0);
	y += a[i] * ME(v[i].v,0,1);
	if (dim == 3) {
	    z += a[i] * ME(v[i].v,0,2);
	}
    }
    return VCreate( StdFrame(S), x, y, z);
}


/*
** Return the difference of vectors: v1 - v2;
*/
Vector VVDiff( v1, v2)
Vector v1, v2;
{
    Vector v;

    if (SpaceOf(v1) != SpaceOf(v2)) {
	(*AffineError)("VVDiff: vectors from different spaces.");
    }
    SpaceOf(v) = SpaceOf(v1);
    v.v = MatrixCreate( 1, Dim(SpaceOf(v1)) + 1);
    ME(v.v,0,0) = ME(v1.v,0,0) - ME(v2.v,0,0);
    ME(v.v,0,1) = ME(v1.v,0,1) - ME(v2.v,0,1);
    if (Dim(SpaceOf(v)) == 3) {
	ME(v.v,0,2) = ME(v1.v,0,2) - ME(v2.v,0,2);
    }
    return v;
}



/*
** Return the geometric dot product of v1 and v2.
*/
Scalar VVDot( v1, v2)
Vector v1, v2;
{
	Scalar dot;

	if (SpaceOf(v1) != SpaceOf(v2)) {
		(*AffineError)("VVDot: vectors from different spaces.");
	}
	dot = ME(v1.v,0,0)*ME(v2.v,0,0) + ME(v1.v,0,1)*ME(v2.v,0,1);
	if (Dim(SpaceOf(v1)) == 3) {
		dot += ME(v1.v,0,2)*ME(v2.v,0,2);
	}
	return dot;
}

	
/*
** Return the vector given by v1 cross v2.
*/
Vector VVCross( v1, v2)
Vector v1, v2;
{
	Vector v;
	
	if (SpaceOf(v1) != SpaceOf(v2)) {
		(*AffineError)("VVCross: vectors from different spaces.");
	}
	if (Dim(SpaceOf(v1)) != 3) {
		(*AffineError)("VVCross: vectors must be from a 3-space.");
	}
	SpaceOf(v) = SpaceOf(v1);
	v.v = MatrixCreate( 1, Dim(SpaceOf(v1)) + 1);
	ME(v.v,0,0) = ME(v1.v,0,1)*ME(v2.v,0,2) - ME(v1.v,0,2)*ME(v2.v,0,1);
	ME(v.v,0,1) = ME(v1.v,0,2)*ME(v2.v,0,0) - ME(v1.v,0,0)*ME(v2.v,0,2);
	ME(v.v,0,2) = ME(v1.v,0,0)*ME(v2.v,0,1) - ME(v1.v,0,1)*ME(v2.v,0,0);
	
	return v;
}


/*
** Return the orthogonal projection of v onto w.
*/
Vector VVProj( v, w)
Vector v, w;
{
    Scalar factor;

    factor = VVDot( v, w) / VVDot( w, w);
    return SVMult( factor, w);
}


/*
** Turn the regular vector into a normal.  In vector space
** terminology, v is mapping to a linear functional according
** to the rule v --> phi_v(w) = (v .)(w), where (.) is the dot
** product.  Normals are represented as column matrices.
*/
Normal VDual(v)
Vector v;
{
	Normal phi;
	
	SpaceOf(phi) = SpaceOf(v);
	phi.n = MatrixTranspose( v.v);
	
	return phi;
}


/*
** Turn the normal vector into a regular vector.
*/
Vector NDual(phi)
Normal phi;
{
	Vector v;
	
	SpaceOf(v) = SpaceOf(phi);
	v.v = MatrixTranspose( phi.n);

	return v;
}


/*
** Apply a linear functional (ie a normal vector) to a
** regular vector.  
*/
Scalar NVApply( phi, v)
Normal phi;
Vector v;
{
    Matrix M;

    if (SpaceOf(phi) != SpaceOf(v)) {
	(*AffineError)("NVApply: vector and normal not from a common space.");
    }
    M = MatrixMult( v.v, phi.n);
    return ME(M, 0, 0);
}
	

/*
** Normalize the given vector.
*/
Vector VNormalize( v)
Vector v;
{
	Scalar mag = VMag(v);
	
	if (mag < EPSILON) {
		(*AffineError)("VNormalize: can't normalize zero vector");
	}
	
	return SVMult( 1.0/mag, v);
}

/*--------------------------------------------------------------*/
/*			Transformation Routines			*/
/*--------------------------------------------------------------*/

/*
** Return the point p transformed by transformation T.
*/
Point PAxform( p, T)
Point p;
AffineMap T;
{
	Point pp;			/* pp = T(p) = */
	
	if (SpaceOf(p) != T.domain) {
		(*AffineError)("PAxform: point not in domain space.");
		return pp;
	}
	SpaceOf(pp) = T.range;
	pp.p = MatrixMult( p.p, T.t);
#ifdef DEBUG
	fprintf(stderr, "AffineMap: ");
	PPrintf(stderr, p);
	fprintf(stderr, " --> ");
	PPrintf(stderr, pp);
	fprintf(stderr, "\n");
#endif
	return pp;
}


/*
** Return the vector v transformed by transformation T.
*/
Vector VAxform( v, T)
Vector v;
AffineMap T;
{
	Vector vv;			/* vv = T(v) = */
	
	if (SpaceOf(v) != T.domain) {
		(*AffineError)("VAxform: vector not in domain space.");
		return vv;
	}
	SpaceOf(vv) = T.range;
	vv.v = MatrixMult( v.v, T.t);
#ifdef DEBUG
	fprintf(stderr, "AffineMap: ");
	VPrintf(stderr, v);
	fprintf(stderr, " --> ");
	VPrintf(stderr, vv);
	fprintf(stderr, "\n");
#endif
	return vv;
}



/*
** Return the linear functional (ie, normal vector) transformed by
** transformation T.
*/
Normal NAxform( n, T)
Normal n;
AffineMap T;
{
	Normal nn;
	
	if (SpaceOf(n) != T.domain) {
		(*AffineError)("NAxform: normal not in domain space.");
		return nn;
	}
	SpaceOf(nn) = T.range;

	/* Normals transform by multiplication on the left by the   */
	/* inverse of the matrix that transforms vectors and points.*/
	if (!T.invertible) {
	  (*AffineError)
	    ("NAxform: attempt to map a normal through a non-invertible map.");
	}
	nn.n = MatrixMult( T.invt, n.n);

	/* Only the linear portion of T should have been inverted.     */
	/* To compensate, we must zero out the last component manually.*/
	switch (Dim(SpaceOf(nn))) {
	  case 2:
	    ME(nn.n,2,0) = 0.0;
	    break;
	  case 3:
	    ME(nn.n,3,0) = 0.0;
	    break;
	}

#ifdef DEBUG
	fprintf(stderr, "AffineMap: ");
	NPrintf(stderr, n);
	fprintf(stderr, " --> ");
	NPrintf(stderr, nn);
	fprintf(stderr, "\n");
#endif
	return nn;
}



/*--------------------------------------------------------------*/
/*			Utility Routines			*/
/*--------------------------------------------------------------*/


F_Printf( fp, F)
FILE *fp;
Frame F;
{
	fprintf( fp, "Frame:\n");
	VPrintf( fp, FV(F,0));
	VPrintf( fp, FV(F,1));
	if (Dim(SpaceOf(F))==3) VPrintf( fp, FV(F,2));
	PPrintf( fp, FOrg(F));
}

PPrintf( fp, p)
FILE *fp;
Point p;
{
	fprintf(fp, "Point: space: %s ", Name(SpaceOf(p)));
	MatrixPrint(p.p, fp);
}

VPrintf( fp, v)
FILE *fp;
Vector v;
{
	fprintf(fp, "Vector: space: %s ", Name(SpaceOf(v)));
	MatrixPrint(v.v, fp);
}

APrintf( fp, T)
FILE *fp;
AffineMap T;
{
	fprintf( fp, "AffineMap: domain: %s, range: %s\n",
				 Name(T.domain), Name(T.range));
	MatrixPrint( T.t, fp);
}

