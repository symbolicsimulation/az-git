/*
** sgp.c : A (very) simple graphics package based on geometric ADT.
**
** Copyright (c) 1987, 1988 Tony D. DeRose
*/
#include <stdio.h>
#include "device.h"
#include "sgp.h"

/* Imports */
extern double fabs();

/* Forward decl's */
static void BuildTransform();
static int ClipLine3D(), ClipLineAgainstHyperplane();
static void BuildViewBox();
extern Hyperplane CreateHyperplane();
extern Scalar EvalHyperplane();

/* Exported variables */
Space World;		            /* The model world.                    */
Space Screen;		            /* The Screen (plays role of NDC)      */

/* Local variables */
static int IsViewingReady=FALSE;    /* True if view pipeline is initialized*/
static AffineMap ViewTransform;	    /* Transforms World |--> Screen        */
static Point Eye;		    /* The position of the Eye.            */
static Vector ViewDir, UpVector;    /* The viewing direction and up vector */
static Frame ViewFrame;		    /* The normalized viewing frame.       */
static Scalar WinHsize, WinVsize;   /* Horiz and Vert size of window.      */
static Scalar Hither=1.0;	    /* Distance to front clipping plane.   */
static Scalar Yon=500.0;	    /* Distance to back clipping plane.    */
static Point VPorigin;		    /* The center of the viewport.         */
static Vector VPx,VPy;		    /* Vectors from VPorigin to the horiz  */
				    /* and vertical boundaries of viewport.*/
static Hyperplane VBTop, VBBottom,  /* Planar boundaries of the viewing box*/
	     VBLeft, VBRight,
	     VBFront, VBBack;


	/*--------------------------------------------------*/
	/*		   Initialize/Exit		    */
	/*--------------------------------------------------*/
	
/*
** Initialize the graphics package.
*/
void SgpInit()
{
	/* Create the world and Screen spaces */
	Screen = SCreate( "Screen", TWOSPACE);
	World  = SCreate( "World",  THREESPACE);
		
	/* Initialize the frame buffer device */
	DeviceInitialize();
	
}	


/*
** De-select the graphics package in preparation for exit.
*/
void SgpExit()
{
	DeviceTerminate();
}

	/*--------------------------------------------------*/
	/*	     Viewing Parameter Specification	    */
	/*--------------------------------------------------*/

/*
** Set the eye point to the given point in the world.
*/
void SgpSetEye( eye)
Point eye;
{
	IsViewingReady = FALSE;
	Eye = eye;
}

/*
** Set the viewing direction to be along the given vector
*/
void SgpSetViewDirection( v)
Vector v;
{
	IsViewingReady = FALSE;
	ViewDir   =  v;
}

/*
** Set the up vector to the given vector.
*/
void SgpSetUpVector( U)
Vector U;
{
	IsViewingReady = FALSE;
	UpVector   = U;
}


/*
** Set the viewport to be the rectangle extending from
** Xndc in [vpleft,vpright] and Yndc in [vpbottom,vptop].
**
** The viewport is modeled here by setting up a ``viewport''
** coordinate frame in the Screen space.  The origin of
** this frame is the center of the viewport.  The x and y
** vectors reach from the origin to the horizontal and 
** vertical boundaries, respectively.  This choice of
** coordinate systems makes it easy to construct the orthogonal
** transformation between the world and Screen spaces.
*/
void SgpSetViewPort( vpleft, vpright, vpbottom, vptop)
Scalar vpleft, vpright, vpbottom, vptop;
{
	Scalar vphsize, vpvsize, vpcenterx, vpcentery;
	
	IsViewingReady = FALSE;

	vphsize = vpright - vpleft;
	vpvsize = vptop - vpbottom;
	vpcenterx = (vpright+vpleft)/2.0;
	vpcentery = (vptop+vpbottom)/2.0;

	VPx      = VCreate( StdFrame(Screen), vphsize/2.0, 0.0);
	VPy      = VCreate( StdFrame(Screen), 0.0, vpvsize/2.0);
	VPorigin = PCreate( StdFrame(Screen), vpcenterx, vpcentery);

}


/*
** Set the horizontal and veritcal dimensions of the window on
** the projection plane.
*/
void SgpSetWindow( hsize, vsize)
Scalar hsize, vsize;
{
	IsViewingReady = FALSE;

	WinHsize = hsize;
	WinVsize = vsize;
}


	/*--------------------------------------------------*/
	/*	          Viewing Pipeline		    */
	/*--------------------------------------------------*/

/*
** Draw a 3 dimensional line segment between the two points
** p1 and p2.
*/
void SgpDrawLine( p1, p2)
Point p1, p2;
{
	Point clip1, clip2, devp1, devp2;

	if (!IsViewingReady) {
		BuildViewing();
	}

	/* Clip the line segment */
	if (ClipLine3D( p1, p2, &clip1, &clip2)) {

		/* Transform to Screen Space */
		devp1 = PAxform( clip1, ViewTransform);
		devp2 = PAxform( clip2, ViewTransform);
		DeviceDrawLine( devp1, devp2);
	}
}


/*
** Build all structures necessary for sending objects
** down the viewing pipeline.
*/
BuildViewing()
{
	BuildTransform();
	BuildViewBox();
	IsViewingReady = TRUE;
}

	
/*
** Build the world to Screen transformation.  This transformation
** is currently an affine transformation that carries the origin
** of the ViewFrame to the origin of the viewport frame, the x-vector
** of the ViewFrame onto the x-vector of the viewport, and the
** y-vector of the ViewFrame onto the y-vector of the viewport.  The
** z-vector of the ViewFrame is mapped to the zero vector in the Screen
** space.
*/
static void BuildTransform()
{
	Vector ViewX;

	/*-------------------------------------------------*/
	/* Create the normalized viewing coordinate frame. */
	/*-------------------------------------------------*/
	ViewDir   = VNormalize( ViewDir);
	ViewX     = VNormalize(VVCross( ViewDir, UpVector));
	UpVector  = VVCross( ViewX, ViewDir);

	ViewFrame = FCreate( "View", Eye, SVMult(WinHsize/2.0, ViewX), 
				  SVMult(WinVsize/2.0, UpVector),
				  SVMult(Hither, ViewDir));

	/* Create the transformation into the Screen space */
	ViewTransform = ACreate( ViewFrame, VPorigin, VPx, VPy, VZero(Screen));
}


/*
** Build the viewing box by representing the boundary planes.
** The viewing box is a rectangular block whose planes are
** aligned with the ViewFrame axes.
*/
static void BuildViewBox()
{
	Point Winul, Winur, Winlr, Winll;
	Point Up, Down, Left, Right;
	
	/*---------------------------------------------------------*/
	/* Create points in the xy ViewFrame plane that lie on the */
        /* top, bottom, left, and right clipping planes.           */
	/*---------------------------------------------------------*/
	Up    = PCreate( ViewFrame, 0.0, 1.0, 0.0);
	Down  = PCreate( ViewFrame, 0.0,-1.0, 0.0);
	Left  = PCreate( ViewFrame,-1.0, 0.0, 0.0);
	Right = PCreate( ViewFrame, 1.0, 0.0, 0.0);

	/*---------------------------------------------------------*/
	/* Create points at the corners of the window on the front */
        /* clipping plane.                                         */
	/*---------------------------------------------------------*/
	Winul = PCreate( ViewFrame, -1.0, 1.0, 1.0);
	Winur = PCreate( ViewFrame,  1.0, 1.0, 1.0);
	Winll = PCreate( ViewFrame, -1.0,-1.0, 1.0);
	Winlr = PCreate( ViewFrame,  1.0,-1.0, 1.0);
	

	/*--------------------------------------------------------*/
	/* Construct the clipping planes                          */
	/*--------------------------------------------------------*/
	VBTop    = CreateHyperplane( Up, Winur, Winul);
	VBBottom = CreateHyperplane( Down, Winll, Winlr);
	VBLeft   = CreateHyperplane( Left, Winul, Winll);
	VBRight  = CreateHyperplane( Right, Winlr, Winur);
	VBFront  = CreateHyperplane( Winur, Winul, Winll);

	/*--------------------------------------------------------*/
	/* The back plane is at depth Yon, but View_z is of length*/
	/* Hither, so the back plane is Yon/Hither units in the   */
	/* View_z direction.                                      */
	/*--------------------------------------------------------*/
	VBBack.b = PCreate( ViewFrame, 0.0, 0.0, Yon/Hither);
	VBBack.n = NCreate( ViewFrame, 0.0, 0.0, 1.0);
}


/*
** Do 3-D line clipping to the view box.  Return TRUE if a portion
** of the segment survives the clipping, FALSE otherwise.
*/
static int ClipLine3D( p1, p2, cp1, cp2)
Point p1, p2;
Point *cp1, *cp2;
{
	if (!ClipLineAgainstHyperplane( VBTop,    &p1, &p2)) {
		return FALSE;
	}
	if (!ClipLineAgainstHyperplane( VBBottom, &p1, &p2)) {
		return FALSE;
	}
	if (!ClipLineAgainstHyperplane( VBLeft,   &p1, &p2)) {
		return FALSE;
	}
	if (!ClipLineAgainstHyperplane( VBRight,  &p1, &p2)) {
		return FALSE;
	}
	if (!ClipLineAgainstHyperplane( VBFront,  &p1, &p2)) {
		return FALSE;
	}
	if (!ClipLineAgainstHyperplane( VBBack,   &p1, &p2)) {
		return FALSE;
	}

	*cp1 = p1;
	*cp2 = p2;

	return TRUE;
}


/*
** Clip a line segment against the given Hyperplane.  Return TRUE if
** a portion of the segment survives the clipping; return FALSE
** otherwise.
*/
static int ClipLineAgainstHyperplane( P, p1, p2)
Hyperplane P;
Point *p1, *p2;
{
	Point lp1, lp2;		/* Local copies of endpoints. */
	Scalar pp1, pp2;	/* Value of plane equation at p1 and p2 */
	Point intersect;	/* Point of intersection. */
	
	lp1 = *p1;
	lp2 = *p2;

	pp1 = EvalHyperplane( P, lp1);
	pp2 = EvalHyperplane( P, lp2);
	
	if ((pp1 > 0) && (pp2 > 0)) {
		/* Both points are outside --- trivial reject */
		return FALSE;
	}
	if ((pp1 < 0) && (pp2 < 0)) {
		/* Both points are inside --- trivial accept */
		return TRUE;
	}		
	/*--------------------------------------------*/
	/* The line segment crosses the Hyperplane.   */
	/* In fact, the Hyperplane cuts the line into */
	/* ratios |pp1| to |pp2|. This is used        */
	/* to compute the point of intersection.      */
	/*--------------------------------------------*/
	intersect = PPrr( lp1, lp2, fabs(pp1), fabs(pp2));
	
	/* Figure out which endpoint to throw out */
	if (pp1 < 0) {
		/* Throw out lp2 */
		*p1 = lp1;
		*p2 = intersect;
	} else {
		/* Throw out lp1 */
		*p1 = intersect;
		*p2 = lp2;
	}
	return TRUE;
}

		
	/*------------------------------------------------------*/
	/*	Routines to implement oriented Hyperplanes.     */
	/*------------------------------------------------------*/


/*
** Return the Hyperplane containing the points p1, p2, p3.  The
** normal to the Hyperplane is computed using the right hand rule.
** That is, if the fingers of the right hand are curled around
** the points p1, p2, p3, in that order, then the thumb will
** point in the direction of the normal.
*/
Hyperplane CreateHyperplane( p1, p2, p3)
Point p1, p2, p3;
{
	Hyperplane W;
	
	W.b = p1;
	W.n = VDual( VVCross( PPDiff(p2,p1), PPDiff(p3,p1)));
	
	return W;
}

/*
** Evaluate the affine functional associated with hyperplane P at point q.
*/
Scalar EvalHyperplane( P, q)
Hyperplane P;
Point q;
{
	return NVApply( P.n, PPDiff( q, P.b));
}
