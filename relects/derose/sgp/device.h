/*
** device.h : Definitions for device module interface.
**
** Copyright (c) 1987, Tony D. DeRose
*/

extern void DeviceInitialize();
extern void DeviceClear();
extern void DeviceDrawLine();
extern void DeviceTerminate();
extern void DeviceWritePixel();


