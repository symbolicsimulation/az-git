/*
   File: lib457.c
   Authors: Tony DeRose,
            K.R. Sloan
   Last Modified: 6 October 1988
   Purpose: Library of routines for CSci 457, Fall '88
 */

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "framebuffer.h"
#include "image.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

XWMHints xwmh =
 {
  (InputHint|StateHint),    /* flags */
  False,                    /* input */
  NormalState,              /* initial state */
  0,                        /* icon pixmap */
  0,                        /* icon window */
  0, 0,                     /* icon location */
  0,                        /* icon mask  */
  0                         /* Window group */      
 };

/* X-Framebuffer definitions */
#define fb_NAME "FrameBuffer"
#define fb_FONT "6x13"

/* Variables local to the library */
int WindowOpened = FALSE;         /* TRUE if the window is opened */
static Display *dpy ;             /* The display to post the window to */
static Window win;                /* Window descriptor of framebuffer */
static GC gc;                     /* GC to draw with */        
static XFontStruct *fontstruct;   /* font descriptor */
static XEvent event;              /* Event received */
static XSizeHints xsh;            /* Size hints for window manager */
static char *geomspec;            /* Window geometry string */
static XSetWindowAttributes xswa; /* Temporary Set Window Attribute */
static char Header[18] = 
 {        /* Header for targa files - my isn't this informative */
  0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 0
 };

/* Exported variables */
char *fb_error_message = NULL;
  
/*------------------------------------------------------------*/
/*                                                            */
/*               X Framebuffer Routines                       */
/*                                                            */
/*------------------------------------------------------------*/

/*
** Create the frame buffer window, display it on the screen,
** and return the range of its device coordinates.
*/
int fb_open( minX, minY, maxX, maxY)
 int *minX, *minY, *maxX, *maxY;
 {
  unsigned long fg, bg, bd;   /* pixel values */
  unsigned long bw;           /* border width */ 
  unsigned long pad;          /* padding for text */
  XGCValues gcv;              /* Struct for creating GC */
  XWindowAttributes xwa;      /* for querying window */
  
  /*
    Open the display, using the DISPLAY environment variable
   */
  if (WindowOpened) 
   {
    perror("fb_open: attempt to open the already opened framebuffer.\n");
    return(FB_ERROR);
   }
  dpy = XOpenDisplay((char *)0);
  if ((Display *)0 == dpy)
   {
    fb_error_message = "fb_open: couldn't open the display.";
    return(FB_ERROR);
   }

  /*
     Load a font
   */
  fontstruct = XLoadQueryFont(dpy, fb_FONT); 
  
  /*
    Select colors
   */
  bd = BlackPixel(dpy,DefaultScreen(dpy));
  bg = WhitePixel(dpy,DefaultScreen(dpy));
  fg = BlackPixel(dpy,DefaultScreen(dpy));

  /*
    set border width and pad
   */
  pad = 1; bw = 1;
  
  /*
    provide hints for initial position and size
   */

  xsh.flags = (PPosition | PSize | PMinSize | PMaxSize | PResizeInc);
  xsh.height = 240;
  xsh.width  = 320;
  xsh.x = (DisplayWidth (dpy, DefaultScreen(dpy)) - xsh.width ) / 2;
  xsh.y = (DisplayHeight(dpy, DefaultScreen(dpy)) - xsh.height) / 2;
  xsh.min_width = 320;  xsh.min_height = 240;
  xsh.max_width = 1280;  xsh.max_height = 920;
  xsh.width_inc = 16;    xsh.height_inc = 12;
  
  /*
    Create the window
   */
  win = XCreateSimpleWindow( dpy, DefaultRootWindow(dpy),
            xsh.x, xsh.y, xsh.width, xsh.height,
            bw, bd, bg);
	    
  /*
    Set standard properties for the window managers
   */
  XSetStandardProperties(dpy, win, fb_NAME, fb_NAME, None, "", 0, &xsh);
  XSetWMHints(dpy, win, &xwmh);

  /*
    make sure we have a colormap
    set bit gravity to minimize Expose events
   */
  xswa.colormap = DefaultColormap(dpy, DefaultScreen(dpy));
  xswa.bit_gravity = CenterGravity;
  XChangeWindowAttributes(dpy, win, (CWColormap | CWBitGravity), &xswa);
 
  /*
    create Graphics Context
   */
  gcv.font = fontstruct->fid;
  gcv.foreground = fg;
  gcv.background = bg;
  gc = XCreateGC(dpy, win, (GCFont | GCForeground | GCBackground), &gcv);
 
  /*
    allow Exposure events    
   */

  XSelectInput(dpy, win, ExposureMask);
 
  /*
    map window to make it visible
   */

  XMapWindow(dpy, win);

  /*
    wait for an exposure event
   */
  while(1)
   {
    XNextEvent(dpy, &event);
    if (  (Expose == event.type) && (0 == event.xexpose.count) )
     {
      while (XCheckTypedEvent(dpy, Expose, &event)); /* get them all */
      break;
     }
   }
 
  /*
    refuse all events
   */
  XSelectInput(dpy,win,NoEventMask);
  
  /*
    Find out what size the window ended up begin set to 
   */
  if (0 == XGetWindowAttributes(dpy,win,&xwa))
   {
    fb_error_message = "Couldn't query the frame buffer window.";
    return FB_ERROR;
   }

  /*
    Set the output parameters 
   */
  *minX = 0;
  *minY = 0;
  *maxX = xwa.width  - 1;
  *maxY = xwa.height - 1;

  /*
    Everything went well...
   */
  WindowOpened = TRUE;
  return FB_SUCCESS;
 }


/*
  Destroy the frame buffer window.
 */
int fb_close()
 {
  if (!WindowOpened) 
   {
    fb_error_message = 
        "fb_close: attempt to close already closed framebuffer.";
    return FB_ERROR;
   }

  XDestroyWindow(dpy,win);
  WindowOpened = FALSE;
  XSync(dpy,1);
  return FB_SUCCESS;
 }

/*
   Write a pixel in the given color.
 */
fb_writePixel( x, y, color)
 int x, y;
 COLOR color;
 {
  if (!WindowOpened) 
   {
    fb_error_message = "fb_writePixel: window not opened.";
    return FB_ERROR;
   }
  switch (color) 
   {
    case WHITE:
     XSetForeground(dpy, gc, WhitePixel(dpy,DefaultScreen(dpy)));
     break;
    case BLACK:
     XSetForeground(dpy, gc, BlackPixel(dpy,DefaultScreen(dpy)));
     break;
    default:
     fb_error_message = "fb_writePixel: unknown color.";
     return FB_ERROR;
   }
  XDrawPoint(dpy, win, gc, x, y);
  return FB_SUCCESS;
 }

/*
   Clear the entire framebuffer window
 */
fb_clear()
 {
  if (!WindowOpened) 
   {
    fb_error_message = "fb_clear: window not opened.";
    return FB_ERROR;
   }
  XClearWindow(dpy,win);
  return FB_SUCCESS;
 }

/*
  Clear a portion of the framebuffer window
 */
fb_clearRectangle( x, y, xsize, ysize)
 int x, y, xsize, ysize;
 {
  if (!WindowOpened) 
   {
    fb_error_message = "fb_clearRectangle: window not opened.";
    return FB_ERROR;
   }
  XClearArea(dpy, win, x, y, xsize, ysize, False);
  fb_flush();
  return FB_SUCCESS;
 }


/*
  Draw a line in the given color.
 */
fb_drawLine(x1, y1, x2, y2, color)
 int x1, y1, x2, y2;
 COLOR color;
 {
  switch (color) 
   {
    case WHITE:
     XSetForeground(dpy, gc, WhitePixel(dpy,DefaultScreen(dpy)));
     break;
    case BLACK:
     XSetForeground(dpy, gc, BlackPixel(dpy,DefaultScreen(dpy)));
    break;
   }
  XDrawLine(dpy, win, gc, x1, y1, x2, y2);
  fb_flush();
 }


/*
  Draw some text at the given position.
 */
fb_text( x, y, str)
 int x, y;
 char *str;
 {
  if (!WindowOpened) 
   {
    fb_error_message = "fb_text: window not opened.";
    return FB_ERROR;
   }

  XSetForeground(dpy, gc, BlackPixel(dpy,DefaultScreen(dpy)));
  XDrawString(dpy, win, gc, x, y, str, strlen(str));
  fb_flush();
  return FB_SUCCESS;
 }

fb_flush()
 {
  XFlush(dpy);    
  return(FB_SUCCESS);
 }
 

/*------------------------------------------------------------*/
/*                                                            */
/*               Image File Interface Routines                */
/*                                                            */
/*------------------------------------------------------------*/

IMAGE *ImCreate( xResolution, yResolution)
 int xResolution, yResolution;
 {
  extern char *malloc();
  IMAGE *newImage;
  
  newImage = (IMAGE *) malloc(sizeof(IMAGE));
  if ((IMAGE *)0 == newImage)  return(newImage);

  /* Fill in the fields of the image descriptor. */
  newImage->xres = xResolution;
  newImage->yres = yResolution;
  newImage->im = (char *)malloc(xResolution * yResolution * 4);

  if ((char *) 0 == newImage->im)
   {
    (void) free(newImage);
    newImage = (IMAGE *)0;
    return(newImage);
   }
  return(newImage);
 }


int WriteTargaHeader(targaFile, xsize, ysize)
 FILE *targaFile;
 int xsize, ysize;
 {
  /* The xsize and ysize fields are the only ones needed */
  Header[12] = (char) (xsize & 0xff);
  Header[13] = (char) (xsize >> 8);
  Header[14] = (char) (ysize & 0xff);
  Header[15] = (char) (ysize >> 8);

  if (18 != fwrite(Header, 1, 18, targaFile))
   {
    fprintf(stderr, "WriteTargaHeader: Error writing header\n");
    exit(1); /* a bit presumptuous, but... */
   }
 }

/*
** Write out an image into a targa formatted file.
** NOTE: to use the display program on the AT, the
** filename must end in .tga.
*/
void ImWrite(image, filename)
 IMAGE *image;
 char *filename;
 {
  FILE *targaFile;
  int i;
  int CharsPerScanline;

  targaFile = fopen(filename, "w");
  WriteTargaHeader(targaFile, image->xres, image->yres);

  /* Write out a scan line at a time */
  /* Beware - this has been changed to put <0,0> at the lower left */
  CharsPerScanline = 4*(image->xres);
  for (i = 0; i<(image->yres); i++) 
   {
    if (   CharsPerScanline
        != fwrite((image->im)+(i*CharsPerScanline), 
                  1, CharsPerScanline, targaFile) )
     {
      fprintf(stderr,"ImWrite: error writing scanline %d\n", i);
      exit(1); /* a bit persumptious, but... */
     }
   }
  fclose(targaFile);
 }

void ImSetPixel(image, x, y, red, green, blue)
 IMAGE *image;
 int x, y;
 int red, green, blue;
 {
  char *p;

  p = (image->im) + (4 * (x + (y*(image->xres))));
  *p++ = blue;
  *p++ = green;
  *p++ = red;
  *p   = 0; 
 }
