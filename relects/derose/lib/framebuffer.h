/*
   File: framebuffer.h
   Authors: Tony DeRose,
            K.R. Sloan
   Last Modified: 16 September 1988
   Purpose: Include file for use with lib457.a
*/

#ifndef min
#define min(a,b)        (((a)<(b))?(a):(b))
#define max(a,b)        (((a)<(b))?(b):(a))
#endif

/* Error Codes */
#define FB_ERROR        (-1)
#define FB_SUCCESS      0

/* Predefined Colors */
#define WHITE           0
#define BLACK           1

/* Color type (just an int for now) */
typedef int COLOR;

/* External definitions */
extern int fb_open(), fb_close(), fb_writePixel(), fb_flush();
extern char *fb_error_message;
