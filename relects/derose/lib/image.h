/*
   File: image.h
   Authors: Tony DeRose,
            K.R. Sloan
   Last Modified: 20 October 1988
   Purpose: Simple image file interface for targa images.
 */

typedef struct 
 {
  int xres, yres;        /* Number of bits in x and y directions */
  char *im;              /* The 2D array of pixel data */
 } IMAGE;

/* image = ImCreate(xsize, ysize) creates, allocates, and clears an image */
extern IMAGE *ImCreate();
/* ImWrite(image,filename) writes out the image in ATT Targa format */
extern void ImWrite();
/* ImSetPixel(image, x, y, r, g, b) writes a pixel value */
extern void ImSetPixel();
