;;;-*- Package: (CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package 'CM-User :use '(Lisp *Lisp))

;;;============================================================

(defparameter *animate-gradient-steps?*  t)

;;;============================================================

#+*lisp-simulator
(setq *lisp::*do-not-print-anything-while-*cold-booting* t)

;;;============================================================

(eval-when (compile load eval)
  (deftype Processor-Cube-Address ()
    '(Field-Pvar *log-number-of-processors-limit*)))

;;;============================================================
;;; A collection of objects represented by a *defstruct pvar.
;;; Each processor for which the member?!! predicate is true
;;; represents one element of the collection.

(*defstruct (Parallel-Collection
	      (:conc-name nil))
  (member? nil :type Boolean))

;;;============================================================

(*defstruct (Single
	      (:include Parallel-Collection))
  "An object to layout on the screen."
  (x      0.0 :type Single-Float)
  (y      0.0 :type Single-Float)
  (grad-x 0.0 :type Single-Float)
  (grad-y 0.0 :type Single-Float)
  (loss   0.0 :type Single-Float))

;;;============================================================

(*defstruct (Pair
	      (:include Parallel-Collection))
  "A pair of objects to layout on the screen."
  ;; abstract properties:
  (dist   0.0 :type Single-Float)
  (weight 0.0 :type Single-Float)
  (order  0.0 :type Single-Float)

  ;; computed properties as laid out on the screen
  (dx           0.0 :type Single-Float)
  (dy           0.0 :type Single-Float)
  (2d-dist      0.0 :type Single-Float)

  ;; a copy of the 0th single
  (single-address0 0 :type (Unsigned-Byte 32) :cm-type Processor-Cube-Address)
  (x0      0.0 :type Single-Float)
  (y0      0.0 :type Single-Float)
  (new-x0  0.0 :type Single-Float)
  (new-y0  0.0 :type Single-Float)
  (grad-x0 0.0 :type Single-Float)
  (grad-y0 0.0 :type Single-Float)

  ;; a copy of the 1st single
  (single-address1 0 :type (Unsigned-Byte 32) :cm-type Processor-Cube-Address)
  (x1      0.0 :type Single-Float)
  (y1      0.0 :type Single-Float)
  (new-x1  0.0 :type Single-Float)
  (new-y1  0.0 :type Single-Float)
  (grad-x1 0.0 :type Single-Float)
  (grad-y1 0.0 :type Single-Float))

;;;============================================================
;;; distances
;;;============================================================

(*proclaim
  '(function l2-norm!! (Single-Float-Pvar Single-Float-Pvar)
	     Single-Float-Pvar))

(*defun l2-norm!! ($x $y)
  (declare (type Single-Float-Pvar $x $y))
  (sqrt!! (+!! (*!! $x $x) (*!! $y $y))))

(*proclaim
  '(function l2-dist!! ( Single-Float-Pvar Single-Float-Pvar
			Single-Float-Pvar Single-Float-Pvar)
	     Single-Float-Pvar))

(*defun l2-dist!! ($x0 $y0 $x1 $y1)
  (declare (type Single-Float-Pvar $x0 $y0 $x1 $y1))
  (*let (($dx (-!! $x0 $x1))
	 ($dy (-!! $y0 $y1)))
    (l2-norm!! $dx $dy)))

;;;==================================================================
;;; associate a processor with some object on the front end
;;;==================================================================

(defvar *processor-table* (make-hash-table))
(defvar *next-free-cube-address* 0 "Used to allocate processors.")

(defun initialize-processor-table ()
  (clrhash *processor-table*)
  (setf *next-free-cube-address* 0))

(defun allocate-processor-cube-address (object)
  (prog1
    *next-free-cube-address*
    (if (> *next-free-cube-address* *number-of-processors-limit*)
	(error "Allocating too many processors.")
	(progn
	  (setf (gethash object *processor-table*) *next-free-cube-address*)
	  (incf *next-free-cube-address*)))))

(defun allocate-processor-cube-addresses (v)
  (let ((old-cube-address *next-free-cube-address*))
    (incf *next-free-cube-address* (length v))
    (if (> *next-free-cube-address* *number-of-processors-limit*)
	(error "Allocating too many processors.")
	;; else
	(let ((p old-cube-address))
	  (map nil #'(lambda (object)
		       (setf (gethash object *processor-table*) p)
		       (incf p))
	       v)))
    (values old-cube-address *next-free-cube-address*)))

(defun processor-cube-address (object) (gethash object *processor-table*))
  
;;;============================================================
;;; Basic Layout operations
;;;============================================================

(*defun *initialize-cm-for-layout (layout $pairs $singles)
  (declare (type lay:Layout-controller layout)
	   (type (pvar Pair) $pairs)
	   (type (pvar Single) $singles))
  (initialize-processor-table)
  ;; assign processors to singles
  (multiple-value-bind
    (singles-start singles-end)
      (allocate-processor-cube-addresses (lay:singles layout))
    (*when (<=!! (the Processor-Cube-Address (!! singles-start))
		 (self-address!!)
		 (the Processor-Cube-Address (!! (- singles-end 1))))
      (*setf (member?!! $singles) t!!))
    (array-to-pvar (lay:x layout)  (alias!! (single-x!! $singles))
		   :cube-address-start singles-start
		   :cube-address-end singles-end)
    (array-to-pvar (lay:y layout) (alias!! (single-y!! $singles))
		   :cube-address-start singles-start
		   :cube-address-end singles-end)
    ;; assign processors to pairs
    (dolist (pair (lay:pairs layout))
      (let ((single0 (lay:pair-single0 pair))
	    (single1 (lay:pair-single1 pair))
	    (p (allocate-processor-cube-address pair)))
	(*setf (pref (member?!! $pairs) p) t)
	(*setf (pref (pair-single-address0!! $pairs) p)
	       (processor-cube-address single0))
	(*setf (pref (pair-single-address1!! $pairs) p)
	       (processor-cube-address single1))
	(let ((d (funcall (lay:dist-fn layout) single0 single1)))
	  (*setf (pref (pair-dist!! $pairs) p) d)
	  (*setf (pref (pair-weight!! $pairs) p)
		 (funcall (lay:weight-fn layout) single0 single1 d)))))
    (values (cons singles-start singles-end))))

;;;============================================================

(*defun *initialize-cm-for-ordered-layout (layout $pairs $singles)
  (declare (type Layout-Controller layout)
	   (type (pvar Pair) $pairs)
	   (type (pvar Single) $singles))
  (initialize-processor-table)
  ;; assign processors to singles
  (multiple-value-bind
    (singles-start singles-end)
      (allocate-processor-cube-addresses (lay:singles layout))
    (*when (<=!! (the Processor-Cube-Address (!! singles-start))
		 (self-address!!)
		 (the Processor-Cube-Address (!! (- singles-end 1))))
      (*setf (member?!! $singles) t!!))
    (array-to-pvar
      (lay:x layout)  (alias!! (single-x!! $singles))
      :cube-address-start singles-start :cube-address-end singles-end)
    (array-to-pvar
      (lay:y layout) (alias!! (single-y!! $singles))
      :cube-address-start singles-start :cube-address-end singles-end)
    ;; assign processors to pairs
    (dolist (pair (lay:pairs layout))
      (let ((single0 (lay:pair-single0 pair))
	    (single1 (lay:pair-single1 pair))
	    (p (allocate-processor-cube-address pair)))
	(*setf (pref (member?!! $pairs) p) t)
	(*setf (pref (pair-single-address0!! $pairs) p)
	       (processor-cube-address single0))
	(*setf (pref (pair-single-address1!! $pairs) p)
	       (processor-cube-address single1))
	(let ((d (funcall (lay:dist-fn layout) single0 single1)))
	  (*setf (pref (pair-dist!!   $pairs) p) d)
	  (*setf (pref (pair-weight!! $pairs) p)
		 (funcall (lay:weight-fn layout) single0 single1 d)))
	(*setf (pref (pair-order!!  $pairs) p)
	       (funcall (lay:order-fn layout) single0 single1))))
    (values (cons singles-start singles-end))))

;;;============================================================
;;; un-directed pair version 

(*defun *collect-data-from-singles ($pairs $singles)

  (declare (type (pvar Pair) $pairs)
	   (type (pvar Single) $singles))

  (*when (member?!! $pairs)
    (*setf (pair-x0!! $pairs)
	   (pref!! (single-x!! $singles) (pair-single-address0!! $pairs)
		   :collision-mode :many-collisions))
    (*setf (pair-y0!! $pairs)
	   (pref!! (single-y!! $singles) (pair-single-address0!! $pairs)
		   :collision-mode :many-collisions))
    (*setf (pair-x1!! $pairs)
	   (pref!! (single-x!! $singles) (pair-single-address1!! $pairs)
		   :collision-mode :many-collisions))
    (*setf (pair-y1!! $pairs)
	   (pref!! (single-y!! $singles) (pair-single-address1!! $pairs)
		   :collision-mode :many-collisions))
    (*setf (pair-new-x0!! $pairs) (pair-x0!! $pairs))
    (*setf (pair-new-y0!! $pairs) (pair-y0!! $pairs))
    (*setf (pair-new-x1!! $pairs) (pair-x1!! $pairs))
    (*setf (pair-new-y1!! $pairs) (pair-y1!! $pairs))))

;;;============================================================

(*defun *compute-deltas ($pairs)

  (declare (type (pvar Pair) $pairs))

  (*when (member?!! $pairs)
    (*setf (pair-dx!! $pairs)
	   (-!! (pair-new-x0!! $pairs) (pair-new-x1!! $pairs)))
    (*setf (pair-dy!! $pairs)
	   (-!! (pair-new-y0!! $pairs) (pair-new-y1!! $pairs)))
    (*setf (pair-2d-dist!! $pairs)
	   (l2-norm!! (pair-dx!! $pairs) (pair-dy!! $pairs)))))

;;;============================================================

(*defun *compute-loss ($pairs)
  (declare (type (pvar Pair) $pairs)
	   (type Single-Float-Pvar $a $1-a))
  (*compute-deltas $pairs)
  (*when (member?!! $pairs)
    (*let* (($dl (-!! (pair-dist!! $pairs) (pair-2d-dist!! $pairs)))
	    ($w (pair-weight!! $pairs)))
      (declare (type Single-Float-Pvar $dl $w))
      (*sum (*!! $w $dl $dl)))))
                            
;;;============================================================

(*defun *compute-ordered-loss ($pairs $a $1-a)
  (declare (type (pvar Pair) $pairs)
	   (type Single-Float-Pvar $a $1-a))
  (*compute-deltas $pairs)
  (*when (member?!! $pairs)
    (*let* (($d (pair-dist!! $pairs))
	    ($dl (-!! $d (pair-2d-dist!! $pairs)))
	    ($w (pair-weight!! $pairs))
	    ($d-loss (*!! $w $dl $dl))
	    ($g (+!! $d (*!! (pair-order!! $pairs) (pair-dx!! $pairs))))
	    ($o-loss (*!! $w $g $g)))
      (declare (type Single-Float-Pvar $d $dl $w $d-loss $g $o-loss))
      (*sum (+!! (*!! $1-a $d-loss)
		 (*!! $a $o-loss))))))
                            
;;;============================================================

(pcl:defmethod lay::steepest-descent ((l lay:Point-Cloud-Layout))
  (if (and (pcl:slot-boundp l 'lay:lay:order-fn)
	   (not (null (lay:lay:order-fn l))))
      (gradient-ordered-layout l)
      (gradient-layout l)))

