;;; -*- Mode: LISP; Syntax: Common-lisp; Package: User; -*-

(in-package 'User)

(load "boo:>az>2dg>make")
(load-2dg)

(load-system 'starlisp-simulator-f14 :silent t :no-warn t) 
;;(compile-system 'starlisp-simulator-f14 :silent t :no-warn t)

(load "boo:>az>basic-math>minimize-1d")

(load ">az>clos>victoria-day>defsys")
(pcl::load-pcl)

#+symbolics (load ">az>clos>tools>7debug-patch")
(load ">az>clos>tools>additions")
(load ">az>clos>tools>copy")
#+symbolics (load ">az>clos>tools>grapher")
 
(defparameter *layout-directory* "boo:>az>layout>")

(defparameter *layout-files* 
	      '( ;; in Layout package
		"layout"
		;; in CM-User package
		"cm-defs"
		"cm-grad-step"
		;; in Layout package
		"examples>circles"
		;;"examples>etree"
		"examples>pedigree"
		))

(defun compile-layout ()
  (mapc 
    #'(lambda (f)
	(load 
	  (compile-file 
	    (concatenate 'String *layout-directory* f ".lisp"))))
    *layout-files*))

(defun load-layout ()
  (mapc 
    #'(lambda (f)
	(load 
	  (concatenate 'String *layout-directory* f)))
    *layout-files*))

(compile-layout) 