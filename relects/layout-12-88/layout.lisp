;;;-*- Package: (Layout :use (Lisp PCL) :nicknames (lay)); Syntax: Common-Lisp; Mode: Lisp; -*-

(in-package 'Layout :use '(Lisp PCL) :nicknames '(lay))

(export '( next-power-of-2 make-random-vector
	  trivial-weight standard-weight trivial-order
	  ;; plotting stuff
	  *debug* *plot-steps?* *plot-points?*
	  *canvas* *plot* *function-canvas* *function-plot*
	  get-canvas get-plot get-function-canvas get-function-plot
	  plot-xy plot-function
	  ;; simple pairs
	  make-pair pair-single0 pair-single1 collect-singles
	  collect-all-unordered-pairs collect-all-ordered-pairs
	  collect-singles-from-pairs
	  ;; classes 
	  Layout-Controller
	  canvas plot
	  Point-Cloud-Layout
	  singles x y dist-fn weight-fn order-fn pairs
	  points single-point-table
	  dx dy steps
	  cardinality nprocessors
	  plot-animation-step
	  Graph-Layout
	  make-edge-plot-objects
	  Directed-Graph-Layout
	  ))

;;;============================================================
;;; These don't really belong here:

(defun next-power-of-2 (n) (expt 2 (integer-length (1- n))))

(defun make-random-vector (n)
  (let ((v (make-array n)))
    (dotimes (i n) (setf (aref v i) (random 1.0)))
    v))

 ;;;============================================================

(defun trivial-weight (single0 single1 dist)
  (declare (ignore single0 single1 dist))
  1.0)

(defun standard-weight (single0 single1 dist)
  (declare (ignore single0 single1))
  (/ 1.0 dist))

(defun trivial-order (single0 single1) (declare (ignore single0 single1)) 0.0)

;;;============================================================

(defparameter *debug* nil)

;;;============================================================

(defparameter *plot-steps?* t)
(defparameter *plot-points?* t)

;;;============================================================

(defparameter *canvas* nil)
(defparameter *plot* nil)
(defparameter *function-canvas* nil)
(defparameter *function-plot* nil)

(defun get-canvas ()
  (when (null *canvas*) (setf *canvas* (2dp:make-canvas)))
  *canvas*)

(defun get-plot ()
  (when (null *plot*) (setf *plot* (2dp:make-plot)))
  (when *debug*
    (setf (2dp:tics-p *plot* :left) t)
    (setf (2dp:tics-p *plot* :bottom) t))
  *plot*)

(defun get-function-canvas ()
  (when (null *function-canvas*) (setf *function-canvas* (2dp:make-canvas)))
  *function-canvas*)

(defun get-function-plot ()
  (when (null *function-plot*)
    (setf *function-plot* (2dp:make-plot))
    (setf (2dp:tics-p *function-plot* :left) t)
    (setf (2dp:tics-p *function-plot* :bottom) t))
  *function-plot*)

;;;============================================================

(defun plot-function (f min max
		      &key
		      (nsteps 32)
		      (plot (get-function-plot))
		      (canvas (get-function-canvas)))
  (when (> min max) (rotatef min max))
  (let ((step (/ (- max min) (float nsteps))))
    (setf (2dp:plot-objects plot) nil)
    (2dp:plot-curve
      plot
      (2dg:with-collection
	(do ((x min (+ x step)))
	    ((> x max))
	  (2dg:collect (2dg:make-position x (funcall f x))))))
    (2dp:rescale-plot plot)
    (2dp:redraw-plot plot canvas)))

;;;============================================================
;;; pairs (edges)

(deftype Pair () 'cons)
(defmacro make-pair (single0 single1) `(cons ,single0 ,single1))

(defmacro pair-single0 (pair) `(car ,pair))
(defmacro pair-single1 (pair) `(cdr ,pair))

;;; note that setf's come for free (at least in Genera)

;;;============================================================

(defun collect-all-unordered-pairs (things)
  (declare (type Sequence things))
  (etypecase things
    (List
      (mapcon #'(lambda (l)
		  (let ((1st (first l)))
		    (mapcar #'(lambda (2nd) (make-pair 1st 2nd)) (rest l))))
	things))
    (Vector
      (let ((pairs ())
	    (n (length things)))
	(dotimes (i n )
	  (let ((1st (aref things i)))
	    (do ((j (+ i 1) (+ j 1)))
		((>= j n))
	      (push (make-pair 1st (aref things j)) pairs))))
	pairs))))

(defun collect-all-ordered-pairs (things)
  (declare (type Sequence things))
  (let ((pairs ()))
    (map nil #'(lambda (1st)
		 (map nil #'(lambda (2nd)
			      (unless (eq 1st 2nd)
				(push (make-pair 1st 2nd) pairs)))
		      things)) things)
    pairs))

(defun collect-singles-from-pairs (pairs)
  (let ((singles ()))
    (dolist (pair pairs)
      (pushnew (pair-single0 pair) singles)
      (pushnew (pair-single1 pair) singles))
    singles))

;;;============================================================
;;; Layout controllers
;;;============================================================

(defclass Layout-Controller ()
     ((canvas
	:type 2dg:Graphics-Canvas
	:reader canvas
	:initarg :canvas)
      (plot
	:type 2dp:Plot
	:reader plot
	:initarg :plot))
  (:default-initargs
    :canvas (get-canvas)
    :plot (2dp:make-plot)))

;;;============================================================

(defclass Point-Cloud-Layout (Layout-Controller)
     ((singles
	:type Sequence ;; of T's (anything)
	:reader singles
	:initarg :singles)
      (x
	:type (Vector Single-Float)
	:reader x
	:initarg :x)
      (y
	:type (Vector Single-Float)
	:reader y
	:initarg :y)
      (dist-fn ;; applied to 2 <singles>
	:type (Function (T T) Single-Float)
	:accessor dist-fn
	:initarg :dist-fn)
      (weight-fn
	;; applied to 2 <singles> and a distance that should be the same
	;; as that returned by <dist-fn>
	:type (Function (T T Single-Float) Single-Float)
	:reader weight-fn
	:initarg :weight-fn)
      (order-fn ;; applied to 2 <singles>, returns -1.0, 0.0, or 1.0
	:type (Function (T T) Single-Float)
	:accessor order-fn
	:initarg :order-fn)
      (pairs
	:type Sequence ;; of Pair's of the <singles> above
	:reader pairs
	:initarg :pairs)
      (points
	:type List ;; of 2dp:Point's
	:reader points)
      (single-point-table ;; map each single to corresponding point.
	:type Hash-Table 
	:reader single-point-table
	:initform (make-hash-table :test #'eq))
      ;; steps for animating the optimization
      (dx
	:type (Vector Single-Float)
	:reader dx
	:initarg :dx)
      (dy
	:type (Vector Single-Float)
	:reader dy
	:initarg :dy)
      ;; The tail of a step plot-object shares the position
      ;; object of the corresponding point plot object,
      ;; so only the head position has to be updated.
      (steps
	:type List ;; of 2dp:Line-Segment's
	:reader steps)))

;;;------------------------------------------------------------

(defmethod *initialize-instance :after ((l Point-Cloud-Layout)
					&rest init-list
					&key
					(glyphs
					  (let* ((n (length (singles l)))
						 (g (make-array n)))
					    (dotimes (i n)
					      (setf (aref g i)
						    2dg:*circle-bitmap*))
					    g))
					&allow-other-keys)
  (declare (ignore init-list))

  ;; initialize singles and pairs
  (unless (or (slot-boundp l 'singles) (slot-boundp l 'pairs))
    (error "Either <singles> or <pairs> must be provided."))
  (when (and (slot-boundp l 'singles) (not (slot-boundp l 'pairs)))
    (setf (slot-value l 'pairs) (collect-all-unordered-pairs (singles l))))
  (when (and (not (slot-boundp l 'singles)) (slot-boundp l 'pairs))
    (setf (slot-value l 'singles) (collect-singles-from-pairs (pairs l))))
  (unless (slot-boundp l 'x)
    (setf (slot-value l 'x) (make-random-vector (length (singles l)))))
  (unless (slot-boundp l 'y)
    (setf (slot-value l 'y) (make-random-vector (length (singles l)))))

  ;; initialize points
  (setf (slot-value l 'points)
	(map 'List
	     #'(lambda (x y g)
		 (2dp:make-point (2dg:make-position x y) :symbol g))
	     (x l) (y l) glyphs))
  (2dp:add-plot-objects (points l) (plot l))

  ;; initialize steps---Warning: this relies on each step's tail
  ;; position being the same position object as the corresponding
  ;; point's position.

  (unless (slot-boundp l 'dx)
    (setf (slot-value l 'dx) (make-array (length (singles l))
					 :element-type 'Single-Float
					 :initial-element 0.0)))
  (unless (slot-boundp l 'dy)
    (setf (slot-value l 'dy) (make-array (length (singles l))
					 :element-type 'Single-Float
					 :initial-element 0.0)))
  (setf (slot-value l 'steps)
	(map 'List
	     #'(lambda (x0 y0 dx dy point)
		 (2dp:make-line-segment
		   (2dp:point-position point)
		   (2dg:make-position (+ x0 dx) (+ y0 dy))))
	     (x l) (y l) (dx l) (dy l) (points l)))
  (2dp:add-plot-objects (steps l) (plot l)))
  
;;;------------------------------------------------------------

(defmethod cardinality ((l Point-Cloud-Layout)) (length (singles l)))

;;;------------------------------------------------------------

(defmethod nprocessors ((l Point-Cloud-Layout))
  (next-power-of-2 (+ (length (singles l)) (length (pairs l)))))
  
;;;------------------------------------------------------------
;;; Warning: this relies on each step's tail position being the same
;;; position object as the corresponding point's position.

(defmethod plot-animation-step ((l Point-Cloud-Layout))
  (map nil
       #'(lambda (x0 y0 dx dy point step)
	   (let ((pos (2dp:point-position point))
		 (head-pos (2dp:line-segment-head-position step)
			   #||(2dp:arrow-head-position step)||#))
	     (setf (2dg:position-x pos) x0)
	     (setf (2dg:position-y pos) y0)
	     (setf (2dg:position-x head-pos) (+ x0 dx))
	     (setf (2dg:position-y head-pos) (+ y0 dy))) )
       (x l) (y l) (dx l) (dy l) (points l) (steps l))
  (2dp:rescale-plot (plot l))
  (2dp:redraw-plot (plot l) (canvas l)))

;;;============================================================
;;;
;;; The Graph-Layout classes defined here are all "abstract" classes,
;;; and should not be instantiated. The plan is that specialized,
;;; instantiable Graph-Layout subclasses will be defined for different
;;; choices of "raw data structure" used to represent a graph. At the
;;; moment, the only specialization needed is a make-edge-plot-objects
;;; method. See etree.lisp and pedigree.lisp for examples.
;;;
;;; Warning: the Graph-Layout classes, as now defined, rely on the edge
;;; plot objects being defined using the position objects eq to the
;;; position objects used in the point plot objects that correspond the
;;; the edge's nodes.  This allows (method plot-animation-step
;;; (Point-Cloud-Layout)) to work for graph layouts as well.

(defclass Graph-Layout (Point-Cloud-Layout)
     ((edge-plot-objects
	:type Sequence ;; of 2dp:Arrows's or 2dp:Line-Segments
	:reader edge-plot-objects)))

;;;------------------------------------------------------------

(defmethod *initialize-instance :after ((l Graph-Layout) &rest init-list)
  (declare (ignore init-list))
  (let ((table (single-point-table l)))
    ;; assume single and point sequences correspond
    (map nil 
	 #'(lambda (s p) (setf (gethash s table) p))
	 (singles l) (points l)))
  (setf (slot-value l 'edge-plot-objects) (make-edge-plot-objects l))
  (2dp:add-plot-objects (edge-plot-objects l) (plot l)))

;;;------------------------------------------------------------

(defmethod make-edge-plot-objects ((l Graph-Layout))
  (error
    "make-edge-plot-objects
     should be redefined for any instantiable Graph-Layout class."))

;;;============================================================
;;; For a Directed-Graph-Layout, the edge-plot-objects
;;; should be arrows.

(defclass Directed-Graph-Layout (Graph-Layout)
     ())

