;;;-*- Package: (CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

(export '(gradient-layout gradient-ordered-layout))

;;;============================================================

(*defun *clear-single-for-gradient-step ($singles)
  (declare (type (pvar Single) $singles))
  (*when (member?!! $singles)
    (*setf (single-grad-x!! $singles) (!! 0.0))
    (*setf (single-grad-y!! $singles) (!! 0.0))
    (*setf (single-loss!! $singles) (!! 0.0))))

;;;============================================================

(*defun *collect-grad-from-singles ($pairs $singles)

   (declare (type (pvar Pair) $pairs)
	   (type (pvar Single) $singles))
  
  (*when (member?!! $pairs)
    (*setf (pair-grad-x0!! $pairs)
	   (pref!! (single-grad-x!! $singles) (pair-single-address0!! $pairs)
		   :collision-mode :many-collisions))
    (*setf (pair-grad-y0!! $pairs)
	   (pref!! (single-grad-y!! $singles) (pair-single-address0!! $pairs) 
		   :collision-mode :many-collisions))
    (*setf (pair-grad-x1!! $pairs)
	   (pref!! (single-grad-x!! $singles) (pair-single-address1!! $pairs)
		   :collision-mode :many-collisions))
    (*setf (pair-grad-y1!! $pairs)
	   (pref!! (single-grad-y!! $singles) (pair-single-address1!! $pairs)
		   :collision-mode :many-collisions))))

;;;============================================================

(*defun *try-gradient-step ($pairs step-size)

  (declare (type (pvar Pair) $pairs))
  
  (*when (member?!! $pairs)
    (*let (($step-size (!! step-size)))
      (declare (type Float-Pvar $step-size))
      (*setf (pair-new-x0!! $pairs)
	     (+!! (pair-x0!! $pairs)
		  (*!! $step-size (pair-grad-x0!! $pairs))))
      (*setf (pair-new-y0!! $pairs)
	     (+!! (pair-y0!! $pairs)
		  (*!! $step-size (pair-grad-y0!! $pairs))))
      (*setf (pair-new-x1!! $pairs)
	     (+!! (pair-x1!! $pairs)
		  (*!! $step-size (pair-grad-x1!! $pairs))))
      (*setf (pair-new-y1!! $pairs)
	     (+!! (pair-y1!! $pairs)
		  (*!! $step-size (pair-grad-y1!! $pairs)))))))

;;;============================================================

(*defun *compute-grad ($pairs $singles)
  (declare (type (pvar Pair) $pairs)
	   (type (pvar Single) $singles))
  (*clear-single-for-gradient-step $singles)
  (*collect-data-from-singles $pairs $singles)
  (*compute-deltas $pairs)
  (*let (($grad-x0 (!! 0.0))
	 ($grad-y0 (!! 0.0))
	 ($grad-x1 (!! 0.0))
	 ($grad-y1 (!! 0.0)))
    (declare (type Single-Float-Pvar $grad-x0 $grad-y0 $grad-x1 $grad-y1))
    (*when (member?!! $pairs)
      ;; compute each pair's contribution to the gradient
      (*let* (($2d (pair-2d-dist!! $pairs))
	      ($w*dl (*!! (pair-weight!! $pairs)
			  (-!! (pair-dist!! $pairs) (pair-2d-dist!! $pairs)))) 
	      ($grad-x (*!! $w*dl (/!! (pair-dx!! $pairs) $2d)))
	      ($grad-y (*!! $w*dl (/!! (pair-dy!! $pairs) $2d))))
	(declare (type Single-Float-Pvar $2d $w*dl $grad-x $grad-y))
	;; send it to the singles
	(*pset :add $grad-x $grad-x0 (pair-single-address0!! $pairs))
	(*pset :add $grad-y $grad-y0 (pair-single-address0!! $pairs))
	(*pset :add (-!! $grad-x) $grad-x1 (pair-single-address1!! $pairs))
	(*pset :add (-!! $grad-y) $grad-y1 (pair-single-address1!! $pairs))))
    (*when (member?!! $singles)
      (*setf (single-grad-x!! $singles) (+!! $grad-x0 $grad-x1))
      (*setf (single-grad-y!! $singles) (+!! $grad-y0 $grad-y1))))
  (*collect-grad-from-singles $pairs $singles))

;;;============================================================

(*defun *compute-ordered-grad ($pairs $singles $alpha $1-alpha)
  (declare (type (pvar Pair) $pairs)
	   (type (pvar Single) $singles)
	   (type Single-Float-Pvar $alpha $1-alpha))
  (*clear-single-for-gradient-step $singles)
  (*collect-data-from-singles $pairs $singles)
  (*compute-deltas $pairs)
  (*let (($grad-x0 (!! 0.0))
	 ($grad-y0 (!! 0.0))
	 ($grad-x1 (!! 0.0))
	 ($grad-y1 (!! 0.0)))
    (declare (type Single-Float-Pvar $grad-x0 $grad-y0 $grad-x1 $grad-y1))
    (*when (member?!! $pairs)
      (*let* (($d (pair-dist!! $pairs))
	      ($2d (pair-2d-dist!! $pairs))
	      ($dl (-!! $d (pair-2d-dist!! $pairs)))
	      ($w (pair-weight!! $pairs))
	      ($w*dl (*!! $w $dl))
	      ($o (pair-order!! $pairs))
	      ($-2*w*o (*!! (!! -2.0) $w $o)) 
	      ($grad-x (*!! $w*dl (/!! (pair-dx!! $pairs) $2d)))
	      ($grad-y (*!! $w*dl (/!! (pair-dy!! $pairs) $2d)))
	      ($o-grad-x (*!! $-2*w*o (+!! (*!! $o (pair-dx!! $pairs)) $d))))
	(declare (type Single-Float-Pvar
		       $d $dl $w $w*dl $o $-2*w*o
		       $grad-x $grad-y $o-grad-x))
	;; compute each pair's contribution to the gradient
	(*setf $grad-x (+!! (*!! $1-alpha $grad-x) (*!! $alpha $o-grad-x)))
	;; send it to the singles
	(*setf $grad-y (*!! $1-alpha $grad-y))
	(*pset :add $grad-x $grad-x0 (pair-single-address0!! $pairs))
	(*pset :add $grad-y $grad-y0 (pair-single-address0!! $pairs))
	(*pset :add (-!! $grad-x) $grad-x1 (pair-single-address1!! $pairs))
	(*pset :add (-!! $grad-y) $grad-y1 (pair-single-address1!! $pairs))))
    (*when (member?!! $singles)
      (*setf (single-grad-x!! $singles) (+!! $grad-x0 $grad-x1))
      (*setf (single-grad-y!! $singles) (+!! $grad-y0 $grad-y1))))
  (*collect-grad-from-singles $pairs $singles))

;;;============================================================

(*defun *take-gradient-step (step-size $singles)

  (declare (type Single-Float step-size)
	   (type (pvar Single) $singles))

  (*when (member?!! $singles)
    (*let (($step-size (the Single-Float-Pvar (!! step-size))))
      (declare (type Single-Float-Pvar $step-size))
      (*setf (single-x!! $singles)
	     (+!! (single-x!! $singles)
		  (*!! $step-size (single-grad-x!! $singles))))
      (*setf (single-y!! $singles)
	     (+!! (single-y!! $singles)
		  (*!! $step-size (single-grad-y!! $singles)))))))

;;;============================================================

(*defun *animate-gradient-step ($singles start end step-size layout)
  
  (declare (type (pvar Single) $singles)
	   (type Fixnum start end)
	   (type Single-Float step-size)
	   (type lay:Layout-Controller layout)) 

  (pvar-to-array
    (single-x!! $singles) (lay:x layout)
    :cube-address-start start :cube-address-end end)
  (pvar-to-array
    (single-y!! $singles) (lay:y layout)
    :cube-address-start start :cube-address-end end)

  (*let (($step-size (the float-pvar (!! step-size))))
    (declare (type float-pvar $step-size))
    (pvar-to-array
      (*!! $step-size (single-grad-x!! $singles)) (lay:dx layout)
      :cube-address-start start :cube-address-end end)
    (pvar-to-array
      (*!! $step-size (single-grad-y!! $singles)) (lay:dy layout)
      :cube-address-start start :cube-address-end end))

  (lay:plot-animation-step layout))

;;;============================================================

(defun gradient-layout (layout
			&key
			(step-deflation 1.0)
			(step-size 1.0)
			(max-iterations 1000))

  (declare (type lay:Layout-controller layout)
	   (type Single-Float step-size)
	   (type Fixnum max-iterations))

  ;; enough processors?
  (if (> (lay:nprocessors layout) *number-of-processors-limit*)
      (*cold-boot :initial-dimensions (list (lay:nprocessors layout)))
      (*warm-boot))
  (*let* (($pairs (make-pair!!))
	  ($singles (make-single!!)))
    (declare (type (pvar Pair) $pairs)
	     (type (pvar Single) $singles)) 
    (let* ((current-loss 0.0)
	   (single-bounds (*initialize-cm-for-layout layout $pairs $singles))
	   (singles-start (first single-bounds))
	   (singles-end (rest single-bounds)))
      (flet ((try-step (step)
	       (*try-gradient-step $pairs step) 
	       (*compute-loss $pairs)))
	(dotimes (k max-iterations (warn "~a iterations" max-iterations))
	  (*compute-grad $pairs $singles)
	  (when lay:*debug*
	    (lay:plot-function
	      #'try-step (* -10.0 (+ 0.01 step-size))
	      (* 10.0 (+ 0.01 step-size)) :nsteps 20))
	  (multiple-value-setq
	    (step-size current-loss)
	    (bm:minimize-1d #'try-step -0.01 (* 10.0 (abs step-size))
			    :abs-tolerance 0.01))
	  (let ((actual-step (* step-deflation step-size)))
	    (format *standard-output*
		    "~& best, actual=   ~f; ~f, ~
                       ~% loss= ~f." 
		    step-size actual-step current-loss)
	    (when *animate-gradient-steps?*
	      (*animate-gradient-step
		$singles singles-start singles-end actual-step layout))
	    (*take-gradient-step actual-step $singles)))))))


;;;============================================================

(defun gradient-ordered-layout (layout
				&key
				(step-deflation 1.0)
				(alpha 0.5)
				(step-size 1.0)
				(max-iterations 1000))

  (declare (type Sequence layout)
	   (type Single-Float alpha step-size)
	   (type Fixnum max-iterations))

  ;; enough processors?
  (if (> (lay:nprocessors layout) *number-of-processors-limit*)
      (*cold-boot :initial-dimensions (list (lay:nprocessors layout)))
      (*warm-boot))
  (*let* (($pairs (make-pair!!))
	  ($singles (make-single!!)) 
	  ($alpha (!! alpha))
	  ($1-alpha (-!! (!! 1.0) $alpha)))
    (declare (type (pvar Pair) $pairs)
	     (type (pvar Single) $singles)
	     (type Single-Float-Pvar $alpha $1-alpha)) 
    (let* ((current-loss 0.0)
	   (single-bounds
	     (*initialize-cm-for-ordered-layout layout $pairs $singles))
	   (singles-start (first single-bounds))
	   (singles-end (rest single-bounds)))
      (flet ((try-step (step)
	       (*try-gradient-step $pairs step) 
	       (*compute-ordered-loss $pairs $alpha $1-alpha)))
	(dotimes (k max-iterations (warn "~a iterations" max-iterations))
	  (*compute-ordered-grad $pairs $singles $alpha $1-alpha)
	  (when lay:*debug*
	    (lay:plot-function
	      #'try-step (* -10.0 (+ 0.01 step-size))
	      (* 10.0 (+ 0.01 step-size)) :nsteps 20))
	  (multiple-value-setq
	    (step-size current-loss)
	    (bm:minimize-1d #'try-step -0.01 (* 10.0 (abs step-size))
			    :abs-tolerance 0.01))
	  (let ((actual-step
		  #||(* step-deflation step-size)||#
		  (random
		    (+ 0.01 (abs (* 2.0 step-deflation step-size))))))
	    (format *standard-output*
		    "~& best, actual=   ~f; ~f, ~
                       ~% loss= ~f." 
		    step-size actual-step current-loss)
	    (when *animate-gradient-steps?* 
	      (*animate-gradient-step
		$singles singles-start singles-end actual-step layout))
	    (*take-gradient-step actual-step $singles)))))))