;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: My-TV -*-
;;;
;;; Copyright 1986. John Alan McDonald. All Rights Reserved. 
;;;
;;;======================================================================
;;;
;;; shorter names for fast 1d array refs
;;; 

(defmacro ar-1d (    array index) `(sys:%1d-aref      ,array ,index))
(defmacro as-1d (val array index) `(sys:%1d-aset ,val ,array ,index))

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :draw-points)
	   
	   (x y
	      &optional &key
	      (size 1)
	      (color -1)
	      (preclipped? nil)
	      (alu tv:char-aluf)
	      )
  
  (declare
    (type (lisp:Vector integer) x y)
    (type integer size)
    (type (or integer (lisp:Vector integer)) color)
    (type (or nil t) preclipped?)
    (type alu alu) 
    )
  
  (let (1d-array w off)
    (cond
      ((or tv:exposed-p (eql tv:deexposed-typeout-action :expose))
       (setf 1d-array 1d-screen)
       (setf w (array-dimension (send tv:screen :screen-array) 0))
       (setf off (+ tv:x-offset (* w tv:y-offset)))
       )
      ((and (eql tv:deexposed-typeout-action :permit)
	    (not (null tv:bit-array))
	    )
       (setf 1d-array 1d-bit-save)
       (setf w (array-dimension tv:bit-array 0))
       (setf off 0)
       )
      (t
       (send self :output-hold-exception)
       )
      ) 
    (if preclipped?
	(etypecase color
	  (number
	    (send self :draw-preclipped-single-color-points
		  x y size color alu 1d-array w off)
	    )
	  (lisp:Vector
	    (send self :draw-preclipped-multi-color-points
		  x y size color alu 1d-array w off)
	    )
	  )
	;; else
	(etypecase color
	  (number
	    (send self :draw-single-color-points
		  x y size color alu 1d-array w off)
	    )
	  (lisp:Vector
	    (send self :draw-multi-color-points
		  x y size color alu 1d-array w off)
	    )
	  )
	)
    )
  )


;;;----------------------------------------------------------------------
;;;
;;;		      Macros to clean up the code:
;;;
;;;----------------------------------------------------------------------
;;;
;;; Note that <draw-1-pixel-point> takes arrays as arguments,
;;; whereas all the rest take the individual coordinates.
;;; 

(defmacro draw-1-pixel-point (x y c) 
 `(as-1d ,c sc (+ off ,x (* w ,y)))
 )

;;;----------------------------------------------------------------------

(defmacro draw-2-pixel-point (x y c)
  
  `(let ((index (+ off ,x (* w ,y))))
     (as-1d ,c sc index)
     (as-1d ,c sc (+ index w))
     )
  )

;;;----------------------------------------------------------------------

(defmacro draw-4-pixel-point (x y c) 
  
  `(let* ((index (+ off ,x (* w ,y))) 
	  (index+w (+ index w))
	  )
     (as-1d ,c sc index)
     (as-1d ,c sc (+ index 1))
     (as-1d ,c sc index+w)
     (as-1d ,c sc (+ index+w 1))
     )
  )

;;;----------------------------------------------------------------------

(defmacro clip-2-pixel-point? (xi yi xmin xmax ymin ymax)
  
  `(or (< ,xi ,xmin)
       (> ,xi ,xmax)
       (< ,yi ,ymin)
       (> (+ ,yi 1) ,ymax)
       )
  )

;;;----------------------------------------------------------------------

(defmacro clip-4-pixel-point? (xi yi xmin xmax ymin ymax)
  
  `(or (< ,xi ,xmin)
       (> (+ ,xi 1) ,xmax)
       (< ,yi ,ymin)
       (> (+ ,yi 1) ,ymax)
       )
  )


;;;----------------------------------------------------------------------
;;;
;;;		     Specialized drawing messages:
;;;
;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :draw-preclipped-single-color-points)
	   
	   (x y size color alu 1d-array w off)
  
  (declare (type (lisp:Vector integer) x y)
	   (type integer size)
	   (type integer color)
	   (type alu alu)
	   )
  (ignore alu)
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (x1 x) (y1 y) 
	  (npoints (min (length x)(length y)))
	  ) 
      (declare (sys:array-register-1d sc x1 y1))
      (ecase size
	(1 (loop for i from 0 below npoints
		 do (draw-1-pixel-point (ar-1d x1 i) (ar-1d y1 i) color)))
	(2 (loop for i from 0 below npoints 
		 do (draw-2-pixel-point (ar-1d x1 i) (ar-1d y1 i) color))) 
	(4 (loop for i from 0 below npoints
		 Do (draw-4-pixel-point (ar-1d x1 i) (ar-1d y1 i) color))) 
	)
      )
    )  
  )

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :draw-preclipped-multi-color-points)
	   
	   (x y size color alu 1d-array w off)
  
  (declare
    (type integer size)
    (type (lisp:Vector integer) x y color)
    (type alu alu)
    )
  (ignore alu)
  
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (x1 x) (y1 y) (c color) 
	  (npoints (min (length x)(length y)))
	  ) 
      (declare (sys:array-register-1d sc x1 y1 c))
      (ecase size
	(1 (loop
	     for i from 0 below npoints
	     do (draw-1-pixel-point (ar-1d x1 i) (ar-1d y1 i) (ar-1d c i)) 
	     ) 
	   )
	(2 (loop
	     for i from 0 below npoints
	     for color = (ar-1d c i)
	     do (draw-2-pixel-point (ar-1d x1 i) (ar-1d y1 i) color) 
	     )
	   )
	(4 (loop
	     for i from 0 below npoints
	     for color = (ar-1d c i)
	     do (draw-4-pixel-point (ar-1d x1 i) (ar-1d y1 i) color) 
	     )
	   )
	) 
      )
    )  
  )

;;;----------------------------------------------------------------------
;;;
;;; We clip whole points for speed. The alternative would be to clip
;;; only the pixels necessary.
;;;

(defmethod (Graphics-Mixin :draw-single-color-points)

	   (x y size color alu 1d-array w off)
  
  (declare (type (lisp:Vector integer) x y)
	   (type integer size)
	   (type integer color)
	   (type alu alu) 
	   )
  (ignore alu)
  
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (x1 x) (y1 y) 
	  (npoints (min (length x)(length y)))
	  ) 
      (declare (sys:array-register-1d sc x1 y1))
      (multiple-value-bind 
	(xmin xmax ymin ymax) (send clipping-region :bounds)
	(ecase size
	  (1 (loop for i from 0 below npoints
		   for xi = (ar-1d x1 i)
		   for yi = (ar-1d y1 i) 
		   unless (clip-point? xi yi xmin xmax ymin ymax)
		   do (draw-1-pixel-point xi yi color)))
	  (2 (loop for i from 0 below npoints
		   for xi  = (ar-1d x1 i)
		   for yi  = (ar-1d y1 i)
		   unless (clip-2-pixel-point? xi yi xmin xmax ymin ymax)
		   do (draw-2-pixel-point xi yi color)))
	  (4 (loop for i from 0 below npoints
		   for xi  = (ar-1d x1 i)
		   for yi  = (ar-1d y1 i)
		   unless (clip-4-pixel-point? xi yi xmin xmax ymin ymax)
		   do (draw-4-pixel-point xi yi color)))
	  )
	)
      )
    )  
  )

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :draw-multi-color-points)

	   (x y size color alu 1d-array w off)
  
  (declare (type integer size)
	   (type (lisp:Vector integer) x y color)
	   (type alu alu) ;; need to (deftype alu...) for this to work
	   )
  (ignore alu)
  
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (x1 x) (y1 y) (c color)
	   (npoints (min (length x)(length y)))
	   ) 
      (declare (sys:array-register-1d sc x1 y1))
      (multiple-value-bind
	(xmin xmax ymin ymax) (send clipping-region :bounds)
	(ecase size
	  (1 (loop for i from 0 below npoints
		   for xi = (ar-1d x1 i)
		   for yi = (ar-1d y1 i) 
		   unless (clip-point? xi yi xmin xmax ymin ymax)
		   do (draw-1-pixel-point xi yi (ar-1d c i))))
	  (2 (loop for i from 0 below npoints
		   for color = (ar-1d c i)
		   for xi  = (ar-1d x1 i)
		   for yi  = (ar-1d y1 i)
		   unless (clip-2-pixel-point? xi yi xmin xmax ymin ymax)
		   do (draw-2-pixel-point xi yi color)))
	  (4 (loop for i from 0 below npoints
		   for color = (ar-1d c i)
		   for xi  = (ar-1d x1 i)
		   for yi  = (ar-1d y1 i)
		   unless (clip-4-pixel-point? xi yi xmin xmax ymin ymax)
		   do (draw-4-pixel-point xi yi color)))
	  )
	)
      )
    ) 
  )


;;;========================================================================

(defmethod (Graphics-Mixin :move-points)
	   
	   ( old-x old-y new-x new-y
	    &optional &key
	    (size 1)
	    (color -1)
	    (preclipped? nil)
	    (alu tv:char-aluf)
	    )
  
  (declare (type (lisp:Vector integer) old-x old-y new-x new-y)
	   (type integer size)
	   (type (or integer (lisp:Vector integer)) color)
	   (type (or nil t) preclipped?)
	   (type alu alu) ;; need to (deftype alu...) for this to work
	   )
  (ignore alu)
  
  (let (1d-array w off)
    (cond
      ((or tv:exposed-p (eql tv:deexposed-typeout-action :expose))
       (setf 1d-array 1d-screen)
       (setf w (array-dimension (send tv:screen :screen-array) 0))
       (setf off (+ tv:x-offset (* w tv:y-offset)))
       )
      ((and (eql tv:deexposed-typeout-action :permit)
	    (not (null tv:bit-array))
	    )
       (setf 1d-array 1d-bit-save)
       (setf w (array-dimension tv:bit-array 0))
       (setf off 0)
       )
      (t
       (send self :output-hold-exception)
       )
      ) 
    (if preclipped?
	(etypecase color
	  (number
	    (send self :move-preclipped-single-color-points
		  old-x old-y new-x new-y size color alu 1d-array w off)
	    )
	  (lisp:Vector
	    (send self :move-preclipped-multi-color-points
		  old-x old-y new-x new-y size color alu 1d-array w off)
	    )
	  )
	;; else
	(etypecase color
	  (number
	    (send self :move-single-color-points
		  old-x old-y new-x new-y size color alu 1d-array w off)
	    )
	  (lisp:Vector
	    (send self :move-multi-color-points
		  old-x old-y new-x new-y size color alu 1d-array w off)
	    )
	  )
	)
    )
  )

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :move-single-color-points)
	   
	   (old-x old-y new-x new-y size color alu 1d-array w off)
  
  (declare
    (type (lisp:Vector integer) old-x old-y new-x new-y)
    (type integer size)
    (type integer color)
    (type alu alu) ;; need to (deftype alu...) for this to work
    )
  (ignore alu)
  
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (ox old-x) (oy old-y) (nx new-x) (ny new-y)
	  (npoints (min (length old-x)(length old-y)
			(length new-x)(length new-y)))
	  ) 
      (declare (sys:array-register-1d sc ox oy nx ny))
      (multiple-value-bind
	(xmin xmax ymin ymax) (send clipping-region :bounds)
	(ecase size
	  (1 (loop
	       for i from 0 below npoints 
	       for oxi = (ar-1d ox i) 
	       for oyi = (ar-1d oy i)
	       for nxi = (ar-1d nx i) 
	       for nyi = (ar-1d ny i)
	       unless (clip-point? oxi oyi xmin xmax ymin ymax)
	       do (draw-1-pixel-point oxi oyi 0) 
	       unless (clip-point? nxi nyi xmin xmax ymin ymax)
	       do (draw-1-pixel-point nxi nyi color)
	       )
	     )
	  (2 (loop
	       for i from 0 below npoints 
	       for oxi = (ar-1d ox i) 
	       for oyi = (ar-1d oy i)
	       for nxi = (ar-1d nx i)
	       for nyi = (ar-1d ny i)
	       unless (clip-2-pixel-point? oxi oyi xmin xmax ymin ymax)
	       do (draw-2-pixel-point oxi oyi 0)
	       unless (clip-2-pixel-point? nxi nyi xmin xmax ymin ymax)
	       do (draw-2-pixel-point nxi nyi color)
	       )
	     )
	  (4 (loop
	       for i from 0 below npoints
	       for oxi = (ar-1d ox i) 
	       for oyi = (ar-1d oy i)
	       for nxi = (ar-1d nx i) 
	       for nyi = (ar-1d ny i)
	       unless (clip-4-pixel-point? oxi oyi xmin xmax ymin ymax)
	       do (draw-4-pixel-point oxi oyi 0)
	       unless (clip-4-pixel-point? nxi nyi xmin xmax ymin ymax)
	       do (draw-4-pixel-point nxi nyi color)
	       )
	     )
	  )
	)
      )
    )  
  )

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :move-preclipped-single-color-points)
	   
	   (old-x old-y new-x new-y size color alu 1d-array w off)
  
  (declare
    (type (lisp:Vector integer) old-x old-y new-x new-y)
    (type integer size)
    (type integer color)
    (type alu alu) ;; need to (deftype alu...) for this to work
    )
  (ignore alu)
  
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (ox old-x) (oy old-y) (nx new-x) (ny new-y) 
	  (npoints (min (length old-x)(length old-y)
			(length new-x)(length new-y)))
	  )
      (declare (sys:array-register-1d sc ox oy nx ny))
      (ecase size
	(1 (loop
	     for i from 0 below npoints 
	     for oxi = (ar-1d ox i) 
	     for oyi = (ar-1d oy i)
	     for nxi = (ar-1d nx i) 
	     for nyi = (ar-1d ny i)
	     do 
	     (draw-1-pixel-point oxi oyi 0) 
	     (draw-1-pixel-point nxi nyi color)
	     )
	   )
	(2 (loop
	     for i from 0 below npoints 
	     for oxi = (ar-1d ox i) 
	     for oyi = (ar-1d oy i)
	     for nxi = (ar-1d nx i)
	     for nyi = (ar-1d ny i)
	     do 
	     (draw-2-pixel-point oxi oyi 0)
	     (draw-2-pixel-point nxi nyi color)
	     )
	   )
	(4 (loop
	     for i from 0 below npoints
	     for oxi = (ar-1d ox i) 
	     for oyi = (ar-1d oy i)
	     for nxi = (ar-1d nx i) 
	     for nyi = (ar-1d ny i)
	     do
	     (draw-4-pixel-point oxi oyi 0)
	     (draw-4-pixel-point nxi nyi color)
	     )
	   )
	)
      )
    )  
  )

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :move-multi-color-points)
	   
	   (old-x old-y new-x new-y size color alu 1d-array w off)
  
  (declare (type (lisp:Vector integer) old-x old-y new-x new-y color)
	   (type integer size)
	   (type alu alu)
	   )
  (ignore alu)
  
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (ox old-x) (oy old-y) (nx new-x) (ny new-y)
	  (c color) 
	  (npoints (min (length old-x)(length old-y)
			(length new-x)(length new-y)))
	  ) 
      (declare (sys:array-register-1d sc ox oy nx ny c))
      (multiple-value-bind
	(xmin xmax ymin ymax) (send clipping-region :bounds)
	(ecase size
	  (1 (loop
	       for i from 0 below npoints 
	       for oxi = (ar-1d ox i) 
	       for oyi = (ar-1d oy i)
	       for nxi = (ar-1d nx i) 
	       for nyi = (ar-1d ny i)
	       unless (clip-point? oxi oyi xmin xmax ymin ymax)
	       do (draw-1-pixel-point oxi oyi 0) 
	       unless (clip-point? nxi nyi xmin xmax ymin ymax)
	       do (draw-1-pixel-point nxi nyi (ar-1d c i))
	       )
	     )
	  (2 (loop
	       for i from 0 below npoints 
	       for oxi = (ar-1d ox i) 
	       for oyi = (ar-1d oy i)
	       for nxi = (ar-1d nx i) 
	       for nyi = (ar-1d ny i)
	       for color = (ar-1d c i)
	       unless (clip-2-pixel-point? oxi oyi xmin xmax ymin ymax)
	       do (draw-2-pixel-point oxi oyi 0) 
	       unless (clip-2-pixel-point? nxi nyi xmin xmax ymin ymax)
	       do (draw-2-pixel-point nxi nyi color)
	       )
	     )
	  (4 (loop
	       for i from 0 below npoints 
	       for oxi = (ar-1d ox i) 
	       for oyi = (ar-1d oy i)
	       for nxi = (ar-1d nx i) 
	       for nyi = (ar-1d ny i)
	       for color = (ar-1d c i)
	       unless (clip-4-pixel-point? oxi oyi xmin xmax ymin ymax)
	       do (draw-4-pixel-point oxi oyi 0) 
	       unless (clip-4-pixel-point? nxi nyi xmin xmax ymin ymax)
	       do (draw-4-pixel-point nxi nyi color)
	       )
	     )
	  ) 
	)
      )
    )  
  )

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :move-preclipped-multi-color-points)
	   
	   (old-x old-y new-x new-y size color alu 1d-array w off)
  
  (declare
    (type (lisp:Vector integer) old-x old-y new-x new-y color)
    (type integer size)
    (type alu alu)
    )
  (ignore alu)
  
  (tv:prepare-sheet (self)
    (let ((sc 1d-array) (ox old-x) (oy old-y) (nx new-x) (ny new-y)
	  (c color) 
	  (npoints (min (length old-x)(length old-y)
			(length new-x)(length new-y)))
	  ) 
      (declare (sys:array-register-1d sc ox oy nx ny c))
      (ecase size
	(1 (loop for i from 0 below npoints 
		 for oxi = (ar-1d ox i) 
		 for oyi = (ar-1d oy i)
		 for nxi = (ar-1d nx i) 
		 for nyi = (ar-1d ny i)
		 do
		 (draw-1-pixel-point oxi oyi 0) 
		 (draw-1-pixel-point nxi nyi (ar-1d c i))
		 )
	   )
	(2 (loop for i from 0 below npoints 
		 for oxi = (ar-1d ox i) 
		 for oyi = (ar-1d oy i)
		 for nxi = (ar-1d nx i)
		 for nyi = (ar-1d ny i)
		 for color = (ar-1d c i)
		 do
		 (draw-2-pixel-point oxi oyi 0)
		 (draw-2-pixel-point nxi nyi color)
		 )
	   )
	(4 (loop for i from 0 below npoints
		 for oxi = (ar-1d ox i) 
		 for oyi = (ar-1d oy i)
		 for nxi = (ar-1d nx i) 
		 for nyi = (ar-1d ny i)
		 for color = (ar-1d c i)
		 do
		 (draw-4-pixel-point oxi oyi 0)
		 (draw-4-pixel-point nxi nyi color)
		 )
	   )
	)
      )
    )  
  )

