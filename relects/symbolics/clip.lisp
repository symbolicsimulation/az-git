;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Arizona -*-
;;;
;;; Copyright 1987. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;======================================================================
;;; 		Rectangles
;;;======================================================================
;;;
;;; A Rectangle is taken to be a cartesian product of Closed-Intervals:
;;;
;;;      [xmin,xmax] X [ymin,ymax].
;;;


(defclass Rectangle
	  (Set)
     (xmin xmax ymin ymax)
  (:accessor-prefix ||)
  )

;;;----------------------------------------------------------------------

(defmethod bounds ((r Rectangle))
  (values (xmin r) (xmax r) (ymin r) (ymax r)))

;;;----------------------------------------------------------------------

(defmethod set-bounds ((r Rectangle) nxmin nxmax nymin nymax) 

  (setf (xmin r) nxmin)
  (setf (xmax r) nxmax)
  (setf (ymin r) nymin)
  (setf (ymax r) nymax))

;;;----------------------------------------------------------------------

(defmethod xdiameter ((r Rectangle)) (- (xmax r) (xmin r))) ;; the width
(defmethod ydiameter ((r Rectangle)) (- (ymax r) (ymin r))) ;; the height
(defmethod diameter ((r Rectangle)) (values (xdiameter r) (ydiameter r)))

;;;----------------------------------------------------------------------

(xdefmethod xradius ((r Rectangle)) (truncate (- (xmax r) (xmin r)) 2))
(xdefmethod yradius ((r Rectangle)) (truncate (- (ymax r) (ymin r)) 2))
(xdefmethod radius ((r Rectangle)) (values (xradius r) (yradius r)))

;;;----------------------------------------------------------------------

(xdefmethod xcenter ((r Rectangle)) (truncate (+ (xmax r) (xmin r)) 2))
(xdefmethod ycenter ((r Rectangle)) (truncate (+ (ymax r) (ymin r)) 2))
(xdefmethod center ((r Rectangle)) (values (xcenter r) (ycenter r)))

;;;----------------------------------------------------------------------
;;; maintain the constraints by leaving the upper-left corner alone.

(xdefmethod-setf xdiameter ((r Rectangle)) (xd)
  (unless (null xd) (setf (xmax r) (+ (xmin r) xd))))

(xdefmethod-setf ydiameter ((r Rectangle)) (yd)
  (unless (null yd) (setf (ymax r) (+ (ymin r) yd))))

(xdefmethod set-diameter ((r Rectangle) xd yd)
  (setf (xdiameter r) xd)
  (setf (ydiameter r) yd))

;;;----------------------------------------------------------------------
;;; Returns <t> if the point is in the Rectangle.

(xdefmethod member-xy? ((r Rectangle) x y)
  (and (<= (xmin r) x (xmax r)) (<= (ymin r) y (ymax r))))

;;;----------------------------------------------------------------------
;;; Returns <t> if the rectangle is contained in the Rectangle.


(xdefmethod contains-rectangle? ((r Rectangle) xmin0 xmax0 ymin0 ymax0)
  
  (and (>= xmin0 xmin)
       (<= xmax0 xmax)
       (>= ymin0 ymin)
       (<= ymax0 ymax)
       ) 
  )

;;;----------------------------------------------------------------------
;;;
;;; Returns <nil> if the intersect-intervals is empty.
;;;

(xdefmethod clip-rectangle ((r Rectangle)xmin0 xmax0 ymin0 ymax0)
  
  (multiple-value-bind
    (xmin1 xmax1) (intersect-intervals xmin xmax xmin0 xmax0)
    (multiple-value-bind
      (ymin1 ymax1) (intersect-intervals ymin ymax ymin0 ymax0)
      (values xmin1 xmax1 ymin1 ymax1)
      )
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Returns <t> if <a-Rectangle> intersects the Rectangle.
;;;

(xdefmethod intersects? ((r Rectangle) a-Rectangle)
  
  (multiple-value-bind (xmin0 xmax0 ymin0 ymax0) (send a-Rectangle :bounds)
    (not (null (clip-rectangle r xmin0 xmax0 ymin0 ymax0)))
    )
  )

;;;----------------------------------------------------------------------

(xdefmethod clip-line-visibility ((r Rectangle)point-x point-y)
  
  (let ((visibility (cond ((< point-x xmin) 1)
			  ((> point-x xmax) 2)
			  (t 0)
			  )
		    )
	)
    (cond ((< point-y ymin) (logior 4 visibility))
	  ((> point-y ymax) (logior 8 visibility))
	  ( t                         visibility )
	  )
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; The following clipping METHOD takes the bounds of a line and returns the
;;; multiple values FROM-X Y1 X2 Y2 which are the clipped bounds of the line.
;;; 

(xdefmethod clip-line ((r Rectangle)x1 y1 x2 y2)
  
  (loop
    with xmax-1 = (- xmax 1)
    with ymax-1 = (- ymax 1)
    with exchanged = nil
    with v2 = (clip-line-visibility r x2 y2)
    for  v1 = (clip-line-visibility r x1 y1)
    until (and (zerop v1) (zerop v2)) ;; completely visible
    do

    ;; if all off the screen, return nil as a flag
    (unless (zerop (logand v1 v2)) (return nil))
    
    ;; exchange points to try to make to point visible
    (when (zerop v1)
      (rotatef x1 x2)
      (rotatef y1 y2)
      (rotatef v1 v2)
      (setf exchanged (not exchanged))
      )
    
    ;; If x2 = x1 then v1 = 0, 4 or 8 so there is no danger of divide by zero in the next "push"
    
    (cond ((ldb-test #8r0001 v1)		; push toward left edge
	   (incf y1 (truncate (* (- y2 y1) (- xmin x1)) (- x2 x1)))
	   (setf x1 xmin)
	   )
	  ((ldb-test #8r0101 v1)		; push toward right edge
	   (incf y1 (truncate (* (- y2 y1) (- xmax-1 x1)) (- x2 x1)))
	   (setf x1 xmax-1)
	   )
	  )
    (cond ((ldb-test #8r0201 v1)		; push toward top
	   
	   ;; It is possible that y2 = y1 at this point because of the
	   ;; effects of the last "push", but in that case x2 is
	   ;; probably equal to x1 as well (or at least close to it) so
	   ;; we needn't draw anything: 
	   
	   (if (= y2 y1) (return nil))
	   (incf x1 (truncate (* (- x2 x1) (- xmin y1)) (- y2 y1)))
	   (setf y1 ymin)
	   )
	  ((ldb-test #8r0301 v1)		; push toward bottom
	   ;; same:
	   (if (= y2 y1) (return nil))
	   (incf x1 (truncate (* (- x2 x1) (- ymax-1 y1)) (- y2 y1)))
	   (setf y1 ymax-1)
	   )
	  )
    finally
    (when exchanged (rotatef x1 x2) (rotatef y1 y2))
    (return (values x1 y1 x2 y2))    ;; return clipped points
    )		
  )


;;;======================================================================
;;;			 Clipping-Rectangle Macros
;;;======================================================================

(xdefmacro clip-point? (x y xmin xmax ymin ymax)
  
  `(not (and (<= ,xmin ,x ,xmax) (<= ,ymin ,y ,ymax)))
  )

;;;---------------------------------------------------------------------------
;;;
;;; Is rectangle 0 not within rectangle 1?
;;;

(xdefmacro clip-rectangle?

	  (xmin0 xmax0 ymin0 ymax0
	   xmin1 xmax1 ymin1 ymax1
	   )

  `(or (< ,xmin0 ,xmin1)
       (> ,xmax0 ,xmax1)
       (< ,ymin0 ,ymin1)
       (> ,ymax0 ,ymax1)
       )
  )





