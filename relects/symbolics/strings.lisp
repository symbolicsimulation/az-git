;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: My-TV -*-
;;;
;;; Copyright 1986. John Alan McDonald. All Rights Reserved. 
;;;
;;;======================================================================

;;;======================================================================
;;;			 Characters and Strings
;;;======================================================================
;;;
;;; The original version in tv:graphics-mixin didn't do any clipping.
;;; Would like to have a version that draws partial characters when clipping
;;; occurs; same for draw-string.
;;; 

(defmethod (Graphics-Mixin :draw-char)
	   
	   (font char x y &optional (alu tv:char-aluf))
  
  (let* ((table (tv:font-char-width-table font))
	 (xmax0 (- (+ x
		      (if table
			  (aref table (ldb tv:%%ch-char char))
			  (tv:font-char-width font)
			  )
		      )
		   1
		   )
		)
	 (ymax0 (- (+ y (tv:font-char-height font)) 1))
	 )
    
    (if (send clipping-region :contains-rectangle? x xmax0 y ymax0)
	(tv:prepare-sheet (self)
	  (send tv:screen :%draw-char font char x y alu self)
	  )
	)
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draw character so left-side baseline is at x,y and clip if whole
;;; character won't fit
;;;

(defun-method draw-char-to-right Graphics-Mixin
	      
	      (char font x y h w baseline alu)
  
  (let* ((new-x (- x (tv:left-kern char font)))
	 (new-y (- y baseline))
	 (xmax0 (- (+ new-x w) 1))
	 (ymax0 (- (+ new-y h) 1))
	 )
    (if (send clipping-region :contains-rectangle? new-x xmax0 new-y ymax0)
	(tv:prepare-sheet (self)
	  (send tv:screen :%draw-char font char new-x new-y alu self)
	  )
	;; else
	;;(dbg) ;; find out what's getting clipped
	)
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draw character so right-side baseline is at x,y and clip
;;; if whole character won't fit
;;;

(defun-method draw-char-to-left Graphics-Mixin
	      
	      (char font x y h w baseline alu)
  
  (let* ((new-x (- x w (tv:left-kern char font)))
	 (new-y (- y baseline))
	 (xmax0 (- (+ new-x w) 1))
	 (ymax0 (- (+ new-y h) 1))
	 )
    (if (send clipping-region :contains-rectangle? new-x xmax0 new-y ymax0) 
	(tv:prepare-sheet (self)
	  (send tv:screen :%draw-char font char new-x new-y alu self)
	  ) 
	)
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draw character so center of baseline is at x, y and clip
;;; if whole character won't fit.
;;; 

(defun-method draw-char-centered Graphics-Mixin
	      
	      (char font x y h w baseline alu)
  
  (let* ((new-x (- x (truncate w 2) (tv:left-kern char font)))
	 (new-y (- y baseline))
	 (xmax0 (- (+ new-x w) 1))
	 (ymax0 (- (+ new-y h) 1))
	 )
    (if (send clipping-region :contains-rectangle? new-x xmax0 new-y ymax0) 
	(tv:prepare-sheet (self)
	  (send tv:screen :%draw-char font char new-x new-y alu self)
	  ) 
	)
    )
  )


;;;----------------------------------------------------------------------
;;;
;;; DEFUN-METHODS used by (GRAPHICS-MIXIN :DRAW-STRING).  These
;;; definitions must appear before they are used so that the compiler
;;; optimiziations will happen.
;;;
;;; Draw a (perhaps multi-font) string horizontally, expanding or compressing.
;;; 

(defun-method draw-string-horizontal graphics-mixin
	      
	      ( string
	       last-index x1 y
	       extra extra-per-gap
	       period
	       shortfall back-p fat-p alu font-table default-font
	       )
  
  (loop
    for i from 0 to last-index
    for char = (if back-p
		   (aref string (- last-index i))
		   (aref string i)
		   )
    for font = (if fat-p
		   (aref font-table (ldb tv:%%ch-font char))
		   default-font
		   )
    for font-char-width-table = (tv:font-char-width-table font)
    for font-baseline = (tv:font-baseline font)
    for font-height = (tv:font-char-height font)
    with adjust = (cond ((minusp extra) -1)
			((zerop extra) 0)
			(t 1)
			)
    for condition = (and period
			 (not (zerop shortfall))
			 (zerop (remainder i period))
			 )
    for x = x1 then (if back-p
			(- x ch-width extra-per-gap
			   (cond
			     (condition
			      (setq shortfall (- shortfall adjust))
			      adjust
			      )
			     (t 0)
			     )
			   )
			(+ x ch-width extra-per-gap
			   (cond
			     (condition
			      (setq shortfall (- shortfall adjust))
			      adjust
			      )
			     (t 0)
			     )
			   )
			)
    for ch-width = (if font-char-width-table
		       (aref font-char-width-table (ldb tv:%%ch-char char))
		       (tv:font-char-width font)
		       )
    do (if back-p
	   (draw-char-to-left
	     (ldb tv:%%ch-char char) font x y font-height ch-width
	     font-baseline alu
	     )
	   (draw-char-to-right
	     (ldb tv:%%ch-char char) font x y font-height ch-width
	     font-baseline alu
	     )
	   )
    finally (return (values x y))
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draw a (perhaps multi-font) string vertically, expanding or compressing.
;;; 

(defun-method draw-string-vertical graphics-mixin
	      
	      (string
		last-index y1 x extra extra-per-gap
		period shortfall
		fat-p alu font-table default-font right-p)
  
  (loop
    for i from 0 to last-index
    for char = (aref string i)
    for font = (if fat-p
		   (aref font-table (ldb tv:%%ch-font char))
		   default-font
		   )
    for font-char-width-table = (tv:font-char-width-table font)
    for font-baseline = 0 ;; to fake out character drawing code
    for font-height = (tv:font-char-height font)
    with adjust = (cond ((minusp extra) -1)
			((zerop extra) 0)
			(t 1)
			)
    for condition = (and period
			 (not (zerop shortfall))
			 (zerop (remainder i period))
			 )
    for y = y1 then (+ y font-height extra-per-gap
		       (cond (condition
			      (setq shortfall (- shortfall adjust))
			      adjust
			      )
			     (t 0)
			     )
		       )
    for ch-width = (if font-char-width-table
		       (aref font-char-width-table (ldb tv:%%ch-char char))
		       (tv:font-char-width font)
		       )
    do (if right-p
	   (draw-char-to-right (ldb tv:%%ch-char char)
			       font x y font-height ch-width
			       font-baseline alu
			       )
	   (draw-char-to-left (ldb tv:%%ch-char char)
			      font x y font-height ch-width
			      font-baseline alu
			      )
	   )
    finally (return (values x y))
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draw a (perhaps multi-font) string diagonally, expanding or compressing.
;;; 

(defun-method draw-string graphics-mixin
	      
	      (string
		last-index x1 y1 x2 y2
		x-extra y-extra
		x-extra-per-gap y-extra-per-gap
		x-period y-period shortfall-x
		shortfall-y back-p fat-p alu font-table default-font
		delta-x delta-y stretch-p
		)
  
  (loop
    for i from 0 to last-index
    for char = (if back-p
		   (aref string (- last-index i))
		   (aref string i)
		   )
    for font = (if fat-p
		   (aref font-table (ldb tv:%%ch-font char))
		   default-font
		   )
    for font-char-width-table = (tv:font-char-width-table font)
    for font-baseline = (tv:font-baseline font)
    with x-adjust = (cond ((minusp x-extra) -1)
			  ((zerop x-extra) 0)
			  (t 1)
			  )
    and y-adjust = (cond ((minusp y-extra) -1)
			 ((zerop y-extra) 0)
			 (t 1)
			 )
    with up-p = (< y2 y1)
    for condition-x = (and x-period
			   (not (zerop shortfall-x))
			   (zerop (remainder i x-period))
			   )
    and condition-y = (and y-period
			   (not (zerop shortfall-y))
			   (zerop (remainder i y-period))
			   )
    for lx = x1 then (if back-p
			 (- lx ch-width x-extra-per-gap
			    ;; (break back)
			    (cond (condition-x
				   (setq shortfall-x
					 (- shortfall-x x-adjust)
					 )
				   x-adjust
				   )
				  (t 0)
				  )
			    )
			 (+ lx ch-width x-extra-per-gap
			    ;; (break forward)
			    (cond (condition-x
				   (setq shortfall-x
					 (- shortfall-x x-adjust)
					 )
				   x-adjust
				   )
				  (t 0)
				  )
			    )
			 )
    for ly = y1 then (if up-p
			 (- ly font-height y-extra-per-gap
			    ;; (break up)
			    (cond (condition-y
				   (setq shortfall-y
					 (- shortfall-y y-adjust)
					 )
				   y-adjust
				   )
				  (t 0)
				  )
			    )
			 (+ ly font-height y-extra-per-gap
			    ;; (break down)
			    (cond (condition-y
				   (setq shortfall-y
					 (- shortfall-y y-adjust)
					 )
				   y-adjust
				   )
				  (t 0)
				  )
			    )
			 )
    for ch-width = (if font-char-width-table
		       (aref font-char-width-table (ldb tv:%%ch-char char))
		       (tv:font-char-width font)
		       )
    for font-height = (tv:font-char-height font)
    with x-longer-p = (> (abs delta-x) (abs delta-y))
    with test = (truncate (if x-longer-p (abs delta-x) (abs delta-y)) 2)
    with x = x1 and y = y1
    with right-p = (> x2 x1)
    do
    (multiple-value-setq
      (x y test) (tv:next-free-pixel
		   x y test delta-x delta-y
		   x-longer-p lx ly up-p right-p stretch-p
		   )
      )
    (if right-p
	(draw-char-to-right
	  (ldb tv:%%ch-char char) font x y font-height ch-width font-baseline
	  alu
	  )
	(draw-char-to-left
	  (ldb tv:%%ch-char char) font x y font-height ch-width font-baseline
	  alu
	  )
	)
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draws a character string between given points.  If stretch-p is
;;; non-nil, the characters will be spaced to closely fit between the
;;; points; otherwise, they merely use the line defined by the points.
;;; If toward-x is less than x1, the string ends at x1 y1; otherwise it
;;; starts there and in both cases it will read from left to right."
;;; 
  
(defmethod (Graphics-Mixin :draw-string)
	   
	   ( string x1 y1
	    &optional
	    (x2 (1+ x1))
	    (y2 y1)
	    (stretch-p nil)
	    (font (tv:sheet-current-font self))
	    (alu tv:char-aluf)
	    )
  
  (let* (;; coerce to zetalisp string for the present
	 (string (zl:string string)) 
	 (length (zl:array-active-length string))
	 ;; highest aref index and also number of gaps between characters
	 (end-displacement (1- length))
	 (delta-x (- x2 x1))
	 (delta-y (- y2 y1))
	 (font-table (send self :font-map))
	 ;; predicate for 16-bit strings
	 (fat-string-p (eql 'art-fat-string (zl:array-type string)))
	 ;; predicate for backwards strings
	 (backwards-p (minusp delta-x))
	 (vertical-p (zerop delta-x))
	 (horizontal-p (zerop delta-y))
	 )
    (cond
      (horizontal-p
       (if stretch-p
	   (multiple-value-bind
	     (x-desired ignore) (tv:string-normal-size
				  string end-displacement
				  fat-string-p font-table font
				  )
	     (let* (;; + = expand
		    (excess-x-space (- (abs delta-x) (abs x-desired)))
		    (excess-x-per-gap
		      (truncate excess-x-space end-displacement)
		      )
		    (shortfall-x (- excess-x-space
				    (* end-displacement excess-x-per-gap)
				    )
				 )
		    (x-period (if (zerop shortfall-x)
				  nil
				  (truncate end-displacement shortfall-x)
				  )
			      )
		    )
	       (draw-string-horizontal
		 string end-displacement x1 y1
		 excess-x-space excess-x-per-gap x-period
		 shortfall-x backwards-p fat-string-p alu
		 font-table font
		 )
	       )
	     )
	   ;; else
	   (draw-string-horizontal
	     string end-displacement x1 y1 0 0 nil 0
	     backwards-p fat-string-p alu font-table font
	     )
	   )
       )
      (vertical-p
       (let* (;; canonicalize vertical to always go down
	      (y-start (min y1 y2))	
	      (y-end (max y1 y2))
	      ;; put chars on right side of line if down
	      (right-flag (< y1 y2))	
	      (delta-y (- y-end y-start))
	      )
	 (if stretch-p
	     (multiple-value-bind
	       (ignore y-desired) (tv:string-normal-size
				    string end-displacement fat-string-p
				    font-table font
				    )
	       (let* ((excess-y-space (- (abs delta-y) (abs y-desired)))
		      (excess-y-per-gap
			(truncate excess-y-space end-displacement)
			)
		      (shortfall-y (- excess-y-space
				      (* end-displacement excess-y-per-gap)
				      )
				   )
		      (y-period (if (zerop shortfall-y)
				    nil
				    (truncate end-displacement shortfall-y)
				    )
				)
		      )
		 (draw-string-vertical
		   string end-displacement y-start x1
		   excess-y-space excess-y-per-gap y-period
		   shortfall-y fat-string-p alu font-table font
		   right-flag
		   )
		 )
	       )
	     ;; else
	     (draw-string-vertical
	       string end-displacement y-start x1
	       0 0 nil 0 fat-string-p alu font-table font
	       right-flag
	       )
	     )
	 )
       )
      (t
       (if stretch-p
	   (multiple-value-bind
	     (x-desired y-desired) (tv:string-normal-size
				     string end-displacement fat-string-p
				     font-table font
				     )
	     (let* ((excess-x-space (- (abs delta-x) (abs x-desired)))
		    (excess-x-per-gap
		      (truncate excess-x-space end-displacement)
		      )
		    (shortfall-x (- excess-x-space
				    (* end-displacement excess-x-per-gap)
				    )
				 )
		    (x-period (if (zerop shortfall-x)
				  nil
				  (truncate end-displacement shortfall-x)
				  )
			      )
		    (excess-y-space (- (abs delta-y) (abs y-desired)))
		    (excess-y-per-gap
		      (truncate excess-y-space end-displacement))
		    (shortfall-y (- excess-y-space
				    (* end-displacement excess-y-per-gap)
				    )
				 )
		    (y-period (if (zerop shortfall-y)
				  nil
				  (truncate end-displacement shortfall-y)
				  )
			      )
		    )
	       (draw-string
		 string end-displacement x1 y1 x2 y2
		 excess-x-space excess-y-space excess-x-per-gap
		 excess-y-per-gap x-period y-period shortfall-x shortfall-y
		 backwards-p fat-string-p alu font-table font
		 delta-x delta-y stretch-p
		 )
	       )
	     )
	   ;; else
	   (draw-string
	     string end-displacement x1 y1 x2 y2
	     0 0 0 0
	     nil nil 0 0 backwards-p fat-string-p alu font-table
	     font delta-x delta-y stretch-p
	     )
	   )
       )
      )
    )
  )

;;;----------------------------------------------------------------------

(defmethod (Graphics-Mixin :string-width)
	   
	   (string
	     &optional
	     (font (tv:sheet-current-font self))
	     )
  
  (loop
    with zl-string = (zl:string string) 
    with font-table = (send self :font-map)
    with fat-chars? = (eql 'art-fat-string (zl:array-type zl-string))
    for char being each array-element of zl-string
    for the-font = (if fat-chars?
		       (aref font-table (ldb tv:%%ch-font char))
		       font
		       )
    for height   = (tv:font-char-height the-font)
    maximize height
    )
  )


;;;----------------------------------------------------------------------
;;;
;;; Draw a string so that the baselines of the characters are lined up,
;;; but so that the bottom of the lowest character box is at (- y 1),
;;; that is, just above <y>.  This is in contrast to <:draw-string>,
;;; which puts the characters' baselines at <y>. (The base line is not
;;; at the bottom of the character box, but rather at the bottom of a
;;; character like #\o. The character box allows room below the baseline
;;; for the descenders in characters like #\y.
;;;
;;;  To begin, this only works for horizontal strings.  It hasn't been
;;;  tested with fat strings, because they don't make sense in Common
;;;  Lisp anyway.
;;;
;;; This will all have to change when we have real Common Lisp I/O.
;;;

(defmethod (Graphics-Mixin :draw-string-above)
	   
	   (string
	     x1 y1
	     &optional
	     (x2 (1+ x1))
	     (y2 y1)
	     (stretch-p nil)
	     (font (tv:sheet-current-font self))
	     (alu tv:char-aluf)
	     )
  
  (loop
    with zl-string = (zl:string string) 
    with font-table = (send self :font-map)
    with fat-chars? = (eql 'art-fat-string (zl:array-type zl-string))
    for char being each array-element of zl-string
    for the-font = (if fat-chars?
		       (aref font-table (ldb tv:%%ch-font char))
		       font
		       )
    for height   = (tv:font-char-height the-font)
    for baseline = (tv:font-baseline the-font)
    maximize (- height baseline) into y-shift
    finally
    (send self :draw-string
	  zl-string
	  x1 (- y1 y-shift)
	  x2 (- y2 y-shift)
	  stretch-p
	  font
	  alu
	  )
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draw a string so that the baselines of the characters are lined up,
;;; but so that the top of the highest character box is at (+ y 1), that
;;; is, just below <y>.  This is in contrast to <:draw-string>, which
;;; puts the characters' baselines at <y>. (The base line is not at the
;;; bottom of the character box, but rather at the bottom of a character
;;; like #\o. The character box allows room below the baseline for the
;;; descenders in characters like #\y.
;;;
;;;  It hasn't been tested with fat strings, because they don't make
;;;  sense in Common Lisp anyway.
;;;
;;; This will all have to change when we have real Common Lisp I/O.
;;; 

(defmethod (Graphics-Mixin :draw-string-below)
	   
	   ( string x1 y1
	    &optional
	    (x2 (1+ x1)) (y2 y1)
	    (stretch-p nil)
	    (font (tv:sheet-current-font self))
	    (alu tv:char-aluf)
	    )
  
  (loop
    with zl-string  = (zl:string string) 
    with font-table = (send self :font-map)
    with fat-chars? = (eql 'art-fat-string (zl:array-type zl-string))
    for char being each array-element of zl-string
    for the-font = (if fat-chars?
		       (aref font-table (ldb tv:%%ch-font char))
		       font
		       )
    maximize (tv:font-baseline the-font) into y-shift
    finally
    (send self :draw-string zl-string
	  x1 (+ y1 y-shift 1)
	  x2 (+ y2 y-shift 1)
	  stretch-p font alu
	  )
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draws a string so that the region occupied by the string is centered
;;; over the region specified by [x1,x2] X [y1,y2].
;;;
;;; To begin, only for horizontal strings...
;;; 

(defmethod (Graphics-Mixin :draw-string-horizontal-centered)
	   
	   (string
	     x1 y1 x2 y2
	     &optional
	     (stretch-p nil)
	     (font      (tv:sheet-current-font self))
	     (alu       tv:char-aluf)
	     )
  
  ;; This first computes the region occupied by horizontal string and then
  ;; shifts the region so that it is centered over  [x1,x2] X [y1,y2],
  ;; before drawing the string. To do this for general strings, all we need
  ;; is a method to compute the region occupied by a general string.
  (let* ((zl-string (zl:string string))
	 (x3 0)
	 (y3 0)
	 (x4 (send self :string-length zl-string 0 nil nil font 0))
	 (y4 (send self :string-width zl-string font))
	 )
    (multiple-value-bind (x3 x4) (center-interval-on x3 x4 x1 x2)
      (multiple-value-bind (y3 y4) (center-interval-on y3 y4 y1 y2)
	(ignore y4)
	(send self :draw-string-below
	      zl-string
	      x3  (- y3 1)
	      x4  (- y3 1)
	      stretch-p font alu
	      )
	)
      )
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; Draws a string so that the right-most baseline is at x2, y2.
;;;
;;; To begin, only for horizontal strings...
;;; 

(defmethod (Graphics-Mixin :draw-string-right-adjusted)
	   
	   (string
	     x1 y1 x2 y2
	     &optional
	     (stretch-p nil)
	     (font      (tv:sheet-current-font self))
	     (alu       tv:char-aluf)
	     )

  (ignore x1 y1)
  
  (let* ((zl-string (zl:string string))
	 (x4 (send self :string-length zl-string 0 nil nil font 0))
	 )
    (send self :draw-string
	  zl-string
	  (- x2 x4) y2
	  x2 y2
	  stretch-p font alu
	  )
    )
  )

