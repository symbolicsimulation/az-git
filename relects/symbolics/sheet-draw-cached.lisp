#|| 

(defmacro sheet-draw-cached (alu (operation . rest))
  
  `(if (numberp ,alu)
       ,(if (eql operation '%draw-point)
	    `(setf (aref ,(fourth rest) ,(first rest) ,(second rest))
		   (boole ,alu
			  ,(or (fifth rest) -1)
			  (aref ,(fourth rest) ,(first rest) ,(second rest))
			  )
		   )
	    ;; else
	    `(,operation . ,rest)
	    )
       (,(intern (string-append "SHEET-" (substring (string operation) 1))
		 'tv
		 )
	. ,rest
	)
       )
  )
||#

;;; install this version and recompile for a system with color software:

;;;
;;; There was a bug (a reference to <alu> without a comma,
;;; which caused package problems)
;;; in the version of this in >rel-6>color>extended-alu.lisp.
;;; 

(defmacro sheet-draw-cached (alu (operation . rest))
  
  `(if (numberp ,alu)
       ,(if (eq operation '%draw-point)
 	    (let ((sc (fourth rest))
		  (x (first rest))
		  (y (second rest))
		  )
	      `(setf (aref ,sc ,x ,y)
		     (if (zerop (logand color:%paint-alu ,alu))
			 (logior
			   (logand
			     tv:bitblt-unpacking-mask
			     (boole
			       ,alu
			       tv:tv:bitblt-unpacking-constant
			       (aref ,sc ,x ,y) 
			       )
			     )
			   (boole
			     tv:alu-andca
			     tv:bitblt-unpacking-mask
			     (aref ,sc ,x ,y) 
			     )
			   )
			 (let ((fn (assq ,alu color:all-the-alu-fns)))
			   (if fn
			       (funcall (cadr fn)
					tv:bitblt-unpacking-constant
					(aref ,sc ,x ,y) 
					tv:bitblt-unpacking-mask
					)
			       (ferror "~a is an undefined alu" ,alu)
			       )
			   )
			 )
		     )
	      )
	    `(,operation . ,rest)
	    )
       (,(intern
	   (string-append "SHEET-" (substring (string operation) 1))
	   "tv"
	   )
	. ,rest
	)
       )
  )

