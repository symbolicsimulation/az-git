;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: (MY-TV SCL) -*-
;;;
;;; Copyright 1986. John Alan McDonald. All Rights Reserved. 
;;;
;;;======================================================================
;;;
;;; The purpose of this file is to provide cleaner graphics operations,
;;; in particular, without having to deal with both "outside" and
;;; "inside coordinates".
;;; 

;;; flavors to export:

(export '(Graphics-Mixin))

;;; instance-variable names to export:

(export '(xmin xmax ymin ymax clipping-region))

;;;======================================================================
;;;			     Graphics-Mixin
;;;======================================================================
;;;
;;; This is essentially a device driver for windows that do graphics.
;;; 

(defflavor Graphics-Mixin
	   
	   (;; instance variables
	    (1d-screen  nil)
	    (1d-bit-save nil)
	    ;; used for optimized graphics operations, eg. :draw-points
	    (clipping-region (make-instance 'region))
	    )
	   
	   (tv:Graphics-Mixin)
  
  (:required-flavors tv:essential-window)
  
  (:method-order
   :point
   :draw-point
   :draw-char
   :draw-line
   :draw-rectangle
   :draw-lines
   :draw-triangle
   )
  (:gettable-instance-variables clipping-region)
  (:settable-instance-variables clipping-region)
  )

;;;-----------------------------------------------------------------

(defmethod (:init Graphics-Mixin :after)  (init-plist)
  (ignore init-plist)
  
  (let ((sc-array (send tv:screen :screen-array)))
    (setf 1d-screen (make-array (apply #'* (array-dimensions sc-array)) 
				:element-type (array-element-type sc-array) 
				:displaced-to sc-array
				)
	  )
    (unless (null tv:bit-array)
      (setf 1d-bit-save
	    (make-array (apply #'* (array-dimensions tv:bit-array))
			:element-type (array-element-type tv:bit-array)
			:displaced-to tv:bit-array 
			)
	    )
      )
    )
  
  (send self :set-clipping-region-from-margins)
  )


;;;-----------------------------------------------------------------------
;;;	      Clipping messages 
;;;-----------------------------------------------------------------------

(defmethod (:set-clipping-region-bounds Graphics-Mixin)

	   (nxmin nxmax nymin nymax)
  
  (setf (send clipping-region :xmin) nxmin)
  (setf (send clipping-region :xmax) nxmax)
  (setf (send clipping-region :ymin) nymin)
  (setf (send clipping-region :ymax) nymax)
  (send self :set-margins-from-clipping-region)
  )

;;;-----------------------------------------------------------------------

(defmethod (:clipping-region-bounds Graphics-Mixin)()
  
  (send clipping-region :bounds)
  )

;;;-----------------------------------------------------------------------

(defmethod (:set-margins-from-clipping-region Graphics-Mixin) ()
  
  (setf tv:left-margin-size                (send clipping-region :xmin)   )
  (setf tv:right-margin-size  (- tv:width  (send clipping-region :xmax) 1))
  (setf tv:top-margin-size                 (send clipping-region :ymin)   )
  (setf tv:bottom-margin-size (- tv:height (send clipping-region :ymax) 1))
  )
;
;;;-----------------------------------------------------------------------

(defmethod (:set-clipping-region-from-margins Graphics-Mixin) ()
  
  (setf (send clipping-region :xmin) tv:left-margin-size)
  (setf (send clipping-region :xmax) (- tv:width  tv:right-margin-size  1))
  (setf (send clipping-region :ymin) tv:top-margin-size)
  (setf (send clipping-region :ymax) (- tv:height tv:bottom-margin-size 1))
  )

;;;----------------------------------------------------------------------

(defmethod (:set-clipping-region Graphics-Mixin :after) (new-region)

  (ignore new-region)
  
  (send self :set-margins-from-clipping-region)
  )

;;;----------------------------------------------------------------------

(defmethod (:change-of-size-or-margins Graphics-Mixin :after) (&rest args)

  (ignore args)

  (send self :set-clipping-region-from-margins)
  )

;;;----------------------------------------------------------------------

(defmethod (:set-margins Graphics-Mixin) (ll rr tt bb)

  (setf tv:left-margin-size   ll)
  (setf tv:right-margin-size  rr)
  (setf tv:top-margin-size    tt)
  (setf tv:bottom-margin-size bb)
  (send self :set-clipping-region-from-margins)
  )

;;;----------------------------------------------------------------------

(defmethod (:set-edges Graphics-Mixin :after) (&rest new-edges)

  (ignore new-edges)
  
  (send self :set-clipping-region-from-margins)
  )



;;;----------------------------------------------------------------------
;;;			Assorted Drawing Primitives 
;;;----------------------------------------------------------------------

(defmethod (:draw-triangle Graphics-Mixin)
	   
	   (x1 y1 x2 y2 x3 y3 &optional (alu tv:char-aluf))
  
  (tv:prepare-sheet (self)
    (tv:sheet-draw-triangle
      x1 y1 x2 y2 x3 y3 alu self
      (send clipping-region :xmin)
      (send clipping-region :ymin)
      (send clipping-region :xmax)
      (send clipping-region :ymax)
      )
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; b&w version:
;;;

#|| 

(defmacro sheet-draw-cached (alu (operation . rest))
  
  `(if (numberp ,alu)
       ,(if (eql operation '%draw-point)
	    `(setf (aref ,(fourth rest) ,(first rest) ,(second rest))
		   (boole ,alu
			  ,(or (fifth rest) -1)
			  (aref ,(fourth rest) ,(first rest) ,(second rest))
			  )
		   )
	    ;; else
	    `(,operation . ,rest)
	    )
       (,(intern (string-append "SHEET-" (substring (string operation) 1))
		 'tv
		 )
	. ,rest
	)
       )
  )
||#

;;;----------------------------------------------------------------------
;;; if color system isn't loaded, we will need these defs:

(eval-when (compile load eval)
  (unless (boundp 'color:%paint-alu)
    ;; there's probably a better way to test
    ;; if the color system is loaded than this.

    (defconstant color:%%extended-alu-opcode (byte 7 24.))
    (defconstant color:%paint-alu (si:%logdpb 1 color:%%extended-alu-opcode 0)
      "Special alus for paint"
      )
    (defvar color:all-the-alu-fns nil)
    (si:defvar-standard tv:bitblt-unpacking-constant -1)
    (si:defvar-standard tv:bitblt-unpacking-mask -1)
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; There was a bug (a reference to <alu> without a comma,
;;; which caused package problems)
;;; in the version of this in >rel-6>color>extended-alu.lisp.
;;; 

(defmacro sheet-draw-cached (alu (operation . rest))
  
  `(if (numberp ,alu)
       ,(if (eq operation '%draw-point)
 	    (let ((sc (fourth rest))
		  (x (first rest))
		  (y (second rest))
		  )
	      `(setf (aref ,sc ,x ,y)
		     (if (zerop (logand color:%paint-alu ,alu))
			 (logior
			   (logand
			     tv:bitblt-unpacking-mask
			     (boole
			       ,alu
			       tv:bitblt-unpacking-constant
			       (aref ,sc ,x ,y) 
			       )
			     )
			   (boole
			     tv:alu-andca
			     tv:bitblt-unpacking-mask
			     (aref ,sc ,x ,y) 
			     )
			   )
			 (let ((fn (assq ,alu color:all-the-alu-fns)))
			   (if fn
			       (funcall (cadr fn)
					tv:bitblt-unpacking-constant
					(aref ,sc ,x ,y) 
					tv:bitblt-unpacking-mask
					)
			       (ferror "~a is an undefined alu" ,alu)
			       )
			   )
			 )
		     )
	      )
	    `(,operation . ,rest)
	    )
       (,(intern
	   (string-append "SHEET-" (substring (string operation) 1))
	   "tv"
	   )
	. ,rest
	)
       )
  )

;;;----------------------------------------------------------------------

(defmacro draw-clipped-point-cached (x y alu array)
  
  `(if (send clipping-region :contains-point? ,x ,y)
     (sheet-draw-cached ,alu (%draw-point ,x ,y ,alu ,array))
     )
  )

;;;----------------------------------------------------------------------

(defmethod (:draw-circle-internal Graphics-Mixin)
	   
	   (alu array center-x center-y radius)
  
  (tv:prepare-sheet (self)
    (loop
      with y = 0
      with f = 0
      with x = radius
      initially
      (when (= radius 0)
	(draw-clipped-point-cached center-x center-y alu array)
	(return t)
	)
      do
      (draw-clipped-point-cached (+ center-x x) (- center-y y) alu array)
      (draw-clipped-point-cached (- center-x x) (+ center-y y) alu array)
      (draw-clipped-point-cached (+ center-x y) (+ center-y x) alu array)
      (draw-clipped-point-cached (- center-x y) (- center-y x) alu array)
      
      (setq f (+ f y y 1) y (1+ y))
      (if (>= f x) (setq f (- f x x -1) x (- x 1)))
      (if (> y x) (return t))
      
      (draw-clipped-point-cached (+ center-x x) (+ center-y y) alu array)
      (draw-clipped-point-cached (- center-x x) (- center-y y) alu array)
      (draw-clipped-point-cached (+ center-x y) (- center-y x) alu array)
      (draw-clipped-point-cached (- center-x y) (+ center-y x) alu array)
      
      (if (= y x) (return t))
      )
    )
  )


;;;----------------------------------------------------------------------

(defmacro draw-clipped-octant-point-cached
	  
	  (i xv yv reverse-sense y limit-arr alu array)
  
  `(unless (eql ,reverse-sense
		(and (>= ,y (aref ,limit-arr 0 ,i))
		     (<= ,y (aref ,limit-arr 1 ,i))
		     )
		)
     (draw-clipped-point-cached ,xv ,yv ,alu ,array)
     )
  )

;;;----------------------------------------------------------------------

(defmethod (:draw-circular-arc-cached-internal Graphics-Mixin)
	   
	   (alu array cx cy radius start-theta end-theta)
  
  (using-resource (limit-arr tv:circular-arc-array)
    (multiple-value-bind
      (ignore reverse-sense)
	(tv:compute-octant-endpoints limit-arr radius start-theta end-theta)
      (loop
	with y = 0
	with x = radius
	for f = (+ y y 1)  then (+ f y y 1) 
	do
	(draw-clipped-octant-point-cached
	  0 (+ cx x) (- cy y) reverse-sense y limit-arr alu array)
	(draw-clipped-octant-point-cached
	  4 (- cx x) (+ cy y) reverse-sense y limit-arr alu array)
	(draw-clipped-octant-point-cached
	  6 (+ cx y) (+ cy x) reverse-sense y limit-arr alu array)
	(draw-clipped-octant-point-cached
	  2 (- cx y) (- cy x) reverse-sense y limit-arr alu array)
	
	(incf y)
	(when (>= f x) (decf f (+ x x -1)) (decf x))
	(if (> y x) (return t))
	
	(draw-clipped-octant-point-cached
	  7 (+ cx x) (+ cy y) reverse-sense y limit-arr alu array)
	(draw-clipped-octant-point-cached
	  3 (- cx x) (- cy y) reverse-sense y limit-arr alu array)
	(draw-clipped-octant-point-cached
	  1 (+ cx y) (- cy x) reverse-sense y limit-arr alu array)
	(draw-clipped-octant-point-cached
	  5 (- cx y) (+ cy x) reverse-sense y limit-arr alu array)
	(if (= y x) (return t))
	)
      )
    )
  )



;;;----------------------------------------------------------------------

(defun-in-flavor (draw-clipped-rectangle Graphics-Mixin)
	      
	      (xmin0 xmax0 ymin0 ymax0 alu)
  
  (multiple-value-bind
    (xmin1 xmax1 ymin1 ymax1)
      (send clipping-region :clip-rectangle xmin0 xmax0 ymin0 ymax0) 
    (unless (null xmin1)
      (send tv:screen :%draw-rectangle
	(+ (- xmax1 xmin1) 1) (+ (- ymax1 ymin1) 1) ;; width and height
	xmin1 ymin1
	alu
	self ;;tv:screen-array
	)
      )
    )
  ) 

;;;----------------------------------------------------------------------

(defmethod (:draw-rectangle Graphics-Mixin)
	   (w h x y &optional (alu tv:char-aluf))
  
  (let ((xmax0 (- (+ x w) 1))
	(ymax0 (- (+ y h) 1))
	)
    (tv:prepare-sheet (self)
      (draw-clipped-rectangle x xmax0 y ymax0 alu)
      )
    )
  )

;;;----------------------------------------------------------------------

(defmethod (:draw-filled-in-circle-cached-internal Graphics-Mixin)
	   
	   (alu array cx cy radius)

  (ignore array) ;; it's just the same as tv:screen-array

  (loop
    with x = radius
    with y = 0
    with old-y = y
    ;; the first time through, draw one big rectangle.
    initially
    (draw-clipped-rectangle (- cx x) (+ cx x) (- cy y) (+ cy y) alu)
    
    for error = (+ y y 1) then (+ error y y 1)
    when (>= error x ) ;; will the next chord be shorter?
    do
    (let ((cx+x (+ cx x))
	  (cx-x (- cx x))
	  (cx-y (- cx y))
	  (cx+y (+ cx y))
	  (cy-x (- cy x))
	  (cy+x (+ cy x))
	  (oy+1 (+ old-y 1))
	  )
      ;; draw the middle region: upper & lower rectangles.
      (draw-clipped-rectangle cx-x cx+x (- cy y) (- cy oy+1) alu)
      (draw-clipped-rectangle cx-x cx+x (+ cy oy+1) (+ cy y) alu)
      
      (setf old-y y)
      (if (= x y) (return t))			;finished?
      
      ;; top line.
      (draw-clipped-rectangle cx-y cx+y cy+x cy+x alu)
      ;; bottom line.
      (draw-clipped-rectangle cx-y cx+y cy-x cy-x alu)
      
      (decf error (+ x x -1))
      (decf x)
      )
    
    do (incf y) (if (> y x) (return t))
    )
  )


;;;----------------------------------------------------------------------

;;; Given an edge and a number of sides, draw something The sign of N
;;; determines which side of the line the figure is drawn on.  If the
;;; line is horizontal, the rest of the polygon is in the positive
;;; direction when N is positive.

(defmethod (:draw-regular-polygon Graphics-Mixin)
	   
	   ( x1 y1 x2 y2 n
	    &optional
	    (alu tv:char-aluf)
	    &aux theta
	    )
  
  (setf theta (* 3.14159 (1- (/ 2.0 n))))
  (setf n (abs n))
  (multiple-value-bind
    (xmin xmax ymin ymax) (send clipping-region :bounds)
    (tv:prepare-sheet (self)
      (do ((i 2 (1+ i))
	   (sin-theta (sin theta))
	   (cos-theta (cos theta))
	   (x0 x1)
	   (y0 y1)
	   (x3)
	   (y3)
	   )
	  ((>= i n))
	(setf x3  (- (+ (* x1 cos-theta) (* y2 sin-theta))
		     (* y1 sin-theta)
		     (* x2 (1- cos-theta))
		     )
	      )
	(setf y3 (- (+ (* x1 sin-theta) (* y1 cos-theta))
		    (* x2 sin-theta)
		    (* y2 (1- cos-theta))
		    )
	      )
	(tv:sheet-draw-triangle
	  (round x0) (round y0) (round x2) (round y2) (round x3) (round y3)
	  alu self xmin xmax ymin ymax
	  )
	(setf x1 x2)
	(setf y1 y2)
	(setf x2 x3)
	(setf y2 y3)
	)
      )
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; This didn't use inside bounds, but it has a bug when used with tv:alu-xor.
;;; The bug hasn't been fixed yet...
;;; 

(defmethod (:draw-filled-in-sector Graphics-Mixin)
	   
	   (cx cy r th1 th2 &optional (alu tv:char-aluf))
  
  (tv:prepare-sheet (self)
    (do ((y (- r) (1+ y))
            (x 0)
            (u0 0) (u1 0)				; clipped plane 1
            (v0 0) (v1 0)				; clipped plane 2
            (co-x0 (truncate (* -1000.0 (sin th1))))
            (co-y0 (truncate (*  1000.0 (cos th1))))
            (co-x1 (truncate (* -1000.0 (sin th2))))
            (co-y1 (truncate (*  1000.0 (cos th2))))
            (flag (> (abs (- th1 th2)) 3.14159))
            (r2 (* r r)))
          ((> y r))
      (setf x (isqrt (- r2 (* y y))))		; unclipped line
      (setf u0 (- x))
      (setf u1 x)
      (setf v0 (- x))
      (setf v1 x)					; init clipped lines
      
      (if (plusp (- (* co-y0 y) (* co-x0 u1)))	; clip with first plane
            (setf u1 (if (= 0 co-x0) 0 (truncate (* co-y0 y) co-x0))))
      (if (plusp (- (* co-y0 y) (* co-x0 u0)))
            (setf u0 (if (= 0 co-x0) 0 (truncate (* co-y0 y) co-x0))))
      
      (if (minusp (- (* co-y1 y) (* co-x1 v1)))	; clip with second plane
            (setf v1 (if (= 0 co-x1) 0 (truncate (* co-y1 y) co-x1))))
      (if (minusp (- (* co-y1 y) (* co-x1 v0)))
            (setf v0 (if (= 0 co-x1) 0 (truncate (* co-y1 y) co-x1))))
      
      ;; Ok, we have two lines, [u0 u1] and [v0 v1].  If the angle was greater than pi, then
      ;; draw both of them, otherwise draw their intersect-intervals.
      
      (cond (flag
	    (if (> u1 u0)
	          (send (the instance self) :draw-line
		      (+ cx u0) (+ cy y) (+ cx u1) (+ cy y) alu t)
	          )
	    (if (> v1 v0)
	          (send (the instance self) :draw-line 
		      (+ cx v0) (+ cy y) (+ cx v1) (+ cy y) alu t)
	          )
	    )
	  (t					; compute intersect-intervals
	   (let ((xmin  (max u0 v0)) (xmax (min u1 v1)))
	     (if (> xmax xmin)
	           (send (the instance self) :draw-line
		       (+ cx xmin)  (+ cy y) (+ cx xmax) (+ cy y) alu t)
	           )
	     )
	   )
	  )
      )
    )
  )


;;;----------------------------------------------------------------------
;;;			 Points
;;;----------------------------------------------------------------------

(defmethod (:point Graphics-Mixin) (x y)
  
  (if (send clipping-region :contains-point? x y )
        (tv:prepare-sheet (self) (aref tv:screen-array x y))
        ;; else
        0
        )
  )

;;;----------------------------------------------------------------------

(defmethod (:draw-point Graphics-Mixin)
	   
	   (x y &optional (alu tv:char-aluf) (value -1))
  
  (tv:prepare-sheet (self)
    (if (send clipping-region :contains-point? x y )
      (send tv:screen :%draw-point x y alu self value)
      )
    )
  )


;;;----------------------------------------------------------------------
;;;			    Lines and Curves
;;;----------------------------------------------------------------------

(defmethod (:draw-line Graphics-Mixin)
	   
	   ( x1 y1 x2 y2
	    &optional
	    (alu tv:char-aluf)
	    (draw-end-point t)
	    )
  
  (multiple-value-bind
    (x1 y1 x2 y2) (send clipping-region :clip-line x1 y1 x2 y2)
    (if x1 ;; is the line visible at all?
	(tv:prepare-sheet (self)
	  (send tv:screen :%draw-line x1 y1 x2 y2 alu draw-end-point self) 
	  )
	)
    )
  )

;;;----------------------------------------------------------------------

(defmethod (:draw-clipped-line Graphics-Mixin)
	   
	   ( x1 y1 x2 y2
	    &optional
	    (alu tv:char-aluf)
	    (draw-end-point t)
	    )
  
  (tv:prepare-sheet (self)
    (send tv:screen :%draw-line x1 y1 x2 y2 alu draw-end-point self) 
    )
  )

;;;----------------------------------------------------------------------
;;;
;;; need to include this so that :draw-dashed-line
;;; works in both 6.0 and 6.1
;;; 

(defun-in-flavor (draw-dashed-line-internal graphics-mixin)	
	      
	      ( start-x start-y
	       end-x end-y
	       x-spacing y-spacing
	       x-length y-length
	       n-segs
	       alu
	       )
  
  (tv:prepare-sheet (self)
    (loop for x from (float start-x) by x-spacing
	  for y from (float start-y) by y-spacing
	  repeat (- n-segs 1)

	  do (tv:sheet-draw-line
	       (round x) (round y)
	       (round (+ x x-length)) (round (+ y y-length))
	       alu nil self
	       )
	  
	  finally
	    (incf x x-spacing)
	    (incf y y-spacing)
	    (tv:sheet-draw-line
	      (round x) (round y)
	      (if (< start-x end-x)
		  (min end-x (round (+ x x-length)))
		  (max end-x (round (+ x x-length)))
		  )
	      (if (< start-y end-y)
		  (min end-y (round (+ y y-length)))
		  (max end-y (round (+ y y-length)))
		  )
	      alu t self
	      )
	    )
    )
  )

;;;----------------------------------------------------------------------

(defmethod (:draw-dashed-line Graphics-Mixin)
	   
	   (x1 y1 x2 y2
	       &optional alu dash-spacing space-literally-p offset dash-length
	       )
  
  (when (null alu)          (setf alu tv:char-aluf))	; reasonable default
  (when (null dash-spacing) (setf dash-spacing 20.))
  (when (null offset)       (setf offset 0))
  (when (null dash-length)  (setf dash-length (/ dash-spacing 2)))
  (check-type alu fixnum)
  (setf offset (mod offset dash-spacing))
  
  (multiple-value-setq
    (x1 y1 x2 y2) (send clipping-region :clip-line x1 y1 x2 y2)
    )
  
  (if (not (null x1)) 
      (let* ((lwidth (- x2 x1)) ;; (meaning line is completely invisible)
	     (lheight (- y2 y1))
	     (length (isqrt (+ (expt lwidth 2) (expt lheight 2))))
	     )
	(if (not space-literally-p)
	    ;; we want to draw line segments such that the last one ends up
	    ;; finishing at the end-point.  therefore, separate the entire
	    ;; line length into an odd number of regions, each half of the
	    ;; dash-spacing, and fill in every other one with a line segment.
	    ;; just use dash-spacing to decide how many segments, so the
	    ;; spacing may not be quite right.
	    (let* ((n-half-segs (logior 1 (round (/ length (/ dash-spacing 2)))))
		   (x-len (/ (float lwidth)  n-half-segs))
		   (y-len (/ (float lheight) n-half-segs))
		   )
	      (tv:draw-dashed-line-internal 
		x1 y1
		x2 y2
		(* 2 x-len) (* 2 y-len)
		x-len y-len
		(1+ (/ n-half-segs 2))
		alu
		) ;; n-half-segs is odd, so add 1
	      ) 
	    ;; else
	    ;; don't worry about where it ends.  compute the x and y components and
	    ;; just draw dashes of the exact specified size.
	    (let* ((float-length (sqrt (+ (expt lwidth 2) (expt lheight 2))))
		   (x-component (if (= 0 float-length)
				    0
				    (/ lwidth float-length)
				    )
				)
		   (y-component (if (= 0 float-length)
				    0
				    (/ lheight float-length)
				    )
				)
		   )
	      (tv:draw-dashed-line-internal
		(+ x1 (* offset x-component))
		(+ y1 (* offset y-component))
		x2
		y2
		(* dash-spacing x-component)
		(* dash-spacing y-component)
		(* dash-length x-component)
		(* dash-length y-component)
		(ceiling (/ (- float-length offset) dash-spacing))
		alu
		)
	      )
	    )
	)
      )
  
  #||
  (if x1 ;; if this isn't nil, (meaning line is completely invisible)
      (let* ((lwidth  (- x2 x1))
	     (lheight (- y2 y1))
	     (length  (isqrt (+ (expt lwidth 2) (expt lheight 2))))
	     )
	(if (not space-literally-p)
	    
	    ;; We want to draw line segments such that the last one ends
	    ;; up finishing at the end-point.  Therefore, separate the
	    ;; entire line length into an odd number of regions, each
	    ;; half of the dash-spacing, and fill in every other one
	    ;; with a line segment.  Just use dash-spacing to decide how
	    ;; many segments, so the spacing may not be quite right. 
	    
	    (let* ((n-half-segs
		     (logior 1 (round (/ length (/ dash-spacing 2))))
		     )
		   (x-len (/ (float lwidth ) n-half-segs))
		   (y-len (/ (float lheight) n-half-segs))
		   )
	      (tv:draw-dashed-line-internal 
		x1 y1
		(* 2 x-len) (* 2 y-len)
		x-len y-len
		(1+ (truncate n-half-segs 2)) ;; n-half-segs is odd, so add 1
		alu
		)
	      )	
	    
	    ;; else...  Don't worry about where it ends.  Compute the x
	    ;; and y components and just draw dashes of the exact
	    ;; specified size. 
	    
	    (let* ((float-length (sqrt (+ (expt lwidth 2) (expt lheight 2))))
		   (x-component (if (= 0 float-length)
				    0
				    (/ lwidth float-length)
				    )
				)
		   (y-component (if (= 0 float-length)
				    0
				    (/ lheight float-length)
				    )
				)
		   )
	      (tv:draw-dashed-line-internal
		(+ x1 (* offset x-component))
		(+ y1 (* offset y-component))
		(* dash-spacing x-component)
		(* dash-spacing y-component)
		(* dash-length x-component)
		(* dash-length y-component)
		(round (/ (- float-length offset) dash-spacing))
		alu
		)
	      )
	    )
	)
      )
  ||#
  )

;;;----------------------------------------------------------------------

(defmethod (:draw-wide-curve Graphics-Mixin)
	   
	   (px py curve-width &optional end (alu tv:char-aluf))
  
  (if (null end)
      (setq end (if (array-has-fill-pointer-p px)
		    (fill-pointer px)
		    ;; else
		    (length px)
		    )
	    )
      )
  (setq curve-width (/ curve-width 2.0s0))
  
  (multiple-value-bind
    (xmin xmax ymin ymax) (send clipping-region :bounds)
    (tv:prepare-sheet (self)
      (do ((i 0 (1+ i))
	   (x0) (y0) (x1) (y1) (px1) (py1) (px2) (py2) (px3) (py3) (px4) (py4)
	   )
	  ((>= i end))
	(setf x0 x1)
	(or (setf x1 (aref px i)) (return nil))
	(setf y0 y1)
	(or (setf y1 (aref py i)) (return nil))
	(or (= i 0)
	    (let ((dx (- x1 x0)) (dy (- y1 y0)) len)
	      (setf len (sqrt (+ (* dx dx) (* dy dy))))
	      (and (zerop len) (= i 1) (setf len 1))
	      (unless (zerop len)
		(psetf dx (/ (* curve-width dy) len)
		       dy (/ (* curve-width dx) len)
		       )
		(if (= i 1)
		    (setf px1 (floor (- x0 dx))
			  py1 (floor (+ y0 dy))
			  px2 (floor (+ x0 dx))
			  py2 (floor (- y0 dy))
			  )
		    ;; else
		    (setf px1 px3
			  py1 py3
			  px2 px4
			  py2 py4
			  )
		    )
		(setf px3 (floor (- x1 dx))
		      py3 (floor (+ y1 dy))
		      px4 (floor (+ x1 dx))
		      py4 (floor (- y1 dy))
		      )
		(tv:sheet-draw-triangle
		  px1 py1 px2 py2 px4 py4 alu self xmin ymin xmax ymax)
		(tv:sheet-draw-triangle
		  px1 py1 px3 py3 px4 py4 alu self xmin ymin xmax ymax)
		)
	      )
	    )
	)
      )
    )
  )



