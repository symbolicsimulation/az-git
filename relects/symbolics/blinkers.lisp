;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: My-TV -*-
;;;
;;; Copyright 1986. John Alan McDonald. All Rights Reserved. 
;;;
;;;======================================================================
;;;			Blinkers
;;;========================================================================
;;;
;;; The intention here is to get blinkers that don't use inside coordinates.
;;; They will only work with windows that include mtv:Graphics-Mixin 
;;;

;;; flavor names to export:

(export '(Rectangular-Blinker
	   Character-Blinker
	   Ibeam-Blinker
	   Box-Blinker
	   Hollow-Rectangular-Blinker
	   Bitblt-Blinker
	   Reverse-Character-Blinker
	   )
	)

;;;========================================================================

(defflavor Blinker-Mixin
	   ()
	   ()
  (:required-flavors tv:blinker)
  )

;;;------------------------------------------------------------------------

(defmethod (Blinker-Mixin :set-cursorpos) (x y &aux (old-phase phase))
  
  ;; Set the position of a blinker relative to the sheet it is on. Args
  ;; in terms of raster units.  If blinker was following cursor, it will
  ;; no longer be doing so. 
  
  (tv:with-blinker-ready t
    (multiple-value-bind
      (xmin xmax ymin ymax) (send tv:sheet :clipping-region-bounds)
      (setf x (min (max (truncate x) xmin) xmax))
      (setf y (min (max (truncate y) ymin) ymax))
      )
    (cond ((null tv:visibility)
	   ;; don't open if visibility nil (especially the mouse cursor!)
	   (setf tv:x-pos x tv:y-pos y tv:follow-p nil))
	  ((or (neq x tv:x-pos)	;;only blink if actually moving blinker
	       (neq y tv:y-pos)
	       )
	   (tv:open-blinker self)
	   (setf tv:x-pos x tv:y-pos y tv:follow-p nil)
	   ;; if this is the mouse blinker, and it is not being tracked
	   ;; by microcode, then it is important to turn it back on
	   ;; immediately.
	   (and (neq tv:visibility ':blink)
		old-phase
		(tv:blink self)
		)
	   )
	  )
    )
  )

;;;------------------------------------------------------------------------

(defmethod (Blinker-Mixin :read-cursorpos) ()
 
  (values (or tv:x-pos (tv:sheet-cursor-x tv:sheet))
	  (or tv:y-pos (tv:sheet-cursor-y tv:sheet))
	  )
  )


;;;------------------------------------------------------------------------

(defflavor Blinker
	   
	   ()
	   (Blinker-Mixin tv:Blinker)
  )


;;;------------------------------------------------------------------------

(defflavor Rectangular-Blinker-Mixin
	   
	   ()
	   (Blinker-Mixin)

  (:required-flavors tv:Rectangular-Blinker)
  )

;;;------------------------------------------------------------------------

(defmethod (Rectangular-Blinker-Mixin :set-size-and-cursorpos)
	   
	   (nwidth nheight x y)
  
  ;; This is like :set-size and :set-cursorpos together, in order to
  ;; prevent the user from seeing the intermediate state.  This prevents
  ;; occasional spasticity in menu Blinkers, which looks terrible. 
  
  (tv:with-blinker-ready t
    (multiple-value-bind
      (xmin xmax ymin ymax) (send tv:sheet :clipping-region-bounds)
      (setf x (min (max (truncate x) xmin) xmax))
      (setf y (min (max (truncate y) ymin) ymax))
      )
    (cond ((null tv:visibility)
	   ;;don't open if visibility nil (especially the mouse cursor!)
	   (setq tv:x-pos x
		 tv:y-pos y
		 tv:follow-p nil
		 tv:width nwidth
		 tv:height nheight
		 )
	   )
	  ((or (neq x tv:x-pos)
	       ;;only blink if actually moving blinker
	       (neq y tv:y-pos)
	       (neq tv:width nwidth)
	       (neq tv:height nheight)
	       )
	   (tv:open-blinker self)
	   (setq tv:x-pos x
		 tv:y-pos y
		 tv:follow-p nil
		 tv:width nwidth
		 tv:height nheight
		 )
	   )
	  )
    )
  )

;;;------------------------------------------------------------------------

(defflavor Rectangular-Blinker
	   
	   ()
	   
	   (Rectangular-Blinker-Mixin
	     tv:Rectangular-Blinker
	     )
  )

;;;------------------------------------------------------------------------

(defflavor Hollow-Rectangular-Blinker
	   
	   ()
	   
	   (Rectangular-Blinker-Mixin
	     tv:Hollow-Rectangular-Blinker
	     )
  )

;;;------------------------------------------------------------------------

(defflavor Box-Blinker
	   
	   ()
	   
	   (Rectangular-Blinker-Mixin
	     tv:Box-Blinker 
	     )
  )

;;;------------------------------------------------------------------------

(defflavor Box-Lower-Right-Blinker-Mixin 
	   
	   ()
	   
	   (Rectangular-Blinker-Mixin
	     tv:Box-Lower-Right-Blinker-Mixin
	     ) 
  )

;;;------------------------------------------------------------------------

(defwrapper (Box-Lower-Right-Blinker-Mixin :set-cursorpos) ((x y) . body)

  `(let ((old-x-pos tv:x-pos)
	 (old-y-pos tv:y-pos))
     (unless (and (eq x old-x-pos)
		  (eq y old-y-pos))
       (tv:with-blinker-ready nil
	 (progn . ,body)
	 (incf tv:width (- tv:x-pos old-x-pos))
	 (incf tv:height (- tv:y-pos old-y-pos))
	 )
       )
     )
  )

;;;------------------------------------------------------------------------

(defflavor Stay-Inside-Blinker-Mixin
	   ()
	   (tv:Stay-Inside-Blinker-Mixin)
  (:included-flavors Blinker)
  )

;;;------------------------------------------------------------------------

(defwrapper (Stay-Inside-Blinker-Mixin :set-cursorpos) (xy . body)
  
  `(progn
     (multiple-value-bind
       (xmin xmax ymin ymax) (send tv:sheet :clipping-region-bounds)
       (ignore xmin ymin)
       (setf (first xy)  (min (first xy) (- xmax tv:width)))
       (setf (second xy) (min (second xy) (- ymax tv:height)))
       )
     . ,body
     )
  )

;;;------------------------------------------------------------------------

(defflavor Ibeam-Blinker
	   
	   ()
	   
	   (Blinker-Mixin
	     tv:Ibeam-Blinker
	     )
  )

;;;------------------------------------------------------------------------

(defflavor Character-Blinker
	()
	(Blinker-Mixin
	  tv:Character-Blinker
	  )
  )

;;;------------------------------------------------------------------------

(defmethod (Character-Blinker :blink) ()

  "use a character as a blinker.  any font, any character"

  (let ((wide-p (tv:font-wide-chars-p tv:font)))
    (tv:draw-single-char
      wide-p tv:font tv:char tv:x-pos tv:y-pos tv:alu-xor tv:sheet
      )
    )
  )

 

;;;------------------------------------------------------------------------

(defflavor Bitblt-Blinker
	   ()
	   (Blinker-Mixin tv:Bitblt-Blinker)
  )

;;;------------------------------------------------------------------------

(defmethod (Bitblt-Blinker :blink) ()

  (let* ((x (+ tv:delta-x tv:x-pos))
	 (y (+ tv:delta-y tv:y-pos))
	 (w (min (- (tv:sheet-width tv:sheet) x) tv:width))
	 (h (min (- (tv:sheet-height tv:sheet) y) tv:height))
	 )
    (and (plusp w) (plusp h)
	 (bitblt tv:alu-xor
		 w h
		 tv:array
		 0 0
		 (tv:sheet-screen-array tv:sheet)
		 x y
		 )
	 )
    )
  )

;;;------------------------------------------------------------------------

(defflavor Reverse-Character-Blinker
	()
	(Bitblt-Blinker
	  tv:Reverse-Character-Blinker
	  )
  )

;;;------------------------------------------------------------------------

(Compile-flavor-methods Rectangular-Blinker
			Character-Blinker
			Ibeam-Blinker
			Box-Blinker
			Hollow-Rectangular-Blinker
			Bitblt-Blinker
			Reverse-Character-Blinker
			)
