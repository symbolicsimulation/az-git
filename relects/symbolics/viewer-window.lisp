;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Antelope -*-
;;;
;;; Copyright 1986. John Alan McDonald. All Rights Reserved. 
;;;

(defclass View-Filter-2d
  
  (:instance-variables
   (x-message :type keyword)
   (y-message :type keyword)
   (affine :type (array float (3 3)))
   )
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :after :init) (init-plist)
  
  ;; start with the identity transformation
  (unless (get init-plist :affine)
    (setf affine (make-array '(3 3) :initial-element 0.0))
    (-> self :set-to-identity)
    )
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :print-affine) ()
  
  (loop for i from 0 to 2 do
	(format t "~% ~s ~s ~s"
		(aref affine i 0) 
		(aref affine i 1) 
		(aref affine i 2)
		)
	)
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :set-to-identity) ()
  
  (loop for i from 0 to 2
	do
	(loop for j from 0 to 2 
	      when (eql i j) do (setf (aref affine i i) 1.0)
	      else           do (setf (aref affine i j) 0.0)
	      )
	)
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :apply-to) (item1 &optional item2)
  
  ;; The viewing transformation is an 2d affine transformation
  ;; represented as a 3x3 matrix in floating homogeneous coords. This
  ;; method returns 2 floating values which are supposed to be the x and
  ;; y in window coordinates (pixel units). These floating point
  ;; coordinates should be rounded before drawing. 
  
  (let (x y)
    (typecase item1
      (number
	(setf x item1) (setf y item2))
      ((or list lisp:Vector)
       (setf x (elt item1 0)) (setf y (elt item1 1)) )
      (instance
	(setf x (-> item1 x-message))
	(setf y (-> item1 y-message))
	)
      )
    (values (truncate (+ (* x (aref affine 0 0))
			 (* y (aref affine 0 1))
			 (aref affine 0 2)
			 )
		      )
	    (truncate (+ (* x (aref affine 1 0))
			 (* y (aref affine 1 1))
			 (aref affine 1 2)
			 )
		      )
	    )
    )
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :pre-multiply-by) (vf)
  
  (declare (type View-Filter-2d vf))
  
  (loop
    with new-af = (make-array '(3 3) :initial-element 0.0)
    with af = (-> vf :affine)
    for i from 0 to 2
    do
    (loop
      for j from 0 to 2
      for new-val = (+ (* (aref af i 0) (aref affine 0 j))
		       (* (aref af i 1) (aref affine 1 j))
		       (* (aref af i 2) (aref affine 2 j))
		       )
      do
      (setf (aref new-af i j) (coerce new-val 'single-float))
      )
    finally (setf affine new-af)
    ) 
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :translate-by) (dx dy)
  
  (incf (aref affine 0 2) dx)
  (incf (aref affine 1 2) dy)
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :rotate-by) (theta)
  
  (let* ((c (cos theta))
	(s (sin theta))
	(-s (- s))
	(a00  (aref affine 0 0))
	(a01  (aref affine 0 1))
	(a10  (aref affine 1 0))
	(a11  (aref affine 1 1))
	)
    (setf (aref affine 0 0) (+ (* c a00) (* -s a10)))
    (setf (aref affine 0 1) (+ (* c a01) (* -s a11)))
    (setf (aref affine 1 0) (+ (* s a00) (*  c a10)))
    (setf (aref affine 1 1) (+ (* s a01) (*  c a11)))
    )
  )

;;;----------------------------------------------------------------------

(defmethod (View-Filter-2d :scale-by) (sx &optional sy)
  
  (if (null sy) (setf sy sx))
  (setf (aref affine 0 0) (* sx (aref affine 0 0)))
  (setf (aref affine 0 1) (* sx (aref affine 0 1)))
  (setf (aref affine 0 2) (* sx (aref affine 0 2)))
  (setf (aref affine 1 0) (* sy (aref affine 1 0)))
  (setf (aref affine 1 1) (* sy (aref affine 1 1)))
  (setf (aref affine 1 2) (* sy (aref affine 1 2)))
  )

;;;----------------------------------------------------------------------
;;;
;;; This leaves a 10 pixel space around the inside of the clipping region.
;;; It's mostly a hack to keep centered characters and point-icons from
;;; running into the edges; it also improves the appearance a little.
;;; 

(defmethod (View-Filter-2d :map-bounding-box-to-clipping-region) (bb cr)
  
  (declare (type Region bb)
	   (type Region cr)
	   )
  
  (-> self :set-to-identity)
  (multiple-value-bind (xmin-bb xmax-bb ymin-bb ymax-bb) (send bb :bounds)
    (multiple-value-bind (xmin-cr xmax-cr ymin-cr ymax-cr) (send cr :bounds)
      (-> self :translate-by (- xmin-bb) (- ymin-bb))
      (-> self :scale-by
	  (/ (float (- xmax-cr xmin-cr)) (- xmax-bb xmin-bb))
	  (/ (float (- ymin-cr ymax-cr)) (- ymax-bb ymin-bb))
	  ;; y is inverted on the screen
	  )
      (-> self :translate-by xmin-cr ymax-cr)
      )
    )
  )

;;;----------------------------------------------------------------------

(Compile-and-recompile-flavor View-Filter-2d)


;;;======================================================================

(defclass Icon
  
  (:instance-variables
   
   (window :type Viewer)
   ;; Which window is it in?
   
   (parent :type (or Icon Viewer))
   ;; ...In the implicit tree of display objects.
   ;; The root of the tree is the window itself.
   
   (bounding-box :type Bounding-Box)
   ;; ...In local coordinates.  The <bounding-box>
   ;; is supposed to describe a (minimal) subset of
   ;; the local coordinate space that contains all
   ;; the components of this Icon. 
   
   (delta-vt :type View-Filter)
   ;; ...Takes the local coordinate space to the local coordinate space
   ;; of the <parent>.  A value of <nil> is equivalent to an identity
   ;; transformation. 
   
   (vt :type View-Filter)
   ;; ...Takes the local coordinate space to window coordinates.  <vt>
   ;; is the result of composing the <delta-vt>'s between this Icon and
   ;; the root of the display tree (the <window>). 
   
   (stands-for :type Any)
   ;; Some icons are graphical representations of objects in the environment.
   ;; This is supposed to be somewhat analogous to printed representations.

   )

  (:class-variables
   (display-components :default '())
   )

  (:component-flavors mtv:Region)
  
  (:required-init-keywords
   :window
   )
  )

;;;----------------------------------------------------------------------

(eval-when (compile load eval)
  (define-loop-path
    display-components
    class-variable-list-path
    (of in si:of si:in)
    )
  )

;;;----------------------------------------------------------------------

(defmethod (Icon :who-line-documentation) ()
  
  (if (null stands-for)
      (-> self :string-for-printing)
      ;; else
      (-> stands-for :who-line-documentation)
      )
  )

;;;----------------------------------------------------------------------
;;; In case an Icon stands-for a Symbol...
;;;

(defmethod (Symbol :who-line-documentation) () self)

(defmethod (Symbol :name) () self)

;;;----------------------------------------------------------------------

(defmethod (Icon :mouse-moves) (x y)
  
  (if (-> self :contains-point? x y)
      (loop for icon being the display-components of self
	    for choice = (-> (the Icon icon) :mouse-moves x y)
	    while (null choice)
	    finally (return (if (null choice) self choice))
	    )
      ;; else 
      nil
      )
  )

;;;----------------------------------------------------------------------

(defmethod (Icon :internal-layout) ()

  (loop for it being the display-components of self
	do (-> it :internal-layout)
	)
  )

;;;----------------------------------------------------------------------

(defmethod (Icon :screen-layout) ()

  (loop for it being the display-components of self
	do (-> it :screen-layout)
	)
  )

;;;----------------------------------------------------------------------

(defmethod (Icon :draw) ()
  
  (loop for it being the display-components of self
	do (-> it :draw)
	) 
  )

;;;----------------------------------------------------------------------

(defmethod (Icon :kill) ()
  
  (loop for it being the display-components of self
	do (-> it :kill)
	) 
  )


;;;========================================================================

(defclass Border

  :new-definition

  (:instance-variables
   (thickness :default 4 :type (integer 0 *))
   )
  
  (:component-classes Icon)
  )

;;;------------------------------------------------------------------------

(defmethod (Border :after :init) (init-plist)
  
  (ignore init-plist)
  
  )

;;;------------------------------------------------------------------------

(defmethod (Border :interior-bounds) ()
  
  (values (+ mtv:xmin thickness)
	  (- mtv:xmax thickness)
	  (+ mtv:ymin thickness)
	  (- mtv:ymax thickness)
	  )
  )

;;;------------------------------------------------------------------------

(defmethod (Border :screen-layout) ()
  
  (setf mtv:xmin 0)
  (setf mtv:xmax (- (-> window :width)  1)) ;; zero based arrays!
  (setf mtv:ymin 0)
  (setf mtv:ymax (- (-> window :height) 1)) ;; zero based arrays!
  
  (-> self :set-bounds mtv:xmin mtv:xmax mtv:ymin mtv:ymax)
  )

;;;------------------------------------------------------------------------
;;;
;;; not selectable with the mouse!
;;;

(defmethod (Border :mouse-moves) (x y) (ignore x y) nil)  

;;;------------------------------------------------------------------------

(defmethod (Border :draw) ()
  
  (mtv:with-clipping-region (window self)
    (let* ((xmin+1   (+ mtv:xmin 1))
	   (xmax-1   (- mtv:xmax 1))
	   (ymin+1   (+ mtv:ymin 1))
	   (ymax-1   (- mtv:ymax 1))
	   )
      
      ;; outer box
      (-> window :draw-line mtv:xmin mtv:ymin mtv:xmin mtv:ymax)
      (-> window :draw-line mtv:xmax mtv:ymin mtv:xmax mtv:ymax)
      (-> window :draw-line mtv:xmin mtv:ymin mtv:xmax mtv:ymin)
      (-> window :draw-line mtv:xmin mtv:ymax mtv:xmax mtv:ymax)
      
      (-> window :draw-line xmin+1   mtv:ymin xmin+1   mtv:ymax)
      (-> window :draw-line xmax-1   mtv:ymin xmax-1   mtv:ymax)
      (-> window :draw-line mtv:xmin ymin+1   mtv:xmax ymin+1)
      (-> window :draw-line mtv:xmin ymax-1   mtv:xmax ymax-1)
      )
    ) 
  )


;;;========================================================================

(defclass Title

  (:instance-variables
   (title-string :default "A Title"    :type String)
   (title-xmin :default 0 :type (integer 0 *))
   (title-xmax :default 0 :type (integer 0 *))
   (title-ymin :default 0 :type (integer 0 *))
   (title-ymax :default 0 :type (integer 0 *))
   (title-font   :default *title-font* :type Font)
   (title-alu    :default (aref mtv:*s-alus* mtv:*green-slot*))
   )
  
  (:component-classes Icon)
  )

;;;------------------------------------------------------------------------

(defmethod (Title :internal-layout) ()
  
  (unless (null stands-for)
    (setf title-string (zl:string (-> stands-for :string-for-printing)))
    )
  )

;;;------------------------------------------------------------------------

(defmethod (Title :screen-size) ()
  
  (values
    (-> window :string-length title-string 0 nil nil title-font)
    (tv:font-char-height title-font)
    )
  )

;;;------------------------------------------------------------------------

(defmethod (Title :screen-layout) ()

  #||
  (multiple-value-bind
    (dx dy) (-> self :screen-size)
    (multiple-value-setq
      (title-xmin title-xmax) (center-interval-on 1 dx mtv:xmin mtv:xmax)
      )
    (multiple-value-setq
      (title-ymin title-ymax) (center-interval-on 1 dy mtv:ymin mtv:ymax)
      )
    )
  ||#
  )

;;;------------------------------------------------------------------------

(defmethod (Title :draw) ()

  (mtv:with-clipping-region (window self)
    (-> window :draw-string-horizontal-centered
	title-string
	mtv:xmin mtv:ymin
	mtv:xmax mtv:ymax
	nil
	title-font
	(aref mtv:*s-alus* mtv:*green-slot*)
	)
    )
  )


;;;======================================================================
;;;
;;; {class Viewer} is to be mixed in to all windows in the Antelope system.
;;;

(defclass Viewer

  :new-definition
  
  (:instance-variables
   (border          :type Border)
   (title           :type Title)
   (icon-blinker    :type Blinker)
   (blinker-type    :type Symbol :default 'mtv:box-blinker)
   (blinker-options :type List :default '(:visibility nil))
   (selected-icon   :type Icon)
   (who-line-documentation-string :type String)
   )
  
  (:class-variables
   (display-components :default '(:border :title))
   ;; This is a list of names of instance variables that are considered to be
   ;; the first level of descendants of the window in its display tree.
   )
  
  (:component-flavors
   mtv:Graphics-Mixin
   tv:Minimum-Window
   tv:Select-Mixin
   tv:Stream-Mixin
   tv:Process-Mixin
   )
  
  (:default-init-plist
   :process   '(top-level-loop)
   :save-bits t
   :expose-p  t
   :edges-from :mouse
   :blinker-p nil
   :deexposed-typeout-action :expose
   )
  )

;;;======================================================================

(defmethod (Viewer-Class :make-instance) (&rest init-plist)

  (apply #'tv:make-window named-object:name init-plist)
  )

;;;----------------------------------------------------------------------

(defmethod (Viewer :after :init) (init-plist)
  
  (ignore init-plist)
  
  (setf border (make-instance 'Border
			      :window self
			      :parent self
			      )
	)
  
  (setf title (make-instance 'Title
			     :window self
			     :parent self
			     :stands-for self
			     )
	)
  
  (if (null icon-blinker)
      (setf icon-blinker
	    (apply #'tv:make-blinker self blinker-type blinker-options)
	    )
      )
  )

;;;----------------------------------------------------------------------

(defmethod (Viewer :top-level-loop) ()
  
  (error-restart-loop
    ((zl:error sys:abort)
     (zl:string (string-append named-object:name " top level"))
     )
    (let ((input (-> self :any-tyi)))
      (typecase input
	(integer (hacks:play-string (zl:string input)))
	(list
	  (if (instancep (second input))
	      (hacks:play-string
		(zl:string (-> (-> (second input) :stands-for) :name)))
	      )
	  )
	)
      )
    )
  )


;;;----------------------------------------------------------------------
;;;
;;;		     Graphics, via the Display Tree
;;;		     
;;;----------------------------------------------------------------------

(defmethod (Viewer :layout) ()
  
  (-> border :screen-layout)
  (-> title  :internal-layout)
  
  (let ((th (-> border :thickness))) (-> self :set-margins th th th th))
  
  (multiple-value-bind (dx dy) (-> title :screen-size)
    (multiple-value-bind (x0 x1 y0 nil) (-> mtv:clipping-region :bounds)
      (multiple-value-bind (xt0 xt1) (mtv:center-interval-on 1 (+ dx 4) x0 x1)
	(let ((yt0 (+ y0 2)) (yt1 (+ y0 dy 4)))
	  (-> title :set-bounds xt0 xt1 yt0 yt1)
	  )
	)
      )
    )
  
  (-> title :screen-layout)
  )

;;;----------------------------------------------------------------------

(defmethod (Viewer :draw) ()
  
  (loop for icon being the display-components of self
	do (-> icon :draw)
	) 
  )


;;;-----------------------------------------------------------------
;;;
;;;			   Screen Management
;;;			   
;;;-----------------------------------------------------------------
;;;
;;; Returning <t> will cause the contents of a partially exposed window
;;; to be shown, without having to have bit-save arrays for every
;;; window---saves paging, which is important on the 3640 with 2 mbytes.
;;;

;;;(defmethod (Viewer :screen-manage-deexposed-visibility) () t)

;;;----------------------------------------------------------------------
;;;
;;; This is supposed to free as much space as possible for garbage collection;
;;; it might someday explicitly return things to resources.
;;;

(defmethod (Viewer :after :kill) ()

  (loop for icon being the display-components of self
	do (-> icon :kill)
	)
  )

;;; this takes care of when some comoponents are null

(defmethod (Null :kill) ())

;;;----------------------------------------------------------------------

(defmethod (Viewer :after :refresh) (&optional refresh-type)

  ;;(-> self :home-cursor)
  ;;(-> self :insert-line)
  ;;(-> self :string-out
  ;;    (zl:string (format nil "~s" refresh-type)))
  ;;(-> self :insert-line)
  
  (case refresh-type
   (:use-old-bits
     nil
     )
   (:size-changed
     (-> self :clear-window)
     (-> self :layout)
     (-> self :draw)
     )
   (:complete-redisplay
     (-> self :clear-window)
     (-> self :layout)
     (-> self :draw)
     )
   (nil
     (-> self :clear-window)
     (-> self :layout)
     (-> self :draw)
     )
    )
  )

;;;----------------------------------------------------------------------

(defmethod (Viewer :name-for-selection) ()

  (zl:string (-> self :string-for-printing))
  )


;;;=================================================================
;;;
;;;			      Mouse Stuff
;;;
;;;=================================================================

(defmethod (Viewer :after :set-selected-icon) (icon)

  (-> self :move-blinker icon)
  )

;;;-----------------------------------------------------------------

(defmethod (Viewer :move-blinker)  (icon)
  
  (unless (null icon-blinker)
    (if (null icon)
	(progn
	  (-> icon-blinker :set-visibility nil)
	  (-> self :set-who-line-documentation-string nil)
	  )
	;; else
	(let ((b-left   (- (round (-> icon :xmin)) 2))
	      (b-top    (- (round (-> icon :ymin)) 2))
	      (b-width  (+ (round (-> icon :xdiameter)) 4))
	      (b-height (+ (round (-> icon :ydiameter)) 4))
	      )
	  (-> icon-blinker :set-size-and-cursorpos
	      b-width b-height b-left b-top
	      )
	  (-> icon-blinker :set-visibility t)
	  )
	(-> self :set-who-line-documentation-string
	    (zl:string (-> icon :who-line-documentation))
	    )
	)
    )
  )

;;;-----------------------------------------------------------------
;;;
;;; When the mouse leaves the window, turn off the icon blinker.
;;; 
  
(defmethod (Viewer :handle-mouse) ()

  (-> self :mouse-standard-blinker) 
  (tv:mouse-default-handler self nil)
  (-> self :set-selected-icon nil)
  )

;;;-----------------------------------------------------------------

(defmethod (Viewer :mouse-click) (button x y)
  
  (if (null selected-icon)
      (-> self :force-kbd-input
	  `(:mouse-click ,button ,x ,y)
	  )
      ;; else
      (-> self :force-kbd-input
	  `(:icon ,selected-icon ,button ,x ,y)
	  )
      )
  )

;;;-----------------------------------------------------------------

(defmethod (Viewer :mouse-moves) (x y)
  
  ;;  This message gets sent by the window system whenever the mouse is moved. 
  
  (tv:mouse-set-blinker-cursorpos) ;; Move mouse blinker
  
  (let ((new-icon (loop for icon being the display-components of self
			for selected = (-> icon :mouse-moves x y)
			until (not (null selected))
			finally (return selected)
			)
		  )
	)
    (if (not (eq new-icon selected-icon))
	(-> self :set-selected-icon new-icon)	
	)
    )
  )

