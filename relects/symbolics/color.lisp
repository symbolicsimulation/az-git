;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: My-TV -*-
;;;
;;; Copyright 1986. John Alan McDonald. All Rights Reserved. 
;;;
;;;========================================================================

(export '(*color-screen*

	   alu-0 alu-and alu-andcs alu-d alu-andcd alu-s alu-xor alu-ior   
	   alu-cior alu-cxor alu-cs alu-iorcs alu-cd alu-iorcd alu-cand alu-1

	   *ior-alus* *andcs-alus* *xor-alus* *s-alus* *and-alus* *cd-alus*
	   *cs-alus*

	   *background-slot* *foreground-slot* *black-slot* *gray-slot*
	   *white-slot* *red-slot* *yellow-slot* *green-slot* *cyan-slot*
	   *blue-slot* *magenta-slot* 
	   
	   *background-s* *foreground-s* *black-s* *gray-s* *white-s* *red-s*
	   *yellow-s* *green-s* *cyan-s* *blue-s* *magenta-s*

	   *black-xor* *gray-xor* *white-xor* *red-xor* *yellow-xor*
	   *green-xor* *cyan-xor* *blue-xor* *magenta-xor*
	   )
	)

;;;========================================================================

(eval-when (compile load eval)
  
  (defun color? ()
    
    (and (find-package 'color)  ;; color software
	 (color:color-exists-p) ;; color hardware
	 (not (eql (type-of color:color-screen) 'tv:main-screen))
	 ;; can't use <typep> here because it doesn't work 
	 ;; with flavors
	 )
    )
  )

(eval-when (load eval)
  
  (if (color?)
      (defparameter *color-screen*
		   (if (or (not (boundp 'color:color-scree))
			   (null color:color-screen)
			   )
		       (color:make-color-screen)
		       ;; else use the color-screen that exists
		       color:color-screen
		       )
	)
      ;; else
      (defparameter *color-screen* tv:main-screen)
      )
  )
    
;;;========================================================================
;;;
;;; make it easier to use ihs color coords.
;;;

(eval-when (compile load eval)
  (when (get 'color:managed-map-mixn 'si:flavor)
    (defmethod (color:managed-map-mixin :compute-ihs-data) (i h s)
      (multiple-value-bind (r g b) (color:hexcone-ihs->rgb i  h s)
	(send self :compute-rgb-data r g b)
	)
      )
    )
  )

;;;------------------------------------------------------------------------
;;;
;;; color things should work on b&w screen as well
;;;

(defmethod (tv:main-screen :compute-ihs-data) (i &rest ignore)
  (if (= 0 i)
      0
      ;; else
      1
      )
  )

(defmethod (tv:main-screen :compute-rgb-data) (r g b)
  (if (and (= 0 r) (= 0 g) (= 0 b))
      0
      ;; else
      1
      )
  )

;;;========================================================================
;;;
;;; More alus:
;;;
;;; This is intended to be an exhaustive listing of all 16 binary truth
;;; tables. The system provides a subset of these as <tv:alu-xor>,
;;; <tv:alu-andca>, etc., but it's sometimes useful to have the ones
;;; that are missing.  The value of the alu variables is an integer
;;; which corresponds to an indexing of the 16 truth tables. I believe
;;; the indexing works in the following way:
;;;
;;; Let S be the source (as in a bitblt), B the destination,
;;; and R the result.
;;; Then number the entries in the truth table according to 2*~D + ~S
;;; (~ means complement).
;;;
;;;For example, exclusive-or:
;;; 
;;; D 0 0 1 1
;;; S 0 1 0 1
;;; ---------  
;;; R 0 1 1 0 = 6
;;;
;;;  and-with-complement-of-source:
;;; 
;;; D 0 0 1 1
;;; S 0 1 0 1
;;; ---------  
;;; R 0 0 1 0 = 2
;;;

;;; in the comments below <nil> corresponds to 0 and <t> to 1.
;;; 

(defparameter alu-0       0) ;; nil
(defparameter alu-and     1) ;; (and dest source)
(defparameter alu-andcs   2) ;; (and dest (not source))
(defparameter alu-d       3) ;; dest
(defparameter alu-andcd   4) ;; (and (not dest) source)
(defparameter alu-s       5) ;; source
(defparameter alu-xor     6) ;; (and (or dest source) (not (and dest source)))
(defparameter alu-ior     7) ;; (or dest source)
(defparameter alu-cior    8) ;; (not (or dest source))
(defparameter alu-cxor    9) ;; (or (not (or dest source)) (and dest source))
(defparameter alu-cs     10) ;; (not source)
(defparameter alu-iorcs  11) ;; (or dest (not source))
(defparameter alu-cd     12) ;; (not dest)
(defparameter alu-iorcd  13) ;; (or (not dest) source)
(defparameter alu-cand   14) ;; (not (and dest source))
(defparameter alu-1      15) ;; t


;;;========================================================================
;;;
;;; alu arrays for color screen.
;;;
;;; To draw in a given color, one would first compute the slot, then
;;; select the alu from the appropriate array. For example, to draw an X
;;; in i h s coords:
;;;
;;;  (let ((slot (send color:color-screen :compute-ihs-data i h s)))
;;;    (send window :draw-char fonts:exp8-icon #/a 10 10 (aref *s-alus* data))
;;;    )
;;;    

(defparameter *ior-alus* (make-array 256))

(eval-when (load eval)
  (if (color?)
      (loop
	for slot from 0 to 255
	do
	(setf (aref *ior-alus* slot)
	      (make-instance 'color:general-sc-color-alu :fill-data slot)
	      )
	)
      ;; else
      (loop
	initially (setf (aref *ior-alus* 0) alu-0)
	for slot from 1 to 255
	do (setf (aref *ior-alus* slot) alu-ior)
	)
      )
  )

;;;---------------------------------------------

(defparameter *andcs-alus* (make-array 256))

(eval-when (load eval)
  (if (color?) 
      (loop
	for slot from 0 to 255
	do
	(setf (aref *andcs-alus* slot)
	      (make-instance 'color:general-sc-color-alu
			     :fill-data slot
			     :alu alu-andcs
			     )
	      )
	)
      ;; else
      (loop
	initially (setf (aref *andcs-alus* 0) alu-0)
	for slot from 1 to 255
	do (setf (aref *andcs-alus* slot) alu-andcs)
	)
      )
  )

;;;---------------------------------------------

(defparameter *xor-alus* (make-array 256))

(eval-when (load eval)
  (if (color?) 
      (loop
	for slot from 0 to 255
	do
	(setf (aref *xor-alus* slot)
	      (make-instance 'color:general-sc-color-alu
			     :fill-data slot
			     :alu alu-xor
			     )
	      )
	)
      ;; else
      (loop
	initially (setf (aref *xor-alus* 0) alu-0)
	for slot from 1 to 255
	do (setf (aref *xor-alus* slot) alu-xor)
	)
      )
  )

;;;---------------------------------------------

(defparameter *s-alus* (make-array 256))

(eval-when (load eval)
  (if (color?) 
      (loop
	for slot from 0 to 255
	do (setf (aref *s-alus* slot)
		 (make-instance 'color:general-sc-color-alu
				:fill-data slot
				:alu alu-s
				)
		 )
	)
      ;; else
      (loop
	initially (setf (aref *s-alus* 0) alu-0)
	for slot from 1 to 255
	do (setf (aref *s-alus* slot) alu-s)
	)
      )
  )

;;;---------------------------------------------

(defparameter *and-alus* (make-array 256))

(eval-when (load eval)
  (if (color?) 
      (loop
	for slot from 0 to 255
	do (setf (aref *and-alus* slot)
		 (make-instance 'color:general-sc-color-alu
				:fill-data slot
				:alu alu-and
				)
		 )
	)
      ;; else
      (loop
	initially (setf (aref *and-alus* 0) alu-0)
	for slot from 1 to 255
	do (setf (aref *and-alus* slot) alu-and)
	)
      )
  )

;;;---------------------------------------------

(defparameter *cd-alus* (make-array 256))

(eval-when (load eval)
  (if (color?)
      (loop
	for slot from 0 to 255
	do (setf (aref *cd-alus* slot)
		 (make-instance 'color:general-sc-color-alu
				:fill-data slot
				:alu alu-cd
				)
		 )
	)
      ;; else
      (loop
	initially (setf (aref *cd-alus* 0) alu-0)
	for slot from 1 to 255
	do (setf (aref *cd-alus* slot) alu-cd)
	)
      )
  )

;;;---------------------------------------------

(defparameter *cs-alus* (make-array 256))

(eval-when (load eval)
  (if (color?) 
      (loop
	for slot from 0 to 255
	do (setf (aref *cs-alus* slot)
		 (make-instance 'color:general-sc-color-alu
				:fill-data slot
				:alu alu-cs
				)
		 )
	)
      ;; else
      (loop
	initially (setf (aref *cs-alus* 0) alu-0)
	for slot from 1 to 255
	do (setf (aref *cs-alus* slot) alu-cs)
	)
      )
  )


;;;==============================================================

(eval-when (load eval)
  (defparameter *background-slot* 0) ;; background color value
  (defparameter *foreground-slot* (if (color?) 255 1))
  
  (defparameter *black-slot*
	       (send *color-screen* :compute-rgb-data 0.00 0.00 0.00))
  (defparameter *gray-slot*
	       (send *color-screen* :compute-rgb-data 0.35 0.35 0.35))
  (defparameter *white-slot*
	       (send *color-screen* :compute-rgb-data 1.00 1.00 1.00))
  (defparameter *red-slot*
	       (send *color-screen* :compute-rgb-data 1.00 0.00 0.00))
  (defparameter *yellow-slot*
	       (send *color-screen* :compute-rgb-data 1.00 1.00 0.00))
  (defparameter *green-slot*
	       (send *color-screen* :compute-rgb-data 0.00 1.00 0.00))
  (defparameter *cyan-slot*
	       (send *color-screen* :compute-rgb-data 0.00 1.00 1.00))
  (defparameter *blue-slot*
	       (send *color-screen* :compute-rgb-data 0.00 0.00 1.00))
  (defparameter *magenta-slot*
	       (send *color-screen* :compute-rgb-data 1.00 0.00 1.00))
  
  (defparameter *background-s* (aref *s-alus* *background-slot*))
  (defparameter *foreground-s* (aref *s-alus* *foreground-slot*))
  
  (defparameter *black-s*   (aref *s-alus* *black-slot*))
  (defparameter *gray-s*    (aref *s-alus* *gray-slot*))
  (defparameter *white-s*   (aref *s-alus* *white-slot*))
  (defparameter *red-s*     (aref *s-alus* *red-slot*))
  (defparameter *yellow-s*  (aref *s-alus* *yellow-slot*))
  (defparameter *green-s*   (aref *s-alus* *green-slot*))
  (defparameter *cyan-s*    (aref *s-alus* *cyan-slot*))
  (defparameter *blue-s*    (aref *s-alus* *blue-slot*))
  (defparameter *magenta-s* (aref *s-alus* *magenta-slot*))
  
  
  (defparameter *black-xor*   (aref *xor-alus* *black-slot*))
  (defparameter *gray-xor*    (aref *xor-alus* *gray-slot*))
  (defparameter *white-xor*   (aref *xor-alus* *white-slot*))
  (defparameter *red-xor*     (aref *xor-alus* *red-slot*))
  (defparameter *yellow-xor*  (aref *xor-alus* *yellow-slot*))
  (defparameter *green-xor*   (aref *xor-alus* *green-slot*))
  (defparameter *cyan-xor*    (aref *xor-alus* *cyan-slot*))
  (defparameter *blue-xor*    (aref *xor-alus* *blue-slot*))
  (defparameter *magenta-xor* (aref *xor-alus* *magenta-slot*))
  )