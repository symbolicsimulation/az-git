;;; -*- Mode: LISP; Syntax: Common-lisp; Package: User; -*-


(defparameter *starlisp-simulator-files*
	      '("goofy:>cm>starlisp>simulator>f11>zwei-hacks"
		"goofy:>cm>starlisp>simulator>f11>external-specification"
		"goofy:>cm>starlisp>simulator>f11>simulator-specification"
		"goofy:>cm>starlisp>simulator>f11>definitions"
		"goofy:>cm>starlisp>simulator>f11>pref-setf"
		"goofy:>cm>starlisp>simulator>f11>proclaim"
		"goofy:>cm>starlisp>simulator>f11>port"
		"goofy:>cm>starlisp>simulator>f11>macros"
		"goofy:>cm>starlisp>simulator>f11>genfunctions"
		"goofy:>cm>starlisp>simulator>f11>sim"
		"goofy:>cm>starlisp>simulator>f11>trivfunctions"
		"goofy:>cm>starlisp>simulator>f11>functions"
		"goofy:>cm>starlisp>simulator>f11>addressing"
		"goofy:>cm>starlisp>simulator>f11>local-environment"
		"goofy:>cm>starlisp>simulator>f11>environment"
		"goofy:>cm>starlisp>simulator>f11>hypergrid"
		"goofy:>cm>starlisp>simulator>f11>advanced-functions"
		"goofy:>cm>starlisp>simulator>f11>testforms"
		"goofy:>cm>starlisp>simulator>f11>tests")))

(mapc #'load *starlisp-simulator-files*)