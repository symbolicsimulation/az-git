;;;-*- Package:(CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;;============================================================
;;; from Numerical Recipes in C, p. 297

(defconstant *golden-ratio* (/ (+ 1.0 (sqrt 5.0)) 2.0))
(defconstant *golden-fraction-0* (/ (- 3.0 (sqrt 5.0)) 2.0))
(defconstant *golden-fraction-1* (/ (- (sqrt 5.0) 1.0) 2.0))
(defconstant *tiny* 1.0e-20)
(defconstant *sqrt-single-float-epsilon* (sqrt single-float-epsilon))

;;;------------------------------------------------------------

(defun golden-section-bracket-minimum (f a b)
  (declare (values a b c (f a) (f b) (f c)))
  (let ((fa (funcall f a))
	(fb (funcall f b)))
    ;; make sure we're going downhill 
    (when (> fb fa) (rotatef a b) (rotatef fa fb))
    (let* ((c (+ b (* *golden-ratio* (- b a))))
	   (fc (funcall f c)))
      (loop
	(when (> fc fb) (return)) ;; we've bracketed a minimum
	(let* ((u (+ c (* *golden-ratio* (- c b))))
	       (fu (funcall f u)))
	  (shiftf a b c u)
	  (shiftf fa fb fc fu)))
      (values a b c fa fb fc))))

;;;------------------------------------------------------------

(defun golden-section-minimize (f a b c 
				&key
				(fb (funcall f b))
				(tolerance *sqrt-single-float-epsilon*))
  (declare (values xmin (f xmin)))
  (let* ((x0 a) x1 x2 (x3 c)
	 f0 f1 f2 f3)
    ;; make x0 to x1 the smaller segment
    (cond
      ((> (abs (- c b)) (abs (- b a)))
       (setf x1 b)
       (setf x2 (+ b (* *golden-fraction-0* (- c b))))
       (setf f1 fb)
       (setf f2 (funcall f x2)))
      (t
       (setf x2 b)
       (setf x1 (- b (* *golden-fraction-0* (- b a))))
       (setf f1 (funcall f x1))
       (setf f2 fb)))
    (loop
      ;; (print (list x0 x1 x2 x3))
      (when (<= (abs (- x3 x0))
		(* tolerance (+ 1.0 (abs x1) (abs x2)))) (return))
      (cond
	((< f2 f1)
	 (shiftf x0 x1 x2 (+ (* *golden-fraction-1* x2)
			     (* *golden-fraction-0* x3)))
	 (shiftf f0 f1 f2 (funcall f x2)))
	(t
	 (shiftf x3 x2 x1 (+ (* *golden-fraction-1* x1)
			     (* *golden-fraction-0* x0)))
	 (shiftf f3 f2 f1 (funcall f x1)))))
    (if (< f1 f2)
	(values x1 f1)
	(values x2 f2))))

;;;------------------------------------------------------------

(defun minimize-1d (f a b &key (tolerance *sqrt-single-float-epsilon*))
  (multiple-value-bind (a b c fa fb fc) (bracket-minimum f a b)
    (declare (ignore fa fc))
    (golden-section-minimize f a b c :fb fb :tolerance tolerance)))


(defun square (x)
  (let ((x**2 (* x x)))
    (format *standard-output* "~& x, x**2= ~f, ~f" x x**2)
    x**2))