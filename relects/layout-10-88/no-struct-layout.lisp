;;;-*- Package: (CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;;------------------------------------------------------------------

(*defun *initialize-graph-processors ($node? $edge?)
  (declare (type boolean-pvar $node? $edge?))
  ;; initialize all the processors in the machine
  (*set $node? nil!!)
  (*set $edge? nil!!)
  (initialize-processor-table))

;;;------------------------------------------------------------------

(*defun *build-graph ( edges nodes dist-fn weight-fn initial-x initial-y
		      $node? $node-x $node-y
		     $edge? $edge-node0 $edge-node1
		     $edge-length $edge-weight)
  (declare (type boolean-pvar $node? $edge?)
	   (type float-pvar $node-x $node-y $edge-length $edge-weight)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1))

  (*initialize-graph-processors $node? $edge?)
  
  ;; assign processors to nodes
  (multiple-value-bind
    (nodes-start nodes-end) (allocate-processor-cube-addresses nodes)

    (*when (<=!! (the (field-pvar *log-number-of-processors-limit*)
		      (!! nodes-start))
		 (self-address!!)
		 (the (field-pvar *log-number-of-processors-limit*)
		      (!! (- nodes-end 1))))
      (*set $node? t!!))
    (array-to-pvar
      initial-x $node-x
      :cube-address-start nodes-start :cube-address-end nodes-end)
    (array-to-pvar
      initial-y $node-y
      :cube-address-start nodes-start :cube-address-end nodes-end)

    ;; assign processors to edges
    (dolist (edge edges)
      (let ((node0 (edge-node0 edge))
	    (node1 (edge-node1 edge))
	    (p (allocate-processor-cube-address edge)))
	(*setf (pref $edge? p) t)
	(*setf (pref $edge-node0 p) (processor-cube-address node0))
	(*setf (pref $edge-node1 p) (processor-cube-address node1))
	(let* ((l (funcall dist-fn node0 node1))
	       (w (funcall weight-fn node0 node1 l)))
	  (*setf (pref $edge-length p) l)
	  (*setf (pref $edge-weight p) w))))
  
    (values (cons nodes-start nodes-end))))

;;;------------------------------------------------------------------

(*defun *build-ordered-graph ( edges nodes dist-fn weight-fn order-fn
			      initial-x initial-y
			      $node? $node-x $node-y
			      $edge? $edge-node0 $edge-node1
			      $edge-length $edge-weight $edge-order)
  (declare (type boolean-pvar $node? $edge?)
	   (type float-pvar
		 $node-x $node-y $edge-length $edge-weight $edge-order)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1))
  (*initialize-graph-processors $node? $edge?)
  ;; assign processors to nodes
  (multiple-value-bind
    (nodes-start nodes-end) (allocate-processor-cube-addresses nodes)
    (*when (<=!! (the (field-pvar *log-number-of-processors-limit*)
		      (!! nodes-start))
		 (self-address!!)
		 (the (field-pvar *log-number-of-processors-limit*)
		      (!! (- nodes-end 1))))
      (*set $node? t!!))
    (array-to-pvar
      initial-x $node-x
      :cube-address-start nodes-start :cube-address-end nodes-end)
    (array-to-pvar
      initial-y $node-y
      :cube-address-start nodes-start :cube-address-end nodes-end)
    ;; assign processors to edges
    (dolist (edge edges)
      (let ((node0 (edge-node0 edge))
	    (node1 (edge-node1 edge))
	    (p (allocate-processor-cube-address edge)))
	(*setf (pref $edge? p) t)
	(*setf (pref $edge-node0 p) (processor-cube-address node0))
	(*setf (pref $edge-node1 p) (processor-cube-address node1))
	(let* ((l (funcall dist-fn node0 node1))
	       (o (funcall order-fn node0 node1))
	       (w (funcall weight-fn node0 node1 l)))
	  (*setf (pref $edge-length p) l)
	  (*setf (pref $edge-order p) o)
	  (*setf (pref $edge-weight p) w))))
    (values (cons nodes-start nodes-end))))

;;;------------------------------------------------------------------
#-lucid
(*defun *plot-gradient-step ( $node-x $node-y $node-grad-x $node-grad-y
			     nodes-start nodes-end
			     step-size 
			     nodes x-coords y-coords x-step y-step
			     &key
			     (canvas *canvas*)
			     (plot *plot*)
			     (plot-fn #'plot-xy))
  
  (declare (type float-pvar $node-x $node-y $node-grad-x $node-grad-y))

  (pvar-to-array
    $node-x x-coords
    :cube-address-start nodes-start :cube-address-end nodes-end)
  (pvar-to-array
    $node-y y-coords 
    :cube-address-start nodes-start :cube-address-end nodes-end)

  (*let (($step-size (the float-pvar (!! step-size))))
    (declare (type float-pvar $step-size))
    (pvar-to-array
      (*!! $step-size $node-grad-x) x-step
      :cube-address-start nodes-start :cube-address-end nodes-end)
    (pvar-to-array
      (*!! $step-size $node-grad-y) y-step
      :cube-address-start nodes-start :cube-address-end nodes-end))

  (funcall plot-fn plot canvas nodes x-coords y-coords x-step y-step))

;;;------------------------------------------------------------------

(*defun *take-gradient-step (step-size
			      $node? $node-x $node-y $node-grad-x $node-grad-y)

  (declare (type boolean-pvar $node?)
	   (type float-pvar
		 $node-x $node-y $node-grad-x $node-grad-y
		 $new-node-x $new-node-y))

  (*when $node?
    (*let (($step-size (the float-pvar (!! step-size))))
      (declare (type float-pvar $step-size))
      (*set $node-x (+!! $node-x (*!! $step-size $node-grad-x)))
      (*set $node-y (+!! $node-y (*!! $step-size $node-grad-y))))))

(*defun *try-gradient-step (step-size
			      $node? $node-x $node-y $node-grad-x $node-grad-y
			      $new-node-x $new-node-y)

  (declare (type boolean-pvar $node?)
	   (type float-pvar
		 $node-x $node-y $node-grad-x $node-grad-y
		 $new-node-x $new-node-y))

  (*when $node?
    (*let (($step-size (the float-pvar (!! step-size))))
      (declare (type float-pvar $step-size))
      (*set $new-node-x (+!! $node-x (*!! $step-size $node-grad-x)))
      (*set $new-node-y (+!! $node-y (*!! $step-size $node-grad-y))))))

(*defun *clear-nodes ($node? &rest pvars)
  (declare (type boolean-pvar $node?))
  (*when $node?
    (dolist ($pvar pvars)
      (*set (the float-pvar $pvar) (!! 0.0)))))

;;;============================================================
;;; node oriented version
;;;============================================================

(*defun *nodes-compute-loss (n index f $x $y $loss $step-x $step-y)
  (declare (type float-pvar $x $y $loss $step-x $step-y))
  (*let* (($next-address
	   (mod!! (+!! (self-address!!)
		       (the (field-pvar *log-number-of-processors-limit*)
			    (!! index)))
		  (the (field-pvar *log-number-of-processors-limit*) (!! n))))
	 ($next-x (pref!! $x $next-address :collision-mode :no-collisions))
	 ($next-y (pref!! $y $next-address :collision-mode :no-collisions))
	 ($dx (-!! $x $next-x))
	 ($dy (-!! $y $next-y))
	 ($d (the float-pvar (l2-norm!! $dx $dy))))
    (declare (type (field-pvar *log-number-of-processors-limit*) $next-address)
	     (type float-pvar $next-x $next-y $dx $dy $d))
    (*let (($f (the float-pvar (aref f index))))
      (declare (type float-pvar $f))
      (*let* (($term (-!! $f $d))
	      ($term/f (/!! $term $f)))
	(declare (type float-pvar $term $term/f))
	(*set $loss (+!! $loss (/!! (*!! $term $term) $f)))
	(*set $step-x (+!! $step-x (*!! $term/f (/!! $dx $d))))
	(*set $step-y (+!! $step-y (*!! $term/f (/!! $dy $d))))))))

(*defun *nodes-compute-loss-0 (n index f $x $y $loss $step-x $step-y)
  (declare (type float-pvar $x $y $loss $step-x $step-y))
  (*let (($next-address
	   (mod!! (+!! (self-address!!)
		       (the (field-pvar *log-number-of-processors-limit*)
			    (!! index)))
		  (the (field-pvar *log-number-of-processors-limit*) (!! n))))
	 ($next-x (!! 0.0))
	 ($next-y (!! 0.0))
	 ($dx (!! 0.0))
	 ($dy (!! 0.0))
	 ($d (!! 0.0)))
    (declare (type (field-pvar *log-number-of-processors-limit*) $next-address)
	     (type float-pvar $next-x $next-y $dx $dy $d))
    (*pset :no-collisions $x $next-x $next-address)
    (*pset :no-collisions $y $next-y $next-address)
    (*set $dx (-!! $x $next-x))
    (*set $dy (-!! $y $next-y))
    (*set $d (the float-pvar (l2-norm!! $dx $dy)))
    (*let (($f (the float-pvar (aref f index))))
      (declare (type float-pvar $f))
      (*let* (($term (-!! $f $d))
	      ($term/f (/!! $term $f)))
	(declare (type float-pvar $term $term/f))
	(*set $loss (+!! $loss (/!! (*!! $term $term) $f)))
	(*set $step-x (+!! $step-x (*!! $term/f (/!! $dx $d))))
	(*set $step-y (+!! $step-y (*!! $term/f (/!! $dy $d))))))))

;;;============================================================

(defun make-distance-pvars (fn objects)
  (let* ((n (length objects))
	 (f (make-array n)))
    (do ((j 1 (1+ j))) ;; Never look at offset zero
	((>= j n))
      (let ((pvar (allocate!! (!! 0.0) nil 'float-pvar)))
	(declare (type float-pvar pvar))
	(setf (aref f j) pvar)
	(dotimes (i n)
	  (*setf (pref (the float-pvar pvar) i)
		(the single-float
		     (funcall fn
			      (aref objects i)
			      (aref objects (mod (+ i j) n))))))))
    f))

;;;============================================================

(defun node-mds (objects fn
		  &key
		  (initial-x (make-random-vector (length objects)))
		  (initial-y (make-random-vector (length objects)))
		  (step-size (/ 1.0e-1 (length objects)))
		  (max-iterations 1000)
		  #-lucid (plot-fn #'plot-xy)
		  #-lucid (plot *plot*)
		  #-lucid (canvas *canvas*))
  (let* ((n (length objects))
	 (f (make-distance-pvars fn objects)))
    (if (> n *number-of-processors-limit*)
	(*cold-boot :initial-dimensions (next-power-of-2 n))
	(*warm-boot))
    (*let (($x (!! 0.0)) ($y (!! 0.0)) ($loss (!! 0.0))
	   ($step-x (!! 0.0)) ($step-y (!! 0.0))
	   ($node? (<!! (self-address!!)
			 (the (field-pvar *log-number-of-processors-limit*)
			      (!! n)))))
      (declare (type float-pvar $x $y $loss $step-x $step-y)
	       (type boolean-pvar $node?))
      (array-to-pvar initial-x $x :cube-address-end n)
      (array-to-pvar initial-y $y :cube-address-end n)
      (let ((x-coords (make-array n)) (y-coords (make-array n))
	    (x-step (make-array n)) (y-step (make-array n)))
	(unwind-protect
	    (*when $node?
	      (dotimes (k max-iterations
			  (warn "~a iterations exceeded" max-iterations))
		(do ((i 1 (1+ i)));; Skip the zeroth iteration
		    ((>= i n))
		  (*nodes-compute-loss n i f $x $y $loss $step-x $step-y))
		(let ((current-loss (*sum $loss)))
		  (format t "~&~a loss: ~a~%" k current-loss))
		;; show the gradient step
		(pvar-to-array $x x-coords :cube-address-end n)
		(pvar-to-array $y y-coords :cube-address-end n)
		(*let (($step-size (the float-pvar (!! step-size))))
		  (declare (type float-pvar $step-size))
		  (pvar-to-array
		    (*!! $step-size $step-x) x-step :cube-address-end n)
		  (pvar-to-array
		    (*!! $step-size $step-y) y-step :cube-address-end n))
		#-lucid
		(*plot-gradient-step $x $y $step-x $step-y 0 n step-size
				     objects x-coords y-coords x-step y-step
				     :plot plot :canvas canvas
				     :plot-fn plot-fn)
		;; choose a step size
		#+symbolics
		(setq step-size 
		      (or (scl:prompt-and-accept
			    'float "New value for step-size? ")
			  step-size))
		;; Take a gradient step
		(*let (($step-size (the float-pvar (!! step-size))))
		  (declare (type float-pvar $step-size))
		  (*set $x (+!! $x (*!! $step-size $step-x)))
		  (*set $y (+!! $y (*!! $step-size $step-y))))
		(*set $loss (!! 0.0))
		(*set $step-x (!! 0.0))
		(*set $step-y (!! 0.0))))
                            
	  ;; Deallocate pvars
	  (do ((i 1 (1+ i))) ;; Never look at offset zero
	      ((>= i n))
	    (*deallocate (aref f i)))))
      (let ((x-coords (make-array n))  (y-coords (make-array n)))
	(pvar-to-array $x x-coords :cube-address-end n)
	(pvar-to-array $y y-coords :cube-address-end n)
	(values x-coords y-coords)))))

;;; ============================================================
;;; edge oriented version
;;; ============================================================
;;; un-directed edge version 

(*defun *edge-collect-data-from-nodes (;; input pvars
                                       $edge? $edge-node0 $edge-node1 
                                       $node-x $node-y 
                                       ;; output pvars
                                       $edge-x0 $edge-y0 $edge-x1 
                                       $edge-y1)
  (declare (type boolean-pvar $edge?)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1)
	   (type float-pvar $node-x $node-y $edge-x0 $edge-y0 
		 $edge-x1 $edge-y1))
  (*when $edge?
    (*set $edge-x0 
	  (pref!! $node-x $edge-node0
		  :collision-mode :many-collisions))
    (*set $edge-y0 
	  (pref!! $node-y $edge-node0
		  :collision-mode :many-collisions))
    (*set $edge-x1
	  (pref!! $node-x $edge-node1 
		  :collision-mode :many-collisions))
    (*set $edge-y1
	  (pref!! $node-y $edge-node1 
		  :collision-mode :many-collisions))))


;;; ============================================================


(*defun *edge-compute-deltas (;; input pvars
                              $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1 
                              ;; output pvars
                              $edge-dx $edge-dy $edge-2d-length)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar $edge-x0 $edge-y0 $edge-x1 $edge-y1 
		 $edge-dx $edge-dy $edge-2d-length))
  (*when $edge? (*set $edge-dx (-!! $edge-x0 $edge-x1))
	 (*set $edge-dy (-!! $edge-y0 $edge-y1))
	 (*set $edge-2d-length (l2-norm!! $edge-dx $edge-dy))))


;;; ============================================================


(*defun *edge-collect-grad-from-nodes (;; input pvars
                                       $edge? $edge-node0 $edge-node1 
                                       $node-grad-x $node-grad-y 
                                       ;; output pvars
                                       $edge-grad-x0 $edge-grad-y0 
                                       $edge-grad-x1 $edge-grad-y1)
  (declare (type boolean-pvar $edge?)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1)
	   (type float-pvar $node-grad-x $node-grad-y $edge-grad-x0
		 $edge-grad-y0 $edge-grad-x1 $edge-grad-y1))
  (*when $edge?
    (*set $edge-grad-x0 (pref!! $node-grad-x $edge-node0
				:collision-mode :many-collisions))
    (*set $edge-grad-y0 (pref!! $node-grad-y $edge-node0
				:collision-mode :many-collisions))
    (*set $edge-grad-x1 (pref!! $node-grad-x $edge-node1
				:collision-mode :many-collisions))
    (*set $edge-grad-y1 (pref!! $node-grad-y $edge-node1
				:collision-mode :many-collisions))))


;;; ============================================================


(*defun *edge-try-gradient-step
	(
	 ;; input pvars
	 step-size $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1 
	 $edge-grad-x0 $edge-grad-y0 $edge-grad-x1 $edge-grad-y1 

	 ;; output pvars
	 $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar $edge-x0 $edge-y0 $edge-x1 $edge-y1 
		 $edge-grad-x0 $edge-grad-y0 $edge-grad-x1 
		 $edge-grad-y1 $edge-new-x0 $edge-new-y0 
		 $edge-new-x1 $edge-new-y1))
  (*when $edge? (*let (($step-size (the float-pvar (!! step-size))
                                   ))
		  (declare (type float-pvar $step-size))
		  (*set $edge-new-x0 (+!! $edge-x0
					  (*!! $step-size 
					       $edge-grad-x0))
			)
		  (*set $edge-new-y0 (+!! $edge-y0
					  (*!! $step-size 
					       $edge-grad-y0))
			)
		  (*set $edge-new-x1 (+!! $edge-x1
					  (*!! $step-size 
					       $edge-grad-x1))
			)
		  (*set $edge-new-y1 (+!! $edge-y1
					  (*!! $step-size 
					       $edge-grad-y1))
			))))


;;; ============================================================


(*defun *edge-compute-distance-loss (;; input pvars
                                     $edge? $edge-length $edge-weight
                                     $edge-2d-length 
                                     ;; output pvar
                                     $edge-distance-loss)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar
		 $edge-length $edge-weight $edge-2d-length
		 $edge-distance-loss))
  (*when $edge?
    (*let (($dl (-!! $edge-length $edge-2d-length)))
      (declare (type float-pvar $dl))
      (*set $edge-distance-loss (*!! $edge-weight $dl $dl)))))


;;; ============================================================


(*defun *edge-send-loss-to-nodes (
                                  ;; input pvars
                                  $edge? $edge-loss $edge-node0 
                                  $edge-node1 $node? 

                                  ;; output pvar
                                  $node-loss)
  (declare (type boolean-pvar $edge? $node?)
	   (type float-pvar $edge-loss $node-loss)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1))
  (*let* (($node-loss0 (!! 0.0))
	  ($node-loss1 (!! 0.0)))
    (declare (type float-pvar $node-loss0 $node-loss1))
    (*when $edge? (*pset :add $edge-loss $node-loss0 
			 $edge-node0)
	   (*pset :add $edge-loss $node-loss1 $edge-node1))
    (*when $node? (*set $node-loss (+!! $node-loss0 
					$node-loss1)))))


;;; ============================================================
;;; assumes *edge-compute-distance-loss has just been called. 


(*defun *edge-compute-distance-grad (;; input pvars
                                     $edge? $edge-length $edge-weight
                                     $edge-2d-length $edge-dx $edge-dy
                                     ;; output pvars
                                     $edge-distance-grad-x 
                                     $edge-distance-grad-y)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar
		 $edge-length $edge-weight $edge-2d-length
		 $edge-dx $edge-dy
		 $edge-distance-grad-x $edge-distance-grad-y))
  (*when $edge?
    (*let* (($dl (-!! $edge-length $edge-2d-length)))
      (declare (type float-pvar $dl))
      (*set $edge-distance-grad-x
	    (*!! $edge-weight $dl (/!! $edge-dx $edge-2d-length)))
      (*set $edge-distance-grad-y
	    (*!! $edge-weight $dl (/!! $edge-dy $edge-2d-length))))))


;;; ============================================================


(*defun *edge-compute-order-loss (;; input pvars
                                  $edge? $edge-length $edge-weight
				  $edge-order $edge-dx 
                                  ;; output pvar
                                  $edge-order-loss)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar
		 $edge-length $edge-weight $edge-order $edge-dx 
		 $edge-order-loss))
  (*when $edge?
    (*let (($g 
	     ;; (*!! $edge-weight
	     ;;      (max!! (!! 0.0) (*!! $edge-order $edge-dx)))
	     (+!! (*!! $edge-order $edge-dx) $edge-length)))
      (declare (type float-pvar $g))
      (*set $edge-order-loss (*!! $edge-weight $g $g)))))


;;; ------------------------------------------------------------


(*defun *edge-compute-order-grad (;; input pvars
                                  $edge? $edge-length $edge-weight
				  $edge-order $edge-dx 
                                  ;; output pvars
                                  $edge-order-grad-x)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar $edge-length $edge-weight $edge-order $edge-dx 
		 $edge-order-grad-x))
  (*when $edge?
    (*set $edge-order-grad-x
	  ;; (*!! (!! -2.0) $edge-weight
	  ;; (max!! (!! 0.0) (*!! $edge-order $edge-dx)))
	  (*!! (!! -2.0)
	       $edge-order
	       $edge-weight
	       (+!! (*!! $edge-order $edge-dx) $edge-length)))))


;;; ============================================================


(*defun *edge-send-grad-to-nodes (;; input pvars
                                  $edge? $edge-grad-x $edge-grad-y 
                                  $edge-node0 $edge-node1 $node? 
                                  ;; output pvar
                                  $node-grad-x $node-grad-y)
  (declare (type boolean-pvar $edge? $node?)
	   (type float-pvar $edge-grad-x $edge-grad-y $node-grad-x 
		 $node-grad-y)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1))
  (*let* (($node-grad-x0 (!! 0.0))
	  ($node-grad-y0 (!! 0.0))
	  ($node-grad-x1 (!! 0.0))
	  ($node-grad-y1 (!! 0.0)))
    (declare (type float-pvar $node-grad-x0 $node-grad-y0 
		   $node-grad-x1 $node-grad-y1))
    (*when $edge? (*pset :add $edge-grad-x $node-grad-x0 
			 $edge-node0)
	   (*pset :add $edge-grad-y $node-grad-y0 $edge-node0
		  )
	   (*pset :add (-!! $edge-grad-x)
		  $node-grad-x1 $edge-node1)
	   (*pset :add (-!! $edge-grad-y)
		  $node-grad-y1 $edge-node1))
    (*when $node? (*set $node-grad-x (+!! $node-grad-x0 
					  $node-grad-x1))
	   (*set $node-grad-y (+!! $node-grad-y0 
				   $node-grad-y1)))))


;;; ============================================================

(defun edge-mds (graph dist-fn weight-fn
		 &key
		 (step-deflation 1.0)
		 (nodes (collect-nodes graph))
		 (initial-x (make-random-vector (length nodes)))
		 (initial-y (make-random-vector (length nodes)))
		 (step-size (/ 1.0 (length nodes)))
		 (max-iterations 1000)
		 #-lucid (plot-fn #'plot-xy)
		 #-lucid (plot *plot*)
		 #-lucid (canvas *canvas*))
  (let* ((n-nodes (length nodes))
	 (n-processors (next-power-of-2 (+ (length graph)
					   (length nodes))))
	 (x-coords (make-array n-nodes))
	 (y-coords (make-array n-nodes))
	 (grad-x (make-array n-nodes))
	 (grad-y (make-array n-nodes))
	 (current-loss 0.0))
             
    ;; allocate more processors than we will need
    (if (> n-processors *number-of-processors-limit*)
	(*cold-boot :initial-dimensions (list n-processors))
	(*warm-boot))
    (*let (($node? nil!!)
	   ($node-x (!! 0.0))
	   ($node-y (!! 0.0))
	   ($new-node-x (!! 0.0))
	   ($new-node-y (!! 0.0))
	   ($node-grad-x (!! 0.0))
	   ($node-grad-y (!! 0.0))
	   ($node-loss (!! 0.0))
	   ($edge? nil!!)
	   ($edge-node0 (!! 0))
	   ($edge-node1 (!! 0))
	   ($edge-length (!! 0.0))
	   ($edge-weight (!! 1.0))
	   ($edge-2d-length (!! 0.0))
	   ($edge-x0 (!! 0.0))
	   ($edge-y0 (!! 0.0))
	   ($edge-x1 (!! 0.0))
	   ($edge-y1 (!! 0.0))
	   ($edge-new-x0 (!! 0.0))
	   ($edge-new-y0 (!! 0.0))
	   ($edge-new-x1 (!! 0.0))
	   ($edge-new-y1 (!! 0.0))
	   ($edge-dx (!! 0.0))
	   ($edge-dy (!! 0.0))
	   ($edge-loss (!! 0.0))
	   ($edge-grad-x (!! 0.0))
	   ($edge-grad-y (!! 0.0))
	   ($edge-grad-x0 (!! 0.0))
	   ($edge-grad-y0 (!! 0.0))
	   ($edge-grad-x1 (!! 0.0))
	   ($edge-grad-y1 (!! 0.0)))
      (declare (type boolean-pvar $node? $edge?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $edge-node0 $edge-node1)
	       (type float-pvar $node-x $node-y $new-node-x
		     $new-node-y $node-grad-x 
		     $node-grad-y 
		     $node-loss $edge-length $edge-weight
		     $edge-2d-length $edge-x0 $edge-y0 
		     $edge-x1 $edge-y1 $edge-new-x0 
		     $edge-new-y0 $edge-new-x1 $edge-new-y1
		     $edge-dx $edge-dy $edge-loss 
		     $edge-grad-x 
		     $edge-grad-y 
		     $edge-grad-x0 
		     $edge-grad-y0 
		     $edge-grad-x1 
		     $edge-grad-y1))
      (let* ((node-interval
	       (*build-graph
		 graph nodes dist-fn weight-fn initial-x initial-y
		 $node? $node-x $node-y
		 $edge? $edge-node0 $edge-node1 $edge-length $edge-weight))
	     (nodes-start (car node-interval))
	     (nodes-end (cdr node-interval)))
	(flet ((try-step (step)
		 (*edge-try-gradient-step
		   step 
		   $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1 
		   $edge-grad-x0 $edge-grad-y0 
		   $edge-grad-x1 $edge-grad-y1 
		   $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1)
		 (*edge-compute-deltas
		   $edge? $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1
		   $edge-dx $edge-dy $edge-2d-length)
		 (*edge-compute-distance-loss
		   $edge? $edge-length $edge-weight
		   $edge-2d-length 
		   $edge-loss)
		 (*when $edge? (*sum $edge-loss))))
	  (dotimes (k max-iterations
		      (warn "~a iterations" max-iterations))
	    (*clear-nodes $node? $node-grad-x $node-grad-y 
			  $node-loss)
	    (*edge-collect-data-from-nodes
	      $edge? $edge-node0 $edge-node1 
	      $node-x $node-y $edge-x0 $edge-y0 $edge-x1 $edge-y1)
	    (*edge-compute-deltas 
	      $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1
	      $edge-dx $edge-dy $edge-2d-length)
	    (*edge-compute-distance-grad
	      $edge?
	      $edge-length $edge-weight $edge-2d-length
	      $edge-dx $edge-dy 
	      $edge-grad-x 
	      $edge-grad-y)
	    (*edge-send-grad-to-nodes
	      $edge? $edge-grad-x 
	      $edge-grad-y $edge-node0 
	      $edge-node1 $node? 
	      $node-grad-x 
	      $node-grad-y)
	    (*edge-collect-grad-from-nodes
	      $edge? $edge-node0 $edge-node1 
	      $node-grad-x 
	      $node-grad-y 
	      $edge-grad-x0 
	      $edge-grad-y0 
	      $edge-grad-x1 
	      $edge-grad-y1)
	    (multiple-value-setq
	      (step-size current-loss)
	      (bm:minimize-1d #'try-step -0.01
			      (* 10.0 (abs step-size))
			      :abs-tolerance 0.01) )
	    (let ((actual-step (* step-deflation step-size)))
	      (format *standard-output*
		      "~& best step=   ~f; ~% actual step= ~f, ~
                    ~% loss= ~f,~
                    ~% k= ~d." 
		      step-size actual-step current-loss k)
	      (when *plot-steps?*
		(*plot-gradient-step
		  $node-x $node-y $node-grad-x $node-grad-y
		  nodes-start nodes-end actual-step
		  nodes x-coords y-coords grad-x grad-y 
		  :plot-fn plot-fn :canvas canvas :plot plot))
	      (*take-gradient-step
		actual-step $node? $node-x $node-y $node-grad-x $node-grad-y)
	      )))))))


;;; ============================================================

(defun ordered-edge-mds (graph dist-fn weight-fn order-fn
			 &key
			 (step-deflation 1.0)
			 (alpha 0.5)
			 (nodes (collect-nodes graph))
			 (initial-x (make-random-vector (length nodes)))
			 (initial-y (make-random-vector (length nodes)))
			 (step-size (/ 1.0 (length nodes)))
			 (max-iterations 1000)
			 #-lucid (plot-fn #'plot-xy)
			 #-lucid (plot *plot*)
			 #-lucid (canvas *canvas*))
  (let* ((n-nodes (length nodes))
	 (n-processors (next-power-of-2 (+ (length graph) (length nodes))))
	 (x-coords (make-array n-nodes)) (y-coords (make-array n-nodes))
	 (grad-x (make-array n-nodes)) (grad-y (make-array n-nodes))
	 (current-loss 0.0))
       
    ;; allocate more processors than we will need
    (if (> n-processors *number-of-processors-limit*)
	(*cold-boot :initial-dimensions (list n-processors))
	(*warm-boot))
    (*let (($node? nil!!) ($node-x (!! 0.0)) ($node-y (!! 0.0))
	   ($new-node-x (!! 0.0)) ($new-node-y (!! 0.0))
	   ($node-grad-x (!! 0.0)) ($node-grad-y (!! 0.0))
	   ($node-loss (!! 0.0))
	   ($edge? nil!!) ($edge-node0 (!! 0)) ($edge-node1 (!! 0))
	   ($edge-length (!! 0.0)) ($edge-2d-length (!! 0.0))
	   ($edge-order (!! 0.0)) ($edge-weight (!! 1.0))
	   ($edge-x0 (!! 0.0)) ($edge-y0 (!! 0.0))
	   ($edge-x1 (!! 0.0)) ($edge-y1 (!! 0.0))
	   ($edge-new-x0 (!! 0.0)) ($edge-new-y0 (!! 0.0))
	   ($edge-new-x1 (!! 0.0)) ($edge-new-y1 (!! 0.0))
	   ($edge-dx (!! 0.0)) ($edge-dy (!! 0.0))
	   ($edge-distance-loss (!! 0.0))
	   ($edge-distance-grad-x (!! 0.0)) ($edge-distance-grad-y (!! 0.0))
	   ($edge-grad-x0 (!! 0.0)) ($edge-grad-y0 (!! 0.0))
	   ($edge-grad-x1 (!! 0.0)) ($edge-grad-y1 (!! 0.0))
	   ($edge-order-loss (!! 0.0)) ($edge-order-grad-x (!! 0.0))
	   ($alpha (the float-pvar (!! alpha)))
	   ($1-alpha (the float-pvar (!! (- 1.0 alpha)))))
      (declare (type boolean-pvar $node? $edge?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $edge-node0 $edge-node1)
	       (type float-pvar $node-x $node-y $new-node-x 
		     $new-node-y $node-grad-x $node-grad-y 
		     $node-loss $edge-length $edge-weight $edge-order
		     $edge-2d-length 
		     $edge-x0 $edge-y0 $edge-x1 $edge-y1 
		     $edge-new-x0 $edge-new-y0 $edge-new-x1 
		     $edge-new-y1 $edge-dx $edge-dy 
		     $edge-distance-loss $edge-distance-grad-x 
		     $edge-distance-grad-y $edge-grad-x0 
		     $edge-grad-y0 $edge-grad-x1 $edge-grad-y1 
		     $edge-order-loss $edge-order-grad-x $alpha 
		     $1-alpha))
      (let* ((node-interval
	       (*build-ordered-graph
		 graph nodes dist-fn weight-fn order-fn initial-x initial-y
		 $node? $node-x $node-y
		 $edge? $edge-node0 $edge-node1
		 $edge-length $edge-weight $edge-order))
	     (nodes-start (car node-interval))
	     (nodes-end (cdr node-interval)))
	(flet ((try-step (step)
		 (*edge-try-gradient-step
		   step $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1
		   $edge-grad-x0 $edge-grad-y0 $edge-grad-x1 $edge-grad-y1
		   $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1)
		 (*edge-compute-deltas
		   $edge? $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1 
		   $edge-dx $edge-dy $edge-2d-length)
		 (*edge-compute-distance-loss
		   $edge? $edge-length $edge-weight $edge-2d-length 
		   $edge-distance-loss)
		 (*edge-compute-order-loss
		   $edge? $edge-length $edge-weight $edge-order $edge-dx
		   $edge-order-loss)
		 (*when $edge?
		   (*sum (+!! (*!! $1-alpha $edge-distance-loss)
			      (*!! $alpha $edge-order-loss))))))
	  (dotimes (k max-iterations
		      (warn "~a iterations" max-iterations))
	    (*clear-nodes $node? $node-grad-x $node-grad-y $node-loss)
	    (*edge-collect-data-from-nodes
	      $edge? 
	      $edge-node0 $edge-node1 $node-x 
	      $node-y $edge-x0 $edge-y0 $edge-x1
	      $edge-y1)
	    (*edge-compute-deltas
	      $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1
	      $edge-dx $edge-dy $edge-2d-length)
	    (*edge-compute-distance-grad
	      $edge? $edge-length $edge-weight $edge-2d-length 
	      $edge-dx $edge-dy $edge-distance-grad-x $edge-distance-grad-y)
	    (*edge-compute-order-grad
	      $edge? $edge-length $edge-weight $edge-order $edge-dx 
	      $edge-order-grad-x)
	    (*edge-send-grad-to-nodes
	      $edge?
	      (+!! (*!! $1-alpha $edge-distance-grad-x)
		   (*!! $alpha $edge-order-grad-x))
	      (*!! $1-alpha $edge-distance-grad-y)
	      $edge-node0 $edge-node1 $node? 
	      $node-grad-x $node-grad-y)
	    (*edge-collect-grad-from-nodes
	      $edge? $edge-node0 $edge-node1 $node-grad-x $node-grad-y 
	      $edge-grad-x0 $edge-grad-y0 $edge-grad-x1 $edge-grad-y1)
	    (multiple-value-setq
	      (step-size current-loss)
	      (bm:minimize-1d #'try-step -0.01
			      (* 10.0 (abs step-size))
			      :abs-tolerance 0.01))
	    #-lucid
	    (when *debug*
	      (plot-function
		#'try-step (* -10.0 (+ 0.01 step-size))
		(* 10.0 (+ 0.01 step-size)) :nsteps 20))
	    #-lucid
	    (let ((actual-step
		    (random (+ 0.01 (abs (* 2.0 step-deflation step-size))))))
	    (format *standard-output*
		      "~& best step=   ~f; ~% actual step= ~f, ~
                    ~% loss= ~f,~
                    ~% k= ~d." 
		      step-size actual-step current-loss k) 
	      (when *plot-steps?*
		(*plot-gradient-step
		  $node-x $node-y $node-grad-x $node-grad-y
		  nodes-start nodes-end actual-step
		  nodes x-coords y-coords grad-x grad-y 
		  :plot-fn plot-fn :canvas canvas :plot plot))
	      (*take-gradient-step
		actual-step $node? $node-x $node-y $node-grad-x $node-grad-y)
	      )))))))
