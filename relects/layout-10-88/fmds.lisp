;;;-*- Mode: Lisp; Package: (FMDS Lisp); Syntax: Common-Lisp; -*-

;;;=======================================================

(provide "FRIEDMAN-MDS")

(in-package "FMDS" :use '("LISP") :nicknames '("Friedman-MDS"))

;;; Shadow, Export, Require, Use-package, and Import forms should follow here

(require "MINIMAL-SPANNING-TREE")

#+xerox
(eval-when (load)
  ;; Translated (il:files il:plot) to require forms
  (require "PLOT"))

;;;=======================================================

(defmacro make-2d-point (x y) `(cons ,x ,y))
(defmacro 2d-point-x (2d-point) `(car ,2d-point))
(defmacro 2d-point-y (2d-point) `(cdr ,2d-point))

;;;=======================================================

(defun triangulate (p0 p1 l m &optional p-other d-other allow-violation)

  ;; p0 and p1 are mapped points (x, y) pairs. L and m are distances
  ;; from p0 and p1 respectively to a third (not yet mapped) point, p2.
  ;; P-OTHER is another mapped point used to resolve the positional
  ;; ambiguity. Returns two values. The first value is an (x, y) pair
  ;; for the unmapped point, p2, that perserves the distances l and m,
  ;; and minimizes the difference between ||p2 - p-other|| and d-other.
  ;; The second value returned is an indicator of whether the triangle
  ;; inequality was violated by l, m and d. 
  
  ;; First Compute the result in the rotated and translated
  ;; coordinate system where p0 -> (0 , 0) and p2 -> (d , 0).
  (let* ((x0 (2d-point-x p0)) (y0 (2d-point-y p0))
	 (x1 (2d-point-x p1)) (y1 (2d-point-y p1))
	 ;; Distance between p0 and p1
	 (d (l2-dist p0 p1))
	 ;; Rotation Coefficients
	 (c (/ (- x1 x0) d)) (s (/ (- y1 y0) d))
	 x2-r y2-r)
    ;; Check for violations of theTriangle inequality. 
    (if (or (> l (+ d m)) (> m (+ d l)) (> d (+ l m)))
	(if allow-violation 
	    ;; Resolve violations by adjusting m. Do the rest of the
	    ;; computation exactly to avoid passing a small negative
	    ;; number to sqrt, due to round off error.
	    (setq m (+ d l)
		  x2-r (- l)
		  y2-r 0.0)
	    (error "Unexpected violation of the triangle inequality"))
	;; Implicitly x0-r = 0 , y0-r =0, x1-r = d and y1-r =0.
	(setq x2-r (/ (- (+ (* l l) (* d d)) (* m m)) (* 2 d))
	      y2-r (sqrt (* (- l x2-r) (+ l x2-r)))))
    (when p-other 
      ;; resolve sign ambiguity
      (let* ((p-other-r
	       (apply-rotation
		 (apply-translation p-other (- x0) (- y0)) c (- s)))
	     (x-other-r (2d-point-x p-other-r))
	     (y-other-r (2d-point-y p-other-r))
	     (-y2-r (- y2-r))
	     (p- )
	(when (< (abs (- d-other (l2-dist x2-r -y2-r x-other-r y-other-r)))
		 (abs (- d-other (l2-dist x2-r  y2-r x-other-r y-other-r))))
	  (setq y2-r -y2-r))))
    ;; Transform back to the original coordinate system
    (apply-translation (apply-rotation (make-2d-point x2-r y2-r) c s) x0 y0)))

;;;=======================================================

(defun l2-dist (p0 p1)
  (let* ((x0 (2d-point-x p0)) (y0 (2d-point-y p0))
	 (x1 (2d-point-x p1)) (y1 (2d-point-y p1))
	 (dx (- x0 x1))
	 (dy (- y0 y1)))
    (sqrt (+ (* dx dx) (* dy dy)))))

;;;=======================================================

(defun apply-rotation (p c s) 
  ;; P is an (x,y) pair. C and S are the cosine and sine respectively
  (let ((x (2d-point-x p)) (y (2d-point-y p)))
    (make-2d-point (- (* c x) (* s y))
	  (+ (* s x) (* c y)))))

;;;=======================================================

(defun apply-translation (p x-offset y-offset) 
  ;; P is an (x,y) pair. 
  (let ((x (2d-point-x p)) (y (2d-point-y p)))
    (make-2d-point (+ x x-offset) (+ y y-offset))))

;;;=======================================================
;;; Algorithm taken from "Graphics for the Multivariate Two-sample
;;; Problem", J.H. Friedman and L.C.  Rafsky, JASA, Vol. 76, Number
;;; 374, pgs. 277- 287, June 1981 . 
;;; VERTICES is a vector of vertex objects -- the result of
;;; MST:MAKE-MINIMAL-SPANNING-TREE. OBJECTS is a vector of objects with
;;; a metric defined on them. 
;;; DISTANCE-FN applied to two objects returns the distance between
;;; them. The distance function is assumed to be symmetric. 
;;; Assumes that an index into VERTICES addresses an Object witht he
;;; same index in OBJECTS
;;; Returns a a vector of (x,y) pairs for every object in OBJECTS. 

(defun make-friedman-mds (vertices objects distance-fn)
  
  (when (/= (length vertices) (length objects))
    (error "Invalid arguments: ~s ~s" vertices objects))
  (let* ((points (make-array (length vertices)))
	 (center-index 
	   ;; Find the vertex with most daughters
	   (let ((max-connections 0)
		 max-index)
	     (dotimes (i (length vertices) max-index)
	       (let* ((size
			(length (mst:vertex-connections (aref vertices i)))))
		 (when (>= size max-connections)
		   (setq max-connections size max-index i))))))
	 (center-connections
	   (mst:vertex-connections (aref vertices center-index))))
    (when (< (length center-connections) 2)
      (error "Center vertex has < 2 daughters: ~s"
	     (aref vertices center-index)))
    ;; prepare the spare slots
    (dotimes (i (length vertices))
      (setf (mst:vertex-spare (aref vertices i)) nil))
    ;; Map the depth zero points
    (let ((center-daughters (copy-list center-connections))
	  max-index another-index)
      ;; Find the most distant daughter
      (setq max-index
	    (farthest-daughter
	      center-index center-connections vertices objects distance-fn))
      (setq center-daughters (delete max-index center-daughters))
      ;; Pick another (arbitrary) daughter
      (setq another-index (car center-daughters))
      (setq center-daughters (cdr center-daughters))
      ;; Map the center, furthest daughter, and another (arbitrary) daughter
      (let* ((p-center (make-2d-point 0.0 0.0))
	     (p-max (make-2d-point (funcall distance-fn
				   (aref objects center-index)
				   (aref objects max-index))
			  0.0))
	     (p-other (triangulate p-center p-max
				   (funcall distance-fn
					    (aref objects center-index)
					    (aref objects another-index))
				   (funcall distance-fn
					    (aref objects max-index)
                                            (aref objects another-index)))))
	(setf (aref points center-index) p-center)
	(setf (aref points max-index) p-max)
	(setf (aref points another-index) p-other)
	
	;; For each remaining daughter, map it preserving distances to
	;; the center and the daughter furthest from the center,
	;; resolving ambiguities the another (arbitrary) daughter
	(map-daughters
	  center-daughters p-center center-index p-max max-index p-other 
	  another-index points vertices objects distance-fn)))
    
    ;; mark the depth zero points as mapped (and record parent relation)
    (dolist (index center-connections)
      (setf (mst:vertex-spare (aref vertices index)) center-index))
    
    ;; Mark the center as mapped
    (setf (mst:vertex-spare (aref vertices center-index)) t)
    (map-depth>=2 center-connections points vertices objects distance-fn)
    points))

;;;=======================================================

(defun map-depth>=2 (parent-indices points vertices objects distance-fn)
  ;; Indices is a list of vertex indices. GRAND-PARENT is the object of
  ;; an (already visited) vertex.  INDICES-AT-DEPTH is an assoc list of
  ;; (parent . daughters)
  (let* ((groups-at-depth
	   (mapcan
	     #'(lambda (parent-index)
		 ;; Connected vertices not already visited
		 (let ((result
			 (mapcan
			   #'(lambda (daughter-index)
			       (let ((vertex (aref vertices daughter-index)))
				 (when (null (mst:vertex-spare vertex))
				   (list daughter-index))))
			   (mst:vertex-connections
			     (aref vertices parent-index)))))
		   (if result (list (cons parent-index result)))))
	     parent-indices))
	 (indices-at-depth
	   (mapcan #'(lambda (pair) (copy-list (cdr pair)))
		   groups-at-depth)))
    (loop
      (when (null groups-at-depth) (return nil))
      (let (daughters parent-index max-index another-index)
	;; Find vertex at this depth farthest from its parent
	(let ((m-dist 0) (m-index 0) m-pair)
	  (dolist (pair groups-at-depth)
	    (let ((parent-index (car pair))(indices (cdr pair)))
	      (multiple-value-bind (index dist)
		  (farthest-daughter
		    parent-index indices vertices objects distance-fn)
		(when (>= dist m-dist)
		  (setq m-dist dist m-index index m-pair pair)))))
	  (setq parent-index (car m-pair))
	  (setq daughters (delete m-index (copy-list (cdr m-pair))))
	  (setq groups-at-depth (delete m-pair groups-at-depth))
	  (setq max-index m-index))
	;; Find farthest point already mapped
	(setq another-index (farthest-index-already-mapped
			      max-index vertices objects distance-fn))
	;; Now map max-vertex, preseving distance to parent and farthest
	;; vertex already mapped. This may not be possible because the
	;; distance to the the farthest vertex may violate the triangle
	;; inequality. Resolve by minimizing the discrepancy.
	(let* ((grand-parent-index
		 (mst:vertex-spare (aref vertices parent-index)))
	       (p-grand (aref points grand-parent-index))
	       (p-parent (aref points parent-index))
	       (p-other (aref points another-index))
	       (p-max (triangulate
			p-parent
			p-other
			(funcall distance-fn
				 (aref objects parent-index)
				 (aref objects max-index))
			(funcall distance-fn
				 (aref objects another-index)
				 (aref objects max-index))
			p-grand
			(funcall distance-fn
				 (aref objects grand-parent-index)
				 (aref objects max-index))
			t)))
	  (setf (aref points max-index) p-max)
	  ;; For each remaining daughter, map it preserving distances to
	  ;; parent and max daughter
	  (map-daughters
	    daughters p-parent parent-index p-max max-index p-grand
	    grand-parent-index points vertices objects distance-fn))
	;; Mark all mapped points (and record parent relation)
	(dolist (index daughters)
	  (setf (mst:vertex-spare (aref vertices index))  parent-index))
	(setf (mst:vertex-spare (aref vertices max-index)) parent-index)))
    (when indices-at-depth ;; Recurse
      (map-depth>=2 indices-at-depth points vertices objects distance-fn))))

;;;=======================================================

(defun map-daughters (indices
		      p0 object0-index
		      p1 object1-index
		      p-other object-other-index points 
		      vertices objects distance-fn)
  (let ((object0 (aref objects object0-index))
	(object1 (aref objects object1-index))
	(object-other (aref objects object-other-index))
	object)
    (dolist (index indices)
      (setq object (aref objects index))
      (setf (aref points index)
	    (triangulate
	      p0
	      p1
	      (funcall distance-fn object0 object)
	      (funcall distance-fn object1 object)
	      p-other
	      (funcall distance-fn object-other object))))))

;;;=======================================================

(defun farthest-index-already-mapped (object-index vertices objects
				      distance-fn)
  (let ((object (aref objects object-index))
	(n (length vertices))
	(max-dist 0)
	(max-index 0)
	dist)
    (dotimes (i n)
      (let ((vertex (aref vertices i)))
	(when (eq (mst:vertex-spare vertex) t)
	  ;; Point already mapped
	  (setq dist (funcall distance-fn
			      object
			      (aref objects (mst:vertex-object vertex))))
	  (when (>= dist max-dist) (setq max-dist dist max-index i)))))
    max-index))

;;;=======================================================

(defun farthest-daughter (parent-index daughters vertices objects distance-fn)
  (let ((parent-object (aref objects parent-index))
	(max-dist 0)
	max-index)
    (dolist (index daughters)
      (let ((dist (funcall distance-fn parent-object (aref objects index))))
	(when (>= dist max-dist) (setq max-dist dist max-index index))))
    (values max-index max-dist)))

;;;=======================================================
;; debugging

(defun verify-mds (points vertices objects distance-fn)
  (mst:initialize-for-walk vertices 0)
  (mst:walk-tree
    0 vertices
    #'(lambda (vertex index daughters)
	(let ((point (aref points index))
	      (object (aref objects index)))
	  (dolist (daughter daughters)
	    (let* ((daughter-point (aref points daughter))
		   (ld (l2-dist (2d-point-x point)
				(2d-point-y point)
				(2d-point-x daughter-point)
				(2d-point-y daughter-point)))
		   (lm (funcall distance-fn object (aref objects daughter))))
	      (if (not (= ld lm))
		  (format
		    t 
		    "L2 distance ~s User distance ~s between points ~d ~d~%" 
		    ld lm index daughter))))))))

;;;=======================================================
;; Browser
#+xerox
(defvar *star* '#*(5 5)G@@@MH@@JH@@MH@@G@@@ )

#+xerox
(defun make-mds-browser (objects distance-fn)
  (let* ((n (length objects))
	 (vertices (mst:make-minimal-spanning-tree objects distance-fn))
	 (points (make-friedman-mds vertices objects distance-fn))
	 (plot (il:createplot nil nil "Mds browser")))
    (dotimes (i n)
      (il:plotpoint plot
		    (aref points i)
		    (concatenate 'string "Object-" (prin1-to-string i))
		    *star* nil t))
    (il:rescaleplot plot)
    plot))

;;;=======================================================
;;; Exports
;;;=======================================================
(eval-when (load)
  (export '(make-friedman-mds l2-dist triangulate)
	  (find-package "FRIEDMAN-MDS")))