;;;-*- Package:(CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

(defun make-tree-object (&optional (depth 4) (breadth 2))
  (let* ((objects (make-array 10 :initial-element nil :fill-pointer 1 
			      :adjustable t)))
    (labels ((add-children (parent current-depth)
	       (unless (eq depth current-depth)
		 (dotimes (j (+ (random breadth) 2))
		   (let ((node (append parent (list j))))
		     (vector-push-extend node objects)
		     (add-children node (1+ current-depth)))))))
      (add-children nil 0))
    objects))

#+Symbolics (defparameter *root-bitmap* 
	      (2dg:make-bitmap
		:width 12
		:height 12
		:raw-bitmap
		(graphics:with-output-to-bitmap ()
		  (graphics:draw-circle 5 5 1 :filled nil)
		  (graphics:draw-circle 5 5 3 :filled nil)
		  (graphics:draw-circle 5 5 5 :filled nil))))

(defun graph-distance (node1 node2)
  (do ((node1-tail node1 (cdr node1-tail))
       (node2-tail node2 (cdr node2-tail)))
      ((or (null node1-tail)
	   (null node2-tail)
	   (not (eq (car node1-tail) (car node2-tail))))
       (float (+ (length node1-tail) (length node2-tail))))))

(defun traverse-tree (walk-fn parent nodes)
  (let* ((length-parent (length parent))
	 (child-indices
	   (2dg:with-collection
	     (dotimes (i (length nodes))
	       (let ((node (aref nodes i)))
		 (if (and (= (1+ length-parent) (length node))
			  (= (mismatch parent node) length-parent))
		     (2dg:collect i)))))))
    (funcall walk-fn parent nodes)
    (dolist (child-index child-indices)
      (traverse-tree walk-fn (aref nodes child-index) nodes))))

(defun parent-order (node0 node1)
  (let* ((l0 (length node0)) (l1 (length node1)))
    (cond 
      ((and (= (1+ l0) l1) (= (mismatch node0 node1) l0))
       1.0)
      ((and (= (1+ l1) l0) (= (mismatch node1 node0) l1))
       -1.0)
      (t 0.0))))

(defun ancestor-order (node0 node1)
  (let* ((l0 (length node0)) (l1 (length node1)))
    (cond 
      ((= (mismatch node0 node1) l0)
       1.0)
      ((= (mismatch node1 node0) l1)
       -1.0)
      (t 0.0))))

(defun layout-tree (nodes)
  (let* ((n (length nodes))
	 (x-coords (make-array n))
	 (y-coords (make-array n))
	 (root (aref nodes 0)))
    (setf (aref x-coords 0) 0.0)
    (setf (aref y-coords 0) 0.0)
    (layout-node root nodes 0.0 0.0 0.0
		 (float (* 2 pi) 1.0)
		 x-coords y-coords)
    (values x-coords y-coords)))

(defun layout-node (parent nodes x y angle angle-range x-coords y-coords)
  (let* ((length-parent (length parent))
	 (child-indices
	   (2dg:with-collection
	     (dotimes (i (length nodes))
	       (let ((node (aref nodes i)))
		 (if (and (= (length node) (1+ length-parent))
			  (= (mismatch parent node) length-parent))
		     (2dg:collect i)))))))
    (when child-indices
      (let* ((d-theta (/ angle-range (length child-indices)))
	     (theta (+ (- angle (/ angle-range 2.0)) (/ d-theta 2.0))))
	(dolist (child-index child-indices)
	  (let ((child (aref nodes child-index))
		(child-x (+ x (cos theta)))
		(child-y (+ y (sin theta))))
	    (setf (aref x-coords child-index) child-x)
	    (setf (aref y-coords child-index) child-y)
	    (layout-node
	      child nodes child-x child-y theta d-theta x-coords y-coords)
	    (incf theta d-theta)))))))

#-lucid
(defun plot-initial-layout (nodes
			    &optional x-coords y-coords
			    &key (canvas *canvas*) (plot *plot*))
  (if (or (null x-coords) (null y-coords))
      (multiple-value-setq (x-coords y-coords) (layout-tree nodes)))
  (setf (2dp:plot-objects plot) nil)
  (2dp:add-plot-objects
    (2dg:with-collection
      (dotimes (i (length x-coords))
	(let ((symbol (ecase (mod (length (aref nodes i)) 4)
			(0 *root-bitmap*)
			(1 2dg:*circle-bitmap*)
			(2 2dg:*star-bitmap*)
			(3 2dg:*cross-bitmap*))))
	  (2dg:collect
	    (2dp:make-point
	      (2dg:make-position (aref x-coords i) (aref y-coords i))
	      :symbol symbol)))))
    plot)
  (labels
    ((add-edges (parent parent-index)
       (let*
	 ((length-parent (length parent))
	  (child-indices
	    (2dg:with-collection
	      (dotimes (i (length nodes))
		(let ((node (aref nodes i)))
		  (if (and (= (1+ length-parent) (length node))
			   (= (mismatch parent node) length-parent))
		      (2dg:collect i)))))))
	 (dolist (child-index child-indices)
	   (2dp:add-plot-object
	     (2dp:make-curve
	       (list (2dg:make-position (aref x-coords parent-index)
					(aref y-coords parent-index))
		     (2dg:make-position (aref x-coords child-index)
					(aref y-coords child-index)))
	       :style
	       (2dp::make-line-style :width 1 :dashing 2dg:*dash*))
	     plot)
	   (add-edges (aref nodes child-index) child-index)))))
    (add-edges nil 0))
  (2dp:rescale-plot plot :both)
  (2dp:redraw-plot plot canvas)
  (values x-coords y-coords))

(defparameter *plot-points?* nil)
#-lucid
(defun plot-tree-xy (plot canvas nodes x-coords y-coords x-step y-step)
  (setf nodes (coerce nodes 'Vector))
  (setf (2dp:plot-objects plot) nil)
  (2dp:add-plot-objects
    (2dg:with-collection
      (dotimes (i (length x-coords))
	(let* ((x0 (aref x-coords i))
	       (y0 (aref y-coords i))
	       (x1 (+ x0 (aref x-step i)))
	       (y1 (+ y0 (aref y-step i)))
	       (symbol (ecase (mod (length (aref nodes i)) 4)
			 (0 *root-bitmap*)
			 (1 2dg:*circle-bitmap*)
			 (2 2dg:*star-bitmap*)
			 (3 2dg:*cross-bitmap*))))
	  (when *plot-points?*
	    (2dg:collect
	      (2dp:make-point (2dg:make-position x0 y0) :symbol symbol)))
	  (2dg:collect
	    (2dp:make-curve
	      (list (2dg:make-position x0 y0) (2dg:make-position x1 y1)))))))
    plot)
  (labels
    ((add-edges
	(parent parent-index)
       (let*
	 ((length-parent (length parent))
	  (child-indices
	    (2dg:with-collection
	      (dotimes (i (length nodes))
		(let ((node (aref nodes i)))
		  (if (and (= (1+ length-parent) (length node))
			   (= (mismatch parent node) length-parent))
		      (2dg:collect i)))))))
	 (dolist (child-index child-indices)
	   (2dp:add-plot-object
	     (2dp:make-curve
	       (list (2dg:make-position (aref x-coords parent-index)
					(aref y-coords parent-index))
		     (2dg:make-position (aref x-coords child-index)
					(aref y-coords child-index)))
	       :style
	       (2dp::make-line-style :width 1
				     :dashing 2dg:*dash*))
	     plot)
	   (add-edges (aref nodes child-index) child-index)))))
    (add-edges nil 0))
  (2dp:rescale-plot plot :both)
  (2dp:redraw-plot plot canvas))

