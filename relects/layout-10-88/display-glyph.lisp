;;; -*- Mode: LISP; Syntax: Common-lisp; Package: (Cm-User :use (*Lisp Lisp)); -*-

;;;============================================================

(in-package (or (find-package "CM-USER")
		(make-package "CM-USER" :use '(*Lisp Lisp))))

;;;============================================================

(*defvar $pixels (!! 0))
(proclaim '(type (field-pvar 8) $pixels))

(*defun *make-ball-glyph ($pixels
			   &key
			   (nrows (first *current-cm-configuration*))
			   (ncols (second *current-cm-configuration*)))
  (declare (type (field-pvar 8) $pixels))
  (let* ((c-x (ftruncate nrows 2))
	 (c-y (ftruncate ncols 2))
	 (r (min c-x c-y))
	 (r^2 (* r r)))
    (*let* (($dx (-!! (self-address-grid!! (!! 0))
		      (the float-pvar (!! c-x))))
	    ($dy (-!! (self-address-grid!! (!! 1))
		      (the float-pvar (!! c-y))))
	    ($r^2 (+!! (*!! $dx $dx) (*!! $dy $dy))))
      ;;(declare (type (signed-pvar 32) $dx $dy $r))
      (*set $pixels
	    (max!! (!! 0)
		   (-!! (!! 255)
			(truncate!!
			  (*!! (!! 255.0)
			       (/!! $r^2 (the float-pvar (!! r^2))))))))
      $pixels)))


(*defun *blat-around ($pixels)
  (with-paris-from-*lisp (cmfb:clear *display-id*))
  (dotimes (i 1024)
    (let ((x (random (- 1280 128)))
	  (y (random (- 1024 128))))
      (*when (>!! $pixels (!! 0))
	(*fill-screen-with-8-bit-pvar $pixels :x-offset x :y-offset y)))))