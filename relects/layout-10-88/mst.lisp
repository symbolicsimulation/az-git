;;;-*- Mode: Lisp; Package: (MST Lisp); Syntax: Common-Lisp; -*-
;;;=======================================================
;;; File converted on 15-Mar-88 13:27:37 from source
;;; minimal-spanning-tree Original source
;;; {qv}<pedersen>lisp>minimal-spanning-tree.;12 created 1-Mar-88
;;; 15:10:20
;;; Copyright (c) 1988 by Xerox Corporation
;;;=======================================================

(provide "MINIMAL-SPANNING-TREE")

(in-package "MST" :use '("LISP") :nicknames '("MINIMAL-SPANNING-TREE"))

;;; Shadow, Export, Require, Use-package, and Import forms should follow here

;;;=======================================================

(defstruct (vertex (:copier nil)) object connections spare)


(defstruct (heap-node (:copier nil))
  distance < >= 
  ;; v1 and v2 are (fixnum) indices
  (v1 0 :type (unsigned-byte 16))
  (v2 0 :type (unsigned-byte 16)))


(defvar *cache-size* 3)

;;;=======================================================

(defun make-minimal-spanning-tree (objects distance-fn)
  
  ;; OBJECTS is a vector of objects with metric defined on them.
  ;; DISTANCE-FN applied to two objects returns the distance between
  ;; them. The distance function is assumed to be symmetric. Returns a
  ;; Minimal-Spanning-Tree
  
  (when (not (vectorp objects)) (error "Not a vector: ~s" objects))
  (let* ((n (length objects))
	 (edges 0)
	 (limit (1- n))
	 (vertices (make-vertices n))
	 (heap (make-heap n objects vertices distance-fn))
	 (sets nil))
    (loop (when (eq edges limit) (return nil))
	  (let* ((next-node (get-smallest-from-heap heap))
		 (v1 (heap-node-v1 next-node))
		 (v1-set (get-set v1 sets))
		 (v2 (heap-node-v2 next-node))
		 (v2-set (get-set v2 sets)))
	    (when (or (null v1-set)
		      (null v2-set)
		      (not (eq v1-set v2-set)))
	      ;; accept this edge
	      (incf edges)
	      (push v1 (vertex-connections (aref vertices v2)))
	      (push v2 (vertex-connections (aref vertices v1)))
	      ;; merge sets for v1 and v2
	      (if (null v1-set)
		  (if (null v2-set)
		      (push (list v1 v2) sets)
		      (nconc v2-set (list v1)))
		  (if (null v2-set)
		      (nconc v1-set (list v2))
		      (setq sets (merge-sets v1-set v2-set sets)))))
	    (when (eq 0 (decf (vertex-spare (aref vertices v1))))
	      ;; Refill cache
	      (setq heap (update-heap
			   (aref vertices v1)
			   n objects distance-fn heap v1-set)))))
    vertices))

;;;=======================================================

(defun make-vertices (n)
  (let ((vector (make-array n)))
    (dotimes (i n) (setf (aref vector i) (make-vertex :object i)))
    vector))

;;;=======================================================

(defun get-distances (index n objects distance-fn &optional exceptions)
  
  ;; Compute the (n - (index +1)) distances from index (skipping
  ;; EXCEPTIONS) and return a list of (index . distance) pairs --
  ;; sorted in increasing order of distance from objects[index].
  
  (do ((distances nil)
       (j (1+ index) (1+ j)))
      ((eq j n)
       (sort distances '< :key 'cdr))
    (unless (and exceptions (member j exceptions :test #'eq))
      (push
	(cons j (funcall distance-fn (aref objects index) (aref objects j)))
	distances))))

;;;=======================================================

(defun make-heap (n objects vertices distance-fn
		  &optional (cache-size *cache-size*))
  (let ((heap nil))
    (dotimes (i n)
      (setq heap (update-heap (aref vertices i) n objects distance-fn heap)))
    heap))

;;;=======================================================

(defun update-heap (vertex n objects distance-fn heap
		    &optional
		    (exceptions nil)
		    (cache-size *cache-size*))
  (let* ((index (vertex-object vertex))
	 (distances (get-distances index n objects distance-fn exceptions))
	 (cnt 0)
	 node)
    (dolist (pair distances)
      (when (eq cnt cache-size) (return nil))
      (let ((v2 (car pair))
	    (distance (cdr pair)))
	;; Note: always v1 < v2
	(setq node (make-heap-node :distance distance :v1 index :v2 v2))
	(if (null heap)
	    (setq heap node)
	    (add-to-heap heap node)))
      (incf cnt))
    (setf (vertex-spare vertex) (min cnt (length distances)))
    heap))

;;;=======================================================

(defun add-to-heap (heap new-node)
  (if (null heap)
      new-node
      (let ((distance (heap-node-distance new-node))
	    (current-node heap)
	    node-distance node-< node->=)
	(loop (setq node-distance (heap-node-distance current-node))
	      (setq node-< (heap-node-< current-node))
	      (setq node->= (heap-node->= current-node))
	      (cond
		((< distance node-distance)
		 (when (null node-<)
		   (setf (heap-node-< current-node) new-node)
		   (return nil))
		 (setq current-node node-<))
		(t
		 (when (null node->=)
		   (setf (heap-node->= current-node) new-node)
		   (return nil))
		 (setq current-node node->=))))
	heap)))

;;;=======================================================

(defun return-heap-node (node heap)
  (when node
    (let ((node-< (heap-node-< node))
	  (node->= (heap-node->= node)))
      (setf (heap-node-<  node) nil)
      (setf (heap-node->= node) nil)
      (add-to-heap heap node)
      (return-heap-node node-< heap)
      (return-heap-node node->= heap))))

;;;=======================================================

(defun get-smallest-from-heap (heap)
  (when (null heap) (error "No nodes in heap!"))
  (let ((previous-node nil)
	(current-node heap)
	node-<)
    (loop (when (null (setq node-< (heap-node-< current-node))) (return nil))
	  (setq previous-node current-node)
	  (setq current-node node-<))
    ;; Detach CURRENT-NODE and return
    (cond
      ((null previous-node)
       ;; Top-of-heap is smallest. Do surgury to preserve EQ'ness
       (let ((node->= (heap-node->= current-node)))
	 (cond
	   ((null node->=)
	    (warn "Heap exhausted"))
	   (t 	   ;; switch CURRENT-NODE and NODE->= 
	    (setf (heap-node-< current-node) (heap-node-< node->=))
	    (setf (heap-node-< node->=) nil)
	    (setf (heap-node->= current-node) (heap-node->= node->=))
	    (setf (heap-node->= node->=) nil)
	    (rotatef (heap-node-distance current-node)
		     (heap-node-distance node->=))
	    (rotatef (heap-node-v1 current-node) (heap-node-v1 node->=))
	    (rotatef (heap-node-v2 current-node) (heap-node-v2 node->=))
	    (setq current-node node->=)))))
      (t
       (setf (heap-node-< previous-node) nil)
       ;; Recycle >= subtree
       (return-heap-node (heap-node->= current-node) heap)
       (setf (heap-node->= current-node) nil)))
    current-node))

;;;=======================================================

(defun get-set (vertex sets)
  (do* ((tail sets (cdr tail))
	(set (car tail) (car tail)))
       ((null tail))
    (when (member vertex set :test #'eq) (return set))))

;;;=======================================================

(defun merge-sets (v1-set v2-set sets)
  (setq sets (delete v1-set sets))
  (setq sets (delete v2-set sets))
  (push (nconc v1-set v2-set) sets))

;;;=======================================================
;;; Applications
;;;=======================================================

(defun walk-tree (vertex-id mst walk-fn)
  
  ;; Assumes all the spare slots have been initialized to NIL, except
  ;; for the root spare slot, which has been initialized to T. Applies
  ;; walk-fn to every vertex of MST in a depth-first walk of MST, with
  ;; args vertex, vertex-index, daughter-indices. Always returns nil.
  
  (let* ((vertex (aref mst vertex-id))
	 (connections (vertex-connections vertex))
	 (to-ids (mapcan #'(lambda (id)
			     (let* ((dest (aref mst id))
				    (spare (vertex-spare dest)))
			       (when (null (vertex-spare dest))
				 (setf (vertex-spare dest) t)
				 (list id))))
			 connections)))
    (funcall walk-fn vertex vertex-id to-ids)
    (dolist (index to-ids) (walk-tree index mst walk-fn))))

;;;=======================================================

(defun initialize-for-walk (mst root-index)
  (dotimes (i (length mst) (setf (vertex-spare (aref mst root-index)) t))
    (setf (vertex-spare (aref mst i)) nil)))

;;;=======================================================
;; Debugging
;;;=======================================================

#+xerox
(defun show-mst (mst &optional (root-index 0)) 
  ;; Use the grapher package to display a minimal spanning tree
  (let (node-list)
    ;; Initialize spares
    (initialize-for-walk mst root-index)
    (walk-tree
      root-index
      mst
      #'(lambda (vertex vertex-id to-ids)
	  (push (il:nodecreate vertex-id vertex-id nil to-ids nil nil t)
		node-list)))
    (il:showgraph (il:layoutgraph node-list (list root-index)))))

#+symbolics
(defun show-mst (mst &optional (root-index 0)) 
  (scl:format-graph-from-root
    mst
    #'(lambda (thing stream) (scl:present vertex-id 'number :stream stream))
    #'(lambda (vertex) )
    :stream stream
    :dont-draw-duplicates t
    :orientation :horizontal
    :border '(:shape :rectangle)
    :column-spacing 8 
    :within-column-spacing 2
    :default-drawing-mode :line
    :test #'eq))

;;;=======================================================

(defun print-connections (tree &optional objects)
  (let ((n (length tree)))
    (if objects
	(dotimes (i n)
	  (format t "i ~d object ~s connections ~s~%" 
		  i (aref objects i) (vertex-connections (aref tree i))))
	(dotimes (i n)
	  (format t "i ~d connections ~s~%"
		  i (vertex-connections (aref tree i)))))))

;;;=======================================================
;;; Exports
;;;=======================================================

(eval-when (load)
  (export '( *cache-size* make-minimal-spanning-tree vertex vertex-p 
	    make-vertex vertex-object vertex-connections
	    vertex-spare walk-tree initialize-for-walk)
	  (find-package "MINIMAL-SPANNING-TREE")))