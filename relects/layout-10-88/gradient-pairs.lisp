;;;-*- Package: (CM-USER :USE (LISP *LISP)); Syntax: Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;; ============================================================
;;; un-directed pair version 


(*defun *pair-collect-data-from-nodes (;; input pvars
                                       $pair? $pair-node0 $pair-node1 
                                       $node-x $node-y 
                                       ;; output pvars
                                       $pair-x0 $pair-y0 $pair-x1 
                                       $pair-y1)
	(declare (type boolean-pvar $pair?)
		 (type (field-pvar *log-number-of-processors-limit*)
		       $pair-node0 $pair-node1)
		 (type float-pvar $node-x $node-y $pair-x0 $pair-y0 
		       $pair-x1 $pair-y1))
	(*when $pair?
	       (*set $pair-x0 
		     (pref!! $node-x $pair-node0
			     :collision-mode :many-collisions))
	       (*set $pair-y0 
		     (pref!! $node-y $pair-node0
			     :collision-mode :many-collisions))
	       (*set $pair-x1
		     (pref!! $node-x $pair-node1 
			     :collision-mode :many-collisions))
	       (*set $pair-y1
		     (pref!! $node-y $pair-node1 
			     :collision-mode :many-collisions))))


;;; ============================================================


(*defun *pair-compute-deltas (;; input pvars
                              $pair? $pair-x0 $pair-y0 $pair-x1 $pair-y1 
                              ;; output pvars
                              $pair-dx $pair-dy $pair-2d-length)
  (declare (type boolean-pvar $pair?)
	   (type float-pvar $pair-x0 $pair-y0 $pair-x1 $pair-y1 
		 $pair-dx $pair-dy $pair-2d-length))
  (*when $pair? (*set $pair-dx (-!! $pair-x0 $pair-x1))
	 (*set $pair-dy (-!! $pair-y0 $pair-y1))
	 (*set $pair-2d-length (l2-norm!! $pair-dx $pair-dy))))


;;; ============================================================


(*defun *pair-collect-grad-from-nodes (;; input pvars
                                       $pair? $pair-node0 $pair-node1 
                                       $node-grad-x $node-grad-y 
                                       ;; output pvars
                                       $pair-grad-x0 $pair-grad-y0 
                                       $pair-grad-x1 $pair-grad-y1)
  (declare (type boolean-pvar $pair?)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $pair-node0 $pair-node1)
	   (type float-pvar $node-grad-x $node-grad-y $pair-grad-x0
		 $pair-grad-y0 $pair-grad-x1 $pair-grad-y1))
  (*when $pair?
    (*set $pair-grad-x0 (pref!! $node-grad-x $pair-node0
				:collision-mode :many-collisions))
    (*set $pair-grad-y0 (pref!! $node-grad-y $pair-node0
				:collision-mode :many-collisions))
    (*set $pair-grad-x1 (pref!! $node-grad-x $pair-node1
				:collision-mode :many-collisions))
    (*set $pair-grad-y1 (pref!! $node-grad-y $pair-node1
				:collision-mode :many-collisions))))


;;; ============================================================


(*defun *pair-try-gradient-step
       (
        ;; input pvars
        step-size $pair? $pair-x0 $pair-y0 $pair-x1 $pair-y1 
        $pair-grad-x0 $pair-grad-y0 $pair-grad-x1 $pair-grad-y1 

        ;; output pvars
        $pair-new-x0 $pair-new-y0 $pair-new-x1 $pair-new-y1)
       (declare (type boolean-pvar $pair?)
              (type float-pvar $pair-x0 $pair-y0 $pair-x1 $pair-y1 
                    $pair-grad-x0 $pair-grad-y0 $pair-grad-x1 
                    $pair-grad-y1 $pair-new-x0 $pair-new-y0 
                    $pair-new-x1 $pair-new-y1))
       (*when $pair? (*let (($step-size (the float-pvar (!! step-size))
                                   ))
                           (declare (type float-pvar $step-size))
                           (*set $pair-new-x0 (+!! $pair-x0
                                                   (*!! $step-size 
                                                        $pair-grad-x0))
                                 )
                           (*set $pair-new-y0 (+!! $pair-y0
                                                   (*!! $step-size 
                                                        $pair-grad-y0))
                                 )
                           (*set $pair-new-x1 (+!! $pair-x1
                                                   (*!! $step-size 
                                                        $pair-grad-x1))
                                 )
                           (*set $pair-new-y1 (+!! $pair-y1
                                                   (*!! $step-size 
                                                        $pair-grad-y1))
                                 ))))


;;; ============================================================


(*defun *pair-compute-distance-loss (;; input pvars
                                     $pair? $pair-length $pair-weight
                                     $pair-2d-length 
                                     ;; output pvar
                                     $pair-distance-loss)
  (declare (type boolean-pvar $pair?)
	   (type float-pvar
		 $pair-length $pair-weight $pair-2d-length
		 $pair-distance-loss))
  (*when $pair?
    (*let (($dl (-!! $pair-length $pair-2d-length)))
      (declare (type float-pvar $dl))
      (*set $pair-distance-loss (*!! $pair-weight $dl $dl)))))


;;; ============================================================


(*defun *pair-send-loss-to-nodes (
                                  ;; input pvars
                                  $pair? $pair-loss $pair-node0 
                                  $pair-node1 $node? 

                                  ;; output pvar
                                  $node-loss)
       (declare (type boolean-pvar $pair? $node?)
              (type float-pvar $pair-loss $node-loss)
              (type (field-pvar *log-number-of-processors-limit*)
                    $pair-node0 $pair-node1))
       (*let* (($node-loss0 (!! 0.0))
               ($node-loss1 (!! 0.0)))
              (declare (type float-pvar $node-loss0 $node-loss1))
              (*when $pair? (*pset :add $pair-loss $node-loss0 
                                   $pair-node0)
                     (*pset :add $pair-loss $node-loss1 $pair-node1))
              (*when $node? (*set $node-loss (+!! $node-loss0 
                                                  $node-loss1)))))


;;; ============================================================
;;; assumes *pair-compute-distance-loss has just been called. 


(*defun *pair-compute-distance-grad (;; input pvars
                                     $pair? $pair-length $pair-weight
                                     $pair-2d-length $pair-dx $pair-dy
                                     ;; output pvars
                                     $pair-distance-grad-x 
                                     $pair-distance-grad-y)
  (declare (type boolean-pvar $pair?)
	   (type float-pvar
		 $pair-length $pair-weight $pair-2d-length
		 $pair-dx $pair-dy
		 $pair-distance-grad-x $pair-distance-grad-y))
  (*when $pair?
    (*let* (($dl (-!! $pair-length $pair-2d-length)))
      (declare (type float-pvar $dl))
      (*set $pair-distance-grad-x
	    (*!! $pair-weight $dl (/!! $pair-dx $pair-2d-length)))
      (*set $pair-distance-grad-y
	    (*!! $pair-weight $dl (/!! $pair-dy $pair-2d-length))))))


;;; ============================================================


(*defun *pair-compute-order-loss (;; input pvars
                                  $pair? $pair-length $pair-weight
				  $pair-order $pair-dx 
                                  ;; output pvar
                                  $pair-order-loss)
  (declare (type boolean-pvar $pair?)
	   (type float-pvar
		 $pair-length $pair-weight $pair-order $pair-dx 
		 $pair-order-loss))
  (*when $pair?
    (*let (($g 
	     ;; (*!! $pair-weight
	     ;;      (max!! (!! 0.0) (*!! $pair-order $pair-dx)))
	     (+!! (*!! $pair-order $pair-dx) $pair-length)))
      (declare (type float-pvar $g))
      (*set $pair-order-loss (*!! $pair-weight $g $g)))))


;;; ------------------------------------------------------------


(*defun *pair-compute-order-grad (;; input pvars
                                  $pair? $pair-length $pair-weight
				  $pair-order $pair-dx 
                                  ;; output pvars
                                  $pair-order-grad-x)
  (declare (type boolean-pvar $pair?)
	   (type float-pvar $pair-length $pair-weight $pair-order $pair-dx 
		 $pair-order-grad-x))
  (*when $pair?
    (*set $pair-order-grad-x
	  ;; (*!! (!! -2.0) $pair-weight
	  ;; (max!! (!! 0.0) (*!! $pair-order $pair-dx)))
	  (*!! (!! -2.0)
	       $pair-order
	       $pair-weight
	       (+!! (*!! $pair-order $pair-dx) $pair-length)))))


;;; ============================================================


(*defun *pair-send-grad-to-nodes (;; input pvars
                                  $pair? $pair-grad-x $pair-grad-y 
                                  $pair-node0 $pair-node1 $node? 
                                  ;; output pvar
                                  $node-grad-x $node-grad-y)
  (declare (type boolean-pvar $pair? $node?)
	   (type float-pvar $pair-grad-x $pair-grad-y $node-grad-x 
		 $node-grad-y)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $pair-node0 $pair-node1))
  (*let* (($node-grad-x0 (!! 0.0))
	  ($node-grad-y0 (!! 0.0))
	  ($node-grad-x1 (!! 0.0))
	  ($node-grad-y1 (!! 0.0)))
    (declare (type float-pvar $node-grad-x0 $node-grad-y0 
		   $node-grad-x1 $node-grad-y1))
    (*when $pair? (*pset :add $pair-grad-x $node-grad-x0 
			 $pair-node0)
	   (*pset :add $pair-grad-y $node-grad-y0 $pair-node0
		  )
	   (*pset :add (-!! $pair-grad-x)
		  $node-grad-x1 $pair-node1)
	   (*pset :add (-!! $pair-grad-y)
		  $node-grad-y1 $pair-node1))
    (*when $node? (*set $node-grad-x (+!! $node-grad-x0 
					  $node-grad-x1))
	   (*set $node-grad-y (+!! $node-grad-y0 
				   $node-grad-y1)))))


;;; ============================================================


(defvar *debug* nil)

(defparameter *plot-steps?* #-lucid t #+lucid nil)

(defun pair-mds (graph dist-fn weight-fn
		 &key
		 (nodes (collect-nodes graph))
		 (initial-x (make-random-vector (length nodes)))
		 (initial-y (make-random-vector (length nodes)))
		 (step-size (/ 1.0 (length nodes)))
		 (max-iterations 1000)
		 #-lucid (plot-fn #'plot-xy)
		 #-lucid (plot *plot*)
		 #-lucid (canvas *canvas*))
  (let* ((n-nodes (length nodes))
	 (n-processors (next-power-of-2 (+ (length graph)
					   (length nodes))))
	 (x-coords (make-array n-nodes))
	 (y-coords (make-array n-nodes))
	 (grad-x (make-array n-nodes))
	 (grad-y (make-array n-nodes))
	 (current-loss 0.0))
             
    ;; allocate more processors than we will need
    (when (> n-processors *number-of-processors-limit*)
      (*cold-boot :initial-dimensions (list n-processors)))
    (*let (($node? nil!!)
	   ($node-x (!! 0.0))
	   ($node-y (!! 0.0))
	   ($new-node-x (!! 0.0))
	   ($new-node-y (!! 0.0))
	   ($node-distance-grad-x (!! 0.0))
	   ($node-distance-grad-y (!! 0.0))
	   ($node-distance-loss (!! 0.0))
	   ($pair? nil!!)
	   ($pair-node0 (!! 0))
	   ($pair-node1 (!! 0))
	   ($pair-length (!! 0.0))
	   ($pair-weight (!! 1.0))
	   ($pair-2d-length (!! 0.0))
	   ($pair-x0 (!! 0.0))
	   ($pair-y0 (!! 0.0))
	   ($pair-x1 (!! 0.0))
	   ($pair-y1 (!! 0.0))
	   ($pair-new-x0 (!! 0.0))
	   ($pair-new-y0 (!! 0.0))
	   ($pair-new-x1 (!! 0.0))
	   ($pair-new-y1 (!! 0.0))
	   ($pair-dx (!! 0.0))
	   ($pair-dy (!! 0.0))
	   ($pair-distance-loss (!! 0.0))
	   ($pair-distance-grad-x (!! 0.0))
	   ($pair-distance-grad-y (!! 0.0))
	   ($pair-distance-grad-x0 (!! 0.0))
	   ($pair-distance-grad-y0 (!! 0.0))
	   ($pair-distance-grad-x1 (!! 0.0))
	   ($pair-distance-grad-y1 (!! 0.0)))
      (declare (type boolean-pvar $node? $pair?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $pair-node0 $pair-node1)
	       (type float-pvar $node-x $node-y $new-node-x
		     $new-node-y $node-distance-grad-x 
		     $node-distance-grad-y 
		     $node-distance-loss $pair-length $pair-weight
		     $pair-2d-length $pair-x0 $pair-y0 
		     $pair-x1 $pair-y1 $pair-new-x0 
		     $pair-new-y0 $pair-new-x1 $pair-new-y1
		     $pair-dx $pair-dy $pair-distance-loss 
		     $pair-distance-grad-x 
		     $pair-distance-grad-y 
		     $pair-distance-grad-x0 
		     $pair-distance-grad-y0 
		     $pair-distance-grad-x1 
		     $pair-distance-grad-y1))
      (let* ((node-interval
	       (*build-graph
		 graph nodes dist-fn weight-fn initial-x initial-y
		 $node? $node-x $node-y
		 $pair? $pair-node0 $pair-node1 $pair-length $pair-weight))
	     (nodes-start (car node-interval))
	     (nodes-end (cdr node-interval)))
	(flet ((try-step (step)
		 (*pair-try-gradient-step
		   step 
		   $pair? $pair-x0 $pair-y0 $pair-x1 $pair-y1 
		   $pair-distance-grad-x0 $pair-distance-grad-y0 
		   $pair-distance-grad-x1 $pair-distance-grad-y1 
		   $pair-new-x0 $pair-new-y0 $pair-new-x1 $pair-new-y1)
		 (*pair-compute-deltas
		   $pair? $pair-new-x0 $pair-new-y0 $pair-new-x1 $pair-new-y1
		   $pair-dx $pair-dy $pair-2d-length)
		 (*pair-compute-distance-loss
		   $pair? $pair-length $pair-weight
		   $pair-2d-length 
		   $pair-distance-loss)
		 (*when $pair? (*sum $pair-distance-loss))))
	  (dotimes (k max-iterations
		      (warn "~a iterations" max-iterations))
	    (*clear-nodes $node? $node-distance-grad-x $node-distance-grad-y 
			  $node-distance-loss)
	    (*pair-collect-data-from-nodes
	      $pair? $pair-node0 $pair-node1 
	      $node-x $node-y $pair-x0 $pair-y0 $pair-x1 $pair-y1)
	    (*pair-compute-deltas 
	      $pair? $pair-x0 $pair-y0 $pair-x1 $pair-y1
	      $pair-dx $pair-dy $pair-2d-length)
	    (*pair-compute-distance-grad
	      $pair?
	      $pair-length $pair-weight $pair-2d-length
	      $pair-dx $pair-dy 
	      $pair-distance-grad-x 
	      $pair-distance-grad-y)
	    (*pair-send-grad-to-nodes
	      $pair? $pair-distance-grad-x 
	      $pair-distance-grad-y $pair-node0 
	      $pair-node1 $node? 
	      $node-distance-grad-x 
	      $node-distance-grad-y)
	    (*pair-collect-grad-from-nodes
	      $pair? $pair-node0 $pair-node1 
	      $node-distance-grad-x 
	      $node-distance-grad-y 
	      $pair-distance-grad-x0 
	      $pair-distance-grad-y0 
	      $pair-distance-grad-x1 
	      $pair-distance-grad-y1)
	    (multiple-value-setq
	      (step-size current-loss)
	      (1d-minimize #'try-step 0.0 
			   step-size 
			   :absolute-tolerance 0.01))
	    (format *standard-output* 
		    "~& step-size= ~f; loss= ~f."
		    step-size current-loss)
	    #-lucid
	    (when *plot-steps?*
	      (*plot-gradient-step $node-x 
				   $node-y 
				   $node-distance-grad-x 
				   $node-distance-grad-y 
				   nodes-start nodes-end 
				   step-size nodes 
				   x-coords y-coords 
				   grad-x grad-y :plot-fn
				   plot-fn :canvas canvas
				   :plot plot))
	    (*take-gradient-step step-size 
				 $node? $node-x $node-y 
				 $node-distance-grad-x 
				 $node-distance-grad-y)))))))


;;; ============================================================

(defun ordered-pair-mds (graph dist-fn weight-fn order-fn
			 &key
			 (alpha 0.5)
			 (nodes (collect-nodes graph))
			 (initial-x (make-random-vector (length nodes)))
			 (initial-y (make-random-vector (length nodes)))
			 (step-size (/ 1.0 (length nodes)))
			 (max-iterations 1000)
			 #-lucid (plot-fn #'plot-xy)
			 #-lucid (plot *plot*)
			 #-lucid (canvas *canvas*))
  (let* ((n-nodes (length nodes))
	 (n-processors (next-power-of-2 (+ (length graph) (length nodes))))
	 (x-coords (make-array n-nodes))
	 (y-coords (make-array n-nodes))
	 (grad-x (make-array n-nodes))
	 (grad-y (make-array n-nodes))
	 (current-loss 0.0))
       
    ;; allocate more processors than we will need
    (if (> n-processors *number-of-processors-limit*)
	(*cold-boot :initial-dimensions (list n-processors))
	(*warm-boot))
    (*let (($node? nil!!)
	   ($node-x (!! 0.0))
	   ($node-y (!! 0.0))
	   ($new-node-x (!! 0.0))
	   ($new-node-y (!! 0.0))
	   ($node-grad-x (!! 0.0))
	   ($node-grad-y (!! 0.0))
	   ($node-loss (!! 0.0))
	   ($pair? nil!!)
	   ($pair-node0 (!! 0))
	   ($pair-node1 (!! 0))
	   ($pair-length (!! 0.0))
	   ($pair-order (!! 0.0))
	   ($pair-weight (!! 1.0))
	   ($pair-2d-length (!! 0.0))
	   ($pair-x0 (!! 0.0))
	   ($pair-y0 (!! 0.0))
	   ($pair-x1 (!! 0.0))
	   ($pair-y1 (!! 0.0))
	   ($pair-new-x0 (!! 0.0))
	   ($pair-new-y0 (!! 0.0))
	   ($pair-new-x1 (!! 0.0))
	   ($pair-new-y1 (!! 0.0))
	   ($pair-dx (!! 0.0))
	   ($pair-dy (!! 0.0))
	   ($pair-distance-loss (!! 0.0))
	   ($pair-distance-grad-x (!! 0.0))
	   ($pair-distance-grad-y (!! 0.0))
	   ($pair-grad-x0 (!! 0.0))
	   ($pair-grad-y0 (!! 0.0))
	   ($pair-grad-x1 (!! 0.0))
	   ($pair-grad-y1 (!! 0.0))
	   ($pair-order-loss (!! 0.0))
	   ($pair-order-grad-x (!! 0.0))
	   ($alpha (the float-pvar (!! alpha)))
	   ($1-alpha (the float-pvar (!! (- 1.0 alpha)))))
      (declare (type boolean-pvar $node? $pair?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $pair-node0 $pair-node1)
	       (type float-pvar $node-x $node-y $new-node-x 
		     $new-node-y $node-grad-x $node-grad-y 
		     $node-loss $pair-length $pair-weight $pair-order
		     $pair-2d-length 
		     $pair-x0 $pair-y0 $pair-x1 $pair-y1 
		     $pair-new-x0 $pair-new-y0 $pair-new-x1 
		     $pair-new-y1 $pair-dx $pair-dy 
		     $pair-distance-loss $pair-distance-grad-x 
		     $pair-distance-grad-y $pair-grad-x0 
		     $pair-grad-y0 $pair-grad-x1 $pair-grad-y1 
		     $pair-order-loss $pair-order-grad-x $alpha 
		     $1-alpha))
      (let* ((node-interval
	       (*build-ordered-graph
		 graph nodes dist-fn weight-fn order-fn initial-x initial-y
		 $node? $node-x $node-y
		 $pair? $pair-node0 $pair-node1
		 $pair-length $pair-weight $pair-order))
	     (nodes-start (car node-interval))
	     (nodes-end (cdr node-interval)))
	(flet ((try-step (step)
		 (*pair-try-gradient-step
		   step $pair? $pair-x0 $pair-y0 $pair-x1 $pair-y1
		   $pair-grad-x0 $pair-grad-y0 $pair-grad-x1 $pair-grad-y1
		   $pair-new-x0 $pair-new-y0 $pair-new-x1 $pair-new-y1)
		 (*pair-compute-deltas
		   $pair? $pair-new-x0 $pair-new-y0 $pair-new-x1 $pair-new-y1 
		   $pair-dx $pair-dy $pair-2d-length)
		 (*pair-compute-distance-loss
		   $pair? $pair-length $pair-weight $pair-2d-length 
		   $pair-distance-loss)
		 (*pair-compute-order-loss
		   $pair? $pair-length $pair-weight $pair-order $pair-dx
		   $pair-order-loss)
		 (*when $pair?
		   (*sum (+!! (*!! $1-alpha $pair-distance-loss)
			      (*!! $alpha $pair-order-loss))))))
	  (dotimes (k max-iterations
		      (warn "~a iterations" max-iterations))
	    (*clear-nodes $node? $node-grad-x $node-grad-y $node-loss)
	    (*pair-collect-data-from-nodes
	      $pair? 
	      $pair-node0 $pair-node1 $node-x 
	      $node-y $pair-x0 $pair-y0 $pair-x1
	      $pair-y1)
	    (*pair-compute-deltas
	      $pair? $pair-x0 $pair-y0 $pair-x1 $pair-y1
	      $pair-dx $pair-dy $pair-2d-length)
	    (*pair-compute-distance-grad
	      $pair? $pair-length $pair-weight $pair-2d-length 
	      $pair-dx $pair-dy $pair-distance-grad-x $pair-distance-grad-y)
	    (*pair-compute-order-grad
	      $pair? $pair-length $pair-weight $pair-order $pair-dx 
	      $pair-order-grad-x)
	    (*pair-send-grad-to-nodes
	      $pair?
	      (+!! (*!! $1-alpha $pair-distance-grad-x)
		   (*!! $alpha $pair-order-grad-x))
	      (*!! $1-alpha $pair-distance-grad-y)
	      $pair-node0 $pair-node1 $node? 
	      $node-grad-x $node-grad-y)
	    (*pair-collect-grad-from-nodes $pair? 
					   $pair-node0 $pair-node1 
					   $node-grad-x $node-grad-y 
					   $pair-grad-x0 $pair-grad-y0 
					   $pair-grad-x1 $pair-grad-y1)
	    (multiple-value-setq
	      (step-size current-loss)
	      (1d-minimize #'try-step -0.01
			   (+ (abs step-size)
			      0.01)
			   :absolute-tolerance 0.01))
	    #-lucid
	    (when *debug*
	      (plot-function #'try-step
			     (* -10.0 (+ 0.01 step-size))
			     (* 10.0 (+ 0.01 step-size))
			     :nsteps 20))
	    (format *standard-output* 
		    "~& step-size= ~f; loss= ~f." 
		    step-size current-loss)
	    #-lucid
	    (when *plot-steps?*
	      (*plot-gradient-step $node-x $node-y
				   $node-grad-x $node-grad-y 
				   nodes-start nodes-end 
				   step-size nodes x-coords 
				   y-coords grad-x grad-y 
				   :plot-fn plot-fn :canvas 
				   canvas :plot plot))
	    (*take-gradient-step step-size $node? 
				 $node-x $node-y $node-grad-x 
				 $node-grad-y)))))))
