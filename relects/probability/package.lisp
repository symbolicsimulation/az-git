;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: User -*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package 'User)

(eval-when (compile load eval)
  (unless (find-package "PROBABILITY")
    (make-package "PROBABILITY"
		  :nicknames '(Prob)
		  :use `(PCL Lisp)))  )
