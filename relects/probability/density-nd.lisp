;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Probability-*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved.
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;;
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;;
;;;=======================================================================

(in-package :Probability)

(export '(Probability-with-N-Dimensional-Density
          N-Dimensional-Gaussian-Probability))

;;;=======================================================================

(defclass Probability-with-N-Dimensional-Density (Probability)
  ((dimension
    :accessor dimension
    :initarg :dimension
    :type Number)))

;;;--------------------------------------------------------------

(defmethod density ((pm Probability-with-N-dimensional-Density)
                    (x Sequence))
  (error "Not yet defined."))

;;;=======================================================================

(defclass N-Dimensional-Gaussian-Probability
  (Probability-with-N-dimensional-Density)

  ((mean
      :type Sequence
      :accessor mean
      :initarg :mean)
   (variance
      :type (Array Single-Float 2)
      :reader variance
      :initarg :variance)
   (sqrt-variance
      :type (Array Single-Float 2)
      :reader sqrt-variance
      :initarg :sqrt-variance)
   (1d-gaussian
      :type Gaussian-Probability
      :accessor 1d-gaussian
      :initarg :1d-gaussian)
   (spherical-sample-vector
      :type Vector
      :accessor spherical-sample-vector
      :initarg :spherical-sample-vector)))

;;;--------------------------------------------------------------

(defmethod shared-initialize :after
  ((pm N-Dimensional-Gaussian-Probability)
   slot-names
   &rest
     initargs
   &key
     (mean (bm:make-float-array (dimension pm) :initial-element 0.0))
     (variance (bm:make-identity-float-array (dimension pm)))
     (1d-gaussian (make-instance 'Gaussian-Probability :generator (generator pm)))
     (spherical-sample-vector (bm:make-float-array (dimension pm)))
   &allow-other-keys)

  (declare (ignore slot-names initargs))

  (setf (mean pm) mean)
  (setf (slot-value pm 'variance) variance)
  (setf (1d-gaussian pm) 1d-gaussian)
  (setf (spherical-sample-vector pm) spherical-sample-vector))

;;;--------------------------------------------------------------

(defmethod (setf variance) ((pdm Array)
                            (pm N-Dimensional-Gaussian-Probability))

  (setf (slot-value pm 'variance) pdm)
  (setf (slot-value pm 'sqrt-variance) (bm:cholesky pdm)))

;;;--------------------------------------------------------------

(defmethod sample ((pm N-Dimensional-Gaussian-Probability)
                    &optional (result (make-array (dimension pm))))

  (declare (type Sequence result)
           (values random-vector))

  (assert (and (typep result 'Sequence)
          (= (dimension pm) (length result))))

  (sample-sequence (1d-gaussian pm) (spherical-sample-vector pm))
  (bm:matrix*vector (sqrt-variance pm) (spherical-sample-vector pm) result))

;;;----------------------------------------------------------------

(defmethod density ((pm N-Dimensional-Gaussian-Probability) x)

  (let ((std-x (/ (- x (mean pm))
        (standard-deviation pm))))
    (bm:density-standard-gaussian std-x)))

