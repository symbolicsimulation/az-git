;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Probability -*-
;;;
;;; Copyright 1986. John C. Michalak. All Rights Reserved. 
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================================

(in-package 'Probability)

;;;=======================================================================
;;; Shuffle the builtin CL <random> generator.
;;; see Knuth, v. 2, 2nd ed., p. 32, Algorithm B.

(defvar *shuffle-array-length* 1024)

;;;-----------------------------------------------------------------------

(defclass Shuffle-Generator (Object)     
  ((gaussian-cache
     :accessor gaussian-cache
     :type Single-Float
     :initform -1.0)
   (state
     :accessor state
     :type Random-State
     :initarg :state)
   (shuffle-array
     :accessor shuffle-array
     :type (Vector Number)
     :initform (make-array *shuffle-array-length*))
   (last-value
     :accessor last-value
     :type Number))
  (:default-initargs
    :state *random-state*))

;;;--------------------------------------------------------------

(defmethod shared-initialize :after ((sg Shuffle-Generator) 
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))

  (let ((a (shuffle-array sg)))
    (dotimes (i (length a)) (setf (aref a i) (%sample sg)))
    (setf (last-value sg) (%sample sg))))

;;;--------------------------------------------------------------
;;; "internal" <%sample> method
;;;--------------------------------------------------------------

(defmethod %sample ((sg Shuffle-Generator))

  ;; John Michalak did this. I don't know why (jam, 9-87).
  ;; John Michalak says he got it from Andreas---still don't know why, 12-87.
  #||
  (/ (+ (random (expt 2 20) (state sg))
	0.5)
     (expt 2.0 20))
  ||#
  (random 1.0 (state sg)))
			      
;;;--------------------------------------------------------------

(defmethod generate-standard-uniform ((sg Shuffle-Generator))
  
  (let* ((sa (shuffle-array sg))
	 (i (truncate (* (last-value sg) (length sa)))))
    (setf (last-value sg) (aref sa i))
    (setf (aref sa i) (%sample sg))
    (last-value sg)))

;;;--------------------------------------------------------------

(defmethod generate-uniform ((sg Shuffle-Generator) min max)
  (+ min (* (generate-standard-uniform sg) (- max min))))

;;;--------------------------------------------------------------

(defmethod generate-exponential ((sg Shuffle-Generator) l)
  (/ (log (generate-standard-uniform sg)) (- l)))

;;;--------------------------------------------------------------

(defmethod generate-standard-gaussian ((sg Shuffle-Generator))
  (if (minusp (gaussian-cache sg))
      ;; then compute 2 and save 1 in the cache
      (let ((r (sqrt (generate-exponential sg 0.5))) 
	    (theta (* bm:*2pi* (generate-standard-uniform sg))))
	(setf (gaussian-cache sg) (* r (cos theta)))
	(* r (sin theta)))
      ;; else empty the cache
      (prog1 (gaussian-cache sg)
	     (setf (gaussian-cache sg) -1.0))))

;;;--------------------------------------------------------------

(defmethod generate-gaussian ((sg Shuffle-Generator) mean scale)
  (+ mean (* scale (generate-standard-gaussian sg))))

;;;--------------------------------------------------------------

(defmethod generate-%beta ((sg Shuffle-Generator) a b)
  (let* ((y1 (expt (generate-standard-uniform sg) (/ a)))
	 (y2 (expt (generate-standard-uniform sg) (/ b)))
	 (y1+y2 (+ y1 y2)))
    (if (> y1+y2 1)
	(generate-%beta sg a b)
	(/ y1 y1+y2))))

;;;--------------------------------------------------------------

(defmethod generate-gamma ((sg Shuffle-Generator) a)
  (multiple-value-bind (int rem) (truncate a)  
    (let ((x (if (zerop rem)
		 0.0
		 (* (generate-%beta sg rem (- 1 rem))))))
      (dotimes (i int) (incf x (generate-exponential sg 1.0)))
      x)))

;;;--------------------------------------------------------------

(defmethod generate-beta ((sg Shuffle-Generator) a b)
  (let ((x (generate-gamma sg a)))
    (/ x (+ x (generate-gamma sg b)))))

;;;--------------------------------------------------------------

(defmethod generate-chi-square ((sg Shuffle-Generator) nu)
  (let ((nu/2 (truncate nu 2))
	(temp (if (oddp nu)
		  (expt (generate-standard-gaussian sg) 2)
		  0.0))
	(chi 1.0))
    (when (zerop nu/2) (return-from generate-chi-square temp))
    (dotimes (i nu/2) (setf chi (* chi (generate-standard-uniform sg))))
    (+ temp (- (* 2 (log chi))))))

;;;--------------------------------------------------------------

(defmethod generate-t ((sg Shuffle-Generator) nu)
  (/ (generate-standard-gaussian sg)
     (sqrt (/ (generate-chi-square sg nu) nu))))

;;;--------------------------------------------------------------

(defmethod generate-f ((sg Shuffle-Generator) nu1 nu2)
  (/ (/ (generate-chi-square sg nu1) nu1)
     (/ (generate-chi-square sg nu2) nu2)))

