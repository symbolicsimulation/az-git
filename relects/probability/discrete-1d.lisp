;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Probability-*-
;;;
;;; Copyright 1986. John C. Michalak. All Rights Reserved. 
;;; Copyright 1987. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================================

(in-package 'Probability)

(export '(Discrete-Probability
	   Binomial-Probability
	   Poisson-Probability))
	   
;;;=======================================================================
;;; an abstract class

(defclass Discrete-Probability (Probability) 
  ((initial-probability
     :accessor initial-probability
     :type Single-Float
     :initform 1.0)))

;;;--------------------------------------------------------------
;;;  see Rubinstein, R., Simulation and the Monte Carlo Method,
;;;      Wiley, 1981, Algorithm IT-2, p. 96-97.

(defmethod quantile ((pm Discrete-Probability) p)
  
  ;; This assumes that the <initial-probability> slot is properly initialized
  ;; and that a <relative-probability> method is defined by each discrete
  ;; Distribution class. <initial-probability> is Rubinstein's P0 and
  ;; <relative-probability> is Ak+1.
  
  ;; Note: this assumes the distribution puts mass only on the
  ;; non-negative integers.

  (let* ((k 0)
	 (probability (initial-probability pm))
	 (total-probability probability))
    (loop (when (> total-probability p) (return k))
	  (incf k)
	  (bm:mulf probability (relative-probability pm k))
	  (incf total-probability probability))))

;;;=======================================================================

(defclass Binomial-Probability (Discrete-Probability)
  ;; slots
  ((n
     :accessor binomial-n
     :initarg :n
     :type Fixnum)
   (p
     :accessor binomial-p
     :initarg :p
     :type Single-Float)))

;;;--------------------------------------------------------------

(defmethod shared-initialize :after ((pm Binomial-Probability)
				     slot-names
				     &rest
				     initargs)
	   
  (declare (ignore slot-names initargs))
  (setf (initial-probability pm) (expt (- 1.0 (binomial-p pm))
				       (binomial-n pm))))
  
;;;--------------------------------------------------------------

(defmethod percentile ((pm Binomial-Probability) (x number))
  (bm:percentile-binomial x (binomial-n pm) (binomial-p pm)))
  
;;;--------------------------------------------------------------

#||
This is terrible numerically; but it's what Rubinstein uses.
Maybe we shouldn't base this stuff on Rubinstein?

(defmethod relative-probability ((pm Binomial-Probability) k)
  (let ((p (binomial-p pm)))
    (/ (* (- (binomial-n pm) k)
	  p)
       (* (+ k 1)
	  (- 1 p)))))
||#
  
;;;=======================================================================
  
(defclass Poisson-Probability (Discrete-Probability)       
  ((rate
     :accessor poisson-rate
     :initarg :rate
     :type Number)))
  
;;;--------------------------------------------------------------
  
(defmethod shared-initialize :after ((pm Poisson-Probability)
				     slot-names
				     &rest
				     initargs)
  (declare (ignore slot-names initargs))
  (setf (initial-probability pm) (exp (- (poisson-rate pm)))))
  
;;;--------------------------------------------------------------
  
(defmethod relative-probability ((pm Poisson-Probability) k)
  (/ (poisson-rate pm) (float (+ k 1))))

