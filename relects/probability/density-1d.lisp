;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Probability-*-
;;;
;;; Copyright 1986. John C. Michalak. All Rights Reserved. 
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================================

(in-package 'Probability)

(export '(Probability-with-Density
	   Uniform-Probability
	   Exponential-Probability
	   Gaussian-Probability
	   Cauchy-Probability
	   Logistic-Probability
	   Log-Gaussian-Probability
	   Gamma-Probability
	   Beta-Probability
	   Chi-Square-Probability
	   T-Probability
	   F-Probability))
	   
;;;=======================================================================

(defclass Probability-with-Density (Probability) ())

;;;--------------------------------------------------------------
;;; additional protocol for densities:

(defmethod density ((pm Probability-with-Density) (x Number))
  (error "Not yet defined"))

;;;=======================================================================

(defclass Uniform-Probability (Probability-with-Density)
  ;; slots
  ((minimum
     :accessor minimum
     :initarg :minimim
     :type Number)
   (maximum
     :accessor maximum
     :initarg :maximim
     :type Number))
  (:default-initargs
    :minimum 0.0
    :maximum 1.0))

;;;--------------------------------------------------------------

(defmethod sample ((self Uniform-Probability))
  (generate-uniform (generator self)
		    (minimum self)
		    (- (maximum self) (minimum self))))

;;;--------------------------------------------------------------

(defmethod mean ((self Uniform-Probability))
  (/ (+ (minimum self) (maximum self))
     2.0))

;;;--------------------------------------------------------------

(defmethod standard-deviation ((self Uniform-Probability))
  (/ (- (maximum self) (minimum self))
     2.0 (sqrt 3.0)))

;;;--------------------------------------------------------------

(defmethod variance ((self Uniform-Probability))
  (/ (bm:sq (- (maximum self) (minimum self)))
     12.0))

;;;--------------------------------------------------------------

(defmethod density ((self Uniform-Probability) x)
  (let* ((min (minimum self))
	 (max (maximum self))
	 (ran (- max min)))
    (if (<= min x max) (/ 1.0 ran) 0.0)))
   
;;;--------------------------------------------------------------

(defmethod percentile ((self Uniform-Probability) x)
  (declare (type Number x))
  (check-type x Number)
  (let* ((min (minimum self))
	 (max (maximum self))
	 (ran (- max min)))
    (cond
      ((<=     x min) 0.0)
      ((<  min x max) (/ (- x min) ran))
      ((<= max x    ) 1.0))))
 
;;;--------------------------------------------------------------

(defmethod quantile ((self Uniform-Probability) p)
  (declare (type Number p))
  (check-type p (Number 0.0 1.0)) ;;(assert (<= 0.0 p 1.0))

  (let ((min (minimum self))
	(ran (- (maximum self) (minimum self))))
    (+ min (* ran p))))
 
;;;=======================================================================

(defclass Exponential-Probability (Probability-with-Density)
  ((exponential-lambda
     :accessor exponential-lambda
     :initarg :lambda
     :type Single-Float))
  (:default-initargs :lambda 1.0))

;;;--------------------------------------------------------------

(defmethod sample ((self Exponential-Probability))
  (generate-exponential (generator self) (exponential-lambda self)))
 
;;;--------------------------------------------------------------

(defmethod mean ((self Exponential-Probability))
  (/ (exponential-lambda self)))
 
;;;--------------------------------------------------------------

(defmethod standard-deviation ((self Exponential-Probability))
  (/ (exponential-lambda self)))
 
;;;--------------------------------------------------------------

(defmethod variance ((self Exponential-Probability))
  (/ 1.0 (exponential-lambda self) (exponential-lambda self)))
 
;;;--------------------------------------------------------------

(defmethod density ((self Exponential-Probability) x)
  (let ((l (exponential-lambda self)))
    (* l (exp (- (* l x))))))
 
;;;--------------------------------------------------------------

(defmethod percentile ((self Exponential-Probability) x)
    (- 1.0 (exp (- (* (exponential-lambda self) x)))))
 
;;;--------------------------------------------------------------

(defmethod quantile ((self Exponential-Probability) p)
  ;; (assert (and (<= 0.0 p) (< p 1.0)))
  ;; this will give an error when (= p 1.0):
  (/ (- (log (- 1.0 p)))
     (exponential-lambda self)))
 
;;;=======================================================================

(defclass Gaussian-Probability (Probability-with-Density)
  ((mean
     :accessor mean
     :initarg :mean
     :type Single-Float)
   (standard-deviation
     :accessor standard-deviation
     :initarg :standard-deviation
     :type Single-Float))
  (:default-initargs
    :mean 0.0
    :standard-deviation 1.0))

;;;--------------------------------------------------------------

(defmethod variance ((self Gaussian-Probability))
  (bm:sq (standard-deviation self)))

;;;--------------------------------------------------------------

(defmethod sample ((self Gaussian-Probability))
  (generate-gaussian (generator self) (mean self) (standard-deviation self)))

;;;----------------------------------------------------------------

(defmethod density ((self Gaussian-Probability) x)

  (let ((std-x (/ (- x (mean self))
		  (standard-deviation self))))
    (bm:density-standard-gaussian std-x)))

;;;;----------------------------------------------------------------

(defmethod percentile ((self Gaussian-Probability) x)
  (let ((std-x (/ (- x (mean self))
		  (standard-deviation self))))
    (bm:percentile-standard-gaussian std-x)))

;;;=================================================================
;;; These 3 aren't done yet (obviously).

(defclass Cauchy-Probability (Probability-with-Density) ())

(defclass Logistic-Probability (Probability-with-Density) ())

(defclass Log-Gaussian-Probability (Probability-with-Density) ())

;;;=======================================================================

(defclass Gamma-Probability (Probability-with-Density)
  ((a
     :accessor a
     :initarg :a
     :type Single-Float)
   (b
     :accessor b
     :initarg :b
     :type Single-Float))
  (:default-initargs
    :a 0.0
    :b 1.0))

;;;--------------------------------------------------------------

(defmethod sample ((self Gamma-Probability))
  (* (b self) (generate-gamma (generator self) (a self))))

;;;--------------------------------------------------------------

(defmethod mean ((self Gamma-Probability))
  (* (b self) (+ 1.0 (a self))))

;;;--------------------------------------------------------------

(defmethod standard-deviation ((self Gamma-Probability))
  (* (b self) (sqrt (+ 1.0 (a self)))))

;;;--------------------------------------------------------------

(defmethod variance ((self Gamma-Probability))
  (let ((b (b self)))
    (* b b (+ 1.0 (a self)))))

;;;----------------------------------------------------------------
;;; this is probably a terrible way to do it (jam, 10-87).

(defmethod density ((self Gamma-Probability) x)
  (setf x (/ x (b self)))
  (if (< x 0.0)
      0.0
      (let ((aa (a self)))
	(exp (- (* (- aa 1) (log x))
		x
		(bm:log-gamma aa))))))

;;;----------------------------------------------------------------

(defmethod percentile ((self Gamma-Probability) x)
  (setf x (/ x (b self)))
  (bm:percentile-gamma x (a self)))

;;;=======================================================================

(defclass Beta-Probability (Gamma-Probability) ())

;;;;--------------------------------------------------------------

(defmethod sample ((self Beta-Probability ))
  (generate-beta (generator self) (a self) (b self)))

;;;--------------------------------------------------------------

(defmethod mean ((self Beta-Probability))
  (let ((a (a self)))
    (/ (+ a 1.0)
       (+ a (b self) 2.0))))

;;;--------------------------------------------------------------

(defmethod standard-deviation ((self Beta-Probability))
  (sqrt (variance self)))

;;;--------------------------------------------------------------

(defmethod variance ((self Beta-Probability))
  (let* ((a (a self))
	 (b (b self))
	 (a+b+2 (+ a b 2.0)))
    (/ (* (+ a 1.0) (+ b 1.0))
       (* a+b+2 a+b+2 (+ a b 3.0)))))

;;;--------------------------------------------------------------

(defmethod percentile ((self Beta-Probability) x)

  (bm:percentile-beta x (a self) (b self)))

;;;=======================================================================

(defclass Chi-Square-Probability (Probability-with-Density)
  ((nu
     :accessor nu
     :initarg :nu
     :type Fixnum))
  (:default-initargs :nu 1))

;;;;--------------------------------------------------------------

(defmethod sample ((self Chi-Square-Probability))
  (generate-chi-square (generator self) (nu self)))

;;;--------------------------------------------------------------

(defmethod mean ((self Chi-Square-Probability))
  (nu self))

;;;--------------------------------------------------------------

(defmethod standard-deviation ((self Chi-Square-Probability))
  (sqrt (* 2 (nu self))))

;;;--------------------------------------------------------------

(defmethod variance ((self Chi-Square-Probability))
  (* 2 (nu self)))

;;;--------------------------------------------------------------

(defmethod percentile ((self Chi-Square-Probability) x)
  (bm:percentile-chi-square x (nu self)))

;;;=======================================================================

(defclass T-Probability (Chi-Square-Probability) ())

;;;;--------------------------------------------------------------

(defmethod sample ((self T-Probability))
  (generate-t (generator self) (nu self)))

;;;=======================================================================

(defclass F-Probability (Probability-with-Density)
  ((nu0
     :accessor nu0
     :initarg :nu0
     :type Fixnum)
   (nu1
     :accessor nu1
     :initarg :nu1
     :type Fixnum))
  (:default-initargs
    :nu0 1
    :nu1 1))

;;;--------------------------------------------------------------

(defmethod sample ((self F-Probability))
  (generate-F (generator self) (nu0 self) (nu1 self)))

;;;--------------------------------------------------------------

(defmethod percentile ((self F-Probability) x)
  (bm:percentile-F x (nu0 self) (nu1 self))) 

