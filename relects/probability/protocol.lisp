;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Probability -*-
;;;
;;; Copyright 1986. John C. Michalak. All Rights Reserved. 
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;===============================================================

(in-package 'Probability)

(export '(Probability
	   sample sample-sequence
	   mean standard-deviation variance
	   moment
	   percentile
	   quantile median))

;;;===============================================================
;;; Probability Measures
;;;===============================================================
;;;
;;; Protocol methods:  sample, mean, standard-deviation, variance,
;;; moment, percentile, quantiles, median, ...?

(defclass Probability () ;; an abstract class
  ((generator
     :type Shuffle-Generator
     :reader generator
     :initarg :generator)))

;;;--------------------------------------------------------------

(defmethod shared-initialize :after ((pm Probability) 
				     slot-names
				     &rest
				     initargs)
  (declare (ignore slot-names))
  (unless (slot-boundp pm 'generator)
    (setf (slot-value pm 'generator)
	  (apply #'make-instance 'Shuffle-Generator initargs))))

;;;--------------------------------------------------------------
;;; The default <sample> method uses the inverse transform (quantile).

(defmethod sample ((pm Probability))
  (quantile pm (generate-standard-uniform (generator pm))))

;;;==============================================================
;;; Sample-Sequence:
;;; 
;;; it's useful to be able to fill a sequence with independent samples
;;;--------------------------------------------------------------

(defmethod sample-sequence ((pm Probability) (n Number))
  (sample-sequence pm (make-array n)))

(defmethod sample-sequence ((pm Probability) (v Vector))
  (dotimes (i (length v)) (setf (aref v i) (sample pm)))
  v)

(defmethod sample-sequence ((pm Probability) (l List))
  (mapl #'(lambda (sub-l) (setf (first sub-l) (sample pm))) l)
  l)

;;;==============================================================
;;; Moments, etc.
;;;--------------------------------------------------------------

(defmethod mean ((pm Probability)) (moment pm 1))

;;;--------------------------------------------------------------
;;; The default <standard-deviation> method assumes a specialized
;;; <variance> method is defined.

(defmethod standard-deviation ((pm Probability)) (sqrt (variance pm)))

;;;--------------------------------------------------------------
;;; The default <variance> method assumes a specialized
;;; <standard-deviation> method is defined.

(defmethod variance ((pm Probability))
  (- (moment pm 2) (bm:sq (moment pm 1))))

;;;--------------------------------------------------------------

(defmethod moment ((pm Probability) (k Integer)) (error "Not yet defined"))

;;;--------------------------------------------------------------

(defmethod percentile ((pm Probability) (x Number)) (error "Not yet defined"))

;;;--------------------------------------------------------------

(defmethod quantile ((pm Probability) (x Number)) (error "Not yet defined"))

;;;--------------------------------------------------------------

(defmethod median ((pm Probability)) (quantile pm 0.5))

