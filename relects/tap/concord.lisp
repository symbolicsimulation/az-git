;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 1)
		     (speed 1)
		     (compilation-speed 0))) 

;;;============================================================

(defclass Concord ()
     ((subject
	:type T
	:accessor subject
	:initarg :subject)
      (presentations
	:type List ;; (List Presentation)
	:accessor presentations
	:initarg :presentations)
      (function-name
	:type Symbol
	:accessor function-name
	:initarg :function-name)
      (function-lambda-list
	:type List
	:accessor function-lambda-list
	:initarg :function-lambda-list)
      (update-function
	:type Function
	:accessor update-function
	:initarg :update-function)))

(defclass Eager-Concord (Concord) ())
(defclass Lazy-Concord (Concord) ())

;;;============================================================

(defun make-concord (&rest options)
  (apply #'make-instance 'Eager-Concord options))

;;;============================================================

(defgeneric activate-concord (concord)
  (declare (type Concord concord)))

(defgeneric deactivate-concord (concord)
  (declare (type Concord concord)))

