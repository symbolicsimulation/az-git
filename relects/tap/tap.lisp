;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Tap -*-
;;;
;;; Copyright 1990. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Tap)

(proclaim '(optimize (safety 3)
		     (space 1)
		     (speed 1)
		     (compilation-speed 0)))

(export '(Tap-Dispatcher
	   make-tap-dispatcher
	   add-tap remove-tap remove-all-taps))
	   
;;;============================================================

(defclass Tap-Dispatcher (Standard-Object)
     ((tapped-function-name
	:type Symbol
	:reader tapped-function-name
	:initarg :tapped-function-name)
      (lambda-list
	:type List
	:reader lambda-list
	:initarg :lambda-list)
      (update-function-name
	:type Function
	:reader update-function-name
	:initarg :update-function-name)
      (listener-table
	:type Hash-Table
	:reader listener-table
	:initform (make-hash-table :test #'eql)
	)))

;;;------------------------------------------------------------

(defparameter *tap-dispatcher-table* (make-hash-table :test #'eq))

(defun tap-dispatcher (function-name)
  (gethash function-name *tap-dispatcher-table*))

(defsetf tap-dispatcher (function-name) (dispatcher)
  `(setf (gethash ,function-name *tap-dispatcher-table*) ,dispatcher))

(proclaim '(Inline listeners))

(defun listeners (subject listener-table)
  (declare (Inline gethash)
	   (type Hash-Table listener-table))
  (gethash subject listener-table ()))

(defsetf listeners (subject listener-table) (l)
  `(locally
     (declare (Inline gethash)
	      (type Hash-Table ,listener-table))
     (setf (gethash ,subject ,listener-table) ,l)))

(defun add-tap (dispatcher subject listener)
  (tools:type-check Tap-Dispatcher dispatcher)
  (pushnew listener (listeners subject (listener-table dispatcher))))

(defun remove-tap (dispatcher subject listener)
  (tools:type-check Tap-Dispatcher dispatcher)
  (tools:deletef listener (listeners subject (listener-table dispatcher))))

(defun remove-all-taps (dispatcher subject)
  (setf (listeners subject (listener-table dispatcher)) ()))

;;;------------------------------------------------------------

(defun make-tap-dispatcher (tapped-function-name
			    lambda-list
			    update-function-name)
  (tools:type-check Symbol tapped-function-name update-function-name)
  (tools:type-check List lambda-list)
  (let* ((dispatcher
	   (make-instance 'Tap-Dispatcher
			  :tapped-function-name tapped-function-name
			  :lambda-list lambda-list
			  :update-function-name update-function-name))
	 (subject-name (first lambda-list))
	 (listener-table (listener-table dispatcher))
	 (old-def (symbol-function tapped-function-name))
	 (lambda-expr
	   `(lambda ,lambda-list
	      (multiple-value-prog1
		,(function-call old-def lambda-list)
		(dolist (listener (listeners ,subject-name ,listener-table))
		  (,update-function-name listener ,@lambda-list))))))
    (print lambda-expr)
    (install-advice tapped-function-name (compile nil lambda-expr))
    (setf (tap-dispatcher tapped-function-name) dispatcher)
    dispatcher))

