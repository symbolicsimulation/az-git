;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Tap -*-
;;;
;;; Copyright 1990. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Tap)

(proclaim '(optimize (safety 3)
		     (space 1)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================
;;; Testing taps
;;;============================================================

(progn 
  (remove-advice 'set-g)
  (setf (naive-definition 'set-g) nil)
  (setf (wise-definition 'set-g) nil))

(make-tap-dispatcher 'set-g '(thing g) 'g-changed)

(add-tap (tap-dispatcher 'set-g) *thing0* *thing1*)

(describe (tap-dispatcher 'set-g))
(describe (listener-table (tap-dispatcher 'set-g)))

(defparameter counter 0)
(progn
  (set-g *thing0* (incf counter))
  (g *thing1*))

(time (do-eager-g *thing0* *thing1* 1000000))

;;;============================================================
;;; Testing advice
;;;============================================================

(Progn (eager-b)
       (eager-c)
       (eager-d)
       (eager-e)
       (eager-f))

(progn (print "raw version")
       (fresh-line)
       (time (do-eager-a *thing0* *thing1* 1000000))
       (print "funcall lexical variable")
       (fresh-line)
       (time (do-eager-b *thing0* *thing1* 1000000))
       (print "funcall symbol-plist")
       (fresh-line)
       (time (do-eager-c *thing0* *thing1* 1000000))
       (print "macro expand to funcall lexical variable")
       (fresh-line)
       (time (do-eager-d *thing0* *thing1* 1000000))
       (print "macro expand to standard function call of new name")
       (fresh-line)
       (time (do-eager-e *thing0* *thing1* 1000000))
       (print "macroexpand direct call to old def")
       (fresh-line)
       (time (do-eager-f *thing0* *thing1* 1000000)))

