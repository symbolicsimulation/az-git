;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Tap -*-
;;;
;;; Copyright 1990. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Tap)

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))

(export '(advise remove-advice
		 remove-all-advice))

;;;============================================================

(proclaim '(Inline naive-definition wise-definition))
(defun naive-definition (name)
  (get name 'naive-definition))

(defsetf naive-definition (name) (def)
  `(setf (get ,name 'naive-definition) ,def))

(defun wise-definition (name)
  (get name 'wise-definition))

(defsetf wise-definition (name) (def)
  `(setf (get ,name 'wise-definition) ,def))

(defparameter *advised-functions* ())

(defun install-advice (name new-definition)
  (declare (special *advised-functions*))
  (cond
    ((typep (symbol-function name) 'Standard-Generic-Function)
      (error "Can't advise generic functions (yet)."))
    ((or (naive-definition name) (wise-definition name))
     (error "~a is already advised" name))
    (t 
      (tools:atomic
	(pushnew name *advised-functions*)
	(setf (naive-definition name) (symbol-function name))
	(setf (wise-definition name) new-definition)
	(setf (symbol-function name) (wise-definition name))))))

(defun remove-advice (name)
  (if (naive-definition name)
      (tools:atomic
	(setf (symbol-function name) (naive-definition name)))
      ;; else
      (warn "~a hasn't been advised." name)))

(defun remove-all-advice ()
  (declare (special *advised-functions*))
  (dolist (name *advised-functions*)
    (remove-advice name))
  (setf *advised-functions* ()))

;;;============================================================

(defun coerce-to-keyword (symbol) (intern (string symbol) :Keyword))

(defun lambda-list-keyword? (symbol) (member symbol lambda-list-keywords))

(defun required-args (lambda-list)
  (subseq lambda-list 0 (position-if #'lambda-list-keyword? lambda-list)))

(defun optional-arg-name (optional-arg-expression)
  (if (symbolp optional-arg-expression)
      optional-arg-expression
      (first optional-arg-expression)))

(defun optional-args (lambda-list)
  (let ((start (position '&optional lambda-list)))
    (when start
      (map 'List
	   #'optional-arg-name
	   (subseq lambda-list (+ 1 start)
		   (position-if #'lambda-list-keyword? lambda-list
				:start (+ 1 start)))))))

(defun translate-keyword-expression (key-expr)
  (cond
    ((symbolp key-expr)
     (list (coerce-to-keyword key-expr) key-expr))
    ((listp key-expr)
     (let ((key (first key-expr)))
       (cond
	 ((symbolp key)
	  (list (coerce-to-keyword key) key))
	 ((listp key)
	  (list (coerce-to-keyword (first key)) (second key)))
	 (t (error "Illegal keyword expression: ~a" key-expr)))))
    (t (error "Illegal keyword expression: ~a" key-expr))))

(defun keyword-and-rest-args (lambda-list)
  (let ((rest-pos (position '&rest lambda-list))
	(keyword-start (position '&key lambda-list)))
    (if rest-pos
	;; then just return the name of the &rest argument
	(nth (+ 1 rest-pos) lambda-list)
	;; else we have to parse the keywords
	(when keyword-start
	  `(list 
	     ,@(mapcan #'translate-keyword-expression
		       (subseq lambda-list
			       (+ 1 keyword-start)
			       (position-if #'lambda-list-keyword? lambda-list
					    :start (+ 1 keyword-start)))))))))

(defun function-call (name lambda-list)
  (if (notany #'lambda-list-keyword? lambda-list)
      `(funcall ,name ,@lambda-list)
      ;; else
      `(apply ,name
	      ,@(required-args lambda-list)
	      ,@(optional-args lambda-list)
	      ,(keyword-and-rest-args lambda-list))))

(defmacro advise (name lambda-list
		  &key
		  (before-code ())
		  (after-code ()))
  (let ((old-def (gentemp "naive-")))
    `(let* ((,old-def (symbol-function ',name)))
       (install-advice ',name
		       #'(lambda ,lambda-list 
			   ,before-code
			   (multiple-value-prog1
			     ,(function-call old-def lambda-list)
			     ,after-code))))))

;;; experimental alternatives:

(defmacro advise0 (name lambda-list
		   &key
		   (before-code ())
		   (after-code ()))
  (let ((new-name (gentemp "original-")))
    `(progn
       (setf (symbol-function ',new-name) (symbol-function ',name))
       (install-advice ',name
		       #'(lambda ,lambda-list
			   ,before-code
			   (multiple-value-prog1
			     (,new-name ,@lambda-list)
			     ,after-code))))))

(defmacro advise1 (name lambda-list
		   &key
		   (before-code ())
		   (after-code ()))
  (let ((old-def (symbol-function name)))
    `(install-advice ',name
		     #'(lambda ,lambda-list
			 ,before-code
			 (multiple-value-prog1
			   (funcall ,old-def ,@lambda-list)
			   ,after-code)))))


