;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Tap -*-
;;;
;;; Copyright 1990. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Tap)

(proclaim '(optimize (safety 0)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0)))

;;;============================================================
;;; Testing advice
;;;============================================================

(defun foo (x y) (values (+ x y) (- x y)))

(defun advise-foo ()
  (install-advice 'foo
		  #'(lambda (x y)
		      (print (cons x y))
		      (multiple-value-prog1
			(funcall (naive-definition 'foo) x y)
			(print (cons (+ x y) (- x y)))))))

;;;============================================================

(defstruct (Thing
	     (:conc-name "")
	     (:constructor make-thing)
	     (:print-function print-thing))
  (prev nil :type (or Null Thing))
  (next nil :type (or Null Thing))
  (a 0 :type Fixnum)
  (b 0 :type Fixnum)
  (c 0 :type Fixnum)
  (d 0 :type Fixnum)
  (e 0 :type Fixnum)
  (f 0 :type Fixnum)
  (g 0 :type Fixnum)
  (h 0 :type Fixnum)
  (i 0 :type Fixnum)
  (j 0 :type Fixnum))

(defun print-thing (thing stream depth)
  (declare (ignore depth))
  (pcl:printing-random-thing (thing stream)
    (format stream
	    "Thing prev:~a, next:~a, slots:~a ~a ~a ~a ~a ~a ~a ~a ~a ~a"
	    (short-thing-string (prev thing))
	    (short-thing-string (next thing))
	    (a thing) (b thing) (c thing) (d thing) (e thing)
	    (f thing) (g thing) (h thing) (i thing) (j thing))))

(defun short-thing-string (thing)
  (with-output-to-string (stream)
    (if (null thing)
	(format stream "Nothing")
	;; else
	(pcl:printing-random-thing (thing stream)
	  (format stream "Thing")))))

(defparameter *thing0* (make-thing))
(defparameter *thing1* (make-thing))
(setf (next *thing0*) *thing1*)
(setf (prev *thing1*) *thing0*)

(defun set-a (thing a)
  (declare (type Thing thing)
	   (type Fixnum a))
  (setf (a thing) a)) 
(defun do-eager-a (thing0 thing1 n)
  (declare (type Thing thing0 thing1))
  (dotimes (i n)
    (set-a thing0 i)
    (set-a thing1 i)))


(defun set-b (thing b)
  (declare (type Thing thing)
	   (type Fixnum b))
  (setf (b thing) b))
(defun eager-b ()
  (let ((old-def (symbol-function 'set-b)))
    (install-advice 'set-b
		    #'(lambda (thing b)
			(declare (type Thing thing)
				 (type Fixnum b))
			(multiple-value-prog1
			  (funcall old-def thing b)
			  (setf (b (the Thing (next thing))) b))))))
(defun do-eager-b (thing0 thing1 n)
  (declare (type Thing thing0 thing1))
  (setf (next thing0) thing1)
  (dotimes (i n)
    (set-b thing0 i)))

(defun set-c (thing c)
  (declare (type Thing thing)
	   (type Fixnum c))
  (setf (c thing) c))
(defun eager-c ()
  (install-advice 'set-c
		  #'(lambda (thing c)
		      (declare (type Thing thing)
			       (type Fixnum c))
		      (multiple-value-prog1
			(funcall (naive-definition 'set-c) thing c)
			(setf (c (the Thing (next thing))) c)))))
(defun do-eager-c (thing0 thing1 n)
  (declare (type Thing thing0 thing1))
  (setf (next thing0) thing1)
  (dotimes (i n)
    (set-c thing0 i)))

(defun set-d (thing d)
  (declare (type Thing thing)
	   (type Fixnum d))
  (setf (d thing) d))

(defun eager-d ()
  (advise set-d (thing d)
	  :before-code (declare (type Thing thing)
				(type Fixnum d))
    :after-code (setf (d (the Thing (next thing))) d)))

(defun do-eager-d (thing0 thing1 n)
  (declare (type Thing thing0 thing1))
  (setf (next thing0) thing1)
  (dotimes (i n)
    (set-d thing0 i)))
 
(defun set-e (thing e)
  (declare (type Thing thing)
	   (type Fixnum e))
  (setf (e thing) e))

(defun eager-e ()
  (advise0 set-e (thing e)
	   :before-code (declare (type Thing thing)
				 (type Fixnum e))
	   :after-code (setf (e (the Thing (next thing))) e)))
(defun do-eager-e (thing0 thing1 n)
  (declare (type Thing thing0 thing1))
  (setf (next thing0) thing1)
  (dotimes (i n)
    (set-e thing0 i)))

(defun set-f (thing f)
  (declare (type Thing thing)
	   (type Fixnum f))
  (setf (f thing) f))

(defun eager-f ()
  (advise0 set-f (thing f)
	   :before-code (declare (type Thing thing)
				 (type Fixnum f))
	   :after-code (setf (f (the Thing (next thing))) f)))

(defun do-eager-f (thing0 thing1 n)
  (declare (type Thing thing0 thing1))
  (setf (next thing0) thing1)
  (dotimes (i n)
    (set-f thing0 i)))

;;;============================================================
;;; Testing Taps
;;;============================================================

(defun set-g (thing g)
  (declare (type Thing thing)
	   (type Fixnum g))
  (setf (g thing) g))

(defun g-changed (listener subject g)
  (declare (ignore subject)
	   (type Thing listener subject)
	   (type Fixnum g))
  (setf (g listener) g))

(defun do-eager-g (thing0 thing1 n)
  (declare (type Thing thing0 thing1))
  (setf (next thing0) thing1)
  (dotimes (i n)
    (set-g thing0 i)))

