;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: User; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================

(load  (truename
       (pathname
	(concatenate 'String
	  (namestring 
	   (make-pathname
	    :directory (pathname-directory excl::*source-pathname*)))
	  "../files.lisp"))))

(load-all *tools-directory* *tools-files*)

(load-all *basic-math-directory* *basic-math-files*) 

(load-all *geometry-directory* *geometry-files*)
(load-all *slate-directory* *slate-files*)
(load-all *chart-directory* *chart-files*)

(load-all *cactus-directory* *cactus-files*)



