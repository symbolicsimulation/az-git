;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;;	       Triangular decompositions
;;;=======================================================
;;;
;;; Zeros a partial column (from i.0+1 below i.n) of a Matrix by
;;; multiplying on the left with a Gauss Mapping.  Returns the
;;; Gauss Mapping.

(defmethod zero-col-with-gauss! ((t0 Matrix-Super) 
				 &optional
				 (j (coord-start (domain t0)))
				 (i.0 (coord-start (codomain t0)))
				 (i.n (coord-end (codomain t0)))
				 &key result)
  (with-borrowed-element (v (codomain t0))
    (setf result (make-gauss-for
		  (codomain t0) (col t0 j :result v) i.0 i.n :result result))
    (compose result t0 :result t0)
    result))

;;;-------------------------------------------------------
;;; Zeros a partial column (from i.0+1 below i.n) of a Matrix-Super by
;;; multiplying on the left with a Householder Mapping.  Returns
;;; the Householder Mapping.

(defmethod zero-col-with-householder! ((t0 Matrix-Super)
				       &optional
				       (j (coord-start (domain t0)))
				       (i.0 (coord-start (codomain t0)))
				       (i.n (coord-end (codomain t0)))
				       &key result)
  (with-borrowed-element (v (codomain t0))
    (setf result (make-householder
		  (codomain t0) (col t0 j :result v) i.0 i.n :result result))
    (compose result t0 :result t0)
    result))

;;;-------------------------------------------------------
;;; Zeros a partial row (from j.0+1 below j.n) of a Matrix-Super by
;;; (implicitly) multiplying on the right with a Householder
;;; Mapping.  Returns the Householder Mapping.

;;; straightforward version:

(defmethod zero-row-with-householder! ((t0 Matrix-Super)
				       &optional
				       (i (coord-start (codomain t0)))
				       (j.0 (coord-start (domain t0)))
				       (j.n (coord-end (domain t0)))
				       &key result)
  (with-borrowed-element (v (domain t0))
    (setf result (make-householder
		  (domain t0) (row t0 i :result v) j.0 j.n :result result))
    (compose t0 result :result t0)
    result))

;;; "fast" version: we know what the answer is going to be for the
;;; row we are zeroing, so we can skip one composition with the householder:

;;;(defmethod zero-row-with-householder! ((t0 Matrix-Super)
;;;				       &optional
;;;				       (i (coord-start (codomain t0)))
;;;				       (j.0 (coord-start (domain t0)))
;;;				       (j.n (coord-end (domain t0)))
;;;				       &key result)
;;;  (with-borrowed-element (v (domain t0))
;;;    (let ((2d (2d-array t0)))
;;;      (setf h (make-householder (domain t0) (row t0 i :result v) j.0 j.n h))
;;;      (bm:v<-x! 2d 0.0d0 :start (+ i 1) :end j.n :v-type :row :on i)
;;;      (setf (aref 2d id j.0) (leading-elt h))
;;;      (incf (coord-start (codomain t0)))
;;;      (compose t0 h :result t0)
;;;      (decf (coord-start (codomain t0)))      
;;;      h)))

;;; real fast version...don't use any extra vectors

;;;(defmethod zero-row-with-householder! ((t0 Matrix-Super)
;;;				       &optional
;;;				       (i (coord-start (codomain t0)))
;;;				       (j.0 (coord-start (domain t0)))
;;;				       (j.n (coord-end (domain t0)))
;;;				       &key result)
;;;	   (values Householder))
;;;  (let* ((2d (2d-array t0))
;;;	 (len (- j.n j.0))
;;;	 (p (dimension (domain t0)))
;;;	 (norm1 (bm:v-l1-norm 2d :start j.0 :end j.n :v-type :row :on i))
;;;	 x0 norm2 norm)
;;;    (when (= 0.0d0 norm1) ;; a trivial case
;;;      (return-from zero-row-with-householder!
;;;	(the-identity-map (LVector-Space p)))) 
;;;    (when (null result)
;;;      (setf result (make-instance 'Householder))
;;;      (setf (vec result) (make-zero-lvector len)))
;;;    (let ((vt (vec result)))
;;;      (assert (and (typep result 'Householder) (<= len (length 1d))))
;;;      (setf (domain result) (LVector-Space p))
;;;      (setf (codomain result) (LVector-Space p))
;;;      (setf (coord-start result) j.0) (setf (coord-end result) j.n)
;;;      ;; scale by 1/norm1 for better numerical properties
;;;      ;; see Press et al. Ch. 11.2
;;;      ;; (golub&vanloan suggest 1/norm$, alg. 3.3-1)
;;;      (bm:v<-v*x!
;;;        (1d-array vt) 2d (/ 1.0d0 norm1)
;;;        :end0 len :start1 start :end1 end :v-type1 :row :on1 i)
;;;      (setf norm2 (l2-norm2 vt))
;;;      (setf norm (sqrt norm2))
;;;      (setf x0 (aref (1d-array vt) 0))
;;;      (incf (aref (1d-array vt) 0) (* (signum x0) norm))
;;;      (setf (-2/norm2 result) (/ -1.0d0 (+ norm2 (* (abs x0) norm))))
;;;      (bm:v<-x! 2d 0.0d0 :start (+ i 1) :end end :v-type :row :on i)
;;;      (setf (aref 2d i start) (* (- (signum x0)) norm1 norm))
;;;      (incf (coord-start (codomain t0)))
;;;      (compose t0 result :result t0)
;;;      (decf (coord-start (codomain t0)))      
;;;      result)))


;;;=======================================================
;;;       LU decomposition via Gaussian elimination
;;;=======================================================
;;; returns a factoring of the inverse of t0
;;;
;;; see G&vL chap 4.
;;;
;;; intended for square matrices, actually calculates the
;;; pseudo-inverse of the LU decomposition
;;; 
;;; partial pivoting

(defmethod inverse-lu-decompose ((t0 Matrix))
  (inverse-lu-decompose! (copy-to-matrix t0)))

(defmethod inverse-lu-decompose! ((t0 Matrix) &optional gs)
  (declare (ignore gs))
  (when (upper-triangular? t0)
    (return-from inverse-lu-decompose!
      (make-instance 'Linear-Product
	:factors (list (pseudo-inverse! t0)
		       (the-identity-map (codomain t0))))))
  (when (lower-triangular? t0)
    (return-from inverse-lu-decompose!
      (make-instance 'Linear-Product
	:factors (list (the-identity-map (codomain t0))
		       (pseudo-inverse! t0)))))
  (let ((factors ())
	imax g pv l-1
	(s0 (restrict t0)))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (- (coord-end (codomain t0)) 1))
		  (setf imax (index-of-abs-max-in-col s0 i))
		  (when (/= imax i)
		    (setf pv (make-pivot (codomain s0) i imax))
		    (compose pv s0 :result s0)
		    (push (extend! pv) factors))
		  (setf g (zero-col-with-gauss! s0 i i))
		  (push (extend! g) factors)
		  (setf s0 (tail! s0)))
    (delete-all-properties! t0)
    (add-property! t0 'upper-triangular?)
    (setf l-1 (make-instance 'Linear-Product :factors factors))
    (make-instance 'Linear-Product :factors (list (pseudo-inverse t0) l-1))))

;;;-------------------------------------------------------
;;; "slow" version for testing; doesn't use submatrices

(defmethod slow-inverse-lu-decompose ((t0 Matrix))
  (slow-inverse-lu-decompose! (copy-to-matrix t0)))

(defmethod slow-inverse-lu-decompose! ((t0 Matrix) &optional gs)
  (declare (ignore gs))
  (when (upper-triangular? t0)
    (return-from slow-inverse-lu-decompose!
      (make-instance 'Linear-Product
	:factors (list (pseudo-inverse! t0)
		       (the-identity-map (codomain t0))))))
  (when (lower-triangular? t0)
    (return-from slow-inverse-lu-decompose!
      (make-instance 'Linear-Product
	:factors (list (the-identity-map (codomain t0))
		       (pseudo-inverse! t0)))))
  (let* ((factors ())
	 (2d (2d-array t0))
	 (cod (codomain t0))
	 (m (dimension cod))
	 imax pv g l-1)
    (bm:fast-loop (i :from (coord-start cod) :below (- (coord-end cod) 1))
		  (setf imax (bm:v-abs-max-index
			      2d :start i :end m :v-type :col :on i))
		  (when (/= imax i)
		    (setf pv (make-pivot cod i imax))
		    (compose pv t0 :result t0)
		    (push pv factors))
		  (setf g (zero-col-with-gauss! t0 i i))
		  (push g factors))
    (delete-all-properties! t0)
    (add-property! t0 'upper-triangular?)
    (setf l-1 (make-instance 'Linear-Product :factors factors))
    (make-instance 'Linear-Product :factors (list (pseudo-inverse t0) l-1))))


;;;=======================================================
;;;		    QR Decomposition
;;;=======================================================
;;; number of steps to qr triangularize an mxn array

(defun qr-nsteps (m n) (if (> m n) n (- m 1)))

;;;=======================================================
;;; returns a factoring of the inverse of t0

(defmethod inverse-qr-decompose ((t0 Matrix)) 
  (inverse-qr-decompose! (copy-to-matrix t0)))

(defmethod inverse-qr-decompose! ((t0 Matrix)
				  &optional hs)
  (declare (ignore hs))
  (pseudo-inverse! (qr-decompose! t0)))

;;;-------------------------------------------------------
;;; returns a factoring of t0

(defmethod qr-decompose ((t0 Matrix))
  (qr-decompose! (copy-to-matrix t0)))

(defmethod qr-decompose! ((t0 Matrix) &optional hs)
  (declare (ignore hs))
  ;; optimize a special case:
  (when (upper-triangular? t0)
    (return-from qr-decompose!
      (if (strictly-embedding? t0)
	  (restrict-to-square t0)
	;; else
	(make-instance 'Linear-Product
	  :factors (list (the-identity-map (codomain t0)) t0)))))
  (let* ((qs ()) q 
	 (n (dimension (domain t0)))
	 (m (dimension (codomain t0)))
	 (2d (2d-array t0))
	 (s0 (restrict t0))
	 (imax (qr-nsteps m n))
	 (i 0))
    (if (upper-hessenberg? t0) ;; optimize another special case:
	(loop
	  (setf q (make-givens-for
		   (codomain s0) (aref 2d i i) (aref 2d (+ i 1) i) i (+ i 1)))
	  (compose q s0 :result s0)
	  (push (transpose! (extend! q)) qs)
	  (incf i)
	  (when (>= i imax) (return))
	  (setf s0 (tail! s0)))
      ;; else a general matrix 
      (loop
	(setf q (zero-col-with-householder! s0))
	(push (extend! q) qs)
	(incf i)
	(when (>= i imax) (return))
	(setf s0 (tail! s0))))
    ;; restrict the triangle for later efficiency
					;    (when (strictly-embedding? t0)
					;      (setf t0 (restrict-to-square s0))
					;      (push (make-instance 'Block-Embedding
					;			   :domain (domain t0) :codomain (codomain t0))
					;	    qs))
    (delete-all-properties! t0)
    (add-property! t0 'upper-triangular?)
    (setf q (make-instance 'Linear-Product :factors (nreverse qs)))
    (make-instance 'Linear-Product :factors (list q t0))))

;;;-------------------------------------------------------
;;; slow version doesn't use submatrices

(defmethod slow-qr-decompose ((t0 Matrix))
  (slow-qr-decompose! (copy-to-matrix t0)))

(defmethod slow-qr-decompose! ((t0 Matrix) &optional hs)
  (declare (ignore hs))
  ;; optimize a special case:
  (when (upper-triangular? t0)
    (return-from slow-qr-decompose!
      (make-instance 'Linear-Product
	:factors (list (the-identity-map (codomain t0)) t0))))
  (let* ((qs ()) q
	 (m (dimension (codomain t0)))
	 (n (dimension (domain t0)))
	 (2d (2d-array t0)))
    (if (upper-hessenberg? t0) ;; optimize another special case:
	(dotimes (i (qr-nsteps m n))
	  (setf q (make-givens-for
		   (codomain t0) (aref 2d i i) (aref 2d (+ i 1) i)
		   i (+ i 1)))
	  (compose q t0 :result t0)
	  (push (transpose q :result q) qs))
      ;; else a general matrix 
      (dotimes (i (qr-nsteps m n))
	(setf q (zero-col-with-householder! t0 i i))
	(push q qs)))
    (delete-all-properties! t0)
    (add-property! t0 'upper-triangular?)
    (setf q (make-instance 'Linear-Product :factors (nreverse qs)))
    (make-instance 'Linear-Product :factors (list q t0))))


;;;=======================================================
;;;		    LQ Decomposition
;;;=======================================================
;;; returns a factoring of the inverse of t0

(defmethod inverse-lq-decompose ((t0 Matrix)) 
  (inverse-lq-decompose! (az:copy t0)))

(defmethod inverse-lq-decompose! ((t0 Matrix) &optional hs)
  (declare (ignore hs))
  (pseudo-inverse! (lq-decompose t0)) )

;;;-------------------------------------------------------
;;; returns a factoring of t0

(defmethod lq-decompose ((t0 Matrix)) (lq-decompose! (az:copy t0)))


(defmethod lq-decompose! ((t0 Matrix) &optional hs)
  ;; optimize a special case:
  (when (lower-triangular? t0)
    (return-from lq-decompose!
      (if (strictly-projecting? t0)
	  (restrict-to-square t0)
	;; else
	(make-instance 'Linear-Product
	  :factors (list t0 (the-identity-map (domain t0)))))))
  (let* ((qs ()) q
	 (2d (2d-array t0))
	 (dom (domain t0)) (cod (codomain t0))
	 (m (dimension cod)) (n (dimension dom))
	 (s0 (restrict t0))
	 (i 0)
	 (imax (qr-nsteps n m)))
    (if (lower-hessenberg? t0)
	(loop (setf q (make-givens-for
		       (domain s0) (aref 2d i i) (aref 2d i (+ i 1))
		       i (+ i 1)))
	  (compose-outer s0 q :result s0)
	  (push (extend! q) qs)
	  (incf i)
	  (when (>= i imax) (return))
	  (setf s0 (tail! s0)))
      ;; else the default case:
      (loop (setf q (zero-row-with-householder!
		     s0 i (coord-start (domain s0)) (coord-end (domain s0))
		     :result (pop hs)))
	(push (extend! q) qs)
	(incf i)
	(when (>= i imax) (return))
	(setf s0 (tail! s0))))
    ;; restrict the triangle for later efficiency
					;    (when (strictly-projecting? t0)
					;      (let* ((dom (domain t0))
					;	     (sub (block-subspace dom 0 (dimension (codomain t0)))))
					; 	(setf t0 (restrict-to-square s0))
					;	(push (make-instance 'Block-Projection :domain dom :codomain sub)
					;	      qs)))
    (delete-all-properties! t0)
    (add-property! t0 'lower-triangular?)
    (setf q (make-instance 'Linear-Product :factors qs))
    (make-instance 'Linear-Product :factors (list t0 q))))

;;;-------------------------------------------------------
;;; slow version doesn't use submatrices

(defmethod slow-lq-decompose ((t0 Matrix))
  (slow-lq-decompose! (az:copy t0)))

(defmethod slow-lq-decompose! ((t0 Matrix) &optional hs)
  (declare (ignore hs))
  ;; optimize a special case:
  (when (lower-triangular? t0)
    (return-from slow-lq-decompose!
      (make-instance 'Linear-Product
	:factors (list t0 (the-identity-map (domain t0))))))
  (let* ((qs ()) q
	 (2d (2d-array t0))
	 (dom (domain t0)) (cod (codomain t0))
	 (m (dimension cod)) (n (dimension dom)))
    (if (lower-hessenberg? t0)
	(dotimes (i (qr-nsteps n m))
	  (setf q (make-givens-for
		   (domain t0) (aref 2d i i) (aref 2d i (+ i 1))
		   i (+ i 1)))
	  (compose-outer t0 q :result t0)
	  (push q qs))
      ;; else the default case:
      (dotimes (i (qr-nsteps n m))
	(setf q (zero-row-with-householder! t0 i i))
	(push q qs)))
    (delete-all-properties! t0)
    (add-property! t0 'lower-triangular?)
    (setf q (make-instance 'Linear-Product :factors qs))
    (make-instance 'Linear-Product :factors (list t0 q))))



;;;=======================================================
;;;		Cholesky Decompositions
;;;=======================================================
;;; Golub & VanLoan Alg 5.2-1

(defmethod left-triangular-sqrt ((t0 Matrix)
				 &key
				 (result (az:copy t0) result-supplied?))
  (assert (positive-definite? t0))
  (when (and result-supplied? (not (eq t0 result)))
    (az:copy t0 :result result))
  (left-triangular-sqrt! result))

(defmethod left-triangular-sqrt! ((t0 Matrix))
  (assert (positive-definite? t0))
  (bm:cholesky (2d-array t0) (2d-array t0))
  (delete-all-properties! t0)
  (add-property! t0 'lower-triangular?)
  t0)

;;;-------------------------------------------------------

(defmethod right-triangular-sqrt ((t0 Matrix)
				  &key
				  (result (az:copy t0) result-supplied?))
  (assert (positive-definite? t0))
  (when (and result-supplied? (not (eq t0 result)))
    (az:copy t0 :result result))
  (right-triangular-sqrt! result))

(defmethod right-triangular-sqrt! ((t0 Matrix))
  (assert (positive-definite? t0))
  (transpose! (left-triangular-sqrt! t0))
  (when *verify-properties?* (assert (verify-bands? t0)))
  t0)

;;;=======================================================
;;; returns a factoring of t0

(defmethod cholesky-decompose ((t0 Matrix)
			       &key
			       (result (az:copy t0) result-supplied?))
  (assert (positive-definite? t0))
  (when (and result-supplied? (not (eq t0 result)))
    (az:copy t0 :result result))
  (cholesky-decompose! result))

(defmethod cholesky-decompose! ((t0 Matrix))
  (assert (positive-definite? t0))
  (make-instance 'Symmetric-Outer-Product
    :left (left-triangular-sqrt! t0)))


;;;=======================================================
;;;	     Singular Value Decompositions
;;;=======================================================
#||latex

 This is basically the Chan variation on Golub-Reinsch.
 See: 
 {\sc Chan, T.F.,} (1982) 
 {\it An improved algorithm for computing the singular value decompose,}
 {\sf ACM TOMS 8} (1) 72-83.

||#

(defparameter *zero-element-scale* 20.0d0)

(defparameter *relative-efficiency-householder/givens*
    4
  "Assumes slow givens; called 'C' in Chan (eg Table 1).")

(defparameter *svd-crossover-point*
    (/ (+ (/ 7.0d0 3.0d0) *relative-efficiency-householder/givens*)
       *relative-efficiency-householder/givens*)
  "Called 'tau' in Chan; this is case (a), all singular vectors.")

;;; using simpler test loses speed, but it allows us to temporarily avoid
;;; implementing a lower-bidiagonal-svd. The problem is that the non-zero
;;; part of the result of <householder-bidiagonalize> (which produces an
;;; upper bidiagonal matrix) isn't square if (> (dimension (domain t0))
;;; (dimension (codomain t0)))

;;; correct chan qr test:
;;;  (>= (/ (dimension (codomain t0)) (dimension (domain t0)))
;;;      *svd-crossover-point*)

(defmethod chan-lq-case? ((t0 Matrix-Super))
  (> (dimension (domain t0)) (dimension (codomain t0))))

(defmethod chan-qr-case? ((t0 Matrix-Super))
  (< (dimension (domain t0)) (dimension (codomain t0))))
 
;;;-------------------------------------------------------

(defmethod singular-value-decompose ((t0 Matrix))
  (singular-value-decompose! (az:copy t0)))

;;;-------------------------------------------------------
;;; singular-value-decompose! returns a 3 factor product satisfying:
;;; 
;;; (assert (and (orthogonal-columns? (first (factors svd)))
;;; 	         (diagonal? (second (factors svd)))
;;;	         (orthogonal-rows? (third (factors svd)))))
;;;	         

(defmethod singular-value-decompose! ((t0 Matrix-Super))
  (cond
   ;; first, a trivial special case:
   ((diagonal? t0) 
    (make-instance
	'Linear-Product
      :factors
      (list (the-identity-map (codomain t0)) t0 (the-identity-map (domain t0)))))
   ;; the real dirty work:
   ((and (square? t0) (upper-bidiagonal? t0))
    (upper-bidiagonal-matrix-svd! t0))
   ;; cases where we can speed up by preprocessing the matrix:
   ((chan-lq-case? t0)
    (singular-value-decompose! (lq-decompose! t0)))
   ((chan-qr-case? t0)
    (singular-value-decompose! (qr-decompose! t0)))
   ;; the first step in the usual svd algorithms:
   ((square? t0)
    (singular-value-decompose! (householder-bidiagonalize! t0)))
   (t
    (error "Shouldn't be able to get here!"))))

;;;-------------------------------------------------------
;;;
;;; In order to compute an SVD of a product, we need to partition
;;; factors into a left set with orthgonal columns, a non-orthogonal
;;; middle set, and a right set with orthogonal rows. Then we compute an
;;; svd of the middle and re-partition the factors as above, but now
;;; with a diagonal middle.

(defmethod singular-value-decompose! ((t0 Linear-Product))
  (let* ((fs (factors t0))
	 (i0 (position-if-not #'orthogonal-columns? fs))
	 (i1 (position-if #'orthogonal-rows? fs :start i0))
	 (lefts (if (zerop i0)
		    (list (the-identity-map (codomain t0)))
		  (subseq fs 0 i0)))
	 (rights (if (null i1)
		     (list (the-identity-map (domain t0)))
		   (subseq fs i1)))
	 (middles (if (null i1) 
		      (subseq fs i0)
                    (subseq fs i0 i1)))
	 (middle (if (= 1 (length middles))
		     (first middles)
		   (reduce #'compose middles)))
	 (svd (singular-value-decompose! middle))
	 svd-left svd-right)
    (setf svd-left (first (factors svd)))
    (setf svd-right (third (factors svd)))
    (assert (diagonal? (second (factors svd))))
    (setf (factors svd-left) (nconc lefts (factors svd-left)))
    (setf (factors svd-right) (nconc (factors svd-right) rights))
    svd))
 
;;;-------------------------------------------------------

(defun upper-bidiagonal-matrix-svd! (t0)
  (az:type-check Matrix-Super t0) 
  (assert (upper-bidiagonal? t0))
  (assert (square? t0))
  (let* ((cod (codomain t0))
	 (dom (domain t0))
	 (lefts ())
	 (rights ())
	 (ls ())
	 (rs ())
	 (s0 (restrict t0))
	 (maxi (* 10 (dimension (domain t0)))))
    (dotimes (i maxi (error "Too many iterations."))
      (loop
	(setf s0 (restrict! s0 cod dom))
	(add-property! s0 'upper-bidiagonal?)
	(setf s0 (last-bidiagonal-block! s0))
	(when (diagonal? s0) (return))
	(multiple-value-setq (ls s0 rs) (decouple-last-diagonal-zero! s0))
	(when (and (null ls) (null rs)) (return))
	(setf ls (map 'List #'(lambda (l) (extend! l cod)) ls))
	(setf lefts (nconc lefts ls))
	(setf rs (map 'List #'(lambda (r) (extend! r dom)) rs))
	(setf rights (nconc rs rights)))
      (when (diagonal? s0) (return)) ;; we're done.
      (multiple-value-setq (ls s0 rs) (slow-golub-kahan-step! s0))
      (setf ls (map 'List #'(lambda (l) (extend! l cod)) ls))
      (setf rs (map 'List  #'(lambda (r) (extend! r dom)) rs))
      (setf lefts (nconc lefts ls))
      (setf rights (nconc rs rights))
      (when (diagonal? s0) (return)))
    (setf s0 (restrict! s0 cod dom))
    (add-property! s0 'diagonal?)
    (make-instance 'Linear-Product
      :factors
      (list (make-instance 'Linear-Product :factors lefts)
	    s0
	    (make-instance 'Linear-Product :factors rights)))))

;;;=======================================================
#||latex

 Identify the irreducible diagonal blocks of $T$ by zeroing any
 $T_{i,i+1} = T_{i+1,i}$ such that
 $|T_{i,i+1}| \leq \epsilon \[|T_{i,i}| + |T_{i+1,i+1}|\].$

 Return {\tt t0} restricted to its last unreduced bidiagonal block, 
 if there is one. If ther isn't one, then the matrix is actually diagonal, 
 so we can change the class of {\tt t0} to {\Matrix}.
||#
				    
(defun last-bidiagonal-block! (t0)
  (az:type-check Matrix-Super t0)
  (assert (square? t0))
  (assert (upper-bidiagonal? t0))
  (assert (diagonal-block? t0))
  (let ((2d (2d-array t0))
	(ioff (coord-start (codomain t0)))
	(n (dimension (domain t0)))
	start end)
    (setf end
      (loop for i downfrom (- n 1) above 0
	  for j = (+ i ioff)
	  for j-1 = (- j 1)
	  for alpha = (* *zero-element-scale*
			 (+ 1.0d0
			    (abs (aref 2d j j))
			    (abs (aref 2d j-1 j-1))))
	  do
	    (if (bm:small?  (aref 2d j-1 j) alpha)
		(setf (aref 2d j-1 j) 0.0d0)
	      ;; else
	      (return (+ i 1)))
	  finally
	    ;; If we get here the matrix is diagonal, jump out!!!
	    (add-property! t0 'diagonal?)
	    (return-from last-bidiagonal-block! t0)))
    (setf start
      (loop for i downfrom (- end 1) above 0
	  for j = (+ i ioff)
	  for j-1 = (- j 1)
	  for alpha = (* *zero-element-scale* 
			 (+ 1.0d0
			    (abs (aref 2d j j))
			    (abs (aref 2d j-1 j-1))))
	  do (when (bm:small?  (aref 2d j-1 j) alpha)
	       (setf (aref 2d j-1 j) 0.0d0)
	       (return i))
	  finally ;; If we get here, it's the whole matrix.
	    (return 0)))
    (setf t0 (restrict! t0
			(block-subspace (codomain t0) start end)
			(block-subspace (domain t0) start end)))
    (add-property! t0 'upper-bidiagonal?)
    t0))

;;;=======================================================

(defun decouple-last-diagonal-zero! (t0)
  (az:type-check Matrix-Super t0)
  (assert (square? t0))
  (assert (diagonal-block? t0))
  (assert (upper-bidiagonal? t0))
  
  (let* ((2d (2d-array t0))
	 (start (coord-start (codomain t0)))
	 (end (coord-end (codomain t0)))
	 (e-1 (- end 1))
	 (e-2 (- end 2))
	 (ls ())
	 (rs ())
	 (i0 end)
	 g)
    (cond ;; a small last diagonal element is a special case:
     ((bm:small?
       (aref 2d e-1 e-1)
       (* *zero-element-scale*
	  (+ 1.0d0 (abs (aref 2d e-1 e-2)) (abs (aref 2d e-2 e-2)))))
      (loop for k downfrom e-2 to start
	  do (setf g (make-givens-for
		      (domain t0)
		      (aref 2d k k) (aref 2d k e-1)
		      k e-1))
	     (transpose g :result g)
	     (compose t0 g :result t0)
	     (push (transpose g :result g) rs)))
     (t ;; find the index of the last diagonal element that is small
      (loop for i downfrom e-2 to start
	  do (when (bm:small?  (aref 2d i i)
			       (* *zero-element-scale*
				  (+ 1.0d0 (abs (aref 2d i (+ i 1))))))
	       (setf (aref 2d i i) 0.0d0)
	       (setf i0 i)
	       (return)))
      (unless (/= i0 end)
	(loop for k from (+ i0 1) below end
	    do (setf g (make-givens-for (codomain t0)
					(aref 2d k k) (aref 2d i0 k)
					k i0))
	       (compose g t0 :result t0)
	       (push (transpose g :result g) ls)))))
    ;; At the end, we know <t0> is upper-bidiagonal again:
    (add-property! t0 'upper-bidiagonal?)
    (values (nreverse ls) t0 rs)))

;;;=======================================================

(defmethod slow-golub-kahan-decompose ((t0 Matrix))
  (multiple-value-bind (ls t1 rs) (slow-golub-kahan-step! (az:copy t0))
    (make-instance 'Linear-Product :factors (nconc ls (list t1) rs))))

;;;-------------------------------------------------------
#||latex
 See:
 {\sc Golub and Van Loan,} {\sf Matrix Computations,} sec 8.3, alg. 8.3-1.

 This version is slow, because it uses explicit compositions
 instead of taking into account all the zeros.
||#

(defun slow-golub-kahan-step! (t0)
  (az:type-check Matrix-Super t0)
  (assert (upper-bidiagonal? t0))
  (assert (square? t0))
  (let* ((lefts ()) (rights ())
	 (2d (2d-array t0)) 
	 (start (coord-start (codomain t0)))
	 (start+1 (+ start 1))
	 (end-1 (- (coord-end (codomain t0)) 1))
	 (end-2 (- end-1 1)) (end-3 (- end-2 1))
	 (dm (aref 2d end-2 end-2))
	 (dn (aref 2d end-1 end-1)) ;; diagonal elts
	 (fm (if (>= end-3 start) (aref 2d end-3 end-2) 0.0d0))
	 (fn (aref 2d end-2 end-1)) ;; super diagonal elts
	 (a00 (+ (bm:sq dm) (bm:sq fm)))
	 (a01 (* dm fn))
	 (a11 (+ (bm:sq dn) (bm:sq fn))))
    (multiple-value-bind (l+ l-) (eigenvalues-of-symmetric-2x2 a00 a01 a11)
      ;; get the closest eigenvalue to a11
      (let* ((l (if (<= (abs (- l+ a11)) (abs (- l- a11))) l+ l-))
	     (d0 (aref 2d start start))
	     (f1 (aref 2d start start+1))
	     (g (make-givens-for
		 (domain t0) (- (bm:sq d0) l) (* d0 f1) start start+1))
	     (k start) (k+1 (+ k 1)) (k+2 (+ k 2)))
	;; won't be bidiagonal after compose
	(setf (bands-start t0) -1)
	(setf (bands-end t0) 3) 
	(compose t0 (transpose g :result g) :result t0)
	(push (transpose g :result g) rights)
	;; chase the unwanted element away:
	(loop
	  (when (bm:small?  (aref 2d k+1 k) (aref 2d k k))
	    ;; The unwanted element has evaporated.
	    (return))
	  (setf g (make-givens-for
		   (codomain t0) (aref 2d k k) (aref 2d k+1 k) k k+1))
	  (compose g t0 :result t0)
	  (push (transpose g :result g) lefts)
	  (when (= k end-2) (return))
	  (when (bm:small?  (aref 2d k k+2) (aref 2d k k+1))
	    ;; The unwanted element has evaporated.
	    (return)) 
	  (setf g (make-givens-for
		   (domain t0) (aref 2d k k+1) (aref 2d k k+2) k+1 k+2))
	  (compose t0 (transpose g :result g) :result t0)
	  (push (transpose g :result g) rights)
	  (incf k) (incf k+1) (incf k+2))))
    ;; t0 should be bidiagonal again
    (add-property! t0 'upper-bidiagonal?)
    ;;(coerce-bands! t0)
    (values (nreverse lefts) t0 rights)))

;;;=======================================================
;;; returns a factoring of t0
;;; Golub and VanLoan alg. 6.5-1

(defmethod householder-bidiagonalize ((t0 Matrix))
  (householder-bidiagonalize! (az:copy t0)))

;;;-------------------------------------------------------

(defmethod householder-bidiagonalize! ((t0 Matrix-Super))
  (let* ((ls ()) (rs ()) h
	 (cod (codomain t0)) (m (dimension cod)) (m-1 (- m 1))
	 (dom (domain t0)) (n (dimension dom)) (n-1 (- n 1)) 
	 (s0 (restrict t0))
	 (nsteps-left (min (- m 1) n))
	 (nsteps-right (min m (- n 2))))
    (dotimes (i (max nsteps-left nsteps-right))
      (when (< i nsteps-left)
	(setf h (zero-col-with-householder! s0))
	(push (extend! h cod) ls))
      
      ;; need to restrict attention to the right tail of s0 whether we
      ;; perform the left householder step or not.
      (unless (>= i n-1) (setf (domain s0) (tail-space (domain s0))))
      (when (< i nsteps-right)
	;; using the fact that h is symmetric:
	(setf h (zero-row-with-householder! s0))
	(push (extend! h dom) rs))
      ;; need to restrict attention to the lower tail of s0 whether
      ;; perform the right householder step or not.
      (unless (>= i m-1) (setf (codomain s0) (tail-space (codomain s0)))))
    (add-property! t0 'upper-bidiagonal?)
    (make-instance 'Linear-Product
      :factors (nconc (nreverse ls) (list t0) rs))))


;;;=======================================================
#||latex

 Calls fortran-style (array level) {\tt svd-nash}.
 Returns the factoring: $t_0 = u a v{\dagger}$,
 represented by a Orthogonal-Matrix's {\tt u} and {\tt v}, 
 and a Diagonal-Vector {\tt s}.

||#

(defun nash-svd-decompose (t0)
  (az:type-check Matrix t0)
  ;; nash algorithm only for codomain >= domain:
  (assert (embedding? t0) (t0) "The nash svd only works for embeddings.")
  (multiple-value-bind (u s v) (bm:svd-nash (2d-array t0))
    (let* ((rank (length s))
	   (vs0 (domain t0))
	   (vs1 (LVector-Space rank))
	   (vs2 (codomain t0)))
      (make-instance
	  'Linear-Product
	:factors
	(list
	 (make-instance 'Matrix
	   :2d-array u :domain vs1 :codomain vs2
	   :properties (list 'orthogonal?))
	 (make-instance 'Diagonal-Vector
	   :vec s :domain vs1 :codomain vs1)
	 (transpose
	  (make-instance 'Matrix
	    :2d-array v :domain vs1 :codomain vs0
	    :properties (list 'orthogonal?))))))))


;;;=======================================================
;;;		  Eigen decompositions
;;;=======================================================
;;; returns an orthogonal symmetry factoring of t0

(defmethod slow-eigen-decompose ((t0 Matrix))
  (assert (symmetric? t0))
  (slow-eigen-decompose! (az:copy t0)))

;;;-------------------------------------------------------

(defmethod slow-eigen-decompose! ((t0 Matrix))
  (assert (symmetric? t0))
  (cond
   ((tridiagonal? t0)
    (let ((fs ()) fs0
	  (s0 (restrict t0)))
      (loop
	(setf s0 (last-tridiagonal-block! t0 :result s0))
	(when (diagonal? s0) (return)) ;; we're done--this isn't right!
	(setf fs0 (map 'List #'extend! (slow-implicit-qr-step! s0)))
	(setf fs (nconc fs0 fs)))
      (add-property! t0 'diagonal?)
      (make-instance 'Orthogonal-Similarity-Product
	:right (make-instance 'Linear-Product
		 :factors (nreverse fs))
	:middle t0)))
   (t
    (let* ((sp0 (hessenberg-decompose! t0))
	   (sp1 (slow-eigen-decompose! (middle sp0)))
	   (sp2 (ordered-diagonal-decompose! (middle sp1)))
	   (r1 (right sp1))
	   (r2 (right sp2)))
      (setf (right sp2) (compose r2 (compose r1 (right sp0) :result r1)
				 :result r2))
      sp2))))

;;;=======================================================
;;; returns an orthognal-similarity-product factoring of t0

(defmethod eigen-decompose ((t0 Matrix))
  (assert (symmetric? t0))
  (eigen-decompose! (az:copy t0)))

;;;-------------------------------------------------------

(defmethod eigen-decompose! ((t0 Matrix))
  (assert (symmetric? t0))
  (let (sp0 sp1 sp2)
    (cond
     ((tridiagonal? t0)
      (setf sp1 (tridiagonal-eigen-decompose! t0))
      (setf sp2 (ordered-diagonal-decompose! (middle sp1)))
      (let ((r2 (right sp2)))
	(compose r2 (right sp1) :result r2)))
     (t
      (setf sp0 (hessenberg-decompose! t0))
      (setf sp1 (tridiagonal-eigen-decompose! (middle sp0)))
      (setf sp2 (ordered-diagonal-decompose! (middle sp1)))
      (let ((r1 (right sp1))
	    (r2 (right sp2)))
	(compose r2 (compose r1 (right sp0) :result r1) :result r2))))
    sp2))

;;;=======================================================
;;; returns an orthogonal similarity factoring of t0

(defmethod hessenberg-decompose ((t0 Matrix))
  (hessenberg-decompose! (copy-to-matrix t0)))

(defmethod hessenberg-decompose! ((t0 Matrix))
  (assert (square? t0))
  ;; jump out now in the trivial case
  (when (upper-hessenberg? t0) 
    (return-from hessenberg-decompose!
      (make-instance 'Orthogonal-Similarity-Product
	:right (the-identity-map (domain t0))
	:middle t0)))
  (let ((e (coord-end (domain t0)))
	(factors ()) right h)
    (with-borrowed-element (v (codomain t0))
      (bm:fast-loop (i :from (coord-start (domain t0)) :below (- e 2))
		    (setf h (make-householder
			     (codomain t0) (col t0 i :result v) (+ i 1) e))
		    (rotate-mapping h t0 :result t0)
		    (push h factors)))
    (add-property! t0 'upper-hessenberg?)
    (when (symmetric? t0) (add-property! t0 'tridiagonal?))
    (setf right (make-instance 'Linear-Product :factors factors))
    (make-instance 'Orthogonal-Similarity-Product :right right :middle t0)))

;;;=======================================================

(defmethod tridiagonal-eigen-decompose! ((t0 Matrix-Super))
  (assert (symmetric? t0))
  (assert (tridiagonal? t0))
  (let ((fs ()) fs0
	(s0 (restrict t0))
	(maxi (* 6 (dimension (domain t0)))))
    (dotimes (i maxi (error "Too many iterations."))
      (setf s0 (last-tridiagonal-block! t0 :result s0))
      (when (diagonal? t0) (return)) ;; we're done--this isn't right!
      (setf fs0 (implicit-qr-step! s0))
      (setf fs (nconc fs0 fs)))
    (add-property! t0 'diagonal?)
    (setf fs (map 'List  #'extend! fs))
    (make-instance 'Orthogonal-Similarity-Product
      :right (make-instance
		 'Linear-Product
	       :factors (if (null fs)
			    (list (the-identity-map (domain t0)))
			  fs))
      :middle t0)))

;;;-------------------------------------------------------

(defun small-off-diagonal-elt? (2d i)
  (let ((i+1 (+ i 1)))
    (bm:small?  (aref 2d i i+1) (* 0.5d0(+ (abs (aref 2d i i))
					  (abs (aref 2d i+1 i+1)))))))

;;;=======================================================
#||latex

 Identify the irreducible diagonal blocks of $T$ by zeroing any
 $T_{i,i+1} = T_{i+1,i}$ such that
 $|T_{i,i+1}| \leq \epsilon \[|T_{i,i}| + |T_{i+1,i+1}|\].$

 Return {\tt t0} restricted to its last unreduced tridiagonal block, 
 if there is one. If the matrix is actually diagonal, set the bands
 appropriately.

||#

(defun last-tridiagonal-block! (t0 &key result)
  (az:type-check Matrix-Super t0)
  (assert (square? t0))
  (assert (diagonal-block? t0))
  (assert (symmetric? t0))
  (assert (tridiagonal? t0))  
  ;; find the bottom of the block
  (let ((2d (2d-array t0))
	(cod-start (coord-start (codomain t0)))
	(cod-end (coord-end (codomain t0)))
	end start)
    (setf end
      (loop for i downfrom (- cod-end 2) to cod-start
					    ;;for i+1 = (+ i 1)
	  do (cond ((small-off-diagonal-elt? 2d i)
		    ;;(setf (aref 2d i+1 i) 0.0d0)
		    ;;(setf (aref 2d i i+1) 0.0d0)
		    )
		   (t (return (+ i 2))))
	  finally
	    ;; If we get here the matrix is diagonal,
	    ;; change bands and jump out!
	    (add-property! t0 'diagonal?)
	    (return-from last-tridiagonal-block! t0)))
    (setf start
      (loop for i downfrom (- end 2) to cod-start
					;;for i+1 = (+ i 1)
	  do (when (small-off-diagonal-elt? 2d i)
	       ;;(setf (aref 2d i+1 i) 0.0d0)
	       ;;(setf (aref 2d i i+1) 0.0d0)
	       (return (+ i 1)))
	  finally ;; If we get here, it's the whole matrix.
	    (return cod-start)))
    ;; if we get here, then we return the restricted Matrix
    (restrict
     t0
     (block-subspace (codomain t0) start end)
     (block-subspace (domain t0) start end)
     :result result)))

;;;-------------------------------------------------------
#||latex

 Implicit symmetric QR step with Wilkinson shift

 The explicit QR step factors a shifted $T - \mu I \Rightarrow UR$
 and then overwrites $T \Leftarrow RU + \mu I$.
 This algorithm implicitly performs the same Mapping on an
 irreducible, symmetric, tridiagonal $T$ by overwriting
 $T \Leftarrow \bar{Z}^{\dagger} T \bar{Z}$ where
 $ \bar{Z} = J \ldots J_{n-2}$.
 $\bar{Z}$ is chosen so that $\bar{Z}^{\dagger} \[T - \mu I\]$
 would be upper triangular and so that $\mu$ is Wilkinson shift,
 that is, the eigenvalue of the lower right 2x2 Matrix-Super of $T$
 that is closest to $T_{nn}$.

 See:
 {\sc Golub and Van Loan,} {\bf Matrix Computations,} sec 8.2, alg. 8.2-2
||#

(defun slow-implicit-qr-step (t0)
  (az:type-check Matrix-Super t0)
  (assert (symmetric? t0))
  (assert (tridiagonal? t0))  
  (slow-implicit-qr-step! (az:copy t0)))

(defun slow-implicit-qr-step! (t0)
  ;; This is slow, because it uses an explicit rotate-mapping!
  ;; instead of taking into account all the zeros.
  (az:type-check Matrix-Super t0)
  (assert (symmetric? t0))
  (assert (tridiagonal? t0))  
  (let* ((2d (2d-array t0)) 
	 (start (coord-start (codomain t0))) (end (coord-end (codomain t0)))
	 (e-1 (- end 1)) (e-2 (- end 2))
	 (t11 (aref 2d e-1 e-1)) (t22 (aref 2d e-2 e-2))
	 (t21 (aref 2d e-2 e-1))
	 (d (* 0.5d0(- t22 t21)))
	 (nu (+ d (* (signum d) (sqrt (+ (bm:sq d) (bm:sq t21))))))
	 (mu (progn (assert (not (zerop nu)))
		    (- t11 (/ (bm:sq t21) nu))))
	 (x (- (aref 2d start start) mu))
	 (z (aref 2d (+ start 1) start))
	 (gs ()))
    (loop for k from start to e-2
	for g = (make-givens-for (domain t0) x z k (+ k 1))
	do (transpose g :result g)
	   (rotate-mapping g t0 :result t0)
	   (when (< k e-2)
	     (setf x (aref 2d (+ k 1) k))
	     (setf z (aref 2d (+ k 2) k)))
	   (push (transpose g :result g) gs))	     
    ;; just return a list of gs, rather than a Linear-Product Mapping,
    ;; because this is only an intermediate step in an eigen decompose;
    ;; we'll just want to accumulate these gs with others into
    ;; the Linear-Product Mapping returned by the eigen decompose.
    (add-property! t0 'symmetric?)
    (add-property! t0 'tridiagonal?)
    (nreverse gs)))


;;;=======================================================

(defun implicit-qr-decompose (t0)
  (az:type-check Matrix-Super t0)
  (multiple-value-bind (qs t1) (implicit-qr-step! (az:copy t0))
    (let ((right  (make-instance 'Linear-Product :factors qs)))
      (make-instance 'Orthogonal-Similarity-Product :middle t1 :right right))))

;;;-------------------------------------------------------

(defun implicit-qr-step! (t0) 
  (az:type-check Matrix-Super t0)
  (assert (symmetric? t0))
  (assert (tridiagonal? t0))
  (case (dimension (codomain t0))
    ((0 1)
     (error "Too small for qr step."))
    (2
     ;; This case might also be an error, but, anyway,
     ;; diagonalize the 2x2 case directly, dont bother with shifts.
     (multiple-value-bind (q t1) (diagonalize-2x2! t0)
       (values (list q) t1)))
    (otherwise
     (implicit-qr>=3x3! t0))))

;;;-------------------------------------------------------

(defun implicit-qr>=3x3! (t0)
  (az:type-check Matrix-Super t0)
  (assert (and (tridiagonal? t0) (symmetric? t0)))
  (let* ((start (coord-start (codomain t0)))
	 (s+1 (+ start 1))
	 (s-1 (- start 1))
	 (end (coord-end (codomain t0)))
	 (e-1 (- end 1))
	 (e-2 (- end 2))
	 (2d (2d-array t0))
	 ;; construct the zeroth givens <g0>
	 (t11 (aref 2d e-1 e-1)) (t22 (aref 2d e-2 e-2))
	 (t21 (aref 2d e-2 e-1)) (d (* 0.5d0(- t22 t21)))
	 (nu (+ d (* (signum d) (sqrt (+ (bm:sq d) (bm:sq t21))))))
	 (mu (progn (assert (not (zerop nu)))
		    (- t11 (/ (bm:sq t21) nu))))
	 (x (- (aref 2d start start) mu))
	 (g (make-givens-for (domain t0) x (aref 2d s+1 start) start s+1))
	 (gs ()) z)
    (bm:fast-loop
     (k :from s-1 :below e-2)
     (when (identity-map? g) (return))
     ;; no more change is possible once we get an identity
     (push g gs)
     ;; implicitly rotate-mapping t0 with (transpose g)
     ;; first do the 2x2 square that's valid at every step:
     (let* ((c (c g)) (s (- (s g)))
	    (cs (* c s)) (cc (* c c)) (ss (* s s))
	    (k1 (+ k 1)) (k2 (+ k 2)) (k3 (+ k 3))
	    (t11 (aref 2d k1 k1))
	    (t21 (aref 2d k2 k1))
	    (t22 (aref 2d k2 k2))
	    (nt21 (+ (* cs (- t11 t22)) (* (- cc ss) t21))))
       (setf (aref 2d k2 k1) nt21) (setf (aref 2d k1 k2) nt21)
       (setf (aref 2d k1 k1) (+ (* cc t11) (* -2.0d0 cs t21)(* ss t22)))
       (setf (aref 2d k2 k2) (+ (* ss t11) (*  2.0d0 cs t21)(* cc t22)))
       ;; When not the first step, we need to modify (aref 2d k k1) and
       ;; (aref 2d k1 k). <z> was initialized on the first pass.
       (when (>= k start)
	 (let ((nt10 (- (* c (aref 2d k1 k)) (* s z))))
	   (setf (aref 2d k1 k) nt10) (setf (aref 2d k k1) nt10)))
       ;; When not the last step, we generate an element that's outside
       ;; the tridiagonal. We keep this element in z, instead of
       ;; storing it back into t0 to make it easier to adapt this
       ;; method to more compact representations.
       (when (< k (- end 3))
	 (let* ((t32 (aref 2d k3 k2)) (nt32 (* c t32)))
	   (setf z (* (- s) t32))
	   (setf (aref 2d k3 k2) nt32) (setf (aref 2d k2 k3) nt32)
	   (setf g (make-givens-for (domain t0) (aref 2d k2 k1)
				    z k2 k3))))))
    (values gs t0)))


;;;=======================================================

(defun 2x2-eigen-decompose (t0) 
  (az:type-check Matrix-Super t0)
  (multiple-value-bind (q t1) (diagonalize-2x2! (az:copy t0))
    (let ((right (make-instance 'Linear-Product
		   :factors (list (transpose q :result q)))))
      (make-instance 'Orthogonal-Similarity-Product
	:middle t1 :right right))))

;;;-------------------------------------------------------

(defun diagonalize-2x2! (t0)
  (az:type-check Matrix-Super t0)
  (assert (= 2 (dimension (codomain t0)) (dimension (domain t0))))
  (assert (symmetric? t0))
  (when (diagonal? t0)
    (return-from diagonalize-2x2! (the-identity-map (domain t0))))
  (let* ((i0 (coord-start (codomain t0)))
	 (i1 (+ i0 1))
	 (2d (2d-array t0))
	 (a00 (aref 2d i0 i0))
	 (a01 (aref 2d i0 i1))
	 (a11 (aref 2d i1 i1))
	 alpha beta g l+ l-)
    (multiple-value-setq (l+ l-) (eigenvalues-of-symmetric-2x2 a00 a01 a11))
    ;; construct the givens transform corresponding to the eigenvectors
    (assert (not (zerop a01)))
    (setf alpha (/ (- l+ a00) a01))
    (setf beta (sqrt (+ 1.0d0 (bm:sq alpha))))
    (setf g (make-instance 'Givens
	      :c (/ 1.0d0 beta) :s (/ alpha beta) :i0 i0 :i1 i1
	      :codomain (codomain t0) :domain (domain t0)))
    ;; do (rotate-mapping (transpose g) t0 :result t0) by hand:
    (setf (aref 2d i0 i0) l+)  (setf (aref 2d i0 i1) 0.0d0)
    (setf (aref 2d i1 i0) 0.0d0) (setf (aref 2d i1 i1) l-)
    (values g t0)))

;;;-------------------------------------------------------

(defun eigenvalues-of-symmetric-2x2 (a00 a01 a11)
  (let* ((a00+a11 (+ a00 a11))
	 (mu (sqrt (- (* a00+a11 a00+a11)
		      (* 4.0d0 (- (* a00 a11)
				(* a01 a01)))))))
    (values (* 0.5d0(+ a00+a11 (abs mu)))
	    (* 0.5d0(- a00+a11 (abs mu))))))
  
;;;=======================================================
;;; Use similarity Mapping by pivots to sort the diagonal
;;; elements in descending order.

(defmethod ordered-diagonal-decompose ((t0 Matrix))
  (ordered-diagonal-decompose! (az:copy t0)))

(defmethod ordered-diagonal-decompose! ((t0 Matrix))
  (assert (square? t0))
  (let* ((dom (domain t0))
	 (n (dimension dom))
	 (2d (2d-array t0))
	 (pivots ()) pv imax)
    (dotimes (i (- n 1))
      (setf imax (bm:v-abs-max-index 2d :start i :end n :v-type :diagonal))
      (unless (= i imax)
	(setf pv (make-pivot dom i imax))
	(rotate-mapping pv t0 :result t0)
	(push pv pivots)))
    (make-instance
	'Orthogonal-Similarity-Product
      :right (make-instance 'Linear-Product
	       :factors (if (null pivots)
			    (list (the-identity-map dom))
			  pivots))
      :middle t0)))

