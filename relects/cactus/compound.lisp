;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;; Compound Mappings:
;;;=======================================================

(defclass Product-Mapping (Indirect-Mapping)
     ((factors :type List :initarg :factors :reader factors)))

;;;-------------------------------------------------------

(defmethod codomain ((t0 Product-Mapping))
  (codomain (az:first-elt (factors t0))))

(defmethod domain ((t0 Product-Mapping))
  (domain (az:last-elt (factors t0))))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Product-Mapping)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (not (null (factors t0))))
  ;; check for matching domains and codomains
  (let ((dom (domain (first (factors t0)))))
    (flet ((mate? (f)
	     (assert (eql dom (codomain f)))
	     (setf dom (domain f))))
      (map nil #'mate? (rest (factors t0))))))

;;;=======================================================

(defclass Flat-Product (Product-Mapping Flat-Mapping) ())

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Flat-Product)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (every #'flat? (factors t0))))

;;;=======================================================

(defclass Linear-Product (Linear-Mapping Flat-Product)
     ((reversed-factors
	:type List
	:initarg :reversed-factors)))

;;;-------------------------------------------------------
;;; lazy evaluation of <reversed-factors>:

(defmethod (setf factors) ((fs List) (t0 Linear-Product))
  #||(let ((vspace (codomain (first fs))))
    (setf fs (delete-if #'identity-map? fs))
    (when (null fs) (setf fs (list (the-identity-map vspace)))))||#
  (slot-makunbound  t0 'reversed-factors)
  (setf (slot-value t0 'factors) fs)
  (factors t0))

(defmethod reversed-factors ((t0 Linear-Product))
  (unless (slot-boundp t0 'reversed-factors)
    (setf (slot-value t0 'reversed-factors) (reverse (factors t0))))
  (slot-value t0 'reversed-factors))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Linear-Product)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  #||(let ((vspace (codomain (first (factors t0)))))
       (setf (slot-value t0 'factors)
	 (delete-if #'identity-map? (factors t0)))
       (unless (slot-boundp t0 factor's)
	 (setf (slot-value t0 'factors) (list (the-identity-map vspace)))))||#
  
	 ;; discover properties:
	 (setf (properties t0)
	   (nconc
	    (properties t0)
	    (mapcan
	     #'(lambda (p) (when (every p (factors t0)) (list p)))
	     *properties-of-linear-Mappings-preserved-by-products*)))
	 (delete-duplicates (properties t0))
	 (assert (every #'linear? (factors t0))))

;;;-------------------------------------------------------

(defmethod map-property? ((t0 Linear-Product) (prop Symbol)
			  &key (verify? *verify-properties?*)
			  &allow-other-keys)
  (if verify?
      (every #'(lambda (f) (funcall prop f :verify? verify?)) (factors t0))
      (member prop (properties t0))))

(defmethod modifier? ((t0 Linear-Product)
		      &key (verify? *verify-properties?*)
		      &allow-other-keys)
  (map-property? t0 'modifier? :verify? verify?))

(defmethod orthogonal? ((t0 Linear-Product)
			&key (verify? *verify-properties?*)
			&allow-other-keys)
  (map-property? t0 'orthogonal? :verify? verify?))

(defmethod orthogonal-rows? ((t0 Linear-Product)
			     &key (verify? *verify-properties?*)
			     &allow-other-keys)
  (map-property? t0 'orthogonal-rows? :verify? verify?))

(defmethod orthogonal-columns? ((t0 Linear-Product)
				&key (verify? *verify-properties?*)
				&allow-other-keys)
  (map-property? t0 'orthogonal-columns? :verify? verify?))

;;;-------------------------------------------------------

(defmethod add-property! ((t0 Linear-Product) (prop Symbol)
			  &key
			  (verify? *verify-properties?*)
			  &allow-other-keys)
  (case prop
    ((unitary? modifier? orthogonal? orthogonal-rows? orthogonal-columns?)
      (pushnew prop (properties t0))
      (when verify? (funcall prop t0 :verify? verify?)))
    (otherwise
      (warn "adding unknown property ~a" prop)
      (pushnew prop (properties t0))))
  t0)

(defmethod delete-property! ((t0 Linear-Product) (prop Symbol))
  (case prop
    ((unitary? modifier? orthogonal? orthogonal-rows? orthogonal-columns?) 
     (setf (properties t0) (delete prop (properties t0))))
    (otherwise
      (warn "deleting unknown property ~a" prop)
      (setf (properties t0) (delete prop (properties t0))))))

;;;-------------------------------------------------------
;;; collect the leaves fo the factor tree:

(defmethod harvest-factors ((t0 Linear-Mapping)) (list t0))

(defmethod harvest-factors ((t0 Linear-Product))
  (let ((all-factors ()))
    (map nil
	 #'(lambda (f)
	     (setf all-factors (nconc all-factors (harvest-factors f))))
	 (factors t0))
    all-factors))

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Linear-Product)
			  (vd Abstract-Point)
			  (result Abstract-Point))
  
  (let ((v0 vd) v1 (vs0 (domain t0)) vs1)
    (map nil #'(lambda (f)
		 (setf vs1 (codomain f))
		 (setf v1 (borrow-element vs1))
		 (transform f v0 :result v1)
		 (unless (eql vd v0) (return-element v0 vs0))
		 (setf v0 v1)
		 (setf vs0 vs1))
	 (reversed-factors t0))
    (az:copy v1 :result result)
    (unless (eql vd v0) (return-element v0 vs0))
    result))

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Linear-Product)
				    (vd Abstract-Point)
				    (result Abstract-Point))
  (let ((v0 vd) v1 (vs0 (dual-space (codomain t0))) vs1)
    (map nil
	 #'(lambda (f)
	     (setf vs1 (dual-space (domain f)))
	     (setf v1 (borrow-element vs1))
	     (transform-transpose f v0 :result v1)
	     (unless (eql vd v0) (return-element v0 vs0))
	     (setf v0 v1)
	     (setf vs0 vs1))
	 (factors t0))
    (az:copy v1 :result result)
    (unless (eql vd v0) (return-element v0 vs0))
    result))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Linear-Product) &key result)
  (when (not (or (null result) (eq t0 result)))	     
    (warn "Optional Result ~a ignored." result))
  (if (eq t0 result)
      (transpose! t0)
      ;; else
      (make-instance
	'Linear-Product
	:factors (map 'List #'transpose (reversed-factors t0)))))

(defmethod transpose! ((t0 Linear-Product))
  (setf (factors t0) (nreverse (factors t0))) 
  (map nil #'transpose! (factors t0))
  t0)

;;;=======================================================
;;; Generalization of Cholesky decompose.

(defclass Symmetric-Outer-Product (Symmetric Indirect-Mapping)
     ((left
       :type Linear-Mapping
       :initarg :left
       :accessor left)))

(defmethod codomain ((t0 Symmetric-Outer-Product)) (codomain (left t0)))
(defmethod domain  ((t0 Symmetric-Outer-Product)) (codomain (left t0)))
  
;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Symmetric-Outer-Product))
  (make-instance 'Symmetric-Outer-Product
    :domain (domain t0)
    :codomain (codomain t0)
    :left (az:copy (left t0))))

(defmethod az:copy-to! ((t0 Symmetric-Outer-Product)
			(result Symmetric-Outer-Product))
  (reinitialize-instance result
			 :allow-other-keys t
			 :domain (domain t0)
			 :codomain (codomain t0)
			 :left (az:copy (left t0) :result (left result)))
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Symmetric-Outer-Product)
			  (vd Abstract-Point)
			  (result Abstract-Point))
  (let ((lf (left t0)))
    (with-borrowed-element (v (domain lf))
      (transform lf
		 (transform-transpose lf vd :result v)
		 :result result))))

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Symmetric-Outer-Product)
				    (vd Abstract-Vector)
				    (result Abstract-Vector))
  (let ((lf (left t0)))
    (with-borrowed-element (v (domain lf))
      (transform lf
		 (transform-transpose lf vd :result v)
		 :result result))))

;;;=======================================================

(defclass Symmetric-Inner-Product (Symmetric Indirect-Mapping)
     ((right
       :type Linear-Mapping
       :initarg :right
       :accessor right)))

(defmethod codomain ((t0 Symmetric-Inner-Product)) (domain (right t0)))
(defmethod domain ((t0 Symmetric-Inner-Product)) (domain (right t0)))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Symmetric-Inner-Product))
  (make-instance 'Symmetric-Inner-Product
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :right (az:copy (right t0))))

(defmethod az:copy-to! ((t0 Symmetric-Inner-Product)
			(result Symmetric-Inner-Product))
  (reinitialize-instance result
			 :allow-other-keys t
			 :domain (domain t0)
			 :codomain (codomain t0)
			 :right (az:copy (right t0) :result (right result)))
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Symmetric-Inner-Product)
			  (vd Abstract-Vector)
			  (result Abstract-Vector))
  (let ((rf (right t0)))
    (with-borrowed-element (v (codomain rf))
      (transform-transpose rf
			   (transform rf vd :result v)
			   :result result))))

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Symmetric-Inner-Product)
				    (vd Abstract-Vector)
				    (result Abstract-Vector))
  (let ((rf (right t0)))
    (with-borrowed-element (v (domain rf))
      (transform-transpose rf
			   (transform rf vd :result v)
			   :result result))))

;;;=======================================================

(defclass Orthogonal-Similarity-Product (Linear-Mapping
					  Indirect-Mapping)
     ((middle :type Linear-Mapping :initarg :middle :accessor middle)
      (right  :type Linear-Mapping :initarg :right  :accessor right)))

;;;-------------------------------------------------------

(defmethod codomain ((t0 Orthogonal-Similarity-Product)) (codomain (right t0)))
(defmethod domain   ((t0 Orthogonal-Similarity-Product)) (domain (right t0)))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Orthogonal-Similarity-Product)
				     slot-names &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (orthogonal-rows? (right t0)))
  (assert (eql (codomain (middle t0)) (domain (right t0)))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Orthogonal-Similarity-Product))
  (make-instance 'Orthogonal-Similarity-Product
		 :right (az:copy (right t0))
		 :middle (az:copy (middle t0))))

(defmethod az:copy-to! ((t0 Orthogonal-Similarity-Product)
		     (result Orthogonal-Similarity-Product))
  (az:copy (right t0) :result (right result))
  (az:copy (middle t0) :result (middle result))
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Orthogonal-Similarity-Product)
			  (vd Abstract-Vector)
			  (result Abstract-Vector))
  (with-borrowed-elements ((v0 (codomain (right t0)))
			   (v1 (domain (right t0))))
    (transform-transpose
      (right t0)
      (transform (middle t0)
		 (transform (right t0) vd :result v0)
		 :result v1)
      :result result)))

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Orthogonal-Similarity-Product)
				    (vd Abstract-Vector)
				    (result Abstract-Vector))
  (with-borrowed-elements ((v0 (codomain (right t0)))
			   (v1 (codomain (right t0))))
    (transform-transpose
      (right t0) 
      (transform-transpose
	(middle t0) (transform (right t0) vd :result v0)
	:result v1)
      :result result)))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Orthogonal-Similarity-Product) &key result)
  (when (not (or (null result) (eq t0 result)))	     
    (warn "Optional Result ~a ignored." result))
  (if (eq t0 result)
      (transpose! t0)
      ;; else
      (make-instance 'Orthogonal-Similarity-Product
		     :right (az:copy (right t0))
		     :middle (transpose (middle t0)))))

(defmethod transpose! ((t0 Orthogonal-Similarity-Product))
  (setf (middle t0) (transpose! (middle t0)))
  t0)

;;;=======================================================

(defclass Tensor-Product (Linear-Mapping
			   Indirect-Mapping)
     ((v-codomain :type Abstract-Vector
	 :accessor v-codomain
	 :initarg :v-codomain)
      (v-domain :type Abstract-Vector
	 :accessor v-domain
	 :initarg :v-domain)))

;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 Tensor-Product)
				     slot-names &rest initargs)
  (declare (ignore initargs))
  slot-names
  (unless (slot-boundp t0 'v-domain)
    (setf (v-domain t0) (make-zero-element (domain t0))))
  (unless (slot-boundp t0 'v-codomain)
    (setf (v-codomain t0) (make-zero-element (codomain t0))))
  (assert (element? (v-codomain t0) (codomain t0)))
  (assert (element? (v-domain t0) (domain t0))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Tensor-Product))
  (make-instance 'Tensor-Product
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :v-codomain (az:copy (v-codomain t0))
		 :v-domain (az:copy (v-domain t0))))

(defmethod az:copy-to! ((t0 Tensor-Product)
		     (result Tensor-Product))
  (az:copy (v-codomain t0) :result (v-codomain result))
  (az:copy (v-domain t0) :result (v-domain result))
  result)

;;;-------------------------------------------------------
;;; need methods for both lisp Vector's and Abstract-Vector's

(defmethod transform-to! ((t0 Tensor-Product)
			  (vd Abstract-Vector)
			  (result Abstract-Vector))
  (scale (inner-product (v-domain t0) vd) (v-codomain t0) :result result))

(defmethod transform-transpose-to! ((t0 Tensor-Product)
				    (vd Abstract-Vector)
				    (result Abstract-Vector))
  (scale (inner-product (v-codomain t0) vd) (v-domain t0) :result result))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Tensor-Product)
		      &key
		      (result (make-instance 'Tensor-Product
				:v-codomain (v-domain t0)
				:v-domain (v-codomain t0)
				:codomain (domain t0)
				:domain (codomain t0))
			      result-supplied?))
  (when result-supplied?
    (reinitialize-instance result
			   :allow-other-keys t
			   :v-codomain (v-domain t0)
			   :v-domain (v-codomain t0)
			   :codomain (domain t0)
			   :domain (codomain t0))))

;;;=======================================================

(defclass Symmetric-Tensor-Product (Symmetric
				     Indirect-Mapping)
     ((v-domain :type Abstract-Vector
		:accessor v-domain
		:initarg :v-domain)))

;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 Symmetric-Tensor-Product)
				     slot-names &rest initargs)
  (declare (ignore initargs))
  slot-names
  (unless (slot-boundp t0 'v-domain)
    (setf (v-domain t0) (make-zero-element (domain t0))))
  (assert (element? (v-domain t0) (domain t0))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Symmetric-Tensor-Product))
  (make-instance 'Symmetric-Tensor-Product :vec (az:copy (v-domain t0))))

(defmethod az:copy-to! ((t0 Symmetric-Tensor-Product)
		     (result Symmetric-Tensor-Product))
  (az:copy (v-domain t0) :result (v-domain result))
  result)
  
;;;-------------------------------------------------------
;;; need methods for both lisp Vector's and Abstract-Vector's

(defmethod transform-to! ((t0 Symmetric-Tensor-Product)
			  (vd Abstract-Vector)
			  (result Abstract-Vector))
  (scale (inner-product (v-domain t0) vd) (v-domain t0) :result result))

;;;=======================================================

(defmethod tensor-product ((v-codomain T) (v-domain T)
			   &key
			   result
			   (domain (home-space v-domain))
			   (codomain (home-space v-codomain)))
  (az:type-check Abstract-Vector v-codomain v-domain)
  (cond
    ((null result)
     (setf result (if (and (eq codomain domain)
			   (eq v-codomain v-domain))
		      (make-instance 
			'Symmetric-Tensor-Product
			:codomain codomain
			:domain domain 
			:v-domain v-domain)
		      (make-instance 
			'Tensor-Product
			:domain domain
			:codomain codomain
			:v-codomain v-codomain
			:v-domain v-domain))))
    ((typep result 'Symmetric-Tensor-Product)
     (assert (and (eq codomain domain)
		  (eq v-codomain v-domain)))
     (assert (element? v-domain (domain result)))
     (setf (v-domain result) v-domain))
    (t
     (assert (element? v-domain (domain result)))
     (assert (element? v-codomain (codomain result)))
     (setf (v-domain result) v-domain)
     (setf (v-codomain result) v-codomain)))
  result)


;;;=======================================================

(defclass 2x2-Direct-Sum-Mapping (Linear-Mapping)
     ((domain :type Direct-Sum-Vector-Space
	      :accessor domain
	      :initarg :domain)
      (codomain	:type Direct-Sum-Vector-Space     
		:accessor codomain
		:initarg :codomain)
      (txx :type Linear-Mapping :accessor txx :initarg :txx)
      (txy :type Linear-Mapping :accessor txy :initarg :txy)
      (tyx :type Linear-Mapping :accessor tyx :initarg :tyx)
      (tyy :type Linear-Mapping :accessor tyy :initarg :tyy)))

;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 2x2-Direct-Sum-Mapping)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (let ((xc (xspace (codomain t0)))
	(yc (yspace (codomain t0)))
	(xd (xspace (domain t0)))
	(yd (yspace (domain t0))))
    (if (slot-boundp t0 'txx)
	(assert (and (eq (codomain (txx t0)) (xspace (codomain t0)))
		     (eq (codomain (txx t0)) (xspace (codomain t0)))))
      (setf (txx t0) (make-linear-Mapping xc xd)))
    (if (slot-boundp t0 'txy)
	(assert (and (eq (codomain (txy t0)) (xspace (codomain t0)))
		     (eq (domain   (txy t0)) (yspace (domain t0)))))
      (setf (txy t0) (make-linear-Mapping xc yd))) ;
    (if (slot-boundp t0 'tyx)
	(assert (and (eq (codomain (tyx t0)) (yspace (codomain t0)))
		     (eq (domain   (tyx t0)) (xspace (domain t0)))))
      (setf (tyx t0) (make-linear-Mapping yc xd)))
    (if (slot-boundp t0 'tyy)
	(assert (and (eq (codomain (tyy t0)) (yspace (codomain t0)))
		     (eq (domain   (tyy t0)) (yspace (domain t0)))))
      (setf (tyy t0) (make-linear-Mapping yc yd)))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 2x2-Direct-Sum-Mapping))
  (make-instance '2x2-Direct-Sum-Mapping
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :txx (az:copy (txx t0))
		 :txy (az:copy (txy t0))
		 :tyx (az:copy (tyx t0))
		 :tyy (az:copy (tyy t0))))

(defmethod az:copy-to! ((t0 2x2-Direct-Sum-Mapping)
		    (result 2x2-Direct-Sum-Mapping))
  (az:copy (txx t0) :result (txx result))
  (az:copy (txy t0) :result (txy result))
  (az:copy (tyx t0) :result (tyx result))
  (az:copy (tyy t0) :result (tyy result))	    
  result)

;;;-------------------------------------------------------

#+symbolics
(defmethod format-object ((t0 2x2-Direct-Sum-Mapping)
			  &optional (stream *standard-output*))
  (print (class-of t0) stream)
  (format stream "~%txx:") (format-object (txx t0) stream)
  (format stream "~%txy:") (format-object (txy t0) stream)
  (format stream "~%tyx:") (format-object (tyx t0) stream)
  (format stream "~%tyy:") (format-object (tyy t0) stream)) 

;;;-------------------------------------------------------

(defmethod identity! ((t0 2x2-Direct-Sum-Mapping))
  (setf (txx t0) (identity! (txx t0)))
  (setf (txy t0) (zero! (txy t0)))
  (setf (tyx t0) (zero! (tyx t0)))
  (setf (tyy t0) (identity! (tyy t0)))
  t0)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 2x2-Direct-Sum-Mapping)
			  (vd Direct-Sum-Vector)
			  (result Direct-Sum-Vector))
  (with-borrowed-element (v0 (xspace (codomain t0)))
    (setf (x result) (add (transform (txx t0) (x vd) :result v0)
			  (transform (txy t0) (y vd) :result (x result))
			  :result (x result))))
  (with-borrowed-element (v1 (yspace (codomain t0)))
    (setf (y result) (add (transform (tyx t0) (x vd) :result v1)
			  (transform (tyy t0) (y vd) :result (y result))
			  :result (y result))))
  result) 

;;;=======================================================
;;; It's more efficient to use the usual tensor product
;;; representation for tensor-products of direct-sum's
;;; than what's below:
 
;(defmethod tensor-product ((v-cod Direct-Sum-Vector)
;			   (v-dom Direct-Sum-Vector)
;			   &key
;			   result
;			   (domain (home-space v-dom))
;			   (codomain (home-space v-cod)))
;  (let ((x-cod (x v-cod))
;	(x-dom (x v-dom))
;	(y-cod (y v-cod))
;	(y-dom (y v-dom)))
;    (cond
;      ((null result)
;       (setf result
;	     (make-instance '2x2-Direct-Sum-Mapping
;			     :domain domain
;			     :codomain codomain
;			     :txx (tensor-product x-cod x-dom)
;			     :txy (tensor-product x-cod y-dom)
;			     :tyx (tensor-product y-cod x-dom)
;			     :tyy (tensor-product y-cod y-dom))))
;      (t
;       (az:type-check 2x2-Direct-Sum-Mapping result)
;       (setf (txx result) (tensor-product x-cod x-dom (txx result)))
;       (setf (txy result) (tensor-product x-cod y-dom (txy result)))
;       (setf (tyx result) (tensor-product y-cod x-dom (tyx result)))
;       (setf (tyy result) (tensor-product y-cod y-dom (tyy result))))))
;  result)

;;;=======================================================

(eval-when (compile load eval)
  (export (mapcan
	    #'(lambda (class)
		(let ((name (class-name class)))
		  (if (eq (symbol-package name) (find-package 'ca))
		      (list name)
		      ())))
	       (az:all-subclasses 'Mapping))))