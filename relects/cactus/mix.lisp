;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================

(defmethod scale! ((a Number) (t0 Matrix-Super)
		   &key (space (home-space t0)))
  (az:type-check Double-Float a)
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))

  (when (< a 0)
    (cond ((positive-definite? t0)
	   (delete-property! t0 'positive-definite?)
	   (add-property! t0 'negative-definite?))
	  ((negative-definite? t0)
	   (delete-property! t0 'negative-definite?)
	   (add-property! t0 'positive-definite?) )))
  (let ((2d (2d-array t0))
	(s (coord-start (domain t0)))
	(e (coord-end (domain t0))))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (coord-end (codomain t0)))
      (bm:v*x! 2d a :start s :end e :v-type :row :on i))) 
  t0)

(defmethod scale! ((a Number) (t0 2x2-Direct-Sum-Mapping)
		   &key (space (home-space t0)))
  (az:type-check Double-Float a)
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))

  (when (< a 0)
    (cond ((positive-definite? t0)
	   (delete-property! t0 'positive-definite?)
	   (add-property! t0 'negative-definite?))
	  ((negative-definite? t0)
	   (delete-property! t0 'negative-definite?)
	   (add-property! t0 'positive-definite?) )))
  (scale! a (txx t0)) (scale! a (txy t0))
  (scale! a (tyx t0)) (scale! a (tyy t0))
  t0)

;;;=======================================================
;;; Adding linear Mappings:
;;;=======================================================

(defmethod add ((t0 Matrix-Super) (t1 Matrix-Super)
		&key
		(space (home-space t0))
		(result (make-instance 'Matrix
				   :domain (domain t1)
				   :codomain  (codomain t1)
				   :2d-array
				   (az:make-float-array 
				     (list (dimension (domain t1))
					   (dimension (codomain t1)))))
		    result-supplied?))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (element? t1 space))
  (assert (element? result space))

  (cond
    ((eq t0 result) (add-right! t1 t0))
    ((eq t1 result) (add-right! t0 t1))
    (t (when result-supplied?
	 (az:type-check Matrix result) 
	 (assert (eql (domain t0)   (domain result)))
	 (assert (eql (codomain t0) (codomain result))))
       (az:copy t0 :result result)
       (add-right! t1 result)))
  result)

;;;-------------------------------------------------------

(defmethod add-left! ((t0 Matrix-Super) (t1 Matrix-Super))
  (add-right! t1 t0))

;;;-------------------------------------------------------

(defmethod add-right! :before ((t0 Linear-Mapping)
			       (t1 Linear-Mapping))
	   (assert (eql (domain t0) (domain t1)))
	   (assert (eql (codomain t0) (codomain t1))))

;;;-------------------------------------------------------

(defmethod add-right! ((t0 Matrix-Super) (t1 Matrix-Super))
  (let ((ar0 (2d-array t0))
	(ar1 (2d-array t1))
	(start (coord-start (domain t0)))
	(end (coord-end (domain t0))))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (coord-end (codomain t0)))
      (bm:v+v! ar1 ar0
		:start0 start :end0 end :v-type0 :row :on0 i
		:start1 start :end1 end :v-type1 :row :on1 i)))
  t1)

;;;-------------------------------------------------------

(defmethod add-right! ((t0 Diagonal-Vector) (t1 Matrix-Super))
  (let ((v (1d-array (vec t0)))
	(2d (2d-array t1)))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (coord-end (codomain t0))) 
      (incf (aref 2d i i) (aref v i))))
  t1)

(defmethod add-left! ((t0 Matrix-Super) (t1 Diagonal-Vector))
  (add-left! t1 t0)
  t1)

(defmethod add-to! ((t0 Matrix-Super)
		    (t1 Diagonal-Vector)
		    (result Matrix-Super)
		    &key
		    (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (element? t1 space))
  (assert (element? result space))
  (cond ((eq t0 result) (add-left! t0 t1))
	(t (az:copy t0 :result result)
	   (add-left! t1 result)))
  result)

(defmethod add-to! ((t0 Diagonal-Vector)
		    (t1 Matrix-Super)
		    (result Matrix-Super)
		    &key
		    (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (element? t1 space))
  (assert (element? result space))
  (cond ((eq t1 result) (add-right! t0 t1))
	(t (az:copy t1 :result result)
	   (add-left! t0 result)))
  result)

;;;=======================================================
;;; Subtracting linear Mappings:
;;;=======================================================

(defmethod sub ((t0 Matrix-Super)
		(t1 Matrix-Super)
		&key
		(space (home-space t0))
		(result (make-instance 'Matrix
				       :domain (domain t1)
				       :codomain  (codomain t1))
			result-supplied?))

  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (element? t1 space))
  (assert (element? result space))

  (cond
    ((eq t0 result) (sub-left! t0 t1))
    ((eq t1 result) (sub-right! t0 t1))
    (t (when result-supplied?
	 (az:type-check Matrix result) 
	 (assert (eql (domain t0)   (domain result)))
	 (assert (eql (codomain t0) (codomain result))))
       (az:copy t0 :result result)
       (sub-left! result t1)))
  result)

;;;-------------------------------------------------------

(defmethod sub-left! ((t0 Matrix-Super) (t1 Matrix-Super))
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (codomain t0) (codomain t1)))
  (let ((ar0 (2d-array t0))
	(ar1 (2d-array t1))
	(start (coord-start (domain t0)))
	(end (coord-end (domain t0))))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (coord-end (codomain t0)))
      (bm:v-v! ar0 ar1
		:start0 start :end0 end :v-type0 :row :on0 i
		:start1 start :end1 end :v-type1 :row :on1 i)))
  t0)

;;;-------------------------------------------------------

(defmethod sub-right! ((t0 Matrix-Super) (t1 Matrix-Super))
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (codomain t0) (codomain t1)))
  (let ((ar0 (2d-array t0))
	(ar1 (2d-array t1))
	(start (coord-start (domain t0)))
	(end (coord-end (domain t0))))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (coord-end (codomain t0)))
      (bm:v*x! ar1 -1.0d0 :start start :end  end :v-type :row :on i)
      (bm:v+v! ar1 ar0
		:start0 start :end0 end :v-type0 :row :on0 i
		:start1 start :end1 end :v-type1 :row :on1 i)))
  t1)

;;;=======================================================
;;; General Mixtures:
;;;=======================================================
;;; ???? Are eq results handled properly?

(defmethod linear-mix :before ((a0 T) (t0 Linear-Mapping)
			       (a1 T) (t1 Linear-Mapping)
			       &key
			       (space (home-space t0))
			       (result (make-element space)))
  (az:type-check Double-Float a0 a1)
  (az:type-check Vector-Space space)
  (assert (element? t0 space))
  (assert (element? t1 space))
  (assert (element? result space)))

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (t0 Tensor-Product)
		       (a1 T) (t1 Tensor-Product)
		       &key
		       (space (home-space t0))
		       (result nil))
  (declare (ignore space))
  (let ((v0-c (v-codomain t0))
	(v1-c (v-codomain t1))
	(v0-d (v-domain t0))
	(v1-d (v-domain t1)))
    (cond
     ((null result)
      (setf result (tensor-product (linear-mix a0 v0-c a1 v1-c)
				   (linear-mix a0 v0-d a1 v1-d))))
     ((typep result 'Tensor-Product) 
      (assert (eq (domain t0) (domain result)))
      (setf (v-codomain result)
	(linear-mix a0 v0-c a1 v1-c :result (v-codomain result)))
      (setf (v-domain   result)
	(linear-mix a0 v0-d a1 v1-d :result (v-domain result))))
     ((typep result 'Matrix)
      (with-borrowed-elements ((v-c (codomain t0))
			       (v-d (domain t0)))
	(linear-mix a0 v0-c a1 v1-c :result v-c)
	(linear-mix a0 v0-d a1 v1-d :result v-d)
	(let ((n (dimension (domain t0)))
	      (2d (2d-array result)))
	  (dotimes (i n)
	    (let ((alpha (aref v-c i)))
	      (bm:v<-v*x! 2d v-d alpha
			  :end0 n :v-type0 :row :on0 i
			  :end1 n))))))
     (t
      (error "Can't handle this class of result."))))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (t0 Symmetric-Tensor-Product)
		       (a1 T) (t1 Symmetric-Tensor-Product)
		       &key
		       (space (home-space t0))
		       (result (make-instance 'Symmetric-Tensor-Product
				 :domain (domain t0))))
  (declare (ignore space))
  (let ((v0 (v-domain t0))
	(v1 (v-domain t1)))
    (cond
     ((typep result 'Symmetric-Tensor-Product) 
      (setf (v-domain result)
	(linear-mix a0 v0 a1 v1 :result (v-domain result))))
     ((typep result 'Matrix)
      (let ((n (dimension (domain t0)))
	    (2d (2d-array result)))
	(with-borrowed-element (v (domain t0))
	  (linear-mix a0 v0 a1 v1 :result v)
	  (dotimes (i n)
	    (bm:v<-v*x! 2d v (aref v i)
			:end0 n :v-type0 :row :on0 i
			:end1 n)))))
     (t
      (error "Can't handle this class of result."))))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (t0 Tensor-Product)
		       (a1 T) (t1 Matrix-Super)
		       &key
		       (space (home-space t0))
		       (result nil))
  (declare (ignore space))
  (setf result (cond ((null result)
		      (scale a1 t1))
		     ((typep result 'Matrix-Super)
		      (scale a1 t1 :result result))
		     (t
		      (error "Can't handle this class of result."))))
  (let* ((2d (2d-array result))
	 (v-cod (v-codomain t0))
	 (v-dom (v-domain t0))
	 (1d-dom (1d-array v-dom))
	 (s (coord-start v-dom))
	 (e (coord-end v-dom)) 
	 (n (- e s)))
    (bm:fast-loop (i :from (coord-start v-cod) :below (coord-end v-cod))
		  (bm:v+v*x! 2d 1d-dom (* a0 (coordinate v-cod i))
			     :start0 s :end0 e :v-type0 :row :on0 i
			     :start1 0 :end1 n)))
  result)

(defmethod linear-mix ((a0 T) (t0 Matrix-Super)
		       (a1 T) (t1 Tensor-Product)
		       &key
		       (space (home-space t0))
		       (result nil result-supplied?))
  (declare (ignore space))
  (if result-supplied?
      (linear-mix a1 t1 a0 t0 :result result)
    (linear-mix a1 t1 a0 t0)))

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (t0 Symmetric-Tensor-Product)
		       (a1 T) (t1 Matrix)
		       &key
		       (space (home-space t0))
		       (result nil))
  (declare (ignore space))
  (setf result (cond ((null result) (scale a1 t1))
		     ((typep result 'Matrix) (scale a1 t1 :result result))
		     (t (error "Can't handle this class of result."))))
  (let* ((2d (2d-array result))
	 (v-dom (v-domain t0))
	 (1d-dom (1d-array v-dom))
	 (s (coord-start v-dom))
	 (e (coord-end v-dom)) 
	 (n (- e s)))
    (bm:fast-loop (i :from s :below e)
		  (bm:v+v*x! 2d 1d-dom (* a0 (coordinate v-dom i))
			     :start0 s :end0 e :v-type0 :row :on0 i
			     :end1 n)))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (t0 2x2-Direct-Sum-Mapping)
		       (a1 T) (t1 2x2-Direct-Sum-Mapping)
		       &key
		       (space (home-space t0))
		       (result nil))
  (declare (ignore space))
  (cond ((null result)
	 (setf result (make-instance '2x2-Direct-Sum-Mapping
			:codomain (codomain t0)
			:domain (domain t0))))
	((typep result '2x2-Direct-Sum-Mapping) 
	 (assert (eq (codomain t0) (codomain result)))
	 (assert (eq (domain t0) (domain result))))
	(t (error "Can't handle this class of result.")))
  (linear-mix a0 (txx t0) a1 (txx t1) :result (txx result))
  (linear-mix a0 (txy t0) a1 (txy t1) :result (txy result))
  (linear-mix a0 (tyx t0) a1 (tyx t1) :result (tyx result))
  (linear-mix a0 (tyy t0) a1 (tyy t1) :result (tyy result))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (t0 Symmetric-Tensor-Product)
		       (a1 T) (t1 2x2-Direct-Sum-Mapping)
		       &key
		       (space (home-space t0))
		       (result nil))
  (declare (ignore space))
  (cond ((null result)
	 (setf result (make-instance '2x2-Direct-Sum-Mapping
			:codomain (codomain t0)
			:domain (domain t0))))
	((typep result '2x2-Direct-Sum-Mapping) 
	 (assert (eq (codomain t0) (codomain result)))
	 (assert (eq (domain t0) (domain result))))
	(t (error "Can't handle this class of result.")))
  (let ((xspace (xspace (domain t0)))
	(yspace (yspace (domain t0)))
	(x (x (v-domain t0)))
	(y (y (v-domain t0))))
    (az:with-borrowed-instance
	(tii 'Symmetric-Tensor-Product
	     :codomain xspace :domain xspace :v-domain x)
      (linear-mix a0 tii a1 (txx t1) :result (txx result))
      (setf (slot-value tii 'domain) yspace)
      (setf (slot-value tii 'v-domain) y)
      (reinitialize-instance tii)
      (linear-mix a0 tii a1 (tyy t1) :result (tyy result)))
    (az:with-borrowed-instance
	(tij 'Tensor-Product
	     :codomain xspace :domain yspace
	     :v-codomain x :v-domain y)
      (linear-mix a0 tij a1 (txy t1) :result (txy result))
      (setf (slot-value tij 'domain) xspace)
      (setf (slot-value tij 'v-domain) x)
      (setf (slot-value tij 'codomain) yspace)
      (setf (slot-value tij 'v-codomain) y)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (tyx t1) :result (tyx result))))
  result)

(defmethod linear-mix ((a0 T) (t0 2x2-Direct-Sum-Mapping)
		       (a1 T) (t1 Symmetric-Tensor-Product)
		       &key
		       (space (home-space t0))
		       (result nil result-supplied?))
  (declare (ignore space))
  (if result-supplied?
      (linear-mix a1 t1 a0 t0 :result result)
      (linear-mix a1 t1 a0 t0)))

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (t0 Tensor-Product)
		       (a1 T) (t1 2x2-Direct-Sum-Mapping)
		       &key
		       (space (home-space t0))
		       (result nil))
  (declare (ignore space))
  (cond ((null result)
	 (setf result (make-instance '2x2-Direct-Sum-Mapping
			:codomain (codomain t0)
			:domain (domain t0))))
	((typep result '2x2-Direct-Sum-Mapping) 
	 (assert (eq (codomain t0) (codomain result)))
	 (assert (eq (domain t0) (domain result))))
	(t (error "Can't handle this class of result.")))
  (let ((xsd (xspace (domain t0)))
	(ysd (yspace (domain t0)))
	(xd (x (v-domain t0)))
	(yd (y (v-domain t0)))
	(xsc (xspace (codomain t0)))
	(ysc (yspace (codomain t0)))
	(xc (x (v-codomain t0)))
	(yc (y (v-codomain t0))))

    (az:with-borrowed-instance (tij 'Tensor-Product
	     :codomain xsc :v-codomain xc
	     :domain xsd :v-domain xd) 
      (linear-mix a0 tij a1 (txx t1) :result (txx result))

      (setf (slot-value tij 'codomain) xsc)
      (setf (slot-value tij 'v-codomain) xc)
      (setf (slot-value tij 'domain) ysd)
      (setf (slot-value tij 'v-domain) yd)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (txy t1) :result (txy result))

      (setf (slot-value tij 'codomain) ysc)
      (setf (slot-value tij 'v-codomain) yc)
      (setf (slot-value tij 'domain) xsd)
      (setf (slot-value tij 'v-domain) xd)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (tyx t1) :result (tyx result))
      
      (setf (slot-value tij 'codomain) ysc)
      (setf (slot-value tij 'v-codomain) yc)
      (setf (slot-value tij 'domain) ysd)
      (setf (slot-value tij 'v-domain) yd)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (tyy t1) :result (tyy result))))
  
  result)

(defmethod linear-mix ((a0 T) (t0 2x2-Direct-Sum-Mapping)
		       (a1 T) (t1 Tensor-Product)
		       &key
		       (space (home-space t0))
		       (result nil result-supplied?))
  (declare (ignore space))
  (if result-supplied?
      (linear-mix a1 t1 a0 t0 :result result)
      (linear-mix a1 t1 a0 t0)))



