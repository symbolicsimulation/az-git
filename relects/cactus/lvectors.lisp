;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;;		      LVector-Space
;;;=======================================================
;;; The standard (and the first) instantiable Vector space:

(defclass LVector-Space-Super (Euclidean-Vector-Space) ())
(defclass LVector-Space (LVector-Space-Super) ())

;;;-------------------------------------------------------

(defmethod print-object ((vs LVector-Space) str)
  (format str "(LV ~d)" (dimension vs)))

;;;-------------------------------------------------------

(defmethod scalar-field ((sp LVector-Space)) 'Double-Float)

(defmethod dual-space ((sp LVector-Space)) sp)

;;;-------------------------------------------------------

(defmethod make-element ((sp LVector-Space)
			 &rest options)
  (make-instance
    'Lvector
    :home-space sp 
    :1d-array (if (null options)
		  (az:make-float-array (dimension sp))
		  (apply #'az:make-float-array (dimension sp) options))))

(defmethod make-zero-element ((sp LVector-Space) &rest options)
  (declare (ignore options))
  (make-element sp :initial-element 0.0d0))

;;;-------------------------------------------------------

(defmethod borrow-canonical-basis-element ((sp LVector-Space) (i Integer)
					   &rest options)
  (declare (ignore options))
  (let ((v (borrow-element sp)))
    (zero! v)
    (setf (coordinate v i) 1.0d0)
    v))  

;;;-------------------------------------------------------
;;; To ensure there is only one LVector-Space space of a given dimension.

(defparameter *lvector-space-table* (make-hash-table))

(defun lvector-space (dim)
  (let ((sp (gethash dim *lvector-space-table* nil)))
    (when (null sp)
      (setf sp (make-instance 'Lvector-Space :dimension dim))
      (setf (gethash dim *lvector-space-table*) sp))
    sp))

;;;-------------------------------------------------------

(defmethod block-subspace-class ((sp LVector-Space))
  'LVector-Block-Subspace)

(defmethod canonical-subspace-class ((sp LVector-Space))
  'LVector-Canonical-Subspace)

;;;=======================================================
;;;	    LVector-Subspace
;;;=======================================================
;;; another frequently used vector space, though mostly
;;; invisible to the user.

(defclass LVector-Subspace (LVector-Space-Super)
     ((parent-space
	:type LVector-Space
	:reader parent-space
	:initarg :parent-space)))

;;;-------------------------------------------------------

(defmethod dual-space ((sp LVector-Subspace)) sp) 

;;;-------------------------------------------------------

(defmethod make-element ((sp LVector-Subspace)
			 &rest options)
  (declare (ignore options))
  (make-instance (standard-representation-class sp)
		 :home-space sp
		 :1d-array (az:make-float-array (dimension sp))))

(defmethod make-zero-element ((sp LVector-Subspace)
			      &rest options)
  (declare (ignore options))
  (make-instance (standard-representation-class sp)
		 :home-space sp
		 :1d-array (az:make-float-array (dimension sp)
						:initial-element 0.0d0)))

;;;-------------------------------------------------------

(defmethod borrow-canonical-basis-element ((sp LVector-Subspace)
					   (i Integer)
					   &rest options)
  (declare (ignore options))
  (let ((v (borrow-element sp)))
    (zero! v)
    (setf (coordinate v i) 1.0d0)
    v))  

;;;=======================================================
;;;	    LVector-Block-Subspace
;;;=======================================================
;;; another frequently used vector space, though mostly
;;; invisible to the user.

(defclass LVector-Block-Subspace (Block-SubSpace
				   LVector-Subspace)
     ()) 

(defmethod standard-representation-class ((sp LVector-Block-Subspace))
  'LVector-Block)

;;;=======================================================
;;;	    LVector-Canonical-Subspace
;;;=======================================================
;;; another frequently used vector space, though mostly
;;; invisible to the user.

(defclass LVector-Canonical-Subspace (Canonical-SubSpace
				       LVector-Subspace)
     ((parent-space
	:type LVector-Space
	:reader parent-space
	:initarg :parent-space)))

(defmethod standard-representation-class ((sp LVector-Canonical-Subspace))
  'LVector-Canonical)

;;;=======================================================
;;;		      Lvector-Super
;;;=======================================================
;;; An abstract superclass for classes that define representations for
;;; elements of LVector-Space spaces or LVector-Block-Subspace's.  The
;;; representation is based on a Lisp vector holding a contiguous block
;;; of the coordinates of the vector in the standard basis.

(defclass Lvector-Super (Euclidean-Vector)
     ((1d-array
	:type az:Float-Vector
	:accessor 1d-array
	:initarg :1d-array)

      (home-space
	:type  LVector-Space-Super
	:accessor home-space
	:initarg :home-space)))

;;;-------------------------------------------------------
;;; range of non-zero coordinates:
;;; 
;;; The coordinates from 0 below <start> are implicit zeros.
;;; The coordinates from <end> below (dimension (parent-space v0))
;;; are implicit zeros.
;;; forward these to the <home-space>:

(defmethod coord-start ((v Lvector-Super)) (coord-start (home-space v)))
(defmethod coord-end ((v Lvector-Super)) (coord-end (home-space v)))

;;;------------------------------------------------------------

#+symbolics
(defmethod format-object ((v Lvector-Super)
			  &optional (stream *standard-output*))
  (print (class-of v) stream)
  (format stream "~%An element of ~s" (home-space v)) 
  (format-object (1d-array v) stream)) 

;;;------------------------------------------------------------

(defmethod az:new-copy ((v0 Lvector-Super))
  (let ((result (make-element (home-space v0))))
    (az:copy (1d-array v0) :result (1d-array result))
    result))

;;;------------------------------------------------------------

(defmethod project ((v0 Lvector-Super)
		    (sp LVector-Space)
		    &key
		    (result (make-zero-element sp)
			    result-supplied?))
  (assert (eq (home-space v0) sp))
  (when result-supplied? (assert (eq (home-space result) sp)))
  (az:copy v0 :result result))

(defmethod project ((v0 Lvector-Super)
		    (sp LVector-Block-Subspace)
		    &key
		    (result (make-zero-element sp)
			    result-supplied?))
  (assert (subspace? sp (home-space v0)))
  (when result-supplied? (assert (eq (home-space result) sp)))
  (let ((s (- (coord-start result) (coord-start v0)))
	(e (- (coord-end result) (coord-start v0))))
    (bm:v<-v! (1d-array result) (1d-array v0)
	       :end0 (- e s) :start1 s :end1 e))
  result)

;;;------------------------------------------------------------

(defmethod embed ((v0 Lvector-Super)
		  (sp LVector-Space)
		  &key
		  (result (make-zero-element sp)
			  result-supplied?))
  (when result-supplied?
    (assert (element? v0 sp))
    (assert (eq (home-space result) sp)))
  (let* ((s (coord-start v0))
	 (e (coord-end v0))
	 (n (- e s))
	 (d (dimension result))
	 (1d (1d-array result)))
    (when (> s 0) (bm:v<-x! 1d 0.0d0 :end s))
    (bm:v<-v! 1d (1d-array v0)
	       :start0 s :end0 e
	       :end1 n)
    (when (> d e) (bm:v<-x! 1d 0.0d0 :start e :end d)))
  result)

(defmethod embed ((v0 Lvector-Super)
		  (sp LVector-Block-Subspace)
		  &key
		  (result (make-zero-element sp)
			  result-supplied?))
  (when result-supplied?
    (assert (element? v0 sp))
    (assert (eq (home-space result) sp)))
  (let* ((s (- (coord-start v0) (coord-start result)))
	 (e (- (coord-end v0) (coord-start result)))
	 (n (- e s))
	 (d (dimension result))
	 (1d (1d-array result)))
    (when (> s 0) (bm:v<-x! 1d 0.0d0 :end s))
    (bm:v<-v! 1d (1d-array v0)
	       :start0 s :end0 e
	       :end1 n)
    (when (> d e) (bm:v<-x! 1d 0.0d0 :start e :end d)))
  result)

;;;------------------------------------------------------------

(defmethod fill-randomly! ((v0 Lvector-Super)
			   &rest options
			   &key (min 0.0d0) (max 1.0d0)
			   &allow-other-keys)
  (declare (ignore options))
  (bm:fill-random-vector (1d-array v0) :min min :max max)
  v0)

;;;------------------------------------------------------------

(defmethod coordinate ((v0 Lvector-Super) i
		       &key (coordinate-system :standard))
  (declare (ignore coordinate-system))
  (aref (1d-array v0) (- i (coord-start v0))))

(defmethod (setf coordinate) (new (v0 Lvector-Super) i
		       &key (coordinate-system :standard))
  (declare (ignore coordinate-system))
  (setf (aref (1d-array v0) (- i (coord-start v0))) new))

;;;-------------------------------------------------------

(defmethod scale! (a (v0 Lvector-Super) &key (space (home-space v0)))
  (az:type-check Double-Float a)
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (bm:v*x! (1d-array v0) a :end (dimension v0)) 
  v0)

(defmethod zero! ((v0 Lvector-Super)
		  &key (space (home-space v0)))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (bm:v<-x! (1d-array v0) 0.0d0 :end (dimension v0))
  v0)

;;;------------------------------------------------------------

(defmethod linear-mix ((a0 T) (v0 Lvector-Super)
		       (a1 T) (v1 Lvector-Super)
		       &key
		       (space (spanning-space (home-space v0)
					      (home-space v1)))
		       (result (make-element space)))
  (az:type-check Double-Float a0 a1)
  (az:type-check Euclidean-Vector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (let ((v0-borrowed? nil)
	(v1-borrowed? nil)
	(result-space (home-space result)))
    (unless (eq (home-space v0) result-space)
      (setf v0-borrowed? t)
      (setf v0 (embed v0 result-space :result (borrow-element result-space))))
    (unless (eq (home-space v1) result-space)
      (setf v1-borrowed? t)
      (setf v1 (embed v1 result-space :result (borrow-element result-space))))
    (bm:vector-linear-mix a0 (1d-array v0) a1 (1d-array v1)
			  :result (1d-array result))
    (when v0-borrowed? (return-element v0 result-space))
    (when v1-borrowed? (return-element v1 result-space)))
  result)

;;;------------------------------------------------------------

(defmethod elementwise-product! ((v0 Lvector-Super) (v1 Lvector-Super))
  (assert (subspace? (home-space v0) (home-space v1)))
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (ds (- s0 s1))
	 (n (- (coord-end v0) s0))
	 (n+ds (+ ds n))
	 (va0 (1d-array v0))
	 (va1 (1d-array v1)))
    (bm:v<-x! va1 0.0d0 :end ds)
    (bm:v*v! va1 va0
	      :start0 ds :end0 n+ds
	      :end1 n)
    (bm:v<-x! va1 0.0d0 :start n+ds :end (coord-end v1))))

(defmethod inner-product ((v0 Lvector-Super) (v1 Lvector-Super)
			  &key (space (spanning-space (home-space v0)
						      (home-space v1))))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v.v (1d-array v0) (1d-array v1)
	       :start0 (- smax s0) :end0 (- emin s0)
	       :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l1-dist  ((v0 Lvector-Super) (v1 Lvector-Super)
		     &key
		     (space (spanning-space (home-space v0)
						 (home-space v1))))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v-l1-dist (1d-array v0) (1d-array v1)
		     :start0 (- smax s0) :end0 (- emin s0)
		     :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l2-dist2 ((v0 Lvector-Super) (v1 Lvector-Super)
		     &key (space (spanning-space (home-space v0)
						 (home-space v1))))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v-l2-dist2 (1d-array v0) (1d-array v1)
		      :start0 (- smax s0) :end0 (- emin s0)
		      :start1 (- smax s1) :end1 (- emin s1)))) )

(defmethod sup-dist ((v0 Lvector-Super) (v1 Lvector-Super)
		     &key
		     (space (spanning-space (home-space v0)
					    (home-space v1))))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v-sup-dist (1d-array v0) (1d-array v1)
		     :start0 (- smax s0) :end0 (- emin s0)
		     :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l1-norm  ((v0 Lvector-Super)
		     &key
		     (space (home-space v0)))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (let ((v01d (1d-array v0)))
    (bm:v-l1-norm v01d :end (length v01d))))

(defmethod l2-norm2 ((v0 Lvector-Super)
		     &key
		     (space (home-space v0)))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (let ((v01d (1d-array v0)))
    (bm:v-l2-norm2 v01d :end (length v01d))))

(defmethod sup-norm ((v0 Lvector-Super)
		     &key
		     (space (home-space v0)))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (let ((v1d (1d-array v0)))
    (bm:v-abs-max v1d :end (length v1d))))

;;;------------------------------------------------------------

(defmethod sub ((v0 Lvector-Super)
		(v1 Lvector-Super)
		&key
		(space (spanning-space (home-space v0) (home-space v1)))
		(result (make-element space)))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (sub-to! v0 v1 result :space space))

(defmethod sub-to! ((v0 Lvector-Super)
		    (v1 Lvector-Super)
		    (result Lvector-Super)
		    &key (space (home-space v0)))
  (az:type-check LVector-Space-Super space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (linear-mix 1.0d0 v0 -1.0d0 v1 :space space :result result)
  result)

;;;=======================================================
;;;		      Lvector
;;;=======================================================
;;; The representation class for the elements of a
;;; LVector-Space space

(defclass Lvector (Lvector-Super)
     ((home-space
	:type LVector-Space 
	:accessor home-space
	:initarg :home-space)))

;;;------------------------------------------------------------

(defmethod element? ((p Lvector) (sp LVector-Space))
  (eq (home-space p) sp)) 

;;;------------------------------------------------------------

#+symbolics
(defmethod format-object ((v Lvector)
			  &optional (stream *standard-output*))
  (print (class-of v) stream)
  (format stream "~%An element of ~s" (home-space v)) 
  (format-object (1d-array v) stream)) 

;;;------------------------------------------------------------

(defun make-lvector-from-array (vec)
  (make-instance 'Lvector
		 :home-space (LVector-Space (length vec))
		 :1d-array vec))

(defmacro lvector (&rest coords)
  `(make-instance 'Lvector
		  :home-space (LVector-Space ,(length coords))
		  :1d-array (vector ,@coords)
		  ))

;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((v0 Lvector) (result Lvector))
  (eq (home-space v0) (home-space result)))
  
(defmethod az:copy-to! ((v0 Lvector)
		     (result Lvector))
  (az:copy (1d-array v0) :result (1d-array result))
  result)

;;;=======================================================
;;;	       LVector-Block
;;;=======================================================
;;; a class for elements of a LVector-Block-Subspace

(defclass LVector-Block (Lvector-Super)
     ((home-space
	:type LVector-Block-Subspace
	:accessor home-space
	:initarg :home-space)))

;;;------------------------------------------------------------

(defmethod element? ((p LVector-Block) (sp LVector-Space))
  (eq (parent-space (home-space p)) sp)) 

(defmethod element? ((p LVector-Block) (sp LVector-Block-Subspace))
  (subspace? (home-space p) sp))

;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((v0 LVector-Block) (result LVector-Block))
  (<= (dimension (home-space v0)) (length (1d-array result))))
  
(defmethod az:copy-to! ((v0 LVector-Block)
			(result LVector-Block))
  (setf (home-space result) (home-space v0))
  (bm:v<-v! (1d-array result) (1d-array v0)
	    :end0 (dimension (home-space v0))
	    :end1 (dimension (home-space v0)))
  result)

(defmethod az:verify-copy-result? ((v0 LVector-Block) (result Lvector))
  (element? v0 (home-space result)))

(defmethod az:copy-to! ((v0 LVector-Block)
			(result Lvector))
  (let ((s (coord-start v0))
	(e (coord-end v0))
	(n (dimension result)))
    (when (> s 0) (bm:v<-x! (1d-array result) 0.0d0 :end s))
    (bm:v<-v! (1d-array result) (1d-array v0)
	      :start0 s :end0 e
	      :end1 (- e s))
    (when (< e n) (bm:v<-x! (1d-array result) 0.0d0 :start e :end n)))  
  result) 

;;;;=======================================================
;;; Projections on subspaces:
;;;=======================================================

(defmethod ortho-project ((v Lvector)
			  (subspace LVector-Block-Subspace)
			  &key
			  (result (make-element subspace)
				  result-supplied?))
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (bm:v<-v! (1d-array result) (1d-array v)
	     :end0 (dimension subspace)
	     :start1 (coord-start subspace) :end1 (coord-end subspace)))

(defmethod scaled-ortho-project ((v Lvector)
				 (subspace LVector-Block-Subspace)
				 a
				 &key
				 (result (make-element subspace)
					 result-supplied?))
  (az:type-check Double-Float a)
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (bm:v<-v*x!
    (1d-array result) (1d-array v) a
    :end0 (dimension subspace)
    :start1 (coord-start subspace) :end1 (coord-end subspace)))

;;; trivial cases for compatibility:

(defmethod ortho-project ((v LVector-Block)
			  (subspace LVector-Block-Subspace)
			  &key
			  (result (make-element subspace)
				  result-supplied?))
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (let* ((s (coord-start v))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (va (1d-array v))
	 (ra (1d-array result)))
    (bm:v<-v! ra va :end0 n :start1 ds :end1 n+ds)))

(defmethod scaled-ortho-project ((v LVector-Block)
				 (subspace LVector-Block-Subspace)
				 a
				 &key
				 (result (make-element subspace)
					 result-supplied?))
  (az:type-check Double-Float a)
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (let* ((s (coord-start v))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (va (1d-array v))
	 (ra (1d-array result)))
    (bm:v<-v*x! ra va a :end0 n :start1 ds :end1 n+ds)))


(defmethod ortho-project ((v Lvector)
			  (space LVector-Space)
			  &key
			  (result nil result-supplied?))
  (if result-supplied?
      (az:copy v :result result)
      (az:copy v)))

(defmethod scaled-ortho-project ((v Lvector)
				 (space LVector-Space)
				 a
				 &key
				 (result nil result-supplied?))
  (if result-supplied?
      (scale a v :result result)
      (scale a v)))

;;;=======================================================
;;;	       LVector-Canonical
;;;=======================================================
;;; a class for elements of a LVector-Canonical-Subspace

(defclass LVector-Canonical (Lvector-Super)
     ((home-space
	:type LVector-Canonical-Subspace
	:accessor home-space
	:initarg :home-space)))

;;;------------------------------------------------------------

(defmethod element? ((p LVector-Canonical) (sp LVector-Space))
  (eq (parent-space (home-space p)) sp)) 

(defmethod element? ((p LVector-Canonical) (sp LVector-Canonical-Subspace))
  (subspace? (home-space p) sp))

;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((v0 LVector-Canonical)
				   (result LVector-Canonical))
  (<= (dimension (home-space v0)) (length (1d-array result))))
  
(defmethod az:copy-to! ((v0 LVector-Canonical)
			(result LVector-Canonical))
  (setf (home-space result) (home-space v0))
  (bm:v<-v! (1d-array result) (1d-array v0)
	    :end0 (dimension (home-space v0))
	    :end1 (dimension (home-space v0)))
  result)

(defmethod az:verify-copy-result? ((v0 LVector-Canonical) (result Lvector))
  (element? v0 (home-space result)))

(defmethod az:copy-to! ((v0 LVector-Canonical)
			(result Lvector))
  (let ((s (coord-start v0))
	(e (coord-end v0))
	(n (dimension result)))
    (when (> s 0) (bm:v<-x! (1d-array result) 0.0d0 :end s))
    (bm:v<-v! (1d-array result) (1d-array v0)
	      :start0 s :end0 e :end1 (- e s))
    (when (< e n) (bm:v<-x! (1d-array result) 0.0d0 :start e :end n)))  
  result) 

;;;=======================================================
;;; Projections on subspaces:
;;;=======================================================

#|| --- never finished or corrupted somehow?
(defmethod ortho-project ((v Lvector-Super)
			  (subspace LVector-Canonical-Subspace)
			  &key
			  (result (make-element subspace)
				  result-supplied?))
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (loop
      for index in (index-list subspace)
      for i from 0))
||#

(defmethod scaled-ortho-project ((v Lvector)
				 (subspace LVector-Canonical-Subspace)
				 a
				 &key
				 (result (make-element subspace)
					 result-supplied?))
  (az:type-check Double-Float a)
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (bm:v<-v*x!
    (1d-array result) (1d-array v) a
    :end0 (dimension subspace)
    :start1 (coord-start subspace) :end1 (coord-end subspace)))

;;; trivial cases for compatibility:

(defmethod ortho-project ((v LVector-Canonical)
			  (subspace LVector-Canonical-Subspace)
			  &key
			  (result (make-element subspace)
				  result-supplied?))
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (let* ((s (coord-start v))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (va (1d-array v))
	 (ra (1d-array result)))
    (bm:v<-v! ra va :end0 n :start1 ds :end1 n+ds)))

(defmethod scaled-ortho-project ((v LVector-Canonical)
				 (subspace LVector-Canonical-Subspace)
				 a
				 &key
				 (result (make-element subspace)
					 result-supplied?))
  (az:type-check Double-Float a)
  (assert (element? v (parent-space subspace)))
  (when result-supplied? (assert (eq (home-space result) subspace)))
  (let* ((s (coord-start v))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (va (1d-array v))
	 (ra (1d-array result)))
    (bm:v<-v*x! ra va a :end0 n :start1 ds :end1 n+ds)))

;;;------------------------------------------------------------

#+symbolics
(defmethod format-object ((v Lvector-Canonical)
			  &optional (stream *standard-output*))
  (print (class-of v) stream)
  (format stream "~%An element of ~s" (home-space v)) 
  (format-object (index-list (home-space v)) stream)
  (format-object (1d-array v) stream)) 

;;;------------------------------------------------------------

(defmethod az:new-copy ((v0 Lvector-Canonical))
  (let ((result (make-element (home-space v0))))
    (az:copy (1d-array v0) :result (1d-array result))
    result))

;;;------------------------------------------------------------
;;; slow (because they use <coordinate>) but simple and reliable
;;; methods for projection and embedding:

(defmethod project ((v0 Lvector)
		  (sp LVector-Canonical-SubSpace)
		  &key
		  (result (make-element sp)
			  result-supplied?))
  (when result-supplied?
    (assert (element? result sp))
    (assert (subspace? sp (home-space v0))))
  (dolist (index (index-list sp))
    (setf (coordinate result index) (coordinate result index)))
  result)

(defmethod embed ((v0 Lvector-Canonical)
		  (sp LVector-Space)
		  &key
		  (result (make-zero-element sp)
			  result-supplied?))
  (when result-supplied?
    (assert (element? v0 sp))
    (assert (eq (home-space result) sp))
    (zero! result))
  (dolist (index (index-list (home-space v0)))
    (setf (coordinate result index) (coordinate result index)))
  result)

;;;------------------------------------------------------------

(defmethod fill-randomly! ((v0 Lvector-Canonical)
			   &rest options
			   &key (min 0.0d0) (max 1.0d0)
			   &allow-other-keys)
  (declare (ignore options))
  (bm:fill-random-vector (1d-array v0) :min min :max max)
  v0)

;;;------------------------------------------------------------

(defmethod coordinate ((v0 Lvector-Canonical) i
		       &key
		       (coordinate-system :standard))
  (declare (ignore coordinate-system))
  (aref (1d-array v0) (position i (index-list (home-space v0)))))

(defmethod (setf coordinate) ((new T) (v0 Lvector-Canonical) i
			      &key
			      (coordinate-system :standard))
  (declare (ignore coordinate-system))
  (setf (aref (1d-array v0) (position i (index-list (home-space v0)))) new))

;;;-------------------------------------------------------

(defmethod scale! ((a T) (v0 Lvector-Canonical)
		   &key
		   (space (home-space v0)))
  (az:type-check Double-Float a)
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (bm:v*x! (1d-array v0) a :end (dimension v0)) 
  v0)

(defmethod zero! ((v0 Lvector-Canonical)
		  &key (space (home-space v0)))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (bm:v<-x! (1d-array v0) 0.0d0 :end (dimension v0))
  v0)

;;;------------------------------------------------------------

(defmethod linear-mix ((a0 T) (v0 Lvector-Canonical)
		       (a1 T) (v1 Lvector-Canonical)
		       &key
		       (space (spanning-space (home-space v0)
					      (home-space v1)))
		       (result (make-element space)))
  (az:type-check Double-Float a0 a1)
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (let ((v0-borrowed? nil)
	(v1-borrowed? nil)
	(result-space (home-space result)))
    (unless (eq (home-space v0) result-space)
      (setf v0-borrowed? t)
      (setf v0 (embed v0 result-space :result (borrow-element result-space))))
    (unless (eq (home-space v1) result-space)
      (setf v1-borrowed? t)
      (setf v1 (embed v1 result-space :result (borrow-element result-space))))
    (bm:vector-linear-mix a0 (1d-array v0) a1 (1d-array v1)
			  :result (1d-array result))
    (when v0-borrowed? (return-element v0 result-space))
    (when v1-borrowed? (return-element v1 result-space)))
  result)

;;;------------------------------------------------------------

(defmethod inner-product ((v0 Lvector-Canonical) (v1 Lvector-Canonical)
			  &key
			  (space (spanning-space (home-space v0)
						 (home-space v1))))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v.v (1d-array v0) (1d-array v1)
	      :start0 (- smax s0) :end0 (- emin s0)
	      :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l1-dist  ((v0 Lvector-Canonical) (v1 Lvector-Canonical)
		     &key
		     (space (spanning-space (home-space v0)
					    (home-space v1))))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v-l1-dist (1d-array v0) (1d-array v1)
		    :start0 (- smax s0) :end0 (- emin s0)
		    :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l2-dist2 ((v0 Lvector-Canonical) (v1 Lvector-Canonical)
		     &key
		     (space (spanning-space (home-space v0)
					    (home-space v1))))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v-l2-dist2 (1d-array v0) (1d-array v1)
		     :start0 (- smax s0) :end0 (- emin s0)
		     :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod sup-dist ((v0 Lvector-Canonical)
		     (v1 Lvector-Canonical)
		     &key
		     (space (spanning-space (home-space v0)
					    (home-space v1))))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  
  (let* ((s0 (coord-start v0))
	 (s1 (coord-start v1))
	 (smax (max s0 s1))
	 (emin (min (coord-end v0) (coord-end v1))))
    (unless (>= smax emin)
      (bm:v-sup-dist (1d-array v0) (1d-array v1)
		     :start0 (- smax s0) :end0 (- emin s0)
		     :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l1-norm  ((v0 Lvector-Canonical)
		     &key
		     (space (home-space v0)))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (bm:v-l1-norm (1d-array v0)))

(defmethod l2-norm2 ((v0 Lvector-Canonical)
		     &key
		     (space (home-space v0)))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (bm:v-l2-norm2 (1d-array v0)))

(defmethod sup-norm ((v0 Lvector-Canonical)
		     &key
		     (space (home-space v0)))
  (az:type-check LVector-Space space)
  (assert (element? v0 space))
  (bm:v-abs-max (1d-array v0)))

;;;------------------------------------------------------------

(defmethod sub ((v0 Lvector-Canonical)
		(v1 Lvector-Canonical)
		&key
		(space (spanning-space (home-space v0) (home-space v1)))
		(result (make-element space)))
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (sub-to! v0 v1 result :space space))

(defmethod sub-to! ((v0 Lvector-Canonical)
		    (v1 Lvector-Canonical)
		    (result Lvector-Canonical)
		    &key (space (home-space v0)))
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (linear-mix 1.0d0 v0 -1.0d0 v1 :space space :result result)
  result)



