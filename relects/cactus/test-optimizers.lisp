;;;-*-Package: Cactus; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Cactus)

(eval-when (compile load eval)
  (export '(*amoeba-opt*
	    *sd-opt* *dsd-opt*
	    *cg-opt* *dcg-opt*
	    *idfp-opt* *didfp-opt*
	    *ibfgs-opt* *dibfgs-opt*
	    )))

;;;============================================================
;;; sample instances for experimentation:
 
(defparameter *amoeba-opt* (make-instance 'Amoeba-Optimizer))

(defparameter *sd-opt*  (make-instance 'Steepest-Descent-Optimizer
			  :animate-1d-bracket? (find-package :Chart)
			  :animate-path? (find-package :Chart)
			  :animate-function? (find-package :Chart)))

(defparameter *dsd-opt* (make-instance 'Discrete-Steepest-Descent-Optimizer
			  :animate-1d-bracket? (find-package :Chart)
			  :animate-path? (find-package :Chart)
			  :animate-function? (find-package :Chart)))

(defparameter *cg-opt*  (make-instance 'Conjugate-Gradient-Optimizer
			  :animate-path? (find-package :Chart)
			  :animate-function? (find-package :Chart)
			  :animate-1d-bracket? (find-package :Chart)
			  :1d-absolute-tolerance 1.0d-7
			  :1d-relative-tolerance 1.0d-2))

(defparameter *dcg-opt* (make-instance 'Discrete-Conjugate-Gradient-Optimizer
			  :animate-1d-bracket? (find-package :Chart)
			  :animate-path? (find-package :Chart)
			  :animate-function? (find-package :Chart)))

(defparameter *idfp-opt* (make-instance 'Inverse-DFP-Optimizer
			   :animate-1d-bracket? (find-package :Chart)
			   :animate-path? (find-package :Chart)
			   :animate-function? (find-package :Chart)))

(defparameter *didfp-opt* (make-instance 'Discrete-Inverse-DFP-Optimizer
			    :animate-1d-bracket? (find-package :Chart)
			    :animate-path? (find-package :Chart)
			    :animate-function? (find-package :Chart)))

(defparameter *ibfgs-opt* (make-instance 'Inverse-BFGS-Optimizer
			    :animate-1d-bracket? (find-package :Chart)
			    :animate-path? (find-package :Chart)
			    :animate-function? (find-package :Chart)
			    :1d-absolute-tolerance 1.0d-7
			    :1d-relative-tolerance 1.0d-2))

(defparameter *dibfgs-opt* (make-instance 'Discrete-Inverse-BFGS-Optimizer
			     :animate-1d-bracket? (find-package :Chart)
			     :animate-path? (find-package :Chart)
			     :animate-function? (find-package :Chart)))

;;;============================================================
