;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;; versions of equality for matrices---this should be unified
;;; with some standards for copying and initialization (private slots?)

(defmethod == ((x0 Number) (x2 Number)) (= x0 x2))

(defmethod == ((a0 Array) (a1 Array))
  (ecase (array-rank a0)
    (1 (ecase (array-rank a1)
	 (1 (and (= (length a0) (length a1))
		 (every #'= a0 a1)))
	 (2 (and (= 1 (array-dimension a1 1))
		 (= (length a0) (array-dimension a1 0))
		 (loop for i from 0 below (length a0)
		       always (= (aref a0 i) (aref a1 i 0)))))))
    (2 (ecase (array-rank a1)
	 (1 (and (= 1 (array-dimension a0 1))
		 (= (length a1) (array-dimension a0 0))
		 (loop for i from 0 below (length a1)
		       always (= (aref a0 i 0) (aref a1 i)))))
	 (2 (and (= (array-dimension a0 0) (array-dimension a1 0))
		 (= (array-dimension a0 1) (array-dimension a1 1))
		 (loop with nrows = (array-dimension a0 0)
		       with ncols = (array-dimension a0 1)
		       for i from 0 below nrows
		       always (loop for j from 0 below ncols
				    always (= (aref a0 i j)
					      (aref a1 i j))))))))))

(defmethod == ((t0 Linear-Mapping) (t1 Linear-Mapping))
  "Exact equality."
  (and (eql (domain t0) (domain t1))
       (eql (codomain t0) (codomain t1))
       (== (copy-to-array t0) (copy-to-array t1))))

(defmethod == ((t0 Matrix) (t1 Matrix))
  "Exact equality."
  (and (eql (domain t0) (domain t1))
       (eql (codomain t0) (codomain t1))
       (== (2d-array t0) (2d-array t1))))

;;;-------------------------------------------------------

(defmethod ~= ((x0 Number) (x1 Number) &optional (eps 1.0d0))
  (bm:small? (- x0 x1) (* (+ 1.0d0 (abs x0) (abs x1)) eps)))

(defmethod ~= ((v0 Vector) (v1 Vector) &optional (eps 1.0d0))
  (and (= (length v0) (length v1))
       (let ((norm  (* eps (float (length v0))
		       (+ 1.0d0 (bm:v-abs-max v0) (bm:v-abs-max v1)))))
	 (every #'(lambda (x0 x1) (bm:small? (- x0 x1) norm)) v0 v1))))

(defmethod ~= ((a0 Array) (a1 Array) &optional (eps 1.0d0))
  ;; a "temporary" restriction:
  (assert (= 2 (array-rank a0) (array-rank a1)))
  (and (= (array-dimension a0 0) (array-dimension a1 0))
       (= (array-dimension a0 1) (array-dimension a1 1))
       (block :test
	 (let* ((n (array-dimension a0 0))
		(m (array-dimension a0 1))
		(norm (* eps (float (+ n m))
			 (+ 1.0d0 (bm:abs-max-2d a0) (bm:abs-max-2d a1)))))
	   (dotimes (i n t)
	     (dotimes (j m)
	       (unless (bm:small?  (- (aref a0 i j) (aref a1 i j)) norm)
		 (print (- (aref a0 i j) (aref a1 i j)))
		 (print norm)
		 (return-from :test nil))))))))

(defmethod ~= ((v0 Lvector-Super) (v1 Lvector-Super)
	       &optional (eps 1.0d0))
  (with-borrowed-element (v2 (spanning-space (home-space v0) (home-space v1)))
    (sub v0 v1 :result v2)
    (bm:small? (sup-norm v2) (* eps (+ (sup-norm v0) (sup-norm v1))))))

(defmethod ~= ((t0 Linear-Mapping) (t1 Linear-Mapping)
	       &optional
	       (eps 1.0d0))
  "Approximate equality."
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (codomain t0) (codomain t1)))
  (~= (copy-to-array t0) (copy-to-array t1) eps))

(defmethod ~= ((t0 Matrix) (t1 Matrix)
	       &optional
	       (eps 1.0d0))
  "Approximate equality."
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (codomain t0) (codomain t1)))
  (~= (2d-array t0) (2d-array t1) eps))

;;;=======================================================

(defmethod sup-norm ((t0 Linear-Mapping)
		     &key
		     (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  
  (with-borrowed-element (v0 (domain t0))
    (loop for i from 0 below (dimension (codomain t0))
	  maximize (sup-norm (row t0 i :result v0) :space (domain t0)))))

(defmethod l1-norm ((t0 Linear-Mapping)
		    &key
		     (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  
  (with-borrowed-element (v0 (domain t0))
    (loop for i from 0 below (dimension (codomain t0))
	  sum (l1-norm (row t0 i :result v0) :space (domain t0)))))

(defmethod avg-l1-norm ((t0 Linear-Mapping)
			&key
		     (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (plusp (cardinality t0)))
  (/ (l1-norm t0 :space space) (cardinality t0)))

(defmethod l2-norm ((t0 Linear-Mapping)
		    &key
		    (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (sqrt (l2-norm2 t0 :space space)))

(defmethod avg-l2-norm ((t0 Linear-Mapping)
			&key
			(space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (plusp (cardinality t0)))
  (/ (l2-norm t0 :space space) (cardinality t0)))

(defmethod l2-norm2 ((t0 Linear-Mapping)
		     &key
		     (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  
  (with-borrowed-element (v0 (domain t0))
    (loop for i from 0 below (dimension (codomain t0))
	  sum (l2-norm2 (row t0 i :result v0) :space (domain t0)))))

;;;=======================================================

(defvar *identity-matrix-resource* nil)

;;;-------------------------------------------------------

(defmethod borrow-identity-matrix ((vspace Vector-Space))
  (declare (special *identity-matrix-resource*))
  (let ((t0 (find-if #'(lambda (t0) (eql vspace (domain t0)))
		     *identity-matrix-resource*)))
    (if (null t0)
	(setf t0 (make-identity-matrix vspace))
	(setf *identity-matrix-resource*
	      (delete t0 *identity-matrix-resource*)))
    t0))

;;;-------------------------------------------------------

(defun return-identity-matrix (t0)
  (declare (special *identity-matrix-resource*))
  (push t0 *identity-matrix-resource*))

;;;-------------------------------------------------------
;;;
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>. 

(defmacro with-borrowed-identity-matrix ((name vspace) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym))) 
    `(let* ((,return-name (borrow-identity-matrix ,vspace))
	    (,name ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-identity-matrix ,return-name)))))

;;;=======================================================

(defmethod sup-dist-from-identity-map ((t0 Matrix))
  (with-borrowed-identity-matrix (t1 (domain t0)) (sup-dist t0 t1)))

#||
(defmethod sup-dist-from-identity-map ((a Array))
  (assert (= 2 (array-rank a)))
  (loop for i from 0 below (array-dimension a 0)
	maximize (loop for j from 0 below (array-dimension a 1)
		       if (= i j) maximize (abs (- 1.0d0 (aref a i j)))
		     else       maximize (abs (aref a i j)))))
||#


(defmethod sup-dist-from-identity-map ((a Array))
  (assert (= 2 (array-rank a)))
  (let ((m (array-dimension a 0))
	(n (array-dimension a 1))
	(sup 0.0d0))
    (dotimes (i m)
      (dotimes (j n)
	(let ((x (if (= i j)
		     (abs (- 1.0d0 (aref a i i)))
		   (abs (aref a i j)))))
	  (when (> x sup) (setf sup x)))))
    sup))

;;;=======================================================

(defmethod l1-dist-from-identity-map ((t0 Matrix))
  (with-borrowed-identity-matrix (t1 (domain t0)) (l1-dist t0 t1)))

(defmethod l1-dist-from-identity-map ((a Array))
  (assert (= 2 (array-rank a)))
  (assert (plusp (array-total-size a)))
  (loop for i from 0 below (array-dimension a 0)
	sum (loop for j from 0 below (array-dimension a 1)
		     if (= i j) sum (abs (- 1.0d0 (aref a i j)))
		     else       sum (abs (aref a i j))) into total
	finally (return (/ total (array-total-size a)))))

;;;=======================================================

(defmethod l2-dist-from-identity-map ((t0 Matrix))
  (with-borrowed-identity-matrix (t1 (domain t0)) (l2-dist t0 t1)))

(defmethod l2-dist-from-identity-map ((a Array))
  (assert (= 2 (array-rank a)))
  (assert (plusp (array-total-size a)))
  (loop for i from 0 below (array-dimension a 0)
	sum (loop for j from 0 below (array-dimension a 1)
		  if (= i j) sum (bm:sq (- 1.0d0 (aref a i j)))
		  else       sum (bm:sq (aref a i j))) into total
	finally (return (/ (sqrt total) (array-total-size a)))))

;;;=======================================================
     
(defmethod sup-dist ((t0 Linear-Mapping) (t1 Linear-Mapping)
		     &key
		     (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (element? t1 space))
  (assert (eql (domain t0)   (domain t1)))
  (assert (eql (codomain t0) (codomain t1)))
  (with-borrowed-elements ((v0 (domain t0))
			   (v1 (domain t0)))
    (loop for i from 0 below (dimension (domain t0))
	  do (setf v0 (row t0 i :result v0))
	     (setf v1 (row t1 i :result v1))
	     (sub v0 v1 :result v0)
	  maximize (sup-norm v0))))

(defmethod sup-dist ((a0 Array) (a1 Array)
		     &rest options)
  (assert (null options))
  
  (assert (bm:equal-array-dimensions? a0 a1))
  (case (array-rank a1)
    (1  (loop for i from 0 below (array-dimension a1 0)
	      maximize (abs (- (aref a1 i) (aref a0 i)))))
    (2  (loop with m = (array-dimension a1 1)
	      for i from 0 below (array-dimension a1 0)
	      maximize (loop for j from 0 below m
			     maximize (abs (- (aref a1 i j) (aref a0 i j))))))
    (otherwise
      (sup-dist (make-array (array-total-size a0) :displaced-to a0)
		(make-array (array-total-size a1) :displaced-to a1)))))
  
;;;=======================================================

(defmethod l1-dist ((t0 Matrix)
		    (t1 Matrix)
		    &key
		    (space (home-space t0)))
  (az:type-check Linear-Mapping-Space space)
  (assert (element? t0 space))
  (assert (element? t1 space))
  (l1-dist (2d-array t0) (2d-array t1)))

(defmethod l1-dist ((a0 Array)
		    (a1 Array)
		    &rest options)
  (assert (null options))
  (assert (bm:equal-array-dimensions? a0 a1))
  (case (array-rank a1)
    (1 (loop for i from 0 below (array-dimension a1 0)
	     sum (abs (- (aref a1 i) (aref a0 i)))))
    (2 (loop with m = (array-dimension a1 1)
	     for i from 0 below (array-dimension a1 0)
	     sum (loop for j from 0 below m
		       sum (abs (- (aref a1 i j) (aref a0 i j))))))
    (otherwise
      (l1-dist (make-array (array-total-size a0) :displaced-to a0)
	       (make-array (array-total-size a1) :displaced-to a1)))))

(defmethod avg-l1-dist ((a0 Array) (a1 Array))
  (assert (bm:equal-array-dimensions? a0 a1))
  (assert (plusp (array-total-size a0)))
  (case (array-rank a1)
    (1 (/ (loop for i from 0 below (array-dimension a1 0)
		sum (abs (- (aref a1 i) (aref a0 i))))
	  (array-total-size a0)))
    (2 (/ (loop with m = (array-dimension a1 1)
		for i from 0 below (array-dimension a1 0)
		sum (loop for j from 0 below m
			  sum (abs (- (aref a1 i j) (aref a0 i j)))))
	  (array-total-size a0)))
    (otherwise
      (l1-dist (make-array (array-total-size a0) :displaced-to a0)
	       (make-array (array-total-size a1) :displaced-to a1)))))
  
;;;=======================================================

(defmethod l2-dist2 ((t0 Matrix) (t1 Matrix)
		     &key (space (home-space t0)))
  (assert (element? t0 space))
  (assert (element? t1 space))
  (l2-dist2 (2d-array t0) (2d-array t1)))

(defmethod l2-dist2 ((a0 Array) (a1 Array)
		     &key (space nil))
  (declare (ignore space))
  (assert (bm:equal-array-dimensions? a0 a1))
  (case (array-rank a1)
    (1  (loop for i from 0 below (array-dimension a1 0)
	      sum (bm:sq (- (aref a1 i) (aref a0 i)))))
    (2 (loop with m = (array-dimension a1 1)
	     for i from 0 below (array-dimension a1 0)
	     sum (loop for j from 0 below m
		       sum (bm:sq (- (aref a1 i j) (aref a0 i j))))))
    (otherwise
      (l2-dist2 (make-array (array-total-size a0) :displaced-to a0)
		(make-array (array-total-size a1) :displaced-to a1)))))

(defmethod avg-l2-dist ((a0 Array) (a1 Array))
  (assert (bm:equal-array-dimensions? a0 a1))
  (assert (plusp (array-total-size a0)))
  (case (array-rank a1)
    (1 (/ (sqrt (loop for i from 0 below (array-dimension a1 0)
		      sum (bm:sq (- (aref a1 i) (aref a0 i)))))
	  (array-total-size a0)))
    (2 (/ (sqrt (loop with m = (array-dimension a1 1)
		      for i from 0 below (array-dimension a1 0)
		      sum (loop for j from 0 below m
				sum (bm:sq (- (aref a1 i j) (aref a0 i j))))))
	  (array-total-size a0)))
    (otherwise
      (avg-l2-dist (make-array (array-total-size a0) :displaced-to a0)
		   (make-array (array-total-size a1) :displaced-to a1)))))
  
