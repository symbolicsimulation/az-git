 ;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus; -*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=========================================================
;; test svd

(setf qr0 (qr-decompose (make-random-matrix 11 4)))
(setf (factors qr0) (harvest-factors qr0))
(setf (factors qr0) (delete-if-not #'orthogonal-columns? (factors qr0)))
(setf v0 (copy-to-matrix qr0))
(setf id (make-instance 'Natural-Identity
		       :codomain (block-Subspace (LVector-Space 11) 0 4)
		       :domain (LVector-Space 4)))
(setf v (compose v0 id))

(setf s (make-instance 'Diagonal-Vector
			  :domain (LVector-Space 4)
			  :codomain (LVector-Space 4)
			  :vec '#(1 2 3 4)))

(setf qr1 (qr-decompose (make-random-matrix 4 4)))
(setf (factors qr1) (delete-if-not #'orthogonal-rows? (factors qr1)))
(setf u (copy-to-matrix qr1))

(setf m (compose v (compose-right! s (copy-to-matrix u))))

;;;=========================================================
;;;		 QR (LQ) Decompositions
;;;=========================================================

(defvar *m* 32)
(defvar *n* 2048)
(defvar *matrix0* (read-matrix-from-file
		    "boo:>fossil>data>matrix.data" *m* *n*))
	    
(defvar *matrix* (make-matrix *m* *n*))
(defvar *householders*
	(loop repeat *m*
	      for p from *n* by -1
	      collect (make-instance
			'Householder
			:from 0 :below p
			:domain (LVector-Space p)
			:codomain (LVector-Space p)
			:vec (make-zero-lvector p))))

;;;---------------------------------------------------------

(defun time-lq-decompose! ()
  (with-open-file (out "boo:>fossil>linpack>lq-lisp-times.text"
		       :direction :output
		       :if-exists :append
		       :if-does-not-exist :create)
    (print-system-description out)
    (format out "~%Times for lq-decompose!: m n seconds~%"))
  (let ((a (2d-array *matrix*)) (a0 (2d-array *matrix0*)) seconds)
    (setf (codomain *matrix* (LVector-Space *m*)))
    (setf (domain *matrix* (LVector-Space *n*)))
    (loop for m from (truncate *m* 4) to *m* by (truncate *m* 4) do
      (setf (codomain *matrix*) (block-subspace (LVector-Space *m*) 0 m))
      (loop for n from (truncate *n* 4) to *n* by (truncate *n* 4) do
	(setf (domain *matrix*) (block-subspace (LVector-Space *n*) 0 n))
	(dotimes (i m) (row<-row! a 0 n i a0 0 n i))
	(setf seconds (time-body (lq-decompose! *matrix* *householders*)))
	(with-open-file (out "boo:>fossil>linpack>lq-lisp-times.text"
			     :direction :output
			     :if-exists :append
			     :if-does-not-exist :create)
	  (format *standard-output* "~%~dx~d ~f" m n seconds)
	  (format out "~%~dx~d ~f" m n seconds))))))

;;;---------------------------------------------------------

(setf m (make-random-matrix 64 64))
(setf m1 (make-matrix 64 64))
(meter:make-pc-array (* 64 sys:page-size))
(meter:monitor-all-functions)

(meter:expand-range 0 100)

(meter:report)

(progn
  (az:copy m :result m1)
  (meter:with-monitoring t (lq-decompose! m1)))

;;;---------------------------------------------------------

#+symbolics-fortran
(defun time-sqrsl ()
  (fresh-line *standard-output*)
  (f77:execute ftn-user:timeqr
	       :units ((7 "boo:>fossil>data>matrix.data")
		       (8 "boo:>fossil>linpack>sqrsl-times.text"))))
#||

      

||#

(defvar *max-test-dim* 256)

(defvar *householders*
	(loop for n from *max-test-dim* downto 1
	      collect (make-instance
			'Householder
			:from 0 :below n
			:domain (LVector-Space n)
			:codomain (LVector-Space n)
			:vec (make-zero-lvector n))))

(defvar *test-matrix* (make-matrix-from-array
			(az:make-float-array
			  (list *max-test-dim* *max-test-dim*))))

(defun time-lq-decompose! (&optional
			   (mtx *test-matrix*)
			   (nsteps 4)
			   (step
			     (truncate
			       (min (array-dimension (2d-array mtx) 0)
				    (array-dimension (2d-array mtx) 1))
			       nsteps)))
  (loop with t0 and t1
	for k from 1 to nsteps
	for dim = (* step k)
	do (restrict-Mapping! mtx 0 dim 0 dim)
	   (fill-randomly! mtx)
	   ;;(zl-user:gc-off)
	   (setf t0 (get-internal-run-time))
	   (lq-decompose! mtx)
	   (setf t1 (get-internal-run-time))
	   ;;(zl-user:gc-on) (sleep 10)
	   (format *standard-output*
		   "~%~dx~d ~f"
		   (dimension (codomain mtx))
		   (dimension (domain mtx))
		   (/ (- t1 t0) internal-time-units-per-second))))
 
(progn
  (fill-randomly! *test-matrix*)
  (time (setf lq (lq-decompose! *test-matrix* *householders*)))
  (setf *householders* (factors (right-factor lq))))

(defun time-qr-decompose! (&optional
			   (mtx *test-matrix*)
			   (nsteps 4)
			   (step
			     (truncate
			       (min (array-dimension (2d-array mtx) 0)
				    (array-dimension (2d-array mtx) 1))
			       nsteps)))
  (loop with t0 and t1
	for k from nsteps downto 1
	for dim = (* step k)
	do (restrict-Mapping! mtx 0 dim 0 dim)
	   (fill-randomly! mtx)
	   ;;(zl-user:gc-off)
	   (setf t0 (get-internal-run-time))
	   (qr-decompose! mtx)
	   (setf t1 (get-internal-run-time))
	   ;;(zl-user:gc-on) (sleep 10)
	   (format *standard-output*
		   "~%~dx~d ~f"
		   (dimension (codomain mtx))
		   (dimension (domain mtx))
		   (/ (- t1 t0) internal-time-units-per-second))))

(defun time-inverse-lu-decompose! (&optional
			       (mtx *test-matrix*)
			       (nsteps 4)
			       (step
				 (truncate
				   (min (array-dimension (2d-array mtx) 0)
					(array-dimension (2d-array mtx) 1))
				   nsteps)))
  (loop with t0 and t1
	for k from nsteps downto 1
	for dim = (* step k)
	do (restrict-Mapping! mtx 0 dim 0 dim)
	   (fill-randomly! mtx)
	   ;;(zl-user:gc-off)
	   (setf t0 (get-internal-run-time))
	   (inverse-lu-decompose! mtx)
	   (setf t1 (get-internal-run-time))
	   ;;(zl-user:gc-on) (sleep 10)
	   (format *standard-output*
		   "~%~dx~d ~f"
		   (dimension (codomain mtx))
		   (dimension (domain mtx))
		   (/ (- t1 t0) internal-time-units-per-second))))
(setf nrows 64)
(setf ncols 64)

(setf m (make-random-matrix nrows ncols))

(meter:make-pc-array (* 64 sys:page-size))
(meter:monitor-all-functions)

(meter:expand-range 0 100)

(meter:report)

(meter:with-monitoring t (qr-decompose! m))

;;;=======================================================
;;; compare matrix multiply with tensorized version

(defun mm1 (a1 a2 a3 v1 v2 v3)
  (declare (ignore v1 v2 v3)) 
  (loop with n = (array-dimension a3 0)
	with p = (array-dimension a3 1)
	with m = (array-dimension a2 0)
	for i from 0 below n
	do (loop for j from 0 below p
		 do (setf (aref a3 i j)
			  (bm:v.v a1 0 m :row i a2 0 m :col j)))))

;;; perhaps surprising, but mm2 is the fastest.
;;; its barely faster than mm1, presumeably because row.v is
;;; faster than row-col

(defun mm2 (a1 a2 a3 v1 v2 v3)
  (declare (ignore v1))
  (loop with n = (array-dimension a3 0)
	with p = (array-dimension a3 1)
	with m = (array-dimension a2 0)
	for j from 0 below p
	do (bm:v<-col! v2 0 m a2 0 m j)
	   (loop for i from 0 below n
		 do (setf (aref v3 i) (bm:v.v a1 0 m :row i v2 0 m :vec 0)))
	   (col<-v! a3 0 n j v3 0 n)))

(defun mm3 (a1 a2 a3 v1 v2 v3)
  (loop with n = (array-dimension a3 0)
	with p = (array-dimension a3 1)
	with m = (array-dimension a2 0)
	for j from 0 below p
	do (bm:v<-col! v2 0 m a2 0 m j)
	   (loop for i from 0 below n
		 do (bm:v<-row! v1 0 m a1 0 m i)
		    (setf (aref v3 i) (bm:v.v v1 0 m :vec 0 v2 0 m :vec 0)))
	   (col<-v! a3 0 n j v3 0 n)))

(defun time-mm (n m p)
  (let ((a1 (bm:make-random-2d-array n m))
	(a2 (bm:make-random-2d-array m p))
	(a3 (bm:make-random-2d-array n p))
	(v1 (make-array m))
	(v2 (make-array m))
	(v3 (make-array n)))
    (time (mm1 a1 a2 a3 v1 v2 v3))
    ;;(format-object a3)
    (time (mm2 a1 a2 a3 v1 v2 v3))
    ;;(format-object a3)
    (time (mm3 a1 a2 a3 v1 v2 v3))
    ;;(format-object a3)
    ))

#||

;;;=======================================================

(defmethod compose0 ((t1 Matrix)
		     (t2 Matrix)
		     &optional
		     (t3 (make-instance 'Matrix
					:domain (domain t2)
					:codomain (codomain t1))
			 t3-supplied?))
  
  (when t3-supplied? (compose-check-result t1 t2 t3))
  ;;(compose-set-bounds! t1 t2 t3)
  
  (loop with a1 = (2d-array t1)
	with a2 = (2d-array t2)
	with a3 = (2d-array t3)
	with m = (dimension (domain t1))
	with n = (dimension (codomain t1))
	with p = (dimension (domain t2))
	for j from 0 below p
	do (loop for i from 0 below n
		 do (setf (aref a3 i j)
			  (bm:v.v a1 0 m :row i a2 0 m :col j))))
  t3)

(defmethod compose ((t1 Linear-Mapping)
		    (t2 Matrix)
		    &optional
		    (t3 (make-instance 'Matrix
				       :domain (domain t2)
				       :codomain (codomain t1))
			t3-supplied?))
  
  (when t3-supplied? (compose-check-result t1 t2 t3))
  ;;(compose-set-bounds! t1 t2 t3)
  
  (with-borrowed-elements ((vd (codomain t2))
			   (vr (codomain t3)))
    (loop with a2 = (2d-array t2)
	  with a3 = (2d-array t3)
	  with m = (dimension (domain t1))
	  with n = (dimension (codomain t1))
	  for i from 0 below (dimension (domain t2))
	  do (bm:v<-col! vd 0 m a2 0 m i)
	     (transform t1 vd vr)
	     (col<-v! a3 0 n i vr 0 n)))
  t3)

(defun time-compose (n m p)
  (let ((t1 (make-random-matrix n m))
	(t2 (make-random-matrix m p))
	(t3 (make-random-matrix n p)))
    (time (compose0 t1 t2 t3))
    (time (compose t1 t2 t3))))
  
;;;=======================================================
;;; meter lu decompose

(setf n 16)

(setf m (make-random-matrix nrows ncols))


(setf m1 (az:copy m))

(setf m1 (az:copy m :result m1))

(meter:make-pc-array (* 64 sys:page-size))
(meter:monitor-all-functions)

(meter:expand-codomain 0 200)

(meter:report)

(meter:with-monitoring t (pseudo-inverse! m1))

;;;=======================================================
;;; time lu decompose

(time (pseudo-inverse m))
(time (pseudo-inverse! m1))
(setf a (az:copy (2d-array m)))

(time (math:decompose a))

;;; decompose and solve

(time
  (let ((m-1 (pseudo-inverse m)))
    (transform m-1 v)))

(time
  (multiple-value-bind (lu ps) (math:decompose a)
    (math:solve lu ps v)))

;;;=======================================================

(setf nrows 16)
(setf ncols 1024)

(setf m (make-random-matrix nrows ncols))
(setf m1 (make-matrix nrows ncols))

(setf m1 (az:copy m :result m1))

(meter:make-pc-array (* 64 sys:page-size))
(meter:monitor-all-functions)

(meter:expand-range 0 300)

(meter:report)

(meter:with-monitoring t (qr-decompose! m1))
(meter:with-monitoring t (lq-decompose! m1 user::*householders*))

;;;=======================================================

(setf nrows 64)
(setf ncols 64)

(setf m (make-random-matrix nrows ncols))
(setf l (make-matrix nrows ncols 'Matrix))

(meter:make-pc-array (* 64 sys:page-size))
(meter:monitor-all-functions)

(meter:with-monitoring t (xlq m l))
(meter:report)


||#

(defun foo1 (v0 from0 below0 a v1)
  (let ((v0 v0) (v1 v1))
    (declare (sys:array-register-1d v0 v1))
    (loop for i0 from from0 below below0
	  do (incf (sys:%1d-aref v0 i0) (* a (sys:%1d-aref v1 i0))))))

(defun foo2 (v0 from0 below0 a v1)
  (let ((a a) (v0 v0) (v1 v1))
    (declare (sys:array-register-1d v0 v1))
    (loop for i0 from from0 below below0
	  do (incf (sys:%1d-aref v0 i0) (* a (sys:%1d-aref v1 i0))))))

(defun foo3 (v0 from0 below0 a v1)
  (let ((v0 v0) (v1 v1))
    (declare (sys:array-register-1d v0 v1))
    (dotimes (i (- below0 from0))
      (let ((i0 (+ i from0)))
	(incf (sys:%1d-aref v0 i0) (* a (sys:%1d-aref v1 i0)))))))

(defun foo4 (v0 from0 below0 a v1)
  (let ((v0 v0) (v1 v1))
    (declare (sys:array-register-1d v0 v1))
    (loop for i0 from from0 below below0
	  do (setf (sys:%1d-aref v0 i0)
		   (+ (sys:%1d-aref v0 i0) (* a (sys:%1d-aref v1 i0)))))))

(setf v0 (az:make-float-array 10000))
(setf v1 (az:make-float-array 10000))
(setf a (az:make-float-array 200000))

(defun foo1 (a from below)
  (let ((a a)) (declare (sys:array-register-1d a))
       (loop for i from from below below
	     for ai = (sys:%1d-aref a i)
	     sum (* ai ai))))

(defun foo2 (a from below)
  (let ((a a)) (declare (sys:array-register-1d a))
       (loop for i from from below below
	     sum (bm:sq (sys:%1d-aref a i)))))
