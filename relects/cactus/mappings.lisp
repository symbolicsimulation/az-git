;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;;	 Mappings between Spaces
;;;=======================================================


(defclass Mapping (Abstract-Point)
     ((domain 
	:type Abstract-Space
	:initarg :domain 	
	:accessor domain)
      (codomain 	
	:type Abstract-Space
	:initarg :codomain 	
	:accessor codomain)
      (properties 
	:type List
	:initarg :properties 
	:accessor properties))
  
  (:default-initargs
    :properties ())

  (:documentation
   "Calling it {\tt codomain} rather than {\tt range} is a little pedantic,
but it's sometimes useful to distinguish the two concepts:
Suppose $T$ is a 4x3 {\sf Matrix} taking $R^3 \mapsto R^4$.
The {\tt codomain} of $T$ is $R^4$, but its {\tt range} is the 
3-dimensional subspace of $R^4$ resulting from $T(R^3)$."))

;;;-------------------------------------------------------

(defmethod print-object ((t0 Mapping) stream)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 stream)
	(format stream
		"~@[~a ~]~:(~a~) ~a->~a"
		(mapping-adjective t0)
		(class-name (class-of t0))
		(domain t0) (codomain t0)))
      (call-next-method))) 

(defmethod mapping-adjective ((t0 Mapping)) nil)

;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((t0 Mapping) (result Mapping))
  (and (eq (domain t0) (domain result))
       (eq (codomain t0) (codomain result))
       (verify-mapping-copy-result? t0 result)))

(defmethod verify-mapping-copy-result? ((t0 Mapping) (result Mapping))
  (typep result (class-name (class-of t0))))

;;;-------------------------------------------------------
;;; this will only work for 
(defmethod cardinality ((t0 Mapping))
  (* (dimension (domain t0)) (dimension (codomain t0))))

;;;-------------------------------------------------------

(defmethod flat? ((t0 Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod linear? ((t0 Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod automorphic? ((t0 Mapping) &rest options)
  (declare (ignore options))
  (eq (domain t0) (codomain t0)))

(defmethod square? ((t0 Mapping) &rest options)
  (declare (ignore options))
  (and (finite-dimensional? (domain t0))
       (finite-dimensional? (codomain t0))
       (= (dimension (domain t0))
	  (dimension (codomain t0)))))

;;; dimension increasing
(defmethod embedding? ((t0 Mapping) &rest options)
  (declare (ignore options))
  (<= (dimension (domain t0)) (dimension (codomain t0))))

;;; dimension reducing
(defmethod projecting? ((t0 Mapping) &rest options)
  (declare (ignore options))
  (>= (dimension (domain t0)) (dimension (codomain t0))))

;;; dimension increasing
(defmethod strictly-embedding? ((t0 Mapping) &rest options)
  (declare (ignore options))
  (< (dimension (domain t0)) (dimension (codomain t0))))

;;; dimension reducing
(defmethod strictly-projecting? ((t0 Mapping) &rest options)
  (declare (ignore options))
  (> (dimension (domain t0)) (dimension (codomain t0))))

;;;-------------------------------------------------------
;;; The <nil>'s returned here mean more precisely "don't know" than that
;;; the Mapping doesn't have the property.  Might adopt a two
;;; value return, like <subtypep>, in the future.

(defmethod idempotent? ((t0 Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod modifier? ((t0 Mapping) &rest options)
  (declare (ignore options))
  nil)

;;;=======================================================

(defmethod transform ((t0 Mapping)
		      (vd Abstract-Point)
		      &key
		      (result (make-element (codomain t0))
			      result-supplied?))
  
  (assert (element? vd (domain t0)))
  (when result-supplied? (assert (element? result (codomain t0))))
  (if (eq vd result)
      ;; then try to do it in place
      (transform! t0 vd)
      ;; else do it the usual way
      (transform-to! t0 vd result))) 

;;;-------------------------------------------------------
;;; an inefficient default method for destructive Mapping:

(defmethod transform! ((t0 Mapping) (vd Abstract-Point))
  
  (assert (element? vd (domain t0)))
  (assert (element? vd (codomain t0)))

  (with-borrowed-element (result (codomain t0))
    (transform-to! t0 vd result)
    (az:copy result :result vd))
  vd)

;;;-------------------------------------------------------
;;; an inefficient default method for transforming LVector-Block-Subspace

(defmethod transform-to! ((t0 Mapping)
			  (vd LVector-Block)
			  (result Lvector))
  (with-borrowed-element (v (domain t0))
    (transform-to! t0 (embed vd (domain t0) :result v) result)))

;;;=======================================================
;;; Scalar-valued Mappings
;;;=======================================================

(defclass Real-Functional (Mapping) ())

(defmethod codomain ((t0 Real-Functional)) 'Double-Float)

(defmethod (setf codomain) (new (t0 Real-Functional))
  (declare (ignore new))
  (error "Can't change the codomain of a Real-Functional."))

;;;-------------------------------------------------------

(defun error-cant-accept-result-argument (result)
  (error "can't accept the result argument: ~a" result))

;;;-------------------------------------------------------

(defmethod transform ((t0 Real-Functional) (vd Abstract-Point)
		      &key (result nil))
  (assert (element? vd (domain t0)))
  (unless (null result) (error-cant-accept-result-argument result))
  (transform2 t0 vd))

(defmethod transform2 ((t0 Real-Functional) (vd Abstract-Point))
  (missing-method))

;;;============================================================

(defclass Differentiable-Mapping (Mapping)
     ())

;;;------------------------------------------------------------

(defmethod jacobian :before ((sc Differentiable-Mapping) st ja)
	   (az:type-check Abstract-Point st)
	   (az:type-check Abstract-Vector ja)
	   ;;(az:type-check Linear-Mapping ja)
	   (assert (element? st (domain sc)))
	   (assert (eq (domain sc) (domain ja)))
	   (assert (eq (domain sc) (codomain ja)))
	   (assert (not (eq st ja))))

(defmethod jacobian ((sc Differentiable-Mapping) st ja)
  (declare (ignore st ja))
  (error "Missing a method for <jacobian>"))

;;;=======================================================
;;; Indirect Mappings
;;;=======================================================

(defclass Indirect-Mapping (Mapping)
     ((source 
	:type Mapping
	:initarg :source 
	:accessor source)))

;;;-------------------------------------------------------
;;; Indirect-Mapping's calculate their codomain
;;; and domain from their state, rather than using slots.

(defmethod print-object ((t0 Indirect-Mapping) stream)
  (az:printing-random-thing (t0 stream)
    (format stream "~@[~a ~]~:(~a~) ~a->~a"
	    (mapping-adjective t0)
	    (class-name (class-of t0))
	    (domain t0) (codomain t0)))) 

;;;=======================================================

(defclass Inverse (Indirect-Mapping) ())

;;;-------------------------------------------------------

(defmethod domain   ((t0 Inverse)) (codomain (source t0)))
(defmethod codomain ((t0 Inverse)) (domain   (source t0)))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Inverse))
  (make-instance (class-of t0) :source (source t0)))

(defmethod az:copy-to! ((t0 Inverse) (result Inverse))
  (setf (source result) (az:copy (source t0)))
  result)

;;;-------------------------------------------------------

(defmethod print-object ((t0 Inverse) str)
  (if (az::slots-boundp t0 '(codomain domain source))
      (az:printing-random-thing (t0 str)
	(format str
		"~@[~a ~]~:(~a~) ~a->~a of ~a"
		(mapping-adjective t0)
		(class-name (class-of t0))
		(domain t0) (codomain t0)
		(source t0)))
      (call-next-method)))

;;;=======================================================
;;; Flat Mappings
;;;=======================================================

(defclass Flat-Mapping (Differentiable-Mapping
			 Flat-Point)
     ())

(defmethod flat? ((t0 Flat-Mapping) &rest options)
  (declare (ignore options))
  t)

;;;=======================================================

(defclass Flat-Mapping-Space (Flat-Space)
     ((codomain
	:type Flat-Space
	:reader codomain
	:initarg :codomain)
      (domain
	:type Flat-Space
	:reader domain
	:initarg :domain)))

;;;-------------------------------------------------------

(defmethod dimension ((space Flat-Mapping-Space))
  (* (dimension (codomain space))
     (dimension (domain space))))

(defmethod element? ((t0 T) (space Flat-Mapping-Space))
  (and (typep t0 'Flat-Mapping)
       (eq (codomain t0) (codomain space))
       (eq (domain t0) (domain space))))

;;;=======================================================

(defclass Shift-Mapping (Flat-Mapping)
     ((shift-vector
	:type Abstract-Vector
	:reader shift-vector
	:initarg :shift-vector)))

;;;-------------------------------------------------------

(defmethod (setf shift-vector) ((v T) (t0 Shift-Mapping))
  (assert (element? v (codomain t0)))
  (setf (slot-value t0 'shift-vector) v))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Shift-Mapping)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names 
  (assert (eql (codomain t0) (domain t0)))
  (assert (element? (shift-vector t0) (codomain t0))))

;;;-------------------------------------------------------

(defmethod modifier? ((t0 Shift-Mapping) &rest options)
  (declare (ignore options))
  t)

;;;=======================================================
;;;		 Linear Mappings
;;;=======================================================
;;;
;;; The basic protocol for Linear-Mapping's requires 4 methods
;;; implementing its algebra, <transform> and 2 algebraic operations:
;;; <compose> and <linear-mix> (linear combination).  It also includes <scale>,
;;; <add>, <sub>, <transpose>, and <pseudo-inverse>.


#||latex
 
Linear Mappings are vector Mappings that are {\it linear:} 
$T(a\v{x} + b\v{y}) = aT(\v{x}) + bT(\v{y}).$

||#

(defclass Linear-Mapping (Flat-Mapping
			   Abstract-Vector)
     ())

;;;-------------------------------------------------------

(defmethod print-object ((t0 Linear-Mapping) str)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 str)
	(format str "~@[~a ~]~:(~a~) ~a->~a"
		(mapping-adjective t0)
		(class-name (class-of t0))
		(domain t0) (codomain t0)))
      (call-next-method)))

;;;-------------------------------------------------------

(defmethod transform-transpose ((t0 Linear-Mapping)
				(vd Abstract-Vector)
				&key
				(result (make-element (dual-space (domain t0)))
					result-supplied?))
  (assert (element? vd (dual-space (codomain t0))))
  (when result-supplied? (assert (element? result (dual-space (domain t0)))))
  (cond
    ((eq vd result) (transform-transpose! t0 vd))
    ((symmetric? t0) (transform-to! t0 vd result))
    (t (transform-transpose-to! t0 vd result))))

;;;-------------------------------------------------------
;;; an inefficient default method for destructive Mapping:

(defmethod transform-transpose! ((t0 Linear-Mapping)
				 (vd Abstract-Vector))

  (assert (and (element? vd (dual-space (codomain t0)))
	       (element? vd (dual-space (domain t0)))))
  (with-borrowed-element (result (dual-space (domain t0)))
    (transform-transpose-to! t0 vd result)
    (az:copy result :result vd))
  vd)

;;;-------------------------------------------------------
;;; This first only works if pseudo-inverse! make sense.

(defmethod pseudo-inverse ((t0 Linear-Mapping))
  (pseudo-inverse! (az:copy t0)))

(defmethod pseudo-inverse! ((t0 Linear-Mapping))
  (missing-method))

;;;-------------------------------------------------------

(defmethod linear? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  t)

;;;-------------------------------------------------------

(defmethod make-identity-of-class ((class Symbol) (domain Vector-Space)
				   &rest make-instance-options)
  (apply #'make-identity-of-class (find-class class) domain
	 make-instance-options))

(defmethod make-identity-of-class ((class Class) (domain Vector-Space)
				   &rest make-instance-options)
  (let ((t0 (apply #'make-instance class
		   :domain domain :codomain domain
		   make-instance-options)))
    (identity! t0)
    t0))

(defmethod identity! ((t0 Linear-Mapping))
  (error "Missing Protocol Method"))

;;;-------------------------------------------------------

(defparameter *bandedness-abbreviations*
	      '( upper-triangular? lower-triangular? diagonal?
		upper-hessenberg? lower-hessenberg? tridiagonal?
		upper-bidiagonal? lower-bidiagonal?))

;;;-------------------------------------------------------

(defparameter *properties-of-linear-Mappings*
	      '( symmetric? positive-definite? negative-definite?
		orthogonal? orthogonal-columns? orthogonal-rows? unitary?
		modifier? unit-diagonal?))

(defparameter *properties-of-linear-Mappings-preserved-by-products*
	      '( orthogonal? orthogonal-columns? orthogonal-rows? unitary?
		modifier?))

(defparameter
  *properties-of-linear-mappings-preserved-by-copy-to-matrix*
  '( orthogonal? orthogonal-columns? orthogonal-rows? unitary?
    symmetric? positive-definite? negative-definite? unit-diagonal?))

;;;-------------------------------------------------------

(defmethod col ((t0 Linear-Mapping) (i Integer)
		&key
		(result (make-element (codomain t0)) result-supplied?))
  (when result-supplied? (assert (element? result (codomain t0))))
  (with-canonical-basis-element (vi (domain t0) i)
    (transform t0 vi :result result)))

;;;-------------------------------------------------------

(defmethod row ((t0 Linear-Mapping) (i Integer)
		&key
		(result
		  (make-element (dual-space (domain t0)))
		  result-supplied?))
  (when result-supplied?
    (assert (element? result (dual-space (domain t0)))))
  (with-canonical-basis-element (vi (dual-space (codomain t0)) i)
    (transform-transpose t0 vi :result result))) 

;;;-------------------------------------------------------
;;; defaults:

(defmethod bands-start ((t0 Linear-Mapping))
  (- 1 (dimension (codomain t0))))

(defmethod bands-end ((t0 Linear-Mapping))
  (dimension (domain t0)))

;;;-------------------------------------------------------
;;; nicknames for common band patterns:

(defmethod upper-hessenberg? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (>= (bands-start t0) -1))

(defmethod lower-hessenberg? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (<= (bands-end t0) 2))

(defmethod hessenberg? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (or (lower-hessenberg? t0) (upper-hessenberg? t0)))

(defmethod upper-triangular? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (>= (bands-start t0) 0))

(defmethod lower-triangular? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (<= (bands-end t0) 1))

(defmethod triangular? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (or (upper-triangular? t0) (lower-triangular? t0)))

(defmethod diagonal? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (upper-triangular? t0) (lower-triangular? t0))) 

;;;-------------------------------------------------------

(defmethod identity? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (diagonal? t0) (unit-diagonal? t0)))

(defmethod unit-upper-triangular? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (unit-diagonal? t0) (upper-triangular? t0)))

(defmethod unit-lower-triangular? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (unit-diagonal? t0) (lower-triangular? t0)))

(defmethod upper-bidiagonal? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (lower-hessenberg? t0) (upper-triangular? t0)))

(defmethod lower-bidiagonal? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (upper-hessenberg? t0) (lower-triangular? t0)))

(defmethod tridiagonal? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (lower-hessenberg? t0) (upper-hessenberg? t0)))

(defmethod symmetric-tridiagonal? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (and (symmetric? t0) (tridiagonal? t0))) 

;;;-------------------------------------------------------

(defmethod symmetric? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  nil) 

(defmethod hermitian? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod unitary? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod orthogonal? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod orthogonal-rows? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (orthogonal? t0))

(defmethod orthogonal-columns? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (orthogonal? t0))

(defmethod positive-semi-definite? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (positive-definite? t0))

(defmethod positive-definite? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod negative-semi-definite? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  (negative-definite? t0))

(defmethod negative-definite? ((t0 Linear-Mapping) &rest options)
  (declare (ignore options))
  nil)

(defmethod unit-diagonal? ((t0 Linear-Mapping)
			   &rest options)
  (declare (ignore options))
  nil)

;;;-------------------------------------------------------

(defmethod verify-property? ((t0 Linear-Mapping) (property Symbol))
  (funcall property t0 :verify? t))

(defmethod assert-properties ((t0 Linear-Mapping))
  (map nil #'(lambda (prop) (assert (verify-property? t0 prop)))
       (properties t0)))

(defmethod delete-all-properties! ((t0 Linear-Mapping))
  (setf (properties t0) ()))

;;;-------------------------------------------------------

(defmethod mapping-adjective ((t0 Linear-Mapping))
  (let ((*verify-bands?* nil)) ;; don't verify just for printing 
    (declare (special *verify-bands?* ))
    (cond
      ((and (positive-definite? t0 :verify? nil)
	    (diagonal? t0 :verify? nil))
       "Positive-Definite-Diagonal")
      ((diagonal? t0 :verify? nil) "Diagonal")
      ((and (positive-definite? t0 :verify? nil)
	    (symmetric-tridiagonal? t0 :verify? nil))
       "Positive-Definite-Tridiagonal")
      ((positive-definite? t0 :verify? nil) "Positive-Definite")
      ((symmetric-tridiagonal? t0 :verify? nil)
       "Symmetric-Tridiagonal")
      ((symmetric? t0 :verify? nil) "Symmetric")
      ((upper-Bidiagonal? t0 :verify? nil) "Upper-Bidiagonal")
      ((lower-Bidiagonal? t0 :verify? nil) "Lower-Bidiagonal")
      ((upper-triangular? t0 :verify? nil) "Upper-Triangular")
      ((lower-triangular? t0 :verify? nil) "Lower-Triangular")
      ((tridiagonal? t0 :verify? nil) "Tridiagonal")
      ((upper-hessenberg? t0 :verify? nil) "Upper-Hessenberg")
      ((lower-hessenberg? t0 :verify? nil) "Lower-Hessenberg"))))
  
;;;=======================================================

(defclass Linear-Mapping-Space (Flat-Mapping-Space
				 Vector-Space)
     ((codomain
	:type Vector-Space
	:reader codomain
	:initarg :codomain)
      (domain
	:type Vector-Space
	:reader domain
	:initarg :domain)))

;;;-------------------------------------------------------

(defmethod print-object ((t0 Linear-Mapping-Space) stream)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 stream)
	(format stream
		"~:(~a~) ~a->~a"
		(class-name (class-of t0))
		(domain t0) (codomain t0)))
      (call-next-method))) 

;;;-------------------------------------------------------

(defmethod element? ((t0 Linear-Mapping)
		     (space Linear-Mapping-Space))
  (and (typep t0 'Linear-Mapping)
       (eq (codomain t0) (codomain space))
       (eq (domain t0) (domain space))))

(defmethod make-element ((space Linear-Mapping-Space)
			 &rest options)
  (apply #'make-linear-Mapping (codomain space) (domain space)
	 options))

;;;-------------------------------------------------------
;;; To ensure there is only one LVector-Space space of a given dimension.

(defparameter *Linear-Mapping-Space-table* (make-hash-table))

(defun the-linear-Mapping-space (cod dom)

  (az:type-check Vector-Space cod dom)

  (let ((dom-table (gethash cod *Linear-Mapping-Space-table* nil)))
    (when (null dom-table) (setf dom-table (make-hash-table)))
    (let ((space (gethash dom dom-table nil)))
      (when (null space)
	(setf space
	      (make-instance 'Linear-Mapping-Space
			     :codomain cod
			     :domain dom))
	(setf (gethash dom dom-table) space))
      space)))

;;;-------------------------------------------------------  

(defmethod home-space ((t0 Linear-Mapping))
  (the-linear-Mapping-space (codomain t0) (domain t0)))

;;;=======================================================  
;;; Mixins that assert some property about all their instances. This
;;; also implies that the property can't be lost by changing the state
;;; of the instance.

(defclass Orthogonal (Linear-Mapping) ())

(defmethod orthogonal? ((t0 Orthogonal) &rest options)
  (declare (ignore options))
  t)

;;;-------------------------------------------------------  

(defclass Symmetric (Linear-Mapping) ())

(defmethod shared-initialize :after ((t0 Symmetric)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names 
  (when (and (slot-boundp t0 'codomain)
	     (slot-boundp t0 'domain))
    (assert (eql (codomain t0) (domain t0))))
  t) 

(defmethod symmetric? ((t0 Symmetric) &rest options)
  (declare (ignore options))
  T)

;;;-------------------------------------------------------  

(defmethod automorphic? ((t0 Symmetric)
			 &rest options)
  (declare (ignore options))
  t) 

;;;-------------------------------------------------------  

(defmethod transform-transpose-to! ((t0 Symmetric)
				    (vd Abstract-Vector)
				    (result Abstract-Vector))
  (transform t0
	     (dual-vector vd :result vd)
	     :result (dual-vector result :result result))
  (dual-vector result :result result)) 

;;;-------------------------------------------------------

(defmethod transpose ((t0 Symmetric)
		      &key
		      (result (az:copy t0) result-supplied?))
  (when (and result-supplied? (not (eq t0 result)))
    (assert (typep result (class-name (class-of t0))))
    (assert (eql (domain t0) (domain result)))
    (az:copy t0 :result result))
  result)

(defmethod transpose! ((t0 Symmetric)) t0)

;;;=======================================================
#||latex

For purposes of computing, we need to distinguish
square and dimension-reducing projections,
which are not usually distinguished in mathematics.
In mathematics, one freely uses natural identities
between, for example, a projection from $R^3$ to $R^2$
and the corresponding projection from $R^3$ to $R^3$
whose range is 2-dimensional.

||#

;;;=======================================================
#||latex

A {\sf Projection} is characterized by idempotence: $P*P = P$
A {\sf Projection} is {\sf Orthogonal} iff it is {\sf Symmetric.}
(see eg. Halmos, Finite Dimensional Vector Spaces)
 
||#

(defclass Orthogonal-Projection (Orthogonal Symmetric) ())

(defmethod shared-initialize :after ((t0 Orthogonal-Projection)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names 
  (assert (automorphic? t0)) t)

(defmethod idempotent? ((t0 Orthogonal-Projection) &rest options)
  (declare (ignore options))
  t)

;;;=======================================================
#||latex

A {\sf Block-Projection} projects on the subspace spanned
by a the {\tt [start,end} canonical basis vectors.

||#

(defclass Block-Projection (Linear-Mapping)
     ((codomain 
	:type Block-Subspace
	:initarg :codomain
	:accessor codomain)
      (domain 
	:type Euclidean-Vector-Space
	:initarg :domain
	:accessor domain)))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Block-Projection)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (proper-subspace? (codomain t0) (domain t0))))

;;;-------------------------------------------------------

(defmethod orthogonal-rows? ((t0 Block-Projection) &rest options)
  (declare (ignore options))
  t)

;;;-------------------------------------------------------

(defun make-block-projection (domain codomain)
  (make-instance 'Block-Projection :domain domain :codomain codomain))

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Block-Projection)
			  (vd Lvector-Super)
			  (result Lvector-Super))
  (project vd (codomain t0) :result result)) 

;;;-------------------------------------------------------
;;; same code as (transform-to! Block-Embedding Lvector Lvector)

(defmethod transform-transpose-to! ((t0 Block-Projection)
				    (vd Lvector-Super)
				    (result Lvector-Super))
  (embed (dual-vector vd :result vd)
	 (dual-space (domain t0))
	 :result (dual-vector result :result result))
  (dual-vector result :result result))

;;;=======================================================
#||latex

A {\sf Block-Embedding} maps the canonical basis vectors
of its domain to the {\tt start,end} canonical basis vectors
of its codomain. {\sf Block-Embedding} the pseudo-inverse
of {\sf Block-Projection}.

||#

(defclass Block-Embedding (Linear-Mapping)
     ((codomain 
	:type Euclidean-Vector-Space 
	:initarg :codomain
	:accessor codomain)
      (domain 
	:type Block-Subspace
	:initarg :domain
	:accessor domain)))

;;;-------------------------------------------------------
;;; make sure the embedding is complete

(defmethod shared-initialize :after ((t0 Block-Embedding)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names 
  (assert (proper-subspace? (domain t0) (codomain t0))))

;;;-------------------------------------------------------

(defmethod orthogonal-columns? ((t0 Block-Embedding) &rest options)
  (declare (ignore options))
  t)

;;;-------------------------------------------------------

(defun make-block-embedding (domain codomain)
  (make-instance 'Block-Embedding :domain domain :codomain codomain))

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Block-Embedding)
			  (vd Lvector-Super)
			  (result Lvector-Super))
  (embed vd (codomain t0) :result result))

;;;-------------------------------------------------------
;;; same code as (transform-to! Block-Projection Lvector Lvector)

(defmethod transform-transpose-to! ((t0 Block-Embedding)
				    (vd Lvector-Super)
				    (result Lvector-Super))
  (project (dual-vector vd :result vd)
	   (dual-space (domain t0))
	   :result (dual-vector result :result result))
  (dual-vector result :result result))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Block-Embedding)
		      &key
		      (result (make-instance
				'Block-Projection
				:domain (codomain t0)
				:codomain (domain t0))
			      result-supplied?))
  (when result-supplied?
    (az:type-check Block-Projection result)
    (assert (eql (dual-space (domain t0)) (codomain result)))
    (assert (eql (dual-space (codomain t0)) (domain result))))
  result)

(defmethod transpose! ((t0 Block-Embedding))
  (let ((dom (domain t0))
	(cod (codomain t0)))
    (setf (domain t0) (dual-space cod))
    (setf (codomain t0) (dual-space dom)))
  (change-class t0 'Block-Projection))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Block-Projection)
		      &key
		      (result
			(make-instance 'Block-Embedding
				       :domain (dual-space (codomain t0))
				       :codomain (dual-space (domain t0)))
			result-supplied?))
  (when result-supplied?
    (az:type-check Block-Embedding result)
    (assert (eql (dual-space (domain t0)) (codomain result)))
    (assert (eql (dual-space (codomain t0)) (domain result))))
  result) 

(defmethod transpose! ((t0 Block-Projection))
  (let ((dom (domain t0))
	(cod (codomain t0)))
    (setf (domain t0) (dual-space cod))
    (setf (codomain t0) (dual-space dom)))
  (change-class t0 'Block-Embedding))

;;;=======================================================
;;; The Natural Identity between any two subspaces

(defclass Natural-Identity (Linear-Mapping) ())

(defmethod shared-initialize :after ((t0 Natural-Identity)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (automorphic? t0))
  (when (eql (codomain t0) (domain t0))
    (warn "Using a Natural-Identity in the codomain=domain case."))) 

;;;-------------------------------------------------------

(defmethod unitary? ((t0 Natural-Identity) &rest options)
  (declare (ignore options))
  t)

(defmethod hermitian? ((t0 Natural-Identity) &rest options)
  (declare (ignore options))
  t)

(defmethod orthogonal? ((t0 Natural-Identity) &rest options)
  (declare (ignore options))
  t)

(defmethod idempotent? ((t0 Natural-Identity) &rest options)
  (declare (ignore options))
  t)

(defmethod positive-definite? ((t0 Natural-Identity) &rest options)
  (declare (ignore options))
  t)

(defmethod unit-diagonal? ((t0 Natural-Identity)
			   &rest options)
  (declare (ignore options))
  t)

(defmethod bands-end ((t0 Natural-Identity)) 1)

(defmethod bands-start ((t0 Natural-Identity)) 0)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Natural-Identity)
			  (vd Lvector-Super)
			  (result Lvector-Super))
  (az:copy vd :result result))

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Natural-Identity)
				    (vd Abstract-Vector)
				    (result Abstract-Vector))
  (transform t0
	     (dual-vector vd :result vd)
	     :result (dual-vector result :result result))
  (dual-vector result :result result))
    

;;;-------------------------------------------------------

(defmethod transpose! ((t0 Natural-Identity))
  (let ((dom (domain t0))
	(cod (codomain t0)))
    (setf (domain t0) (dual-space cod))
    (setf (codomain t0) (dual-space dom)))
  t0)

;;;=======================================================

(defclass Zero-Mapping (Linear-Mapping) ())

;;;-------------------------------------------------------

(defun Zero-Mapping? (t0) (typep t0 'Zero-Mapping))
  
;;;-------------------------------------------------------

(defmethod idempotent? ((t0 Zero-Mapping)
			&rest options)
  (declare (ignore options))
  t)

(defmethod bands-end ((t0 Zero-Mapping)) (- (dimension (codomain t0))))

(defmethod bands-start ((t0 Zero-Mapping)) (dimension (domain t0)))

;;;-------------------------------------------------------
;;; Ensure only one zero Mapping for each pair of vector spaces.

(defvar *zero-Mapping-table* (make-hash-table))

(defmethod the-zero-Mapping ((cod Vector-Space) (dom Vector-Space))
  (let ((dom-table (gethash cod *zero-Mapping-table* nil)))
    (when (null dom-table) (setf dom-table (make-hash-table)))
    (let ((zerot (gethash dom dom-table nil)))
      (when (null zerot)
	(setf zerot
	      (make-instance 'Zero-Mapping :codomain cod :domain dom))
	(setf (gethash dom dom-table) zerot))
      zerot)))

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Zero-Mapping)
			  (vd Abstract-Vector)
			  (result Abstract-Vector))
  (zero! result))

(defmethod transform! ((t0 Zero-Mapping) (vd Abstract-Vector))
  (make-zero-element (codomain t0)))

(defmethod transpose ((t0 Zero-Mapping) &key (result t0))
  (assert (eq t0 result))
  (the-zero-Mapping (dual-space (domain t0))
		    (dual-space (codomain t0))))

;;;============================================================

(defclass Affine-Mapping (Flat-Mapping)
     ((aff-jacobian
	:type Linear-Mapping
	:accessor aff-jacobian
	:initarg :jacobian)
      (pre-origin
	:type Affine-Point
	:accessor pre-shift
	:initarg :pre-shift)
      (post-origin
	:type Affine-Point
	:accessor post-shift
	:initarg :post-shift)))

;;;------------------------------------------------------------

(defmethod jacobian ((aff Affine-Mapping) st ja)
  (assert (element? st (domain aff)))
  (az:copy-to! (aff-jacobian aff) ja)
  ja)

;;;============================================================

(defclass Affine-Mapping-Space (Flat-Mapping-Space Affine-Space) ())