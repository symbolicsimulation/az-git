;;;-*- Package: Cactus; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Cactus)

(eval-when (compile load eval)
  (export '(get-strip-chart-for
	    make-strip-chart-for
	    Direct-Sum-Strip-Chart
	    snakes)))

;;;============================================================

(defun dont-know-how-to-make-a-strip-chart-for (space)
  (error "I don't know how to make a strip chart for ~a." space))

(defmethod make-strip-chart-for ((space Euclidean-Space) &rest options)
  (if (= (dimension space) 2)
      (apply #'chart:make-2d-strip-chart options)
      ;; else
      (dont-know-how-to-make-a-strip-chart-for space)))

(defparameter *strip-chart-table* (make-hash-table :test #'eq))

(defun get-strip-chart-for (space &rest options)
  (let ((chart (gethash space *strip-chart-table* nil)))
    (cond ((null chart)
	   (setf chart (apply #'make-strip-chart-for space options))
	   (setf (gethash space *strip-chart-table*) chart))
	  (t
	   (setf (chart:strip-points chart) ())
	   #||(apply #'reinitialize-instance chart options)||#))
    chart))

;;;============================================================
#||
(defclass Direct-Sum-Strip-Chart (petroglyph:Abstract-Strip-Chart
				   petroglyph:XY-Plot)
     ((snakes
	:type Sequence ;; of petroglyph:Snake-Scenes
	:accessor snakes)))

;;;------------------------------------------------------------

(defmethod cactus:make-strip-chart-for ((space Direct-Sum-Space)
					&rest options)

  (if (= (dimension (xspace space)) (dimension (yspace space)))
      (apply #'make-direct-sum-strip-chart-for space options)
      ;; else
      (dont-know-how-to-make-a-strip-chart-for space)))

;;;------------------------------------------------------------

(defun make-direct-sum-strip-chart-for (space
					&rest options
					&key
					(slate (slate:make-slate))
					(region (slate:slate-region slate))
					(point (fill-randomly!
						 (make-element space)))
					(max-length 4)
					&allow-other-keys)
  
  (let ((plot (make-instance 'Direct-Sum-Strip-Chart
			     :slate slate
			     :slate-region region)))
    (setf (petroglyph:plot-updatep plot) nil)
    (setf (snakes plot)
	  (az:with-collection
	    (map nil
		 #'(lambda (x y)
		     (az:collect (petroglyph:plot-add-snake
				      plot (list (g:make-position x y))
				      :max-length max-length)))
		 (1d-array (x point))
		 (1d-array (y point)))))
    (setf (petroglyph:plot-updatep plot) t)
    plot))

;;;------------------------------------------------------------

(defmethod petroglyph:strip-crawl ((plot Direct-Sum-Strip-Chart)
				   (point Direct-Sum-Point))
  (setf (petroglyph:plot-updatep plot) nil)
  (map nil
       #'(lambda (x0 y0 snake) (petroglyph:snake-crawl-xy snake x0 y0))
       (1d-array (x point))
       (1d-array (y point))
       (snakes plot))
  (setf (petroglyph:plot-updatep plot) t)
  (petroglyph:plot-format plot))
||#

