;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;;		      LPoint-Space
;;;=======================================================
;;; The standard (and the first) instantiable affine space:

(defclass LPoint-Space (Euclidean-Affine-Space) ())

;;;-------------------------------------------------------

(defmethod print-object ((space LPoint-Space) stream)
  (format stream "(LP ~d)" (dimension space)))

;;;-------------------------------------------------------

(defmethod scalar-field ((sp LPoint-Space)) 'Double-Float)
(defmethod translation-space ((sp LPoint-Space))
  (LVector-Space (Dimension sp)))

;;;-------------------------------------------------------

(defmethod make-element ((sp LPoint-Space)
			 &rest options)
  (make-instance
    'Lpoint
    :home-space sp 
    :1d-array (if (null options)
		  (az:make-float-array (dimension sp))
		  (apply #'az:make-float-array (dimension sp) options))))

;;;-------------------------------------------------------
;;; To ensure there is only one LPoint-Space space of a given dimension.

(defparameter *LPoint-Space-table* (make-hash-table))

(defun LPoint-Space (dim)
  (let ((sp (gethash dim *LPoint-Space-table* nil)))
    (when (null sp)
      (setf sp (make-instance 'LPoint-Space :dimension dim))
      (setf (gethash dim *LPoint-Space-table*) sp))
    sp))

;;;=======================================================
;;;		      Lpoint
;;;=======================================================

(defclass Lpoint (Euclidean-Affine-Point)
     ((1d-array
	:type az:Float-Vector
	:accessor 1d-array
	:initarg :1d-array)
      (home-space
	:type LPoint-Space
	:accessor home-space
	:initarg :home-space)))

;;;------------------------------------------------------------

(defmethod element? ((p Lpoint) (sp LPoint-Space))
  (eq (home-space p) sp))

;;;------------------------------------------------------------

#+symbolics
(defmethod format-object ((v Lpoint)
			  &optional (stream *standard-output*))
  (print (class-of v) stream)
  (format stream "~%An element of ~s" (home-space v)) 
  (format-object (1d-array v) stream)) 

;;;------------------------------------------------------------

(defmethod az:new-copy ((v0 Lpoint))
  (let ((result (make-element (home-space v0))))
    (az:copy (1d-array v0) :result (1d-array result))
    result))

;;;------------------------------------------------------------

(defmethod fill-randomly! ((p0 Lpoint)
			   &rest options
			   &key (min 0.0d0) (max 1.0d0)
			   &allow-other-keys)
  (declare (ignore options))
  (bm:fill-random-vector (1d-array p0) :min min :max max)
  p0)

;;;------------------------------------------------------------

(defmethod coordinate ((p0 Lpoint) i
		       &key (coordinate-system :standard))
  (declare (ignore coordinate-system))
  (aref (1d-array p0) i))

(defmethod (setf coordinate) (new (p0 Lpoint) i
				  &key (coordinate-system :standard))
  (declare (ignore coordinate-system))
  (setf (aref (1d-array p0) i) new))

;;;------------------------------------------------------------

(defmethod affine-mix ((a0 T) (p0 Lpoint) (a1 T) (p1 Lpoint)
		       &key
		       (space (home-space p0))
		       (result (make-element space)))
  (az:type-check Double-Float a0 a1)
  (assert (= 1.0d0 (+ a0 a1)))
  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (assert (element? result space))
  (bm:vector-linear-mix a0 (1d-array p0)
			a1 (1d-array p1)
			:result (1d-array result))
  result)

;;;-------------------------------------------------------

(defmethod sub ((p0 Lpoint) (p1 Lpoint)
		&key
		(space (home-space p0))
		(result (make-element (translation-space space))))
  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (assert (element? result (translation-space space)))
  (bm:vector-linear-mix 1.0d0 (1d-array p0)
			-1.0d0 (1d-array p1)
			:result (1d-array result))
  result) 

;;;-------------------------------------------------------

(defmethod move-by! ((p Lpoint) amount (v Lvector)
		     &key (space (home-space p)))

  ;; destructively move the point <p> by <amount>*<v>

  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  
  (bm:vector-linear-mix 1.0d0 (1d-array p) amount (1d-array v)
			:result (1d-array p))
  p)

(defmethod move-by-to! ((p Lpoint) amount (v Lvector) (result Lpoint)
			&key (space (home-space p)))
  
  ;; move the point <p> by <amount>*<v>
  ;; and put the answer in <result>

  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  (assert (element? result space))

  (bm:vector-linear-mix 1.0d0 (1d-array p) amount (1d-array v)
			:result (1d-array result))
  result)
 

(defmethod move-by! ((p Lpoint) amount (v LVector-Block)
		     &key (space (home-space p)))

  ;; destructively move the point <p> by <amount>*<v>

  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  
  (let ((tspace (translation-space (home-space p))))
    (with-borrowed-element (vb tspace)
      (embed v tspace :result vb)
      (bm:vector-linear-mix 1.0d0 (1d-array p) amount (1d-array vb)
			    :result (1d-array p))))
  p)

(defmethod move-by-to! ((p Lpoint) amount (v LVector-Block) (result Lpoint)
			&key (space (home-space p)))
  
  ;; destructively move the point <p> by <amount>*<v>

  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  (assert (element? result space))

  (let ((tspace (translation-space (home-space p))))
    (with-borrowed-element (vb tspace)
      (embed v tspace :result vb)
      (bm:vector-linear-mix 1.0d0 (1d-array p) amount (1d-array vb)
			    :result (1d-array result))))
  p) 

;;;------------------------------------------------------------

(defmethod l1-dist  ((p0 Lpoint) (p1 Lpoint)
		     &key
		     (space (home-space p0)))
  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (bm:v-l1-dist (1d-array p0) (1d-array p1)))

(defmethod l2-dist2 ((p0 Lpoint) (p1 Lpoint)
		     &key (space (home-space p0)))
  (az:type-check Euclidean-Affine-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (bm:v-l2-dist2 (1d-array p0) (1d-array p1)))

(defmethod sup-dist ((p0 Lpoint) (p1 Lpoint)
		     &key
		     (space (home-space p0)))
  (az:type-check Euclidean-Affine-Space space)		     
  (assert (element? p0 space))
  (assert (element? p1 space))
  (bm:v-sup-dist (1d-array p0) (1d-array p1)))

;;;------------------------------------------------------------

(defun make-lpoint-from-array (vec)
  (make-instance 'Lpoint
		 :home-space (LPoint-Space (length vec))
		 :1d-array vec))

(defmacro lpoint (&rest coords)
  (let* ((a (gensym))
	 (j -1)
	 (n (length coords))
	 (assignments (mapcar #'(lambda (coord)
				  (incf j)
				  `(setf (aref ,a ,j) ,coord))
			      coords)))
    `(let ((,a (az:make-float-array ,n)))
       ,@assignments
       (make-instance 'Lpoint
	 :home-space (LPoint-Space ,n)
	 :1d-array ,a))))

;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((p0 Lpoint) (result Lpoint))
  (eq (home-space p0) (home-space result)))
  
(defmethod az:copy-to! ((p0 Lpoint) (result Lpoint))
  (az:copy (1d-array p0) :result (1d-array result))
  result)

