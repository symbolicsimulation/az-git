;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;; these don't really belong in this file:

(defun missing-method () (error "Missing Protocol Method"))

;;; a default for <format-object>
(defmethod format-object ((obj T) &optional (stream *standard-output*))
  (print obj stream))

#+symbolics
(defmethod format-object ((arr Array) &optional (stream *standard-output*))
  (bm:format-array arr stream))

#+symbolics
(defmethod describe ((arr Array))
  (call-next-method) ;; default-description
  (format-object arr)) 

(defmethod az:new-copy ((a Array))
  (let ((result (make-array (array-dimensions a)
			    :element-type (array-element-type a))))
    (bm:copy-array-contents a result)
    result))

(defmethod az:verify-copy-result? ((a Array) (result Array))
  (and (bm:equal-array-dimensions? a result)
       (eql (array-element-type a) (array-element-type result))))
  

(defmethod az:copy-to! ((a Array) (result Array))
  (bm:copy-array-contents a result) 
  result)

;;;=======================================================
;;;		    Abstract Spaces
;;;=======================================================

(defclass Abstract-Space (Cactus-Object)
     ((element-resource 
	:type List 
	:initform ()
	:accessor element-resource)))

;;;-------------------------------------------------------
;;; by default nothing is an element of any space:

(defmethod element? ((x T) (sp Abstract-Space)) nil)

;;;-------------------------------------------------------

(defmethod make-element ((sp Abstract-Space)
			 &rest options)
  (declare (ignore options))
  (missing-method))

(defmethod make-random-element ((sp Abstract-Space)
				&rest options
				&key (min -1.0d0) (max 1.0d0)
				&allow-other-keys)
  (fill-randomly! (apply #'make-element sp options) :min min :max max))

;; just to avoid compiler warning messages:
(defmethod fill-randomly! ((x T)
			   &rest options
			   &key (min -1.0d0) (max 1.0d0)
			   &allow-other-keys)
  (declare (ignore options min max))
  (missing-method))

;;;-------------------------------------------------------

(defmethod borrow-element ((sp Abstract-Space) &rest options)
  
  (if (null (element-resource sp))
      (apply #'make-element sp options)
      ;; else
      (pop (element-resource sp))))

;;;-------------------------------------------------------
;;;
;;; This is an example of the slightly confusing analogy between
;;; spaces and classes. It seems too bad that we have to explicitly test
;;; <v> for membership in <sp> rather than just using dispatching the
;;; dispatching mechanism somehow. The problem is that objects know what
;;; class they are an instance of, but <v> can't know what vector space
;;; it's a member of, because it might be a member  of more than one
;;; vector space.

(defmethod return-element ((v T) (sp Abstract-Space)
			   &optional (errorp t))
  
  (if (element? v sp)
      (push v (element-resource sp))
      ;; else
      (when errorp
	(error "Trying to return ~a to the resource of ~a, ~%~
              but ~a is not a member of ~a."
	       v sp v sp))))

;;;-------------------------------------------------------
;;;
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro with-borrowed-element ((name sp &rest options) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-element ,sp ,@options))
	    (,name ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-element ,return-name ,sp)))))

(defmacro with-borrowed-elements (specs &body body)
  (if (= (length specs) 1)
      `(with-borrowed-element ,(first specs)
	 ,@body)
      `(with-borrowed-element ,(first specs)
	 (with-borrowed-elements ,(rest specs)
	   ,@body))))

;;;-------------------------------------------------------

(defmethod flat-space? ((sp Abstract-Space)) nil)

(defmethod vector-space? ((sp Abstract-Space)) nil)

;;;=======================================================
;;;		     Abstract-Point
;;;=======================================================
;;; An abstract super class for classes representating
;;; elements of spaces:

(defclass Abstract-Point (Cactus-Object)
     ((home-space
	:type Abstract-Space
	:accessor home-space
	:initarg :home-space)))

;;;============================================================
;;;		     Flat Spaces
;;;============================================================

(defclass Flat-Space (Abstract-Space)
     ((affine-mixer
	:type Function
	:initform 'affine-mix
	:reader affine-mixer)
      (affine-diffencer
	:type Function
	:initform 'sub
	:reader affine-diffencer)))

;;;-------------------------------------------------------
;; The <translation-space> is the vector space of differences of
;; elements of the Flat space

(defmethod translation-space ((sp Flat-Space))
  (missing-method))

(defmethod affine-space? ((sp Flat-Space)) t) 

(defmethod finite-dimensional? ((sp Flat-Space)) nil) 

;;;=======================================================

(defclass Finite-Dimensional-Flat-Space (Flat-Space)
     ((dimension 
	:type az:Array-Index 
	:initarg :dimension 
	:reader dimension)))

;;;-------------------------------------------------------

(defmethod finite-dimensional? ((sp Finite-Dimensional-Flat-Space)) t) 

;;;-------------------------------------------------------
;;; default coordinate range:

(defmethod coord-start ((sp Finite-Dimensional-Flat-Space)) 0)
(defmethod coord-end ((sp Finite-Dimensional-Flat-Space)) (dimension sp))

;;;=======================================================
;;;		     Flat-Point
;;;=======================================================
;;; An abstract super class for classes representating
;;; elements of Flat spaces:

(defclass Flat-Point (Abstract-Point)
     ((home-space
	:type Flat-Space
	:accessor home-space
	:initarg :home-space)))

;;;------------------------------------------------------------

(defmethod affine-mix ((a0 T) (p0 Flat-Point)
		       (a1 T) (p1 Flat-Point)
		       &key
		       (space (home-space p0))
		       (result (make-element space)))
  (declare (ignore result))
  (missing-method))

;;;------------------------------------------------------------

(defmethod convex-mix ((a0 T) (p0 Flat-Point)
		       (a1 T) (p1 Flat-Point)
		       &key
		       (space (home-space p0))
		       (result (make-element space)
			       result-supplied?))
  (az:type-check Double-Float a0 a1)
  (assert (and (<= 0.0d0 a0 1.0d0)
	       (<= 0.0d0 a1 1.0d0)))
  (assert (= 1.0d0 (+ a0 a1)))
  (az:type-check Flat-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (assert (element? result space))
  (if result-supplied?
      (affine-mix a0 p0 a1 p1 :space space :result result)
      (affine-mix a0 p0 a1 p1 :space space)))

;;;------------------------------------------------------------
;;; copy-to! may take arguments of differing types,
;;; eg, vector and list

(defmethod az:copy-to! ((p0 Flat-Point) (p1 Flat-Point))
  (missing-method))

;;;------------------------------------------------------------

(defmethod fill-randomly! ((p0 Flat-Point)
			   &rest options
			   &key (min 0.0d0) (max 1.0d0)
			   &allow-other-keys)
  (declare (ignore options min max))
  (missing-method))

;;;------------------------------------------------------------

(defmethod dimension ((p0 Flat-Point)) (dimension (home-space p0)))

(defmethod coordinate ((p0 Flat-Point) i
		       &key (coordinate-system :standard))
  (declare (ignore i coordinate-system))
  (missing-method))

(defmethod (setf coordinate) (new (p0 Flat-Point) i
				  &key (coordinate-system :standard))
  (declare (ignore new i coordinate-system))
  (missing-method))

;;;=======================================================
;;;		     Vector Spaces
;;;=======================================================

(defclass Vector-Space (Flat-Space)
     (;; preparing for more generality in the future:
      (scalar-field ;; eg Number, Single-Float, Complex...
	:type Symbol 
	:initform 'Double-Float
	:reader scalar-field)
      (adder :type Function
	     :initform 'add
	     :reader adder)
      (scaler :type Function
	      :initform 'scale
	      :reader scaler)
      (linear-mixer :type Function
	     :initform 'linear-mix
	     :reader linear-mixer)))

;;;-------------------------------------------------------

(defmethod affine-space? ((sp Vector-Space)) t)

;; a Vector-Space is its own translation space
(defmethod translation-space ((sp Vector-Space)) sp)

(defmethod dual-space ((sp Vector-Space))
  (missing-method))


;;;-------------------------------------------------------

(defmethod zero-element? ((x T) (sp Vector-Space))
  (missing-method))

(defmethod make-zero-element ((sp Vector-Space) &rest options)
  (declare (ignore options))
  (missing-method))

;;;-------------------------------------------------------
;;; make a Mapping from <sp1> to <sp0>,
;;; following convention of codomain first.

(defmethod make-linear-mapping ((sp0 Vector-Space)
				(sp1 Vector-Space)
				&rest options)
  (declare (ignore options))
  (missing-method))

;;;=======================================================
;;;		    Abstract-Vector
;;;=======================================================
;;; A parent class for objects that represent elements of 
;;; finite-dimensional inner product spaces:

(defclass Abstract-Vector (Flat-Point)
     ((home-space
	:type Vector-Space
	:accessor home-space
	:initarg :home-space)))

;;;-------------------------------------------------------

(defmethod element? ((v Abstract-Vector) (space Vector-Space))
  (subspace? (home-space v) space))

(defmethod parent-space ((v Abstract-Vector))
  (parent-space (home-space v)))

;;;-------------------------------------------------------

(defmethod dual-vector ((v0 Abstract-Vector)
			&key result)
  (declare (ignore result))
  (missing-method))

;;;-------------------------------------------------------

(defgeneric scale (a v0 &key space result)
  (declare (type Number a)
	   (type Abstract-Vector v0 result)
	   (type Vector-Space space)))

(defmethod scale ((a T) (v0 Abstract-Vector)
		  &key
		  (space (home-space v0))
		  (result (az:copy v0) result-supplied?))
  
  (az:type-check Double-Float a)
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (assert (element? result space))
  
  (when (and result-supplied? (not (eq v0 result)))
    (az:copy v0 :result result))
  (cond ((zerop a) (zero! result))
	((= a 1.0d0) result)
	(t (scale! a result)))
  result)

(defmethod scale! ((a T) (v0 Abstract-Vector)
		   &key (space (home-space v0)))
  (az:type-check Double-Float a)
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (missing-method))

;;;-------------------------------------------------------

(defgeneric minus (v0 &key space result)
  (declare (type Abstract-Vector v0 result)
	   (type Vector-Space space)))

(defmethod minus ((v0 Abstract-Vector)
		  &key
		  (space (home-space v0))
		  (result (az:copy v0) result-supplied?))
  (when (and result-supplied? (not (eq v0 result)))
    (az:type-check Abstract-Vector result)
    (assert (= (dimension v0) (dimension result)))
    (az:copy v0 :result result))
  (minus! result :space space))

(defmethod minus! ((v0 Abstract-Vector)
		   &key
		   (space (home-space v0)))
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (scale! -1.0d0 v0 :space space))

;;;-------------------------------------------------------

(defmethod zero! ((v0 Abstract-Vector)
		  &key
		  (space (home-space v0)))
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (missing-method))

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 T) (v0 Abstract-Vector)
		       (a1 T) (v1 Abstract-Vector)
		       &key
		       (space (home-space v0))
		       (result (make-element space) ))
  (az:type-check Double-Float a0 a1)
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))

  (missing-method))

;;;-------------------------------------------------------

(defmethod affine-mix ((a0 T) (v0 Abstract-Vector)
		       (a1 T) (v1 Abstract-Vector)
		       &key
		       (space (home-space v0))
		       (result (make-element space)
			       result-supplied?))
  (az:type-check Double-Float a0 a1)
  (assert (= 1.0d0 (+ a0 a1)))
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))

  (if result-supplied?
      (linear-mix a0 v0 a1 v1 :space space :result result)
      (linear-mix a0 v0 a1 v1 :space space)))

;;;-------------------------------------------------------

(defmethod add ((v0 Abstract-Vector)
		(v1 Abstract-Vector)
		&key
		(space (home-space v0))
		(result (make-element space)))
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (add-to! v0 v1 result :space space))

(defmethod add-to! ((v0 Abstract-Vector)
		    (v1 Abstract-Vector)
		    (result Abstract-Vector)
		    &key
		    (space (home-space v0)))
  (az:type-check Vector-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (assert (element? result space))
  
  (linear-mix 1.0d0 v0
	      1.0d0 v1
	      :space space
	      :result result)
  result)

;;;------------------------------------------------------------

(defgeneric sub (p0 p1 &key space result)
  (declare (type Flat-Point p0 p1)
	   (type Flat-Space space)
	   (type Abstract-Vector result)))

(defmethod sub ((p0 Flat-Point)
		(p1 Flat-Point)
		&key
		(space (home-space p0))
		(result (make-element (translation-space space))))
  (assert (element? p0 space))
  (assert (element? p1 space))
  (assert (element? result (translation-space space)))
  (sub-to! p0 p1 result))

(defgeneric sub-to! (p0 p1 result &key space)
  (declare (type Flat-Point p0 p1)
	   (type Abstract-Vector result)
	   (type Flat-Space space)))

(defmethod sub-to! ((p0 Flat-Point)
		    (p1 Flat-Point)
		    (result Abstract-Vector)
		    &key
		    (space (home-space p0)))
  (assert (element? p0 space))
  (assert (element? p1 space))
  (assert (element? result (translation-space space)))
  (linear-mix 1.0d0 p0
	      -1.0d0 p1
	      :space space :result result)
  result)

;;;------------------------------------------------------------
;;; using vectors as translations on a flat space:

(defmethod move-by ((p Flat-Point)
		    amount
		    (v Abstract-Vector)
		    &key
		    (space (home-space p))
		    (result (make-element space)))
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  (assert (element? result space))
  (cond ((eq result p) (move-by! p amount v :space space))
	(t (move-by-to! p amount v result :space space))))

(defmethod move-by! ((p Flat-Point)
		     amount
		     (v Abstract-Vector)
		     &key
		     (space (home-space p)))
  ;; destructively move the point <p> by <amount>*<v>
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  (missing-method))

(defmethod move-by-to! ((p Flat-Point)
			amount
			(v Abstract-Vector)
			(result Flat-Point)
			&key
			(space (home-space p)))
  ;; move the point <p> by <amount>*<v>
  ;; and put the answer in <result>
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  (assert (element? result space))
  (missing-method))


(defmethod move-by! ((p Abstract-Vector)
		     amount
		     (v Abstract-Vector)
		     &key
		     (space (home-space p)))
  ;; destructively move the point <p> by <amount>*<v>
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  (linear-mix 1.0d0 p amount v :result p))

(defmethod move-by-to! ((p Abstract-Vector)
			amount
			(v Abstract-Vector)
			(result Flat-Point)
			&key
			(space (home-space p)))
  ;; move the point <p> by <amount>*<v>
  ;; and put the answer in <result>
  (assert (element? p space))
  (az:type-check Double-Float amount)
  (assert (element? v (translation-space space)))
  (assert (element? result space))
  (linear-mix 1.0d0 p amount v :result result))

;;;=======================================================

(defclass Euclidean-Space (Finite-Dimensional-Flat-Space) ())
(defclass Euclidean-Point (Flat-Point) ())

;;;------------------------------------------------------------

(defmethod l1-dist  ((p0 Euclidean-Point) (p1 Euclidean-Point)
		     &key
		     (space (home-space p0)))
  (az:type-check Euclidean-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (missing-method))

(defun l2-dist (p0 p1 &rest options)
  (sqrt (apply #'l2-dist2 p0 p1 options)))

(defmethod l2-dist2 ((p0 Euclidean-Point) (p1 Euclidean-Point)
		     &key
		     (space (home-space p0)))
  (az:type-check Euclidean-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (missing-method))

(defmethod sup-dist ((p0 Euclidean-Point) (p1 Euclidean-Point)
		     &key
		     (space (home-space p0)))
  (az:type-check Euclidean-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  (missing-method))

;;;=======================================================

(defclass Inner-Product-Space (Vector-Space)
     ((inner-product-fn
	:type Function
	:initform 'inner-product
	:reader inner-product-fn)) )

;;;=======================================================

(defclass Euclidean-Vector-Space (Euclidean-Space
				   Inner-Product-Space)
     ((block-subspace-table
	:type Hash-Table)
      (canonical-subspace-table
	:type Hash-Table)))

;;;-------------------------------------------------------

(defmethod make-canonical-basis-element ((sp Euclidean-Vector-Space)
					 (i Integer)
					 &rest options)
  (declare (ignore options))
  (missing-method))

;;;-------------------------------------------------------

(defmethod borrow-canonical-basis-element ((sp Euclidean-Vector-Space)
					   (i Integer)
					   &rest options)
  (declare (ignore options))
  (missing-method))

(defmacro with-canonical-basis-element ((vname sp i) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-canonical-basis-element ,sp ,i))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-element ,return-name ,sp)))))

;;;-------------------------------------------------------

(defmethod parent-space ((sp Euclidean-Vector-Space)) sp)

;;;-------------------------------------------------------

(defmethod block-subspace-table ((sp Euclidean-Vector-Space))
  (unless (slot-boundp sp 'block-subspace-table)
    (setf (slot-value sp 'block-subspace-table)
	  (make-hash-table :test #'eql)))
  (slot-value sp 'block-subspace-table))
      
;;;-------------------------------------------------------

(defmethod canonical-subspace-table ((sp Euclidean-Vector-Space))
  (unless (slot-boundp sp 'canonical-subspace-table)
    (setf (slot-value sp 'canonical-subspace-table)
	  (make-hash-table :test #'equal)))
  (slot-value sp 'canonical-subspace-table))
      
;;;=======================================================
;;;		    Euclidean-Vector
;;;=======================================================
;;; A parent class for objects that represent elements of 
;;; finite-dimensional inner product spaces:

(defclass Euclidean-Vector (Euclidean-Point
			     Abstract-Vector)
     ((home-space
	:type Euclidean-Vector-Space
	:accessor home-space
	:initarg :home-space)))

;;;-------------------------------------------------------

(defmethod dual-vector ((v0 Euclidean-Vector)
			&key result)
  (cond
    ((eq v0 result) result)
    ((null result) (az:copy v0))
    (t (az:copy v0 :result result))))

;;;-------------------------------------------------------

(defmethod fill-with-canonical-basis-vector! ((v0 Euclidean-Vector) i)
  (zero! v0)
  (setf (coordinate v0 i) 1.0d0)
  v0)

;;;-------------------------------------------------------

(defmethod inner-product ((v0 Euclidean-Vector)
			  (v1 Euclidean-Vector)
			  &key
			  (space (home-space v0)))
  (assert (element? v0 space))
  (assert (element? v1 space))
  (missing-method))

;;;------------------------------------------------------------

(defmethod l2-norm2 ((v0 Euclidean-Vector)
		     &key
		     (space (home-space v0)))
  (az:type-check Euclidean-Vector-Space space)
  (assert (element? v0 space))
  (missing-method))

(defmethod l2-norm ((v0 Euclidean-Vector)
		    &key
		    (space (home-space v0)))
  ;; this should work for anything for which <l2-norm2> is defined
  (az:type-check Euclidean-Vector-Space space)
  (assert (element? v0 space))
  (sqrt (l2-norm2 v0 :space space)))

(defmethod l1-norm ((v0 Euclidean-Vector)
		    &key
		    (space (home-space v0)))
  (az:type-check Euclidean-Vector-Space space)
  (assert (element? v0 space))
  (missing-method))

(defmethod sup-norm ((v0 Euclidean-Vector)
		     &key
		     (space (home-space v0)))
  (az:type-check Euclidean-Vector-Space space)
  (assert (element? v0 space))
  (missing-method))

;;;=======================================================
;;; Subspaces:
;;;=======================================================
;;; <nil> means "don't know" rather than "isn't".
;;; might change to 2 value return like <subtypep>

(defmethod subspace? ((sub Vector-Space) (sp Vector-Space))
  (or (eq sub sp)
      (proper-subspace? sub sp)))

;; default:
(defmethod proper-subspace? ((sub Vector-Space) (sp Vector-Space)) nil)

;;;-------------------------------------------------------
;;; If <sp0> and <sp1> have the same parent space, return the smallest
;;; subspace of that parent that contains both of them.

(defmethod spanning-space ((sp0 Vector-Space) (sp1 Vector-Space))
  (assert (eq (parent-space sp0)
	      (parent-space sp1)))
  (cond ((eq sp0 sp1) sp0)
	((proper-subspace? sp0 sp1) sp1)
	((proper-subspace? sp1 sp0) sp0)
	;; this isn't the smallest answer, but it's good enough.
	(t (parent-space sp0))))

;;;=======================================================
;;;		   Block-SubSpace
;;;=======================================================
;;; A block subspace is one spanned by the [start,end)
;;; canonical basis vectors.

(defclass Block-SubSpace (Euclidean-Vector-Space)
     ((parent-space
	:type Euclidean-Vector-Space
	:initarg :parent-space
	:reader   parent-space)
      (coord-start
	:type az:Array-Index
	:initarg :coord-start
	:reader coord-start)
      (coord-end
	:type az:Array-Index
	:initarg :coord-end
	:reader coord-end)))

;;;-------------------------------------------------------

(defmethod dimension ((sp Block-SubSpace))
  (- (coord-end sp) (coord-start sp)))

;;;-------------------------------------------------------

(defmethod print-object ((vs Block-SubSpace) str)
  (format str "(BSS ~a ~d ~d)"
	  (parent-space vs) (coord-start vs) (coord-end vs)))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((sp Block-SubSpace)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (< -1
	     (coord-start sp)
	     (coord-end sp)
	     (+ (dimension (parent-space sp)) 1))))

;;;-------------------------------------------------------

(defmethod proper-subspace? ((sub Block-SubSpace)
			     (sp Euclidean-Vector-Space))
  (eql (parent-space sub) sp))

(defmethod proper-subspace? ((sub Block-SubSpace)
			     (sp Block-SubSpace))
  (and (eql (parent-space sub) (parent-space sp))
       (<= (coord-start sp) (coord-start sub) (coord-end sub) (coord-end sp))))

;;;-------------------------------------------------------
;;; If <sp0> and <sp1> have the same parent space, return the smallest
;;; subspace of that parent that contains both of them.

(defmethod spanning-space ((sp0 Block-SubSpace) (sp1 Euclidean-Vector-Space))
  (if (subspace? sp0 sp1)
      sp1
      (error "No space spans ~s and ~s." sp0 sp1)) )

(defmethod spanning-space ((sp0 Euclidean-Vector-Space) (sp1 Block-SubSpace))
  (if (subspace? sp1 sp0)
      sp0
      (error "No space spans ~s and ~s." sp0 sp1)) )

(defmethod spanning-space ((sp0 Block-SubSpace) (sp1 Block-SubSpace))
  (if (eq (parent-space sp0) (parent-space sp1))
      (block-subspace (parent-space sp0)
		      (min (coord-start sp0) (coord-start sp1))
		      (max (coord-end sp0) (coord-end sp1)))
      (error "No space spans ~s and ~s." sp0 sp1)))

;;;=======================================================
;;; To ensure there is only one Block-SubSpace for a given
;;; <parent-space>, <start>, and <end>

(defmethod block-subspace-class ((sp Euclidean-Vector-Space))
  (missing-method))

(defmethod block-subspace ((sp Euclidean-Vector-Space) start
			   &optional (end (dimension sp)))
  (cond
    ((and (= start 0) (= end (dimension sp))) ;; trivial case
     sp)
    (t
     (assert (< -1 start end (+ (dimension sp) 1)))
     (let* ((starttable (block-subspace-table sp))
	    (endtable (gethash start starttable)))
       (when (null endtable)
	 (setf endtable (make-hash-table :test #'eql))
	 (setf (gethash start starttable) endtable))
       (let ((subspace (gethash end endtable)))
	 (when (null subspace)
	   (setf subspace (make-instance
			    (block-subspace-class sp)
			    :parent-space sp
			    :coord-start start
			    :coord-end end))
	   (setf (gethash end endtable) subspace))
	 subspace)))))

;;;-------------------------------------------------------
;;; The block-subspace of a Block-SubSpace is
;;; referred back to the parent-space.

(defmethod block-subspace ((sp Block-SubSpace) start
			   &optional (end (dimension sp)))
  (cond
    ((and (= start (coord-start sp)) (= end (coord-end sp))) ;; trivial case
     sp)
    (t
     (block-subspace (parent-space sp) start end))))

;;;-------------------------------------------------------
;;; a frequently occurring special case (isn't this a lot like cdr?)

(defmethod tail-space ((sp Euclidean-Vector-Space))
  (block-subspace sp (+ (coord-start sp ) 1) (coord-end sp)))

;;;=======================================================
;;;		   Canonical-SubSpace
;;;=======================================================
;;; A Canonical subspace is one spanned by the canonical
;;; basis vectors whose indices are listed in <index-list>.

(defclass Canonical-SubSpace (Euclidean-Vector-Space)
     ((parent-space
	:type Euclidean-Vector-Space
	:initarg :parent-space
	:reader parent-space)
      (index-list
	:type List
	:initarg :index-list
	:reader index-list)))

;;;-------------------------------------------------------

(defmethod dimension ((sp Canonical-SubSpace)) (length (index-list sp)))

;;;-------------------------------------------------------

(defmethod print-object ((sp Canonical-SubSpace) str)
  (format str "(BSS ~a ~d ~d)" (parent-space sp) (index-list sp)))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((sp Canonical-SubSpace)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names

  (assert (every #'(lambda (i) (< -1 i (dimension (parent-space sp))))
		 (index-list sp))))

;;;-------------------------------------------------------

(defmethod proper-subspace? ((sub Canonical-SubSpace)
			     (sp Euclidean-Vector-Space))
  (eql (parent-space sub) sp))

(defmethod proper-subspace? ((sub Canonical-SubSpace)
			     (sp Canonical-SubSpace))
  (and (eql (parent-space sub) (parent-space sp))
       (not (eq sub sp))
       (subsetp (index-list sub) (index-list sp))))

;;;-------------------------------------------------------
;;; If <sp0> and <sp1> have the same parent space, return the smallest
;;; subspace of that parent that contains both of them.

(defmethod spanning-space ((sp0 Canonical-SubSpace)
			   (sp1 Euclidean-Vector-Space))
  (if (subspace? sp0 sp1)
      sp1
      (error "No space spans ~s and ~s." sp0 sp1)))

(defmethod spanning-space ((sp0 Euclidean-Vector-Space)
			   (sp1 Canonical-SubSpace))
  (if (subspace? sp1 sp0)
      sp0
      (error "No space spans ~s and ~s." sp0 sp1)))

(defmethod spanning-space ((sp0 Canonical-SubSpace)
			   (sp1 Canonical-SubSpace))
  (if (eq (parent-space sp0) (parent-space sp1))
    (canonical-subspace (parent-space sp0)
			 (union (index-list sp0) (index-list sp1)))
    (error "No space spans ~s and ~s." sp0 sp1)))

;;;=======================================================
;;; To ensure there is only one Canonical-SubSpace for a given
;;; <parent-space>, <start>, and <end>

(defmethod canonical-subspace-class ((sp Euclidean-Vector-Space))
  (missing-method))

;;;-------------------------------------------------------

(defmethod canonical-subspace ((sp Euclidean-Vector-Space)
			       index-list)

  (setf index-list (sort (delete-duplicates index-list) #'<))
  (assert (every #'(lambda (i) (< -1 i (dimension sp)))
		 index-list))
  (cond
   ((loop for index in index-list
	for i from 0
	always (= i index))
     sp)
   (t 
    (let ((subspace (gethash index-list (canonical-subspace-table sp))))
      (when (null subspace)
	(setf subspace (make-instance
			   (canonical-subspace-class sp)
			 :parent-space sp
			 :index-list index-list))
	(setf (gethash index-list (canonical-subspace-table sp)) subspace))
      subspace))))

;;;-------------------------------------------------------
;;; The Canonical-subspace of a Canonical-SubSpace is
;;; referred back to the parent-space.

(defmethod canonical-subspace ((sp Canonical-SubSpace)
			       index-list)
  (cond ((equal index-list (index-list sp)) sp) ;; trivial case
	(t (canonical-subspace (parent-space sp) index-list))))

;;;============================================================
;;;		     Affine Spaces
;;;============================================================
;;; Flat Spaces that are not Vector spaces:

(defclass Affine-Space (Flat-Space)
     ((translation-space
	:type Vector-Space
	:reader translation-space)))

;;;-------------------------------------------------------
;;; make a Mapping from <sp1> to <sp0>,
;;; following convention of codomain first.

(defmethod make-affine-Mapping ((sp0 Affine-Space) (sp1 Affine-Space)
				       &rest options)
  (declare (ignore options))
  (missing-method))

;;;============================================================

(defclass Affine-Point (Flat-Point)
     ((home-space
	:type Affine-Space
	:accessor home-space
	:initarg :home-space))) 

;;;============================================================

(defclass Euclidean-Affine-Space (Euclidean-Space
				   Affine-Space)
     ((translation-space
	:type Euclidean-Vector-Space 
	:reader translation-space)))

;;;============================================================

(defclass Euclidean-Affine-Point (Euclidean-Point
				   Affine-Point)
     ((home-space
	:type Euclidean-Affine-Space
	:accessor home-space
	:initarg :home-space)))

