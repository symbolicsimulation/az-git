;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;;		    Direct-Sum-Space
;;;=======================================================
;;; Another class of euclidean spaces,
;;; resulting from direct sums of other spaces:

(defclass Direct-Sum-Space (Euclidean-Space)
     ((xspace
	:type Euclidean-Space
	:accessor xspace
	:initarg :xspace)
      (yspace
	:type Euclidean-Space
	:accessor yspace
	:initarg :yspace)))

;;;=======================================================
;;;		    Direct-Sum-Vector-Space
;;;=======================================================
;;; Another class of vector spaces,
;;; resulting from direct sums of other spaces:

(defclass Direct-Sum-Vector-Space (Direct-Sum-Space Euclidean-Vector-Space)
     ((xspace
	:type Euclidean-Vector-Space
	:accessor xspace
	:initarg :xspace)
      (yspace
	:type Euclidean-Vector-Space
	:accessor yspace
	:initarg :yspace)))

;;;-------------------------------------------------------
;;; To ensure there is only one Direct-Sum-Vector-Space
;;; for a given pair of summands.

(defvar *Direct-Sum-Vector-Space-table* (make-hash-table :test #'eq))

(defmethod direct-sum ((xsp Euclidean-Vector-space)
		       (ysp Euclidean-Vector-space))
  (let ((inner-table (gethash xsp *Direct-Sum-Vector-Space-table* nil))
	(dsp nil))
    (cond ((null inner-table)
	   (setf inner-table (make-hash-table :test #'eq))
	   (setf (gethash xsp *Direct-Sum-Vector-Space-table*) inner-table))
	  (t
	   (setf dsp (gethash ysp inner-table nil))))
    (when (null dsp)
      (setf dsp (make-instance 'Direct-Sum-Vector-Space
			       :xspace xsp :yspace ysp))
      (setf (gethash ysp inner-table nil) dsp))
    dsp))

;;;-------------------------------------------------------

(defmethod dimension ((vs Direct-Sum-Vector-Space))
  (+ (dimension (xspace vs)) (dimension (yspace vs))))

;;;-------------------------------------------------------
;;; make a Mapping from <ds1> to <ds0>,
;;; following convention of codomain first

(defmethod make-linear-mapping ((codomain Direct-Sum-Vector-Space)
				(domain Direct-Sum-Vector-Space)
				&rest options)
  (declare (ignore options))
  (make-instance
    '2x2-Direct-Sum-Mapping
    :codomain codomain
    :domain domain
    :txx (make-linear-Mapping (xspace codomain) (xspace domain))
    :txy (make-linear-Mapping (xspace codomain) (yspace domain))
    :tyx (make-linear-Mapping (yspace codomain) (xspace domain))
    :tyy (make-linear-Mapping (yspace codomain) (yspace domain))))

;;;=======================================================
;;;		   Direct-Sum-Vector
;;;=======================================================
;;; a class for elements of some Direct-Sum-Vector-Space's:

(defclass Direct-Sum-Vector (Euclidean-Vector)
     ((x
	:type Euclidean-Vector
	:accessor x
	:initarg :x)
      (y
	:type Euclidean-Vector
	:accessor y
	:initarg :y)))

;;;-------------------------------------------------------

(defmethod home-space ((v Direct-Sum-Vector))
  (direct-sum (home-space (x v)) (home-space (y v))))

;;;-------------------------------------------------------

(defmethod element? ((v Direct-Sum-Vector) (sp Direct-Sum-Vector-Space))
  (and (element? (x v) (xspace sp))
       (element? (y v) (yspace sp))))

;;;------------------------------------------------------------

(defmethod make-element ((vs Direct-Sum-Vector-Space)
			 &rest options)
  (declare (ignore options))
  (make-instance 'Direct-Sum-Vector
		 :home-space vs
		 :x (make-element (xspace vs))
		 :y (make-element (yspace vs))))

(defmethod make-zero-element ((vs Direct-Sum-Vector-Space) &rest options)
  (declare (ignore options))
  (make-instance 'Direct-Sum-Vector
		 :home-space vs
		 :x (make-zero-element (xspace vs))
		 :y (make-zero-element (yspace vs))))

;;;-------------------------------------------------------

(defmethod dimension ((v Direct-Sum-Vector))
  (+ (dimension (x v)) (dimension (y v))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((v Direct-Sum-Vector))
  (make-instance (class-of v)
		 :home-space (home-space v)
		 :x (az:copy (x v))
		 :y (az:copy (y v))))

(defmethod az:copy-to! ((v Direct-Sum-Vector) (result Direct-Sum-Vector))
  (setf (home-space result) (home-space v))
  (az:copy (x v) :result (x result))
  (az:copy (y v) :result (y result))
  result)

;;;------------------------------------------------------------

#+symbolics
(defmethod format-object ((v Direct-Sum-Vector)
			  &optional (stream *standard-output*))
  (print (class-of v) stream)
  (format stream "~%x:")
  (format-object (x v) stream)
  (format stream "~%y:")
  (format-object (y v) stream)) 

;;;------------------------------------------------------------

(defmethod fill-randomly! ((v Direct-Sum-Vector)
			   &rest options)
  (apply #'fill-randomly! (x v) options)
  (apply #'fill-randomly! (y v) options)
  v)

;;;------------------------------------------------------------

(defmethod fill-with-canonical-basis-vector! ((v Direct-Sum-Vector) k)
  (zero! v)
  (let ((n (dimension (x v))))
    (if (< k n)
	(setf (x v) (fill-with-canonical-basis-vector! (x v) k))
	(setf (y v) (fill-with-canonical-basis-vector! (y v) (- k n)))))
  v)

;;;------------------------------------------------------------

(defmethod coordinate ((v Direct-Sum-Vector) k
		       &key
		       (coordinate-system :standard))
  (declare (ignore coordinate-system))
  
  (let ((n (dimension (x v))))
    (if (< k n)
	(coordinate (x v) k)
	(coordinate (y v) (- k n)))))

(defmethod (setf coordinate) (new (v Direct-Sum-Vector) k
				  &key
				  (coordinate-system :standard))
  (declare (ignore coordinate-system))
  
  (let ((n (dimension (x v))))
    (if (< k n)
	(setf (coordinate (x v) k) new)
	(setf (coordinate (y v) (- k n)) new))))

;;;------------------------------------------------------------

(defmethod zero! ((v Direct-Sum-Vector)
		  &key (space (home-space v)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  (zero! (x v))
  (zero! (y v))
  v)

;;;------------------------------------------------------------

(defmethod minus ((v Direct-Sum-Vector)
		  &key
		  (space (home-space v))
		  (result (az:copy v) result-supplied?))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  (cond ((and result-supplied? (not (eq v result)))
	 (minus (x v) :result (x result) :space (xspace space))
	 (minus (y v) :result (y result) :space (yspace space)))
	(t
	 (minus! (x result) :space (xspace space))
	 (minus! (y result) :space (yspace space))))
  result)

(defmethod minus! ((v Direct-Sum-Vector)
		   &key
		   (space (home-space v)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  (minus! (x v) :space (xspace space))
  (minus! (y v) :space (yspace space))
  v)

;;;------------------------------------------------------------

(defmethod scale ((a T) (v Direct-Sum-Vector)
		  &key
		  (space (home-space v))
		  (result (az:copy v) result-supplied?))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  
  (cond ((and result-supplied? (not (eq v result)))
	 (scale a (x v) :result (x result) :space (xspace space))
	 (scale a (y v) :result (y result) :space (yspace space)))
	(t
	 (scale! a (x result) :space (xspace space))
	 (scale! a (y result) :space (yspace space))))
  result)

(defmethod scale! ((a T) (v Direct-Sum-Vector)
		     &key
		     (space (home-space v)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  
  (scale! a (x v) :space (xspace space))
  (scale! a (y v) :space (yspace space))
  v)

;;;------------------------------------------------------------

(defmethod linear-mix ((a0 T) (v0 Direct-Sum-Vector)
		       (a1 T) (v1 Direct-Sum-Vector)
		       &key
		       (space (home-space v0))
		       (result (az:copy v0) result-supplied?))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))
  (cond (result-supplied?
	 (linear-mix a0 (x v0) a1 (x v1)
		     :result (x result) :space (xspace space))
	 (linear-mix a0 (y v0) a1 (y v1)
		     :result (y result) :space (yspace space)))
	(t
	 (linear-mix a0 (x result) a1 (x v1)
		     :result (x result) :space (xspace space))
	 (linear-mix a0 (y result) a1 (y v1)
		     :result (y result) :space (yspace space))))
  result)

;;;------------------------------------------------------------

(defmethod inner-product ((v0 Direct-Sum-Vector)
			  (v1 Direct-Sum-Vector)
			  &key
			  (space (home-space v0)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v0 space))
  (assert (element? v1 space))

  (+ (inner-product (x v0) (x v1) :space (xspace space))
     (inner-product (y v0) (y v1) :space (yspace space))))

(defmethod l2-norm2 ((v Direct-Sum-Vector)
		     &key
		     (space (home-space v)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  (+ (l2-norm2 (x v) :space (xspace space))
     (l2-norm2 (y v) :space (yspace space))))

(defmethod l1-norm ((v Direct-Sum-Vector)
		    &key
		    (space (home-space v)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  (+ (l1-norm (x v) :space (xspace space))
     (l1-norm (y v) :space (yspace space))))

(defmethod sup-norm ((v Direct-Sum-Vector)
		     &key
		     (space (home-space v)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? v space))
  (max (sup-norm (x v) :space (xspace space))
       (sup-norm (y v) :space (yspace space))))

;;;=======================================================
;;;		    Direct-Sum-Affine-Space
;;;=======================================================
;;; Another class of Affine spaces,
;;; resulting from direct sums of other spaces:
;;; The direct sum of two affine spaces is an affine space
;;; whose points are ordered pairs of points from the
;;; two spaces and whose translation space is the direct sum
;;; of the two translation spaces.

(defclass Direct-Sum-Affine-Space (Direct-Sum-Space Euclidean-Affine-Space)
     ((xspace
	:type Euclidean-Affine-Space
	:accessor xspace
	:initarg :xspace)
      (yspace
	:type Euclidean-Affine-Space
	:accessor yspace
	:initarg :yspace)))

;;;-------------------------------------------------------

(defmethod translation-space ((sp Direct-Sum-Affine-Space))
  (direct-sum (translation-space (xspace sp))
	      (translation-space (yspace sp))))

;;;-------------------------------------------------------
;;; To ensure there is only one Direct-Sum-Affine-Space
;;; for a given pair of summands.

(defvar *Direct-Sum-Affine-Space-table* (make-hash-table :test #'eq))

(defmethod direct-sum ((xsp Euclidean-Affine-Space)
		       (ysp Euclidean-Affine-Space))
  (let ((inner-table (gethash xsp *Direct-Sum-Affine-Space-table* nil))
	(dsp nil))
    (cond ((null inner-table)
	   (setf inner-table (make-hash-table :test #'eq))
	   (setf (gethash xsp *Direct-Sum-Affine-Space-table*) inner-table))
	  (t
	   (setf dsp (gethash ysp inner-table nil))))
    (when (null dsp)
      (setf dsp (make-instance 'Direct-Sum-Affine-Space
			       :xspace xsp :yspace ysp))
      (setf (gethash ysp inner-table nil) dsp))
    dsp))

;;;-------------------------------------------------------

(defmethod dimension ((vs Direct-Sum-Affine-Space))
  (+ (dimension (xspace vs)) (dimension (yspace vs))))

;;;=======================================================
;;;		   Direct-Sum-Point
;;;=======================================================
;;; a class for elements of some Direct-Sum-Affine-Space's:

(defclass Direct-Sum-Point (Euclidean-Affine-Point)
     ((x
	:type Euclidean-Affine-Point
	:accessor x
	:initarg :x)
      (y
	:type Euclidean-Affine-Point
	:accessor y
	:initarg :y)))

;;;-------------------------------------------------------

(defmethod home-space ((p Direct-Sum-Point))
  (direct-sum (home-space (x p)) (home-space (y p))))

;;;-------------------------------------------------------

(defmethod element? ((p Direct-Sum-Point) (sp Direct-Sum-Affine-Space))
  (and (element? (x p) (xspace sp))
       (element? (y p) (yspace sp))))

;;;------------------------------------------------------------

(defmethod make-element ((sp Direct-Sum-Affine-Space)
			 &rest options)
  (declare (ignore options))
  (make-instance 'Direct-Sum-Point
		 :home-space sp
		 :x (make-element (xspace sp))
		 :y (make-element (yspace sp))))

;;;-------------------------------------------------------

(defmethod dimension ((p Direct-Sum-Point))
  (+ (dimension (x p)) (dimension (y p))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((p Direct-Sum-Point))
  (make-instance (class-of p)
		 :home-space (home-space p)
		 :x (az:copy (x p))
		 :y (az:copy (y p))))

(defmethod az:copy-to! ((p Direct-Sum-Point) (result Direct-Sum-Point))
  (setf (home-space result) (home-space p))
  (az:copy (x p) :result (x result))
  (az:copy (y p) :result (y result))
  result)

;;;------------------------------------------------------------

#+symbolics
(defmethod format-object ((p Direct-Sum-Point)
			  &optional (stream *standard-output*))
  (print (class-of p) stream)
  (format stream "~%x:")
  (format-object (x p) stream)
  (format stream "~%y:")
  (format-object (y p) stream)) 

;;;------------------------------------------------------------

(defmethod fill-randomly! ((p Direct-Sum-Point)
			   &rest options)
  (apply #'fill-randomly! (x p) options)
  (apply #'fill-randomly! (y p) options)
  p)

;;;------------------------------------------------------------

(defmethod coordinate ((p Direct-Sum-Point) k
		       &key
		       (coordinate-system :standard))
  (declare (ignore coordinate-system))
  
  (let ((n (dimension (x p))))
    (if (< k n)
	(coordinate (x p) k)
	(coordinate (y p) (- k n)))))

(defmethod (setf coordinate) (new (p Direct-Sum-Point) k
				  &key
				  (coordinate-system :standard))
  (declare (ignore coordinate-system))

  (let ((n (dimension (x p))))
    (if (< k n)
	(setf (coordinate (x p) k) new)
	(setf (coordinate (y p) (- k n)) new))))

;;;------------------------------------------------------------

(defmethod affine-mix ((a0 T) (p0 Direct-Sum-Point)
		       (a1 T) (p1 Direct-Sum-Point)
		       &key
		       (space (home-space p0))
		       (result (make-element space) result-supplied?))
  (az:type-check Double-Float a0 a1)
  (assert (= 1.0d0 (+ a0 a1)))
  (assert (element? p0 space))
  (assert (element? p1 space))
  (assert (element? result space))
  (cond (result-supplied?
	 (affine-mix a0 (x p0) a1 (x p1) :result (x result))
	 (affine-mix a0 (y p0) a1 (y p1) :result (y result)))
	(t
	 (affine-mix a0 (x result) a1 (x p1) :result (x result))
	 (affine-mix a0 (y result) a1 (y p1) :result (y result))))
  result)

;;;------------------------------------------------------------

(defmethod sub ((p0 Direct-Sum-Point)
		(p1 Direct-Sum-Point)
		&key
		(space (home-space p0))
		(result (make-element (translation-space space))))
  (assert (and (eq space (home-space p0))
	       (eq space (home-space p1))))
  (assert (element? result (translation-space space)))
  (sub (x p0) (x p1) :result (x result))
  (sub (y p0) (y p1) :result (y result)))

;;;------------------------------------------------------------

(defmethod move-by! ((p Direct-Sum-Point) amount (v Direct-Sum-Vector)
		     &key
		     (space (home-space p)))

  ;; destructively move the point <p> by <amount>*<p>
  ;; use <move-by> rather than <move-by!> to get error checking
  ;; in the component spaces.

  (az:type-check Euclidean-Space space)
  (az:type-check Double-Float amount)
  (assert (element? p space))
  (assert (element? v (translation-space space)))
  
  (move-by (x p) amount (x v) :space (xspace space) :result (x p))
  (move-by (y p) amount (y v) :space (yspace space) :result (y p))
  p)

(defmethod move-by-to! ((p Direct-Sum-Point) amount (v Direct-Sum-Vector)
			(result Direct-Sum-Point)
			&key
			(space (home-space p)))

  ;; move the point <p> by <amount>*<v>
  ;; and put the answer in <result>
  ;; use <move-by> rather than <move-by-to!> to get error checking
  ;; in the component spaces.

  (az:type-check Euclidean-Space space)
  (az:type-check Double-Float amount)
  (assert (element? p space))
  (assert (element? v (translation-space space)))
  (assert (element? result space))
  
  (move-by (x p) amount (x v) :space (xspace space) :result (x result))
  (move-by (y p) amount (y v) :space (yspace space) :result (y result))
  result)
 

;;;------------------------------------------------------------

(defmethod l2-dist2 ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point)
		     &key
		     (space (home-space p0)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))

  (+ (l2-dist2 (x p0) (x p1) :space (xspace space))
     (l2-dist2 (y p0) (y p1) :space (yspace space))))

(defmethod l1-dist ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point)
		    &key
		    (space (home-space p0)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  
  (+ (l1-dist (x p0) (x p1) :space (xspace space))
     (l1-dist (y p0) (y p1) :space (yspace space))))

(defmethod sup-dist ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point)
		     &key
		     (space (home-space p0)))
  (az:type-check Direct-Sum-Space space)
  (assert (element? p0 space))
  (assert (element? p1 space))
  
  (max (sup-dist (x p0) (x p1) :space (xspace space))
       (sup-dist (y p0) (y p1) :space (yspace space))))

