;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Cactus)

(eval-when (compile load eval)
  (export '(Cactus-Object

	    format-object

	    Abstract-Space
	    home-space
	    element?
	    make-element
	    make-random-element
	    borrow-element return-element
	    with-borrowed-element with-borrowed-elements
	    flat-space? vector-space?

	    Flat-Space
	    translation-space
	    Finite-Dimensional-Flat-Space
	    finite-dimensional?

	    Flat-Point
	    affine-mix convex-mix
	    fill-randomly!
	    dimension coordinate
	    l2-dist2 l2-dist l1-dist sup-dist

	    Vector-Space
	    dual-space
	    zero-element? make-zero-element
	    make-linear-Mapping
	    Inner-Product-Space
	    Euclidean-Vector-Space
	    make-canonical-basis-element
	    borrow-canonical-basis-element
	    with-canonical-basis-element

	    Abstract-Vector
	    dual-vector
	    scale scale! zero!
	    linear-mix add minus minus!
	    Euclidean-Vector
	    inner-product
	    l2-norm2 l2-norm l1-norm sup-norm
	    project embed
	    subspace? proper-subspace? spanning-space
	    sub move-by
	   
	    Affine-Space
	    make-affine-Mapping
	    Euclidean-Affine-Space
	    Affine-Point
	    Euclidean-Affine-Point

	    LPoint-Space
	    Lpoint
	    make-lpoint-from-array

	    LVector-Space
	    Lvector-Super
	    Lvector
	    make-lvector-from-array
	    
	    
	    Direct-Sum-Space
	    Direct-Sum-Vector-Space
	    xspace yspace
	    direct-sum
	    Direct-Sum-Vector
	    x y
	    Direct-Sum-Affine-Space
	    Direct-Sum-Point

	    domain codomain transform transpose
	    pseudo-inverse
	    make-identity--of-class identity!
	    tensor-product

	    Matrix
	    read-matrix-from-file

	    Gauss Givens Householder Pivot
	    
	    compose rotate-mapping

	    inverse-lu-decompose inverse-lu-decompose!
	    inverse-qr-decompose inverse-qr-decompose!
	    qr-decompose qr-decompose!
	    inverse-lq-decompose inverse-lq-decompose!
	    lq-decompose lq-decompose!
	    cholesky-decompose cholesky-decompose!
	    eigen-decompose eigen-decompose!
	    singular-value-decompose singular-value-decompose!

	    determinant

	    Objective-Function last-value evaluation-count
	    C1-Objective-Function minus-grad
	    forward-dif-minus-grad
	    central-dif-minus-grad
	    Nonlinear-Least-Squares-Objective-Function

	    Objective-Lisp-Function
	    transform2
	    
	    Optimizer
	    Gradient-Optimizer Discrete-Gradient-Optimizer
	    Hessian-Optimizer Discrete-Hessian-Optimizer
	    Amoeba-Optimizer
	    Line-Search-Optimizer
	    Direction-Set-Optimizer
	    Steepest-Descent-Optimizer Discrete-Steepest-Descent-Optimizer
	    Conjugate-Gradient-Optimizer Discrete-Conjugate-Gradient-Optimizer
	    Quasi-Newton-Optimizer
	    Inverse-Matrix-QN-Optimizer
	    Inverse-DFP-Optimizer Discrete-Inverse-DFP-Optimizer
	    Inverse-BFGS-Optimizer Discrete-Inverse-BFGS-Optimizer
	    Cholesky-QN-Optimizer
	    Cholesky-DFP-Optimizer Discrete-Cholesky-DFP-Optimizer
	    Cholesky-BFGS-Optimizer Discrete-Cholesky-BFGS-Optimizer
	    Trust-Region-Optimizer
	    Newton-Optimizer)))

