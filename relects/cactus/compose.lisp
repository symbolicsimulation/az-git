;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Cactus -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;;=======================================================

(in-package :Cactus)

;;;=======================================================
;;;			Compose
;;;=======================================================

(defmethod compose ((t0 Mapping)
		    (t1 Mapping)
		    &key result)
  (assert (eql (domain t0) (codomain t1)))
  (assert (null result))
  (make-instance 'Product-Mapping
		 :factors (list t0 t1)))
  
;;;-------------------------------------------------------

(defmethod compose ((t0 Linear-Mapping)
		    (t1 Linear-Mapping)
		    &key
		    (result
		      (make-instance 'Matrix
				     :domain (domain t1)
				     :codomain (codomain t0))
		      result-supplied?))
  (assert (eql (domain t0) (codomain t1)))
  (when result-supplied?
    (az:type-check Linear-Mapping result)
    (assert (eql (domain result) (domain t1)))
    (assert (eql (codomain t0) (codomain result))))
  (cond
    ((eq result t0) (compose-left! t0 t1))
    ((eq result t1) (compose-right! t0 t1))
    (t (compose-to! t0 t1 result)))
  result)

;;;-------------------------------------------------------
;;; 2 special cases:

(defmethod compose ((t0 Identity-Map)
		    (t1 Mapping)
		    &key result)
  (assert (eq (domain t0) (codomain t1)))
  (cond
    ((eq result t1) t1)
    ((null result) (az:copy t1))
    (t (az:copy t1 :result result))))

(defmethod compose ((t0 Mapping)
		    (t1 Identity-Map)
		    &key result)
  (assert (eq (domain t0) (codomain t1)))
  (cond
    ((eq result t0)
     t0)
    ((null result) (az:copy t0))
    (t (az:copy t0 :result result))))

;;;-------------------------------------------------------
;;; 2 special cases:

(defmethod compose ((t0 Identity-Map)
		    (t1 Linear-Mapping)
		    &key result)
  (assert (eq (domain t0) (codomain t1)))
  (cond
    ((eq result t1) t1)
    ((null result) (az:copy t1))
    (t (az:copy t1 :result result))))

(defmethod compose ((t0 Linear-Mapping)
		    (t1 Identity-Map)
		    &key result)
  (assert (eq (domain t0) (codomain t1)))
  (cond
    ((eq result t0) t0)
    ((null result) (az:copy t0))
    (t (az:copy t0 :result result))))

;;;=======================================================
;;;	    Compose-to!
;;;=======================================================
;;; Default method:
;;; Apply t0 to cols of t1 to produce cols of result

(defmethod compose-to! ((t0 Linear-Mapping)
			(t1 Linear-Mapping)
			(result Matrix-Super))
  
  (assert (not (eq t0 result)))
  (assert (not (eq t1 result)))
  (with-borrowed-elements ((v1 (codomain t1))
			   (v2 (codomain result)))
    (bm:fast-loop (i :from (coord-start (domain t1))
		     :below (coord-end (domain t1)))
      (setf v1 (col t1 i :result v1))
      (transform t0 v1 :result v2)
      (setf (col result i) v2)))
  result)

;;;-------------------------------------------------------
;;; If the t0 is a Matrix-Super, then this might be better than the default:
;;; Apply (transpose t1) to rows of t0 to produce rows of result

(defmethod compose-to! ((t0 Matrix-Super)
			(t1 Linear-Mapping)
			(result Matrix-Super))

  (assert (not (eq t0 result)))
  (assert (not (eq t1 result)))
  (with-borrowed-elements ((vr (dual-space (domain result)))
			   (v0 (dual-space (domain t0))))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (coord-end (codomain t0)))
      (setf v0 (row t0 i :result v0))
      (transform-transpose t1 v0 :result vr)
      (setf (row result i) vr)))
  result)

;;;-------------------------------------------------------

(defmethod compose-to! :after ((t0 Linear-Mapping)
			       (t1 Linear-Mapping)
			       (result Matrix-Super))
  (delete-all-properties! result)
  (setf (bands-start result) (+ (bands-start t0) (bands-start t1)))
  (setf (bands-end result) (+ (bands-end t0) (bands-end t1) -1))
  (when *verify-properties?* (assert (verify-bands? result))))

(defmethod compose-to! :after ((t0 Linear-Mapping)
			       (t1 Linear-Mapping)
			       (result Restricted-Matrix))
  (delete-all-properties! (source result)))

;;;=======================================================
;;;	    Compose-Left! and Compose-Right! 
;;;=======================================================
;;;
;;; destructive versions of compose, should be thought of as part of the
;;; Modifier protocol rather than the Linear-Mapping protocol.
;;; 

(defmethod compose-left! :before ((t0 Linear-Mapping) (t1 Modifier))
	   (assert (automorphic? t1))
	   (assert (eql (domain t0) (codomain t1)))
	   (assert (eql (domain t0) (domain t1)))
	   (assert (not (eq t0 t1))))

(defmethod compose-right! :before ((t0 Modifier) (t1 Linear-Mapping))
	   (assert (automorphic? t0))
	   (assert (eql (domain t0) (codomain t1)))
	   (assert (eql (codomain t0) (codomain t1)))
	   (assert (not (eq t0 t1))))

;;;-------------------------------------------------------
;;; slow default primary methods:

(defmethod compose-right! ((t0 Linear-Mapping)
			   (t1 Linear-Mapping))
  (with-borrowed-element (result (home-space t1))
    (compose-to! t0 t1 result)
    (az:copy result :result t1))
  t1)

(defmethod compose-left! ((t0 Linear-Mapping)
			  (t1 Linear-Mapping))
  (with-borrowed-element (result (home-space t0))
    (compose-to! t0 t1 result)
    (az:copy result :result t0))
  t0)

;;;-------------------------------------------------------
;;; trivial special cases:

(defmethod compose-right! ((t0 Identity-Map)
			   (t1 Linear-Mapping))
  t1)

(defmethod compose-left! ((t0 Linear-Mapping)
			  (t1 Identity-Map))
  t0)

;;; need these because otherwise modifier methods might be "more
;;; specialized".

(defmethod compose-right! ((t0 Identity-Map)
			   (t1 Matrix-Super))
  t1)

(defmethod compose-left! ((t0 Matrix-Super)
			  (t1 Identity-Map))
  t0)

;;;-------------------------------------------------------

(defmethod compose-left! :after ((t0 Matrix-Super) (t1 Modifier))
  (delete-all-properties! t0)
  (setf (bands-start t0) (+ (bands-start t0) (bands-start t1)))
  (setf (bands-end t0) (+ (bands-end t0) (bands-end t1) -1))
  (when *verify-properties?* (assert (verify-bands? t0))))

(defmethod compose-left! :after ((t0 Restricted-Matrix) (t1 Modifier))
  (delete-all-properties! (source t0)))

(defmethod compose-right! :after ((t0 Modifier) (t1 Matrix-Super))
  (delete-all-properties! t1)
  (setf (bands-start t1) (+ (bands-start t0) (bands-start t1)))
  (setf (bands-end t1) (+ (bands-end t0) (bands-end t1) -1))
  (when *verify-properties?* (assert (verify-bands? t1)))) 

(defmethod compose-right! :after ((t0 Modifier) (t1 Restricted-Matrix))
  (delete-all-properties! (source t1)))


;;;=======================================================
;;;		     Compose-Inner
;;;=======================================================
#||latex
Compute $A^\dagger \circ B$ with out forming $A^\dagger$.
||#

(defmethod compose-inner ((t0 Linear-Mapping)
			  (t1 Linear-Mapping)
			  &key
			  (result
			    (make-instance 'Matrix
					   :domain (domain t1)
					   :codomain (domain t0))
			    result-supplied?))
  
  (assert (eql (codomain t0) (codomain t1)))
  (when result-supplied?
    (az:type-check Linear-Mapping result)
    (assert (eql (domain result) (domain t1)))
    (assert (eql (domain t0) (codomain result))))
  (cond
    ((eq result t0) (compose-inner-left! t0 t1))
    ((eq result t1) (compose-inner-right! t0 t1))
    (t (compose-inner-to! t0 t1 result)))
  result)

;;;-------------------------------------------------------
;;; special cases:

(defmethod compose-inner ((t0 Symmetric)
			  (t1 Linear-Mapping)
			  &key result)
  (if result
      (compose t0 t1 :result result)
      (compose t0 t1)))

(defmethod compose-inner ((t0 Linear-Mapping)
			  (t1 Identity-Map)
			  &key result)
  (assert (eq (codomain t0) (codomain t1)))
  (if result
      (transpose t0 :result result)
      (transpose t0))) 

;;;=======================================================
;;;		     Compose-Inner-To!
;;;=======================================================
;;; Apply (transpose t0) to cols of t1 to produce cols of result

(defmethod compose-inner-to! :before ((t0 Linear-Mapping)
				      (t1 Linear-Mapping)
				      (result Linear-Mapping))
  
  (assert (not (eq t0 result)))
  (assert (not (eq t1 result))))

;;;-------------------------------------------------------

(defmethod compose-inner-to! ((t0 Linear-Mapping)
			      (t1 Linear-Mapping)
			      (result Matrix-Super))
  (with-borrowed-elements ((v1 (codomain t1))
			   (v2 (codomain result)))
    (bm:fast-loop (i :from (coord-start (domain t1)) :below (coord-end (domain t1)))
      (setf v1 (col t1 i :result v1))
      (transform-transpose t0 v1 :result v2)
      (setf (col result i) v2)))
  result)

;;;-------------------------------------------------------
;;; Apply (transpose t1) to rows of (transpose t0)==cols of t0
;;; to produce cols of result

(defmethod compose-inner-to! ((t0 Matrix-Super)
			      (t1 Linear-Mapping)
			      (result Matrix-Super))
  (with-borrowed-elements ((v0 (codomain t0))
			   (v2 (domain result)))
    (bm:fast-loop (i :from (coord-start (domain t0)) :below (coord-end (domain t0)))
      (setf v0 (col t0 i :result v0))
      (transform-transpose t1 v0 :result v2)
      (setf (row result i) v2)))
  result)

;;;-------------------------------------------------------

(defmethod compose-inner-to! :after ((t0 Linear-Mapping)
				     (t1 Linear-Mapping)
				     (result Matrix-Super))
  (delete-all-properties! result) 
  (setf (bands-start result) (+ (- 1 (bands-end t0)) (bands-start t1)))
  (setf (bands-end result) (+ (- 1 (bands-start t0)) (bands-end t1) -1))
  (when *verify-properties?* (assert (verify-bands? result))))

(defmethod compose-inner-to! :after ((t0 Linear-Mapping)
				     (t1 Linear-Mapping)
				     (result Restricted-Matrix))
  (delete-all-properties! (source result)))

;;;=======================================================
;;;		  Compose-inner-right!
;;;=======================================================
#||latex

 Destructively replace $T_1$ with $T_0^\dagger \circ T_1$, 
 without forming $T_0^\dagger$

||#

(defmethod compose-inner-right! :before ((t0 Modifier)
					 (t1 Linear-Mapping))
  (assert (automorphic? t0))
  (assert (eql (codomain t0) (codomain t1)))
  (assert (eql (domain t0) (codomain t1)))
  (assert (not (eq t0 t1))))

;;;-------------------------------------------------------
;;; slow default methods:

(defmethod compose-inner-right! ((t0 Linear-Mapping)
				 (t1 Linear-Mapping))
  (assert (not (eq t0 t1)))
  (with-borrowed-element (result (home-space t1))
    (compose-inner-to! t0 t1 result)
    (az:copy result :result t1))
  t1)

(defmethod compose-inner-left! ((t0 Linear-Mapping)
				(t1 Linear-Mapping))
  (assert (not (eq t0 t1)))
  (with-borrowed-element (result (home-space t0))
    (compose-inner-to! t0 t1 result)
    (az:copy result :result t0))
  t0)

;;;-------------------------------------------------------

(defmethod compose-inner-right! :after ((t0 Modifier) (t1 Matrix-Super))
  (delete-all-properties! t1) 
  (let ((start (bands-start t1))
	(end (bands-end t1)))
    (setf (bands-start t1) (+ (- 1 (bands-end t0)) start))
    (setf (bands-end t1) (+ (- 1 (bands-start t0)) end -1)))
  (when *verify-properties?* (assert (verify-bands? t1))))

(defmethod compose-inner-right! :after ((t0 Modifier) (t1 Restricted-Matrix))
  (delete-all-properties! (source t1)))

;;;=======================================================
;;;		     Compose-Outer
;;;=======================================================
;;;            t                  t
;;; computes AB  without forming B
;;; 

(defmethod compose-outer ((t0 Linear-Mapping)
			  (t1 Linear-Mapping)
			  &key
			  (result
			    (make-instance 'Matrix
					   :domain (codomain t1)
					   :codomain  (codomain t0))
			    result-supplied?))
  (assert (eql (domain t0) (domain t1)))
  (when result-supplied?
    (az:type-check Linear-Mapping result)
    (assert (eql (domain result) (codomain t1)))
    (assert (eql (codomain t0) (codomain result))))
  (cond
    ((eq result t0) (compose-outer-left! t0 t1))
    ((eq result t1) (compose-outer-right! t0 t1))
    (t (compose-outer-to! t0 t1 result)))
  result)

;;;-------------------------------------------------------
;;; save writing code for special cases:

(defmethod compose-outer ((t0 Linear-Mapping)
			  (t1 Symmetric)
			  &key result)
  (if result
      (compose t0 t1 :result result)
      (compose t0 t1)))

(defmethod compose-outer ((t0 Identity-Map)
			  (t1 Linear-Mapping)
			  &key result)
  (assert (eq (domain t0) (domain t1)))
  (if result
      (transpose t1 :result result)
      (transpose t1)))

;;;=======================================================
;;; compose-outer-to!
;;;=======================================================
;;; Default method:
;;; Apply t0 to cols of (transpose t1)===rows of t1
;;; to produce cols of result

(defmethod compose-outer-to! ((t0 Linear-Mapping)
			      (t1 Linear-Mapping)
			      (result Matrix-Super))
  (with-borrowed-element (v1 (domain t1))
    (with-borrowed-element (v2 (codomain t0))
      (bm:fast-loop (i :from (coord-start (domain result))
		       :below (coord-end (domain result)))
	(setf v1 (row t1 i :result v1))
	(transform t0 v1 :result v2)
	(setf (col result i) v2))))
  result)

;;;-------------------------------------------------------
;;; Apply t1 to rows of t0 to produce rows of result

(defmethod compose-outer-to! ((t0 Matrix-Super)
			      (t1 Linear-Mapping)
			      (result Matrix-Super))
  (with-borrowed-element (v0 (domain t0))
    (with-borrowed-element (v2 (domain result))
      (bm:fast-loop (i :from (coord-start (codomain t0)) :below (coord-end (codomain t0)))
	(setf v0 (row t0 i :result v0))
	(transform t1 v0 :result v2)
	(setf (row result i) v2))))
  result)

;;;-------------------------------------------------------

(defmethod compose-outer-to! :after ((t0 Linear-Mapping)
				     (t1 Linear-Mapping)
				     (result Matrix-Super))
  (delete-all-properties! result) 
  (setf (bands-start result) (+ (bands-start t0) (- 1 (bands-end t1))))
  (setf (bands-end result) (+ (bands-end t0) (- 1 (bands-start t1)) -1))
  (when *verify-properties?* (assert (verify-bands? result))))

(defmethod compose-outer-to! :after ((t0 Linear-Mapping)
				     (t1 Linear-Mapping)
				     (result Restricted-Matrix))
  (delete-all-properties! (source result)))

;;;=======================================================
;;;		  Compose-outer-left!
;;;=======================================================
#||latex

 Destructively replace $T_0$ with $T_0 \circ T_1^\dagger$, 
 without forming $T_1^\dagger$

||#

(defmethod compose-outer-left! :before ((t0 Linear-Mapping)
					(t1 Modifier))
  (assert (automorphic? t1))
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (domain t0) (codomain t1))))

;;;-------------------------------------------------------
;;; slow default methods:

(defmethod compose-outer-right! ((t0 Linear-Mapping)
				 (t1 Linear-Mapping))
  (assert (not (eq t0 t1)))
  (with-borrowed-element (result (home-space t1))
    (compose-outer-to! t0 t1 result)
    (az:copy result :result t1))
  t1)

(defmethod compose-outer-left! ((t0 Linear-Mapping)
				(t1 Linear-Mapping))
  (assert (not (eq t0 t1)))
  (with-borrowed-element (result (home-space t0))
    (compose-outer-to! t0 t1 result)
    (az:copy result :result t0))
  t0)

;;;-------------------------------------------------------

(defmethod compose-outer-left! :after ((t0 Matrix-Super) (t1 Modifier))
  (delete-all-properties! t0)
  (let ((start (bands-start t0))
	(end (bands-end t0)))
    (setf (bands-start t0) (+ start (- 1 (bands-end t1))))
    (setf (bands-end t0) (+ end (- 1 (bands-start t1)) -1)))
  (when *verify-properties?* (assert (verify-bands? t0))))

(defmethod compose-outer-left! :after ((t0 Restricted-Matrix) (t1 Modifier))
  (delete-all-properties! (source t0)))

;;;=======================================================
;;;		    Default methods
;;;=======================================================
;;; destructively modify the columns of t1

(defmethod compose-right! ((t0 Modifier) (t1 Matrix-Super))
  (with-borrowed-element (v1 (codomain t1))
    (bm:fast-loop (i :from (coord-start (domain t1))
		     :below (coord-end (domain t1)))
      (setf v1 (col t1 i :result v1))
      (transform t0 v1 :result v1)
      (setf (col t1 i) v1)))
  t1)

;;;-------------------------------------------------------
;;; destructively modify the rows of t0

(defmethod compose-left! ((t0 Matrix-Super) (t1 Modifier))
  (with-borrowed-element (v0 (domain t0))
    (bm:fast-loop (i :from (coord-start (codomain t0))
		     :below (coord-end (codomain t0)))
      (setf v0 (row t0 i :result v0))
      (transform-transpose t1 v0 :result v0)
      (setf (row t0 i) v0)))
  t0)

;;;-------------------------------------------------------
;;; destructively modify the columns of t1

(defmethod compose-inner-right! ((t0 Modifier) (t1 Matrix-Super))
  (if (symmetric? t0)
      (compose-right! t0 t1)
      ;; else
      (with-borrowed-element (v1 (codomain t1))
	(bm:fast-loop (i :from (coord-start (domain t1))
			 :below (coord-end (domain t1)))
	  (setf v1 (col t1 i :result v1))
	  (transform-transpose t0 v1 :result v1)
	  (setf (col t1 i) v1))))
  t1)

;;;-------------------------------------------------------
;;; destructively modify the rows of t0

(defmethod compose-outer-left! ((t0 Matrix-Super) (t1 Modifier))
  (if (symmetric? t1)
      (compose-left! t0 t1)
      ;; else
      (with-borrowed-element (v0 (domain t0))
	(bm:fast-loop (i :from (coord-start (codomain t0))
			 :below (coord-end (codomain t0)))
	  (setf v0 (row t0 i :result v0))
	  (transform t1 v0 :result v0)
	  (setf (row t0 i) v0))))
  t0)

;;;=======================================================
;;;	    Specialized methods for matrices
;;;=======================================================
;;; see Golub & vanLoan Alg 3.4-2

(defmethod compose-right! ((t0 Givens) (t1 Matrix-Super))
  (let ((2d (2d-array t1))
	(s (coord-start (domain t1)))
	(e (coord-end (domain t1))))
    (bm:v-rot-v! 2d 2d (c t0) (s t0)
		  :start0 s :end0 e :v-type0 :row :on0 (i0 t0) 
		  :start1 s :end1 e :v-type1 :row :on1 (i1 t0))) 
  t1)

(defmethod compose-left! ((t0 Matrix-Super) (t1 Givens))
  (let ((2d (2d-array t0))
	(s (coord-start (codomain t0)))
	(e (coord-end (codomain t0))))
    (bm:v-rot-v! 2d 2d (c t1) (- (s t1))
		  :start0 s :end0 e :v-type0 :col :on0 (i0 t1) 
		  :start1 s :end1 e :v-type1 :col :on1 (i1 t1))) 
  t0)

(defmethod compose-inner-right! ((t0 Givens) (t1 Matrix-Super))
  (let ((2d (2d-array t1))
	(s (coord-start (domain t1)))
	(e (coord-end (domain t1))))
    (bm:v-rot-v! 2d 2d (c t0) (- (s t0))
		  :start0 s :end0 e :v-type0 :row :on0 (i0 t0) 
		  :start1 s :end1 e :v-type1 :row :on1 (i1 t0)))
  t1)

(defmethod compose-outer-left! ((t0 Matrix-Super) (t1 Givens))
  (let ((2d (2d-array t0))
	(s (coord-start (codomain t0)))
	(e (coord-end (codomain t0))))
    (bm:v-rot-v! 2d 2d (c t1) (s t1)
		  :start0 s :end0 e :v-type0 :col :on0 (i0 t1) 
		  :start1 s :end1 e :v-type1 :col :on1 (i1 t1)))
  t0)

;;;-------------------------------------------------------
;;; multiplying from the right Pivots the cols:

(defmethod compose-left! ((t0 Matrix-Super) (t1 Pivot))
  (let ((2d (2d-array t0))
	(s (coord-start (codomain t0)))
	(e (coord-end (codomain t0))))
    (bm:v<->v! 2d 2d
		:start0 s :end0 e :v-type0 :col :on0 (i0 t1) 
		:start1 s :end1 e :v-type1 :col :on1 (i1 t1)))
  t0)

;;;-------------------------------------------------------
;;; multiplying from the left Pivots the rows:

(defmethod compose-right! ((t0 Pivot) (t1 Matrix-Super))
  (let ((2d (2d-array t1))
	(s (coord-start (domain t1)))
	(e (coord-end (domain t1))))
    (bm:v<->v! 2d 2d
		:start0 s :end0 e :v-type0 :row :on0 (i0 t0)
		:start1 s :end1 e :v-type1 :row :on1 (i1 t0)))
  t1)

;;;-------------------------------------------------------

(defmethod compose-right! ((t0 Gauss) (t1 Matrix-Super))
  (let* ((s (coord-start (vec t0))) 
	 (e (coord-end (vec t0)))
	 (1d (1d-array (vec t0)))
	 (2d (2d-array t1))
	 (k (k t0))
	 (dom (domain t1)))
    (loop for j from (coord-start dom) below (coord-end dom)
	  for alpha = (- (aref 2d k j)) 
	  do (bm:v+v*x! 2d 1d alpha
			 :start0 s :end0 e :v-type0 :col :on0 j
			 :start1 0 :end1 (- e s))))
  t1)

;;;-------------------------------------------------------

(defmethod compose-right! ((t0 Householder) (t1 Matrix-Super))
  (let* ((1d (1d-array (vec t0)))
	 (-2/norm2 (-2/norm2 t0))
	 (s (coord-start (vec t0)))
	 (e (coord-end (vec t0)))
	 (len (- e s))
	 (2d (2d-array t1))
	 (dom (domain t1)))
    (bm:fast-loop  (j :from (coord-start dom) :below (coord-end dom))
      (let* ((ip (bm:v.v 1d 2d
			  :end0 len
			  :start1 s :end1 e :v-type1 :col :on1 j))
	     (alpha (* -2/norm2 ip)))
	(bm:v+v*x! 2d 1d alpha
		    :start0 s :end0 e :v-type0 :col :on0 j
		    :end1 len))))
  t1)

(defmethod compose-left! ((t0 Matrix-Super) (t1 Householder))
  (let* ((1d (1d-array (vec t1)))
	 (-2/norm2 (-2/norm2 t1))
	 (v1 (vec t1))
	 (home (home-space v1))
	 (s (coord-start home))
	 (e (coord-end home))
	 (len (- e s))
	 (2d (2d-array t0))
	 (cod (codomain t0)))
    (bm:fast-loop (j :from (coord-start cod) :below (coord-end cod))
      (let* ((ip (bm:v.v 1d 2d
			  :end0 len
			  :start1 s :end1 e :v-type1 :row :on1 j))
	     (alpha (* -2/norm2 ip)))
	(bm:v+v*x! 2d 1d alpha
		    :start0 s :end0 e :v-type0 :row :on0 j
		    :end1 len))))
  t0)

;;;=======================================================
;;; some methods for Linear-Products:

(defmethod compose-right! ((t0 Linear-Mapping) (t1 Linear-Product))
  (setf (factors t1) (nconc (list t0) (factors t1)))
  t1)

(defmethod compose-left! ((t0 Linear-Product) (t1 Linear-Mapping))
  (setf (factors t0) (nconc (factors t1) (list t0)))
  t0)

(defmethod compose-right! ((t0 Linear-Product) (t1 Linear-Product))
  (setf (factors t1) (nconc (copy-list (factors t0)) (factors t1)))
  t1)

(defmethod compose-left! ((t0 Linear-Product) (t1 Linear-Product))
  (setf (factors t0) (append (factors t0) (factors t1)))
  t0)

;;;=======================================================
;;;          T
;;; compute U *A*U
;;;

(defmethod rotate-mapping ((u Linear-Mapping)
			   (a Linear-Mapping)
			   &key
			   (result (make-instance
				       'Matrix
				     :domain (domain u)
				     :codomain (codomain u))))
  (cond
   ((eq a result) (rotate-mapping! u a))
   (t (rotate-mapping-to! u a result))))

;;;-------------------------------------------------------

(defmethod rotate-mapping-to! :before ((u Linear-Mapping)
				       (a Linear-Mapping)
				       (result Matrix))
  (assert (orthogonal-rows? u))	   
  (assert (eql (codomain u) (domain a)))
  (assert (eql (codomain u) (codomain a)))
  (assert (eql (domain u) (domain result)))
  (assert (eql (domain u) (codomain result))))

;;;-------------------------------------------------------

(defmethod rotate-mapping-to! ((u Linear-Mapping)
			       (a Linear-Mapping)
			       (result Matrix))
  (setf result 
    (if (modifier? u)
	(rotate-mapping! u (copy-to-matrix a :result result)) 
      ;; else
      (compose (compose-inner u a) u :result result)))
  (if (symmetric? a) (add-property! result 'symmetric?))
  result)

;;;=======================================================
  
(defmethod rotate-mapping! :before ((u Linear-Mapping)
				    (a Linear-Mapping))
  (assert (modifier? u))
  (assert (eql (codomain u) (domain a)))
  (assert (eql (codomain u) (codomain a))))
  
;;;-------------------------------------------------------
  
(defmethod rotate-mapping! ((u Linear-Product)
			    (a Linear-Mapping))
  (map nil #'(lambda (f) (rotate-mapping! f a)) (factors u))
  a)
  
;;;-------------------------------------------------------
#||latex

In this, the most common case, we can compute $u^{-1}au$,
with explicitly forming $u^{-1}$, using the fact that
$u^{-1}= u^\dagger$.

Similarity transformers ($u$) are usually either Householder or Givens
Mappings. This method is used in the Hessenberg decompose.

See:
{\sc Golub and Van Loan,} {\bf Matrix Computations,} sec 7.4, alg. 7.4-1.
||#
  
(defmethod rotate-mapping! ((u Modifier) (a Matrix-Super))
  (let ((symmetry-flag (symmetric? a)))
    (compose (compose-inner u a :result a) u :result a)
    (when symmetry-flag (add-property! a 'symmetric?)))
  a)
  
;;;-------------------------------------------------------

(defmethod rotate-mapping! ((t0 Householder) (t1 Matrix-Super))
  (assert (subspace? (affected-space t0) (domain t1)))
  (assert (automorphic? t1))
  (if (symmetric? t1)
      (rotate-symmetric-Mapping! t0 t1)     
    (compose (compose-inner t0 t1 :result t1) t0 :result t1)))

;;;-------------------------------------------------------
#||latex
Optimize another common special case.
This method is used in tri-diagonalization of symmetric matrices.
This is a generalization of: 
{\sc Golub and Van Loan,} {\bf Matrix Computations,}
sec 8.2, alg. 8.2-1.
||#

(defmethod rotate-symmetric-mapping! ((t0 Householder)
				      (t1 Matrix-Super))
  (assert (subspace? (affected-space t0) (domain t1)))
  (assert (symmetric? t1))
  (let ((space (domain t1)) (space1 (affected-space t0))
	alpha (-2/norm2 (-2/norm2 t0)) (v (vec t0)))
    ;; (domain t1) is partitioned into 3 parts: The (affected-space t0),
    ;; the space "before" (affected-space t0) and the space "after".
    ;; The 3 spaces partition the matrix into 9 blocks; 5 are affected
    ;; by the rotation.

    ;; The 2 blocks above and to the right of the center block.
    (unless (eq (coord-start space) (coord-start space1))
      (let ((space0 (block-subspace space (coord-start space)
				    (coord-start space1))))
	(az::with-borrowed-instances
	 ((t10 'Restricted-Matrix :source t1
	       :codomain space :domain space0)
	  (t01 'Restricted-Matrix :source t1
	       :codomain space0 :domain space)) 
	 (compose t0 t10 :result t10)
	 (transpose t10 :result t01))))
    
    ;; The 2 blocks below and to the left of the center block.
    (unless (eq (coord-end space) (coord-end space1))
      (let ((space2 (block-subspace space (coord-end space1)
				    (coord-end space))))
	(az::with-borrowed-instances
	 ((t12 'Restricted-Matrix :source t1
	       :codomain space :domain space2)
	  (t21 'Restricted-Matrix :source t1
	       :codomain space2 :domain space))
	 (compose t0 t12 :result t12)
	 (transpose t12 :result t21))))
    
    ;; The center block; this part is based on G&vL p 277.
    (with-borrowed-element
     (w space1)
     (az::with-borrowed-instances
      ((t11 'Restricted-Matrix :source t1
	    :codomain space1 :domain space1)
       (tensor 'Tensor-Product
	       :v-codomain v :v-domain w
	       :codomain space1 :domain space1))
      (transform t11 v :result w)
      (setf alpha (* -2/norm2 -2/norm2 0.5d0(inner-product w v)))
      (linear-mix  -2/norm2 w alpha v :result w)
      (add t11 tensor :result t11)
      (transpose tensor :result tensor)
      (add t11 tensor :result t11))))
  (when *verify-properties?* (assert (verify-bands? t1)))
  (add-property! t1 'symmetric?)
  t1)
   
  
;;;-------------------------------------------------------
;;; Optimize another common special case.
;;; This method is used in eigen-decompose of symmetric matrices.
  
(defmethod rotate-mapping! ((t0 Pivot) (t1 Matrix-Super))
  (cond ((diagonal? t1)
	 (let ((2d (2d-array t1))
	       (i0 (i0 t0))
	       (i1 (i1 t0)))
	   (rotatef (aref 2d i0 i0)) (aref 2d i1 i1))
	 ;; we don't need to determine the new bandwidth of t1, because
	 ;; rotate-mapping-ing by a Pivot maintains diagonality
	 (when *verify-properties?* (assert (verify-bands? t1)))) 
	(t ;; else
	 (let ((symmetry-flag (symmetric? t1)))
	   (compose (compose t0 t1 :result t1) t0 :result t1)
	   (when symmetry-flag (add-property! t1 'symmetric?)))))
  t1)
  
  
  
