;;;-*- Package: Cactus; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Cactus)

;;;============================================================

(defclass Objective-Lisp-Function (C1-Objective-Function)
     ((fun
	:type Function ;;(Function (Flat-Point) Double-Float)
	:accessor fun
	:initarg :function)
      (dfun
	:type Function ;; (Function (Flat-Point) Double-Float)
	:accessor dfun
	:initarg :dfunction)
      (negative-grad
	:type Function ;; (Function (Flat-Point) (Abstract-Vector))
	:accessor negative-grad
	:initarg :negative-grad)))

;;;------------------------------------------------------------

(defmethod transform2 ((t0 Objective-Lisp-Function) pd)
  (funcall (fun t0) pd))

(defmethod minus-grad ((t0 Objective-Lisp-Function) p g)
  (funcall (negative-grad t0) p g))	   

;;;------------------------------------------------------------
;;; Estimate the accuracy of <t0> at <pd> by comparing
;;; single and double precision versions at several random points
;;; in the neighborhood. See Gill-Murray-Wright, sec 8.5.2.1.

(defmethod accuracy ((t0 Objective-Lisp-Function) pd)
  (assert (element? pd (domain t0)))
  (if (eql 1.0d0 1.0d0)
      ;; then there's only one precision
      double-float-epsilon
      ;; else
      (let ((maximum (abs (- (funcall (dfun t0) pd) (funcall (fun t0) pd))))
	    (eps (* 2.0d0 double-float-epsilon
		    (+ 1.0d0 (bm:v-sup-norm (1d-array pd)))))
	    delta)
	(with-borrowed-element (p (domain t0))
	  (dotimes (i 3)
	    (setf p (fill-randomly! p :min (- eps) :max eps))
	    (setf delta (abs (- (funcall (dfun t0) p) (funcall (fun t0) p))))
	    (when (> delta maximum) (setf maximum delta))))
	(az:fl maximum))))

;;;============================================================

(defun rosenbrock (p &optional (precision 1.0d0))
  (assert (element? p (LPoint-Space 2)))
  (let* ((1dp (1d-array p))
	 (x0 (float (aref 1dp 0) precision))
	 (x1 (float (aref 1dp 1) precision))
	 (a (float 100.0d0 precision))
	 (b precision))
    (+ (* a (bm:sq (- x1 (bm:sq x0))))
       (bm:sq (- b x0)))))

(defun rosenbrock-minus-grad (p g)
  (assert (element? p (LPoint-Space 2)))
  (assert (element? g (LVector-Space 2)))
  (let* ((1dp (1d-array p))
	 (1dg (1d-array g))
	 (x0 (aref 1dp 0))
	 (x1 (aref 1dp 1)))
    (setf (aref 1dg 0) (+ (* 400.0d0 x0 (- x1 (bm:sq x0)))
			  (* 2.0d0 (- 1.0d0 x0))))
    (setf (aref 1dg 1) (* -200.0d0 (- x1 (bm:sq x0)))))
  g)

(defparameter *rosenbrock-function*
	      (make-instance 'Objective-Lisp-Function
			      :domain (LPoint-Space 2)
			      :function 'rosenbrock
			      :dfunction #'(lambda (p) (rosenbrock p 1.0d0))
			      :negative-grad 'rosenbrock-minus-grad))


