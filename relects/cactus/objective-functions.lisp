;;;-*- Package: Cactus; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;============================================================

(in-package :Cactus)

;;;============================================================
;;; Objective Functions to minimize
;;;============================================================

(defclass Objective-Function (Real-Functional)
     (;; save the last value returned...generally useful and doesn't
      ;; cost much
      (last-value 
	:type Double-Float
	:accessor last-value
	:initform most-positive-double-float)
      (evaluation-count
	:type az:Array-Index
	:accessor evaluation-count
	:initform 0)	
      ;; stuff for animating the minimization of the Objective-Function:
      (animate?
	:type (or T Null)
	:accessor animate?
	:initarg :animate?)
      (strip-chart
	:type (or Null chart:2D-Strip-Chart)
	:accessor strip-chart
	:initarg :strip-chart)
      (max-strip-length
	:type az:Positive-Fixnum
	:accessor max-strip-length
	:initarg :max-strip-length))
  (:default-initargs
    :animate? nil
    :strip-chart nil
    :max-strip-length 128)) 

;;;------------------------------------------------------------

(defmethod clear ((ob Objective-Function))
  (format *standard-output* "~%evaluation-count= ~d." (evaluation-count ob))
  (setf (evaluation-count ob) 0)
  (unless (az:empty-slot? ob 'strip-chart)
    (setf (chart:strip-points (strip-chart ob)) nil)))

;;;------------------------------------------------------------

(defmethod transform ((ob Objective-Function)
		      (vd Abstract-Point)
		      &key
		      (result nil))
  (assert (element? vd (domain ob)))
  (unless (null result)
    (error "Real-Functional's can't accept result arguments."))
  (setf (last-value ob) (transform2 ob vd))
  (last-value ob))

(defmethod transform :after ((ob Objective-Function)
			     (st Abstract-Point)
			     &key
			     (result nil))
  (declare (ignore result))
  (incf (evaluation-count ob))
  (when (animate? ob) (animate ob)))

(defmethod animate ((ob Objective-Function))
  (when (null (strip-chart ob))
    (setf (strip-chart ob)
      (chart:make-2d-strip-chart :name (format nil "~a" ob)
                                 :x-label "Iterations"
                                 :y-label "Value")))
  (chart:strip-crawl (strip-chart ob)
		     (g:make-chart-point :x (az:fl (evaluation-count ob))
					 :y (last-value ob))))

;;;============================================================
;;; esitmate the accuracy with which <ob> can be calculated
;;; at <st>. This is really just wishful thinking. See eg.
;;; Dennis-Schnabel p 97.

(defmethod accuracy ((ob Objective-Function) st)
  (assert (element? st (domain ob)))
  double-float-epsilon)

;;;============================================================
;;; a crude heuristic choice, based on Dennis-Schnabel 
;;; page 106 and equation 5.6.2.

(defmethod forward-dif-stepsize :before ((ob Objective-Function) st h
					 &optional (eps 1.0d0))
  (assert (element? st (domain ob)))
  (assert (element? h (translation-space (domain ob))))
  (assert (plusp eps)))

(defmethod forward-dif-stepsize ((ob Objective-Function) st h
				 &optional (eps 1.0d0))
  (let ((alpha (sqrt (accuracy ob st))))
    (assert (not (zerop alpha)))
    (bm:fast-loop (i :from (coord-start (domain ob))
		     :below (coord-end (domain ob)))
      (let ((sti (coordinate st i)))
	(setf (coordinate h i)
              (* alpha (bm:sign sti) (max (abs sti) eps))))))
  h)

;;;------------------------------------------------------------
;;; a slow, but generic method

(defmethod forward-dif-minus-grad :before ((ob Objective-Function) st h s0 g)
  (assert (element? st (domain ob)))
  (assert (element? h (translation-space (domain ob))))
  (az:type-check Double-Float s0) 
  (assert (element? g (translation-space (domain ob)))))

(defmethod forward-dif-minus-grad ((ob Objective-Function) st h s0 g)
  (bm:fast-loop (i :from (coord-start (domain ob))
		   :below (coord-end (domain ob)))
    (let ((hi (coordinate h i)))
      (assert (not (zerop hi)))
      (incf (coordinate st i) hi)
      (setf (coordinate g i) (/ (- s0 (transform ob st)) hi))
      (decf (coordinate st i) hi)))
  g)

;;;============================================================
;;; a crude heuristic choice, based on Dennis-Schnabel 
;;; page 106 and equation 5.6.4.

(defmethod central-dif-stepsize :before ((ob Objective-Function) st h
					 &optional (eps 1.0d0))
  (assert (element? st (domain ob)))
  (assert (element? h (translation-space (domain ob))))
  (assert (plusp eps)))

(defmethod central-dif-stepsize ((ob Objective-Function) st h
				 &optional (eps 1.0d0))
  (let ((alpha (expt (accuracy ob st) (/ 1.0d0 3.0d0))))
    (bm:fast-loop (i :from (coord-start (domain ob))
		     :below (coord-end (domain ob)))
      (let ((sti (coordinate st i)))
	(setf (coordinate h i) (* alpha (bm:sign sti) (max (abs sti) eps))))))
  h)

;;;------------------------------------------------------------

(defmethod central-dif-minus-grad :before ((ob Objective-Function) st h s0 g)
  (assert (element? st (domain ob)))
  (assert (element? h (translation-space (domain ob))))
  (az:type-check Double-Float s0)
  (assert (element? g (translation-space (domain ob)))))

(defmethod central-dif-minus-grad ((ob Objective-Function) st h s0 g)
  (bm:fast-loop (i :from (coord-start (domain ob))
		   :below (coord-end (domain ob)))
    (let* ((hi (coordinate h i))
	   (2hi (* 2.0d0 hi)))
      (assert (not (zerop hi)))
      (incf (coordinate st i) hi)
      (setf s0 (transform ob st))
      (decf (coordinate st i) 2hi)
      (setf (coordinate g i) (/ (- (transform ob st) s0) 2hi))
      (incf (coordinate st i) hi)))
  g) 

;;;============================================================

(defclass C1-Objective-Function (Objective-Function
				  Differentiable-Mapping)
     ())

;;;------------------------------------------------------------

(defmethod minus-grad :before ((ob C1-Objective-Function) st grad)
  (az:type-check Abstract-Point st)
  (az:type-check Abstract-Vector grad))

(defmethod minus-grad ((ob C1-Objective-Function) st grad)
  (declare (ignore st grad))
  (error "Missing a method for <minus-grad>"))


