;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: (Collections :use (pcl bm lisp) :nicknames (co))-*-
;;;
;;; Copyright 1988. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;======================================================================

(in-package 'Collections 
	    :nicknames '(co)
	    :use `(pcl bm lisp))

(export '(Iterator List-Iterator Vector-Iterator))

;;;======================================================================
;;; Iterators for sequences
;;;======================================================================

(defclass Iterator ()
     ((collection
	:type T
	:accessor collection
	:initarg :collection)
      (state
	:type T
	:accessor state)))

;;;----------------------------------------------------------------------

(defclass List-Iterator (Iterator)
     ((collection
	:type List
	:accessor collection
	:initarg :collection)
      (state
	:type List
	:accessor state)))

;;;----------------------------------------------------------------------

(defmethod shared-initialize :after ((it List-Iterator)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (setf (state it) (collection it)))

;;;----------------------------------------------------------------------

(defmethod done? ((it List-Iterator)) (endp (state it)))

;;;----------------------------------------------------------------------

(defmethod next ((it List-Iterator)) (pop (state it)))

;;;======================================================================

(defclass Vector-Iterator (Iterator)
     ((collection
	:type Vector
	:accessor collection
	:initarg :collection)
      (state
	:type Fixnum
	:accessor state
	:initform 0)))

;;;----------------------------------------------------------------------

(defmethod done? ((it Vector-Iterator))
  (>= (state it) (length (collection it))))

;;;----------------------------------------------------------------------

(defmethod next ((it Vector-Iterator)) 
  (prog1
    (aref (collection it) (state it))
    (incf (state it))))

;;;======================================================================

(defmethod get-iterator ((it List)) 
  (make-instance 'List-Iterator :collection it))

;;;----------------------------------------------------------------------

(defmethod get-iterator ((it Vector)) 
  (make-instance 'Vector-Iterator :collection it))

