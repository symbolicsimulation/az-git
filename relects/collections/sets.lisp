;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: (Collections :use (pcl bm lisp) :nicknames (co))-*-
;;;
;;; Copyright 1988. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;====================================================================

(in-package 'Collections 
	    :nicknames '(co)
	    :use `(pcl bm lisp))

(export '(Set-Collection
	   Interval Closed-Interval Integer-Interval 2d-Integer-Interval))

;;;====================================================================
;;;			Representations of Sets
;;;====================================================================
;;;
;;; The Set protocol will attempt to imitate that defined for Lists in
;;; Steele, Ch. 15.5.  Enumerated sets will usually be represented,
;;; directly or indirectly, by Lists. Other sets will define their
;;; methods for <member?>, <union>, etc., (for example, see Intervals,
;;; Regions...).
;;; 
;;;====================================================================
;;;			 An abstract Set class
;;;====================================================================

(defclass Set-Collection (Collection) ())

;;;====================================================================
;;;			   Methods for Lists
;;;====================================================================

(defmethod member? (item (l List)) (member item l))

(defmethod c-adjoin (item (l List))
  (adjoin item l))

(defmethod c-union ((l1 List) (l2 List))
  (union l1 l2))

(defmethod c-nunion ((l1 List) (l2 List))
  (nunion l1 l2))

(defmethod c-intersection ((l1 List) (l2 List))
  (intersection l1 l2)) 

(defmethod c-nintersection ((l1 List) (l2 List))
  (nintersection l1 l2)) 

(defmethod c-set-difference ((l1 List) (l2 List))
  (set-difference l1 l2)) 

(defmethod c-nset-difference ((l1 List) (l2 List))
  (nset-difference l1 l2)) 

(defmethod c-set-exclusive-or ((l1 List) (l2 List))
  (set-exclusive-or l1 l2)) 

(defmethod c-nset-exclusive-or ((l1 List) (l2 List))
  (nset-exclusive-or l1 l2)) 

(defmethod subset? ((l1 List) (l2 List))
  (subsetp l1 l2)) 


;;;====================================================================
;;; General Unions of Sets
;;;====================================================================

(defclass Union-of-Sets (Set-Collection)
     ((terms :type Sequence :accessor terms)))

;;;====================================================================

(defmethod member? (item (u Union-of-Sets))
  (some #'(lambda (s) (member item s)) (terms u)))

(defmethod c-union ((u0 Union-of-Sets) (u1 Union-of-Sets))
  (make-instance 'Union-of-Sets (union (terms u0) (terms u1))))

(defmethod c-nunion ((u0 Union-of-Sets) (u1 Union-of-Sets))
  (setf (terms u0) (nunion (terms u0) (terms u1))))

;;;===============================================================
;;;			Intervals
;;;===============================================================

(defclass Interval (Set-Collection) ())

;;;===============================================================
;;; Do this 1st, because it's easy and useful.

(defclass Closed-Interval (Interval)
     ((minimum
	:type Number
	:accessor minimum
	:initarg :minimum)
      (maximum
	:type Number
	:accessor maximum
	:initarg :maximum)))

;;;--------------------------------------------------------------------

(defmethod shared-initialize :after ((i0 Closed-Interval)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (assert (slot-boundp i0 'minimum))
  (assert (slot-boundp i0 'maximum))
  (assert (<= (minimum i0) (maximum i0))))

;;;====================================================================
;;;		      Methods for the Set protocol
;;;====================================================================
;;; see Steele 15.5

(defmethod member? ((item Number) (i Closed-Interval))
  (<= (minimum i) item (maximum i)))

;;;--------------------------------------------------------------------

(defmethod c-union ((i0 Closed-Interval) (i1 Closed-Interval))
  (if (intersects? i0 i1)
      (make-instance 'Closed-Interval
		     :minimum (min (minimum i0) (minimum i1))
		     :maximum (max (maximum i0) (maximum i1)))
      ;; else
      (make-instance 'Union :terms (list i0 i1))))

;;;--------------------------------------------------------------------

(defmethod c-nunion ((i0 Closed-Interval) (i1 Closed-Interval))
  (cond
    ((= (minimum i0) (maximum i1)) (setf (minimum i0) (minimum i1)))
    ((= (minimum i1) (maximum i0)) (setf (maximum i0) (maximum i1)))
    (t (error "<nunion> not meaningful for disjoint Closed-Intervals"))))

;;;--------------------------------------------------------------------

(defmethod c-intersection ((i0 Closed-Interval) (i1 Closed-Interval))
  (let ((min (max (minimum i0) (minimum i1)))
	(max (min (maximum i0) (maximum i1))))
    (when (<= min max)
      (make-instance 'Closed-Interval :minimum min :maximum max))))

;;;--------------------------------------------------------------------

(defmethod c-nintersection ((i0 Closed-Interval)
				     (i1 Closed-Interval))
  (let ((min (max (minimum i0) (minimum i1)))
	(max (min (maximum i0) (maximum i1))))
    (when (<= min max)
      (setf (minimum i0) min)
      (setf (maximum i0) max)))) 

;;;--------------------------------------------------------------------

(defmethod subset? ((i0 Closed-Interval) (i1 Closed-Interval))
  (<= (minimum i1) (minimum i0) (maximum i0) (maximum i1)))

;;;----------------------------------------------------------------------
;;; Additional methods in the Set protocol

(defmethod intersects? ((i0 Closed-Interval) (i1 Closed-Interval))
  (not (or (< (start i1) (end i0))
	   (< (start i0) (end i1)))))

;;;----------------------------------------------------------------------

(defmethod sup ((i0 Closed-Interval) (i1 Closed-Interval))
  "The smallest enclosing Closed-Interval."
  (make-instance 'Closed-Interval
		 :minimum (min (minimum i0) (minimum i1))
		 :maximum (max (maximum i0) (maximum i1))))

;;;----------------------------------------------------------------------

(defmethod nsup ((i0 Closed-Interval) (i1 Closed-Interval))
  "Set i0 to be the smallest enclosing Closed-Interval."
  (setf (minimum i0) (min (minimum i0) (minimum i1)))
  (setf (maximum i0) (max (maximum i0) (maximum i1))))

;;;----------------------------------------------------------------------

(defmethod inf ((i0 Closed-Interval) (i1 Closed-Interval))
  "The largest contained Closed-Interval."
  (intersection i0 i1))

;;;----------------------------------------------------------------------

(defmethod ninf ((i0 Closed-Interval) (i1 Closed-Interval))
  "Replace i0 by the largest contained Closed-Interval."
  (nintersection i0 i1))


;;;===============================================================
;;; Integer-Intervals
;;;===============================================================
;;;
;;; Integer-Intervals are intervals of integers, specified as open at
;;; the upper end, ie., the interval covers those integers from <start>
;;; below <end>. This is done to be consistant with the keyword
;;; arguments in the sequence functions. Although it's a bit
;;; counter-intuitive, my experience is that it simplifies most
;;; calculations. It's especially advantageous when you're dealing with
;;; partitions. In any case, what's really important is to adopt some
;;; convention and stick with it rigorously, to avoid fencepost errors,
;;; which are a curse of much bitmap graphics software.

(defclass Integer-Interval (Interval Finite-Collection)
     ((start
	:type Integer
	:accessor start)
      (end
	:type Integer
	:accessor end)))

;;;--------------------------------------------------------------------

(defmethod shared-initialize :after ((i0 Integer-Interval)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (assert (slot-boundp i0 'start))
  (assert (slot-boundp i0 'end))
  (assert (< (start i0) (end i0))))

;;;====================================================================
;;; Methods for the Set protocol, see Steele 15.5.

(defmethod member? ((item Integer) (i0 Integer-Interval))
  (<= (start i0) item (- (end i0) 1)))

;;;--------------------------------------------------------------------

(defmethod c-union ((i0 Integer-Interval) (i1 Integer-Interval))
  (cond
    ((intersects? i0 i1)
     (make-instance 'Integer-Interval
		    :start (min (start i0) (start i1))
		    :end (max (end i0) (end i1))))
    ((= (start i0) (end i1))
     (make-instance 'Integer-Interval :start (start i1) :end (end i0)))
    ((= (start i1) (end i0))
     (make-instance 'Integer-Interval :start (start i0) :end (end i1)))
    (t (make-instance 'Union-of-Sets :terms (list i0 i1)))))

;;;--------------------------------------------------------------------

(defmethod c-nunion ((i0 Integer-Interval) (i1 Integer-Interval))
  (cond
    ((intersects? i0 i1)
     (setf (start i0) (min (start i0) (start i1)))
     (setf (end i0) (max (end i0) (end i1))))
    ((= (start i0) (end i1))
     (setf (start i0) (start i1)))
    ((= (start i1) (end i0))
     (setf (end i0) (end i1)))
    (t (make-instance 'Union-of-Sets :terms (list i0 i1)))))

;;;--------------------------------------------------------------------

(defmethod c-intersection ((i0 Integer-Interval)
				    (i1 Integer-Interval))
  (let ((start (max (start i0) (start i1)))
	(end (min (end i0) (end i1))))
    (when (< start end)
      (make-instance 'Integer-Interval :start start :end end))))

;;;--------------------------------------------------------------------

(defmethod c-nintersection ((i0 Integer-Interval)
				     (i1 Integer-Interval))
  (let ((start (max (start i0) (start i1)))
	(end (min (end i0) (end i1))))
    (when (< start end)
      (setf (start i0) start)
      (setf (end i0) end))))

;;;--------------------------------------------------------------------

(defmethod subset? ((i0 Integer-Interval) (i1 Integer-Interval))
  (<= (start i1) (start i0) (end i0) (end i1)))

;;;----------------------------------------------------------------------
;;; Additional methods in the Set protocol

(defmethod intersects? ((i0 Integer-Interval) (i1 Integer-Interval))
  (not (or (<= (start i1) (end i0))
	   (<= (start i0) (end i1)))))

;;;----------------------------------------------------------------------

(defmethod sup ((i0 Integer-Interval) (i1 Integer-Interval))
  "The smallest enclosing Integer-Interval."
  (make-instance 'Integer-Interval
		 :start (min (start i0) (start i1))
		 :end (max (end i0) (end i1))))

;;;----------------------------------------------------------------------

(defmethod nsup ((i0 Integer-Interval) (i1 Integer-Interval))
  "Replace i0 by the smallest enclosing Integer-Interval."
  (setf (start i0) (min (start i0) (start i1)))
  (setf (end i0) (max (end i0) (end i1))))

;;;----------------------------------------------------------------------

(defmethod cardinality ((i Integer-Interval))
  "Returns the number of integers contained in the Integer-Interval.
   This is needed for graphics computation (counting pixels)."
  (- (end i) (start i)))
  
;;;----------------------------------------------------------------------
;;;
;;; Returns the endpoints of the Integer-Interval which is the result of
;;; moving Integer-Interval 0 so its center matches the center of
;;; Integer-Interval 1. The center of an Integer-Interval from xmax to
;;; xmax is (truncate (+ xmax xmax) 2). This gives the obviously correct
;;; pixel for an Integer-Interval containing an odd number of pixels
;;; [remember that the length of the Integer-Interval is (+ (- xmax xmax)
;;; 1) !] and chooses the lower of the two possible choices for an
;;; Integer-Interval of even length. The result will be that when
;;; centering odd and even Integer-Intervals together an even
;;; Integer-Interval will be placed in the lower of the two possible
;;; positions and, conversely, an odd Integer-Interval placed in the
;;; upper position.
;;; 

(defmethod center-on ((i0 Integer-Interval) (i1 Integer-Interval)) 
  (let ((shift (- (truncate (+ (start i0) (end i0)) 2)
		  (truncate (+ (start i1) (end i1)) 2))))
    (values (+ (start i0) shift) (+ (end i0) shift))))
  
;;;======================================================================
;;; 		2d-Intervals
;;;======================================================================
;;;
;;; A 2d-Interval is taken to be a cartesian product of Integer-Intervals:
;;;
;;;      [x-start,x-end] X [y-start,y-end].
;;;
;;; This comes up frequently in graphics applications.
;;; 

(defclass 2d-Interval (Set-Collection)
     ((x-start
	:type Fixnum
	:accessor x-start
	:initarg :x-start)
      (x-end
	:type Fixnum
	:accessor x-end
	:initarg :x-end)
      (y-start
	:type Fiynum
	:accessor y-start
	:initarg :y-start)
      (y-end
	:type Fiynum
	:accessor y-end
	:initarg :y-end)))

;;;--------------------------------------------------------------------

(defmethod shared-initialize :after ((i0 2d-Interval)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  
  (assert (slot-boundp i0 'x-start))
  (assert (slot-boundp i0 'x-end))
  (assert (< (x-start i0) (x-end i0)))
  
  (assert (slot-boundp i0 'y-start))
  (assert (slot-boundp i0 'y-end))
  (assert (< (y-start i0) (y-end i0))))

;;;----------------------------------------------------------------------

(defmethod bounds ((r 2d-Interval))
  (values (x-start r) (x-end r) (y-start r) (y-end r)))

;;;----------------------------------------------------------------------

(defmethod set-bounds ((r 2d-Interval) x-start x-end y-start y-end) 

  (setf (x-start r) x-start)
  (setf (x-end r) x-end)
  (setf (y-start r) y-start)
  (setf (y-end r) y-end))

;;;----------------------------------------------------------------------

(defmethod xdiameter ((r 2d-Interval)) (- (x-end r) (x-start r)));; the width
(defmethod ydiameter ((r 2d-Interval)) (- (y-end r) (y-start r)));; the height
(defmethod diameter  ((r 2d-Interval)) (values (xdiameter r) (ydiameter r)))

;;;----------------------------------------------------------------------

(defmethod xradius ((r 2d-Interval)) (truncate (- (x-end r) (x-start r)) 2))
(defmethod yradius ((r 2d-Interval)) (truncate (- (y-end r) (y-start r)) 2))
(defmethod radius  ((r 2d-Interval)) (values (xradius r) (yradius r)))

;;;----------------------------------------------------------------------

(defmethod xcenter ((r 2d-Interval)) (truncate (+ (x-end r) (x-start r)) 2))
(defmethod ycenter ((r 2d-Interval)) (truncate (+ (y-end r) (y-start r)) 2))
(defmethod center  ((r 2d-Interval)) (values (xcenter r) (ycenter r)))

;;;----------------------------------------------------------------------
;;; maintain the constraints by leaving the upper-left corner alone.

(defmethod (setf xdiameter) (xd (r 2d-Interval))
  (unless (null xd) (setf (x-end r) (+ (x-start r) xd))))

(defmethod (setf ydiameter) (yd (r 2d-Interval))
  (unless (null yd) (setf (y-end r) (+ (y-start r) yd))))

(defmethod set-diameter ((r 2d-Interval) xd yd)
  (setf (xdiameter r) xd)
  (setf (ydiameter r) yd))

;;;----------------------------------------------------------------------
;;; Returns <t> if the point is in the 2d-Interval.

(defmethod member-xy? ((r 2d-Interval) x y)
  (and (<= (x-start r) x (x-end r))
       (<= (y-start r) y (y-end r))))

;;;----------------------------------------------------------------------
;;; Returns <t> if the 2d-Interval is contained in the 2d-Interval.


(defmethod subset? ((r0 2d-Interval) (r1 2d-Interval) )
  
  (and (<= (x-start r1) (x-start r0) (x-end r0) (x-end r1))
       (<= (y-start r1) (y-start r0) (y-end r0) (y-end r1))))

;;;----------------------------------------------------------------------
;;; Returns <t> if <a-2d-Interval> intersects the 2d-Interval.

(defmethod intersects? ((r0 2d-Interval) (r1 2d-Interval))
  (and (<= (max (x-start r0) (x-start r1))
	   (min (x-end r0) (x-end r1)))
       (<= (max (y-start r0) (y-start r1))
	   (min (y-end r0) (y-end r1)))))







 

