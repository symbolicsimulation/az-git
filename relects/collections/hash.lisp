;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: (Collections :use (pcl bm lisp) :nicknames (co))-*-
;;;
;;; Copyright 1988. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;======================================================================

;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: (Collections :use (pcl bm lisp) :nicknames (co)) -*-
;;;
;;; Copyright 1988. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;======================================================================

(in-package 'Collections 
	    :nicknames '(co)
	    :use `(pcl bm lisp))

(export '(Index-Hash
	   ;; index protocol:
	   look-up
	   look-up-exact
	   look-up-all
	   look-up-all-of-type
	   remove-entry!
	   add-entry!
	   ;; 2 key tables
	   Hash-Table-With-2-Keys
	   Hash-Table-With-2-Symmetric-Keys
	   ))

;;;======================================================================
;;; 
;;; An index is like a hash table, only a name corresponds to a list
;;; of values, rather than a single value.

(defclass Index-Hash (Collection)
     ((table :initform (make-hash-table)
	     :reader table
	     :type Hash-table)))

;;;------------------------------------------------------------------------

(defmethod add-entry! ((index Index-Hash) name new-item)
  
  ;; make sure there is only one item for each name
  ;; of any given exact type.
  (labels ((same-type-as-new-item? (x) (eql (type-of x) (type-of new-item))))
    (setf (gethash name (table index))
	  (cons new-item
		(delete-if #'same-type-as-new-item?
			   (gethash name (table index) ()))))))

;;;------------------------------------------------------------------------

(defmethod remove-entry! ((index Index-Hash) name item)
  (let ((entry (gethash name (table index) ())))
    (unless (null entry)
      (setf entry (delete item entry))
      (if (null entry)
	  (remhash name (table index))
	  ;; else
	  (setf (gethash name (table index)) entry)))))

;;;------------------------------------------------------------------------
;;;
;;; returns the first item found that satisfies:
;;; the name of <item> is <name>
;;; The type of <item> is a subtype of <type>.
;;;  

(defmethod look-up ((index Index-Hash) name type)
  
  (find-if #'(lambda (item) (typep item type))
	   (gethash name (table index) ())))

;;;------------------------------------------------------------------------
;;;
;;; returns the first item found that satisfies:
;;; the name of <item> is <name>
;;; the type of <item> is exactly <type>.
;;;  

(defmethod look-up-exact ((index Index-Hash) name type)
  
  (find-if #'(lambda (item) (eq (type-of item) type))
	   (gethash name (table index) ())))

;;;------------------------------------------------------------------------
;;;
;;; returns a list of all items found that satisfy:
;;; the name of <item> is <name>
;;; the type of <item> is a subtype of <type>.
;;;      

(defmethod look-up-all ((index Index-Hash) name type)
  
 (remove-if-not #'(lambda (item) (typep item type))
		(gethash name (table index) ())))

;;;------------------------------------------------------------------------
;;;
;;; returns a list of all items found that satisfy:
;;; the type of <item> is <type>
;;;      

(defmethod look-up-all-of-type ((index Index-Hash) type)
  
  (let ((items ()))
    (labels ((typecheck (item) (typep item type))
	     (collect-items (name entry)
	       (declare (ignore name))
	       (append items (remove-if-not #'typecheck entry))))
      (maphash #'collect-items (table index))
      items)))

;;;=================================================================

(defclass Hash-Table-With-2-Keys (Collection)
     ((outer-hash :initform (make-hash-table :test #'equal)
		  :accessor outer-hash
		  :type Hash-table)))
  
;;;-----------------------------------------------------------------

(defmethod gethash2 (key1 key2 (table Hash-Table-With-2-Keys)
			  &optional (default nil))
  
  (let ((inner-hash (gethash key2 (outer-hash table))))
    (if (null inner-hash)
	default
	;; else
	(gethash key1 inner-hash default))))

;;;-----------------------------------------------------------------

(defmethod (setf gethash2) (v key1 key2 (table Hash-Table-With-2-Keys))
  
  (let* ((outer (outer-hash table))
	 (inner-hash (gethash key2 outer)))
    (when (null inner-hash)
      (setf inner-hash (make-hash-table :test #'equal))
      (setf (gethash key2 outer) inner-hash))
    (setf (gethash key1 inner-hash) v)))

;;;-----------------------------------------------------------------

(defmethod clrhash2 ((table Hash-Table-With-2-Keys))
  
  (clrhash (outer-hash table))
  table)

;;;-----------------------------------------------------------------

(defmethod remhash2 (key1 key2 (table Hash-Table-With-2-Keys))
  
    (let* ((outer (outer-hash table))
	   (inner-hash (gethash key2 outer)))
      (remhash key1 inner-hash)
      (when (zerop (hash-table-count inner-hash)) (remhash key2 outer))))

;;;-----------------------------------------------------------------

(defmethod maphash2 (f (table Hash-Table-With-2-Keys))
  
  (maphash #'(lambda (key2 tab)
	       (maphash #'(lambda (key1 val) (funcall f key1 key2 val))
			tab))    
	   (outer-hash table)))

;;;-----------------------------------------------------------------

(defmethod hash-table-count2 ((table Hash-Table-With-2-Keys))
  
  (let ((sum 0))
    (flet ((count-items (key tab)
	     (declare (ignore key))
	     (incf sum (hash-table-count tab))))
    (maphash #'count-items (outer-hash table))
    sum)))


