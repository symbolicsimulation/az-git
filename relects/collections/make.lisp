;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: User -*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package 'User)

;;;============================================================

(load #+symbolics "boo:>az>tools>compile-if-needed"
      #+:coral "ccl;az:tools:compile-if-needed")

(load #+symbolics "boo:>az>files"
      #+:coral "ccl;az:files")

;;;============================================================

(compile-all-if-needed *tools-math-directory* *tools-files*)

(compile-all-if-needed *basic-math-directory* *basic-math-files*) 

#+symbolics (compile-if-needed *fix-redefine-warnings-file*)

(compile-if-needed *pcl-defsys-file*)

(let (#+:coral (*warn-if-redefine-kernel* nil))
  #+:coral (declare (special *warn-if-redefine-kernel*))
  (pcl::load-pcl))

(compile-all-if-needed *clos-tools-directory* *clos-tools-files*) 

(compile-all-if-needed *collections-directory* *collections-files*) 
