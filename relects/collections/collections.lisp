;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: (Collections :use (pcl bm lisp) :nicknames (co))-*-
;;;
;;; Copyright 1988. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;======================================================================

(in-package 'Collections 
	    :nicknames '(co)
	    :use `(pcl bm lisp))

(export '(Collection 
	   Collection-in-Sequence
	   Generalized-Sequence
	   Collection-in-List
	   Collection-in-Vector
	   Tree-Node
	   swap-tree-node))

;;;======================================================================
;;; the great grandpa
;;;======================================================================
;;; we should spell out the protocol here:

(defclass Collection ()()) 

;;;======================================================================
;;;			 Common Lisp Sequences
;;;======================================================================

;;; the essential generic function for generalized sequence;

(defmethod contents ((s Sequence)) (values s))

(defmethod contents ((s Null)) '())

;;;======================================================================
;;;		      Collection-in-Sequence Class
;;;======================================================================
;;;
;;; Most collection classes will be implemented by storing their
;;; contents in a sequence.
;;;

(defclass Collection-in-Sequence (Collection)
     ((contents
	:type Sequence
	:accessor contents
	:initarg :contents))
  (:default-initargs :contents ()))

(defmethod default-contents-type ((cs Collection-in-Sequence)) 'List)

;;;======================================================================
;;; A Generalized-Sequence Type
;;;======================================================================

(deftype Generalized-Sequence () '(or Sequence Collection-in-Sequence))

(defun generalized-sequence? (s) (typep s 'Generalized-Sequence))


;;;======================================================================
;;; Generalized Sequence functions that don't dispatch.
;;;======================================================================

(defmethod empty? (s)
  (let ((si (contents s)))
    (or (null si) (= (length si) 0)))) 

(defmethod element-classes (s)
  (let ((classes '()))
    (flet ((collect-class (x) (pushnew (class-of x) classes)))
      (map nil #'collect-class (contents s)))
    (values classes)))
  
(defmethod element-slots-union (s) 
  "Returns the union of the slots defined for each element."
  (reduce #'(lambda (x y) (union x (all-slot-names y)))
	  (element-classes (contents s) )
	  :initial-value '()))
  
(defmethod element-slots-intersection (s) 
  "Returns the Intersecvtion of the slots defined for each element."
  (check-type s Generalized-Sequence)
  (reduce #'(lambda (x y) (intersection x (all-slot-names y)))
	  (element-classes (contents s) )
	  :initial-value '()))
  
;;;----------------------------------------------------------------------
;;; Steele, Ch 14.1

(defmethod element ((s Sequence) i) (elt (contents s) i))
(defmethod (setf element) (v (s Sequence) i)
  (setf (elt (contents s) i) v))

(defmethod cardinality ((s Sequence)) (length s))

;;;----------------------------------------------------------------------
;;; Steele, Ch 14.2

(defmethod c-concatenate (result-type &rest seqs)
  (cond
    ((subtypep result-type 'sequence)
     (apply #'concatenate result-type (map 'list #'contents seqs)))
    ((subtypep result-type 'Collection-in-Sequence)
     (let ((result (make-instance result-type)))
       (setf (contents result)
	     (apply #'concatenate
		    (default-contents-type result)
		    (map 'list #'contents seqs)))
       result))
    (t (error "~a is not a legal result type for <concatenate>."))))
	

(defmethod c-map (result-type function &rest seqs)
  (cond
    ((subtypep result-type 'sequence)
     (apply #'map result-type function (map 'list #'contents seqs)))
    ((subtypep result-type 'Collection-in-Sequence)
     (let ((result (make-instance result-type)))
       (setf (contents result)
	     (apply #'map (default-contents-type result)
		    function
		    (map 'list #'contents seqs)))
       result))
    (t (error "~a is not a legal result type for <concatenate>."))))

(defmethod some? (predicate &rest seqs)
  (apply #'some predicate (map 'list #'contents seqs)))

(defmethod every? (predicate &rest seqs)
  (apply #'every predicate (map 'list #'contents seqs)))

(defmethod notany? (predicate &rest seqs)
  (apply #'notany predicate (map 'list #'contents seqs)))

(defmethod notevery? (predicate &rest seqs)
  (apply #'notevery predicate (map 'list #'contents seqs)))

(defmethod c-reduce (function s &rest options)
  (apply #'reduce function (contents s) options))

;;;----------------------------------------------------------------------
;;; Steele, Ch 14.4

(defmethod c-find (item s &rest options)
  (apply #'find item (contents s) options))

(defmethod c-find-if (test s &rest options)
  (apply #'find-if test (contents s) options))

(defmethod c-find-if-not (test s &rest options)
  (apply #'find-if-not test (contents s) options))

(defmethod c-position (item s &rest options)
  (apply #'position item s options))

(defmethod c-position-if (test s &rest options)
  (apply #'position-if test (contents s) options))

(defmethod c-position-if-not (test s &rest options)
  (apply #'position-if-not test (contents s) options))

(defmethod c-count (item s &rest options)
  (apply #'count item s options))

(defmethod c-count-if (test s &rest options)
  (apply #'count-if test (contents s) options))

(defmethod c-count-if-not (test s &rest options)
  (apply #'count-if-not test (contents s) options))

(defmethod c-mismatch (s1 s2 &rest options)
  (apply #'mismatch (contents s1) (contents s2) options))

(defmethod c-search (s1 s2 &rest options)
  (apply #'search (contents s1) (contents s2) options))

;;;----------------------------------------------------------------------
;;; Steele, Ch 14.5

(defmethod c-merge (result-type s1 s2 predicate &key key)
  (if (subtypep result-type 'sequence)
      (merge (contents s1) (contents s2) predicate :key key) 
      ;; else
      (let ((result (make-instance result-type)))
	(setf (contents result)
	      (merge (default-contents-type result)
			  (contents s1) (contents s2) predicate :key key))
	(values result)))) 
 
;;;======================================================================
;;;			  Specialized Methods
;;;======================================================================
;;;
;;; I define methods for sequence functions that are supposed to return
;;; a sequence of the same type or destructively modify their sequence
;;; argument(s).
;;;
;;;======================================================================
;;;			Methods for CL sequences
;;;======================================================================
;;; Steele, Ch 14.1

(defmethod c-subseq ((s Sequence) start &optional end)
  (subseq s start end))

(defmethod c-copy-seq ((s Sequence)) (copy-seq s))
(defmethod c-reverse  ((s Sequence)) (reverse  s))
(defmethod c-nreverse ((s Sequence)) (nreverse s))

;;;----------------------------------------------------------------------
;;; Steele, Ch 14.3

(defmethod c-fill ((s Sequence) item &rest options)
  (apply #'fill s item options))

(defmethod c-replace ((s1 Sequence) s2 &rest options)
  (apply #'replace s1 (contents s2) options))

(defmethod c-remove (item (s Sequence) &rest options)
  (apply #'remove item s options))

(defmethod c-remove-if (test (s Sequence) &rest options)
  (apply #'remove-if test s options))

(defmethod c-remove-if-not (test (s Sequence) &rest options)
  (apply #'remove-if-not test s options))

(defmethod c-delete (item (s Sequence) &rest options)
  (apply #'delete item s options))

(defmethod c-delete-if (test (s Sequence) &rest options)
  (apply #'delete-if test s options))

(defmethod c-delete-if-not (test (s Sequence) &rest options)
  (apply #'delete-if-not test s options))

(defmethod c-remove-duplicates ((s Sequence) &rest options)
  (apply #'remove-duplicates s options))

(defmethod c-delete-duplicates ((s Sequence) &rest options)
  (apply #'delete-duplicates s options))

(defmethod c-substitute (newitem olditem (s Sequence) &rest options)
  (apply #'substitute newitem olditem s options))

(defmethod c-substitute-if (newitem test (s Sequence) &rest options)
  (apply #'substitute-if newitem test s options))

(defmethod c-substitute-if-not (newitem test (s Sequence)
						 &rest options)
  (apply #'substitute-if-not newitem test s options))

(defmethod c-nsubstitute (newitem olditem (s Sequence) &rest options)
  (apply #'nsubstitute newitem olditem s options))

(defmethod c-nsubstitute-if (newitem test (s Sequence) &rest options)
  (apply #'nsubstitute-if newitem test s options))

(defmethod c-nsubstitute-if-not (newitem test (s Sequence)
						  &rest options)
  (apply #'nsubstitute-if-not newitem test s options))

;;;----------------------------------------------------------------------
;;; Steele, Ch 14.5

(defmethod c-sort ((s Sequence) predicate &key key)
  (sort s predicate :key key))

(defmethod c-stable-sort ((s Sequence) predicate &key key)
  (stable-sort s predicate :key key))

;;;======================================================================
;;;		   Methods for Collection-in-Sequence
;;;======================================================================
;;; Steele, Ch 14.1

(defmethod c-subseq ((s Collection-in-Sequence) start &optional end)
  (copy-slots s :contents (subseq (contents s) start end)))

(defmethod c-copy-seq ((s Collection-in-Sequence))
  (copy-slots s :contents (copy-seq (contents s))))

(defmethod c-reverse  ((s Collection-in-Sequence))
  (copy-slots s :contents (reverse (contents s))))

(defmethod c-nreverse ((s Collection-in-Sequence))
  (setf (contents s) (nreverse (contents s))))

;;;----------------------------------------------------------------------
;;; Steele, Ch 14.3

(defmethod c-fill ((s Collection-in-Sequence) item &rest options)
  (setf (contents s) (apply #'fill (contents s) item options)))

(defmethod c-replace ((s1 Collection-in-Sequence) s2 &rest options)
  (setf (contents s1)
	(apply #'replace (contents s1) (contents s2) options)))

(defmethod c-remove (item (s Collection-in-Sequence) &rest options)
  (copy-slots
    s :contents (apply #'remove item (contents s) options)))

(defmethod c-remove-if (test (s Collection-in-Sequence)
				      &rest options)
  (copy-slots
    s :contents (apply #'remove-if test (contents s) options)))

(defmethod c-remove-if-not (test (s Collection-in-Sequence)
					  &rest options)
  (copy-slots
    s :contents (apply #'remove-if-not test (contents s) options)))

(defmethod c-delete (item (s Collection-in-Sequence) &rest options)
  (setf (contents s) (apply #'delete item (contents s) options)))

(defmethod c-delete-if (test (s Collection-in-Sequence)
				      &rest options)
  (setf (contents s) (apply #'delete-if test (contents s) options)))

(defmethod c-delete-if-not (test (s Collection-in-Sequence)
					  &rest options)
  (setf (contents s) (apply #'delete-if-not test (contents s) options)))

(defmethod c-remove-duplicates ((s Collection-in-Sequence)
					 &rest options)
  (copy-slots
    s :contents (apply #'remove-duplicates (contents s) options)))

(defmethod c-delete-duplicates ((s Collection-in-Sequence)
					 &rest options)
  (setf (contents s) (apply #'delete-duplicates (contents s) options)))

(defmethod c-substitute (newitem olditem (s Collection-in-Sequence)
					  &rest options)
  (copy-slots
    s
    :contents (apply #'substitute newitem olditem (contents s) options)))

(defmethod c-substitute-if (newitem test (s Collection-in-Sequence)
					     &rest options)
  (copy-slots
    s
    :contents (apply #'substitute-if newitem test (contents s) options)))

(defmethod c-substitute-if-not (newitem test
						 (s Collection-in-Sequence)
						 &rest options)
  (copy-slots 
    s
    :contents
    (apply #'substitute-if-not newitem test (contents s) options)))

(defmethod c-nsubstitute (newitem olditem (s Collection-in-Sequence)
					   &rest options)
  (setf (contents s)
	(apply #'nsubstitute newitem olditem (contents s) options)))

(defmethod c-nsubstitute-if (newitem test (s Collection-in-Sequence)
					      &rest options)
  (setf (contents s)
	(apply #'nsubstitute-if newitem test (contents s) options)))

(defmethod c-nsubstitute-if-not (newitem test
						  (s Collection-in-Sequence)
						  &rest options)
  (setf (contents s)
	(apply #'nsubstitute-if-not newitem test (contents s) options)))

;;;----------------------------------------------------------------------
;;; Steele, Ch 14.5

(defmethod c-sort ((s Collection-in-Sequence) predicate &key key)
  (setf (contents s) (sort (contents s) predicate :key key)))

(defmethod c-stable-sort ((s Collection-in-Sequence) predicate
				   &key key)
  (setf (contents s) (stable-sort (contents s) predicate :key key)))


;;;======================================================================

(defclass Collection-in-List (Collection-in-Sequence)
     ((contents
	:type List
	:accessor contents
	:initarg :contents)))

;;;======================================================================

(defclass Collection-in-Vector (Collection-in-Sequence)
     ((contents
	:type Vector
	:accessor contents
	:initarg :contents)))

(defmethod shared-initialize :after ((cv Collection-in-Vector)
				     slot-names
				     &rest initargs
				     &key
				     length
				     &allow-other-keys)
  (declare (ignore slot-names initargs))
  (if (slot-boundp cv 'contents)
      (unless (or (null length) (= length (length (contents cv))))
	(error ":length and :contents are not consistent."))
      (if (null length)
	  (error "either :length or :contents must be supplied.")
	  (make-array length))))

;;;=======================================================
;;; Trees
;;;=======================================================

(defun map-over-nodes (f tree)
  (if (atom tree)
      (funcall f tree)
      ;; else
      (progn
	(funcall f tree)
	(mapc #'(lambda (child) (map-over-nodes f child)) tree))))

(defun map-over-leaves (f tree)
  (if (atom tree)
      (funcall f tree)
      ;; else
      (mapc #'(lambda (child) (map-over-leaves f child)) tree)))

;;;=======================================================
;;; The children of a General-Tree-Node are kept in the <contents>
;;; slot that's inherited from Collection-in-Sequence.

(defclass Tree-Node (Collection-in-List) ())

;;;-----------------------------------------------------------------

(defmethod swap-tree-node ((tree Tree-Node))
  (rotatef (first (contents tree)) (second (contents tree))))

;;;-----------------------------------------------------------------

(defmethod terminal-node? ((tree Tree-Node))
  (zerop (length (contents tree))))

;;;-----------------------------------------------------------------
;;; The leaves of a tree may not be Tree-Node themselves

(defmethod terminal-node? ((item Object)) t)

;;;-----------------------------------------------------------------

(defmethod leaves ((tree Tree-Node))
  
  (if (terminal-node? tree)
      (cons tree nil)
      (mapcan #'leaves (contents tree))))

;;;-----------------------------------------------------------------

(defmethod as-nested-list ((tree Tree-Node))
  (if (terminal-node? tree)
      (cons tree nil)
      (mapcar #'as-nested-list (contents tree))))

;;;-----------------------------------------------------------------

(defmethod first-child ((tree Tree-Node))
  (first (contents tree)))

(defmethod second-child ((tree Tree-Node))
  (second (contents tree)))

(defmethod (setf first-child) (new-child (tree Tree-Node))
  (setf (first (contents tree)) new-child))

(defmethod (setf second-child) (new-child (tree Tree-Node))
  (setf (second (contents tree)) new-child))

;;;------------------------------------------------------------

(defmethod first-leaf ((tree Tree-Node))
  
  (if (terminal-node? tree)
      tree
      (first-leaf (first-child tree))))

