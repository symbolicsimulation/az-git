;;;-*- Package: Basic-Math; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :Basic-Math)

;;;============================================================ 
;;; from Numerical Recipes in C, p. 297 

(defconstant -golden-ratio- (/ (+ 1.0d0 (sqrt 5.0d0)) 2.0d0))
(defconstant -golden-fraction-0- (/ (- 3.0d0 (sqrt 5.0d0)) 2.0d0))
(defconstant -golden-fraction-1- (/ (- (sqrt 5.0d0) 1.0d0) 2.0d0))
(defconstant -tiny- 1.0d-20)
(defconstant -maximum-magnification- 100.0d0)
(defconstant -brent-zeps-  1.0d-10 "???")

(declaim (type Double-Float
	       -golden-ratio- -golden-fraction-0- -golden-fraction-1-
	       -tiny- -maximum-magnification- -brent-zeps-))

;;;============================================================ 

(declaim
 (ftype (Function (Double-Float Double-Float) Double-Float) fortran-sign)
 (Inline fortran-sign))

(defun fortran-sign (x y)
  (declare (type Double-Float x y))
  (if (>= y 1.0d0) (abs x) (- (abs x))))

;;;------------------------------------------------------------

(declaim (ftype
	  (Function (Double-Float Double-Float Double-Float) Double-Float)
	  close-enough?)
	 (Inline close-enough?))

(defun close-enough? (a b tol)
  (declare (type Double-Float a b tol))
  (<= (* 2.0d0 (abs (- a b)))
      (* tol (+ (abs a) (abs b) double-float-epsilon))))

;;;------------------------------------------------------------
;;; Is <b> between <a> and <c>?

(declaim (ftype
	  (Function (Double-Float Double-Float Double-Float) (Member t null))
	  between? strictly-between?)
	 (Inline between? strictly-between?))

(defun between? (a b c)
  (declare (type Double-Float a b c))
  (not (minusp (* (- a b) (- b c)))))

(defun strictly-between? (a b c)
  (declare (type Double-Float a b c))
  (plusp (* (- a b) (- b c))))

;;;============================================================ 
;;; Routines for initially bracketting a minumum
;;;============================================================ 

(declaim
 (ftype
  (Function
   (Double-Float Double-Float Double-Float Double-Float Double-Float
		 Double-Float)
   az:Boolean)
  monotone?))

(defun monotone? (a b c fa fb fc)
  (declare (type Double-Float a b c fa fb fc))
  (let ((alpha (* (- fa fb) (- c b)))
	(beta (* (- fc fb) (- a b))))
    (close-enough? alpha beta 1.0d0)))

;;;------------------------------------------------------------

(declaim (ftype (Function (Double-Float Double-Float Double-Float Double-Float
					Double-Float Double-Float)
			  Double-Float)
		parabola-minimizing-x))

(defun parabola-minimizing-x (a b c fa fb fc)
  (declare (type Double-Float a b c fa fb fc))
  (when (monotone? a b c fa fb fc)
    (error "(= fa fb fc), function appears flat."))
  (let* ((b-a (- b a))
	 (b-c (- b c))
	 (r (* b-a (- fb fc)))
	 (q (* b-c (- fb fa)))
	 (q-r (- q r)))
    (declare (type Double-Float b-a b-c r q q-r))
    (assert (not (zerop q-r)))
    (- b (/ (- (* q b-c) (* r b-a))
	    (* 2.0d0 (fortran-sign (max (abs q-r) -tiny-) q-r))))))

;;;------------------------------------------------------------

(defun bracket-minimum (f a0 b0)
  (declare (ftype (Function (Double-Float) Double-Float) f)
	   (type Double-Float a0 b0))
  (assert (/= a0 b0))
  (let* ((c0 (* 0.5d0 (+ a0 b0)))
	 (a a0)
	 (b b0)
	 (fa (funcall f a))
	 (fb (funcall f b)))
    (declare (type Double-Float c0 a b fa fb))
    (when (> fb fa) (rotatef a b) (rotatef fa fb)) ;; ensure downhill
    (let* ((c (+ b (* -golden-ratio- (- b a))))
	   (fc (funcall f c))
	   (u 0.0d0)
	   (ulim 0.0d0)
	   (fu 0.0d0))
      (declare (type Double-Float c fc u ulim fu))
      (loop
	(when (> fc fb) (return)) ;; we've bracketed a minimum
	(when (or (large? u c0) (large? a c0) (large? b c0) (large? c c0))
	  (error "Trial points too large, function appears monotone."))
	(cond
	  ((monotone? a b c fa fb fc) ;; don't fit a parabola
	   (setf u (+ c (* -golden-ratio- (- c b))))
	   (setf fu (funcall f u)))
	  (t
	   (setf u (parabola-minimizing-x a b c fa fb fc))
	   ;; We won't go farther than <ulim>
	   (setf ulim (+ b (* -maximum-magnification- (- c b))))
	   (cond ((strictly-between? b u c)
		  ;; Parabolic u is between b and c; try it
		  (setf fu (funcall f u))
		  (cond ((< fu fc) ;; Got a minimum between b and c
			 (shiftf a b u) (shiftf fa fb fu) (return))
			((> fu fb) ;; Got a minimum between a and u
			 (setf c u) (setf fc fu) (return))
			(t ;; Parabolic u no use; use default magnification
			 (setf u (+ c (* -golden-ratio- (- c b))))
			 (setf fu (funcall f u)))))
		 ((strictly-between? c u ulim)
		  ;; Parabolic fit between c and its allowed limit
		  (setf fu (funcall f u))
		  (when (< fu fc)
		    (shiftf b c u (+ u (* -golden-ratio- (- u c))))
		    (shiftf fb fc fu (funcall f u))))
		 ((between? u ulim c)
		  ;; Limit parabolic u to maximum allowed value
		  (setf u ulim) (setf fu (funcall f u)))
		 (t ;; Reject parabolic u; use default magnification
		  (setf u (+ c (* -golden-ratio- (- c b))))
		  (setf fu (funcall f u))))))
	;; Eliminate oldest point and continue
	(shiftf a b c u) (shiftf fa fb fc fu))
      (values a b c fa fb fc)))) 

;;;============================================================ 

(defun golden-section-minimize (f a b c
				&key
				(fb (funcall f b))
				(tolerance (sqrt double-float-epsilon))
				(max-iterations 100))
  
  "A very robust, but slow, 1d minimizer. Assumes we have already
bracketed the minimum."

  (declare (ftype (Function (Double-Float) Double-Float) f)
	   (type Double-Float a b c fb tolerance)
	   (type az:Positive-Fixnum max-iterations)
	   (:returns (values (type Double-Float xmin fmin))))
  
  (let* ((x0 a)
	 (x1 most-positive-double-float)
	 (x2 most-positive-double-float)
	 (x3 c)
	 (f0 most-positive-double-float)
	 (f1 most-positive-double-float)
	 (f2 most-positive-double-float)
	 (f3 most-positive-double-float))
    (declare (type Double-Float x0 x1 x2 x3 f0 f1 f2 f3))
    ;; make x0 to x1 the smaller segment
    (cond ((> (abs (- c b)) (abs (- b a)))
	   (setf x1 b)
	   (setf x2 (+ b (* -golden-fraction-0- (- c b))))
	   (setf f1 fb)
	   (setf f2 (funcall f x2)))
	  (t
	   (setf x1 (- b (* -golden-fraction-0- (- b a))))
	   (setf x2 b)
	   (setf f1 (funcall f x1))
	   (setf f2 fb)))
    (dotimes (i max-iterations (error "Too many iterations."))
      (declare (type az:Positive-Fixnum i))
      (when (<= (abs (- x3 x0)) (* tolerance (+ 1.0d0 (abs x1) (abs x2))))
	(return))
      (cond ((< f2 f1)
	     (shiftf x0 x1 x2 (+ (* -golden-fraction-1- x2)
				 (* -golden-fraction-0- x3)))
	     (shiftf f0 f1 f2 (funcall f x2)))
	    (t
	     (shiftf x3 x2 x1 (+ (* -golden-fraction-1- x1)
				 (* -golden-fraction-0- x0)))
	     (shiftf f3 f2 f1 (funcall f x1)))))
    (if (< f1 f2) (values x1 f1) (values x2 f2))))

;;;============================================================ 
;;; The 1d minimizer recommended by Numerical Recipes.
;;; Assumes we have already bracketted the minimum.

(defun brent-minimize-1d (f ax bx cx
			  &key
			  (relative-tolerance (sqrt double-float-epsilon))
			  (absolute-tolerance -brent-zeps-)
			  (max-iterations 100))
  (declare (ftype (Function (Double-Float) Double-Float) f)
	   (type Double-Float ax bx cx relative-tolerance absolute-tolerance)
	   (type az:Positive-Fixnum max-iterations)
	   (:returns (values xmin (f xmin))))
  (let* ((e 0.0d0)
	 (d most-positive-double-float)
	 (a (min ax cx))
	 (b (max ax cx))
	 (x bx)
	 (u most-positive-double-float)
	 (v bx)
	 (w bx)
	 (fx (funcall f x))
	 (fu most-positive-double-float)
	 (fv fx)
	 (fw fx))
    (declare (type Double-Float e d a b x u v w fx fu fw))
    (dotimes (i max-iterations (error "Too many iterations."))
      (declare (type az:Positive-Fixnum i))
      (let* ((xm (* 0.5d0 (+ a b)))
	     (tol1 (+ (* relative-tolerance (abs x)) absolute-tolerance)) 
	     (tol2 (* 2.0d0 tol1)))
	(declare (type Double-Float xm tol1 tol2))
	(when (<= (abs (- x xm)) (- tol2 (* 0.5d0 (- b a)))) (return))
	(cond ((> (abs e) tol1) ;; then construct trial parabolic fit
	       (let* ((x-w (- x w))
		      (x-v (- x v))
		      (r (* x-w (- fx fv)))
		      (q (* x-v (- fx fw)))
		      (q-r (- q r))
		      (p (- (* q x-v) (* r x-w)))
		      (etemp e))
		 (declare (type Double-Float x-w x-v r q q-r p etemp))
		 (setf q (* 2.0d0 q-r))
		 (when (> q 0.0d0) (setf p (- p)))
		 (setf q (abs q))
		 (setf e d)
		 (cond ((or (>= (abs p) (* 0.5d0 q etemp))
			    (<= p (* q (- a x)))
			    (>= p (* q (- b x))))
			(setf e (if (>= x xm) (- a x) (- b x)))
			(setf d (* -golden-fraction-0- e)))
		       (t ;; take the parabolic step
			(setf d (/ p q))
			(setf u (+ x d))
			(when (or (< (- u a) tol2) (< (- b u) tol2))
			  (setf d (fortran-sign tol1 (- xm x))))))))
	      (t  ;; take a gold section step
	       (setf e (if (>= x xm) (- a x) (- b x)))
	       (setf d (* -golden-fraction-0- e))))
	(setf u (+ x (if (>= (abs d) tol1) d (fortran-sign tol1 d))))
	(setf fu (funcall f u))
	(cond ((<= fu fx)
	       (if (>= u x) (setf a x) (setf b x))
	       (shiftf v w x u)
	       (shiftf fv fw fx fu))
	      (t
	       (if (< u x) (setf a u) (setf b u))
	       (cond ((or (<= fu fw) (= w x))
		      (rotatef v w)
		      (rotatef fv fw))
		     ((or (<= fu fv) (= v x) (= v w))
		      (setf v u)
		      (setf fv fu)))))))
    (format *standard-output* "~%x=~f, f(x)=~f" x fx)
    (values x fx)))
  
;;;============================================================ 
;;; The top level 1d minimizer.

(defun minimize-1d (f a b
		    &key
		    (relative-tolerance (sqrt double-float-epsilon))
		    (absolute-tolerance -brent-zeps-))
  (declare (ftype (Function (Double-Float) Double-Float) f)
	   (type Double-Float a b relative-tolerance absolute-tolerance)
	   (:returns (values xmin (f xmin))))
  (assert (/= a b))
  (multiple-value-bind (a b c fa fb fc) (bracket-minimum f a b)
    (declare (type Double-Float a b c fa fb fc)
	     (ignore fa fb fc)
	     )
    ;;(format t "~%Bracket:")
    ;;(format t "~%(x,f) = (~f,~f) (~f,~f) (~f,~f)." a fa b fb c fc)
    (brent-minimize-1d f a b c
		       :relative-tolerance relative-tolerance
		       :absolute-tolerance absolute-tolerance)))


;;;============================================================
;;; A coarse 1-step partial minimizer for use in early phases of
;;; a high dimensional optimization:

(defun coarse-minimize-1d (f a b &rest ignored-options)
  ;; <ignored-options> provides calling consistancy with
  ;; <minimize-1d>.
  (declare (ftype (Function (Double-Float) Double-Float) f)
	   (type Double-Float a b)
	   (ignore ignored-options)) 
  (multiple-value-bind (a b c fa fb fc) (bracket-minimum f a b)
    (declare (type Double-Float a b c fa fb fc))
    (let* ((d (parabola-minimizing-x a b c fa fb fc))
	   (fd (funcall f d)))
      (declare (type Double-Float d fd))
      (if (< fd fb)
	  (values d fd)
	  (values b fb)))))
