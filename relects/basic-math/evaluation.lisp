;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;================================================================

(in-package :Basic-Math)

;;;================================================================
;;; Evaluation
;;;================================================================

(defconstant *max-iterations* 100)
 
;;;================================================================
;;; Numerical Recipes 5.3.

(defun evaluate-polynomial (coefficient-sequence x)
  ;; the highest order coef comes first!
  (let ((value 0.0d0))
    (map nil #'(lambda (c) (setf value (+ c (* x value))))
	 coefficient-sequence)
    value))

;;;----------------------------------------------------------------
;;; Continued Fractions: Numerical Recipes, Ch 5.2
#||
(defun continued-fraction (a-term b-term
			   &key
			   (max-iterations *max-iterations*))
  (loop
      for n from 0 below max-iterations
      for a-value = 0 then (funcall a-term n)
      for b-value = (funcall b-term n)
		    ;; the recursion:
      for num-2 = 0.0d0 then num-1
      for den-2 = 0.0d0 then den-1
      for num-1 = 1.0d0 then num
      for den-1 = 0.0d0 then den
      for num = b-value then (+ (* b-value num-1) (* a-value num-2))
      for den = 1.0d0 then (+ (* b-value den-1) (* a-value den-2))
      for f-1 = most-positive-double-float then f
      for f = (progn (assert (and (not (zerop num)) (not (zerop den))))
		     (/ num den))
      do
	;; test for completion:
	(when  (and (> n 0) (< (abs (/ (- f f-1) f)) -small-Double-Float-))
	  (return f))
        
        (when (and (under/overflow-danger? den) (/= den 0.0d0))
	  ;; then rescale
	  (setf num-1 (/ num-1 den))
	  (setf den-1 (/ den-1 den))
	  (setf num f)
	  (setf den 1.0d0))
        
      finally (error "No convergence by the ~:r iteration." n)))
||#

(defun continued-fraction (a-term b-term
			   &key
			   (max-iterations *max-iterations*))
  (let* ((a-value 0.0d0)
	 (b-value (funcall b-term 0))
	 (num-2 0.0d0)
	 (den-2 0.0d0)
	 (num-1 1.0d0)
	 (den-1 0.0d0)
	 (num b-value)
	 (den 1.0d0)
	 (f-1 most-positive-double-float)
	 (f (/ num den)))
    (assert (not (zerop f)))
    (dotimes (n max-iterations
	       (error "No convergence by the ~:r iteration." n))
      ;; test for completion:
      (when  (and (> n 0)
		  (< (abs (/ (- f f-1) f)) -small-double-float-))
	(return f))
      (when (and (under/overflow-danger? den) (/= den 0.0d0))
	;; then rescale
	(setf num-1 (/ num-1 den))
	(setf den-1 (/ den-1 den))
	(setf num f)
	(setf den 1.0d0))
      
      (setf a-value (funcall a-term n))
      (setf b-value (funcall b-term n))
      ;; the recursion:
      (shiftf num-2 num-1 num (+ (* b-value num-1) (* a-value num-2)))
      (shiftf den-2 den-1 den (+ (* b-value den-1) (* a-value den-2)))
      (assert (and (not (zerop num)) (not (zerop den))))
      (shiftf f-1 f (/ num den)))))



