;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================
;;;	   Useful functions on sequences (usually) of numbers
;;;=======================================================================

(in-package :Basic-Math)

;;;======================================================================

(defun iota (s)
  (etypecase s
    (Integer
     (iota (az:make-float-vector s)))
    (az:Float-Vector
     (let ((v s))
       (declare (type az:Float-Vector v))
       (dotimes (i (length v)) (setf (aref v i) (az:fl i)))
       v))
    (Vector
     (let ((v s))
       (declare (type Vector v))
       (dotimes (i (length v)) (setf (aref v i) i))
       v))
    (List
     (let ((l s)
	   (i 0))
       (declare (type az:Positive-Fixnum i)
		(type List l))
       (mapl #'(lambda (sub-l)
		 (declare (type List sub-l))
		 (setf (first sub-l) i)
		 (incf i))
	     l)))))

;;;------------------------------------------------------------------

(defun ordered-list? (l predicate)
  (declare (type List l))

  (mapl #'(lambda (sub)
	    (declare (type List sub))
	    (unless (or (null (rest sub))
			(funcall predicate (first sub) (second sub)))
	      (return-from ordered-list? nil)))
	l)
  t)

(defun ordered-vector? (v predicate)
  (declare (type Vector v))
  (dotimes (i (- (length v) 1))
    (unless (funcall predicate (aref v i) (aref v (+ i 1)))
      (return-from ordered-vector? nil)))
  t)

(defun ordered? (s predicate)
  (declare (type Sequence s)
	   (type (Function (T T) boolean) predicate)
	   (values boolean))
  (etypecase s
    (List (ordered-list? s predicate))
    (Vector (ordered-vector? s predicate))))
    
;;;------------------------------------------------------------------

(defun index-of-nearest-neighbor (x s distance)
  
  (declare (type T x)
	   (type Sequence s)
	   (type (Function (T T) Double-Float) distance)
	   (:returns (type az:Array-Index index)))
  
  (etypecase s
    (List
      (loop with dmin = (funcall distance x (first s))
	    with imin = 0
	    for i from 1
	    for xi in (rest s)
	    for d = (funcall distance x xi)
	    do (when (< d dmin) (setf imin i) (setf dmin d))
	    finally (return imin)))
    (Vector
      (loop with dmin = (funcall distance x (aref s 0))
	    with imin = 0
	    for i from 1 below (length s)
	    for d = (funcall distance x (aref s i))
	    do (when (< d dmin) (setf imin i) (setf dmin d))
	    finally (return imin)))))

;;;------------------------------------------------------------------
;;; Numerical Recipes 3.4
;;; subroutines LOCATE and HUNT
;;;
;;; Returns the index of the first element of <seq> such that
;;; (pred element item) is true or <nil> if <item> is outside the
;;; range covered by <seq>.

(defun locate (item s &key (guess -1) (test #'<))
  
  "Return index of lower neighbor of <item> in an ordered <s>."

  (declare (type T item)
	   (type Sequence s)
	   (type Fixnum guess)
	   (type (Function (T T) Boolean) test)
 	   (:returns (type (Or Null az:Array-Index) index)))
  
  (assert (ordered? s test))
  
  (let ((lower 0)
	(upper (- (length s) 1))
	(inc 1))
    (flet ((bisection-search ()
	     (loop (when (<= (- upper lower) 1) (return lower))
	       (let ((middle (truncate (+ upper lower) 2)))
		 (if (funcall test item (elt s middle))
		     (setf upper middle)
		   (setf lower middle)))))
	   (hunt-up ()
	     (setf lower guess)
	     (loop (setf guess (+ guess inc))
	       (cond ((>= guess upper) (return nil))
		     ((funcall test (elt s guess) item)
		      (setf lower guess)
		      (incf inc inc))
		     (t (setf upper guess) (return t)))))
	   (hunt-down ()
	     (setf upper guess)
	     (loop (setf guess (- guess inc))
	       (cond ((<= guess lower) (return nil))
		     ((funcall test item (elt s guess))
		      (setf upper guess)
		      (incf inc inc))
		     (t (setf lower guess) (return t))))))
      (cond
       ((or (funcall test item (elt s lower)) 
	    (funcall test (elt s upper) item))
	;; item outside range of seq
	nil)
       ((<= lower guess upper)
	(if (funcall test (elt s guess) item)
	    (hunt-up)
	  (hunt-down))
	(bisection-search))
       (t ;; guess is outside range so ignore it.
	(bisection-search))))))

;;;=======================================================

(defun permute (seq &optional (random-state *random-state*))
  (declare (type Sequence seq))
  (let ((n (length seq)))
    (declare (type az:Positive-Fixnum n))
    (dotimes (i (- n 1))
      (rotatef (elt seq i)
	       (elt seq (+ i (random (- n i) random-state))))))
  seq)