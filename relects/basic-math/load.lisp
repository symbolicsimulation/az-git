 ;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: User; Patch-File: T;-*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================

(load #+symbolics "boo:>az>tools>compile-if-needed"
      #+:coral "ccl;az:az:compile-if-needed"
      #+:excl "/belgica-0g/local/az/tools/compile-if-needed")

(load #+symbolics "boo:>az>files"
      #+:coral "ccl;az:files"
      #+:excl "/belgica-0g/local/az/files")

;;;============================================================

(load-all *tools-directory* *tools-files*) 

(load-all *basic-math-directory* *basic-math-files*) 

