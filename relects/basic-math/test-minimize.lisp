;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Basic-Math)

;;;============================================================ 
  
(defun square (x)
       (let ((x**2 (* x x)))
            (format *standard-output* "~& x, x**2= ~f, ~f" x x**2)
            x**2))

(defun my-sin (x)
       (format t "x ~f sin(x) ~F~%" x (sin x))
       (sin x))

(defun test-bracket (n &optional (min -10.0)
		     (max 10.0))
  (let ((delta (- max min)))
    (dotimes (i n)
      (let ((a (+ min (random delta)))
	    (b (+ min (random delta))))
	(multiple-value-bind (new-a new-b new-c fa fb fc)
	    (bracket-minimum #'sin a b)
	  (unless (and (or (< new-a new-b new-c)
			   (< new-c new-b new-a))
		       (< fb fc)
		       (< fb fa))
	    (format t "~%Violation *******~%")
	    (format t "a ~f b ~f ~%" a b)
	    (format t "new-a ~f new-b ~f new-c ~f fa ~f fb ~f fc ~f ~%"
		    new-a new-b new-c fa fb fc)
	    (format t "Violation *******~%~%")))))))

(defun test-brent-1d-minimize (n &optional (min -10.0) (max 10.0))
  (let ((delta (- max min)))
    (dotimes (i n)
      (let ((a (+ min (random delta)))
	    (b (+ min (random delta))))
	(multiple-value-bind
	  (new-a new-b new-c fa fb fc) (bracket-minimum #'sin a b)
	  (unless (and (or (< new-a new-b new-c) (< new-c new-b new-a))
		       (< fb fc)
		       (< fb fa))
	    (format t "~%Violation *******~%")
	    (format t "a ~f b ~f ~%" a b)
	    (format t "new-a ~f new-b ~f new-c ~f fa ~f fb ~f fc ~f ~%"
		    new-a new-b new-c fa fb fc)
	    (format t "Violation *******~%~%"))
	(multiple-value-bind
	  (xmin fmin) (brent-1d-minimize #'sin new-a new-b new-c)
	  (format t "xmin ~f fmin ~f ~%" xmin fmin)))))))

;;;------------------------------------------------------------


