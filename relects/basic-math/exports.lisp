;;;-*- Package: :Basic-Math; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :Basic-Math)

(eval-when (compile load eval)
  (export '(-double-float-radix-
	    -double-float-digits-
	    -single-float-radix-
	    -single-float-digits-
	    -smallest-positive-magnitude-
	    -largest-magnitude-
	    -smallest-relative-spacing-
	    -largest-relative-spacing-
	    -log10-double-float-radix-
	    -log-smallest-positive-magnitude-
	    -log-largest-magnitude-
	    -log-smallest-relative-spacing-
	     -pi/2- -2pi- -lnpi- -sqrt2pi- -lnsqrt2pi-
	    -large-double-float- -small-double-float-
	    typed-0 typed-1
	    typed-most-negative typed-most-positive
	    typed-least-negative typed-least-positive 
	    large? small? sign sq abs-difference
	    l2-dist2-xy l2-dist-xy
	    l2-norm2-xy l2-norm-xy
	    %row-major-index
	    under/overflow-danger? 
	    assert<
	    fast-loop
	    borrow-vector return-vector
	    with-borrowed-vector with-borrowed-vectors

	    integral-number?
	    abs-difference
	    under/overflow-danger?

	    ;; fast, unsafe versions
	    ;;%v-abs-max-index
	    ;;%v-abs-max
	    ;;%v-sup-norm %v-l1-norm %v-l2-norm2 %v-l2-norm
	    ;;%v-l1-dist %v-l2-dist2 %v-l2-dist %v-sup-dist
	    ;;%v<-x! %v+x! %v*x!
	    ;;%v.v %v<-v! %v<->v! %v<-v*x!
	    ;;%v+v! %v+v*x! %v-v! %v*v!
	    ;;%v-rot-v!
	    ;; "safe" versions
	    v-abs-max-index
	    v-abs-max
	    v-sup-norm v-l1-norm v-l2-norm2 v-l2-norm
	    v-l1-dist v-l2-dist2 v-l2-dist v-sup-dist
	    v<-x! v+x! v*x!
	    v.v v<-v! v<->v! v<-v*x!
	    v+v! v+v*x! v-v! v*v!
	    v-rot-v!
	  
	    ;; experimental
	    abs-max-2d
	    
	    format-array
	     make-fixnum-array
	    make-identity-float-array
	    make-random-vector fill-random-vector
	    make-random-2d-array fill-random-2d-array
	    make-random-symmetric-2d-array fill-random-symmetric-2d-array
	    borrow-canonical-basis-vector
	    print-array
	    equal-array-dimensions?
	    diagonal-zeros?
	    copy-array-contents
	    add-arrays subtract-arrays scale-array
	    2d-array-sup-norm
	    vector-zero!
	    vector-scale!
	    vector-add-to!
	    vector-sub-to!
	    vector-linear-mix
	    matrix-multiply multiply-matrices
	    matrix-inner-product matrix-outer-product
	    matrix*vector matrix^t*vector
	    matrix*diagonal diagonal*matrix
	    determinant-2x2 determinant-3x3
	    svd-nash
	    back-substitution transposed-back-substitution
	    forward-elimination transposed-forward-elimination
	    estimate-condition-of-upper-triangle
	    estimate-condition-of-lower-triangle
	    cholesky cholesky-ldlt update-cholesky-ldlt downdate-cholesky-ldlt

	    time-body cache-value with-collection once-only
	    print-date print-system-description
	    first-elt last-elt
	    multiple-value-setf

	    iota
	    ordered?
	    index-of-nearest-neighbor
	    locate
	    hunt
	    dot-product
	    permute
	    
	    trapezoidal-rule
	    simpsons-rule
	    make-polynomial-interpolator
	    make-rational-interpolator 
	    make-linear-interpolator 
	    evaluate-polynomial
	    continued-fraction
	    density-standard-gaussian
	    percentile-standard-gaussian
	    log-gamma
	    log-beta
	    beta
	    percentile-gamma
	    percentile-beta
	    percentile-F
	    percentile-binomial
	    percentile-chi-square
	    percentile-T

	    fortran-sign close-enough? minimize-1d coarse-minimize-1d

	    make-polynomial-interpolator
	    make-rational-interpolator

	    simplex

	    
	    Tic-Info
	    ti-min ti-max ti-inc ti-n ti-labels
	    ti-length
	    get-nice-tic-info)))


