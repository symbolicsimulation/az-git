;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :Basic-Math)

;;;=======================================================================
;;; Numerical constants
;;;=======================================================================

(declaim (type az:Positive-Fixnum -double-float-radix- -double-float-digits-))

(declaim (type Double-Float
	       -smallest-positive-magnitude-
	       -largest-magnitude-
	       -smallest-relative-spacing-
	       -largest-relative-spacing-
	       -log10-double-float-radix-
	       -log-smallest-positive-magnitude-
	       -log-largest-magnitude-
	       -log-smallest-relative-spacing-
	       -pi/2-
	       -2pi-
	       -lnpi-
	       -sqrt2pi-
	       -lnsqrt2pi-
	       -convergence-epsilon-
	       -large-double-float-
	       -small-double-float-))

;;;=======================================================================
;;;
;;; These correspond to the r1mach constants in many Fortran libraries:
;;;
;;;  double-precision machine constants
;;;
;;;  r1mach(1) = b**(emin-1), the smallest positive magnitude.
;;; is lisp:least-positive-double-float
;;;
;;;  r1mach(2) = b**emax*(1 - b**(-t)), the largest magnitude.
;;; is lisp:most-positive-double-float
;;;
;;;  r1mach(3) = b**(-t), the smallest relative spacing.
;;;
;;;  r1mach(4) = b**(1-t), the largest relative spacing.
;;;
;;;  r1mach(5) = log10(b)
;;;
;;;  to alter this function for a particular environment,
;;;  the desired set of data statements should be activated by
;;;  removing the c column 1.
;;;

(defconstant -double-float-radix- (float-radix 1.0d0))

(defconstant -double-float-digits- (float-digits 1.0d0))

(defconstant -smallest-positive-magnitude- least-positive-double-float
	     "r1mach(1)")

(defconstant -largest-magnitude-
	     (max most-positive-double-float (- most-negative-double-float))
	     "r1mach(2)")


(defconstant -smallest-relative-spacing-
	       (expt -double-float-radix- (- -double-float-digits-))
	     "r1mach(3)")

(defconstant -largest-relative-spacing-
	       (expt -double-float-radix- (- 1 -double-float-digits-))
	     "r1mach(4)")

(defconstant -log10-double-float-radix-
    (/ (log -double-float-radix-) (log 10.0d0))
	     "r1mach(5)")


(defconstant -log-smallest-positive-magnitude-
 	      (log -smallest-positive-magnitude-))

(defconstant -log-largest-magnitude- (log -largest-magnitude-))

(defconstant -log-smallest-relative-spacing-
	      (log -smallest-relative-spacing-))

;;;=======================================================================
 
(defconstant -pi/2- (/ pi 2.0d0))
(defconstant -2pi- (* 2.0d0 pi))
(defconstant -lnpi- (log pi))
(defconstant -sqrt2pi- (sqrt -2pi-))
(defconstant -lnsqrt2pi- (log -sqrt2pi-))

;;;------------------------------------------------------------------

(defconstant -large-double-float- (sqrt most-positive-double-float))
(defconstant -small-double-float- (* 100.0d0 -largest-relative-spacing-))

;;;=======================================================================
;;; handy, optimize-able, inline-able procedures

(declaim
 (ftype (Function (Double-Float) Double-Float) sign sq)
 (ftype (Function (Double-Float &optional Double-Float) az:Boolean)
	small?)
 (ftype (Function (Double-Float Double-Float) Double-Float)
	sq-diff abs-difference l2-norm2-xy l2-norm-xy)
 (ftype (Function (Double-Float Double-Float &optional Double-Float)
		  az:Boolean)
	large?)
 (ftype (Function (Double-Float Double-Float Double-Float Double-Float)
		  Double-Float)
	l2-dist2-xy l2-dist-xy)
 (Inline large? small? sign sq  abs-difference l2-norm2-xy
	 l2-norm-xy sq-diff l2-dist2-xy l2-dist-xy %row-major-xy))

  

;;; Is (abs x) extremely large compared to (abs y)?
(defun large? (x y &optional (big -large-double-float-))
  (> (abs x) (* (+ (abs y) 1.0d0) big))) 

;;; Is x negligibly small compared to y?
(defun small? (x &optional (y 1.0d0)) (= (+ x y) y))

;;; Like <signum>, but returns 1.0 if argument is zero.
(defun sign (a) (if (zerop a) 1.0d0 (signum a)))

(defun sq (a) (* a a))

;;; handy in some macros
(defun sq-diff (a b) (sq (- a b)))

;;; this often needs to be passed to other functions
(defun abs-difference (x1 x2) (abs (- x1 x2)))

(defun l2-norm2-xy (x y) (+ (sq x) (sq y)))

(defun l2-norm-xy (x y) (sqrt (l2-norm2-xy x y)))

(defun l2-dist2-xy (x0 y0 x1 y1) (+ (sq (- x0 x1)) (sq (- y0 y1))))
(defun l2-dist-xy (x0 y0 x1 y1) (sqrt (l2-dist2-xy x0 y0 x1 y1)))

;;;------------------------------------------------------------------
;;; Sometimes this needs to be fast, and we know <row-length>

(declaim (ftype (Function (az:Array-Index az:Array-Index az:Array-Index) az:Array-Index)
		%row-major-index)
	 (Inline %row-major-index))

(defun %row-major-index (i j row-length) (+ (* i row-length) j))

;;;------------------------------------------------------------------

(declaim (Inline typed-0 typed-1 typed-most-negative typed-most-positive
		 typed-least-negative typed-least-positive large? small?
		 sign sq bound abs-difference l2-norm2-xy l2-norm-xy
		 sq-diff l2-dist2-xy l2-dist-xy %row-major-index))

(defun typed-0 (type)
  ;; ignore non-atomic types---this would only matter for complex
  (when (listp type) (setf type (first type)))
  (ecase type
    ((Integer Fixnum Bignum Ratio Rational Bit) 0)
    (Short-Float 0.0s0)
    (Single-Float 0.0f0)
    (Float 0.0)
    (Double-Float 0.0d0)
    (Long-Float 0.0l0)
    (Complex #c(0.0 0.0))))

(defun typed-1 (type)
  ;; ignore non-atomic types---this would only matter for complex
  (ecase type
    ((Integer Fixnum Bignum Ratio Rational Bit) 1)
    (Short-Float 1.0s0)
    (Single-Float 1.0f0)
    (Float 1.0)
    (Double-Float 1.0d0)
    (Long-Float 1.0l0)
    (Complex #c(1.0 0.0))))

(defun typed-most-negative (type)
  ;; ignore non-atomic types---this would only matter for complex
  (when (listp type) (setf type (first type)))
  (ecase type
    ((Integer Fixnum Bignum Ratio Rational Bit) most-negative-fixnum)
    (Short-Float most-negative-short-float)
    (Single-Float most-negative-single-float)
    (Float most-negative-single-float)
    (Double-Float most-negative-double-float)
    (Long-Float most-negative-long-float)))

(defun typed-most-positive (type)
  ;; ignore non-atomic types---this would only matter for complex
  (when (listp type) (setf type (first type)))
  (ecase type
    ((Integer Fixnum Bignum Ratio Rational Bit) most-positive-fixnum)
    (Short-Float most-positive-short-float)
    (Single-float most-positive-single-float)
    (Float most-positive-single-float)
    (Double-Float most-positive-double-float)
    (Long-Float most-positive-long-float)))

(defun typed-least-negative (type)
  ;; ignore non-atomic types---this would only matter for complex
  (when (listp type) (setf type (first type)))
  (ecase type
    ((Integer Fixnum Bignum) -1)
    (Short-Float least-negative-short-float)
    (Single-float least-negative-single-float)
    (Float least-negative-single-float)
    (Double-Float least-negative-double-float)
    (Long-Float least-negative-long-float)))

(defun typed-least-positive (type)
  ;; ignore non-atomic types---this would only matter for complex
  (when (listp type) (setf type (first type)))
  (ecase type
    ((Integer Fixnum Bignum Bit) 1)
    (Short-Float least-positive-short-float)
    (Single-float least-positive-single-float)
    (Float least-positive-single-float)
    (Double-Float least-positive-double-float)
    (Long-Float least-positive-long-float)))

;;;------------------------------------------------------------------

(defun integral-float? (x)
  (declare (type Double-Float x)
	   (:returns (type az:Boolean)))
  (= x (az:fl (truncate x))))

;;;------------------------------------------------------------------
;;; See Dennis and Schnabel A1.3.1.
;;; This is very crude, can be off by a factor of 2.

(defun compute-double-float-epsilon ()
  (let ((eps 1.0d0)
	(step 0.5d0))
    (loop
      (az:mulf eps step)
      (when (= 1.0d0 (+ 1.0d0 eps)) (return (/ eps step))))))
  
;;;------------------------------------------------------------------
;;; Numerical Recipes, Ch 5.2
;;; Test if <x> is getting close to limits on float numbers

(defun under/overflow-danger? (x &optional (scale 100.0d0))
  (not
    (etypecase x
      (Short-Float
	(or (< (/ scale most-negative-short-float)
	       x
	       (* scale least-negative-short-float))
	    (< (* scale least-positive-short-float)
	       x
	       (/ scale most-positive-short-float))))
      (Single-Float
	(or (< (/ scale most-negative-single-float)
	       x
	       (* scale least-negative-single-float))
	    (< (* scale least-positive-single-float)
	       x
	       (/ scale most-positive-single-float))))
      (Double-Float
	(or (< (/ scale most-negative-double-float)
	       x
	       (* scale least-negative-double-float))
	    (< (* scale least-positive-double-float)
	       x
	       (/ scale most-positive-double-float))))
      (Long-Float
	(or (< (/ scale most-negative-long-float)
	       x
	       (* scale least-negative-long-float))
	    (< (* scale least-positive-long-float)
	       x
	       (/ scale most-positive-long-float))))))) 

;;;=======================================================

(defmacro assert< ((str epsilon) &body forms)
  
  `(progn
     ,@(mapcar
	 #'(lambda (f)
	     (let ((val (gensym)))
	       `(let ((,val ,f))
		  (unless (< (abs ,val) ,epsilon)
		    (format *error-output*
			    "~%For ~s ~%~a ~%is too large (~a > ~a)"
			    ,str ',f ,val ,epsilon)))))
	 forms)))

;;;=======================================================  
;;; Fast, unrolled loops:
;;;=======================================================  

(defun expand-fast-loop (i start end by unroll body)
  (if (= unroll 1)
      ;; the simple case
      `(progn
	 (az:type-check az:Array-Index ,start ,end)
	 (az:type-check Fixnum ,by)
	 (do ((,i (the az:Array-Index ,start) (the az:Array-Index
						(+ ,i (the Fixnum ,by)))))
	     ((>= ,i (the az:Array-Index ,end)))
	   (declare (type az:Array-Index ,i))
	   ,@body))
      ;; else
      (let* ((body+incf `(,@body (incf ,i (the Fixnum ,by))))
	     (repeated-body ())
	     (nsteps `(ceiling (- ,end ,start) ,by))
	     (remainder `(mod ,nsteps ,unroll))
	     (end-1 `(+ (* ,remainder ,by) ,start)))
	(dotimes (i unroll)
	  (setf repeated-body (append repeated-body body+incf)))
	`(progn
	   (az:type-check az:Array-Index ,start ,end)
	   (az:type-check Fixnum ,by)
	   (let ((,i (the az:Array-Index ,start)))
	     (declare (type az:Array-Index ,i))
	     (do () ((>= ,i (the az:Array-Index ,end-1)))
	       ,@body+incf)
	     (do () ((>= ,i (the az:Array-Index ,end)))
	       (declare (type az:Array-Index ,end))
	       ,@repeated-body))))))

;;;-------------------------------------------------------
;;; only steps upwards, at present
 
(defmacro fast-loop ((i
		      &key
		      ((:from start) 0)
		      ((:below end))
		      (by 1)
		      (unroll 1))
		     &body body)
  (az:type-check Symbol i)
  (az:type-check az:Array-Index unroll)
  (az:once-only
   (start end by)
   (let ((by-1 (expand-fast-loop i start end 1  unroll body))
	 (by-2 (expand-fast-loop i start end by unroll body)))
     (if (and (numberp by) (= by 1))
	 ;; <by> = 1 at macro-expansion time
	 by-1
       ;; else check for <by> = 1 at run time.  This makes a
       ;; significant difference on a symbolics with fpa, in simple
       ;; float array ops; I don't know if it's worthwhile in
       ;; general.
       `(if (= ,by 1) ,by-1 ,by-2)))))

;;;-------------------------------------------------------

(defmacro fast-loop-2 ((i0
			 &key
			 ((:from start0) 0)
			 ((:below end0)))
		       (i1
			 &key
			 ((:from start1) 0)
			 ((:below end1)))
		       &body body)
  (az:type-check Symbol i0 i1)
  (az:once-only (start0 end0 start1 end1)
    (if (equal start0 start1)
	;; then we can just use just one iteration variable
	`(progn
	   (az:type-check az:Array-Index ,start0 ,end0 ,start1 ,end1)
	   (do ((,i0 ,start0 (the az:Array-Index (+ ,i0 1))))
	       ((>= ,i0 (the az:Array-Index ,(if (equal end0 end1)
					      end0
					      `(min ,end0 ,end1)))))
	     (declare (type az:Array-Index ,i0))
	     ,@(subst i0 i1 body)))
	;; two index loop
	`(progn
	   (az:type-check az:Array-Index ,start0 ,end0 ,start1 ,end1)
	   (do ((,i0 ,start0 (the az:Array-Index (+ ,i0 1)))
		(,i1 ,start1 (the az:Array-Index (+ ,i1 1))))
	       ((>= ,i0 (the az:Array-Index (+ ,start0 (min (- ,end0 ,start0)
							 (- ,end1 ,start1))))))
	     (declare (type az:Array-Index ,i0 ,i1))
	     ,@body))))) 

;;;=======================================================

(defconstant -vector-length-step- 32)

(defun make-long-enough-vector (len)
  (make-array len 
	      :element-type 'Double-Float 	
	      :initial-element 0.0d0))

(defparameter *vector-resource* ())

;;;-------------------------------------------------------

(defun borrow-vector (len)
  (declare (special *vector-resource*))
  (flet ((long-enough? (w) (= (length w) len) ))
    (let ((v (find-if #'long-enough? *vector-resource*)))
      (cond
       ((null v) (make-long-enough-vector len))
       (t (setf *vector-resource* (delete v *vector-resource*))
	  v)))))

;;;-------------------------------------------------------

(defun return-vector (v)
  (declare (special *vector-resource*))
  (assert (typep v 'Vector))
  (push v *vector-resource*))

;;;=======================================================
;;;
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro with-borrowed-vector ((vname min-length) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-vector ,min-length))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-vector ,return-name)))))

;;; a convenience:

(defmacro with-borrowed-vectors ((vnames min-length) &body body)
  (let ((form `(progn ,@body))
	(vlen (if (atom min-length) min-length (gensym))))
    (dolist (vname vnames)
      (setf form `(with-borrowed-vector (,vname ,vlen) ,form)))
    (unless (atom min-length)
      (setf form `(let ((,vlen ,min-length)) ,form)))
    form))

;;;------------------------------------------------------------------
;;; for integer-valued floats and complex numbers with integer-valued
;;; parts

(defun integral-number? (x)
  (and (zerop (rem (realpart x) 1))
       (zerop (rem (imagpart x) 1))))

;;;===============================================================
;;; These functions come in handy for inferences about inequalities.
;;; We'll need them to implement Intervals with open and closed
;;; boundaries.
;;;
;;; The arguments have 5 possible values in order:

(defvar *inequalities* (map 'list #'symbol-function '(< <= = >= >)))

(defun inequality->score (fun) (position fun *inequalities*))
(defun score->inequality (i) (elt *inequalities* i))

(defun inequality-min (&rest funs)
  (score->inequality (apply #'min (map 'list #'inequality->score funs))))
  
(defun inequality-max (&rest funs)
  (score->inequality (apply #'max (map 'list #'inequality->score funs))))