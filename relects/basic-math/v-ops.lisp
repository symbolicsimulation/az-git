;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package:Basic-Math -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Basic-Math)

;;;=======================================================
;;;	 Extended V operations
;;;=======================================================
;;;
;;; The idea here is to define a set of operations on 1d arrays and 1d
;;; portions of 2d arrays. They are analogs of the Fortran blas used in
;;; Linpack and elsewhere. There are 2 purposes for doing this:
;;;
;;; 1) The implementation of the operations can be specialized
;;;  for a particular Lisp environment to get high performance. In the
;;;  Symbolics this means using array register declarations. It might
;;;  also mean using an array processor. In Lisps with particularly
;;;  bad Floating point implementations, it might mean calling Fortran,
;;;  C or assembly routines to avoid boxing.
;;;
;;; 2) It may simplify common iterations and reduce our dependence
;;;  on the LOOP macro (especially if we provide some S or APL-like
;;;  syntatic sugar to make it look like implied iterations).
;;;
;;; We will define a number of generalized 1d array operations.A
;;; generalized 1d array is: a (partial) 1d array, (partial) row of 2d
;;; array, or (partial) column of 2d array.
;;; 
;;; As a general policy, the burden of type, size, bounds checking, etc.
;;; is left to the caller. These routines are supposed to be as fast as
;;; possible and using them means that you are willing to sacrifice
;;; safety for speed.
;;;
;;;=======================================================

(defmacro %v-length (v-type array)
  (ecase v-type
    ((:vec :vector)    `(length ,array))
    (:row              `(array-dimension ,array 1))
    ((:col :column)    `(array-dimension ,array 0))
    ((:diag :diagonal) `(min (array-dimension ,array 0)
			     (array-dimension ,array 1)))))

(defun access-form (v-type v i on)
  (ecase v-type
    ((:vec :vector) `(aref ,v ,i))
    (:row `(aref ,v ,on ,i))
    ((:col :column) `(aref ,v ,i ,on))
    ((:diag :diagonal) `(aref ,v ,i ,i))))

(defparameter *v-types*
	      '(:vec :vector :row :col :column :diag :diagonal))

(defparameter *v-categories*
	      '((:vec :vector) (:row) (:col :column) (:diag :diagonal)))

(defmacro expand-unary-v-op (op v start end v-type on &rest rest)
  `(progn
     (assert (member ,v-type *v-categories*))
     (case ,v-type
       ,@(mapcar
	   #'(lambda (slice-type)
	       `(,slice-type
		 (,op ,v ,@rest
		  :start ,start :end ,end
		  :v-type ,(first slice-type) :on ,on)))
	   *v-categories*))) )

(defmacro expand-binary-v-op (op
			      v0 start0 end0 v-type0 on0
			      v1 start1 end1 v-type1 on1
			      &rest rest)
  
  `(progn
     (assert (member ,v-type0 *v-types*))
     (assert (member ,v-type1 *v-types*))
     (case ,v-type0
       ,@(mapcar
	  #'(lambda (slice-type0)
	      `(,slice-type0
		(case ,v-type1
		  ,@(mapcar
		     #'(lambda (slice-type1)
			 `(,slice-type1
			   (,op ,v0 ,v1 ,@rest
				:start0 ,start0 :end0 ,end0
				:v-type0 ,(first slice-type0) :on0 ,on0
				:start1 ,start1 :end1 ,end1
				:v-type1 ,(first slice-type1) :on1 ,on1)))
		     *v-categories*))))
	  *v-categories*))))

;;;-------------------------------------------------------

(defun prune-decls (var-list)
  (remove-duplicates (remove-if #'constantp var-list)))

(defun clean-declare (&rest clauses)
  (let ((result ()))
    (dolist (clause clauses)
      (if (eql (first clause) 'type)
	  (let ((type (second clause))
		(vars (prune-decls (cddr clause))))
	    (unless (null vars)
	      (push `(type ,type ,@vars) result)))
	;; else
	(push clause result)))
    `(declare (optimize (safety 1) (space 3) (speed 3))
      ,@(delete-duplicates (nreverse result) :test #'equal))))

(defun v-declaration (v-type precision)
  (ecase v-type
    ((:vec :vector)
     `(Simple-Array ,precision (*)))
     ((:row :col :column :diag :diagonal)
      `(Simple-Array ,precision (* *)))))

;;;=======================================================

(defmacro check-v-op-args (v v-type start end on precision)
  (assert (member v-type *v-types*))
  (assert (subtypep precision 'Number))
  `(progn
     ,(ecase v-type
	((:vec :vector)
	 `(az:type-check (Simple-Array ,precision (*)) ,v))
	((:row :col :column :diag :diagonal)
	 `(az:type-check (Simple-Array ,precision (* *)) ,v)))
     (az:type-check az:Array-Index ,start ,end ,on)
     (assert (<= ,start ,end (%v-length ,v-type ,v)))
     ,@(unless (member v-type '(:vec :vector))
	 `((assert
	     (<= ,on
		 ,(ecase v-type
		    (:row
		      `(array-dimension ,v 0))
		    ((:col :column)
		     `(array-dimension ,v 1))
		    ((:diag :diagonal)
		     `(min (array-dimension ,v 0)
			   (array-dimension ,v 1))))))))))

;;;=======================================================
;;;	Find the maximum element
;;;=======================================================

(defmacro %v-abs-max-index (v
			    &key
			    (v-type :vec)
			    (start 0)
			    (end `(%v-length ,v-type ,v))
			    (on 0)
			    (precision 'Double-Float))
  (az:once-only (v start end on)
    (let ((i (gensym))
	  (imax (gensym))
	  (max (gensym))
	  (val (gensym)))
      `(let ((,v ,v)
	     (,imax 0)
	     (,val ,(typed-0 precision))
	     (,max ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type precision) ,v)
			 `(type az:Array-Index ,start ,end ,on ,imax)
			 `(type ,precision ,val ,max))
	 ,on ;; eliminate errors if it's not used by <access-form>
	 (fast-loop (,i :from ,start :below ,end)
	   (setf ,val (abs ,(access-form v-type v i on)))
	   (when (> ,val ,max)
	     (setf ,imax ,i)
	     (setf ,max ,val)))
	 ,imax))))

(defmacro v-abs-max-index (v
			   &key
			   (v-type :vec)
			   (start 0)
			   (end `(%v-length ,v-type ,v))
			   (on 0)
			   (precision 'Double-Float))
  (az:once-only (v start end on)
    `(progn
       (check-v-op-args ,v ,v-type ,start ,end ,on ,precision)
       (%v-abs-max-index ,v
			 :v-type ,v-type
			 :start ,start
			 :end ,end
			 :on ,on
			 :precision ,precision))))

;;;=======================================================
;;; L-infinity Norm
;;;=======================================================

(defmacro %v-abs-max (v
		      &key
		      (v-type :vec)
		      (start 0)
		      (end `(%v-length ,v-type ,v))
		      (on 0)
		      (precision 'Double-Float)) 
  (az:once-only (v start end on)
    (let ((i (gensym))
	  (max (gensym))
	  (val (gensym)))
      `(let ((,v ,v)
	     (,val ,(typed-0 precision))
	     (,max ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type precision) ,v)
			 `(type az:Array-Index ,start ,end ,on)
			 `(type ,precision ,max ,val))
	 ,on ;; eliminate errors if it's not used by <access-form>
	 (fast-loop (,i :from ,start :below ,end)
	   (setf ,val (abs ,(access-form v-type v i on)))
	   (when (> ,val ,max)
	     (setf ,max ,val)))
	 ,max))))

(defmacro v-abs-max (v
		       &key
		       (v-type :vec)
		       (start 0)
		       (end `(%v-length ,v-type ,v))
		       (on 0)
		       (precision 'Double-Float))
  (az:once-only (v start end on)
    `(progn
       (check-v-op-args ,v ,v-type ,start ,end ,on ,precision)
       (%v-abs-max ,v
		     :v-type ,v-type
		     :start ,start
		     :end ,end
		     :on ,on
		     :precision ,precision))))

(defmacro %v-sup-norm (v
		       &key
		       (v-type :vec)
		       (start 0)
		       (end `(%v-length ,v-type ,v))
		       (on 0)
		       (precision 'Double-Float))
  `(%v-abs-max
     ,v :v-type ,v-type :start ,start :end ,end :on ,on :precision ,precision))

(defmacro v-sup-norm (v
		      &key
		      (v-type :vec)
		      (start 0)
		      (end `(%v-length ,v-type ,v))
		      (on 0)
		      (precision 'Double-Float))
  `(v-abs-max
     ,v :v-type ,v-type :start ,start :end ,end :on ,on :precision ,precision))

;;;=======================================================
;;; L2 Norm
;;;=======================================================

(defmacro %v-l2-norm (v
		      &key
		      (v-type :vec)
		      (start 0)
		      (end `(%v-length ,v-type ,v))
		      (on 0)
		      (precision 'Double-Float)) 
  `(sqrt  (%v-l2-norm2 ,v :start ,start :end ,end :v-type ,v-type :on ,on
		       :precision ,precision)))

(defmacro v-l2-norm (v
		     &key
		     (v-type :vec)
		     (start 0)
		     (end `(%v-length ,v-type ,v))
		     (on 0)
		     (precision 'Double-Float))
  `(sqrt  (v-l2-norm2 ,v :start ,start :end ,end :v-type ,v-type :on ,on
		      :precision ,precision)))

(defmacro %v-l2-norm2 (v
		       &key
		       (v-type :vec)
		       (start 0)
		       (end `(%v-length ,v-type ,v))
		       (on 0)
		       (precision 'Double-Float))
  (az:once-only (v start end on)
    (let ((i (gensym))
	  (sum (gensym))
	  (val (gensym)))
      `(let ((,v ,v)
	     (,val ,(typed-0 precision))
	     (,sum ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type precision) ,v)
			 `(type az:Array-Index ,start ,end ,on)
			 `(type ,precision ,val ,sum))
	 ,on ;; eliminate errors if it's not used by <access-form>
	 (fast-loop (,i :from ,start :below ,end :unroll 1)
	   (setf ,val ,(access-form v-type v i on))
	   (incf ,sum (* ,val ,val)))
	 ,sum))))

(defmacro v-l2-norm2 (v
		      &key
		      (v-type :vec)
		      (start 0)
		      (end `(%v-length ,v-type ,v))
		      (on 0)
		      (precision 'Double-Float))
  (az:once-only (v start end on)
    `(progn
       (check-v-op-args ,v ,v-type ,start ,end ,on ,precision)
       (%v-l2-norm2 ,v
		    :v-type ,v-type
		    :start ,start
		    :end ,end
		    :on ,on
		    :precision ,precision))))

;;;=======================================================
;;; L1 Norm
;;;=======================================================

(defmacro %v-l1-norm (v
		      &key
		      (v-type :vec)
		      (start 0)
		      (end `(%v-length ,v-type ,v))
		      (on 0)
		      (precision 'Double-Float))
  (az:once-only (v start end on)
    (let ((i (gensym))
	  (sum (gensym)))
      `(let ((,v ,v)
	     (,sum ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type precision) ,v)
			 `(type az:Array-Index ,start ,end ,on)
			 `(type ,precision ,sum))
	 ,on ;; eliminate errors if it's not used by <access-form>
	 (fast-loop (,i :from ,start :below ,end)
	   (incf ,sum (abs ,(access-form v-type v i on))))
	 ,sum))))

(defmacro v-l1-norm (v
		     &key
		     (v-type :vec)
		     (start 0)
		     (end `(%v-length ,v-type ,v))
		     (on 0)
		     (precision 'Double-Float))
  (az:once-only (v start end on)
    `(progn
       (check-v-op-args ,v ,v-type ,start ,end ,on ,precision)
       (%v-l1-norm ,v
		   :v-type ,v-type
		   :start ,start
		   :end ,end
		   :on ,on
		   :precision ,precision)))) 

;;;=======================================================
;;; Fill with a constant
;;;=======================================================

(defmacro %v<-x! (v x
		  &key
		  (v-type :vec)
		  (start 0)
		  (end `(%v-length ,v-type ,v))
		  (on 0)
		  (precision 'Double-Float))
  (az:once-only (v start end on x)
    (let ((i (gensym)))
      `(let ((,v ,v))
	 ,(clean-declare `(type ,(v-declaration v-type precision) ,v)
			 `(type az:Array-Index ,start ,end ,on)
			 `(type ,precision ,x))
	 ,on ;; eliminate errors if it's not used by <access-form>
	 (fast-loop (,i :from ,start :below ,end)
	   (setf ,(access-form v-type v i on) ,x))
	 ,v))))

(defmacro v<-x! (v x
		 &key
		 (v-type :vec)
		 (start 0)
		 (end `(%v-length ,v-type ,v))
		 (on 0)
		 (precision 'Double-Float))
  (az:once-only (v x start end on)
    `(progn
       (check-v-op-args ,v ,v-type ,start ,end ,on ,precision)
       (az:type-check Double-Float ,x)
       (%v<-x! ,v ,x
	       :v-type ,v-type
	       :start ,start
	       :end ,end
	       :on ,on
	       :precision ,precision))))

;;;=======================================================
;;; Increment elements
;;;=======================================================

(defmacro %v+x! (v x
		 &key
		 (v-type :vec)
		 (start 0)
		 (end `(%v-length ,v-type ,v))
		 (on 0)
		 (precision 'Double-Float))
  (az:once-only (v start end on x)
    (let ((i (gensym)))
      `(let ((,v ,v))
	 ,(clean-declare `(type ,(v-declaration v-type precision) ,v)
			 `(type az:Array-Index ,start ,end ,on)
			 `(type ,precision ,x))
	 ,on ;; eliminate errors if it's not used by <access-form>
	 (fast-loop (,i :from ,start :below ,end)
	   (incf ,(access-form v-type v i on) ,x))
	 ,v))))

(defmacro v+x! (v x
		&key
		(v-type :vec)
		(start 0)
		(end `(%v-length ,v-type ,v))
		(on 0)
		(precision 'Double-Float))
  (az:once-only (v x start end on)
    `(progn
       (check-v-op-args ,v ,v-type ,start ,end ,on ,precision)
       (az:type-check Double-Float ,x)
       (%v+x! ,v ,x
	      :v-type ,v-type
	      :start ,start
	      :end ,end
	      :on ,on
	      :precision ,precision)))) 

;;;=======================================================
;;; Scale elements
;;;=======================================================

(defmacro %v*x! (v x
		 &key
		 (v-type :vec)
		 (start 0)
		 (end `(%v-length ,v-type ,v))
		 (on 0)
		 (precision 'Double-Float))
  (az:once-only (v start end on x)
    (let ((i (gensym)))
      `(let ()
	 ,(clean-declare `(type ,(v-declaration v-type precision) ,v)
			 `(type az:Array-Index ,start ,end ,on)
			 `(type ,precision ,x))
	 ,on ;; eliminate errors if it's not used by <access-form>
	 (fast-loop (,i :from ,start :below ,end)
	   (az:mulf ,(access-form v-type v i on) ,x))
	 ,v))))


(defmacro v*x! (v x
		&key
		(v-type :vec)
		(start 0)
		(end `(%v-length ,v-type ,v))
		(on 0)
		(precision 'Double-Float))
  (az:once-only (v x start end on)
    `(progn
       (check-v-op-args ,v ,v-type ,start ,end ,on ,precision)
       (az:type-check Double-Float ,x)
       (%v*x! ,v ,x
	      :v-type ,v-type
	      :start ,start
	      :end ,end
	      :on ,on
	      :precision ,precision))))

;;;=======================================================
;;; Dot Product
;;;=======================================================

(defmacro %v.v (v0 v1
		&key
		(v-type0 :vec)
		(start0 0)
		(end0 `(%v-length ,v-type0 ,v0))
		(on0 0)
		(v-type1 :vec)
		(start1 0)
		(end1 `(%v-length ,v-type1 ,v1))
		(on1 0)
		(precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym))
	  (sum (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))) 
	     (,sum ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			 `(type (,precision ,(typed-0 precision) *) ,sum))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (incf ,sum (* ,(access-form v-type0 v0 i0 on0)
			 ,(access-form v-type1 v1 i1 on1))))
	 ,sum))))

(defmacro v.v (v0 v1
	       &key
	       (v-type0 :vec)
	       (start0 0)
	       (end0 `(%v-length ,v-type0 ,v0))
	       (on0 0)
	       (v-type1 :vec)
	       (start1 0)
	       (end1 `(%v-length ,v-type1 ,v1))
	       (on1 0)
	       (precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v.v ,v0 ,v1
	     :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
	     :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
	     :precision ,precision))))

;;;=======================================================
;;; L1 Distance
;;;=======================================================

(defmacro %v-l1-dist (v0 v1
		      &key
		      (v-type0 :vec)
		      (start0 0)
		      (end0 `(%v-length ,v-type0 ,v0))
		      (on0 0)
		      (v-type1 :vec)
		      (start1 0)
		      (end1 `(%v-length ,v-type1 ,v1))
		      (on1 0)
		      (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym))
	  (sum (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))) 
	     (,sum ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			 `(type (,precision ,(typed-0 precision) *) ,sum))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (incf ,sum (abs (the ,precision
				(- ,(access-form v-type0 v0 i0 on0)
				   ,(access-form v-type1 v1 i1 on1))))))
	 ,sum))))

(defmacro v-l1-dist (v0 v1
		     &key
		     (v-type0 :vec)
		     (start0 0)
		     (end0 `(%v-length ,v-type0 ,v0))
		     (on0 0)
		     (v-type1 :vec)
		     (start1 0)
		     (end1 `(%v-length ,v-type1 ,v1))
		     (on1 0)
		     (precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v-l1-dist ,v0 ,v1
		   :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
		   :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
		   :precision ,precision))))

;;;=======================================================
;;; L2 Distance
;;;=======================================================

(defmacro %v-l2-dist2 (v0 v1
		       &key
		       (v-type0 :vec)
		       (start0 0)
		       (end0 `(%v-length ,v-type0 ,v0))
		       (on0 0)
		       (v-type1 :vec)
		       (start1 0)
		       (end1 `(%v-length ,v-type1 ,v1))
		       (on1 0)
		       (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym))
	  (val (gensym))
	  (sum (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1)))
	     (,val ,(typed-0 precision))
	     (,sum ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			 `(type (,precision ,(typed-0 precision) *) ,sum))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
		      (setf ,val (- ,(access-form v-type0 v0 i0 on0)
				    ,(access-form v-type1 v1 i1 on1)))
		      (incf ,sum (* ,val ,val)))
	 ,sum))))

(defmacro v-l2-dist2 (v0 v1
		      &key
		      (v-type0 :vec)
		      (start0 0)
		      (end0 `(%v-length ,v-type0 ,v0))
		      (on0 0)
		      (v-type1 :vec)
		      (start1 0)
		      (end1 `(%v-length ,v-type1 ,v1))
		      (on1 0)
		      (precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v-l2-dist2 ,v0 ,v1
		    :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
		    :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
		    :precision ,precision)))) 

(defmacro %v-l2-dist (v0 v1
		      &key
		      (v-type0 :vec)
		      (start0 0)
		      (end0 `(%v-length ,v-type0 ,v0))
		      (on0 0)
		      (v-type1 :vec)
		      (start1 0)
		      (end1 `(%v-length ,v-type1 ,v1))
		      (on1 0)
		      (precision 'Double-Float))
  `(sqrt
    (%v-l2-dist2 ,v0 ,v1
		 :start0 ,start0 :end0 ,end0 :v-type0 ,v-type0 :on0 ,on0
		 :start1 ,start1 :end1 ,end1 :v-type1 ,v-type1 :on1 ,on1
		 :precision ,precision)))


(defmacro v-l2-dist (v0 v1
		     &key
		     (v-type0 :vec)
		     (start0 0)
		     (end0 `(%v-length ,v-type0 ,v0))
		     (on0 0)
		     (v-type1 :vec)
		     (start1 0)
		     (end1 `(%v-length ,v-type1 ,v1))
		     (on1 0)
		     (precision 'Double-Float))
  `(sqrt
    (v-l2-dist2 ,v0 ,v1
		:start0 ,start0 :end0 ,end0 :v-type0 ,v-type0 :on0 ,on0
		:start1 ,start1 :end1 ,end1 :v-type1 ,v-type1 :on1 ,on1
		:precision ,precision)))

;;;=======================================================
;;; Sup Distance
;;;=======================================================

(defmacro %v-sup-dist (v0 v1
		       &key
		       (v-type0 :vec)
		       (start0 0)
		       (end0 `(%v-length ,v-type0 ,v0))
		       (on0 0)
		       (v-type1 :vec)
		       (start1 0)
		       (end1 `(%v-length ,v-type1 ,v1))
		       (on1 0)
		       (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym))
	  (val (gensym))
	  (max (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1)))
	     (,val ,(typed-0 precision))
	     (,max ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			 `(type ,precision ,val)
			 `(type (,precision ,(typed-0 precision) *) ,max))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
		      (setf ,val (abs (the ,precision
					(- ,(access-form v-type0 v0 i0 on0)
					   ,(access-form v-type1 v1 i1 on1)))))
		      (when (>= ,val ,max) (setf ,max ,val)))
	 ,max))))

(defmacro v-sup-dist (v0 v1
		      &key
		      (v-type0 :vec)
		      (start0 0)
		      (end0 `(%v-length ,v-type0 ,v0))
		      (on0 0)
		      (v-type1 :vec)
		      (start1 0)
		      (end1 `(%v-length ,v-type1 ,v1))
		      (on1 0)
		      (precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v-sup-dist ,v0 ,v1
		    :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
		    :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
		    :precision ,precision)))) 

;;;=======================================================
;;; Copy elements
;;;=======================================================

(defmacro %v<-v! (v0 v1
		  &key
		  (v-type0 :vec)
		  (start0 0)
		  (end0 `(%v-length ,v-type0 ,v0))
		  (on0 0)
		  (v-type1 :vec)
		  (start1 0)
		  (end1 `(%v-length ,v-type1 ,v1))
		  (on1 0)
		  (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
		      (setf ,(access-form v-type0 v0 i0 on0)
			,(access-form v-type1 v1 i1 on1)))
	 ,v0))))

(defmacro v<-v! (v0 v1
		 &key
		 (v-type0 :vec)
		 (start0 0)
		 (end0 `(%v-length ,v-type0 ,v0))
		 (on0 0)
		 (v-type1 :vec)
		 (start1 0)
		 (end1 `(%v-length ,v-type1 ,v1))
		 (on1 0)
		 (precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v<-v! ,v0 ,v1
	       :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
	       :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
	       :precision ,precision)))) 

;;;=======================================================
;;; Swap elements
;;;=======================================================

(defmacro %v<->v! (v0 v1
		   &key
		   (v-type0 :vec)
		   (start0 0)
		   (end0 `(%v-length ,v-type0 ,v0))
		   (on0 0)
		   (v-type1 :vec)
		   (start1 0)
		   (end1 `(%v-length ,v-type1 ,v1))
		   (on1 0)
		   (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (rotatef ,(access-form v-type0 v0 i0 on0)
		    ,(access-form v-type1 v1 i1 on1)))
	 ,v0))))

(defmacro v<->v! (v0 v1
		  &key
		  (v-type0 :vec)
		  (start0 0)
		  (end0 `(%v-length ,v-type0 ,v0))
		  (on0 0)
		  (v-type1 :vec)
		  (start1 0)
		  (end1 `(%v-length ,v-type1 ,v1))
		  (on1 0)
		  (precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v<->v! ,v0 ,v1
		:v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
		:v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
		:precision ,precision)))) 

;;;=======================================================
;;; Elementwise addition
;;;=======================================================

(defmacro %v+v! (v0 v1
		  &key
		  (v-type0 :vec)
		  (start0 0)
		  (end0 `(%v-length ,v-type0 ,v0))
		  (on0 0)
		  (v-type1 :vec)
		  (start1 0)
		  (end1 `(%v-length ,v-type1 ,v1))
		  (on1 0)
		  (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (incf ,(access-form v-type0 v0 i0 on0)
		 ,(access-form v-type1 v1 i1 on1)))
	 ,v0)))) 

(defmacro v+v! (v0 v1
		&key
		(v-type0 :vec)
		(start0 0)
		(end0 `(%v-length ,v-type0 ,v0))
		(on0 0)
		(v-type1 :vec)
		(start1 0)
		(end1 `(%v-length ,v-type1 ,v1))
		(on1 0)
		(precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v+v! ,v0 ,v1
	      :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
	      :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
	      :precision ,precision)))) 

;;;=======================================================
;;; Elementwise subtraction
;;;=======================================================

(defmacro %v-v! (v0 v1
		  &key
		  (v-type0 :vec)
		  (start0 0)
		  (end0 `(%v-length ,v-type0 ,v0))
		  (on0 0)
		  (v-type1 :vec)
		  (start1 0)
		  (end1 `(%v-length ,v-type1 ,v1))
		  (on1 0)
		  (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (decf ,(access-form v-type0 v0 i0 on0)
		 ,(access-form v-type1 v1 i1 on1)))
	 ,v0)))) 

(defmacro v-v! (v0 v1
		&key
		(v-type0 :vec)
		(start0 0)
		(end0 `(%v-length ,v-type0 ,v0))
		(on0 0)
		(v-type1 :vec)
		(start1 0)
		(end1 `(%v-length ,v-type1 ,v1))
		(on1 0)
		(precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v-v! ,v0 ,v1
	      :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
	      :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
	      :precision ,precision)))) 

;;;=======================================================
;;; Elementwise multiplication
;;;=======================================================

(defmacro %v*v! (v0 v1
		 &key
		 (v-type0 :vec)
		 (start0 0)
		 (end0 `(%v-length ,v-type0 ,v0))
		 (on0 0)
		 (v-type1 :vec)
		 (start1 0)
		 (end1 `(%v-length ,v-type1 ,v1))
		 (on1 0)
		 (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1)
    (let ((i0 (gensym))
	  (i1 (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (az:mulf ,(access-form v-type0 v0 i0 on0)
		 ,(access-form v-type1 v1 i1 on1)))
	 ,v0))))

(defmacro v*v! (v0 v1
		&key
		(v-type0 :vec)
		(start0 0)
		(end0 `(%v-length ,v-type0 ,v0))
		(on0 0)
		(v-type1 :vec)
		(start1 0)
		(end1 `(%v-length ,v-type1 ,v1))
		(on1 0)
		(precision 'Double-Float)) 
  (az:once-only (v0 v1 start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (%v*v! ,v0 ,v1
	      :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
	      :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
	      :precision ,precision))))

;;;=======================================================
;;; Variations on the blas Saxpy
;;;=======================================================
;;;
;;; These functions overwrite the specified elements of their 1st
;;; argument with a scalar times the correspomnding element of the
;;; second arg.

(defmacro %v<-v*x! (v0 v1 x
		    &key
		    (v-type0 :vec)
		    (start0 0)
		    (end0 `(%v-length ,v-type0 ,v0))
		    (on0 0)
		    (v-type1 :vec)
		    (start1 0)
		    (end1 `(%v-length ,v-type1 ,v1))
		    (on1 0)
		    (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1 x)
    (let ((i0 (gensym))
	  (i1 (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			 `(type ,precision ,x))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (setf ,(access-form v-type0 v0 i0 on0)
		 (* ,(access-form v-type1 v1 i1 on1) ,x)))
	 ,v0))))

(defmacro v<-v*x! (v0 v1 x
		   &key
		   (v-type0 :vec)
		   (start0 0)
		   (end0 `(%v-length ,v-type0 ,v0))
		   (on0 0)
		   (v-type1 :vec)
		   (start1 0)
		   (end1 `(%v-length ,v-type1 ,v1))
		   (on1 0)
		   (precision 'Double-Float)) 
  (az:once-only (v0 v1 x start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (az:type-check Double-Float ,x)
       (%v<-v*x! ,v0 ,v1 ,x
		 :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
		 :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
		 :precision ,precision))))

;;;=======================================================
;;; blas Saxpy
;;;=======================================================
;;;
;;; These functions overwrite the specified elements of their 1st
;;; argument with the sum of each element and x scalar times the
;;; correspomnding element of the second arg.

(defmacro %v+v*x! (v0 v1 x
		   &key
		   (v-type0 :vec)
		   (start0 0)
		   (end0 `(%v-length ,v-type0 ,v0))
		   (on0 0)
		   (v-type1 :vec)
		   (start1 0)
		   (end1 `(%v-length ,v-type1 ,v1))
		   (on1 0)
		   (precision 'Double-Float))
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1 x)
    (let ((i0 (gensym))
	  (i1 (gensym)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0)
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			 `(type ,precision ,x))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (incf ,(access-form v-type0 v0 i0 on0)
		 (* ,(access-form v-type1 v1 i1 on1) ,x)))
	 ,v0))))

(defmacro v+v*x! (v0 v1 x
		  &key
		  (v-type0 :vec)
		  (start0 0)
		  (end0 `(%v-length ,v-type0 ,v0))
		  (on0 0)
		  (v-type1 :vec)
		  (start1 0)
		  (end1 `(%v-length ,v-type1 ,v1))
		  (on1 0)
		  (precision 'Double-Float)) 
  (az:once-only (v0 v1 x start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (az:type-check Double-Float ,x)
       (%v+v*x! ,v0 ,v1 ,x
		:v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
		:v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
		:precision ,precision))))

;;;=======================================================
;;;	      blas Srot
;;;=======================================================

(defmacro %v-rot-v! (v0 v1 c s
		     &key
		     (v-type0 :vec)
		     (start0 0)
		     (end0 `(%v-length ,v-type0 ,v0))
		     (on0 0)
		     (v-type1 :vec)
		     (start1 0)
		     (end1 `(%v-length ,v-type1 ,v1))
		     (on1 0)
		     (precision 'Double-Float)) 
  (az:once-only (v0 start0 end0 on0 v1 start1 end1 on1 c s)
    (let* ((i0 (gensym))
	   (i1 (gensym))
	   (val0 (gensym))
	   (val1 (gensym))
	   (access0 (access-form v-type0 v0 i0 on0))
	   (access1 (access-form v-type1 v1 i1 on1)))
      `(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1)))

	     (,val0 ,(typed-0 precision))
	     (,val1 ,(typed-0 precision)))
	 ,(clean-declare `(type ,(v-declaration v-type0 precision) ,v0) 
			 `(type ,(v-declaration v-type1 precision) ,v1)
			 `(type az:Array-Index
				,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			 `(type ,precision ,c ,s ,val0 ,val1))
	 ,on0 ,on1 ;; eliminate errors if not used by <access-form>
	 (fast-loop-2 (,i0 :from ,start0 :below ,end0)
		      (,i1 :from ,start1 :below ,end1)
	   (setf ,val0 ,access0)
	   (setf ,val1 ,access1)
	   (setf ,access0 (+ (* ,s ,val1) (* ,c ,val0)))
	   (setf ,access1 (- (* ,c ,val1) (* ,s ,val0))))
	 ,v0))))

(defmacro v-rot-v! (v0 v1 c s
		    &key
		    (v-type0 :vec)
		    (start0 0)
		    (end0 `(%v-length ,v-type0 ,v0))
		    (on0 0)
		    (v-type1 :vec)
		    (start1 0)
		    (end1 `(%v-length ,v-type1 ,v1))
		    (on1 0)
		    (precision 'Double-Float)) 
  (az:once-only (v0 v1 c s start0 end0 on0 start1 end1 on1)
    `(progn
       (check-v-op-args ,v0 ,v-type0 ,start0 ,end0 ,on0 ,precision)
       (check-v-op-args ,v1 ,v-type1 ,start1 ,end1 ,on1 ,precision)
       (az:type-check Double-Float ,c ,s)
       (%v-rot-v! ,v0 ,v1 ,c ,s
		  :v-type0 ,v-type0 :start0 ,start0 :end0 ,end0 :on0 ,on0
		  :v-type1 ,v-type1 :start1 ,start1 :end1 ,end1 :on1 ,on1
		  :precision ,precision)))) 

;;;=======================================================
;;;	       experimental 2d array ops
;;;=======================================================

(defun abs-max-2d (v)
  (declare (type az:Float-Matrix v)) 
  (let ((v v)
	(val 0.0d0)
	(max 0.0d0))
    (declare (optimize (safety 1) (speed 3) (space 3))
	     (type az:Float-Matrix v)
	     (type (Double-Float 0.0d0) val max))
    (dotimes (i (array-dimension v 0))
      (declare (type az:Array-Index i))
      (dotimes (j (array-dimension v 1))
	(declare (type az:Array-Index j))
	(setf val (abs (aref v i j)))
	(when (> val max)
	  (setf max val))))
    max))

;;;=======================================================



