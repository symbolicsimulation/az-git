;;; -*- Mode: LISP; Syntax: Common-lisp; Package: User; -*-

(in-package 'user)

(export '(compile-load-cm-mds))
 
(defparameter *cm-mds-directory*
  #+lucid "/pooh-2g/pedersen/cm/mds/")

(defparameter *cm-mds-files* 
  '("minimize-1d"
    "utilities"
    "gradient-edges"
    "gradient-nodes"
    "examples"))

(defun compile-load-cm-mds ()
  (mapc 
    #'(lambda (f)
	(load 
	  (compile-file 
	    (concatenate 'String *cm-mds-directory* f ".lisp"))))
    *cm-mds-files*))

(defun load-cm-mds ()
  (mapc 
    #'(lambda (f)
	(load 
	  (concatenate 'String *cm-mds-directory* f)))
    *cm-mds-files*))
	
