;;;-*- Package: (CM-USER :USE (LISP *LISP)); Syntax: Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;; ============================================================
;;; un-directed edge version 


(*defun *edge-collect-data-from-nodes (;; input pvars
                                       $edge? $edge-node0 $edge-node1 
                                       $node-x $node-y 
                                       ;; output pvars
                                       $edge-x0 $edge-y0 $edge-x1 
                                       $edge-y1)
	(declare (type boolean-pvar $edge?)
		 (type (field-pvar *log-number-of-processors-limit*)
		       $edge-node0 $edge-node1)
		 (type float-pvar $node-x $node-y $edge-x0 $edge-y0 
		       $edge-x1 $edge-y1))
	(*when $edge?
	       (*set $edge-x0 
		     (pref!! $node-x $edge-node0
			     :collision-mode :many-collisions))
	       (*set $edge-y0 
		     (pref!! $node-y $edge-node0
			     :collision-mode :many-collisions))
	       (*set $edge-x1
		     (pref!! $node-x $edge-node1 
			     :collision-mode :many-collisions))
	       (*set $edge-y1
		     (pref!! $node-y $edge-node1 
			     :collision-mode :many-collisions))))


;;; ============================================================


(*defun *edge-compute-deltas (;; input pvars
                              $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1 
                              ;; output pvars
                              $edge-dx $edge-dy $edge-2d-length)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar $edge-x0 $edge-y0 $edge-x1 $edge-y1 
		 $edge-dx $edge-dy $edge-2d-length))
  (*when $edge? (*set $edge-dx (-!! $edge-x0 $edge-x1))
	 (*set $edge-dy (-!! $edge-y0 $edge-y1))
	 (*set $edge-2d-length (l2-norm!! $edge-dx $edge-dy))))


;;; ============================================================


(*defun *edge-collect-grad-from-nodes (;; input pvars
                                       $edge? $edge-node0 $edge-node1 
                                       $node-grad-x $node-grad-y 
                                       ;; output pvars
                                       $edge-grad-x0 $edge-grad-y0 
                                       $edge-grad-x1 $edge-grad-y1)
  (declare (type boolean-pvar $edge?)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1)
	   (type float-pvar $node-grad-x $node-grad-y $edge-grad-x0
		 $edge-grad-y0 $edge-grad-x1 $edge-grad-y1))
  (*when $edge?
    (*set $edge-grad-x0 (pref!! $node-grad-x $edge-node0
				:collision-mode :many-collisions))
    (*set $edge-grad-y0 (pref!! $node-grad-y $edge-node0
				:collision-mode :many-collisions))
    (*set $edge-grad-x1 (pref!! $node-grad-x $edge-node1
				:collision-mode :many-collisions))
    (*set $edge-grad-y1 (pref!! $node-grad-y $edge-node1
				:collision-mode :many-collisions))))


;;; ============================================================


(*defun *edge-try-gradient-step
       (
        ;; input pvars
        step-size $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1 
        $edge-grad-x0 $edge-grad-y0 $edge-grad-x1 $edge-grad-y1 

        ;; output pvars
        $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1)
       (declare (type boolean-pvar $edge?)
              (type float-pvar $edge-x0 $edge-y0 $edge-x1 $edge-y1 
                    $edge-grad-x0 $edge-grad-y0 $edge-grad-x1 
                    $edge-grad-y1 $edge-new-x0 $edge-new-y0 
                    $edge-new-x1 $edge-new-y1))
       (*when $edge? (*let (($step-size (the float-pvar (!! step-size))
                                   ))
                           (declare (type float-pvar $step-size))
                           (*set $edge-new-x0 (+!! $edge-x0
                                                   (*!! $step-size 
                                                        $edge-grad-x0))
                                 )
                           (*set $edge-new-y0 (+!! $edge-y0
                                                   (*!! $step-size 
                                                        $edge-grad-y0))
                                 )
                           (*set $edge-new-x1 (+!! $edge-x1
                                                   (*!! $step-size 
                                                        $edge-grad-x1))
                                 )
                           (*set $edge-new-y1 (+!! $edge-y1
                                                   (*!! $step-size 
                                                        $edge-grad-y1))
                                 ))))


;;; ============================================================


(*defun *edge-compute-distance-loss (;; input pvars
                                     $edge? $edge-length $edge-weight
                                     $edge-2d-length 
                                     ;; output pvar
                                     $edge-distance-loss)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar
		 $edge-length $edge-weight $edge-2d-length
		 $edge-distance-loss))
  (*when $edge?
    (*let (($dl (-!! $edge-length $edge-2d-length)))
      (declare (type float-pvar $dl))
      (*set $edge-distance-loss (*!! $edge-weight $dl $dl)))))


;;; ============================================================


(*defun *edge-send-loss-to-nodes (
                                  ;; input pvars
                                  $edge? $edge-loss $edge-node0 
                                  $edge-node1 $node? 

                                  ;; output pvar
                                  $node-loss)
       (declare (type boolean-pvar $edge? $node?)
              (type float-pvar $edge-loss $node-loss)
              (type (field-pvar *log-number-of-processors-limit*)
                    $edge-node0 $edge-node1))
       (*let* (($node-loss0 (!! 0.0))
               ($node-loss1 (!! 0.0)))
              (declare (type float-pvar $node-loss0 $node-loss1))
              (*when $edge? (*pset :add $edge-loss $node-loss0 
                                   $edge-node0)
                     (*pset :add $edge-loss $node-loss1 $edge-node1))
              (*when $node? (*set $node-loss (+!! $node-loss0 
                                                  $node-loss1)))))


;;; ============================================================
;;; assumes *edge-compute-distance-loss has just been called. 


(*defun *edge-compute-distance-grad (;; input pvars
                                     $edge? $edge-length $edge-weight
                                     $edge-2d-length $edge-dx $edge-dy
                                     ;; output pvars
                                     $edge-distance-grad-x 
                                     $edge-distance-grad-y)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar
		 $edge-length $edge-weight $edge-2d-length
		 $edge-dx $edge-dy
		 $edge-distance-grad-x $edge-distance-grad-y))
  (*when $edge?
    (*let* (($dl (-!! $edge-length $edge-2d-length)))
      (declare (type float-pvar $dl))
      (*set $edge-distance-grad-x
	    (*!! $edge-weight $dl (/!! $edge-dx $edge-2d-length)))
      (*set $edge-distance-grad-y
	    (*!! $edge-weight $dl (/!! $edge-dy $edge-2d-length))))))


;;; ============================================================


(*defun *edge-compute-order-loss (;; input pvars
                                  $edge? $edge-length $edge-weight
				  $edge-order $edge-dx 
                                  ;; output pvar
                                  $edge-order-loss)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar
		 $edge-length $edge-weight $edge-order $edge-dx 
		 $edge-order-loss))
  (*when $edge?
    (*let (($g 
	     ;; (*!! $edge-weight
	     ;;      (max!! (!! 0.0) (*!! $edge-order $edge-dx)))
	     (+!! (*!! $edge-order $edge-dx) $edge-length)))
      (declare (type float-pvar $g))
      (*set $edge-order-loss (*!! $edge-weight $g $g)))))


;;; ------------------------------------------------------------


(*defun *edge-compute-order-grad (;; input pvars
                                  $edge? $edge-length $edge-weight
				  $edge-order $edge-dx 
                                  ;; output pvars
                                  $edge-order-grad-x)
  (declare (type boolean-pvar $edge?)
	   (type float-pvar $edge-length $edge-weight $edge-order $edge-dx 
		 $edge-order-grad-x))
  (*when $edge?
    (*set $edge-order-grad-x
	  ;; (*!! (!! -2.0) $edge-weight
	  ;; (max!! (!! 0.0) (*!! $edge-order $edge-dx)))
	  (*!! (!! -2.0)
	       $edge-order
	       $edge-weight
	       (+!! (*!! $edge-order $edge-dx) $edge-length)))))


;;; ============================================================


(*defun *edge-send-grad-to-nodes (;; input pvars
                                  $edge? $edge-grad-x $edge-grad-y 
                                  $edge-node0 $edge-node1 $node? 
                                  ;; output pvar
                                  $node-grad-x $node-grad-y)
  (declare (type boolean-pvar $edge? $node?)
	   (type float-pvar $edge-grad-x $edge-grad-y $node-grad-x 
		 $node-grad-y)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1))
  (*let* (($node-grad-x0 (!! 0.0))
	  ($node-grad-y0 (!! 0.0))
	  ($node-grad-x1 (!! 0.0))
	  ($node-grad-y1 (!! 0.0)))
    (declare (type float-pvar $node-grad-x0 $node-grad-y0 
		   $node-grad-x1 $node-grad-y1))
    (*when $edge? (*pset :add $edge-grad-x $node-grad-x0 
			 $edge-node0)
	   (*pset :add $edge-grad-y $node-grad-y0 $edge-node0
		  )
	   (*pset :add (-!! $edge-grad-x)
		  $node-grad-x1 $edge-node1)
	   (*pset :add (-!! $edge-grad-y)
		  $node-grad-y1 $edge-node1))
    (*when $node? (*set $node-grad-x (+!! $node-grad-x0 
					  $node-grad-x1))
	   (*set $node-grad-y (+!! $node-grad-y0 
				   $node-grad-y1)))))


;;; ============================================================


(defvar *debug* t)

(defparameter *plot-steps?* #-lucid t #+lucid nil)

(defun edge-mds (graph dist-fn weight-fn
		 &key
		 (nodes (collect-nodes graph))
		 (initial-x (make-random-vector (length nodes)))
		 (initial-y (make-random-vector (length nodes)))
		 (step-size (/ 1.0 (length nodes)))
		 (max-iterations 1000)
		 #-lucid (plot-fn #'plot-xy)
		 #-lucid (plot *plot*)
		 #-lucid (canvas *canvas*))
  (let* ((n-nodes (length nodes))
	 (n-processors (next-power-of-2 (+ (length graph)
					   (length nodes))))
	 (x-coords (make-array n-nodes))
	 (y-coords (make-array n-nodes))
	 (grad-x (make-array n-nodes))
	 (grad-y (make-array n-nodes))
	 (current-loss 0.0))
             
    ;; allocate more processors than we will need
    (when (> n-processors *number-of-processors-limit*)
      (*cold-boot :initial-dimensions (list n-processors)))
    (*let (($node? nil!!)
	   ($node-x (!! 0.0))
	   ($node-y (!! 0.0))
	   ($new-node-x (!! 0.0))
	   ($new-node-y (!! 0.0))
	   ($node-distance-grad-x (!! 0.0))
	   ($node-distance-grad-y (!! 0.0))
	   ($node-distance-loss (!! 0.0))
	   ($edge? nil!!)
	   ($edge-node0 (!! 0))
	   ($edge-node1 (!! 0))
	   ($edge-length (!! 0.0))
	   ($edge-weight (!! 1.0))
	   ($edge-2d-length (!! 0.0))
	   ($edge-x0 (!! 0.0))
	   ($edge-y0 (!! 0.0))
	   ($edge-x1 (!! 0.0))
	   ($edge-y1 (!! 0.0))
	   ($edge-new-x0 (!! 0.0))
	   ($edge-new-y0 (!! 0.0))
	   ($edge-new-x1 (!! 0.0))
	   ($edge-new-y1 (!! 0.0))
	   ($edge-dx (!! 0.0))
	   ($edge-dy (!! 0.0))
	   ($edge-distance-loss (!! 0.0))
	   ($edge-distance-grad-x (!! 0.0))
	   ($edge-distance-grad-y (!! 0.0))
	   ($edge-distance-grad-x0 (!! 0.0))
	   ($edge-distance-grad-y0 (!! 0.0))
	   ($edge-distance-grad-x1 (!! 0.0))
	   ($edge-distance-grad-y1 (!! 0.0)))
      (declare (type boolean-pvar $node? $edge?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $edge-node0 $edge-node1)
	       (type float-pvar $node-x $node-y $new-node-x
		     $new-node-y $node-distance-grad-x 
		     $node-distance-grad-y 
		     $node-distance-loss $edge-length $edge-weight
		     $edge-2d-length $edge-x0 $edge-y0 
		     $edge-x1 $edge-y1 $edge-new-x0 
		     $edge-new-y0 $edge-new-x1 $edge-new-y1
		     $edge-dx $edge-dy $edge-distance-loss 
		     $edge-distance-grad-x 
		     $edge-distance-grad-y 
		     $edge-distance-grad-x0 
		     $edge-distance-grad-y0 
		     $edge-distance-grad-x1 
		     $edge-distance-grad-y1))
      (let* ((node-interval
	       (*build-graph
		 graph nodes dist-fn weight-fn initial-x initial-y
		 $node? $node-x $node-y
		 $edge? $edge-node0 $edge-node1 $edge-length $edge-weight))
	     (nodes-start (car node-interval))
	     (nodes-end (cdr node-interval)))
	(flet ((try-step (step)
		 (*edge-try-gradient-step
		   step 
		   $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1 
		   $edge-distance-grad-x0 $edge-distance-grad-y0 
		   $edge-distance-grad-x1 $edge-distance-grad-y1 
		   $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1)
		 (*edge-compute-deltas
		   $edge? $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1
		   $edge-dx $edge-dy $edge-2d-length)
		 (*edge-compute-distance-loss
		   $edge? $edge-length $edge-weight
		   $edge-2d-length 
		   $edge-distance-loss)
		 (*when $edge? (*sum $edge-distance-loss))))
	  (dotimes (k max-iterations
		      (warn "~a iterations" max-iterations))
	    (*clear-nodes $node? $node-distance-grad-x $node-distance-grad-y 
			  $node-distance-loss)
	    (*edge-collect-data-from-nodes
	      $edge? $edge-node0 $edge-node1 
	      $node-x $node-y $edge-x0 $edge-y0 $edge-x1 $edge-y1)
	    (*edge-compute-deltas 
	      $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1
	      $edge-dx $edge-dy $edge-2d-length)
	    (*edge-compute-distance-grad
	      $edge?
	      $edge-length $edge-weight $edge-2d-length
	      $edge-dx $edge-dy 
	      $edge-distance-grad-x 
	      $edge-distance-grad-y)
	    (*edge-send-grad-to-nodes
	      $edge? $edge-distance-grad-x 
	      $edge-distance-grad-y $edge-node0 
	      $edge-node1 $node? 
	      $node-distance-grad-x 
	      $node-distance-grad-y)
	    (*edge-collect-grad-from-nodes
	      $edge? $edge-node0 $edge-node1 
	      $node-distance-grad-x 
	      $node-distance-grad-y 
	      $edge-distance-grad-x0 
	      $edge-distance-grad-y0 
	      $edge-distance-grad-x1 
	      $edge-distance-grad-y1)
	    (multiple-value-setq
	      (step-size current-loss)
	      (1d-minimize #'try-step 0.0 
			   step-size 
			   :absolute-tolerance 0.01))
	    (format *standard-output* 
		    "~& step-size= ~f; loss= ~f."
		    step-size current-loss)
	    #-lucid
	    (when *plot-steps?*
	      (*plot-gradient-step $node-x 
				   $node-y 
				   $node-distance-grad-x 
				   $node-distance-grad-y 
				   nodes-start nodes-end 
				   step-size nodes 
				   x-coords y-coords 
				   grad-x grad-y :plot-fn
				   plot-fn :canvas canvas
				   :plot plot))
	    (*take-gradient-step step-size 
				 $node? $node-x $node-y 
				 $node-distance-grad-x 
				 $node-distance-grad-y)))))))


;;; ============================================================

#||
(defun edge-mds0 (graph dist-fn &key (nodes (collect-nodes graph))
                        (initial-x (make-random-vector (length nodes)))
                        (initial-y (make-random-vector (length nodes)))
                        (step-size (/ 1.0 (length nodes)))
                        (max-iterations 1000)
                        #-lucid (plot-fn #'plot-xy)
                        #-lucid (plot *plot*)
                        #-lucid (canvas *canvas*))
       (let* ((n-nodes (length nodes))
              (n-processors (next-power-of-2 (+ (length graph)
                                                (length nodes))))
              (x-coords (make-array n-nodes))
              (y-coords (make-array n-nodes))
              (grad-x (make-array n-nodes))
              (grad-y (make-array n-nodes))
              (current-loss 0.0))
             
             ;; allocate more processors than we will need
             (when (> n-processors *number-of-processors-limit*)
                   (*cold-boot :initial-dimensions 9list n-processors)))
             (*let (($node? nil!!)
                    ($node-x (!! 0.0))
                    ($node-y (!! 0.0))
                    ($new-node-x (!! 0.0))
                    ($new-node-y (!! 0.0))
                    ($node-distance-grad-x (!! 0.0))
                    ($node-distance-grad-y (!! 0.0))
                    ($node-distance-loss (!! 0.0))
                    ($edge? nil!!)
                    ($edge-node0 (!! 0))
                    ($edge-node1 (!! 0))
                    ($edge-length (!! 0.0))
                    ($edge-2d-length (!! 0.0))
                    ($edge-x0 (!! 0.0))
                    ($edge-y0 (!! 0.0))
                    ($edge-x1 (!! 0.0))
                    ($edge-y1 (!! 0.0))
                    ($edge-dx (!! 0.0))
                    ($edge-dy (!! 0.0))
                    ($edge-distance-loss (!! 0.0))
                    ($edge-distance-grad-x (!! 0.0))
                    ($edge-distance-grad-y (!! 0.0)))
                   (declare (type boolean-pvar $node? $edge?)
                          (type (field-pvar 
                                       *log-number-of-processors-limit*
                                       )
                                $edge-node0 $edge-node1)
                          (type float-pvar $node-x $node-y $new-node-x
                                $new-node-y $node-distance-grad-x 
                                $node-distance-grad-y 
                                $node-distance-loss $edge-length 
                                $edge-2d-length $edge-x0 $edge-y0 
                                $edge-x1 $edge-y1 $edge-dx $edge-dy 
                                $edge-distance-loss 
                                $edge-distance-grad-x 
                                $edge-distance-grad-y))
                   (let* ((node-interval
			   (*build-graph graph nodes 
					 dist-fn initial-x 
					 initial-y $node? 
					 $node-x $node-y $edge?
					 $edge-node0 $edge-node1
					 $edge-length))
                          (nodes-start (car node-interval))
                          (nodes-end (cdr node-interval)))
                         (flet ((try-step (step)
                                       (*try-gradient-step step $node?
                                              $node-x $node-y 
                                              $node-distance-grad-x 
                                              $node-distance-grad-y 
                                              $new-node-x $new-node-y)
                                       (*edge-collect-data-from-nodes
                                        $edge? $edge-node0 $edge-node1
                                        $new-node-x $new-node-y 
                                        $edge-x0 $edge-y0 $edge-x1 
                                        $edge-y1)
                                       (*edge-compute-deltas $edge? 
                                              $edge-x0 $edge-y0 
                                              $edge-x1 $edge-y1 
                                              $edge-dx $edge-dy 
                                              $edge-2d-length)
                                       (*edge-compute-distance-loss
                                        $edge? $edge-length 
                                        $edge-2d-length 
                                        $edge-distance-loss)
                                           
                                           ; (*edge-send-distance-loss-to-nodes
                                           ; $edge?
                                           ; $edge-distance-loss
                                           ; $edge-node0 $edge-node1
                                           ; $node?
                                           ; $node-distance-loss)
                                           ; (*when $node? (*sum
                                           ; $node-distance-loss)) 
                                       (*when $edge? (*sum 
                                                    $edge-distance-loss
                                                           ))))
                               (dotimes (k max-iterations
                                           (warn "~a iterations" 
                                                 max-iterations))
                                   (*clear-nodes $node? 
                                          $node-distance-grad-x 
                                          $node-distance-grad-y 
                                          $node-distance-loss)
                                   (*edge-collect-data-from-nodes
                                    $edge? $edge-node0 $edge-node1 
                                    $node-x $node-y $edge-x0 $edge-y0 
                                    $edge-x1 $edge-y1)
                                   (*edge-compute-deltas $edge? 
                                          $edge-x0 $edge-y0 $edge-x1 
                                          $edge-y1 $edge-dx $edge-dy 
                                          $edge-2d-length)
                                           
                                           ; (*edge-compute-distance-loss
                                           ; $edge? $edge-length
                                           ; $edge-2d-length
                                           ; $edge-distance-loss) 
                                   (*edge-compute-distance-grad $edge?
                                          $edge-length $edge-2d-length
                                          $edge-dx $edge-dy 
                                          $edge-distance-grad-x 
                                          $edge-distance-grad-y)
                                           
                                           ; (*edge-send-distance-loss-to-nodes
                                           ; $edge?
                                           ; $edge-distance-loss
                                           ; $edge-node0 $edge-node1
                                           ; $node?
                                           ; $node-distance-loss) 
                                   (*edge-send-distance-grad-to-nodes
                                    $edge? $edge-distance-grad-x 
                                    $edge-distance-grad-y $edge-node0 
                                    $edge-node1 $node? 
                                    $node-distance-grad-x 
                                    $node-distance-grad-y)
                                   (multiple-value-setq
                                    (step-size current-loss)
                                    (1d-minimize #'try-step 0.0 
                                           step-size :tolerance 0.01))
                                   (format *standard-output* 
                                          "~& step-size= ~f; loss= ~f."
                                          step-size current-loss)
				   #-lucid
                                   (when *plot-steps?*
                                         (*plot-gradient-step $node-x 
                                                $node-y 
                                                $node-distance-grad-x 
                                                $node-distance-grad-y 
                                                nodes-start nodes-end 
                                                step-size nodes 
                                                x-coords y-coords 
                                                grad-x grad-y :plot-fn
                                                plot-fn :canvas canvas
                                                :plot plot))
                                   (*take-gradient-step step-size 
                                          $node? $node-x $node-y 
                                          $node-distance-grad-x 
                                          $node-distance-grad-y)))))))
||#


;;; ============================================================


(defun ordered-edge-mds (graph dist-fn weight-fn order-fn
			 &key
			 (alpha 0.5)
			 (nodes (collect-nodes graph))
			 (initial-x (make-random-vector (length nodes)))
			 (initial-y (make-random-vector (length nodes)))
			 (step-size (/ 1.0 (length nodes)))
			 (max-iterations 1000)
			 #-lucid (plot-fn #'plot-xy)
			 #-lucid (plot *plot*)
			 #-lucid (canvas *canvas*))
  (let* ((n-nodes (length nodes))
	 (n-processors (next-power-of-2 (+ (length graph) (length nodes))))
	 (x-coords (make-array n-nodes))
	 (y-coords (make-array n-nodes))
	 (grad-x (make-array n-nodes))
	 (grad-y (make-array n-nodes))
	 (current-loss 0.0))
       
    ;; allocate more processors than we will need
    (if (> n-processors *number-of-processors-limit*)
	(*cold-boot :initial-dimensions (list n-processors))
	(*warm-boot))
    (*let (($node? nil!!)
	   ($node-x (!! 0.0))
	   ($node-y (!! 0.0))
	   ($new-node-x (!! 0.0))
	   ($new-node-y (!! 0.0))
	   ($node-grad-x (!! 0.0))
	   ($node-grad-y (!! 0.0))
	   ($node-loss (!! 0.0))
	   ($edge? nil!!)
	   ($edge-node0 (!! 0))
	   ($edge-node1 (!! 0))
	   ($edge-length (!! 0.0))
	   ($edge-order (!! 0.0))
	   ($edge-weight (!! 1.0))
	   ($edge-2d-length (!! 0.0))
	   ($edge-x0 (!! 0.0))
	   ($edge-y0 (!! 0.0))
	   ($edge-x1 (!! 0.0))
	   ($edge-y1 (!! 0.0))
	   ($edge-new-x0 (!! 0.0))
	   ($edge-new-y0 (!! 0.0))
	   ($edge-new-x1 (!! 0.0))
	   ($edge-new-y1 (!! 0.0))
	   ($edge-dx (!! 0.0))
	   ($edge-dy (!! 0.0))
	   ($edge-distance-loss (!! 0.0))
	   ($edge-distance-grad-x (!! 0.0))
	   ($edge-distance-grad-y (!! 0.0))
	   ($edge-grad-x0 (!! 0.0))
	   ($edge-grad-y0 (!! 0.0))
	   ($edge-grad-x1 (!! 0.0))
	   ($edge-grad-y1 (!! 0.0))
	   ($edge-order-loss (!! 0.0))
	   ($edge-order-grad-x (!! 0.0))
	   ($alpha (the float-pvar (!! alpha)))
	   ($1-alpha (the float-pvar (!! (- 1.0 alpha)))))
      (declare (type boolean-pvar $node? $edge?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $edge-node0 $edge-node1)
	       (type float-pvar $node-x $node-y $new-node-x 
		     $new-node-y $node-grad-x $node-grad-y 
		     $node-loss $edge-length $edge-weight $edge-order
		     $edge-2d-length 
		     $edge-x0 $edge-y0 $edge-x1 $edge-y1 
		     $edge-new-x0 $edge-new-y0 $edge-new-x1 
		     $edge-new-y1 $edge-dx $edge-dy 
		     $edge-distance-loss $edge-distance-grad-x 
		     $edge-distance-grad-y $edge-grad-x0 
		     $edge-grad-y0 $edge-grad-x1 $edge-grad-y1 
		     $edge-order-loss $edge-order-grad-x $alpha 
		     $1-alpha))
      (let* ((node-interval
	       (*build-ordered-graph
		 graph nodes dist-fn weight-fn order-fn initial-x initial-y
		 $node? $node-x $node-y
		 $edge? $edge-node0 $edge-node1
		 $edge-length $edge-weight $edge-order))
	     (nodes-start (car node-interval))
	     (nodes-end (cdr node-interval)))
	(flet ((try-step (step)
		 (*edge-try-gradient-step
		   step $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1
		   $edge-grad-x0 $edge-grad-y0 $edge-grad-x1 $edge-grad-y1
		   $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1)
		 (*edge-compute-deltas
		   $edge? $edge-new-x0 $edge-new-y0 $edge-new-x1 $edge-new-y1 
		   $edge-dx $edge-dy $edge-2d-length)
		 (*edge-compute-distance-loss
		   $edge? $edge-length $edge-weight $edge-2d-length 
		   $edge-distance-loss)
		 (*edge-compute-order-loss
		   $edge? $edge-length $edge-weight $edge-order $edge-dx
		   $edge-order-loss)
		 (*when $edge?
		   (*sum (+!! (*!! $1-alpha $edge-distance-loss)
			      (*!! $alpha $edge-order-loss))))))
	  (dotimes (k max-iterations
		      (warn "~a iterations" max-iterations))
	    (*clear-nodes $node? $node-grad-x $node-grad-y $node-loss)
	    (*edge-collect-data-from-nodes
	      $edge? 
	      $edge-node0 $edge-node1 $node-x 
	      $node-y $edge-x0 $edge-y0 $edge-x1
	      $edge-y1)
	    (*edge-compute-deltas
	      $edge? $edge-x0 $edge-y0 $edge-x1 $edge-y1
	      $edge-dx $edge-dy $edge-2d-length)
	    (*edge-compute-distance-grad
	      $edge? $edge-length $edge-weight $edge-2d-length 
	      $edge-dx $edge-dy $edge-distance-grad-x $edge-distance-grad-y)
	    (*edge-compute-order-grad
	      $edge? $edge-length $edge-weight $edge-order $edge-dx 
	      $edge-order-grad-x)
	    (*edge-send-grad-to-nodes
	      $edge?
	      (+!! (*!! $1-alpha $edge-distance-grad-x)
		   (*!! $alpha $edge-order-grad-x))
	      (*!! $1-alpha $edge-distance-grad-y)
	      $edge-node0 $edge-node1 $node? 
	      $node-grad-x $node-grad-y)
	    (*edge-collect-grad-from-nodes $edge? 
					   $edge-node0 $edge-node1 
					   $node-grad-x $node-grad-y 
					   $edge-grad-x0 $edge-grad-y0 
					   $edge-grad-x1 $edge-grad-y1)
	    (multiple-value-setq
	      (step-size current-loss)
	      (1d-minimize #'try-step -0.01
			   (+ (abs step-size)
			      0.01)
			   :absolute-tolerance 0.01))
	    #-lucid
	    (when *debug*
	      (plot-function #'try-step
			     (* -10.0 (+ 0.01 step-size))
			     (* 10.0 (+ 0.01 step-size))
			     :nsteps 20))
	    (format *standard-output* 
		    "~& step-size= ~f; loss= ~f." 
		    step-size current-loss)
	    #-lucid
	    (when *plot-steps?*
	      (*plot-gradient-step $node-x $node-y
				   $node-grad-x $node-grad-y 
				   nodes-start nodes-end 
				   step-size nodes x-coords 
				   y-coords grad-x grad-y 
				   :plot-fn plot-fn :canvas 
				   canvas :plot plot))
	    (*take-gradient-step step-size $node? 
				 $node-x $node-y $node-grad-x 
				 $node-grad-y)))))))
