;;;-*- Package:(CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;;============================================================
;;; distances

(defun l2-dist (v0 v1)
  (check-type v0 Vector)
  (check-type v1 Vector)
  (assert (= (length v0) (length v1)))
  (let ((sum 0.0))
    (dotimes (i (length v0))
      (let ((dx (- (aref v0 i) (aref v1 i))))
	(incf sum (* dx dx))))
    (sqrt sum)))

#+Symbolics 
(defparameter *root-bitmap* 
  (2dg:make-bitmap
    :width 12
    :height 12
    :raw-bitmap
    (graphics:with-output-to-bitmap ()
      (graphics:draw-circle 5 5 1 :filled nil)
      (graphics:draw-circle 5 5 3 :filled nil)
      (graphics:draw-circle 5 5 5 :filled nil))))

;;;============================================================

(defun make-2d-circle-objects (&optional (n 30))
  (let* ((objects (make-array n))
	 (inc (float (/ (* 2 pi) n) 1.0e0)))
    (dotimes (i n)
      (let ((theta (* i inc)))
	(setf (aref objects i) (vector (cos theta) (sin theta)))))
    objects))

(defparameter *2d-circle-objects* (make-2d-circle-objects 32))

(defparameter *2d-circle-graph*
	      (make-complete-undirected-graph
		(coerce *2d-circle-objects* 'List)))

;;;============================================================

(defun make-3d-circle-objects (&optional (n 30))
  (let* ((n/3 (truncate n 3))
	 (objects (make-array (* 3 n/3)))
	 (dtheta (/ (* 2 pi) n/3))
	 (j 0))
    (dotimes (i n/3)
      (let ((theta (* i dtheta)))
	(setf (aref objects j) (vector (+ 1.0 (cos theta)) (sin theta) 0.0))
	(incf j)))
    (dotimes (i n/3)
      (let ((theta (* (+ i 0.01) dtheta)))
	(setf (aref objects j) (vector (cos theta) 0.0 (- (sin theta) 1.0)))
	(incf j)))
    (dotimes (i n/3)
      (let ((theta (* i dtheta)))
	(setf (aref objects j) (vector 0.0 (+ (cos theta) 1.0) (sin theta)))
	(incf j)))
    objects))

(defparameter *3d-circle-objects* (make-3d-circle-objects 32))

;;;============================================================

(defun make-tree-object (&optional (depth 4) (breadth 2))
  (let* ((objects (make-array 10 :initial-element nil :fill-pointer 1 
			      :adjustable t)))
    (labels ((add-children (parent current-depth)
	       (unless (eq depth current-depth)
		 (dotimes (j (+ (random breadth) 2))
		   (let ((node (append parent (list j))))
		     (vector-push-extend node objects)
		     (add-children node (1+ current-depth)))))))
      (add-children nil 0))
    objects))



(defun graph-distance (node1 node2)
  (do ((node1-tail node1 (cdr node1-tail))
       (node2-tail node2 (cdr node2-tail)))
      ((or (null node1-tail)
	   (null node2-tail)
	   (not (eq (car node1-tail) (car node2-tail))))
       (float (+ (length node1-tail) (length node2-tail))))))

#-lucid
(defun traverse-tree (walk-fn parent nodes)
  (let* ((length-parent (length parent))
	 (child-indices
	   (2dg:with-collection
	     (dotimes (i (length nodes))
	       (let ((node (aref nodes i)))
		 (if (and (= (1+ length-parent) (length node))
			  (= (mismatch parent node) length-parent))
		     (2dg:collect i)))))))
    (funcall walk-fn parent nodes)
    (dolist (child-index child-indices)
      (traverse-tree walk-fn (aref nodes child-index) nodes))))

(defun parent-order (node0 node1)
  (let* ((l0 (length node0)) (l1 (length node1)))
    (cond 
      ((and (= (1+ l0) l1) (= (mismatch node0 node1) l0))
       1.0)
      ((and (= (1+ l1) l0) (= (mismatch node1 node0) l1))
       -1.0)
      (t 0.0))))

(defun ancestor-order (node0 node1)
  (let* ((l0 (length node0)) (l1 (length node1)))
    (cond 
      ((= (mismatch node0 node1) l0)
       1.0)
      ((= (mismatch node1 node0) l1)
       -1.0)
      (t 0.0))))
#-lucid
(defun layout-tree (nodes)
  (let* ((n (length nodes))
	 (x-coords (make-array n))
	 (y-coords (make-array n))
	 (root (aref nodes 0)))
    (setf (aref x-coords 0) 0.0)
    (setf (aref y-coords 0) 0.0)
    (layout-node root nodes 0.0 0.0 0.0
		 (float (* 2 pi) 1.0)
		 x-coords y-coords)
    (values x-coords y-coords)))

#-lucid
(defun layout-node (parent nodes x y angle angle-range x-coords y-coords)
  (let* ((length-parent (length parent))
	 (child-indices
	   (2dg:with-collection
	     (dotimes (i (length nodes))
	       (let ((node (aref nodes i)))
		 (if (and (= (length node) (1+ length-parent))
			  (= (mismatch parent node) length-parent))
		     (2dg:collect i)))))))
    (when child-indices
      (let* ((d-theta (/ angle-range (length child-indices)))
	     (theta (+ (- angle (/ angle-range 2.0)) (/ d-theta 2.0))))
	(dolist (child-index child-indices)
	  (let ((child (aref nodes child-index))
		(child-x (+ x (cos theta)))
		(child-y (+ y (sin theta))))
	    (setf (aref x-coords child-index) child-x)
	    (setf (aref y-coords child-index) child-y)
	    (layout-node
	      child nodes child-x child-y theta d-theta x-coords y-coords)
	    (incf theta d-theta)))))))

#-lucid
(defun plot-initial-layout (nodes
			    &optional x-coords y-coords
			    &key (canvas *canvas*) (plot *plot*))
  (if (or (null x-coords) (null y-coords))
      (multiple-value-setq (x-coords y-coords) (layout-tree nodes)))
  (setf (2dp:plot-objects plot) nil)
  (2dp:add-plot-objects
    (2dg:with-collection
      (dotimes (i (length x-coords))
	(let ((symbol (ecase (mod (length (aref nodes i)) 4)
			(0 *root-bitmap*)
			(1 2dg:*circle-bitmap*)
			(2 2dg:*star-bitmap*)
			(3 2dg:*cross-bitmap*))))
	  (2dg:collect
	    (2dp:make-point
	      (2dg:make-position (aref x-coords i) (aref y-coords i))
	      :symbol symbol)))))
    plot)
  (labels
    ((add-edges (parent parent-index)
       (let*
	 ((length-parent (length parent))
	  (child-indices
	    (2dg:with-collection
	      (dotimes (i (length nodes))
		(let ((node (aref nodes i)))
		  (if (and (= (1+ length-parent) (length node))
			   (= (mismatch parent node) length-parent))
		      (2dg:collect i)))))))
	 (dolist (child-index child-indices)
	   (2dp:add-plot-object
	     (2dp:make-curve
	       (list (2dg:make-position (aref x-coords parent-index)
					(aref y-coords parent-index))
		     (2dg:make-position (aref x-coords child-index)
					(aref y-coords child-index)))
	       :style
	       (2dp::make-line-style :width 1 :dashing 2dg:*dash*))
	     plot)
	   (add-edges (aref nodes child-index) child-index)))))
    (add-edges nil 0))
  (2dp:rescale-plot plot :both)
  (2dp:redraw-plot plot canvas)
  (values x-coords y-coords))

(defparameter *plot-points?* nil)

#-lucid
(defun plot-tree-xy (plot canvas nodes x-coords y-coords x-step y-step)
  (setf nodes (coerce nodes 'Vector))
  (setf (2dp:plot-objects plot) nil)
  (2dp:add-plot-objects
    (2dg:with-collection
      (dotimes (i (length x-coords))
	(let* ((x0 (aref x-coords i))
	       (y0 (aref y-coords i))
	       (x1 (+ x0 (aref x-step i)))
	       (y1 (+ y0 (aref y-step i)))
	       (symbol (ecase (mod (length (aref nodes i)) 4)
			 (0 *root-bitmap*)
			 (1 2dg:*circle-bitmap*)
			 (2 2dg:*star-bitmap*)
			 (3 2dg:*cross-bitmap*))))
	  (when *plot-points?*
	    (2dg:collect
	      (2dp:make-point (2dg:make-position x0 y0) :symbol symbol)))
	  (2dg:collect
	    (2dp:make-curve
	      (list (2dg:make-position x0 y0) (2dg:make-position x1 y1)))))))
    plot)
  (labels
    ((add-edges
	(parent parent-index)
       (let*
	 ((length-parent (length parent))
	  (child-indices
	    (2dg:with-collection
	      (dotimes (i (length nodes))
		(let ((node (aref nodes i)))
		  (if (and (= (1+ length-parent) (length node))
			   (= (mismatch parent node) length-parent))
		      (2dg:collect i)))))))
	 (dolist (child-index child-indices)
	   (2dp:add-plot-object
	     (2dp:make-curve
	       (list (2dg:make-position (aref x-coords parent-index)
					(aref y-coords parent-index))
		     (2dg:make-position (aref x-coords child-index)
					(aref y-coords child-index)))
	       :style
	       (2dp::make-line-style :width 1
				     :dashing 2dg:*dash*))
	     plot)
	   (add-edges (aref nodes child-index) child-index)))))
    (add-edges nil 0))
  (2dp:rescale-plot plot :both)
  (2dp:redraw-plot plot canvas))

(defun sqrt-graph-distance (node1 node2) (sqrt (graph-distance node1 node2)))
(defun log-graph-distance (node1 node2)
  (log (+ 1.0 (graph-distance node1 node2))))
(defun square-graph-distance (node1 node2)
  (let ((d (graph-distance node1 node2)))
    (* d d)))

(defun sibling? (node0 node1)
  (and (= (length node0) (length node1))
       (= (- (length node0) 1) (mismatch node0 node1))))

(defparameter *sibling-distance-factor* 5.0)

(defun sibling-distance (node0 node1)
  (if (sibling? node0 node1)
      (let ((sibling-index0 (first (last node0)))
	    (sibling-index1 (first (last node1))))
	(/ (abs (- sibling-index0 sibling-index1)) *sibling-distance-factor*))
      ;; else
      (graph-distance node0 node1)))

(defun generation-distance (node0 node1)
  (* (+ 1.0 (abs (- (length node0) (length node1))))
     (graph-distance node0 node1)))

(defun trivial-weight (node0 node1 length)
  (declare (ignore node0 node1 length))
  1.0)

(defun standard-weight (node0 node1 length)
  (declare (ignore node0 node1))
  (/ 1.0 length))

(defun parent-weight (node0 node1 length)
  (let ((p (parent-order node0 node1)))
    (* (+ 1.0 (* p p)) (/ 1.0 length))))

(defun ancestor-weight (node0 node1 length)
  (let ((p (ancestor-order node0 node1)))
    (* (+ 1.0 (* p p)) (/ 1.0 length))))

(defun trivial-order (node0 node1) (declare (ignore node0 node1)) 0.0)

(defparameter *tree-object* (make-tree-object 3 3))
(defparameter *tree-graph* (make-complete-undirected-graph *tree-object*))
(defvar *initial-x*)
(defvar *initial-y*)

