;;;-*- Package:(CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;;============================================================

#-lucid
(defparameter *canvas* (2dp:make-canvas))
#-lucid
(defparameter *plot* (let ((p (2dp:make-plot)))
		       (setf (2dp:tics-p p :left) t)
		       (setf (2dp:tics-p p :bottom) t)
		       p))


#-lucid
(defparameter *function-canvas* (2dp:make-canvas))
#-lucid
(defparameter *function-plot* (let ((p (2dp:make-plot)))
				(setf (2dp:tics-p p :left) t)
				(setf (2dp:tics-p p :bottom) t)
				p))

;;;============================================================

#-lucid
(defun plot-xy (plot canvas nodes x-coords y-coords x-step y-step)
  (declare (ignore nodes))
  (setf (2dp:plot-objects plot) nil)
  (let* ((n (length x-coords))
	 (n/3 (ceiling n 3)))
    (2dp:add-plot-objects
      (2dg:with-collection
	(dotimes (i n)
	  (let* ((x0 (aref x-coords i))
		 (y0 (aref y-coords i))
		 (x1 (+ x0 (aref x-step i)))
		 (y1 (+ y0 (aref y-step i)))
		 (symbol (ecase (truncate i n/3)
			   (0 2dg:*circle-bitmap*)
			   (1 2dg:*star-bitmap*)
			   (2 2dg:*cross-bitmap*))))
	    (2dg:collect (2dp:make-point
			   (2dg:make-position x0 y0) :symbol symbol))
	    (2dg:collect (2dp:make-curve (list (2dg:make-position x0 y0)
					       (2dg:make-position x1 y1)))))))
      plot)
    (2dp:rescale-plot plot)
    (2dp:redraw-plot plot canvas)))

;;;============================================================


#-lucid
(defun plot-function (f min max
		      &key
		      (nsteps 32)
		      (plot *function-plot*)
		      (canvas *function-canvas*))
  (when (> min max) (rotatef min max))
  (let ((step (/ (- max min) (float nsteps))))
    (setf (2dp:plot-objects plot) nil)
    (2dp:plot-curve
      plot
      (2dg:with-collection
	(do ((x min (+ x step)))
	    ((> x max))
	  (2dg:collect (2dg:make-position x (funcall f x))))))
    (2dp:rescale-plot plot)
    (2dp:redraw-plot plot canvas)))
		      
;;;============================================================
;;; random utilities:

(defun next-power-of-2 (n)
       (expt 2 (integer-length (1- n))))

(defun make-random-vector (n)
  (let ((v (make-array n)))
    (dotimes (i n) (setf (aref v i) (random 1.0)))
    v))

;;;============================================================
;;; distances

#+*lisp-hardware
(*proclaim
  '(function l2-norm!! (float-pvar float-pvar) float-pvar))

(*defun l2-norm!! ($x $y)
  (declare (type float-pvar $x $y))
  (sqrt!! (+!! (*!! $x $x) (*!! $y $y))))

#+*lisp-hardware 
(*proclaim
  '(function l2-dist!! (float-pvar float-pvar float-pvar float-pvar)
	     float-pvar))

(*defun l2-dist!! ($x0 $y0 $x1 $y1)
  (declare (type float-pvar $x0 $y0 $x1 $y1))
  (*let (($dx (-!! $x0 $x1))
	 ($dy (-!! $y0 $y1)))
    (l2-norm!! $dx $dy)))

;;;============================================================

(defun make-complete-undirected-graph (nodes)
  ;; returns an edge-list, each edge is a list of 2 nodes
  (setf nodes (coerce nodes 'List))
  (mapcon
    #'(lambda (l)
	(let ((1st-node (first l)))
	  (mapcar #'(lambda (2nd-node) (list 1st-node 2nd-node))
		  (rest l))))
    nodes))

(defun make-complete-directed-graph (nodes)
  ;; returns an edge-list, each edge is a list of 2 nodes
  (declare (type List nodes))
  (mapcan
    #'(lambda (1st-node)
	(mapcar #'(lambda (2nd-node) (list 1st-node 2nd-node))
		nodes))
    nodes))

(defmacro edge-node0 (edge) `(first ,edge))
(defmacro edge-node1 (edge) `(second ,edge))

(defun collect-nodes (graph)
  (let ((nodes ()))
    (dolist (edge graph)
      (pushnew (edge-node0 edge) nodes)
      (pushnew (edge-node1 edge) nodes))
    nodes))

;;;==================================================================
;;; associate a processor with some object on the front end

(defvar *processor-table* (make-hash-table))
(defvar *next-free-cube-address* 0 "Used to allocate processors.")

(defun initialize-processor-table ()
  (clrhash *processor-table*)
  (setf *next-free-cube-address* 0))

(defun allocate-processor-cube-address (object)
  (prog1
    *next-free-cube-address*
    (if (> *next-free-cube-address* *number-of-processors-limit*)
	(error "Allocating too many processors.")
	(progn
	  (setf (gethash object *processor-table*) *next-free-cube-address*)
	  (incf *next-free-cube-address*)))))

(defun allocate-processor-cube-addresses (v)
  (let ((old-cube-address *next-free-cube-address*))
    (incf *next-free-cube-address* (length v))
    (if (> *next-free-cube-address* *number-of-processors-limit*)
	(error "Allocating too many processors.")
	;; else
	(let ((p old-cube-address))
	  (map nil #'(lambda (object)
		       (setf (gethash object *processor-table*) p)
		       (incf p))
	       v)))
    (values old-cube-address *next-free-cube-address*)))

(defun processor-cube-address (object) (gethash object *processor-table*))
  
l;;;------------------------------------------------------------------

(*defun *initialize-graph-processors ($node? $edge?)
  (declare (type boolean-pvar $node? $edge?))
  ;; initialize all the processors in the machine
  (*set $node? nil!!)
  (*set $edge? nil!!)
  (initialize-processor-table))

;;;------------------------------------------------------------------

(*defun *build-graph ( edges nodes dist-fn weight-fn initial-x initial-y
		      $node? $node-x $node-y
		     $edge? $edge-node0 $edge-node1
		     $edge-length $edge-weight)
  (declare (type boolean-pvar $node? $edge?)
	   (type float-pvar $node-x $node-y $edge-length $edge-weight)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1))

  (*initialize-graph-processors $node? $edge?)
  
  ;; assign processors to nodes
  (multiple-value-bind
    (nodes-start nodes-end) (allocate-processor-cube-addresses nodes)

    (*when (<=!! (the (field-pvar *log-number-of-processors-limit*)
		      (!! nodes-start))
		 (self-address!!)
		 (the (field-pvar *log-number-of-processors-limit*)
		      (!! (- nodes-end 1))))
      (*set $node? t!!))
    (array-to-pvar
      initial-x $node-x
      :cube-address-start nodes-start :cube-address-end nodes-end)
    (array-to-pvar
      initial-y $node-y
      :cube-address-start nodes-start :cube-address-end nodes-end)

    ;; assign processors to edges
    (dolist (edge edges)
      (let ((node0 (edge-node0 edge))
	    (node1 (edge-node1 edge))
	    (p (allocate-processor-cube-address edge)))
	(*setf (pref $edge? p) t)
	(*setf (pref $edge-node0 p) (processor-cube-address node0))
	(*setf (pref $edge-node1 p) (processor-cube-address node1))
	(let* ((l (funcall dist-fn node0 node1))
	       (w (funcall weight-fn node0 node1 l)))
	  (*setf (pref $edge-length p) l)
	  (*setf (pref $edge-weight p) w))))
  
    (values (cons nodes-start nodes-end))))

;;;------------------------------------------------------------------

(*defun *build-ordered-graph ( edges nodes dist-fn weight-fn order-fn
			      initial-x initial-y
			      $node? $node-x $node-y
			      $edge? $edge-node0 $edge-node1
			      $edge-length $edge-weight $edge-order)
  (declare (type boolean-pvar $node? $edge?)
	   (type float-pvar
		 $node-x $node-y $edge-length $edge-weight $edge-order)
	   (type (field-pvar *log-number-of-processors-limit*)
		 $edge-node0 $edge-node1))
  (*initialize-graph-processors $node? $edge?)
  ;; assign processors to nodes
  (multiple-value-bind
    (nodes-start nodes-end) (allocate-processor-cube-addresses nodes)
    (*when (<=!! (the (field-pvar *log-number-of-processors-limit*)
		      (!! nodes-start))
		 (self-address!!)
		 (the (field-pvar *log-number-of-processors-limit*)
		      (!! (- nodes-end 1))))
      (*set $node? t!!))
    (array-to-pvar
      initial-x $node-x
      :cube-address-start nodes-start :cube-address-end nodes-end)
    (array-to-pvar
      initial-y $node-y
      :cube-address-start nodes-start :cube-address-end nodes-end)
    ;; assign processors to edges
    (dolist (edge edges)
      (let ((node0 (edge-node0 edge))
	    (node1 (edge-node1 edge))
	    (p (allocate-processor-cube-address edge)))
	(*setf (pref $edge? p) t)
	(*setf (pref $edge-node0 p) (processor-cube-address node0))
	(*setf (pref $edge-node1 p) (processor-cube-address node1))
	(let* ((l (funcall dist-fn node0 node1))
	       (o (funcall order-fn node0 node1))
	       (w (funcall weight-fn node0 node1 l)))
	  (*setf (pref $edge-length p) l)
	  (*setf (pref $edge-order p) o)
	  (*setf (pref $edge-weight p) w))))
    (values (cons nodes-start nodes-end))))

;;;------------------------------------------------------------------
#-lucid
(*defun *plot-gradient-step ( $node-x $node-y $node-grad-x $node-grad-y
			     nodes-start nodes-end
			     step-size 
			     nodes x-coords y-coords x-step y-step
			     &key
			     (canvas *canvas*)
			     (plot *plot*)
			     (plot-fn #'plot-xy))
  
  (declare (type float-pvar $node-x $node-y $node-grad-x $node-grad-y))

  (pvar-to-array
    $node-x x-coords
    :cube-address-start nodes-start :cube-address-end nodes-end)
  (pvar-to-array
    $node-y y-coords 
    :cube-address-start nodes-start :cube-address-end nodes-end)

  (*let (($step-size (the float-pvar (!! step-size))))
    (declare (type float-pvar $step-size))
    (pvar-to-array
      (*!! $step-size $node-grad-x) x-step
      :cube-address-start nodes-start :cube-address-end nodes-end)
    (pvar-to-array
      (*!! $step-size $node-grad-y) y-step
      :cube-address-start nodes-start :cube-address-end nodes-end))

  (funcall plot-fn plot canvas nodes x-coords y-coords x-step y-step))

;;;------------------------------------------------------------------

(*defun *take-gradient-step (step-size
			      $node? $node-x $node-y $node-grad-x $node-grad-y)

  (declare (type boolean-pvar $node?)
	   (type float-pvar
		 $node-x $node-y $node-grad-x $node-grad-y
		 $new-node-x $new-node-y))

  (*when $node?
    (*let (($step-size (the float-pvar (!! step-size))))
      (declare (type float-pvar $step-size))
      (*set $node-x (+!! $node-x (*!! $step-size $node-grad-x)))
      (*set $node-y (+!! $node-y (*!! $step-size $node-grad-y))))))

(*defun *try-gradient-step (step-size
			      $node? $node-x $node-y $node-grad-x $node-grad-y
			      $new-node-x $new-node-y)

  (declare (type boolean-pvar $node?)
	   (type float-pvar
		 $node-x $node-y $node-grad-x $node-grad-y
		 $new-node-x $new-node-y))

  (*when $node?
    (*let (($step-size (the float-pvar (!! step-size))))
      (declare (type float-pvar $step-size))
      (*set $new-node-x (+!! $node-x (*!! $step-size $node-grad-x)))
      (*set $new-node-y (+!! $node-y (*!! $step-size $node-grad-y))))))

(*defun *clear-nodes ($node? &rest pvars)
  (declare (type boolean-pvar $node?))
  (*when $node?
    (dolist ($pvar pvars)
      (*set (the float-pvar $pvar) (!! 0.0)))))
                            

