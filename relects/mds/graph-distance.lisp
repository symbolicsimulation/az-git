;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: (Cm-user :use (Lisp *Lisp)); -*-

(in-package "CM-USER" :use '("LISP" "*LISP")) 

;;; Based on sample program from cm-user manual for finding
;;; the shortest path between two nodes in a undirected graph

;;;------------------------------------------------------------------
;;; global  pvars

(*defvar $node? nil "True when processor contains a node.")
(*defvar $edge? nil "True when processor contains an edge.")
(*defvar $node-name nil "The name fo the node for each processor.")
(*defvar $distance-from-start nil "Distance of a node from the start.")
(*defvar $node0-name nil)
(*defvar $node1-name nil)
(*defvar $node0 nil "One of the nodes in a edge.")
(*defvar $node1 nil "The other node in a edge.")
(*defvar $distance nil "The distance between connected cities.")

;;;------------------------------------------------------------------

(defvar *p-table* (make-hash-table))
(defvar *next-free-p* 0 "Used to allocate processors.")

;;;------------------------------------------------------------------

(defun graph-distance (start stop graph)
  (build-graph graph)
  (find-shortest-path start)
  (format *standard-output*
	  "~% The distance from ~a to ~a is ~d."
	  start
	  stop
	  (pref $distance-from-start (gethash stop *p-table*))))

;;;------------------------------------------------------------------

(defun allocate-processor () (prog1 *next-free-p* (incf *next-free-p*)))

(defun initialize-graph-processors ()
  ;; allocate more processors than we will need
  (*cold-boot :initial-dimensions '(128 1))
  ;; initialize all the processors in the machine
  (*set $node? nil!!)
  (*set $edge? nil!!)
  (clrhash *p-table*)
  (setf *next-free-p* 0))

;;;------------------------------------------------------------------

(defun collect-nodes (graph)
  (delete-duplicates
    (mapcan #'(lambda (node-edges)
		(cons (first node-edges) (mapcar #'first (rest node-edges))))
	    graph)))

;;;------------------------------------------------------------------

(defun build-graph (graph)
  
  (initialize-graph-processors)
  
  ;; assign processors to nodes
  (dolist (node (collect-nodes graph))
    (let ((p (allocate-processor)))
      (setf (gethash node *p-table*) p)
      (*setf (pref $node? p) t)
      (*setf (pref $node-name p) node)))
  
  ;; assign processors to edges
  (dolist (node-edges graph)
    (let ((node0 (first node-edges)))
      (dolist (edge (rest node-edges))
	(let ((node1 (first edge))
	      (distance (second edge))
	      (p (allocate-processor)))
	  (*setf (pref $edge? p) t)
	  (*setf (pref $distance p) distance)
	  (*setf (pref $node0 p) (gethash node0 *p-table*))
	  (*setf (pref $node1 p) (gethash node1 *p-table*))
	  (*setf (pref $node0-name p) node0)
	  (*setf (pref $node1-name p) node1))))))

;;;------------------------------------------------------------------

(defun find-shortest-path (start)
  (let ((start-processor (gethash start *p-table*))
	(any-new-distance-shorter? t))
    
    (*when $node? (*set $distance-from-start (!! 30000)))
    (*setf (pref $distance-from-start start-processor) 0)
    
    (*let (($new-distance $distance-from-start))
      (loop
	(*when $edge?
	  (*let (($distance-of-node
		   (+!! $distance (pref!! $distance-from-start $node0))))
	    (*pset :min $distance-of-node $new-distance $node1)))
	
	(*when $node?
	  (setf any-new-distance-shorter?
		(*or (<!! $new-distance $distance-from-start)))
	  (*set $distance-from-start
		(min!! $distance-from-start $new-distance)))
	
	(unless any-new-distance-shorter? (return nil))))))

;;;------------------------------------------------------------------
;;;
;;; an example:
;;;

(defparameter *city-graph*
	      '((New-York (Boston 220) (Washington 500))
		(Los-Angeles (San-Francisco 600) (Washington 2500))
		(Boston (New-York 220) (Washington 600) (Chicago 1000))
		(Washington (New-York 500) (Boston 600)
			    (Miami 1000) (Los-Angeles 2500))
		(San-Francisco (Chicago 1500) (Los-Angeles 600))
		(Miami (Chicago 2500))
		(Chicago (Boston 1000) (San-Francisco 1500) (Miami 2500))))
