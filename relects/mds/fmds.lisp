;;;-*- Package: FRIEDMAN-MDS; Syntax: Common-Lisp; Base: 10 -*-


;;; File converted on 15-Mar-88 14:06:38 from source friedman-mds
;;; Original source {qv}<pedersen>lisp>friedman-mds.;11 created  1-Mar-88 17:46:08

;;; Copyright (c) 1988 by Xerox Corporation


(provide "FRIEDMAN-MDS")

(in-package "FRIEDMAN-MDS" :use '("LISP") :nicknames '("FMDS"))

;;; Shadow, Export, Require, Use-package, and Import forms should follow here



;; Translated (il:files il:minimal-spanning-tree) to require forms

(require "MINIMAL-SPANNING-TREE")
(eval-when (load)
       
       ;; Translated (il:files il:plot) to require forms

       (require "PLOT"))

(defun triangulate (p0 p1 l m &optional p-other d-other allow-violation)
   
   ;; p0 and p1 are mapped points (x, y) pairs. L and m are distances from p0 and p1 respectively to
   ;; a third (not yet mapped) point, p2. P-OTHER is another mapped point used to resolve the
   ;; positional ambiguity. Returns two values. The first value is an (x, y) pair for the unmapped
   ;; point, p2, that perserves the distances l and m, and minimizes the difference between || p2 -
   ;; p-other|| and d-other. The second value returned is an indicator of whether the triangle
   ;; inequality was violated by l, m and d.

   (macrolet ((x (position)
                 `(car ,position))
              (y (position)
                 `(cdr ,position)))
          ;; First Compute the result  in the rotated and translated  coordinate system where p0 ->
          ;; (0 , 0) and p2 -> (d , 0).

          (let* ((x0 (x p0))
                 (y0 (y p0))
                 (x1 (x p1))
                 (y1 (y p1))
                 ;; Distance between p0 and p1

                 (d (l2-norm x0 y0 x1 y1))
                 ;; Rotation Coefficients

                 (c (/ (- x1 x0)
                       d))
                 (s (/ (- y1 y0)
                       d))
                 x2-r y2-r)
                ;; Check for violations of theTriangle inequality. 

                (if (or (> l (+ d m))
                        (> m (+ d l))
                        (> d (+ l m)))
                    (if allow-violation 
                        ;;  Resolve  violations by adjusting  m. Do the rest of the computation
                        ;; exactly to avoid passing a small negative number to sqrt, due to round
                        ;; off error.

                        (setq m (+ d l)
                              x2-r
                              (- l)
                              y2-r 0.0)
                        (error "Unexpected violation of the triangle inequality"))
                    ;; Implicitly x0-r = 0 , y0-r =0, x1-r = d and y1-r =0.

                    (setq x2-r (/ (- (+ (* l l)
                                        (* d d))
                                     (* m m))
                                  (* 2 d))
                          y2-r
                          (sqrt (* (- l x2-r)
                                   (+ l x2-r)))))
                (if p-other 
                    ;; resolve sign ambiguity

                    (let* ((p-other-r (apply-rotation (apply-translation p-other (- x0)
                                                             (- y0))
                                             c
                                             (- s)))
                           (x-other-r (x p-other-r))
                           (y-other-r (y p-other-r)))
                          (if (< (abs (- d-other (l2-norm x2-r (- y2-r)
                                                        x-other-r y-other-r)))
                               (abs (- d-other (l2-norm x2-r y2-r x-other-r y-other-r))))
                              (setq y2-r (- y2-r)))))
                ;; Transform back to the original coordinate system

                (apply-translation (apply-rotation (cons x2-r y2-r)
                                          c s)
                       x0 y0))))


(defun l2-norm (x1 y1 x2 y2) (let ((d-x (- x1 x2))
                                   (d-y (- y1 y2)))
                                  (sqrt (+ (* d-x d-x)
                                           (* d-y d-y)))))


(defun apply-rotation (p c s) 
                              ;; P is an (x,y) pair. C and S are the cosine and sine respectively

                              (let ((x (car p))
                                    (y (cdr p)))
                                   (cons (- (* c x)
                                            (* s y))
                                         (+ (* s x)
                                            (* c y)))))


(defun apply-translation (p x-offset y-offset) 
                                 ;; P is an (x,y) pair. 

                                               (let ((x (car p))
                                                     (y (cdr p)))
                                                    (cons (+ x x-offset)
                                                          (+ y y-offset))))


(defun make-friedman-mds (vertices objects distance-fn)
   
;;; Algorithm taken from "Graphics for the Multivariate Two-sample Problem", J.H. Friedman and L.C.
;;; Rafsky, JASA, Vol. 76, Number 374, pgs. 277- 287, June 1981 .

;;; VERTICES is a vector of vertex objects -- the result of MST:MAKE-MINIMAL-SPANNING-TREE. OBJECTS
;;; is a vector of objects with a metric defined on them.

;;; DISTANCE-FN applied to two objects returns the distance between them. The distance function is
;;; assumed to be symmetric.

;;; Assumes that an index into VERTICES addresses an Object witht he same index in OBJECTS

;;; Returns a a vector of (x,y) pairs for every object in OBJECTS.

   (if (not (eq (length vertices)
                (length objects)))
       (error "Invalid arguments: ~s ~s" vertices objects))
   (let* ((points (make-array (length vertices)))
          (center-index 
                 ;; Find the vertex with most daughters

                 (let ((max-connections 0)
                       max-index)
                      (dotimes (i (length vertices)
                                  max-index)
                             (let* ((size (length (mst:vertex-connections (aref vertices i)))))
                                   (when (>= size max-connections)
                                         (setq max-connections size max-index i))))))
          (center-connections (mst:vertex-connections (aref vertices center-index))))
         (if (< (length center-connections)
              2)
             (error "Center vertex has < 2 daughters: ~s" (aref vertices center-index)))
         ;; prepare the spare slots

         (dotimes (i (length vertices))
                (setf (mst:vertex-spare (aref vertices i))
                      nil))
         ;; Map the depth zero points

         (let ((center-daughters (copy-list center-connections))
               max-index another-index)
              ;; Find the most distant daughter

              (setq max-index (farthest-daughter center-index center-connections vertices objects 
                                     distance-fn))
              (setq center-daughters (delete max-index center-daughters))
              ;; Pick another (arbitrary) daughter

              (setq another-index (car center-daughters))
              (setq center-daughters (cdr center-daughters))
              ;; Map the center, furthest daughter, and another (arbitrary) daughter

              (let* ((p-center (cons 0.0 0.0))
                     (p-max (cons (funcall distance-fn (aref objects center-index)
                                         (aref objects max-index))
                                  0.0))
                     (p-other (triangulate p-center p-max (funcall distance-fn (aref objects 
                                                                                     center-index)
                                                                 (aref objects another-index))
                                     (funcall distance-fn (aref objects max-index)
                                            (aref objects another-index)))))
                    (setf (aref points center-index)
                          p-center)
                    (setf (aref points max-index)
                          p-max)
                    (setf (aref points another-index)
                          p-other)
                    ;; For each remaining daughter, map it preserving distances to the center and
                    ;; the daughter furthest from the center, resolving ambiguities the another
                    ;; (arbitrary) daughter

                    (map-daughters center-daughters p-center center-index p-max max-index p-other 
                           another-index points vertices objects distance-fn)))
         ;; mark the depth zero points as mapped (and record parent relation)

         (dolist (index center-connections)
                (setf (mst:vertex-spare (aref vertices index))
                      center-index))
         ;; Mark the center as mapped

         (setf (mst:vertex-spare (aref vertices center-index))
               t)
         (map-depth>=2 center-connections points vertices objects distance-fn)
         points))


(defun map-depth>=2 (parent-indices points vertices objects distance-fn)
   
   ;; Indices is a list of vertex indices. GRAND-PARENT is the object of an (already visited) vertex.

   (let*
    ((groups-at-depth
      (mapcan #'(lambda (parent-index)
                       ;; Connected vertices not already visited

                       (let ((result (mapcan #'(lambda (daughter-index)
                                                      (let ((vertex (aref vertices daughter-index)))
                                                           (when (null (mst:vertex-spare vertex))
                                                                 (list daughter-index))))
                                            (mst:vertex-connections (aref vertices parent-index)))))
                            (if result (list (cons parent-index result))))) parent-indices))
     (indices-at-depth (mapcan #'(lambda (pair)
                                        (copy-list (cdr pair))) groups-at-depth)))
    ;; INDICES-AT-DEPTH is an assoc list of (parent . daughters)

    (loop (if (null groups-at-depth)
              (return nil))
          (let (daughters parent-index max-index another-index)
               ;; Find vertex at this depth farthest from its parent

               (let ((m-dist 0)
                     (m-index 0)
                     m-pair)
                    (dolist (pair groups-at-depth)
                           (let ((parent-index (car pair))
                                 (indices (cdr pair)))
                                (multiple-value-bind (index dist)
                                       (farthest-daughter parent-index indices vertices objects 
                                              distance-fn)
                                       (if (>= dist m-dist)
                                           (setq m-dist dist m-index index m-pair pair)))))
                    (setq parent-index (car m-pair))
                    (setq daughters (delete m-index (copy-list (cdr m-pair))))
                    (setq groups-at-depth (delete m-pair groups-at-depth))
                    (setq max-index m-index))
               ;; Find farthest point already mapped

               (setq another-index (farthest-index-already-mapped max-index vertices objects 
                                          distance-fn))
               ;; Now map max-vertex, preseving distance to parent and farthest vertex already
               ;; mapped. This may not be possible because the distance to the the farthest vertex
               ;; may violate the triangle inequality. Resolve by minimizing the discrepancy. 

               (let* ((grand-parent-index (mst:vertex-spare (aref vertices parent-index)))
                      (p-grand (aref points grand-parent-index))
                      (p-parent (aref points parent-index))
                      (p-other (aref points another-index))
                      (p-max (triangulate p-parent p-other (funcall distance-fn (aref objects 
                                                                                      parent-index)
                                                                  (aref objects max-index))
                                    (funcall distance-fn (aref objects another-index)
                                           (aref objects max-index))
                                    p-grand
                                    (funcall distance-fn (aref objects grand-parent-index)
                                           (aref objects max-index))
                                    t)))
                     (setf (aref points max-index)
                           p-max)
                     ;; For each remaining daughter, map it preserving distances to parent and max
                     ;; daughter

                     (map-daughters daughters p-parent parent-index p-max max-index p-grand 
                            grand-parent-index points vertices objects distance-fn))
               ;; Mark all mapped points (and record parent relation)

               (dolist (index daughters)
                      (setf (mst:vertex-spare (aref vertices index))
                            parent-index))
               (setf (mst:vertex-spare (aref vertices max-index))
                     parent-index)))
    (if indices-at-depth 
        ;; Recurse

        (map-depth>=2 indices-at-depth points vertices objects distance-fn))))


(defun map-daughters (indices p0 object0-index p1 object1-index p-other object-other-index points 
                            vertices objects distance-fn) (let ((object0 (aref objects object0-index)
                                                                       )
                                                                (object1 (aref objects object1-index)
                                                                       )
                                                                (object-other (aref objects 
                                                                                   object-other-index
                                                                                    ))
                                                                object)
                                                               (dolist (index indices)
                                                                      (setq object (aref objects 
                                                                                         index))
                                                                      (setf (aref points index)
                                                                            (triangulate p0 p1
                                                                                   (funcall 
                                                                                          distance-fn 
                                                                                          object0 
                                                                                          object)
                                                                                   (funcall 
                                                                                          distance-fn 
                                                                                          object1 
                                                                                          object)
                                                                                   p-other
                                                                                   (funcall 
                                                                                          distance-fn 
                                                                                         object-other 
                                                                                          object))))))


(defun farthest-index-already-mapped (object-index vertices objects distance-fn)
   (let ((object (aref objects object-index))
         (n (length vertices))
         (max-dist 0)
         (max-index 0)
         dist)
        (dotimes (i n)
               (let ((vertex (aref vertices i)))
                    (when (eq (mst:vertex-spare vertex)
                              t)
                          ;; Point already mapped

                          (setq dist (funcall distance-fn object (aref objects (mst:vertex-object
                                                                                vertex))))
                          (if (>= dist max-dist)
                              (setq max-dist dist max-index i)))))
        max-index))


(defun farthest-daughter (parent-index daughters vertices objects distance-fn)
   (let ((parent-object (aref objects parent-index))
         (max-dist 0)
         max-index)
        (dolist (index daughters)
               (let ((dist (funcall distance-fn parent-object (aref objects index))))
                    (if (>= dist max-dist)
                        (setq max-dist dist max-index index))))
        (values max-index max-dist)))


;; debugging


(defun verify-mds (points vertices objects distance-fn)
   (mst:initialize-for-walk vertices 0)
   (mst:walk-tree 0 vertices
          #'(lambda (vertex index daughters)
                   (let ((point (aref points index))
                         (object (aref objects index)))
                        (dolist (daughter daughters)
                               (let* ((daughter-point (aref points daughter))
                                      (ld (l2-norm (car point)
                                                 (cdr point)
                                                 (car daughter-point)
                                                 (cdr daughter-point)))
                                      (lm (funcall distance-fn object (aref objects daughter))))
                                     (if (not (= ld lm))
                                         (format t 
                                             "L2 distance ~s User distance ~s between points ~d ~d~%" 
                                                ld lm index daughter))))))))


;; Brower


(defvar *star* '#*(5 5)G@@@MH@@JH@@MH@@G@@@ )


(defun make-mds-browser (objects distance-fn) (let* ((n (length objects))
                                                     (vertices (mst:make-minimal-spanning-tree 
                                                                      objects distance-fn))
                                                     (points (make-friedman-mds vertices objects 
                                                                    distance-fn))
                                                     (plot (il:createplot nil nil "Mds browser")))
                                                    (dotimes (i n)
                                                           (il:plotpoint plot (aref points i)
                                                                  (concatenate 'string "Object-"
                                                                         (prin1-to-string i))
                                                                  *star* nil t))
                                                    (il:rescaleplot plot)
                                                    plot))

(eval-when (load)
       (export '(make-friedman-mds l2-norm triangulate) (find-package "FRIEDMAN-MDS")))