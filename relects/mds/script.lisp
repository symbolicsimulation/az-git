;;;-*- Package:(CM-USER :USE (LISP *LISP)); Syntax:Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;;============================================================

#+*lisp-hardware
(cm:attach (* 2 8192))
#+*lisp-hardware (cm:detach)
(*cold-boot :initial-dimensions (* 2 8192))

;;;============================================================

(multiple-value-setq (*initial-x* *initial-y*) (layout-tree *tree-object*))

(edge-mds *tree-graph* #'log-graph-distance #'standard-weight
	  :nodes *tree-object*
	  :initial-x *initial-x*
	  :initial-y *initial-y*
	  #-lucid :plot-fn #-lucid #'plot-tree-xy)


(ordered-edge-mds
  *tree-graph*
  #'graph-distance
  #'standard-weight
  #'ancestor-order
  :nodes *tree-object*
  :initial-x *initial-x*
  :initial-y *initial-y*
  #-lucid :plot-fn #-lucid #'plot-tree-xy
  :alpha 0.5)


(node-mds
  *tree-object*
  #'graph-distance
  :initial-x *initial-x*
  :initial-y *initial-y*
  #-lucid :plot-fn #-lucid #'plot-tree-xy)
