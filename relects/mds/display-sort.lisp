;;; -*- Mode: LISP; Syntax: Common-lisp; Package: (Cm-User :use (*Lisp Lisp)); -*-

(in-package (or (find-package "CM-USER")
		(make-package "CM-USER" :use '(*Lisp Lisp))))

(*defvar $pix (!! 0))
(*proclaim '(type (field-pvar 8) $pix))
(*fill-random-8-bit-pvar $pix)

;;----------------------------------------------------------------------

(defmacro *fill-screen-with-8-bit-pvar ($pix
					&key
					(buffer :green)
					(x-offset 0)
					(y-offset 0))
  `(progn
     (with-paris-from-*lisp
       (cmfb:write-always
	 *display-id* ,buffer (pvar-location ,$pix) ,x-offset ,y-offset))
     ,$pix))

(*defun *fill-screen-with-boolean-pvar ($pix &key (buffer :overlay))
  (declare (type boolean-pvar $pix))
  (with-paris-from-*lisp
    (cmfb:write-always
      *display-id* buffer (pvar-location $pix) 255 255))
  $pix)

;;----------------------------------------------------------------------

(*defun *sort-within-rows ($pix)
  (declare (type (field-pvar 8) $pix))
  (*fill-random-8-bit-pvar $pix)
  (let ((last-row-index (- (first *current-cm-configuration*) 1))
	(any-swap? t))

    (*let (($even? (evenp!! (self-address-grid!! (!! 0))))
	   ($odd?  (oddp!!  (self-address-grid!! (!! 0))))
	   ($swap? t!!))
      (declare (type boolean-pvar $even? $odd? $swap?))

      (*when (=!! (self-address-grid!! (!! 0))
		  (the (field-pvar 10) (!! last-row-index)))
	(*set $even? nil!!)
	(*set $odd? nil!!))
      (*fill-screen-with-8-bit-pvar $pix)
      (*unless (off-grid-border-relative-p!! (!! 1) (!! 0))
	(loop
	  (*when $even?
	    (*set $swap?
		  (>!! $pix (pref-grid-relative!! $pix (!! 1) (!! 0))))
	    (setf any-swap? (*or $swap?))
	    (*when $swap?
	      (*let (($temp (pref-grid-relative!! $pix (!! 1) (!! 0))))
		(declare (type (field-pvar 8) $temp))
		(*setf (pref-grid-relative!! $pix (!! 1) (!! 0)) $pix)
		(*set $pix $temp))))
	  (*when $odd?
	    (*set $swap?
		  (>!! $pix (pref-grid-relative!! $pix (!! 1) (!! 0))))
	    (setf any-swap? (or any-swap? (*or $swap?)))
	    (*when $swap?
	      (*let (($temp (pref-grid-relative!! $pix (!! 1) (!! 0))))
		(declare (type (field-pvar 8) $temp))
		(*setf (pref-grid-relative!! $pix (!! 1) (!! 0)) $pix)
		(*set $pix $temp))))
	  (*fill-screen-with-8-bit-pvar $pix)
	  (unless any-swap? (return t)))))))

;;;----------------------------------------------------------------------

(*defun *sort-within-rows-with-shuffle-address ($pix)
  (declare (type (field-pvar 8) $pix))
  (*fill-random-8-bit-pvar $pix)
  (let ((last-row-index (- (first *current-cm-configuration*) 1))
	(any-swap? t))
    (*let (($even? (evenp!! (self-address-grid!! (!! 0))))
	   ($odd?  (oddp!!  (self-address-grid!! (!! 0))))
	   ($swap? t!!)
	   ($shuffled-pix (!! 0)) ($shuffle-dest (!! 0)))
      (declare (type boolean-pvar $even? $odd? $swap?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $shuffle-dest)
	       (type (field-pvar 8) $shuffled-pix))
      (with-paris-from-*lisp
	(cmfb:my-shuffle-address (pvar-location $shuffle-dest))) 
      (*when (=!! (self-address-grid!! (!! 0))
		  (the (field-pvar 10) (!! last-row-index)))
	(*set $even? nil!!)
	(*set $odd? nil!!))
      (*pset :no-collisions $pix $shuffled-pix $shuffle-dest)
      (*fill-screen-with-shuffled-8-bit-pvar $shuffled-pix)
      (*unless (off-grid-border-relative-p!! (!! 1) (!! 0))
	(loop
	  (*when $even?
	    (*set $swap?
		  (>!! $pix (pref-grid-relative!! $pix (!! 1) (!! 0))))
	    (setf any-swap? (*or $swap?))
	    (*when $swap?
	      (*let (($temp (pref-grid-relative!! $pix (!! 1) (!! 0))))
		(declare (type (field-pvar 8) $temp))
		(*setf (pref-grid-relative!! $pix (!! 1) (!! 0)) $pix)
		(*set $pix $temp))))
	  (*when $odd?
	    (*set $swap?
		  (>!! $pix (pref-grid-relative!! $pix (!! 1) (!! 0))))
	    (setf any-swap? (or any-swap? (*or $swap?)))
	    (*when $swap?
	      (*let (($temp (pref-grid-relative!! $pix (!! 1) (!! 0))))
		(declare (type (field-pvar 8) $temp))
		(*setf (pref-grid-relative!! $pix (!! 1) (!! 0)) $pix)
		(*set $pix $temp))))
	  (*pset :no-collisions $pix $shuffled-pix $shuffle-dest)
	  (*fill-screen-with-shuffled-8-bit-pvar $shuffled-pix)
	  (unless any-swap? (return t)))))))


;;;----------------------------------------------------------------

(defmacro *fill-screen-with-shuffled-8-bit-pvar ($pix &key (buffer :green))
  `(progn
     (with-paris-from-*lisp
       (cmfb:write-preshuffled-always
	 *display-id* ,buffer (pvar-location ,$pix) 255 255))
     ,$pix))

(defmacro *shuffle-pvar ($pvar $shuffled-address)
  `(*pset :no-collisions ,$pvar ,$pvar ,$shuffled-address))

(defmacro *swap-within-rows ($even? $odd? $pix $temp
			     $prev-shuffled $next-shuffled $swap?)
  `(progn
     (*when ,$even?
       (*pset :no-collisions ,$pix ,$temp ,$prev-shuffled))
     (*when ,$odd?
       (*set ,$swap? (>!! ,$pix ,$temp))
       (*when ,$swap?
	 (*pset :no-collisions ,$pix ,$pix ,$next-shuffled)
	 (*set ,$pix ,$temp))
       (*or ,$swap?))))

;;;----------------------------------------------------------------

(*defun *sort-with-shuffled-coordinates ($pix)
  (declare (type (field-pvar 8) $pix))
  (*fill-random-8-bit-pvar $pix)
  (let ((last-row-index (- (first *current-cm-configuration*) 1))
	(any-swapped? t))
    (*let (($even? (evenp!! (self-address-grid!! (!! 0))))
	   ($odd?  (oddp!!  (self-address-grid!! (!! 0))))
	   ($swap? t!!)
	   ($self-shuffled (!! 0))
	   ($prev-shuffled (!! 0))
	   ($next-shuffled (!! 0))
	   ($temp (!! 0)))
      (declare (type boolean-pvar $even? $odd? $swap?)
	       (type (field-pvar *log-number-of-processors-limit*)
		     $self-shuffled  $prev-shuffled $next-shuffled)
	       (type (field-pvar 8) $temp))
      (with-paris-from-*lisp
	(cmfb:my-shuffle-address (pvar-location $self-shuffled))
	(cmfb:shuffle-from-x-y
	  (pvar-location $prev-shuffled)
	  (pvar-location (1-!! (self-address-grid!! (!! 0))))
	  (pvar-location (self-address-grid!! (!! 1))))
	(cmfb:shuffle-from-x-y
	  (pvar-location $next-shuffled)
	  (pvar-location (1+!! (self-address-grid!! (!! 0))))
	  (pvar-location (self-address-grid!! (!! 1)))))
      (*when (=!! (self-address-grid!! (!! 0))
		  (the (field-pvar *log-number-of-processors-limit*)
		       (!! last-row-index)))
	(*set $even? nil!!)
	(*set $odd? nil!!))
      (*shuffle-pvar $even? $self-shuffled)
      (*shuffle-pvar $odd? $self-shuffled)
      (*shuffle-pvar $pix $self-shuffled)
      (*shuffle-pvar $prev-shuffled $self-shuffled)
      (*shuffle-pvar $next-shuffled $self-shuffled)
      (*fill-screen-with-shuffled-8-bit-pvar $pix)
      (dotimes (i 512 (error "too many iterations"))
	(setf any-swapped?
	      (*swap-within-rows
		$odd? $even? $pix $temp $prev-shuffled $next-shuffled $swap?))
	(setf any-swapped?
	      (or any-swapped?
		  (*swap-within-rows
		    $even? $odd? $pix $temp
		    $prev-shuffled $next-shuffled $swap?)))
	(*fill-screen-with-shuffled-8-bit-pvar $pix)
	(unless any-swapped? (return t))))))
