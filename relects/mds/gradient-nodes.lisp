;;;-*- Package: (CM-USER :USE (LISP *LISP)); Syntax: Common-Lisp; Mode:Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;;============================================================

(*defun *nodes-compute-loss (n index f $x $y $loss $step-x $step-y)
  (declare (type float-pvar $x $y $loss $step-x $step-y))
  (*let* (($next-address
	   (mod!! (+!! (self-address!!)
		       (the (field-pvar *log-number-of-processors-limit*)
			    (!! index)))
		  (the (field-pvar *log-number-of-processors-limit*) (!! n))))
	 ($next-x (pref!! $x $next-address :collision-mode :no-collisions))
	 ($next-y (pref!! $y $next-address :collision-mode :no-collisions))
	 ($dx (-!! $x $next-x))
	 ($dy (-!! $y $next-y))
	 ($d (the float-pvar (l2-norm!! $dx $dy))))
    (declare (type (field-pvar *log-number-of-processors-limit*) $next-address)
	     (type float-pvar $next-x $next-y $dx $dy $d))
    (*let (($f (the float-pvar (aref f index))))
      (declare (type float-pvar $f))
      (*let* (($term (-!! $f $d))
	      ($term/f (/!! $term $f)))
	(declare (type float-pvar $term $term/f))
	(*set $loss (+!! $loss (/!! (*!! $term $term) $f)))
	(*set $step-x (+!! $step-x (*!! $term/f (/!! $dx $d))))
	(*set $step-y (+!! $step-y (*!! $term/f (/!! $dy $d))))))))

(*defun *nodes-compute-loss-0 (n index f $x $y $loss $step-x $step-y)
  (declare (type float-pvar $x $y $loss $step-x $step-y))
  (*let (($next-address
	   (mod!! (+!! (self-address!!)
		       (the (field-pvar *log-number-of-processors-limit*)
			    (!! index)))
		  (the (field-pvar *log-number-of-processors-limit*) (!! n))))
	 ($next-x (!! 0.0))
	 ($next-y (!! 0.0))
	 ($dx (!! 0.0))
	 ($dy (!! 0.0))
	 ($d (!! 0.0)))
    (declare (type (field-pvar *log-number-of-processors-limit*) $next-address)
	     (type float-pvar $next-x $next-y $dx $dy $d))
    (*pset :no-collisions $x $next-x $next-address)
    (*pset :no-collisions $y $next-y $next-address)
    (*set $dx (-!! $x $next-x))
    (*set $dy (-!! $y $next-y))
    (*set $d (the float-pvar (l2-norm!! $dx $dy)))
    (*let (($f (the float-pvar (aref f index))))
      (declare (type float-pvar $f))
      (*let* (($term (-!! $f $d))
	      ($term/f (/!! $term $f)))
	(declare (type float-pvar $term $term/f))
	(*set $loss (+!! $loss (/!! (*!! $term $term) $f)))
	(*set $step-x (+!! $step-x (*!! $term/f (/!! $dx $d))))
	(*set $step-y (+!! $step-y (*!! $term/f (/!! $dy $d))))))))

;;;============================================================

(defun make-distance-pvars (fn objects)
  (let* ((n (length objects))
	 (f (make-array n)))
    (do ((j 1 (1+ j))) ;; Never look at offset zero
	((>= j n))
      (let ((pvar (allocate!! (!! 0.0) nil 'float-pvar)))
	(declare (type float-pvar pvar))
	(setf (aref f j) pvar)
	(dotimes (i n)
	  (*setf (pref (the float-pvar pvar) i)
		(the single-float
		     (funcall fn
			      (aref objects i)
			      (aref objects (mod (+ i j) n))))))))
    f))

;;;============================================================

(defun node-mds (objects fn
		  &key
		  (initial-x (make-random-vector (length objects)))
		  (initial-y (make-random-vector (length objects)))
		  (step-size (/ 1.0e-1 (length objects)))
		  (max-iterations 1000)
		  #-lucid (plot-fn #'plot-xy)
		  #-lucid (plot *plot*)
		  #-lucid (canvas *canvas*))
  (let* ((n (length objects))
	 (f (make-distance-pvars fn objects)))
    (if (> n *number-of-processors-limit*)
	(*cold-boot :initial-dimensions (next-power-of-2 n))
	(*warm-boot))
    (*let (($x (!! 0.0)) ($y (!! 0.0)) ($loss (!! 0.0))
	   ($step-x (!! 0.0)) ($step-y (!! 0.0))
	   ($node? (<!! (self-address!!)
			 (the (field-pvar *log-number-of-processors-limit*)
			      (!! n)))))
      (declare (type float-pvar $x $y $loss $step-x $step-y)
	       (type boolean-pvar $node?))
      (array-to-pvar initial-x $x :cube-address-end n)
      (array-to-pvar initial-y $y :cube-address-end n)
      (let ((x-coords (make-array n)) (y-coords (make-array n))
	    (x-step (make-array n)) (y-step (make-array n)))
	(unwind-protect
	    (*when $node?
	      (dotimes (k max-iterations
			  (warn "~a iterations exceeded" max-iterations))
		(do ((i 1 (1+ i)));; Skip the zeroth iteration
		    ((>= i n))
		  (*nodes-compute-loss n i f $x $y $loss $step-x $step-y))
		(let ((current-loss (*sum $loss)))
		  (format t "~&~a loss: ~a~%" k current-loss))
		;; show the gradient step
		(pvar-to-array $x x-coords :cube-address-end n)
		(pvar-to-array $y y-coords :cube-address-end n)
		(*let (($step-size (the float-pvar (!! step-size))))
		  (declare (type float-pvar $step-size))
		  (pvar-to-array
		    (*!! $step-size $step-x) x-step :cube-address-end n)
		  (pvar-to-array
		    (*!! $step-size $step-y) y-step :cube-address-end n))
		#-lucid
		(*plot-gradient-step $x $y $step-x $step-y 0 n step-size
				     objects x-coords y-coords x-step y-step
				     :plot plot :canvas canvas
				     :plot-fn plot-fn)
		;; choose a step size
		#+symbolics
		(setq step-size 
		      (or (scl:prompt-and-accept
			    'float "New value for step-size? ")
			  step-size))
		;; Take a gradient step
		(*let (($step-size (the float-pvar (!! step-size))))
		  (declare (type float-pvar $step-size))
		  (*set $x (+!! $x (*!! $step-size $step-x)))
		  (*set $y (+!! $y (*!! $step-size $step-y))))
		(*set $loss (!! 0.0))
		(*set $step-x (!! 0.0))
		(*set $step-y (!! 0.0))))
                            
	  ;; Deallocate pvars
	  (do ((i 1 (1+ i))) ;; Never look at offset zero
	      ((>= i n))
	    (*deallocate (aref f i)))))
      (let ((x-coords (make-array n))  (y-coords (make-array n)))
	(pvar-to-array $x x-coords :cube-address-end n)
	(pvar-to-array $y y-coords :cube-address-end n)
	(values x-coords y-coords)))))


