;;;-*- Mode: Lisp; Package: MINIMAL-SPANNING-TREE; Syntax: Common-Lisp; -*-


;;; File converted on 15-Mar-88 13:27:37 from source minimal-spanning-tree
;;; Original source {qv}<pedersen>lisp>minimal-spanning-tree.;12 created  1-Mar-88 15:10:20

;;; Copyright (c) 1988 by Xerox Corporation


(provide "MINIMAL-SPANNING-TREE")

(in-package "MINIMAL-SPANNING-TREE" :use '("LISP") :nicknames '("MST"))

;;; Shadow, Export, Require, Use-package, and Import forms should follow here



(defstruct (vertex (:copier nil)) object connections spare)


(defstruct (heap-node (:copier nil)) distance < >= 
                                 ;; v1 and v2 are (fixnum) indices

                                              (v1 0 :type (unsigned-byte 16))
                                              (v2 0 :type (unsigned-byte 16)))


(defvar *cache-size* 3)


(defun make-minimal-spanning-tree (objects distance-fn)
   
;;; OBJECTS is a vector of objects with metric defined on them. DISTANCE-FN applied to two objects
;;; returns the distance between them. The distance function is assumed to be symmetric. Returns a
;;; Minimal-spanning-tree

   (if (not (vectorp objects))
       (error "Not a vector: ~s" objects))
   (let* ((n (length objects))
          (edges 0)
          (limit (1- n))
          (vertices (make-vertices n))
          (heap (make-heap n objects vertices distance-fn))
          (sets nil))
         (loop (if (eq edges limit)
                   (return nil))
               (let* ((next-node (get-smallest-from-heap heap))
                      (v1 (heap-node-v1 next-node))
                      (v1-set (get-set v1 sets))
                      (v2 (heap-node-v2 next-node))
                      (v2-set (get-set v2 sets)))
                     (when (or (null v1-set)
                               (null v2-set)
                               (not (eq v1-set v2-set)))
                           ;; accept this edge

                           (incf edges)
                           (push v1 (vertex-connections (aref vertices v2)))
                           (push v2 (vertex-connections (aref vertices v1)))
                           ;; merge sets for v1 and v2

                           (if (null v1-set)
                               (if (null v2-set)
                                   (push (list v1 v2)
                                         sets)
                                   (nconc v2-set (list v1)))
                               (if (null v2-set)
                                   (nconc v1-set (list v2))
                                   (setq sets (merge-sets v1-set v2-set sets)))))
                     (when (eq 0 (decf (vertex-spare (aref vertices v1))))
                           ;; Refill cache

                           (setq heap (update-heap (aref vertices v1)
                                             n objects distance-fn heap v1-set)))))
         vertices))


(defun make-vertices (n) (let ((vector (make-array n)))
           