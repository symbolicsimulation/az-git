;;;-*- Package:(CM-USER :USE (LISP *LISP)); Syntax: Common-Lisp; Mode: Lisp; -*-

(in-package "CM-USER" :use '("LISP" "*LISP"))

;;; ============================================================ 

;;; from Numerical Recipes in C, p. 297 


(defconstant *golden-ratio* (/ (+ 1.0 (sqrt 5.0))
                               2.0))

(defconstant *golden-fraction-0* (/ (- 3.0 (sqrt 5.0))
                                    2.0))

(defconstant *golden-fraction-1* (/ (- (sqrt 5.0)
                                       1.0)
                                    2.0))

(defconstant *tiny* 1.0E-20)

(defconstant *sqrt-single-float-epsilon* (sqrt single-float-epsilon))

(defconstant *maximun-magnification* 100.0)

(defun sign (x y) (if (>= y 0.0) (abs x) (- (abs x))))

;;; ------------------------------------------------------------  

(defun parabola-minimum (a b c fa fb fc)
  (let* ((b-a (- b a))
	 (b-c (- b c))
	 (r (* b-a (- fb fc)))
	 (q (* b-c (- fb fa)))
	 (q-r (- q r)))
    (- b (/ (- (* q b-c) (* r b-a))
	    (* 2.0 (sign (max (abs q-r) *tiny*) q-r))))))


;;;------------------------------------------------------------


(defun bracket-minimum (f a b)
  #|(declare (values a b c (f a) (f b) (f c)))|#
  (let ((fa (funcall f a))
	(fb (funcall f b)))
    ;; make sure we're going downhill
    (when (> fb fa) (rotatef a b) (rotatef fa fb))
    (let* ((c (+ b (* *golden-ratio* (- b a))))
	   (fc (funcall f c)))
      (loop
	(when (> fc fb) (return)) ;; we've bracketed a minimum
	(let* ((u (parabola-minimum a b c fa fb fc))
	       (u-lim 
		 ;; We won't go farther than this
		 (+ b (* *maximun-magnification* (- c b))))
	       fu)
	  (cond ((> (* (- b u)
		       (- u c))
		    0.0)
                                     
		 ;; Parabolic u is between b and c; try it
		 (setq fu (funcall f u))
		 (cond ((< fu fc)
                                            
			;; Got a minimum between b and c
			(shiftf a b u)
			(shiftf fa fb fu)
			(return))
		       ((> fu fb)
                                            
			;; Got a minimum between a and u
			(setq c u)
			(setq fc fu)
			(return))
		       (t 
			;; Parabolic u was no use; use default
			;; magnification
			(setq u (+ c (* *golden-ratio* (- c b))))
			(setq fu (funcall f u)))))
		((> (* (- c u)
		       (- u u-lim))
		    0.0)
                                     
		 ;; Parabolic fit is between c and its allowed limit
		 (setq fu (funcall f u))
		 (when (< fu fc)
		   (shiftf b c u (+ u (* *golden-ratio* (- u c))))
		   (shiftf fb fc fu (funcall f u))))
		((>= (* (- u u-lim)
			(- u-lim c))
		     0.0)
                                     
		 ;; Limit parabolic u to maximum allowed value
		 (setq u u-lim)
		 (setq fu (funcall f u)))
		(t 
		 ;; Reject parabolic u; use default magnification
		 (setq u (+ c (* *golden-ratio* (- c b))))
		 (setq fu (funcall f u))))
                              
	  ;; Eliminate oldest point and continue
	  (shiftf a b c u)
	  (shiftf fa fb fc fu)))
      (values a b c fa fb fc))))


;;; Debugging


(defun square (x)
       (let ((x**2 (* x x)))
            (format *standard-output* "~& x, x**2= ~f, ~f" x x**2)
            x**2))

(defun my-sin (x)
       (format t "x ~f sin(x) ~F~%" x (sin x))
       (sin x))

(defun test-bracket (n &optional (min -10.0)
		     (max 10.0))
  (let ((delta (- max min)))
    (dotimes (i n)
      (let ((a (+ min (random delta)))
	    (b (+ min (random delta))))
	(multiple-value-bind (new-a new-b new-c fa fb fc)
	    (bracket-minimum #'sin a b)
	  (unless (and (or (< new-a new-b new-c)
			   (< new-c new-b new-a))
		       (< fb fc)
		       (< fb fa))
	    (format t "~%Violation *******~%")
	    (format t "a ~f b ~f ~%" a b)
	    (format t "new-a ~f new-b ~f new-c ~f fa ~f fb ~f fc ~f ~%"
		    new-a new-b new-c fa fb fc)
	    (format t "Violation *******~%~%")))))))

(defun test-brent-1d-minimize (n &optional (min -10.0) (max 10.0))
  (let ((delta (- max min)))
    (dotimes (i n)
      (let ((a (+ min (random delta)))
	    (b (+ min (random delta))))
	(multiple-value-bind
	  (new-a new-b new-c fa fb fc) (bracket-minimum #'sin a b)
	  (unless (and (or (< new-a new-b new-c) (< new-c new-b new-a))
		       (< fb fc)
		       (< fb fa))
	    (format t "~%Violation *******~%")
	    (format t "a ~f b ~f ~%" a b)
	    (format t "new-a ~f new-b ~f new-c ~f fa ~f fb ~f fc ~f ~%"
		    new-a new-b new-c fa fb fc)
	    (format t "Violation *******~%~%"))
	(multiple-value-bind
	  (xmin fmin) (brent-1d-minimize #'sin new-a new-b new-c)
	  (format t "xmin ~f fmin ~f ~%" xmin fmin)))))))

;;; ------------------------------------------------------------


(defun golden-section-minimize (f a b c &key (fb (funcall f b))
				(tolerance *sqrt-single-float-epsilon*))
       

  #|(declare (values xmin (f xmin)))|#

  (let* ((x0 a)
	 x1 x2 (x3 c)
	 f0 f1 f2 f3)
             
    ;; make x0 to x1 the smaller segment
    (cond ((> (abs (- c b))
	      (abs (- b a)))
	   (setf x1 b)
	   (setf x2 (+ b (* *golden-fraction-0* (- c b))))
	   (setf f1 fb)
	   (setf f2 (funcall f x2)))
	  (t (setf x2 b)
	     (setf x1 (- b (* *golden-fraction-0* (- b a))))
	     (setf f1 (funcall f x1))
	     (setf f2 fb)))
    (loop 
      ;; (print (list x0 x1 x2 x3))
      (when (<= (abs (- x3 x0))
		(* tolerance (+ 1.0 (abs x1)
				(abs x2))))
	(return))
      (cond ((< f2 f1)
	     (shiftf x0 x1 x2 (+ (* *golden-fraction-1* x2)
				 (* *golden-fraction-0* x3)))
	     (shiftf f0 f1 f2 (funcall f x2)))
	    (t (shiftf x3 x2 x1 (+ (* *golden-fraction-1* x1)
				   (* *golden-fraction-0* x0)))
	       (shiftf f3 f2 f1 (funcall f x1)))))
    (if (< f1 f2)
	(values x1 f1)
	(values x2 f2))))


;;;------------------------------------------------------------

(defconstant *brent-zeps* 1.0e-10 "???")

(defun brent-1d-minimize (f ax bx cx
			  &key
			  (relative-tolerance *sqrt-single-float-epsilon*)
			  (absolute-tolerance *brent-zeps*)
			  (max-iterations 100))
  ;; returns (values xmin (f xmin))
  (let* ((e 0.0) d (a (min ax cx)) (b (max ax cx))
	 (x bx) u (v bx) (w bx) (fx (funcall f x)) fu (fv fx) (fw fx))
    (dotimes (i max-iterations (error "Too many iterations."))
      (let* ((xm (* 0.5 (+ a b)))
	     (tol1 (+ (* relative-tolerance (abs x)) absolute-tolerance)) 
	     (tol2 (* 2.0 tol1)))
	(when (<= (abs (- x xm)) (- tol2 (* 0.5 (- b a)))) (return))
	(cond ((> (abs e) tol1)
	       ;; then construct trial parabolic fit
	       (let* ((x-w (- x w)) (x-v (- x v))
		      (r (* x-w (- fx fv)))
		      (q (* x-v (- fx fw)))
		      (q-r (- q r))
		      (p (- (* q x-v) (* r x-w)))
		      (etemp e))
		 (setf q (* 2.0 q-r))
		 (when (> q 0.0) (setf p (- p)))
		 (setf q (abs q))
		 (setf e d)
		 (cond ((or (>= (abs p) (* 0.5 q etemp))
			    (<= p (* q (- a x)))
			    (>= p (* q (- b x))))
			(setf e (if (>= x xm) (- a x) (- b x)))
			(setf d (* *golden-fraction-0* e)))
		       (t ;; take the parabolic step
			(setf d (/ p q))
			(setf u (+ x d))
			(when (or (< (- u a) tol2) (< (- b u) tol2))
			  (setf d (sign tol1 (- xm x))))))))
	      (t  ;; take a golden section step
	       (setf e (if (>= x xm) (- a x) (- b x)))
	       (setf d (* *golden-fraction-0* e))))
	(setf u (if (>= (abs d) tol1) (+ x d) (+ x (sign tol1 d))))
	(setf fu (funcall f u))
	(cond ((<= fu fx)
	       (if (>= u x) (setf a x) (setf b x))
	       (shiftf v w x u)
	       (shiftf fv fw fx fu))
	      (t
	       (if (< u x) (setf a u) (setf b u))
	       (cond ((or (<= fu fw) (= w x))
		      (rotatef v w)
		      (rotatef fv fw))
		     ((or (<= fu fv) (= v x) (= v w))
		      (setf v u)
		      (setf fv fu)))))))
    (values x fx)))
  
;;;------------------------------------------------------------


(defun 1d-minimize (f a b
		    &key
		    (relative-tolerance *sqrt-single-float-epsilon*)
		    (absolute-tolerance *brent-zeps*))
  (multiple-value-bind (a b c fa fb fc) (bracket-minimum f a b)
    (declare (ignore fa fb fc))
    (brent-1d-minimize f a b c
		       :relative-tolerance relative-tolerance
		       :absolute-tolerance absolute-tolerance)))
