;;; -*- Mode: LISP; Syntax: Common-lisp; Package: (Cm-User :use (*Lisp Lisp)); -*-

(in-package (or (find-package "CM-USER")
		(make-package "CM-USER" :use '(*Lisp Lisp))))

(defvar *display-id*)

(dfs:load-file-set 'cmfb-f4307)

(defun init-cm (&key (nrows 128) (ncols 128) (half :ucc0))
  (cm:attach half)
  (*cold-boot :initial-dimensions (list nrows ncols))
  (setf *display-id* (cmfb:attach-display))
  (with-paris-from-*lisp (cmfb:initialize-display *display-id*)))

(with-paris-from-*lisp (cmfb:clear *display-id*))

(defun display-8-bit-pvar ($pvar
			   &key
			   (x-offset 0)
			   (y-offset 0)
			   (buffer-id :green))
  (*let (($bits (!! 0)))
    (declare (type (field-pvar 8) $bits))
    (when (> (*max $pvar) 255)
      (cerror "Lose the high bits"
	      "A value in ~s is too big to fit into an 8-bit pixel."
	      $pvar))
    (*set $bits $pvar)
    (cmfb:write-always *display-id*
		       buffer-id
		       (pvar-location $bits)
		       x-offset y-offset)))


(defmacro *count (&optional pvar)
  (if pvar
      `(*sum (if!! ,pvar (!! 1) (!! 0)))
      `(*sum (!! 1))))


(defun junk ()
  (*let (($pix (mod!! (self-address-grid!! (!! 0)) (!! 255))))
    (declare (type (field-pvar 8) $pix))
    (with-paris-from-*lisp
      (cmfb:write-always *display-id*
			 :green
			 (pvar-location $pix) 0 0))))


(defun fill-screen (pixel-value)
  (*let (($pix (mod!! (!! pixel-value) (!! 255))))
    (declare (type (field-pvar 8) $pix))
    (with-paris-from-*lisp
      (cmfb:write-preshuffled-always *display-id*
				     :green
				     (pvar-location $pix) 0 0))))



(*defun *fill-random-screen-0 (&optional (display-id *display-id*))
  (*let (($pix (!! 0)))
    (let ((buff (make-array 512)))
      (declare (type (field-pvar 8) $pix))
      (dotimes (i 512)
	(print i)
	(dotimes (j 512) (setf (aref buff j) (random 256)))
	(array-to-pvar buff $pix
		       :cube-address-start (* 512 i)
		       :cube-address-end (* 512 (+ i 1)))))
    (with-paris-from-*lisp
      (cmfb:write-always display-id
			 :green
			 (pvar-location $pix) 0 0))
    $pix))

(*defun *fill-random-screen-1 (&optional (display-id *display-id*))
  (*let (($pix (!! 0)))
    (declare (type (field-pvar 8) $pix))
    (with-paris-from-*lisp
      (cm:unsigned-random (pvar-location $pix) 8)
      (cmfb:write-always display-id :green (pvar-location $pix) 0 0))
    $pix))


(*defun *fill-random-screen-2 (&optional (display-id *display-id*))
  (*let (($pix (!! 0)))
    (declare (type (field-pvar 8) $pix))
    (with-paris-from-*lisp
      (cm:unsigned-random (pvar-location $pix) 8)
      (cmfb:write-preshuffled-always
	display-id :green (pvar-location $pix) 0 0))
    $pix))

(*defun *fill-random-8-bit-pvar ($pvar)
  (declare (type (field-pvar 8) $pvar))
  (with-paris-from-*lisp (cm:unsigned-random (pvar-location $pvar) 8))
  $pvar)