;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Arizona; -*-
;;;
;;; Copyright 1987. John Alan McDonald. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================
;;;
;;; This file holds some generally useful things that I
;;; think should be in the User package.
;;;
 
(export '(suppressing-redefinition-warnings
	   affix
	   make-keyword))

;;;=======================================================
;;; Symbolics specific!

#+symbolics
(defmacro suppressing-redefinition-warnings (&body body)
  `(compiler-let ((sys:inhibit-fdefine-warnings t))
     (setf sys:inhibit-fdefine-warnings t)  
     ,@body  
     (setf sys:inhibit-fdefine-warnings nil)))

#-symbolics ;; a null version
(defmacro suppressing-redefinition-warnings (&body body)
  `(compiler-let () ,@body))

;;;=======================================================
;;; Produce a new interned symbol which is the concatentation
;;; of some other symbols and strings, eg.
;;; (affix 'foo "-" 'table) --> 'foo-table.

(defun affix (&rest names)

  (declare (type list names) ;; each name is a symbol or string
	   (values names-concatenated-into-one-interned-symbol))

  (values ;; return one value
    (intern (apply #'concatenate 'string (mapcar #'string names)))))

;;;=======================================================

(defun make-keyword (s)
  (declare (type (or symbol string) s)
	   (values keyword))
  "Returns a keyword symbol whose print name is the same as s.
   Not case sensitive."

  (intern (string-upcase (string s)) 'keyword))