;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Arizona -*-
;;;
;;; Copyright 1988. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package 'Arizona)

(export '(Named-Object name
	  as exactly-as all-as all-of-type))

;;;=======================================================

(defparameter *named-object-index* (make-instance 'Index-Hash))

(defun as (type name) (look-up *named-object-index* name type))
(defun exactly-as (type name) (look-up-exact *named-object-index* name type))
(defun all-as (type name) (look-up-all *named-object-index* name type))
(defun all-of-type (type) (look-up-all-of-type *named-object-index* type))

;;;=======================================================

(defclass Named-Object ()
     ((name :type Symbol :initform nil :reader name)))

;;;-----------------------------------------------------------------

(defmethod-setf name ((self Named-Object)) (new-name) 
  
  (remove-entry! *named-object-index* (name self) self)
  (setf (slot-value self 'name)
	(cond
	  ((symbolp new-name) new-name)
	  ((stringp new-name) (intern new-name))))
  (unless (null (name self))
    (add-entry! *named-object-index* (name self) self)))

;;;-----------------------------------------------------------------

(defmethod initialize :before ((self Named-Object) init-list)

  (setf (name self) (getf init-list :name))
  )

;;;-----------------------------------------------------------------

(defmethod kill :after  ((self Named-Object))

  (remove-entry! *named-object-index* (name self) self))

;;;-----------------------------------------------------------------

(defmethod print-object ((self Named-Object) str depth)
  
  (declare (ignore depth))  

  (format str
	  "~%{~a ~a}"
	  (string-capitalize (pcl:class-name (class-of self)))
	  (name self)))


;;;=================================================================
;;;
;;; Stuff for modifying the common lisp readtable so that the printed
;;; representation of Named-Object objects: "{a-type a-name}"
;;; is also readable and returns an appropriate object.
;;; Do this by defining #\{ as a reader macro that
;;; translates "{a-type a-name}" to "(as 'a-type 'a-name)"
;;;

(defun curly-bracket-reader (stream ch)
  
  (declare (ignore ch))
  
  ;; (as type name)
  (let ((l (read-delimited-list #\} stream t)))
    
  `(as ',(first l) ',(second l))))

;;;=================================================================
;;;
;;; If you need to restore the original readtable, evaluate:
;;;
;;; (copy-readtable nil *readtable*)
;;;

(set-macro-character #\{ #'curly-bracket-reader)

(set-syntax-from-char #\} #\) )
