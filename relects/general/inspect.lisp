;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Arizona -*-
;;;
;;; Copyright 1988. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package 'Arizona)

;;;=======================================================

(defvar *inspect-stream*
	#+symbolics
	(find-if #'(lambda (w) (typep w 'dw::dynamic-lisp-listener))
		 (scl:send tv:main-screen :inferiors))
	#-symbolics
	*standard-output*)

;;;=======================================================

#+symbolics
(defun inspect-object (object &optional (str *inspect-stream*))
  (when (typep str 'tv:essential-expose) (scl:send str :expose))
  (let ((*standard-output* str))
    (declare (special *standard-output*))
    (describe object)))