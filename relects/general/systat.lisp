;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: PCL -*-
;;;
;;; Copyright 1989. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;;======================================================================

(in-package 'pcl)

(export '(systat source-bytes object-bytes source-lines source-files
		 top-level-expressions top-level-expressions-in-file))

;;;-----------------------------------------------------------------

(defun source-bytes (pathroot)
  
  (loop
    for file-plist in (fs:directory-list
			(merge-pathnames pathroot "*.lisp.newest"))
    for length-in-bytes = (getf (rest file-plist) :length-in-bytes)
    when (not (null length-in-bytes)) sum length-in-bytes))

;;;-----------------------------------------------------------------

(defun object-bytes (pathroot)
  
  (loop
    for file-plist in (fs:directory-list
			(merge-pathnames pathroot "*.bin.newest")) 
    for length-in-bytes = (getf (rest file-plist) :length-in-bytes)
    when (not (null length-in-bytes)) sum length-in-bytes))

;;;-----------------------------------------------------------------

(defun source-lines (pathroot)
  
  (loop for pathname in (directory (merge-pathnames pathroot "*.lisp.newest"))
	sum (with-open-file (file-stream pathname)
	      (loop for line = (read-line file-stream nil nil)
		    until (null line) count t))))

;;;-----------------------------------------------------------------

(defparameter *expression-types*
	      '(;; standard CL definitions
		defconstant
		define-modify-macro
		define-setf-method
		defmacro     
		defparameter
		defsetf
		defstruct
		deftype      
		defun        
		defvar
		;; CLOS definitions
		defclass
		defgeneric-options
		defgeneric-options-setf
		define-method-combination
		defmethod    
		defmethod-setf
		;; everything else at top level
		other-expression))

;;;-----------------------------------------------------------------

(defun initial-count-plist ()

  (loop for type in *expression-types*
	collect type
	collect 0))
	
;;;-----------------------------------------------------------------

(defun top-level-expressions-in-file (pathname counts)
  
  (with-open-file (file-stream pathname)
    (loop for expression = (read file-stream nil nil) until (null expression)
	  do (incf (getf counts (first expression) 0))
	  finally (return counts))))

;;;-----------------------------------------------------------------

(defun top-level-expressions (pathroot)
  
  (loop with counts = ()
	for pathname in (directory
			  (merge-pathnames pathroot "*.lisp.newest")) 
	do (setf counts (top-level-expressions-in-file pathname counts))
	finally (return counts)))

;;;-----------------------------------------------------------------

(defun source-files (pathroot)
  
  (loop for file-plist in (rest
			    (fs:directory-list
			      (merge-pathnames pathroot "*.lisp.newest"))) 
	;; the first item from fs:directory-list-list is the fs:directory-list
	for pathname = (first file-plist)
	for length-in-bytes = (getf (rest file-plist) :length-in-bytes)
	count (not (null length-in-bytes))))

;;;-----------------------------------------------------------------

(defun systat (pathroot)
  
  (let ((*print-length* nil))
    (format
      t
      "~%~
     Number of source files:    ~s ~%~
     Number of lines of source: ~s ~%~
     Number of bytes of source: ~s ~%~
     Number of bytes of binary: ~s ~%~
     Expressions: ~% ~s
    "
      (source-files pathroot)
      (source-lines pathroot)
      (source-bytes pathroot)
      (object-bytes pathroot)
      (top-level-expressions pathroot))))

;;;-----------------------------------------------------------------

(defun generic-function-nargs-histogram (pack)
  (let ((count-plist '()))
    (do-symbols (sym pack)
      (when (and (eql (symbol-package sym) (find-package 'az)) (fboundp sym))
	(let ((f (symbol-function sym)))
	  (when (typep f 'pcl:standard-generic-function)
	    (let ((meth (first (pcl:generic-function-methods f))))
	      (incf (getf count-plist (number-of-dispatching-args meth)
			  0)))))))
    count-plist))

(defun classify-generic-functions-by-nargs (pack)
  (let ((plist '()))
    (do-symbols (sym pack)
      (when (and (eql (symbol-package sym) (find-package 'az)) (fboundp sym))
	(let ((f (symbol-function sym)))
	  (when (typep f 'pcl:standard-generic-function)
	    (let* ((meth (first (pcl:generic-function-methods f)))
		   (nargs (number-of-dispatching-args meth)))
	      (push f (getf plist nargs nil)))))))
    plist))

(defun classify-methods-by-nargs (pack)
  (let ((plist '()))
    (do-symbols (sym pack)
      (when (and (eql (symbol-package sym) (find-package 'az)) (fboundp sym))
	(let ((f (symbol-function sym)))
	  (when (typep f 'pcl:standard-generic-function)
	    (let* ((meths (pcl:generic-function-methods f))
		   (nargs (number-of-dispatching-args (first meths))))
	      (setf (getf plist nargs)
		    (append (getf plist nargs nil) meths)))))))
    plist))

(defun method-nargs-histogram (pack)
  (let ((count-plist '()))
    (do-symbols (sym pack)
      (when (and (eql (symbol-package sym) (find-package pack)) (fboundp sym))
	(let ((f (symbol-function sym)))
	  (when (typep f 'pcl:standard-generic-function)
	    (let* ((meths (pcl:generic-function-methods f))
		   (n-args (number-of-dispatching-args (first meths)))
		   (n-meths (length meths)))
	      (incf (getf count-plist n-args 0) n-meths))))))
    count-plist))


(defmethod number-of-dispatching-args ((meth pcl:standard-method))
  (length (pcl:method-type-specifiers meth)))

(defun find-1-method-generic-functions (pack)
  (when (symbolp pack) (setf pack (find-package pack)))
  (assert (packagep pack))
  (let ((funs ()))
    (do-symbols (sym pack)
      (when (and (eq pack (symbol-package sym))
		 (fboundp sym)) 
	(let ((f (symbol-function sym)))
	  (when (and (typep f 'pcl:standard-generic-function)
		     (= 1 (length (pcl:generic-function-methods f))))
	    (push sym funs)))))
    funs))