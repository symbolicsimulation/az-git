;;; -*- Package: USER; Mode: LISP; Syntax: Common-lisp -*-

(lmfs:backup-dumper
  :tape-name "arizona"
  :dump-type :complete
  :pathnames '("boo:>arizona>clos>**>*.*.newest"
	       "boo:>arizona>linear-algebra>**>*.*.newest"
	       "boo:>jam>lispm-init.lisp" "boo:>jam>init-7.lisp.newest"
	       "boo:>sys>site>clos.translations.newest"
	       "boo:>sys>site>clos.system.newest"
	       "boo:>sys>site>linear-algebra.translations.newest"
	       "boo:>sys>site>linear-algebra.system.newest"))