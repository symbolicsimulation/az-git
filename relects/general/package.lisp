;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: User -*-
;;;
;;; Copyright 1987. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(eval-when (compile load eval)
  (unless (find-package "ARIZONA")
    (make-package "ARIZONA"
		  :nicknames '(az)
		  :use `(Linear-Algebra Collections PCL Basic-Math Lisp))
    (shadowing-import pcl:*pcl-shadows-of-lisp-package* 'az)))
