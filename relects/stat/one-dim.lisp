;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Arizona -*-

(defmethod mean ((s Lisp:Vector) &key (key #'identity))
  
  (cond
    ((integerp key)
     (loop with n = (length s)
	   for i from 0 below n
	   sum (aref (aref s i) key) into total
	   finally (return (/ total (float n)))))
    ((eq key #'identity)
     (loop with n = (length s)
	   for i from 0 below n
	   sum (aref s i) into total
	   finally (return (/ total (float n)))))
    (t
     (loop with n = (length s)
	   for i from 0 below n
	   sum (funcall key (aref s i)) into total
	   finally (return (/ total (float n)))))))

(defmethod mean ((s List) &key (key #'identity))
  
  (cond
    ((integerp key)
     (loop with n = (length s)
	   for x in s
	   sum (aref x key) into total
	   finally (return (/ total (float n)))))
    ((eq key #'identity)
     (loop with n = (length s)
	   for x in s
	   sum x into total
	   finally (return (/ total (float n)))))
    (t
     (loop with n = (length s)
	   for x in s
	   sum (funcall key x) into total
	   finally (return (/ total (float n)))))))

(defmethod mean ((s Collection-in-Sequence) &key (key #'identity))
  (let ((sum 0.0))
    (labels ((sum-funcall (x) (incf sum (funcall key x)))
	     (sum-aref (x) (incf sum (aref x key))))
    (map nil (if (integerp key) #'sum-aref #'sum-funcall) s)
    (/ sum (length s)))))  

;;;------------------------------------------------------------

(defmethod variance (s &key (key #'identity))
  
  (let ((mean (mean s :key key))
	(var 0.0))
    (labels ((sum-funcall (x)
	       (let ((v (funcall key x)))
		 (incf var (- (* v v) mean))))
	     (sum-aref (x)
	       (let ((v (aref x key)))
		 (incf var (- (* v v) mean)))))
      (map nil (if (integerp key) #'sum-aref #'sum-funcall) s)
      (/ var
	 (float (- (length s) 1))))))

;;;------------------------------------------------------------

(defmethod standard-deviation (s &key (key #'identity))
  
  (sqrt (variance s key)))

;;;------------------------------------------------------------

(defmethod nmedian (s &key (key #'identity))

  ;; this method is destructive!
  (sort s #'< :key key)

  (let* ((len (length s))
	 (i (floor len 2)))
    ;; <floor> rather than <ceiling>,
    ;; because indexing is zero based.
    (if (oddp len)
	(funcall key (elt s i))
	;; else
	(/ (+ (funcall key (elt s (- i 1)))
	      (funcall key (elt s i)) )
	   2.0)
	)
    )
  )

;;;------------------------------------------------------------

(defmethod median (s &key (key #'identity))
  
  ;; this method is non destructive!
  (nmedian (copy-seq s) key))

;;;------------------------------------------------------------

(defmethod nmedian-element (s &key (key #'identity))

  ;; this method is destructive!
  (sort s #'< :key key)

  (let* ((len (length s))
	 (i (floor len 2)))
    ;; <floor> rather than <ceiling>,
    ;; because indexing is zero based.
    (if (oddp len)
	(elt s i)
	;; else
	(list (elt s (- i 1)) (elt s i)) 
	)
    )
  )

;;;------------------------------------------------------------

(defmethod median-element (s &key (key #'identity))

  ;; this method is non destructive!
  (nmedian-element (copy-seq s) key))



