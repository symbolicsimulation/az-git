;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Arizona -*-
;;;
;;; Copyright 1987. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distanceribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;======================================================================

(defun mean-distance (tree1 tree2 distance)
  
  (/ (float (sum-distance tree1 tree2 distance))
     (count-leaves tree1)
     (count-leaves tree2)))

(defun sum-distance (tree1 tree2 leaf-distance)
  (if (and (atom tree1) (atom tree2))
      (funcall leaf-distance tree1 tree2)
      ;; else
      (if (atom tree1)
	  (loop for sub2 in tree2
		sum (sum-distance tree1 sub2 leaf-distance))
	  ;; else
	  (loop for sub1 in tree1
		sum (sum-distance sub1 tree2 leaf-distance)))))

(defun count-leaves (tree)
  (if (atom tree)
      1
      (loop for sub in tree sum (count-leaves sub))))
 
;;;-----------------------------------------------------------------

(defun min-distance (tree1 tree2 leaf-distance)
  
    (if (atom tree1)
	(if (atom tree2)
	    (funcall leaf-distance tree1 tree2)
	    ;; else
	    (loop for sub2 in tree2
		  minimize (min-distance tree1 sub2 leaf-distance)))
	;; else
	(loop for sub1 in tree1
	      minimize (min-distance sub1 tree2 leaf-distance))))

;;;-----------------------------------------------------------------

(defun max-distance (tree1 tree2 leaf-distance)
  (if (atom tree1)
      (if (atom tree2)
	  (funcall leaf-distance tree1 tree2)
	  ;; else
	  (loop for sub2 in tree2
		maximize (max-distance tree1 sub2 leaf-distance)))
      ;; else
      (loop for sub1 in tree1
	    maximize (max-distance sub1 tree2 leaf-distance))))

;;;-----------------------------------------------------------------

(defun closest-children (tree tree-distance leaf-distance)
  (loop with 1st = (first  tree)
	with 2nd = (second tree)
	with min-d = (funcall tree-distance 1st 2nd leaf-distance)
	for remainder on tree
	do (loop with ch1 = (first remainder)
		 for ch2 in (rest remainder)
		 for d = (funcall tree-distance ch1 ch2 leaf-distance)
		 when (< d min-d)
		   do (setf 1st ch1)
		      (setf 2nd ch2)
		      (setf min-d d))
	finally (return (values 1st 2nd))))
  
;;;-----------------------------------------------------------------
    
(defun cluster! (tree  
		 tree-distance ;; computes distance between trees
		 leaf-distance ) ;; computes distance between atoms

  (setf tree (copy-tree tree)) ;; non-destructive!
  (loop until (<= (length tree) 2)
	do (multiple-value-bind
	     (1st 2nd) (closest-children tree tree-distance leaf-distance)
	     (setf tree (delete 2nd tree))
	     (setf tree (nsubstitute (list 1st 2nd) 1st tree)))
	finally (return tree)))


;;;=================================================================
;;;
;;; Permute the leaves of a of a sub binary tree to minimize <loss-fn>.
;;; 

(defun permute-binary-tree (sub-tree whole-tree	loss-fn)
  
  (if (not (atom sub-tree))
      (loop with loss-1 and loss-2
	    initially
	      (setf loss-1 (funcall loss-fn whole-tree))
	      (swap-tree-node sub-tree)
	      (setf loss-2 (funcall loss-fn whole-tree))
	      (when (>= loss-2 loss-1)
		(rotatef (first sub-tree) (second sub-tree)))
	    do (permute-binary-tree (first sub-tree) whole-tree loss-fn)
	       (permute-binary-tree (second sub-tree) whole-tree loss-fn)
	       (setf loss-1 (funcall loss-fn whole-tree))
	       (rotatef (first sub-tree) (second sub-tree)) 
	       (setf loss-2 (funcall loss-fn whole-tree))
	    until (>= loss-2 loss-1)
	    finally (rotatef (first sub-tree) (second sub-tree)))))