;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: MRI; -*-

;;;=====================================================================

(in-package :MRI)

;;;=====================================================================

(color:make-color-screen)

(color:make-color-screen :width 1952 :height 1024)

(gray-scale-lut)
(color-cube-lut)


(defparameter channels (collect-channels
			 "boo:>jam>mri>images>pd.bin"
			 "boo:>jam>mri>images>t1.bin"
			 "boo:>jam>mri>images>t2.bin"))

(defparameter pruned-channels (remove-if #'zero-channels? channels))

(length pruned-channels)

(setf windows make-Rotated-Scatterbyte-stack 72))

(loop for w in windows
      for remaining on windows
      for theta from 0 by 5
      do
  (print (length remaining))
  (scl:send w :expose)
  (scl:send w :clear-window)
  (rotated-false-color-scatterbyte pruned-channels
				    2 3 4
				   theta
				   0 1 3 2 4
				   :window w
				   :color-fun #'user::image-pixval))

(setf w
      (false-color-scatterbyte
	pruned-channels 0 1 3 2 4 :color-fun #'mri->pixval
	:window w) )

(false-color-scatterbyte
	pruned-channels 0 1 3 2 4 :color-fun #'rgb->bright-pixval
	:window w)
(false-color-scatterbyte
	pruned-channels 0 1 3 2 4 :color-fun #'rgb->pixval
	)
(loop 
  (loop for w in windows
	do (scl:without-interrupts (scl:send w :expose))))
 


(defun channel-sorter (v0 v1) (< (aref v0 4) (aref v1 4)))

(time (setf pruned-channels (sort pruned-channels #'channel-sorter)))
(scatterbyte pruned-channels 2 3)
(setf wv
      (vector (false-color-binary-image
		"boo:>jam>mri>images>pd.bin"
		"boo:>jam>mri>images>t1.bin"
		"boo:>jam>mri>images>t2.bin")
	      (false-color-binary-scatterbyte
		"boo:>jam>mri>images>pd.bin"
		"boo:>jam>mri>images>t1.bin"
		"boo:>jam>mri>images>pd.bin"
		"boo:>jam>mri>images>t1.bin"
		"boo:>jam>mri>images>t2.bin")
	      (false-color-binary-scatterbyte
		"boo:>jam>mri>images>pd.bin"
		"boo:>jam>mri>images>t2.bin"
		"boo:>jam>mri>images>pd.bin"
		"boo:>jam>mri>images>t1.bin"
		"boo:>jam>mri>images>t2.bin")
	      (false-color-binary-scatterbyte
		"boo:>jam>mri>images>t1.bin"
		"boo:>jam>mri>images>t2.bin"
		"boo:>jam>mri>images>pd.bin"
		"boo:>jam>mri>images>t1.bin"
		"boo:>jam>mri>images>t2.bin")))

(defun flip-windows (wv)
  (let ((nw (length wv)))
  (loop
    (dotimes (i nw)
      (scl:send (aref wv i) :expose))
    (dotimes (i nw)
      (scl:send (aref wv (- nw i 1)) :expose)))))

(flip-windows wv)
      
(print wv)

(binary-scatterbyte
  "boo:>jam>mri>images>pd.bin"
  "boo:>jam>mri>images>t1.bin")

(binary-scatterbyte
  "boo:>jam>mri>images>pd.bin"
  "boo:>jam>mri>images>t2.bin")
(binary-scatterbyte
  "boo:>jam>mri>images>t1.bin"
  "boo:>jam>mri>images>t2.bin")


(defparameter w nil)
(setf w
      (display-ascii-image-file
	"boo:>jam>mri>images>pd.ascii"
	:window w))

(binify-ascii-image-file
  "boo:>jam>mri>images>pd.ascii"
  "boo:>jam>mri>images>pd.bin")

(binify-ascii-image-file
  "boo:>jam>mri>images>t1.ascii"
  "boo:>jam>mri>images>t1.bin")

(binify-ascii-image-file
  "boo:>jam>mri>images>t2.ascii"
  "boo:>jam>mri>images>t2.bin")

(binify-ascii-image-file
  "boo:>jam>mri>images>cslotpd.data"
  "boo:>jam>mri>images>cslotpd.bin")

(binify-ascii-image-file
  "boo:>jam>mri>images>cslott1.data"
  "boo:>jam>mri>images>cslott1.bin")

(binify-ascii-image-file
  "boo:>jam>mri>images>cslott2.data"
  "boo:>jam>mri>images>cslott2.bin")
 
(display-binary-image-file
  "boo:>jam>mri>images>pd.bin"
  :window w)
(display-binary-image-file
  "boo:>jam>mri>images>t1.bin"
  :window w)
(display-binary-image-file
  "boo:>jam>mri>images>t2.bin"
  :window w)

(display-binary-image-file
  "boo:>jam>mri>images>cslotpd.bin"
  :window w)
(display-binary-image-file
  "boo:>jam>mri>images>cslott1.bin"
  :window w)
(display-binary-image-file
  "boo:>jam>mri>images>cslott2.bin"
  :window w)

(display-binary-image-file
	"boo:>jam>mri>images>pd.bin")

(display-ascii-image-file
	"boo:>jam>mri>images>pd.ascii")

(setf w ())
(average-gray-binary-image
	"boo:>jam>mri>images>pd.bin"
	"boo:>jam>mri>images>t1.bin"
	"boo:>jam>mri>images>t2.bin"
	:window w)
(average-gray-binary-image
	"boo:>jam>mri>images>cslotpd.bin"
	"boo:>jam>mri>images>cslott1.bin"
	"boo:>jam>mri>images>cslott2.bin"
	:window w)
(setf w 
      (false-color-binary-image
	"boo:>jam>mri>images>pd.bin"
	"boo:>jam>mri>images>t1.bin"
	"boo:>jam>mri>images>t2.bin"
	:window w))
(setf w 
      (false-color-binary-image
	"boo:>jam>mri>images>pd.bin"
	"boo:>jam>mri>images>t2.bin"
	"boo:>jam>mri>images>t1.bin"
	:window w))

(setf w 
      (false-color-binary-image
	"boo:>jam>mri>images>t1.bin"
	"boo:>jam>mri>images>pd.bin"
	"boo:>jam>mri>images>t2.bin"
	:window w))
(setf w 
      (false-color-binary-image
	"boo:>jam>mri>images>t1.bin"
	"boo:>jam>mri>images>t2.bin"
	"boo:>jam>mri>images>pd.bin"
	:window w))

(setf w 
      (false-color-binary-image
	"boo:>jam>mri>images>t2.bin"
	"boo:>jam>mri>images>pd.bin"
	"boo:>jam>mri>images>t1.bin"
	:window w))

(setf w 
      (false-color-binary-image
	"boo:>jam>mri>images>t2.bin"
	"boo:>jam>mri>images>t1.bin"
	"boo:>jam>mri>images>pd.bin"
	:window w))

(setf w ())
(false-color-binary-image
  "boo:>jam>mri>images>cslotpd.bin"
  "boo:>jam>mri>images>cslott1.bin"
  "boo:>jam>mri>images>cslott2.bin")
(false-color-binary-image
  "boo:>jam>mri>images>cslotpd.bin"
  "boo:>jam>mri>images>cslott2.bin"
  "boo:>jam>mri>images>cslott1.bin")
(false-color-binary-image
  "boo:>jam>mri>images>cslott1.bin"
  "boo:>jam>mri>images>cslotpd.bin"
  "boo:>jam>mri>images>cslott2.bin") 
 (false-color-binary-image
  "boo:>jam>mri>images>cslott1.bin"
  "boo:>jam>mri>images>cslott2.bin"
  "boo:>jam>mri>images>cslotpd.bin")
(false-color-binary-image
  "boo:>jam>mri>images>cslott2.bin"
  "boo:>jam>mri>images>cslotpd.bin"
  "boo:>jam>mri>images>cslott1.bin")
(false-color-binary-image
  "boo:>jam>mri>images>cslott2.bin"
  "boo:>jam>mri>images>cslott1.bin"
  "boo:>jam>mri>images>cslotpd.bin")

(false-color-binary-image
  "boo:>jam>mri>images>pd.bin"
  "boo:>jam>mri>images>t1.bin"
  "boo:>jam>mri>images>t2.bin"
  :window w)
(false-color-binary-image
  "boo:>jam>mri>images>pd.bin"
  "boo:>jam>mri>images>t2.bin"
  "boo:>jam>mri>images>t1.bin"
  :window w)
(false-color-binary-image
  "boo:>jam>mri>images>t1.bin"
  "boo:>jam>mri>images>pd.bin"
  "boo:>jam>mri>images>t2.bin"
  :window w) 
(false-color-binary-image
  "boo:>jam>mri>images>t1.bin"
  "boo:>jam>mri>images>t2.bin"
  "boo:>jam>mri>images>pd.bin"
  :window w)
(false-color-binary-image
  "boo:>jam>mri>images>t2.bin"
  "boo:>jam>mri>images>pd.bin"
  "boo:>jam>mri>images>t1.bin"
  :window w)
(false-color-binary-image
  "boo:>jam>mri>images>t2.bin"
  "boo:>jam>mri>images>t1.bin"
  "boo:>jam>mri>images>pd.bin"
  :window w)


