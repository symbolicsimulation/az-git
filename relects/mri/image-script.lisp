;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; -*-

;;;=====================================================================

(in-package :cl-user)

;;;=====================================================================

(color:make-color-screen)

(setf image
      (color:make-image 'color:rgb-image
			:name :brain
			:width 256
			:height 256))




(defun load-mri-image (image r-pathname g-pathname b-pathname)
  (with-open-file (r-file r-pathname :element-type '(Unsigned-Byte 8))
    (with-open-file (g-file g-pathname :element-type '(Unsigned-Byte 8))
      (with-open-file (b-file b-pathname :element-type '(Unsigned-Byte 8))
	(let ((r (scl:send image :red))
	      (g (scl:send image :green))
	      (b (scl:send image :blue)))
	  (dotimes (j 256)
	    (dotimes (i 256)	      
	      (setf (aref r i j) (read-byte r-file))
	      (setf (aref g i j) (read-byte g-file))
	      (setf (aref b i j) (read-byte b-file))))))))) 


(load-mri-image image
		"boo:>jam>mri>images>pd.bin"
		"boo:>jam>mri>images>t1.bin"
		"boo:>jam>mri>images>t2.bin")

(load-mri-image image
		"boo:>jam>mri>images>cslotpd.bin"
		"boo:>jam>mri>images>cslott1.bin"
		"boo:>jam>mri>images>cslott2.bin")

(scl:defflavor Image-Window
	()
	(tv:window
	  tv:stream-mixin
	  tv:process-mixin)
  :gettable-instance-variables
  :settable-instance-variables
  (:default-init-plist
    :blinker-p nil
    :expose-p t
    :label nil))

(defparameter w (tv:make-window 'Image-Window
		  :blinker-p nil
		  :superior color:color-screen
		  :edges-from ':mouse
		  :minimum-width 264
		  :minimum-height 264
		  :expose-p t
		  :borders 2
		  :save-bits t))

(scl:send image :create-8b-image
	  :window w
	  :method :peano)

(color:write-image "boo:>jam>mri>images>brain.image" image)
(scl:send w :expose)


(setf rgb-brain (color:view-image w "*"
		  :clear-before-load t))

(defun image-pixval (x y r g b)
  (declare (ignore r g b))
    (aref (scl:send w :screen-array) x y))

color::
(defun coerce-screen-color-map (window color-map)
  (tv:with-prepared-sheet-or-raster (window :bits-per-pixel bits-per-pixel :screen screen)
    (when (and screen color-map)
      (when (and screen (= bits-per-pixel 8) (send screen :color-map))
	(when (symbolp color-map)
	  (setq color-map
		(or (standard-map-with-name color-map)
		    (and (boundp color-map) (symeval color-map))
		    (ferror "~A doesn't specify a color map" color-map))))
	(if (instancep color-map)
	    (send color-map ':write-screen-map screen)
	    (send screen ':new-color-map color-map))))))
