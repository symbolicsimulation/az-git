;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: MRI; -*-

;;;=====================================================================

(in-package :MRI)

;;;=====================================================================

(defun gray-scale-lut ()
  (dotimes (i 256)
    (let ((k (* i 4)))
      (scl:send color:color-screen :write-color-map-image i k k k)))
  (scl:send color:color-screen :update-color-map-from-image))

;;;=====================================================================


(defparameter *white-pixval* 255)

;(defparameter *pixval-scale* (truncate 1023 5)) 
;
;(defun rgb->pixval (r g b)
;  (+ (truncate b *pixval-scale*)
;     (* 6 (truncate g  *pixval-scale*))
;     (* 36 (truncate r  *pixval-scale*))))
;
;(defun pixval->rgb (i)
;  (check-type i (Integer 0 215))
;  (let* ((r (* (truncate i 36) *pixval-scale*))
;	 (g (* (truncate (mod i 36) 6) *pixval-scale*))
;	 (b (* (mod i 6) *pixval-scale*)))
;    (values r g b)))
	       

(defun rgb->pixval (x y r g b)
  (declare (ignore x y))
  (scl:send color:color-screen :compute-rgb-data
	    (/ r 256.0)
	    (/ g 256.0)
	    (/ b 256.0)))

(defun rgb->bright-pixval (x y r g b)
  (declare (ignore x y))
  (scl:send color:color-screen :compute-rgb-data
	    (+ (/ r 512.0) 0.5)
	    (+ (/ g 512.0) 0.5)
	    (+ (/ b 512.0) 0.5)))

(defun mri->pixval (x y t1 pd t2)
  (setf t2 (max 0 (min 255 (* 2 t2))))
  (rgb->pixval x y
	       (/ (+ (- pd t1) 128) 2.0)
	       (/ (+ (- t1 t2) 128) 2.0)
	       t2 ))

(defun pixval->rgb (i) (scl:send color:color-screen :read-color-map i))
	       
;(defun color-cube-lut ()
;  (dotimes (i 216)
;    (multiple-value-bind (r g b) (pixval->rgb i)
;      (scl:send color:color-screen :write-color-map-image i r g b)))
;  (dotimes (i 40)
;    (scl:send color:color-screen :write-color-map-image
;	      (+ i 216) 1023 1023 1023))
;  (scl:send color:color-screen :update-color-map-from-image))

(defun color-cube-lut ()
  (scl:send color:color-screen :standardize-color-map)) 

;;;=====================================================================

(scl:defflavor Image-Window
	()
	(tv:window
	  tv:stream-mixin
	  tv:process-mixin)
  :gettable-instance-variables
  :settable-instance-variables
  (:default-init-plist
    :blinker-p nil
    :expose-p t
    :label nil))

;;;---------------------------------------------------------------------

(scl:defmethod (:live? Image-Window) () (zerop (tv:sheet-dead))) 
(scl:defmethod (:dead? Image-Window) () (not (zerop (tv:sheet-dead)))) 

;;;---------------------------------------------------------------------

(defun make-image-window (&key
			   (image-width 256)
			   (image-height 256))
  (tv:make-window 'Image-Window
		  :blinker-p nil
		  :superior color:color-screen
		  :edges-from ':mouse
		  :minimum-width (+ image-width 8)
		  :minimum-height (+ image-height  8)
		  :expose-p t
		  :borders 2
		  :save-bits t))

;;;=====================================================================

(defun dead-window? (w)
  (or (null w)
      (scl:send w :dead?)))

(defun live-window? (w)
  (and (not (null w))
       (scl:send w :live?))) 

;;;=====================================================================

(scl:defflavor Scatterbyte-Window
	()
	(Image-Window)
  :gettable-instance-variables
  :settable-instance-variables
  (:default-init-plist
    :blinker-p nil
    :expose-p t
    :label nil))

;;;---------------------------------------------------------------------

(defun make-scatterbyte-window ()
  (tv:make-window 'Scatterbyte-Window
		  :blinker-p nil
		  :superior color:color-screen
		  :edges-from ':mouse
		  :minimum-width (+ 256 8)
		  :minimum-height (+ 256 8)
		  :expose-p t
		  :borders 2
		  :save-bits t))

;;;=====================================================================

(defparameter *scatterbyte-depth-buffer*
	      (make-array '(256 256) :element-type '(Unsigned-Byte 255)))

(defun clear-depth-buffer ()
  (dotimes (i 256)
    (dotimes (j 256)
      (setf (aref *scatterbyte-depth-buffer* i j) 0))))

(proclaim '(inline scatterbyte-depth))
(defun scatterbyte-depth (x y) (aref *scatterbyte-depth-buffer* x y))
(defsetf scatterbyte-depth (x y) (z)
  `(setf (aref *scatterbyte-depth-buffer* ,x ,y) ,z))
  
;;;=====================================================================

(scl:defflavor Rotated-Scatterbyte-Window
	()
	(ScatterByte-Window)
  :gettable-instance-variables
  :settable-instance-variables
  (:default-init-plist
    :blinker-p nil
    :expose-p t
    :label nil))

;;;---------------------------------------------------------------------

(defun make-Rotated-Scatterbyte-Window ()
  (tv:make-window 'Rotated-Scatterbyte-Window
			   :blinker-p nil
			   :superior color:color-screen
			   :edges-from ':mouse
			   :minimum-width (+ 256 8)
			   :minimum-height (+ 256 8)
			   :expose-p t
			   :borders 2
			   :save-bits t))


(defun make-Rotated-Scatterbyte-stack (nwindows
				       &key
				       (first-window
					 (tv:make-window
					   'Rotated-Scatterbyte-Window
					   :blinker-p nil
					   :superior color:color-screen
					   :edges-from ':mouse
					   :minimum-width (+ 256 8)
					   :minimum-height (+ 256 8)
					   :expose-p t
					   :borders 2
					   :save-bits t)))
  (let ((windows-stack ())
	(edges (multiple-value-list (scl:send first-window :edges))))
    (dotimes (i (- nwindows 1))
      (push
	(tv:make-window 'Rotated-Scatterbyte-Window
			:blinker-p nil
			:superior color:color-screen
			:edges edges
			:minimum-width (+ (ceiling (* (sqrt 2.0) 256)) 8)
			:minimum-height (+ 256 8)
			:expose-p t
			:borders 2
			:save-bits t)
	windows-stack))
    windows-stack))
    