;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: MRI; -*-

;;;=====================================================================

(in-package :MRI)

;;;=====================================================================

(defun display-ascii-image-file (pathname
				 &key
				 (image-width 256)
				 (image-height 256)
				 (window ()))
  (unless (live-window? window) (setf window (make-image-window)))
  (with-open-file (data-file pathname)
    (let* ((screen-array (scl:send window :screen-array))
	   (screen-width (scl:send window :inside-width))
	   (screen-height (scl:send window :inside-height)))
      (dotimes (j (min image-height screen-height))
	(dotimes (i (min image-width screen-width))	      
	  (setf (aref screen-array (+ j 4) (+ i 4))
		(truncate (read data-file t nil)))))))
  window)

(defun binify-ascii-image-file (inpath
				outpath
				&key
				(image-width 256)
				(image-height 256))
  (with-open-file (infile inpath
			  :direction :input)
    (with-open-file (outfile outpath
			     :direction :output
			     :if-does-not-exist :create
			     :if-exists :new-version
			     :element-type '(Unsigned-Byte 8))
      (dotimes (j image-height)
	(dotimes (i image-width)	      
	  (write-byte (truncate (read infile t nil)) outfile)))))
  outpath)

(defun display-binary-image-file (pathname
				  &key
				  (image-width 256)
				  (image-height 256)
				  (window ()))
  (unless (live-window? window) (setf window (make-image-window)))
  (with-open-file (data-file pathname
			     :direction :input
			     :element-type '(Unsigned-Byte 8))
    (let* ((screen-array (scl:send window :screen-array))
	   (screen-width (scl:send window :inside-width))
	   (screen-height (scl:send window :inside-height)))
      (dotimes (j (min image-height screen-height))
	(dotimes (i (min image-width screen-width))	      
	  (setf (aref screen-array (+ j 4) (+ i 4)) (read-byte data-file)))
	(dotimes (i (- image-width screen-width))	      
	  (read-byte data-file)))))
  window)

(defun false-color-binary-image (r-pathname g-pathname b-pathname
				 &key
				 (image-width 256)
				 (image-height 256)
				 (window ()))
  (unless (live-window? window) (setf window (make-image-window)))
  (with-open-file (r-file r-pathname
			  :direction :input
			  :element-type '(Unsigned-Byte 8))
    (with-open-file (g-file g-pathname
			    :direction :input
			    :element-type '(Unsigned-Byte 8))
      (with-open-file (b-file b-pathname
			      :direction :input
			      :element-type '(Unsigned-Byte 8))
	(let* ((screen-array (scl:send window :screen-array))
	       (screen-width (scl:send window :inside-width))
	       (screen-height (scl:send window :inside-height)))
	  (dotimes (j (min image-height screen-height))
	    (dotimes (i (min image-width screen-width))	      
	      (setf (aref screen-array (+ j 4) (+ i 4))
		    (rgb->pixval i j
				 (read-byte r-file)
				 (read-byte g-file)
				 (read-byte b-file))))
	    (dotimes (i (- image-width screen-width))	      
	      (read-byte r-file)
	      (read-byte g-file)
	      (read-byte b-file)))))))
  window)

;;;---------------------------------------------------------------------

(defun binary-scatterbyte (x-pathname y-pathname
			   &key
			   (image-width 256)
			   (image-height 256)
			   (window ()))
  (unless (live-window? window) (setf window (make-scatterbyte-window)))
  (with-open-file (x-file x-pathname
			  :direction :input
			  :element-type '(Unsigned-Byte 8))
    (with-open-file (y-file y-pathname
			    :direction :input
			    :element-type '(Unsigned-Byte 8))
      (let* ((screen-array (scl:send window :screen-array)))
	(dotimes (j image-height)
	  (dotimes (i image-width)	      
	    (setf (aref screen-array
			(+ (read-byte y-file) 4)
			(+ (read-byte x-file) 4))
		  *white-pixval*))))))
  window)

(defun false-color-binary-scatterbyte (x-pathname y-pathname
				       r-pathname g-pathname b-pathname
				       &key
				       (image-width 256) (image-height 256)
				       (window ()))
  (unless (live-window? window) (setf window (make-scatterbyte-window)))
  (with-open-file ( x-file x-pathname
		   :direction :input :element-type '(Unsigned-Byte 8))
    (with-open-file ( y-file y-pathname
		     :direction :input :element-type '(Unsigned-Byte 8))
      (with-open-file ( r-file r-pathname
		       :direction :input :element-type '(Unsigned-Byte 8))
	(with-open-file ( g-file g-pathname
			 :direction :input :element-type '(Unsigned-Byte 8))
	  (with-open-file ( b-file b-pathname
			   :direction :input :element-type '(Unsigned-Byte 8))
	    (let* ((screen-array (scl:send window :screen-array)))
	      (dotimes (j image-height)
		(dotimes (i image-width)	      
		  (setf (aref screen-array (+ (read-byte y-file) 4)
			      (+ (read-byte x-file) 4))
			(rgb->pixval i j
				     (read-byte r-file) (read-byte g-file)
				     (read-byte b-file)))))))))))
  window)

;;;---------------------------------------------------------------------

(defun average-gray-binary-image (r-pathname g-pathname b-pathname
				 &key
				 (image-width 256)
				 (image-height 256)
				 (window ()))
  (unless (live-window? window) (setf window (make-image-window)))
  (with-open-file (r-file r-pathname
			  :direction :input
			  :element-type '(Unsigned-Byte 8))
    (with-open-file (g-file g-pathname
			    :direction :input
			    :element-type '(Unsigned-Byte 8))
      (with-open-file (b-file b-pathname
			      :direction :input
			      :element-type '(Unsigned-Byte 8))
	(let* ((screen-array (scl:send window :screen-array))
	       (screen-width (scl:send window :inside-width))
	       (screen-height (scl:send window :inside-height)))
	  (dotimes (j (min image-height screen-height))
	    (dotimes (i (min image-width screen-width))	      
	      (setf (aref screen-array (+ j 4) (+ i 4))
		    (truncate (+ (read-byte r-file)
				 (read-byte g-file)
				 (read-byte b-file))
			      3)))
	    (dotimes (i (- image-width screen-width))	      
	      (read-byte r-file)
	      (read-byte g-file)
	      (read-byte b-file)))))))
  window) 

;;;=====================================================================

(defun dont-prune (x y r g b) (declare (ignore x y r g b)) nil)

(defun make-channel-vector (x y r g b)
  (let ((v (make-array 5 :element-type '(Unsigned-Byte 8))))
    (setf (aref v 0) x)
    (setf (aref v 1) y)
    (setf (aref v 2) r)
    (setf (aref v 3) g)
    (setf (aref v 4) b)
    v))
  
(defun collect-channels (r-pathname g-pathname b-pathname
			 &key
			 (image-width 256)
			 (image-height 256)
			 (prune? #'dont-prune))

  (let ((channels ()))
    (with-open-file (r-file r-pathname
			    :direction :input
			    :element-type '(Unsigned-Byte 8))
      (with-open-file (g-file g-pathname
			      :direction :input
			      :element-type '(Unsigned-Byte 8))
	(with-open-file (b-file b-pathname
				:direction :input
				:element-type '(Unsigned-Byte 8))
	  (dotimes (y image-height)
	    (dotimes (x image-width)
	      (let ((r (read-byte r-file))
		    (g (read-byte g-file))
		    (b (read-byte b-file)))
		(unless (funcall prune? x y r g b)
		  (push (make-channel-vector x y r g b) channels))))))))
    channels))

(defun zero-channels? (v)
  (and (zerop (aref v 2))
       (zerop (aref v 3))
       (zerop (aref v 4))))

;;;---------------------------------------------------------------------

(defun display-image (channels x-i y-i pixval-i
		      &key
		      (image-width 256)
		      (image-height 256)
		      (window ()))
  (unless (live-window? window) (setf window (make-image-window
					       :image-width image-width
					       :image-height image-height)))
  (let* ((screen-array (scl:send window :screen-array))
	 (screen-left (scl:send window :inside-left))
	 (screen-top (scl:send window :inside-top))
	 (screen-width (scl:send window :inside-width))
	 (screen-height (scl:send window :inside-height)))
    (dolist (channel-vector channels)
      (let ((x (aref channel-vector x-i))
	    (y (aref channel-vector y-i)))
	(when (and (< x screen-width)
		   (< y screen-height))
	  (setf (aref screen-array (+ y screen-top) (+ x screen-left))
		(aref channel-vector pixval-i))))))
  window)

;;;---------------------------------------------------------------------

(defun scatterbyte (channels x-i y-i &key (window ()))
  (unless (live-window? window)
    (setf window (make-scatterbyte-window)))
  (multiple-value-bind (screen-left screen-top screen-right screen-bottom)
      (scl:send window :inside-edges)
    (declare (ignore screen-right screen-bottom))
    (let* ((screen-array (scl:send window :screen-array))
	   (screen-width (scl:send window :inside-width))
	   (screen-height (scl:send window :inside-height))) 
      (dolist (channel-vector channels)
	(let ((x (aref channel-vector x-i))
	      (y (aref channel-vector y-i)))
	  (when (and (< x screen-width)
		     (< y screen-height))
	    (setf (aref screen-array (+ y screen-top) (+ x screen-left))
		  *white-pixval*))))))
  window)

(defun false-color-scatterbyte (channels x-i y-i sx-i sy-i r-i g-i b-i
				&key
				(window ())
				(color-fun 'rgb->pixval))
  (unless (live-window? window) (setf window (make-scatterbyte-window)))
  (multiple-value-bind (screen-left screen-top screen-right screen-bottom)
      (scl:send window :inside-edges)
    (declare (ignore screen-right screen-bottom))
    (let* ((screen-array (scl:send window :screen-array))
	   (screen-width (scl:send window :inside-width))
	   (screen-height (scl:send window :inside-height)))
      (dolist (channel-vector channels)
	(let ((x (aref channel-vector x-i))
	      (y (aref channel-vector y-i)))
	  (when (and (< x screen-width)
		     (< y screen-height))
	    (setf (aref screen-array (+ y screen-top) (+ x screen-left))
		  (funcall color-fun
			   (aref channel-vector sx-i)
			   (aref channel-vector sy-i)
			   (aref channel-vector r-i)
			   (aref channel-vector g-i)
			   (aref channel-vector b-i))))))))
  window)

;;;---------------------------------------------------------------------

(defun rotated-scatterbyte (channels ix iy iz theta
			    &key
			    (window ()))
  (setf theta (float (/ theta pi) 1.0f0))
  (unless (live-window? window)
    (setf window (make-rotated-scatterbyte-window)))
  (multiple-value-bind (screen-left screen-top screen-right screen-bottom)
      (scl:send window :inside-edges)
    (declare (ignore screen-right screen-bottom))
    (let* ((screen-array (scl:send window :screen-array))
	   (screen-width (scl:send window :inside-width))
	   (screen-height (scl:send window :inside-height))
	   (c (cos theta))
	   (s (sin theta))
	   (screen-width/2 (truncate screen-width 2)))
      (dolist (channel-vector channels)
	(let* ((x (- (aref channel-vector ix) 128))
	       (y (aref channel-vector iy))
	       (z (- (aref channel-vector iz) 128))
	       (rx (floor (+ (* c x) (* s z) screen-width/2))))
	  (when (and (< rx screen-width)
		     (< y screen-height))
	    (setf (aref screen-array (+ y screen-top) (+ rx screen-left))
		  *white-pixval*))))))
  window)

(defun rotated-false-color-scatterbyte (channels
					ix iy iz
					theta
					isx isy ir ig ib
					&key
					(color-fun 'rgb->pixval)
					(window ()))
  (setf theta (float (* (/ theta 360.0) 2.0 pi) 1.0f0))
  (unless (live-window? window)
    (setf window (make-rotated-scatterbyte-window)))
  (clear-depth-buffer)
  (multiple-value-bind (screen-left screen-top screen-right screen-bottom)
      (scl:send window :inside-edges)
    (declare (ignore screen-right screen-bottom))
    (let* ((screen-array (scl:send window :screen-array))
	   (c (/ (cos theta) (sqrt 2)))
	   (s (/ (sin theta) (sqrt 2))))
      (dolist (channel-vector channels)
	(let* ((x (- (aref channel-vector ix) 127))
	       (y (aref channel-vector iy))
	       (z (- (aref channel-vector iz) 127))
	       (rx (floor (+ (* c x) (* s z) 128)))
	       (rz (floor (+ (- (* c z) (* s x)) 128))))
	  (when (and (< rx 255)
		     (< y 255)
		     (> rz (scatterbyte-depth rx y)))
	    (setf (scatterbyte-depth rx y) rz)
	    (setf (aref screen-array (+ y screen-top) (+ rx screen-left))
		  (funcall color-fun
			   (aref channel-vector isx)
			   (aref channel-vector isy)
			   (aref channel-vector ir)
			   (aref channel-vector ig)
			   (aref channel-vector ib))))))))
  window)