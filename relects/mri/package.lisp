;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(proclaim '(optimize (safety 3)
		     (space 1)
		     (speed 1)
		     (compilation-speed 0)))

(eval-when (compile load eval)
  (unless (find-package :MRI)
    (make-package :MRI :use `(:PCL :Lisp))))
