;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; Patch-File: T;-*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

(proclaim '(optimize (safety 3) (space 1) (speed 1) (compilation-speed 0)))

;;;============================================================


(load #+symbolics "boo:>az>tools>compile-if-needed"
      #+:coral "ccl;az:tools:compile-if-needed"
      #+:excl "/belgica-2g/jam/az/tools/compile-if-needed"
      #+cymbolics "/belgica-2g/jam/az/tools/compile-if-needed")

(load #+symbolics "boo:>az>files90"
      #+:coral "ccl;az:files90"
      #+:excl "/belgica-2g/jam/az/files90"
      #+cymbolics "belgica:/belgica-2g/jam/az/files90")

;;;============================================================

(compile-all-if-needed *tools-directory* *tools-files*)

(compile-all-if-needed *basic-math-directory* *basic-math-files*) 

(defvar *load-pcl?*)
(setf *load-pcl?* (not (find-package "PCL")))

(when *load-pcl?*
  (compile-if-needed *pcl-defsys-file*))

(when *load-pcl?*
  (let (#+:coral (*warn-if-redefine-kernel* nil))
    #+:coral (declare (special *warn-if-redefine-kernel*))
    (pcl::load-pcl))) 

;; make sure that we have all tools, including abstract-proplist-object
;; hack:
#+excl (shadow '(wt::object) (find-package :wt))
(compile-all-if-needed *clos-tools-directory* *clos-tools-files*) 


(compile-all-if-needed *slate-directory* *slate-files*)

(defparameter *mri-directory* "boo:>jam>mri>")
(defparameter *mri-files* '("package"
			    "defs"
			    "image"))
(compile-all-if-needed *mri-directory* *mri-files*)


