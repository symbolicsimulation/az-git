;;;-*- Package:Layout; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Layout)

(proclaim '(optimize (safety 3) (space 1) (speed 1) (compilation-speed 0)))

;;;============================================================

(defun make-2d-circle-vectors (&optional (n 30))
  (let* ((vectors (make-array n))
	 (inc (float (/ (* 2 pi) n) 1.0e0)))
    (dotimes (i n)
      (let ((theta (* i inc)))
	(setf (aref vectors i) (vector (cos theta) (sin theta)))))
    vectors))

(defparameter *2d-circle-vectors* (make-2d-circle-vectors 32))

(defparameter *2d-circle-configuration-space*
	      (make-instance
		'MDS-Configuration-Space
		:subjects *2d-circle-vectors*
		:pairs (collect-all-unordered-pairs *2d-circle-vectors*)
		:dist-fn 'cactus:l2-dist
		:weight-fn 'standard-weight))

(defparameter *2d-circle-start* (cactus:fill-randomly!
				  (cactus:make-element
				    *2d-circle-configuration-space*)))

;;;============================================================

(defparameter *3d-circle-glyphs* nil)

(defun make-3d-circle-vectors (&optional (n 30))
  (assert (plusp n))
  (let* ((n/3 (truncate n 3))
	 (vectors (make-array (* 3 n/3)))
	 (dtheta (/ (* 2.0 (float pi 1.0)) n/3))
	 (j 0))
    (setf *3d-circle-glyphs* (make-array (* 3 n/3)))
    (dotimes (i n/3)
      (let ((theta (* i dtheta)))
	(setf (aref vectors j) (vector (+ 1.0 (cos theta)) (sin theta) 0.0))
	(setf (aref *3d-circle-glyphs* j) :ellipse)
	(incf j)))
    (dotimes (i n/3)
      (let ((theta (* (+ i 0.1) dtheta)))
	(setf (aref vectors j) (vector (cos theta) 0.0 (- (sin theta) 1.0)))
	(setf (aref *3d-circle-glyphs* j) :star)
	(incf j)))
    (dotimes (i n/3)
      (let ((theta (* (+ i 0.2) dtheta)))
	(setf (aref vectors j) (vector 0.0 (+ (cos theta) 1.0) (sin theta)))
	(setf (aref *3d-circle-glyphs* j) :cross)
	(incf j)))
    vectors))

(defparameter *3d-circle-vectors* (make-3d-circle-vectors 60))

(defparameter *3d-circle-configuration-space* 
	      (make-instance
		'MDS-Configuration-Space
		:glyphs *3d-circle-glyphs*
		:subjects *3d-circle-vectors*
		:pairs (collect-all-unordered-pairs *3d-circle-vectors*)
		:dist-fn 'cactus:l2-dist
		:weight-fn 'standard-weight))

(defparameter *3d-circle-start* (cactus:fill-randomly!
				  (cactus:make-element
				    *3d-circle-configuration-space*)))