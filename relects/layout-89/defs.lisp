;;;-*- Package: Layout; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Layout)

(proclaim '(optimize (safety 3) (space 1) (speed 1) (compilation-speed 0)))

;;;============================================================
;;; These don't really belong here:

(defun make-random-vector (n)
  (let ((v (make-array n)))
    (dotimes (i n) (setf (aref v i) (random 1.0)))
    v))

(defun make-random-positions (n)
  (let ((v (make-array n)))
    (dotimes (i n)
      (setf (aref v i) (g:make-position (random 1.0) (random 1.0))))
    v))

;;;============================================================

(defun trivial-weight (subject0 subject1 dist)
  (declare (ignore subject0 subject1 dist))
  1.0)

(defun standard-weight (subject0 subject1 dist)
  (declare (ignore subject0 subject1))
  (assert (plusp dist))
  (/ 1.0 dist))

(defun trivial-order (subject0 subject1) 
  (declare (ignore subject0 subject1))
  0.0)

(defun unit-distance (subject0 subject1) 
  (declare (ignore subject0 subject1))
  1.0)

;;;============================================================

(defparameter *debug* nil)

;;;============================================================
;;; pairs (edges)

(deftype Pair () 'cons)
(defmacro make-pair (single0 single1) `(cons ,single0 ,single1))

(defmacro pair0 (pair) `(car ,pair))
(defmacro pair1 (pair) `(cdr ,pair))

;;; note that setf's come for free (at least in Genera)

;;;============================================================

(defun collect-all-unordered-pairs (things)
  (declare (type Sequence things))
  (etypecase things
    (List
      (mapcon #'(lambda (l)
		  (let ((1st (first l)))
		    (mapcar #'(lambda (2nd) (make-pair 1st 2nd)) (rest l))))
	things))
    (Vector
      (let ((pairs ())
	    (n (length things)))
	(dotimes (i n )
	  (let ((1st (aref things i)))
	    (do ((j (+ i 1) (+ j 1)))
		((>= j n))
	      (push (make-pair 1st (aref things j)) pairs))))
	pairs))))

(defun collect-all-ordered-pairs (things)
  (declare (type Sequence things))
  (let ((pairs ()))
    (map nil #'(lambda (1st)
		 (map nil #'(lambda (2nd)
			      (unless (eq 1st 2nd)
				(push (make-pair 1st 2nd) pairs)))
		      things)) things)
    pairs))

(defun collect-subjects-from-pairs (pairs)
  (let ((subjects ()))
    (dolist (pair pairs)
      (pushnew (pair0 pair) subjects)
      (pushnew (pair1 pair) subjects))
    subjects))

;;;==================================================================
;;; map random objects to addresses, which might be either indexes
;;; into vectors or cube addresses on the CM.
;;;==================================================================

(defvar *address-table* (make-hash-table))
(defvar *next-free-address* 0)

(defun initialize-address-table ()
  (clrhash *address-table*)
  (setf *next-free-address* 0))

(defun address (object) (gethash object *address-table*)) 
(defsetf address (object) (address)
  `(setf (gethash ,object *address-table*) ,address))

(defun assign-addresses (sequence &optional (starting-address 0))
  (let ((p starting-address))
    (map nil #'(lambda (object)
		 (setf (address object) p)
		 (incf p))
	 sequence)
    (values starting-address p)))

(defun allocate-address (object)
  (prog1
    *next-free-address*
    #+(or :*lisp-simulator :*lisp-hardware)
    (when (> *next-free-address* *number-of-processors-limit*)
      (error "Allocating too many addresses."))
    (setf (address object) *next-free-address*)
    (incf *next-free-address*)))

(defun allocate-addresses (sequence)
  (let ((old-address *next-free-address*))
    #+(or :*lisp-simulator :*lisp-hardware)
    (when (> (+ *next-free-address* (length sequence))
	     *number-of-processors-limit*)
      (error "Allocating too many processors."))
    (map nil #'(lambda (object) (allocate-address object))
	 sequence)
    (values old-address *next-free-address*)))




