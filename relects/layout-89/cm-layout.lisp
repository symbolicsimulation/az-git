;;;-*- Package: Layout; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Layout)

(proclaim '(optimize (safety 3) (space 1) (speed 1) (compilation-speed 0)))

;;;============================================================
;;; CM definitions
;;;============================================================

(eval-when (compile load eval)
  (deftype CM-Address ()
    '(Field-Pvar *log-number-of-processors-limit*)))

;;;============================================================
;;; Pvar functions
;;;============================================================

(*proclaim
  '(ftype (Function (Single-Float-Pvar Single-Float-Pvar) Single-Float-Pvar)
	  l2-norm2!!))

(*defun l2-norm2!! ($x $y)
  (declare (type Single-Float-Pvar $x $y))
  (+!! (*!! $x $x) (*!! $y $y)))

;;;------------------------------------------------------------

(*proclaim
  '(ftype (Function (Single-Float-Pvar Single-Float-Pvar) Single-Float-Pvar)
	  l2-norm!!))

(*defun l2-norm!! ($x $y)
  (declare (type Single-Float-Pvar $x $y))
  (sqrt!! (+!! (*!! $x $x) (*!! $y $y))))

;;;------------------------------------------------------------

(*proclaim '(Function l2-dist!!
		      ( Single-Float-Pvar Single-Float-Pvar
		       Single-Float-Pvar Single-Float-Pvar) Single-Float-Pvar))

(*defun l2-dist!! ($x0 $y0 $x1 $y1)
  (declare (type Single-Float-Pvar $x0 $y0 $x1 $y1))
  (*let (($dx (-!! $x0 $x1))
	 ($dy (-!! $y0 $y1)))
    (l2-norm!! $dx $dy)))

;;;------------------------------------------------------------
;;; *scale! returns its result by side effect on its last argument

(*proclaim '(Function *scale! ( Single-Float Single-Float-Pvar) nil))

(*defun *scale! (a $v)
  (declare (Return-Pvar-P nil)
	   (type Single-Float a)
	   (type Single-Float-Pvar $v))
  (cond ((zerop a)
	 (*set $v (!! 0.0)))
	((/= a 1.0)
	 (*set $v (*!! (the Single-Float-Pvar (!! a)) $v))))
  nil)

;;;------------------------------------------------------------
;;; *linear-mix returns its result by side effect on
;;; its last argument

(*proclaim '(Function *linear-mix ( Single-Float Single-Float-Pvar
				   Single-Float Single-Float-Pvar
				   &key Single-Float-Pvar) nil))

(*defun *linear-mix (a0 $v0 a1 $v1
			&key
			(($vr :result)
			 (allocate!! (!! 0.0) nil 'Single-Float-Pvar)
			     result-supplied?))
	(declare (Return-Pvar-P nil)
		 (type Single-Float a0 a1)
		 (type Single-Float-Pvar $v0 $v1))
	;;(when result-supplied? (check-type $vr Single-Float-Pvar))
	(*set $vr
	      (cond
		((= 0.0 a0)
		 (if (= 1.0 a1) $v1 (*!! (the Single-Float-Pvar (!! a1)) $v1)))
		((= 0.0 a1)
		 (if (= 1.0 a0) $v0 (*!! (the Single-Float-Pvar (!! a0)) $v0)))
		((= 1.0 a0)
		 (+!! $v0
		      (if (= 1.0 a1) $v1 (*!! (the Single-Float-Pvar (!! a1)) $v1))))
		((= 1.0 a1)
		 (+!! (*!! (the Single-Float-Pvar (!! a0)) $v0) $v1))
		(t
		 (+!! (*!! (the Single-Float-Pvar (!! a0)) $v0)
		      (*!! (the Single-Float-Pvar (!! a1)) $v1)))))
	nil)

;;;============================================================
;;; deallocate pvar's in instance slots

(defun deallocate-pvar-in-slot (instance slot-name)
  (when (slot-boundp instance slot-name)
    (let ((pvar (slot-value instance slot-name)))
      (cond
	((pvarp pvar)
	 (*deallocate pvar)
	 (slot-makunbound instance slot-name))
	(t ;; else
	 (warn "Trying to *deallocate a non-pvar slot (~a of ~a is ~a)."
	       slot-name instance pvar))))))

;;;============================================================
;;; CM Stress calculator classes
;;;============================================================

(defclass CM-MDS-Stress (C1-Objective-Function)
     (;;does this processor represent a subject?
      ($subject? :type Boolean-Pvar :accessor $subject? :initarg :$subject?)

      ;; subjects get mapped to processors with cubve address
      ;; from <start> below <end>.
      (start :type CM-Address :accessor start :initarg :start)
      (end   :type CM-Address :accessor end   :initarg :end)

      ;;does this processor represent a pair of subjects?
      ($pair? :type Boolean-Pvar :accessor $pair? :initarg :$pair?)

      ;; the addresses of the processors representing the subjects
      ;; in each pair
      ($address0 :type CM-Address :accessor $address0 :initarg :$address0)
      ($address1 :type CM-Address :accessor $address1 :initarg :$address1)

      ;; abstract properties of pairs of subjects
      ($pair-dist :type Single-Float-Pvar :accessor $pair-dist)
      ;; a negative value implies this pair is ignored for MDS
      ($pair-weight :type Single-Float-Pvar :accessor $pair-weight)

      ;; some cached data for stress and gradient calculations
      ($dx :type Single-Float-Pvar :accessor $dx)
      ($dy :type Single-Float-Pvar :accessor $dy)
      ($2d :type Single-Float-Pvar :accessor $2d)
      ($dl :type Single-Float-Pvar :accessor $dl)
      ($jacobian-x :type Single-Float-Pvar :accessor $jacobian-x)
      ($jacobian-y :type Single-Float-Pvar :accessor $jacobian-y)
      ($mds-jacobian-x :type Single-Float-Pvar :accessor $mds-jacobian-x)
      ($mds-jacobian-y :type Single-Float-Pvar :accessor $mds-jacobian-y)))

;;;------------------------------------------------------------

(defparameter *cm-mds-stress* (make-instance 'CM-MDS-Stress))

;;;------------------------------------------------------------

(defmethod initialize-objective-function :after ((lc MDS-Configuration-Space)
						 (sc CM-MDS-Stress))

  ;; initialize CM
  #+*lisp-hardware
  (if (> (nprocessors lc) *number-of-processors-limit*)
      (*cold-boot :initial-dimensions (list (nprocessors lc)))
      (*warm-boot))
  #+*lisp-simulator
  ;; always cold-boot the simulator, to make it as small as possible
  (*cold-boot :initial-dimensions (list (nprocessors lc)))

  (initialize-address-table)
  ;; allocate slot pvars
  (setf ($subject? sc)    (allocate!! nil!! nil 'Boolean-Pvar))
  (setf ($pair? sc)       (allocate!! nil!! nil 'Boolean-Pvar))
  (setf ($address0 sc)    (allocate!! nil!! nil 'CM-Address))
  (setf ($address1 sc)    (allocate!! nil!! nil 'CM-Address))
  (setf ($pair-dist   sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($pair-weight sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($dx sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($dy sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($2d sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($dl sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($jacobian-x sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($jacobian-y sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($mds-jacobian-x sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($mds-jacobian-y sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))

  ;; assign processors to subjects
  (let (($subject? ($subject? sc)))
    (*locally 
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $subject?))
      (multiple-value-bind (start end) (assign-addresses (subjects lc))
	(setf (start sc) start)
	(setf (end sc) end)
	(*when (<=!! (the CM-Address (!! start))
		     (self-address!!)
		     (the CM-Address (!! (- end 1))))
	  (declare (Return-Pvar-P nil))
	  (*setf $subject? t!!)
	  nil) )))
  
  ;; assign processors to pairs
  (let (($pair? ($pair? sc))
	($address0 ($address0 sc))
	($address1 ($address1 sc))
	($pair-dist ($pair-dist sc))
	($pair-weight ($pair-weight sc))
	(dist-fn (dist-fn lc))
	(weight-fn (weight-fn lc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type CM-Address $address0 $address1)
	       (type Single-Float-Pvar $pair-dist $pair-weight))
      (dolist (pair (pairs lc))
	(let ((s0 (pair0 pair))
	      (s1 (pair1 pair))
	      (p (allocate-address pair)))
	  (*setf (pref $pair? p) t)
	  (*setf (pref $address0 p) (address s0))
	  (*setf (pref $address1 p) (address s1))
	  (let ((d (funcall dist-fn s0 s1)))
	    (*setf (pref $pair-dist p) d)
	    (*setf (pref $pair-weight p) (funcall weight-fn s0 s1 d))))))))

;;;------------------------------------------------------------

(defmethod cardinality ((sc CM-MDS-Stress))
  (- (end sc) (start sc)))

;;;------------------------------------------------------------

(defmethod nprocessors ((lc MDS-Configuration-Space))
  (next-power-of-two->= (max (length (subjects lc)) (length (pairs lc)))))

;;;------------------------------------------------------------
     
(defmethod clear-objective-function :after ((sc CM-MDS-Stress))
  (deallocate-pvar-in-slot sc '$subject?)
  (deallocate-pvar-in-slot sc '$pair?)
  (deallocate-pvar-in-slot sc '$address0)
  (deallocate-pvar-in-slot sc '$address1)
  (deallocate-pvar-in-slot sc '$pair-dist)
  (deallocate-pvar-in-slot sc '$pair-weight)
  (deallocate-pvar-in-slot sc '$dx)
  (deallocate-pvar-in-slot sc '$dy)
  (deallocate-pvar-in-slot sc '$2d)
  (deallocate-pvar-in-slot sc '$dl)
  (deallocate-pvar-in-slot sc '$jacobian-x)
  (deallocate-pvar-in-slot sc '$jacobian-y)
  (deallocate-pvar-in-slot sc '$mds-jacobian-x)
  (deallocate-pvar-in-slot sc '$mds-jacobian-y))

;;;============================================================

(defclass CM-OMDS-Stress (CM-MDS-Stress OMDS-Stress)
     (($pair-order :type Single-Float-Pvar :accessor $pair-order)
      (a :type Single-Float :accessor a	:initarg :a)
      (b :type Single-Float :accessor b	:initarg :b)
      ;; some cached data for stress and gradient calculations
      ($omds-jacobian-x :type Single-Float-Pvar :accessor $omds-jacobian-x)
      ($omds-jacobian-y :type Single-Float-Pvar :accessor $omds-jacobian-y))
  (:default-initargs :a 0.50 :b 0.50))

;;;------------------------------------------------------------

(defparameter *cm-omds-stress* (make-instance 'CM-OMDS-Stress))

;;;------------------------------------------------------------

(defmethod initialize-objective-function :after ((lc MDS-Configuration-Space)
				     (sc CM-OMDS-Stress))
  ;; need to fill in order pvar
  (setf ($pair-order sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($omds-jacobian-x sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($omds-jacobian-y sc) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (let (($pair-order ($pair-order sc))
	(order-fn (order-fn lc)))
    (*locally
      (declare (Return-Pvar-P nil) 
	       (type Single-Float-Pvar $pair-order))
      (dolist (pair (pairs lc))
	(*setf (pref $pair-order (address pair))
	       (funcall order-fn (pair0 pair) (pair1 pair))))
      sc)))

;;;------------------------------------------------------------
     
(defmethod clear-objective-function :after ((sc CM-OMDS-Stress))
  (deallocate-pvar-in-slot sc '$pair-order)
  (deallocate-pvar-in-slot sc '$omds-jacobian-x)
  (deallocate-pvar-in-slot sc '$omds-jacobian-y))

;;;============================================================

(defclass CM-ROMDS-Stress (CM-OMDS-Stress)
     ((c :type Single-Float :accessor c	:initarg :c)
      ;; some cached data for stress and gradient calculations
      ($romds-jacobian-x :type Single-Float-Pvar
			   :accessor $romds-jacobian-x)
      ($romds-jacobian-y :type Single-Float-Pvar
			   :accessor $romds-jacobian-y))
  (:default-initargs :c 0.1))

;;;------------------------------------------------------------

(defparameter *cm-ROMDS-stress*
	      (make-instance 'CM-ROMDS-Stress))

;;;------------------------------------------------------------

(defmethod initialize-objective-function :after ((lc MDS-Configuration-Space)
				     (sc CM-ROMDS-Stress))
  (setf ($romds-jacobian-x sc)
	(allocate!! (!! 0.0) nil 'Single-Float-Pvar))
  (setf ($romds-jacobian-y sc)
	(allocate!! (!! 0.0) nil 'Single-Float-Pvar)))

;;;------------------------------------------------------------

(defmethod clear-objective-function :after ((sc CM-ROMDS-Stress))
  (deallocate-pvar-in-slot sc '$romds-jacobian-x)
  (deallocate-pvar-in-slot sc '$romds-jacobian-y))


;;;============================================================
;;; State objects stored in pvars:
;;;============================================================
;;; CM-2d-Points-State vectors are represented by Single-Float-Pvar's
;;; in a contiguous range of processors, with cube addresses
;;; from <start> below <end>.

;(defclass CM-2d-Points-State (Stress-State)
;     ((objective-function :type CM-MDS-Stress 
;	      :accessor objective-function
;	      :initarg :objective-function)
;      ($x :type Single-Float-Pvar :accessor $x)
;      ($y :type Single-Float-Pvar :accessor $y)))

;;;;------------------------------------------------------------
;
;(defmethod shared-initialize :after ((st CM-2d-Points-State) slot-names
;					&rest initargs)
;  (declare (ignore slot-names initargs))
;  (unless (slot-boundp st 'objective-function)
;    (error "Stress calculator must be supplied."))
;  (setf ($x st) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
;  (setf ($y st) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
;  st)
;
;;;;------------------------------------------------------------
;
;(defmethod kill ((st CM-2d-Points-State))
;  (deallocate-pvar-in-slot st '$x)
;  (deallocate-pvar-in-slot st '$y)
;  (slot-makunbound st 'objective-function))
;
;;;;------------------------------------------------------------

;(defmethod copy ((st CM-2d-Points-State)
;                 &rest options
;		 &key (result (new-copy self) result-supplied?)
;		 &allow-other-keys)
;  (setf result (make-instance 'CM-2d-Points-State
;				:objective-function (objective-function st)))
;    (copy st :result result))

;;;;============================================================
;
;(defmethod zero! ((st CM-2d-Points-State))
;  "Zero! st destructively."
;  (let (($subject? ($subject? (objective-function st)))
;	($x ($x st))
;	($y ($y st)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $subject?)
;	       (type Single-Float-Pvar $x $y))
;      (*when $subject?
;	(declare (Return-Pvar-P nil))
;	(*set $x (!! 0.0))
;	(*set $y (!! 0.0))
;	nil)))
;  st)
;
;;;;------------------------------------------------------------
;
;(defmethod scale! ((st CM-2d-Points-State) a)
;  "Scale st destructively."
;  (let (($subject? ($subject? (objective-function st)))
;	($x ($x st))
;	($y ($y st))
;	($a (!! a)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $subject?)
;	       (type Single-Float-Pvar $x $y $a))
;      (*when $subject?
;	(declare (Return-Pvar-P nil))
;	(*set $x (*!! $a $x))
;	(*set $y (*!! $a $y))
;	nil)))
;  st)
;
;;;;------------------------------------------------------------

;(defmethod copy-to! ((st0 CM-2d-Points-State) (st1 CM-2d-Points-State)
;		     &rest options)
;  (declare (ignore options))
;  (assert (eq (objective-function st0) (objective-function st1)))
;  (let (($subject? ($subject? (objective-function st0)))
;	($x0 ($x st0)) ($y0 ($y st0))
;	($x1 ($x st1)) ($y1 ($y st1)))
;    (*locally    
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $subject?)
;	       (type Single-Float-Pvar $x1 $y1 $x0 $y0))
;      (*when $subject?
;	     (declare (Return-Pvar-P nil))
;	     (*set $x1 $x0)
;	     (*set $y1 $y0)
;	     nil)))
;  st1)

;;;;------------------------------------------------------------
;
;(defmethod move-by ((st0 CM-2d-Points-State)
;		    (st1 CM-2d-Points-State)
;		    &optional (scale 1.0))
;  (assert (eq (objective-function st0) (objective-function st1)))
;  (let (($subject0? ($subject? (objective-function st0)))
;	($x0 ($x st0)) ($y0 ($y st0))
;	($x1 ($x st1)) ($y1 ($y st1)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $subject0?)
;	       (type Single-Float-Pvar $x0 $y0 $x1 $y1))
;      (*when $subject0?
;	(declare (Return-Pvar-P nil))
;	(cond ((= scale 1.0)
;	       (*set $x0 (+!! $x0 $x1))
;	       (*set $y0 (+!! $y0 $y1))
;	       nil)
;	      (t
;	       (*let (($scale (the Single-Float-Pvar (!! scale))))
;		 (declare (Return-Pvar-P nil)
;			  (type Single-Float-Pvar $scale))
;		 (*set $x0 (+!! $x0 (*!! $scale $x1)))
;		 (*set $y0 (+!! $y0 (*!! $scale $y1)))
;		 nil))))))
;  st0)
;
;;;;------------------------------------------------------------

;(defmethod fill-with-canonical-basis-vector! ((st CM-2d-Points-State) k)
;  "Fill st with the k-th canonical basis vector."
; (multiple-value-bind (j i) (truncate k (cardinality (objective-function st))) 
;    (let ((start (start (objective-function st))))
;      (assert (< -1 i (- (end (objective-function st)) start)))
;      (assert (<= 0 j 1))
;      (zero! st)
;      (if (= j 0)
;	  (let (($x ($x st)))
;	    (*locally
;	      (declare (type Single-Float-Pvar $x))
;	      (*setf (pref $x (+ i start)) 1.0)))
;	  (let (($y ($y st)))
;	    (*locally
;	      (declare (type Single-Float-Pvar $y))
;	      (*setf (pref $y (+ i start)) 1.0))))))
;  st)

;;;============================================================

;(defmethod copy-to! ((st0 2d-Points-State) (st1 CM-2d-Points-State)
;		     &rest options)
;  (declare (ignore options))
;  (let ((s (start (objective-function st1)))
;	(e (end (objective-function st1)))
;	($subject? ($subject? (objective-function st1)))
;	($x ($x st1))
;	($y ($y st1)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Fixnum s e)
;	       (type Boolean-Pvar $subject?)
;	       (type Single-Float-Pvar $x $y))
;      (assert (= (- e s) (length (x st0))))
;      (array-to-pvar (x st0) $x :cube-address-start s :cube-address-end e)
;      (array-to-pvar (y st0) $y :cube-address-start s :cube-address-end e)))
;  st1)

;;;------------------------------------------------------------

;(defmethod copy-to! ((st0 CM-2d-Points-State) (st1 2d-Points-State)
;		     &rest options)
;  (declare (ignore options))
;  (let ((s (start (objective-function st0)))
;	(e (end (objective-function st0)))
;	($subject? ($subject? (objective-function st0)))
;	($x ($x st0))
;	($y ($y st0)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $subject?)
;	       (type Single-Float-Pvar $x $y))
;      (assert (= (- end start) (length (x st1))))
;      (*when $subject?
;	(declare (Return-Pvar-P nil))
;	(pvar-to-array $x (x st1) :cube-address-start s :cube-address-end e)
;	(pvar-to-array $y (y st1) :cube-address-start s :cube-address-end e))))
;  st1)

;;;============================================================
;;; CM-2d-Points-State vectors are represented by Single-Float-Pvar's
;;; in a contiguous range of processors, with cube addresses
;;; from <start> below <end>.

(defclass CM-2d-Point-Pairs-State (Stress-State)
     ((objective-function :type CM-MDS-Stress
			 :accessor objective-function
			 :initarg :objective-function)
      ($x0 :type Single-Float-Pvar :accessor $x0)
      ($y0 :type Single-Float-Pvar :accessor $y0)
      ($x1 :type Single-Float-Pvar :accessor $x1)
      ($y1 :type Single-Float-Pvar :accessor $y1)))

;;;------------------------------------------------------------
;;; <$pair?>, <start>, and <end> must be provided in the init list.

(defmethod shared-initialize :after ((st CM-2d-Point-Pairs-State)
				     slot-names
				     &rest initargs)
   (declare (ignore slot-names initargs))
   (unless (slot-boundp st 'objective-function)
    (error "Stress calculator must be supplied.")) 
   (setf ($x0 st) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
   (setf ($y0 st) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
   (setf ($x1 st) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
   (setf ($y1 st) (allocate!! (!! 0.0) nil 'Single-Float-Pvar))
   st)

;;;------------------------------------------------------------

(defmethod kill ((st CM-2d-Point-Pairs-State))
  (deallocate-pvar-in-slot st '$x0)
  (deallocate-pvar-in-slot st '$y0)
  (deallocate-pvar-in-slot st '$x1)
  (deallocate-pvar-in-slot st '$y1)
  (slot-makunbound st 'objective-function))

;;;------------------------------------------------------------

(defmethod new-copy ((st CM-2d-Point-Pairs-State)
			  &rest options)
  (declare (ignore options))
  (make-instance 'CM-2d-Point-Pairs-State
		 :objective-function (objective-function st)))

;;;============================================================

(defmethod zero! ((st CM-2d-Point-Pairs-State))
  "Zero! p0 destructively."
  (let (($pair? ($pair? (objective-function st)))
	($x0 ($x0 st)) ($y0 ($y0 st))
	($x1 ($x1 st)) ($y1 ($y1 st)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $x0 $y0 $x1 $y1))
      (*when $pair?
	(declare (Return-Pvar-P nil))
	(*set $x0 (!! 0.0))
	(*set $y0 (!! 0.0))
	(*set $x1 (!! 0.0))
	(*set $y1 (!! 0.0))
	nil)))
  st)

;;;------------------------------------------------------------

(defmethod scale (a (st0 CM-2d-Point-Pairs-State)
		     (st1 CM-2d-Point-Pairs-State))
  (assert (eq (objective-function st0) (objective-function st1)))
  (let (($pair? ($pair? (objective-function st0)))
	($x00 ($x0 st0)) ($y00 ($y0 st0)) ($x10 ($x1 st0)) ($y10 ($y1 st0))
	($x01 ($x0 st1)) ($y01 ($y0 st1)) ($x11 ($x1 st1)) ($y11 ($y1 st1)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar
		     $x00 $y00 $x10 $y10 $x01 $y01 $x11 $y11))
      (*let (($a (!! a)))
	(declare (Return-Pvar-P nil)
		 (type Single-Float-Pvar $a))
	(*when $pair?
	  (declare (Return-Pvar-P nil))
	  (*set $x01 (*!! $a $x00))
	  (*set $y01 (*!! $a $y00))
	  (*set $x11 (*!! $a $x10))
	  (*set $y11 (*!! $a $y10))
	  nil))))
  st1)

(defmethod scale! (a (st0 CM-2d-Point-Pairs-State))
  (let (($pair? ($pair? (objective-function st0)))
	($x0 ($x0 st0)) ($y0 ($y0 st0)) ($x1 ($x1 st0)) ($y1 ($y1 st0)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $x0 $y0 $x1 $y1))
      (*let (($a (!! a)))
	(declare (Return-Pvar-P nil)
		 (type Single-Float-Pvar $a))
	(*when $pair?
	  (declare (Return-Pvar-P nil))
	  (*set $x0 (*!! $a $x0))
	  (*set $y0 (*!! $a $y0))
	  (*set $x1 (*!! $a $x1))
	  (*set $y1 (*!! $a $y1))
	  nil))))
  st0)

;;;------------------------------------------------------------

(defmethod copy-to! ((st0 CM-2d-Point-Pairs-State)
		     (st1 CM-2d-Point-Pairs-State)
		     &rest options)
  (declare (ignore options))
  (assert (eq (objective-function st0) (objective-function st1)))
  (let (($pair? ($pair? (objective-function st0)))
	($x00 ($x0 st0)) ($y00 ($y0 st0)) ($x10 ($x1 st0)) ($y10 ($y1 st0))
	($x01 ($x0 st1)) ($y01 ($y0 st1)) ($x11 ($x1 st1)) ($y11 ($y1 st1)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar
		     $x00 $y00 $x10 $y10 $x01 $y01 $x11 $y11))
      (*when $pair?
	     (declare (Return-Pvar-P nil))
	     (*set $x01 $x00)
	     (*set $y01 $y00)
	     (*set $x11 $x10)
	     (*set $y11 $y10)
	     nil)))
  st0)

;;;------------------------------------------------------------

;(defmethod move-by ((st0 CM-2d-Point-Pairs-State)
;		    (st1 CM-2d-Point-Pairs-State)
;		    &optional (scale 1.0))
;  (assert (eq (objective-function st0) (objective-function st1)))
;  (let (($pair? ($pair? (objective-function st0)))
;	($x00 ($x0 st0)) ($y00 ($y0 st0)) ($x10 ($x1 st0)) ($y10 ($y1 st0))
;	($x01 ($x0 st1)) ($y01 ($y0 st1)) ($x11 ($x1 st1)) ($y11 ($y1 st1)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $pair?)
;	       (type Single-Float-Pvar
;		     $x00 $y00 $x10 $y10 $x01 $y01 $x11 $y11))
;      (*when $pair?
;	(declare (Return-Pvar-P nil))
;	(cond ((= scale 1.0)
;	       (*set $x00 (+!! $x00 $x01))
;	       (*set $y00 (+!! $y00 $y01))
;	       (*set $x10 (+!! $x10 $x11))
;	       (*set $y10 (+!! $y10 $y11))
;	       nil)
;	      (t
;	       (*let (($scale (the Single-Float-Pvar (!! scale))))
;		 (declare (Return-Pvar-P nil)
;			  (type Single-Float-Pvar $scale))
;		 (*set $x00 (+!! $x00 (*!! $scale $x01)))
;		 (*set $y00 (+!! $y00 (*!! $scale $y01)))
;		 (*set $x10 (+!! $x10 (*!! $scale $x11)))
;		 (*set $y10 (+!! $y10 (*!! $scale $y11)))
;		 nil))))))
;  st0)

;;;------------------------------------------------------------

;(defmethod move-from ((st0 CM-2d-Point-Pairs-State)
;		      (st1 CM-2d-Point-Pairs-State)
;		      (st2 CM-2d-Point-Pairs-State)
;		      &optional (scale 1.0))
;  (assert (and (eq (objective-function st0) (objective-function st1))
;	       (eq (objective-function st0) (objective-function st2))))
;  (let (($pair? ($pair? (objective-function st0)))
;	($x00 ($x0 st0)) ($y00 ($y0 st0)) ($x10 ($x1 st0)) ($y10 ($y1 st0))
;	($x01 ($x0 st1)) ($y01 ($y0 st1)) ($x11 ($x1 st1)) ($y11 ($y1 st1))
;	($x02 ($x0 st2)) ($y02 ($y0 st2)) ($x12 ($x1 st2)) ($y12 ($y1 st2)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $pair?)
;	       (type Single-Float-Pvar
;		     $x00 $y00 $x10 $y10
;		     $x01 $y01 $x11 $y11
;		     $x02 $y02 $x12 $y12))
;      (*when $pair?
;	(declare (Return-Pvar-P nil))
;	(cond ((= scale 1.0)
;	       (*set $x00 (+!! $x01 $x02))
;	       (*set $y00 (+!! $y01 $y02))
;	       (*set $x10 (+!! $x11 $x12))
;	       (*set $y10 (+!! $y11 $y12))
;	       nil)
;	      (t
;	       (*let (($scale (the Single-Float-Pvar (!! scale))))
;		 (declare (Return-Pvar-P nil)
;			  (type Single-Float-Pvar $scale))
;		 (*set $x00 (+!! $x01 (*!! $scale $x02)))
;		 (*set $y00 (+!! $y01 (*!! $scale $y02)))
;		 (*set $x10 (+!! $x11 (*!! $scale $x12)))
;		 (*set $y10 (+!! $y11 (*!! $scale $y12)))
;		 nil))))))
;  st0)

;;;------------------------------------------------------------

;(defmethod add! ((st0 CM-2d-Point-Pairs-State) (st1 CM-2d-Point-Pairs-State)
;		 (str CM-2d-Point-Pairs-State))
;  (assert (and (eq (objective-function st0) (objective-function st1))
;	       (eq (objective-function st0) (objective-function str))))
;  (let (($pair? ($pair? (objective-function st0)))
;	($x00 ($x0 st0)) ($y00 ($y0 st0)) ($x10 ($x1 st0)) ($y10 ($y1 st0))
;	($x01 ($x0 st1)) ($y01 ($y0 st1)) ($x11 ($x1 st1)) ($y11 ($y1 st1))
;	($x0r ($x0 str)) ($y0r ($y0 str)) ($x1r ($x1 str)) ($y1r ($y1 str)))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $pair?)
;	       (type Single-Float-Pvar
;		     $x00 $y00 $x10 $y10
;		     $x01 $y01 $x11 $y11
;		     $x0r $y0r $x1r $y1r))
;      (*when $pair?
;	(declare (Return-Pvar-P nil))
;	  (*set $x0r (+!! $x00 $x01))
;	  (*set $y0r (+!! $y00 $y01))
;	  (*set $x1r (+!! $x10 $x11))
;	  (*set $y1r (+!! $y10 $y11))
;	  nil)))
;  str)

;;;------------------------------------------------------------

(defmethod linear-mix ((a0 T) (st0 CM-2d-Point-Pairs-State)
		       (a1 T) (st1 CM-2d-Point-Pairs-State)
		       &key
		       (result (copy st0) result-supplied?))
  
  (assert (eq (objective-function st0) (objective-function st1)))
  (when result-supplied?
    (assert (eq (objective-function st0) (objective-function result))))
  (let (($pair? ($pair? (objective-function st0)))
	($x00 ($x0 st0)) ($y00 ($y0 st0)) ($x10 ($x1 st0)) ($y10 ($y1 st0))
	($x01 ($x0 st1)) ($y01 ($y0 st1)) ($x11 ($x1 st1)) ($y11 ($y1 st1))
	($x0r ($x0 result)) ($y0r ($y0 result)) ($x1r ($x1 result)) ($y1r ($y1 result)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar
		     $x00 $y00 $x10 $y10
		     $x01 $y01 $x11 $y11
		     $x0r $y0r $x1r $y1r))
      (*when $pair?
	     (declare (Return-Pvar-P nil))
	     (*linear-mix a0 $x00 a1 $x01 $x0r)
	     (*linear-mix a0 $y00 a1 $y01 $y0r)
	     (*linear-mix a0 $x10 a1 $x11 $x1r)
	     (*linear-mix a0 $y10 a1 $y11 $y1r))))
  result)

;;;------------------------------------------------------------

(defmethod fill-randomly ((st CM-2d-Point-Pairs-State)
			      &key (min -1.0) (max 1.0))
  (zero! st)
  (let (($subject? ($subject? (objective-function st)))
	($pair? ($pair? (objective-function st)))
	($x0 ($x0 st)) ($y0 ($y0 st))
	($x1 ($x1 st)) ($y1 ($y1 st))
	($a0 ($address0 (objective-function st)))
	($a1 ($address1 (objective-function st))))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $subject? $pair?)
	       (type Single-Float-Pvar $x0 $y0 $x1 $y1)
	       (type CM-Address $a0 $a1))
      (*let* (($range (the Single-Float-Pvar (!! (- max min))))
	      ($min (the Single-Float-Pvar (!! min)))
	      ($x (!! 0.0))
	      ($y (!! 0.0)))
	(declare (Return-Pvar-P nil)
		 (type Single-Float-Pvar $x $y $range $min))
	(*when $subject?
	  (declare (Return-Pvar-P nil))
	  (*set $x (+!! (random!! $range) $min))
	  (*set $y (+!! (random!! $range) $min)))
	(*when $pair?
	  (declare (Return-Pvar-P nil))
	  (*setf $x0 (pref!! $x $a0 :collision-mode :many-collisions))
	  (*setf $y0 (pref!! $y $a0 :collision-mode :many-collisions))
	  (*setf $x1 (pref!! $x $a1 :collision-mode :many-collisions))
	  (*setf $y1 (pref!! $y $a1 :collision-mode :many-collisions))))))
  st)

;;;------------------------------------------------------------

(defmethod fill-with-canonical-basis-vector! ((st CM-2d-Point-Pairs-State) k)
  "Fill st with the k-th canonical basis vector."
  (zero! st)
  (multiple-value-bind (j i) (truncate k (cardinality (objective-function st)))
    (let ((start (start (objective-function st)))
	  ($pair? ($pair? (objective-function st)))
	  ($x0 ($x0 st)) ($y0 ($y0 st))
	  ($x1 ($x1 st)) ($y1 ($y1 st))
	  ($address0 ($address0 (objective-function st)))
	  ($address1 ($address1 (objective-function st))))
      (*locally
	(declare (Return-Pvar-P nil)
		 (type Boolean-Pvar $pair?)
		 (type Single-Float-Pvar $x0 $y0 $x1 $y1)
		 (type CM-Address $address0 $address1))
	(*let (($x (!! 0.0)))
	  (declare (Return-Pvar-P nil)
		   (type Single-Float-Pvar $x))
	  (*setf (pref $x (+ i start)) 1.0)
	  (*when $pair?
	    (declare (Return-Pvar-P nil))
	    (ecase j
	      (0 (*setf $x0 (pref!! $x $address0
				    :collision-mode :many-collisions))
		 (*setf $x1 (pref!! $x $address1
				    :collision-mode :many-collisions)))
	      (1 (*setf $y0 (pref!! $x $address0
				    :collision-mode :many-collisions))
		 (*setf $y1 (pref!! $x $address1
				    :collision-mode :many-collisions)))))))))
  st)

;;;------------------------------------------------------------

(defmethod coordinate ((st CM-2d-Point-Pairs-State) k
		       &key (coordinate-system :standard))
  (multiple-value-bind (j i) (truncate k (cardinality (objective-function st)))
    (let (($pair? ($pair? (objective-function st)))
	  ($x0 ($x0 st)) ($y0 ($y0 st))
	  ($x1 ($x1 st)) ($y1 ($y1 st))
	  ($address0 ($address0 (objective-function st)))
	  ($address1 ($address1 (objective-function st))))
      (*locally
	(declare (Return-Pvar-P nil)
		 (type Boolean-Pvar $pair?)
		 (type Single-Float-Pvar $x0 $y0 $x1 $y1)
		 (type CM-Address $address0 $address1 $i))
	(*let (($i (!! i)))
	  (declare (type CM-Address $i))
	  (*when $pair?
	    (declare (Return-Pvar-P nil))
	    (let ((addr (*when (=!! $address0 $i) (*min (self-address!!)))))
	      (cond
		((null addr)
		 (setf addr (*when (=!! $address1 $i) (*min (self-address!!))))
		 (when (null addr) (error "Object ~d not in any pair."))
		 (ecase j
		   (0 (pref $x1 addr))
		   (1 (pref $y1 addr))))
		(t (ecase j
		     (0 (pref $x0 addr))
		     (1 (pref $y0 addr))))))))))))

(defmethod (setf coordinate) (new (st CM-2d-Point-Pairs-State) k
		       &key (coordinate-system :standard))
  (multiple-value-bind (j i) (truncate k (cardinality (objective-function st)))
    (let ((start (start (objective-function st)))
	  ($pair? ($pair? (objective-function st)))
	  ($x0 ($x0 st)) ($y0 ($y0 st))
	  ($x1 ($x1 st)) ($y1 ($y1 st))
	  ($address0 ($address0 (objective-function st)))
	  ($address1 ($address1 (objective-function st))))
      (*locally
	(declare (Return-Pvar-P nil)
		 (type Boolean-Pvar $pair?)
		 (type Single-Float-Pvar $x0 $y0 $x1 $y1)
		 (type CM-Address $address0 $address1))
	(*let (($new (!! 0.0)))
	  (declare (Return-Pvar-P nil)
		   (type Single-Float-Pvar $new))
	  (*setf (pref $new (+ i start)) new)
	  (*when $pair?
	    (declare (Return-Pvar-P nil))
	    (*let (($i (!! i)))
	      (declare (Return-Pvar-P nil)
		       (type CM-Address $i))
	      (ecase j
		(0 (*when (=!! $i $address0)
		     (declare (Return-Pvar-P nil))
		     (*setf $x0 (pref!! $new $address0
					:collision-mode :many-collisions)))
		   (*when (=!! $i $address1)
		     (declare (Return-Pvar-P nil))
		     (*setf $x1 (pref!! $new $address1
					:collision-mode :many-collisions))))
		(1 (*when (=!! $i $address0)
		     (declare (Return-Pvar-P nil))
		     (*setf $y0 (pref!! $new $address0
					:collision-mode :many-collisions)))
		   (*when (=!! $i $address1)
		     (declare (Return-Pvar-P nil))
		     (*setf $y1 (pref!! $new $address1
					:collision-mode :many-collisions)))
		   )))))))))

;;;------------------------------------------------------------
;;; this would be trivial for CM-2d-Points-State

(defmethod inner-product ((st0 CM-2d-Point-Pairs-State)
			  (st1 CM-2d-Point-Pairs-State))
  (assert (eq (objective-function st0) (objective-function st1)))
  (let (($subject? ($subject? (objective-function st0)))
	($pair? ($pair? (objective-function st0)))
	($address0 ($address0 (objective-function st0)))
	($address1 ($address1 (objective-function st0)))
	($x00 ($x0 st0)) ($y00 ($y0 st0))
	($x10 ($x1 st0)) ($y10 ($y1 st0))
	($x01 ($x0 st1)) ($y01 ($y0 st1))
	($x11 ($x1 st1)) ($y11 ($y1 st1)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $subject? $pair?)
	       (type Single-Float-Pvar $x00 $y00 $x10 $y10 $x01 $y01 $x11 $y11)
	       (type CM-Address $address0 $address1))
      (*let (($x0 (!! 0.0)) ($y0 (!! 0.0))
	     ($x1 (!! 0.0)) ($y1 (!! 0.0)))
	(declare (Return-Pvar-P nil)
		 (type Single-Float-Pvar $x0 $y0 $x1 $y1))
	;; collapse the pairs to a contiguous block of processors
	(*when $pair?
	  (declare (Return-Pvar-P nil))
	  (*pset :overwrite $x00 $x0 $address0)
	  (*pset :overwrite $y00 $y0 $address0)
	  (*pset :overwrite $x10 $x0 $address1)
	  (*pset :overwrite $y10 $y0 $address1)
	  (*pset :overwrite $x01 $x1 $address0)
	  (*pset :overwrite $y01 $y1 $address0)
	  (*pset :overwrite $x11 $x1 $address1)
	  (*pset :overwrite $y11 $y1 $address1))
	(*when $subject?
	  (declare (Return-Pvar-P nil))
	  (*sum (+!! (*!! $x0 $x1)
		     (*!! $y0 $y1))))))))

;;;------------------------------------------------------------
;;; this would be trivial for CM-2d-Points-State

(defmethod l2-norm2 ((st0 CM-2d-Point-Pairs-State))
  (let (($subject? ($subject? (objective-function st0)))
	($pair? ($pair? (objective-function st0)))
	($address0 ($address0 (objective-function st0)))
	($address1 ($address1 (objective-function st0)))
	($x00 ($x0 st0)) ($y00 ($y0 st0))
	($x10 ($x1 st0)) ($y10 ($y1 st0)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Fixnum s e)
	       (type Boolean-Pvar $subject? $pair?)
	       (type Single-Float-Pvar $x00 $y00 $x10 $y10)
	       (type CM-Address $address0 $address1))
      (*let (($x0 (!! 0.0)) ($y0 (!! 0.0)))
	    (declare (Return-Pvar-P nil)
		     (type Single-Float-Pvar $x0 $y0))
	    ;; collapse the pairs to a contiguous block of processors
	    (*when $pair?
		   (declare (Return-Pvar-P nil))
		   (*pset :overwrite $x00 $x0 $address0)
		   (*pset :overwrite $y00 $y0 $address0)
		   (*pset :overwrite $x10 $x0 $address1)
		   (*pset :overwrite $y10 $y0 $address1))
	    (*when $subject?
		   (declare (Return-Pvar-P nil))
		   (*sum (+!! (*!! $x0 $x0) (*!! $y0 $y0))))))))

;;;============================================================

;(defmethod copy-to! ((st0 CM-2d-Points-State) (st1 CM-2d-Point-Pairs-State)
;		     &rest options)
;  (declare (ignore options))
;  (assert (eq (objective-function st1) (objective-function st0)))
;  (let (($x ($x st0)) ($y ($y st0))
;	($pair? ($pair? (objective-function st1)))
;	($x0 ($x0 st1)) ($y0 ($y0 st1))
;	($x1 ($x1 st1)) ($y1 ($y1 st1))
;	($address0 ($address0 (objective-function st1)))
;	($address1 ($address1 (objective-function st1))))
;    (*locally
;      (declare (Return-Pvar-P nil)
;	       (type Boolean-Pvar $pair?)
;	       (type Single-Float-Pvar $x0 $y0 $x1 $y1 $x $y)
;	       (type CM-Address $address1 $address0))
;      (*when $pair?
;	(declare (Return-Pvar-P nil))
;	(*setf $x0 (pref!! $x $address0 :collision-mode :many-collisions))
;	(*setf $y0 (pref!! $y $address0 :collision-mode :many-collisions))
;	(*setf $x1 (pref!! $x $address1 :collision-mode :many-collisions))
;	(*setf $y1 (pref!! $y $address1 :collision-mode :many-collisions)))))
;  st1)

;;;============================================================

(defmethod copy-to! ((st0 2d-Points-State) (st1 CM-2d-Point-Pairs-State)
		     &rest options)
  (declare (ignore options))
  (let ((s (start (objective-function st1)))
	(e (end (objective-function st1)))
	($pair? ($pair? (objective-function st1)))
	($x0 ($x0 st1)) ($y0 ($y0 st1))
	($x1 ($x1 st1)) ($y1 ($y1 st1))
	($address0 ($address0 (objective-function st1)))
	($address1 ($address1 (objective-function st1))))
    (assert (= (- e s) (length (x st0))))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Fixnum s e)
	       (type Boolean-Pvar $subject? $pair?)
	       (type Single-Float-Pvar $x0 $y0 $x1 $y1)
	       (type CM-Address $address0 $address1))
      (*let (($x (!! 0.0))
	     ($y (!! 0.0)))
	(declare (Return-Pvar-P nil)
		 (type Single-Float-Pvar $x $y))
	(array-to-pvar (x st0) $x :cube-address-start s :cube-address-end e)
	(array-to-pvar (y st0) $y :cube-address-start s :cube-address-end e)
	(*when $pair?
	  (declare (Return-Pvar-P nil))
	  (*setf $x0 (pref!! $x $address0 :collision-mode :many-collisions))
	  (*setf $y0 (pref!! $y $address0 :collision-mode :many-collisions))
	  (*setf $x1 (pref!! $x $address1 :collision-mode :many-collisions))
	  (*setf $y1 (pref!! $y $address1 :collision-mode :many-collisions))))
      st1)))

;;;------------------------------------------------------------

(defmethod copy-to! ((st0 CM-2d-Point-Pairs-State) (st1 2d-Points-State)
		     &rest options)
  (declare (ignore options))
  (let ((s (start (objective-function st0)))
	(e (end (objective-function st0)))
	($pair? ($pair? (objective-function st0)))
	($x0 ($x0 st0)) ($y0 ($y0 st0))
	($x1 ($x1 st0)) ($y1 ($y1 st0))
	($address0 ($address0 (objective-function st0)))
	($address1 ($address1 (objective-function st0))))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Fixnum s e)
	       (type Boolean-Pvar $subject? $pair?)
	       (type Single-Float-Pvar $x0 $y0 $x1 $y1)
	       (type CM-Address $address0 $address1))
      (assert (= (- e s) (length (x st1))))
      (*let (($x (!! 0.0))
	     ($y (!! 0.0)))
	(declare (Return-Pvar-P nil)
		 (type Single-Float-Pvar $x $y))
	;; collapse the pairs to a contiguous block of processors
	(*when $pair?
	  (declare (Return-Pvar-P nil))
	  (*pset :overwrite $x0 $x $address0)
	  (*pset :overwrite $y0 $y $address0)
	  (*pset :overwrite $x1 $x $address1)
	  (*pset :overwrite $y1 $y $address1))
	;; copy the contiguous block to front end arrays
	(pvar-to-array $x (x st1) :cube-address-start s :cube-address-end e)
	(pvar-to-array $y (y st1) :cube-address-start s :cube-address-end e))))
  st1)

;;;============================================================

(defmethod compute-cached-data ((sc CM-MDS-Stress)
			     (st CM-2d-Point-Pairs-State))
  ;; compute each pair's contribution to the gradient
  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($dx ($dx sc))
	($dy ($dy sc))
	($2d ($2d sc))
	($dl ($dl sc)) 
	($x0 ($x0 st))
	($y0 ($y0 st))
	($x1 ($x1 st))
	($y1 ($y1 st)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $d $dx $dy $2d $dl $x0 $y0 $x1 $y1))
      (*when $pair? 
	(declare (Return-Pvar-P nil)) 
	(*setf $dx (-!! $x0 $x1))
	(*setf $dy (-!! $y0 $y1))
	(*setf $2d (l2-norm!! $dx $dy))
	(*when (plusp!! $d)
	  (declare (Return-Pvar-P nil))
	  (*setf $dl (-!! $2d $d))
	  nil)))))

;;;============================================================
;;; Stress calculations
;;;============================================================
;;; assumes compute-cached-data has been called

(defmethod mds-stress ((sc CM-MDS-Stress))
  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($w ($pair-weight sc))
	($dl ($dl sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?) 
	       (type Single-Float-Pvar $d $w $dl))
      (*when (and!! $pair? (plusp!! $d))
	(declare (Return-Pvar-P nil))
	  (*sum (*!! $w $dl $dl))))))

;;;------------------------------------------------------------

(defmethod transform ((sc CM-MDS-Stress)
			     (st CM-2d-Point-Pairs-State))
  (assert (eq sc (objective-function st)))
  (compute-cached-data sc st)
  (mds-stress sc)) 

;;;------------------------------------------------------------
;;; assumes compute-cached-data has been called

(defmethod omds-stress ((sc CM-OMDS-Stress))
  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($w ($pair-weight sc))
	($o ($pair-order sc))
	($dy ($dy sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $d $w $o $dy))
      (*when (and!! $pair?
		    (not!! (zerop!! $o))
		    (plusp!! $d)) 
	(declare (Return-Pvar-P nil))
	(*let (($loss (-!! (*!! $o $dy) $d)))
	  (declare (Return-Pvar-P nil)
		   (type Single-Float-Pvar $loss))
	  (*sum (*!! $w $loss $loss)))))))

;;;------------------------------------------------------------

(defmethod transform ((sc CM-OMDS-Stress)
			     (st CM-2d-Point-Pairs-State))
  (assert (eq sc (objective-function st)))
  (compute-cached-data sc st)
  (+ (* (a sc) (mds-stress sc))
     (* (b sc) (omds-stress sc))))
      
;;;------------------------------------------------------------
;;; 1/r version

(defmethod romds-stress ((sc CM-ROMDS-Stress))
  (let (($pair? ($pair? sc))
	($2d ($2d sc))
	($w ($pair-weight sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $2d $w))
      (*when $pair? (*sum (/!! $w $2d))))))

;;;------------------------------------------------------------

(defmethod transform ((sc CM-ROMDS-Stress)
			     (st CM-2d-Point-Pairs-State))
  (assert (eq sc (objective-function st)))
  (compute-cached-data sc st)
  (+ (* (a sc) (mds-stress sc))
     (* (b sc) (omds-stress sc))
     (* (c sc) (romds-stress sc))))

;;;============================================================
;;; Jacobian calculations
;;;============================================================
;;; assumes compute-cached-data has been called

(defmethod mds-jacobian ((sc CM-MDS-Stress))
  ;; compute each pair's contribution to the gradient
  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($dx ($dx sc))
	($dy ($dy sc))
	($2d ($2d sc))
	($dl ($dl sc))
	($mds-jacobian-x ($mds-jacobian-x sc))
	($mds-jacobian-y ($mds-jacobian-y sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar
		     $d $dx $dy $2d $dl $mds-jacobian-x $mds-jacobian-y))
      (*when (and!! $pair? (plusp!! $d))
	(declare (Return-Pvar-P nil))
	(*let (($fac (/!! (*!! (!! 2.0) $dl) $2d)))
	  (declare (Return-Pvar-P nil)
		   (type Single-Float-Pvar $fac)) 
	  (*setf $mds-jacobian-x (*!! $fac $dx))
	  (*setf $mds-jacobian-y (*!! $fac $dy))
	  nil)))))

;;;------------------------------------------------------------
;;; assumes compute-cached-data has been called

(defmethod omds-jacobian ((sc CM-OMDS-Stress))

  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($o ($pair-order sc))
	($omds-jacobian-y ($omds-jacobian-y sc))
	($dy ($dy sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $d $o $omds-jacobian-y $dy))
      (*when (and!! $pair?
		    (not!! (zerop!! $o))
		    (plusp!! $d))
	(declare (Return-Pvar-P nil))
	(*setf $omds-jacobian-y (*!! (!! 2.0) $o (-!! (*!! $o $dy) $d)))
	nil))))
      
;;;------------------------------------------------------------
;;; 1/r version

(defmethod romds-jacobian ((sc CM-ROMDS-Stress))
  (let (($pair? ($pair? sc))
	($dx ($dx sc))
	($dy ($dy sc))
	($2d ($2d sc))
	($romds-jacobian-x ($romds-jacobian-x sc))
	($romds-jacobian-y ($romds-jacobian-y sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar
		     $dx $dy $2d $romds-jacobian-x $romds-jacobian-y))
      (*when $pair?
	(declare (Return-Pvar-P nil))
	(*let (($2d3 (*!! (!! -1.0) $2d $2d $2d)))
	  (declare (Return-Pvar-P nil)
		   (type Single-Float-Pvar $2d3))
	  (*setf $romds-jacobian-x (/!! $dx $2d3))
	  (*setf $romds-jacobian-y (/!! $dy $2d3))
	  nil)))))


;;;============================================================
;;; Gradient calculations
;;;============================================================

(defmethod reduce-jacobian-to-minus-grad ((sc CM-MDS-Stress)
						 (gr CM-2d-Point-Pairs-State))
  (let (($subject? ($subject? sc))
	($pair? ($pair? sc))
	($address0 ($address0 sc))
	($address1 ($address1 sc))
	($jacobian-x ($jacobian-x sc))
	($jacobian-y ($jacobian-y sc))
	($grx0 ($x0 gr))
	($gry0 ($y0 gr))
	($grx1 ($x1 gr))
	($gry1 ($y1 gr)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $subject? $pair?)
	       (type CM-Address $address0 $address1)
	       (type Single-Float-Pvar $jacobian-x $jacobian-y
		     $grx0 $gry0 $grx1 $gry1))
      (*let (($gx (!! 0.0)) ($gy (!! 0.0))
	     ($gx0 (!! 0.0)) ($gy0 (!! 0.0))
	     ($gx1 (!! 0.0)) ($gy1 (!! 0.0)))
	(declare (Return-Pvar-P nil)
		 (type Single-Float-Pvar $gx $gy $gx0 $gy0 $gx1 $gy1))
	(*when $pair? 
	  (declare (Return-Pvar-P nil)) 
	  ;; send it to the subjects
	  ;; <gx0,gy0> get the positive gradient
	  ;; <gx1,gy1> get the negative gradient
	  (*pset :add $jacobian-x $gx0 $address0)
	  (*pset :add $jacobian-y $gy0 $address0)
	  (*pset :add $jacobian-x $gx1 $address1)
	  (*pset :add $jacobian-y $gy1 $address1)
	  nil)
	;; sum over pairs for each subject
	(*when $subject?
	  (declare (Return-Pvar-P nil))
	  ;; subtract the two terms to get the right answer
	  (*setf $gx (-!! $gx1 $gx0))
	  (*setf $gy (-!! $gy1 $gy0))
	  nil)
	;; send back to the pair processors
	(*when $pair?
	  (declare (Return-Pvar-P nil))
	  (*setf $grx0 (pref!! $gx $address0 :collision-mode :many-collisions))
	  (*setf $gry0 (pref!! $gy $address0 :collision-mode :many-collisions))
	  (*setf $grx1 (pref!! $gx $address1 :collision-mode :many-collisions))
	  (*setf $gry1 (pref!! $gy $address1 :collision-mode :many-collisions))
	  )))))

;;;------------------------------------------------------------

(defmethod minus-grad ((sc CM-MDS-Stress)
					(st CM-2d-Point-Pairs-State)
					(gr CM-2d-Point-Pairs-State))
  (assert (eq sc (objective-function st)))
  (assert (eq sc (objective-function gr)))
  (zero! gr)
  (compute-cached-data sc st)
  (mds-jacobian sc)
  ;; transfer gradient from pair processors (in <st>) to subject
  ;; processors (in <gr>)
  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($w ($pair-weight sc))
	($jacobian-x ($jacobian-x sc))
	($jacobian-y ($jacobian-y sc))
	($mds-jacobian-x ($mds-jacobian-x sc))
	($mds-jacobian-y ($mds-jacobian-y sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $d $w $jacobian-x $jacobian-y
		     $mds-jacobian-x $mds-jacobian-y))
      (*when (and!! $pair? (plusp!! $d))
	(declare (Return-Pvar-P nil)) 
	;; send it to the subjects
	(*setf $jacobian-x (*!! $w $mds-jacobian-x))
	(*setf $jacobian-y (*!! $w $mds-jacobian-y))
	nil))) 
  (reduce-jacobian-to-minus-grad sc gr)
  gr)

;;;------------------------------------------------------------

(defmethod minus-grad ((sc CM-OMDS-Stress)
		       (st CM-2d-Point-Pairs-State)
		       (gr CM-2d-Point-Pairs-State))
  (assert (eq sc (objective-function st)))
  (assert (eq sc (objective-function gr)))
  (zero! gr)
  (compute-cached-data sc st)
  (mds-jacobian sc)
  (omds-jacobian sc)
  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($w ($pair-weight sc))
	($jacobian-x ($jacobian-x sc))
	($jacobian-y ($jacobian-y sc))
	($mds-jacobian-x ($mds-jacobian-x sc))
	($mds-jacobian-y ($mds-jacobian-y sc))
	($omds-jacobian-y ($omds-jacobian-y sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar
		     $d $w $jacobian-x $jacobian-y
		     $mds-jacobian-x $mds-jacobian-y $omds-jacobian-y))
      (*let (($a (the Single-Float-Pvar (!! (a sc))))
	     ($b (the Single-Float-Pvar (!! (b sc)))))
	    (declare (type Single-Float-Pvar $a $b))
	    (*when (and!! $pair? (plusp!! $d))
		   (declare (Return-Pvar-P nil)) 
		   ;; send it to the subjects
		   (*setf $jacobian-x (*!! $w $a $mds-jacobian-x))
		   (*setf $jacobian-y
			  (*!! $w (+!! (*!! $a $mds-jacobian-y)
				       (*!! $b $omds-jacobian-y))))
		   nil))))
  (reduce-jacobian-to-minus-grad sc gr)
  gr)

;;;------------------------------------------------------------

(defmethod minus-grad ((sc CM-ROMDS-Stress)
		       (st CM-2d-Point-Pairs-State)
		       (gr CM-2d-Point-Pairs-State))
  (assert (eq sc (objective-function st)))
  (assert (eq sc (objective-function gr)))
  (zero! gr)
  (compute-cached-data sc st)
  (mds-jacobian sc)
  (omds-jacobian sc)
  (romds-jacobian sc)
  (let (($pair? ($pair? sc))
	($d ($pair-dist sc))
	($w ($pair-weight sc))
	($jacobian-x ($jacobian-x sc))
	($jacobian-y ($jacobian-y sc))
	($mds-jacobian-x ($mds-jacobian-x sc))
	($mds-jacobian-y ($mds-jacobian-y sc))
	($omds-jacobian-y ($omds-jacobian-y sc))
	($romds-jacobian-x ($romds-jacobian-x sc))
	($romds-jacobian-y ($romds-jacobian-y sc)))
    (*locally
      (declare (Return-Pvar-P nil)
	       (type Boolean-Pvar $pair?)
	       (type Single-Float-Pvar $d $w
		     $jacobian-x $jacobian-y
		     $mds-jacobian-x $mds-jacobian-y
		     $omds-jacobian-y
		     $romds-jacobian-x $romds-jacobian-y))
      (*let (($a (the Single-Float-Pvar (!! (a sc))))
	     ($b (the Single-Float-Pvar (!! (b sc))))
	     ($c (the Single-Float-Pvar (!! (c sc)))))
	    (declare (type Single-Float-Pvar $a $b $c))
	    (*cond
	      ((and!! $pair? (plusp!! $d))
	       (*setf $jacobian-x (*!! $w (+!! (*!! $a $mds-jacobian-x)
					       (*!! $c $romds-jacobian-x))))
	       (*setf $jacobian-y (*!! $w (+!! (*!! $a $mds-jacobian-y)
					       (*!! $b $omds-jacobian-y)
					       (*!! $c $romds-jacobian-y)))))
	      (t!!
		(*setf $jacobian-x (*!! $w $c $romds-jacobian-x))
		(*setf $jacobian-y (*!! $w $c $romds-jacobian-y)))))))
  (reduce-jacobian-to-minus-grad sc gr)
  gr)