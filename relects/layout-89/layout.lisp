;;;-*- Package: Layout; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Layout)

(proclaim '(optimize (safety 3) (space 1) (speed 1) (compilation-speed 0)))

;;;============================================================
;;; Configuration spaces:
;;;============================================================

(defclass Configuration-Space (cactus:Direct-Sum-Affine-Space)
    ((subjects
	:type Sequence 
	:accessor subjects
	:initarg :subjects)
     (glyph-descriptions
	:type Sequence  
	:accessor glyph-descriptions
	:initarg :glyph-descriptions)))

;;;------------------------------------------------------------

(defmethod shared-initialize :after ((space Configuration-Space)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (let* ((dim (length (subjects space)))
	 (half-space (cactus:LPoint-Space dim)))
    (setf (cactus:xspace space) half-space)
    (setf (cactus:yspace space) half-space)
    (when (empty-slot? space 'glyph-descriptions)
      (setf (glyph-descriptions space)
	    (make-array dim :initial-element :star)))))

;;;============================================================

(defclass MDS-Configuration-Space (Configuration-Space)
     ((pairs
	:type Sequence ;; of Pair's of the domain's subjects above
	:accessor pairs
	:initarg :pairs)
      (dist-fn ;; applied to 2 <subjects>
	:type Function ;; (Function (T T) Float-Scalar)
	:accessor dist-fn
	:initarg :dist-fn)
      (weight-fn
	;; applied to 2 <subjects> and a distance that should be the same
	;; as that returned by <dist-fn>
	:type Function ;; (Function (T T Float-Scalar) Float-Scalar)
	:accessor weight-fn
	:initarg :weight-fn)))

;;;============================================================

(defclass OMDS-Configuration-Space (MDS-Configuration-Space)
     ((order-fn ;; applied to 2 <subjects>, returns -1.0, 0.0, or 1.0
	:type Function ;;(Function (T T) Float-Scalar)
	:accessor order-fn
	:initarg :order-fn)))

;;;============================================================

(defclass Graph-Configuration-Space (MDS-Configuration-Space)
     ((edge-pairs
       :type Sequence ;; of Pair's
       :accessor edge-pairs
       :initarg :edge-pairs)))

;;;============================================================

(defclass Directed-Graph-Configuration-Space (Graph-Configuration-Space
					       OMDS-Configuration-Space)
     ())

;;;============================================================
;;; Strip Charts for Configuration spaces:
;;;============================================================

(defclass Configuration-Space-Strip-Chart (cactus:Direct-Sum-Strip-Chart) ())

;;;------------------------------------------------------------

(defmethod cactus:make-strip-chart-for ((space Configuration-Space)
					&rest options
					&key
					(slate (slate:make-slate))
					(region (slate:slate-region slate))
					(point (cactus:fill-randomly!
						 (cactus:make-element space)))
					(max-length 4)
					&allow-other-keys)
  (let ((plot (make-instance 'Configuration-Space-Strip-Chart
			     :slate slate
			     :slate-region region)))
    (setf (petroglyph:plot-updatep plot) nil)
    (setf (cactus:snakes plot)
	  (tools:with-collection
	    (map nil
		 #'(lambda (x y g)
		     (tools:collect
		       (petroglyph:plot-add-snake
			 plot
			 (list (g:make-position x y))
			 :max-length max-length
			 :glyph-description g)))
		 (cactus::1d-array (cactus:x point))
		 (cactus::1d-array (cactus:y point))
		 (glyph-descriptions space))))
    (setf (petroglyph:plot-updatep plot) t)
    plot))

;;;------------------------------------------------------------

(defmethod petroglyph:strip-crawl ((plot petroglyph:Graph-Strip-Chart)
				   (point cactus:Direct-Sum-Point))
  (setf (petroglyph:plot-updatep plot) nil)
  (map nil
       #'(lambda (x0 y0 snake) (petroglyph:snake-crawl-xy snake x0 y0))
       (cactus::1d-array (cactus:x point))
       (cactus::1d-array (cactus:y point))
       (petroglyph:nodes (petroglyph:snake plot)))
  (setf (petroglyph:plot-updatep plot) t)
  (petroglyph:plot-format plot))

;;;------------------------------------------------------------

(defmethod cactus:make-strip-chart-for ((space Graph-Configuration-Space)
					&rest options
					&key
					(slate (slate:make-slate))
					(region (slate:slate-region slate))
					(point (cactus:fill-randomly!
						 (cactus:make-element space)))
					(max-length 4)
					&allow-other-keys)
  (let* ((plot (make-instance 'petroglyph::Graph-Strip-Chart
			      :slate slate
			      :slate-region region))
	 (graph (make-instance 'petroglyph:Graph-Snake
			       :nodes ()
			       :edges ())))
    (setf (petroglyph:plot-updatep plot) nil)
    (map nil
	 #'(lambda (s x y g)
	     (let ((node (make-instance
			   'petroglyph:Graph-Node-Snake
			   :parent graph
			   :world-positions (list (g:make-position x y))
			   :max-length max-length
			   :glyph-description g)))
	       (setf (petroglyph:node-for-subject s graph) node)
	       (push node (petroglyph:nodes graph))))
	 (subjects space)
	 (cactus::1d-array (cactus:x point))
	 (cactus::1d-array (cactus:y point))
	 (glyph-descriptions space))
    (map nil
	 #'(lambda (p)
	     (let* ((s0 (pair0 p))
		    (s1 (pair1 p))
		    (node0 (petroglyph:node-for-subject s0 graph))
		    (node1 (petroglyph:node-for-subject s1 graph)))
	       (push (make-instance 'petroglyph:Graph-Edge-Scene
				    :parent graph
				    :node0 node0 :node1 node1)
		     (petroglyph:edges graph))))
	 (edge-pairs space))
    (setf (petroglyph:snake plot) graph)
    (setf (petroglyph:parent graph) plot)
    (petroglyph:scene-add-child graph plot)
    (setf (petroglyph:plot-updatep plot) t)
    plot))

;;;============================================================
;;; Objective functions on configuration spaces
;;;============================================================

(defclass MDS-Stress (cactus:C1-Objective-Function)
     (;; the number of positions to be computed
      (cardinality :type Fixnum :accessor cardinality)
      (number-of-pairs :type Fixnum :accessor number-of-pairs)
      ;; the indexes of the subjects in each active pair
      (index0 :type List :accessor index0)
      (index1 :type List :accessor index1)
      ;; abstract properties of pairs of subjects
      (dist :type Float-Vector :accessor dist)
      ;; a negative value implies this pair is ignored for MDS
      (weight :type Float-Vector :accessor weight)

      ;; caches
      (dx :type Float-Vector :accessor dx)
      (dy :type Float-Vector :accessor dy)
      (2d :type Float-Vector :accessor 2d)
      (dl :type Float-Vector :accessor dl)))

;;;------------------------------------------------------------

(defparameter *mds-stress* (make-instance 'MDS-Stress))

;;;------------------------------------------------------------

(defmethod initialize-objective-function ((space MDS-Configuration-Space)
					  (sc MDS-Stress))

  (setf (cactus:domain sc) space)
  (initialize-address-table)
  (assign-addresses (subjects space))
  (setf (number-of-pairs sc) (length (pairs space)))
  (let ((m (number-of-pairs sc)))
    (setf (index0 sc) (bm:make-fixnum-array m))
    (setf (index1 sc) (bm:make-fixnum-array m))
    (setf (dist sc)   (bm:make-float-array m :initial-element -1.0))
    (setf (weight sc) (bm:make-float-array m :initial-element -1.0))
    (setf (dx sc)     (bm:make-float-array m))
    (setf (dy sc)     (bm:make-float-array m))
    (setf (2d sc)     (bm:make-float-array m))
    (setf (dl sc)     (bm:make-float-array m)))
  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(weight (weight sc))
	(dist (dist sc))
	(dist-fn (dist-fn space))
	(weight-fn (weight-fn space))
	(k 0))
    (dolist (pair (pairs space))
      (let* ((s0 (pair0 pair))
	     (s1 (pair1 pair))
	     (i0 (address s0))
	     (i1 (address s1))
	     (d (funcall dist-fn s0 s1)))
	(setf (aref index0 k) i0)
	(setf (aref index1 k) i1)
	(setf (aref dist k) d)
	(setf (aref weight k) (funcall weight-fn s0 s1 d))
	(incf k)))))

;;;============================================================
;;; Stress calculations
;;;============================================================

(proclaim
  '(ftype (Function (Float-Scalar Float-Scalar Float-Scalar) Float-Scalar)
	  mds-term))
(proclaim '(Inline mds-term))

(defun mds-term (dx dy d) (bm:sq (- (bm:l2-norm-xy dx dy) d)))

;;;------------------------------------------------------------

(defmethod compute-cached-data ((sc MDS-Stress) (st cactus:Direct-Sum-Point))

  ;; a temporary restriction:
  (check-type (cactus:x st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:y st) cactus:Euclidean-Affine-Point)

  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(dx (dx sc))
	(dy (dy sc))
	(2d (2d sc))
	(dl (dl sc)) 
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st))))
    (dotimes (i (number-of-pairs sc))
      (let* ((i0 (aref index0 i))
	     (i1 (aref index1 i))
	     (dxi (- (aref x i0) (aref x i1)))
	     (dyi (- (aref y i0) (aref y i1)))
	     (2di (bm:l2-norm-xy dxi dyi)))
	(setf (aref dx i) dxi)
	(setf (aref dy i) dyi)
	(setf (aref 2d i) 2di)
	(setf (aref dl i) (- 2di (aref d i)))))))

;;;------------------------------------------------------------
;;; assumes compute-cached-data has been called

(defmethod mds-stress ((sc MDS-Stress))
  (let ((w (weight sc))
	(d (dist sc))
	(dl (dl sc))
	(sum 0.0))
    (dotimes (i (length w))
      (when (plusp (aref d i))
	(let ((dli (aref dl i)))
	  (incf sum (* (aref w i) dli dli)))))
    sum))

;;;------------------------------------------------------------

(defmethod cactus:transform2 ((sc MDS-Stress) (st cactus:Direct-Sum-Point))

  ;; a temporary restriction:
  (check-type (cactus:x st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:y st) cactus:Euclidean-Affine-Point)

  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(w (weight sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(sum 0.0))
    (dotimes (i (number-of-pairs sc))
      (when (plusp (aref d i))
	(let* ((i0 (aref index0 i))
	       (i1 (aref index1 i))
	       (dxi (- (aref x i0) (aref x i1)))
	       (dyi (- (aref y i0) (aref y i1)))
	       (2di (bm:l2-norm-xy dxi dyi))
	       (dli (- 2di (aref d i))))
	  (incf sum (* (aref w i) dli dli)))))
    sum))

;;;============================================================
;;; Gradient calculations
;;;============================================================
;;; assumes compute-cached-data has been called
;;; adds the mds term to gr

(defmethod add-mds-minus-grad ((sc MDS-Stress) (gr cactus:Direct-Sum-Vector)
			       &optional (scale 1.0))
  
  ;; a temporary restriction:
  (check-type (cactus:x gr) cactus:Euclidean-Vector)
  (check-type (cactus:y gr) cactus:Euclidean-Vector)

  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(w (weight sc))
	(dx (dx sc))
	(dy (dy sc))
	(2d (2d sc))
	(dl (dl sc))
	(xgr (cactus::1d-array (cactus:x gr)))
	(ygr (cactus::1d-array (cactus:y gr)))
	(-2*scale (* -2.0 scale))) 
    (dotimes (i (number-of-pairs sc))
      (when (plusp (aref d i))
	(assert (plusp (aref 2d i)))
	(let* ((i0 (aref index0 i))
	       (i1 (aref index1 i))
	       (fac (/ (* -2*scale (aref w i) (aref dl i)) (aref 2d i)))
	       (grx (* fac (aref dx i)))
	       (gry (* fac (aref dy i))))
	  (incf (aref xgr i0) grx)
	  (incf (aref ygr i0) gry)
	  ;; negative contribution to 2nd subject in pair
	  (decf (aref xgr i1) grx)
	  (decf (aref ygr i1) gry))))))

;;;------------------------------------------------------------

(defmethod cactus:minus-grad ((sc MDS-Stress)
			      (st cactus:Direct-Sum-Point)
			      (gr cactus:Direct-Sum-Vector))
  ;; a temporary restriction:
  (check-type (cactus:x st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:y st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:x gr) cactus:Euclidean-Vector)
  (check-type (cactus:y gr) cactus:Euclidean-Vector)
  
  (cactus:zero! gr)
  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(w (weight sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(xgr (cactus::1d-array (cactus:x gr)))
	(ygr (cactus::1d-array (cactus:y gr)))) 
    (dotimes (i (number-of-pairs sc))
      (when (plusp (aref d i))
	(let* ((i0 (aref index0 i))
	       (i1 (aref index1 i))
	       (dxi (- (aref x i0) (aref x i1)))
	       (dyi (- (aref y i0) (aref y i1)))
	       (2di (bm:l2-norm-xy dxi dyi))
	       (dli (- 2di (aref d i))) 
	       (fac (progn (assert (plusp 2di))
			   (/ (* -2.0 (aref w i) dli) 2di)))
	       (grx (* fac dxi))
	       (gry (* fac dyi)))
	  (incf (aref xgr i0) grx)
	  (incf (aref ygr i0) gry)
	  ;; negative contribution to 2nd subject in pair
	  (decf (aref xgr i1) grx)
	  (decf (aref ygr i1) gry)))))
  gr)

;;;------------------------------------------------------------

(defmethod cactus:forward-dif-minus-grad ((sc MDS-Stress) st h s0 g)
  (declare (ignore s0))
  (cactus:zero! g)
  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(w (weight sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(xh (cactus::1d-array (cactus:x h)))
	(yh (cactus::1d-array (cactus:y h)))
	(xg (cactus::1d-array (cactus:x g)))
	(yg (cactus::1d-array (cactus:y g))))
    (dotimes (i (number-of-pairs sc))
      (let ((di (aref d i))
	    (wi (aref w i)))
	(when (plusp (aref d i))
	  (let* ((i0 (aref index0 i))
		 (i1 (aref index1 i))
		 (x0 (aref x i0))
		 (y0 (aref y i0))
		 (x1 (aref x i1))
		 (y1 (aref y i1))
		 (xh0 (aref xh i0))
		 (yh0 (aref yh i0))
		 (xh1 (aref xh i1))
		 (yh1 (aref yh i1))
		 (dx (- x0 x1))
		 (dy (- y0 y1))
		 (l (mds-term dx dy di)))
	    (assert (not (zerop xh0)))
	    (assert (not (zerop yh0)))
	    (assert (not (zerop xh1)))
	    (assert (not (zerop yh1)))
	    (incf (aref xg i0)
		  (* wi (/ (- l (mds-term (+ dx xh0) dy di)) xh0)))
	    (incf (aref yg i0)
		  (* wi (/ (- l (mds-term dx (+ dy yh0) di)) yh0)))
	    (incf (aref xg i1)
		  (* wi (/ (- l (mds-term (- dx xh1) dy di)) xh1)))
	    (incf (aref yg i1)
		  (* wi (/ (- l (mds-term dx (- dy yh1) di)) yh1))))))))
  g)

;;;------------------------------------------------------------

(defmethod cactus:central-dif-minus-grad ((sc MDS-Stress) st h s0 g)
  (declare (ignore s0))
  (cactus:zero! g)
  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(w (weight sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(xh (cactus::1d-array (cactus:x h)))
	(yh (cactus::1d-array (cactus:y h)))
	(xg (cactus::1d-array (cactus:x g)))
	(yg (cactus::1d-array (cactus:y g)))) 
    (dotimes (i (number-of-pairs sc))
      (let ((di (aref d i))
	    (wi/2 (* 0.5 (aref w i))))
	(when (and (plusp di) (plusp wi/2))
	  (let* ((i0 (aref index0 i))
		 (i1 (aref index1 i))
		 (x0 (aref x i0)) (y0 (aref y i0))
		 (x1 (aref x i1)) (y1 (aref y i1))
		 (xh0 (aref xh i0)) (yh0 (aref yh i0))
		 (xh1 (aref xh i1)) (yh1 (aref yh i1))
		 (dx (- x0 x1)) (dy (- y0 y1)))
	    (assert (not (zerop xh0)))
	    (assert (not (zerop yh0)))
	    (assert (not (zerop xh1)))
	    (assert (not (zerop yh1)))
	    (incf (aref xg i0)
		  (* wi/2 (/ (- (mds-term (- dx xh0) dy di)
				(mds-term (+ dx xh0) dy di)) xh0)))
	    (incf (aref yg i0)
		  (* wi/2 (/ (- (mds-term dx (- dy yh0) di)
				(mds-term dx (+ dy yh0) di)) yh0)))
	    (incf (aref xg i1)
		  (* wi/2 (/ (- (mds-term (+ dx xh1) dy di)
				(mds-term (- dx xh1) dy di)) xh1)))
	    (incf (aref yg i1)
		  (* wi/2 (/ (- (mds-term dx (+ dy yh1) di)
				(mds-term dx (- dy yh1) di)) yh1))))))))
  g)

;;;============================================================

(defclass OMDS-Stress (MDS-Stress)
     ((order :type Float-Vector :accessor order)
      (a :type Float-Scalar :accessor a	:initarg :a)
      (b :type Float-Scalar :accessor b	:initarg :b))
  (:default-initargs :a 1.0 :b 1.0))

;;;------------------------------------------------------------

(defparameter *omds-stress* (make-instance 'OMDS-Stress)) 

;;;------------------------------------------------------------

(defmethod initialize-objective-function :after ((lc MDS-Configuration-Space)
						 (sc OMDS-Stress))
	   (setf (order sc) (bm:make-float-array (length (pairs lc))))
	   (let ((order (order sc))
		 (order-fn (order-fn lc))
		 (k 0))
	     (dolist (pair (pairs lc))
	       (setf (aref order k) (funcall order-fn (pair0 pair) (pair1 pair)))
	       (incf k))))

;;;------------------------------------------------------------

(proclaim
  '(ftype (Function (Float-Scalar Float-Scalar Float-Scalar Float-Scalar)
		    Float-Scalar)
	  order-term
	  omds-term))
(proclaim '(Inline order-term omds-term))

(defun omds-term (dx dy d o a b)
  (if (zerop o)
      (* a (mds-term dx dy d))
      (+ (* a (mds-term dx dy d))
	 (* b (order-term dy d o)))))

;;;------------------------------------------------------------
;;; assumes compute-cached-data has been called

(defmethod omds-stress ((sc OMDS-Stress))
  (let ((d (dist sc))
	(w (weight sc))
	(o (order sc))
	(dy (dy sc))
	(sum 0.0))
    (dotimes (i (number-of-pairs sc))
      (let ((oi (aref o i))
	    (di (aref d i)))
	(unless (or (zerop oi) (minusp di))
	  (incf sum (* (aref w i) (order-term (aref dy i) di oi))))))
    sum))

;;;------------------------------------------------------------      

(defmethod cactus:transform2 ((sc OMDS-Stress) (st cactus:Direct-Sum-Point))

  ;; a temporary restriction:
  (check-type (cactus:x st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:y st) cactus:Euclidean-Affine-Point)

  (let ((a (a sc))
	(b (b sc))
	(index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(w (weight sc))
	(o (order sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(sum 0.0))
    (dotimes (i (number-of-pairs sc))
      (let ((di (aref d i))
	    (wi (aref w i))
	    (oi (aref o i)))
	(when (and (plusp di) (plusp wi))
	  (let* ((i0 (aref index0 i))
		 (i1 (aref index1 i))
		 (dxi (- (aref x i0) (aref x i1)))
		 (dyi (- (aref y i0) (aref y i1))))
	    (incf sum (* wi (omds-term dxi dyi di oi a b)))))))
    sum))

;;;============================================================
;;; Gradient calculations
;;;============================================================
;;; assumes compute-cached-data has been called
;;; adds the mds term to gr

(defmethod add-omds-minus-grad ((sc OMDS-Stress) (gr cactus:Direct-Sum-Vector)
				&optional (scale 1.0))
  ;; a temporary restriction:
  (check-type (cactus:x gr) cactus:Euclidean-Vector)
  (check-type (cactus:y gr) cactus:Euclidean-Vector)

  ;; compute each pair's contribution to the gradient
  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(d (dist sc))
	(w (weight sc))
	(o (order sc))
	(dy (dy sc))
	(ygr (cactus::1d-array (cactus:y gr)))
	(-2*scale (* -2.0 scale))) 
    (dotimes (i (number-of-pairs sc))
      (let ((di (aref d i)) (oi (aref o i)))
	(unless (or (zerop oi) (minusp di))
	  (let* ((i0 (aref index0 i))
		 (i1 (aref index1 i))
		 (gry (* -2*scale (aref w i) oi
			 (- (* oi (aref dy i)) (aref d i)))))
	    (incf (aref ygr i0) gry)
	    ;; negative contribution to 2nd subject in pair
	    (decf (aref ygr i1) gry)))))))

;;;------------------------------------------------------------

(defmethod cactus:minus-grad ((sc OMDS-Stress)
			      (st cactus:Direct-Sum-Point)
			      (gr cactus:Direct-Sum-Vector))
  ;; a temporary restriction:
  (check-type (cactus:x st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:y st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:x gr) cactus:Euclidean-Vector)
  (check-type (cactus:y gr) cactus:Euclidean-Vector)
  
  (cactus:zero! gr)
  (let ((a (a sc)) (b (b sc))
	(index0 (index0 sc)) (index1 (index1 sc))
	(d (dist sc)) (w (weight sc)) (o (order sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(xgr (cactus::1d-array (cactus:x gr)))
	(ygr (cactus::1d-array (cactus:y gr)))) 
    (dotimes (i (number-of-pairs sc))
      (let ((di (aref d i))
	    (oi (aref o i))
	    (wi (aref w i)))
	(when (and (plusp di) (plusp wi))
	  (let* ((i0 (aref index0 i))
		 (i1 (aref index1 i))
		 (dxi (- (aref x i0) (aref x i1)))
		 (dyi (- (aref y i0) (aref y i1)))
		 (2di (bm:l2-norm-xy dxi dyi))
		 (dli (- 2di di)) 
		 (fac (progn (assert (plusp 2di))
			     (/ (* -2.0 dli) 2di)))
		 (mds-grx (* fac dxi))
		 (mds-gry (* fac dyi))
		 (grx (* wi a mds-grx))
		 (gry (* wi (if (zerop oi)
				(* a mds-gry)
				(+ (* a mds-gry)
				   (* b (* -2.0 oi (- (* oi dyi) di))))))))
	    (incf (aref xgr i0) grx)
	    (incf (aref ygr i0) gry)
	    ;; negative contribution to 2nd subject in pair
	    (decf (aref xgr i1) grx)
	    (decf (aref ygr i1) gry))))))
  gr)

;;;------------------------------------------------------------

(defmethod cactus:forward-dif-minus-grad ((sc OMDS-Stress) st h s0 g)
  (declare (ignore s0))
  (cactus:zero! g)
  (let ((a (a sc)) (b (b sc))
	(index0 (index0 sc)) (index1 (index1 sc))
	(d (dist sc)) (w (weight sc)) (o (order sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(xh (cactus::1d-array (cactus:x h)))
	(yh (cactus::1d-array (cactus:y h)))
	(xg (cactus::1d-array (cactus:x g)))
	(yg (cactus::1d-array (cactus:y g))))
    (dotimes (i (number-of-pairs sc))
      (let ((di (aref d i)) (wi (aref w i)) (oi (aref o i)))
	(when (and (plusp di) (plusp wi))
	  (let* ((i0 (aref index0 i))
		 (i1 (aref index1 i))
		 (x0 (aref x i0)) (y0 (aref y i0))
		 (x1 (aref x i1)) (y1 (aref y i1))
		 (xh0 (aref xh i0)) (yh0 (aref yh i0))
		 (xh1 (aref xh i1)) (yh1 (aref yh i1))
		 (dx (- x0 x1)) (dy (- y0 y1))
		 (l (omds-term dx dy di oi a b)))
	    (assert (not (zerop xh0)))
	    (assert (not (zerop yh0)))
	    (assert (not (zerop xh1)))
	    (assert (not (zerop yh1)))
	    (incf (aref xg i0)
		  (* wi (/ (- l (omds-term (+ dx xh0) dy di oi a b))
			   xh0)))
	    (incf (aref yg i0)
		  (* wi (/ (- l (omds-term dx (+ dy yh0) di oi a b))
			   yh0)))
	    (incf (aref xg i1)
		  (* wi (/ (- l (omds-term (- dx xh1) dy di oi a b))
			   xh1)))
	    (incf (aref yg i1)
		  (* wi (/ (- l (omds-term dx (- dy yh1) di oi a b))
			   yh1))))))))
  g)

;;;------------------------------------------------------------

(defmethod cactus:central-dif-minus-grad ((sc OMDS-Stress) st h s0 g)
  (declare (ignore s0))
  (cactus:zero! g)
  (let ((a (a sc)) (b (b sc))
	(index0 (index0 sc)) (index1 (index1 sc))
	(d (dist sc)) (w (weight sc)) (o (order sc))
	(x (cactus::1d-array (cactus:x st)))
	(y (cactus::1d-array (cactus:y st)))
	(xh (cactus::1d-array (cactus:x h)))
	(yh (cactus::1d-array (cactus:y h)))
	(xg (cactus::1d-array (cactus:x g)))
	(yg (cactus::1d-array (cactus:y g))))
    (dotimes (i (number-of-pairs sc))
      (let ((di (aref d i))
	    (wi/2 (* 0.5 (aref w i)))
	    (oi (aref o i)))
	(when (and (plusp di) (plusp wi/2))
	  (let* ((i0 (aref index0 i))
		 (i1 (aref index1 i))
		 (x0 (aref x i0)) (y0 (aref y i0))
		 (x1 (aref x i1)) (y1 (aref y i1))
		 (xh0 (aref xh i0)) (yh0 (aref yh i0))
		 (xh1 (aref xh i1)) (yh1 (aref yh i1))
		 (dx (- x0 x1)) (dy (- y0 y1)))
	    (assert (not (zerop xh0)))
	    (assert (not (zerop yh0)))
	    (assert (not (zerop xh1)))
	    (assert (not (zerop yh1)))
	    (incf (aref xg i0)
		  (* wi/2 (/ (- (omds-term (- dx xh0) dy di oi a b)
				(omds-term (+ dx xh0) dy di oi a b))
			     xh0)))
	    (incf (aref yg i0)
		  (* wi/2 (/ (- (omds-term dx (- dy yh0) di oi a b)
				(omds-term dx (+ dy yh0) di oi a b))
			     yh0)))
	    (incf (aref xg i1)
		  (* wi/2 (/ (- (omds-term (+ dx xh1) dy di oi a b)
				(omds-term (- dx xh1) dy di oi a b))
			     xh1)))
	    (incf (aref yg i1)
		  (* wi/2 (/ (- (omds-term dx (+ dy yh1) di oi a b)
				(omds-term dx (- dy yh1) di oi a b))
			     yh1))))))))
  g)

;;;============================================================

(defclass ROMDS-Stress (OMDS-Stress)
     ((c :type Float-Scalar :accessor c	:initarg :c)
      (sigma :type Float-Scalar :accessor sigma :initarg :sigma))
  (:default-initargs :c 0.01 :sigma 1.0))

;;;------------------------------------------------------------

(defparameter *ROMDS-stress* (make-instance 'ROMDS-Stress))

;;;------------------------------------------------------------
;;; assumes compute-cached-data has been called

;;; 1/r repulsion
(defmethod romds-stress ((sc ROMDS-Stress))
  (let ((w (weight sc))
	(2d (2d sc))
	(sum 0.0))
    (dotimes (i (length w))
      (assert (plusp (aref 2d i)))
      (incf sum (/ (aref w i) (aref 2d i))))
    sum))

;;;------------------------------------------------------------      

(defmethod cactus:transform2 ((sc ROMDS-Stress) (st cactus:Direct-Sum-Point))

  (check-type (cactus:x st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:y st) cactus:Euclidean-Affine-Point)

  (compute-cached-data sc st)
  (+ (* (a sc) (mds-stress sc))
     (* (b sc) (omds-stress sc))
     (* (c sc) (romds-stress sc))))

;;;------------------------------------------------------------
;;; Gradient calculations
;;;------------------------------------------------------------
;;; assumes compute-cached-data has been called
;;; adds the mds term to gr

;;; 1/r repulsion
(defmethod add-romds-minus-grad ((sc ROMDS-Stress)
				 (gr cactus:Direct-Sum-Vector)
				 &optional (scale 1.0))
  (check-type (cactus:x gr) cactus:Euclidean-Vector)
  (check-type (cactus:y gr) cactus:Euclidean-Vector)

  (let ((index0 (index0 sc))
	(index1 (index1 sc))
	(w (weight sc))
	(dx (dx sc))
	(dy (dy sc))
	(2d (2d sc))
	(xgr (cactus::1d-array (cactus:x gr)))
	(ygr (cactus::1d-array (cactus:y gr)))) 
    (dotimes (i (number-of-pairs sc))
      (let* ((i0 (aref index0 i))
	     (i1 (aref index1 i))
	     (2di (aref 2d i))
	     (fac (progn (assert (plusp 2di))
			 (/ (* scale (aref w i))
			    (* 2di 2di 2di))))
	     (grx (* fac (aref dx i)))
	     (gry (* fac (aref dy i))))
	(incf (aref xgr i0) grx)
	(incf (aref ygr i0) gry)
	(decf (aref xgr i1) grx)
	(decf (aref ygr i1) gry)))))

;;;------------------------------------------------------------

(defmethod cactus:minus-grad ((sc ROMDS-Stress)
			      (st cactus:Direct-Sum-Point)
			      (gr cactus:Direct-Sum-Vector))

  (check-type (cactus:x st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:y st) cactus:Euclidean-Affine-Point)
  (check-type (cactus:x gr) cactus:Euclidean-Vector)
  (check-type (cactus:y gr) cactus:Euclidean-Vector)

  (compute-cached-data sc st)
  (cactus:zero! gr)
  (add-mds-minus-grad sc gr (a sc))
  (add-omds-minus-grad sc gr (b sc))
  (add-romds-minus-grad sc gr (c sc))
  gr)






