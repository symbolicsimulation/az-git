;;;-*- Package:Layout; Syntax:Common-Lisp; Mode:Lisp; -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Layout)

(proclaim '(optimize (safety 3) (space 1) (speed 1) (compilation-speed 0)))

;;;============================================================

(defclass Pedigree ()
     ((individuals
	:type Sequence
	:accessor individuals
	:initform ())
      (families
	:type Sequence
	:accessor families
	:initform ())
      (kinship-table
	:type Hash-Table
	:accessor kinship-table
	:initform (make-hash-table :test #'eq))
      (ancestor-table
	:type Hash-Table
	:accessor ancestor-table
	:initform (make-hash-table :test #'eq))
      (kinship-order-table
	:type Hash-Table
	:accessor kinship-order-table
	:initform (make-hash-table :test #'eq))
      (kinship-distance-table
	:type Hash-Table
	:accessor kinship-distance-table
	:initform (make-hash-table :test #'eq))))

;;;------------------------------------------------------------

(defmethod collect-pedigree-pairs ((p Pedigree))
  (mapcan #'(lambda (f)
	      (list* (make-pair (father f) f)
		     (make-pair (mother f) f)
		     (map 'List #'(lambda (c) (make-pair c f)) (children f))))
	  (families p)))

;;;============================================================
;;; Pedigree nodes (Individual's and Family's) only make sense with
;;; respect to a particular pedigree. A more natural approach would have
;;; the Nodes independent of any particular pedigree; kinship
;;; calculations, founder? predicates, etc., would have to take a
;;; pedigree as an additional argument.

(defclass Pedigree-Node ()
     ((pedigree
	:type Pedigree
	:accessor pedigree
	:initarg :pedigree)))

;;;============================================================

(defclass Individual (Pedigree-Node)
     ((father
	:type Individual
	:accessor father)
      (mother
	:type Individual
	:accessor mother)
      (birth-family
	:type Family
	:accessor birth-family)
      (families
	:type List
	:accessor families
	:initform ())
      (sex-code ;; (MALE=1, FEMALE=2)
	:type Fixnum
	:accessor sex-code
	:initarg :sex-code)))
  
;;;------------------------------------------------------------

(defmethod male? ((ind Individual))
  (= (sex-code ind) 1))

(defmethod female? ((ind Individual))
  (= (sex-code ind) 2))

(defmethod terminal? ((ind Individual))
  (or (not (slot-boundp ind 'families))
      (null (families ind)))) 

(defmethod founder? ((ind Individual))
  (not (or (slot-boundp ind 'father)
	   (slot-boundp ind 'mother))))

;;;------------------------------------------------------------

(defmethod get-family ((father Individual)
		       (mother Individual)
		       (pedigree Pedigree))
  (assert (and (eq (pedigree father) (pedigree mother))
	       (eq (pedigree father) pedigree)))
  (let ((fam (find-if #'(lambda (f) (and (eq father (father f))
					 (eq mother (mother f))))
		      (families pedigree))))
    (when (null fam)
      (setf fam (make-instance 'Family
				:father father
				:mother mother
				:pedigree pedigree))
      (push fam (families pedigree)))
    fam))

;;;------------------------------------------------------------

(defmethod compute-ancestor? ((a Individual) (b Individual))
  (unless (founder? b)
    (or (eq a (father b))
	(eq a (mother b))
	(ancestor? a (father b))
	(ancestor? a (mother b)))))

;;; often there are faster methods for <not-ancestor?>

(defmethod not-ancestor? ((a Individual) (b Individual))
  (not (ancestor? a b)))

;;;------------------------------------------------------------
;;; memo-ized!

(defmethod ancestor? ((a Individual) (b Individual))
  (let* ((outer-table (ancestor-table (pedigree a)))
	 (inner-table (gethash a outer-table)))
    (when (null inner-table)
      (setf inner-table (make-hash-table :test #'eq))
      (setf (gethash a outer-table) inner-table))
    (let ((p (gethash b inner-table)))
      (when (null p)
	(setf p (compute-ancestor? a b))
	(setf (gethash b inner-table) p))
      p)))

;;;============================================================

(defclass Family (Pedigree-Node)
     ((father
	:type Individual
	:accessor father
	:initarg :father)
      (mother
	:type Individual
	:accessor mother
	:initarg :mother)
      (children
	:type List ;; of Individual's
	:accessor children
	:initform ())))

;;;============================================================

(defmethod parent? ((ind Individual) (family Family))
  (or (eq ind (father family))
      (eq ind (mother family))))

(defmethod child? ((ind Individual) (family Family))
  (member ind (children family) :test #'eq))

(defmethod family-member? ((ind Individual) (family Family))
  (or (parent? ind family)
      (child? ind family)))

;;;------------------------------------------------------------

(defmethod compute-kinship ((a Individual) (b Individual))
  ;; assumes (not-ancestor? a b)
  (cond
    ((eq a b)
     (if (founder? a)
	 0.5
	 (* 0.5 (+ 1.0 (kinship (father a) (mother a))))))
    (t 
     (if (founder? a)
	 0.0
	 (* 0.5 (+ (kinship (father a) b) (kinship (mother a) b)))))))

;;; memo-ized!

(defmethod kinship ((a Individual) (b Individual))
  (assert (eq (pedigree a) (pedigree b)))
  (unless (not-ancestor? a b) (rotatef a b))
  (let* ((kinship-table (kinship-table (pedigree a)))
	 (inner-table (gethash a kinship-table)))
    (when (null inner-table)
      (setf inner-table (make-hash-table :test #'eq))
      (setf (gethash a kinship-table) inner-table))
    (let ((k (gethash b inner-table)))
      (when (null k)
	(setf k (compute-kinship a b))
	(setf (gethash b inner-table) k))
      k)))

;;;------------------------------------------------------------

(defmethod compute-kinship-distance ((a Individual) (b Individual)) 
  (- (log (/ (+ 0.00001 (kinship a b)) 1.00001))))

(defmethod compute-kinship-distance ((h Individual) (f Family))
  (let ((min most-positive-single-float)
	(dist most-positive-single-float)
	(closest nil))
    (cond ((parent? h f) ;; half the distance to the closest child
	   (dolist (c (children f))
	     (setf dist (kinship-distance h c))
	     (when (< dist min) (setf min dist)))
	   (/ min 3.0))
	  ((child? h f) ;; half the distance to the closest parent
	   (/ (min (kinship-distance h (father f))
		   (kinship-distance h (mother f))) 3.0))
	  (t ;; not a member of the family;
	   ;; use distance to closest member of family plus the distance
	   ;; from that member to the family node
	   (dolist (c (children f))
	     (setf dist (kinship-distance h c))
	     (when (< dist min) (setf min dist) (setf closest c)))
	   (setf dist (kinship-distance h (father f)))
	   (when (< dist min) (setf min dist) (setf closest (father f)))
	   (setf dist (kinship-distance h (mother f)))
	   (when (< dist min) (setf min dist) (setf closest (mother f)))
	   (+ min (kinship-distance closest f))))))

(defmethod compute-kinship-distance ((f Family) (h Individual))
  (compute-kinship-distance h f))

(defmethod compute-kinship-distance ((f0 Family) (f1 Family))
  ;; return the distance from f0 to closest member of f1,
  ;; plus the distance from that member to f1
  (let ((min most-positive-single-float)
	(dist most-positive-single-float)
	(closest nil))
    (dolist (c (children f1))
      (setf dist (kinship-distance f0 c))
      (when (< dist min) (setf min dist) (setf closest c)))
    (setf dist (kinship-distance f0 (father f1)))
    (when (< dist min) (setf min dist) (setf closest (father f1)))
    (setf dist (kinship-distance f0 (mother f1)))
    (when (< dist min) (setf min dist) (setf closest (mother f1)))
    (+ min (kinship-distance closest f1))))

;;;------------------------------------------------------------
;;; memo-ized!

(defun kinship-distance (a b)
  (assert (eq (pedigree a) (pedigree b)))
  (let* ((kinship-distance-table (kinship-distance-table (pedigree a)))
	 (inner-table (gethash a kinship-distance-table)))
    (when (null inner-table)
      (setf inner-table (make-hash-table :test #'eq))
      (setf (gethash a kinship-distance-table) inner-table))
    (let ((k (gethash b inner-table)))
      (when (null k)
	(setf k (compute-kinship-distance a b))
	(setf (gethash b inner-table) k))
      k)))

;;;------------------------------------------------------------
;;; a simple distance:
;;; 1.0 for every edge in the pedigree graph,
;;; -1.0 (meaning ignore this edge) for any other pair of nodes

(defmethod family-distance ((a T) (b T)) -1.0)

(defmethod family-distance ((a Individual) (b Family))
  (if (family-member? a b) 1.0 -1.0))

(defmethod family-distance ((b Family) (a Individual))
  (family-distance a b))

;;;------------------------------------------------------------
;;; a partial ordering

(defmethod family-order ((a T) (b T)) 0.0)

(defmethod family-order ((a Individual) (b Family))
  (cond ((parent? a b) 1.0)
	((child? a b) -1.0)
	(t 0.0)))

(defmethod family-order ((a Family) (b Individual))
  (cond ((parent? b a) -1.0)
	((child? b a) 1.0)
	(t 0.0)))

;;;------------------------------------------------------------

(defmethod kinship-order ((a Individual) (b Individual))
  (cond ((ancestor? a b) 1.0)
	((ancestor? b a) -1.0)
	(t 0.0)))

(defmethod kinship-order ((a Family) (b Individual))
  (cond ((or (member b (children a))
	     (find-if #'(lambda (c) (ancestor? c b)) (children a)))
	 1.0)
	((or (eql b (father a))
	     (eql b (mother a))
	     (ancestor? b (father a))
	     (ancestor? b (mother a)))
	 -1.0)
	(t
	 0.0)))

(defmethod kinship-order ((a Individual) (b Family))
  (- (kinship-order b a)))

(defmethod kinship-order ((a Family) (b Family)) 0.0)

;;;============================================================
;;;
;;; Now the Horses:
;;;

#||
From charlie@elk.stat Thu Nov 10 22:45:23 1988
Received: by entropy.ms.washington.edu (5.52.1/6.12)
	id AA25787; Thu, 10 Nov 88 22:45:18 PST
Received: by elk.stat.washington.edu (5.51/6.12)
	id AA05205; Thu, 10 Nov 88 22:45:07 PST
Date: Thu, 10 Nov 88 22:45:07 PST
From: charlie@elk.stat (Charlie Geyer)
Message-Id: <8811110645.AA05205@elk.stat.washington.edu>
To: jam@entropy.ms
Subject: Pedigree Example
Status: R

There is an example pedigree of horses in the file

  /mica/charlie/pedigree.example

on the RT's.  The pedigree has 47 individuals, one per line of the
file.  Each line has 10 items of data on the individual.

  (1) name (actually a number, the international studbook number.  The
      horses have names, but not in the file I extracted)

  (2) father's name (same kind of number), NA if the individual is a
      founder

  (3) mother's name (same kind of number), NA if the individual is a
      founder

  (4) sex (MALE=1, FEMALE=2)

  (5) place of birth

  (6) country of birth

  (7) current location (as of sometime last year)

  (8) country of current location

  (9) date of birth (in the form yymmdd, where yy is the last two
      digits of the year, mm the number of the month, and dd is the
      day of the month.  Some of the early dates are approximate, the
      ones with zeros for month and day)

 (10) date of death (same format), NA if still alive.

You can construct the pedigree from knowing the parents of each
individual.  It may help to know that the pedigree is actually
ordered so that each individual follows his parents in the file.  This
helps in building a list with only links back to parents.

The best indicator of relationship is the kinship coefficient defined
as follows:

  k(A,B) = k(B,A)

so it is only necessary to know how to compute k(A,B) if A is not an
ancestor of B.  Assume this.  Then if A is not a founder

  k(A,B) = [ k(p(A),B) + k(m(A),B) ] / 2

if A is not B and

  k(A,A) = [ 1 + k(p(A),m(A)) ] / 2

where p(A) and m(A) denote the parents of A.  And if A is a founder
(recall A is not an ancestor of B), then

  k(A,B) = 0

if A is not B and

  k(A,A) = 1/2

A nice easy recursive function to program.  Note that "A is not an
ancestor of B" is true if A follows B in the file.
||#
;;;============================================================

(defparameter *horse-records*
	      '(#(1 -1 -1 1 bijsk ssr askania-nova ssr 010000 150929)
		#(5 -1 -1 2 bijsk ssr askania-nova ssr 010000 150906)
		#(17 -1 -1 1 bijsk ssr washington usa 010000 211127)
		#(18 -1 -1 2 bijsk ssr cincinnati usa 010000 350000)
		#(39 -1 -1 1 hagenbeck brd new-york usa 020000 190619)
		#(40 -1 -1 2 hagenbeck brd new-york usa 020000 231129)
		#(52 -1 -1 2 bijsk ssr askania-nova ssr 020000 150515)
		#(100 39 40 2 new-york usa new-york usa 090502 260701)
		#(101 39 40 2 new-york usa philadelphia usa 110910 360131)
		#(103 39 40 1 new-york usa sidney aus 120906 450225)
		#(107 39 100 2 new-york usa sidney aus 160712 330626)
		#(113 17 18 1 cincinnati usa philadelphia usa 120215 430820)
		#(118 113 101 1 philadelphia usa washington usa 230608 500118)
		#(119 113 101 2 philadelphia usa washington usa 260713 590606)
		#(121 118 119 2 washington usa munchen brd 320425 490716)
		#(211 -1 -1 1 woburn eng london eng 010000 331218)
		#(212 -1 -1 2 woburn eng london eng 010000 391025)
		#(222 103 107 2 sidney aus sidney aus 231123 361031)
		#(224 103 222 2 sidney aus munchen brd 290522 510720)
		#(421 1 5 1 askania-nova ssr askania-nova ssr 050525 350114)
		#(429 1 52 2 askania-nova ssr askania-nova ssr 140427 351201)
		#(430 1 5 1 askania-nova ssr leningrad ssr 140510 330400)
		#(180 211 212 1 london eng whipsnade eng 310815 621217)
		#(182 180 212 1 whipsnade eng munchen brd 370509 530706)
		#(187 421 429 1 askania-nova ssr schorfheide ddr 200514 450000)
		#(189 430 429 2 askania-nova ssr schorfheide ddr 260616 450000)
		#(198 187 189 2 berlin-west ddr schorfheide ddr 310713 450000)
		#(140 187 198 2 munchen brd munchen brd 350530 450601)
		#(143 187 140 2 munchen brd munchen brd 380616 490713)
		#(149 182 140 2 munchen brd catskill usa 450427 721011)
		#(150 182 143 1 munchen brd catskill usa 460515 720808)
		#(151 182 121 2 munchen brd catskill usa 460515 640420)
		#(154 182 224 1 munchen brd bern swi 480517 750712)
		#(157 182 224 1 munchen brd catskill usa 500513 720913)
		#(163 182 149 2 munchen brd catskill usa 520711 770516)
		#(168 154 151 2 munchen brd catskill usa 550620 -1)
		#(171 154 149 1 munchen brd catskill usa 560711 750822)
		#(228 157 151 2 catskill usa catskill usa 580404 -1)
		#(277 150 151 2 catskill usa catskill usa 620627 -1)
		#(319 150 163 2 catskill usa san-pasqual usa 640806 810407)
		#(320 171 168 1 catskill usa san-diego usa 640822 770807)
		#(341 171 168 1 catskill usa catskill usa 650831 780911)
		#(458 320 319 2 san-diego usa san-pasqual usa 700210 -1)
		#(469 171 228 1 catskill usa marwell eng 700511 -1)
		#(568 341 277 1 catskill usa san-pasqual usa 730820 -1)
		#(961 469 458 2 san-pasqual usa san-pasqual usa 810505 -1)
		#(2100 568 961 2 san-pasqual usa san-pasqual usa 871201 -1)
		)) 

;;;============================================================

(defconstant *month-days-alist*
	     '((0 0 0) (1 31 0) (2 29 31) (3 31 60) (4 30 91) (5 31 121)
	       (6 30 152)
	       (7 31 182) (8 31 213) (9 30 244) (10 31 274) (11 30 305)
	       (12 31 335)))

(defun convert-date (packed-date)
  (declare (type Fixnum packed-date))
  (let* ((year (truncate packed-date 10000))
	 (month (truncate (- packed-date (* year 10000)) 100))
	 (days-so-far (third (assoc month *month-days-alist*)))
	 (month-day (- packed-date (* year 10000) (* month 100)))
	 (year-day (+ days-so-far month-day)))
    (+ year (/ year-day 366.0))))
  
;;;============================================================

(defclass Horse (Individual)
     ((record-number ;; provides a cheap way to test for not-ancestor,
	:type Fixnum ;; since it preserves the ancestry partial ordering.
	:accessor record-number 
	:initarg :record-number)
      (studbook-number
	:type Fixnum
	:accessor studbook-number
	:initarg :studbook-number)
      (birth-place
	:type Symbol
	:accessor birth-place
	:initarg :birth-place)
      (birth-country
	:type Symbol
	:accessor birth-country
	:initarg :birth-country)
      (current-place
	:type Symbol
	:accessor current-place
	:initarg :current-place)
      (current-country
	:type Symbol
	:accessor current-country
	:initarg :current-country)
      (birth-date ;; fractional years since 1900
	:type Fixnum
	:accessor birth-date
	:initarg :birth-date) 
      (death-date ;; fractional years since 1900
	:type Fixnum
	:accessor death-date
	:initarg :death-date)))

;;;============================================================

(defun find-horse (studbook-number pedigree)
  (if (minusp studbook-number)
      nil
      (find-if #'(lambda (horse) (= studbook-number (studbook-number horse)))
	       (individuals pedigree))))

;;;------------------------------------------------------------

;; fast test for <not-ancestor?> based on ordering of records:
(defmethod not-ancestor? ((h0 Horse) (h1 Horse))
    (>= (record-number h0) (record-number h1)))

(defmethod ancestor? ((a Horse) (b Horse))
  (unless (or (founder? b) (not-ancestor? a b))
    (or (eq a (father b))
	(eq a (mother b))
	(ancestor? a (father b))
	(ancestor? a (mother b)))))

;;;------------------------------------------------------------

(defmethod birth-order ((a T) (b T)) 0.0)

(defmethod birth-order ((a Horse) (b Horse))
  (let ((date-a (birth-date a))
	(date-b (birth-date b)))
    (cond
      ((or (minusp date-a)
	   (minusp date-b)
	   (= date-a date-b))
       0.0)
      ((< date-a date-b)
       1.0)
      ((> date-a date-b)
       -1.0))))

;;;------------------------------------------------------------

(defmethod approx-date ((a Horse))
  (if (plusp (death-date a))
      (* 0.5 (+ (birth-date a) (death-date a)))
      (float (birth-date a))))

(defmethod approx-date ((a Family))
  (* 0.5 (+ (approx-date (father a))
	    (approx-date (mother a)))))

;;;============================================================

(defun make-horse-pedigree ()
  (let ((pedigree (make-instance 'pedigree))
	(record-number 0))
    (map nil
	 #'(lambda (hr)
	     (let* ((h (make-instance
			 'Horse
			 :record-number record-number
			 :studbook-number (aref hr 0)
			 :sex-code (aref hr 3)
			 :birth-place (aref hr 4)
			 :birth-country (aref hr 5)
			 :current-place (aref hr 6)
			 :current-country (aref hr 7)
			 :birth-date (convert-date (aref hr 8))
			 :death-date (convert-date (aref hr 9))
			 :pedigree pedigree))
		    (pa (find-horse (aref hr 1) pedigree))
		    (ma (find-horse (aref hr 2) pedigree)))
	       (unless (null pa) (setf (slot-value h 'father) pa))
	       (unless (null ma) (setf (slot-value h 'mother) ma))
	       (push h (individuals pedigree))
	       (incf record-number)))
	 *horse-records*)

    ;; fill in the family info
    (map nil #'(lambda (h)
		 (unless (founder? h)
		   (let ((family (get-family (father h) (mother h) pedigree)))
		     (pushnew h (children family))
		     (setf (birth-family h) family))))
	 (individuals pedigree))
    (map nil #'(lambda (f)
		 (pushnew f (families (father f)))
		 (pushnew f (families (mother f))))
	 (families pedigree))
    pedigree))

(defparameter *horse-pedigree* (make-horse-pedigree))

;;;============================================================
;;; pens for pedigrees:

(defparameter *default-pen* (slate:make-pen))
(defparameter *gray-pen* (slate:make-pen :shade :gray-shade))
(defparameter *light-gray-pen* (slate:make-pen :shade :light-gray-shade))

;;;============================================================
;;; glyphs for pedigrees:

(defparameter *family-glyph* nil)
(defparameter *male-glyph* nil)
(defparameter *female-glyph* nil)
(defparameter *male-founder-glyph* nil)
(defparameter *female-founder-glyph* nil)
(defparameter *male-terminal-glyph* nil)
(defparameter *female-terminal-glyph* nil)

(defun family-glyph (slate) 
  (check-type slate slate:Slate)
  (when (null *family-glyph*)
    (setf *family-glyph* (slate:make-similar-invisible-slate slate))
    (slate:draw-filled-rectangle-xy *default-pen* *family-glyph* 0 1 4 5)))

(defun male-glyph (slate)
  (check-type slate slate:Slate)
  (when (null *male-glyph*)
    (setf *male-glyph* (slate:make-similar-invisible-slate slate))
    (slate:draw-rectangle-xy *default-pen* *male-glyph* 0 0 10 10 ))
  *male-glyph*)

(defun female-glyph (slate)
  (check-type slate slate:Slate)
  (when (null *female-glyph*)
    (setf *female-glyph* (slate:make-similar-invisible-slate slate))
    (slate:draw-circle-xy *default-pen* *female-glyph* 5 5 5))
  *female-glyph*)

(defun male-founder-glyph (slate)
  (check-type slate slate:Slate)
  (when (null *male-founder-glyph*)
    (setf *male-founder-glyph* (slate:make-similar-invisible-slate slate))
    (slate:draw-rectangle-xy *default-pen* *male-glyph* 0 0 10 10 )
    (slate:draw-filled-rectangle-xy *gray-pen* *male-glyph* 0 0 10 10))
  *male-founder-glyph*)

(defun female-founder-glyph (slate)
  (check-type slate slate:Slate)
  (when (null *female-founder-glyph*)
    (setf *female-founder-glyph* (slate:make-similar-invisible-slate slate))
    (slate:draw-circle-xy *default-pen* *female-founder-glyph* 5 5 5)
    (slate:draw-filled-circle-xy *gray-pen* *female-founder-glyph* 5 5 5))
  *female-founder-glyph*)

(defun male-terminal-glyph (slate)
  (check-type slate slate:Slate)
  (when (null *male-terminal-glyph*)
    (setf *male-terminal-glyph* (slate:make-similar-invisible-slate slate))
    (slate:draw-rectangle-xy *default-pen* *male-glyph* 0 0 10 10 )
    (slate:draw-filled-rectangle-xy *light-gray-pen* *male-glyph* 0 0 10 10))
  *male-terminal-glyph*)

(defun female-terminal-glyph (slate)
  (check-type slate slate:Slate)
  (when (null *female-terminal-glyph*)
    (setf *female-terminal-glyph*
	  (slate:make-similar-invisible-slate slate))
    (slate:draw-circle-xy *default-pen* *female-terminal-glyph* 5 5 5)
    (slate:draw-filled-circle-xy *light-gray-pen* *female-terminal-glyph*
				  5 5 5))
  *female-terminal-glyph*)


;;;============================================================

(defclass Pedigree-Configuration-Space (Directed-Graph-Configuration-Space)
     ((pedigree
	:type Pedigree
	:accessor pedigree
	:initarg :pedigree)))

;;;------------------------------------------------------------

(defmethod make-edge-plot-objects ((lc Pedigree-Configuration-Space))
  (let ((table (single-point-table lc)))
    (tools:with-collection
      (map
	nil
	#'(lambda (single point)
	    ;; let the families do the work
	    (when (typep single 'Family)
	      (let ((pos (pet:point-position point))
		    (father-pos
		      (pet:point-position (gethash (father single) table)))
		    (mother-pos
		      (pet:point-position (gethash (mother single) table))) 
		    (children (children single)))
		(tools:collect (pet:make-arrow father-pos pos))
		(tools:collect (pet:make-arrow mother-pos pos))
		(dolist (child children)
		  (let ((child-pos (pet:point-position (gethash child table))))
		    (collect (pet:make-arrow pos child-pos)))))))
	(subjects lc) (points lc)))))

;;;------------------------------------------------------------
;;; Warning: this assumes that the point plot objects have
;;; already been created. It also assumes that the <subjects>
;;; and <points> sequences are in matching order.

(defmethod make-pedigree-Configuration-Space ((p Pedigree)
				 &key
				 (dist-fn 'family-distance)
				 (weight-fn 'trivial-weight)
				 (order-fn 'family-order))
  (let* ((subjects (concatenate 'Vector (individuals p) (families p)))
	 (glyphs
	   (map 'Vector
		#'(lambda (s)
		    (if (typep s 'Horse)
			(if (male? s)
			    (cond ((founder? s) *male-founder-glyph*)
				  ((terminal? s) *male-terminal-glyph*)
				  (t *male-glyph*))
			    (cond ((founder? s) *female-founder-glyph*)
				  ((terminal? s) *female-terminal-glyph*)
				  (t *female-glyph*)))
			*family-glyph*))
		subjects)))
    (make-instance
      'Pedigree-Configuration-Space
      :pedigree p
      :glyphs glyphs
      :subjects subjects
      :dist-fn dist-fn
      :weight-fn weight-fn
      :order-fn order-fn)))

(defparameter *horse-pedigree-Configuration-Space*
	      (make-pedigree-Configuration-Space *horse-pedigree*
				    :order-fn 'family-order)) 

(defparameter *horse-pedigree-start* (copy (state *horse-pedigree-Configuration-Space*)))

;;;============================================================

(defclass Kinship-Configuration-Space (Pedigree-Configuration-Space) ())
		  
;;;------------------------------------------------------------
;;; Warning: this assumes that the point plot objects have
;;; already been created. It also assumes that the <subjects>
;;; and <points> sequences are in matching order.

(defmethod make-kinship-Configuration-Space ((p Pedigree)
				&key
				(dist-fn 'kinship-distance)
				(weight-fn 'standard-weight)
				(order-fn 'kinship-order))
  (let* ((subjects (concatenate 'Vector (individuals p) (families p)))
	 (glyphs
	   (map 'Vector
		#'(lambda (s)
		    (if (typep s 'Horse)
			(if (male? s)
			    (cond ((founder? s) :male-founder-glyph)
				  ((terminal? s) :male-terminal-glyph)
				  (t :male-glyph))
			    (cond ((founder? s) :female-founder-glyph)
				  ((terminal? s) :female-terminal-glyph)
				  (t :female-glyph)))
			:family-glyph))
		subjects)))
    (make-instance
      'Kinship-Configuration-Space
      :pedigree p
      :glyphs glyphs
      :subjects subjects
      :dist-fn dist-fn
      :weight-fn weight-fn
      :order-fn order-fn)))

(defparameter *horse-kinship-Configuration-Space*
	      (make-kinship-Configuration-Space *horse-pedigree*
				   :order-fn 'kinship-order)) 

(defparameter *horse-kinship-start* (copy (state *horse-kinship-Configuration-Space*)))
