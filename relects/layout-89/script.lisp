;;;-*- Package: Layout; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1990. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Layout)

(proclaim '(optimize (safety 3) (space 1) (speed 1) (compilation-speed 0))) 

;;;============================================================


(redraw-plot *plot* *slate*)

;;;============================================================

(initialize-objective-function *2d-circle-configuration-space* *mds-stress*)

(initialize-objective-function *3d-circle-configuration-space* *mds-stress*)

(initialize-objective-function
  *2d-circle-graph-configuration-space* *mds-stress*)

(initialize-objective-function
  *horse-kinship-configuration-space* *mds-stress*)
(initialize-objective-function
  *horse-kinship-configuration-space* *omds-stress*)
(initialize-objective-function
  *horse-kinship-configuration-space* *romds-stress*)

(initialize-objective-function
  *horse-pedigree-configuration-space* *romds-stress*)

;;;============================================================

(optimize cactus::*amoeba-opt* *mds-stress* (copy *2d-circle-start*))
(optimize cactus::*sd-opt* *mds-stress* (copy *2d-circle-start*))
(optimize cactus::*dsd-opt* *mds-stress* (copy *2d-circle-start*))


(optimize cactus::*cg-opt* *mds-stress* (copy *2d-circle-start*))

(optimize cactus::*dcg-opt* *mds-stress* (copy *2d-circle-start*))
(optimize cactus::*idfp-opt* *mds-stress* (copy *2d-circle-start*))
(optimize cactus::*didfp-opt* *mds-stress* (copy *2d-circle-start*))
(optimize cactus::*ibfgs-opt* *mds-stress* (copy *2d-circle-start*))
(optimize cactus::*dibfgs-opt* *mds-stress* (copy *2d-circle-start*))

(optimize cactus::*amoeba-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*sd-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*dsd-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*cg-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*dcg-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*idfp-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*didfp-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*ibfgs-opt* *mds-stress* (copy *3d-circle-start*))
(optimize cactus::*dibfgs-opt* *mds-stress* (copy *3d-circle-start*))

(optimize cactus::*cg-opt* *mds-stress* (copy *2d-circle-graph-start*))
(optimize cactus::*dcg-opt* *mds-stress* (copy *2d-circle-graph-start*))


(optimize cactus::*cg-opt* *mds-stress* (copy *horse-kinship-start*))
(optimize cactus::*cg-opt* *omds-stress* (copy *horse-kinship-start*))
(optimize cactus::*dcg-opt* *omds-stress* (copy *horse-kinship-start*))

(optimize cactus::*cg-opt* *romds-stress* (copy *horse-pedigree-start*))
(optimize cactus::*dcg-opt* *romds-stress* (copy *horse-pedigree-start*))

