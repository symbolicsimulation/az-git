;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defclass Muchi-Presentation (Presentation Display-Leaf)
	  ((subject
	    :type Muchi
	    :accessor subject)))

;;;============================================================

(defclass Muchi-Image-Presentation (Muchi-Presentation)
	  ((magnification
	    :type slate:Magnification-Level
	    :accessor magnification)
	   (channel
	    :type Tools:Array-Index
	    :accessor channel)
	   (paint-overlay-rect
	    :type g:Screen-Rect
	    :reader paint-overlay-rect)))

;;;============================================================

(defun make-presentation-for-muchi (Muchi slate &key (channel 0))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0))
	   (type Muchi-Image-Presentation display)
	   (type List build-options))
  (let ((display (allocate-instance (find-class 'Muchi-Image-Presentation))))
    (initialize-display display
			:build-options (list :subject Muchi
					     :slate slate
					     :channel channel))
    display))

;;;============================================================

(defmethod slate-rect ((display Muchi-Image-Presentation))
  (slate:slate-rect (display-slate display)))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Muchi-Image-Presentation)
				build-options)
  (declare (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Muchi-Image-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject)))
    (declare (type slate:Slate slate))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp) (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; local slots:
    (setf (slot-value display 'channel) (getf build-options :channel 0))
    (setf (slot-value display 'paint-overlay-rect) (g:make-screen-rect))

    ;; slots inherited from Presentation:
    (setf (slot-value display 'subject) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject))
    (setf (display-name display)
      (getf build-options :display-name
	    (format nil "Channel ~d of ~s" (channel display) subject)))

    ;; input interactors
    (setf (current-interactor display) (build-interactors display))
    ))

;;;------------------------------------------------------------

(defmethod build-interactors ((display Muchi-Image-Presentation))
  (setf (painter display)
    (make-instance 'Muchi-Image-Painter :interactor-display display))
  (setf (default-interactor display)
    (make-instance 'Presentation-Interactor :interactor-display display)))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((display Muchi-Image-Presentation)
				 layout-options)
  (declare (ignore layout-options)
	   (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Muchi-Image-Presentation display))

  (let* ((muchi (subject display))
	 (muchi-width (muchi-ncols muchi))
	 (muchi-height (muchi-nrows muchi))
	 (slate (display-slate display))
	 (magnification
	  (min (truncate (slate:slate-width slate) muchi-width)
	       (truncate (slate:slate-height slate) muchi-height)))
	 (display-rect (slate:slate-rect slate)))
    (declare (type Muchi muchi)
	     (type slate:Slate slate)
	     (type g:Positive-Screen-Coordinate muchi-width muchi-height)
	     (type slate:Magnification-Level magnification)
	     (type g:Screen-Rect display-rect))
    (setf (magnification display) magnification)
    (setf (g:screen-rect-origin display-rect) (slate:slate-origin slate))
    (setf (g:screen-rect-width display-rect)
      (* magnification muchi-width))
    (setf (g:screen-rect-height display-rect)
      (* magnification muchi-height))
    display))

;;;------------------------------------------------------------

(defmethod apply-lens (lens (display Muchi-Image-Presentation))
  (declare (ignore lens))
  (setf (slot-value display 'display-pen) (subject-pen (subject display))))

;;;------------------------------------------------------------
;;; Handling :configure-notify events
;;;------------------------------------------------------------

(defmethod reconfigure-display ((display Muchi-Image-Presentation)
				 width height)
  (declare (type g:Positive-Screen-Coordinate width height))
  
  (let* ((muchi (subject display))
	 (muchi-width (muchi-ncols muchi))
	 (muchi-height (muchi-nrows muchi))
	 (magnification (min (truncate width muchi-width)
			     (truncate height muchi-height))))
    (declare (type Muchi muchi)
	     (type g:Positive-Screen-Coordinate muchi-width muchi-height)
	     (type slate:Magnification-Level magnification))
    (unless (= (magnification display) magnification)
      (erase-display display nil)
      (layout-display display nil)
      (draw-display display)
      (slate:finish-drawing (display-slate display)))))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod draw-display ((display Muchi-Image-Presentation))
  (draw-magnified-muchi-channel
   (display-pen display) (display-slate display)
   (subject display) (channel display)
   (magnification display))
  ;;(tools:stamp-time! (redraw-timestamp display))
  )

;;;------------------------------------------------------------

(defmethod erase-display ((display Muchi-Image-Presentation) rect)
  (let ((slate (display-slate display)))
    (slate:with-clipping-region (slate (clipping-region display))
     (setf (slate:pen-pixel-value (erasing-fill-pen))
       (slate:slate-background-pixel-value slate))
     (slate::%draw-rect (erasing-fill-pen) slate (slate-rect display)))
    (unless (null rect)
      (g::%copy-screen-rect (slate-rect display) rect))))

;;;============================================================

#||
(defmethod paint-presentation ((display Muchi-Image-Presentation)
			       pen muchi-rect)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   ;;(:explain :types :calls)
	   (type Muchi-Image-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect muchi-rect))

  (let* ((mag (magnification display))
	 (paint-overlay-rect (paint-overlay-rect display)))
    (declare (type slate:Magnification-Level mag)
	     (type g:Screen-Rect paint-overlay-rect))

    ;; convert to screen coordinates and do a quick redraw
    (setf (g::%screen-rect-xmin paint-overlay-rect)
      (* mag (g::%screen-rect-xmin muchi-rect)))
    (setf (g::%screen-rect-ymin paint-overlay-rect)
      (* mag (g::%screen-rect-ymin muchi-rect)))
    (setf (g::%screen-rect-width paint-overlay-rect)
      (* mag (g::%screen-rect-width muchi-rect)))
    (setf (g::%screen-rect-height paint-overlay-rect)
      (* mag (g::%screen-rect-height muchi-rect)))

    (slate::%draw-rect pen (display-slate display) paint-overlay-rect)))
||#

(defmethod paint-presentation ((display Muchi-Image-Presentation)
			       pen muchi-rect)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   ;;(:explain :types :calls)
	   (type Muchi-Image-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect muchi-rect))

  (let* ((mag (magnification display))
	 (drawable (slate::slate-drawable (display-slate display)))
	 (gcontext 
	  (svref *painting-gcontexts* (ash (slate::%pen-pixel-value pen) -5))))
    (declare (type slate:Magnification-Level mag)
	     (type xlib:Drawable drawable)
	     (type xlib:Gcontext gcontext))

    ;; convert to screen coordinates and do a quick draw
    (xlib:draw-rectangle
     drawable gcontext
     (* mag (g::%screen-rect-xmin muchi-rect))
     (* mag (g::%screen-rect-ymin muchi-rect))
     (* mag (g::%screen-rect-width muchi-rect))
     (* mag (g::%screen-rect-height muchi-rect))
     t)))


;;;------------------------------------------------------------

(defmethod paint-stroke ((display Muchi-Image-Presentation) 
			 (stroke Muchi-Image-Stroke)
			 &rest options)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (ignore options))

  (let* ((mag (magnification display))
	 (drawable (slate::slate-drawable (display-slate display)))
	 (rects (stroked-rectangles stroke))
	 (pen (stroke-pen stroke))
	 (gcontext 
	  (svref *painting-gcontexts* (ash (slate::%pen-pixel-value pen) -5))))
    (declare (type slate:Magnification-Level mag)
	     (type xlib:Drawable drawable)
	     (type List rects)
	     (type slate:Pen pen)
	     (type xlib:Gcontext gcontext))

    (dolist (r rects)
      (declare (type g:Screen-Rect r))
      ;; convert to screen coordinates and do a quick draw
      (xlib:draw-rectangle
       drawable gcontext
       (* mag (g::%screen-rect-xmin r)) (* mag (g::%screen-rect-ymin r))
       (* mag (g::%screen-rect-width r)) (* mag (g::%screen-rect-height r))
       t))

    (slate:force-drawing (display-slate display))

    (tools:atomic
     (tools:deletef display (stroke-presentations-to-be-updated stroke))
     (when (null (stroke-presentations-to-be-updated stroke))
       (return-stroke stroke)))))

;;;------------------------------------------------------------

(defmethod paint-stroke ((display Muchi-Image-Presentation)
			 (stroke Muchi-Scatter-Stroke)
			 &rest options)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (ignore options))

  (let* ((mag (magnification display))
	 (drawable (slate::slate-drawable (display-slate display)))
	 (stroked-bins (stroked-bins stroke))
	 (bins (bins (stroked-presentation stroke)))
	 (pen (stroke-pen stroke))
	 (gcontext 
	  (svref *painting-gcontexts*
		 (ash (the (Unsigned-Byte 8) (slate::%pen-pixel-value pen))
		      -5))))
    (declare (type slate:Magnification-Level mag)
	     (type xlib:Drawable drawable)
	     (type (Simple-Array (Unsigned-Byte 8) (256 256)) stroked-bins)
	     (type 2d-Muxel-Table bins)
	     (type slate:Pen pen)
	     (type xlib:Gcontext gcontext))

    (dotimes (i 256)
      (declare (type tools:Array-Index i))
      (dotimes (j 256)
	(declare (type tools:Array-Index j))
	(when (= 1 (aref stroked-bins i j))
	  (let ((bin (aref bins i j)))
	    (declare (type List bin))
	    (unless (null bin)
	      (dotimes (k (array-dimension bin 0))
		(declare (type tools:Array-Index k))
		(let ((pad (aref bin k)))
		  (declare (type Pixel-Address pad))
		  (xlib:draw-rectangle
		   drawable gcontext
		   (* mag (the Image-Coordinate (%pixel-address-x pad)))
		   (* mag (the Image-Coordinate (%pixel-address-y pad)))
		   mag mag t))))))))

    (slate:force-drawing (display-slate display))

    (tools:atomic
     (tools:deletef display (stroke-presentations-to-be-updated stroke))
     (when (null (stroke-presentations-to-be-updated stroke))
       (return-stroke stroke)))))

;;;------------------------------------------------------------

(defmethod (setf channel) (new-channel (display Muchi-Image-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'channel) new-channel)
  (draw-display display)
  (activate-display display nil))

;;;============================================================

(defun compute-muchi-rect! (display rect &key (result (g:make-screen-rect)))
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   ;;(:explain :types :calls)
	   (type Muchi-Image-Presentation display)
	   (type g:Screen-Rect rect result))
  (let* ((muchi (subject display))
	 (mag (magnification display))
	 (xmin (g::%screen-rect-xmin rect))
	 (ymin (g::%screen-rect-ymin rect))
	 (xmax (+ xmin (g::%screen-rect-width rect)))
	 (ymax (+ ymin (g::%screen-rect-height rect)))
	 (nrows (muchi-nrows muchi))
	 (ncols (muchi-ncols muchi)))
    (declare (type Muchi muchi)
	     (type slate:Magnification-Level mag)
	     (type g:Screen-Coordinate xmin ymin xmax ymax nrows ncols))

    ;; compute clipped region in image coordinates
    (setf xmin (max 0 (min ncols (floor xmin mag))))
    (setf ymin (max 0 (min nrows (floor ymin mag))))
    (setf (g::%screen-rect-xmin result) xmin)
    (setf (g::%screen-rect-ymin result) ymin)
    (setf (g::%screen-rect-width result)
      (- (max 0 (min ncols (ceiling xmax mag))) xmin))
    (setf (g::%screen-rect-height result)
      (- (max 0 (min nrows (ceiling ymax mag))) ymin))
    result))

;;;============================================================
;;; Interactors for Muchi-Image-Presentations
;;;============================================================

(defclass Muchi-Painter (Painter)
	  ((interactor-display
	    :type Muchi-Presentation
	    :accessor interactor-display
	    :initarg :interactor-display)))

(defclass Muchi-Image-Painter (Muchi-Painter)
	  ((interactor-display
	    :type Muchi-Image-Presentation
	    :accessor interactor-display
	    :initarg :interactor-display)
	   (muchi-rect
	    :type g:Screen-Rect
	    :reader muchi-rect
	    :initform (g:make-screen-rect))
	   (stroke
	    :type Muchi-Image-Stroke
	    :initform ()
	    :accessor stroke)))

;;;============================================================
;;; methods for Muchi-Image-Painter
;;;============================================================

(defmethod borrow-stroke ((painter Muchi-Image-Painter))
  (let* ((presentation (interactor-display painter))
	 (stroke (pop *muchi-image-strokes*)))
    (when (null stroke) (setf stroke (make-instance 'Muchi-Image-Stroke)))
    (setf (stroke-interactor stroke) painter)
    (setf (stroked-presentation stroke) presentation)
    (setf (stroked-rectangles stroke) ())
    stroke))

;;;------------------------------------------------------------

(defmethod handle-motion-notify ((painter Muchi-Image-Painter) x y)
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   ;;(:explain :types :calls)
	   (type Image-Coordinate x y))
  (let* ((display (interactor-display painter))
	 (slate (display-slate display))
	 (queue (slate:update-slate-input-queue slate))
	 (rect (painter-rect painter))
	 (stroke (painter-stroke painter))
	 (muchi-rect (g::%borrow-screen-rect 0 0 0 0))
	 (pen (stroke-pen stroke)))
    (declare (type Muchi-Image-Presentation display)
	     (type slate:CLX-Slate slate)
	     (type slate:Input-Queue queue)
	     (type g:Screen-Rect rect muchi-rect)
	     (type Muchi-Image-Stroke stroke)
	     (type slate:Pen pen))
    ;; paint whatever falls under the brush region
    (setf (g::%screen-rect-xmin rect) x)
    (setf (g::%screen-rect-ymin rect) y)
    (compute-muchi-rect! display rect :result muchi-rect)
    (paint-presentation display pen muchi-rect)
    (push muchi-rect (stroked-rectangles stroke))
    ;; check for any other :motion-notify events
    ;; and handle them now, saving a little scheduler overhead
    (loop
      (let ((event (slate::%dequeue-input-event queue)))
	(declare (type List event))
	(unless (eq (first event) :motion-notify)
	  (unless (null event) (slate::%push-input-event event queue))
	  (return))
	(setf (g::%screen-rect-xmin rect) (second event))
	(setf (g::%screen-rect-ymin rect) (third event)))
      (compute-muchi-rect! display rect
			   :result (g::%borrow-screen-rect 0 0 0 0))
      (paint-presentation display pen muchi-rect)
      (push muchi-rect (stroked-rectangles stroke)))
    ;; get those bitag s out on the screen
    (slate::%force-drawing slate)))

;;;------------------------------------------------------------

(defmethod handle-button-release ((painter Muchi-Image-Painter))
  (let ((display (interactor-display painter)))
    ;; now apply the paint stroke to the Muchi itself
    (paint-muchi (subject display) (painter-stroke painter))
    (setf (painter-stroke painter) nil) ;; to be safe
    ;; return control to the top level handler
    (tools:atomic
     (setf (current-interactor display) (default-interactor display)))))







