;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))

;;;============================================================

(defstruct (Muxel-Bucket
	    (:conc-name nil)
	    (:constructor %make-muxel-bucket)
	    (:print-function print-muxel-bucket))
  (muxel-bucket-name "" :type (or Symbol String))
  (muxel-bucket-nchannels 0 :type tools:Array-Index)
  (muxel-bucket-nrows 0 :type tools:Array-Index)
  (muxel-bucket-ncols 0 :type tools:Array-Index)
  (muxel-bucket-channel-coordinates (make-array 0 :element-type 'Fixnum)
				    :type (Simple-Array Fixnum (*)))
  (muxel-bucket-pixels (make-array (list 0 0) :element-type 'Muxel)
		       :type (Simple-Array Muxel (* *))))

;;;------------------------------------------------------------

(defun print-muxel-bucket (bucket stream depth)
  (declare (ignore depth))
  (format stream "{~s ~dx~dx~d}"
	  (muxel-bucket-name bucket)
	  (muxel-bucket-nchannels bucket)
	  (muxel-bucket-nrows bucket)
	  (muxel-bucket-ncols bucket)))

;;;------------------------------------------------------------

(defun make-muxel-bucket (nchannels nrows ncols)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (let ((bucket
	 (%make-muxel-bucket
	  :muxel-bucket-name (gentemp "Muxel-Bucket-")
	  :muxel-bucket-nchannels nchannels
	  :muxel-bucket-nrows nrows
	  :muxel-bucket-ncols ncols
	  :muxel-bucket-channel-coordinates
	  (make-array nchannels :element-type 'Fixnum :initial-element 0)
	  :muxel-bucket-pixels
	  (make-array (list nrows ncols) :element-type 'Muxel))))
    bucket))

(defun read-packed-muxel-bucket (image-data-path channel-coordinate-path
				 nchannels nrows ncols
				 &key
				 (element-type '(Unsigned-Byte 24)))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (assert (equal element-type '(Unsigned-Byte 24)))
  (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	 (pixels (muxel-bucket-pixels bucket))
	 (channel-coords (muxel-bucket-channel-coordinates bucket)))
    (declare (type Muxel-Bucket bucket)
	     (type (Simple-Array Muxel (* *)))
	     (type (Simple-Array Fixnum (*)) channel-coords))
    (with-open-file (str channel-coordinate-path :direction :input)
      (dotimes (i nchannels) (setf (aref channel-coords i) (read str))))
    (with-open-file (str image-data-path
		     :direction :input
		     :element-type element-type)
      (dotimes (i nrows)
	(dotimes (j ncols)
	  (let* ((pixel (make-muxel j i nchannels))
		 (bytes (muxel-bytes pixel)))
	    (declare (type (Simple-Array (Unsigned-Byte 8) (*)) bytes))
	    (dotimes (k nchannels)
	      (setf (aref bytes k) (ldb (byte 8 16) (read-byte str))))
	    (setf (aref pixels i j) pixel)))))
    bucket))

(defun range-pixel-planes (plane-paths nchannels nrows ncols)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0))
	   (ignore nchannels))
  (let* ((min most-positive-fixnum)
	 (max most-negative-fixnum))
    (dolist (plane-path plane-paths)
      (with-open-file (str plane-path :direction :input)
	(dotimes (i (* ncols nrows))
	  (let ((x (read str)))
	    (when (< x min) (setf min x))
	    (when (> x max) (setf max x))))))
    (values min max)))

(defun read-muxel-bucket-planes (plane-paths nchannels nrows ncols)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (multiple-value-bind
      (min max) (range-pixel-planes plane-paths nchannels nrows ncols)
    (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	   (nrows (muxel-bucket-nrows bucket))
	   (ncols (muxel-bucket-ncols bucket))
	   (pixels (muxel-bucket-pixels bucket))
	   (channel 0))
      (declare (type Muxel-Bucket bucket))
      (dotimes (i nrows)
	(dotimes (j ncols)
	  (setf (aref pixels i j) (make-muxel j i nchannels))))
      (dolist (plane-path plane-paths)
	(with-open-file (str plane-path :direction :input)
	  (dotimes (i nrows)
	    (dotimes (j ncols)
	      (let ((pixel (aref pixels i j)))
		(declare (type Muxel pixel))
		(setf (aref (muxel-bytes pixel) channel)
		  (truncate (* 255 (- (read str) min)) max))))))
	(incf channel))
      bucket)))

(defun read-byte-bucket-planes (plane-paths nchannels nrows ncols)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	 (nrows (muxel-bucket-nrows bucket))
	 (ncols (muxel-bucket-ncols bucket))
	 (pixels (muxel-bucket-pixels bucket))
	 (channel 0))
    (declare (type Muxel-Bucket bucket))
    (setf (muxel-bucket-pixels bucket)
      (tools:with-collection
       (dotimes (i nrows)
	 (dotimes (j ncols)
	   (tools:collect (make-muxel j i nchannels))))))
    (dolist (plane-path plane-paths)
      (with-open-file (str plane-path :direction :input)
	(dotimes (i nrows)
	  (dotimes (j ncols)
	    (let ((pixel (aref pixels i j)))
	      (declare (type Muxel pixel))
	      (setf (aref (muxel-bytes pixel) channel)
		(min 255 (max 0 (read str))))))))
      (incf channel))
    bucket))

(defun standardize-bucket-planes (bucket)
  (let ((pixels (muxel-bucket-pixels bucket))
	(max 0)
	(min 255))
    (dotimes (channel (muxel-bucket-nchannels bucket))
      (setf min 255)
      (setf max 0)
      (dotimes (i (muxel-bucket-nrows bucket))
	(dotimes (j (muxel-bucket-ncols bucket))
	  (let* ((pixel (aref pixels i j))
		 (x (aref (muxel-bytes pixel) channel)))
	    (declare (type Muxel pixel))
	    (when (< x min) (setf min x))
	    (when (> x max) (setf max x)))))
      (print min) (print max)
      (dotimes (i (muxel-bucket-nrows bucket))
	(dotimes (j (muxel-bucket-ncols bucket))
	  (let* ((pixel (aref pixels i j))
		 (x (aref (muxel-bytes pixel) channel)))
	    (declare (type Muxel pixel))
	    (setf (aref (muxel-bytes pixel) channel)
	      (truncate (* 255 (- x min)) max))))))))

(defun write-packed-muxel-bucket-planes (plane-paths bucket
					 &key
					 (element-type `(Unsigned-Byte 8)))
  (let ((pixels (muxel-bucket-pixels bucket))
	(channel 0))
    (dolist (plane-path plane-paths)
      (with-open-file (str plane-path
		       :direction :output
		       :if-exists :overwrite
		       :element-type element-type)
	(dotimes (i (muxel-bucket-nrows bucket))
	  (dotimes (j (muxel-bucket-ncols bucket))
	    (let ((pixel (aref pixels i j)))
	      (declare (type Muxel pixel))
	      (write-byte (aref (muxel-bytes pixel) channel) str)))))
      (incf channel))))

(defun read-packed-muxel-bucket-planes (plane-paths
					&key
					(nchannels 0)
					(nrows 0)
					(ncols 0)
					(image-nrows nrows)
					(image-ncols ncols)
					(skip-bytes 0)
					(skip-rows 0)
					(skip-cols 0)
					(element-type '(Unsigned-Byte 8)))
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type tools:Array-Index nchannels nrows ncols image-nrows image-ncols
		 skip-bytes skip-rows skip-cols)
	   (ignore image-nrows))
  (assert (equal element-type '(Unsigned-Byte 8)))
  (let ((value nil)
	(old-gc-spread (sys:gsgc-parameter :generation-spread))
	(excl:*record-source-files* nil)
	(excl:*tenured-bytes-limit* nil))
    (setf (sys:gsgc-parameter :generation-spread) 1)  
    ;;#+:excl (print "gc") #+:excl (finish-output)
    ;;#+:excl (excl:gc t)
    ;;(room t)
    (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	   (pixels (muxel-bucket-pixels bucket))
	   (channel 0))
      (declare (type Muxel-Bucket bucket)
	       (type (Simple-Array Fixnum (*)) channel-coords))
      (print "making bucket") (finish-output)
      (dotimes (i nrows)
	(declare (type tools:Array-Index i))
	(dotimes (j ncols)
	  (declare (type tools:Array-Index j))
	  (setf (aref pixels i j) (make-muxel j i nchannels))))
      (print "reading data") (finish-output)
      (dolist (plane-path plane-paths)
	(with-open-file (str plane-path
			 :direction :input
			 :element-type element-type)
	  (print channel)
	  (advance-stream (+ skip-bytes (* skip-rows image-ncols)) str)
	  (dotimes (i nrows)
	    (declare (type tools:Array-Index i))
	    (advance-stream skip-cols str)
	    (dotimes (j ncols)
	      (declare (type tools:Array-Index j))
	      (setf (aref (muxel-bytes (aref pixels i j)) channel) 
		(read-byte str)))
	    (advance-stream (- image-ncols ncols skip-cols) str)))
	(incf channel))
      (setf value bucket))
    (print "gc") (finish-output)
    (excl:gc :tenure) (excl:gc :tenure) (excl:gc t)
    (room t)
    (setf (sys:gsgc-parameter :generation-spread) old-gc-spread)
    value))

;;;============================================================

(defun muxel-bucket-subset (bucket test-fn)
  (%make-muxel-bucket
   :muxel-bucket-nchannels (muxel-bucket-nchannels bucket)
   :muxel-bucket-nrows (muxel-bucket-nrows bucket)
   :muxel-bucket-ncols (muxel-bucket-ncols bucket)
   :muxel-bucket-channel-coordinates
   (muxel-bucket-channel-coordinates bucket)
   :muxel-bucket-pixels
   (remove-if-not test-fn
		  (muxel-bucket-pixels bucket))))

(defun muxel-over-threshold (muxel threshold)
  (some #'(lambda (byte) (> byte threshold))
	(muxel-bytes muxel)))

;;;============================================================
;;; Drawing Magnified Pixel Buckets
;;;============================================================

#||
(defgeneric %draw-magnified-muxel-bucket ( pen slate
					       bucket channel magnification)
	    (declare (type slate:Pen pen)
		     (type slate:Slate slate)
		     (type Muxel-Bucket bucket)
		     (type tools:Array-Index channel)
		     (type slate:Magnification-Level magnification)))

(defun draw-magnified-muxel-bucket (pen slate bucket channel magnification)
  (tools:type-check slate:Pen pen)
  (tools:type-check slate:Slate slate)
  (tools:type-check Muxel-Bucket bucket)
  (tools:type-check tools:Array-Index channel)
  (tools:type-check slate:Magnification-Level magnification)
  (%draw-magnified-muxel-bucket pen slate bucket channel magnification))
||#

;;;============================================================
;;; X method

;;; this version draws each magnified pixel

(defun draw-magnified-muxel-bucket (pen slate bucket channel mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (ignore pen)
	   (type slate:Pen pen)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket)
	   (type tools:Array-Index channel)
	   (type slate:Magnification-Level mag))
  (let ((drawable (the xlib:Drawable (slate::slate-drawable slate)))
	(pixels (muxel-bucket-pixels bucket))
	(nrows (muxel-bucket-nrows bucket))
	(ncols (muxel-bucket-ncols bucket))
	(x-display (the xlib:Display
		     (slate::x-display
		      (the slate:Screen (slate:slate-screen slate)))))) 
    (declare (type xlib:Drawable drawable)
	     (type xlib:Display x-display))
    (xlib:with-display
     (x-display)
     (dotimes (i nrows)
       (dotimes (j ncols)
	 (let ((pixel (aref pixels i j)))
	   (declare (type Muxel pixel))
	   (xlib:draw-rectangle
	    drawable
	    (aref (the (Simple-Array xlib:GContext (*))
		    slate::*pixval-gcontexts*)
		  (logior (the (Unsigned-Byte 8)
			    (ash (ldb
				  (byte 3 5)
				  (the (Unsigned-Byte 8)
				    (%muxel-value pixel)))
				 5))
			  (the (Unsigned-Byte 8)
			    (ash (aref (muxel-bytes pixel) channel) -3)
			    )))
	    (the xlib:Card16 (* mag (muxel-x pixel)))
	    (the xlib:Card16 (* mag (muxel-y pixel)))
	    mag mag t)))))))

(defun update-magnified-muxel-bucket (slate bucket timestamp channel mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (inline tools::%timestamp<)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket)
	   (type tools:Timestamp timestamp)
	   (type tools:Array-Index channel)
	   (type slate:Magnification-Level mag))
  (let ((drawable (the xlib:Drawable (slate::slate-drawable slate)))
	(pixels (muxel-bucket-pixels bucket))
	(nrows (muxel-bucket-nrows bucket))
	(ncols (muxel-bucket-ncols bucket))
	(x-display (the xlib:Display
		     (slate::x-display
		      (the slate:Screen (slate:slate-screen slate)))))) 
    (declare (type xlib:Drawable drawable)
	     (type xlib:Display x-display))
    (xlib:with-display
     (x-display)
     (dotimes (i nrows)
       (dotimes (j ncols)
	 (let ((pixel (aref pixels i j)))
       (declare (type Muxel pixel))
       (when (tools::%timestamp<
	      timestamp (muxel-timestamp pixel))
	 (xlib:draw-rectangle
	  drawable
	  (aref (the (Simple-Array xlib:GContext (*))
		  slate::*pixval-gcontexts*)
		(logior (the (Unsigned-Byte 8)
			  (ash (ldb
				(byte 3 5)
				(the (Unsigned-Byte 8)
				  (%muxel-value pixel)))
			       5))
			(the (Unsigned-Byte 8)
			  (ash (aref (muxel-bytes pixel) channel) -3))))
	  (the xlib:Card16 (* mag (muxel-x pixel)))
	  (the xlib:Card16 (* mag (muxel-y pixel)))
	  mag mag t))))))))

(defun magnified-muxel-bucket-movie (slate bucket mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket)
	   (type slate:Magnification-Level mag))
  (let ((nchannels (muxel-bucket-nchannels bucket))) 
    (dotimes (i nchannels)
      (draw-magnified-muxel-bucket nil slate bucket i mag))
    (dotimes (i nchannels)
      (draw-magnified-muxel-bucket nil slate bucket (- nchannels i 1) mag))
    (slate:finish-drawing slate)))

#||

(defun read-packed-muxel-bucket (image-data-path channel-coordinate-path
				 nchannels nrows ncols
				 &key
				 (element-type '(Unsigned-Byte 24)))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (assert (equal element-type '(Unsigned-Byte 24)))
  (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	 (channel-coords (muxel-bucket-channel-coordinates bucket)))
    (declare (type Muxel-Bucket bucket)
	     (type (Simple-Array Fixnum (*)) channel-coords))
    (with-open-file (str channel-coordinate-path :direction :input)
      (dotimes (i nchannels) (setf (aref channel-coords i) (read str))))
    (with-open-file (str image-data-path
		     :direction :input
		     :element-type element-type)
      (setf (muxel-bucket-pixels bucket)
	(tools:with-collection
	 (dotimes (j nrows)
	   (dotimes (i ncols)
	     (let* ((pixel (make-muxel i j nchannels))
		    (bytes (muxel-bytes pixel)))
	       (declare (type (Simple-Array (Unsigned-Byte 8) (*)) bytes))
	       (dotimes (k nchannels)
		 (setf (aref bytes k) (ldb (byte 8 16) (read-byte str))))
	       (tools:collect pixel)))))))
    bucket))

(defun range-pixel-planes (plane-paths nchannels nrows ncols)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0))
	   (ignore nchannels))
  (let* ((min most-positive-fixnum)
	 (max most-negative-fixnum))
    (dolist (plane-path plane-paths)
      (with-open-file (str plane-path :direction :input)
	(dotimes (i (* ncols nrows))
	  (let ((x (read str)))
	    (when (< x min) (setf min x))
	    (when (> x max) (setf max x))))))
    (values min max)))

(defun read-muxel-bucket-planes (plane-paths nchannels nrows ncols)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (multiple-value-bind
      (min max) (range-pixel-planes plane-paths nchannels nrows ncols)
    (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	   (channel 0))
      (declare (type Muxel-Bucket bucket))
      (setf (muxel-bucket-pixels bucket)
	(tools:with-collection
	 (dotimes (j nrows)
	   (dotimes (i ncols)
	     (tools:collect (make-muxel i j nchannels))))))
      (dolist (plane-path plane-paths)
	(with-open-file (str plane-path :direction :input)
	  (dolist (pixel (muxel-bucket-pixels bucket))
	    (declare (type Muxel pixel))
	    (setf (aref (muxel-bytes pixel) channel)
	      (truncate (* 255 (- (read str) min)) max))))
	(incf channel))
      bucket)))

(defun read-byte-bucket-planes (plane-paths nchannels nrows ncols)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	 (channel 0))
    (declare (type Muxel-Bucket bucket))
    (setf (muxel-bucket-pixels bucket)
      (tools:with-collection
       (dotimes (j nrows)
	 (dotimes (i ncols)
	   (tools:collect (make-muxel i j nchannels))))))
    (dolist (plane-path plane-paths)
      (with-open-file (str plane-path :direction :input)
	(dolist (pixel (muxel-bucket-pixels bucket))
	  (declare (type Muxel pixel))
	  (setf (aref (muxel-bytes pixel) channel)
	    (min 255 (max 0 (read str))))))
      (incf channel))
    bucket))

(defun standardize-bucket-planes (bucket)
  (let ((nchannels (muxel-bucket-nchannels bucket))
	(pixels (muxel-bucket-pixels bucket))
	(max 0)
	(min 255))
    (dotimes (channel nchannels)
      (setf min 255)
      (setf max 0)
      (dolist (pixel pixels)
	(declare (type Muxel pixel))
	(let ((x (aref (muxel-bytes pixel) channel)))
	  (when (< x min) (setf min x))
	  (when (> x max) (setf max x))))
      (print min) (print max)
      (dolist (pixel pixels)
	(declare (type Muxel pixel))
	(let ((x (aref (muxel-bytes pixel) channel)))
	  (setf (aref (muxel-bytes pixel) channel)
	    (truncate (* 255 (- x min)) max)))))))

(defun write-packed-muxel-bucket-planes (plane-paths bucket
					 &key
					 (element-type `(Unsigned-Byte 8)))
  (let ((channel 0))
    (dolist (plane-path plane-paths)
      (with-open-file (str plane-path
		       :direction :output
		       :if-exists :overwrite
		       :element-type element-type)
	(dolist (pixel (muxel-bucket-pixels bucket))
	  (declare (type Muxel pixel))
	  (write-byte (aref (muxel-bytes pixel) channel) str)))
      (incf channel))))

(defun read-packed-muxel-bucket-planes (plane-paths nchannels nrows ncols
					&key
					(element-type '(Unsigned-Byte 8)))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0)))
  (assert (equal element-type '(Unsigned-Byte 8)))
  (let* ((bucket (make-muxel-bucket nchannels nrows ncols))
	 (channel 0))
    (declare (type Muxel-Bucket bucket)
	     (type (Simple-Array Fixnum (*)) channel-coords))
    (setf (muxel-bucket-pixels bucket)
      (tools:with-collection
       (dotimes (j nrows)
	 (dotimes (i ncols)
	   (tools:collect (make-muxel i j nchannels))))))
    (dolist (plane-path plane-paths)
      (with-open-file (str plane-path
		       :direction :input
		       :element-type element-type)
	(dolist (pixel (muxel-bucket-pixels bucket))
	  (declare (type Muxel pixel))
	  (setf (aref (muxel-bytes pixel) channel) (read-byte str))))
      (incf channel))
    bucket))

;;;============================================================

(defun muxel-bucket-subset (bucket test-fn)
  (%make-muxel-bucket
   :muxel-bucket-nchannels (muxel-bucket-nchannels bucket)
   :muxel-bucket-nrows (muxel-bucket-nrows bucket)
   :muxel-bucket-ncols (muxel-bucket-ncols bucket)
   :muxel-bucket-channel-coordinates
   (muxel-bucket-channel-coordinates bucket)
   :muxel-bucket-pixels
   (remove-if-not test-fn
		  (muxel-bucket-pixels bucket))))

(defun muxel-over-threshold (muxel threshold)
  (some #'(lambda (byte) (> byte threshold))
	(muxel-bytes muxel)))

;;;============================================================
;;; Drawing Magnified Pixel Buckets
;;;============================================================

#||
(defgeneric %draw-magnified-muxel-bucket ( pen slate
					       bucket channel magnification)
	    (declare (type slate:Pen pen)
		     (type slate:Slate slate)
		     (type Muxel-Bucket bucket)
		     (type tools:Array-Index channel)
		     (type slate:Magnification-Level magnification)))

(defun draw-magnified-muxel-bucket (pen slate bucket channel magnification)
  (tools:type-check slate:Pen pen)
  (tools:type-check slate:Slate slate)
  (tools:type-check Muxel-Bucket bucket)
  (tools:type-check tools:Array-Index channel)
  (tools:type-check slate:Magnification-Level magnification)
  (%draw-magnified-muxel-bucket pen slate bucket channel magnification))
||#

;;;============================================================
;;; X method

;;; this version draws each magnified pixel

(defun draw-magnified-muxel-bucket (pen slate bucket channel mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (ignore pen)
	   (type slate:Pen pen)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket)
	   (type tools:Array-Index channel)
	   (type slate:Magnification-Level mag))
  (let ((drawable (the xlib:Drawable (slate::slate-drawable slate)))
	(x-display (the xlib:Display
		     (slate::x-display
		      (the slate:Screen (slate:slate-screen slate)))))) 
    (declare (type xlib:Drawable drawable)
	     (type xlib:Display x-display))
    (xlib:with-display
     (x-display)
     (dolist (pixel (the List (muxel-bucket-pixels bucket)))
       (declare (type Muxel pixel))
       (xlib:draw-rectangle
	drawable
	(aref (the (Simple-Array xlib:GContext (*)) slate::*pixval-gcontexts*)
	      (logior (the (Unsigned-Byte 8)
			(ash (ldb
			      (byte 3 5)
			      (the (Unsigned-Byte 8)
				(%muxel-value pixel)))
			     5))
		      (the (Unsigned-Byte 8)
			(ash (aref (muxel-bytes pixel) channel) -3)
			)))
	(the xlib:Card16 (* mag (muxel-x pixel)))
	(the xlib:Card16 (* mag (muxel-y pixel)))
	mag mag t)))))

(defun update-magnified-muxel-bucket (slate bucket timestamp channel mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (inline tools::%timestamp<)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket)
	   (type tools:Timestamp timestamp)
	   (type tools:Array-Index channel)
	   (type slate:Magnification-Level mag))
  (let ((drawable (the xlib:Drawable (slate::slate-drawable slate)))
	(x-display (the xlib:Display
		     (slate::x-display
		      (the slate:Screen (slate:slate-screen slate)))))) 
    (declare (type xlib:Drawable drawable)
	     (type xlib:Display x-display))
    (xlib:with-display
     (x-display)
     (dolist (pixel (the List (muxel-bucket-pixels bucket)))
       (declare (type Muxel pixel))
       (when (tools::%timestamp<
	      timestamp (muxel-timestamp pixel))
	 (xlib:draw-rectangle
	  drawable
	  (aref (the (Simple-Array xlib:GContext (*))
		  slate::*pixval-gcontexts*)
		(logior (the (Unsigned-Byte 8)
			  (ash (ldb
				(byte 3 5)
				(the (Unsigned-Byte 8)
				  (%muxel-value pixel)))
			       5))
			(the (Unsigned-Byte 8)
			  (ash (aref (muxel-bytes pixel) channel) -3))))
	  (the xlib:Card16 (* mag (muxel-x pixel)))
	  (the xlib:Card16 (* mag (muxel-y pixel)))
	  mag mag t))))))

(defun magnified-muxel-bucket-movie (slate bucket mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket)
	   (type slate:Magnification-Level mag))
  (let ((nchannels (muxel-bucket-nchannels bucket))) 
    (dotimes (i nchannels)
      (draw-magnified-muxel-bucket nil slate bucket i mag))
    (dotimes (i nchannels)
      (draw-magnified-muxel-bucket nil slate bucket (- nchannels i 1) mag))
    (slate:finish-drawing slate)))

||#