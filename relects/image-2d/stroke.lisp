;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (compilation-speed 0)
		     (safety 3)
		     (space 0)
		     (speed 1)))

;;;============================================================

(defclass Muchi-Stroke (Presentation-Stroke)
	  ((stroke-interactor
	    :type Muchi-Painter
	    :accessor stroke-interactor)
	   (stroked-presentation
	    :type Muchi-Presentation
	    :accessor stroked-presentation)))

;;;============================================================

(defclass Muchi-Image-Stroke (Muchi-Stroke)
	  ((stroke-interactor
	    :type Muchi-Image-Painter
	    :accessor stroke-interactor)
	   (stroked-presentation
	    :type Muchi-Image-Presentation
	    :accessor stroked-presentation)
	   (stroked-rectangles
	    :type List
	    :accessor stroked-rectangles
	    :initform ())))

;;;============================================================

(defclass Muchi-Scatter-Stroke (Muchi-Stroke)
	  ((stroke-interactor
	    :type Muchi-Scatter-Painter
	    :accessor stroke-interactor)
	   (stroked-presentation
	    :type Muchi-Scatter-Presentation
	    :accessor stroked-presentation)
	   (stroked-bins
	    :type (Simple-Array (Unsigned-Byte 8) (256 256))
	    :accessor stroked-bins
	    :initform (make-array '(256 256)
				  :element-type '(Unsigned-Byte 8)
				  :initial-element #b0))))

;;;============================================================
;;; a resource of  Muchi-Image-Stroke's
;;;============================================================

(defparameter *muchi-image-strokes* ())

(defmethod return-stroke ((stroke Muchi-Image-Stroke))
  (if (null (stroke-presentations-to-be-updated stroke))
      (push stroke *muchi-image-strokes*)
    ;; else
    (error "Trying to return ~a before it's done." stroke)))

;;;============================================================
;;; a resource of  Muchi-Scatter-Stroke's
;;;============================================================

(defparameter *muchi-scatter-strokes* ())

(defmethod return-stroke ((stroke Muchi-Scatter-Stroke))
  (if (null (stroke-presentations-to-be-updated stroke))
      (push stroke *muchi-scatter-strokes*)
    ;; else
    (error "Trying to return ~a before it's done." stroke)))

;;;============================================================
;;; Paint the Muchi itself
;;;============================================================

(defgeneric paint-muchi (muchi stroke)
	    (declare (type Muchi muchi)
		     (type Stroke stroke)))

;;;============================================================

(defmethod paint-muchi (muchi (stroke Muchi-Image-Stroke))
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Muchi muchi))
  (let* ((pen (stroke-pen stroke))
	 (pixel-value (slate::%pen-pixel-value pen))
	 (paint-array (muchi-paint muchi))
	 (painter (stroke-interactor stroke))
	 (others (remove (interactor-display painter)
			 (subject-presentations muchi))))
    (declare (type slate:Pen pen)
	     (type (Unsigned-Byte 8) pixel-value)
	     (type Muchi-Channel paint-array)
	     (type List others))
    ;; modify the database object
    (dolist (r (stroked-rectangles stroke))
      (declare (type g:Screen-Rect r))
      (let* ((xmin (g::%screen-rect-xmin r))
	     (xmax (+ xmin (g::%screen-rect-width r)))
	     (ymin (g::%screen-rect-ymin r))
	     (ymax (+ ymin (g::%screen-rect-height r))))
	(declare (type g:Screen-Coordinate xmin ymin xmax ymax))
	(do ((i ymin (+ i 1)))
	    ((>= i ymax))
	  (declare (type tools:Array-Index i))
	  (do ((j xmin (+ j 1)))
	      ((>= j xmax))
	    (declare (type tools:Array-Index j))
	    (setf (aref paint-array i j) pixel-value)))))
    ;; update the other presentations
    (setf (stroke-presentations-to-be-updated stroke) others)
    (dolist (presentation others)
      (send-input presentation :funcall #'paint-stroke presentation stroke))))

;;;============================================================

(defmethod paint-muchi (muchi (stroke Muchi-Scatter-Stroke))
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Muchi muchi))
  (let* ((pen (stroke-pen stroke))
	 (pixel-value (slate::%pen-pixel-value pen))
	 (paint-array (muchi-paint muchi))
	 (painter (stroke-interactor stroke))
	 (others (remove (interactor-display painter)
			 (subject-presentations muchi)))
	 (stroked-bins (stroked-bins stroke))
	 (bins (bins (stroked-presentation stroke))))
    (declare (type slate:Pen pen)
	     (type (Unsigned-Byte 8) pixel-value)
	     (type Muchi-Channel paint-array)
	     (type (Simple-Array (Unsigned-Byte 8) (256 256)) stroked-bins)
	     (type 2d-Muxel-Table bins))
    ;; modify the database object
    (dotimes (i 256)
      (declare (type tools:Array-Index i))
      (dotimes (j 256)
	(declare (type tools:Array-Index j))
	(when (= 1 (aref stroked-bins i j))
	  (let ((bin (aref bins i j)))
	    (declare (type Muxel-Table-Bin bin))
	    (unless (null bin)
	      (dotimes (k (array-dimension bin 0))
		(let ((pad (aref bin k)))
		  (declare (type Pixel-Address pad))
		  (setf (aref paint-array
			      (the Image-Coordinate (%pixel-address-y pad))
			      (the Image-Coordinate (%pixel-address-x pad)))
		    pixel-value))))))))
    ;; update the other presentations
    (setf (stroke-presentations-to-be-updated stroke) others)
    (dolist (presentation others)
      (send-input presentation :funcall #'paint-stroke presentation stroke))))

