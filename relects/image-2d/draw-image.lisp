;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1991. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

(export '(Image
	  image-width
	  image-height
	  image-rect
	  draw-image
	  Magnification-Level
	  draw-magnified-image))

;;;============================================================
;;; This stuff only works under X (for now)

#+:excl (require :clx)
#-:xlib (eval-when (compile load eval)
	  (error "this file can only be used with clx")) 

;;;============================================================
;;; An Image data type
;;;============================================================
;;; eventually Slate should have its own image type

(deftype Image () 'xlib:Image)

;;;============================================================

(proclaim '(Inline %image-width)) 
(defun %image-width (image)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type Image image))
  (xlib:image-width image))

(defun image-width (image)
  (tools:type-check Image image)
  (%image-width image))

(proclaim '(Inline %image-height)) 
(defun %image-height (image)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type Image image))
  (xlib:image-height image))

(defun image-height (image)
  (tools:type-check Image image)
  (%image-height image))


(defun %image-rect (image rect)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type Image image)
	   (type g:Screen-Rect rect))
  (setf (g::%screen-rect-xmin rect) 0)
  (setf (g::%screen-rect-ymin rect) 0)
  (setf (g::%screen-rect-width rect) (xlib:image-width image))
  (setf (g::%screen-rect-height rect) (xlib:image-height image))
  rect)

(defun image-rect (image &key (result (g:make-screen-rect)))
  (tools:type-check Image image)
  (tools:type-check g:Screen-Rect result)
  (%image-rect image result))

;;;============================================================
;;; Drawing Images
;;;============================================================

(defgeneric %draw-image (pen slate image from-rect to-point)
	    (declare (type Pen pen)
		     (type Slate slate)
		     (type Image image)
		     (type g:Screen-Rect from-rect)
		     (type g:Screen-Point to-point)))

(defun draw-image (pen slate image from-rect to-point)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check Image image)
  (tools:type-check g:Screen-Rect from-rect)
  (tools:type-check g:Screen-Point to-point)

  (%draw-image pen slate image from-rect to-point))

;;;============================================================
;;; X method

(defmethod %draw-image (pen (slate CLX-Slate) image from-rect to-point)
  (declare (optimize (speed 0)
		     (safety 3)
		     (space 0)
		     (compilation-speed 0))
	   (type Pen pen)
	   (type CLX-Slate slate)
	   (type Image image)
	   (type g:Screen-Rect from-rect)
	   (type g:Screen-Point to-point))
  (let* ((screen (slate-screen slate))
	 (gc (screen-pen-gcontext screen pen))) 
    (declare (type Screen screen)
	     (type xlib:GContext gc))
    (update-gcontext-from-slate slate gc)
    (update-gcontext-from-pen pen screen gc)
    (xlib:put-image (slate-drawable slate) gc image
		    :x (g::%screen-point-x to-point)
		    :y (g::%screen-point-y to-point)
		    :src-x (g::%screen-rect-xmin from-rect)
		    :src-y (g::%screen-rect-ymin from-rect)
		    :width (g::%screen-rect-width from-rect)
		    :height (g::%screen-rect-height from-rect))))

;;;============================================================
;;; Drawing Magnified Images
;;;============================================================

(deftype Magnification-Level () '(Integer 0 2048))

(defgeneric %draw-magnified-image (pen slate image magnification)
	    (declare (type Pen pen)
		     (type Slate slate)
		     (type Image image)
		     (type Magnification-Level magnification)))

(defun draw-magnified-image (pen slate image magnification)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check Image image)
  (tools:type-check Magnification-Level magnification)
  (%draw-magnified-image pen slate image magnification))

;;;============================================================
;;; X method
;;; this version draws each magnified pixel

;;; Assuming we have just one screen which is 8 bits deep,
;;; we can save the overhead of gcontext changes
;;; by pre-allocating a seperate gcontext for
;;; each of the 256 possible rectangles we'd want to draw.

(defparameter *pixval-gcontexts*
    (let ((array (make-array 256 :element-type 'xlib:GContext)))
      (dotimes (i 256)
	(setf (aref array i)
	  (xlib:create-gcontext
	   :drawable (xlib:screen-root (default-x-screen))
	   :cache-p nil
	   :function boole-1
	   :foreground i)))
      array))

(defmethod %draw-magnified-image (pen (slate CLX-Slate) image magnification)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type Pen pen)
	   (type CLX-Slate slate)
	   (type xlib:Image-Z image)
	   (type Magnification-Level magnification))
  (let* ((screen (slate-screen slate))
	 (gc (screen-pen-gcontext screen pen)) 
	 (drawable (slate-drawable slate))
	 (pixarray (xlib:image-z-pixarray image))
	 (width (xlib:image-width image))
	 (height (xlib:image-height image))
	 (pixval 0)
	 (old-pixval (aref pixarray 0 0))
	 (left 0)
	 (top 0)
	 (start-x 0)
	 (end-x 0)) 
    (declare ;;(:explain :types :calls)
     (type Screen screen)
     (type xlib:GContext gc)
     (type xlib:Drawable drawable)
     (type xlib:Pixarray pixarray)
     (type g:Positive-Screen-Coordinate
	   width height left top start-x end-x)
     (type Pixel-Value pixval old-pixval))
    (update-gcontext-from-slate slate gc)
    (update-gcontext-from-pen pen screen gc)
    (dotimes (y height)
      (declare (type tools:Array-Index y))
      (setf left 0)
      (setf start-x 0)
      (setf end-x 0)
      (setf old-pixval (aref pixarray y 0))
      (setf gc (aref (the (Simple-Array xlib:GContext (256))
		       *pixval-gcontexts*)
		     old-pixval))
      (loop
	(incf end-x)
	(when (>= end-x width)
	  ;; draw last rectangle and return
	  (xlib:draw-rectangle
	   drawable gc left top
	   (* magnification (- end-x start-x)) magnification t)
	  (return))
	(setf pixval (aref pixarray y end-x))
	(when (/= pixval old-pixval)
	  (xlib:draw-rectangle
	   drawable gc left top
	   (* magnification (- end-x start-x)) magnification t)
	  (setf old-pixval pixval)
	  (setf gc (aref (the (Simple-Array xlib:GContext (256))
			   *pixval-gcontexts*)
			 old-pixval))
	  (setf start-x end-x)
	  (setf left (* magnification start-x))))
      (incf top magnification))))



