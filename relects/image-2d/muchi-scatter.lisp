;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
	    (space 0)
	    (speed 1)
	    (compilation-speed 0)))

;;;============================================================

(defclass Muchi-Scatter-Presentation (Muchi-Presentation)
	  ((x-channel
	    :type tools:Array-Index
	    :accessor x-channel)
	   (y-channel
	    :type tools:Array-Index
	    :accessor y-channel)
	   (glyph-size
	    :type (Unsigned-Byte 8)
	    :accessor glyph-size)
	   (2d-histogram
	    :type (Simple-Array Fixnum (256 256))
	    :reader 2d-histogram)
	   (bins
	    :type 2d-Muxel-Table
	    :reader bins)
	   (rect-buffers
	    :type Simple-Vector
	    :reader rect-buffers)
	   (rect-buffer-ptrs
	    :type Simple-Vector
	    :reader rect-buffer-ptrs)))

;;;============================================================

(defun make-scatter-presentation-for-muchi (muchi slate
					    &key
					    (x-channel 0)
					    (y-channel 1)
					    (glyph-size 4))
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type Muchi-Scatter-Presentation display)
	   (type List build-options))
  (let ((display (allocate-instance
		  (find-class 'Muchi-Scatter-Presentation))))
    (initialize-display display
			:build-options (list :subject muchi
					     :slate slate
					     :x-channel x-channel
					     :y-channel y-channel
					     :glyph-size glyph-size))
    display))

;;;============================================================
;;; Building
;;;============================================================

(defparameter *rect-buffer-length* 256)

(defmethod build-display-local ((display Muchi-Scatter-Presentation)
				build-options)
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Muchi-Scatter-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject))
	 (nchannels (muchi-nchannels subject))
	 (rect (slate:slate-rect slate)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp) (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    (setf (slate-rect display) (g::%copy-screen-rect
				rect (g::%make-screen-rect 0 0 0 0)))
    (setf (temp-rect display) (g::%copy-screen-rect
			       rect (g::%make-screen-rect 0 0 0 0)))
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; local slots
    (assert (and (<= (getf build-options :x-channel 0) nchannels)
		 (<= (getf build-options :y-channel 1) nchannels)))
    (setf (slot-value display 'x-channel) (getf build-options :x-channel 0))
    (setf (slot-value display 'y-channel) (getf build-options :y-channel 1))
    (setf (slot-value display 'glyph-size) (getf build-options :glyph-size 4))
    (setf (slot-value display '2d-histogram)
      (make-array '(256 256)
		  :element-type 'Fixnum
		  :initial-element 0))
    (setf (slot-value display 'bins)
      (make-array '(256 256)
		  :element-type 'Muxel-Table-Bin
		  :initial-element nil))
    (setf (slot-value display 'rect-buffers)
      (vector
       ()
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)))
    (setf (slot-value display 'rect-buffer-ptrs)
      (copy-seq (rect-buffers display)))

    ;; slots inherited from Presentation:
    (setf (slot-value display 'subject) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject))
    (setf (display-name display)
      (getf build-options :display-name
	    (format nil "Channels ~d vs ~d of ~s"
		    (x-channel display) (y-channel display)
		    subject)))

    ;; input interactors
    (setf (current-interactor display) (build-interactors display)))
  display)

;;;------------------------------------------------------------

(defmethod build-interactors ((display Muchi-Scatter-Presentation))
  (setf (painter display)
    (make-instance 'Muchi-Scatter-Painter :interactor-display display))
  (setf (default-interactor display)
    (make-instance 'Presentation-Interactor :interactor-display display)))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((display Muchi-Scatter-Presentation)
				 layout-options)
  (declare ;;(:explain :types :calls)
	   (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3)))
  layout-options
  (let* ((bins (bins display))
	 (muchi (subject display))
	 (channels (muchi-channels muchi))
	 (x-channel (aref channels (x-channel display)))
	 (y-channel (aref channels (y-channel display)))
	 (2d-histogram (2d-histogram display))
	 (nrows (muchi-nrows muchi))
	 (ncols (muchi-ncols muchi)))
    (declare (type 2d-Muxel-Table bins)
	     (type Muchi muchi)
	     (type Muchi-Channel-Vector channels)
	     (type Muchi-Channel x-channel y-channel)
	     (type 2d-Histogram-Array 2d-histogram)
	     (type tools:Array-Index nrows ncols))
    (2d-histogram-muchi-channel x-channel y-channel
				:histogram-array 2d-histogram)
    (dotimes (y nrows)
      (declare (type Image-Coordinate y))
      (dotimes (x ncols)
	(declare (type Image-Coordinate x))
	(let* ((i (aref y-channel y x))
	       (j (aref x-channel y x))
	       (bin (aref bins i j)))
	  (declare (type (Unsigned-Byte 8) i j)
		   (type Muxel-Table-Bin bin))
	  (when (null bin)
	    (setf bin
	      (make-array (the tools:Array-Index (aref 2d-histogram i j))
			  :element-type 'Fixnum))
	    (setf (aref bins i j) bin))
	  ;; We fill the bin array from the back, decrementing
	  ;; the histogram array to keep track of where the next
	  ;; Pixel Address goes.
	  (setf (aref (the (Simple-Array Fixnum (*)) bin)
		      (the tools:Array-Index (decf (aref 2d-histogram i j))))
	    (%pixel-address x y))))))
  display)

;;;------------------------------------------------------------

(defmethod apply-lens (lens (display Muchi-Scatter-Presentation))
  (declare (ignore lens))
  (setf (slot-value display 'display-pen) (subject-pen (subject display))))

;;;------------------------------------------------------------
;;; Handling :configure-notify events
;;;------------------------------------------------------------

(defmethod reconfigure-display ((display Muchi-Scatter-Presentation)
				width height)
  width height t
  #||
  (declare (type g:Positive-Screen-Coordinate width height))
  
  (let* ((muchi (subject display))
	 (muchi-width (muchi-ncols muchi))
	 (muchi-height (muchi-nrows muchi))
	 (magnification (min (truncate width muchi-width)
			     (truncate height muchi-height))))
    (declare (type Muchi muchi)
	     (type g:Positive-Screen-Coordinate muchi-width muchi-height)
	     (type slate:Magnification-Level magnification))
    (unless (= (magnification display) magnification)
      (erase-display display nil)
      (layout-display display nil)
      (draw-display display)
      (slate:finish-drawing (display-slate display))))
  ||#
  )

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod erase-display ((display Muchi-Scatter-Presentation) rect)
  (slate:with-clipping-region
   ((display-slate display) (clipping-region display))
   (setf (slate:pen-pixel-value (erasing-fill-pen))
     (slate:slate-background-pixel-value (display-slate display)))
   (slate::%draw-rect (erasing-fill-pen) (display-slate display)
		      (slate-rect display)))
  (unless (null rect)
      (g::%copy-screen-rect (slate-rect display) rect)))

;;;------------------------------------------------------------

(proclaim '(Simple-Vector *pure-hue-gcontexts*))

(defparameter *pure-hue-gcontexts*
    (vector
     (aref slate::*pixval-gcontexts* #b00011111)
     (aref slate::*pixval-gcontexts* #b00111111)
     (aref slate::*pixval-gcontexts* #b01011111)
     (aref slate::*pixval-gcontexts* #b01111111)
     (aref slate::*pixval-gcontexts* #b10011111)
     (aref slate::*pixval-gcontexts* #b10111111)
     (aref slate::*pixval-gcontexts* #b11011111)
     (aref slate::*pixval-gcontexts* #b11111111)))

(defmethod draw-display ((display Muchi-Scatter-Presentation))
  (declare ;;(:explain :types :calls)
   (optimize (compilation-speed 0)
	     (safety 1)
	     (space 0)
	     (speed 3)))
  (let* ((slate (display-slate display))
	 (drawable (slate::slate-drawable slate))
	 (muchi (subject display))
	 (channels (muchi-channels muchi))
	 (nrows (muchi-nrows muchi))
	 (ncols (muchi-ncols muchi))
	 (x-channel (aref channels (x-channel display)))
	 (y-channel (aref channels (y-channel display)))
	 (paint (muchi-paint muchi))
	 (size (glyph-size display))
	 (gcs *pure-hue-gcontexts*)
	 (rects (rect-buffers display))
	 (ptrs (rect-buffer-ptrs display)))
    (declare (type slate:CLX-Slate slate)
	     (type Muchi muchi)
	     (type Muchi-Channel-Vector channels)
	     (type Muchi-Channel x-channel y-channel paint)
	     (type xlib:Drawable drawable)
	     (type tools:Array-Index size)
	     (type (Simple-Vector 8) gcs rects))
    ;; initialize the buffers
    (dotimes (i 8) (setf (svref ptrs i) (svref rects i)))
    ;; sort the rectangles into the buffers, drawing as the buffers fill
    (dotimes (i nrows)
      (declare (type tools:Array-Index i))
      (dotimes (j ncols)
	(declare (type tools:Array-Index j))
	(let* ((h (ash (aref paint i j) -5))
	       (ptr (svref ptrs h)))
	  (declare (type (Integer 0 7) h)
		   (type List ptr))
	  (setf (car ptr) (ash (aref x-channel i j) 1))
	  (setf ptr (cdr ptr))
	  (setf (car ptr) (ash (aref y-channel i j) 1))
	  (setf ptr (cdr ptr))
	  (setf (car ptr) size)
	  (setf ptr (cdr ptr))
	  (setf (car ptr) size)
	  (setf (svref ptrs h) (cdr ptr))
	  (when (null (cdr ptr))
	    (xlib:draw-rectangles drawable (svref gcs h) (aref rects h) t)
	    (setf (svref ptrs h) (svref rects h)))))
      ;; Draw the contents of any non-empty buffers. This will draw some
      ;; rectangles twice, because we don't keep track of partially filled
      ;; buffers.
      (dotimes (i 8)
	(let ((rs (svref rects i)))
	  (declare (type List rs))
	  (unless (eq rs (svref ptrs i))
	    (xlib:draw-rectangles drawable (svref gcs i) rs t)))))))

;;;------------------------------------------------------------

(defmethod paint-stroke ((display Muchi-Scatter-Presentation)
			 (stroke Muchi-Image-Stroke)
			 &rest options)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (ignore options))

  (let* ((drawable (slate::slate-drawable (display-slate display)))
	 (muchi (subject display))
	 (channels (muchi-channels muchi))
	 (x-channel (aref channels (x-channel display)))
	 (y-channel (aref channels (y-channel display)))
	 (size (glyph-size display))
	 (rectangles (stroked-rectangles stroke))
	 (pen (stroke-pen stroke))
	 (gc (svref *pure-hue-gcontexts*
		    (ash (slate::%pen-pixel-value pen) -5))))
    (declare (type Muchi muchi)
	     (type Muchi-Channel-Vector channels)
	     (type Muchi-Channel x-channel y-channel paint)
	     (type xlib:Drawable drawable)
	     (type tools:Array-Index size)
	     (type List rectangles)
	     (type slate:Pen pen)
	     (type xlib:Gcontext gc))

    (dolist (r rectangles)
      (declare (type g:Screen-Rect r))
      (let* ((xmin (g::%screen-rect-xmin r))
	     (ymin (g::%screen-rect-ymin r))
	     (xmax (+ xmin (g::%screen-rect-width r)))
	     (ymax (+ ymin (g::%screen-rect-height r))))
	(declare (type tools:Array-Index xmin xmax ymin ymax))
	(do ((i ymin (+ i 1))) ((>= i ymax))
	  (declare (type tools:Array-Index i))
	  (do ((j xmin (+ j 1))) ((>= j xmax))
	    (declare (type tools:Array-Index j))
	    (xlib:draw-rectangle
	     drawable gc
	     (ash (aref x-channel i j) 1)
	     (ash (aref y-channel i j) 1)
	     size size t)))))

    (slate:force-drawing (display-slate display))

    (tools:atomic
     (tools:deletef display (stroke-presentations-to-be-updated stroke))
     (when (null (stroke-presentations-to-be-updated stroke))
       (return-stroke stroke)))))

;;;------------------------------------------------------------

(defmethod paint-stroke ((display Muchi-Scatter-Presentation)
			 (stroke Muchi-Scatter-Stroke)
			 &rest options)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (ignore options))

  (let* ((drawable (slate::slate-drawable (display-slate display)))
	 (channels (muchi-channels (subject display)))
	 (x-channel (aref channels (x-channel display)))
	 (y-channel (aref channels (y-channel display)))
	 (size (glyph-size display))
	 (stroked-bins (stroked-bins stroke))
	 (bins (bins (stroked-presentation stroke)))
	 (pen (stroke-pen stroke))
	 (gc (svref *pure-hue-gcontexts*
		    (ash (slate::%pen-pixel-value pen) -5))))
    (declare (type xlib:Drawable drawable)
	     (type Muchi-Channel-Vector channels)
	     (type Muchi-Channel x-channel y-channel)
	     (type tools:Array-Index size)
	     (type (Simple-Array (Unsigned-Byte 8) (256 256)) stroked-bins)
	     (type 2d-Muxel-Table bins)
	     (type slate:Pen pen)
	     (type xlib:Gcontext gc))

    (dotimes (i 256)
      (declare (type tools:Array-Index i))
      (dotimes (j 256)
	(declare (type tools:Array-Index j))
	(when (= 1 (aref stroked-bins i j))
	  (let ((bin (aref bins i j)))
	    (declare (type List bin))
	    (unless (null bin)
	      (dotimes (k (array-dimension bin 0))
		(let ((pad (aref bin k)))
		  (declare (type Pixel-Address pad))
		  (let ((i (%pixel-address-y pad))
			(j (%pixel-address-x pad)))
		    (declare (type tools:Array-Index i j))
		    (xlib:draw-rectangle
		     drawable gc
		     (ash (aref x-channel i j) 1) (ash (aref y-channel i j) 1)
		     size size t)))))))))

    (slate:force-drawing (display-slate display))

    (tools:atomic
     (tools:deletef display (stroke-presentations-to-be-updated stroke))
     (when (null (stroke-presentations-to-be-updated stroke))
       (return-stroke stroke)))))

;;;------------------------------------------------------------

(defmethod (setf x-channel) (new-channel (display Muchi-Scatter-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'x-channel) new-channel)
  (erase-display display nil)
  (layout-display display nil)
  (draw-display display)
  (activate-display display nil))

(defmethod (setf y-channel) (new-channel (display Muchi-Scatter-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'y-channel) new-channel)
  (erase-display display nil)
  (layout-display display nil)
  (draw-display display)
  (activate-display display nil))

(defmethod (setf channels) (channels (display Muchi-Scatter-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'x-channel) (first channels))
  (setf (slot-value display 'y-channel) (second channels))
  (erase-display display nil)
  (layout-display display nil)
  (draw-display display)
  (activate-display display nil))

;;;============================================================
;;; Input
;;;============================================================

(defmethod paint-presentation ((display Muchi-Scatter-Presentation) pen rect)

  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3))
	   (type Muchi-Scatter-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect rect))

  (let* ((slate (display-slate display))
 	 (drawable (slate::slate-drawable slate))
 	 (bins (bins display))
	 (size (glyph-size display))
	 (xmin (max 0 (g::%screen-rect-xmin rect)))
	 (ymin (max 0 (g::%screen-rect-ymin rect)))
	 (xmax (min 256 (+ xmin (g::%screen-rect-width rect))))
	 (ymax (min 256 (+ ymin (g::%screen-rect-height rect))))
	 (pixval (slate::%pen-pixel-value pen))
	 (gc (aref slate::*pixval-gcontexts* pixval)))
    (declare (type slate:Slate slate)
	     (type xlib:Drawable drawable)
	     (type (Simple-Array T (256 256)) bins)
	     (type tools:Array-Index size)
	     (type (Unsigned-Byte 8) xmin ymin xmax ymax pixval)
	     (type xlib:Gcontext gc))

    (do ((i ymin (+ i 1))) ((>= i ymax))
      (declare (type Image-Coordinate i))
      (do ((j xmin (+ j 1))) ((>= j xmax))
	(declare (type Image-Coordinate j))
	(let ((bin (aref bins i j)))
	  (unless (or (null bin) (zerop (array-dimension bin 0)))
	    (xlib:draw-rectangle
	     drawable gc (ash j 1) (ash i 1) size size t)))))))

;;;============================================================
;;; Interactors for Muchi-Scatter-Presentations
;;;============================================================

(defclass Muchi-Scatter-Painter (Muchi-Painter)
	  ((interactor-display
	    :type Muchi-Scatter-Presentation
	    :accessor interactor-display
	    :initarg :interactor-display)))

;;;============================================================
;;; methods for Muchi-Scatter-Painter
;;;============================================================

(defmethod borrow-stroke ((painter Muchi-Scatter-Painter))
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3)))

  (let* ((presentation (interactor-display painter))
	 (stroke (pop *muchi-scatter-strokes*)))
    (if (null stroke)
	(setf stroke (make-instance 'Muchi-Scatter-Stroke))
      ;; else clear the stroke marks
      (let ((stroked-bins (stroked-bins stroke)))
	(declare (type (Simple-Array (Unsigned-Byte 8) (256 256))
		       stroked-bins))
	(dotimes (i 256)
	  (declare (type tools:Array-Index i))
	  (dotimes (j 256)
	    (declare (type tools:Array-Index j))
	    (setf (aref stroked-bins i j) #b0)))))
    (setf (stroke-interactor stroke) painter)
    (setf (stroked-presentation stroke) presentation)
    stroke))

;;;------------------------------------------------------------

(defmethod handle-motion-notify ((painter Muchi-Scatter-Painter) x y)
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3)))
  
  (let* ((display (interactor-display painter))
	 (slate (display-slate display))
	 (queue (slate:update-slate-input-queue slate))
	 (rect (painter-rect painter))
	 (stroke (painter-stroke painter))
	 (stroked-bins (stroked-bins stroke))
	 (pen (stroke-pen stroke)))
    (declare (type Muchi-Scatter-Presentation display)
	     (type slate:CLX-Slate slate)
	     (type slate:Input-Queue queue)
	     (type g:Screen-Rect rect muchi-rect)
	     (type Muchi-Scatter-Stroke stroke)
	     (type (Simple-Array (Unsigned-Byte 8) (256 256)) stroked-bins)
	     (type slate:Pen pen))
    (tools:type-check (Simple-Array (Unsigned-Byte 8) (256 256)) stroked-bins)
    ;; paint whatever falls under the brush region
    (setf (g::%screen-rect-xmin rect) (ash x -1))
    (setf (g::%screen-rect-ymin rect) (ash y -1))
    (paint-presentation display pen rect)
    (let* ((xmin (max 0 (g::%screen-rect-xmin rect)))
	   (ymin (max 0 (g::%screen-rect-ymin rect)))
	   (xmax (min 256 (+ xmin (g::%screen-rect-width rect))))
	   (ymax (min 256 (+ ymin (g::%screen-rect-height rect)))))
      (declare (type (Unsigned-Byte 8) xmin ymin xmax ymax))
      (do ((i ymin (+ i 1))) ((>= i ymax))
	(declare (type (Unsigned-Byte 8) i))
	(do ((j xmin (+ j 1))) ((>= j xmax))
	  (declare (type (Unsigned-Byte 8) j))
	  (setf (aref stroked-bins i j) #b1))))

    ;; check for any other :motion-notify events
    ;; and handle them now, saving a little scheduler overhead
    (loop
      (let ((event (slate::%dequeue-input-event queue)))
	(declare (type List event))
	(unless (eq (first event) :motion-notify)
	  (unless (null event) (slate::%push-input-event event queue))
	  (return))
	(setf (g::%screen-rect-xmin rect) (ash (second event) -1))
	(setf (g::%screen-rect-ymin rect) (ash (third event) -1)))
      (paint-presentation display pen rect)
      (let* ((xmin (max 0 (g::%screen-rect-xmin rect)))
	     (ymin (max 0 (g::%screen-rect-ymin rect)))
	     (xmax (min 256 (+ xmin (g::%screen-rect-width rect))))
	     (ymax (min 256 (+ ymin (g::%screen-rect-height rect)))))
	(declare (type (Unsigned-Byte 8) xmin ymin xmax ymax))
	(tools:type-check (Simple-Array (Unsigned-Byte 8) (256 256))
			  stroked-bins)

	(do ((i ymin (+ i 1))) ((>= i ymax))
	  (declare (type (Unsigned-Byte 8) i))
	  (do ((j xmin (+ j 1))) ((>= j xmax))
	    (declare (type (Unsigned-Byte 8) j))
	    (setf (aref stroked-bins i j) #b1)))))

    ;; get those bits out on the screen
    (slate::%force-drawing slate)))

;;;------------------------------------------------------------

(defmethod handle-button-release ((painter Muchi-Scatter-Painter))
  (let ((display (interactor-display painter)))
    ;; now apply the paint stroke to the Muchi itself
    (paint-muchi (subject display) (painter-stroke painter))
    (setf (painter-stroke painter) nil) ;; to be safe
    ;; return control to the top level handler
    (tools:atomic
     (setf (current-interactor display) (default-interactor display)))))







