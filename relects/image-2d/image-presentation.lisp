;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defclass Image-Presentation (Presentation Display-Leaf)
	  ((magnification
	    :type slate:Magnification-Level
	    :accessor magnification)
	   (subject
	    :type slate:Image
	    :accessor subject)))

;;;============================================================

(defun make-presentation-for-image (image slate)
    (declare (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Image-Presentation display)
	   (type List build-options))
    (let ((display (allocate-instance (find-class 'Image-Presentation))))
    (initialize-display display
			:build-options (list :subject image
					     :slate slate))
    
    display))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Image-Presentation)
				build-options)
  (declare (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Image-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject))
	 (rect (slate:slate-rect slate)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp)
      (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    (setf (slate-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (temp-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; slots inherited from Presentation:
    (setf (slot-value display 'subject) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject)) 
    (setf (display-name display)
      (getf build-options :display-name
	    (format nil "~s presentation" subject)))

    ;; input interactors
    (setf (current-interactor display) (build-interactors display))
    ))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((display Image-Presentation)
				 layout-options)
  (declare (ignore layout-options)
	   (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Image-Presentation display))
  (let* ((image (subject display))
	 (image-width (slate:image-width image))
	 (image-height (slate:image-height image))
	 (slate (display-slate display))
	 (magnification
	  (min (truncate (slate:slate-width slate) image-width)
	       (truncate (slate:slate-height slate) image-height)))
	 (display-rect (slate:slate-rect slate)))
    (declare (type slate:Image image)
	     (type slate:Slate slate)
	     (type g:Positive-Screen-Coordinate image-width image-height)
	     (type slate:Magnification-Level magnification)
	     (type g:Screen-Rect display-rect))
    (setf (magnification display) magnification)
    (setf (g:screen-rect-origin display-rect)
      (slate:slate-origin slate))
    (setf (g:screen-rect-width display-rect)
      (* magnification image-width))
    (setf (g:screen-rect-height display-rect)
      (* magnification image-height))
    display))

;;;------------------------------------------------------------

(defmethod apply-lens (lens (display Image-Presentation))
  (declare (ignore lens))
  (setf (slot-value display 'display-pen) (subject-pen (subject display))))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod draw-display ((display Image-Presentation))
  (slate:draw-magnified-image
   (display-pen display) (display-slate display) (subject display)
   (magnification display))
  (tools:stamp-time! (redraw-timestamp display)))

;;;------------------------------------------------------------

(defmethod erase-display ((display Image-Presentation) rect)
  (slate:with-clipping-region ((display-slate display)
			       (clipping-region display))
    (setf (slate:pen-pixel-value (erasing-fill-pen))
       (slate:slate-background-pixel-value (display-slate display)))
    (slate::%draw-rect (erasing-fill-pen) (display-slate display)
		       (slate-rect display)))
  (unless (null rect)
      (g::%copy-screen-rect (slate-rect display) rect)))

;;;============================================================
;;; Input
;;;============================================================

#||
(defmethod display-interesting-input-events ((display Image-Presentation))
  '( ;; :enter-notify
    ;; :leave-notify
    ;;:motion-notify
    ;;
    ;;:pointer-motion-hint
    
    :button-press
    :button-release
    ;;:exposure
    ;; :visibility-notify
    :configure-notify
    ))

(defparameter *paint-brush-rect* (g:make-screen-rect :width 16 :height 16))
 
(defmethod display-top-level-input-handler ((display Image-Presentation))
  (let* ((slate (display-slate display))
	 (input-queue (slate:slate-input-queue slate))
	 (event (slate::%dequeue-input-event input-queue))
	 (event-key (first event)))
    (case event-key
      (:button-press
       (setf (slate:slate-interesting-input-events slate)
	 `(:motion-notify :button-press :button-release))
       (setf (slate:slate-input-handler slate)
	 #'(lambda ()
	     (image-painting-input-handler
	      display *left-image-painting-pen*
	      *paint-brush-rect*
	      ))))
      (:destroy-request	   
       (kill-object display)
       (slate:kill-slate slate :destroy-window t))
      (:destroy-notify
       ;; Note that kill-slate doesn't have to call xlib:destroy-window,
       ;; because it's presumeably already been destroyed.
       (kill-object display)
       (slate:kill-slate slate :destroy-window nil))
      (:exposure
       (g:with-borrowed-screen-rect
	(r :left (second event)
	   :top (third event)
	   :width (fourth event)
	   :height (fifth event))
	(repair-damage display r))
       (slate:finish-drawing (display-slate display)))
      (:configure-notify
       (erase-display display nil)
       (layout-display display nil)
       (draw-display display))
      
      (:funcall (apply (second event) (cddr event)))

      )))
 
(defun image-painting-input-handler (display pen rect)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type Image-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect rect))
  (let* ((slate (display-slate display))
	 (queue (slate:update-slate-input-queue slate))
	 (event (slate::%dequeue-input-event queue))
	 (event-key (first event)))
    (declare (type slate:CLX-Slate slate)
	     (type slate:Input-Queue queue)
	     (type List event)
	     (type Keyword event-key))
    (case event-key
      (:destroy-request	   
       (kill-object display)
       (slate:kill-slate slate :destroy-window t))
      (:destroy-notify
       ;; Note that kill-slate doesn't have to call xlib:destroy-window,
       ;; because it's presumeably already been destroyed.
       (kill-object display)
       (slate:kill-slate slate :destroy-window nil))
      (:motion-notify
       ;; paint rect under mouse position
       (setf (g::%screen-rect-xmin rect) (second event))
       (setf (g::%screen-rect-ymin rect) (third event))
       (slate::%draw-rect pen slate rect)
       ;; check for any other :motion-notify events
       ;; and handle them now, saving a little scheduler overhead
       (loop
	 (setf event (slate::%dequeue-input-event queue))
	 (unless (eq (first event) :motion-notify)
	   (unless (null event) (slate::%push-input-event event queue))
	   (return))
	 (setf (g::%screen-rect-xmin rect) (second event))
	 (setf (g::%screen-rect-ymin rect) (third event))
	 (slate::%draw-rect pen slate rect))
       ;; get those bits out on the screen
       (slate::%force-drawing slate))

      (:button-release
       ;; return control to the top level handler
       (tools:atomic
	(setf (slate:slate-interesting-input-events slate)
	  (display-interesting-input-events display))
	(setf (slate:slate-input-handler slate)
	  #'(lambda () (display-top-level-input-handler display)))))
      
      (:funcall (apply (second event) (cddr event)))

      )))

||#



