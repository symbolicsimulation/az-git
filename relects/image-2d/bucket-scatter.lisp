;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defclass Scatter-Bucket-Presentation (Presentation Display-Leaf)
	  ((x-channel
	    :type tools:Array-Index
	    :accessor x-channel)
	   (y-channel
	    :type tools:Array-Index
	    :accessor y-channel)
	   (glyph-size
	    :type (Unsigned-Byte 8)
	    :accessor glyph-size)
	   (subject
	    :type Muxel-Bucket
	    :accessor subject)
	   (bins
	    :type 2d-Muxel-Table
	    :accessor bins)))

;;;============================================================

(defun make-scatter-presentation-for-muxel-bucket (muxel-bucket slate
						   &key
						   (x-channel 0)
						   (y-channel 1)
						   (glyph-size 4)
						   (bin-factor 64))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0))
	   (type Scatter-Bucket-Presentation display)
	   (type List build-options))
  (let ((display (allocate-instance
		  (find-class 'Scatter-Bucket-Presentation))))
    (initialize-display display
			:build-options (list :subject muxel-bucket
					     :slate slate
					     :x-channel x-channel
					     :y-channel y-channel
					     :glyph-size glyph-size
					     :bin-factor bin-factor))
    display))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Scatter-Bucket-Presentation)
				build-options)
  (declare (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Scatter-Bucket-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject))
	 (nchannels (muxel-bucket-nchannels subject))
	 (rect (slate:slate-rect slate)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp)
      (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    (setf (slate-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (temp-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; input interactors
    (setf (current-interactor display) (build-interactors display))

    ;; local slots
    (assert (and (<= (getf build-options :x-channel 0) nchannels)
		 (<= (getf build-options :y-channel 1) nchannels)))
    (setf (slot-value display 'x-channel) (getf build-options :x-channel 0))
    (setf (slot-value display 'y-channel) (getf build-options :y-channel 1))
    (setf (slot-value display 'glyph-size) (getf build-options :glyph-size 4))
    (let ((bin-factor (getf build-options :bin-factor 64)))
      (setf (slot-value display 'bins)
	(make-array (list bin-factor bin-factor)
		    :element-type 'List
		    :initial-element ())))

    ;; slots inherited from Presentation:
    (setf (slot-value display 'subject) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject))
    (setf (display-name display)
      (getf build-options :display-name
	    (format nil "Channels ~d vs ~d of ~s"
		    (x-channel display) (y-channel display)
		    subject)))
    ))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((display Scatter-Bucket-Presentation)
				 layout-options)
  (declare (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (ignore layout-options)
	   (type Scatter-Bucket-Presentation display))

  (let* ((slate (display-slate display))
	 (display-rect (slate-rect display))
	 (bins (bins display))
	 (xbins (array-dimension bins 1))
	 (ybins (array-dimension bins 0))
	 (bucket (subject display))
	 (x-channel (x-channel display))
	 (y-channel (y-channel display))
	 (pixels (muxel-bucket-pixels bucket))
	 (nrows (muxel-bucket-nrows bucket))
	 (ncols (muxel-bucket-ncols bucket)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect display-rect)
	     (type 2d-Muxel-Table bins)
	     (type Muxel-Bucket bucket)
	     (type (Simple-Array Muxel (* *)) pixels)
	     (type tools:Array-Index xbins ybins nrows ncols))
    (assert (and (<= 256 (slate:slate-width slate))
		 (<= 256 (slate:slate-height slate))))
    (setf (g:screen-rect-origin display-rect) (slate:slate-origin slate))
    (setf (g:screen-rect-width display-rect) 256)
    (setf (g:screen-rect-height display-rect) 256)
    (let* ((bin-width (ceiling (g:screen-rect-width display-rect) xbins))
	   (bin-height (ceiling (g:screen-rect-height display-rect) ybins)))
      (declare (type (Unsigned-Byte 8) bin-width bin-height))
      (dotimes (i ybins) (dotimes (j xbins) (setf (aref bins i j) ())))
      (dotimes (i nrows)
	(declare (type tools:Array-Index i))
	(dotimes (j ncols)
	  (declare (type tools:Array-Index j))
	  (let* ((pixel (aref pixels i j))
		 (bytes (muxel-bytes pixel))
		 (x (aref bytes x-channel))
		 (y (aref bytes y-channel)))
	    (declare (type Muxel pixel)
		     (type (Simple-Array (Unsigned-Byte 8) (*)) bytes)
		     (type  (Unsigned-Byte 8) x y))
	    (push pixel
		  (aref bins
			(truncate x bin-width)
			(truncate y bin-height))))))))
  display)

;;;------------------------------------------------------------

(defmethod apply-lens (lens (display Scatter-Bucket-Presentation))
  (declare (ignore lens))
  (setf (slot-value display 'display-pen) (subject-pen (subject display))))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod erase-display ((display Scatter-Bucket-Presentation) rect)
  (slate:with-clipping-region ((display-slate display)
			       (clipping-region display))
    (setf (slate:pen-pixel-value (erasing-fill-pen))
       (slate:slate-background-pixel-value (display-slate display)))
    (slate::%draw-rect (erasing-fill-pen) (display-slate display)
		       (slate-rect display)))
  (unless (null rect)
      (g::%copy-screen-rect (slate-rect display) rect)))

;;;------------------------------------------------------------

(defmethod draw-display ((display Scatter-Bucket-Presentation))
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0)))
  (let* ((slate (display-slate display))
	 (drawable (slate::slate-drawable slate))
	 (bucket (subject display))
	 (pixels (muxel-bucket-pixels bucket))
	 (nrows (muxel-bucket-nrows bucket))
	 (ncols (muxel-bucket-ncols bucket))
	 (x-channel (x-channel display))
	 (y-channel (y-channel display))
	 (glyph-size (glyph-size display)))
    (declare (type slate:CLX-Slate slate)
	     (type Muxel-Bucket bucket)
	     (type xlib:Drawable drawable)
	     (type tools:Array-Index x-channel y-channel glyph-size))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (dotimes (i nrows)
       (dotimes (j ncols)
	 (let* ((pixel (aref pixels i j))
		(bytes (muxel-bytes pixel)))
	   (declare (type Muxel pixel)
		    (type (Simple-Array (Unsigned-Byte 8) (*)) bytes))
	   (xlib:draw-rectangle
	    drawable
	    (the xlib:GContext
	      (aref (the (Simple-Array xlib:GContext (256))
		      slate::*pixval-gcontexts*)
		    (%muxel-value pixel)))
	    (aref bytes x-channel) (aref bytes y-channel)
	    glyph-size glyph-size t))))))
  (tools:stamp-time! (redraw-timestamp display)))

;;;------------------------------------------------------------

(defmethod paint-stroke ((display Scatter-Bucket-Presentation)
			 (stroke Stroke)
			 &rest options)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (ignore options))
  (let* ((slate (display-slate display))
	 (timestamp (redraw-timestamp display))
	 (drawable (slate::slate-drawable slate))
	 (bucket (subject display))
	 (pixels (muxel-bucket-pixels bucket))
	 (nrows (muxel-bucket-nrows bucket))
	 (ncols (muxel-bucket-ncols bucket))
	 (x-channel (x-channel display))
	 (y-channel (y-channel display))
	 (glyph-size (glyph-size display)))
    (declare (inline tools::%timestamp<)
	     (type slate:CLX-Slate slate)
	     (type tools:Timestamp timestamp)
	     (type Muxel-Bucket bucket)
	     (type xlib:Drawable drawable)
	     (type tools:Array-Index x-channel y-channel glyph-size))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (dotimes (i nrows)
       (dotimes (j ncols)
	 (let ((pixel (aref pixels i j)))
	   (declare (type Muxel pixel))
	   (when (tools::%timestamp< timestamp (muxel-timestamp pixel))
	     (let ((bytes (muxel-bytes pixel)))
	       (declare (type (Simple-Array (Unsigned-Byte 8) (*)) bytes))
	       (xlib:draw-rectangle
		drawable
		(the xlib:GContext
		  (aref (the (Simple-Array xlib:GContext (256))
			  slate::*pixval-gcontexts*)
			(%muxel-value pixel)))
		(aref bytes x-channel) (aref bytes y-channel)
		glyph-size glyph-size t))))))))
    (tools:atomic
   (tools:deletef display (stroke-presentations-to-be-updated stroke))
   (when (null (stroke-presentations-to-be-updated stroke))
     (return-stroke stroke)))
  (tools:stamp-time! (redraw-timestamp display)))

#||
(defmethod draw-display ((display Scatter-Bucket-Presentation))
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0)))
  (let* ((slate (display-slate display))
	 (drawable (slate::slate-drawable slate))
	 (bucket (subject display))
	 (x-channel (x-channel display))
	 (y-channel (y-channel display))
	 (glyph-size (glyph-size display)))
    (declare (type slate:CLX-Slate slate)
	     (type Muxel-Bucket bucket)
	     (type xlib:Drawable drawable)
	     (type tools:Array-Index x-channel y-channel glyph-size))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (dolist (pixel (muxel-bucket-pixels bucket))
       (declare (type Muxel pixel))
       (let ((bytes (muxel-bytes pixel)))
	 (declare (type (Simple-Array (Unsigned-Byte 8) (*)) bytes))
	 (xlib:draw-rectangle
	  drawable
	  (the xlib:GContext
	    (aref (the (Simple-Array xlib:GContext (256))
		    slate::*pixval-gcontexts*)
		  (%muxel-value pixel)))
	  (aref bytes x-channel) (aref bytes y-channel)
	  glyph-size glyph-size t)))))
  (tools:stamp-time! (redraw-timestamp display)))

;;;------------------------------------------------------------

(defmethod paint-stroke ((display Scatter-Bucket-Presentation)
			 (stroke Stroke)
			 &rest options)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (ignore options))
  (let* ((slate (display-slate display))
	 (timestamp (redraw-timestamp display))
	 (drawable (slate::slate-drawable slate))
	 (bucket (subject display))
	 (x-channel (x-channel display))
	 (y-channel (y-channel display))
	 (glyph-size (glyph-size display)))
    (declare (inline tools::%timestamp<)
	     (type slate:CLX-Slate slate)
	     (type tools:Timestamp timestamp)
	     (type Muxel-Bucket bucket)
	     (type xlib:Drawable drawable)
	     (type tools:Array-Index x-channel y-channel glyph-size))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (dolist (pixel (muxel-bucket-pixels bucket))
       (declare (type Muxel pixel))
       (when (tools::%timestamp< timestamp (muxel-timestamp pixel))
	 (let ((bytes (muxel-bytes pixel)))
	   (declare (type (Simple-Array (Unsigned-Byte 8) (*)) bytes))
	   (xlib:draw-rectangle
	    drawable
	    (the xlib:GContext
	      (aref (the (Simple-Array xlib:GContext (256))
		      slate::*pixval-gcontexts*)
		    (%muxel-value pixel)))
	    (aref bytes x-channel) (aref bytes y-channel)
	    glyph-size glyph-size t))))))
    (tools:atomic
   (tools:deletef display (stroke-presentations-to-be-updated stroke))
   (when (null (stroke-presentations-to-be-updated stroke))
     (return-stroke stroke)))
  (tools:stamp-time! (redraw-timestamp display)))
||#

;;;============================================================
;;; Activation
;;;============================================================

(defmethod (setf x-channel) (new-channel (display Scatter-Bucket-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'x-channel) new-channel)
  (erase-display display nil)
  (layout-display display nil)
  (draw-display display)
  (activate-display display nil))

(defmethod (setf y-channel) (new-channel (display Scatter-Bucket-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'y-channel) new-channel)
  (erase-display display nil)
  (layout-display display nil)
  (draw-display display)
  (activate-display display nil))

(defmethod (setf channels) (channels (display Scatter-Bucket-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'x-channel) (first channels))
  (setf (slot-value display 'y-channel) (second channels))
  (erase-display display nil)
  (layout-display display nil)
  (draw-display display)
  (activate-display display nil))

;;;============================================================
;;; Input
;;;============================================================

#||
(defmethod paint-presentation ((display Scatter-Bucket-Presentation) pen rect)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (type Scatter-Bucket-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect rect))
  (let* ((slate (display-slate display))
	 (drawable (slate::slate-drawable slate))
	 (bucket (subject display))
	 (x-channel (x-channel display))
	 (y-channel (y-channel display))
	 (size (glyph-size display))
	 (xmin (g::%screen-rect-xmin rect))
	 (ymin (g::%screen-rect-ymin rect))
	 (xmax (+ xmin (g::%screen-rect-width rect)))
	 (ymax (+ ymin (g::%screen-rect-height rect)))
	 (pixels (muxel-bucket-pixels bucket))
	 (nrows (muxel-bucket-nrows bucket))
	 (ncols (muxel-bucket-ncols bucket))
	 (pixval (slate:pen-pixel-value pen))
	 (gc (aref (the (Simple-Array xlib:Gcontext (256))
		     slate::*pixval-gcontexts*)
		   pixval))
	 (pixel (aref pixels 0 0))
	 (bytes (muxel-bytes pixel))
	 (x (aref bytes x-channel))
	 (y (aref bytes y-channel)))
    (declare (inline tools::%stamp-time!)
	     (type slate:Slate slate)
	     (type xlib:Drawable drawable)
	     (type Muxel-Bucket bucket)
	     (type (Simple-Array Muxel (* *)) pixels)
	     (type tools:Array-Index x-channel y-channel glyph-size nrows ncols)
	     (type (Unsigned-Byte 8) xmin ymin xmax ymax pixval)
	     (type xlib:Gcontext gc)
	     (type Muxel pixel)
	     (type (Simple-Array (Unsigned-Byte 8) (*)) bytes)
	     (type (Unsigned-Byte 8) x y))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (dotimes (i nrows)
       (declare (type tools:Array-Index i))
       (dotimes (j ncols)
	 (declare (type tools:Array-Index j))
	 (setf pixel (aref pixels i j))
	 (setf bytes (muxel-bytes pixel))
	 (setf x (aref bytes x-channel))
	 (setf y (aref bytes y-channel))
	 (when (and (/= (%muxel-value pixel) pixval)
		    (<= xmin x) (< x xmax)
		    (<= ymin y) (< y ymax))
	   (setf (muxel-value pixel) pixval)
	   (xlib:draw-rectangle drawable gc x y size size t)))
       (slate::%force-drawing slate)))))
||#

(defmethod paint-presentation ((display Scatter-Bucket-Presentation) pen rect)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (type Scatter-Bucket-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect rect))
  (let* ((slate (display-slate display))
	 (drawable (slate::slate-drawable slate))
	 (bins (bins display))
	 (x-channel (x-channel display))
	 (y-channel (y-channel display))
	 (size (glyph-size display))
	 (xmin (g::%screen-rect-xmin rect))
	 (ymin (g::%screen-rect-ymin rect))
	 (xmax (+ xmin (g::%screen-rect-width rect)))
	 (ymax (+ ymin (g::%screen-rect-height rect)))
	 (pixval (slate:pen-pixel-value pen))
	 (gc (aref (the (Simple-Array xlib:Gcontext (256))
		     slate::*pixval-gcontexts*)
		   pixval))
	 (display-rect (slate-rect display))
	 (xbins (array-dimension bins 1))
	 (ybins (array-dimension bins 0))
	 (bin-width (ceiling (g:screen-rect-width display-rect) xbins))
	 (bin-height (ceiling (g:screen-rect-height display-rect) ybins))
	 (xmin-bin (min xbins (max 0 (truncate xmin bin-width))))
	 (xmax-bin (min xbins (max 0 (ceiling xmax bin-width))))
	 (ymin-bin (min ybins (max 0 (truncate ymin bin-height))))
	 (ymax-bin (min ybins (max 0 (ceiling ymax bin-height)))))
    (declare (type slate:Slate slate)
	     (type xlib:Drawable drawable)
	     (type tools:Array-Index x-channel y-channel size)
	     (type (Unsigned-Byte 8)
		   xmin ymin xmax ymax xmin-bin ymin-bin xmax-bin ymax-bin
		   pixval xn)
	     (type xlib:Gcontext gc))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (loop for ibin from xmin-bin below xmax-bin do
       (loop for jbin from ymin-bin below ymax-bin do
	 (let ((bin (aref bins ibin jbin)))
	   (dolist (pixel bin)
	     (declare (type Muxel pixel))
	     (let* ((bytes (muxel-bytes pixel))
		    (x (aref bytes x-channel))
		    (y (aref bytes y-channel)))
	       (declare (type (Simple-Array (Unsigned-Byte 8) (*)) bytes)
			(type (Unsigned-Byte 8) x y))
	       (when (and (/= (%muxel-value pixel) pixval)
			  (<= xmin x) (< x xmax)
			  (<= ymin y) (< y ymax))
		 (setf (muxel-value pixel) pixval)
		 (xlib:draw-rectangle drawable gc x y size size t)))
	     ;;(slate::%force-drawing slate)
	     )))))))

