;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

(proclaim '(optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0)))

(export '(draw-series))

;;;============================================================
;;; This stuff only works under X (for now)

#+:excl (require :clx)
#-:xlib (eval-when (compile load eval)
	  (error "this file can only be used with clx"))

;;;============================================================
;;; Drawing Series

(defgeneric %draw-series (pen slate dx ys)
	    (declare (type Pen pen)
		     (type CLX-Slate slate)
		     (type g:Screen-Coordinate dx)
		     (type g:Screen-Coordinate-List ys)))

;; default method:

(defmethod %draw-series (pen slate dx ys)
  (let* ((x 0)
	 (points 
	  (tools:with-collection
	   (dolist (y ys)
	     (tools:collect (g:make-screen-point :x x :y y))
	     (incf x dx)))))  
    (%draw-polyline pen slate points)))

(defun draw-series (pen slate dx ys)
  (tools:type-check Pen pen)
  (tools:type-check CLX-Slate slate)
  (tools:type-check g:Screen-Coordinate dx)
  ;;(tools:type-check g:Screen-Coordinate-Sequence ys)
  (%draw-series pen slate dx ys))

;;;============================================================
;;; X method

(defparameter *series-coordinate-list* '(:coordinates))

(defun series-coordinates (x dx ys)
  (declare ;;(:explain :types :calls)
	   (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type g:Screen-Coordinate x dx)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) ys))
  (let* ((i 0)
	(n (length ys))
	(coordinate-list *series-coordinate-list*))
    (declare (type tools:Array-Index i n)
	     (type Cons coordinate-list *series-coordinate-list*)
	     (special *series-coordinate-list*))
    ;; first fill in the available elements of the list
    (loop
      ;; assuming we always have an even number
      ;; of spaces in the cdr of the list
      (when (null (the Cons (cdr coordinate-list))) (return))
      (setf (car (the Cons (cdr coordinate-list))) x)
      (setf coordinate-list (the Cons (cdr coordinate-list)))
      (setf (car (the Cons (cdr coordinate-list))) (aref ys i))
      (setf coordinate-list (the Cons (cdr coordinate-list)))
      (incf i)
      (incf x dx)
      (when (>= i n)
	(rplacd (the Cons coordinate-list) nil)
	(return)))
    ;; now cons the cdr of the ys onto the end
    (loop
      (when (>= i n) (return))
      (rplacd (the Cons coordinate-list) (list x (aref ys i)))
      (incf x dx)
      (incf i)
      (setf coordinate-list
	(the Cons (cdr (the Cons (cdr coordinate-list)))))))
  *series-coordinate-list*)

(defmethod %draw-series (pen (slate CLX-Slate) dx ys)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type Pen pen)
	   (type CLX-Slate slate)
	   (type g:Screen-Coordinate dx))
  (let* ((screen (slate-screen slate))
	 (x-display (x-display screen))
	 (gc (screen-pen-gcontext screen pen))
	 (drawable (slate-drawable slate))) 
    (declare (type Screen screen)
	     (type xlib:Display x-display)
	     (type g:Screen-Coordinate n)
	     (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type g:Screen-Coordinate i))
    (xlib:with-display
     (x-display)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (xlib:draw-lines drawable gc (rest (series-coordinates 0 dx ys))))))

#||
(defmethod %draw-series (pen (slate CLX-Slate) dx ys)
  (declare (optimize (speed 3)
		     (space 0)
		     (safety 1)
		     (compilation-speed 0))
	   (type Pen pen)
	   (type CLX-Slate slate)
	   (type g:Screen-Coordinate dx))
  (let* ((x 0)
	 (l (length ys))
	 (n (* 2 l))
	 (gc (slate-gcontext slate))
	 (drawable (slate-drawable slate))
	 (i 0)) 
    (declare (type g:Screen-Coordinate x l n)
	     (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type g:Screen-Coordinate i))
    (update-gcontext-from-pen pen (slate-screen slate) gc)
    (xlib:draw-lines drawable gc (tools:with-collection
				  (dotimes (k l)
				    (declare (type Fixnum k))
				    (tools:collect x)
				    (tools:collect (aref ys k))
				    (incf x dx))))
    (%move-to-xy slate x (aref ys (- l 1)))))


(defparameter *series-coordinate-vector*
    (make-array 0 :element-type 'xlib:Int16))

(defun series-coordinate-vector (n)
  (unless (= (length *series-coordinate-vector*) n)
    (setf *series-coordinate-vector*
      (make-array n
		  :element-type 'xlib:Int16
		  :initial-element 0)))
  *series-coordinate-vector*)

(defmethod %draw-series (pen (slate CLX-Slate) dx ys)
  (declare (optimize (speed 3)
		     (space 0)
		     (safety 1)
		     (compilation-speed 0))
	   (type Pen pen)
	   (type CLX-Slate slate)
	   (type g:Screen-Coordinate dx))
  (let* ((x 0)
	 (n (* 2 (length ys)))
	 (gc (slate-gcontext slate))
	 (drawable (slate-drawable slate))
	 (pxys (the (Simple-Array xlib:Int16 (*))
		 (series-coordinate-vector n)))
	 (i 0)) 
    (declare (type g:Screen-Coordinate x n)
	     (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type (Simple-Array g:Screen-Coordinate (*)) pxys)
	     (type g:Screen-Coordinate i))
    (update-gcontext-from-pen pen (slate-screen slate) gc)
    (dotimes (k (length ys))
      (setf (aref pxys i) x)
      (incf x dx)
      (incf i)
      (setf (aref pxys i) (aref ys k))
      (incf i))
    (xlib:draw-lines drawable gc (the (Simple-Array xlib:Int16 (*)) pxys))
    (%move-to-xy slate (aref pxys (- n 2)) (aref pxys (- n 1))))) 
||#