#include 	<fcntl.h>
#include 	<stdio.h>
#include	<math.h>

#define MAXCHARS 128

/* for consistency with ANSI C: */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

void read_byte_array (path, nskip, nbytes, array)

     char path[MAXCHARS];
     int nskip, nbytes;
     char* array;
{
  int inpfl;			/* file ptr */

  printf("reading %d bytes from %s\n", nbytes, path); 
  fflush(stdout);
  if ((inpfl = open(path,O_RDONLY,0)) == -1)
    { printf("can't open input file %s\n", path); }
  else
    { if (read(inpfl,array,nskip) < nskip) 
	{ printf("error on file %s\n", path); }
      if (read(inpfl,array,nbytes) < nbytes) 
	{ printf("error on file %s\n", path); }
      }
}

void read_padded_image (path, nskip, nrows, ncols, padded_ncols, array)

     char path[MAXCHARS];
     int nskip, nrows, ncols, padded_ncols;
     char* array;
{
  FILE *input;	
  int  i, pad;

  pad = padded_ncols - ncols;

  printf("reading %d bytes from %s\n", nrows*ncols, path); 
  fflush(stdout);
  
  /* open the image file */
  input = fopen(path,"r");
  if (input == NULL)
    fprintf(stderr, "can't open input file %s\n", path);
  /* skip the header, if any */
  else if ((nskip >> 0) && (0 != fseek(input, (long int) nskip, SEEK_SET)) )
    fprintf(stderr,"error on file %s\n", path);
  else
    { if (pad == 0)		/* then read it in one chunk */
       { if (fread(array,1,ncols*nrows,input) < ncols*nrows)
	  fprintf(stderr,"error on file %s\n", path);
       }
    else			/* read it in a row at a time */	
      { for(i=0; i<nrows; i++)
	  { if (fread(array,1,ncols,input) < ncols)
	      fprintf(stderr,"error on file %s\n", path);
	    array = array + padded_ncols;
	  }
      }
    }

  /* close the image file */
  fclose(input);
}

