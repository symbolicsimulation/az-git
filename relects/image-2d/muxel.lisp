;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defstruct (Muxel
	    (:conc-name nil)
	    (:constructor %make-muxel))
  (muxel-x 0 :type Tools:Array-Index)
  (muxel-y 0 :type Tools:Array-Index)
  (muxel-nchannels 0 :type Tools:Array-Index)
  (%muxel-value #b11111111 :type Byte)
  (muxel-timestamp (tools:make-timestamp) :type tools:Timestamp)
  (muxel-bytes (make-array 0 :element-type 'Byte)
		   :type (Simple-Array (Unsigned-Byte 8) (*))))

;;;------------------------------------------------------------

(defun make-muxel (x y nchannels)
  (%make-muxel
   :muxel-x x
   :muxel-y y
   :muxel-nchannels nchannels
   :muxel-bytes (make-array nchannels :element-type 'Byte)))

;;;------------------------------------------------------------

(proclaim '(inline muxel-byte))
(defun muxel-byte (muxel i)
  (declare (optimize (speed 3)
		     (space 0)
		     (safety 1)
		     (compilation-speed 0))
	   (type Muxel muxel)
	   (type Tools:Array-Index i))
  (aref (the (Simple-Array (Unsigned-Byte 8) (*))
	  (muxel-bytes muxel))
	i))

(defsetf muxel-byte (pixel i) (x)
  `(locally (declare (optimize (safety 1)
			       (space 0)
			       (speed 3)
			       (compilation-speed 0))
		     (type Muxel ,pixel)
		     (type Tools:Array-Index ,i)
		     (type (Unsigned-Byte 8) ,x))
     (setf (aref (the (Simple-Array (Unsigned-Byte 8) (*)) (muxel-bytes ,pixel)) ,i)
       ,x)))

;;;------------------------------------------------------------
;;; accessors for the pixel value that update the timestamp

(proclaim '(inline muxel-value))
(defun muxel-value (muxel)
  (declare (optimize (speed 3)
		     (space 0)
		     (safety 1)
		     (compilation-speed 0))
	   (type Muxel muxel))
  (%muxel-value muxel))

(defsetf muxel-value (muxel) (x)
  `(locally (declare (optimize (safety 1)
			       (space 0)
			       (speed 3)
			       (compilation-speed 0))
		     (inline tools::%stamp-time!)
		     (type Muxel ,muxel)
		     (type (Unsigned-Byte 8) ,x))
     (tools:atomic
      (setf (%muxel-value ,muxel) ,x)
      (tools::%stamp-time! (muxel-timestamp ,muxel)))))


