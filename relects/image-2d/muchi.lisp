;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

(export '(Muchi-Channel
	  Muchi-Channel-Vector
	  Muchi))

;;;============================================================

(deftype Muxel-Table-Bin () '(or Null (Simple-Array Fixnum (*))))

(deftype 2d-Muxel-Table () '(Simple-Array Muxel-Table-Bin (256 256)))

(deftype Histogram-Array () '(Simple-Array Fixnum 256))
(deftype 2d-Histogram-Array () '(Simple-Array Fixnum (256 256)))

;;;============================================================
;;; Muchi Channel arrays
;;;============================================================

(deftype Muchi-Channel () '(Simple-Array (Unsigned-Byte 8) (* *)))

(defun make-muchi-channel (nrows ncols &key (initial-element 0))
  (make-array (list nrows ncols)
	      :element-type '(Unsigned-Byte 8) 
	      :initial-element initial-element))

(defun muchi-channel-nrows (muchi-channel)
  (tools:type-check Muchi-Channel muchi-channel)
  (array-dimension muchi-channel 0))

(defun muchi-channel-ncols (muchi-channel)
  (tools:type-check Muchi-Channel muchi-channel)
  (array-dimension muchi-channel 1))

;;;------------------------------------------------------------
;;; Note that this assumes that the output array is passed
;;; already zeroed when it's passed in.

(defun histogram-muchi-channel (channel
				&key
				(histogram-array
				 (make-array '256
					     :element-type 'Fixnum
					     :initial-element 0)))
  (declare (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3)))
  (tools:type-check Muchi-Channel channel)
  (tools:type-check Histogram-Array histogram-array)
  (let ((nrows (muchi-channel-nrows channel))
	(ncols (muchi-channel-ncols channel))
	(channel channel)
	(histogram-array histogram-array))
    (declare (type tools:Array-Index nrows ncols)
	     (type Muchi-Channel channel)
	     (type Histogram-Array histogram-array))
    (dotimes (i nrows)
      (declare (type tools:Array-Index i))
      (dotimes (j ncols)
	(declare (type tools:Array-Index j))
	(incf (aref histogram-array (aref channel i j)))))))

(defun 2d-histogram-muchi-channel (channel0 channel1
				   &key
				   (histogram-array
				    (make-array '(256 256)
						:element-type 'Fixnum
						:initial-element 0)))
  (declare ;;(:explain :types :calls)
	   (optimize (compilation-speed 0)
		     (safety 1)
		     (space 0)
		     (speed 3)))
  (tools:type-check Muchi-Channel channel0 channel1)
  (tools:type-check 2d-Histogram-Array histogram-array)
  (let ((nrows (muchi-channel-nrows channel0))
	(ncols (muchi-channel-ncols channel0))
	(channel0 channel0)
	(channel1 channel1)
	(histogram-array histogram-array))
    (declare (type tools:Array-Index nrows ncols)
	     (type Muchi-Channel channel0 channel1)
	     (type 2d-Histogram-Array histogram-array))
    (dotimes (i nrows)
      (declare (type tools:Array-Index i))
      (dotimes (j ncols)
	(declare (type tools:Array-Index j))
	(incf (aref histogram-array
		    (aref channel1 i j) (aref channel0 i j)))))))

;;;============================================================
;;; A vector of Muchi Channel arrays
;;;============================================================

(deftype Muchi-Channel-Vector () '(Simple-Array Muchi-Channel (*)))

(defun make-muchi-channel-vector (nchannels nrows ncols
				 &key (initial-element 0))
  (let ((v (make-array nchannels
		       :element-type `Muchi-Channel
		       :initial-element (make-muchi-channel 0 0))))
    (dotimes (channel nchannels)
      (setf (aref v channel)
	(make-muchi-channel nrows ncols :initial-element initial-element)))
    v))

;;;============================================================
;;; Mutli-Channel Image Objects
;;;============================================================

(defstruct (Muchi
	    (:conc-name nil)
	    (:constructor %make-muchi)
	    (:print-function print-muchi))

  (muchi-nchannels 0 :type tools:Array-Index)
  (muchi-nrows 0 :type tools:Array-Index)
  (muchi-ncols 0 :type tools:Array-Index)
  (muchi-channels (make-muchi-channel-vector 0 0 0)
		  :type Muchi-Channel-Vector)
  (muchi-paint (make-muchi-channel 0 0) :type Muchi-Channel)
  (muchi-paint-pixmaps () :type List))

;;;------------------------------------------------------------

(defun print-muchi (muchi stream depth)
  (declare (ignore depth))
  (format stream "{Muchi ~dx~dx~d}"
	  (muchi-nchannels muchi)
	  (muchi-nrows muchi)
	  (muchi-ncols muchi)))

;;;------------------------------------------------------------

(defun make-muchi (nchannels nrows ncols)
  (%make-muchi
   :muchi-nchannels nchannels
   :muchi-nrows nrows
   :muchi-ncols ncols
   :muchi-paint
   (make-muchi-channel nrows ncols :initial-element #b11100000)
   :muchi-channels (make-muchi-channel-vector nchannels nrows ncols)))

(defun read-muchi (path nchannels nrows ncols)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0)))
  (let* ((image (make-muchi nchannels nrows ncols))
	 (channels (muchi-channels image)))
    (declare (type Muchi muchi)
	     (type Muchi-Channel-Vector channels))
    (with-open-file (str path
		     :direction :input
		     :element-type '(Unsigned-Byte 8))
      (fresh-line)
      (dotimes (i nrows)
	(format t "~d " i)
	(dotimes (j ncols)
	  (dotimes (k nchannels)
	    (setf (aref (aref channels k) i j) (read-byte str))))))
    image))

(defun advance-stream (nbytes stream)
  (declare (type tools:Array-Index nbytes)
	   (type Stream stream))
  (dotimes (i nbytes)
    (declare (type tools:Array-Index i))
    (read-byte stream)))

(defun read-muchi-channels (paths
			    &key
			    (nchannels (length paths))
			    (nrows 0) (ncols 0)
			    (image-nrows nrows) (image-ncols ncols)
			    (skip-bytes 0)
			    (skip-rows 0)
			    (skip-cols 0))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0))
	   (type List paths)
	   (type tools:Array-Index
		 nchannels nrows ncols image-nrows image-ncols
		 skip-bytes skip-rows skip-cols)
	   (ignore image-nrows))
  #+:excl (excl:gc t)
  (let* ((image (make-muchi nchannels nrows ncols))
	 (channels (muchi-channels image))
	 (k 0)
	 #+:excl (old-gc-spread (sys:gsgc-parameter :generation-spread))
	 #+:excl (excl:*record-source-files* nil)
	 #+:excl (excl:*tenured-bytes-limit* nil))
    (declare #+:excl (special excl:*record-source-files*
			      excl:*tenured-bytes-limit*)
	     (type Muchi muchi)
	     (type Muchi-Channel-Vector channels))    
    #+:excl (setf (sys:gsgc-parameter :generation-spread) 1)
    (dolist (path paths)
      (print k) (finish-output)
      (when (>= k nchannels) (return))
      (let ((channel (aref channels k)))
	(declare (type Muchi-Channel channel))
	(with-open-file (str path
			 :direction :input
			 :element-type '(Unsigned-Byte 8))
	  (advance-stream (+ skip-bytes (* skip-rows image-ncols)) str)
	  (dotimes (i nrows)
	    (declare (type tools:Array-Index i))
	    (advance-stream skip-cols str)
	    (dotimes (j ncols)
	      (declare (type tools:Array-Index j))
	      (setf (aref channel i j) (read-byte str)))
	    (advance-stream (- image-ncols ncols skip-cols) str)))
	(incf k)))
    #+:excl
    (progn (print "gc") (finish-output)
	   (excl:gc :tenure) (excl:gc :tenure) (excl:gc t) (room t)
	   (setf (sys:gsgc-parameter :generation-spread) old-gc-spread))

    image))

(defun c-read-muchi-channels (paths
			      &key
			      (nchannels (length paths))
			      (nrows 0) (ncols 0)
			      (image-nrows nrows)
			      (image-ncols ncols)
			      (skip-bytes 0)
			      (skip-rows 0)
			      (skip-cols 0))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0))
	   (type List paths)
	   (type tools:Array-Index
		 nchannels nrows ncols image-nrows image-ncols
		 skip-bytes skip-rows skip-cols)
	   (ignore skip-cols)
	   (ignore image-nrows))
  ;;(tools:with-gc-tuned-for-large-allocation
  (let* ((image (make-muchi nchannels nrows ncols))
	 (channels (muchi-channels image))
	 (k 0))
    (declare (type Muchi muchi)
	     (type Muchi-Channel-Vector channels))    
    (dolist (path paths)
      (when (>= k nchannels) (return))
      (let ((channel (aref channels k)))
	(declare (type Muchi-Channel channel))
	(read_byte_array path
			 (+ skip-bytes (* image-ncols skip-rows))
			 (* nrows ncols)
			 channel)
	(incf k)))

    image)
  ;; )
  )

;;;============================================================
;;; Drawing Magnified Much Image channels
;;;============================================================

(defun draw-magnified-muchi-paint (pen slate muchi mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type slate:Pen pen)
	   (ignore pen)
	   (type slate:CLX-Slate slate)
	   (type Muchi muchi)
	   (type tools:Array-Index channel)
	   (type slate:Magnification-Level mag))
  (let* ((drawable (the xlib:Drawable (slate::slate-drawable slate)))
	 (nrows (muchi-nrows muchi))
	 (ncols (muchi-ncols muchi))
	 (muchi-paint (muchi-paint muchi))) 
    (declare (type xlib:Drawable drawable)
	     (type Muchi-Channel muchi-paint))
    (if (= mag 1)
	(dotimes (i nrows)
	  (declare (type tools:Array-Index i))
	  (dotimes (j ncols)
	    (declare (type tools:Array-Index j))
	    (xlib:draw-rectangle
	     drawable
	     (aref (the (Simple-Array xlib:GContext (*))
		     slate::*pixval-gcontexts*)
		   (aref muchi-paint i j))
	     j i 1 1 t)))
      ;; else
      (dotimes (i nrows)
	(declare (type tools:Array-Index i))
	(dotimes (j ncols)
	  (declare (type tools:Array-Index j))
	  (xlib:draw-rectangle
	   drawable
	   (aref (the (Simple-Array xlib:GContext (*))
		   slate::*pixval-gcontexts*)
		 (aref muchi-paint i j))
	   (the xlib:Card16 (* mag j))
	   (the xlib:Card16 (* mag i))
	   mag mag t))))))

(defun draw-magnified-muchi-channel (pen slate muchi channel mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type slate:Pen pen)
	   (ignore pen)
	   (type slate:CLX-Slate slate)
	   (type Muchi muchi)
	   (type tools:Array-Index channel)
	   (type slate:Magnification-Level mag))
  (let* ((drawable (the xlib:Drawable (slate::slate-drawable slate)))
	 (nrows (muchi-nrows muchi))
	 (ncols (muchi-ncols muchi))
	 (muchi-channel (aref (muchi-channels muchi) channel))
	 (muchi-paint (muchi-paint muchi))) 
    (declare (type xlib:Drawable drawable)
	     (type Muchi-Channel muchi-channel muchi-paint))
    (if (= mag 1)
	(dotimes (i nrows)
	  (declare (type tools:Array-Index i))
	  (dotimes (j ncols)
	    (declare (type tools:Array-Index j))
	    (xlib:draw-rectangle
	     drawable
	     (aref (the (Simple-Array xlib:GContext (*))
		     slate::*pixval-gcontexts*)
		   (logior
		    (the (Unsigned-Byte 8)
		      (ash (ldb
			    (byte 3 5)
			    (the (Unsigned-Byte 8) (aref muchi-paint i j)))
			   5))
		    (the (Unsigned-Byte 8) (ash (aref muchi-channel i j) -3))))
	     j i 1 1 t)))
      ;; else
      (dotimes (i nrows)
	(declare (type tools:Array-Index i))
	(dotimes (j ncols)
	  (declare (type tools:Array-Index j))
	  (xlib:draw-rectangle
	   drawable
	   (aref (the (Simple-Array xlib:GContext (*))
		   slate::*pixval-gcontexts*)
		 (logior
		  (the (Unsigned-Byte 8)
		    (ash (ldb
			  (byte 3 5)
			  (the (Unsigned-Byte 8) (aref muchi-paint i j)))
			 5))
		  (the (Unsigned-Byte 8) (ash (aref muchi-channel i j) -3))))
	   (the xlib:Card16 (* mag j))
	   (the xlib:Card16 (* mag i))
	   mag mag t))))))

#||
     (if (= mag 1)
	 (let ((image (xlib:create-image
		       :data muchi-channel
		       :depth 8 :format :z-format :height nrows :width ncols)))
	   (xlib:put-image
	    drawable (slate::screen-pen-gcontext screen pen) image
	    :x 0 :y 0 :width ncols :height nrows :src-x 0 :src-y 0))
       ;; else draw a rectangle for each pixel

||#

;;;============================================================

(defun magnified-muchi-movie (slate muchi mag)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type slate:CLX-Slate slate)
	   (type Muchi-Channel muchi-channel)
	   (type slate:Magnification-Level mag))
  (let ((nchannels (muchi-nchannels muchi))) 
    (dotimes (i nchannels)
      (draw-magnified-muchi-channel
       nil slate muchi i mag))
    (dotimes (i nchannels)
      (draw-magnified-muchi-channel
       nil slate muchi (- nchannels i 1) mag))
    (slate:finish-drawing slate)))

