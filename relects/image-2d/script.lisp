;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
(in-package :Clay) 

(setf *print-array* t)
(setf *print-pretty* nil)
(setf *print-length* 8)









(setf *left-image-painting-pen*
  *red-image-painting-pen*)
(setf *left-image-painting-pen*
  *green-image-painting-pen*)
(setf *left-image-painting-pen*
  *blue-image-painting-pen*)
(setf *left-image-painting-pen*
  *yellow-image-painting-pen*)
(setf *left-image-painting-pen*
  *magenta-image-painting-pen*)
(setf *left-image-painting-pen*
  *cyan-image-painting-pen*)

(setf *middle-image-painting-pen* *red-image-painting-pen*)
(setf *middle-image-painting-pen* *green-image-painting-pen*)
(setf *middle-image-painting-pen* *blue-image-painting-pen*)
(setf *middle-image-painting-pen* *yellow-image-painting-pen*)
(setf *middle-image-painting-pen* *magenta-image-painting-pen*)
(setf *middle-image-painting-pen* *cyan-image-painting-pen*)

(progn  
  (defparameter *mri-muchi*
      (time (c-read-muchi-channels
	     (list "/belgica-2g/jam/az/examples/mri/pd.byte"
		   "/belgica-2g/jam/az/examples/mri/t1.byte"
		   "/belgica-2g/jam/az/examples/mri/t2.byte")
	     :nrows 256
	     :ncols 256)))

  (defparameter *mri-presentation-0*
      (time
       (make-presentation-for-muchi
	*mri-muchi*
	(slate:make-slate
	 :left 4 :top 4 :width 512 :height 512 :backing-store :always)
	:channel 0)))
  (defparameter *mri-presentation-1*
      (time
       (make-presentation-for-muchi
	*mri-muchi*
	(slate:make-slate
	 :left 528 :top 4 :width 512 :height 512 :backing-store :always)
	:channel 1)))

  (defparameter *muchi-scatter-01*
      (time
       (make-scatter-presentation-for-muchi
	*mri-muchi*
	(slate:make-slate
	 :left 600 :top 560 :width 512 :height 512 :backing-store :always)
	:x-channel 0 :y-channel 1 :glyph-size 4)))
  (defparameter *mri-presentation-2*
      (time
       (make-presentation-for-muchi
	*mri-muchi*
	(slate:make-slate :width 256 :height 256 :backing-store :always)
	:channel 2)))

  (defparameter *muchi-scatter-21*
      (time
       (make-scatter-presentation-for-muchi
	*mri-muchi*
	(slate:make-slate :width 512 :height 512 :backing-store :always)
	:x-channel 2 :y-channel 1 :glyph-size 4)))

  (defparameter *muchi-scatter-02*
      (time
       (make-scatter-presentation-for-muchi
	*mri-muchi*
	(slate:make-slate :width 512 :height 512 :backing-store :always)
	:x-channel 0 :y-channel 2 :glyph-size 4)))
  ) 

(setf (glyph-size *muchi-scatter-01*) 4)
(draw-display *muchi-scatter-01*)
(defparameter *paint-slate*
    (slate:make-slate :width 512 :height 512 :backing-store :always))
(slate:clear-slate *paint-slate*) 
(draw-magnified-muchi-paint
 *gray-image-painting-pen* *paint-slate* *mri-muchi* 2)

(time (draw-display *muchi-scatter-01*))
(time (x-draw-display *muchi-scatter-01*))
(time (dotimes (i 10) (draw-display *muchi-scatter-01*)))



(bins *muchi-scatter-01*)


(tools:deletef (first (subject-presentations *mri-muchi*))
	       (subject-presentations *mri-muchi*))

(subject *mri-presentation-0*)

(subject-presentations (subject *muchi-scatter-01*))
(describe *muchi-scatter-01*)

(defparameter *slate*
    (slate:make-slate :width 512
		      :height 512
		      :backing-store :always))

(slate:clear-slate *slate*)
(time
 (progn  

  (draw-magnified-muchi-channel
   *gray-image-painting-pen* *slate*
   (aref (muchi-channels *mri-muchi*) 0) 2)

  (slate:finish-drawing *slate*)))

(setf (slate:screen-colormap *screen*) *gray-colormap*)
(setf (slate:screen-colormap *screen*) *standard-colormap*)
(setf (slate:screen-colormap *screen*) *colornames-colormap*)
(setf (slate:screen-colormap *screen*) *image-painting-colormap*)


(setf (slate:screen-colormap *screen*) *egami-colormap*)

(setf *image-array* nil)


(tools:with-gc-tuned-for-large-allocation
 (defparameter *manous-nrows* 1024)
 (defparameter *manous-ncols* 1024)
 (defparameter *manous-muchi*
     (time (c-read-muchi-channels
	    (list "/belgica-0g/local/az/examples/manous/88-man-conf-b1.pic"
		  "/belgica-0g/local/az/examples/manous/88-man-conf-b2.pic"
		  "/belgica-0g/local/az/examples/manous/88-man-conf-b3.pic"
		  "/belgica-0g/local/az/examples/manous/88-man-conf-b4.pic"
		  "/belgica-0g/local/az/examples/manous/88-man-conf-b5.pic"
		  "/belgica-0g/local/az/examples/manous/88-man-conf-b7.pic"
		  )
	    :nrows *manous-nrows*
	    :ncols *manous-ncols*
	    ;;:skip-rows 400
	    ;;:skip-cols 400
	    ;;:image-nrows 1024
	    ;;:image-ncols 1024
	    :skip-bytes 512)))

 (defparameter *manous-scatter-03*
     (time
      (make-scatter-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 0 :top 600 :width 260 :height 260
			 :backing-store :always)
       :x-channel 0 :y-channel 3 :glyph-size 1)))
#||
 (defparameter *manous-scatter-04*
     (time
      (make-scatter-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 270 :top 600 :width 260 :height 260
			 :backing-store :always)
       :x-channel 0 :y-channel 4 :glyph-size 1)))

 (defparameter *manous-scatter-34*
     (time
      (make-scatter-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 540 :top 600 :width 260 :height 260
			 :backing-store :always)
       :x-channel 3 :y-channel 4 :glyph-size 1)))

 (defparameter *manous-presentation-3*
     (time
      (make-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 0 :top 0 :width 1024 :height 1024
			 :backing-store :always)
       :channel 3)))

 (defparameter *manous-presentation-0*
     (time
      (make-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 0 :top 0 :width 1024 :height 1024
			 :backing-store :always)
       :channel 0)))
||#
#||
 (defparameter *manous-presentation-1*
     (time
      (make-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 0 :top 0 :width 1024 :height 1024
			 :backing-store :always)
       :channel 1)))
 (defparameter *manous-presentation-2*
     (time
      (make-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 0 :top 0 :width 1024 :height 1024
			 :backing-store :always)
       :channel 2)))
||#
#||
 (defparameter *manous-presentation-4*
     (time
      (make-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 0 :top 0 :width 1024 :height 1024
			 :backing-store :always)
       :channel 4)))
||#
#||
 (defparameter *manous-presentation-5*
     (time
      (make-presentation-for-muchi
       *manous-muchi*
       (slate:make-slate :left 0 :top 0 :width 1024 :height 1024
			 :backing-store :always)
       :channel 5)))
||#
)



(defparameter *test-slate* (slate:make-slate :width 1024 :height 1024
					     :backing-store :always))

(time
 (let* ((slate *test-slate*)
       (muchi *manous-muchi*)
       (pen (default-pen))
       (nrows (muchi-nrows muchi))
       (ncols (muchi-ncols muchi))
       (channel 0)
       (drawable (the xlib:Drawable (slate::slate-drawable slate)))
       (screen (slate:slate-screen slate))
       (muchi-channel (aref (muchi-channels muchi) channel))
       (x-display (slate::x-display screen))
       (image (xlib:create-image
	       :data muchi-channel
	       :depth 8 :format :z-format
	       :height nrows :width ncols)))
  (xlib:put-image
   drawable
   (slate::screen-pen-gcontext screen pen) image
   :x 0 :y 0 :width ncols :height nrows :src-x 0 :src-y 0)))

(defparameter *manous-scatter-01*
    (time
     (make-scatter-presentation-for-muchi
      *manous-muchi*
      (slate:make-slate :width 260 :height 260 :backing-store :always)
      :x-channel 0 :y-channel 1 :glyph-size 2)))

(dotimes (i 13)
  (excl:gc t)
  (let ((*print-array* nil)
	(*print-pretty* nil))
    (setf *rect-buffer-length* (ash 2 i))
    (setf (slot-value *manous-scatter-01* 'rect-buffers)
      (vector
       ()
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)
       (make-list (* 4 *rect-buffer-length*) :initial-element 0)))
    (setf (slot-value  *manous-scatter-01* 'rect-buffer-ptrs)
      (copy-seq (rect-buffers  *manous-scatter-01*)))
    (print *rect-buffer-length*)
    (fresh-line)
    (erase-display *manous-scatter-01* nil)
    (time
     (progn (draw-display *manous-scatter-01*)
	    (slate:finish-drawing (display-slate *manous-scatter-01*))))
    (finish-output)
    (sleep 60)))

(dotimes (i 4)
  #||
  (time
   (progn
     (draw-display *manous-scatter-01*)
     (slate:finish-drawing (display-slate *manous-scatter-01*))))
  ||#
  (time
   (progn (x-draw-display *manous-scatter-01*)
	  (slate:finish-drawing (display-slate *manous-scatter-01*)))))

(dotimes (i 10)
  (draw-display *manous-scatter-01*))

(draw-display *manous-presentation-0*)
(slate:clear-slate *manous-slate*)
(time
 (progn  
  (draw-magnified-muchi-channel
   (default-pen) *manous-slate*
   (aref (muchi-channels *manous-muchi*) 0) 1)
  (slate:finish-drawing *manous-slate*)))


(defparameter *mri-bucket*
    (time (read-packed-muxel-bucket-planes
	   (list "/belgica-2g/jam/az/examples/mri/pd.byte"
		 "/belgica-2g/jam/az/examples/mri/t1.byte"
		 "/belgica-2g/jam/az/examples/mri/t2.byte")
	   :nchannels 3 :nrows 256 :ncols 256)))
(progn 
(defparameter *mri-presentation-0*
    (time
     (make-presentation-for-muxel-bucket
      *mri-bucket*
      (slate:make-slate :width 256 :height 256 :backing-store :always)
      :channel 0)))

(defparameter *mri-scatter-bucket-01*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *mri-bucket*
      (slate:make-slate :width 256 :height 256 :backing-store :always)
      :x-channel 0 :y-channel 1 :glyph-size 1)))
	
(defparameter *mri-scatter-bucket-12*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *mri-bucket*
      (slate:make-slate :width 256
			:height 256
			:backing-store :always)
      :x-channel 1
      :y-channel 2
      :glyph-size 2)))

(defparameter *mri-scatter-bucket-20*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *mri-bucket*
      (slate:make-slate :width 256
			:height 256
			:backing-store :always)
      :x-channel 2
      :y-channel 0
      :glyph-size 2)))

(defparameter *mri-presentation-1*
    (time
     (make-presentation-for-muxel-bucket
      *mri-bucket*
      (slate:make-slate :width 256 :height 256
			:backing-store :always)
      :channel 1)))

(defparameter *mri-presentation-2*
    (time
     (make-presentation-for-muxel-bucket
      *mri-bucket*
      (slate:make-slate :width 256 :height 256
			:backing-store :always)
      :channel 2)))
)
		  
(progn
  (setf (slate:screen-colormap *screen*) *image-painting-colormap*)
  (defparameter *manous-nrows* 256)
  (defparameter *manous-ncols* 256)
  (defparameter *manous-bucket*
      (time (read-packed-muxel-bucket-planes
	     (list "/belgica-0g/local/az/examples/manous/88-man-conf-b1.pic"
		   "/belgica-0g/local/az/examples/manous/88-man-conf-b2.pic"
		   "/belgica-0g/local/az/examples/manous/88-man-conf-b3.pic"
		   "/belgica-0g/local/az/examples/manous/88-man-conf-b4.pic"
		   "/belgica-0g/local/az/examples/manous/88-man-conf-b5.pic"
		   "/belgica-0g/local/az/examples/manous/88-man-conf-b7.pic")
	     :nchannels 6 
	     :nrows *manous-nrows*
	     :ncols *manous-ncols*
	     :skip-rows 400
	     :skip-cols 400
	     :image-nrows 1024
	     :image-ncols 1024
	     :skip-bytes 512)))) 

(defparameter *manous-image-presentations*
    (time
     (dotimes (channel 6)
       (make-presentation-for-muxel-bucket
	*manous-bucket*
	(slate:make-slate
	 :width *manous-ncols*
	 :height *manous-nrows*)
	:channel channel))))

(defparameter *manous-image-presentation-0*
    (time
     (make-presentation-for-muxel-bucket
      *manous-bucket*
      (slate:make-slate
       :width *manous-ncols*
       :height *manous-nrows*)
      :channel 0)))

(defparameter *manous-scatter-presentations*
    (time
     (dotimes (i 6)
       (loop for j from (+ i 1) below 6 do
	 (make-scatter-presentation-for-muxel-bucket
	  *manous-bucket*
	  (slate:make-slate :width 256 :height 256)
	  :x-channel i :y-channel j :glyph-size 2)))))
(progn
  (defparameter *raimundos-nrows* 489)
  (defparameter *raimundos-ncols* 1077)
  (defparameter *raimundos-bucket*
      (time (read-packed-muxel-bucket-planes
	     (list "/belgica-0g/local/az/examples/raimundos/raimundos-b1.pic"
		   "/belgica-0g/local/az/examples/raimundos/raimundos-b2.pic"
		   "/belgica-0g/local/az/examples/raimundos/raimundos-b3.pic"
		   "/belgica-0g/local/az/examples/raimundos/raimundos-b4.pic"
		   "/belgica-0g/local/az/examples/raimundos/raimundos-b5.pic"
		   "/belgica-0g/local/az/examples/raimundos/raimundos-b7.pic")
	     :nchannels 6 
	     :nrows *raimundos-nrows*
	     :ncols *raimundos-ncols*
	     :image-nrows 489
	     :image-ncols 1077
	     :skip-bytes 512))))
 
(defparameter *raimundos-presentation-3*
    (time
       (make-presentation-for-muxel-bucket
	*raimundos-bucket*
	(slate:make-slate
	 :width *raimundos-ncols*
	 :height *raimundos-nrows*)
	:channel 3)))

(defparameter *raimundos-presentation-4*
    (time
       (make-presentation-for-muxel-bucket
	*raimundos-bucket*
	(slate:make-slate
	 :width *raimundos-ncols*
	 :height *raimundos-nrows*)
	:channel 4)))

(defparameter *raimundos-scatter-bucket-34*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *raimundos-bucket*
      (slate:make-slate :width 256 :height 256)
      :x-channel 3 :y-channel 4 :glyph-size 1)))
(defparameter *raimundos-presentations*
    (time
     (dotimes (channel 6)
       (make-presentation-for-muxel-bucket
	*raimundos-bucket*
	(slate:make-slate
	 :width *raimundos-ncols*
	 :height *raimundos-nrows*)
	:channel channel))))


(defparameter *raimundos-scatter-bucket-23*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *raimundos-bucket*
      (slate:make-slate :width 256 :height 256)
      :x-channel 2 :y-channel 3 :glyph-size 1)))

(defparameter *raimundos-scatter-bucket-34*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *raimundos-bucket*
      (slate:make-slate :width 256 :height 256)
      :x-channel 3 :y-channel 4 :glyph-size 1)))
	
(defparameter *raimundos-scatter-bucket-12*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *raimundos-bucket*
      (slate:make-slate :width 256
			:height 256)
      :x-channel 1
      :y-channel 2
      :glyph-size 1)))

(defparameter *raimundos-scatter-bucket-20*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *raimundos-bucket*
      (slate:make-slate :width 256
			:height 256)
      :x-channel 2
      :y-channel 0
      :glyph-size 1)))
)

(defparameter *pet-bucket*
    (time 
     (read-packed-muxel-bucket
      *thym-packed-path* *thym-times-path* 39 72 72)))

(describe *pet-bucket*)
(subject-presentations *pet-bucket*)

(tools:deletef (first (subject-presentations *pet-bucket*))
	       (subject-presentations *pet-bucket*))
(tools:deletef (third (subject-presentations *pet-bucket*))
	       (subject-presentations *pet-bucket*))
	       
(defparameter *pet-presentation*
    (time
     (make-presentation-for-muxel-bucket
      *pet-bucket*
      (slate:make-slate
       :width (* 3 72)
       :height (* 3 72)
       :backing-store :always)
      :channel 10)))

(defparameter *pet-presentation-1*
    (time
     (make-presentation-for-muxel-bucket
      *pet-bucket*
      (slate:make-slate :width (* 3 72) :height (* 3 72)
			:backing-store :always)
      :channel 8)))

(defparameter *spaghetti-bucket*
    (time
     (make-spaghetti-presentation-for-muxel-bucket
      *pet-bucket*
      (slate:make-slate :width 380 :height 256 :backing-store :always))))

(defparameter *scatter-bucket*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *pet-bucket*
      (slate:make-slate :width 256 :height 256 :backing-store :always)
      :x-channel 8
      :y-channel 12)))



(defparameter *small-pet-bucket*
    (time 
     (muxel-bucket-subset
      *pet-bucket*
      #'(lambda (pixel) (muxel-over-threshold pixel 64)))))

(write-packed-muxel-bucket-planes (list "/belgica-2g/jam/az/examples/mri/pd.byte"
					"/belgica-2g/jam/az/examples/mri/t1.byte"
					"/belgica-2g/jam/az/examples/mri/t2.byte")
				  *mri-bucket*)


(defparameter *mri-bucket*
    (time (read-byte-bucket-planes
	   (list "/belgica-2g/jam/az/examples/mri/pd.ascii"
		 "/belgica-2g/jam/az/examples/mri/t1.ascii"
		 "/belgica-2g/jam/az/examples/mri/t2.ascii")
	   3 256 256)))
(standardize-bucket-planes *mri-muxel-bucket*)

(defparameter *small-mri-bucket*
    (time 
     (muxel-bucket-subset
      *mri-bucket*
      #'(lambda (pixel) (muxel-over-threshold pixel 16)))))

(length (muxel-bucket-pixels *small-mri-bucket*))

(defparameter *mri-scatter-bucket-01*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *small-mri-bucket*
      (slate:make-slate :width 256 :height 256)
      :x-channel 0 :y-channel 1 :glyph-size 2)))

(defparameter *mri-scatter-bucket-12*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *small-mri-bucket*
      (slate:make-slate :width 256
			:height 256)
      :x-channel 1
      :y-channel 2
      :glyph-size 2)))

(defparameter *mri-scatter-bucket-20*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *small-mri-bucket*
      (slate:make-slate :width 256
			:height 256)
      :x-channel 2
      :y-channel 0
      :glyph-size 2)))

(defparameter *mri-presentation-0*
    (time
     (make-presentation-for-muxel-bucket
      *small-mri-bucket*
      (slate:make-slate :width 256 :height 256)
      :channel 0)))

(defparameter *mri-presentation-1*
    (time
     (make-presentation-for-muxel-bucket
      *small-mri-bucket*
      (slate:make-slate :width 256 :height 256)
      :channel 1)))

(defparameter *mri-presentation-2*
    (time
     (make-presentation-for-muxel-bucket
      *small-mri-bucket*
      (slate:make-slate :width 256 :height 256)
      :channel 2)))

(defparameter *small-scatter-bucket*
    (time
     (make-scatter-presentation-for-muxel-bucket
      *small-pet-bucket*
      (slate:make-slate :width 256
			:height 256)
      :x-channel 8
      :y-channel 12)))

(let ((lag 16))
  (dotimes (i 8)
    (loop for x from 0 to (- 38 lag)
     for y = (+ x lag)
     do (setf (channels *small-scatter-bucket*) (list x y)))
    (loop for x downfrom (- 38 lag) to 0
     for y = (+ x lag)
     do (setf (channels *small-scatter-bucket*) (list x y)))))



(time
 (let ((lag 8)
       (i 0))
   (loop for x from 4 to (- 38 lag)
    for y = (+ x lag)
    do
     (incf i)
     (setf (channels *scatter-bucket*) (list x y)))
   (loop for x downfrom (- 38 lag) to 5
    for y = (+ x lag)
    do
     (incf i)
     (setf (channels *scatter-bucket*) (list x y)))
   (print i)))

(let ((n (muxel-bucket-nchannels (subject *pet-presentation*))))
  (dotimes (k 10)
    (dotimes (i n)
      (setf (channel *pet-presentation*) i))
    (dotimes (i n)
      (setf (channel *pet-presentation*) (- n i 1)))))


(time
 (dotimes (i 10)
   (draw-display *small-spaghetti-bucket*)))

(setf (channel *small-pet-presentation*) 16)
(defparameter *pet-presentation*
    (time
     (make-presentation-for-muxel-bucket *pet-bucket* *image-slate*
					 :channel 8)))

(defparameter *spaghetti-bucket*
    (time
     (make-spaghetti-presentation-for-muxel-bucket
      *pet-bucket* *spaghetti-slate*)))

(time
 (dotimes (i 10)
   (draw-display *spaghetti-bucket*)))

(draw-display *small-pet-presentation*)
(slate:finish-drawing *image-slate*)




(length (muxel-bucket-pixels *small-pet-bucket*))
(time
 (magnified-muxel-bucket-movie *image-slate* *pet-bucket* 8))

(time
 (magnified-muxel-bucket-movie *image-slate* *small-pet-bucket* 8))


(time
 (let* ((pen (red-fill-pen)))
   (dotimes (i 39)
     (draw-magnified-muxel-bucket pen *image-slate* *pet-bucket* i 4)
     (slate:finish-drawing *image-slate*))
   (dotimes (i 39)
     (draw-magnified-muxel-bucket pen *image-slate* *pet-bucket* (- 38 i) 4)
     (slate:finish-drawing *image-slate*))))

(make-presentation-for-image
 (sixth (muchi-channel-images *thym-after-image*))
 *image-slate*)

(let* ((drawable (slate::slate-drawable *image-slate*))
       (image (sixth (muchi-channel-images *thym-after-image*))))
  (xlib:get-image
   drawable
   :data (xlib:image-z-pixarray image)
   :x 0 :y 0
   :width 72;;(xlib:drawable-width drawable)
   :height 72;;(xlib:drawable-height drawable)
   :format :z-pixmap
   :result-type 'xlib:Image-Z))

(time 
 (pack-muchi *thym-after-path* *thym-packed-path* 39 72 72))


(time
 (dotimes (i 8)
   (let* ((pen (default-pen))
	  (to-pnt (g:make-screen-point))
	  (from-rect (slate:image-rect
		      (first (muchi-channel-images *thym-after-image*))))
	  (channel-images (muchi-channel-images *thym-after-image*))
	  (reversed-images (reverse channel-images)))
     (dolist (channel-image channel-images)
       (slate:draw-image pen *image-slate* channel-image from-rect to-pnt))
     (dolist (channel-image reversed-images)
       (slate:draw-image pen *image-slate* channel-image from-rect to-pnt)))))

(time
 (let* ((pen (red-fill-pen))
	(channel-images (muchi-channel-images *thym-after-image*))
	(reversed-images (reverse channel-images)))
   (dolist (channel-image channel-images)
     (slate:draw-magnified-image pen *image-slate* channel-image 8)
     (slate:force-drawing *image-slate*))
   (dolist (channel-image reversed-images)
     (slate:draw-magnified-image pen *image-slate* channel-image 8)
     (slate:force-drawing *image-slate*))
   (slate:finish-drawing *image-slate*)))



(test-image-painting 32)
(test-clx-image-painting 32)

(time
 (draw-muchi-curves
  (default-pen)
  *spaghetti-slate* *thym-after-image*))



(setf pixel (muxel *thym-after-image* 0 1))
(setf byte-vector (muxel-bytes pixel))
(setf coord-list (slate::series-coordinates 0 1 byte-vector))
(setf rc (rest coord-list))
(length coord-list)
