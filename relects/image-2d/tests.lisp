;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))
  
;;;============================================================

(defparameter *thym-after-path* "~jam/az/data/pet/thym.after")

(defparameter *thym-packed-path* "~jam/az/data/pet/thym.after.24")
(defparameter *thym-times-path* "~jam/az/data/pet/thym.times")

(defparameter *screen* (slate:default-screen))

(defparameter *standard-colormap* (slate:screen-colormap *screen*))

(defparameter *gray-colormap*
    (slate:make-colormap-for
     *screen*
     :default-initialization :gray-scale))

(defparameter *colornames-colormap*
    (slate:make-colormap-for
     *screen*
     :default-initialization :color-names))

(defparameter *image-painting-colormap*
    (slate:make-colormap-for
     *screen*
     :default-initialization :image-painting))

(defparameter *egami-colormap*
    (slate:make-colormap-for
     *screen*
     :default-initialization :egami))

(setf (slate:screen-colormap *screen*) *image-painting-colormap*)


