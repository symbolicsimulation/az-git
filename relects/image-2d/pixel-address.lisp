;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

(export '(Pixel-Address))

;;;============================================================
;;; Pixel Address
;;;============================================================
;;; This packs the x,y coordinates of an image pixel
;;; into a single 24 bit integer.

(deftype Image-Coordinate () '(Unsigned-Byte 12))

(deftype Pixel-Address () '(Unsigned-Byte 24))


(defmacro %pixel-address (x y) `(logior (ash ,x 12) ,y))

(defun pixel-address (x y)
  (tools:type-check Image-Coordinate x y)
  (%pixel-address x y))

#||
(proclaim '(Inline %pixel-address-x))
(proclaim '(Function %pixel-address-x (Pixel-Address) Image-Coordinate))

(defun %pixel-address-x (pixel-address)
  (declare (optimize (safety 1)
		     (speed 3)
		     (space 0)
		     (compilation-speed 0))
	   (type Pixel-Address pixel-address))
  (ash pixel-address -12))

(proclaim '(Inline  %pixel-address-y))
(proclaim '(Function %pixel-address-y (Pixel-Address) Image-Coordinate))

(defun %pixel-address-y (pixel-address)
  (declare (optimize (safety 1)
		     (speed 3)
		     (space 0)
		     (compilation-speed 0))
	   (type Pixel-Address pixel-address))
  (logand #b111111111111 pixel-address))
||#

;;; inline declarations don't seem to work
(defmacro %pixel-address-x (pixel-address)
   `(ash ,pixel-address -12) )
(defmacro %pixel-address-y (pixel-address)
   `(logand #b111111111111 ,pixel-address))

(proclaim '(Inline  %pixel-address-xy))
(proclaim '(Function %pixel-address-xy (Pixel-Address)
	    Image-Coordinate Image-Coordinate))

(defun %pixel-address-xy (pixel-address)
  (declare (optimize (safety 1)
		     (speed 3)
		     (space 0)
		     (compilation-speed 0))
	   (type Pixel-Address pixel-address))
  (values
   (ash pixel-address -12)
   (logand #b111111111111 pixel-address)))

(defun pixel-address-x (pixel-address)
   (tools:type-check Pixel-Address pixel-address)
   (%pixel-address-x pixel-address))

(defun pixel-address-y (pixel-address)
   (tools:type-check Pixel-Address pixel-address)
   (%pixel-address-y pixel-address))

(defun pixel-address-xy (pixel-address)
   (tools:type-check Pixel-Address pixel-address)
   (%pixel-address-xy pixel-address))

