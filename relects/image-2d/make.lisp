;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; Patch-File: T;-*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

(proclaim '(optimize (compilation-speed 0)
		     (safety 3)
		     (space 0)
		     (speed 1)))

;;;============================================================

(load #+:coral "ccl;az:tools:compile-if-needed"
      #+:excl "/belgica-0g/local/az/tools/compile-if-needed"
      #+symbolics "/belgica-0g/local/az/tools/compile-if-needed")

(load #+:coral "ccl;az:files"
      #+:excl "/belgica-0g/local/az/files"
      #+symbolics "belgica:/belgica-0g/local/az/files")

(load #+:coral "ccl;jam:az:files"
      #+:excl "/belgica-2g/jam/az/files"
      #+symbolics "belgica:/belgica-2g/jam/az/files")

;;;============================================================

(load-all *tools-directory* *tools-files*)

(tools:with-gc-tuned-for-large-allocation
 
 (load-all *geometry-directory* *screen-geometry-files*) 

 (defvar *load-pcl?*)
 (setf *load-pcl?* (not (find-package "PCL")))

 (when *load-pcl?*
   (compile-if-needed *pcl-defsys-file*))

 (when *load-pcl?*
   (let (#+:coral (*warn-if-redefine-kernel* nil))
     #+:coral (declare (special *warn-if-redefine-kernel*))
     (pcl::load-pcl))) 

 ;; make sure that we have all tools,
 ;; including abstract-proplist-object hack:
 #+:excl (shadow '(wt::object) (find-package :wt))
 (load-all *clos-tools-directory* *clos-tools-files*) 

 (compile-all-if-needed *slate-directory* *slate-files*)
 (compile-all-if-needed *tap-directory* *tap-files*)

 (compile-all-if-needed *clay-directory* *clay-files*)

 (compile-all-if-needed *image-2d-directory* *image-2d-files*)

 )
