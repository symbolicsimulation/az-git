;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defclass Spaghetti-Bucket-Presentation (Presentation Display-Leaf)
	  ((dx
	    :type g:Positive-Screen-Coordinate
	    :accessor dx)
	   (subject
	    :type Muxel-Bucket
	    :accessor subject)))

;;;============================================================

(defun make-spaghetti-presentation-for-muxel-bucket (muxel-bucket slate)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0))
	   (type Spaghetti-Bucket-Presentation display)
	   (type List build-options))
  (let ((display (allocate-instance
		  (find-class 'Spaghetti-Bucket-Presentation))))
    (initialize-display display
			:build-options (list :subject muxel-bucket
					     :slate slate))
    display))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Spaghetti-Bucket-Presentation)
				build-options)
  (declare (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Spaghetti-Bucket-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject))
	 (rect (slate:slate-rect slate)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp)
      (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    (setf (slate-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (temp-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; slots inherited from Presentation:
    (setf (slot-value display 'subject) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject))
    (setf (display-name display)
      (getf build-options :display-name
	    (format nil "~s presentation" subject)))

    ;; input interactors
    (setf (current-interactor display) (build-interactors display))
    ))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((display Spaghetti-Bucket-Presentation)
				 layout-options)
  (declare (ignore layout-options)
	   (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Spaghetti-Bucket-Presentation display))

  (let* ((muxel-bucket (subject display))
	 (nchannels (muxel-bucket-nchannels muxel-bucket))
	 (slate (display-slate display))
	 (dx (truncate (slate:slate-width slate) nchannels))
	 (display-rect (slate-rect display)))
    (declare (type Muxel-Bucket muxel-bucket)
	     (type slate:Slate slate)
	     (type g:Positive-Screen-Coordinate
		   muxel-bucket-width muxel-bucket-height)
	     (type slate:Magnification-Level magnification)
	     (type g:Screen-Rect display-rect))
    (setf (dx display) dx)
    (setf (g:screen-rect-origin display-rect) (slate:slate-origin slate))
    (setf (g:screen-rect-width display-rect) (* dx nchannels))
    (setf (g:screen-rect-height display-rect) 256)
    display))

;;;------------------------------------------------------------

(defmethod apply-lens (lens (display Spaghetti-Bucket-Presentation))
  (declare (ignore lens))
  (setf (slot-value display 'display-pen) (subject-pen (subject display))))

;;;============================================================
;;; Drawing
;;;============================================================
;;; this function will only work with CLX

(defparameter *noodle-coordinate-list* '(:coordinates))

(defun set-noodle-xs (xs)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (type (Simple-Array Fixnum (*)) xs))
  (let* ((i 0)
	 (n (length xs))
	 (coordinate-list *noodle-coordinate-list*))
    (declare (type tools:Array-Index i n)
	     (type Cons coordinate-list *noodle-coordinate-list*)
	     (special *noodle-coordinate-list*))
    ;; first fill in the available elements of the list
    (loop
      ;; assuming we always have an even number
      ;; of spaces in the cdr of the list
      (when (null (the Cons (cdr coordinate-list))) (return))
      (setf (car (the Cons (cdr coordinate-list))) (aref xs i))
      (setf coordinate-list (the Cons (cddr coordinate-list)))
      (incf i)
      (when (>= i n)
	(rplacd (the Cons coordinate-list) nil)
	(return)))
    ;; now cons the cdr of the xs onto the end
    (loop
      (when (>= i n) (return))
      (rplacd (the Cons coordinate-list) (list (aref xs i) 0))
      (incf i)
      (setf coordinate-list
	(the Cons (cdr (the Cons (cdr coordinate-list)))))))
  *noodle-coordinate-list*)

(defun set-noodle-ys (ys)
  ;; this assumes set-noodle-xs has been called first,
  ;; so the list is guaranteed to be long enough
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) ys))
  (let ((coordinate-list (cdr *noodle-coordinate-list*)))
    (declare (type Cons coordinate-list *noodle-coordinate-list*)
	     (special *noodle-coordinate-list*))
    ;; first fill in the available elements of the list
    (dotimes (i (length ys))
      (declare (type Fixnum i))
      ;; invert y, to get the right sense on the screen---lognot
      (setf (car (the Cons (cdr coordinate-list))) (aref ys i))
      (setf coordinate-list (the Cons (cddr coordinate-list)))))
  *noodle-coordinate-list*)

(defun draw-spaghetti-bucket (pen slate bucket)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type slate:Pen pen)
	   (ignore pen)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket))
  (let ((drawable (slate::slate-drawable slate))
	(nrows (muxel-bucket-nrows bucket))
	(ncols (muxel-bucket-ncols bucket))
	(pixels (muxel-bucket-pixels bucket)))
    (declare (type xlib:Drawable drawable))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (set-noodle-xs (muxel-bucket-channel-coordinates bucket))
     (dotimes (i nrows)
       (dotimes (j ncols)
	 (let ((pixel (aref pixels i j)))
	   (declare (type Muxel pixel))
	   (xlib:draw-lines
	    drawable
	    (the xlib:GContext
	      (aref (the (Simple-Array xlib:GContext (256))
		      slate::*pixval-gcontexts*)
		    (%muxel-value pixel)))
	    (rest (set-noodle-ys (muxel-bytes pixel))))))))))

(defun update-spaghetti-bucket (timestamp slate bucket)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (inline tools::%timestamp<)
	   (type tools:Timestamp timestamp)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket))
  (let ((drawable (slate::slate-drawable slate))
	(nrows (muxel-bucket-nrows bucket))
	(ncols (muxel-bucket-ncols bucket))
	(pixels (muxel-bucket-pixels bucket)))
    (declare (type xlib:Drawable drawable))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (set-noodle-xs (muxel-bucket-channel-coordinates bucket))
     (dotimes (i nrows)
       (dotimes (j ncols)
	 (let ((pixel (aref pixels i j)))
	   (declare (type Muxel pixel))
	   (when (tools::%timestamp< timestamp (muxel-timestamp pixel))
	     (xlib:draw-lines
	      drawable
	      (the xlib:GContext
		(aref (the (Simple-Array xlib:GContext (256))
			slate::*pixval-gcontexts*)
		      (%muxel-value pixel)))
	      (rest (set-noodle-ys (muxel-bytes pixel)))))))))))

#||
(defun draw-spaghetti-bucket (pen slate bucket)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type slate:Pen pen)
	   (ignore pen)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket))
  (let ((drawable (slate::slate-drawable slate)))
    (declare (type xlib:Drawable drawable))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (set-noodle-xs (muxel-bucket-channel-coordinates bucket))
     (dolist (pixel (muxel-bucket-pixels bucket))
       (declare (type Muxel pixel))
       (xlib:draw-lines
	drawable
	(the xlib:GContext
	  (aref (the (Simple-Array xlib:GContext (256))
		  slate::*pixval-gcontexts*)
		(muxel-value pixel)))
	(rest (set-noodle-ys (muxel-bytes pixel))))))))

(defun update-spaghetti-bucket (timestamp slate bucket)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   (type tools:Timestamp timestamp)
	   (type slate:CLX-Slate slate)
	   (type Muxel-Bucket bucket))
  (let ((drawable (slate::slate-drawable slate)))
    (declare (type xlib:Drawable drawable))
    (xlib:with-display
     ((slate::x-display (slate:slate-screen slate)))
     (set-noodle-xs (muxel-bucket-channel-coordinates bucket))
     (dolist (pixel (muxel-bucket-pixels bucket))
       (declare (type Muxel pixel))
       (when (tools::%timestamp< timestamp (muxel-timestamp pixel))
	 (xlib:draw-lines
	  drawable
	  (the xlib:GContext
	    (aref (the (Simple-Array xlib:GContext (256))
		    slate::*pixval-gcontexts*)
		  (muxel-value pixel)))
	  (rest (set-noodle-ys (muxel-bytes pixel)))))))))
||#
;;;------------------------------------------------------------

(defmethod draw-display ((display Spaghetti-Bucket-Presentation))
  (draw-spaghetti-bucket (display-pen display)
			 (display-slate display)
			 (subject display))
  (tools:stamp-time! (redraw-timestamp display)))

;;;------------------------------------------------------------

(defmethod erase-display ((display Spaghetti-Bucket-Presentation) rect)
  (slate:with-clipping-region ((display-slate display)
			       (clipping-region display))
    (setf (slate:pen-pixel-value (erasing-fill-pen))
       (slate:slate-background-pixel-value (display-slate display)))
    (slate::%draw-rect (erasing-fill-pen) (display-slate display)
		       (slate-rect display)))
  (unless (null rect)
      (g::%copy-screen-rect (slate-rect display) rect)))

;;;------------------------------------------------------------

(defmethod paint-stroke ((display Spaghetti-Bucket-Presentation)
			 (stroke Stroke) &rest options)
  (declare (ignore options))
  (update-spaghetti-bucket (redraw-timestamp display)
			   (display-slate display)
			   (subject display))
  (tools:atomic
   (tools:deletef display (stroke-presentations-to-be-updated stroke))
   (when (null (stroke-presentations-to-be-updated stroke))
     (return-stroke stroke)))
  (tools:stamp-time! (redraw-timestamp display)))

;;;============================================================
;;; Input
;;;============================================================
;;; haven't implemented painting yet

(defmethod paint-presentation ((display Spaghetti-Bucket-Presentation)
			       pen rect)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (type Spaghetti-Bucket-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect rect)
	   (ignore pen rect))
  nil)

