;;;-*- Package: :Clay; Syntax: Common-Lisp; Modise: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defclass Muxel-Bucket-Presentation (Presentation Display-Leaf)
	  ((magnification
	    :type slate:Magnification-Level
	    :accessor magnification)
	   (channel
	    :type tools:Array-Index
	    :accessor channel)
	   (subject
	    :type Muxel-Bucket
	    :accessor subject)))

;;;============================================================

(defun make-presentation-for-muxel-bucket (muxel-bucket slate
					   &key (channel 0))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0))
	   (type Muxel-Bucket-Presentation display)
	   (type List build-options))
  (let ((display (allocate-instance (find-class 'Muxel-Bucket-Presentation))))
    (initialize-display display
			:build-options (list :subject muxel-bucket
					     :slate slate
					     :channel channel))
    display))

;;;============================================================

(defmethod slate-rect ((display Muxel-Bucket-Presentation))
  (slate:slate-rect (display-slate display)))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Muxel-Bucket-Presentation)
				build-options)
  (declare (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Muxel-Bucket-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject))
	 ;;(rect (slate:slate-rect slate))
	 )
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp) (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    #||
    (setf (slate-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (temp-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    ||#
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; input interactors
    (setf (current-interactor display) (build-interactors display))

    ;; local slots:
    (setf (slot-value display 'channel) (getf build-options :channel 0))

    ;; slots inherited from Presentation:
    (setf (slot-value display 'subject) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject))
    (setf (display-name display)
      (getf build-options :display-name
	    (format nil "Channel ~d of ~s" (channel display) subject)))
    ))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((display Muxel-Bucket-Presentation)
				 layout-options)
  (declare (ignore layout-options)
	   (optimize (safety 3)
		     (speed 0)
		     (space 0)
		     (compilation-speed 0))
	   (type Muxel-Bucket-Presentation display))

  (let* ((muxel-bucket (subject display))
	 (muxel-bucket-width (muxel-bucket-ncols muxel-bucket))
	 (muxel-bucket-height (muxel-bucket-nrows muxel-bucket))
	 (slate (display-slate display))
	 (magnification
	  (min (truncate (slate:slate-width slate) muxel-bucket-width)
	       (truncate (slate:slate-height slate) muxel-bucket-height)))
	 (display-rect (slate:slate-rect slate)))
    (declare (type Muxel-Bucket muxel-bucket)
	     (type slate:Slate slate)
	     (type g:Positive-Screen-Coordinate
		   muxel-bucket-width muxel-bucket-height)
	     (type slate:Magnification-Level magnification)
	     (type g:Screen-Rect display-rect))
    (setf (magnification display) magnification)
    (setf (g:screen-rect-origin display-rect)
      (slate:slate-origin slate))
    (setf (g:screen-rect-width display-rect)
      (* magnification muxel-bucket-width))
    (setf (g:screen-rect-height display-rect)
      (* magnification muxel-bucket-height))
    display))

;;;------------------------------------------------------------

(defmethod apply-lens (lens (display Muxel-Bucket-Presentation))
  (declare (ignore lens))
  (setf (slot-value display 'display-pen) (subject-pen (subject display))))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod draw-display ((display Muxel-Bucket-Presentation))
  (draw-magnified-muxel-bucket
   (display-pen display) (display-slate display) (subject display)
   (channel display)
   (magnification display))
  (tools:stamp-time! (redraw-timestamp display)))

;;;------------------------------------------------------------

(defmethod erase-display ((display Muxel-Bucket-Presentation) rect)
  (let ((slate (display-slate display)))
    (slate:with-clipping-region (slate (clipping-region display))
     (setf (slate:pen-pixel-value (erasing-fill-pen))
       (slate:slate-background-pixel-value slate))
     (slate::%draw-rect (erasing-fill-pen) slate (slate-rect display)))
    (unless (null rect)
      (g::%copy-screen-rect (slate-rect display) rect))))

;;;------------------------------------------------------------

(defmethod paint-stroke ((display Muxel-Bucket-Presentation) stroke
			 &rest options)
  (declare (ignore options))
  (update-magnified-muxel-bucket
   (display-slate display) (subject display)
   (redraw-timestamp display)
   (channel display)
   (magnification display))
  (tools:atomic
   (tools:deletef display (stroke-presentations-to-be-updated stroke))
   (when (null (stroke-presentations-to-be-updated stroke))
     (return-stroke stroke)))
  (tools:stamp-time! (redraw-timestamp display)))

;;;============================================================
;;; Activation
;;;============================================================

(defmethod activate-display ((display Muxel-Bucket-Presentation)
			     activation-options)
  (declare (ignore activation-options))
  ;;(activate-display-local display activation-options)
  ;;(activate-children display)
  (activate-presentation-constraints display)
  (activate-input display))

;;;------------------------------------------------------------

(defmethod (setf channel) (new-channel (display Muxel-Bucket-Presentation))
  (deactivate-display display)
  (setf (slot-value display 'channel) new-channel)
  (draw-display display)
  (activate-display display nil))


(defparameter *overlay-rect* (g:make-screen-rect))
(proclaim '(type g:Screen-Rect *overlay-rect*))

(defmethod paint-presentation ((display Muxel-Bucket-Presentation) pen rect)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :types :calls)
	   (type Muxel-Bucket-Presentation display)
	   (type slate:Pen pen)
	   (type g:Screen-Rect rect *overlay-rect*)
	   (special *overlay-rect*))
  (let* ((bucket (subject display))
	 (mag (magnification display))
	 (xmin (g::%screen-rect-xmin rect))
	 (ymin (g::%screen-rect-ymin rect))
	 (xmax (+ xmin (g::%screen-rect-width rect)))
	 (ymax (+ ymin (g::%screen-rect-height rect)))
	 (pixels (muxel-bucket-pixels bucket))
	 (nrows (muxel-bucket-nrows bucket))
	 (ncols (muxel-bucket-ncols bucket))
	 (pixel-value (slate::%pen-pixel-value pen)))
    (declare (type Muxel-Bucket bucket)
	     (type slate:Magnification-Level mag)
	     (type g:Screen-Coordinate xmin ymin xmax ymax nrows ncols)
	     (type (Unsigned-Byte 8) pixel-value))

    ;; compute clipped region in image coordinates
    (setf xmin (max 0 (min ncols (floor xmin mag))))
    (setf ymin (max 0 (min nrows (floor ymin mag))))
    (setf xmax (max 0 (min ncols (ceiling xmax mag))))
    (setf ymax (max 0 (min nrows (ceiling ymax mag))))

    ;; mark pixels in bucket
    (loop for i from ymin below ymax do
      (loop for j from xmin below xmax do
	(let* ((pixel (aref pixels i j))
	       (muxel-value (%muxel-value pixel)))
	  (declare (type Muxel pixel)
		   (type (Unsigned-Byte 8) muxel-value))
	  (when (/= muxel-value pixel-value)
	    (setf (muxel-value pixel) pixel-value)))))
    
    ;; convert back to screen coordinates and do a quick redraw
    (tools:mulf xmin mag)
    (tools:mulf ymin mag)
    (g::%set-screen-rect-coords
     *overlay-rect* xmin ymin (- (* mag xmax) xmin) (- (* mag ymax) ymin))
    (slate::%draw-rect pen (display-slate display) *overlay-rect*)))



