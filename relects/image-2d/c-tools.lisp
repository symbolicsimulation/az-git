;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: (:Npsol :use (:Lisp)) -*-
;;;
;;; Copyright 1991. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Clay)

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;=======================================================

#-:excl (error "This file can only be used with Franz Allegro CL.")

(require :Foreign)

;;;=======================================================

(ff:reset-entry-point-table)

(load "~jam/az/image/c-tools.o"
      :system-libraries '("m" "c"))

(ff:defforeign 'read_byte_array
	       :entry-point (ff:convert-to-lang "read_byte_array" :language :C)
	       :language :C
	       :arguments
	       '((Simple-String) ;; pathname
		 Fixnum ;; nskip
		 Fixnum ;; nbytes
		 (Simple-Array (Unsigned-Byte 8) (* *)) ;; byte array
		 )
	       :print t
	       :arg-checking t
	       :return-type :void)

