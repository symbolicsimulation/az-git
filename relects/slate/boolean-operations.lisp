;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Logical Operations
;;;============================================================
;;;
;;; The logical operation determines how setting (and unsetting)
;;; pixel (by a drawing operation) combines with the existing
;;; state of the pixel. For now, the set of logical operations
;;; is the set of binary truth tables, specified by the 16
;;; "boole-" constants in Common Lisp. (CLtL 12:222).
;;;
;;; The whole notion of "logical operation" is somewhat
;;; problematic for color and Postscript devices, so this
;;; may need to be changed in the future.
;;; 
;;; Note that it doesn't make sense to define 
;;; {\sf copy-boolean-operation} and {\sf make-boolean-operation}.


(defparameter *boolean-operations* '(boole-clr
				      boole-set
				      boole-1
				      boole-2
				      boole-c1
				      boole-c2
				      boole-and
				      boole-ior
				      boole-xor
				      boole-eqv
				      boole-nand
				      boole-nor
				      boole-andc1
				      boole-andc2
				      boole-orc1
				      boole-orc2))
(defun boolean-operation? (op)
  #+symbolics (and (typep op 'Fixnum) (<= 0 op 15))
  #-symbolics (member op *boolean-operations* :test #'eql))

(deftype Boolean-Operation () '(satisfies boolean-operation?))

(defun equal-boolean-operations? (op0 op1)
  (az:declare-check (type slt:Boolean-Operation op0 op1))
  (eql op0 op1))

;;; A function that's sometimes useful:  One bw slates, many drawing
;;; operations are defined without any reference to color, implicitly
;;; assuming that you are drawing with 1 pixel value. However, they
;;; often take an alu or boolean operation argument, that determines how
;;; the implicit 1 is combined with what's already on the screen.
;;; Inverting the boolean operation gives you the effect of drawing with
;;; zero pixel values.

(defun invert-operation (op)
  (ecase op
    (#.boole-ior #.boole-orc1)
    (#.boole-orc1 #.boole-ior)
    (#.boole-xor #.boole-eqv)
    (#.boole-eqv #.boole-xor)
    (#.boole-1 #.boole-c1)
    (#.boole-c1 #.boole-1)
    (#.boole-and #.boole-andc1)
    (#.boole-andc1 #.boole-and)
    (#.boole-orc2 #.boole-nand)
    (#.boole-nand #.boole-orc2)
    (#.boole-nor #.boole-andc2)
    (#.boole-andc2 #.boole-nor)
    ((#.boole-set #.boole-clr #.boole-2 #.boole-c2) op)))