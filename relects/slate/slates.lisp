;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; no presumption is made here about whether {\sf slate-rect}
;;; or {\sf slate-left}, {\sf slate-top}, {\sf slate-width},
;;; {\sf slate-height} are more primitive. Nothing good will
;;; happen if you modify the rect returned by {\sf slate-rect}.

(defgeneric slate-rect (slate))

(defgeneric slate-left (slate))
(defgeneric slate-top (slate))
(defgeneric slate-width (slate))
(defgeneric slate-height (slate))

(defgeneric slate-right (slate))
(defgeneric slate-bottom (slate))

(defgeneric (setf slate-name) (name slate)
            (declare (type name (or String Symbol))))

(defgeneric slate-background-pixel-value (slate))

(defgeneric (setf slate-background-pixel-value) (pixel-value slate)
	    (declare (type Pixel-Value pixel-value)))

;;;============================================================
;;; Slates
;;;============================================================

(defclass Slate (Slate-Object)
	  ((slate-screen
	    :type Screen
	    :reader slate-screen
	    :initarg :slate-screen
	    :initarg :screen)
	   (slate-point
	    :type g:Screen-Point
	    ;; note that there is no ":accessor slate-point",
	    ;; because we define it below with :result arg
	    :initform (g:make-screen-point))
	   (slate-rect
	    :type g:Screen-Rect
	    ;; note that there is no ":accessor slate-rect",
	    ;; this slot is used to hold a rect that
	    ;; can be reused, rather than consing up a new
	    ;; screen-rect every time slate-rect is called
	    :initform (g::%make-screen-rect 0 0 0 0))
	   ;; Note (see below) that setf of slate-clipping-region has side
	   ;; effects (update-device-clipping-region) that setf of
	   ;; slate-clipping-rect does not.
	   (slate-clipping-rect
	    :type (or Null g:Screen-Rect)
	    :accessor slate-clipping-rect
	    :initarg :slate-clipping-rect)
	   (slate-name
	    :type (or Symbol String)
	    :reader slate-name
	    :initarg :slate-name)
	   (slate-origin
	    :type g:Screen-Point
	    :reader slate-origin
	    :initform (g:make-screen-point))
	   (slate-color-metric
	    :type (or Symbol Function)
	    :reader slate-color-metric
	    :initarg :slate-color-metric)
	   (slate-background-color
	    :type Color
	    :reader slate-background-color
	    :initarg :slate-background-color)
	   (slate-background-pixel-value
	    :type Pixel-Value
	    :reader slate-background-pixel-value
	    :initarg :slate-background-pixel-value))
  (:default-initargs
      :slate-background-color (colorname->color :white)
    :slate-clipping-rect nil
    :slate-name (gentemp "SLATE-")))

;;;============================================================
;;; Generalized accessors 
;;;============================================================

(defmethod (setf slate-name) (name slate)
  (az:declare-check (type (or String Symbol) name)
		    (type Slate slate))
  (setf (slot-value slate 'slate-name) name))

;;;------------------------------------------------------------

(defmethod (setf slate-background-pixel-value) (pixel-value (slate Slate))
  (warn "Can't set background for ~s to the pixel value:~s" 
	slate pixel-value))

;;;------------------------------------------------------------

(defmethod (setf slate-background-color) (background (slate Slate))
  (let* ((colormap (screen-colormap (slate-screen slate)))
	 (metric (make-color-metric colormap :background-color background))
	 (pixel-value (color->pixel-value colormap background :metric metric)))
    (setf (slot-value slate 'slate-color-metric) metric)
    (setf (slot-value slate 'slate-background-color) background)
    (setf (slate-background-pixel-value slate) pixel-value)))

;;;============================================================
;;; make a slate on a given screen
;;;============================================================

(defgeneric make-slate-for-screen (screen
				   &rest options
				   &key
				   width
				   height
				   &allow-other-keys)
	    (declare (type Screen screen)
		     (type g:Positive-Screen-Coordinate width height)))

(defgeneric make-invisible-slate-for-screen (screen
					     &rest options
					     &key
					     width
					     height
					     &allow-other-keys)
	    (declare (type Screen screen)
		     (type g:Positive-Screen-Coordinate width height)))

(defun make-slate (&rest options
                   &key (screen (default-screen))
                   &allow-other-keys)
  (az:declare-check (type Screen screen))
  (apply #'make-slate-for-screen screen options))

(defun make-invisible-slate (&rest options
                             &key (screen (default-screen))
                             &allow-other-keys)
  (az:declare-check (type Screen screen))
  (apply #'make-invisible-slate-for-screen screen options))

;;;============================================================
;;; (attempt to) destroy a slate.
;;; An experimental implementation of killing based on using
;;; {\sf change-class} to convert the object to a "dead" object.
;;; This might be the basis for a more general protocol
;;; for all sorts of objects.

(defclass Dead-Slate (Slate az:Dead-Object) ())

#||
(defmethod az:kill-object ((slate Slate) &rest options)
  (declare (ignore options))
  (az:declare-check (type Slate slate))
  (unless (typep slate 'Dead-Slate) (change-class slate 'Dead-Slate)))
||#

;;;============================================================
;;; standard invisible slates (aka bitmaps, pixmaps in X)
;;;============================================================
;;;
;;; Every class of (visible) slate is required to have a (at least one)
;;; corresponding invisible slate class. Instances of an invisible
;;; slate class accept all the same output operations as visible
;;; slates. (The reason for "invisible-slate" rather than "bitmap" is
;;; to encourage extension to Postscript devices.)
;;; 
;;; There will be methods for (copy-slate-rect invisible-slate
;;; invisible-rect visible-slate visible-rect) for every legal
;;; invisible/visible slate class pair. {\sf copy-slate-rect} will copy
;;; the contents of {\sf invisible-rect} on {\sf invisible-slate} to
;;; {\sf visible-slate}, copying the origin of {\sf invisible-rect} to the
;;; origin of {\sf visible-rect}, clipping at the boundaries of
;;; {\sf visible-rect}, and leaving the extra "pixels" of {\sf visible-rect}
;;; untouched, if {\sf invisible-rect} is smaller than {\sf visible-rect}.
;;; 
;;; (On some devices, there may be more than one kind of invisible
;;; slate that can be used with a given visible slt:  for example,
;;; it may be possible to copy bw bitmaps to color slates and vice
;;; versa.)
;;; 

(defclass Invisible-Slate (Slate) ())

;;; These default methods assume the origin of all slates is (0,0)

(defmethod slate-left (slate)
  (az:declare-check (type Slate slate))
  0)

(defmethod slate-top (slate)
  (az:declare-check (type Slate slate))
  0)

(defmethod slate-right (slate)
  (az:declare-check (type Slate slate))
  (slate-width slate))

(defmethod slate-bottom (slate)
  (az:declare-check (type Slate slate))
  (slate-height slate))

;;;============================================================ 
;;; clipping-region
;;;============================================================
;;;
;;; Note that setf of slate-clipping-region has side effects
;;; (update-device-clipping-region) that setf of slate-clipping-rect
;;; does not.

(defgeneric update-device-clipping-region (slate-rect)
  (declare (type Slate slate)))

(defgeneric %slate-clipping-region (slate result)
  (declare (type Slate slate)
           (type g:Screen-Rect result)))

(defmethod %slate-clipping-region (slate result)
  (let ((clipr (slate-clipping-rect slate)))
    (if (null clipr)
        (g::%copy-screen-rect (slate-rect slate) result)
        (g::%copy-screen-rect clipr result))))

(defgeneric (setf %slate-clipping-region) (new-rect slate)
  (declare (type g:Screen-Rect new-rect)
           (type Slate slate)))

(defmethod (setf %slate-clipping-region) (new-rect slate)
  (let ((clipr (slate-clipping-rect slate)))
    (unless (eql new-rect clipr)
      (setf (slate-clipping-rect slate)
            (if (null new-rect)
                nil
                (if (null clipr)
                    (g:copy-screen-rect new-rect)
                    (g:copy-screen-rect new-rect :result clipr))))
      (update-device-clipping-region slate))))
  
(defun slate-clipping-region (slate
                              &key
                              (result (g::%make-screen-rect 0 0 0 0)))
  (az:declare-check (type Slate slate)
		    (type g:Screen-Rect result))
  (%slate-clipping-region slate result))

(defun set-slate-clipping-region (slate new-rect)
  (az:declare-check (type Slate slate)
		    (type (or Null g:Screen-Rect) new-rect))
  (setf (%slate-clipping-region slate) new-rect))

(defsetf slate-clipping-region set-slate-clipping-region)

;;;============================================================
;;; temporarily change the clipping region of a slate
;;; Note that setf of slate-clipping-region has side effects
;;; that setf of slate-clipping-rect does not.

(defmacro with-clipping-region ((slate clipr) &body body)
  (let ((old-clipr (gensym "old-clipr-")))
    (gensym "G")
    (az:once-only (slate clipr)
      `(let ((,old-clipr (slate-clipping-rect ,slate)))
         (unwind-protect         
             (progn
               (unless (eql ,old-clipr ,clipr)
                 (setf (slate-clipping-region ,slate) ,clipr))
               ,@body)
           (unless (null ,clipr)
             (setf (slate-clipping-region ,slate) ,old-clipr)))))))



