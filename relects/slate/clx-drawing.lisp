;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; GContext stuff
;;;============================================================
;;; maintaining consistency between Pens and Gcontexts:

(defun update-gcontext-from-pen (pen screen gc)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Screen screen)
	   (type xlib:Gcontext gc))
  (let* ((gc-plist (xlib:gcontext-plist gc))
	 (gc-time (getf gc-plist 'pen-time)))
    (declare (type List gc-plist)
	     (type az:Timestamp gc-time))
    (unless (and (eq pen (getf gc-plist 'pen))
		 (az::%timestamp<
		  (the az:Timestamp (%pen-changed-time pen))
		  gc-time)
		 (or (%pen-pixel-value pen)
		     (az::%timestamp<
		      (the az:Timestamp (color-time (%pen-color pen)))
		      gc-time)))
      (setf (getf gc-plist 'pen) pen)
      (let* ((xscreen (xlib:display-default-screen (xdisplay screen)))
	     (pixval (screen-pen-pixel-value screen pen))
	     (tile (fill-style-tile (the Fill-Style (%pen-fill-style pen))))
	     (ls (pen-line-style pen)))
	(declare (type xlib:Screen xscreen)
		 (type Pixel-Value pixval)
		 (type (or Null Tile) tile)
		 (type Line-Style ls))
	(setf (xlib:gcontext-foreground gc)
	  (if (typep screen 'RGB-CLX-Screen)
	      pixval
	    (if (zerop pixval) 
		(xlib:screen-white-pixel xscreen)
	      (xlib:screen-black-pixel xscreen))))
	(setf (xlib:gcontext-font gc) (%screen-font screen (%pen-font pen)))
	(setf (xlib:gcontext-plane-mask gc) (%pen-plane-mask pen))
	(setf (xlib:gcontext-function gc) (%pen-operation pen))
	(setf (xlib:gcontext-line-width gc) (line-style-thickness ls))
	(setf (xlib:gcontext-dash-offset gc) (line-style-dash-offset ls))
	(setf (xlib:gcontext-dashes gc) (line-style-dashes ls))
	(setf (xlib:gcontext-line-style gc) (line-style-dash-rule ls))
	(if (null tile)
	    (setf (xlib:gcontext-fill-style gc) :solid)
	  ;; else
	  (progn
	    (setf (xlib:gcontext-fill-style gc) :tiled)
	    (setf (xlib:gcontext-tile gc)
	      (slate-drawable (tile-screen-slate tile screen))))))
      (az::%stamp-time! gc-time))))

;;;============================================================

(defun screen-pen-gcontext (screen pen)
  (declare (optimize (safety 1) (speed 3))
	   (type Screen screen)
	   (type Pen pen))
  (let ((gc (getf (%pen-screen-gcontexts pen) screen nil)))
    (when (null gc)
      (let ((xscreen (xscreen screen)))
	(declare (type xlib:Screen xscreen))
	(setf gc (xlib:create-gcontext
		  :exposures :off
		  :drawable (xlib:screen-root xscreen)
		  :cache-p t))
	(locally (declare (type xlib:Gcontext gc))
	  (setf (getf (xlib:gcontext-plist gc) 'pen) ())
	  (setf (getf (xlib:gcontext-plist gc) 'pen-time)
	    (az:make-timestamp))
	  (setf (getf (%pen-screen-gcontexts pen) screen) gc))))
    gc))

;;;============================================================
;;; clipping regions

#||
(defmethod update-device-clipping-region ((slate CLX-Output-Slate))
  (let ((rect (slate-clipping-region slate)))
    (setf (xlib:gcontext-clip-mask (slate-gcontext slate))
      (list (g::%screen-rect-left rect)
	    (g::%screen-rect-top rect)
	    (g::%screen-rect-width rect)
	    (g::%screen-rect-height rect)))))
||#

;;; this is no longer appropriate, since the slates don't own
;;; the gcontexts, which are what need to be notified about the
;;; clipping region.

(defmethod update-device-clipping-region ((slate CLX-Output-Slate))
  t)

(defun update-gcontext-from-slate (slate gc)
  (declare (optimize (safety 1) (speed 3))
	   (type Slate slate)
	   (type xlib:Gcontext gc))
  (let ((rect (slate-clipping-rect slate))
	(clip-mask (xlib:gcontext-clip-mask gc)))
    (declare (type (or Null g:Screen-Rect) rect))
    (setf (xlib:gcontext-background gc) (slate-background-pixel-value slate))
     (if (null rect)
	;; then make sure clipping is to window borders
	(unless (eql clip-mask :none)
	  (setf (xlib:gcontext-clip-mask gc) :none))
      ;; else just set the clip mask
      ;; without checking to see if it's necessary
      (locally (declare (type g:Screen-Rect rect))
	(setf (xlib:gcontext-clip-mask gc)
	  (list (g::%screen-rect-left rect)
		(g::%screen-rect-top rect)
		(g::%screen-rect-width rect)
		(g::%screen-rect-height rect)))))))

;;;============================================================
;;; Flushing output
;;;============================================================

(defmethod %finish-drawing ((slate CLX-Output-Slate))
  (xlib:display-finish-output (xdisplay (slate-screen slate))))

(defmethod %force-drawing ((slate CLX-Output-Slate))
  (xlib:display-force-output (xdisplay (slate-screen slate))))

;;;============================================================
;;; drawing points
;;;============================================================

(defmethod %draw-point-xy (pen (slate CLX-Output-Slate) x y)
  (let* ((screen (slate-screen slate))
	 (gc (screen-pen-gcontext screen pen)))
    (update-gcontext-from-slate slate gc)
    (update-gcontext-from-pen pen screen gc)
    (xlib:draw-point (slate-drawable slate) gc x y)))

(defparameter *point-coordinate-vector*
    (make-array 64
		:element-type `g:Screen-Coordinate
		:initial-element 0))

(defun point-coordinate-vector (n)
  (unless (= (length *point-coordinate-vector*) n)
    (setf *point-coordinate-vector*
      (make-array n
		  :element-type 'g:Screen-Coordinate
		  :initial-element 0)))
  *point-coordinate-vector*)

(defmethod %draw-points (pen (slate CLX-Output-Slate) points)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type List points))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (drawable (slate-drawable slate))
	 (n (* 2 (length points)))
	 (pxys (the (Simple-Array g:Screen-Coordinate (*))
		 (point-coordinate-vector n)))
	 (i 0)) 
    (declare (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type g:Screen-Coordinate n)
	     (type (Simple-Array g:Screen-Coordinate (*)) pxys)
	     (type g:Screen-Coordinate i))
    (xlib:with-display (xdisplay)
      (update-gcontext-from-pen pen screen  gc)
      (dolist (p points)
	(declare (type g:Screen-Point p))
	(setf (aref pxys i) (the g:Screen-Coordinate (g::%screen-point-x p)))
	(incf i)
	(setf (aref pxys i) (the g:Screen-Coordinate (g::%screen-point-y p)))
	(incf i))
      (xlib:draw-points drawable gc pxys))))

;;;============================================================
;;; drawing lines
;;;============================================================

;;; Argh!  The X line drawing is totally uncontrollable.  I think that
;;; the following will work for horizontal and vertical lines; for any
;;; other lines, there are undoubtedly off-by-one errors. (Sannella)

(defun %draw-line-xy-CLX (drawable gcontext x0 y0 x1 y1)
  (declare (optimize (safety 1) (speed 3))
	   (type xlib:Drawable drawable)
	   (type xlib:GContext gcontext)
	   (type g:Screen-Coordinate x0 y0 x1 y1))
  ;; the following ugly hack tries to simulate mac-syle line drawing,
  ;; by adjusting the endpoints by one to include final pixels
  (when (> x0 x1) (rotatef x0 x1) (rotatef y0 y1))
  (cond ((= x0 x1)
	 (cond ((<= y0 y1) (incf y1))
	       (t (decf y1))))
	((< x0 x1)
	 (cond ((< y0 y1) (incf x1) (incf y1))
	       ((= y0 y1) (incf x1))
	       ((> y0 y1) (decf y1))))
	(t (error "shouldn't be possible to get here")))
  (xlib:draw-line drawable gcontext x0 y0 x1 y1))

(defmethod %draw-line-xy (pen (slate CLX-Output-Slate) x0 y0 x1 y1)
  (let* ((w (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen)))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (%draw-line-xy-CLX w gc x0 y0 x1 y1))))

(defparameter *segment-coordinate-vector*
    (make-array 64
		:element-type `g:Screen-Coordinate
		:initial-element 0))

(defun segment-coordinate-vector (n)
  (unless (= (length *segment-coordinate-vector*) n)
    (setf *segment-coordinate-vector*
      (make-array n
		  :element-type 'g:Screen-Coordinate
		  :initial-element 0)))
  *segment-coordinate-vector*)

(defmethod %draw-lines (pen (slate CLX-Output-Slate) points0 points1)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type List points0 points1))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (drawable (slate-drawable slate))
	 (gc (screen-pen-gcontext screen pen))
	 (nsegments (min (length points0) (length points1)))
	 (n (* 4 nsegments))
	 (pxys (the (Simple-Array g:Screen-Coordinate (*))
		 (segment-coordinate-vector n)))
	 (i 0))
    (declare (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type (Simple-Array g:Screen-Coordinate (*)) pxys)
	     (type g:Screen-Coordinate nsegments n i))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen  gc)
     (loop for p0 in points0
      for p1 in points1 do
       (setf (aref pxys i) (g::%screen-point-x p0)) (incf i)
       (setf (aref pxys i) (g::%screen-point-y p0)) (incf i)
       (setf (aref pxys i) (g::%screen-point-x p1)) (incf i)
       (setf (aref pxys i) (g::%screen-point-y p1)) (incf i))
     (xlib:draw-segments drawable gc pxys))))

(defmethod %draw-arrow-xy (pen (slate CLX-Output-Slate) x0 y0 x1 y1)

  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type g:Screen-Coordinate x0 y0 x1 y1)
	   (special *arrowhead-p0* *arrowhead-p1*))

  (unless (and (= x0 x1) (= y0 y1))
    (let* ((screen (slate-screen slate))
	   (xdisplay (xdisplay screen))
	   (drawable (slate-drawable slate))
	   (gc (screen-pen-gcontext screen pen))
	   (line-style (%pen-line-style pen))
	   (theta (atan (az:fl (- y1 y0))
			(az:fl (- x1 x0))))
	   (alpha (- pi (line-style-arrowhead-angle line-style)))
	   (beta0 (- theta alpha))
	   (beta1 (+ theta alpha))
	   (l (line-style-arrowhead-length line-style))
	   (dl (az:fl l))
	   (x2  (+ x1 (the g:Screen-Coordinate
			(truncate
			 (the Double-Float
			   (* dl (the Double-Float (cos beta0))))))))
	   (y2  (+ y1 (the g:Screen-Coordinate
			(truncate 
			 (the Double-Float
			   (* dl (the Double-Float (sin beta0))))))))
	   (x3  (+ x1 (the g:Screen-Coordinate
			(truncate 
			 (the Double-Float
			   (* dl (the Double-Float (cos beta1))))))))
	   (y3  (+ y1 (the g:Screen-Coordinate
			(truncate 
			 (the Double-Float
			   (* dl (the Double-Float (sin beta1)))))))))

      (declare (type Screen screen)
	       (type xlib:Display xdisplay)
	       (type xlib:Drawable drawable)
	       (type xlib:GContext gc)
	       (type Line-Style line-style)
	       (type g:Screen-Coordinate l x2 y2 x3 y3)
	       (type Double-Float dl theta alpha beta0 beta1))

      (xlib:with-display (xdisplay)
       (update-gcontext-from-slate slate gc)
       (update-gcontext-from-pen pen screen gc)
       (xlib:draw-segments drawable gc
			   (list x0 y0 x1 y1 x1 y1 x2 y2 x1 y1 x3 y3))))))


(declaim (type (Simple-Array Fixnum (2)) *p2* *p3*))
(defparameter *p2* (make-array 2 :element-type 'Fixnum))
(defparameter *p3* (make-array 2 :element-type 'Fixnum))

(defmethod %draw-arrow (pen (slate CLX-Output-Slate) p0 p1)

  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type g:Screen-Point p0 p1)
	   (special *p2* *p3*))

  (unless (g::%equal-screen-points? p0 p1)
    (let* ((screen (slate-screen slate))
	   (gc (screen-pen-gcontext screen pen))
	   (line-style (%pen-line-style pen))
	   (x0 (g::%screen-point-x p0))
	   (y0 (g::%screen-point-y p0))
	   (x1 (g::%screen-point-x p1))
	   (y1 (g::%screen-point-y p1))
	   (p2 *p2*)
	   (p3 *p3*))

      (declare (type Screen screen)
	       (type xlib:GContext gc)
	       (type Line-Style line-style)
	       (type g:Screen-Coordinate x0 y0 x1 y1)
	       (type (Simple-Array Fixnum (2)) p2 p3))

      (setf (aref p2 0) x0)
      (setf (aref p2 1) y0)
      (setf (aref p3 0) x1)
      (setf (aref p3 1) y1)
      (az:c_arrowhead_points p2 p3
			     (line-style-arrowhead-angle line-style)
			     (line-style-arrowhead-length line-style))
      (xlib:with-display ((xdisplay screen))
       (update-gcontext-from-slate slate gc)
       (update-gcontext-from-pen pen screen gc)
       #||
       (xlib:draw-segments (slate-drawable slate) gc
			   (list x1 y1 x0 y0
				 x1 y1 (aref p2 0) (aref p2 1)
				 x1 y1 (aref p3 0) (aref p3 1))
			   )
       ||#
       (xlib:draw-lines (slate-drawable slate) gc
			(list x0 y0
			      x1 y1
			      (aref p2 0) (aref p2 1)
			      (aref p3 0) (aref p3 1)
			      x1 y1)
			)))))

;;;============================================================
;;; Polylines

(defparameter *polyline-coordinate-vector*
    (make-array 64
		:element-type `g:Screen-Coordinate
		:initial-element 0))

(defun polyline-coordinate-vector (n)
  (unless (= (length *polyline-coordinate-vector*) n)
    (setf *polyline-coordinate-vector*
      (make-array n
		  :element-type 'g:Screen-Coordinate
		  :initial-element 0)))
  *polyline-coordinate-vector*)

(defmethod %draw-polyline (pen (slate CLX-Output-Slate) points)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type g:Screen-Point-List points))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (drawable (slate-drawable slate))
	 (n (* 2 (length points)))
	 (pxys (the (Simple-Array g:Screen-Coordinate (*))
		 (polyline-coordinate-vector n)))
	 (i 0)) 
    (declare (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type g:Screen-Coordinate n)
	     (type (Simple-Array g:Screen-Coordinate (*)) pxys)
	     (type g:Screen-Coordinate i))
    (xlib:with-display (xdisplay)
      (update-gcontext-from-slate slate gc)
      (update-gcontext-from-pen pen screen gc)
      (dolist (p points)
	(declare (type g:Screen-Point p))
	(setf (aref pxys i) (the g:Screen-Coordinate (g::%screen-point-x p)))
	(incf i)
	(setf (aref pxys i) (the g:Screen-Coordinate (g::%screen-point-y p)))
	(incf i))
      (xlib:draw-lines drawable gc pxys))))

;;;============================================================
;;; drawing screen-rects
;;;============================================================

(defun %draw-rect-xy-CLX (w gc filled? thickness left top width height)
  (declare (optimize (safety 1) (speed 3))
	   (type xlib:Drawable w)
	   (type xlib:GContext gc)
	   (type g:Screen-Coordinate left top)
	   (type g:Positive-Screen-Coordinate width height))
  (unless filled?
    (let ((delta (ash thickness -1)))
      (declare (type g:Positive-Screen-Coordinate delta))
      (setf width (max 0 (- width 1 delta)))
      (setf height (max 0 (- height 1 delta)))
      (setf left (+ left delta))
      (setf top (+ top delta))))
  (xlib:draw-rectangle w gc left top width height filled?))

(defmethod %draw-rect (pen (slate CLX-Output-Slate) rect)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type g:Screen-Rect rect))
  (let* ((left (g::%screen-rect-xmin rect))
	 (top (g::%screen-rect-ymin rect))
	 (width (g::%screen-rect-width rect))
	 (height (g::%screen-rect-height rect))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (w (slate-drawable slate))
	 (gc (screen-pen-gcontext screen pen))
	 (filled?
	  (not (eq :outline (fill-style-type (%pen-fill-style pen)))))
	 (thickness
	  (line-style-thickness (the Line-Style (%pen-line-style pen)))))
    (declare (type Screen screen)
	     (type xlib:Drawable w)
	     (type xlib:GContext gc)
	     (type g:Screen-Coordinate left top)
	     (type g:Positive-Screen-Coordinate width height thickness)
	     (type (Member t nil)  filled?))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (unless filled?
       (let ((delta (ash thickness -1)))
	 (declare (type g:Positive-Screen-Coordinate delta))
	 (setf width (max 0 (- width 1 delta)))
	 (setf height (max 0 (- height 1 delta)))
	 (setf left (+ left delta))
	 (setf top (+ top delta))))
     (xlib:draw-rectangle w gc left top width height filled?))))

(defmethod %draw-rect-xy (pen (slate CLX-Output-Slate) left top width height)
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (w (slate-drawable slate))
	 (gc (screen-pen-gcontext screen pen)))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (%draw-rect-xy-CLX
      w gc
      (not (eq :outline (fill-style-type (%pen-fill-style pen))))
      (line-style-thickness (the Line-Style (%pen-line-style pen)))
      left top width height))))

(defmethod %draw-rects-1 (pen (slate CLX-Output-Slate) rects)
  (let* ((w (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (filled?
	  (not (eq :outline (fill-style-type (%pen-fill-style pen)))))
	 (thickness
	  (line-style-thickness (the Line-Style (%pen-line-style pen)))))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (dolist (r rects)
       (%draw-rect-xy-CLX w gc filled? thickness
			  (g::%screen-rect-left r)
			  (g::%screen-rect-top r)
			  (g::%screen-rect-width r)
			  (g::%screen-rect-height r))))))

(defparameter *rect-coordinate-vector*
    (make-array 64
		:element-type `g:Screen-Coordinate
		:initial-element 0))

(defun rect-coordinate-vector (n)
  (unless (= (length *rect-coordinate-vector*) n)
    (setf *rect-coordinate-vector*
      (make-array n
		  :element-type 'g:Screen-Coordinate
		  :initial-element 0)))
  *rect-coordinate-vector*)

(defmethod %draw-rects (pen (slate CLX-Output-Slate) rects)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type g:Screen-Rect-List rects))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (drawable (slate-drawable slate))
	 (n (* 4 (length rects)))
	 (i 0)
	 (rltwh (the (Simple-Array g:Screen-Coordinate (*))
		  (rect-coordinate-vector n)))
	 (filled?
	  (not (eq :outline (fill-style-type (%pen-fill-style pen)))))
	 (thickness
	  (line-style-thickness (the Line-Style (%pen-line-style pen))))) 
    (declare (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type g:Screen-Coordinate n i)
	     (type (Simple-Array g:Screen-Coordinate (*)) rltwh))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (dolist (r rects)
       (declare (type g:Screen-Rect r))
       (let ((left (g::%screen-rect-left r))
	     (top (g::%screen-rect-top r))
	     (width (g::%screen-rect-width r))
	     (height (g::%screen-rect-height r)))
	 (declare (type g:Screen-Coordinate left top)
		  (type g:Positive-Screen-Coordinate width height))
	 (unless filled?
	   (let ((delta (ash thickness -1)))
	     (declare (type g:Positive-Screen-Coordinate delta))
	     (setf width (max 0 (- width 1 delta)))
	     (setf height (max 0 (- height 1 delta)))
	     (setf left (+ left delta))
	     (setf top (+ top delta))))
	 (setf (aref rltwh i) left) (incf i)
	 (setf (aref rltwh i) top) (incf i)
	 (setf (aref rltwh i) width) (incf i)
	 (setf (aref rltwh i) height) (incf i)))
     (xlib:draw-rectangles
      drawable gc rltwh
      (not (eq :outline (fill-style-type (%pen-fill-style pen))))))))

(defmethod clear-slate ((slate CLX-Output-Slate)
			&key (screen-rect nil))
  (if screen-rect
      (progn
	(setf (%pen-pixel-value (erasing-fill-pen))
	  (slate-background-pixel-value slate))
	(%draw-rect (erasing-fill-pen) slate screen-rect))
    (xlib:clear-area (slate-drawable slate))))

(defmethod clear-slate ((slate Invisible-CLX-Slate)
			&key (screen-rect (slate-rect slate)))
  (setf (%pen-pixel-value (erasing-fill-pen))
    (slate-background-pixel-value slate))
  (%draw-rect (erasing-fill-pen) slate screen-rect))

;;;============================================================
;;; drawing ellipses
;;;============================================================

(defun %draw-ellipse-xy-CLX (w gc filled? left top width height)
  (declare (optimize (safety 1) (speed 3))
	   (type xlib:Drawable w)
	   (type xlib:GContext gc)
	   (type g:Screen-Coordinate left top)
	   (type g:Positive-Screen-Coordinate width height))  
  (cond
   ;; Note: X is inconsistent between filled and non-filled cases.
   (filled? (setf width (max 0 (1+ width)))
	    (setf height (max 0 (1+ height)))
	    (decf left)
	    (decf top))
   (t (setf width (max 0 (1- width)))
      (setf height (max 0 (1- height)))))
  (xlib:draw-arc w gc left top width height 0 #.(* 2 pi) filled?)) 

(defmethod %draw-ellipse-xy (pen (slate CLX-Output-Slate)
			     left top width height)
  (let* ((w (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen)))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (%draw-ellipse-xy-CLX
      w gc 
      (not (eq :outline (fill-style-type (%pen-fill-style pen))))
      left top width height))))

;;;------------------------------------------------------------

(defparameter *ellipse-coordinate-vector*
    (make-array 64
		:element-type 'g:Screen-Coordinate
		:initial-element 0))

(defun ellipse-coordinate-vector (n)
  (unless (= (length *ellipse-coordinate-vector*) n)
    (let ((v (make-array n
			 :element-type 'g:Screen-Coordinate
			 :initial-element 0)))
      (do ((i 3 (+ i 6)))
	  ((>= i n))
	(setf (aref v i) 0)
	(setf (aref v (+ i 1)) 7))
      (setf *ellipse-coordinate-vector* v)))  
  *ellipse-coordinate-vector*)

(defmethod %draw-ellipses (pen (slate CLX-Output-Slate) rects)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type g:Screen-Rect-List rects))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (drawable (slate-drawable slate))
	 (n (* 6 (length rects)))
	 (i 0)
	 (xywhaa (the (Simple-Array g:Screen-Coordinate (*))
		   (ellipse-coordinate-vector n)))
	 (filled?
	  (not (eq :outline (fill-style-type (%pen-fill-style pen)))))) 
    (declare (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type g:Screen-Coordinate n i)
	     (type (Simple-Array g:Screen-Coordinate (*)) xywhaa))
    (xlib:with-display (xdisplay)
      (update-gcontext-from-slate slate gc)
      (update-gcontext-from-pen pen screen gc)
      (cond
       ;; Note: X is inconsistent between filled and non-filled cases.
       (filled?
	(dolist (r rects)
	  (declare (type g:Screen-Rect r))
	  (let ((left (g::%screen-rect-left r))
		(top (g::%screen-rect-top r))
		(width (g::%screen-rect-width r))
		(height (g::%screen-rect-height r)))
	    (declare (type g:Screen-Coordinate left top)
		     (type g:Positive-Screen-Coordinate width height))
	    (setf (aref xywhaa i) (- left 1))
	    (incf i)
	    (setf (aref xywhaa i) (- top 1))
	    (incf i)
	    (setf (aref xywhaa i) (max 0 (+ width 1)))
	    (incf i)
	    (setf (aref xywhaa i) (max 0 (+ height 1)))
	    (incf i 3))))
       (t (dolist (r rects)
	    (declare (type g:Screen-Rect r))
	    (let ((left (g::%screen-rect-left r))
		  (top (g::%screen-rect-top r))
		  (width (g::%screen-rect-width r))
		  (height (g::%screen-rect-height r)))
	      (declare (type g:Screen-Coordinate left top)
		       (type g:Positive-Screen-Coordinate width height))
	      (setf (aref xywhaa i) left)
	      (incf i)
	      (setf (aref xywhaa i) top)
	      (incf i)
	      (setf (aref xywhaa i) (max 0 (- width 1)))
	      (incf i)
	      (setf (aref xywhaa i) (max 0 (- height 1)))
	      (incf i 3)))))
      (xlib:draw-arcs drawable gc xywhaa filled?))))

;;;------------------------------------------------------------

(defparameter *circle-coordinate-vector*
    (make-array 64
		:element-type `g:Screen-Coordinate
		:initial-element 0))

(defun circle-coordinate-vector (n)
  (unless (= (length *circle-coordinate-vector*) n)
    (let ((v (make-array n
			 :element-type 'g:Screen-Coordinate
			 :initial-element 0)))
      (do ((i 3 (+ i 6)))
	  ((>= i n))
	(setf (aref v i) 0)
	(setf (aref v (+ i 1)) 7))
      (setf *circle-coordinate-vector* v)))
  *circle-coordinate-vector*)

(defmethod %draw-circles (pen (slate CLX-Output-Slate) points radii)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type g:Screen-Point-List points)
           (type g:Positive-Screen-Coordinate-List radii))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (drawable (slate-drawable slate))
	 (n (* 6 (min (length points) (length radii))))
	 (i 0)
	 (xywhaa (circle-coordinate-vector n))
	 (filled?
	  (not (eq :outline (fill-style-type (%pen-fill-style pen)))))) 
    (declare (type xlib:GContext gc)
	     (type xlib:Drawable drawable)
	     (type g:Screen-Coordinate n i)
	     (type (Simple-Array g:Screen-Coordinate (*)) xywhaa))
    (xlib:with-display (xdisplay)
      (update-gcontext-from-slate slate gc)
      (update-gcontext-from-pen pen screen gc)
      (cond
       ;; Note: X is inconsistent between filled and non-filled cases.
       (filled?
	(loop for p in points
	    for radius Fixnum in radii do
	      (let ((d (max 0 (+ (* 2 radius) 1)))
		    (left (- (g::%screen-point-x p) radius))
		    (top (- (g::%screen-point-y p) radius)))
		(declare (type g:Screen-Coordinate left top)
			 (type g:Positive-Screen-Coordinate d))
		(setf (aref xywhaa i) (- left 1)) (incf i)
		(setf (aref xywhaa i) (- top 1)) (incf i)
		(setf (aref xywhaa i) d) (incf i)
		(setf (aref xywhaa i) d) (incf i 3))))
       (t
	(loop for p in points
	    for radius Fixnum in radii do
	      (let ((d (max 0 (- (* 2 radius) 1)))
		    (left (- (g::%screen-point-x p) radius))
		    (top (- (g::%screen-point-y p) radius)))
		(declare (type g:Screen-Coordinate left top)
			 (type g:Positive-Screen-Coordinate d))
		(setf (aref xywhaa i) left) (incf i)
		(setf (aref xywhaa i) top) (incf i)
		(setf (aref xywhaa i) d) (incf i)
		(setf (aref xywhaa i) d) (incf i 3)))))
      (xlib:draw-arcs drawable gc xywhaa filled?))))

#||
(defmethod %draw-circles-1 (pen (slate CLX-Output-Slate) points radii)
  (let* ((w (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (filled? (not (eq :outline (fill-style-type (%pen-fill-style pen))))))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (loop
      for p in points
      for r in radii
      for d = (+ r r)
      do (%draw-ellipse-xy-CLX
	  w gc filled?
	  (- (g::%screen-point-x p) r) (- (g::%screen-point-y p) r) d d)))))
||#

;;;============================================================
;;; Drawing Polygons
;;;============================================================

(defmethod %draw-polygon-xy (pen (slate CLX-Output-Slate) xy-list)
  (let* ((w (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (filled? (not (eq :outline (fill-style-type (%pen-fill-style pen))))))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (if filled?
	 (xlib:draw-lines w gc xy-list :fill-p t)
       (xlib:draw-lines
	w gc (append xy-list (list (first xy-list) (second xy-list)))
	:fill-p nil)))))

(defmethod %draw-polygon (pen (slate CLX-Output-Slate) point-list)
  (%draw-polygon-xy pen slate (g::point-list->xy-list point-list))) 

;;;============================================================
;;; copy-slate-rect (bitblt) operations
;;;============================================================

(defmethod %copy-slate-rect-xy (pen
				(from-slate CLX-Output-Slate)
				from-left from-top width height
				(to-slate CLX-Output-Slate)
				to-left to-top)
  (let* ((screen (slate-screen to-slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))	 
	 ;; Have to clip width, height to source slate, so X won't just
	 ;; assume 0`s
	 (clipped-width
	  (+ 1 (g::%iclip (- (slate-width from-slate) from-left) 0 width)))
	 (clipped-height
	  (+ 1 (g::%iclip (- (slate-height from-slate) from-top) 0 height))))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate to-slate gc)
     (update-gcontext-from-pen pen screen gc)
     (xlib:copy-area (slate-drawable from-slate)
		     gc
		     from-left from-top clipped-width clipped-height
		     (slate-drawable to-slate) to-left to-top ))))

(defmethod %copy-slate-rects (pen (from-slate CLX-Output-Slate) from-rects
			      (to-slate CLX-Output-Slate) to-points)
  (declare (optimize (safety 1) (speed 3))
           (type Pen pen)
           (type CLX-Output-Slate from-slate to-slate))
  (let* ((screen (slate-screen to-slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (from-drawable (slate-drawable from-slate))
	 (from-width (slate-width from-slate))
	 (from-height (slate-height from-slate))
	 (to-drawable (slate-drawable to-slate)))
    (declare (type xlib:GContext gc)
	     (type xlib:Drawable from-drawable to-drawable)
	     (type g:Positive-Screen-Coordinate from-width from-height))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate to-slate gc)
     (update-gcontext-from-pen pen (slate-screen to-slate) gc)
     (loop
      for r in from-rects
      for p in to-points do
       (let* ((r r)
	      (p p)
	      (r-left (g::%screen-rect-left r))
	      (r-width (g::%screen-rect-width r))
	      (r-top (g::%screen-rect-top r))
	      (r-height (g::%screen-rect-height r))
	      ;; Have to clip width, height to source slate, so X won't just
	      ;; assume 0`s
	      (clipped-width
	       (+ 1 (g::%iclip (- from-width r-left) 0 r-width)))
	      (clipped-height
	       (+ 1 (g::%iclip (- from-height r-top) 0 r-height))))
	 (declare (type g:Screen-Rect r)
		  (type g:Screen-Point p)
		  (type g:Screen-Coordinate r-left r-top)
		  (type g:Positive-Screen-Coordinate
			r-width r-height clipped-width clipped-height))
	 (xlib:copy-area
	  from-drawable gc
	  r-left r-top clipped-width clipped-height
	  to-drawable (g::%screen-point-x p) (g::%screen-point-y p)))))))

;;;============================================================
;;; Drawing strings and characters
;;;============================================================

(defmethod %draw-character (pen (slate CLX-Output-Slate) char point)
  (let* ((window (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (x (g::%screen-point-x point))
	 (y (g::%screen-point-y point)))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (xlib:draw-glyph window gc x y char))))

(defmethod %draw-string (pen (slate CLX-Output-Slate) string point)
  (let* ((window (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen))
	 (x (g::%screen-point-x point))
	 (y (g::%screen-point-y point)))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (xlib:draw-glyphs window gc x y string))))

(defmethod %draw-centered-string (pen (slate CLX-Output-Slate) string point)
  (declare (optimize (safety 1) (speed 3))
           (type Pen pen)
           (type CLX-Output-Slate slate)
	   (type Simple-String string)
	   (type g:Screen-Point point))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen)))
    (multiple-value-bind
	(w a d)
	(%analyze-string-placement screen string (%pen-font pen))
      (declare (type g:Screen-Coordinate w a d))
      (let ((window (slate-drawable slate))
	    (gc (screen-pen-gcontext screen pen))
	    (x (- (g::%screen-point-x point) (ash w -1)))
	    (y (+ (g::%screen-point-y point) (ash (- a d) -1))))
	(declare (type xlib:Drawable window)
		 (type xlib:GContext gc)
		 (type g:Screen-Coordinate x y))
	(xlib:with-display (xdisplay)
	 (update-gcontext-from-slate slate gc)
	 (update-gcontext-from-pen pen screen gc)
	 (xlib:draw-glyphs window gc x y string :width w))))))

;;; layout determined by global font characteristics
#||
(defmethod %draw-vertical-string (pen (slate CLX-Output-Slate) string point)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type Simple-String string)
	   (type g:Screen-Point point))
  (let* ((font (%pen-font pen))
	 (xfont (%screen-font screen font))
	 (str-width (xlib:maxchar-width xfont))
	 (a (xlib:maxchar-ascent xfont))
	 (d (xlib:maxchar-descent xfont))
	 (init-x (g::%screen-point-x point))
	 (x (+ init-x (ash (the Fixnum str-width) -1)))
	 (y (g::%screen-point-y point))
	 (window (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (gc (screen-pen-gcontext screen pen)))
    (declare (type Font font)
             (type xlib:Font xfont)
	     (type g:Screen-Coordinate str-width init-x x y)
	     (type xlib:Drawable window)
	     (type xlib:Gcontext gc))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (dotimes (i (length string))
       (declare (type Fixnum i))
       (let* ((char (char (the String string) i))
	      (w (xlib:char-width
		  xfont (the Fixnum (char-code char)))))
	 (declare (type Character char)
		  (type g:Positive-Screen-Coordinate w))
	 (incf y a) 
	 ;; center char within vertical column
	 (xlib:draw-glyph window gc (- x (ash w -1)) y char)
	 ;; move down by this char's descent
	 (incf y d))))))
||#

;;; per character layout
(defmethod %draw-vertical-string (pen (slate CLX-Output-Slate) string point)
  (declare (optimize (safety 1) (speed 3))
	   (type Pen pen)
	   (type CLX-Output-Slate slate)
	   (type Simple-String string)
	   (type g:Screen-Point point))
  (let* ((screen (slate-screen slate))
	 (xdisplay (xdisplay screen))
	 (font (%pen-font pen))
	 (xfont (%screen-font screen font))
	 (str-width (%vertical-string-width screen string font))
	 (init-x (g::%screen-point-x point))
	 (x (+ init-x (ash (the Fixnum str-width) -1)))
	 (y (g::%screen-point-y point))
	 (window (slate-drawable slate))
	 (gc (screen-pen-gcontext screen pen)))
    (declare (type Screen screen)
	     (type Font font)
             (type xlib:Font xfont)
	     (type g:Screen-Coordinate str-width init-x x y)
	     (type xlib:Drawable window)
	     (type xlib:Gcontext gc))
    (xlib:with-display (xdisplay)
     (update-gcontext-from-slate slate gc)
     (update-gcontext-from-pen pen screen gc)
     (dotimes (i (length string))
       (declare (type Fixnum i))
       (let* ((char (char string i))
	      (xfont-index (char-code char))
	      (w (xlib:char-width xfont xfont-index))
	      (a (xlib:char-ascent xfont xfont-index))
	      (d (xlib:char-descent xfont xfont-index)))
	 (declare (type Character char)
		  (type Fixnum xfont-index)
		  (type g:Screen-Coordinate w a d))
	 ;; except for first char, move down by this char's ascent
	 ;; a+d turns out to be exact character height, with no
	 ;; extra room, so add a pixel space for readability
	 ;; We should probably be using the font line height,
	 ;; or some such, here. As it is this doesn't do the right thing
	 ;; with spaces.
	 (incf y (+ 1 a)) 
	 ;; center char within vertical column
	 (xlib:draw-glyph window gc (- x (ash w -1)) y char)
	 ;; move down by this char's descent
	 (incf y d))))))


