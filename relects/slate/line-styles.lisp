;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Dashing Patterns
;;;============================================================
;;;
;;; A dashing pattern is a Sequence of Fixnums, giving numbers of
;;; on and off "pixels". This is based on Symbolics graphics,
;;; for want of a better model.
#||
(defun dash-description? (d)
  (and (typep d 'Sequence)
       (every #'(lambda (x) (typep x 'Fixnum)) d)))

(deftype Dash-Description () '(satisfies dash-description?))

(defun equal-dash-descriptions? (d0 d1)
  (az:declare-check (type Dash-Description d0 d1))
  (or (eql d0 d1)
      (and (= (length d0) (length d1))
	   (every #'= d0 d1))))

(defun copy-dash-description (d &key result)
  (az:declare-check (type Dash-Description d result))
  (if (null result)
      (copy-seq d)
      (progn
	(assert (= (length d) (length result)))
	(dotimes (i (length d))
	  (setf (elt result i) (elt d i)))
	result)))
||#
;;;============================================================
;;; Line Styles
;;;============================================================

(defstruct (Line-Style 
	    (:conc-name nil)
	    (:constructor make-line-style)
	    (:copier %%copy-line-style))
  (line-style-dash-rule
   :solid
   :type (Member :solid
		 #-:clx-mit-r5 :on-off-dash
		 #+:clx-mit-r5 :dash
		 :double-dash))
  (line-style-thickness
   0
   :type g:Positive-Screen-Coordinate)
  
  (line-style-dashes
   4
   :type (or List g:Positive-Screen-Coordinate))
  (line-style-dash-offset
   0
   :type g:Positive-Screen-Coordinate)
  (line-style-cap
   :butt
   :type (Member :not-last :butt :round :projecting)) ;; for future expansion
  (line-style-join
   :miter
   :type (Member :miter :round :bevel)) ;; for future expansion
  (line-style-arrowhead-length
   8
   :type g:Positive-Screen-Coordinate)
  (line-style-arrowhead-angle
   (/ pi 12.0d0)
   :type Double-Float)) 

(defun line-style? (x) (typep x 'Line-Style))

(defun equal-line-styles? (ls0 ls1)
  (and (= (line-style-thickness ls0)
	  (line-style-thickness ls1))
       (equal (line-style-dashes ls0) (line-style-dashes ls1))
       (= (line-style-dash-offset ls0)
	  (line-style-dash-offset ls1))
       (eql (line-style-dash-rule ls0) (line-style-dash-rule ls1))
       (eql (line-style-cap ls0) (line-style-cap ls1))
       (eql (line-style-join ls0) (line-style-join ls1))
       (= (line-style-arrowhead-length ls0)
	  (line-style-arrowhead-length ls1))
       (= (line-style-arrowhead-angle ls0)
	  (line-style-arrowhead-angle ls1))))

(defun copy-line-style (ls &key (result (make-line-style)))
  (setf (line-style-thickness result) (line-style-thickness ls))
  (setf (line-style-dashes result)
    (if (listp (line-style-dashes ls))
	(copy-list (line-style-dashes ls))
      ;; else
      (line-style-dashes ls)))
  (setf (line-style-dash-offset result) (line-style-dash-offset ls))
  (setf (line-style-cap result) (line-style-cap ls))
  (setf (line-style-join result) (line-style-join ls))
  (setf (line-style-arrowhead-length result)
    (line-style-arrowhead-length ls))
  (setf (line-style-arrowhead-angle result)
    (line-style-arrowhead-angle ls))
  result)

