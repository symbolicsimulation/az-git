;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; built-in menu and i/o facilities
;;;============================================================

;;; takes a list of 'menu items', each of which is a list whose
;;; first item is a string, and whose second item is a function
;;; to be applied to the remainder of the list if the item is
;;; selected. If the machine-specific slate doesn't support menus,
;;; provide simple tty-oriented menu:

(defgeneric menu (pen slate menu-list)
  (declare (type Pen pen)
	   (type Input-Slate slate)
	   (type List menu-list)))

(defmethod menu (pen slate menu-list)
  (az:declare-check (type Pen pen)
		    (type Input-Slate slate))
  (let (selected-item-number)
    ;; print out menu options
    (dotimes (i (length menu-list))
      (format t "(~D) ~A~%" (+ i 1) (car (nth i menu-list))))
    (format t "type number to select choice: ")
    (setq selected-item-number (read))
    (cond ( (and (integerp selected-item-number)
                 (> selected-item-number 0)
                 (<= selected-item-number (length menu-list)))
	    (let ((selected-item (nth (- selected-item-number 1) menu-list)))
	      (apply (second selected-item) (rest (rest selected-item)))) )
          ( t
	    (format t "illegal selection~%")
	    nil ) )
    ))
        
    
;;; prints out a string, and allows the user to type in a string in response.
;;; If the machine-specific slate doesn't support special "dialog boxes",
;;; provide simple tty-oriented interface

(defgeneric input-string (pen slate prompt-string)
  (declare (type Pen pen)
	   (type Input-Slate slate)
	   (type String prompt-string)
	   (values String)))

(defmethod input-string (pen slate prompt-string)
  (az:declare-check (type Pen pen)
		    (type Input-Slate slate))
  (format t (cond ( prompt-string prompt-string )
		  ( t "Enter string: " )))
  (read-line))




