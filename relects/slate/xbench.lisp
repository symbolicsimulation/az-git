;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================

(progn
(setf *slate* (default-slate t))
(setf *invisible-slate* (make-invisible-slate
			 :screen (slate-screen *slate*)
			 :width (slate-width *slate*)
			 :height (slate-height *slate*)))

(defparameter *origin* (g:make-screen-point :x 0 :y 0))
(defparameter *n* 1000)
(defparameter *points*
    (g::make-random-screen-points (slate-rect *slate*) *n*))

(defparameter *point-seq*
    (loop for p in *points*
     collect (g:screen-point-x p)
     collect (g:screen-point-y p)))

(defparameter *more-points*
    (g::make-random-screen-points (slate-rect *slate*) *n*))

(defparameter *n-curves* 5000)

#||
(defparameter *curves*
    (loop for i from 0 below *n-curves* collect (make-random-curve)))
(defparameter *xy-curves*
    (loop for curve in *curves*
      collect (g::%point-list->xy-list curve)))
||#

(defparameter *seg-seq*
    (loop
     for p0 in *points*
     for p1 in *more-points*
     collect (g:screen-point-x p0)
     collect (g:screen-point-y p0)
     collect (g:screen-point-x p1)
     collect (g:screen-point-y p1)))


(defparameter *radius* 5)
(defparameter *radii* (make-list *n* :initial-element *radius*))

(defparameter *circle-seq*
    (loop
     with diameter = (* 2 *radius*)
     with 2pi = (* 2.0d0 pi)
     for p in *points*
     collect (- (g:screen-point-x p) *radius*)
     collect (- (g:screen-point-y p) *radius*)
     collect diameter
     collect diameter
     collect 0.0d0
     collect 2pi))

(defparameter *rects*
    (g::make-random-screen-rects (slate-rect *slate*) *n*))

(defparameter *rect-seq*
   (loop for r in *rects*
     collect (g:screen-rect-left r)
     collect (g:screen-rect-top r)
     collect (g:screen-rect-width r)
     collect (g:screen-rect-height r)))
)


(let ((drawable (slate-drawable *slate*))
      (gcontext (screen-pen-gcontext (slate-screen *slate*) *xor-red-pen*)))
  (clear-slate *slate*)
  (update-gcontext-from-pen *xor-red-pen* (slate-screen *slate*) gcontext)
  (time
   (progn
     (dotimes (i 1000)
       (xlib:draw-points drawable gcontext *point-seq*))
     (xlib:display-finish-output (xdisplay (slate-screen *slate*))))))

(progn
  (clear-slate *slate*)
  (time
   (progn
   (dotimes (i 1000)
     (%draw-points *fast-xor-red-pen* *slate* *points*))
   (%finish-drawing *slate*))))

(let ((drawable (slate-drawable *slate*))
      (gcontext (screen-pen-gcontext (slate-screen *slate*) *xor-red-pen*)))
  (clear-slate *slate*)
  (update-gcontext-from-pen *xor-red-pen* (slate-screen *slate*) gcontext)
  (time
   (progn
     (dotimes (i 11)
       (xlib:draw-lines drawable gcontext *point-seq*))
     (xlib:display-finish-output (xdisplay (slate-screen *slate*))))))

(time
 (progn
   (dotimes (i 11)
     (%draw-polyline *xor-red-pen* *slate* *points*)
     (%finish-drawing *slate*))))

(let ((from-drawable (slate-drawable *invisible-slate*))
      (to-drawable (slate-drawable *slate*))
      (width (slate-width *invisible-slate*))
      (height (slate-height *invisible-slate*))
      (gcontext (screen-pen-gcontext (slate-screen *slate*) *xor-red-pen*)))
  (time
   (progn
     (dotimes (i 100)
       (update-gcontext-from-pen (erasing-fill-pen)
				 (slate-screen *invisible-slate*) gcontext)
       (xlib:draw-rectangle from-drawable gcontext 0 0 width height)
       (update-gcontext-from-pen *xor-red-pen*
				 (slate-screen *invisible-slate*) gcontext)
       (xlib:draw-segments from-drawable gcontext *point-seq*)
       (xlib:copy-area from-drawable gcontext 0 0 width height
		       to-drawable 0 0))
     (xlib:display-finish-output (xdisplay (slate-screen *slate*)))
     (clear-slate *slate*))))

(time
 (progn
   (dotimes (i 100)
     (clear-slate *invisible-slate*)
     (%draw-polyline *xor-red-pen* *invisible-slate* *points*)
     (%copy-slate-rect *xor-red-pen*
		       *invisible-slate* (slate-rect *invisible-slate*)
		       *slate* *origin*))
   (%finish-drawing *slate*)))

(let ((pen (copy-pen *xor-red-pen*))) 
  (time (draw-curves *curves* pen)))

(let ((drawable (slate-drawable *slate*))
      (gcontext (screen-pen-gcontext (slate-screen *slate*) *xor-red-pen*)))
  (clear-slate *slate*)
  (time
   (progn
     (loop for xy-curve in *xy-curves*
      for i from 0
      do
       (setf (xlib:gcontext-foreground gcontext) (mod i 256))
       (xlib:draw-lines drawable gcontext xy-curve))
     (xlib:display-finish-output (xdisplay (slate-screen *slate*))))))


(let ((drawable (slate-drawable *slate*))
      (gcontext (screen-pen-gcontext (slate-screen *slate*) *xor-red-pen*)))
  (clear-slate *slate*)
  (update-gcontext-from-pen *xor-red-pen* (slate-screen *slate*) gcontext)
  (time
   (progn
     (xlib:draw-arcs drawable gcontext *circle-seq* nil)
     (xlib:display-finish-output (xdisplay (slate-screen *slate*))))))

(progn (clear-slate *slate*)
       (time
	(progn
	  (%draw-circles *xor-red-pen* *slate* *points* *radii*)
	  (%finish-drawing *slate*))))

(time
 (progn
   (%draw-circles-1 *xor-red-pen* *slate* *points* *radii*)
   (%finish-drawing *slate*)))


(let ((drawable (slate-drawable *slate*))
      (gcontext (screen-pen-gcontext (slate-screen *slate*) *xor-red-pen*)))
  (clear-slate *slate*)
  (update-gcontext-from-pen *xor-red-pen*
			    (slate-screen *slate*) gcontext)
  (time
   (progn
     (xlib:draw-rectangles drawable gcontext *rect-seq* t)
     (xlib:display-finish-output (xdisplay (slate-screen *slate*))))))

(progn
  (clear-slate *slate*)
  (time (progn
	  (%draw-rects *xor-red-pen* *slate* *rects*)
	  (%finish-drawing *slate*))))

(progn
  (clear-slate *slate*)
  (time (progn
	  (%draw-rects-1 *xor-red-fill-pen* *slate* *rects*)
	  (%finish-drawing *slate*))))


;;;============================================================

(defparameter *bitmap*
    (make-invisible-slate
     :screen (slate-screen *slate*)
     :width 10 :height 10))

(defparameter *rlist*
    (make-list *n* :initial-element (slate-rect *bitmap*)))
(progn
  (clear-slate *bitmap*)
  (draw-ellipse *red-fill-pen* *bitmap*
		(g:make-screen-rect :width 10 :height 10))
  (draw-line *white-pen* *bitmap*
	     (g:make-screen-point) (g:make-screen-point :x 9 :y 9))
  (draw-line *green-pen* *bitmap*
	     (g:make-screen-point :y 9) (g:make-screen-point :x 9))
  (draw-rect *blue-pen* *bitmap*
	     (g:make-screen-rect :left 2 :top  2 :width 6 :height 6)))

(progn
  (clear-slate *slate*) 
  (time
   (progn
     (%copy-slate-rects *xor-red-pen* *bitmap* *rlist* *slate* *points*)
     (%copy-slate-rects *xor-red-pen* *bitmap* *rlist* *slate* *points*)
     (%finish-drawing *slate*))))

