%%% -*- Mode:Lisp; Syntax: Common-Lisp; -*-

\section{Drawing on Slates}
\label{drawing-on-slates}

Most drawing functions take 
(1) a {\sf pen} argument
that determines the ``style'' of drawing, that is, color, font,
filling regions vs. outlining, and so on,
(2) a {\sf slate} argument that is the surface to be drawn on,
and
(3) one or more additional arguments that determine which pixels
on the slate are to be altered.

No drawing operation will affect any pixels outside the {\sf slate}'s
clipping region.

Most drawing operations come in both singular and plural forms
(eg. {\sf draw-point} and {\sf draw-points}).
The reason for this is that it is often possible to implement
the plural operations much more efficiently 
than repeated calls to the singular version.
(X also has both singular and plural drawing operations.)

\subsection{Clipping}

All drawing on a slate is clipped to the slate's clipping region,
which will always be a subset of the slate's {\sf slate-rect}.
 
The clipping region can be changed by hand using:

\begin{verbatim}
(defun slate-clipping-region (slate &key result)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Rect result)
  (values result))

(defun (setf slate-clipping-region) (region slate)
  (tools:type-check g:Screen-Rect region)
  (tools:type-check Slate slate)
  (values region))

\end{verbatim}

However, it's preferable to bind it temporarily, using:
\begin{verbatim}

(defmacro with-clipping-region ((slate region) &body body))

\end{verbatim}

In the future, clipping regions may be generalized to shapes other than
simple 
\\
{\sf g:Screen-Rect}'s.

\subsection{Flushing output}

In most implementations, drawing operations will be buffered
and you will get the best performance if you let the system
decide exactly when to draw on the screen.
However, sometimes it's necessary to be able to guarantee that
all pending drawing operations have completed, for example,
to synchronize output with input events.
For this purpose, we provide two operations for flushing the output buffers:

\begin{verbatim}

(defun finish-drawing (slate)
  (tools:type-check Slate slate)
  (values nil))
\end{verbatim}
{\sf finish-drawing} is analogous to the standard CL {\sf finish-output} 
(\cite{Stee90}, p. 579), that is,
it attempts to ensure that all the drawing done on {\sf slate}
is completed before it returns (the returned value is always {\sf nil}).

\begin{verbatim}

(defun force-drawing (slate)
  (tools:type-check Slate slate)
  (values nil))
\end{verbatim}
{\sf force-drawing} is analogous to the standard CL {\sf force-output} 
(\cite{Stee90}, p. 579), that is,
it initiates the emptying of any internal buffers,
but returns {\sf nil} immediately, without waiting for the drawing to
actually complete.
 

\subsection{Pen position}

The pen's current position on the slate is
used by relative drawing operations:

\begin{verbatim}
(defun slate-point (slate &key result)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point result)
  (values result))

(defun (setf slate-point) (pos slate)
  (tools:type-check g:Screen-Point pos)
  (tools:type-check Slate slate)
  (values g:Screen-Point))

(defun move-to (slate pos)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point pos)
  (values g:Screen-Point))

(defun move-by (slate dp)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Vector dp)
  (values g:Screen-Point))
\end{verbatim}

\subsection{Points}

\begin{verbatim}

(defun draw-point (pen slate point) 
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point point))

(defun draw-points (pen slate point-list)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point-List point-list))

\end{verbatim}

{\sf draw-point} should draw a square of pixels, centered on {\sf point}.
The width of the square should be
{\sf (line-style-thickness (pen-line-style pen))}
{\sf draw-point} ignores the tiling and filling properties of the {\sf pen}.

\subsection{Lines}

\begin{verbatim} 

(defun draw-to (pen slate point)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point point))
\end{verbatim}
{\sf draw-to} draws a line of width 
{\sf (line-style-thickness (pen-line-style pen))}
from the {\sf slate}'s current point
to the destination {\sf point}.
Zero thickness is interpreted to mean draw the fastest possible visible line.
{\sf draw-to} ignores the tiling and filling properties of the {\sf pen}.

\begin{verbatim}

(defun draw-by (pen slate v)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Vector v))
\end{verbatim}
{\sf draw-by} draws a line of width 
{\sf (line-style-thickness (pen-line-style pen))}
from the {\sf slate}'s current point
to the current point plus the displacement vector {\sf v}.
Zero thickness is interpreted to mean draw the fastest possible visible line.
{\sf draw-by} ignores the tiling and filling properties of the {\sf pen}.

\begin{verbatim}

(defun draw-line (pen slate p0 p1)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-point p0 p1))
\end{verbatim}
{\sf draw-line} draws a line of width 
{\sf (line-style-thickness (pen-line-style pen))}
from {\sf p0} to {\sf p1}.
Zero thickness is interpreted to mean draw the fastest possible visible line.
{\sf draw-line} ignores the tiling and filling properties of the {\sf pen}.

\begin{verbatim}

(defun draw-lines (pen slate points0 points1) 
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point-List points0 points1))
\end{verbatim}
{\sf draw-lines} is the plural version of {\sf draw-line}.
It draws several (unconnected) lines with a given pen on a given slate.
This is different from draw-polyline!

\begin{verbatim} 

(defun draw-arrow (pen slate p0 p1)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point p0 p1))

(defun draw-arrows (pen slate points0 points1) 
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point-List points0 points1))
\end{verbatim}
{\sf draw-arrow} draws an arrow whose tail is at {\sf p0} and head at {\sf p1}.

\begin{verbatim}

(defun draw-polyline (pen slate point-list)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point-List point-list)
  (%draw-polyline pen slate point-list))

(defun draw-polylines (pen slate point-list-list)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check List point-list-list)
  (%draw-polylines pen slate point-list-list))
\end{verbatim}
{\sf draw-polyline} draws as piecewise linear curve interpolating
the points in {\sf point-list}.


\subsection{Geometric Shapes}

\begin{verbatim}

(defun draw-rect (pen slate rect)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Rect rect)) 

(defun draw-rects (pen slate rects)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Rect-List rects))

(defun clear (slate &key rect)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Rect rect)) 
\end{verbatim}

\begin{verbatim}

(defun draw-ellipse (pen slate rect)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Rect rect))

(defun draw-ellipses (pen slate rects)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Rect-List rects))
\end{verbatim}
{\sf draw-ellipse} draws the largest ellipse that will fit within {\sf rect}.

\begin{verbatim}

(defun draw-circle (pen slate point r)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point point)
  (tools:type-check g:Positive-Screen-Coordinate r))

(defun draw-circles (pen slate points rs)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point-List points)
  (tools:type-check g:Positive-Screen-Coordinate-List rs))
\end{verbatim}
For convenience, we also provide {\sf draw-circle},
which draws a circle centered on {\sf point} of radius {\sf r}.

\begin{verbatim}

(defun draw-polygon (pen slate point-list)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check g:Screen-Point-List point-list))

(defun draw-polygons (pen slate point-list-list)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check List point-list-list))
\end{verbatim}

\subsection{Characters and Strings}

\begin{verbatim}

(defun draw-character (pen slate char p)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check Character char)
  (tools:type-check g:Screen-Point p))

(defun draw-characters (pen slate chars points)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check (List Character) chars)
  (tools:type-check g:Screen-Point-List points))
\end{verbatim}
The exact placement of the character relative to {\sf point}
is described in detail in section~\ref{text-measurements}
and illustrated in figure~\ref{character-placement}.

\begin{verbatim} 
(defun draw-string (pen slate string point)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check String string)
  (tools:type-check g:Screen-Point point))
\end{verbatim}
As described in section~\ref{text-measurements},
the text is drawn with the left baseline starting at {\sf point}. 
When the drawing is done, the {\sf slate-point} of the slate is at the
baseline and the first pixel to the right of the last character.

\begin{verbatim} 

(defun draw-centered-string (pen slate string point)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check String string)
  (tools:type-check g:Screen-Point point))
\end{verbatim}
The text is drawn with the center of the string's screen rect at {\sf point}. 
When the drawing is done, the {\sf slate-point} of the slate is {\sf point}.

\begin{verbatim} 

(defun draw-vertical-string (pen slate string point)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check String string)
  (tools:type-check g:Screen-Point point))
\end{verbatim}
The text is drawn with the point at the upper left hand corner, not
the left baseline, aligned with {\sf point}. 
When the drawing is done,
the {\sf slate-point} is just below the lower left corner of the last
character's screen rect.

\begin{verbatim} 

(defun draw-centered-vertical-string (pen slate string point)
  (tools:type-check Pen pen)
  (tools:type-check Slate slate)
  (tools:type-check String string)
  (tools:type-check g:Screen-Point point))
\end{verbatim}
The text is drawn with the center of the string's screen rect at {\sf point}. 
When the drawing is done, the {\sf slate-point} of the slate is {\sf point}.

\subsection{Bitblt-like operations}

\begin{verbatim}  

(defun copy-slate-rect (pen
			      from-slate from-rect
			      to-slate to-point)
  (tools:type-check Pen pen)
  (tools:type-check Slate from-slate to-slate)
  (tools:type-check g:Screen-Rect from-rect)
  (tools:type-check g:Screen-Point to-point))
  
(defun copy-slate-rects (pen
			       from-slate from-rects
			       to-slate to-points)
  (tools:type-check Pen pen)
  (tools:type-check Slate from-slate to-slate)
  (tools:type-check g:Screen-Rect-List from-rects)
  (tools:type-check g:Screen-Point-List to-points))
\end{verbatim}
{\sf copy-slate-rect} copies the source pixels 
in {\sf from-rect} on {\sf from-slate}
to {\sf to-slate} so that the rect of affected destination pixels 
has its origin at {\sf to-point}.
The source pixels are combined with the destination pixels
using the {\sf (pen-boolean-operation pen)}.
{\sf from-slate} and {\sf to-slate} must have the same {\sf slate-screen}.
(This may be relaxed in the future.)