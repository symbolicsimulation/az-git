%%% -*- Mode:Lisp; Syntax: Common-Lisp; -*-

\section{Colors}
\label{colors}

\subsection{The Abstract Color Type}

To use color, one needs to create an instance of {\sf Color}.

There is no (exported) {\sf make-color} function, 
because having one would require either
exposing the internal implementation of colors or supporting 
a complicated interface. 
To make a color object,
use one of the functions 
described in sections ~\ref{color-coordinate-systems} and ~\ref{color-names}.

The type predicate for color objects is {\sf color?},
the equality predicate is {\sf equal-colors?},
and the copier is {\sf copy-color}.

\subsection{Color coordinate systems}
\label{color-coordinate-systems}

(Note that the IHS, Hexcone-IHS, and YIQ color specifications
are currently broken, Feb, 1992).

Color objects can be created, 
or an existing color object can be modified,
in RGB, IHS, Hexcone-IHS, or YIQ color coordinates, 
using the functions: 
{\sf rgb-{\tt>}color},
{\sf ihs-{\tt>}color},
{\sf hexcone-ihs-{\tt>}color},
and
{\sf yiq-{\tt>}color}.

Color coordinates are all {\sf (Double-Float 0.0d0 1.0d0)}.

The coordinates of an existing color object,
in the various coordinate can be extracted with:
{\sf color-{\tt>}rgb},
{\sf color-{\tt>}ihs},
{\sf color-{\tt>}hexcone-ihs},
and
{\sf color-{\tt>}yiq}.

For convenience in color calculation,
functions are provided for converting between coordinate systems:
{\sf rgb-{\tt>}ihs},
{\sf ihs-{\tt>}rgb},
{\sf rgb-{\tt>}hexcone-ihs},
{\sf hexcone-ihs-{\tt>}rgb},
{\sf rgb-{\tt>}yiq},
and
{\sf yiq-{\tt>}rgb}.

For the exact relationship between RGB and the other coordinate systems,
see the source code. 
(In future, we should get a precise definition with references
and add to this spec.)


\subsection{Color Names}
\label{color-names}

Colors can also be specified by name, as in X \cite{Nye88a,Sche90}.
A {\sf Color-Name} is a keyword,
corresponding to the color names in the X server's color database,
usually found in a file called {\sf rgb.txt},
which may vary from server to server.
Because {\sf Color-Name}s are just keywords, 
we haven't bothered to implement a full abstract type 
as described in \cite{McDo91g}.
You can test if a keyword is a currently valid {\sf Color-Name}
by doing {\sf (typep keyword 'Color-Name)}.
To get a color object corresponding to a given keyword,
use {\sf color-name-{\tt>}color}.

\subsection{A Resource of Color Objects}

We provide a resource of color objects, following the 
protocol discussed in \cite{McDo91g},
using the functions {\sf borrow-color} and {\sf return-color},
and the macro {\sf with-borrowed-color}.

{\sf borrow-color} and {\sf with-borrowed-color}
do not take any initialization options,
because to do so would require either exposing the
internal representation of colors,
or implementing a complicated interface that could handle
all the possible ways of specifying colors.
In the future we may add functions like {\sf borrow-color-rgb},
{\sf borrow-color-ihs}, and so on.

\subsection{Color Metrics}
\label{color-metrics}

Colormaps (see section~\ref{colormaps})
use color metric functions to choose a pixel value
whose entry in the colormap best approximates a given color.
A color metric is a function of two arguments,
which must be color objects,
which returns a non-negative {\sf Double-Float} result.
It must return zero if the two arguments are equal in the sense
of {\sf equal-colors?}.
It may return zero for colors that are not equal,
meaning that the application using the metric doesn't care
about the difference between the two colors,
which might be the case, for example, 
if the two colors can't be distinguished on the display device.
The default color metric for standard colormaps is
{\sf color-l1-dist} which computes the sum of the absolute
differences of the rgb values.


