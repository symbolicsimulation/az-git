%%% -*- Mode:Lisp; Syntax: Common-Lisp; -*-

\section{Overview}
\label{slate-overview}

This document describes the Slate module,
a component of a system called Arizona,
now under development at the U. of Washington.

Arizona is intended to be a portable, public-domain collection 
of tools supporting scientific computing, quantitative graphics, 
and data analysis,
implemented in Common Lisp
and CLOS (the Common Lisp Object System) \cite{Stee90}.
This document assumes the reader is familiar with Common Lisp and CLOS.
An overview of Arizona is given in \cite{McDo88b}
and an introduction to the current release is in \cite{McDo91g}.

Slate is a low level ``device-independent'' graphics package 
that has been ported to a number
of Common Lisp platforms, window systems, and other graphics devices.
It is intended to be used by developers 
of higher level scientific and statistical graphics systems
rather than end users.
Slate is something like a simplified version of CLX \cite{CLX89}, ported
to run on window systems and graphics devices other than X11.

The source code implementing Slate is found in az/slate/. 
The definitions are in the {\sf Slate} package.
Slate requires the Tools module (described in \cite{McDo91g})
and the Geometry module (described in \cite{McDo91j}).

\subsection{Motivation}

A legitimate question is why we are defining a new CL graphics/window
interface, rather than just using one or more of 
Garnet \cite{Myer90b,Myer90c}, 
CLIM, 
Common Windows \cite{Inte86,Fran89a,Fran90a}, 
CLX \cite{CLX89}, etc.?

The primary reason is that, as of Summer 1990,
none of the various proposed standard CL window interfaces
were stable or widely available enough.

A secondary reason is 
that higher level interface systems currently under development
seem not to be designed with interactive scientific graphics in mind.
Having our own low level system gives us the opportunity to adjust
both the set of basic operations provided to better fit the higher
level systems we want to build.
And we can optimize the implementations to give the necessary performance
in operations that are pretty basic for us but unusual
for general purpose window/mouse based user interfaces
(eg. rotating point clouds, grand tour \cite{Buja87a,Buja87b,Hurl87},
image painting \cite{McDo90a}).

\subsection{Design Goals}

Some of the design goals of Slate are:
\begin{itemize}

\item It should be robust against errors in the calls from a higher level
      graphics system.

\item It should be easy to port to a new window system,
      hardcopy device, or Lisp compiler.

\item It should permit high perfomance implementations.

\item Unless there's a good reason to do otherwise,
      the design should be as close to X as possible,
      because we expect the X port to be the most important.

\end{itemize}

\subsection{Slate Abstractions}
\label{slate-abstractions}

The basic abstraction in Slate is (surprise) the {\it slate}
(see section~\ref{slates}).
Slates are surfaces that can be drawn on and can receive input
of various kinds.
To make porting easy, the imaging model is fairly primitive;
Slates are essentially bitmaps of some finite depth.

At present the set of drawing operations
allows us to outline, fill, or tile simple geometric shapes
like points, line segments, or polygons,
draw characters and strings in a variety of fonts and colors,
and copy rectangular sets of pixels from one slate to another.
(In the near future, we expect to extend the set of drawing operations
to include support for multispectral images.
In the far future, we hope to have support for realistically rendered
3d images.)

The behavior of all the drawing operations in Slate 
can be described in the following way:
The drawing operation changes the values of a set of destination pixels
in the slate.
For each destination pixel, a source pixel value is calculated
and the destination pixel is set to some simple function 
(at present some boolean combination of the bits) of the source
and destination pixel values.

A typical drawing operation takes three types of arguments:
a {\it pen}, which is an abstraction that encapsulates
boolean operation, color, font, line style, etc. (see section~\ref{pens}),
a slate to draw on,
and a specification for the set of destination pixels.
Sets of destination pixels are specified
using the abstractions for discrete screen geometry
provided by the Geometry package (see \cite{McDo91j}).

Slate provides seperate abstractions for the more complex pen parameters,
like colors (section \ref{colors}),
boolean operations (section \ref{boolean-operations}),
fonts (section \ref{fonts}), 
line styles (section \ref{line-styles}), 
and tiling patterns (section \ref{tiles}).

There are two basic kinds of slates, standard, visible slates
and {\it invisible slates.}
All drawing operations work ``in the same way''
on both visible and invisible slates
An invisible slate serves roughly the same purpose as a pixmap in X.
It gives us a surface to draw on that can be copied rapidly
to multiple places on one or more visible slates.
We could do the same thing with only visible slates,
but we'd have to clutter the screen with little bitmaps 
that we don't really want the user to see.
(Note that for slightly technical reasons, a Tile is not the same thing
as an invisible slate. For details see section~\ref{tiles}.)

Another major abstraction in Slate in the {\it screen}.
The screen object captures information about the device a slate
is actually displayed on that isn't specific to any slate,
such as, the number of bits per pixel.
(We considered not having a screen object,
letting the device specific information be represented by class variables
of each instantiable slate class.
However, this got us into chicken and egg problems
where we needed to have some slate made in order to get at the
information needed to make a slate.
Moreover, having a screen object makes Slate more closely match the
abstractions in CLX.)
Every slate is on a particular screen, even invisible slates,
which is specified when the slate is made.
A slate's screen cannot be changed;
in other words, slates cannot be moved between screens.
Pixel regions can only be copied between slates on the same screen.
(This restriction might be relaxed in various ways in the future.)

What color the user sees for a given pixel value is determined
by the current {\it colormap} of the slate's screen.
For more on colormaps, see section~\ref{colormaps}.