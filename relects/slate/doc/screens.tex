%%% -*- Mode:Lisp; Syntax: Common-Lisp; -*-

\section{Screens}
\label{screens}

The reason for having Screen objects is to have a place 
to collect information
about the underlying display device
that is not specific to any Slate
and that can be used before any Slates are made.

\subsection{Getting a screen to use.}

The recommended way to get a screen to use is to call
{\sf default-screen} which will usually return the only
screen on the system.

For the rarer case where a system has more than one screen available,
one can call {\sf all-screens} to get a list of all the available screens.

\begin{verbatim}
(defun default-screen () (values Screen)) 

(defun all-screens () (values (List Screen)))
\end{verbatim}

\subsection{Screen class}

\begin{verbatim}
(defclass Screen (Standard-Object))
\end{verbatim}

\subsection{Screen dimensions}

\begin{verbatim}
(defgeneric screen-pixel-depth (screen)
  (declare (type Screen screen))
  (:documentation "How many bits per pixel?"))
\end{verbatim}

The {\sf screen-rect} is the area of the screen that's visible.
The {\sf screen-origin} is the point at the upper left corner of the screen.
The {\sf screen-extent} is a vector whose coordinates are the width and height
of the screen.
\begin{verbatim}
(defgeneric screen-rect ((screen Screen) &key result)
  (declare (type Screen screen)
           (type g:Screen-Rect result)
           (values result)))

(defgeneric screen-origin (screen &key result)
  (declare (type Screen screen)
           (type g:Screen-Point result)
           (values result))) 

(defgeneric screen-extent (screen &key result)
  (declare (type Screen screen)
           (type g:Screen-Vector result)
           (values result)))
\end{verbatim}

These functions give the conversion between screen coordinates 
and real world dimensions
(at least approximately). 
It's not yet decided if we should
restrict these to the best Fixnum approximation, or allow
Rationals, or even Floats.
Most of the implementations don't support this yet.

\begin{verbatim}

(defgeneric pixels-per-inch-x (screen)
  (declare (type Screen screen)
           (values Float)))

(defgeneric pixels-per-inch-y (screen)
  (declare (type Screen screen)
           (values Float)))

(defgeneric pixels-per-milimeter-x (screen)
  (declare (type Screen screen)
           (values Float)))

(defgeneric pixels-per-milimeter-y (screen)
  (declare (type Screen screen)
           (values Float)))

\end{verbatim}

\subsection{Text measurements}
\label{text-measurements}

The size and exact placement of text does not depend on 
the specific slate that it's drawn, but only on 
how the abstract specification of font, etc., is translated
for the screen the slate is on.
Therefore functions to determine the size and shape of regions
affected by drawing characters and strings are refered to a screen object
rather than a slate.
Another reason for making them functions of the screen rather than a slate
is so they can be used internally, before any slates are created.

To simplify matters a little, we require that character and string
size and placement depend only on the font (and the screen, of course)
and not on any other pen parameters. 
Therefore the text placement functions take a font argument
rather than a pen argument.
 
\begin{figure}
\begin{center}
\begin{minipage}{4.0in}
\begin{verbatim}
            .............
            ..----------.<-- y - ascent
            ..----------.
            ..-qqqqqq---.
            ..-q----q---.
baseline -->..-qqqqqq---.<-- y
            ..------q---.
            ..------q---.
            ..------q---.
            .............<-- y + descent
              |         |
              |         |
              x         x + width
\end{verbatim}
\end{minipage}
\end{center}
\caption{Placement of character drawn at {\sf (x,y)}.
Pixels in the {\sf character-screen-rect} are marked with a dash;
pixels outside are marked with period.}
\label{character-placement}
\end{figure}

Character placement is illustrated in figure~\ref{character-placement}.
(This discussion is based to some degree
on the way character and font metrics are handled in X11 
\cite{Nye88a,Sche90}.)
Suppose we draw the character {\sf q} at a point {\sf (x,y)} 
on some slate.
There are a number of functions that allow us to find out which pixels
will be affected.
{\sf character-screen-rect} returns a screen rect that 
contains all the pixels that are changed;
however, it is not necessarily the minimal such rect,
but rather a rect that is chosen so that drawing sequences of characters
will look nice if their rects are pasted together without any space between,
at least horizontally, if not vertically.

The {\sf character-screen-rect} of a character drawn at {\sf (x,y)}
is not placed, 
as one might naively expect,
so that the rect's origin (the upper left corner)
is at {\sf (x,y)}.
To make it easier to line up a sequence of characters,
it is drawn so that the left most column of the rect is at {\sf x}
and the {\it baseline} column is at {\sf y}.

\begin{verbatim}

(defun character-screen-rect (screen char font &key position result)
  (tools:type-check Screen screen)
  (tools:type-check Character char)
  (tools:type-check Font font)
  (tools:type-check g:Screen-Point position)
  (tools:type-check g:Screen-Rect result)
  (values result))

\end{verbatim}

We provide functions for computing {\sf character-width} 
and {\sf character-height}
for convenience and because it can often be done much more
efficently than first computing the {\sf character-screen-rect}
and then taking the {\sf g:screen-rect-width}
and/or 
\\
{\sf g:screen-rect-height}.

\begin{verbatim}

(defun character-width (screen char font)
  (tools:type-check Screen screen)
  (tools:type-check Character char)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun character-height (screen char font)
  (tools:type-check Screen screen)
  (tools:type-check Character char)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

\end{verbatim}

The position of the baseline can be determined from the 
{\sf character-ascent} using the fact that the 
origin of character's screen rect is {\sf (x, y - character-ascent).}

The {\sf character-descent} is defined as 
{\sf (character-height - character-ascent)};
we provide a seperate function for convenience 
and because it can sometimes be computed without first computing height
and ascent and subtracting.

\begin{verbatim}

(defun character-ascent (screen char font)
  (tools:type-check Screen screen)
  (tools:type-check Character char)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun character-descent (screen char font)
  (tools:type-check Screen screen)
  (tools:type-check Character char)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

\end{verbatim}

Conceptually, the behavior of {\sf string-screen-rect}, {\sf string-width},
and so on, can be derived from the analogous character functions
in the following way:
Paste together the screen rects for the characters so that the baselines
match, leaving no room between the rects.
{\sf string-screen-rect} is the minimal {\sf screen-rect} that contains
all the characters' {\sf screen-rect}s.
{\sf string-width} is the sum of the characters' widths.
{\sf string-ascent} is the maximum {\sf character-ascent},
{\sf string-descent} the maximum {\sf character-descent}.
{\sf string-height} is the sum of {\sf string-ascent} and {\sf string-descent},
which is equal to the height of the {\sf string-screen-rect},
(and which might be larger than the largest 
\\
{\sf character-height}).

\begin{verbatim}
(defun string-screen-rect (screen string font &key position result)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (tools:type-check g:Screen-Point position)
  (tools:type-check g:Screen-Rect result)
  (values result))

(defun string-width (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun string-height (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun string-ascent (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun string-descent (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))
\end{verbatim}

{\sf maximum-string-width} and {\sf maximum-string-height}
are useful for drawing a series of strings that, for example,
should be equally spaced
vertically and centered horizontally, as in a menu.

\begin{verbatim}

(defun maximum-string-width (screen strings font)
  (tools:type-check Screen screen)
  (tools:type-check List strings)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun maximum-string-height (screen strings font)
  (tools:type-check Screen screen)
  (tools:type-check List strings)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

\end{verbatim}

A vertical string's rect has the screen point at its upper left
corner, not the left baseline of the first character.
This is done to make it easier to draw a sequence of
vertical strings without overlapping.
{\sf vertical-string-ascent} is therefore always zero.
\\
{\sf vertical-string-descent} is the sum of 
the heights of all the characters.

\begin{verbatim}
(defun vertical-string-screen-rect (screen string font
                                          &key position result)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font )
  (tools:type-check g:Screen-Point position)  
  (tools:type-check g:Screen-Rect result)
  (values result))

(defun vertical-string-width (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun vertical-string-height (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun vertical-string-ascent (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun vertical-string-descent (screen string font)
  (tools:type-check Screen screen)
  (tools:type-check String string)
  (tools:type-check Font font)
  (values g:Screen-Coordinate)) 

(defun maximum-vertical-string-width (screen strings font)
  (tools:type-check Screen screen)
  (tools:type-check List strings)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

(defun maximum-vertical-string-height (screen strings font)
  (tools:type-check Screen screen)
  (tools:type-check List strings)
  (tools:type-check Font font)
  (values g:Screen-Coordinate))

\end{verbatim}

\subsection{Screen class hierarchy}

\begin{figure}
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(5.0,3.0)
\put(0,0){\special{psfile=screens.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
\caption {A class inheritance graph for the screen classes.
\label{screens-fig}}
\end{figure}

The screen class hierarchy
is corresponds roughly
to the colormap class hierarchy (see section~\ref{colormaps},
figures ~\ref{colormaps-fig} and ~\ref{screens-fig}).
Every screen has a {\sf screen-colormap} which is an instance of the
corresponding colormap class.

{\sf make-colormap-for-screen} makes a colormap of the appropriate type,
possibly using {\sf options} to initialize the map.

Screens can be classified into a 2 dimensional table of basic types
corresponding to the two-dimensional table of Colormap types
(see section~\ref{colormaps} and \cite{Nye88a}, ch. 7, tables 7-1 and  7-2).

One dimension of this table determines whether the colormap is writable.
This dimension of the table is represented in Slate
by the following two abstract classes:

\begin{verbatim}
Read-Only-Colormap-Screen
Read-Write-Colormap-Screen
\end{verbatim}

The other dimension of the table reflects how the colormap
translates pixel values into color.
Slate represents this dimension by the following 4 classes:

\begin{verbatim}

Monochrome-Screen
RGB-Screen
Single-Index-RGB-Screen
Decomposed-Index-RGB-Screen
\end{verbatim}

Considering all possible combinations of the above classes
(exlcuding {\sf RGB-Screen}) 
gives us 6 basic abstract screen classes:

\begin{verbatim}
Static-Gray-Screen
Static-Color-Screen
True-Color-Screen
Gray-Scale-Screen
Pseudo-Color-Screen
Direct-Color-Screen
\end{verbatim}


The 2 most common special cases of above:

\begin{verbatim}
B&W-Screen
Byte-Pseudo-Color-Screen
\end{verbatim}  

The instantiable screen classes are subclasses of the above,
specialized for lisp platforms.
The only screen classes avaialable in the release of Slate
require Franz Allegro Common Lisp release 4.x and CLX:
Examples are:
\begin{itemize}

\item X11 (Franz Allegro Common Lisp CLX)
\begin{verbatim}
CLX-Screen
B&W-CLX-Screen
RGB-CLX-Screen
\end{verbatim} 
\end{itemize}