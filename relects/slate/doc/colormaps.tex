\section{Colormaps}
\label{colormaps}

Colormaps are used for two approximately inverse purposes:
(1) to determine how a pixel value is mapped into color
and 
(2) to find the pixel value whose entry in the colormap 
best approximates a given color.

\subsection{Pixel Values}
\label{pixel-values}

{\sf Pixel-Value} is a type for pixel values,
that is, indexes into a color map.
It is a subtype of {\sf Integer}, eg: {\sf (Unsigned-Byte 32)}.

\subsection{Colormap Protocol}

Colormaps in Slate combine the functions of the Visual and 
Colormap in X. 

All colormap classes inherit from the abstract class {\sf Colormap}.

The basic protocol for {\sf Colormap}s includes a number of generic functions:
The {\sf pixel-depth} of a colormap 
is the maximum number of bits of a pixel value that the colormap will use.
{\sf legal-pixel-value?} is a more precise way to test if a pixel value
is within the range accepted by the colormap.
{\sf color-{\tt>}pixel-value} returns the pixel value whose entry best approximates
the given color description (in the sense of a given color metric).
{\sf pixel-value-{\tt>}color} returns the color at a given pixel value.
For writable colormaps (see section~\ref{colormap-classes}), 
{\sf setf} of {\sf pixel-value-{\tt>}color} can be used to change the
color associated with a given pixel value in writeable colormaps.
{\sf color-name-{\tt>}pixel-value} finds the pixel value whose color 
best approximates the named color. 

\subsection{Colormap Classes}
\label{colormap-classes}

\begin{figure}
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(5.0,3.0)
\put(0,0){\special{psfile=colormaps.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
\caption {A class inheritance graph for the colormap classes.
\label{colormaps-fig}}
\end{figure}

Colormaps can be classified into a 2 dimensional table of basic types
(see \cite{Nye88a,Sche90}, ch. 7, tables 7-1 and  7-2).

One dimension of this table determines whether the colormap is writable.
This dimension of the table is represented in Slate
by the two abstract classes: 
{\sf Read-Only-Colormap} and {\sf Read-Write-Colormap}.

The other dimension of the table reflects how the colormap
translates pixel values into color.
Slate represents this dimension by the following 4 classes:

A {\sf Monochrome-Colormap} is used for a display device
that translates a pixel value into a single intensity or gray level.
The colormap is so that $r=g=b$ for all the colors in the map.

An {\sf RGB-Colormap} is used for a display device that 
translates a pixel value into 3 separate RGB intensities.
A {\sf Single-Index-RGB-Colormap}
corresponds to a display device 
that uses all bits of the pixel value as the same index 
into red, green, and blue intensity tables.
A {\sf Decomposed-Index-RGB-Colormap} corresponds to a device
that splits the pixel value's bits into 3 fields
(eg. three 8 bit fields fromn a 24 bit pixel value)
that are used as three independent indexes into the red, green, and blue
intensity tables.

Considering all possible combinations of the above classes
(exlcuding {\sf RGB-Colormap}) 
gives us 6 basic abstract colormap classes:
{\sf Static-Gray-Colormap},
{\sf Static-Colormap},
{\sf True-Colormap},
{\sf Gray-Scale-Colormap},
{\sf Pseudo-Colormap},
and
{\sf Direct-Colormap}.

The most common instantiable case is a {\sf Static-Gray-Colormap} of depth 1
that is, a {\sf BW-Colormap}.
The second most common case is a {\sf Pseudo-Colormap} of depth 8, 
that is, a {\sf Byte-Pseudo-Colormap}.
To allow the same pixel values to be used with reasonably consistent
results on both {\sf Byte-Pseudo-Colormap}'s and {\sf BW-Colormap}'s,
a {\sf BW-Colormap} translates a zero pixel value to the
background color (often white) 
and anything else to the foreground color (often black). 


