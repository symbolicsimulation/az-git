;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Flush output buffers 
;;;============================================================

;;; like {\sf finish-output} on a stream
(defgeneric %finish-drawing (slate)
	    (declare (type Slate slate)))

(defmethod %finish-drawing ((slate Slate))
  ;; default method does nothing
  nil)

(defun finish-drawing (slate)
  (az:declare-check (type Slate slate))
  (%finish-drawing slate))


;;; like {\sf force-output} on a stream
(defgeneric %force-drawing (slate)
	    (declare (type Slate slate)))

(defmethod %force-drawing ((slate Slate))
  ;; default method does nothing
  nil)

(defun force-drawing (slate)
  (az:declare-check (type Slate slate))
  (%force-drawing slate))

;;;============================================================
;;; Absolute line drawing
;;;============================================================
;;; {\sf %draw-line-xy} must be supplied by the instantiable slate.

(defgeneric %draw-line-xy (pen slate x0 y0 x1 y1)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate x0 y0 x1 y1)))

(defmethod %draw-line-xy (pen slate x0 y0 x1 y1)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate x0 y0 x1 y1))
  (error "No method for %draw-line-xy for ~a"
	 (list pen slate x0 y0 x1 y1)))

(defun draw-line-xy (pen slate x0 y0 x1 y1)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate x0 y0 x1 y1))
  (%draw-line-xy pen slate x0 y0 x1 y1))

;;;------------------------------------------------------------

(defgeneric %draw-line (pen slate p0 p1)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point p0 p1)))

(defmethod %draw-line (pen slate p0 p1)
  (%draw-line-xy
    pen slate
    (g::%screen-point-x p0) (g::%screen-point-y p0)
    (g::%screen-point-x p1) (g::%screen-point-y p1)))

(defun draw-line (pen slate p0 p1)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-point p0 p1))
  (%draw-line pen slate p0 p1))

;;;------------------------------------------------------------
;;; Draw several (unconnected) lines with a given pen on a given slate.
;;; This is different from draw-polyline!

(defgeneric %draw-lines (pen slate points0 points1)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point-List points0 points1)))

(defmethod %draw-lines (pen slate points0 points1)
  (mapc #'(lambda (p0 p1) (%draw-line pen slate p0 p1)) points0 points1))

(defun draw-lines (pen slate points0 points1) 
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Point-List points0 points1))
  (%draw-lines pen slate points0 points1))

;;;============================================================
;;; drawing points
;;;============================================================

(defgeneric %draw-point-xy (pen slate x y)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate x y)))

(defmethod %draw-point-xy (pen slate x y)
  (%draw-line-xy
   pen slate
   x y
   (+ x (line-style-thickness (the Line-Style (%pen-line-style pen)))) y))

(defun draw-point-xy (pen slate x y)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate x y))
  (%draw-point-xy pen slate x y))

;;;------------------------------------------------------------

(defgeneric %draw-point (pen slate point) 
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point point)))

(defmethod %draw-point (pen slate p)
  (%draw-point-xy pen slate (g::%screen-point-x p) (g::%screen-point-y p)))

(defun draw-point (pen slate point) 
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-point point))
  (%draw-point pen slate point))

;;;------------------------------------------------------------

(defgeneric %draw-points-xy (pen slate xys)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate-List xys)))

(defmethod %draw-points-xy (pen slate xys)
  (loop (when (null xys) (return))
	(%draw-point-xy pen slate (pop xys) (pop xys))))

(defgeneric %draw-points (pen slate point-list)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-point-List point-list)))

(defmethod %draw-points (pen slate point-list)
  (dolist (p point-list) (%draw-point pen slate p)))

(defun draw-points-xy (pen slate xy-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate-List xy-list))
  (%draw-points-xy pen slate xy-list))

(defun draw-points (pen slate point-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Point-List point-list))
  (%draw-points pen slate point-list))

;;;============================================================
;;; Arrows
;;;============================================================

(defgeneric %draw-arrow-xy (pen slate x0 y0 x1 y1)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate x0 y0 x1 y1)))

(defmethod %draw-arrow-xy (pen slate x0 y0 x1 y1)
  (unless (and (= x0 x1) (= y0 y1))
    (let* ((line-style (%pen-line-style pen))
	   (dx (- x1 x0)) 
	   (dy (- y1 y0))
	   (theta (atan dy dx))
	   (alpha (- pi (line-style-arrowhead-angle line-style)))
	   (beta0 (- theta alpha))
	   (beta1 (+ theta alpha))
	   (l (line-style-arrowhead-length line-style))
	   (x2 (round (+ x1 (* l (cos beta0)))))
	   (y2 (round (+ y1 (* l (sin beta0)))))
	   (x3 (round (+ x1 (* l (cos beta1)))))
	   (y3 (round (+ y1 (* l (sin beta1))))))
      ;; arrow shaft
      (%draw-line-xy pen slate x0 y0 x1 y1)
      ;; arrow head
      (%draw-line-xy pen slate x1 y1 x2 y2)
      (%draw-line-xy pen slate x1 y1 x3 y3)
      ;;(%draw-line-xy pen slate x2 y2 x3 y3)
      )))

(defgeneric %draw-arrow (pen slate p0 p1)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point p0 p1))) 

(defmethod %draw-arrow (pen slate p0 p1)
  (%draw-arrow-xy pen slate 
		  (g::%screen-point-x p0) (g::%screen-point-y p0)
		  (g::%screen-point-x p1) (g::%screen-point-y p1)))

(defun draw-arrow-xy (pen slate x0 y0 x1 y1)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate x0 y0 x1 y1))
  (%draw-arrow-xy pen slate x0 y0 x1 y1))

(defun draw-arrow (pen slate p0 p1)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-point p0 p1))
  (%draw-arrow pen slate p0 p1))

;;;------------------------------------------------------------

(defgeneric %draw-arrows (pen slate points0 points1)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point-List points0 points1)))

(defmethod %draw-arrows (pen slate points0 points1)
  (mapc #'(lambda (p0 p1) (%draw-arrow pen slate p0 p1)) points0 points1))

(defun draw-arrows (pen slate points0 points1) 
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Point-List points0 points1))
  (%draw-arrows pen slate points0 points1))

;;;============================================================
;;; Polylines (Piecewise Linear Curves)
;;;============================================================

(defgeneric %draw-polyline-xy (pen slate xy-list)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate-List xy-list)))

(defmethod %draw-polyline-xy (pen slate xy-list)
  (let* ((xys xy-list)
	 (x0 (pop xys))
	 (y0 (pop xys)))
    (loop
      (when (null xys) (return))
      (let ((x1 (pop xys))
	    (y1 (pop xys)))
	(%draw-line-xy pen slate x0 y0 x1 y1)
	(setf x0 x1)
	(setf y0 y1)))))

(defun draw-polyline-xy (pen slate xy-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate-List xy-list))
  (%draw-polyline-xy pen slate xy-list))

;;;------------------------------------------------------------

(defgeneric %draw-polyline (pen slate point-list)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point-List point-list)))

(defmethod %draw-polyline (pen slate point-list)
  (let ((p0 (first point-list)))
    (dolist (p1 (rest point-list))
      (%draw-line pen slate p0 p1)
      (setf p0 p1))))

(defun draw-polyline (pen slate point-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Point-List point-list))
  (%draw-polyline pen slate point-list))

;;;------------------------------------------------------------

(defgeneric %draw-polylines (pen slate point-list-list)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type List point-list-list)))

(defmethod %draw-polylines (pen slate point-list-list)
  (dolist (point-list point-list-list)
    (draw-polyline pen slate point-list)))

(defun draw-polylines (pen slate point-list-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type List point-list-list))
  (%draw-polylines pen slate point-list-list))

;;;============================================================ 
;;; copy-slate-rect (aka bitblt) operations
;;;============================================================

(defgeneric %copy-slate-rect-xy (pen
				  from-slate
				  from-left from-top from-width from-height
				  to-slate to-left to-top)
  (declare (type Pen pen)
	   (type Slate from-slate to-slate)
	   (type g:Screen-Coordinate from-left from-top to-left to-top)
	   (type g:Positive-Screen-Coordinate from-width from-height))) 

;(defmethod %copy-slate-rect-xy (pen
;				  from-slate
;				  from-left from-top from-width from-height
;				  to-slate to-left to-top)
;  (multiple-value-bind (new-left new-top new-width new-height)
;      (g::clip-screen-rect-specs to-left to-top from-width from-height
;			  (slate-clipping-rect to-slate))
;    (cond ((or (>= 0 new-width) (>= 0 new-height))
;           ;; Gross clipping
;           nil)
;          (t  ;; Adjusted copy
;           ;; first, if clipping moved left-top pt, move from-left&top
;           (unless (= new-left to-left)
;	     (incf from-left (- new-left to-left)))
;           (unless (= new-top to-top)
;	     (incf from-top (- new-top to-top)))
;           (%copy-preclipped-slate-rect-xy
;	     pen from-slate from-left from-top new-width new-height
;	     to-slate new-left new-top)))))

(defun copy-slate-rect-xy (pen
			   from-slate
			   from-left from-top from-width from-height
			   to-slate to-left to-top)
  (az:declare-check (type Pen pen)
		    (type Slate from-slate to-slate)
		    (type g:Screen-Coordinate
			  from-left from-top to-left to-top)
		    (type g:Positive-Screen-Coordinate from-width from-height))
  ;; {\sf from-slate} and {\sf to-slate} need to be made for the same screen.
  (assert (eql (slate-screen from-slate) (slate-screen to-slate)))
  (%copy-slate-rect-xy
   pen from-slate from-left from-top from-width from-height
   to-slate to-left to-top))

;;;------------------------------------------------------------

(defgeneric %copy-slate-rect (pen from-slate from-rect to-slate to-point)
  (declare (type Pen pen)
	   (type Slate from-slate to-slate)
	   (type g:Screen-Rect from-rect)
	   (type g:Screen-Point to-point)))

(defmethod %copy-slate-rect (pen from-slate from-rect to-slate to-point)
  (%copy-slate-rect-xy
    pen from-slate
    (g::%screen-rect-left from-rect)
    (g::%screen-rect-top from-rect)
    (g::%screen-rect-width from-rect)
    (g::%screen-rect-height from-rect)
    to-slate
    (g::%screen-point-x to-point)
    (g::%screen-point-y to-point)))
  
(defun copy-slate-rect (pen from-slate from-rect to-slate to-point)
  (az:declare-check (type Pen pen)
		    (type Slate from-slate to-slate)
		    (type g:Screen-Rect from-rect)
		    (type g:Screen-Point to-point))
  ;; {\sf from-slate} and {\sf to-slate} need to be made for the same screen.
  (assert (eql (slate-screen from-slate) (slate-screen to-slate)))
  (%copy-slate-rect pen from-slate from-rect to-slate to-point))

;;;------------------------------------------------------------

(defgeneric %copy-slate-rects (pen from-slate from-rects to-slate to-points)
  (declare (type Pen pen)
	   (type Slate from-slate to-slate)
	   (type g:Screen-Rect-List from-rects)
	   (type g:Screen-Point-List to-points)))

(defmethod %copy-slate-rects (pen from-slate from-rects to-slate to-points)
  (mapc #'(lambda (from-rect to-point)
	    (%copy-slate-rect pen from-slate from-rect to-slate to-point))
	from-rects to-points))
  
(defun copy-slate-rects (pen from-slate from-rects to-slate to-points)
  (az:declare-check (type Pen pen)
		    (type Slate from-slate to-slate)
		    (type g:Screen-Rect-List from-rects)
		    (type g:Screen-Point-List to-points))
  (%copy-slate-rects pen from-slate from-rects to-slate to-points))

;;;============================================================
;;; drawing screen-rects
;;;============================================================

(defgeneric %draw-rect-xy (pen slate left top width height)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate left top)
	   (type g:Positive-Screen-Coordinate width height)))

(defmethod %draw-rect-xy (pen slate left top width height)
  ;; This doesn't handle filling or tiling.
  (multiple-value-bind (new-l new-t new-width new-height)
      (g::clip-screen-rect-specs left top width height
				(slate-clipping-region slate))
    (let* ((right (+ left width))
           (bottom (+ top height))
           (new-r (+ new-l new-width))
           (new-b (+ new-t new-height))
	   (thickness
	    (line-style-thickness (the Line-Style (%pen-line-style pen))))
           ;; Adjust left,right,top,bottom to draw non-zero
           ;; (pen-thickness pen) line so it is entirely within the
           ;; clipping screen-rect
           (low-adj (floor thickness 2))
           (high-adj (ceiling thickness 2))
           (-l (+ new-l low-adj))
           (-r (- new-r high-adj))
           (-t (+ new-t low-adj))
           (-b (- new-b high-adj)))
      ;; do vertical lines, if width>=(pen-thickness pen)
      (when (<= thickness new-width)
        (when (<= new-l left) (%draw-line-xy pen slate -l -t -l -b))
        (when (>= new-r right) (%draw-line-xy pen slate -r -t -r -b)))
      ;; do horizontal lines, if height>=(pen-thickness pen)
      (when (<= thickness new-height)
        (when (<= new-t top) (%draw-line-xy pen slate -l -t -r -t))
        (when (>= new-b bottom) (%draw-line-xy pen slate -l -b -r -b))))))

(defun draw-rect-xy (pen slate left top width height)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate left top)
		    (type g:Positive-Screen-Coordinate width height))
  (%draw-rect-xy pen slate left top width height))

;;;------------------------------------------------------------

(defgeneric %draw-rect (pen slate rect)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Rect rect)))

(defmethod %draw-rect (pen slate rect)
  (%draw-rect-xy
    pen slate
    (g::%screen-rect-left rect) (g::%screen-rect-top rect)
    (g::%screen-rect-width rect) (g::%screen-rect-height rect)))

(defun draw-rect (pen slate rect)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Rect rect))
  (%draw-rect pen slate rect)) 

;;;------------------------------------------------------------

(defgeneric %draw-rects (pen slate rects)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Rect-List rects)))

(defmethod %draw-rects (pen slate rects)
  (dolist (r rects) (%draw-rect pen slate r)))

(defun draw-rects (pen slate rects)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Rect-List rects))
  (%draw-rects pen slate rects))

;;;------------------------------------------------------------

(defgeneric clear-slate (slate &key screen-rect)
  (declare (type Slate slate)
	   (type g:Screen-Rect screen-rect))) 

;;;============================================================
;;; Ellipses
;;;============================================================

(defgeneric %draw-ellipse-xy (pen slate left top width height) 
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate left top)
	   (type g:Positive-Screen-Coordinate width height))) 

(defun draw-ellipse-xy (pen slate left top width height)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate left top)
		    (type g:Positive-Screen-Coordinate width height))
  (%draw-ellipse-xy pen slate left top width height))

;;;------------------------------------------------------------

(defgeneric %draw-ellipse (pen slate rect)
  (declare (type Pen pen)
	   (type Slate slate)
	   (g:Screen-Rect rect)))

(defmethod %draw-ellipse (pen slate rect)
  (%draw-ellipse-xy pen slate
		   (g::%screen-rect-left rect) (g::%screen-rect-top rect)
		   (g::%screen-rect-width rect) (g::%screen-rect-height rect)))

(defun draw-ellipse (pen slate rect)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Rect rect))
  (%draw-ellipse pen slate rect))

;;;------------------------------------------------------------

(defgeneric %draw-ellipses (pen slate rects)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Rect-List rects)))

(defmethod %draw-ellipses (pen slate rects)
  (dolist (r rects) (%draw-ellipse pen slate r)))

(defun draw-ellipses (pen slate rects)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Rect-List rects))
  (%draw-ellipse pen slate rects))

;;;============================================================
;;; Circles
;;;============================================================

(defgeneric %draw-circle-xy (pen slate x y r)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate x y)
	   (type g:Positive-Screen-Coordinate r)))

(defmethod %draw-circle-xy (pen slate x y r)
  (let ((diameter (* 2 r)))
    (%draw-ellipse-xy pen slate (- x r) (- y r) diameter diameter)))

(defun draw-circle-xy (pen slate x y r)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate x y)
		    (type g:Positive-Screen-Coordinate r))
  (%draw-circle-xy pen slate x y r))

;;;------------------------------------------------------------

(defgeneric %draw-circle (pen slate point r)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point point)
	   (type g:Positive-Screen-Coordinate r)))

(defmethod %draw-circle (pen slate point r)
  (%draw-circle-xy pen slate
		   (g::%screen-point-x point) (g::%screen-point-y point) r))

(defun draw-circle (pen slate point r)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Point point)
		    (type g:Positive-Screen-Coordinate r))
  (%draw-circle pen slate point r))

;;;------------------------------------------------------------

(defgeneric %draw-circles (pen slate points rs)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Point-List points)
	   (type g:Positive-Screen-Coordinate-List rs)))

(defmethod %draw-circles (pen slate points rs)
  (mapc #'(lambda (p r) (%draw-circle pen slate p r)) points rs))

(defun draw-circles (pen slate points rs)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Point-List points)
		    (type g:Positive-Screen-Coordinate-List rs))
  (%draw-circles pen slate points rs))

;;;============================================================
;;; Polygons
;;;============================================================

(defgeneric %draw-polygon-xy (pen slate xy-list)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type g:Screen-Coordinate-List xy-list)))

(defmethod %draw-polygon-xy (pen slate xys)
  (let* ((first-x (pop xys))
	 (first-y (pop xys))
	 (x0 first-x)
	 (y0 first-y))
    (loop
      (when (null xys) (return))
      (let ((x1 (pop xys))
	    (y1 (pop xys)))
	(%draw-line-xy pen slate x0 y0 x1 y1)
	(setf x0 x1)
	(setf y0 y1)))
    (%draw-line-xy pen slate x0 y0 first-x first-y)))

(defun draw-polygon-xy (pen slate xy-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-Coordinate-List xy-list))
  (%draw-polygon-xy pen slate xy-list)) 

;;;------------------------------------------------------------

(defgeneric %draw-polygon (pen slate point-list)
	    (declare (type Pen pen)
		     (type Slate slate)
		     (type g:Screen-point-List point-list)))

(defmethod %draw-polygon (pen slate point-list)
  (let ((p0 (first point-list)))
    (dolist (p1 (rest point-list))
      (%draw-line pen slate p0 p1)
      (setf p0 p1))
    (%draw-line pen slate p0 (first point-list))))

(defun draw-polygon (pen slate point-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type g:Screen-point-List point-list))
  (%draw-polygon pen slate point-list))

;;;------------------------------------------------------------

(defgeneric %draw-polygons (pen slate point-list-list)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type List point-list-list)))

(defmethod %draw-polygons (pen slate point-list-list)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type List point-list-list))(dolist (p-list point-list-list)
	   (%draw-polygon pen slate p-list)))

(defun draw-polygons (pen slate point-list-list)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type List point-list-list))
  (%draw-polygons pen slate point-list-list))

;;;============================================================
;;; Drawing Characters
;;;============================================================

(defgeneric %draw-character (pen slate char p)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type Character char)
	   (type g:Screen-Point p)))

(defun draw-character (pen slate char p)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type Character char)
		    (type g:Screen-Point p))
  (%draw-character pen slate char p))

;;;------------------------------------------------------------

(defun Character-List? (l)
  (and (listp l) (every #'characterp l)))

(deftype Character-List () `(satisfies Character-List?))

(defgeneric %draw-characters (pen slate chars points)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type Character-List chars)
	   (type g:Screen-Point-List points)))

(defmethod %draw-characters (pen slate chars points)
  (mapc #'(lambda (char point) (%draw-character pen slate char point))
	chars points))

(defun draw-characters (pen slate chars points)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type Character-List chars)
		    (type g:Screen-Point-List points))
  (%draw-characters pen slate chars points))

;;;============================================================
;;; Drawing Strings
;;;============================================================

(defgeneric %draw-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point))) 

(defmethod %draw-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point))
  (g::%with-borrowed-screen-point
   (p :x (g::%screen-point-x point)
      :y (g::%screen-point-y point)) 
   (dotimes (i (length string))
     (%draw-character pen slate (char string i) p)
     (incf (g::%screen-point-x p)
	   (%character-width
	    (slate-screen slate) (char string i) (%pen-font pen))))))

(defun draw-string (pen slate string point)

  "The text is drawn with the left baseline starting at {\sf point}.
When the drawing is done, the {\sf slate-point} of the slate is at the
baseline and the first pixel to the right of the last character."

  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type String string)
		    (type g:Screen-Point point))
  (%draw-string pen slate string point))

;;;============================================================
;;; Drawing Centered Strings
;;;============================================================

(defgeneric %draw-centered-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point))) 

(defmethod %draw-centered-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point))
  (multiple-value-bind
      (w a d)
      (%analyze-string-placement (slate-screen slate) string (%pen-font pen))
    (g::%with-borrowed-screen-point
     (p :x (- (g::%screen-point-x point) (truncate w 2))
	:y (+ (g::%screen-point-y point) (truncate (- a d) 2)))
     (%draw-string pen slate string p))))

(defun draw-centered-string (pen slate string point)
  "The text is drawn with the center of the string's screen rect at
{\sf point}. When the drawing is done, the {\sf slate-point}
of the slate is {\sf point}."
(az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type String string)
		    (type g:Screen-Point point))
  (%draw-centered-string pen slate string point))

;;;============================================================
;;; Drawing Strings relative to upper left corner
;;;============================================================

(defgeneric %draw-cornered-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point))) 

(defmethod %draw-cornered-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point))
  (g::%with-borrowed-screen-point
   (p
    :x (g::%screen-point-x point)
    :y (+ (g::%screen-point-y point)
	  (%string-ascent (slate-screen slate) string (%pen-font pen))))
   (%draw-string pen slate string p)))

(defun draw-cornered-string (pen slate string point)

  "The text is drawn with the upper left corner of the string's screen
rect at {\sf point}. When the drawing is done, the {\sf slate-point}
of the slate is {\sf point}."

  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type String string)
		    (type g:Screen-Point point))
  (%draw-cornered-string pen slate string point))

;;;============================================================
;;; Drawing Vertical Strings
;;;============================================================
;;; The text is drawn with the point at the upper left hand corner, not
;;; the left baseline, aligned with {\sf point}. When the drawing is done,
;;; the {\sf slate-point} is just below the lower left corner of the last
;;; character's screen rect.

(defgeneric %draw-vertical-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point)))

(defmethod %draw-vertical-string (pen slate string point)
  (g::%with-borrowed-screen-point
   (p :x (g::%screen-point-x point)
      :y (g::%screen-point-y point))
   (let* ((screen (slate-screen slate))
	  (font (%pen-font pen))
	  (str-width (%vertical-string-width screen string font))
	  (init-x (g::%screen-point-x p)))
     (dotimes (i (length string))
       (let ((char (char string i)))
	 (multiple-value-bind
	     (w a d) (%analyze-character-placement screen char font)
	   ;; except for first char, move down by this char's ascent
	   (incf (g::%screen-point-y p) a)
	   ;; center char within vertical column
	   (setf (g::%screen-point-x p)
	     (+ init-x (truncate (- str-width w) 2)))
	   (%draw-character pen slate char p)
	   ;; move down by this char's descent
	   (incf (g::%screen-point-y p) d))))
     (setf (g::%screen-point-x p) init-x))))

(defun draw-vertical-string (pen slate string point)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type String string)
		    (type g:Screen-Point point))
  (%draw-vertical-string pen slate string point))

;;;============================================================
;;; Drawing Centered Vertical Strings
;;;============================================================
;;; The text is drawn with the center of the string's screen rect at
;;; {\sf point}. When the drawing is done, the {\sf slate-point} of
;;; the slate is {\sf point}.

(defgeneric %draw-centered-vertical-string (pen slate string point)
  (declare (type Pen pen)
	   (type Slate slate)
	   (type String string)
	   (type g:Screen-Point point))) 

(defmethod %draw-centered-vertical-string (pen slate string point)
  (multiple-value-bind
    (w a h)
      (%analyze-vertical-string-placement
	(slate-screen slate) string (%pen-font pen))
    (declare (ignore a))
    (g::%with-borrowed-screen-point (p :x (- (g::%screen-point-x point)
					     (truncate w 2))
				       :y (- (g::%screen-point-y point)
					     (truncate h 2)))
      (%draw-vertical-string pen slate string p))))

(defun draw-centered-vertical-string (pen slate string point)
  (az:declare-check (type Pen pen)
		    (type Slate slate)
		    (type String string)
		    (type g:Screen-Point point))
  (%draw-centered-vertical-string pen slate string point))

