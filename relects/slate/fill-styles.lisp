;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================

(defstruct (Fill-Style 
	    (:conc-name nil)
	    (:constructor make-fill-style)
	    (:copier %%copy-fill-style))
  (fill-style-type
   :outline
   :type (Member :outline :solid :tiled)
   ;; (Member :outline :solid :tiled :stippled :opaque-stippled)
   )
  (fill-style-rule
   :even-odd
   :type (Member :even-odd :winding))
  (fill-style-tile nil :type (or Null Tile))
  (fill-style-stipple nil :type Null)) 

(defun fill-style? (x) (typep x 'Fill-Style))

(defun copy-fill-style (fs &key (result (make-fill-style)))
  (setf (fill-style-type result) (fill-style-type fs))
  (setf (fill-style-rule result) (fill-style-rule fs))
  (unless (null (fill-style-tile fs))
    (setf (fill-style-tile result) (fill-style-tile fs)))
  #||
  (unless (null (fill-style-stipple fs))
    (setf (fill-style-stipple result) (fill-style-stipple fs)))
  ||#
  result)

(defun equal-fill-styles? (fs0 fs1)
  (or (eql fs0 fs1)
      (and (eql (fill-style-type fs0) (fill-style-type fs1))
           (eql (fill-style-rule fs0) (fill-style-rule fs1))
           (eql (fill-style-tile fs0) (fill-style-tile fs1))
           (eql (fill-style-stipple fs0) (fill-style-stipple fs1)))))