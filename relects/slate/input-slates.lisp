;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; A Class for Slates that accept input
;;;============================================================

(defclass Input-Slate (Slate) ())

;;;============================================================
;;; pointer and keyboard queries
;;;============================================================

(defgeneric selected? (slate)
  (declare (type Input-Slate slate)))

(defgeneric mouse-down? (slate)
  (declare (type Input-Slate slate)))

(defgeneric mouse-up? (slate)
  (declare (type Input-Slate slate))) 

(defmethod mouse-up? (slate)
  (az:declare-check (type Input-Slate slate))
  (not (mouse-down? slate)))

(defgeneric mouse-screen-point (slate &key result)
  (declare (type Input-Slate slate)
	   (type g:Screen-point result)))

;;;============================================================

(defparameter *modifier-keynames*
    '(:shift :control :meta :hyper :caps-lock))

(defun keyname? (x) (or (characterp x)
			(member x *modifier-keynames*)))

(deftype Keyname () '(satisfies keyname?))

(defgeneric key-down? (slate keyname)
  (declare (type Input-Slate slate)
	   (type Keyname keyname))) ;; need a keyname type

(defgeneric key-up? (slate keyname)
  (declare (type Input-Slate slate)
	   (type Keyname keyname))) 

(defmethod key-up? (slate keyname)
  (az:declare-check (type Input-Slate slate)
		    (type Keyname keyname))
  (not (key-down? slate keyname)))



