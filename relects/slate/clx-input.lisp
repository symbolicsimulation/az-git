;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Slate mouse I/O
;;;============================================================

(defmethod selected? ((slate CLX-Slate)) t)

(defmethod mouse-down? ((slate CLX-Slate))
    (get-button-state slate :button-1))

(defun get-button-state (slate button)
    (multiple-value-bind (x y same-screen-p child mask root-x root-y root)
	(xlib:query-pointer (slate-drawable slate))
      (declare (ignore x y same-screen-p child root-x root-y root))
      (logtest mask (xlib::make-state-mask button))))

(defmethod mouse-screen-point ((slate CLX-Slate)
			       &key
			       (result (g::%make-screen-point 0 0)))
  (az:declare-check (type g:Screen-Point result))
  (multiple-value-bind (x y same-screen-p)
      (xlib:pointer-position (slate-drawable slate))
    (declare (ignore same-screen-p))
    (g::set-screen-point-coords result :x x :y y)    
    result))

(defun get-xkey-state (slate char)
    (let* ((display (xdisplay (slate-screen slate)))
	   (keymap (xlib:query-keymap display)))
      (xlib:character-in-map-p display char keymap)))

(defmethod key-down? ((slate CLX-Slate) keyname)
    (az:declare-check (type (or Character Keyword) keyname))
    (cond ((characterp keyname)
	   (get-xkey-state slate keyname))
	  ((find keyname '(:shift :lock :control :meta))
	   (get-button-state slate keyname))
	  ((eql keyname :meta)
	   (get-button-state slate :mod-1))
	  (t nil)))

;;;------------------------------------------------------------
;;; Prints out a string, and allows the user to type in a string in
;;; response.
#||
(defmethod input-string (pen (slate CLX-Slate) prompt)
  (let* ((main-w (slate-drawable slate))
	 (screen (slate-screen slate))
	 (xscreen (xscreen screen))
	 (font (pen-font pen))
	 (xdisplay (xdisplay screen))
	 (prompt-string (format nil "  ~A  " prompt))
	 (answer-string "")
	 boxwidth boxheight line-height line-ascent
	 w gc )
    (multiple-value-bind (width maxascent maxdescent)
	(get-xmetrics pen slate prompt-string)
      (setf boxwidth (round (max width 300)))
      (setf line-height (max (+ maxascent maxdescent)
			     (string-height
			      (slate-screen slate) "M" font)))
      (setf line-ascent maxascent)
      (setf boxheight (+ line-height line-height)))

    (setf w (xlib:create-window
	     :parent (xlib:screen-root xscreen)
	     :background (xlib:screen-white-pixel xscreen)
	     ;;:foreground (xlib:screen-black-pixel xscreen)
	     :border (xlib:screen-black-pixel xscreen)
	     :border-width 1
	     :colormap (xcolormap screen)
	     :bit-gravity :north-west
	     :x (xlib:drawable-x main-w)
	     :y (xlib:drawable-y main-w)
	     :width boxwidth
	     :height boxheight
	     :cursor (arrow-cursor screen)
	     :backing-store :not-useful
	     :event-mask '(:exposure :button-press :button-release)))
    (setf gc (xlib:create-gcontext
	      :drawable w
	      :exposures :off
	      :background (xlib:screen-white-pixel xscreen)
	      :foreground (xlib:screen-black-pixel xscreen)
	      :cache-p t))
    (update-gcontext-from-pen pen screen gc)

    ;; set window hints, so menu will appear without prompting,
    ;; at current position of mouse
    (setf (xlib:wm-normal-hints w)
      (multiple-value-bind (root-x root-y root)
	  (xlib:global-pointer-position xdisplay)
	(declare (ignore root))
	(xlib:make-wm-size-hints
	 :x root-x
	 :y root-y
	 :width boxwidth
	 :height boxheight)))

    ;; map the window
    (xlib:map-window w)

    (unwind-protect
	(block event-loop
	  (labels ((display-strings ()
		     (xlib:clear-area w)
		     (xlib:draw-glyphs w gc
				       0 line-ascent
				       prompt-string)
		     (xlib:draw-glyphs
		      w gc
		      (let* ((answer-space 10)
			     (answer-width
			      (+ answer-space
				 (string-width
				  (slate-screen slate) 
				  answer-string
				  font))))
			(+ answer-space
			   (cond ((> answer-width boxwidth)
				  (- boxwidth answer-width))
				 (t 0))))
		      (+ line-ascent line-height)
		      answer-string)
		     (xlib:display-finish-output xdisplay)))
	    (loop
	      (xlib:event-case
	       (xdisplay :force-output-p t :discard-p t)
	       (exposure ;; handle exposure events
		(count)
		(when (zerop count)
		  ;; Ignore all but the last exposure event
		  (display-strings))
		nil)
	       (key-press
		(code state)
		(let ((c (xlib:keycode->character xdisplay code state)))
		  (setf answer-string
		    (cond
		     ((eql c #\rubout)
		      (subseq answer-string
			      0 (max 0 (1- (length answer-string)))))
		     ((eql c #\return)
		      (return-from event-loop answer-string))
		     (t (concatenate 'string answer-string
				     (string c)))))
		  (display-strings))
		nil)
	       (destroy-notify ;; handle destroy events
		()
		(setf answer-string "")
		(return-from event-loop ""))))))
      (progn (xlib:destroy-window w)
	     (xlib:display-finish-output xdisplay)))
    answer-string
    ))
||#
