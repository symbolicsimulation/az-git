;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================

(deftype Pixel-Value ()
  "A type for pixel values, that is, indexes into a color map."
  '(Unsigned-Byte 32))

(defparameter *all-pixel-planes-mask* #xffffffff)

;;;============================================================
;;; Colormaps

(defclass Colormap (Slate-Object) ()
  (:documentation
   "A root class for all color maps."))

;;;------------------------------------------------------------

(defgeneric pixel-depth (colormap)
	    (declare (type Colormap colormap))
	    (:documentation
	     "The <pixel-depth> is the maximum number of bits of a pixel value
that a colormap will use."))

(defgeneric legal-pixel-value? (colormap pixel-value)
	    (declare (type Colormap colormap)
		     (type Pixel-Value pixel-value))
	    (:documentation
	     "<legal-pixel-value?> is a precise way to test if a pixel value
is within the range accepted by a colormap."))

(defgeneric colormap-length (colormap)
	    (declare (type Colormap colormap)
		     (:returns (type az:Array-Index)))
	    (:documentation
	     "The <colormap-length> is the number of entries in the map."))

(defmethod colormap-length ((colormap Colormap))
  (ash 1 (pixel-depth colormap)))

(defgeneric colormap-vector (colormap)
	    (declare (type Colormap colormap)
		     (:returns (type (Simple-Array Color (*)))))
	    (:documentation
	     "Returns a vector of the colors in the colormap, indexed
by pixel value. Modifying the returned array has unpredictable
results."))

;;;------------------------------------------------------------

(defgeneric make-color-metric (colormap &rest options)
	    (declare (type Colormap colormap))
	    (:documentation
	     "Makes a standard color metric for <colormap>."))

(defmethod make-color-metric ((colormap Colormap) &rest options)
  (declare (ignore options))
  'color-l1-dist)

;;;------------------------------------------------------------

(defgeneric color->pixel-value (colormap color &key metric &allow-other-keys)
	    (declare (type Colormap colormap)
		     (type Color color)
		     (type (or Symbol Function) metric))
	    (:documentation
	     "Returns the pixel value whose entry best approximates
the given color description (in the sense of <metric>).
(corresponds to XAllocColor)."))

(defgeneric pixel-value->color (colormap pixel-value &key result)
  (declare (type Colormap colormap)
	   (type Pixel-Value pixel-value)
	   (type Color result))
  (:documentation
   "Returns the color at a given pixel value."))

(defmethod rgb8->pixel-value ((colormap Colormap) r8 g8 b8
			      &key (metric 'color-l1-dist)
			      &allow-other-keys)
  (az:declare-check (type (Unsigned-Byte 8) r8 g8 b8))
  (with-borrowed-color (color)
    (color->pixel-value colormap (rgb8->color r8 g8 b8 :result color)
			:metric metric))) 

(defmethod rgb16->pixel-value ((colormap Colormap) r16 g16 b16
			       &key (metric 'color-l1-dist)
			       &allow-other-keys)
  (az:declare-check (type Color-Level r16 g16 b16))
  (with-borrowed-color (color)		       
    (color->pixel-value colormap (rgb16->color r16 g16 b16 :result color)
			:metric metric)))

(defmethod rgb->pixel-value ((colormap Colormap) r g b
			      &key (metric 'color-l1-dist)
			      &allow-other-keys)
  (assert (<= 0.0d0 r 1.0d0))
  (assert (<= 0.0d0 g 1.0d0))
  (assert (<= 0.0d0 b 1.0d0))
  (with-borrowed-color (color)
    (color->pixel-value colormap (rgb->color r g b :result color)
			:metric metric)))

#||
(defmethod ihs->pixel-value ((colormap Colormap) i h s
			      &key (metric 'color-l1-dist)
			      &allow-other-keys)
  (with-borrowed-color (color)
    (color->pixel-value colormap (ihs->color i h s :result color)
			:metric metric)))

(defmethod hexcone-ihs->pixel-value ((colormap Colormap) i h s
			      &key (metric 'color-l1-dist)
			      &allow-other-keys)
  (with-borrowed-color (color)
    (color->pixel-value colormap
			(hexcone-ihs->color i h s :result color)
			:metric metric)))

(defmethod yiq->pixel-value ((colormap Colormap) y i q
			      &key (metric 'color-l1-dist)
			      &allow-other-keys)
  (with-borrowed-color (color)
    (color->pixel-value colormap (yiq->color y i q :result color)
			:metric metric)))
||#
;;; XAllocNamedColor

(defgeneric colorname->pixel-value (colormap name
					 &key metric 
					 &allow-other-keys)
  (declare (type Colormap colormap)
	   (type Colorname name)
	   (type (or Symbol Function) metric)))

(defmethod colorname->pixel-value ((colormap Colormap) name
				    &key (metric 'color-l1-dist)
				    &allow-other-keys)
  (az:declare-check (type Colorname name))
  (with-borrowed-color (color)
    (color->pixel-value colormap
			(colorname->color name :result color)
			:metric metric)))

;;;============================================================
;;; 1st dimension of abstract colormap types:

(defclass Read-Only-Colormap (Colormap) ()
  (:documentation
   "<Read-Only-Colormap>s cannot be modified."))

(defclass Read-Write-Colormap (Colormap) ()
  (:documentation
   "<Read-Write-Colormap>s have writeable entries."))

(defgeneric (setf pixel-value->color) (new-color colormap pixel-value)
	    (declare (type Color new-color)
		     (type Read-Write-Colormap colormap)
		     (type Pixel-Value pixel-value))
	    (:documentation
	     "<setf> of <pixel-value->color> can be used to change the
color associated with a given pixel value in writeable colormaps."))

;;;============================================================
;;; 2nd dimension of abstract colormap types:

(defclass Monochrome-Colormap (Colormap) ()
  (:documentation
   "A Monochrome-Colormap is used for a display device that translates
a pixel value into a single intensity or gray level.  The colormap is
so that r=g=b for all the colors in the map."))

(defclass RGB-Colormap (Colormap) ()
  (:documentation
   "An RGB-Colormap is used for a display device that translates a
pixel value into 3 separate RGB intensities."))

(defclass Single-Index-RGB-Colormap (RGB-Colormap) ()
  (:documentation
   "A <Single-Index-RGB-Colormap> corresponds to a display device that
uses all bits of the pixel value as the same index into red, green,
and blue intensity tables."))

(defclass Decomposed-Index-RGB-Colormap (RGB-Colormap) ()
  (:documentation
   "A <Decomposed-Index-RGB-Colormap> corresponds to a device that
splits the pixel value's bits into 3 fields (eg. three 8 bit fields
fromn a 24 bit pixel value) that are used as three independent indexes
into the red, green, and blue intensity tables."))

;;; 6 basic colormap classes:

(defclass Static-Gray-Colormap (Read-Only-Colormap Monochrome-Colormap) ()
  (:documentation
   "A <Static-Gray-Colormap> is read-only and monochrome."))

;;; not used yet
(defclass Static-Colormap (Read-Only-Colormap
			   Single-Index-RGB-Colormap)
	  ()
  (:documentation
   "A <Static-Colormap> is read-only and uses a single index for look
up in red, green, and blue tables."))

(defclass True-Colormap (Read-Only-Colormap Decomposed-Index-RGB-Colormap)
	  ()
  (:documentation
   "A <True-Colormap> is read-only and uses three separate indexes for
look up in red, green, and blue tables."))

(defclass Gray-Scale-Colormap (Read-Write-Colormap Monochrome-Colormap)
	  ()
  (:documentation
   "A <Gray-Scale-Colormap> is a writeable monochrome colormap."))

(defclass Direct-Colormap (Read-Write-Colormap
			   Decomposed-Index-RGB-Colormap)
	  ()
  (:documentation
   "A <Direct-Colormap> uses three separate indexes for lookup in
writeable red, green, and blue tables."))

(defclass Pseudo-Colormap (Read-Write-Colormap Single-Index-RGB-Colormap)
	  ()
  (:documentation
   "A <Pseudo-Colormap> uses a single index for lookup in writeable
red, green, and blue tables."))

;;;============================================================ 

(defclass BW-Colormap (Static-Gray-Colormap)
	  ((colormap-black-pixel-value
	   :type (Member 0 1)
	   :reader colormap-black-pixel-value
	   :initarg :colormap-black-pixel-value))

  (:default-initargs :colormap-black-pixel-value 0)
  
  (:documentation
   "The most common case, a black and white screen, uses a
<Static-Gray-Colormap> of depth 1.  To allow the same pixel values to
be used with reasonably consistent results on both
<Byte-Pseudo-Colormap>s and <BW-Colormap>s, a <BW-Colormap> translates
a zero pixel value to the background color (often white) and anything
else to the foreground color (often black)."))

;;;------------------------------------------------------------

(defmethod pixel-depth ((colormap BW-Colormap)) 1)

(defmethod legal-pixel-value? ((colormap BW-Colormap) pixel-value)
  (typep pixel-value '(Integer 0 1)))

(defmethod colormap-vector ((colormap BW-Colormap))
  (if (zerop (colormap-black-pixel-value colormap))
      (vector *black-color* *white-color*)
    (vector *white-color* *black-color*)))

(defun colormap-white-pixel-value (colormap)
  (az:declare-check (type BW-Colormap colormap))
  (if (zerop (colormap-black-pixel-value colormap)) 1 0))

;;;------------------------------------------------------------

(defmethod make-color-metric ((colormap BW-Colormap)
			      &key
			      (background-color (colorname->color :white))
			      (black-on-white? t)
			      &allow-other-keys)
  (if black-on-white?
      #'(lambda (c0 c1)
	  (if (or (and (equal-colors? c0 background-color)
		       (equal-colors? c1 *white-color*))
		  (and (equal-colors? c1 background-color)
		       (equal-colors? c0 *white-color*))
		  (equal-colors? c0 *black-color*)
		  (equal-colors? c1 *black-color*))
	      0.0d0 1.0d0))
    ;; else
    #'(lambda (c0 c1)
	(if (or (and (equal-colors? c0 background-color)
		     (equal-colors? c1 *black-color*))
		(and (equal-colors? c1 background-color)
		     (equal-colors? c0 *black-color*))
		(equal-colors? c0 *white-color*)
		(equal-colors? c1 *white-color*))
	    0.0d0 1.0d0))))

;;;------------------------------------------------------------

(defmethod color->pixel-value ((colormap BW-Colormap) color
			       &key
			       (metric 'color-l1-dist)
			       &allow-other-keys)

  (az:declare-check (type Color color)
		    (type (or Symbol Function) metric))
  (if (< (funcall metric color *black-color*)
	 (funcall metric color *white-color*))
      (colormap-black-pixel-value colormap)
    ;; else
    (colormap-white-pixel-value colormap)))

(defmethod pixel-value->color ((colormap BW-Colormap) pixel-value
			       &key (result (%make-color)))

  (assert (legal-pixel-value? colormap pixel-value))
  (if (= pixel-value (colormap-black-pixel-value colormap))
      (copy-color *black-color* :result result)
    (copy-color *white-color* :result result)))

;;;============================================================
;;; 2nd most common case:

(defclass Byte-Pseudo-Colormap (Pseudo-Colormap)
	  ((colormap-vector
	    :type (Simple-Array Color (*))
	    :reader colormap-vector
	    :initform
	    (let ((colormap-vector (make-array 256)))
	      (dotimes (i 256)
		(setf (aref colormap-vector i)
		  (%make-color :r16 0 :g16 0 :b16 0)))
	      colormap-vector)))
  (:documentation
   "The second most common case is a {\sf Pseudo-Colormap} of depth 8."))

;;;------------------------------------------------------------

(defmethod pixel-depth ((colormap Byte-Pseudo-Colormap)) 8)

(defmethod legal-pixel-value? ((colormap Byte-Pseudo-Colormap) pixel-value)
  (typep pixel-value '(Integer 0 #xFF)))

;;;------------------------------------------------------------

(defmethod color->pixel-value ((colormap Byte-Pseudo-Colormap) color
			       &key
			       (metric '%color-l1-dist)
			       &allow-other-keys)
  (declare (optimize (safety 1) (speed 3)))
  (az:declare-check (type Color color))
  (let* ((colormap colormap)
	 (color color)
	 (colormap-vector (colormap-vector colormap))
	 (pixel-value 255)
	 (colormap-entry (aref colormap-vector pixel-value))
	 (min-dist (funcall metric color colormap-entry))
	 (dist 0))
    (declare (type Byte-Pseudo-Colormap colormap)
	     (type Color color colormap-entry)
	     (type (Simple-Array Color (*)) colormap-vector)
	     (type Pixel-Value pixel-value)
	     (type Fixnum min-dist dist))
    (dotimes (i 255)
      (declare (type Pixel-Value i))
      (setf colormap-entry (aref colormap-vector i))
      (setf dist (funcall metric color colormap-entry))
      (when (< dist min-dist)
	(setf min-dist dist)
	(setf pixel-value i)))
    pixel-value))

(defmethod pixel-value->color ((colormap Byte-Pseudo-Colormap) pixel-value
			       &key (result (%make-color)))
  (assert (legal-pixel-value? colormap pixel-value))
  (copy-color (aref (colormap-vector colormap) pixel-value)
	      :result result))

(defmethod (setf pixel-value->color) (new-color (colormap Byte-Pseudo-Colormap)
				      pixel-value)
  (az:declare-check (type Color new-color))
  (assert (legal-pixel-value? colormap pixel-value))
  (copy-color new-color 
	      :result (aref (colormap-vector colormap) pixel-value)))


;;;============================================================ 

(defclass True-24bit-Colormap (True-Colormap) ()
  
  (:documentation
   "The 3rd common case, a a 24bit true color screen, uses a
<True-24bit-Colormap> of depth 1."))

;;;------------------------------------------------------------

(defmethod pixel-depth ((colormap True-24bit-Colormap)) 24)

(defmethod legal-pixel-value? ((colormap True-24bit-Colormap) pixel-value)
  (typep pixel-value '(Integer 0 #xFFFFFF)))

(defmethod colormap-vector ((colormap True-24bit-Colormap))
  (warn "Colormap Vectors cannot be returned from True-24bit-Colormap")
  '#())

;;;------------------------------------------------------------

(defmethod color->pixel-value ((colormap True-24bit-Colormap) color
			       &key
			       (metric 'color-l1-dist)
			       &allow-other-keys)

  (az:declare-check (type Color color)
		    (type (or Symbol Function) metric))
  (multiple-value-bind (r g b) (color->rgb8 color)
    (declare (type (Unsigned-Byte 8) r g b))
    (logior (ash b 16) (ash g 8) r)))  

(defmethod pixel-value->color ((colormap True-24bit-Colormap) pixel-value
			       &key (result (%make-color)))
  (az:declare-check (type (Unsigned-Byte 24) pixel-value))
  (locally (declare (type (Unsigned-Byte 24) pixel-value))
    (let ((r      (logand pixel-value #x0000FF))
	  (g (ash (logand pixel-value #x00FF00) -8))
	  (b (ash (logand pixel-value #xFF0000) -16)))
      (rgb8->color r g b :result result))))





