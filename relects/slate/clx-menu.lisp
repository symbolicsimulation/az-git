;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; built-in menu and i/o facilities
;;;============================================================

(defun menu-window (slate width height)
  (let* ((screen (slate-screen slate))
	 (xscreen (xscreen screen)))

    (multiple-value-bind
	(x y same-screen-p child state-mask root-x root-y root)
	(xlib:query-pointer (slate-drawable slate))
      (declare (ignore same-screen-p child state-mask root))

      (g:with-borrowed-screen-rect
       (r :left x :top y :width width :height height)
       (g:keep-screen-rect-in r (slate-rect slate))
       (let ((w (xlib:create-window
		 :parent (xlib:screen-root xscreen)
		 :background (xlib:screen-white-pixel xscreen)
		 :border (xlib:screen-black-pixel xscreen)
		 :border-width 1
		 :colormap (xcolormap screen)
		 :bit-gravity :north-west
		 :x (+ (- root-x x 2) (g:screen-rect-xmin r))
		 :y (+ (- root-y y 2) (g:screen-rect-ymin r))
		 :width width
		 :height height
		 :cursor (arrow-cursor screen)
		 :backing-store :not-useful
		 :event-mask '(:exposure :button-press :button-release))))
	 (setf (xlib:wm-name w)
	   (concatenate 'String "Menu for " (string (slate-name slate))))
	 (setf (xlib:wm-icon-name w)
	   (concatenate 'String "Menu for " (string (slate-name slate))))

	 ;; set window hints, so menu will appear without prompting,
	 ;; at current point of mouse
	 (setf (xlib:wm-normal-hints w)
	   (xlib:make-wm-size-hints
	    :user-specified-size-p t
	    :user-specified-position-p t
	    :x root-x
	    :y root-y
	    :width width
	    :height height))

	 ;; map the window
	 (xlib:map-window w)
	 ;; wait until the window is actually on the screen before returning
	 (loop (when (eq (xlib:window-map-state w) :viewable) (return)))
	 w)))))

(defun menu-specs (menu-list screen font)
  (let ((maxwidth 0)
	(last-y 0)
	(new-y 0)
	item-string
	menu-specs)
    (dolist (item menu-list)
      (setf item-string (format nil "~A" (first item)))
      (multiple-value-bind (width maxascent maxdescent)
	  (analyze-string-placement screen item-string font)
	(setf maxwidth (max maxwidth width))
	(setf new-y (+ maxascent maxdescent last-y))
	(setf menu-specs
	  (cons (list item-string last-y new-y (+ last-y maxascent) item)
		menu-specs))
	(setf last-y new-y)))
    (values (reverse menu-specs) (+ maxwidth 10) last-y)))

(defun menu-button-state (w button)
    (multiple-value-bind (x y same-screen-p child mask root-x root-y root)
	(xlib:query-pointer w)
      (declare (ignore x y same-screen-p child root-x root-y root))
      (logtest mask (xlib:make-state-mask button))))

(defun menu-button-1-down (w) (menu-button-state w :button-1))

(defun menu-button-down? (w)
  (multiple-value-bind (x y same-screen-p child mask root-x root-y root)
	(xlib:query-pointer w)
      (declare (ignore x y same-screen-p child root-x root-y root))
      (logtest mask (xlib:make-state-mask :button-1 :button-2 :button-3))))


(defun menu-get-current-item (x y menu-specs menu-width menu-height)
  (when (and (>= x 0) (<= x menu-width)
	     (>= y 0) (<= y menu-height))
    (dolist (spec menu-specs)
      (when (and (>= y (nth 1 spec))
		 (<= y (nth 2 spec)))
	(return spec)))))

(defun menu-xor-spec (item w gc width)
  (when item
    (setf (xlib:gcontext-function gc) boole-xor)
    (xlib:draw-rectangle w gc
			 0 (nth 1 item)
			 width
			 (- (nth 2 item) (nth 1 item))
			 t)
    (xlib:display-finish-output (xlib:window-display w))))

(defun menu-handle-exposure (w gc menu-specs)
  (xlib:clear-area w)
  (setf (xlib:gcontext-function gc) boole-1)
  (dolist (spec menu-specs)
    (xlib:draw-glyphs w gc 5 (nth 3 spec) (nth 0 spec)))
  (xlib:display-finish-output (xlib:window-display w))
  nil)

(defun menu-handle-button-press (code x y w gc menu-specs menu-w menu-h)
  (declare (ignore code))
  (let* ((current-item
	  (menu-get-current-item x y menu-specs menu-w menu-h))
	 (last-item nil))
    (loop (when (not (menu-button-down? w)) (return))
      (when (not (eql last-item current-item))
	(menu-xor-spec last-item w gc menu-w)
	(menu-xor-spec current-item w gc menu-w))	  	
      (multiple-value-bind
	  (xx xy same-screen-p) (xlib:pointer-position w)
	(declare (ignore same-screen-p))
	(setf last-item current-item)
	(setf current-item
	  (menu-get-current-item xx xy menu-specs menu-w menu-h))))
    (nth 4 last-item)))

;;;============================================================
;;; Takes a list of 'menu items', each of which is a list whose
;;; first item is a string, and whose second item is a function
;;; to be applied to the thrid item in the list if the item is selected.

(defmethod menu (pen (slate CLX-Slate) menu-list)
  (declare (type Pen pen)
	   (type CLX-SLate slate)
	   (type List menu-list))
  (let* ((screen (slate-screen slate))
	 (font (pen-font pen))
	 (xdisplay (xdisplay screen))
	 (selected-item nil)
	 (gc (screen-pen-gcontext screen pen)))
    (declare (type Screen screen)
	     (type Font font)
	     (type xlib:Display xdisplay)
	     (type List selected-item)
	     (type xlib:Gcontext gc))
    (update-gcontext-from-slate slate gc)
    (update-gcontext-from-pen pen screen gc)
    (multiple-value-bind (specs w h) (menu-specs menu-list screen font)
      (declare (type List specs)
	       (type g:Positive-Screen-Coordinate h w))
      (let ((window (menu-window slate w h)))
	(declare (type xlib:Window window))
	(menu-handle-exposure window gc specs)
	(xlib:with-event-queue ((ac:clx-display))
	  (unwind-protect
	      ;; protected form
	      (block event-loop
		(loop
		  (xlib:event-case
		   (xdisplay :force-output-p t :discard-p t)
		   (:exposure ;; handle exposure events
		    (count)
		    ;; Ignore all but the last exposure event
		    (locally (declare (type xlib:Card16 count))
		      (when (= 0 (the xlib:Card16 count))
			(menu-handle-exposure window gc specs))))
		   (:button-press ;; handle mouse button down
		    (x y code)
		    (setf selected-item
		      (menu-handle-button-press code x y window gc specs w h))
		    (return-from event-loop nil))
		   (:button-release
		    ()
		    nil))))
	    ;; cleanup form
	    (progn (xlib:destroy-window window)
		   (xlib:display-finish-output (xlib:window-display window))))
	  ;; execute the selected item
	  (when selected-item
	    #+:excl
	    (apply 'mp:process-run-function
		   (first selected-item)
		   (second selected-item)
		   (rest (rest selected-item)))
	    #+:cmu
	    (apply (first selected-item)
		   (second selected-item)
		   (rest (rest selected-item)))))))))
