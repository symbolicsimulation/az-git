 ;;;-*- Package: :cl-user; Syntax: Common-Lisp; Mode: Lisp; -*-

(in-package :cl-user)

;;(setf depth 8)
(setf depth 24)

(print (setf xscreen (first (xlib:display-roots (ac:clx-display)))))
(print (setf visual (find :true-color
			(rest (assoc depth (xlib:screen-depths xscreen)))
			:key #'xlib:visual-info-class)))
(print (setf colormap (xlib:create-colormap visual
					    (xlib:screen-root xscreen))))
(print (setf w (xlib:create-window
		:background #x000000
		:backing-pixel #x000000
		:backing-planes #xffffff
		:backing-store :not-useful
		:class :input-output
		:colormap colormap
		:cursor :none
		:depth depth
		:save-under :off
		:visual (xlib:visual-info-id visual)
		:parent (xlib:screen-root xscreen)
		:x 500
		:y 1
		:width 100
		:height 100)))

(print (setf gc (xlib:create-gcontext :drawable w 2)))

(print (setf (xlib:wm-name w) "test"))
(print (setf (xlib:wm-icon-name w) "test"))
(print (xlib:map-window w))
(print (xlib:display-force-output (ac:clx-display)))
