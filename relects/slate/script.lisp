;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================

(setf *slate* (make-slate :screen (default-screen)
			  :left 600 :top 100 :width 258 :height 258
			  :backing-store :always))
(show-colormap *slate*)
(ps-dump-slate *slate* "junk.ps" :showpage? t)

(show-named-colors *slate*)


(defparameter *screen* (slate-screen *slate*))
(defparameter *xscreen* (xscreen *screen*))
(defparameter *default-colormap* (screen-colormap *screen*))
(defparameter *gray-scale-colormap*
    (make-colormap-for *screen* :initialization 'fill-gray-scale-colormap))
(setf (screen-colormap *screen*) *gray-scale-colormap*)
(setf (screen-colormap *screen*) *default-colormap*)
  
(test-random-fonts *xor-red-pen* *slate* :nfonts 1)

(test-random-fonts *xor-red-pen* *slate* :nfonts 10 :seconds 0.1)

(time (test-all-fonts *fast-brown-pen* *slate* :seconds 0))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
    (draw-polygon *fast-xor-red-pen* *slate* *poly*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
			(draw-polygon *fast-xor-red-fill-pen* *slate* *poly*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
    (draw-polygon *xor-gray-tile-pen* *slate* *poly*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
    (draw-rect *fast-xor-red-pen* *slate* *r1*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
    (draw-rect *fast-xor-red-fill-pen* *slate* *r1*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
    (draw-rect *xor-gray-tile-pen* *slate* *r1*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (draw-rect *fast-xor-green-pen* *slate* *r1*)
  (with-clipping-region (*slate* *cr*)
    ;;(draw-ellipse *fast-xor-red-pen* *slate* *cr*)
    (draw-ellipse *fast-xor-red-pen* *slate* *r1*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (draw-rect *fast-xor-green-pen* *slate* *r1*)
  (with-clipping-region (*slate* *cr*)
    (draw-ellipse *xor-gray-fill-pen* *slate* *r1*))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (draw-rect *fast-xor-green-pen* *slate* *r1*)
  (with-clipping-region (*slate* *cr*)
    (draw-ellipse *xor-gray-tile-pen* *slate* *r1*))
  (finish-drawing *slate*))

(let* ((dx 10)
       (dy 10)
       (sscreen-rect (slate-rect *slate*))
       (left (+ (g:screen-rect-left sscreen-rect) dx))
       (top (+ (g:screen-rect-top sscreen-rect) dy))
       (width (- (g:screen-rect-width sscreen-rect) dx dx))
       (height (- (g:screen-rect-height sscreen-rect) dy dy))
       (r0 (g::%make-screen-rect left top width height))
       (r1 (g::%make-screen-rect
	    (+ left 1) (+ top 1) (- width 2) (- height 2))))
  (clear-slate *slate* :screen-rect r0)
  (draw-rect *fast-xor-green-pen* *slate* r0)
  (draw-rect *fast-xor-yellow-pen* *slate* r1)
  
  (with-clipping-region (*slate* r1)
    (test-all-drawing-ops *fast-xor-brown-pen* *slate* :nscreen-points 3
			  :seconds 0.3))
  (finish-drawing *slate*))

(let* ((dx 10)
       (dy 10)
       (sscreen-rect (slate-rect *slate*))
       (left (+ (g:screen-rect-left sscreen-rect) dx))
       (top (+ (g:screen-rect-top sscreen-rect) dy))
       (width (- (g:screen-rect-width sscreen-rect) dx dx))
       (height (- (g:screen-rect-height sscreen-rect) dy dy))
       (r0 (g::%make-screen-rect left top width height))
       (r1 (g::%make-screen-rect
	    (+ left 1) (+ top 1) (- width 2) (- height 2))))
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* r0)
  ;(draw-rect *fast-xor-blue-pen* *slate* r1)
  (with-clipping-region (*slate* r1)
    (test-copy-slate-rects *fast-xor-brown-pen* *slate* :seconds 0.3))
  (finish-drawing *slate*))

(test-all-drawing-ops
  *fast-xor-red-pen* *slate* :nscreen-points 8 :seconds 0.1 :print? nil)


(let ((p0 (g::%make-screen-point 110 0))
      (p1 (g::%make-screen-point 30 30)))
  (clear-slate *slate*)
  (draw-line *default-pen* *slate* p0 p1 )
  (draw-line *default-pen* *slate* p1 (g::%make-screen-point 432 0))
  (draw-line *red-pen* *slate*
	     (g::%make-screen-point 10 10) (g::%make-screen-point 60 60))
  (draw-line *wide-red-pen* *slate*
	     (g::%make-screen-point 20 10) (g::%make-screen-point 70 60))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 0 0) (g::%make-screen-point 5 5))
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 5 5) (g::%make-screen-point 10 0))
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 1 1) (g::%make-screen-point 3 3))
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 1 1) (g::%make-screen-point 5 5))
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 5 1) (g::%make-screen-point 1 5))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 1 1) (g::%make-screen-point 3 3))
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 1 1) (g::%make-screen-point 1 5))
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 1 1) (g::%make-screen-point 5 1))
  (draw-rect *blue-fill-pen* *slate* (g::%make-screen-rect 5 5 3 4))
  (draw-rect *default-pen* *slate* (g::%make-screen-rect 9 9 3 4))
  (draw-rect *xor-blue-pen* *slate* (g::%make-screen-rect 13 13 3 4))
  (let ((x 17))
    (dotimes (i 10)
      (draw-rect *default-pen* *slate* (g::%make-screen-rect x 2 i i))
      (draw-rect *default-pen* *slate* (g::%make-screen-rect x 12 i (+ 1 i)))
      (draw-rect *blue-fill-pen* *slate*
		 (g::%make-screen-rect x 22 i i))
      (draw-rect *blue-fill-pen* *slate*
		 (g::%make-screen-rect x 32 i (+ 1 i)))
      (incf x (+ i 2))))
  (finish-drawing *slate*))

(progn
  (draw-line-xy
   *xor-blue-pen* *slate*
   0 (1- (slate-height *slate*))
   (1- (slate-width *slate*)) 0)
  (draw-line
   *xor-blue-pen* *slate*
   (g::%make-screen-point 0 0)
   (g::%make-screen-point (1- (slate-width *slate*))
			  (1- (slate-height *slate*))))
  (finish-drawing *slate*))


(clear-slate *slate*)
(progn
  (time (test-hv-lines *fast-xor-brown-pen* *slate*))
  (time (test-hv-lines *xor-brown-pen* *slate*)))

(progn
  (time (test-slanted-lines *fast-xor-brown-pen* *slate*))
  (time (test-slanted-lines *xor-brown-pen* *slate*)))

(progn
  (time (test-ellipses *fast-xor-brown-pen* *slate*))
  (time (test-ellipses *xor-brown-pen* *slate*)))

(let ((p (g::%make-screen-point 2 100)))
  (draw-string *default-pen* *slate* "a" p)
  (draw-string *default-pen* *slate* "b" p)
  (draw-character *default-pen* *slate* #\a p)
  (draw-character *default-pen* *slate* #\b p)
  (finish-drawing *slate*))

(time
 (progn
   (setf *rect0* (g:make-screen-rect :left 2 :top 4 :width 250 :height 46))
   (setf *rect1* (g:make-screen-rect :left 2 :top 54 :width 250 :height 46))
   (setf *rect2* (g:make-screen-rect :left 2 :top 104 :width 250 :height 46))
   (setf *rect3* (g:make-screen-rect :left 2 :top 154 :width 250 :height 46))
   (setf *rect4* (g:make-screen-rect :left 2 :top 204 :width 250 :height 46))
   (setf *bm0* (boxtile))
   (setf *bm1* (ellipse-tile))
   (setf *bm2* (cross-tile))
   (setf *bm3* (star-tile))
   (setf *bm4* (gray-tile))
   (setf *penn* *red-pen*)
   (setf *pen0* (copy-pen *red-fill-pen*))
   (setf *pen1* (copy-pen *red-fill-pen*))
   (setf *pen2* (copy-pen *red-fill-pen*))
   (setf *pen3* (copy-pen *red-fill-pen*))
   (setf *pen4* (copy-pen *red-fill-pen*))
   (setf (pen-fill-style *pen0*)
     (make-fill-style :fill-style-type :tiled
		      :fill-style-tile (boxtile)))
   (setf (pen-fill-style *pen1*)
     (make-fill-style :fill-style-type :tiled
		      :fill-style-tile (ellipse-tile)))
   (setf (pen-fill-style *pen2*)
     (make-fill-style :fill-style-type :tiled
		      :fill-style-tile (cross-tile)))
   (setf (pen-fill-style *pen3*)
     (make-fill-style :fill-style-type :tiled
		      :fill-style-tile (star-tile)))
   (setf (pen-fill-style *pen4*)
     (make-fill-style :fill-style-type :tiled
		      :fill-style-tile (gray-tile)))
   (draw-rect *pen0* *slate* *rect0*)
   (draw-rect *pen1* *slate* *rect1*)
   (draw-rect *pen2* *slate* *rect2*)
   (draw-rect *pen3* *slate* *rect3*)
   (draw-rect *pen4* *slate* *rect4*)
   (draw-rect *penn* *slate* *rect0*)
   (draw-rect *penn* *slate* *rect1*)
   (draw-rect *penn* *slate* *rect2*)
   (draw-rect *penn* *slate* *rect3*)
   (draw-rect *penn* *slate* *rect4*)
   (finish-drawing *slate*)))

(Time (test-copy-slate-rect *slate*))

(progn
  ;; test multi-colored bitmaps
  (defparameter *bm6* (make-invisible-slate :screen (slate-screen *slate*)
				    :width 10 :height 20))
  (clear-slate *bm6*)
  (draw-ellipse *red-fill-pen* *bm6* (g::%make-screen-rect 0 0 10 20))
  (draw-line
    *default-pen* *bm6*
    (g::%make-screen-point 0 0)
    (g::%make-screen-point 9 19))
  (draw-line
    *green-pen* *bm6* (g::%make-screen-point 0 19) (g::%make-screen-point 9 0))
  (draw-rect *blue-pen* *bm6* (g::%make-screen-rect 2 2 6 16))
  (time
    (progn
      (clear-slate *slate*) 
      (dotimes (x 20)
	(dotimes (y 6)
	  (copy-slate-rect
	    *default-pen*
	    *bm6* (g::%make-screen-rect 0 0 100 100)
	    *slate* (g::%make-screen-point (* x 11) (* y 21)))))))
  (finish-drawing *slate*))

(let ((p (g::%make-screen-point 130 100)))
  (clear-slate *slate*)
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 0 20) (g::%make-screen-point 400 20))
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 0 30) (g::%make-screen-point 400 30))
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 10 0) (g::%make-screen-point 10 400))
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 20 0) (g::%make-screen-point 20 400))
  (draw-rect *blue-fill-pen* *slate* (g::%make-screen-rect 20 80 50 50))
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 0 85) (g::%make-screen-point 400 85))
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 0 95) (g::%make-screen-point 400 95))
  (draw-line *xor-blue-pen* *slate*
	     (g::%make-screen-point 25 0) (g::%make-screen-point 25 400))
  (draw-string *default-pen* *slate* "Foogarbar" p)
  (draw-string *red-pen* *slate* "Foogarbar" p)

  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 30 40) (g::%make-screen-point 60 30))
  (draw-line *wide-pen* *slate*
	     (g::%make-screen-point 60 30) (g::%make-screen-point 90 50))
  (draw-line *medium-pen* *slate*
	     (g::%make-screen-point 90 50) (g::%make-screen-point 130 100))
  (draw-line *zero-width-pen* *slate*
	     (g::%make-screen-point 60 30) (g::%make-screen-point 90 50))

  (draw-arrow *default-pen* *slate*
	      (g::%make-screen-point 100 130) (g::%make-screen-point 110 50)) 
  (dotimes (i 8)
    (let ((x (round (* 30 (cos (* 2 pi (/ i 8.0d0))))))
	  (y (round (* 30 (sin (* 2 pi (/ i 8.0d0)))))))
      (draw-arrow *default-pen*
		  *Slate*
		  (g::%make-screen-point 100 100)
		  (g::%make-screen-point (+ 100 x) (+ 100 y)))))

  (setf *save-screen-rect*
	(string-screen-rect
	  (slate-screen *slate*) "Foogar" (%pen-font  *default-pen*)))
  (draw-string *default-pen* *slate* "Foogar" p)
  (draw-rect *default-pen* *slate* *save-screen-rect*)
  (setf p (g::%make-screen-point 100 100) )
  (setf *save-screen-rect*
	(vertical-string-screen-rect
	  (slate-screen *slate*) "Foogar" (pen-font  *default-pen*)
	  :position (slate-point *slate*)))
  (draw-vertical-string *default-pen* *slate* "Foogar" (slate-point *slate*))
  (draw-rect *default-pen* *slate* *save-screen-rect*)
  (draw-vertical-string *default-pen* *slate* "Foogar" (slate-point *slate*))
  (draw-vertical-string *default-pen* *slate* "QQQQQ" (slate-point *slate*))
  (finish-drawing *slate*))

(let ((p (g::%make-screen-point 180 100)))
  (clear-slate *slate*)
  (draw-vertical-string *default-pen* *slate* "ABCDEFGHIJ"
			p)
  (setf p (g::%make-screen-point 200 100))
  (setf *save-screen-rect*
	(vertical-string-screen-rect
	  (slate-screen *slate*) "ABCDEFGHIJ" (pen-font *default-pen*)
	  :position p))
  (setq *other-screen-rect* (g::%make-screen-rect 80 150 300 30))
  (with-clipping-region (*slate* *other-screen-rect*)
    (draw-vertical-string *default-pen* *slate* "ABCDEFGHIJ"
			 p ))
  (draw-rect *default-pen* *slate* *save-screen-rect*)
  (draw-rect *default-pen* *slate* *other-screen-rect*)
  (finish-drawing *slate*))

(let ((r0 (g::%make-screen-rect 20 20 40 40))
      (r1 (g::%make-screen-rect 0 0 100 100)))
  (clear-slate *slate*)
  (with-clipping-region (*slate* r1)
    (draw-rect *default-pen* *slate* r0)
    (draw-rect *medium-pen* *slate* r0)
    (draw-rect *fast-xor-red-fill-pen* *slate* r0)
    (draw-ellipse *default-pen* *slate* r0)
    (draw-ellipse *medium-pen* *slate* r0)
    (draw-ellipse *green-fill-pen* *slate* r0))
  (finish-drawing *slate*))
(progn 
;; check intra-window copying.
  (clear-slate *slate*)
  (draw-line *default-pen* *slate*
	     (g::%make-screen-point 0 0) (g::%make-screen-point 30 30))
  (draw-rect *blue-fill-pen* *slate*
			   (g::%make-screen-rect 125 0 200 200))
  (draw-rect *red-fill-pen* *slate*
			   (g::%make-screen-rect 125 0 200 200))
  (copy-slate-rect *default-pen* *slate*
		   (g::%make-screen-rect 0 0 1000 1000)
		   *slate* (g::%make-screen-point 150 20))
  (copy-slate-rect *default-pen*
		   (tile-screen-slate *bm1* (slate-screen *slate*))
		   (g::%make-screen-rect 0 0 100 100)
		   *slate* (g::%make-screen-point 10 10))
  (finish-drawing *slate*))






(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
			(dotimes (i 100)
			  (draw-point *fast-xor-red-pen* *slate*
				      (g::%make-screen-point i i))))
  (finish-drawing *slate*))

(with-clipping-region (*slate* *cr*)
  (draw-line *fast-xor-red-pen* *slate*
	     (g::%make-screen-point 0 1) (g::%make-screen-point 100 1 )))
(draw-point *fast-xor-red-pen* *slate* (g::%make-screen-point 100 100))
(draw-point *fast-xor-red-pen* *slate* (g::%make-screen-point 99 99))
(draw-point *fast-xor-red-pen* *slate* (g::%make-screen-point 98 98))
(draw-point *fast-xor-red-pen* *slate* (g::%make-screen-point 0 0))

(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 0 0) (g::%make-screen-point 100 0))
(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 0 1) (g::%make-screen-point 100 1))
(draw-line *fast-xor-red-pen* *slate*
   (g::%make-screen-point 0 2) (g::%make-screen-point 100 2))
(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 0 98) (g::%make-screen-point 98 98))
(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 0 99) (g::%make-screen-point 98 99))
(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 98 0) (g::%make-screen-point 98 98))
(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 99 0) (g::%make-screen-point 99 98))
(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 0 0) (g::%make-screen-point 0 98))
(draw-line *fast-xor-red-pen* *slate*
	   (g::%make-screen-point 1 0) (g::%make-screen-point 1 98))

(Progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-yellow-pen* *slate* *r0*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr0*)
  (draw-rect *fast-xor-red-fill-pen* *slate* *cr0*)
  (draw-line *fast-xor-red-fill-pen* *slate*
	     (g::%make-screen-point 1 1)
	     (g::%make-screen-point 10 10))
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-yellow-pen* *slate* *r0*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr0*)
  (draw-rect *fast-xor-red-fill-pen* *slate* *cr0*)
  (finish-drawing *slate*))

(progn
  (dotimes (i 10)
    (draw-point *fast-xor-red-pen* *slate* (g::%make-screen-point i i)))
  (finish-drawing *slate*))

(clear-slate *slate*)
(time
  (let ((p (g::%make-screen-point 0 0)))
    (dotimes (i 11)
      (g::set-screen-point-coords p :x i :y i)
      (draw-vertical-string
	*fast-xor-green-pen* *slate*
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" p))
    (finish-drawing *slate*)))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (draw-rect *fast-xor-red-fill-pen* *slate* *cr*)
  (finish-drawing *slate*))

(progn
  (clear-slate *slate*)
  (draw-rect *fast-xor-green-pen* *slate* *r*)
  (draw-rect *fast-xor-yellow-pen* *slate* *cr*)
  (with-clipping-region (*slate* *cr*)
    (draw-point *fast-xor-red-pen* *slate* (g::%make-screen-point 100 100))
    (draw-point *fast-xor-red-pen* *slate* (g::%make-screen-point 110 110)))
  (finish-drawing *slate*))

;;;============================================================

(defparameter *count* 0)

(defun add-one (x) (incf (symbol-value x) 1))
(defun add-two (x) (incf (symbol-value x) 2))

(defparameter *menu-list*
    `(("add one" ,#'add-one *count*)
      ("add two" ,#'add-two *count*)))

(menu *red-pen* *slate* *menu-list*)




