;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================
;;;
;;; This file is about Pens, which are something like the graphics
;;; contexts in X. Pens package together all the drawing parameters
;;; that might be needed by drawing operations.  They are to be
;;; specified in a completely device independent way. They may cache
;;; information that speeds their use on particular screens (eg. pixel
;;; value for a color, when the look up table is immutable.)
;;;
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Pens---package up the drawing parameters,
;;; so they don't have to be passed individually
;;;============================================================

(defstruct (Pen
	    (:conc-name nil)
	    (:constructor %make-pen)
	    (:copier %%copy-pen)
	    (:print-function print-pen))

  (%pen-font (small-font) :type Font)

  (%pen-operation boole-1 :type Boolean-Operation)
  ;; the operation determines how "source" pixels
  ;; are combined with "destination" pixels in drawing operations

  (%pen-plane-mask *all-pixel-planes-mask* :type Pixel-Value)
  ;; Only the bits that are 1 in the plane mask are affected
  ;; by drawing operations.

  ;; Color:
  ;;
  ;; A pen's color can be specified either by the {\sf pen-pixel-value}
  ;; slot or the {\sf pen-color} slot. The {\sf pen-pixel-value} slot
  ;; has priority; the {\sf pen-color} slot is used to determine the
  ;; color only if the {\sf pen-pixel-value} slot is nil.  When the
  ;; pixel value slot is used, the system will draw with a pixel-value
  ;; = {\sf (min (pen-pixel-value pen) (maximum-pixel-value screen))}.
  ;; When the color slot is used, the system will draw with the color in
  ;; the current colormap that best approximates the specified color, in
  ;; the sense of the pen's color metric (see below).

  (%pen-color (colorname->color :blue) :type Color)
  (%pen-pixel-value nil :type (or Null Pixel-Value))
  ;;(%pen-color-metric 'color-l1-dist :type (or Symbol Function))

  ;; line drawing:
  (%pen-line-style (make-line-style) :type Line-Style)

  ;; area filling:
  (%pen-fill-style (make-fill-style) :type Fill-Style)

  ;; arc drawing 
  (%pen-arc-mode nil) ;; for future expansion

  ;; for reducing the number of times we need to update
  ;; device dependent objects, like xlib:Gcontexts
  (%pen-changed-time (az:make-timestamp) :type az:Timestamp)

  ;; a plist used by the CLX implementation to cache a xlib:gcontext
  ;; for each screen the pen is used on. Could also be used for equivalent
  ;; purpose by other implementations.
  (%pen-screen-gcontexts () :type List)
  )

;;;============================================================
;;; safe accessors, which also update timestamp
;;;============================================================

(defun pen-font (pen)
  (az:declare-check (type Pen pen))
  (%pen-font pen))

(defsetf pen-font (pen) (new-font)
  `(progn
     (az:declare-check (type Pen ,pen)
		       (type Font ,new-font))
     (ac:atomic
      (setf (%pen-font ,pen) ,new-font)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-font)))

(defun pen-operation (pen)
  (az:declare-check (type Pen pen))
  (%pen-operation pen))

(defsetf pen-operation (pen) (new-operation)
  `(progn
     (az:declare-check (type Pen ,pen)
		       (type Boolean-Operation ,new-operation))
     (ac:atomic
      (setf (%pen-operation ,pen) ,new-operation)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-operation)))

(defun pen-plane-mask (pen)
  (az:declare-check (type Pen pen))
  (%pen-plane-mask pen))

(defsetf pen-plane-mask (pen) (new-plane-mask)
  `(progn
     (az:declare-check (type Pen ,pen)
		       (type (or Null Plane-Mask) ,new-plane-mask))
     (ac:atomic
      (setf (%pen-plane-mask ,pen) ,new-plane-mask)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-plane-mask)))

(defun pen-color (pen)
  (az:declare-check (type Pen pen))
  (%pen-color pen))

(defsetf pen-color (pen) (new-color)
  `(progn
     (az:declare-check (type Pen ,pen)
		       (type Color ,new-color))
     (ac:atomic
      (setf (%pen-color ,pen) ,new-color)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-color)))

(defun pen-pixel-value (pen)
  (az:declare-check (type Pen pen))
  (%pen-pixel-value pen))

(defsetf pen-pixel-value (pen) (new-pixel-value)
  (az:once-only
   (pen new-pixel-value)
   `(progn
      (az:declare-check (type Pen ,pen)
			(type (or Null Pixel-Value) ,new-pixel-value))
      (ac:atomic
       (unless (eql (%pen-pixel-value ,pen) ,new-pixel-value)
	 (setf (%pen-pixel-value ,pen) ,new-pixel-value)
	 (az::%stamp-time! (%pen-changed-time ,pen))
	 ,new-pixel-value)))))

#||
(defun pen-color-metric (pen)
  (az:declare-check (type Pen pen))
  (%pen-color-metric pen))

(defsetf pen-color-metric (pen) (new-color-metric)
  `(progn
     (az:declare-check (type Pen ,pen)
		       (type (or Symbol Function) ,new-color-metric))
     (ac:atomic
      (setf (%pen-color-metric ,pen) ,new-color-metric)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-color-metric)))
||#
(defun pen-line-style (pen)
  (az:declare-check (type Pen pen))
  (%pen-line-style pen))

(defsetf pen-line-style (pen) (new-line-style)
  `(progn
     (az:declare-check (type Pen ,pen)
		       (type Line-Style ,new-line-style))
     (ac:atomic
      (setf (%pen-line-style ,pen) ,new-line-style)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-line-style)))

(defun pen-fill-style (pen)
  (az:declare-check (type Pen pen))
  (%pen-fill-style pen))

(defsetf pen-fill-style (pen) (new-fill-style)
  `(progn
     (az:declare-check (type Pen ,pen)
		       (type Fill-Style ,new-fill-style))
     (ac:atomic
      (setf (%pen-fill-style ,pen) ,new-fill-style)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-fill-style)))

(defun pen-arc-mode (pen)
  (az:declare-check (type Pen pen))
  (%pen-arc-mode pen))

(defsetf pen-arc-mode (pen) (new-arc-mode)
  `(progn
     (az:declare-check (type Pen ,pen)
		       ;; (type Arc-Mode ,new-arc-mode)
		       )
     (ac:atomic
      (setf (%pen-arc-mode ,pen) ,new-arc-mode)
      (az:stamp-time! (%pen-changed-time ,pen))
      ,new-arc-mode)))

;;;============================================================

(defun make-pen (&key
		 (pen-font (small-font))
		 (pen-operation boole-1)
		 (pen-color (colorname->color :blue))
		 (pen-pixel-value nil)
		 (pen-plane-mask *all-pixel-planes-mask*)
		 ;;(pen-color-metric 'color-l1-dist)
		 (pen-line-style (make-line-style))
		 (pen-fill-style (make-fill-style)))
  (let ((pen (%make-pen :%pen-font pen-font
			:%pen-operation pen-operation
			:%pen-plane-mask pen-plane-mask
			:%pen-color pen-color
			:%pen-pixel-value pen-pixel-value
			;;:%pen-color-metric pen-color-metric
			:%pen-line-style pen-line-style
			:%pen-fill-style pen-fill-style)))
    (az:stamp-time! (%pen-changed-time pen))
    pen))

(defun pen? (x) (typep x 'Pen))

(defun equal-pens? (pen0 pen1)
  ;; ignores timestamps!
  (or (eql pen0 pen1)
      (and (equal-line-styles? (%pen-line-style pen0)
			       (%pen-line-style pen1))
	   (equal-fill-styles? (%pen-fill-style pen0)
			       (%pen-fill-style pen1))
	   (equal-fonts? (%pen-font pen0)
			 (%pen-font pen1))
	   (equal-boolean-operations? (%pen-operation pen1)
				      (%pen-operation pen0))
	   (equal-colors? (%pen-color pen1)
			  (%pen-color pen0))
	   (eql (%pen-pixel-value pen1)
		(%pen-pixel-value pen0))
	   #||
	   (eql (%pen-color-metric pen1)
		(%pen-color-metric pen0))
	   ||#
	   )))

(defun %copy-pen (pen result)
  (ac:atomic
   (setf (%pen-font result) (%pen-font pen))
   (setf (%pen-operation result) (%pen-operation pen))
   (setf (%pen-pixel-value result) (%pen-pixel-value pen))
   ;;(setf (%pen-color-metric result) (%pen-color-metric pen))
   (copy-color (%pen-color pen) :result (%pen-color result))
   (copy-line-style (%pen-line-style pen) :result (%pen-line-style result))
   (copy-fill-style (%pen-fill-style pen) :result (%pen-fill-style result))
   (az:stamp-time! (%pen-changed-time pen)))
  result)

(defun copy-pen (pen &key (result (make-pen)))
  (az:declare-check (type Pen pen result))
  (%copy-pen pen result))

;;;============================================================

(defun print-pen (pen stream depth)
  (declare (ignore depth))
  (az:printing-random-thing
   (pen stream)
   (format stream "Pen")))

;;;============================================================
;;; These should be made obsolete:

(defun pen-thickness (pen)
  (az:declare-check (type Pen pen))
  (line-style-thickness (%pen-line-style pen)))

(defun pen-dashes (pen)
  (az:declare-check (type Pen pen))
  (line-style-dashes (%pen-line-style pen)))

(defun pen-arrowhead-length (pen)
  (az:declare-check (type Pen pen))
  (line-style-arrowhead-length (%pen-line-style pen)))

(defun pen-arrowhead-angle (pen)
  (az:declare-check (type Pen pen))
  (line-style-arrowhead-angle (%pen-line-style pen)))

;(defun pen-tile (pen)
;  (az:declare-check (type Pen pen))
;  (fill-style-tile (%pen-fill-style pen)))

;(defun pen-stipple (pen)
;  (az:declare-check (type Pen pen))
;  (fill-style-stipple (%pen-fill-style pen)))

;(defsetf pen-stipple (pen) (x)
;  `(progn
;     (az:declare-check (type Pen ,pen))
;     (type Stipple ,x)
;     (setf (fill-style-stipple (%pen-fill-style ,pen)) ,x)))

;(defun pen-fill-rule (pen)
;  (az:declare-check (type Pen pen))
;  (fill-style-fill-rule (%pen-fill-style pen)))
;
;(defsetf pen-fill-rule (pen) (x)
;  `(progn
;     (az:declare-check (type Pen ,pen)
;     (type (Member t nil)  ,x))
;     (setf (fill-style-fill-rule (%pen-fill-style ,pen)) ,x)))

(defun %dashed-pen? (pen)
  (not (null (line-style-dashes (%pen-line-style pen)))))

(defun dashed-pen? (pen)
  (az:declare-check (type Pen pen))
  (%dashed-pen? pen))

;;;============================================================

(defun screen-pen-pixel-value (screen pen)
  ;;(az:declare-check (type Screen screen)
  ;;(type Pen pen))
  (let ((pv (%pen-pixel-value pen)))
    (if (null pv)
	(color->pixel-value (screen-colormap screen) (%pen-color pen))
	pv)))
;    (etypecase c
;      ;;(Pixel-Value c)
;      ;; faster, but less careful:
;      (Fixnum c)
;      (Color (color->pixel-value (screen-colormap screen) c)))))

;;;============================================================

;(eval-when (compile load eval) 
;(proclaim '(Inline %bw-pen-pixel-value %bw-pen-operation)))

;(defun %bw-pen-pixel-value (pen)
;  (let ((c (%pen-color pen)))
;    (if (typep c 'Pixel-Value)
;	;; then truncate to 1.
;	(min c 1)
;	;; else map black to 0 pixels, anything else to 1.
;	(if (%black-color? c) 0 1)))) 

(defun %bw-pen-pixel-value (pen)
  (let ((pv (%pen-pixel-value pen)))
    (if pv 
	;; then truncate to 1.
	(min pv 1)
	;; else map black to 0 pixels, anything else to 1.
	(if (%black-color? (%pen-color pen)) 0 1))))

(defun bw-pen-pixel-value (pen)
  (az:declare-check (type Pen pen))
  (%bw-pen-pixel-value pen))

(defun %bw-pen-operation (pen)
  (if (zerop (%bw-pen-pixel-value pen))
      ;; then invert operation (source is zero)
      (invert-operation (%pen-operation pen))
      (%pen-operation pen)))

(defun bw-pen-operation (pen)
  (az:declare-check (type Pen pen))
  (%bw-pen-operation pen))

;;;============================================================

(defparameter *filling-style* (make-fill-style :fill-style-type :solid))

(defparameter *writing-pen* nil)

(defun writing-pen ()
  (when (null *writing-pen*)
    (setf *writing-pen* (make-pen :pen-operation boole-1
				  :pen-font (normal-font)
				  :pen-pixel-value 255)))
  *writing-pen*)

(defparameter *xor-pen* nil)

(defun xor-pen ()
  (when (null *xor-pen*)
    (setf *xor-pen* (make-pen :pen-operation boole-xor
			      :pen-font (normal-font)
			      :pen-pixel-value 255)))
  *xor-pen*)

(defparameter *erasing-pen* nil)

(defun erasing-pen ()
  (when (null *erasing-pen*)
    (setf *erasing-pen* (make-pen :pen-operation boole-1
				  :pen-font (normal-font)
				  :pen-pixel-value 0)))
  *erasing-pen*)

(defparameter *erasing-fill-pen* nil)

(defun erasing-fill-pen ()
  (when (null *erasing-fill-pen*)
    (setf *erasing-fill-pen* (make-pen :pen-operation boole-1
				       :pen-font (normal-font)
				       :pen-pixel-value 0
				       :pen-fill-style
				       (copy-fill-style *filling-style*))))
  *erasing-fill-pen*)