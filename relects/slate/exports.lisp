;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

(eval-when (compile load eval)
  (export '(Boolean-Operation
	    
	    ;; Abstract Color-Descriptions
	    Pixel-Value
	    ;;*all-pixel-planes-mask*
	    ;; Colormaps (similar to "visuals" in X, see Nye, Vol 1, p 178)
	    Colormap
	    legal-pixel-value?
	    pixel-depth
	    colormap-length
	    color->pixel-value
	    pixel-value->color
	    colorname->pixel-value
	    Read-Only-Colormap
	    Read-Write-Colormap
	    Monochrome-Colormap
	    RGB-Colormap
	    Single-Index-RGB-Colormap
	    Decomposed-Index-RGB-Colormap

	    Static-Gray-Colormap
	    Static-Colormap
	    True-Colormap

	    Gray-Scale-Colormap
	    Pseudo-Colormap
	    Direct-Colormap

	    Bw-Colormap
	    ;;Wb-Colormap
	    Byte-Pseudo-Colormap

	    fill-gray-scale-colormap
	    fill-colornames-colormap

	    CLX-Screen
	    Bw-CLX-Screen
	    RGB-CLX-Screen
	    True-24bit-Color-CLX-Screen

	    CLX-Output-Slate
	    CLX-Slate
	    Invisible-CLX-Slate

	    ;; Abstract Color-Descriptions
	    Color
	    color?
	    copy-color
	    equal-colors?
	    color-l1-dist

	    rgb->color color->rgb
	    ;; rgb->ihs ihs->rgb
	    ;; rgb->yiq yiq->rgb
	    ;; rgb->hexcone-ihs hexcone-ihs->rgb
	    ;; rgb16->color color->rgb16
	    rgb8->color color->rgb8
	    ;;ihs->color color->ihs
	    ;;hexcone-ihs->color color->hexcone-ihs
	    ;;yiq->color color->yiq

	    borrow-color
	    return-color
	    with-borrowed-color 

	    Colorname
	    colorname?
	    colorname->color

	    ;; flushing output
	    finish-drawing force-drawing
	    ;; drawing points (pixels)
	    draw-point draw-points

	    ;; drawing lines
	    draw-line draw-lines
	    draw-polyline draw-polylines
	    draw-polygon draw-polygons
	    draw-arrow draw-arrows
	    ;; copy-slate-rect (aka bitblt) operations
	    copy-slate-rect copy-slate-rects

	    ;; drawing screen-rects
	    clear-slate
	    draw-rect draw-rects
         
	    ;; drawing ellipses
	    draw-ellipse draw-ellipses
	    draw-circle draw-circles
         
	    ;; drawing strings and characters
	    draw-character draw-characters
	    draw-string draw-strings
	    draw-centered-string draw-centered-strings
	    draw-cornered-string 
	    draw-vertical-string draw-vertical-strings
	    draw-centered-vertical-string draw-centered-vertical-strings

	    ps-dump-slate

	    ;; Fonts
	    Font-Family
	    Font-Face
	    Font-Size
	    Font
	    equal-fonts?
	    small-font
	    normal-font
	    large-font
	    large-italic-font

	    Fill-Style
	    fill-style?
	    make-fill-style
	    copy-fill-style
	    equal-fill-styles?
	    fill-style-type
	    fill-style-rule
	    fill-style-tile
	    fill-style-stipple


	    ;; some functions for getting standard tiles
	    boxtile
	    ellipse-tile c
	    ross-tile
	    star-tile
	    gray-tile

	    ;; Button-Number
	    ;; Event

	    menu
	    input-string
	
	    Line-Style
	    make-line-style
	    line-style?
	    copy-line-style
	    equal-line-styles?
	    line-style-thickness
	    line-style-dashes
	    line-style-dash-offset
	    line-style-cap
	    line-style-join
	    line-style-arrowhead-length
	    line-style-arrowhead-angle

	    Pen 
	    pen?
	    make-pen
	    copy-pen
	    equal-pens?
	    pen-line-style
	    pen-fill-style
	    pen-pixel-value
	    pen-color
	    ;;pen-color-metric
	    pen-operation
	    pen-plane-mask
	    pen-font
	    writing-pen
	    xor-pen
	    erasing-pen
	    erasing-fill-pen


	    ;; Screens
	    default-screen
	    all-screens

	    Screen
	    RGB-Screen
	    ;;Monochrome-Screen
	    ;;Read-Only-Colormap-Screen
	    ;;Read-Write-Colormap-Screen
	    ;;Single-Index-RGB-Screen
	    ;;Pseudo-Color-Screen
	    Byte-Pseudo-Color-Screen
	    ;;Static-Gray-Screen
	    BW-Screen
	    screen-pixel-depth
	    screen-rect
	    screen-origin
	    screen-extent
	    screen-colormap

	    make-colormap-for

	    inches-per-pixel-x
	    inches-per-pixel-y
	    milimeters-per-pixel-x
	    milimeters-per-pixel-y
	    pixels-per-inch-x
	    pixels-per-inch-y
	    pixels-per-milimeter-x
	    pixels-per-milimeter-y
          
	    ;; string and character measurements
	    character-screen-rect 
	    character-width
	    character-height
	    character-ascent
	    character-descent
	    string-screen-rect
	    string-extent
	    string-width
	    string-height
	    string-ascent
	    string-descent
	    maximum-string-width
	    maximum-string-height
	    vertical-string-screen-rect
	    vertical-string-extent
	    vertical-string-width v
	    ertical-string-height
	    vertical-string-ascent
	    vertical-string-descent
	    maximum-vertical-string-width
	    maximum-vertical-string-height


	    ;;Slate-Object

	    Slate
	    Dead-Slate
	    slate-screen
	    make-slate
	    make-invisible-slate
	    slate-clipping-region
	    with-clipping-region
          
	    Invisible-Slate

	    slate-top
	    slate-left
	    slate-width
	    slate-height
	    slate-right
	    slate-bottom
	    slate-origin
	    slate-rect
	    slate-name

	    slate-background-color
	    slate-background-pixel-value
	    ;; accessing the current screen-point
	    slate-point

	    default-slate bw-slate rgb-slate

	    ;; Tiles
	    Tile 
	    ;; tile-screen-slate
	    make-tile
	    tile?
	    equal-tiles?
	    tile-width
	    tile-height
	    tile-draw-function

	    #||
	    Dash-Description
	    dash-description?
	    copy-dash-description
	    equal-dash-descriptions?
	    ||#


	    Input-Slate
	    selected?  
	    mouse-down?
	    mouse-up?
	    mouse-screen-point
	    key-down?         
	    key-up?
	    )))

