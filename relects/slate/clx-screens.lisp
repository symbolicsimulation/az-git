;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Colors

(defun xcolorname (colorname)
  (az:declare-check (type Colorname colorname))
  (substitute #\space #\- (symbol-name colorname)))

(defun color-level->rgb-val (color-level)
  (az:declare-check (type Color-Level color-level))
  (the Double-Float (/ (the Color-Level color-level)
		#.(az:fl #xffff))))

(defun rgb-val->color-level (rgb-val)
  (az:declare-check (type xlib:RGB-Val rgb-val))
  (truncate (* (the xlib:RGB-Val rgb-val) #.(az:fl #xffff))))

(defparameter *xcolor-cache* (make-hash-table :test #'eq)
  "To avoid consing too many xlib:Color objects")

(defun color->xcolor (color)
  (az:declare-check (type Color color))
  (let ((xcolor (gethash color *xcolor-cache* nil)))
    (when (null xcolor)
      (setf xcolor (xlib:make-color))
      (setf (gethash color *xcolor-cache* nil) xcolor))
    ;; make sure {\sf xcolor> is consistent with {\sf color>
    (setf (xlib:color-red xcolor)
      (color-level->rgb-val (color-r16 color)))
    (setf (xlib:color-green xcolor)
      (color-level->rgb-val (color-g16 color)))
    (setf (xlib:color-blue xcolor)
      (color-level->rgb-val (color-b16 color)))
    xcolor))

(defun xcolor->color (xcolor &key (result (%make-color)))
  (az:declare-check (type xlib:Color xcolor)
		    (type Color result))
  (set-color-coords result 
		    (rgb-val->color-level (xlib:color-red xcolor))
		    (rgb-val->color-level (xlib:color-green xcolor))
		    (rgb-val->color-level (xlib:color-blue xcolor)))
  result)


;;;============================================================
;;; Getting at some X defaults:
;;;============================================================

(defun default-xscreen (&key (xdisplay (ac:clx-display)))
  (xlib:display-default-screen xdisplay))

(defun default-visual-info-class (&key 
				  (xdisplay (ac:clx-display))
				  (xscreen (default-xscreen
					     :xdisplay xdisplay)))
  (xlib:visual-info-class 
   (xlib:visual-info
   xdisplay
   (xlib:window-visual (xlib:screen-root xscreen)))))

(defun default-visual-info (&key
			    (xdisplay (ac:clx-display))
			    (xscreen (default-xscreen :xdisplay xdisplay)))
  (xlib:visual-info
   xdisplay
   (xlib:window-visual (xlib:screen-root xscreen))))

(defun default-colormap (&key 
			 (xdisplay (ac:clx-display))
			 (xscreen (default-xscreen
				    :xdisplay xdisplay)))
  (xlib:screen-default-colormap xscreen))

(defun default-screen-depth (&key
			    (xdisplay (ac:clx-display))
			    (xscreen (default-xscreen :xdisplay xdisplay)))
  (xlib:screen-root-depth xscreen))

;;;============================================================
;;; Screens

(defclass CLX-Screen (Screen)
	  ((xhost
	    :type T
	    ;; Host type is not specified in clx, usually will be a string
	    :reader xhost
	    :initarg :xhost)
	   (xdisplay
	    :type xlib:Display
	    :reader xdisplay
	    :initarg :xdisplay)
	   (xscreen
	    :type xlib:Screen
	    :reader xscreen
	    :initarg :xscreen)
	   (xcolormap
	    :type xlib:Colormap
	    :reader xcolormap
	    :initarg :xcolormap)
	   (arrow-cursor
	    :type (or Null xlib:Cursor)
	    :initform nil)))

;;;------------------------------------------------------------

(defmethod screen-width ((screen CLX-Screen))
  (declare (type CLX-Screen screen))
  (xlib:screen-width (xscreen screen)))

(defmethod screen-height ((screen CLX-Screen))
  (declare (type CLX-Screen screen))
  (xlib:screen-height (xscreen screen)))

(defmethod screen-origin ((screen CLX-Screen)
			  &key (result (g:make-screen-point)))
  (declare (type CLX-Screen screen)
           (type g:Screen-Point result))
  (g::set-screen-point-coords result :x 0 :y 0))

(defmethod screen-extent ((screen CLX-Screen)
			  &key (result (g:make-screen-vector)))
  (declare (type CLX-Screen screen)
	   (type g:Screen-Vector result))
  (g::set-screen-vector-coords result
			      :x (screen-width screen)
			      :y (screen-height screen)))

(defmethod screen-rect ((screen CLX-Screen)
			&key (result (g:make-screen-rect)))
  (declare (type CLX-Screen screen)
           (type g:Screen-Rect result))
  (g::set-screen-rect-coords result
			    :left 0
			    :width (screen-width screen)
			    :top 0
			    :height (screen-height screen)))

;;;============================================================

(defclass BW-CLX-Screen (BW-Screen CLX-Screen) ())

;;;------------------------------------------------------------     

(defmethod make-colormap-for ((screen BW-CLX-Screen) &rest options)
  (declare (ignore options))
  (make-instance 'BW-Colormap
    :colormap-black-pixel-value (xlib:screen-black-pixel (xscreen screen))))

;;;------------------------------------------------------------
;;; creates slate screen object

(defun make-bw-clx-Screen (&key
			   (xhost (ac:clx-host))
			   (xdisplay (ac:clx-display))
			   (xscreen (default-xscreen))
			   (xcolormap
			    (xlib:screen-default-colormap xscreen)))
  (az:declare-check (type xlib:Display xdisplay)
		    (type xlib:Screen xscreen)
		    (type xlib:Colormap xcolormap))
  (let ((screen (make-instance 'BW-CLX-Screen
		  :xhost xhost
		  :xdisplay xdisplay
		  :xscreen xscreen
		  :xcolormap xcolormap)))
    screen))

;;;============================================================
;;; This is a very special case---we can worry about generalizing
;;; to all possible X color screen types later.

(defclass RGB-CLX-Screen (Byte-Pseudo-Color-Screen CLX-Screen) ())

;;;------------------------------------------------------------

(defconstant *all-byte-pixels* `#.(loop for i from 0 to 255 collect i))

(defmethod fill-colornames-colormap ((screen RGB-CLX-Screen) colormap)
  (let* ((default-xcolormap (xlib:screen-default-colormap (xscreen screen)))
	 (lut-vector (colormap-vector colormap))
	 (xcolors (xlib:query-colors default-xcolormap *all-byte-pixels*
				     :result-type 'Vector)))
    (declare (type xlib:Colormap default-xcolormap)
	     (type (Simple-Array Color (*)) lut-vector)
	     (type Vector xcolors))
    (dotimes (pixval 255)
      (setf (aref lut-vector pixval)
	(xcolor->color (aref xcolors pixval)
			:result (aref lut-vector pixval)))))
  colormap)

(defmethod update-hardware-colormap ((screen RGB-CLX-Screen))
  (let ((colormap (screen-colormap screen))
	(xcolormap (xcolormap screen)))
    (declare (type Colormap colormap)
	     (type xlib:Colormap xcolormap))
    (dotimes (i 256)
      (xlib:store-color
       xcolormap i
       (color->xcolor (aref (colormap-vector colormap) i))))
    (xlib:display-force-output (xdisplay screen))))

;;;------------------------------------------------------------
;;; creates slate screen object

(defun make-rgb-clx-screen (&key
			    (xhost (ac:clx-host))
			    (xdisplay (ac:clx-display))
			    (xscreen (default-xscreen))
			    (xcolormap
			     (xlib:create-colormap
			      (xlib:window-visual (xlib:screen-root xscreen))
			      (xlib:screen-root xscreen) t)))
  (az:declare-check (type xlib:Display xdisplay)
		    (type xlib:Screen xscreen)
		    (type xlib:Colormap xcolormap))
  (let ((screen (make-instance 'RGB-CLX-Screen
		  :xhost xhost
		  :xdisplay xdisplay
		  :xscreen xscreen
		  :xcolormap xcolormap)))
    (setf (slot-value screen 'screen-colormap) (make-colormap-for screen))
    (update-hardware-colormap screen)
    screen))

;;;============================================================
;;; This is a very special case---we can worry about generalizing
;;; to all possible X color screen types later.

(defclass True-24bit-Color-CLX-Screen (True-24bit-Color-Screen CLX-Screen) ())

;;;------------------------------------------------------------

(defun true-24bit-color-visual (&key
				(xdisplay (ac:clx-display))
				(xscreen
				 (default-xscreen :xdisplay xdisplay)))
  (find :true-color (rest (assoc 24 (xlib:screen-depths xscreen)))
	:key #'xlib:visual-info-class))

(defun make-true-24bit-color-clx-screen
    (&key
     (xhost (ac:clx-host))
     (xdisplay (ac:clx-display))
     (xscreen (default-xscreen))
     (xvisual (true-24bit-color-visual :xscreen xscreen))
     (xcolormap (xlib:create-colormap xvisual (xlib:screen-root xscreen))))
  (az:declare-check (type xlib:Display xdisplay)
		    (type xlib:Screen xscreen)
		    (type xlib:Colormap xcolormap))
  (let ((screen (make-instance 'True-Color-CLX-Screen
		  :xhost xhost
		  :xdisplay xdisplay
		  :xscreen xscreen
		  :xcolormap xcolormap)))
    (setf (slot-value screen 'screen-colormap) (make-colormap-for screen))
    screen))

;;;============================================================

(defun make-default-screen ()
  (let ((visual-info-class (default-visual-info-class))
	(depth (default-screen-depth)))
    (cond ((and (eq visual-info-class :Static-Gray) (= depth 1))
	   (make-bw-CLX-screen))
	  ((and (eq visual-info-class :Pseudo-Color) (= depth 8))
	   (make-rgb-CLX-screen))
	  (t
	   (error "Can't yet handle ~s screens of depth ~d (yet)."
		  visual-info-class depth)))))

;;;============================================================
;;; caching access to the arrow cursor

(defun arrow-cursor (screen)
  (az:declare-check (type CLX-Screen screen))
  (let ((cursor (slot-value screen 'arrow-cursor)))
    (when (null cursor)
      (setf cursor
	(let* ((cursor-font (xlib::open-font (xdisplay screen) "cursor"))
	       (colormap (xcolormap screen))
	       (black-color (xlib:lookup-color colormap "black"))
	       (white-color (xlib:lookup-color colormap "white")))
	  (xlib:create-glyph-cursor :source-font cursor-font
				    :mask-font cursor-font
				    :source-char 132
				    :mask-char 133
				    :foreground black-color
				    :background white-color))))
    (setf (slot-value screen 'arrow-cursor) cursor)
    cursor))

;;;============================================================

;;;============================================================
;;; Font stuff
;;;============================================================

(defparameter *xdefault-font* nil)

(defun xdefault-font (screen)
    (when (null *xdefault-font*)
      (setf *xdefault-font* (xlib::open-font (xdisplay screen) "fixed")))
    *xdefault-font*)

(defun xfont-family (family)
    (az:declare-check (type Font-Family family))
    (ecase family
      (:helvetica "helvetica")
      (:times "times")
      (:fix "fixed")))

(defun xfont-face (face)
    (az:declare-check (type Font-Face face))
    (ecase face
      (:roman "medium-r")
      (:bold "bold-r")
      (:italic "medium-i")
      (:bold-italic "bold-i")))

(defun xfont-namestring (family face size)
  (az:declare-check (type Font-Family family)
		    (type Font-Face face)
		    (type Font-Size size))
  (format nil "-adobe-~a-~a-*--~a-*"
	  (xfont-family family)
	  (xfont-face face)
	  size))
#||
(defun xlist-font-names (display family face size)
  (when (xlib:list-fonts display (xfont-namestring family face size))
    (xlib:list-font-names display (xfont-namestring family face size))))
||#

(defun xlist-font-names (display family face size)
  (xlib:list-font-names display (xfont-namestring family face size)))

(defmethod make-screen-font ((screen CLX-Screen) font)
  (az:declare-check (type Font font))
  (let* ((family (font-family font))
	 (face (font-face font))
	 (size (font-size font))
	 (display (xdisplay screen))
	 (font-names (xlist-font-names display family face size)))
    (when (null font-names) ;; try 1 pt smaller
      (setf font-names (xlist-font-names display family face (- size 1))))
    (when (null font-names) ;; try 1 pt larger
      (setf font-names (xlist-font-names display family face (+ size 1))))
    (when (and (null font-names) (eql face :bold-italic))
      ;; try a simple face
      (setf font-names
	(xlist-font-names display family :italic size)))
    (when (null font-names) ;; try 1 pt smaller
      (setf font-names
	(xlist-font-names display family :italic (- size 1))))
    (when (null font-names) ;; try 1 pt larger
      (setf font-names
	(xlist-font-names display family :italic (+ size 1))))
    (when (and (null font-names) (eql face :bold-italic))
      ;; try a simple face
      (setf font-names (xlist-font-names display family :bold size)))
    (when (null font-names) ;; try 1 pt smaller
      (setf font-names (xlist-font-names display family :bold (- size 1))))
    (when (null font-names) ;; try 1 pt larger
      (setf font-names (xlist-font-names display family :bold (+ size 1))))
    (when (null font-names) ;; try a simple face
      (setf font-names (xlist-font-names display family :roman size)))
    (when (null font-names) ;; try 1 pt smaller
      (setf font-names (xlist-font-names display family :roman (- size 1))))
    (when (null font-names) ;; try 1 pt larger
      (setf font-names (xlist-font-names display family :roman (+ size 1))))
    (if (null font-names)
	;; if still nothing, use default
	(xdefault-font screen)
      (xlib::open-font display (first font-names)))
    ))

;;;============================================================
;;; String and Char measurement
;;;============================================================

(defmethod %analyze-character-placement ((screen CLX-Screen) char font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Character char)
	     (type Font font))
    (let ((xfont (%screen-font screen font))
	  (xfont-index (char-code char)))
      (declare (type xlib:Font xfont)
	       (type Fixnum xfont-index))
      (values (xlib:char-width xfont xfont-index)
	      (xlib:char-ascent xfont xfont-index)
	      (xlib:char-descent xfont xfont-index))))

(defmethod %character-screen-rect ((screen CLX-Screen) char font origin result)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Character char)
	     (type Font font)
	     (type g:Screen-Point origin)
	     (type g:Screen-Rect result))
    (let* ((xfont (%screen-font screen font))
	   (xfont-index (char-code char))
	   (a (xlib:char-ascent xfont xfont-index)))
      (declare (type xlib:Font xfont)
	       (type Fixnum xfont-index)
	       (type g:Screen-Coordinate a))
      (g::%set-screen-rect-coords
       result
       (g::%screen-point-x origin)
       (- (the g:Screen-Coordinate (g::%screen-point-y origin)) a)
       (xlib:char-width xfont xfont-index)
       (+ a (the g:Screen-Coordinate (xlib:char-descent xfont xfont-index))))
      result))

(defmethod %character-width ((screen CLX-Screen) char font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Character char)
	     (type Font font))
    (xlib:char-width (%screen-font screen font) (char-code char)))

(defmethod %character-height ((screen CLX-Screen) char font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Character char)
	     (type Font font))
    (let ((xfont (%screen-font screen font))
	  (xfont-index (char-code char)))
      (declare (type xlib:Font xfont)
	       (type Fixnum xfont-index))
      (+ (the g:Screen-Coordinate (xlib:char-ascent xfont xfont-index))
	 (the g:Screen-Coordinate (xlib:char-descent xfont xfont-index)))))

(defmethod %character-ascent ((screen CLX-Screen) char font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Character char)
	     (type Font font))
    (xlib:char-ascent (%screen-font screen font) (char-code char)))

(defmethod %character-descent ((screen CLX-Screen) char font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Character char)
	     (type Font font))
    (xlib:char-descent (%screen-font screen font) (char-code char)))

;;;------------------------------------------------------------

(defmethod %analyze-string-placement ((screen CLX-Screen) string font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Simple-String string)
	     (type Font font))
    (multiple-value-bind
	(width ascent descent left-bearing right-bearing
	 overall-ascent overall-descent overall-direction next-start)
	(xlib:text-extents (the xlib:Font (%screen-font screen font)) string)
      (declare (ignore ascent descent left-bearing right-bearing
		       overall-direction next-start)
	       (type g:Screen-Coordinate width overall-ascent overall-descent))
      (values width overall-ascent overall-descent)))

(defmethod %string-width ((screen CLX-Screen) string font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-screen screen)
	     (type Simple-String string)
	     (type Font font))
    (let ((width (xlib:text-width (%screen-font screen font) string)))
      (declare (type g:Screen-Coordinate width))
      width))

(defmethod %string-screen-extent ((screen CLX-Screen) string font result)
  (declare (optimize (safety 1) (speed 3))
	   (type CLX-screen screen)
	   (type Simple-String string)
	   (type Font font)
	   (type g:Screen-Vector result))
  (multiple-value-bind
      (width ascent descent left-bearing right-bearing
       overall-ascent overall-descent overall-direction next-start)
      (xlib:text-extents (the xlib:Font (%screen-font screen font)) string)
    (declare (ignore ascent descent left-bearing right-bearing
		     overall-direction next-start)
	     (type g:Screen-Coordinate width overall-ascent overall-descent))
    (setf (g::%screen-vector-x result) width)
    (setf (g::%screen-vector-y result) (+ overall-ascent overall-descent))))

(defmethod %analyze-vertical-string-placement ((screen CLX-Screen) string font)
  (declare (optimize (safety 1) (speed 3))
	   (type CLX-screen screen)
	   (type Simple-String string)
	   (type Font font))
  (let ((width 0)
	(height 0)
	(xfont (%screen-font screen font)))
    (declare (type g:Positive-Screen-Coordinate width height)
	     (type xlib:Font xfont))
    (dotimes (i (length string))
      (let* ((xfont-index (char-code (char string i)))
	     (w (xlib:char-width xfont xfont-index))
	     (a (xlib:char-ascent xfont xfont-index))
	     (d (xlib:char-descent xfont xfont-index)))
	(declare (type Fixnum xfont-index)
		 (type g:Screen-Coordinate w a d))
	(az:maxf width w)
	;; except for first char, move down by this char's ascent
	;; a+d turns out to be exact character height, with no
	;; extra room, so add a pixel space for readability
	;; We should probably be using the font line height,
	;; or some such, here. As it is this doesn't do the right thing
	;; with spaces.
	(incf height (+ a d 1))))
    (values width 0 height)))

(defmethod %vertical-string-screen-extent ((screen CLX-Screen)
					   string font result)
  (declare (optimize (safety 1) (speed 3))
	   (type CLX-screen screen)
	   (type Simple-String string)
	   (type Font font)
	   (type g:Screen-Vector result))
  (let ((width 0)
	(height 0)
	(xfont (%screen-font screen font)))
    (declare (type g:Positive-Screen-Coordinate width height)
	     (type xlib:Font xfont))
    (dotimes (i (length string))
      (let* ((xfont-index (char-code (char string i)))
	     (w (xlib:char-width xfont xfont-index))
	     (a (xlib:char-ascent xfont xfont-index))
	     (d (xlib:char-descent xfont xfont-index)))
	(declare (type Fixnum xfont-index)
		 (type g:Screen-Coordinate w a d))
	(az:maxf width w)
	;; except for first char, move down by this char's ascent
	;; a+d turns out to be exact character height, with no
	;; extra room, so add a pixel space for readability
	;; We should probably be using the font line height,
	;; or some such, here. As it is this doesn't do the right thing
	;; with spaces.
	(incf height (+ a d 1))))
    (setf (g::%screen-vector-x result) width)
    (setf (g::%screen-vector-y result) height)))

(defmethod %vertical-string-width ((screen CLX-Screen) string font)
  (declare (optimize (safety 1) (speed 3))
	   (type CLX-screen screen)
	   (type Simple-String string)
	   (type Font font))
  (let ((xfont (%screen-font screen font))
	(width 0))
    (declare (type xlib:Font xfont)
	     (type g:Screen-Coordinate width))
    (dotimes (i (length string))
      (declare (type Fixnum i))
      (let ((w (xlib:char-width
		xfont
		(the Fixnum
		  (char-code (char string i))))))
	(declare (type g:Screen-Coordinate w))
	(az:maxf width w)))
    (values width)))

(defmethod %vertical-string-height ((screen CLX-Screen) string font)
    (declare (optimize (safety 1) (speed 3))
	     (type CLX-Screen screen)
	     (type Simple-String string)
	     (type Font font))
    (let ((xfont (%screen-font screen font))
	  (height 0))
      (declare (type xlib:Font xfont)
	       (type g:Screen-Coordinate height))
      (dotimes (i (length string))
	(declare (type Fixnum i))
	(let ((index (char-code (char string i))))
	  (declare (type Fixnum index))
	  (incf height (+ (the g:Screen-Coordinate
			    (xlib:char-ascent xfont index))
			  (the g:Screen-Coordinate
			    (xlib:char-descent xfont index))))))
      (values height)))

