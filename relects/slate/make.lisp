;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-

(in-package :cl-user)

;;;============================================================

(when (find-package :WT) (funcall (intern :composer :wt)))  

(load (truename
       (pathname
	(concatenate 'String
	  (namestring 
	   (make-pathname
	    :directory (pathname-directory *load-truename*)))
	  "../az.system"))))

(mk:compile-system :Slate)

;;;============================================================
#||

(load (truename
       (pathname
	(concatenate 'String
	  (namestring 
	   (make-pathname
	    :directory (pathname-directory *load-truename*)))
	  "../files.lisp"))))

(compile-all *tools-directory* *tools-files*) 
(compile-all *geometry-directory* *screen-geometry-files*) 

(compile-all *slate-directory* *slate-files*)
||#

