;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Font Descriptions
;;;============================================================

(eval-when (compile load eval)

  (deftype Font-Family ()
    "The 3 possible values for a <Font-Family> are :helvetica, :times,
and :fix."
    '(member :helvetica :times :fix))
  
  (deftype Font-Face ()
    "The possible values for a <Font-Face> are :roman, :bold, :italic,
or :bold-italic."
    '(member :roman :bold :italic :bold-italic))
  
  (deftype Font-Size ()
    "<Font-Size> is an integer between 0 and 255."
    '(Integer 0 255))

  (defparameter *default-font-size* 10)
  )

;;;------------------------------------------------------------

(defstruct (Font
	     (:conc-name nil)
	     (:constructor %make-font)
	     (:copier %copy-font)
	    (:print-function print-font))

  "A <Font> is essentially a triple of family, face and size,
represented by instances of the types <Font-Family>, <Font-Face>, and
<Font-Size>.  <Font>s are immutable, that is, once created, the
family, face, or size cannot be changed."

  (%font-family :fix :type Font-Family)
  (%font-face :roman :type Font-Face)
  (%font-size 10 :type Font-Size)
  (font-plist () :type List))

;;;------------------------------------------------------------
;;; Fonts are immutable:

(defun font-family (font)
  "Accessor for a <Font>'s family attribute."
  (az:declare-check (type Font font))
  (%font-family font))

(defsetf font-family (font) (family)
  "Attempting to use <setf> with <font-family> will signal an error."
  (error "Can't change a font's family: ~s ~s" font family))

(defun font-face (font)
  "Accessor for a <Font>'s face attribute."
  (az:declare-check (type Font font))
  (%font-face font))

(defsetf font-face (font) (face)
  "Attempting to use <setf> with <font-face> will signal an error."
  (error "Can't change a font's face: ~s ~s" font face))

(defun font-size (font)
  "Accessor for a <Font>'s size attribute."
  (az:declare-check (type Font font))
  (%font-size font))

(defsetf font-size (font) (size)
  "Attempting to use <setf> with <font-size> will signal an error."
  (error "Can't change a font's size: ~s ~s" font size))

;;;------------------------------------------------------------

;(eval-when (compile load eval) (proclaim '(Inline read-font-cache)))

(defun read-font-cache (font key)
  ;;(az:declare-check (type Font font))
  (getf (font-plist font) key))

(defsetf read-font-cache (font key) (value)
  `(progn
     ;;(az:declare-check (type Font ,font))
     (setf (getf (font-plist ,font) ,key) ,value)))

;;;------------------------------------------------------------
;;; Font descriptions are meant to be immutable, and can therefore
;;; be cached uniquely

(defparameter *font-family-table* (make-hash-table :test #'eq))

(defun font (&key (family :fix) (face :roman) (size 12))
  "This is the constructor for the <font> type."
  (az:declare-check (type Font-Family family)
		    (type Font-Face face)
		    (type Font-Size size))
  (let ((face-table (gethash family *font-family-table* nil)))
    (when (null face-table)
      (setf face-table (make-hash-table :test #'eq))
      (setf (gethash family *font-family-table* nil) face-table))
    (let ((size-table (gethash face face-table nil)))
      (when (null size-table)
	(setf size-table (make-hash-table :test #'eq))
	(setf (gethash face face-table nil) size-table))
      (let ((font (gethash size size-table nil)))
	(when (null font)
	  (setf font (%make-font
		      :%font-family family
		      :%font-face face
		      :%font-size size))
	  (setf (gethash size size-table nil) font))
	font))))

;;;============================================================

(defun print-font (font stream depth)
  (declare (ignore depth))
  (az:printing-random-thing
   (font stream)
   (format stream "Font ~a ~a ~a "
	   (font-family font)
	   (font-face font)
	   (font-size font))))

;;;------------------------------------------------------------
;;; This works as long as Fonts are really unique:

(defun equal-fonts? (f0 f1) (eql f0 f1))

;;;------------------------------------------------------------
;;; some standard fonts
;;; use functions to avoid constant objects in saved lisp images

(defun small-font        () (font :size 10))
(defun normal-font       () (font :size 12))
(defun large-font        () (font :size 14))
(defun large-italic-font () (font :face :italic :size 14))