;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Screens, based on X11 "visuals"
;;;============================================================

(defclass Screen (Slate-Object)
     ((screen-colormap
        :type Colormap
        :reader screen-colormap)
      (screen-font-cache
       :type Hash-Table
       :reader screen-font-cache
       :initform (make-hash-table :test #'eq))))

;;;------------------------------------------------------------

(defgeneric screen-pixel-depth (screen)
  (declare (type Screen screen))
  (:documentation "How many bits per pixel?"))

(defgeneric screen-ncolors (screen)
  (declare (type Screen screen))
  (:documentation "How many different colors can be on the screen?"))

(defmethod screen-ncolors ((screen Screen))
  (expt 2 (screen-pixel-depth screen)))

(defgeneric make-colormap-for (screen &rest options)
  (declare (type Screen screen)))

;;;------------------------------------------------------------

(defgeneric screen-origin (screen &key result)
  (declare (type Screen screen)
           (type g:Screen-Point result)))

(defgeneric screen-extent (screen &key result)
  (declare (type Screen screen)
           (type g:Screen-Vector result)))

(defgeneric screen-rect ((screen Screen) &key result)
  (declare (type Screen screen)
           (type g:Screen-Rect result)))

(defmethod screen-left ((screen Screen))
  (g:with-borrowed-screen-point (p)
    (g:screen-point-x (screen-origin screen :result p))))

(defmethod screen-top ((screen Screen))
  (g:with-borrowed-screen-point (p)
    (g:screen-point-y (screen-origin screen :result p))) )

(defmethod screen-right ((screen Screen))
  (g:with-borrowed-screen-rect (r)
    (g:screen-rect-right (screen-rect screen :result r))))

(defmethod screen-bottom ((screen Screen))
  (g:with-borrowed-screen-rect (r)
    (g:screen-rect-bottom (screen-rect screen :result r))))

(defmethod screen-width ((screen Screen))
  (g:with-borrowed-screen-rect (r)
    (g:screen-rect-width (screen-rect screen :result r))))

(defmethod screen-height ((screen Screen))
  (g:with-borrowed-screen-rect (r)
    (g:screen-rect-height (screen-rect screen :result r))))

;;;------------------------------------------------------------     
;;; Conversion between screen coordinates and real size
;;; (at least approximately). It's not yet decided if we should
;;; restrict these to the best Fixnum approximation, or allow
;;; Rationals, or even Floats.

(defgeneric pixels-per-inch-x (screen)
  (declare (type Screen screen)))

(defgeneric pixels-per-inch-y (screen)
  (declare (type Screen screen)))

(defgeneric pixels-per-milimeter-x (screen)
  (declare (type Screen screen)))

(defgeneric pixels-per-milimeter-y (screen)
  (declare (type Screen screen)))

;;;------------------------------------------------------------     
;;; for making tiles (later)

(defgeneric screen-preferred-tile-width (screen)
  (declare (type Screen screen)))

(defmethod screen-preferred-tile-width (screen)
  screen ;; compiler warnings that shouldn't be
  16)

(defgeneric screen-preferred-tile-height (screen)
  (declare (type Screen screen)))

(defmethod screen-preferred-tile-height (screen)
  screen ;; compiler warnings that shouldn't be
  16)

;;;============================================================
;;; Font, char , and string measurements
;;;============================================================
;;; Ther are all sorts of circularities in these default definitions
;;; that probably ought to be removed.

(defgeneric make-screen-font (screen font)
            (declare (type Screen screen)
                     (type Font font)))

(defun %screen-font (screen font)
  (declare (optimize (safety 1) (speed 3))
	   (type Screen screen)
	   (type Font font))
  (let ((s-font
	 (gethash font (the Hash-Table (screen-font-cache screen)) nil)))
    (when (null s-font)
      (setf s-font (make-screen-font screen font))
      (setf (gethash font (screen-font-cache screen)) s-font))
    s-font))

(defun screen-font (screen font)
  (az:declare-check (type Screen screen)
		    (type Font font))
  (%screen-font screen font))
   
;;;------------------------------------------------------------
;;; returns width, ascent, and descent as multiple values.

(defgeneric %analyze-character-placement (screen char font)
  (declare (type Screen screen)
           (type Character char)
           (type Font font)))

(defun analyze-character-placement (screen char font)
  (az:declare-check (type Screen screen)
		    (type Character char)
		    (type Font font))
  (%analyze-character-placement screen char font))

;;;------------------------------------------------------------

(defgeneric %character-screen-rect (screen char font origin result)
  (declare (type Screen screen)
           (type Character char)
           (type Font font)
           (type g:Screen-Point origin)
           (type g:Screen-Rect result)))

(defmethod %character-screen-rect (screen char font origin result)
  (multiple-value-bind (w a d) (%analyze-character-placement screen char font)
    (g::%set-screen-rect-coords
      result
       (g::%screen-point-x origin) (- (g::%screen-point-y origin) a)
       w (+ a d))))

(defun character-screen-rect (screen char font
                              &key
                              (position (g::%make-screen-point 0 0))
                              (result (g::%make-screen-rect 0 0 0 0)))
  (az:declare-check (type Screen screen)
		    (type Character char)
		    (type Font font)
		    (type g:Screen-Point position)
		    (type g:Screen-Rect result))
  (%character-screen-rect screen char font position result))

;;;------------------------------------------------------------

(defgeneric %character-width (screen char font)
  (declare (type Screen screen)
           (type Character char)
           (type Font font)))

(defmethod %character-width (screen char font)
  (multiple-value-bind (w a d) (%analyze-character-placement screen char font)
    (declare (ignore a d))
    w))

(defun character-width (screen char font)
  (az:declare-check (type Screen screen)
		    (type Character char)
		    (type Font font))
  (%character-width screen char font))

(defgeneric %character-height (screen char font)
  (declare (type Screen screen)
           (type Character char)
           (type Font font)))

(defmethod %character-height (screen char font)
  (multiple-value-bind (w a d) (%analyze-character-placement screen char font)
    (declare (ignore w))
    (+ a d)))

(defun character-height (screen char font)
  (az:declare-check (type Screen screen)
		    (type Character char)
		    (type Font font))
  (%character-height screen char font))

;;;------------------------------------------------------------

(defgeneric %character-ascent (screen char font)
  (declare (type Screen screen)
           (type Character char)
           (type Font font)))

(defmethod %character-ascent (screen char font)
  (multiple-value-bind (w a d) (%analyze-character-placement screen char font)
    (declare (ignore w d))
    a))

(defun character-ascent (screen char font)
  (az:declare-check (type Screen screen)
		    (type Character char)
		    (type Font font))
  (%character-ascent screen char font))

(defgeneric %character-descent (screen char font)
  (declare (type Screen screen)
           (type Character char)
           (type Font font)))

(defmethod %character-descent (screen char font)
  (multiple-value-bind (w a d) (%analyze-character-placement screen char font)
    (declare (ignore w a))
    d))

(defun character-descent (screen char font)
  (az:declare-check (type Screen screen)
		    (type Character char)
		    (type Font font))
  (%character-descent screen char font))

;;;------------------------------------------------------------
;;; returns string width, ascent, and descent, by iterating over the chars

(defgeneric %analyze-string-placement (screen string font)
            (declare (type Screen screen)
                     (type String string)
                     (type Font font)))

(defmethod %analyze-string-placement (screen string font)
  (let ((width 0)
        (maxascent 0)
        (maxdescent 0))
    (dotimes (i (length string))
      (multiple-value-bind
        (w a d) (%analyze-character-placement screen (char string i) font)
        (az:maxf maxascent a)
        (az:maxf maxdescent d)
        (incf width w)))
    (values width maxascent maxdescent)))

(defun analyze-string-placement (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%analyze-string-placement screen string font))

;;;------------------------------------------------------------

(defgeneric %string-screen-rect (screen string font position result)
  (declare (type Screen screen)
           (type String string)
           (type Font font)
           (type g:Screen-Point position)
           (type g:Screen-Rect result)) )

(defmethod %string-screen-rect (screen string font position result)
  (multiple-value-bind
      (w a d) (%analyze-string-placement screen string font)
    (g::%set-screen-rect-coords
     result
     (g::%screen-point-x position) (- (g::%screen-point-y position) a)
     w (+ a d))))

(defun string-screen-rect (screen string font
                           &key
                           (position (g::%make-screen-point 0 0))
                           (result (g::%make-screen-rect 0 0 0 0)))
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font)
		    (type g:Screen-Point position)
		    (type g:Screen-Rect result))
  (%string-screen-rect screen string font position result))

;;;------------------------------------------------------------

(defgeneric %string-screen-extent (screen string font result)
  (declare (type Screen screen)
           (type String string)
           (type Font font)
           (type g:Screen-Vector result)))

(defmethod %string-screen-extent (screen string font result)
  (multiple-value-bind
      (w a d) (%analyze-string-placement screen string font)
    (g::%set-screen-vector-coords result w (+ a d))))

(defun string-screen-extent (screen string font
			     &key (result (g::%make-screen-vector 0 0)))
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font)
		    (type g:Screen-Vector result))
  (%string-screen-extent screen string font result))

;;;------------------------------------------------------------

(defgeneric %string-width (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font)))

(defmethod %string-width ((screen Screen) string font)
  (multiple-value-bind (w a d) (%analyze-string-placement screen string font)
    (declare (ignore a d))
    w))

(defun string-width (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%string-width screen string font))

(defgeneric %string-height (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font)))

(defmethod %string-height (screen string font)
  (multiple-value-bind (w a d) (%analyze-string-placement screen string font)
    (declare (ignore w))
    (+ a d)))

(defun string-height (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%string-height screen string font))

;;;------------------------------------------------------------

(defun maximum-string-height (screen strings font)
  (az:declare-check (type Screen screen)
		    (type List strings)
		    (type Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%string-height screen string font))
      (when (> x max) (setf max x)))
    max))

(defun maximum-string-width (screen strings font)
  (az:declare-check (type Screen screen)
		    (type List strings)
		    (type Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%string-width screen string font))
      (when (> x max) (setf max x)))
    max))

;;;------------------------------------------------------------

(defgeneric %string-ascent (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font)))

(defmethod %string-ascent (screen string font)
  (multiple-value-bind (w a d) (%analyze-string-placement screen string font)
    (declare (ignore w d))
    a))

(defun string-ascent (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%string-ascent screen string font))

(defgeneric %string-descent (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font)))

(defmethod %string-descent (screen string font)
  (multiple-value-bind (w a d) (%analyze-string-placement screen string font)
    (declare (ignore w a))
    d))

(defun string-descent (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%string-descent screen string font))

;;;------------------------------------------------------------
;;; returns string width, ascent, and descent, by iterating over the chars

(defgeneric %analyze-vertical-string-placement (screen string font)
            (declare (type Screen screen)
                     (type String string)
                     (type Font font)))

(defmethod %analyze-vertical-string-placement (screen string font)
  (let (;;(1st-char-ascent (%character-ascent screen (char string 0) font))
        (width 0)
        (height 0))
    (dotimes (i (length string))
      (multiple-value-bind
        (w a d) (%analyze-character-placement screen (char string i) font)
        (az:maxf width w)
        (incf height (+ a d))))
    ;;(values width 1st-char-ascent (- height 1st-char-ascent))
    (values width 0 height)))

(defun analyze-vertical-string-placement (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%analyze-vertical-string-placement screen string font))

;;;------------------------------------------------------------
;;; A vertical string's region has the screen point at its upper left
;;; corner, not the left baseline of the first character.
;;; This is done to make it easier to draw a sequence of
;;; vertical strings without overlapping.

(defgeneric %vertical-string-screen-rect (screen string font position result)
  (declare (type Screen screen)
           (type String string)
           (type Font font)
           (type g:Screen-Point position)
           (type g:Screen-Rect result)))

(defmethod %vertical-string-screen-rect (screen string font position result)
  (multiple-value-bind
      (w a d) (%analyze-vertical-string-placement screen string font)
    (g::%set-screen-rect-coords
     result
     (g::%screen-point-x position) (g::%screen-point-y position)
     w (+ a d))))

(defun vertical-string-screen-rect (screen string font 
                                    &key
                                    (position (g:make-screen-point))
                                    (result (g:make-screen-rect)))
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font)
		    (type g:Screen-Point position)  
		    (type g:Screen-Rect result))
  (%vertical-string-screen-rect screen string font position result))

;;;------------------------------------------------------------

(defgeneric %vertical-string-screen-extent (screen string font result)
  (declare (type Screen screen)
           (type String string)
           (type Font font)
           (type g:Screen-Vector result)))

(defmethod %vertical-string-screen-extent (screen string font result)
  (multiple-value-bind
      (w a d) (%analyze-vertical-string-placement screen string font)
    (g::%set-screen-vector-coords result w (+ a d))))

(defun vertical-string-screen-extent (screen string font
				      &key
				      (result (g::%make-screen-vector 0 0)))
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font)
		    (type g:Screen-Vector result))
  (%vertical-string-screen-extent screen string font result))

;;;------------------------------------------------------------

(defgeneric %vertical-string-width (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font)))

(defmethod %vertical-string-width (screen string font)
  (multiple-value-bind
    (w a d) (%analyze-vertical-string-placement screen string font)
    (declare (ignore a d))
    w))

(defun vertical-string-width (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%vertical-string-width screen string font))

(defgeneric %vertical-string-height (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font)))

(defmethod %vertical-string-height (screen string font)
  (multiple-value-bind
    (w a d) (%analyze-vertical-string-placement screen string font)
    (declare (ignore w))
    (+ a d)))

(defun vertical-string-height (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%vertical-string-height screen string font))

;;;------------------------------------------------------------

(defun maximum-vertical-string-width (screen strings font)
  (az:declare-check (type Screen screen)
		    (type List strings)
		    (type Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%vertical-string-width screen string font))
      (when (> x max) (setf max x)))
    max))

(defun maximum-vertical-string-height (screen strings font)
  (az:declare-check (type Screen screen)
		    (type List strings)
		    (type Font font))
  (assert (every #'stringp strings))
  (let ((max 0) x)
    (dolist (string strings)
      (setf x (%vertical-string-height screen string font))
      (when (> x max) (setf max x)))
    max))

;;;------------------------------------------------------------

(defgeneric %vertical-string-ascent (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font))) 

(defmethod %vertical-string-ascent (screen string font)
  (multiple-value-bind
    (w a d) (%analyze-vertical-string-placement screen string font)
    (declare (ignore w d))
    a))

(defun vertical-string-ascent (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%vertical-string-ascent screen string font))

(defgeneric %vertical-string-descent (screen string font)
  (declare (type Screen screen)
           (type String string)
           (type Font font)))

(defmethod %vertical-string-descent (screen string font)
  (multiple-value-bind
    (w a d) (%analyze-vertical-string-placement screen string font)
    (declare (ignore w a))
    d))

(defun vertical-string-descent (screen string font)
  (az:declare-check (type Screen screen)
		    (type String string)
		    (type Font font))
  (%vertical-string-descent screen string font)) 

;;;============================================================

(defparameter *screens* ())

(defun add-screen-to-screens (screen)
  (az:declare-check (type Screen screen))
  (push screen *screens*))

(defun default-screen () (first *screens*))

(defun all-screens () (copy-list *screens*))

;;;============================================================
;;; 1st dimension of abstract screen types:

(defclass Read-Only-Colormap-Screen (Screen)
     ((screen-colormap
        :type Read-Only-Colormap
        :reader screen-colormap)))

(defmethod shared-initialize :after ((screen Read-Only-Colormap-Screen)
                                     slot-names
                                     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (setf (slot-value screen 'screen-colormap)
        (make-colormap-for screen)))

(defclass Read-Write-Colormap-Screen (Screen)
     ((screen-colormap
        :type Read-Write-Colormap
        :accessor screen-colormap)))

(defgeneric update-hardware-colormap (screen)
  (declare (type Read-Write-Colormap-Screen screen)))

(defmethod (setf screen-colormap) ((new-colormap Read-Write-Colormap)
				   (screen Read-Write-Colormap-Screen))
  (setf (slot-value screen 'screen-colormap) new-colormap)
  (update-hardware-colormap screen)
  new-colormap)

;;; 2nd dimension of abstract screen types:

(defclass Monochrome-Screen (Screen)
     ((screen-colormap
        :type Monochrome-Colormap
        :accessor screen-colormap)))

(defclass RGB-Screen (Screen)
     ((screen-colormap
        :type RGB-Colormap
        :accessor screen-colormap)))

(defun rgb-screen? (screen) (typep screen 'RGB-Screen))

(defclass Single-Index-RGB-Screen (RGB-Screen)
     ((screen-colormap
        :type Single-Index-RGB-Colormap
        :accessor screen-colormap)))

(defclass Pseudo-Color-Screen (Read-Write-Colormap-Screen
                                Single-Index-RGB-Screen)
     ((screen-colormap
        :type Pseudo-Colormap
        :accessor screen-colormap)))


(defclass Decomposed-Index-RGB-Screen (RGB-Screen) 
     ((screen-colormap
        :type Decomposed-Index-RGB-Colormap
        :accessor screen-colormap)))

;;; 6 basic screen classes:

(defclass Static-Gray-Screen (Read-Only-Colormap-Screen Monochrome-Screen)
     ((screen-colormap
        :type Static-Gray-Colormap
        :accessor screen-colormap)))


(defclass Static-Color-Screen (Read-Only-Colormap-Screen
                                Single-Index-RGB-Screen)
     ((screen-colormap
        :type Static-Colormap
        :accessor screen-colormap)))

(defclass True-Color-Screen (Read-Only-Colormap-Screen
                              Decomposed-Index-RGB-Screen)
     ((screen-colormap
        :type True-Colormap
        :accessor screen-colormap)))

#||
(defclass Gray-Scale-Screen (Read-Write-Colormap-Screen Monochrome-Screen)
     ((screen-colormap
        :type Gray-Scale-Colormap
        :accessor screen-colormap)))


(defclass Direct-Color-Screen (Read-Write-Colormap-Screen
                                Decomposed-Index-RGB-Screen)
     ((screen-colormap
        :type Direct-Colormap
        :accessor screen-colormap)))
||#

;;;============================================================
;;; The 3 most common special cases of above:

(defclass Bw-Screen (Static-Gray-Screen)
     ((screen-colormap
        :type Bw-Colormap
        :accessor screen-colormap)))

(defun bw-screen? (screen) (typep screen 'Bw-Screen))

(defmethod screen-pixel-depth ((screen Bw-Screen)) 1) 
(defmethod screen-ncolors ((screen Bw-Screen)) 2)

;;;------------------------------------------------------------     

(defmethod make-colormap-for ((screen Bw-Screen)
			      &rest options)
  (declare (ignore options))
  (make-instance 'Bw-Colormap))  

;;;============================================================

(defclass Byte-Pseudo-Color-Screen (Pseudo-Color-Screen)
     ((screen-colormap
       :type Byte-Pseudo-Colormap
       :accessor screen-colormap)))

;;;------------------------------------------------------------

(defmethod screen-pixel-depth ((screen Byte-Pseudo-Color-Screen)) 8)
(defmethod screen-ncolors ((screen Byte-Pseudo-Color-Screen)) 256)

;;;------------------------------------------------------------

(defgeneric fill-6-6-6-colormap (screen colormap))
(defmethod fill-6-6-6-colormap ((screen Byte-Pseudo-Color-Screen)
				colormap)
  (az:declare-check (type Colormap colormap))
  (error "not implemented"))

(defgeneric fill-3-3-2-colormap (screen colormap))
(defmethod fill-3-3-2-colormap ((screen Byte-Pseudo-Color-Screen)
				colormap)
  (az:declare-check (type Byte-Pseudo-Colormap colormap))
  (let ((colormap-vector (colormap-vector colormap))
	(rg-step (ash 65536 -3))
	(b-step (ash 65536 -2)))
    (dotimes (r 8)
      (dotimes (g 8)
	(dotimes (b 4)
	  (let ((i (+ (ash r 5) (ash g 2) b)))
	    (setf (aref colormap-vector i)
	      (rgb16->color (* r rg-step) (* g rg-step) (* b b-step)
			    :result (aref colormap-vector i))))))))
  colormap)

(defgeneric fill-colornames-colormap (screen colormap))
(defmethod fill-colornames-colormap ((screen Byte-Pseudo-Color-Screen)
				     colormap)
  (az:declare-check (type Byte-Pseudo-Colormap colormap))
  (let ((pixval 1)	
	(colormap-vector (colormap-vector colormap)))
    (dolist (c0 *colorname-plist*)
      (when (> pixval 254)
	(warn "More color names than colormap entries.")
	(return))
      ;; just skip over the names in the plist
      (when (typep c0 'Color)
	;; only add colormap entries for distinct colors
	(unless (find-if #'(lambda (c1) (equal-colors? c0 c1))
			 colormap-vector
			 :end pixval)
	  (setf (pixel-value->color colormap pixval) c0)
	  (incf pixval))))
    (let ((white (colorname->color :white)))
      (dotimes (i (- 256 pixval))
	(setf (pixel-value->color colormap (+ i pixval)) white))))      
  colormap)

(defgeneric fill-gray-scale-colormap (screen colormap))
(defmethod fill-gray-scale-colormap ((screen
						  Byte-Pseudo-Color-Screen)
						 colormap)
  (az:declare-check (type Byte-Pseudo-Colormap colormap))
  (let ((colormap-vector (colormap-vector colormap)))
    (dotimes (i 256)
      (setf (aref colormap-vector i)
	(rgb8->color i i i :result (aref colormap-vector i)))))
  colormap)

(defmethod make-colormap-for ((screen Byte-Pseudo-Color-Screen)
			      &rest options
			      &key
			      (initialization 'fill-colornames-colormap)
			      &allow-other-keys)
  (declare (ignore options))
  (let ((colormap (make-instance 'Byte-Pseudo-Colormap)))
    (funcall initialization screen colormap)
    colormap))

;;;============================================================

(defclass True-24bit-Color-Screen (True-Color-Screen)
	  ((screen-colormap
	    :type True-24bit-Colormap
	    :accessor screen-colormap)))


(defmethod screen-pixel-depth ((screen True-24bit-Color-Screen)) 24) 
(defmethod screen-ncolors ((screen True-24bit-Color-Screen)) (ash 1 24))

;;;------------------------------------------------------------     

(defmethod make-colormap-for ((screen True-24bit-Color-Screen)
			      &rest options)
  (declare (ignore options))
  (make-instance 'True-24bit-Colormap))  

;;;============================================================

(defparameter *bw-screen* nil)

(defun bw-screen (&optional (error? t))
  (when (null *bw-screen*)
    (setf *bw-screen* (find-if #'bw-screen? *screens*))
    (when (and error? (null *bw-screen*))
      (error "No bw screen!")))
  *bw-screen*)

(defparameter *rgb-screen* nil)  

(defun rgb-screen (&optional (error? t))
  (when (null *rgb-screen*)
    (setf *rgb-screen* (find-if #'rgb-screen? *screens*))
    (when (and error? (null *rgb-screen*))
      (error "No rgb screen!")))
  *rgb-screen*)

(defparameter *color-screen* nil)

(defun color-screen (&optional (error? t))
  (when (null *color-screen*)
    (setf *color-screen* (if (rgb-screen) (rgb-screen) (bw-screen)))
    (when (and error? (null *color-screen*))
      (error "No screens!")))
  *color-screen*)


