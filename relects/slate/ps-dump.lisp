;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================

(defun colormap-lit (colormap depth
		     &key
		     (result
		      (make-array
		       (colormap-length colormap)
		       :element-type `(Unsigned-Byte ,depth))))

  "Extract a vector of lightness values (the average of r, g, and b
scaled to a maximum of 2^pixel-depth) from a colormap."

  (az:declare-check (type Colormap colormap)
		    (type Vector result)
		    (:returns result))
  (let ((lut (colormap-vector colormap)))
    (declare (type (Simple-Array Color (*)) lut))
    (dotimes (i (colormap-length colormap))
      (setf (aref result i) (color->lit (aref lut i) depth))))
  result)

;;;============================================================

#+:xlib
(defun window-parent (w)
  (multiple-value-bind (children parent root) (xlib:query-tree w)
    (declare (ignore children root))
    parent))

#+:xlib
(defun whole-window (slate)
  (window-parent (slate-drawable slate)))

#+:xlib
(defun slate-image-array (slate whole-window?)

  "Return an array holding an image of the contents of the slate.  The
array may not have the same dimensions as the slate. In particular, if
<whole-window?> is not <nil>, then borders, title bars, etc., will be
included as well as the strictly Slate contents of the window.  Also
return the effective width and height of the window's image, which may
be smaller that the array size, due to padding."

  (az:declare-check (type Slate slate)
		    (:returns
		     (values (type (Array Number (* *)) slate-image)
			     (type az:Array-Index width height))))
  
  (let* ((win (if whole-window?
		  ;; then get the parent window,
		  ;; which includes the title bar,, etc.
		  (whole-window slate)
		;; else just the window itself
		(slate-drawable slate)))
	 (w (xlib:drawable-width win))
	 (h (xlib:drawable-height win)))
    (values
     (xlib:image-z-pixarray
      (xlib:get-image win :x 0 :y 0 :width w :height h :format :z-pixmap))
     w h)))

;;;============================================================

(defun ps-dump-header (stream width height depth iscale)

  "Dump an image with lower left corner at current origin, scaling by
iscale (iscale=1 means 1/300 inch per pixel)."

  (let ((boxwidth (ceiling (* width iscale 72) 300))
	(boxheight (ceiling (* height iscale 72) 300)))
    (format stream
	    "~
%!PS-Adobe-1.0
%%BoundingBox: 0 0 ~d ~d
%%EndComments
%%Pages: 0
%%EndProlog

gsave

% scale appropriately
~d ~d scale

% allocate space for one scanline of input
/picstr ~d string def

% read and dump the image
~d ~d ~d
[~d 0 0 ~d 0 ~d]
{ currentfile picstr readhexstring pop }
image
"
	    ;; bounding box:
	    boxwidth boxheight
	    ;; scaling
	    boxwidth boxheight
	    ;; length of string  to allocate
	    (ceiling (* width depth) 8)

	    ;; first args to image operator
	    width height depth

	    ;; transformation mtx
	    width (- height) height)

    (fresh-line stream)
    (fresh-line stream)))

;;;============================================================

(defconstant *hexdigits* "0123456789abcdef")
(declaim (type Simple-String *hexdigits*))

;;;-----------------------------------------------------------

(defparameter *ps-dump-buffer* "")

(defun ps-dump-buffer (size)
  (when (/= (length *ps-dump-buffer*) size)
    (setf *ps-dump-buffer* (make-string size :initial-element #\F)))
  *ps-dump-buffer*)

;;;-----------------------------------------------------------

(defun ps-dump-1bit-data (stream width height data lit)
  (declare (optimize (safety 1) (speed 3))
	   (type Stream stream)
	   (type az:Array-Index width height)
	   (type (Simple-Array Bit (* *)) data)
	   (type (Simple-Array Bit (*)) lit))
  (let* ((buflen (* (ceiling width 8) 2))
	 ;; We need to print an integer number of bytes for each scanline.
	 ;; Each byte is represented by 2 hex digit chars.
	 (buf (ps-dump-buffer buflen))
	 (nibble 0)
	 (ibuf 0)
	 (shift 3))
    (declare (type az:Array-Index buflen nibble ibuf)
	     (type Simple-String buf)
	     (type (Integer -1 3) shift))
    (dotimes (j height)
      (declare (type az:Array-Index j))
      (setf shift 3)
      (setf ibuf 0)
      (setf nibble 0)
      (dotimes (i width)
	(declare (type az:Array-Index i))
	(setf nibble
	  (logior nibble
		  (ash (the Bit (sbit lit (aref data j i)))
		       shift)))
	(decf shift)
	(when (< shift 0)
	  (setf (schar buf ibuf) (schar *hexdigits* nibble))
	  (setf nibble 0)
	  (setf shift 3)
	  (incf ibuf)))
      (unless (= shift 3) (setf (schar buf ibuf) (schar *hexdigits* nibble)))
      (fresh-line stream)
      (write-string buf stream))
    (fresh-line stream)))

;;;-----------------------------------------------------------

(defun ps-dump-4bit-data (stream width height data lit)
  (declare (optimize (safety 1) (speed 3))
	   (type Stream stream)
	   (type az:Array-Index width height)
	   (type (Simple-Array (Unsigned-Byte 4) (* *)) data)
	   (type (Simple-Array (Unsigned-Byte 4) (*)) lit))
  (let* ((buflen (* 2 (ceiling width 2)))
	 ;; need to print an integer number of bytes,
	 ;; each represented by 2 hex digits.
	 (buf (ps-dump-buffer buflen)))
    (declare (type Simple-String buf))
    (dotimes (j height)
      (declare (type az:Array-Index j))
      (dotimes (i width)
	(declare (type az:Array-Index i))
	(setf (schar buf i) (schar *hexdigits* (aref lit (aref data j i)))))
      (fresh-line stream)
      (write-string buf stream))
    (fresh-line stream)))

;;;-----------------------------------------------------------

(defun ps-dump-8bit-data (stream width height data lit)
  (declare (optimize (safety 1) (speed 3))
	   (type Stream stream)
	   (type az:Array-Index width height)
	   (type (Simple-Array (Unsigned-Byte 8) (* *)) data)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) lit))
  (dotimes (j height)
    (let ((buf (ps-dump-buffer (* 2 width)))
	  (2i 0)
	  (byte 0))
      (declare (type az:Array-Index j 2i byte)
	       (type Simple-String buf))
      (dotimes (i width)
	(declare (type az:Array-Index i))
	(setf byte (aref lit (aref data j i)))
	(setf (schar buf 2i) (schar *hexdigits* (ash byte -4)))
	(incf 2i)
	(setf (schar buf 2i) (schar *hexdigits* (logand byte #x0f)))
	(incf 2i))
      (fresh-line stream)
      (write-string buf stream)))
  (fresh-line stream))

;;;============================================================

(defun ps-dump-trailer (stream showpage?)
  (fresh-line stream)
  (fresh-line stream)
  (when showpage? (format stream "showpage"))
  (fresh-line stream)
  (format stream "grestore")
  (fresh-line stream)
  (format stream "%%Trailer")
  (fresh-line stream)
  (fresh-line stream))

;;;============================================================

(defun ps-dump-slate (slate file-name
		      &key
		      (scale 3)
		      (whole-window? nil)
		      (showpage? nil)
		      (depth (screen-pixel-depth (slate-screen slate))))

  "Dump the slate (as an image) to a postscript file.

<scale> determines the magnification from slate pixels to printer
pixels, eg., if <scale> is 1, then a 300x300 slate will print as a 1
inch x 1 inch bitmap on a 300 dpi printer.

If <whole-window?> is not <nil>, then borders, title bars, etc., will
be dumped as well as the strictly Slate contents of the window.

If <showpage?> is not <nil>, then a <showpage> instruction will be
appended to the file. <showpage?> should be <nil> for dumps that are
intended to be included in other documents as figures and should be
<t> if the dump is intended to be printed by itself.

<depth> determines the number of bits per pixel sent to the postscript
printer."

  (az:declare-check (type Slate slate)
		    (type String file-name)
		    (type (Integer 0 128) scale)
		    (type (Member 1 2 4 8) depth))
  (multiple-value-bind
      (data width height)(slate-image-array slate whole-window?)
    (let ((lit (colormap-lit (screen-colormap (slate-screen slate)) depth)))
      (with-open-file (stream file-name
		       :direction :output
		       :if-exists :supersede
		       :if-does-not-exist :create)
	(ps-dump-header stream width height depth scale)
	(ecase depth
	  (1 (ps-dump-1bit-data stream width height data lit))
	  (4 (ps-dump-4bit-data stream width height data lit))
	  (8 (ps-dump-8bit-data stream width height data lit)))
	(ps-dump-trailer stream showpage?))))
  t)

;;;============================================================
#||
(defun ps-dump-slate (slate file-name
		      &key
		      (width 8.5d0)
		      (height 11.0d0)
		      (scale 4)
		      (whole-window? nil)
		      ;;(reverse-video? nil)
		      (gray-scale nil)
		      )
  "Dump the contents of the <slate> to a postscript file.

<file-name> must be a string, not a pathname. (This could be easily
fixed.)

If <whole-window?> is nil, then just the interior of the <slate> is
dumped. Otherwise the borders, titlebar, etc., are included in the
dump.

If <gray-scale?> is non-nil, the colors (actually the gray levels) of
the pixels in the window are approximated by 4x4 bit patterns.
Otherwise each pixel is mapped to either black or white."

  (az:declare-check (type Slate slate)
		    (type String file-name)
		    (type (Double-Float 0.0d0 *) width height)
		    (type Fixnum scale)
		    (type (or Null Fixnum) gray-scale))

  (let* ((w (slate-drawable slate))
	 (wid (xlib:window-id
	       (if whole-window?
		   ;; then get the parent window,
		   ;; which includes the borders, etc.
		   (multiple-value-bind
		       (children parent root) (xlib:query-tree w)
		     (declare (ignore children root))
		     parent)
		 ;; else just the window itself
		 w)))
	 (command-string (format
			  nil
			  "xwd -id ~d  ~
                           | xpr -portrait -psfig -scale ~d ~
                                 -width ~f -height ~f -left 0.0d0 -top 0.0d0 ~
                                 -device ps -output ~a"
			  wid scale width height file-name)))
    (when gray-scale
      (setf command-string
	(concatenate 'String command-string
		      (format nil " -gray ~d" gray-scale))))
    (excl:run-shell-command command-string :wait t)))

||#