;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================

(defun random-color (&key (result (%make-color)))
 (set-color-coords result (random 65536) (random 65536) (random 65536)))
#||
(defun test-rgb->ihs ()
  (let ((r0 (random 1.0d0))
	(g0 (random 1.0d0))
	(b0 (random 1.0d0)))
    (multiple-value-bind
	(r1 g1 b1) (multiple-value-call #'ihs->rgb (rgb->ihs r0 g0 b0))
      (unless (and (= r0 r1)
		   (= g0 g1)
		   (= b0 b1))
	(format t "~% r: ~f ~f, g: ~f ~f, b: ~f ~f" r0 r1 g0 g1 b0 b1)))))

(defun test-color-transforms (n)
  (dotimes (i n) (test-rgb->ihs)))
||#
;;;============================================================

(defun random-font-family ()
  (ecase (random 3)
    (0 :helvetica)
    (1 :times)
    (2 :fix)))

(defun random-font-face ()
  (ecase (random 4)
    (0 :roman)
    (1 :bold)
    (2 :italic)
    (3 :bold-italic)))

(defun random-font-size () (+ 1 (random 32)))

(defun random-font ()
  (font :family (random-font-family)
	:face (random-font-face)
	:size (random-font-size)))

(defun test-random-fonts (pen slate &key (nfonts 4) (seconds 2))
  (let ((pen (copy-pen pen))
	(origin (g::%make-screen-point 32 32))
	(str ""))
    (dotimes (i nfonts)
      (setf (pen-font pen) (random-font))
      (setf str (format nil "~a ~a ~a"
			(font-family (%pen-font pen))
			(font-face (%pen-font pen))
			(font-size (%pen-font pen))))
      (clear-slate slate)
      (print str)
      (draw-string pen slate str origin)
      (draw-vertical-string pen slate str origin)
      (finish-drawing slate)
      (sleep seconds))))

(defun test-all-fonts (pen slate &key (print? nil) (seconds 0))
  (let ((pen (copy-pen pen))
	(origin (g::%make-screen-point 32 32))
	(str ""))
    (dolist (family '(:fix :times :helvetica))
      (dolist (face '(:roman :italic :bold :bold-italic))
	(loop for size from 4 to 24 do
	  (setf (pen-font pen) (font :family family
				     :face face
				     :size size))
	  (setf str (format nil "~a ~a ~a"
			    (font-family (%pen-font pen))
			    (font-face (%pen-font pen))
			    (font-size (%pen-font pen))))
	  (when print? (print str))
	  (clear-slate slate)
	  (draw-string pen slate str origin)
	  (draw-vertical-string pen slate str origin)
	  (finish-drawing slate)
	  (sleep seconds))))))

;;;============================================================
;;; test slates:

(defvar *slate* nil)

(defparameter *bw-slate* ())

(defun bw-slate (&optional (new? nil))
  (when (or new? (null *bw-slate*))
    (setf *bw-slate* (make-slate :screen (bw-screen))))
  *bw-slate*)

(defparameter *rgb-slate* ())

(defun rgb-slate (&optional (new? nil))
  (when (or new? (null *rgb-slate*))
    (setf *rgb-slate* (make-slate :screen (rgb-screen))))
  *rgb-slate*)

(defparameter *default-slate* ())

(defun default-slate (&optional (new? nil))
  (when (or new? (null *default-slate*))
    (setf *default-slate* (make-slate :screen (default-screen))))
  *default-slate*)

;;;============================================================

(defparameter *r0* (g::%make-screen-rect 0 0 10 10))
(defparameter *cr0* (g::%make-screen-rect 1 1 8 8))

(defparameter *r* (g::%make-screen-rect 0 0 100 100))
(defparameter *cr* (g::%make-screen-rect 1 1 98 98))
(defparameter *poly* (list (g::%make-screen-point 1 1)
			  (g::%make-screen-point 98 98)
			  (g::%make-screen-point 75 1)))
(defparameter *r1* (g::%make-screen-rect 10 10 100 100))

;;;============================================================

(defparameter *line-style-0*
    (make-line-style :line-style-thickness 0))

(defparameter *line-style-5*
    (make-line-style :line-style-thickness 5))

(defparameter *line-style-10*
    (make-line-style :line-style-thickness 10))

;;;============================================================
;;; test pens:

(defun get-fast-pen-pixel-value (colorname)
  (if (rgb-screen nil)
      (colorname->pixel-value (screen-colormap (rgb-screen nil))
			       colorname)
      ;; else
      (if (eql colorname :white) 0 1)))

(defparameter *zero-width-pen* (make-pen :pen-line-style *line-style-0*))
(defparameter *default-pen* (make-pen))
(defparameter *medium-pen* (make-pen :pen-line-style *line-style-5*))
(defparameter *wide-pen* (make-pen :pen-line-style *line-style-10*))

(defparameter *red-ior-pen* (make-pen
			      :pen-color (colorname->color :red)
			      :pen-operation boole-ior))
(defparameter *wide-red-pen* (make-pen :pen-color (colorname->color :red)
				       :pen-line-style *line-style-10*))

(defparameter *black-pen* (make-pen :pen-color (colorname->color :black)))
(defparameter *black-fill-pen* (make-pen :pen-fill-style *filling-style*
					:pen-color (colorname->color :black)))
(defparameter *xor-black-pen* (make-pen :pen-operation boole-xor
				       :pen-color (colorname->color :black)))
(defparameter *xor-black-fill-pen*
    (make-pen :pen-operation boole-xor :pen-fill-style *filling-style*
	      :pen-color (colorname->color :black)))

(defparameter *gray-pen* (make-pen :pen-color (colorname->color :gray)))
(defparameter *gray-fill-pen* (make-pen :pen-fill-style *filling-style*
					:pen-color (colorname->color :gray)))
(defparameter *xor-gray-pen* (make-pen :pen-operation boole-xor
				       :pen-color (colorname->color :gray)))
(defparameter *xor-gray-fill-pen*
    (make-pen :pen-operation boole-xor :pen-fill-style *filling-style*
	      :pen-color (colorname->color :gray)))

(defparameter *gray-filling-style*
    (make-fill-style :fill-style-type :tiled
		     :fill-style-tile (gray-tile)))

(defparameter *fast-gray-pen*
    (make-pen :pen-color (colorname->color :gray)
	      :pen-pixel-value (get-fast-pen-pixel-value :gray)))
(defparameter *fast-xor-gray-pen*
    (make-pen :pen-color (colorname->color :gray)
	      :pen-pixel-value (get-fast-pen-pixel-value :gray)
	      :pen-operation boole-xor))
(defparameter *xor-gray-tile-pen*
    (make-pen :pen-color (colorname->color :gray)
	      :pen-operation boole-xor
	      :pen-fill-style *gray-filling-style*))
(defparameter *gray-tile-pen*
    (make-pen :pen-color (colorname->color :gray)
	      :pen-fill-style *gray-filling-style*))

(defparameter *white-pen* (make-pen))
(defparameter *white-fill-pen* (make-pen :pen-fill-style *filling-style*))
(defparameter *xor-white-pen* (make-pen :pen-operation boole-xor))
(defparameter *xor-white-fill-pen*
	      (make-pen :pen-operation boole-xor
			:pen-fill-style *filling-style*))
(defparameter *fast-white-pen*
	      (make-pen :pen-pixel-value (get-fast-pen-pixel-value :white)))
(defparameter *fast-xor-white-pen*
	      (make-pen :pen-pixel-value (get-fast-pen-pixel-value :white)
			:pen-operation boole-xor))

(defparameter *yellow-pen* (make-pen :pen-color (colorname->color :yellow)))
(defparameter *xor-yellow-pen* (make-pen
				 :pen-color (colorname->color :yellow) 
				 :pen-operation boole-xor))
(defparameter *fast-yellow-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :yellow)))
(defparameter *fast-xor-yellow-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :yellow)
     :pen-operation boole-xor))

(defparameter *red-pen* (make-pen :pen-color (colorname->color :red)))
(defparameter *red-fill-pen*
	      (make-pen :pen-color (colorname->color :red)
			:pen-fill-style *filling-style*))
(defparameter *xor-red-pen* (make-pen
				 :pen-color (colorname->color :red) 
				 :pen-operation boole-xor))
(defparameter *fast-red-pen*
    (make-pen
      :pen-color (colorname->color :red)
     :pen-pixel-value (get-fast-pen-pixel-value :red)
     ))
(defparameter *fast-xor-red-pen*
    (make-pen
      :pen-color (colorname->color :red)
    :pen-pixel-value (get-fast-pen-pixel-value :red) 
     :pen-operation boole-xor))

(defparameter *fast-xor-red-fill-pen*
    (make-pen
      :pen-color (colorname->color :red)
     :pen-pixel-value (get-fast-pen-pixel-value :red)
     :pen-operation boole-xor
     :pen-fill-style *filling-style*))

(defparameter *ellipse-filling-style*
    (make-fill-style :fill-style-type :tiled
		     :fill-style-tile (ellipse-tile)))
(defparameter *fast-xor-red-ellipse-tile-pen* 
    (make-pen
     :pen-color (colorname->color :red)
     :pen-pixel-value (get-fast-pen-pixel-value :red) 
     :pen-operation boole-xor
     :pen-fill-style *ellipse-filling-style* ))

(Defparameter *green-pen* (make-pen :pen-color (colorname->color :green)))
(defparameter *green-fill-pen*
	      (make-pen :pen-color (colorname->color :green)
			:pen-fill-style *filling-style*))
(defparameter *xor-green-pen* (make-pen
				 :pen-color (colorname->color :green) 
				 :pen-operation boole-xor))
(defparameter *fast-green-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :green)))
(defparameter *fast-xor-green-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :green) 
     :pen-operation boole-xor))

(defparameter *blue-pen* (make-pen :pen-color (colorname->color :blue)))
(defparameter *blue-fill-pen*
    (make-pen  :pen-fill-style *filling-style*
	       :pen-color (colorname->color :blue)))
(defparameter *xor-blue-pen* (make-pen
				 :pen-color (colorname->color :blue) 
				 :pen-operation boole-xor))
(defparameter *fast-blue-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :blue)))
(defparameter *fast-xor-blue-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :blue)
     :pen-operation boole-xor))

(defparameter *brown-pen* (make-pen :pen-color (colorname->color :brown)))
(defparameter *xor-brown-pen* (make-pen
				 :pen-color (colorname->color :brown) 
				 :pen-operation boole-xor))
(defparameter *fast-brown-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :brown)))
(defparameter *fast-xor-brown-pen*
    (make-pen
     :pen-pixel-value (get-fast-pen-pixel-value :brown)
     :pen-operation boole-xor))

;;;============================================================

(defparameter *curve-length* 32)

(defun make-random-curve (&key (slate *slate*)
			       (length *curve-length*))
  (declare (type Slate slate)
	   (type g:Screen-Coordinate length))
  (let* ((w (slate-width slate))
	 (h (slate-height slate))
	 (x (slate-left slate))
	 (dx (truncate w length))
	 (top (slate-top slate))
	 (bottom (slate-bottom slate))
	 (y (+ top (random h)))
	 (dy (truncate h length))
	 (2dy (* 2 dy)))
    (declare (optimize (safety 1) (speed 3))
	     (type g:Screen-Coordinate w h x dx top bottom y dy 2dy))
    (az:with-collection
     (dotimes (i length)
       (declare (type g:Screen-Coordinate i))
       (az:collect (g::%make-screen-point x y))
       (incf x dx)
       (setf y (max top
		    (min bottom
			 (the g:Screen-Coordinate
			   (+ y
			      (the g:Screen-Coordinate
				(- (the g:Screen-Coordinate (random 2dy))
				   dy)))))))))))

(defun draw-curves (curves pen)
  (let ((i 0))
    (dolist (curve curves)
      (setf (pen-pixel-value pen) (mod i 256))
      (%draw-polyline pen *slate* curve)
      (incf i)))
  (%finish-drawing *slate*))

;;;============================================================

(defun test-copy-slate-rects (pen slate
			      &key
			      (nrects 3)
			      (npoints 3)
			      (seconds 2))
  (let* ((slate-rect (slate-rect slate))
	 (enclosing-rect (g::%make-screen-rect
			   (- (g:screen-rect-left slate-rect) 20)
			   (- (g:screen-rect-top slate-rect) 20)
			   (+ (g:screen-rect-width slate-rect) 25)
			   (+ (g:screen-rect-height slate-rect) 25)))
	 (rects (g::make-random-screen-rects enclosing-rect nrects))
	 (points (g::make-random-screen-points enclosing-rect npoints)))
    (dolist (r rects) (draw-ellipse pen slate r))
    (dolist (r rects)
      (dolist (p points)
	(sleep seconds)
	(copy-slate-rect pen slate r slate p)
	(finish-drawing slate)))))

;;;============================================================

(defun test-all-drawing-ops (pen slate
			     &key
			     (nscreen-points 10)
			     (seconds 2)
			     (print? t))

  (let* ((slate-rect (slate-rect slate))
	 (screen-rect (g::%make-screen-rect
			(- (g:screen-rect-left slate-rect) 20)
			(- (g:screen-rect-top slate-rect) 20)
			(+ (g:screen-rect-width slate-rect) 25)
			(+ (g:screen-rect-height slate-rect) 25)))
	 (r (round
	      (random (/ (+ (g:screen-rect-width screen-rect)
			    (g:screen-rect-height screen-rect)) 4.0d0))))
	 (r0 (random r))
	 (r1 (random r))
	 (screen-point0 (g::make-random-screen-point screen-rect))
	 (screen-point1 (g::make-random-screen-point screen-rect))
	 (screen-points (g::make-random-screen-points
			  screen-rect nscreen-points))
	 (screen-rect0 (g::make-random-screen-rect screen-rect))
	 (screen-rect1 (g::make-random-screen-rect screen-rect))
	 (fill-pen (copy-pen pen))
	 (tile-pen (copy-pen pen)))
    (setf (fill-style-type (pen-fill-style fill-pen)) :solid)
    (setf (fill-style-type (pen-fill-style tile-pen)) :tiled)
    (setf (fill-style-tile (pen-fill-style tile-pen)) (gray-tile))
    (when print? (print "draw-character"))
    (draw-character pen slate #\0 screen-point1)
    (draw-character pen slate #\1 screen-point0)
    (let ((code (char-code #\A)))
      (dolist (p screen-points)
	(draw-character pen slate (code-char code) p)
	(setf code (mod (+ code 1) 128))))

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-vertical-string"))
    (draw-points pen slate screen-points)
    (dolist (p screen-points)
      (draw-vertical-string
	pen slate
	(format nil "~s" (cons (g:screen-point-x p) (g:screen-point-y p)))
	p))
    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-string"))
    (dolist (p screen-points)
      (draw-point pen slate p)
      (draw-string
	pen slate
	(format nil "~s" (cons (g:screen-point-x p) (g:screen-point-y p)))
	p))

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-point"))
    (dolist (p screen-points) (draw-point pen slate p))

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-points"))
    (draw-points pen slate screen-points)
    (when print? (print "draw-line"))
    (draw-line pen slate screen-point0 screen-point1)
    (when print? (print "draw-polyline"))
    (draw-polyline pen slate screen-points)
    (when print? (print "draw-polygon"))
    (draw-polygon pen slate screen-points)
    (when print? (print "draw-filled-polygon"))
    (draw-polygon fill-pen slate screen-points)

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-tiled-polygon"))
    (draw-polygon tile-pen slate screen-points)

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-arrow"))
    (dolist (p screen-points) (draw-arrow pen slate screen-point0 p))
    (when print? (print "draw-rect"))
    (draw-rect pen slate screen-rect0)
    (draw-rect pen slate screen-rect1)
    (when print? (print "copy-slate-rect"))
    (copy-slate-rect pen slate screen-rect0 slate screen-point0)
    (copy-slate-rect pen slate screen-rect1 slate screen-point1)

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-filled-rect"))
    (draw-rect pen slate screen-rect0)
    (draw-rect pen slate screen-rect1)
    (draw-rect fill-pen slate screen-rect0)
    (draw-rect fill-pen slate screen-rect1)

    (finish-drawing slate) (sleep seconds) (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-tiled-rect"))
    (draw-rect pen slate screen-rect0)
    (draw-rect pen slate screen-rect1)
    (draw-rect tile-pen slate screen-rect0)
    (draw-rect tile-pen slate screen-rect1)
         
    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-ellipse"))
    (draw-rect pen slate screen-rect0)
    (draw-rect pen slate screen-rect1)
    (draw-ellipse pen slate screen-rect0)
    (draw-ellipse pen slate screen-rect1)

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-filled-ellipse"))
    (draw-rect pen slate screen-rect0)
    (draw-rect pen slate screen-rect1)
    (draw-ellipse fill-pen slate screen-rect0)
    (draw-ellipse fill-pen slate screen-rect1)

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-tiled-ellipse"))
    (draw-rect pen slate screen-rect0)
    (draw-rect pen slate screen-rect1)
    (draw-ellipse pen slate screen-rect0)
    (draw-ellipse pen slate screen-rect1)
    (draw-ellipse tile-pen slate screen-rect0)
    (draw-ellipse tile-pen slate screen-rect1)

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-circle"))
    (draw-circle pen slate screen-point0 r0) 
    (draw-circle pen slate screen-point1 r1) 

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-filled-circle"))
    (draw-circle pen slate screen-point0 r0) 
    (draw-circle pen slate screen-point1 r1) 
    (draw-circle fill-pen slate screen-point0 r0) 
    (draw-circle fill-pen slate screen-point1 r1) 

    (finish-drawing slate) (sleep seconds)
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (when print? (print "draw-tiled-circle"))
    (draw-circle pen slate screen-point0 r0) 
    (draw-circle pen slate screen-point1 r1) 
    (draw-circle tile-pen slate screen-point0 r0) 
    (draw-circle tile-pen slate screen-point1 r1) 
    ))

  
;;;============================================================
;; test horizontal and vertical lines.  Inscribed screen-rects are
;; drawn with xor so there are holes at the corners.

(defun test-hv-lines (pen slate)
  (az:declare-check (type Pen pen)
		    (type Slate slate))
  (let* ((maxx (1- (slate-width slate)))
	 (maxy (1- (slate-height slate)))
	 (inc 0)
	 (maxinc (truncate (min maxx maxy) 2))
	 (p0 (g::%make-screen-point 0 0))
	 (p1 (g::%make-screen-point 0 0))
	 (p2 (g::%make-screen-point 0 0))
	 (p3 (g::%make-screen-point 0 0)))
    (loop (when (> inc maxinc) (return))
      (let ((x2 (- maxx inc))
	    (y2 (- maxy inc)))
	(g::set-screen-point-coords p0 :x inc :y inc)
	(g::set-screen-point-coords p1 :x inc :y y2)
	(g::set-screen-point-coords p2 :x x2 :y y2)
	(g::set-screen-point-coords p3 :x x2 :y inc)
	(draw-line pen slate p0 p1)
	(draw-line pen slate p1 p2)
	(draw-line pen slate p2 p3)
	(draw-line pen slate p3 p0))
      (incf inc 2)))
  (finish-drawing slate))

;;;============================================================
;; test non-vertical or horizontal lines

(defun test-slanted-lines (pen slate)
  (az:declare-check (type Pen pen)
		    (type Slate slate))
  (let* ((x 1)
	 (y 6)
	 (maxx (truncate (slate-width slate) 2))
	 (maxy (truncate (slate-height slate) 2))
	 (width (1- (slate-width slate)))
	 (height (1- (slate-height slate)))
	 (screen-point0 (g::%make-screen-point 0 1))
	 (screen-point1 (g::%make-screen-point 0 (1- height))))
    (loop (when (> x maxx) (return))
      (setf (g:screen-point-x screen-point0) x)
      (setf (g:screen-point-x screen-point1) (- width x))
      (draw-line pen slate screen-point0 screen-point1)
      (rotatef (g:screen-point-x screen-point0)
	       (g:screen-point-x screen-point1))
      (draw-line pen slate screen-point0 screen-point1)
      (incf x 5))
    (setf (g:screen-point-x screen-point0) 1)
    (setf (g:screen-point-x screen-point1) (1- width))
    (loop (when (> y maxy) (return))
      (setf (g:screen-point-y screen-point0) y)
      (setf (g:screen-point-y screen-point1) (- height y))
      (draw-line pen slate screen-point0 screen-point1)
      (rotatef (g:screen-point-y screen-point0)
	       (g:screen-point-y screen-point1))
      (draw-line pen slate screen-point0 screen-point1)
      (incf y 5)))
  (finish-drawing slate))

;;;============================================================

(defun show-named-colors (slate)
  (let* ((plist *colorname-plist*)
	 (slate-width (slate-width slate))
	 (slate-height (slate-height slate))
	 (ncolors (/ (length *colorname-plist*) 2))
	 (nxsteps (truncate (sqrt ncolors)))
	 (nysteps (ceiling (/ ncolors nxsteps)))
	 (xstep (truncate slate-width nxsteps))
	 (y-step (truncate slate-height nxsteps))
	 (width (- xstep 2))
	 (height (- y-step 2))
	 (x 0)
	 (y 0)
	 (pen (make-pen :pen-fill-style *filling-style*)))
    (declare (special *colorname-plist*))
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (with-borrowed-color (c)
      (g:with-borrowed-screen-rect (r :width width :height height)
        (catch :done
	  (dotimes (i nxsteps)
	    (dotimes (j nysteps)
	      (when (null plist) (throw :done t))
	      (setf (pen-color pen)
		(colorname->color (first plist) :result c))
	      (setf plist (cddr plist))
	      (setf (g:screen-rect-left r) x)
	      (setf (g:screen-rect-top r) y)
	      (draw-rect pen slate r)
	      (incf x xstep))
	    (incf y y-step)
	    (setf x 0))))))
  (finish-drawing slate)) 

;;;============================================================

(defun show-colormap (slate)
  (declare (optimize (safety 1) (speed 3)))
  (let* ((slate-width (slate-width slate))
	 (slate-height (slate-height slate))
	 (xstep (truncate slate-width 16))
	 (y-step (truncate slate-height 16))
	 (width (- xstep 2))
	 (height (- y-step 2))
	 (x 0)
	 (y 0)
	 (pen (make-pen :pen-fill-style *filling-style*)))
    (clear-slate slate :screen-rect (slate-clipping-region slate))
    (g:with-borrowed-screen-rect
     (r :width width :height height)
     (dotimes (i 16)
       (dotimes (j 16)
	 (setf (pen-pixel-value pen) (+ j (* i 16)))
	 (setf (g:screen-rect-left r) x)
	 (setf (g:screen-rect-top r) y)
	 (draw-rect pen slate r)
	 (incf x xstep))
       (incf y y-step)
       (setf x 0))))
  (finish-drawing slate))

;;;============================================================

(defparameter *line-pnt0* (g::%make-screen-point 0 0))
(defparameter *line-pnt1* (g::%make-screen-point 0 0))
(defun test-lines (pen slate p v)
  ;; draw lines on two sides of a screen-rect specified by p, v
  (let ((p0 *line-pnt0*)
	(p1 *line-pnt1*)
	(x (g::%screen-point-x p))
	(y (g::%screen-point-y p))
	(w (g::%screen-vector-x v))
	(h (g::%screen-vector-y v)))
    (g::%set-screen-point-coords p0 (+ x -2) y)
    (g::%set-screen-point-coords p1 (+ x -2) (+ y h -1))
    (%draw-line pen slate p0 p1)
    (g::%set-screen-point-coords p0 x (+ y -2))
    (g::%set-screen-point-coords p1 (+ x w -1) (+ y -2))
    (%draw-line pen slate p0 p1))
  (finish-drawing slate))

(defparameter *fill-pen* (make-pen))
(defparameter *pen3* (make-pen))
(defparameter *pen4* (make-pen))

(defun test-ellipses (pen slate)
  (let ((x 5))
    (%copy-pen pen *fill-pen*)
    (%copy-pen pen *pen3*)
    (%copy-pen pen *pen4*)
    (setf (fill-style-type (%pen-fill-style *fill-pen*)) :solid)
    (setf (line-style-thickness (%pen-line-style *pen3*)) 3)
    (setf (line-style-thickness (%pen-line-style *pen4*)) 4)
    (g:with-borrowed-screen-rect (r)
				 (g::%with-borrowed-screen-point (p)
								 (g::%with-borrowed-screen-vector (v)
												  (dotimes (i 10)
												    (let ((i4 (* 4 i)) (y 5))
												      (g::%set-screen-point-coords p x y)
												      (g::%set-screen-vector-coords v i4 i4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-ellipse pen slate r)
												      (incf (g::%screen-point-y p) 50)
												      (incf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-ellipse pen slate r)
												      (incf (g::%screen-point-y p) 50)
												      (decf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-ellipse *fill-pen* slate r)
												      (incf (g::%screen-point-y p) 50)
												      (incf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-ellipse pen slate r)
												      (incf (g::%screen-point-y p) 50)
												      (decf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-ellipse *pen3* slate r)
												      (incf (g::%screen-point-y p) 50)
												      (incf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-ellipse *pen4* slate r)
												      (incf x (+ 5 i4))))
												  (dotimes (i 10)
												    (let ((i4 (* 4 i)) (y 5))
												      (g::%set-screen-point-coords p x y)
												      (g::%set-screen-vector-coords v i4 i4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-rect pen slate r)
												      (incf (g::%screen-point-y p) 50)
												      (incf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-rect pen slate r)
												      (incf (g::%screen-point-y p) 50)
												      (decf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-rect *fill-pen* slate r)
												      (incf (g::%screen-point-y p) 50)
												      (incf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-rect *fill-pen* slate r)
												      (incf (g::%screen-point-y p) 50)
												      (decf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-rect *pen3* slate r)
												      (incf (g::%screen-point-y p) 50)
												      (incf (g::%screen-vector-y v) 4)
												      (g::%set-screen-rect-origin-extent r p v)
												      (test-lines pen slate p v)
												      (%draw-rect *pen4* slate r)
												      (incf x (+ 5 i4))))))))
  (finish-drawing slate))

;;;============================================================

(defun test-copy-slate-rect (slate)
  (let ((x 10)
	(screen (slate-screen slate)))
    (dolist (from-slate (list (tile-screen-slate (gray-tile) screen)
			      (tile-screen-slate (ellipse-tile) screen)
			      (tile-screen-slate (boxtile) screen)
			      (tile-screen-slate (star-tile) screen)
			      (tile-screen-slate (cross-tile) screen)))
      (copy-slate-rect *default-pen* from-slate
		       (g::%make-screen-rect 0 0 32 32)
			slate (g::%make-screen-point x 40))
      (copy-slate-rect *default-pen* from-slate
		       (g::%make-screen-rect 8 8 32 32)
			slate (g::%make-screen-point x 80))
      (copy-slate-rect *default-pen* from-slate
		       (g::%make-screen-rect 16 16 32 32)
			slate (g::%make-screen-point x 120))
      (incf x 40)))
  (finish-drawing slate))

;;;============================================================
#||
(defun trans-rgb-names (inpath outpath)
  (with-open-file (infile inpath :direction :input)
    (with-open-file ( outfile outpath
		     :direction :output
		     :if-does-not-exist :create
		     :if-exists :supersede) 
      (let (r g b namestring)
	(loop
	  (setf r (read infile nil nil))
	  (when (null r) (return))
	  (setf g (read infile))
	  (setf b (read infile))
	  (setf namestring
		(string-trim
		  '(#\Tab #\Space #\Newline #\Return)
		  (nsubstitute #\- #\Space
			       (read-line infile))))
      
	  (format outfile 
		  "~%(defconstant *~A* '#(~d ~d ~d))"
		  namestring r g b))))))

(defun colorname-table (inpath)
  (with-open-file (infile inpath :direction :input)
     (az:with-collection
      (let (r g b namestring name)
	(loop
	  (setf r (read infile nil nil))
	  (when (null r) (return))
	  (setf g (read infile))
	  (setf b (read infile))
	  (setf namestring
		(string-upcase
		  (string-trim
		  '(#\Tab #\Space #\Newline #\Return)
		  (nsubstitute #\- #\Space
			       (read-line infile)))))
	  (setf name (intern namestring :Keyword))
	  (az:collect (cons name (vector r g b))))))))

(defun colornames (inpath)
  (with-open-file (infile inpath :direction :input)
    (az:with-collection
      (loop
	(when (null (read infile nil nil)) (return))
	(read infile)
	(read infile) 
	(az:collect
	  (intern
	    (string-upcase
	      (string-trim '(#\Tab #\Space #\Newline #\Return)
			   (nsubstitute #\- #\Space
					(read-line infile)))) :Keyword))))))

(defun trans-rgb-names (inpath outpath)
  (with-open-file (infile inpath :direction :input)
    (with-open-file ( outfile outpath
		     :direction :output
		     :if-does-not-exist :create
		     :if-exists :supersede)
      (format outfile "~%(defparameter *colorname-alist* '(")
      (let (r g b namestring)
	(loop (setf r (read infile nil nil))
	      (when (null r) (return))
	      (setf g (read infile))
	      (setf b (read infile))
	      (setf namestring
		    (string-trim '(#\Tab #\Space #\Newline #\Return)
				 (nsubstitute #\- #\Space
					      (read-line infile))))
	      (format outfile "~%:~A '#(~d ~d ~d)"
		      namestring r g b)))
      (format outfile ")"))))

||#