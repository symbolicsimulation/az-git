;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================

(load (truename
       (pathname
	(concatenate 'String
	  (namestring 
	   (make-pathname
	    :directory (pathname-directory excl::*source-pathname*)))
	  "../files.lisp"))))

(load-all *tools-directory* *tools-files*) 
(load-all *geometry-directory* *screen-geometry-files*)

(defvar *load-pcl?*)
(setf *load-pcl?* (not (find-package "PCL")))

(when *load-pcl?*
  #+symbolics (compile-if-needed *fixredefine-warnings-file*)
  (compile-if-needed *pcl-defsys-file*)
  #+:coral (let ((*warn-if-redefine-kernel* nil))
             (declare (special *warn-if-redefine-kernel*))
             (pcl::load-pcl))
  #-:coral (pcl::load-pcl))

;; make sure that we have all tools
#+excl (shadow '(wt::object) (find-package :wt))
(load-all *clos-tools-directory* *clos-tools-files*) 

(load-all *slate-directory* *slate-files*) 

