;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Slate's
;;;============================================================

(defclass CLX-Output-Slate (Slate)
	  ((slate-width
	    :type g:Positive-Screen-Coordinate
	    :reader slate-width)
	   (slate-height
	    :type g:Positive-Screen-Coordinate
	    :reader slate-height)
	   (slate-drawable
	    :type xlib:Drawable
	    :reader slate-drawable
	    :initarg :slate-drawable)
	   ))

;;;============================================================
;;; a non-trivial writer method for slate-background-pixel-value

(defmethod (setf slate-background-pixel-value) (pixval
						(slate CLX-Output-Slate))
  (ac:atomic
   (setf (slot-value slate 'slate-background-pixel-value) pixval)
   (setf (xlib:window-background (slate-drawable slate)) pixval)))

;;;------------------------------------------------------------

(defmethod (setf slate-name) (name (slate CLX-Output-Slate))
  (az:declare-check (type (or String Symbol) name)
		    (type (not Invisible-Slate) slate))
  (let ((w (slate-drawable slate)))
    (az:declare-check (type xlib:Window w))
    (setf (slot-value slate 'slate-name) name)
    (setf (xlib:wm-name w) (slate-name slate))
    (setf (xlib:wm-icon-name w) (slate-name slate))))

;;;============================================================

(defclass CLX-Slate (CLX-Output-Slate Input-Slate) ())

;;;------------------------------------------------------------

(defmethod az:kill-object ((slate CLX-Slate)
			   &rest options
			   &key (destroy-window t))
  (declare (ignore options))
  ;;(format t "~%killing slt: ~a" slate) (finish-output)
  (when destroy-window
    ;;(format t "~%destroying window: ~a" (slate-drawable slate))
    (xlib:destroy-window (slate-drawable slate))
    (finish-drawing slate))

  ;;(format t "~%(typep slate 'Dead-Slate) = ~a" (typep slate 'Dead-Slate))
  ;;(finish-output)
  (unless (typep slate 'Dead-Slate) (change-class slate 'Dead-Slate))
  slate)

;;;============================================================

(defclass Invisible-CLX-SLate (CLX-Output-SLate Invisible-Slate) ())

;;;============================================================

(defmethod az:kill-object ((slate Invisible-CLX-SLate) &rest options)
  (declare (ignore options))
  (xlib:free-pixmap (slate-drawable slate))
  (unless (typep slate 'Dead-Slate) (change-class slate 'Dead-Slate))
  slate)

;;;============================================================

(defmethod make-slate-for-screen ((screen CLX-Screen)
				  &rest options
				  &key
				  (left -1) (top -1)
				  (width -1) (height -1)
				  (slate-name (gentemp "SLATE-"))
				  (backing-store :not-useful)
				  (background-color (colorname->color :white))
				  &allow-other-keys)
  (declare (ignore options))
  (let* ((xscreen (xscreen screen))
	 (colormap (screen-colormap screen))
	 (color-metric (make-color-metric colormap
					  :background-color background-color))
	 (background-pixel-value
	  (color->pixel-value colormap background-color :metric color-metric))
	 (w (xlib:create-window
	     :parent (xlib:screen-root xscreen)
	     :background background-pixel-value
	     :border (xlib:screen-black-pixel xscreen)
	     :border-width 1
	     :colormap (xcolormap screen)
	     :bit-gravity :north-west
	     :x left :y top
	     :width (if (minusp width) 256 width)
	     :height (if (minusp height) 256 height)
	     ;;:override-redirect :on
	     :cursor (arrow-cursor screen)
	     :do-not-propagate-mask 0
	     :backing-store (ecase (xlib:screen-backing-stores xscreen)
			      (:always backing-store)
			      (:when-mapped
			       (if (eq backing-store :when-mapped)
				   :when-mapped :not-useful))
			      (:never :not-useful))))
	 (slate (make-instance 'CLX-Slate
		  :slate-screen screen
		  :slate-drawable w
		  :slate-name slate-name
		  :slate-background-color background-color
		  :slate-color-metric color-metric
		  :slate-background-pixel-value background-pixel-value)))
    (update-slate-extent-from-drawable slate)

    ;; Set the window's name and icon name to be the same
    ;; as the slate's.
    (setf (xlib:wm-name w) (slate-name slate))
    (setf (xlib:wm-icon-name w) (slate-name slate))
    (setf (xlib:wm-protocols w) '(:wm_delete_window))

    ;; set the wm hints so the window appears at the specified position,
    ;; unless either position is negative, in which case put it at the mouse
    ;; position:
    (setf (xlib:wm-normal-hints w)
      (xlib:make-wm-size-hints
       ;; :base-width (if (minusp width) 256 width)
       ;; :base-height (if (minusp height) 256 height)
       :user-specified-position-p (not (or (minusp left) (minusp top)))
       :user-specified-size-p (not (or (minusp width) (minusp height)))
       :x (max 0 left) :y (max 0 top)
       :width (if (minusp width) 256 width)
       :height (if (minusp height) 256 height)       
       ))

    ;; set up event handling
    (setf (xlib:window-override-redirect w) :off)

    ;; map the window
    (xlib:map-window w)
    ;; wait until the window is actually on the screen before returning
    (loop (when (eq (xlib:window-map-state w) :viewable) (return)))
    slate))

;;;============================================================
  
(defmethod make-invisible-slate-for-screen ((screen CLX-Screen)
					    &rest options
					    &key
					    (width
					     (screen-preferred-tile-width
					      screen))
					    (height
					     (screen-preferred-tile-height
					      screen))
					    (background-color
					     (colorname->color :white))
					    &allow-other-keys)
  (declare (ignore options)
           (optimize (safety 1) (speed 3))
	   (type CLX-Screen screen)
	   (type g:Positive-Screen-Coordinate width height))
  
  (let* ((xscreen (xscreen screen))
	 (colormap (screen-colormap screen))
	 (color-metric (make-color-metric colormap
					  :background-color background-color))
	 (pixmap (xlib:create-pixmap
		  :drawable (xlib:screen-root xscreen)
		  :depth (screen-pixel-depth screen)
		  :width width
		  :height height))
	 (slate
	  ;; try avoiding initialization overhead:
	  (allocate-instance (find-class 'Invisible-CLX-SLate))))
    (declare (type xlib:Screen xscreen)
	     (type xlib:Pixmap pixmap)
	     ;; (type xlib:Gcontext gc)
	     (type Invisible-CLX-SLate slate))
    (setf (slot-value slate 'slate-point) (g::%make-screen-point 0 0))
    (setf (slot-value slate 'slate-rect) (g::%make-screen-rect 0 0 0 0))
    (setf (slot-value slate 'slate-clipping-rect) nil)
    (setf (slot-value slate 'slate-origin) (g::%make-screen-point 0 0))
    (setf (slot-value slate 'slate-screen) screen)
    (setf (slot-value slate 'slate-width) width)
    (setf (slot-value slate 'slate-height) height)
    (setf (slot-value slate 'slate-drawable) pixmap)
    (setf (slot-value slate 'slate-background-color) background-color)
    (setf (slot-value slate 'slate-color-metric) color-metric)
    (setf (slot-value slate 'slate-background-pixel-value)
      (color->pixel-value colormap background-color :metric color-metric))
    ;;(setf (slot-value slate 'slate-gcontext) gc)
    ;;(setf (getf (xlib:gcontext-plist gc) 'pen) ())
    ;;(setf (getf (xlib:gcontext-plist gc) 'pen-time) (az:make-timestamp))
    (clear-slate slate)
    slate))

;;;============================================================
;;; Accessing slate screen-rect
;;;============================================================

(defun update-slate-extent-from-drawable (slate)
  (let ((drawable (slate-drawable slate)))
    (xlib:with-state
     (drawable)
     (setf (slot-value slate 'slate-width)
       (xlib:drawable-width drawable))
     (setf (slot-value slate 'slate-height)
       (xlib:drawable-height drawable)))))

(defmethod slate-rect ((slate CLX-Output-Slate))
  (declare (optimize (safety 1) (speed 3))
	   (type CLX-Output-Slate slate))
  (let ((r (slot-value slate 'slate-rect)))
    (declare (type g:Screen-Rect r))
    (setf (g::%screen-rect-xmin r) 0)
    (setf (g::%screen-rect-ymin r) 0)
    (setf (g::%screen-rect-width r) (slate-width slate))
    (setf (g::%screen-rect-height r) (slate-height slate))
    r))

  