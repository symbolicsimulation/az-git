;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; Color Descriptions
;;;============================================================

(deftype Array-Index () '(Integer 0 #.array-dimension-limit))

(deftype Color-Level () '(Unsigned-Byte 16))

(defstruct (Color
	    (:constructor %make-color)
	    (:copier %color-copier)
	    (:print-function print-color))

  "A Color is basically a triple of unsigned 16 bit RGB values (like
in X11). It provides a standard, internal representation for colors.
Color can be created by giving a triple of 16bit RGB values, a triple
of 8bit RGB values (which is dangerous, because it's easy to confuse
the two, a triple of [0.0d0,1.0d0] floating point RGB values, a triple
of [0.0d0,1.0d0] floating point IHS values, a triple of [0.0d0,1.0d0]
floating point Hexcone-IHS values, a triple of [0.0d0,1.0d0] floating
point YIQ values, or a keyword that's defined in the named color
database."

  (r16 0 :type Color-Level)
  (g16 0 :type Color-Level)
  (b16 0 :type Color-Level)
  (time (az:make-timestamp) :type az:Timestamp))

;;;============================================================

(defun print-color (color stream depth)
  (declare (ignore depth))
  (format stream "{Color ~d ~d ~d}"
	  (color-r16 color) (color-g16 color) (color-b16 color)))

;;;============================================================

(eval-when (compile load eval) (proclaim '(inline set-color-coords)))

(defun set-color-coords (color r16 g16 b16)
  (declare (optimize (safety 1) (speed 3))
	   (type Color color)
	   (type Color-Level r16 g16 b16))
  (ac:atomic
   (setf (color-r16 color) r16)
   (setf (color-g16 color) g16)
   (setf (color-b16 color) b16)
   (az:stamp-time! (color-time color)))
  color)

;;;============================================================

(defun %copy-color-to! (color result)
  (set-color-coords result
		    (color-r16 color) (color-g16 color) (color-b16 color))
  (values result))

(defun copy-color (color &key (result (%make-color)))
  (az:declare-check (type Color color result))
  (%copy-color-to! color result)
  (values result))

;;;============================================================

(defun %equal-colors? (c0 c1)
  (or (eql c0 c1)
      (and (= (color-r16 c0) (color-r16 c1))
	   (= (color-g16 c0) (color-g16 c1))
	   (= (color-b16 c0) (color-b16 c1)))))

(defun equal-colors? (c0 c1)
  (az:declare-check (type Color c0 c1))
  (%equal-colors? c0 c1))

(defun %black-color? (color)
  (and (zerop (color-r16 color))
       (zerop (color-g16 color))
       (zerop (color-b16 color))))

(defun black-color? (color)
  (az:declare-check (type Color color))
  (%black-color? color))

;;;============================================================

(eval-when (compile load eval) (proclaim '(Inline %color-l1-dist )))

(defun %color-l1-dist (c0 c1)
  (declare (optimize (safety 1) (speed 3))
	   (type Color c0 c1))
  (+ (the Fixnum (abs (the Fixnum (- (color-r16 c0) (color-r16 c1)))))
     (the Fixnum (abs (the Fixnum (- (color-g16 c0) (color-g16 c1)))))
     (the Fixnum (abs (the Fixnum (- (color-b16 c0) (color-b16 c1)))))))

(defun color-l1-dist (c0 c1)
  "This is the default color metric for color screens."
  (az:declare-check (type Color c0 c1))
  (%color-l1-dist c0 c1))
 
;;;============================================================
;;; Conversions between color coordinate systems:
#|| this stuff is wrong!!! best to start over again....

(defconstant *ihs-rgb-c1* (sqrt (/ 6.0d0)))
(defconstant *ihs-rgb-c2* (sqrt (/ 2.0d0)))
(defconstant *ihs-rgb-c3* (sqrt (/ 3.0d0)))

(defun rgb->ihs (r g b)
  (az:declare-check (type (Double-Float 0.0d0 1.0d0) r g b))
  (let* ((x (* *ihs-rgb-c1* (- (+ r r) b g)))
	 (y (* *ihs-rgb-c2* (- g b)))
	 (z (* *ihs-rgb-c3* (+ r g b)))
	 (q (+ (* x x) (* y y)))
	 (hh 0.0d0)
	 (sat 0.0d0)
	 (int (sqrt (+ q (* z z)))))
    (cond ((zerop q)
	   (setf hh 0.0d0)
	   (setf sat 0.0d0))
	  (t
	   (setf hh (/ (+ (atan y x) pi (* 2.0d0 pi))))
	   (setf hh (mod (+ hh 0.5d0) 1.0d0))
	   (setf sat
		 (let ((f (/ z int)))
		   (atan (sqrt (- 1.0d0 (* f f))) f)))))
    (values int hh sat)))

(defun ihs->rgb (int hh sat)
  (let* ((hh (mod (- hh 0.5d0) 1.0d0))
	 (hh (- (* hh 2.0d0 pi) pi))
	 (s3 (sin sat))
	 (z (* *ihs-rgb-c3* (cos sat) int))
	 (x (* *ihs-rgb-c1* s3 (cos hh) int))
	 (y (* *ihs-rgb-c2* s3 (sin hh) int)))
    (values (az:bound 0.0d0 (+ x x z) 1.0d0)
	    (az:bound 0.0d0 (+ y z (- x)) 1.0d0)
	    (az:bound 0.0d0 (- z x y) 1.0d0))))

;;;------------------------------------------------------------

(defun rgb->yiq (r g b)
  (values (+ (* 0.30 r) (* 0.59 g) (* 0.11 b))
	  (+ (* 0.60 r) (* -0.28 g) (* -0.32 b))
	  (+ (* 0.21 r) (* -0.52 g) (* 0.31 b))))

(defun yiq->rgb (y i q)
  (values (az:bound 0.0d0 (+ (* 0.948262244 i) (* 0.62401264 q) y) 1.0d0)
	  (az:bound 0.0d0 (+ (* -0.27606635 i) (* -0.63981043 q) y) 1.0d0)
	  (az:bound 0.0d0 (+ (* -1.10545024 i) (* 1.72985782 q) y) 1.0d0)))

;;;------------------------------------------------------------
;;; from alvy ray smith siggraph 1978

(defun rgb->hexcone-ihs (r g b)
  (if (and (zerop r) (zerop g) (zerop b))
      (values 0.0d0 0.0d0 0.0d0)
      (let* ((v (max r g b))
	     (x (min r g b))
	     (sat (/ (- v x) v)))
	(if (not (zerop sat))
	    (let* ((lr (/ (- v r) (- v x)))
		   (lg (/ (- v g) (- v x)))
		   (lb (/ (- v b) (- v x)))
		   (hh (cond ((= r v) (if (= g x) (+ 5 lb) (- 1 lg)))
			     ((= g v) (if (= b x) (+ 1 lr) (- 3 lb)))
			     (t (if (= r x) (+ 3 lg) (- 5 lr))))))
	      (values v (/ hh 6.0d0) sat))
	    (values v 0.0d0 0.0d0)))))

(defun hexcone-ihs->rgb (v h ss)
  (multiple-value-bind (int f) (floor (* 6 h))
    (let* ((m (* v (- 1.0d0 ss)))
	   (n (* v (- 1.0d0 (* ss (if (logbitp 1 int) f (- 1 f)))))))
      (case int
	(1 (values n v m))
	(2 (values m v n))
	(3 (values m n v))
	(4 (values n m v))
	(5 (values v m n))
	(otherwise (values v n m))))))

||#
;;;============================================================
;;; Specifying colors in different color coordinate systems:

(defun rgb16->color (r16 g16 b16 &key (result (%make-color)))
  (az:declare-check (type (Unsigned-Byte 16) r16 g16 b16)
		    (type Color result))
  (set-color-coords result r16 g16 b16)
  (values result))

(defun color->rgb16 (color)
  "Compute the 16 bit RGB coordinates of a color object."
  (az:declare-check (type Color color))
  (values
   (color-r16 color)
   (color-g16 color)
   (color-b16 color)))

(defun rgb8->color (r8 g8 b8 &key (result (%make-color)))
  (az:declare-check (type (Unsigned-Byte 8) r8 g8 b8)
		    (type Color result))
  (set-color-coords result (ash r8 8) (ash g8 8) (ash b8 8))
  (values result))

(defun color->rgb8 (color)
  "Compute the 8 bit RGB coordinates of a color object."
  (az:declare-check (type Color color))
  (values
   (ash (color-r16 color) -8)
   (ash (color-g16 color) -8)
   (ash (color-b16 color) -8)))

(defun rgb->color (r g b &key (result (%make-color)))
  "This is the basic function for creating or modifying a color object."
  (az:declare-check (type (Double-Float 0.0d0 1.0d0) r g b)
		    (type Color result))
  (set-color-coords result
		    (round (* (az:fl 65535) r))
		    (round (* (az:fl 65535) g))
		    (round (* (az:fl 65535) b)))
  (values result))

(defun color->rgb (color)
  "Compute the RGB coordinates of a color object."
  (az:declare-check (type Color color))
  (values
   (/ (az:fl (color-r16 color)) (az:fl 65535)) 
   (/ (az:fl (color-g16 color)) (az:fl 65535)) 
   (/ (az:fl (color-b16 color)) (az:fl 65535))))

;;;------------------------------------------------------------

(defun color->lit (color depth)
  "This function is intended to be used for approximating color displays
in gray scale."
  (let ((shift (- depth 16)))
    (multiple-value-bind (r g b) (color->rgb16 color)
      (truncate (ash (+ r g b) shift) 3))))

(defun color->lightness (color)
  "This function is intended to be used for approximating color displays
in gray scale."
  (/ (multiple-value-call #'+ (color->rgb color)) 3.0d0))

;;;------------------------------------------------------------

#||
(defun ihs->color (i h s &key (result (%make-color)))
  (az:declare-check (type (Double-Float 0.0d0 1.0d0) i h s)
		    (type Color result))
  (multiple-value-bind (r g b) (ihs=rgb i h s)
    (rgb->color r g b :result result)))

(defun color->ihs (color)
  (az:declare-check (type Color color))
  (multiple-value-bind (r g b) (color->rgb color)
    (rgb->ihs r g b)))

(defun hexcone-ihs->color (i h s &key (result (%make-color)))
  (az:declare-check (type Color result))
  (multiple-value-bind (r g b) (hexcone-ihs=rgb i h s)
    (rgb->color r g b :result result)))

(defun color->hexcone-ihs (color)
  (az:declare-check (type Color color))
  (multiple-value-bind (r g b) (color->rgb color)
    (rgb->hexcone-ihs r g b)))

(defun yiq->color (y i q &key (result (%make-color)))
  (az:declare-check (type Color result))
  (multiple-value-bind (r g b) (yiq=rgb y i q)
    (rgb->color r g b :result result)))

(defun color->yiq (color)
  (az:declare-check (type Color color))
  (multiple-value-bind (r g b) (color->rgb color)
    (rgb->yiq r g b)))
||#

;;;============================================================
;;; based on X's rgb.txt color database

(defun read-colorname-database (database-file)
  (let ((line "") (index 0) (r 0) (g 0) (b 0))
    (with-open-file (database database-file :direction :input)
      (az:with-collection
       (loop 
	 (setf line (read-line database nil nil))
	 (when (null line) (return))
	 (with-input-from-string (s line :index index)
	   (setf r (read s))
	   (setf g (read s))
	   (setf b (read s)))
	 (az:collect (intern
			 (nsubstitute #\- #\space
				      (string-upcase
				       (string-left-trim '(#\space #\tab)
							 (subseq line index))))
			 :keyword))
	 (az:collect (rgb8->color r g b)))))))

(defvar *colorname-plist* ())

(defparameter *important-colornames*
    '(:black
      :blue
      :aquamarine
      :navy
      :coral
      :cyan
      :firebrick
      :brown
      :gold
      :green
      :gray
      :khaki
      :magenta
      :maroon
      :orange
      :orchid
      :pink
      :plum
      :red
      :salmon
      :sienna
      :tan
      :thistle
      :turquoise
      :violet
      :wheat
      :white
      :yellow
      :MediumAquamarine
      :mediumblue
      :mediumslateblue
      :mediumorchid
      :mediumseagreen
      :mediumturquoise
      :mediumvioletred
      :CadetBlue
      :CornflowerBlue
      :DarkSlateBlue
      :LightBlue
      :LightSteelBlue
      :MediumBlue
      :MediumSlateBlue
      :MidnightBlue
      :NavyBlue
      :SkyBlue
      :SlateBlue
      :SteelBlue
      :sandybrown
      :goldenrod
      :DarkGreen
      :DarkOliveGreen
      :ForestGreen
      :LimeGreen
      :PaleGreen
      :SeaGreen
      :SpringGreen
      :YellowGreen
      :DarkSlateGray
      :DimGray
      :LightGray
      :DarkOrchid
      :IndianRed
      :OrangeRed
      :VioletRed
      :DarkTurquoise
      :BlueViolet
      :GreenYellow))

(defun colorname? (name)
  "The type predicate for <Colorname>s."
  (and (typep name 'Keyword)
       (member name *colorname-plist*)))

(deftype Colorname ()
  "A type for Xstyle color names."
  '(satisfies colorname?))

(defun colorname->color (name &key (result (%make-color)))
  "Create or modify a color object to correspond to a given color name."
  (az:declare-check (type Colorname name)
		    (type Color result))
  (let ((color (getf *colorname-plist* name nil)))
    (if (null color)
	(error "~%No color named ~s" name)
      (if (null result)
	  (copy-color color)
	(copy-color color :result result)))))

;;;============================================================

(defparameter *color-resource* ()
  "A resource of color descriptions.")

(defun borrow-color ()
  "Borrow a color object from the resource."
  (declare (special *color-resource*))
  (let ((cd (pop *color-resource*)))
    (cond ((null cd) (%make-color :r16 0 :g16 0 :b16 0))
	  (t cd))))

(defun return-color (cd)
    "Return a color object to the resource."
  (declare (special *color-resource*))
  (az:declare-check (type Color cd))
  (push cd *color-resource*)) 

(defmacro with-borrowed-color ((vname) &body body)
  "A macro that automatically borrows and returns color objects."
   (az:declare-check (type Symbol vname))
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name (borrow-color))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-color ,return-name)))))

#|| versions with rgb parameters
(defun borrow-color (r16 g16 b16)
  (declare (special *color-resource*))
  (az:declare-check (type Color-Level r16 g16 b16))
  (let ((cd (pop *color-resource*)))
    (cond ((null cd)
	   (%make-color :r16 r16 :g16 g16 :b16 b16))
	  (t
	   (set-color-coords cd r16 g16 b16)
	   cd))))

(defmacro with-borrowed-color ((vname &key (r16 0) (g16 0) (b16 0))
			       &body body)
  ;; use {\sf return-name} so we deallocate the right thing,
  ;; even if the user re-assigns {\sf name}.
  (az:declare-check (type Symbol vname))
  (let ((return-name (gensym "BORROW-")))
    `(let* ((,return-name (borrow-color ,r16 ,g16 ,b16))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-color ,return-name)))))

||#

(defparameter *black-color* (rgb16->color 0 0 0))
(defparameter *white-color* (rgb16->color 65535 65535 65535))