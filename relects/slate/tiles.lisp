;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;;
;;; Tiles are basically used for filling with textures. The first time a
;;; Tile is used on a particular Screen, it makes an Invisible-Slate for
;;; that Screen, applies {\sf tile-draw-function} to the Slate, and saves the
;;; Slate in a cache indexed by Screen. The reason for have a Tile
;;; object that's distinct from the Slate is so that Tiles (and,
;;; consequently, Pens) can be specified in a device independent way.

(defun missing-tile-draw-function (slate)
  (declare (ignore slate))
  (error "Some tile doesn't have a draw function defined."))
       
(defstruct (Tile
	     (:conc-name nil)
	     (:constructor %make-tile)
	     (:copier %%copy-tile))
  (%tile-width 32 :type g:Positive-Screen-Coordinate)
  (%tile-height 32 :type g:Positive-Screen-Coordinate)
  (%tile-draw-function #'missing-tile-draw-function :type Function)
  (tile-cache (make-hash-table :test #'eq) :type Hash-Table))

(defun make-tile (&key
		  (tile-width 32)
		  (tile-height 32)
		  (tile-draw-function #'missing-tile-draw-function))
  (%make-tile :%tile-width tile-width
	      :%tile-height tile-height
	      :%tile-draw-function tile-draw-function))

(defun tile? (x)
  "The predicate for the <Tile> abstract type."
  (typep x 'Tile))

;;; Enforce immutability:
(declaim (inline tile-width tile-height tile-draw-function))

(defun tile-width (tile) (%tile-width tile))
(defsetf tile-width (tile) (width)
  (error "Can't setf tile-width: ~s ~s" tile width))

(defun tile-height (tile) (%tile-height tile))
(defsetf tile-height (tile) (height)
  (error "Can't setf tile-height: ~s ~s" tile height))

(defun tile-draw-function (tile) (%tile-draw-function tile))
(defsetf tile-draw-function (tile) (draw-function)
  (error "Can't setf tile-draw-function: ~s ~s" tile draw-function))

;;; {\sf copy-tile} is not appropriate if tiles are immutable?
;(defun copy-tile (tile &key (result (make-tile)))
;  (az:declare-check (type Tile tile result))
;  (setf (tile-width result) (tile-width tile))
;  (setf (tile-height result) (tile-height tile))
;  (setf (tile-draw-function result) (tile-draw-function tile))
;  (clrhash (tile-cache result)) ;;conservative slow choice
;  result)

(defun equal-tiles? (t0 t1)
  "We ignore the cache in the test for equality:  wishful thinking?"
  (or (eql t0 t1)
      (and (= (tile-width t0) (tile-width t1))
	   (= (tile-height t0) (tile-height t1))
	   (eql (tile-draw-function t0) (tile-draw-function t1)))))
  
(defun make-tile-screen-slate (tile screen)
  (let ((slate (make-invisible-slate
		 :screen screen
		 :width (tile-width tile)
		 :height (tile-height tile))))
    (funcall (tile-draw-function tile) slate)
    slate))

(defun tile-screen-slate (tile screen)
  (let* ((cache (tile-cache tile))
	 (tile-screen-slate (gethash screen cache nil)))
    (when (null tile-screen-slate)
      (setf tile-screen-slate (make-tile-screen-slate tile screen))
      (setf (gethash screen cache nil) tile-screen-slate))
    tile-screen-slate))

