;;;-*- Package: :Slate; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Slate)

;;;============================================================
;;; This file is supposed to contain load time initializations:

;;;============================================================
;;; Read the color name database (from X)
;;;============================================================

(setf *colorname-plist*
  (read-colorname-database user::*xcolorname-database-file*))

;;;============================================================
;;; Make screens as appropriate
;;;============================================================

#+(or :excl :cmu)
(eval-when (eval load)
  (when (ac:clx-host)
    (add-screen-to-screens (make-default-screen))
    (when (true-24bit-color-visual)
      (add-screen-to-screens (make-true-24bit-color-clx-screen)))
    ;; allocate some colors in the default x colormap
    ;; to share with other applications, and to have in the same place
    ;; in all slate colormaps, so the rest of the screen is still readable
    ;; when the mouse is in a slt:
    (when (eq :Pseudo-Color (default-visual-info-class))
      (let ((default-colormap (default-colormap))
	    (pixval 0))
	(dolist (c0 *important-colornames*)
	  (setf pixval (xlib:alloc-color default-colormap (xcolorname c0)))
	  (when (>= pixval 31) (return)))))
    ))

#+:coral
(eval-when (eval load)
  (add-screen-to-screens (make-bw-mac-screen))
  (if ccl:*color-available*
    (add-screen-to-screens (make-color-mac-screen)))
  )


#+symbolics
(eval-when (eval load)
  ;; This nonsense is to avoid barfs when trying to read the
  ;; following expression without the color package.
  (when (and (find-package :color)
	     (not (null (funcall (intern "COLOR-SYSTEM-DESCRIPTION" :color)))))
    (add-screen-to-screens
      (make-rgb-stv-screen (funcall (intern "MAKE-COLOR-SCREEN" :color)))))
  (add-screen-to-screens (make-stv-screen tv:main-screen)))

;;;============================================================
;;; Assorted standard tiles
;;;============================================================

(defparameter *boxtile* nil)

(defun boxtile ()
  (when (null *boxtile*)
    (setf *boxtile*
	  (make-tile
	    :tile-draw-function
	    #'(lambda (slate)
		(az:declare-check (type Invisible-Slate slate))
		(draw-rect (writing-pen) slate (slate-rect slate))))))
  *boxtile*)
	    
(defparameter *ellipse-tile* nil)

(defun ellipse-tile ()
  (when (null *ellipse-tile*)
    (setf *ellipse-tile*
	  (make-tile
	    :tile-draw-function
	    #'(lambda (slate)
		(az:declare-check (type Invisible-Slate slate))
		(draw-ellipse (writing-pen) slate (slate-rect slate))))))
  *ellipse-tile*)

(defparameter *cross-tile* nil)

(defun cross-tile ()
  (when (null *cross-tile*)
    (setf *cross-tile*
	  (make-tile
	    :tile-draw-function
	    #'(lambda (slate)
		(az:declare-check (type Invisible-Slate slate))
		(let ((w (- (slate-width slate) 1))
		      (h (- (slate-height slate) 1)))
		  (draw-line-xy (writing-pen) slate 0 0 w h)
		  (draw-line-xy (writing-pen) slate 0 h w 0))))))
  *cross-tile*)

(defparameter *star-tile* nil)

(defun star-tile ()
  (when (null *star-tile*)
    (setf *star-tile*
      (make-tile
       :tile-draw-function
       #'(lambda (slate)
	   (az:declare-check (type Invisible-Slate slate))
	   (let ((center-x (truncate (slate-width slate) 2))
		 (center-y (truncate (slate-height slate) 2))
		 (ang (/ (* 2.0d0 pi) 8.0d0)))
	     (dotimes (r 8)
	       (draw-line-xy
		(writing-pen) slate
		center-x center-y
		(+ center-x (round (* center-x (sin (* r ang)))))
		(+ center-y (round (* center-y (cos (* r ang))))))))))))
  *star-tile*)

(defparameter *gray-tile* nil)

(defun gray-tile ()
  (when (null *gray-tile*)
    (setf *gray-tile*
	  (make-tile
	    :tile-draw-function
	    #'(lambda (slate)
		(az:declare-check (type Invisible-Slate slate))
		(let* ((screen-rect (slate-rect slate))
		       (xmin (g:screen-rect-left screen-rect))
		       (ymin (g:screen-rect-top screen-rect))
		       (width (slate-width slate))
		       (height (slate-height slate))
		       (xmax (g:screen-rect-right screen-rect))
		       (ymax (g:screen-rect-bottom screen-rect))
		       (x xmin)
		       (y ymin)
		       (nx (truncate width 2))
		       (ny (truncate height 2)))
		  (dotimes (i ny)
		    (draw-line-xy (xor-pen) slate xmin y xmax y)
		    (incf y 2))
		  (dotimes (i nx)
		    (draw-line-xy (xor-pen) slate x ymin x ymax)
		    (incf x 2)))))))
  *gray-tile*)


