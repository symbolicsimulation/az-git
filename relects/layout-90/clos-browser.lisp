;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 
  
(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defparameter *highlit-classes* ())
;;(list (find-class 'cactus::identity))

;;;============================================================
;;; Class-Graph
;;;============================================================
;;; The nodes in a Class-Graph are the classes themselves.

(defmethod node-name ((cl Class)) (class-name cl))

;;;============================================================
;;; The edges in a Class-Graph are instances of Subclass-Pair.

(defclass Subclass-Pair ()
     ((parent-class
	:type Class
	:accessor parent-class
	:accessor edge-node0
	:initarg :parent-class)
      (child-class
	:type Class
	:accessor child-class
	:accessor edge-node1
	:initarg :child-class)))

;;;------------------------------------------------------------

(defun make-subclass-pair (parent child)
  (let ((pair (allocate-instance (find-class 'Subclass-Pair))))
    (setf (parent-class pair) parent)
    (setf (child-class pair) child)
    pair))

;;;============================================================
;;; The purpose of having Class-Graph objects is to enable us
;;; to focus our attention on a subset of the inheritance relations
;;; in a set of classes.

(defclass Class-Graph (Simple-DiGraph) ())

;;;------------------------------------------------------------

(defun make-class-graph (classes)
  (let ((class-graph (allocate-instance (find-class 'Class-Graph)))
	(edges ()))
    (setf (nodes class-graph) classes)
    (dolist (class0 classes)
      (dolist (class1 classes)
	(when (member class1 (pcl::class-direct-subclasses class0))
	  (push (make-subclass-pair class0 class1) edges))))
    (setf (edges class-graph) edges)
    class-graph))

;;;============================================================

(defclass Class-Presentation (Graph-Node-Presentation) ())

;;;------------------------------------------------------------

(defmethod make-presentation-for ((class Class))
  (let ((p (allocate-instance (find-class 'Class-Presentation))))
    (setf (subject p) class)
    p))

;;;------------------------------------------------------------

(defmethod build-display-local ((display Class-Presentation) build-options)

  ;; slots inherited from Display
  (setf (slot-value display 'redraw-timestamp)
    (tools:make-timestamp))
  (setf (slate-rect display) (g:make-screen-rect))
  (setf (temp-rect display) (g:make-screen-rect))
  (setf (clipping-region display) nil)
  (setf (temp-clipping-region display) nil)
  (setf (lit? display) nil)

  ;; slots inherited from Display-Leaf
  (setf (slot-value display 'display-pen) (subject-pen (subject display))) 

  ;; local slots
  (setf (name-string display)
    (string-capitalize (string (class-name (subject display)))))
      (setf (display-name display)
      (getf build-options :display-name
	    (format nil "~s presentation" (subject display)))))

;;;============================================================

(defclass Subclass-Pair-Presentation (DiGraph-Edge-Presentation) ())

;;;------------------------------------------------------------

(defmethod make-presentation-for ((subclass-pair Subclass-Pair))
  (let ((p (allocate-instance (find-class 'Subclass-Pair-Presentation))))
    (setf (subject p) subclass-pair)
    p))

;;;============================================================

(defclass Class-Graph-Presentation (DiGraph-Presentation) ())

;;;------------------------------------------------------------

(defmethod build-children ((gp Class-Graph-Presentation))
  (let* ((subject (subject gp))
	 (snodes (nodes subject))
	 (sedges (edges subject))
	 (pnodes ())
	 (pedges ()))
    ;; make presentations for the nodes in the graph
    (dolist (snode snodes)
      (let ((gnp (make-presentation-for snode)))
	(setf (parent gnp) gp)
	(push gnp pnodes)))
    (setf (nodes gp) (nreverse pnodes))

    ;; make presentations for the edges in the graph
    (dolist (sedge sedges)
      (let ((gep (make-presentation-for sedge)))
	(setf (parent gep) gp)
	(push gep pedges)))
    (setf (edges gp) (nreverse pedges))

    ;; recreate the graph structure in the presentation nodes and edges
    (dolist (gep (edges gp))
      (let* ((sedge (subject gep))
	     (snode0 (edge-node0 sedge))
	     (gnp0 (find-presentation-of snode0 gp))
	     (snode1 (edge-node1 sedge))
	     (gnp1 (find-presentation-of snode1 gp)))
	(push gep (from-edges gnp0))
	(push gep (to-edges gnp1))
	(setf (edge-node0 gep) gnp0)
	(setf (edge-node1 gep) gnp1)))))

;;;------------------------------------------------------------

(defun make-class-graph-presentation (class-graph slate)
  (let ((gp (allocate-instance (find-class 'Class-Graph-Presentation)))
	(screen-rect (slate:slate-rect slate)))
    (setf (display-slate gp) slate)
    (setf (subject gp) class-graph)
    (g:copy-screen-rect screen-rect :result (slate-rect gp))
    (g:copy-screen-rect screen-rect :result (temp-rect gp))
    (build-display gp nil)
    (layout-display gp nil)
    (slate:clear-slate (display-slate gp))
    (draw-display gp)
    gp)) 

;;;============================================================
;;; Generic-Function-Graph
;;;============================================================
;;; The nodes in a Generic-Function-Graph are classes and methods.

(defmethod node-name ((method Standard-Method))
  (with-output-to-string (stream)
    (let ((generic-function (pcl::method-generic-function method)))
      (format stream "(~a~{ ~a~}~{ ~a~})"
	      (pcl::generic-function-name generic-function) 
	      (pcl::method-qualifiers method)
	      (pcl::unparse-specializers method))) ))

;;;============================================================
;;; The edges in a Generic-Function-Graph are instances of
;;; Class-Method-Pair.

(defclass Class-Method-Pair ()
     ((class
	:type Class
	:accessor class
	:accessor edge-node0
	:initarg :class)
      (method
	:type Standard-Method
	:accessor method
	:accessor edge-node1
	:initarg :method)))

;;;------------------------------------------------------------

(defun make-class-method-pair (class method)
  (let ((pair (allocate-instance (find-class 'Class-Method-Pair))))
    (setf (class pair) class)
    (setf (method pair) method)
    pair))

;;;============================================================
;;; The purpose of having Generic-Function-Graph objects is to enable us
;;; to focus our attention on a subset of the inheritance and
;;; specialization relations in a set of classes and methods.

(defclass Generic-Function-Graph (Simple-DiGraph) ())

;;;------------------------------------------------------------

(defun make-generic-function-graph (gf)
  (let* ((digraph (allocate-instance (find-class 'Generic-Function-Graph)))
	 (edges ())
	 (methods (copy-list (pcl::generic-function-methods gf)))
	 (specializing-classes
	   (delete-duplicates
	     (mapcan #'(lambda (method)
			 (copy-list
			   (pcl::method-specializers method)))
		     methods)))
	 (classes
	   (delete-duplicates
	     (mapcon #'(lambda (remainder)
			 (let ((c0 (first remainder)))
			   (delete-duplicates
			     (mapcan #'(lambda (c1)
					 (all-classes-between c0 c1))
				     (rest remainder)))))
		     specializing-classes))))
    
    (dolist (class0 classes)
      (dolist (class1 classes)
	(when (member class1 (pcl::class-direct-subclasses class0))
	  (push (make-subclass-pair class0 class1) edges)))
      (dolist (method methods)
	(when (member class0 (pcl::method-specializers method))
	  (push (make-class-method-pair class0 method) edges))))
    (setf (nodes digraph) (concatenate 'list classes methods))
    (setf (edges digraph) edges)
    digraph))

;;;============================================================

(defclass Method-Presentation (Graph-Node-Presentation)
     ((strings
	:type List
	:accessor strings)
      (pens
	:type List
	:accessor pens)))

;;;------------------------------------------------------------

(defmethod make-presentation-for ((method Standard-Method))
  (let ((p (allocate-instance (find-class 'Method-Presentation))))
    (setf (subject p) method)
    p)) 

;;;------------------------------------------------------------

(defmethod build-display-local ((display Method-Presentation) build-options)
  (declare (ignore build-options))

  ;; slots inherited from Display
  (setf (slot-value display 'redraw-timestamp)
    (tools:make-timestamp))
  (setf (slate-rect display) (g:make-screen-rect))
  (setf (temp-rect display) (g:make-screen-rect))
  (setf (clipping-region display) nil)
  (setf (temp-clipping-region display) nil)
  (setf (lit? display) nil)

  ;; slots inherited from Presentaion
  (setf (slot-value display 'display-pen) (subject-pen (subject display))) 

  ;; local slots
  (setf (name-string display)
	(string-capitalize (string (class-name (subject display)))))
  (let ((m (subject display)))
    (setf (slot-value display 'display-pen) (subject-pen m))
    (setf (strings display)
	  (list (string-downcase
		  (pcl::generic-function-name
		    (pcl::method-generic-function m)))))
    (setf (pens display)
	  (list (display-pen display)))
    (mapc #'(lambda (q)
	      (push (string-downcase q) (strings display))
	      (push (small-font-pen) (pens display)))
	  (pcl::method-qualifiers m))
    (mapc #'(lambda (c)
	      (push (string-capitalize (class-name c)) (strings display))
	      (push (subject-pen c) (pens display)))
	  (pcl::method-specializers m)))
  (setf (strings display) (nreverse (strings display)))
  (setf (pens display) (nreverse (pens display)))
  display)

(defmethod layout-display ((p Method-Presentation) layout-options)
  (declare (ignore layout-options))
  (let* ((pens (pens p))
	 (strings (strings p))
	 (slate (display-slate p))
	 (widths (mapcar #'(lambda (pen str)
			     (slate:string-width pen slate str))
			 pens strings))
	 (heights (mapcar #'(lambda (pen str)
			      (slate:string-height pen slate str))
			  pens strings))
	 (descents (mapcar #'(lambda (pen str)
			       (slate:string-descent pen slate str))
			   pens strings))
	 (margin 4)
	 (width (+ margin (apply #'max widths)))
	 (height (+ margin (apply #'+ heights)))
	 (invisible-slate (slate:make-invisible-slate
			    :screen (slate:slate-screen slate)
			    :width width :height height)))
    (setf (invisible-slate p) invisible-slate)
    (g:with-borrowed-screen-rect (r :width width :height height)
      (slate:draw-rect (display-pen p) invisible-slate r))
    (g:with-borrowed-screen-point (screen-point :x 2 :y (- height margin))
      (mapc #'(lambda (pen s h d)
		(incf (g:screen-point-y screen-point) (- d h))
		(slate:draw-string pen invisible-slate s screen-point))
	    pens strings heights descents))
    (setf (allowed-rect p)
	  (possible-point-rect-xy
	    width height margin (slate:slate-rect slate)
	    :result (allowed-rect p)))
    ;; random initialization
    (setf (slate-point p) (random-screen-point-in-rect
			    (allowed-rect p)
			    :result (slate-point p)))
    (setf (slate-rect p)
	  (g:set-screen-rect-coords
	    (slate-rect p)
	    :left (g:screen-point-x (slate-point p))
	    :top (g:screen-point-y (slate-point p))
	    :width width :height height))))

;;;============================================================

(defclass Class-Method-Pair-Presentation (DiGraph-Edge-Presentation) ())

(defmethod make-presentation-for ((class-method-pair Class-Method-Pair))
  (let ((p (allocate-instance (find-class 'Class-Method-Pair-Presentation))))
    (setf (subject p) class-method-pair)
    p)) 

;;;============================================================

(defclass Generic-Function-Presentation (DiGraph-Presentation) ())

;;;------------------------------------------------------------

(defmethod build-children ((gp Generic-Function-Presentation))
  (let* ((subject (subject gp))
	 (snodes (nodes subject))
	 (sedges (edges subject))
	 (pnodes ())
	 (pedges ()))
    ;; make presentations for the nodes in the graph
    (dolist (snode snodes)
      (let ((gnp (make-presentation-for snode)))
	(setf (parent gnp) gp)
	(push gnp pnodes)))
    (setf (nodes gp) (nreverse pnodes))

    ;; make presentations for the edges in the graph
    (dolist (sedge sedges)
      (let ((gep (make-presentation-for sedge)))
	(setf (parent gep) gp)
	(push gep pedges)))
    (setf (edges gp) (nreverse pedges))

    ;; recreate the graph structure in the presentation nodes and edges
    (dolist (gep (edges gp))
      (let* ((sedge (subject gep))
	     (snode0 (edge-node0 sedge))
	     (gnp0 (find-presentation-of snode0 gp))
	     (snode1 (edge-node1 sedge))
	     (gnp1 (find-presentation-of snode1 gp)))
	(push gep (from-edges gnp0))
	(push gep (to-edges gnp1))
	(setf (edge-node0 gep) gnp0)
	(setf (edge-node1 gep) gnp1)))))

;;;------------------------------------------------------------

(defun make-Generic-Function-presentation (Generic-Function slate)
  (let ((gp (allocate-instance
	      (find-class 'Generic-Function-Presentation)))
	(screen-rect (slate:slate-rect slate)))
    (setf (display-slate gp) slate)
    (setf (subject gp) Generic-Function)
    (g:copy-screen-rect screen-rect :result (slate-rect gp))
    (g:copy-screen-rect screen-rect :result (temp-rect gp))
    (build-display gp nil)
    (layout-display gp nil)
    (slate:clear-slate (display-slate gp))
    (draw-display gp)
    gp)) 

