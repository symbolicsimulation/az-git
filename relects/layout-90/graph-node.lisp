;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))
  
;;;============================================================

(defclass Graph-Node-Presentation (Presentation Display-Leaf) 
	  ( ;; slots for the graph strucutre
	   (from-edges
	    :type List
	    :accessor from-edges)
	   (to-edges
	    :type List
	    :accessor to-edges)
	   ;; slots used by various graph analysis algorithms
	   (node-index
	    :type Array-Index
	    :accessor node-index)
	   (node-predecessor
	    :type (or Null Graph-Node-Presentation)
	    :accessor node-predecessor)
	   (node-distance
	    ;; the mininmal number of edges in a path
	    ;; from node0 to node1 is
	    ;; (aref (node-distance node0) (node-index node1))
	    :type (Simple-Array Fixnum (*))
	    :accessor node-distance)
	   ;; slots used for display and input
	   (slate-point
	    :type g:Screen-Point
	    :accessor slate-point)
	   (temp-point
	    :type g:Screen-Point
	    :accessor temp-point)
	   (string-point
	    :type g:Screen-Point
	    :accessor string-point)
	   (invisible-slate
	    :type slate:Invisible-Slate
	    :accessor invisible-slate)
	   (graph-node-margin
	    :type g:Positive-Screen-Coordinate
	    :accessor graph-node-margin)
	   (name-string
	    :type String
	    :accessor name-string)
	   (allowed-rect
	    :type g:Screen-rect
	    :accessor allowed-rect)))

;;;------------------------------------------------------------

(defgeneric node-name (node)
  ;; This is supposed to work for anything that can be thought of as a
  ;; node in a graph.
  (declare (type T node))) 

(defmethod node-name ((display Graph-Node-Presentation))
  (node-name (subject display)))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Graph-Node-Presentation)
				build-options)

  (declare (optimize (speed 3)
		     (safety 1)
		     (space 0)
		     (compilation-speed 0))
	   (type Graph-Node-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject))
	 (rect (slate:slate-rect slate)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp) (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    (setf (slate-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (temp-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; slots inherited from Presentation:
    (setf (subject display) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject)) 

    ;; local slots
    (setf (name-string display) (string-downcase (string (node-name display))))
    (setf (slate-point display) (g::%make-screen-point 0 0))
    (setf (temp-point display) (g::%make-screen-point 0 0))
    (setf (string-point display) (g::%make-screen-point 0 0))
    (setf (from-edges display) ())
    (setf (to-edges display) ())
    (setf (allowed-rect display) (g::%make-screen-rect 0 0 0 0))))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((display Graph-Node-Presentation)
				 layout-options)
  (declare (ignore layout-options)
	   (optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0))
	   (type Graph-Node-Presentation display))
  (setf (graph-node-margin display) 4)
  (let* ((slate (display-slate display))
	 (point (slate-point display))
	 (string-point (string-point display))
	 (rect (slate-rect display))
	 (slate-rect (slate:slate-rect slate))
	 (allowed-rect (allowed-rect display))
	 (margin (graph-node-margin display))
	 (margin/2 (ash margin -1))
	 (extent (display-minimum-extent display)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect slate-rect allowed-rect)
	     (type g:Screen-Point point)
	     (type g:Screen-Vector extent))
    (%possible-point-rect extent margin slate-rect allowed-rect)
    (setf (g::%screen-point-x string-point)
      margin/2)
    (setf (g::%screen-point-y string-point)
      (+ margin/2
	 (slate::%string-ascent
	  (slate:slate-screen slate)
	  (name-string display) (slate::%pen-font (display-pen display)))))
    (setf (g::%screen-rect-xmin rect) (g::%screen-point-x point))
    (setf (g::%screen-rect-ymin rect) (g::%screen-point-y point))
    (setf (g::%screen-rect-width rect) (g:screen-vector-x extent))
    (setf (g::%screen-rect-height rect) (g:screen-vector-y extent))
    (graph-node-update-invisible-slate display)
    display))

;;;------------------------------------------------------------

(defmethod display-minimum-extent ((display Graph-Node-Presentation)
				   &key
				   (result (g:make-screen-vector)))
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0))
	   (type Graph-Node-Presentation display)
	   (type g:Screen-Vector result))
  (let ((margin (graph-node-margin display)))
    (declare (type g:Positive-Screen-Coordinate margin))
    (multiple-value-bind
	(w ascent descent)
	(slate::%analyze-string-placement
	 (slate:slate-screen (display-slate display))
	 (name-string display)
	 (slate::%pen-font (display-pen display)))
      (declare (type g:Screen-Coordinate w ascent descent))
      (setf (g:screen-vector-x result) (+ margin w))
      (setf (g:screen-vector-y result) (+ margin ascent descent))))
  result)

;;;------------------------------------------------------------

(defun graph-node-randomize-position (display)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0))
	   (type Graph-Node-Presentation display))
  ;; random initialization
  (%random-screen-point-in-rect (allowed-rect display) (slate-point display))
  (setf (g:screen-rect-origin (slate-rect display)) (slate-point display)))

;;;------------------------------------------------------------

(defmethod apply-lens (lens (display Graph-Node-Presentation))
  (declare (ignore lens))
  (setf (slot-value display 'display-pen) (subject-pen (subject display)))
  (setf (name-string display) (string-downcase (string (node-name display)))))

;;;------------------------------------------------------------

(defmethod node-l2-dist2 ((display0 Graph-Node-Presentation)
			  (display1 Graph-Node-Presentation))
  (g:screen-rect-min-l2-dist2 (slate-rect display0) (slate-rect display1)))

(defmethod node-l2-dist ((display0 Graph-Node-Presentation)
			 (display1 Graph-Node-Presentation))
  (g:screen-rect-min-l2-dist (slate-rect display0) (slate-rect display1)))

;;;============================================================
;;; Drawing
;;;============================================================

(defmethod draw-display ((display Graph-Node-Presentation))
  (let ((display-screen-rect (slate-rect display)))
    (slate:with-clipping-region ((display-slate display)
				 (clipping-region display))
      (slate::%copy-slate-rect-xy
	(display-pen display) (invisible-slate display)
	0 0
	(g::%screen-rect-width display-screen-rect)
	(g::%screen-rect-height display-screen-rect)
	(display-slate display)
	(g::%screen-rect-left display-screen-rect)
	(g::%screen-rect-top display-screen-rect))))
  (when (lit? display) (highlight-display display))
  (tools:stamp-time! (redraw-timestamp display)))

;;;------------------------------------------------------------

(defmethod erase-display ((display Graph-Node-Presentation))
  (when (lit? display) (lowlight-display display))
  (slate:with-clipping-region ((display-slate display)
			       (clipping-region display))
    (setf (slate:pen-pixel-value (erasing-fill-pen))
       (slate:slate-background-pixel-value (display-slate display)))
    (slate::%draw-rect (erasing-fill-pen) (display-slate display)
		       (slate-rect display)))
  (slate-rect display))

;;;------------------------------------------------------------

(defun graph-node-update-invisible-slate (display)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0))
	   (type Graph-Node-Presentation display))
  (let* ((slate (display-slate display))
	 (screen (slate:slate-screen slate))
	 (rect (slate-rect display))
	 (width (g::%screen-rect-width rect))
	 (height (g::%screen-rect-height rect))
	 (pen (display-pen display))
	 (string (name-string display)))
    (when (or (not (slot-boundp display 'invisible-slate))
	      (> width (slate:slate-width (invisible-slate display)))
	      (> height (slate:slate-height (invisible-slate display))))
      ;; then we need to make a new invisible slate
      (setf (invisible-slate display)
	(slate:make-invisible-slate :screen screen
				    :width width
				    :height height
				    :slate-background-pixel-value 0)))
    ;; redraw the contents of the invisible slate
    (let ((invisible-slate (invisible-slate display)))
      (slate:clear-slate invisible-slate)
      (slate::%draw-rect
       pen invisible-slate (slate:slate-rect invisible-slate))
      (slate::%draw-string
       pen invisible-slate string (string-point display)))))

;;;------------------------------------------------------------

(defmethod swap-temp-coords ((display Graph-Node-Presentation))
  (rotatef (slate-point display) (temp-point display))
  (rotatef (slate-rect display) (temp-rect display))) 

(defmethod move-display-to ((display Graph-Node-Presentation) pos)
  (check-type pos g:Screen-point)
  ;; because this requires maintaining constraints amoung
  ;; the siblings of <display>, it must be handled by the parent
  (move-node (parent display) display pos))

;;;------------------------------------------------------------
;; automate redrawing whenever pen is changed

(defmethod (setf display-pen) (pen (display Graph-Node-Presentation))
  (setf (slot-value display 'display-pen) pen)
  ;; animate
  (erase-display display)
  (graph-node-update-invisible-slate display)
  (draw-display display))

;;;============================================================
;;; Activation
;;;===========================================================

(defmethod kill-object ((display Graph-Node-Presentation))
  (deactivate-display display)
  (slate:kill-slate (invisible-slate display))
  (change-class display `Dead-Object))

;;;============================================================
;;; Input
;;;============================================================

