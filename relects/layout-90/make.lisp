;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; Patch-File: T;-*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================
;;; set gc parameters for fast system loading

#+:excl (excl:gc t)
(let #-:excl ()
     #+:excl ((old-gc-spread (sys:gsgc-parameter :generation-spread))
	      (excl:*record-source-files* nil)
	      (excl:*tenured-bytes-limit* nil))
     #+:excl (declare (Special excl:*record-source-files*
			       excl:*tenured-bytes-limit*))
     (unwind-protect
	 (progn
	   #+:excl (setf (sys:gsgc-parameter :generation-spread) 1)

;;;============================================================

	   (load #+:coral "ccl;az:tools:compile-if-needed"
		 #+:excl "/belgica-0g/local/az/tools/compile-if-needed"
		 #+symbolics "/belgica-0g/local/az/tools/compile-if-needed")

	   (load #+:coral "ccl;az:files"
		 #+:excl "/belgica-0g/local/az/files"
		 #+symbolics "belgica:/belgica-0g/local/az/files")

	   (load #+:coral "ccl;jam:az:files"
		 #+:excl "/belgica-2g/jam/az/files"
		 #+symbolics "belgica:/belgica-2g/jam/az/files")

;;;============================================================

	   (load-all *tools-directory* *tools-files*)
	   (load-all *geometry-directory* *geometry-files*) 

	   (defvar *load-pcl?*)
	   (setf *load-pcl?* (not (find-package "PCL")))

	   (when *load-pcl?*
	     (compile-if-needed *pcl-defsys-file*))

	   (when *load-pcl?*
	     (let (#+:coral (*warn-if-redefine-kernel* nil))
	       #+:coral (declare (special *warn-if-redefine-kernel*))
	       (pcl::load-pcl))) 

	   ;; make sure that we have all tools,
	   ;; including abstract-proplist-object hack:
	   #+excl (shadow '(wt::object) (find-package :wt))
	   (load-all *clos-tools-directory* *clos-tools-files*) 

	   (compile-all-if-needed *slate-directory* *slate-files*)
	   (compile-all-if-needed *tap-directory* *tap-files*)


	   ;;(load-all *basic-math-directory* *basic-math-files*) 
	   ;;(load-all *chart-directory* *chart-files*)
	   ;;(load-all *cactus-directory* *cactus-files*)


	   (compile-all-if-needed *clay-directory* *clay-files*)
	   (compile-all-if-needed *layout-directory* *layout-files*)
	   (compile-if-needed "~jam/az/examples/england/build")

	   (load "~az/npsol/l-npsol")
	   (compile-if-needed "~jam/az/layout/npsol-layout")


	   )
       #+:excl
       (progn (print "gc") (finish-output)
	      (excl:gc :tenure) (excl:gc :tenure) (excl:gc t) (room t)
	      (setf (sys:gsgc-parameter :generation-spread) old-gc-spread))
       #-:excl ()))
