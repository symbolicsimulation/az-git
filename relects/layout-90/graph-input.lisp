;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))
  
;;;============================================================
;;; Objects to handle input to Graph-Presentations
;;;============================================================

(defclass Graph-Interactor (Presentation-Interactor) ())

;;============================================================
;;; Interactor for node dragging
;;;============================================================

(defclass Node-Dragging-Interactor (Graph-Interactor)
	  ((node
	    :type Graph-Node-Presentation
	    :accessor node)
	   (curr-pos
	    :type g:Screen-Point
	    :accessor curr-pos
	    :initform (g:make-screen-point))
	   (last-pos
	    :type g:Screen-Point
	    :accessor last-pos
	    :initform (g:make-screen-point))
	   (origin
	    :type g:Screen-Point
	    :accessor origin
	    :initform (g:make-screen-point))
	   (dp
	    :type g:Screen-Vector
	    :accessor dp
	    :initform (g:make-screen-vector))
	   ))

;;;============================================================
;;; Methods for Graph-Interactor 
;;;============================================================

(defmethod interesting-input-events ((interactor Graph-Interactor))
  '( ;; :enter-notify
    ;; :leave-notify
    :motion-notify
    ;;:pointer-motion-hint
    
    :button-press
    :button-release
    ;; :exposure
    ;; :visibility-notify
    :configure-notify
    :funcall
    ))

;;;============================================================

(defmethod handle-button-press ((interactor Graph-Interactor) button x y)
  (declare (optimize (compilation-speed 0)
		     (safety 3)
		     (space 0)
		     (speed 0)))
  button
  (let ((display (interactor-display interactor)))
    (tools:atomic
     (let* ((dragger (node-dragging-interactor display))
	    (curr-pos (curr-pos dragger)))
       (g:set-screen-point-coords curr-pos :x x :y y)
       (setf (node dragger) (find-node-under display curr-pos))
       (unless (null (node dragger))
	 (g:screen-rect-origin (slate-rect (node dragger))
			       :result (origin dragger))
	 (g:copy-screen-point curr-pos :result (last-pos dragger))
	 (g:subtract-screen-points (origin dragger) curr-pos
				   :result (dp dragger))
	 (setf (current-interactor display) dragger))))))

;;;============================================================
;;; Methods for Node-Dragging-Interactor 
;;;============================================================

(defmethod interesting-input-events ((interactor Node-Dragging-Interactor))
  '( :motion-notify
    :button-press
    :button-release
    :funcall
    ))

;;;============================================================

(defmethod handle-motion-notify ((interactor Node-Dragging-Interactor) x y)
  (declare (optimize (compilation-speed 0)
		     (safety 3)
		     (space 0)
		     (speed 0)))
  (let* ((display (interactor-display interactor))
	 (slate (display-slate display))
	 (queue (slate:update-slate-input-queue slate))
	 (node (node interactor))
	 (curr-pos (curr-pos interactor))
	 (last-pos (last-pos interactor))
	 (origin (origin interactor))
	 (dp (dp interactor)))
    ;; jump to the last motion notify event on the queue
    (loop (let ((next-event (slate::%dequeue-input-event queue)))
	    (unless (eq (first next-event) :motion-notify)
	      (unless (null next-event)
		(slate:push-input-event next-event queue))
	      (return))
	    (setf x (second next-event))
	    (setf y (third next-event))))
    ;; follow mouse with current node, if any
    (unless (null node)
      (g:copy-screen-point curr-pos :result last-pos)
      (setf (g:screen-point-x curr-pos) x)
      (setf (g:screen-point-y curr-pos) y)
      (unless (g:equal-screen-points? curr-pos last-pos)
	(g:move-screen-point curr-pos dp :result origin)
	(move-display-to node origin)))))

(defun find-node-under (display point)
  (dolist (node (nodes display))
    (when (g::%screen-point-in-rect? point (slate-rect node))
      (return-from find-node-under node)))
  nil)





