;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

  
;;;============================================================
;;; Directed Graphs:
;;;============================================================

(defclass Digraph-Presentation (Graph-Presentation) ())

;;;------------------------------------------------------------

(defmethod build-children ((display DiGraph-Presentation))
  (let* ((subject (subject display))
	 (slate (display-slate display))
	 (snodes (nodes subject))
	 (sedges (edges subject))
	 (sn-table (subject-node-table display))
	 (se-table (subject-edge-table display)))
    ;; make presentations for the nodes in the graph
    (setf (nodes display)
      (tools:with-collection
       (dolist (snode snodes)
	 (let ((gnp (allocate-instance (find-class 'Graph-Node-Presentation))))
	   (build-display-local gnp (list :subject snode
					  :slate slate
					  :parent display))
	   (setf (gethash snode sn-table) gnp)
	   (tools:collect gnp)))))
    (graph-presentation-index-nodes display)


    ;; make presentations for the edges in the graph
    ;; and recreate the graph structure
    ;; in the presentation nodes and edges
    (setf (edges display)
      (tools:with-collection
       (dolist (sedge sedges)
	 (let ((gep (allocate-instance
		     (find-class 'DiGraph-Edge-Presentation)))
	       (gnp0 (gethash (edge-node0 sedge) sn-table))
	       (gnp1 (gethash (edge-node1 sedge) sn-table)))
	   (build-display-local gep (list :subject sedge
					  :slate slate
					  :parent display))
	   (setf (gethash sedge se-table) gep)
	   (push gep (from-edges gnp0))
	   (push gep (to-edges gnp1))
	   (setf (edge-node0 gep) gnp0)
	   (setf (edge-node1 gep) gnp1)
	   (tools:collect gep)))))))


;;;============================================================

(defun make-presentation-for-digraph (digraph slate
				      &key
				      (display-name nil)
				      (initial-node-configuration nil))
  (tools:type-check slate:Input-Slate Slate)
  (let ((display (allocate-instance (find-class 'Digraph-Presentation))))
    (initialize-display display
			:build-options (if display-name
					   (list :display-name display-name
						 :subject digraph
						 :slate slate)
					 (list :subject digraph
					       :slate slate))
			:layout-options (list :initial-node-configuration
					      initial-node-configuration))
    display))


