;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

  
;;;============================================================

(defclass Graph-Edge-Presentation (Presentation Line-Display)
     ((edge-node0
	:type Graph-Node-Presentation
	:accessor edge-node0)
      (edge-node1
	:type Graph-Node-Presentation
	:accessor edge-node1)))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Graph-Edge-Presentation)
				build-options)

  (declare (optimize (speed 3)
		     (safety 1)
		     (space 0)
		     (compilation-speed 0))
	   (type Graph-Edge-Presentation display)
	   (type List build-options))
  (let* ((slate (getf build-options :slate))
	 (subject (getf build-options :subject))
	 (rect (slate:slate-rect slate)))
    (declare (type slate:Slate slate)
	     (type g:Screen-Rect rect))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp)
      (tools:make-timestamp))
    (setf (parent display) (getf build-options :parent))
    (setf (display-slate display) slate)
    (setf (slate-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (temp-rect display)
      (g::%copy-screen-rect rect (g::%make-screen-rect 0 0 0 0)))
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; slots inherited from Presentation:
    (setf (subject display) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject)) 

    ;; slots inherited from Line-Display
    (setf (slate-from-point display) (g::%make-screen-point 0 0))
    (setf (temp-from-point display) (g::%make-screen-point 0 0))
    (setf (slate-to-point display) (g::%make-screen-point 0 0))
    (setf (temp-to-point display) (g::%make-screen-point 0 0))))

;;;============================================================
;;; Layout
;;;============================================================

(defmethod layout-display-local ((gep Graph-Edge-Presentation) layout-options)
  (declare (ignore layout-options))
  (compute-edge-attachment-screen-points
    (slate-rect (edge-node0 gep))
    (slate-rect (edge-node1 gep))
    (slate-from-point gep)
    (slate-to-point gep)
    (slate-rect gep)))

;;;------------------------------------------------------------

(defmethod temp-layout-display ((gep Graph-Edge-Presentation) layout-options)
  (declare (ignore layout-options))
  (compute-edge-attachment-screen-points
    (temp-rect (edge-node0 gep))
    (temp-rect (edge-node1 gep))
    (temp-from-point gep)
    (temp-to-point gep)
    (temp-rect gep)))

(defmethod swap-temp-coords ((gep Graph-Edge-Presentation))
  (rotatef (slate-from-point gep) (temp-from-point gep))
  (rotatef (slate-to-point gep) (temp-to-point gep))
  (rotatef (slate-rect gep) (temp-rect gep)))

;;;------------------------------------------------------------
;;; destructively modifies <edge-pos0>, <edge-pos1>, and  <edge-screen-rect>.

(defun compute-edge-attachment-screen-points (node-rect0 node-rect1
					      edge-pos0 edge-pos1 edge-rect)
  (declare ;;#+:excl (:explain :types :calls)
   (optimize (speed 3)
	     (safety 1)
	     (space 0)
	     (compilation-speed 0))
   (type g:Screen-Rect node-rect0 node-rect1 edge-rect)
   (type g:Screen-Point edge-pos0 edge-pos1))
  (let* ((xmin0 (- (g::%screen-rect-xmin node-rect0) 1))
	 (xmax0 (+ xmin0 (g::%screen-rect-width node-rect0)))
	 (ymin0 (- (g::%screen-rect-ymin node-rect0) 1))
	 (ymax0 (+ ymin0 (g::%screen-rect-height node-rect0)))
	 (xmin1 (- (g::%screen-rect-xmin node-rect1) 1))
	 (xmax1 (+ xmin1 (g::%screen-rect-width node-rect1)))
	 (ymin1 (- (g::%screen-rect-ymin node-rect1) 1))
	 (ymax1 (+ ymin1 (g::%screen-rect-height node-rect1)))	 
	 (midpoint 0))
    (declare (type g:Screen-Coordinate
		   xmin0 xmax0 ymin0 ymax0 xmin1 xmax1 ymin1 ymax1 midpoint))
    (cond ((< xmax0 xmin1) ;; 0 interval is left of 1 interval
	   (setf (g::%screen-point-x edge-pos0) xmax0)
	   (setf (g::%screen-point-x edge-pos1) xmin1)
	   (setf (g::%screen-rect-xmin edge-rect) xmax0)
	   (setf (g::%screen-rect-width edge-rect) (- xmin1 xmax0)))
	  ((< xmax1 xmin0) ;; 0 interval is right of 1 interval
	   (setf (g::%screen-point-x edge-pos0) xmin0)
	   (setf (g::%screen-point-x edge-pos1) xmax1)
	   (setf (g::%screen-rect-xmin edge-rect) xmax1)
	   (setf (g::%screen-rect-width edge-rect) (- xmin0 xmax1)))
	  (t ;; intervals intersect
	   (setf midpoint (ash (+ (max xmin0 xmin1) (min xmax0 xmax1)) -1))
	   (setf (g::%screen-point-x edge-pos0) midpoint)
	   (setf (g::%screen-point-x edge-pos1) midpoint)
	   (setf (g::%screen-rect-xmin edge-rect) midpoint)
	   (setf (g::%screen-rect-width edge-rect) 1)))
    (cond ((< ymax0 ymin1) ;; 0 interval is left of 1 interval
	   (setf (g::%screen-point-y edge-pos0) ymax0)
	   (setf (g::%screen-point-y edge-pos1) ymin1)
	   (setf (g::%screen-rect-ymin edge-rect) ymax0)
	   (setf (g::%screen-rect-height edge-rect) (- ymin1 ymax0)))
	  ((< ymax1 ymin0) ;; 0 interval is right of 1 interval
	   (setf (g::%screen-point-y edge-pos0) ymin0)
	   (setf (g::%screen-point-y edge-pos1) ymax1)
	   (setf (g::%screen-rect-ymin edge-rect) ymax1)
	   (setf (g::%screen-rect-height edge-rect) (- ymin0 ymax1)))
	  (t ;; intervals intersect
	   (setf midpoint (ash (+ (max ymin0 ymin1) (min ymax0 ymax1)) -1))
	   (setf (g::%screen-point-y edge-pos0) midpoint)
	   (setf (g::%screen-point-y edge-pos1) midpoint)
	   (setf (g::%screen-rect-ymin edge-rect) midpoint)
	   (setf (g::%screen-rect-height edge-rect) 1)))))

;;;------------------------------------------------------------

(defmethod edge-l2-norm2 ((gep Graph-Edge-Presentation))
  (let* ((screen-rect (slate-rect gep))
	 (w (g:screen-rect-width screen-rect))
	 (h (g:screen-rect-height screen-rect)))
    (+ (* w w) (* h h))))

(defmethod edge-l2-norm ((gep Graph-Edge-Presentation))
  (let* ((screen-rect (slate-rect gep))
	 (w (g:screen-rect-width screen-rect))
	 (h (g:screen-rect-height screen-rect)))
    (sqrt (+ (* w w) (* h h)))))

;;;============================================================
;;; For Directed Graphs:
;;;============================================================

(defclass Digraph-Edge-Presentation (Graph-Edge-Presentation Arrow-Display) ())

