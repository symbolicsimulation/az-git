;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================

(defclass Simple-Graph (Standard-Object)
     ((nodes
	:type Sequence
	:accessor nodes
	:initarg :nodes)
      (edges
	:type Sequence
	:accessor edges
	:initarg :edges)))

;;;============================================================

(defclass Simple-DiGraph (Simple-Graph) ())

;;;============================================================

(defmethod node-name ((node Integer)) (format nil "~d" node))
(defmethod node-name ((node Symbol)) node)
(defmethod node-name ((node String)) node)
(defmethod edge-node0 ((edge Cons)) (first edge))
(defmethod edge-node1 ((edge Cons)) (second edge))

;;;------------------------------------------------------------

(defun configuration-dimension (graph) (* 2 (length (nodes graph))))

;;;============================================================

(defun collect-nodes-from-edges (edges)
  (let ((nodes ()))
    (dolist (edge edges)
      (pushnew (edge-node0 edge) nodes)
      (pushnew (edge-node1 edge) nodes))
    nodes))

(defun make-simple-graph (edges)
  (make-instance 'Simple-Graph
		 :nodes (collect-nodes-from-edges edges)
		 :edges edges))

(defun make-simple-digraph (edges)
  (make-instance 'Simple-DiGraph
		 :nodes (collect-nodes-from-edges edges)
		 :edges edges))



