;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

  
;;;============================================================

(defclass Graph-Presentation (Presentation)
     ((nodes
	:type Sequence
	:accessor nodes)
      (edges
	:type Sequence
	:accessor edges)
      (initial-node-configuration
       :type (or Null (Simple-Array g:Screen-Coordinate (*)))
       :accessor initial-node-configuration)
      (subject-node-table
       :type Hash-table
       :accessor subject-node-table)
      (subject-edge-table
       :type Hash-table
       :accessor subject-edge-table)
      (node-dragging-interactor
       :type Node-Dragging-Interactor
       :accessor node-dragging-interactor)
      ))

;;;============================================================

(defun make-presentation-for-graph (graph slate
				    &key
				    (display-name nil)
				    (initial-node-configuration nil))
  (tools:type-check slate:Input-Slate Slate)
  (let ((display (allocate-instance (find-class 'Graph-Presentation))))
    (initialize-display
     display
     :build-options
     (if display-name
	 (list :display-name display-name
	       :subject graph
	       :slate slate)
       (list :subject graph
	     :slate slate))
     :layout-options
     (list :initial-node-configuration initial-node-configuration))
    
    display))

;;;============================================================
;;; Traversing the display tree
;;;============================================================

(defmethod map-over-children (result-type function
			      (display Graph-Presentation))
  (cond
   ((null result-type)
    (dolist (node (nodes display)) (funcall function node))
    (dolist (edge (edges display)) (funcall function edge)))
   (t
    (concatenate result-type
      (map result-type function (nodes display))
      (map result-type function (edges display))))))

(defmethod some-child? (function (display Graph-Presentation))
  (or (some function (nodes display))
      (some function (edges display))))

(defmethod find-child-if (function (display Graph-Presentation))
  (let ((found (find-if function (nodes display))))
    (when (null found)
      (setf found (find-if function (edges display))))
    found))

;;;============================================================
;;; Building
;;;============================================================

(defmethod build-display-local ((display Graph-Presentation) build-options)

  (let ((slate (getf build-options :slate))
	(subject (getf build-options :subject)))
    ;; slots inherited from Display
    (setf (slot-value display 'redraw-timestamp)
      (tools:make-timestamp))
    (setf (parent display) nil)
    (setf (display-slate display) slate)
    (setf (slate-rect display) (g:copy-screen-rect (slate:slate-rect slate)))
    (setf (temp-rect display) (g:copy-screen-rect (slate:slate-rect slate)))
    (setf (clipping-region display) nil)
    (setf (temp-clipping-region display) nil)
    (setf (lit? display) nil)

    ;; slots inherited from Presentation
    (setf (subject display) subject)
    (setf (slot-value display 'display-pen) (subject-pen subject)) 
    (setf (display-name display)
      (getf build-options :display-name
	    (format nil "~s presentation" subject)))


    ;; local slots
    (setf (nodes display) ())
    (setf (edges display) ())
    (setf (subject-node-table display) (make-hash-table :test #'eq))
    (setf (subject-edge-table display) (make-hash-table :test #'eq))

    ;; input interactors
    (setf (current-interactor display) (build-interactors display))
    ))

;;;------------------------------------------------------------

(defmethod build-children ((display Graph-Presentation))
  (let* ((subject (subject display))
	 (slate (display-slate display))
	 (snodes (nodes subject))
	 (sedges (edges subject))
	 (sn-table (subject-node-table display))
	 (se-table (subject-edge-table display)))
    ;; make presentations for the nodes in the graph
    (setf (nodes display)
      (tools:with-collection
       (dolist (snode snodes)
	 (let ((gnp (allocate-instance (find-class 'Graph-Node-Presentation))))
	   (build-display-local gnp (list :subject snode
					  :slate slate
					  :parent display))
	   (setf (gethash snode sn-table) gnp)
	   (tools:collect gnp)))))
    (graph-presentation-index-nodes display)


    ;; make presentations for the edges in the graph
    ;; and recreate the graph structure
    ;; in the presentation nodes and edges
    (setf (edges display)
      (tools:with-collection
       (dolist (sedge sedges)
	 (let ((gep (allocate-instance (find-class 'Graph-Edge-Presentation)))
	       (gnp0 (gethash (edge-node0 sedge) sn-table))
	       (gnp1 (gethash (edge-node1 sedge) sn-table)))
	   (build-display-local gep (list :subject sedge
					  :slate slate
					  :parent display))
	   (setf (gethash sedge se-table) gep)
	   (push gep (from-edges gnp0))
	   (push gep (to-edges gnp1))
	   (setf (edge-node0 gep) gnp0)
	   (setf (edge-node1 gep) gnp1)
	   (tools:collect gep)))))))

(defmethod graph-presentation-index-nodes ((display Graph-Presentation))
  (loop for n = (length (nodes display))
   for i from 0
   for node in (nodes display)
   do
    (setf (node-index node) i)
    (setf (node-distance node)
      (make-array n
		  :element-type 'Fixnum
		  :initial-element most-positive-fixnum))))

;;;------------------------------------------------------------

(defmethod build-interactors ((display Graph-Presentation))
  (setf (node-dragging-interactor display)
    (make-instance 'Node-Dragging-Interactor
      :interactor-display display))
  (setf (default-interactor display)
    (make-instance 'Graph-Interactor
      :interactor-display display)))

;;;============================================================
;;; Minimal Layout
;;;============================================================

(defmethod layout-display-local ((display Graph-Presentation) layout-options)
  (let ((initial-node-configuration
	 (getf layout-options :initial-node-configuration)))
    (unless (null initial-node-configuration)
      (assert (= (length initial-node-configuration)
		 (configuration-dimension display))))
    (setf (initial-node-configuration display) initial-node-configuration)))


(defmethod layout-children ((display Graph-Presentation))

  (dolist (node (nodes display)) (layout-display-local node nil))

  (if (null (initial-node-configuration display))
      ;; put the node is random palces
      (dolist (node (nodes display))
	(graph-node-randomize-position node))
    ;; else use the supplied starting position 
    (let ((configuration-vector (initial-node-configuration display))
	  (i 0) x y)
    (dolist (node (nodes display))
      (setf x (aref configuration-vector i))
      (incf i)
      (setf y (aref configuration-vector i))
      (incf i)
      (g:set-screen-point-coords (slate-point node) :x x :y y)
      (setf (g:screen-rect-origin (slate-rect node)) (slate-point node)))))
  
  (dolist (edge (edges display)) (layout-display-local edge nil)))

;;;============================================================

(defmethod draw-display ((display Graph-Presentation))
  (dolist (d (edges display)) (draw-display d))
  (dolist (d (nodes display)) (draw-display d))
  (when (lit? display) (highlight-display display))
  (slate::%force-drawing (display-slate display))
  (tools:stamp-time! (redraw-timestamp display)))

(defmethod erase-display ((display Graph-Presentation))
  (when (lit? display) (lowlight-display display))
  (dolist (d (nodes display)) (erase-display d))
  (dolist (d (edges display)) (erase-display d))
  (slate-rect display)
  (slate::%force-drawing (display-slate display)))

(defmethod repair-damage ((display Graph-Presentation) damaged-rect)
  ;;(check-type damaged-rect g:Screen-Rect)
  (when (intersect-display? display damaged-rect)
    (dolist (edge (edges display))
      (repair-damage edge damaged-rect))
    (dolist (node (nodes display))
      (repair-damage node damaged-rect)))
  (slate::%force-drawing (display-slate display)))

;;;============================================================
;;; Activation
;;;============================================================

(defmethod activate-display ((display Graph-Presentation)
			     activation-options)
  (activate-display-local display activation-options)
  ;;(activate-children display)
  (activate-presentation-constraints display)
  (activate-input display))

(defmethod activate-display-local ((display Graph-Presentation)
				   activation-options)
  activation-options) 

;;;============================================================
;;; Input
;;;============================================================

;;;============================================================
;;; Handling :configure-notify events
;;;============================================================

(defmethod reconfigure-display ((display Graph-Presentation) width height)
   width height
  (erase-display display)
  ;;(layout-display display nil)
  (draw-display display)
  (slate:finish-drawing (display-slate display)))

;;;============================================================

(defgeneric move-node (display gnp pos)
  (declare (type Graph-Presentation display)
	   (type Graph-Node-Presentation gnp)
	   (type g:Screen-Point pos)))

(defmethod move-node ((display Graph-Presentation)
		      (gnp Graph-Node-Presentation)
		      pos)
  (check-type pos g:Screen-point)
  (let* ((gnp-screen-rect (slate-rect gnp))
	 (new-x (g::%screen-point-x pos))
	 (new-y (g::%screen-point-y pos))
	 (w (g::%screen-rect-width gnp-screen-rect))
	 (h (g::%screen-rect-height gnp-screen-rect))
	 (from-edges (from-edges gnp))
	 (to-edges (to-edges gnp)))
    ;; compute new, temp coordinates
    (g:set-screen-point-coords (temp-point gnp) :x new-x :y new-y)
    (g:set-screen-rect-coords
     (temp-rect gnp) :left new-x :top new-y :width w :height h)
    (dolist (edge from-edges)
      (compute-edge-attachment-screen-points
       (temp-rect (edge-node0 edge))
       (slate-rect (edge-node1 edge))
       (temp-from-point edge)
       (temp-to-point edge)
       (temp-rect edge)))
    (dolist (edge to-edges)
      (compute-edge-attachment-screen-points
       (slate-rect (edge-node0 edge))
       (temp-rect (edge-node1 edge))
       (temp-from-point edge)
       (temp-to-point edge)
       (temp-rect edge)))
    ;; erase, swap coords, and draw in new screen-point
    (dolist (edge from-edges)
      (let ((damaged-screen-rect (erase-display edge)))
	(swap-temp-coords edge)
	(draw-display edge)
	(repair-damage display damaged-screen-rect)))
    (dolist (edge to-edges)
      (let ((damaged-screen-rect (erase-display edge)))
	(swap-temp-coords edge)
	(draw-display edge)
	(repair-damage display damaged-screen-rect)))
    (let ((damaged-screen-rect (erase-display gnp)))
      (swap-temp-coords gnp)
      (draw-display gnp)
      (repair-damage display damaged-screen-rect))))

;(defun move-graph-nodes (display)
;  (check-type display Graph-Presentation)
;  (let ((display-slate (slate display)))
;    (setf (slate:message slate)
;	  "mouse-down to move nodes; mouse-down outside window to stop")
;    (loop
;      (let ((node (select-display
;		    display
;		    :test #'(lambda (d) (typep d 'Graph-Node-Presentation))
;		    :return-on-mouse-up nil)))
;        (cond ((outside-display? (slate:mouse-screen-point slate) display)
;	       (return))
;              ((not (null node))
;	       (drag-display node))))
;      (loop (when (slate:mouse-up? slate) (return))))
;    (setf (slate:message slate)"done")))

;;;------------------------------------------------------------
;;; internal method used to find the presentation of a given subject

(defmethod %find-presentation-of (subj (display Graph-Presentation))
  (if (eql subj (subject display))
      (throw :search-done display)
      ;; else
      (dolist (node (nodes display) nil)
	(when (eql subj (subject node))
	  (throw :search-done node)))))

;;;------------------------------------------------------------
;;; Highlights the node(s) currently under <pos>

(defun highlight-nodes-under-screen-point (display pos)
  (check-type display Graph-Presentation)
  (check-type pos g:Screen-Point)
  (dolist (gnp (nodes display))
    (let* ((reg (slate-rect gnp))
	   (lit? (lit? gnp))
	   (under-pos? (g::%screen-point-in-rect? pos reg)))
      (when (and (not lit?) under-pos?) (highlight-display gnp))
      (when (and lit? (not under-pos?)) (lowlight-display gnp)))))

(defun highlight-nodes-under-mouse (display)
  (check-type display Graph-Presentation)
  (let ((slate (display-slate display))
	(screen-rect (slate-rect display))
        (current-pos (g::%make-screen-point 0 0))
	(last-pos (g::%make-screen-point 0 0)))
    (loop (when (slate:mouse-down? slate) (return))
	  (rotatef current-pos last-pos)
	  (slate:mouse-screen-point slate :result current-pos)
	  (unless (or (g::%equal-screen-points? last-pos current-pos)
		      (not (g::%screen-point-in-rect?
			    current-pos screen-rect)))
	    ;; either we haven't moved or we are outside the node
	    ;; boundaries, so we know we are not pointing at any
	    ;; sub-nodes 
	    (highlight-nodes-under-screen-point display current-pos)))))

(defun select-nodes-with-mouse (display)
  (check-type display Graph-Presentation)
  (highlight-nodes-under-mouse display)
  (remove-if-not #'lit? (nodes display)))

(defun select-node-with-mouse (display)
  (check-type display Graph-Presentation)
  (first (select-nodes-with-mouse display)))

(defun drag-node-with-mouse (display)
  ;;(check-type display Graph-Presentation)
  (let ((node (select-node-with-mouse display)))
    (unless (null node) (drag-display node))))

