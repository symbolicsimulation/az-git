;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;============================================================
;;; compute the number of edges in the shortest path between
;;; all pairs of graph-node-presentations in a graph-presentation.

;;; breadth first search (see \cite{Corm90}, p 470).
;;; Use node pens to mark the "color" of the nodes, to make it easy
;;; to animate the search. Use red, green, blue to substitute for
;;; white, gray, black.

(defun undirected-breadth-first-search (graph origin)
  (declare (type Graph-Presentation graph)
	   (type Graph-Node-Presentation origin))
  (let ((origin-index (node-index origin))
	(q (make-queue)))
    (dolist (node (nodes graph))
      (setf (slot-value node 'display-pen) (red-pen))
      (setf (aref (node-distance node) origin-index) most-positive-fixnum)
      (setf (node-predecessor node) nil))
    (setf (slot-value origin 'display-pen) (green-pen))
    (setf (aref (node-distance origin) origin-index) 0)
    (enqueue origin q)
    (loop (when (empty-queue? q) (return))
      (let* ((node0 (dequeue q))
	     (dist0 (aref (node-distance node0) origin-index)))
	(dolist (edge (from-edges node0))
	  (let ((node1 (edge-node1 edge)))
	    (when (eq (slot-value node1 'display-pen) (red-pen))
	      (setf (slot-value node1 'display-pen) (green-pen))
	      (setf (aref (node-distance node1) origin-index) (+ dist0 1))
	      (setf (node-predecessor node1) node0)
	      (enqueue node1 q))))
	(dolist (edge (to-edges node0))
	  (let ((node1 (edge-node0 edge)))
	    (when (eq (slot-value node1 'display-pen) (red-pen))
	      (setf (slot-value node1 'display-pen) (green-pen))
	      (setf (aref (node-distance node1) origin-index) (+ dist0 1))
	      (setf (node-predecessor node1) node0)
	      (enqueue node1 q))))
	(setf (slot-value node0 'display-pen) (blue-pen))))))

(defun animated-undirected-breadth-first-search (graph origin)
  (declare (type Graph-Presentation graph)
	   (type Graph-Node-Presentation origin))
  (let ((origin-index (node-index origin))
	(q (make-queue)))
    (dolist (node (nodes graph))
      (setf (display-pen node) (red-pen))
      (setf (aref (node-distance node) origin-index) most-positive-fixnum)
      (setf (node-predecessor node) nil))
    (setf (display-pen origin) (green-pen))
    (setf (aref (node-distance origin) origin-index) 0)
    (enqueue origin q)
    (loop (when (empty-queue? q) (return))
      (let* ((node0 (dequeue q))
	     (dist0 (aref (node-distance node0) origin-index)))
	(dolist (edge (from-edges node0))
	  (let ((node1 (edge-node1 edge)))
	    (when (eq (display-pen node1) (red-pen))
	      (setf (display-pen node1) (green-pen))
	      (setf (aref (node-distance node1) origin-index) (+ dist0 1))
	      (setf (node-predecessor node1) node0)
	      (enqueue node1 q))))
	(dolist (edge (to-edges node0))
	  (let ((node1 (edge-node0 edge)))
	    (when (eq (display-pen node1) (red-pen))
	      (setf (display-pen node1) (green-pen))
	      (setf (aref (node-distance node1) origin-index) (+ dist0 1))
	      (setf (node-predecessor node1) node0)
	      (enqueue node1 q))))
	(setf (display-pen node0) (blue-pen)))))
  (slate:finish-drawing (display-slate graph)))

;;;============================================================

(defun configuration-point (gp
			    &key
			    (result (make-array (configuration-dimension gp))))
  (tools:type-check Graph-Presentation gp)
  (tools:type-check Vector result)
  (assert (= (configuration-dimension gp) (length result)))

  (let ((i 0) pos)
    (dolist (node (nodes gp))
      (setf pos (slate-point node))
      (setf (aref result i) (g:screen-point-x pos))
      (incf i)
      (setf (aref result i) (g:screen-point-y pos))
      (incf i)))
  result)

(defun node-sizes (gp
		   &key
		   (result (make-array (configuration-dimension gp))))
  (tools:type-check Graph-Presentation gp)
  (tools:type-check Vector result)
  (assert (= (configuration-dimension gp) (length result)))

  (let ((i 0) reg)
    (dolist (node (nodes gp))
      (setf reg (slate-rect node))
      (setf (aref result i) (g:screen-rect-width reg))
      (incf i)
      (setf (aref result i) (g:screen-rect-height reg))
      (incf i)))
  result)

;;;------------------------------------------------------------

(defun move-nodes-to-configuration-point (gp configuration-vector)
  (tools:type-check Graph-Presentation gp)
  (tools:type-check Vector configuration-vector)
  (assert (= (configuration-dimension gp) (length configuration-vector)))

  ;; set new node screen-points in temp coordinates
  (let ((i 0) x y)
    (dolist (node (nodes gp))
      (setf x (round (aref configuration-vector i)))
      (incf i)
      (setf y (round (aref configuration-vector i)))
      (incf i)
      (g:set-screen-point-coords (temp-point node) :x x :y y)
      (setf (g:screen-rect-origin (temp-rect node)) (temp-point node))
      (setf (g:screen-rect-extent (temp-rect node))
	    (g:screen-rect-extent (slate-rect node)))))

  ;; set new edge screen-points in temp coordinates
  (dolist (edge (edges gp))
    (compute-edge-attachment-screen-points
      (temp-rect (edge-node0 edge))
      (temp-rect (edge-node1 edge))
      (temp-from-point edge)
      (temp-to-point edge)
      (temp-rect edge)))

  ;; now erase and redraw with minimum blanking
  (erase-display gp)
  (dolist (node (nodes gp)) (swap-temp-coords node))
  (dolist (edge (edges gp)) (swap-temp-coords edge))
  (draw-display gp))
  
;;;============================================================

(defun read-configuration-point (path &key (configuration-vector nil))
  (tools:type-check (or String Pathname) path)
  (with-open-file (s path :direction :input)
    (let ((n (read s)))
      (if (null configuration-vector)
	  (setf configuration-vector (make-array n))
	(progn
	  (tools:type-check Vector configuration-vector)
	  (assert (= n (length configuration-vector)))))
      (dotimes (i n) (setf (aref configuration-vector i) (read s)))))
  configuration-vector)

;;;============================================================

(defgeneric root-node? (gnp) (declare (type Graph-Node-Presentation gnp)))

(defmethod root-node? ((gnp Graph-Node-Presentation))
  (null (to-edges gnp)))

(defgeneric leaf-node? (gnp) (declare (type Graph-Node-Presentation gnp)))

(defmethod leaf-node? ((gnp Graph-Node-Presentation))
  (null (from-edges gnp)))

(defun pin-roots-to-top (gp)
  (tools:type-check Graph-Presentation gp)
  (let* ((sscreen-rect (slate-rect gp))
	 (sleft (g:screen-rect-left sscreen-rect))
	 (sbottom (g:screen-rect-bottom sscreen-rect))
	 (swidth (g:screen-rect-width sscreen-rect)))
    (dolist (node (nodes gp))
      (when (root-node? node)
	(let ((y (- sbottom (g:screen-rect-height (slate-rect node)))))
	  (g:set-screen-rect-coords
	   (allowed-rect node)
	   :left sleft :top y :width swidth :height 0))))))

(defun pin-roots-to-left (gp)
  (tools:type-check Graph-Presentation gp)
  (let* ((sscreen-rect (slate-rect gp))
	 (sleft (g:screen-rect-left sscreen-rect))
	 (stop (g:screen-rect-top sscreen-rect))
	 (sheight (g:screen-rect-height sscreen-rect)))
    (dolist (node (nodes gp))
      (when (root-node? node)
	(g:set-screen-rect-coords
	 (allowed-rect node)
	 :left sleft :top stop :width 0 :height sheight)))))

