;;; -*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
(in-package :Clay) 

;;;============================================================

(defparameter *all-classes-slate*
    (slate:make-slate
     :width 512 :height 512
     :backing-store :always))

(defparameter *all-classes*
    (make-class-graph
          (cons (find-class 'Standard-Object)
		(all-subclasses 'Standard-Object))))

(defparameter *all-classes-presentation*
    (make-presentation-for-digraph
     *all-classes* *all-classes-slate*
     :display-name "All Classes"))

(time 
 (dolist (origin (nodes *all-classes-presentation*))
   (undirected-breadth-first-search
    *all-classes-presentation* origin)))


(time
 (layout-dag-using-npsol 'graph-energy
			 *all-classes-presentation*
			 :spring-constant 1.0d0
			 :length 64))

(time
 (layout-display-using-npsol 'graph-energy
			     *all-classes-presentation*
			     :spring-constant 1.0d0
			     :length 80))

;;;============================================================

(defparameter *interactor-slate* (slate:make-slate :backing-store :always))
(defparameter *interactor-classes*
    (make-class-graph
          (cons (find-class 'Interactor)
		(all-subclasses 'Interactor))))

(defparameter *Interactor-classes-presentation*
    (make-presentation-for-digraph
     *interactor-classes* *interactor-slate*
     :display-name "Interactor Classes"))

(time 
 (dolist (origin (nodes *interactor-classes-presentation*))
   (undirected-breadth-first-search
    *interactor-classes-presentation* origin)))


(time
 (layout-dag-using-npsol 'graph-energy
			 *interactor-classes-presentation*
			 :spring-constant 1.0d0
			 :length 64))

(time
 (layout-display-using-npsol 'graph-energy
			     *interactor-classes-presentation*
			     :spring-constant 1.0d0
			     :length 64))

;;;============================================================

(defparameter *muchi-presentation-slate*
    (slate:make-slate :backing-store :always))
(defparameter *muchi-presentation-classes*
    (make-class-graph
          (cons (find-class 'Muchi-Presentation)
		(all-subclasses 'Muchi-Presentation))))

(defparameter *Muchi-Presentation-classes-presentation*
    (make-presentation-for-digraph
     *muchi-presentation-classes* *muchi-presentation-slate*
     :display-name "Muchi-Presentation Classes"))

(time 
 (dolist (origin (nodes *muchi-presentation-classes-presentation*))
   (undirected-breadth-first-search
    *muchi-presentation-classes-presentation* origin)))


(time
 (layout-display-using-npsol 'graph-energy
			     *muchi-presentation-classes-presentation*
			     :spring-constant 1.0d0
			     :length 64))

(time
 (layout-dag-using-npsol 'graph-energy
			 *muchi-presentation-classes-presentation*
			 :spring-constant 1.0d0
			 :length 64))

;;;============================================================

(defparameter *stroke-slate*
    (slate:make-slate :backing-store :always))
(defparameter *stroke-classes*
    (make-class-graph
          (cons (find-class 'Stroke)
		(all-subclasses 'Stroke))))

(defparameter *Stroke-classes-presentation*
    (make-presentation-for-digraph
     *stroke-classes* *stroke-slate*
     :display-name "Stroke Classes"))

(time 
 (dolist (origin (nodes *stroke-classes-presentation*))
   (undirected-breadth-first-search
    *stroke-classes-presentation* origin)))


(time
 (layout-display-using-npsol 'graph-energy
			     *stroke-classes-presentation*
			     :spring-constant 1.0d0
			     :length 64))

(time
 (layout-dag-using-npsol 'graph-energy
			 *stroke-classes-presentation*
			 :spring-constant 1.0d0
			 :length 64))

;;;============================================================

(defparameter *penguin-slate* (slate:make-slate
			       :backing-store :always))
(defparameter *penguin-classes*
    (make-class-graph
     (cons
      (find-class 'penguin::Retained-Object)	 
      (all-subclasses 'penguin::Retained-Object))))

(defparameter *penguin-classes-presentation*
    (make-presentation-for-digraph
     *penguin-classes* *penguin-slate*
     :display-name "Penguin Class Inheritance Graph"))

(setf (display-name *penguin-classes-presentation*)
  "Penguin Class Inheritance Graph")
(time 
 (dolist (origin (nodes *penguin-classes-presentation*))
   (undirected-breadth-first-search
    *penguin-classes-presentation* origin)))


(time
 (layout-display-using-npsol 'graph-energy
			     *penguin-classes-presentation*
			     :spring-constant 1.0d0
			     :length 64))

(time
 (layout-dag-using-npsol 'graph-energy
			 *penguin-classes-presentation*
			 :spring-constant 1.0d0
			 :length 64))

;;;============================================================

(defparameter *xpr-graph*
    (make-simple-graph
     `((xwd xwud) (xwd xpr) (xwd |X|)
       (xpr xwd) (xpr xwud) (xpr |X|)
       (xwud xwd) (xwud xpr) (xwud |X|)
       (xdpr xwd) (xdpr xpr) (xdpr lpr) (xdpr xwud) (xdpr |X|)
       (lpr lpq) (lpr lprm) (lpr plot) (lpr pr) (lpr screendump)
       (lpr troff) (lpr printcap) (lpr rasterfile) (lpr lpc)
       (lpr lpd)
       (lpq lpr) (lpq lprm)  (lpq lpc) (lpq lpd)
       (lprm lpr) (lprm lpq)  (lprm lpd)
       (screendump lpr) (screendump rastrepl) (screendump screenload)
       (screendump rasfilter8to1)
       (screenload rastrepl) (screenload screendump)
       (screenload  rasfilter8to1)
       ;;(troff checknr) (troff chmod) (troff eqn)
       (troff lpr)
       ;;(troff nroff) (troff tbl) (troff col)
       (troff printcap)
       ;;(troff man) (troff me) (troff ms)
       (troff lpd)
       )))

(defparameter *xpr-presentation*
    (make-presentation-for-graph
     *xpr-graph*
     (slate:make-slate :width 384 :height 384
		       :slate-name "some Man pages"
		       :backing-store :always)
     :display-name "Man pages"))


(dolist (origin (nodes *xpr-presentation*))
  (animated-undirected-breadth-first-search *xpr-presentation* origin))

(layout-display-using-npsol 'graph-energy
			    *xpr-presentation*
			    :spring-constant 1.0d0
			    :length 80)

(layout-display-using-npsol 'graph-energy0
			    *xpr-presentation*
			    :spring-constant 1.0d0
			    :length 80)


;;;============================================================
;;; from Andersen, Krebs, and Andersen
;;; "STENO: An expert system for medical diagnosis basaed on
;;; graphical models and model search."

(defparameter *aka-slate* (slate:make-slate))

(defparameter *aka-nodes*
    '(sex
      angina
      previous-AMI
      Q-wave-in-ecg
      q-wave-informative
      st-segment-informative
      st-segment-shift
      maximum-workload
      left-ventricular-hypertrophy
      hypercholestrolemia
      smoker
      hereditary-predisposition
      congential-heart-disease
      coronary-artery-disease
      ))

(defparameter *aka-graph*
    (make-simple-graph
     '((sex angina)
       (sex Q-wave-in-ecg)
       (sex st-segment-informative)
       (angina Q-wave-in-ecg)
       (angina hypercholestrolemia)
       (angina maximum-workload)
       (angina left-ventricular-hypertrophy)
       (angina congential-heart-disease)
       (angina st-segment-informative)
       (angina st-segment-shift)
       (angina coronary-artery-disease)
       (angina previous-AMI)
       (previous-ami left-ventricular-hypertrophy)
       (previous-ami maximum-workload)
       (previous-ami congential-heart-disease)
       (previous-ami st-segment-shift)
       (previous-ami st-segment-informative)
       (previous-ami Q-wave-in-ecg)
       (Q-wave-in-ecg hereditary-predisposition)
       (Q-wave-in-ecg left-ventricular-hypertrophy)
       (Q-wave-in-ecg st-segment-informative)
       (Q-wave-in-ecg q-wave-informative)
       (Q-wave-in-ecg coronary-artery-disease)
       (q-wave-informative left-ventricular-hypertrophy)
       (q-wave-informative st-segment-informative)
       (st-segment-informative coronary-artery-disease)
       (st-segment-informative smoker)
       (st-segment-informative maximum-workload)
       (st-segment-informative congential-heart-disease)
       (st-segment-informative st-segment-shift)
       (st-segment-shift congential-heart-disease)
       (st-segment-shift coronary-artery-disease)
       (st-segment-shift left-ventricular-hypertrophy)
       (maximum-workload hypercholestrolemia)
       (maximum-workload left-ventricular-hypertrophy)
       (maximum-workload congential-heart-disease)
       (left-ventricular-hypertrophy congential-heart-disease)
       (hypercholestrolemia coronary-artery-disease)
       (smoker coronary-artery-disease)
       (hereditary-predisposition coronary-artery-disease)
       (congential-heart-disease coronary-artery-disease)

       )))

(defparameter *aka-presentation*
    (make-presentation-for-graph *aka-graph* *aka-slate*))

(time 
 (dolist (origin (nodes *aka-presentation*))
   (undirected-breadth-first-search
    *aka-presentation* origin)))

(time
 (layout-display-using-npsol 'graph-energy
			     *aka-presentation*
			     :spring-constant 1.0d0
			     :length 128
			     :animate? t))

(time
 (layout-display-using-npsol 'graph-energy0
			     *aka-presentation*
			     :spring-constant 1.0d0
			     :length 128
			     :animate? t))

(describe (sixth (nodes *aka-presentation*)))

(defparameter *small-aka-slate* (slate:make-slate))
(defparameter *small-aka-graph*
    (make-simple-graph
     '((cath sex)
       (cath previous-AMI)
       (cath Q-wave-in-ecg)
       (cath q-wave-informative)
       (cath st-segment-shift)
       (cath maximum-workload)
       (cath hypercholestrolemia)
       (cath smoker)
       (cath hereditary-predisposition)
       (cath congential-heart-disease)
       (Q-wave-in-ecg hereditary-predisposition)
       (Q-wave-in-ecg sex)
       (Q-wave-in-ecg q-wave-informative)
       (Q-wave-in-ecg previous-AMI)
       (st-segment-shift previous-AMI)
       (st-segment-shift congential-heart-disease)
       (previous-AMI congential-heart-disease)
       )))

(defparameter *small-aka-presentation*
    (let ((p (make-presentation-for-graph
	     *small-aka-graph* *small-aka-slate*
	     :display-name "AKA")))
      (time 
       (dolist (origin (nodes p))
	 (undirected-breadth-first-search p origin)))
      p))


(time
 (layout-display-using-npsol 'graph-energy
			     *small-aka-presentation*
			     :spring-constant 1.0d0
			     :length 128
			     :animate? t))

(time
 (layout-display-using-npsol 'graph-energy0
			     *small-aka-presentation*
			     :spring-constant 1.0d0
			     :length 128
			     :animate? t))

;;;============================================================
;;; from Lauritzen and Spiegalhalter,
;;; "Local Computations with Probabilities on Graphical Structures
;;; and their application to expert systems,"
;;; JRSSB (1988) 50, (2) 157-224.

(defparameter *ls-slate* (slate:make-slate))
(defparameter *ls-graph*
    (make-simple-digraph '((visit-to-asia tuberculosis)
			   (tuberculosis tb-or-cancer)
			   (lung-cancer tb-or-cancer)
			   (tb-or-cancer positive-x-ray)
			   (smoking lung-cancer)
			   (smoking bronchitis)
			   (bronchitis dyspnea)
			   (tb-or-cancer dyspnea)
			   )))

(defparameter *ls-presentation*
    (make-presentation-for-digraph *ls-graph* *ls-slate*))

(time 
 (dolist (origin (nodes *ls-presentation*))
   (undirected-breadth-first-search
    *ls-presentation* origin)))
(time
 (layout-display-using-npsol 'graph-energy0
			     *ls-presentation*
			     :spring-constant 1.0d0
			     :length 80
			     :animate? t))

(time
 (layout-display-using-npsol 'graph-energy
			     *ls-presentation*
			     :spring-constant 1.0d0
			     :length 80
			     :animate? t))



(time
 (layout-dag-using-npsol 'graph-energy0
			 *ls-presentation*
			 :spring-constant 1.0d0
			 :length 80
			 :animate? t))

(time
 (layout-dag-using-npsol 'graph-energy
			 *ls-presentation*
			 :spring-constant 1.0d0
			 :length 80
			 :animate? nil))

;;;============================================================

(defparameter *cactus-slate* (slate:make-slate))
(defparameter *cactus-classes*
    (make-class-graph)
     (append
      (list (find-class 'Standard-Object)	 
	    (find-class 'cactus::Abstract-Point)
	    (find-class 'cactus::Abstract-Space)
	    )
      (all-subclasses 'cactus::Abstract-Space)
      (all-subclasses 'cactus::Abstract-Point)))

(defparameter *cactus-classes-presentation*
    (make-presentation-for-digraph *cactus-classes* *cactus-slate*))

(time 
 (dolist (origin (nodes *cactus-classes-presentation*))
   (undirected-breadth-first-search
    *cactus-classes-presentation* origin)))

(time 
 (dolist (origin (nodes *cactus-classes-presentation*))
   (animated-undirected-breadth-first-search
    *cactus-classes-presentation* origin)))
(time
 (layout-display-using-npsol 'graph-energy
			     *cactus-classes-presentation*
			     :spring-constant 1.0d0
			     :length 64))

(time
 (layout-dag-using-npsol 'graph-energy
			 *cactus-classes-presentation*
			 :spring-constant 1.0d0
			 :length 64))

(all-superclasses `cactus::Abstract-Point)

;;;============================================================

(defparameter *slate4* (slate:make-slate))
(defparameter *c4* (make-class-graph
		     (cons (find-class 'Display)
			   (all-subclasses 'Display))))

(defparameter *cgp4* (make-presentation-for-digraph *c4* *slate4*))

(dolist (origin (nodes *cgp4*))
  (animated-undirected-breadth-first-search *cgp4* origin))

(layout-display-using-npsol 'graph-energy
			    *cgp4*
			    :spring-constant 1.0d0
			    :length 80)

;;;============================================================

(defparameter *gs* (slate:make-slate))
(defparameter *g* (make-simple-digraph '((a b) (a c) (a d) (a e)
					 (a f) (b z) (c z) (d z)
					 (e z) (f z)
					 (z g) (z h) (i z) (j z)
					 (k z) (l z)
					 )))

(defparameter *gp* (make-presentation-for-digraph *g* *gs*))

(dolist (origin (nodes *gp*))
  (animated-undirected-breadth-first-search *gp* origin))

(layout-display-using-npsol 'graph-energy
			    *gp*
			    :spring-constant 1.0d0
			    :length 80)

(layout-dag-using-npsol 'graph-energy
			    *gp*
			    :spring-constant 1.0d0
			    :length 80)


;;;============================================================

(defparameter *graph* (make-simple-graph '((alpha beta)
					   (beta gamma)
					   (gamma delta)
					   (delta alpha))))

(defparameter *gp* 
    (make-presentation-for-graph
     *graph* *slate*))


(dolist (origin (nodes *gp*))
  (undirected-breadth-first-search *gp* origin))

(dotimes (i 10)
  (layout-display-using-npsol 'graph-energy5 *gp*
			      :length 64
			      :spring-constant 1.0d0
			      ))
(display-pen (first (nodes *gp*)))
(dolist (node (nodes *gp*)) (draw-display node))
(draw-display *gp*)
(undirected-breadth-first-search *gp* (first (nodes *gp*)))
(layout-display-using-npsol *gp*)

(setf (slate:slate-input-handler *slate*)
  #'(lambda ()
      (slate::debugging-slate-input-handler *slate*)))

;;;============================================================

(defparameter *triangles*
	      (make-simple-graph '((a b)(a d)(a e)
				   (b e)(b f)(b c)
				   (c f)(c g)
				   (d h)(d i)(d e)
				   (e i)(e j)(e f)
				   (f j)(f k)(f g)
				   (g k)(g l)
				   (h m)(h i)
				   (i m)(i n)(i j)
				   (j n)(j o)(j k)
				   (k o)(k p)(k l)
				   (l p)
				   (m q)(m n)
				   (n q)(n r)(n o)
				   (o r)(o s)(o p)
				   (p s)(q r)(r s)
				   )))
(length (nodes *triangles*))
(defparameter *trianglep* 
	      (make-presentation-for-graph
		*triangles*
		(slate:default-slate)))

(dolist (origin (nodes *trianglep*))
  (undirected-breadth-first-search *trianglep* origin))

(time
(layout-display-using-npsol 'graph-energy *trianglep*
			    :length 32
			    :spring-constant 1.0d0
			    :animate? t
			    ))


(layout-display-using-npsol 'graph-energy0 *trianglep* :length 128)
(layout-display-using-npsol 'graph-energy1 *trianglep* :length 160)
(layout-display-using-npsol 'graph-energy2 *trianglep*
			    :length 128
			    :spring-constant 4.0d1
			    ;;:repulsion-constant 1.0d-2
			    )
(layout-display-using-npsol 'graph-energy3 *trianglep*
			    :length 64
			    :spring-constant 2.0d2
			    ;;:repulsion-constant 1.0d-2
			    )

(loop for k from 100.0d0 to 1000.0d0 by 25.0d0
 do
  (layout-display-using-npsol 'graph-energy4 *trianglep*
				:length 64
				:spring-constant k
				;;:repulsion-constant 1.0d-2
				)
  (layout-display-using-npsol 'graph-energy4 *trianglep*
				:length 96
				:spring-constant (/ k 5.0)
				;;:repulsion-constant 1.0d-2
				)
  )
(Draw-display *trianglep*)


(file-graph *trianglep*
	    "belgica:/belgica-2g/jam/npsol-layout/triangles.in"
	    "triangles.out")

(move-nodes-to-configuration-point
  *trianglep*
  (read-configuration-point
  "belgica:/belgica-2g/jam/npsol-layout/triangles.out"))

;;;============================================================

(progn
  (setf *subject-pen-consistency-constraint* nil)
  (tap:remove-advice 'subject-pen)
  (setf (tap::naive-definition 'subject-pen) nil)
  (setf (tap::wise-definition 'subject-pen) nil)
  (tap:remove-advice 'set-subject-pen)
  (setf (tap::naive-definition 'set-subject-pen) nil)
  (setf (tap::wise-definition 'set-subject-pen) nil))

(defparameter *bold-pen*
	      (let ((pen (slate:copy-pen (subject-pen 'alpha))))
		(setf (slate:pen-font pen) (slate:font
					     :family :times
					     :face :bold
					     :size 8))
		pen)) 

(defparameter *italic-pen*
	      (let ((pen (slate:copy-pen (subject-pen 'alpha))))
		(setf (slate:pen-font pen) (slate:font
					     :family :times
					     :face :italic
					     :size 8))
		pen))


;;;============================================================
;;; this example is from Bohringer and Paulisch,
;;; "Using constraints to schieve stability in automatic
;;; graph layout algroithms", CHI'90 Proceedings

(defparameter *physics-slate* (slate:make-slate))
(defparameter *physics-g*
	      (make-simple-graph
		'((mechanics general-relativity)
		  (mechanics kinetics)
		  (mechanics statics)
		  (mechanics wave-theory)
		  (mechanics quantum-mechanics)
		  (kinetic-thermodynamics thermodynamics)
		  (electricity electrodynamics)
		  (electricity electrostatics)
		  (kinetics special-relativity)
		  (kinetics atomic-physics)
		  (wave-theory acoustics)
		  (wave-theory optics)
		  (thermodynamics atomic-physics)
		  (electrodynamics atomic-physics)
		  (special-relativity general-relativity)
		  (optics atomic-physics)
		  (atomic-physics quantum-mechanics)
		  (atomic-physics nuclear-physics)
		  (atomic-physics physics-of-solids)
		  (quantum-mechanics elementary-particle-physics)
		  (quantum-mechanics nuclear-physics)
		  (quantum-mechanics physics-of-solids)
		  (nuclear-physics elementary-particle-physics)
		  )))

(defparameter *physics-p* 
	      (make-presentation-for-digraph
		*physics-g*
		*physics-slate*))

(dolist (origin (nodes *physics-p*))
  (animated-undirected-breadth-first-search *physics-p* origin))

(layout-display-using-npsol 'graph-energy
			    *physics-p*
			    :length 120
			    :spring-constant 1.0d2
			    )

(layout-dag-using-npsol 'graph-energy
			*physics-p*
			:length 120
			:spring-constant 1.0d2
			)

;;;============================================================

(defparameter *cg-slate* (slate:make-slate :color nil))
(defparameter *cg* (make-class-graph
		     (cons (find-class 'Presentation)
			   (all-subclasses 'Presentation))))

(defparameter *cgp* (make-class-graph-presentation *cg* *cg-slate*))
(loop (drag-node-with-mouse *cgp*))

;;;============================================================

(defparameter *cg-slate* (slate:make-slate :color nil))
(defparameter *cg* (make-class-graph
		     (all-classes-between 'cactus::Mapping 'cactus::Identity)))

(defparameter *cgp* (make-class-graph-presentation *cg* *cg-slate*))
(loop (drag-node-with-mouse *cgp*))

;;;============================================================

(defparameter *compose-slate* (slate:make-slate :color nil))
(defparameter *compose-method-graph*
	      (make-generic-function-graph #'ca::compose))

(defparameter *compose-graph-presentation*
	      (make-generic-function-presentation
		*compose-method-graph* *compose-slate*))

(loop (drag-node-with-mouse *compose-graph-presentation*))


;;;============================================================

(defparameter *compose-to!-slate* (slate:make-slate :color nil))
(defparameter *compose-to!-method-graph*
	      (make-generic-function-graph #'ca::compose-to!))

(defparameter *compose-to!-graph-presentation*
	      (make-generic-function-presentation
		*compose-to!-method-graph* *compose-to!-slate*))

(loop (drag-node-with-mouse *compose-to!-graph-presentation*))


;;;============================================================

(defparameter *compose-left!-slate* (slate:make-slate :color nil))
(defparameter *compose-left!-method-graph*
	      (make-generic-function-graph #'ca::compose-left!))

(defparameter *compose-left!-graph-presentation*
	      (make-generic-function-presentation 
		*compose-left!-method-graph* *compose-left!-slate*))

(loop (drag-node-with-mouse *compose-left!-graph-presentation*))


;;;============================================================

(defparameter *children-slate* (slate:make-slate :color nil))
(defparameter *children-method-graph*
	      (make-generic-function-graph #'display-children))

(defparameter *children-graph-presentation*
	      (make-presentation-for-digraph
		*children-method-graph* *children-slate*))

(loop (drag-node-with-mouse *children-graph-presentation*))


