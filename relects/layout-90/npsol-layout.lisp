;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (safety 3)
		     (space 0)
		     (speed 1)
		     (compilation-speed 0)))

;;;-----------------------------------------------------------------

;;(proclaim '(inline interval-diff))

(defun interval-diff (xmin0 xmax0 xmin1 xmax1)
  (declare ;;(:explain :calls :types)
	   (optimize (speed 3)
		     (safety 1)
		     (space 0)
		     (compilation-speed 0))
	   (type Double-Float xmin0 xmax0 xmin1 xmax1))
  (cond
   ;; 0 interval is left of 1 interval
   ((< xmax0 xmin1) (- xmax0 xmin1))
   ;; 0 interval is right of 1 interval
   ((< xmax1 xmin0) (- xmin0 xmax1))
   ;; intervals intersect
   (t 0.0d0)))
  

;;;============================================================

(defun make-objfn-for (energy display whs xymaxs alpha
		       edge-length spring-constant animate?)
  (let* ((nodes (nodes display))
	 (l (float edge-length 1.0d0))
	 (d-mtx (make-array (length nodes)))
	 (rect (slate:slate-rect (display-slate display)))
	 (x0 (/ (+ (g:screen-rect-left rect) (g:screen-rect-right rect))
		2.0d0))
	 (y0 (/ (+ (g:screen-rect-top rect) (g:screen-rect-bottom rect))
		2.0d0))
	 (ij-pairs (loop
		    for edge in (edges display)
		    for i = (position (edge-node0 edge) nodes)
		    for j = (position (edge-node1 edge) nodes)
		    collect (cons (min i j) (max i j)))))
    (loop for node in nodes
     for i from 0 do
      (setf (aref d-mtx i) (node-distance node)))
    (setf ij-pairs (sort ij-pairs
			 #'(lambda (pair0 pair1)
			     (or (< (car pair0) (car pair1))
				 (and (= (car pair0) (car pair1))
				      (< (cdr pair0) (cdr pair1)))))))
    #'(lambda (mode n x objf objgrd nstate)
	(declare (ignore n nstate))
	(setf (aref objf 0)
	  (funcall energy (aref mode 0)
		   d-mtx ij-pairs alpha x0 y0 l spring-constant
		   x xymaxs whs objgrd))
	(when (and animate? (= (aref mode 0) 2))
	  (move-nodes-to-configuration-point display x)))))

;;;============================================================

(deftype Array-Index () '(Integer 0 #.array-dimension-limit))

(defun graph-energy0 (mode d-mtx edges alpha x0 y0
		      l k xymins xymaxs whs xygrad)
  (declare (optimize (safety 3)
		     (space 0)
		     (speed 0)
		     (compilation-speed 0))
	   ;;(:explain :calls :types)
	   (type (Simple-Array (Simple-Array Fixnum (*)) (*)) d-mtx)	   
	   (ignore edges alpha x0 y0 xymaxs)
	   (type Array-Index mode)
	   (type Double-Float l k)
	   (type (Simple-Array Double-Float (*)) xymins xymaxs whs xygrad))
  (let ((spring-energy 0.0d0)
	(nnodes (truncate (length xymins) 2)))
    (declare (type Array-Index nnodes)
	     (type Double-Float spring-energy repulsion-energy l2))
    (when (> mode 0) ;; compute the gradient
      (dotimes (i (length xygrad))
	(declare (type Array-Index i))
	(setf (aref xygrad i) 0.0d0)))
    (dotimes (i nnodes)
      (declare (type Array-Index i))
      (let* ((2i (ash i 1)) (2i+1 (+ 2i 1))
	     (xmini (aref xymins 2i))
	     (xmaxi (+ xmini (aref whs 2i)))
	     (ymini (aref xymins 2i+1))
	     (ymaxi (+ ymini (aref whs 2i+1)))
	     (d-vec (aref d-mtx i)))
	(declare (type Array-Index 2i 2i+1)
		 (type Double-Float xmini ymini xmaxi ymaxi)
		 (type (Simple-Array Fixnum (*)) d-vec))
	;;(print (list (cons xmini xmaxi) (cons ymini ymaxi)))
	;;(finish-output)
	(dotimes (j nnodes)
	  (declare (type Array-Index j))
	  (when (/= i j)
	    (let* ((2j (ash j 1)) (2j+1 (+ 2j 1))
		   (xminj (aref xymins 2j))
		   (xmaxj (+ xminj (aref whs 2j)))
		   (xmini-xmaxj (- xmini xmaxj))
		   (xmaxi-xminj (- xmaxi xminj))
		   (xfactor 1.0d0)
		   (dxij (cond ((< xmaxi xminj)
				(+ (* 0.75d0 xmaxi-xminj)
				   (* 0.25d0 xmini-xmaxj)))
			       ((< xmaxj xmini)
				(+ (* 0.25d0 xmaxi-xminj)
				   (* 0.75d0 xmini-xmaxj)))
			       (t 
				(setf xfactor 0.50d0)
				(* 0.25d0 (+ xmaxi-xminj xmini-xmaxj)))))
		   (yminj (aref xymins 2j+1))
		   (ymaxj (+ yminj (aref whs 2j+1)))
		   (ymini-ymaxj (- ymini ymaxj))
		   (ymaxi-yminj (- ymaxi yminj))
		   (yfactor 1.0d0)
		   (dyij (cond ((< ymaxi yminj)
				(+ (* 0.75d0 ymaxi-yminj)
				   (* 0.25d0 ymini-ymaxj)))
			       ((< ymaxj ymini)
				(+ (* 0.25d0 ymaxi-yminj)
				   (* 0.75d0 ymini-ymaxj)))
			       (t  
				(setf yfactor 0.50d0)
				(* 0.25d0 (+ ymaxi-yminj ymini-ymaxj)))))
		   (dij (sqrt (the Double-Float
				(+ (* dxij dxij) (* dyij dyij)))))
		   (d (* l (the Double-Float (float (aref d-vec j) 1.0d0)))))
	      (declare (type Array-Index 2j 2j+1)
		       (type Double-Float dij dxij dyij d xfactor yfactor))
	      (if (< dij double-float-epsilon)
		  ;; then handle singular case, gradient is zero
		  (progn
		    (when (evenp mode) ;; compute the energy
		      (incf spring-energy k))
		    (when (> mode 0) ;; compute the gradient
		      (let* ((alpha (/ (* -2.0d0 k) d))
			     (xalpha (* xfactor alpha))
			     (yalpha (* yfactor alpha)))
			(declare (type Double-Float alpha xalpha yalpha))
			(incf (aref xygrad 2i) xalpha)
			(incf (aref xygrad 2i+1) yalpha)
			(decf (aref xygrad 2j) xalpha)
			(decf (aref xygrad 2j+1) yalpha))))
		;; else
		(let ((dij-d/d (- (/ dij d) 1.0d0)))
		  (declare (type Double-Float dij-d/d))
		  (when (evenp mode) ;; compute the energy
		    (incf spring-energy (* k dij-d/d dij-d/d)))
		  (when (> mode 0) ;; compute the gradient
		    (let* ((alpha (/ (* 2.0d0 k dij-d/d) dij d))
			   (xgradij (* xfactor alpha dxij))
			   (ygradij (* yfactor alpha dyij)))
		      (declare (type Double-Float alpha xgradij ygradij))
		      (incf (aref xygrad 2i) xgradij)
		      (incf (aref xygrad 2i+1) ygradij)
		      (decf (aref xygrad 2j) xgradij)
		      (decf (aref xygrad 2j+1) ygradij))))))))))
    #||
    (when (> mode 0)
      (print xygrad)
      (finish-output))
    ||#
    spring-energy))

(defun graph-energy (mode d-mtx edges alpha x0 y0 l k xymins xymaxs whs xygrad)
  (declare (optimize (safety 1)
		     (space 0)
		     (speed 3)
		     (compilation-speed 0))
	   ;;(:explain :calls :types)
	   (type (Simple-Array (Simple-Array Fixnum (*)) (*)) d-mtx)	   
	   (ignore edges alpha x0 y0 xymaxs whs)
	   (type Array-Index mode)
	   (type Double-Float l k)
	   (type (Simple-Array Double-Float (*)) xymins xymaxs whs xygrad))
  (let ((spring-energy 0.0d0)
	(nnodes (truncate (length xymins) 2)))
    (declare (type Array-Index nnodes)
	     (type Double-Float spring-energy repulsion-energy l2))
    (dotimes (i (length xygrad))
      (declare (type Array-Index i))
      (setf (aref xygrad i) 0.0d0))
    (dotimes (i nnodes)
      (declare (type Array-Index i))
      (let* ((2i (ash i 1)) (2i+1 (+ 2i 1))
	     (xi (aref xymins 2i)) (yi (aref xymins 2i+1))
	     (d-vec (aref d-mtx i)))
	(declare (type Array-Index 2i 2i+1)
		 (type Double-Float xi yi)
		 (type (Simple-Array Fixnum (*)) d-vec))
	;;(setf (aref xymaxs 2i) (+ xi (aref whs 2i)))
	;;(setf (aref xymaxs 2i+1) (+ yi (aref whs 2i+1)))
	(dotimes (j nnodes)
	  (declare (type Array-Index j))
	  (when (/= i j)
	    (let* ((2j (ash j 1)) (2j+1 (+ 2j 1))
		   (dxij (- xi (aref xymins 2j)))
		   (dyij (- yi (aref xymins 2j+1)))
		   (dij (sqrt (the Double-Float
				(+ (* dxij dxij) (* dyij dyij)))))
		   (d (* l (the Double-Float (float (aref d-vec j) 1.0d0)))))
	      (declare (type Array-Index 2j 2j+1)
		       (type Double-Float dij dxij dyij d))
	      (if (< dij double-float-epsilon)
		  ;; then handle singular case, gradient is zero
		  (progn
		    (when (evenp mode) ;; compute the energy
		      (incf spring-energy k))
		    (when (> mode 0) ;; compute the gradient
		      (let ((alpha (/ (* -2.0d0 k) d)))
			(declare (type Double-Float alpha))
			(incf (aref xygrad 2i) alpha)
			(incf (aref xygrad 2i+1) alpha)
			(decf (aref xygrad 2j) alpha)
			(decf (aref xygrad 2j+1) alpha))))
		;; else
		(let ((dij-d/d (- (/ dij d) 1.0d0)))
		  (declare (type Double-Float dij-d/d))
		  (when (evenp mode) ;; compute the energy
		    (incf spring-energy (* k dij-d/d dij-d/d)))
		  (when (> mode 0) ;; compute the gradient
		    (let* ((alpha (/ (* 2.0d0 k dij-d/d) dij d))
			   (xgradij (* alpha dxij))
			   (ygradij (* alpha dyij)))
		      (declare (type Double-Float alpha xgradij ygradij))
		      (incf (aref xygrad 2i) xgradij)
		      (incf (aref xygrad 2i+1) ygradij)
		      (decf (aref xygrad 2j) xgradij)
		      (decf (aref xygrad 2j+1) ygradij))))))))))
    spring-energy))

;;;============================================================

(defun layout-display-using-npsol (energy display
				   &key
				   (animate? t)
				   (alpha 1.0d0)
				   (length 32)
				   (spring-constant 1.0d0))
  (let* ((bounding-rect (slate:slate-rect (display-slate display)))
	 (xmin (float (g:screen-rect-xmin bounding-rect) 1.0d0))
	 (xmax (float (g:screen-rect-xmax bounding-rect) 1.0d0))
	 (ymin (float (g:screen-rect-ymin bounding-rect) 1.0d0))
	 (ymax (float (g:screen-rect-ymax bounding-rect) 1.0d0))
	 (nodes (nodes display))
	 (n (* 2 (length nodes)))
	 (x (make-array n :element-type 'Double-Float))
	 (whs (make-array n :element-type 'Double-Float))
	 (xymaxs (make-array n :element-type 'Double-Float))
	 (bl (make-array n :element-type 'Double-Float))
	 (bu (make-array n :element-type 'Double-Float))
	 (a (make-array (list n 0) :element-type 'Double-Float
			:initial-element 0.0d0))
	 (i 0))
    (dolist (node nodes)
      (let* ((rect (slate-rect node))
	     (w (float (g:screen-rect-width rect) 1.0d0))
	     (h (float (g:screen-rect-height rect) 1.0d0)))
	(setf (aref x i) (float (g:screen-rect-xmin rect) 1.0d0))
	(setf (aref whs i) w)
	(setf (aref bl i) xmin)
	(setf (aref bu i) (- xmax w))
	(incf i)
	(setf (aref x i) (float (g:screen-rect-ymin rect) 1.0d0))
	(setf (aref whs i) h)
	(setf (aref bl i) ymin)
	(setf (aref bu i) (- ymax h))
	(incf i)))
    
    (npsol:set-npsol-option "Major Print Level = 10")
    (npsol:set-npsol-option "Verify Level = 0")
    (npsol:set-npsol-option "Derivative Level = 3")
    (npsol:set-npsol-option "Linesearch Tolerance  = 0.1")
    (npsol:set-npsol-option "Minor Iteration Limit = 200")
    (npsol:set-npsol-option "Major Iteration Limit = 200")
    (npsol:set-npsol-option "Optimality Tolerance = 1.0d-6")
    (npsol:set-npsol-option "Infinite Bound = 1.0d15")
    (npsol:npsol-minimize
     (make-objfn-for energy display whs xymaxs alpha length spring-constant
		     animate?)
     x a bl bu)
    (move-nodes-to-configuration-point display x)
    ))


;;;============================================================

(defun layout-dag-using-npsol (energy display
			       &key
			       (animate? t)
			       (alpha 1.0d0)
			       (length 32)
			       (spring-constant 1.0d0))
  (let* ((bounding-rect (slate:slate-rect (display-slate display)))
	 (xmin (float (g:screen-rect-xmin bounding-rect) 1.0d0))
	 (xmax (float (g:screen-rect-xmax bounding-rect) 1.0d0))
	 (ymin (float (g:screen-rect-ymin bounding-rect) 1.0d0))
	 (ymax (float (g:screen-rect-ymax bounding-rect) 1.0d0))
	 (nodes (nodes display))
	 (edges (edges display))
	 (n (* 2 (length nodes)))
	 (nedges (length edges))
	 (nbounds (+ n nedges))
	 (x (make-array n :element-type 'Double-Float))
	 (whs (make-array n :element-type 'Double-Float))
	 (xymaxs (make-array n :element-type 'Double-Float))
	 (bl (make-array nbounds
			 :element-type 'Double-Float
			 :initial-element 64.0d0))
	 (bu (make-array nbounds
			 :element-type 'Double-Float
			 :initial-element 1.0d15))
	 (a (make-array (list n nedges)
			:element-type 'Double-Float
			:initial-element 0.0d0))
	 (i 0))
    (dolist (node nodes)
      (let* ((rect (slate-rect node))
	     (w (float (g:screen-rect-width rect) 1.0d0))
	     (h (float (g:screen-rect-height rect) 1.0d0)))
	(setf (aref x i) (float (g:screen-rect-xmin rect) 1.0d0))
	(setf (aref whs i) w)
	(setf (aref bl i) xmin)
	(setf (aref bu i) (- xmax w))
	(incf i)
	(setf (aref x i) (float (g:screen-rect-ymin rect) 1.0d0))
	(setf (aref whs i) h)
	(setf (aref bl i) ymin)
	(setf (aref bu i) (- ymax h))
	(incf i)))
    (setf i 0)
    (dolist (edge edges)
      (let* ((from-node (edge-node0 edge))
	     (from-index (node-index from-node))
	     (to-node (edge-node1 edge))
	     (to-index (node-index to-node))
	     )
	(setf (aref a (+ 1 (* 2 from-index)) i) -1.0d0)
	(setf (aref a (+ 1 (* 2 to-index)) i) 1.0d0)
	(incf i)))
    
    (npsol:set-npsol-option "Major Print Level = 10")
    (npsol:set-npsol-option "Verify Level = 0")
    (npsol:set-npsol-option "Derivative Level = 3")
    (npsol:set-npsol-option "Linesearch Tolerance  = 0.1")
    (npsol:set-npsol-option "Minor Iteration Limit = 200")
    (npsol:set-npsol-option "Major Iteration Limit = 200")
    (npsol:set-npsol-option "Optimality Tolerance = 1.0d-6")
    (npsol:set-npsol-option "Infinite Bound = 1.0d15")
    (npsol:npsol-minimize
     (make-objfn-for energy display whs xymaxs alpha length spring-constant
		     animate?)
     x a bl bu)
    (move-nodes-to-configuration-point display x)
    ))
