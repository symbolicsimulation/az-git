;;;-*- Package: :Clay; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1991. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  Any distribution of this
;;; software or derivative works must comply with all applicable United
;;; States export control laws.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;============================================================

(in-package :Clay) 

(proclaim '(optimize (compilation-speed 0)
		     (safety 3)
		     (space 0)
		     (speed 0)))

;;;============================================================
			     
(defun make-random-slate-points (n slate)
  (let* ((screen-rect (slate:slate-rect slate))
	 (xmin (g:screen-rect-left screen-rect))
	 (xmax (g:screen-rect-right screen-rect))
	 (ymin (g:screen-rect-top screen-rect))
	 (ymax (g:screen-rect-bottom screen-rect))
	 (screen-points ()))
    (dotimes (i n)
      (push (make-random-screen-point
	     :xmin xmin :xmax xmax
	     :ymin ymin :ymax ymax)
	    screen-points))
    screen-points))

;;;============================================================

(defun make-node () (gensym "n"))

(defun make-n-nodes (n)
  (let ((nodes ()))
    (dotimes (i n) (push (make-node) nodes))
    nodes))

;;;============================================================

(defun make-complete-graph (size)
  (let ((nodes (make-n-nodes size))
	(edges ()))
    (mapcon
      #'(lambda (remaining-nodes)
	  (let ((node (first remaining-nodes))
		(others (rest remaining-nodes)))
	    (dolist (other others) (push (cons node other) edges))))
      nodes)
    (make-instance 'Simple-Graph :nodes nodes :edges edges)))

;;;------------------------------------------------------------

(defun make-random-graph (size edge-probability)
  (let ((nodes (make-n-nodes size))
	(edges ()))
    (mapcon
      #'(lambda (remaining-nodes)
	  (let ((node (first remaining-nodes))
		(others (rest remaining-nodes)))
	    (dolist (other others)
	      (when (<= (random 1.0) edge-probability)
		(push (cons node other) edges)))))
      nodes)
    (make-instance 'Simple-Graph :nodes nodes :edges edges)))

;;;------------------------------------------------------------

(defun make-random-digraph (size edge-probability)
  (let ((nodes (make-n-nodes size))
	(edges ()))
    (mapcan
      #'(lambda (node)
	  (dolist (other nodes)
	    (unless (or (eq node other) 
			(> (random 1.0) edge-probability))
	      (push (cons node other) edges))))
      nodes)
    (make-instance 'Simple-DiGraph :nodes nodes :edges edges)))

;;;------------------------------------------------------------

(defun make-complete-nary-tree (n depth)
  (let ((root (make-node)))
    (multiple-value-bind
      (nodes edges) (%make-complete-nary-tree root n depth)
      (make-instance 'Simple-DiGraph
		     :nodes (cons root nodes)
		     :edges edges))))

(defun %make-complete-nary-tree (root n depth)
  (let* ((children (make-n-nodes n))
	 (nodes (copy-list children))
	 (edges (map 'list #'(lambda (child) (cons root child)) children)))
    (unless (= depth 1)
      (dolist (child children)
	(multiple-value-bind
	  (more-nodes more-edges)
	    (%make-complete-nary-tree child n (- depth 1))
	  (setf nodes (nconc nodes more-nodes))
	  (setf edges (nconc edges more-edges)))))
    (values nodes edges)))

;;;------------------------------------------------------------

(defun make-random-nary-tree (n depth)
  (let ((root (make-node)))
    (multiple-value-bind
      (nodes edges) (%make-random-nary-tree root n depth)
      (make-instance 'Simple-DiGraph
		     :nodes (cons root nodes)
		     :edges edges))))

(defun %make-random-nary-tree (root n depth)
  (let* ((children (make-n-nodes (random (+ n 1))))
	 (nodes (copy-list children))
	 (edges (map 'list #'(lambda (child) (cons root child)) children)))
    (unless (= depth 1)
      (dolist (child children)
	(multiple-value-bind
	  (more-nodes more-edges)
	    (%make-random-nary-tree child n (- depth 1))
	  (setf nodes (nconc nodes more-nodes))
	  (setf edges (nconc edges more-edges)))))
    (values nodes edges)))


;;;============================================================
