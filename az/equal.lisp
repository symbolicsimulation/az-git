;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: :Az; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Az)

;;;=======================================================
;;; A general purpose function for testing equivalence
;;;=======================================================

(defgeneric equal? (o0 o1)
  
  #-sbcl (declare (type T o0 o1))

  (:documentation
   "Are the two objects equal, in whatever sense is appropriate
for the two types?"))

(defmethod equal? ((o0 T) (o1 T))

  "The default sense of equivalence is <eql>."
  (eql o0 o1))
  
