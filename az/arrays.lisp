;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Az -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved.
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;=======================================================

(in-package :Az)

;;;=======================================================
;;; General Array Operations
;;;=======================================================

(defun array-data (array)

  "This function should return a simple 1d array of the same element
type as <array> occupying the same memory locations as <array>. The
correspondence between elements should be the same as for displaced
arrays. If this is not possible, a call to <array-data> should produce
an error."

  (declare (type Simple-Array array))
  
  (if (= (array-rank array) 1)
      ;; then do nothing
      array
    ;; else
    #+:excl (cdr (excl::ah_data array))
    #+:cmu (kernel:%array-data-vector array)
    #-(or :excl :cmu) (error "Can't coerce ~a to 1d array." array)))

;;;=======================================================

(defun %copy-vector! (v result)
  (declare (type Vector v result))
  (dotimes (i (length v)) (setf (aref result i) (aref v i)))
  result)

(defun copy-vector (v &key (result (make-array (length v))))

  "Assumes <result> is at least as long as <v> and of a type that can
hold the elements of <v>. Copies (the pointer) to each element of <v>
to the corresponding place in <result>. If <result> is longer than
<v>, elements beyond the length of <v> are unchanged."

  (declare (type Vector v result)
	   (:returns result))
  (%copy-vector! v result)
  result)

;;;=======================================================

(defun equal-array-dimensions? (a0 &rest others)
  (declare (type Array a0)
	   (type List others)
	   (:returns Boolean))
  
  (let ((rank (array-rank a0))
	(dims (array-dimensions a0)))
    (declare (type az:Positive-Fixnum rank)
	     (type list dims))
    (flet ((%equal-array-dimensions? (a1)
	     (declare (type array a1)
		      (:returns Boolean))
	     (and (= rank (array-rank a1))
		  (equal dims (array-dimensions a1)))))
      
      (every #'%equal-array-dimensions? others))))

;;;-------------------------------------------------------

(defun copy-array-contents (a0
			    &optional
			    (a1 (make-array
				 (array-dimensions a0)
				 :element-type (array-element-type a0))
				a1-supplied?))
  (declare (type Array a0 a1))
  (when a1-supplied?
    (assert (equal-array-dimensions? a0 a1))
    (assert (eql (array-element-type a0) (array-element-type a1)))
    (assert (not (eq a0 a1))))
  (%copy-vector! (array-data a0) (array-data a1))
  a1)

;;;=======================================================

(defmethod new-copy ((a Array))
  (let ((result (make-array (array-dimensions a)
			    :element-type (array-element-type a))))
    (copy-array-contents a result)
    result))

(defmethod verify-copy-result? ((a Array) (result Array))
  (and (equal-array-dimensions? a result)
       (eql (array-element-type a) (array-element-type result))))
  
(defmethod copy-to! ((a Array) (result Array))
  (copy-array-contents a result) 
  result)

;;;=======================================================

(defun print-array (a &optional (stream *standard-output*))
  (let ((*print-array* t)
	(*print-pretty* t))
    #-sbcl (declare (special *print-array* *print-pretty*)) ; $$$ package lock error -jm
    (print a stream)))
 
;;;------------------------------------------------------------
;;; Specialized element-type array operations
;;;------------------------------------------------------------

(defun zero-real-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Real-Vector v))
  (dotimes (i (length v))
    (declare (type Real i))
    (setf (aref v i) 0))
  v)

(defun zero-real-array (a)
  (declare (type Real-Array a))
  (zero-real-vector (array-data a))
  a)

(defun fill-real-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type (Real-Array *) array)
	   (type Real value))
  (if (= value 0)
      (zero-real-array array)
    (let ((adv (array-data array)))
      (declare (type Real-Vector adv))
      (dotimes (i (length adv))
	(declare (type Real i))
	(setf (aref adv i) value))))
  array)

(defun make-real-array (dims &key (initial-element 0))
  (declare (type List dims)
	   (type Real initial-element)
	   (:returns (type (Real-Array *))))
  (fill-real-array (make-array dims :element-type 'Real) initial-element))

(defun make-real-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Real initial-element)
	   (:returns (type Real-Vector)))
  (make-real-array (list len) :initial-element initial-element))

(defun make-real-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Real initial-element)
	   (:returns (type Real-Matrix)))
  (make-real-array (list d0 d1) :initial-element initial-element))
 
;;;=======================================================================

(declaim (inline zero-card8-vector))
(defun zero-card8-vector (array)
  (declare (optimize (safety 0) (speed 3))
	   (type Card8-Vector array))
  #+(and :excl :accelerators) ;; call a C accelerator
  (zero_byte_array (length array) array)
  #-(and :excl :accelerators)
  (dotimes (i (length array))
    (declare (type Card28 i))
    (setf (aref array i) 0))
  array)

(declaim (inline unit-card8-vector))
(defun unit-card8-vector (adv)
  (declare (optimize (safety 0) (speed 3))
	   (type Card8-Vector adv))
  #+(and :excl :accelerators)  ;; call a C accelerator
  (unit_byte_array (length adv) adv)
  #-(and :excl :accelerators)
  (dotimes (i (length adv))
    (declare (type Card28 i))
    (setf (aref adv i) 1))
  adv)

(defun zero-card8-array (array)
  (declare (type (Card8-Array *) array))
   (zero-card8-vector (array-data array)))

(defun unit-card8-array (array)
  (declare (type (Card8-Array *) array))
  (unit-card8-vector (array-data array))
  array)

(defun fill-card8-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type Card8-Array array)
	   (type Card8 value))
  (case value
    (0 (zero-card8-array array))
    (1 (unit-card8-array array))
    (otherwise
     (let ((adv (array-data array)))
       (declare (type Card8-Vector adv))
       (dotimes (i (length adv))
	 (declare (type Array-Index i))
	 (setf (aref adv i) value)))))
  array)

(defun make-card8-array (dims &key (initial-element 0))
  (declare (type List dims)
	   (type Card8 initial-element)
	   (:returns (type Card8-Array)))
  (fill-card8-array (make-array dims :element-type 'Card8) initial-element))

(defun make-card8-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Card8 initial-element)
	   (:returns (type Card8-Vector)))
  (make-card8-array (list len) :initial-element initial-element))

(defun make-card8-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Card8 initial-element)
	   (:returns (type Card8-Matrix)))
 (make-card8-array (list d0 d1) :initial-element initial-element))

;;;------------------------------------------------------------

(defun copy-card8-vector (v0 v1)
  (declare (optimize (safety 0) (speed 3))
	   (type Card28-Vector v0 v1))
  (dotimes (i (length v0))
    (declare (type Card28 i))
    (setf (aref v1 i) (aref v0 i))))

(defun read-card8-array (path nskip nbytes array)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String path)
	   (type Card28 nskip nbytes)
	   (type (Card8-Array *) array)
	   (inline read-byte))
  (format t "~&Reading ~d bytes from ~s~%" nbytes path)
  (force-output)
    
  #+(and :excl :accelerators)  ;; call a C accelerator
  (read_byte_array path nskip nbytes array)

  #-(and :excl :accelerators)
  (let ((adv (array-data array)))
    (declare (type Card8-Vector adv))

    #+:cmu
    (block :file-open-error
      (let ((fd (unix:unix-open path unix:O_RDONLY 0)))
	(declare (type (or Null unix:Unix-FD) fd))
	(if (null fd)
	    ;; then file didn't open
	    (progn (cerror "Ignore error." "Can't open ~a." path)
		   (return-from :file-open-error))
	  ;; else
	  (unwind-protect
	      ;; protected form:
	      (sys:without-gcing
	       (unix:unix-lseek fd nskip unix:L_SET)
	       (unix:unix-read fd (sys:vector-sap adv) nbytes))
	    ;; clean up form:
	    (unix:unix-close fd)))))

    #-:cmu
    (with-open-file (stream path :direction :input :element-type 'Card8)
      (dotimes (i nskip) (declare (type Card28 i)) (read-byte stream))
      (dotimes (i nbytes)
	(declare (type Card28 i))
	(setf (aref adv i) (the az:Card8 (read-byte stream))))))
  (values))

(defun write-card8-array (path nbytes array)
  (declare (optimize (safety 0) (speed 3))
	   (type Simple-String path)
	   (type Card28 nbytes)
	   (type (Card8-Array *) array))
  (format t "~&Writing ~d bytes to ~s~%" nbytes path)
  (force-output)
  (let ((adv (array-data array)))
    (declare (type Card8-Vector adv))
    (with-open-file (stream path 
		     :direction :output 
		     :element-type 'Card8
		     :if-exists :supersede
		     )
      (dotimes (i nbytes)
	(declare (type Card28 i))
	(write-byte (aref adv i) stream))))
  (values))
 
;;;==================================================================

(declaim (inline zero-card16-vector))
(defun zero-card16-vector (adv)
  (declare (optimize (safety 0) (speed 3))
	   (type Card16-Vector adv))
  #+(and :excl :accelerators)  ;; call a C accelerator
  (zero_unsigned_short_array (length adv) adv)
  #-(and :excl :accelerators)
  (dotimes (i (length adv))
    (declare (type Card28 i))
    (setf (aref adv i) 0))
  (values))

(defun zero-card16-array (array)
  (declare (type Card16-Array array))
  (zero-card16-vector (array-data array))
  array)

(defun fill-card16-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type Card16-Array array)
	   (type Card16 value))
  (if (= value 0)
      (zero-card16-array array)
    (let ((adv (array-data array)))
      (declare (type Card16-Vector adv))
      (dotimes (i (length adv))
	(declare (type Array-Index i))
	(setf (aref adv i) value))))
  array)

(defun make-card16-array (dims &key (initial-element 0))
  "Make a Simple-Array of Card16 of the desired dimensions,
initializing the elements by default to 0."
  (declare (type List dims)
	   (type Card16 initial-element)
	   (:returns (type Card16-Array)))
  (fill-card16-array (make-array dims :element-type 'Card16) initial-element))

(defun make-card16-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Card16 initial-element)
	   (:returns (type Card16-Vector)))
  (make-card16-array (list len) :initial-element initial-element))

(defun make-card16-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Card16 initial-element)
	   (:returns (type Card16-Matrix)))
  (make-card16-array (list d0 d1) :initial-element initial-element))
 
;;;==================================================================

(defun zero-card24-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Card24-Vector v))
  (dotimes (i (length v))
    (declare (type Array-Index i))
    (setf (aref v i) 0))
  v)

(defun zero-card24-array (array)
  (declare (type Card24-Array array))
  (zero-card24-vector (array-data array))
  array)

(defun fill-card24-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type Card24-Array array)
	   (type Card24 value))
  (if (= value 0)
      (zero-card24-array array)
    (let ((adv (array-data array)))
      (declare (type Card24-Vector adv))
      (dotimes (i (length adv))
	(declare (type Array-Index i))
	(setf (aref adv i) value))))
  array)

(defun make-card24-array (dims &key (initial-element 0))
  "Make a Simple-Array of Card24 of the desired dimensions,
initializing the elements by default to 0."
  (declare (type List dims)
	   (type Card24 initial-element)
	   (:returns (type Card24-Array)))
  (fill-card24-array (make-array dims :element-type 'Card24) initial-element))

(defun make-card24-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Card24 initial-element)
	   (:returns (type Card24-Vector)))
  (make-card24-array (list len) :initial-element initial-element))

(defun make-card24-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Card24 initial-element)
	   (:returns (type Card24-Matrix)))
  (make-card24-array (list d0 d1) :initial-element initial-element))
 
;;;==================================================================

(defun zero-card28-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Card28-Vector v))
  (dotimes (i (length v))
    (declare (type Array-Index i))
    (setf (aref v i) 0))
  v)

(defun zero-card28-array (array)
  (declare (type Card28-Array array))
  (zero-card28-vector (array-data array))
  array)

(defun fill-card28-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type Card28-Array array)
	   (type Card28 value))
  (if (= value 0)
      (zero-card28-array array)
    (let ((adv (array-data array)))
      (declare (type Card28-Vector adv))
      (dotimes (i (length adv))
	(declare (type Array-Index i))
	(setf (aref adv i) value))))
  array)

(defun make-card28-array (dims &key (initial-element 0))
  "Make a Simple-Array of Card28 of the desired dimensions,
initializing the elements by default to 0."
  (declare (type List dims)
	   (type Card28 initial-element)
	   (:returns (type Card28-Array)))
  (fill-card28-array (make-array dims :element-type 'Card28) initial-element))

(defun make-card28-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Card28 initial-element)
	   (:returns (type Card28-Vector)))
  (make-card28-array (list len) :initial-element initial-element))

(defun make-card28-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Card28 initial-element)
	   (:returns (type Card28-Matrix)))
  (make-card28-array (list d0 d1) :initial-element initial-element))
 
;;;------------------------------------------------------------

(defun iota-card28-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Card28-Vector v))
  (dotimes (i (length v))
    (declare (type Card28 i))
    (setf (aref v i) i))
  v)

(defun integrate-card28-vector (vec integral)
  "Note that <vec> and <integral> may be the same array."
  (declare (optimize (safety 0) (speed 3))
	   (type Card28-Vector vec integral))
  #+(and :excl :accelerators)  ;; call a C accelerator
  (integrate_long_int_array (length vec) vec integral)
  #-(and :excl :accelerators)
  (let ((pin 0))
    (declare (type Card28 pin))
    (dotimes (i (length vec))
      (declare (type Card28 i))
      (incf pin (aref vec i))
      (setf (aref integral i) pin)))
  integral)

(defun max-card28-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Card28-Vector v))
  (let ((max 0))
    (declare (type Card28 max))
    (dotimes (i (length v))
      (declare (type Card28 i))
      (let ((vi (aref v i)))
	(declare (type Card28 vi))
	(when (> vi max) (setf max vi))))
    max))

(defun card28-vector-2nd-max (v)
  "Find the second largest elt of the card28-pvector."
  (declare (optimize (safety 0) (speed 3))
	   (type Card28-Vector v))
  (let ((1st 0)
	(2nd 0))
    (declare (type Card28 1st 2nd))
    (dotimes (i (length v))
      (declare (type Card28 i))
      (let ((vi (aref v i)))
	(declare (type Card28 vi))
	(when (> vi 1st) (setf 2nd 1st) (setf 1st vi))))
    2nd))

;;;============================================================

(defun zero-card32-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Card32-Vector v))
  (dotimes (i (length v))
    (declare (type Array-Index i))
    (setf (aref v i) 0))
  v)

(defun zero-card32-array (array)
  (declare (type Card32-Array array))
  (zero-card32-vector (array-data array))
  array)

(defun fill-card32-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type Card32-Array array)
	   (type Card32 value))
  (if (= value 0)
      (zero-card32-array array)
    (let ((adv (array-data array)))
      (declare (type Card32-Vector adv))
      (dotimes (i (length adv))
	(declare (type Array-Index i))
	(setf (aref adv i) value))))
  array)

(defun make-card32-array (dims &key (initial-element 0))
  "Make a Simple-Array of Card32 of the desired dimensions,
initializing the elements by default to 0."
  (declare (type List dims)
	   (type Card32 initial-element)
	   (:returns (type Card32-Array)))
  (fill-card32-array (make-array dims :element-type 'Card32) initial-element))

;;;------------------------------------------------------------

(defun zero-int16-vector (adv)
  (declare (optimize (safety 0) (speed 3))
	   (type Int16-Vector adv))
  (dotimes (i (length adv))
    (declare (type Card28 i))
    (setf (aref adv i) 0))
  (values))

(defun zero-int16-array (array)
  (declare (type Int16-Array array))
  (zero-int16-vector (array-data array))
  array)

(defun fill-int16-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type Int16-Array array)
	   (type Int16 value))
  (if (= value 0)
      (zero-int16-array array)
    (let ((adv (array-data array)))
      (declare (type Int16-Vector adv))
      (dotimes (i (length adv))
	(declare (type Array-Index i))
	(setf (aref adv i) value))))
  array)

(defun make-int16-array (dims &key (initial-element 0))
  "Make a Simple-Array of Int16 of the desired dimensions,
initializing the elements by default to 0."
  (declare (type List dims)
	   (type Int16 initial-element)
	   (:returns (type Int16-Array)))
  (fill-int16-array (make-array dims :element-type 'Int16) initial-element))

(defun make-int16-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Int16 initial-element)
	   (:returns (type Int16-Vector)))
  (make-int16-array (list len) :initial-element initial-element))

(defun make-int16-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Int16 initial-element)
	   (:returns (type Int16-Matrix)))
  (make-int16-array (list d0 d1) :initial-element initial-element))

;;;------------------------------------------------------------
;;; Int16 vector algebra
;;;------------------------------------------------------------

(defun linear-mix-int16-vectors (x0 v0 x1 v1
				 &key (result (make-int16-vector (length v0))))
  (declare (type Double-Float x0 x1)
	   (type Int16-Vector v0 v1 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (+ (round (* x0 (aref v0 i)))
			     (round (* x1 (aref v1 i))))))
  result)

(defun add-int16-vectors (v0 v1 &key (result (make-int16-vector (length v0))))
  (declare (type Int16-Vector v0 v1 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (+ (aref v0 i) (aref v1 i))))
  result)

(defun sub-int16-vectors (v0 v1 &key (result (make-int16-vector (length v0))))
  (declare (type Int16-Vector v0 v1 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (- (aref v0 i) (aref v1 i))))
  result)

(defun scale-int16-vector (x0 v0 &key (result (make-int16-vector (length v0))))
  (declare (type Double-Float x0)
	   (type Int16-Vector v0 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (round (* x0 (aref v0 i)))))
  result)

(defun negate-int16-vector (v0 &key (result (make-int16-vector (length v0))))
  (declare (type Int16-Vector v0 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (- (aref v0 i))))
  result)

;;;------------------------------------------------------------

(defun zero-fixnum-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Fixnum-Vector v))
  #+(and :excl :accelerators)  ;; call a C accelerator
  (zero_unsigned_long_array (length v) v)
  #-(and :excl :accelerators)
  (dotimes (i (length v))
    (declare (type Fixnum i))
    (setf (aref v i) 0))
  v)

(defun zero-fixnum-array (array)
  (declare (type Fixnum-Array array))
  (zero-fixnum-vector (array-data array))
  array)

(defun fill-fixnum-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type (Fixnum-Array *) array)
	   (type Fixnum value))
  (if (= value 0)
      (zero-fixnum-array array)
    (let ((adv (array-data array)))
      (declare (type Fixnum-Vector adv))
      (dotimes (i (length adv))
	(declare (type Fixnum i))
	(setf (aref adv i) value))))
  array)

(defun make-fixnum-array (dims &key (initial-element 0))
  (declare (type List dims)
	   (type Fixnum initial-element)
	   (:returns (type (Fixnum-Array *))))
  (fill-fixnum-array (make-array dims :element-type 'Fixnum) initial-element))

(defun make-fixnum-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Fixnum initial-element)
	   (:returns (type Fixnum-Vector)))
  (make-fixnum-array (list len) :initial-element initial-element))

(defun make-fixnum-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Fixnum initial-element)
	   (:returns (type Fixnum-Matrix)))
  (make-fixnum-array (list d0 d1) :initial-element initial-element))
 
;;;------------------------------------------------------------

(declaim (inline zero-array-index-array))
(defun zero-array-index-array (array)
  (declare (optimize (safety 0) (speed 3))
	   (type (Array-Index-Array *) array))
  (let ((adv (array-data array)))
    (declare (type Array-Index-Vector adv))
    #+(and :excl :accelerators)  ;; call a C accelerator
    (zero_unsigned_long_array (length adv) adv)
    #-(and :excl :accelerators)
    (dotimes (i (length adv))
      (declare (type Array-Index i))
      (setf (aref adv i) 0)))
  array)

(defun fill-array-index-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type (Array-Index-Array *) array)
	   (type Array-Index value))
  (if (= value 0)
      (zero-array-index-array array)
    (let ((adv (array-data array)))
      (declare (type Array-Index-Vector adv))
      (dotimes (i (length adv))
	(declare (type Array-Index i))
	(setf (aref adv i) value))))
  array)

(defun make-array-index-array (dims &key (initial-element 0))
  (declare (type List dims)
	   (type Array-Index initial-element)
	   (:returns (type (Array-Index-Array *))))
  (fill-array-index-array (make-array dims :element-type 'Array-Index)
			  initial-element))

(defun make-array-index-vector (len &key (initial-element 0))
  (declare (type Array-Index len)
	   (type Array-Index initial-element)
	   (:returns (type Array-Index-Vector)))
  (make-array-index-array (list len) :initial-element initial-element))

(defun make-array-index-matrix (d0 d1 &key (initial-element 0))
  (declare (type Array-Index d0 d1)
	   (type Array-Index initial-element)
	   (:returns (type Array-Index-Matrix)))
  (make-array-index-array (list d0 d1) :initial-element initial-element))
 
;;;=======================================================================

(declaim (inline zero-float-vector))
(defun zero-float-vector (v)
  (declare (optimize (safety 0) (speed 3))
	   (type Float-Vector v))
  (dotimes (i (length v))
    (declare (type Array-Index i))
    (setf (aref v i) 0.0d0))
  v)

(defun zero-float-array (array)
  (declare (type Float-Array array))
  (zero-float-vector (array-data array)))

(defun fill-float-array (array value)
  (declare (optimize (safety 0) (speed 3))
	   (type Float-Array array)
	   (type Double-Float value))
  (if (= value 0.0d0)
      (zero-float-array array)
    (let ((adv (array-data array)))
      (declare (type Float-Vector adv))
      (dotimes (i (length adv))
	(declare (type Array-Index i))
	(setf (aref adv i) value))))
  array)

(defun make-float-array (dims &key (initial-element 0.0d0))
  "Make a Simple-Array of Double-Float of the desired dimensions,
initializing the elements by default to 0."
  (declare (type List dims)
	   (type Double-Float initial-element)
	   (:returns (type Float-Array)))
  (make-array dims
	      :element-type 'Double-Float
	      :initial-element initial-element))

(defun make-float-vector (length &key (initial-element 0.0d0))
  "Make a 1d Simple-Array of Double-Float of the desired length,
initializing the elements by default to 0."
  (make-float-array (list length) :initial-element initial-element))

(defun make-float-matrix (d0 d1 &key (initial-element 0.0d0))
  (declare (type Integer d0 d1)
	   (type Double-Float initial-element)
	   (:returns (type Float-Matrix)))
  (make-float-array (list d0 d1) :initial-element initial-element))

(defun float-vector (&rest initial-contents)
  "Like <vector> or <list>."
  (declare (type List initial-contents)
	   #-:cmu (dynamic-extent initial-contents)
	   (:returns (type Float-Vector)))
  (make-array (list (length initial-contents))
	      :element-type 'Double-Float
	      :initial-contents (mapcar #'(lambda (x) (coerce x 'Double-Float))
					initial-contents)))
 
;;;------------------------------------------------------------

(defparameter *float-vector-resource* ())

;;;-------------------------------------------------------

(defun borrow-float-vector (len)
  (declare (special *vector-resource*))
  (flet ((long-enough? (w)
	   (declare (type Float-Vector w))
	   (= (length w) len)))
    (let ((v (find-if #'long-enough? *float-vector-resource*)))
      (cond
       ((null v) (make-float-vector len))
       (t (setf *float-vector-resource* (delete v *float-vector-resource*))
	  v)))))

;;;-------------------------------------------------------

(defun return-float-vector (v)
  (declare (special *float-vector-resource*))
  (assert (typep v 'Float-Vector))
  (push v *float-vector-resource*))

;;;=======================================================
;;;
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed element allocated in
;;; case of abnormal exit from the <body>.

(defmacro with-borrowed-float-vector ((vname min-length) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-float-vector ,min-length))
	    (,vname ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-float-vector ,return-name)))))

;;; a convenience:

(defmacro with-borrowed-float-vectors ((vnames min-length) &body body)
  (let ((form `(progn ,@body))
	(vlen (if (atom min-length) min-length (gensym))))
    (dolist (vname vnames)
      (setf form `(with-borrowed-float-vector (,vname ,vlen) ,form)))
    (unless (atom min-length)
      (setf form `(let ((,vlen ,min-length)) ,form)))
    form))

;;;------------------------------------------------------------

(defun copy-float-array (a0 a1)
  (declare (type Float-Array a0 a1))
  (let ((d0 (array-data a0))
	(d1 (array-data a1)))
    (declare (type Float-Vector d0 d1))
    (dotimes (i (length d0))
      (declare (type Array-Index i))
      (setf (aref d1 i) (aref d0 i))))
  a1)

;;;------------------------------------------------------------

(defun transpose-float-matrix! (a0 a1)
  (declare (optimize (safety 0) (speed 3))
	   (type Float-Matrix a0 a1))
  (let ((m (array-dimension a0 0))
	(n (array-dimension a0 1)))
    (declare (type Array-Index m n))
    (assert (and (= m (array-dimension a1 1))
		 (= n (array-dimension a1 0))))
    (dotimes (i m)
      (declare (type Array-Index i))
      (dotimes (j n)
	(declare (type Array-Index j))
	(setf (aref a1 j i) (aref a0 i j)))))
  a1)

(defun transpose-float-matrix (a0 &key (result (make-float-matrix 
						(array-dimension a0 1)
						(array-dimension a0 0))))
  (declare (type Float-Matrix a0 result)
	   (inline transpose-float-matrix!)
	   (:returns result))
  (transpose-float-matrix! a0 result))


(defun make-identity-float-matrix (dim)
  
  (let ((a (az:make-float-matrix dim dim :initial-element 0.0d0)))
    (dotimes (i dim) (setf (aref a i i) 1.0d0))
    a))

;;;=======================================================

(defun fill-random-float-vector (v
				 &key
				 (min -1.0d0)
				 (max 1.0d0))
  (az:type-check Double-Float min max)
  (let ((ran (- max min)))
    (dotimes (i (length v)) (setf (aref v i) (+ min (random ran)))))
  v)

(defun random-float-vector (dim
			    &key
			    (min -1.0d0)
			    (max 1.0d0))
  (fill-random-float-vector (az:make-float-vector dim) :min min :max max))

(defun fill-random-float-matrix (a
				 &key
				 (min -1.0d0)
				 (max 1.0d0)
				 (start-row 0) (end-row (array-dimension a 0))
				 (start-col 0) (end-col (array-dimension a 1)))
  (az:type-check Double-Float min max)
  (assert (<= 0 start-row end-row (array-dimension a 0)))
  (assert (<= 0 start-col end-col (array-dimension a 1)))
  (let ((ran (- max min)))
    (az:for (i start-row end-row)
	    (az:for (j start-col end-col)
		    (setf (aref a i j) (+ min (random ran))))))
  a)

(defun random-float-matrix (nrows ncols
			    &key
			    (min -1.0d0)
			    (max 1.0d0))
  (fill-random-float-matrix (az:make-float-matrix nrows ncols)
			    :min min :max max))

(defun fill-random-symmetric-float-matrix (a
					   &key
					   (min -1.0d0)
					   (max 1.0d0))
  (let ((ran (- max min)))
    (dotimes (i (array-dimension a 0))
      (setf (aref a i i) (+ min (random ran)))
      (dotimes (j i)
	(let ((x (+ min (random ran))))
	  (setf (aref a i j) x)
	  (setf (aref a j i) x)))))
  a)

(defun random-symmetric-float-matrix (nrows
				      &key
				      (min -1.0d0)
				      (max 1.0d0))
  (fill-random-symmetric-float-matrix (az:make-float-matrix nrows nrows)
				      :min min :max max))

;;;-------------------------------------------------------

(defun borrow-canonical-basis-vector (len i)
  (let ((v (az:borrow-float-vector len)))
    (dotimes (j len) (setf (aref v j) 0.0d0))
    (setf (aref v i) 1.0d0)
    v))

;;;=======================================================
;;; Array Algebra
;;;=======================================================

(defun linear-mix-float-vectors (x0 v0 x1 v1
				 &key (result (make-float-vector (length v0))))
  (declare (type Double-Float x0 x1)
	   (type Float-Vector v0 v1 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (+ (* x0 (aref v0 i)) (* x1 (aref v1 i)))))
  result)

(defun add-float-vectors (v0 v1 &key (result (make-float-vector (length v0))))
  (declare (type Float-Vector v0 v1 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (+ (aref v0 i) (aref v1 i))))
  result)

(defun sub-float-vectors (v0 v1 &key (result (make-float-vector (length v0))))
  (declare (type Float-Vector v0 v1 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (- (aref v0 i) (aref v1 i))))
  result)

(defun scale-float-vector (x0 v0 &key (result (make-float-vector (length v0))))
  (declare (type Double-Float x0)
	   (type Float-Vector v0 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (* x0 (aref v0 i))))
  result)

(defun negate-float-vector (v0 &key (result (make-float-vector (length v0))))
  (declare (type Float-Vector v0 result))
  (dotimes (i (length v0))
    (declare (type Array-Index i))
    (setf (aref result i) (- (aref v0 i))))
  result)

;;;=======================================================

(declaim (inline flsq))
(defun flsq (x) 
  (declare (optimize (safety 0) (speed 3))
	   (type Double-float x))
  (* x x))

(defun %float-vector-len2 (n v r)

  "Returns the squared norm of the first <n> elts of <v>
in the zeroth element of <r>."

  (declare (optimize (safety 0) (speed 3))
	   (type Array-Index n)
	   (type Float-Vector v)
	   (type (Float-Vector 1) r))

  (setf (aref r 0) 0.0d0)
  (dotimes (i n)
    (declare (type Array-Index i))
    (incf (aref r 0) (the (Double-Float 0.0d0 *) (flsq (aref v i)))))
  (values r))

(defun float-vector-len2 (v)

  "Returns the squared norm of <v>."

  (declare (type Float-Vector v))

  (let ((sum 0.0d0))
    (declare (type (Double-Float 0.0d0 *) sum))
    (dotimes (i (length v))
      (declare (type Array-Index i))
      (incf sum (the (Double-Float 0.0d0 *) (flsq (aref v i)))))
    sum))

;;;------------------------------------------------------------

(defun %float-matrix*vector (m n a v result)

  "Overwrites <result> with the matrix product of <a> and <v>"

  (declare (optimize (safety 0) (speed 3))
	   (type Array-Index m n)
	   (type Float-Vector a v result)
	   (:returns result))
  
  (let ((k 0)
	(sum 0.0d0))
    (declare (type Array-Index k)
	     (type Double-Float sum))
    (dotimes (i m)
      (declare (type Array-Index i))
      (setf sum 0.0d0)
      (dotimes (j n)
	(declare (type Array-Index j))
	(incf sum (* (aref a k) (aref v j)))
	(incf k))
      (setf (aref result i) sum)))
  (values result))

(defun float-matrix*vector (a v
			    &key
			    (result (make-float-vector (array-dimension a 0))))

  "Overwrites <result> with the matrix product of <a> and <v>"

  (declare (type Float-Matrix a)
	   (type Float-Vector v result)
	   (:returns result))

  (assert (not (eq v result)))

  (let ((m (length result))
	(n (length v))) 
    (assert (= n (array-dimension a 1))) 
    (assert (= m (array-dimension a 0)))
    (%float-matrix*vector m n (array-data a) v result)
    result))

;;;------------------------------------------------------------

(defun float-matrix*matrix (a0 a1
			    &key (result
				  (make-float-matrix (array-dimension a0 0)
						     (array-dimension a1 1))))

  "Overwrites <result> with the matrix product of <a0> and <a1>"

  (declare (type Float-Matrix a0 a1 result)
	   (:returns result))

  (assert (not (or (eq a0 result) (eq a1 result))))
  
  (let ((n (array-dimension result 0))
	(p (array-dimension result 1))
	(m (array-dimension a1 0))
	(sum 0.0d0))
    (declare (type Array-Index n p m)
	     (type Double-Float sum))
    (assert (= (array-dimension a0 1) m)) 
    (assert (= (array-dimension a0 0) n))
    (assert (= (array-dimension a1 1) p))
    (dotimes (i n)
      (declare (type Array-Index i))
      (dotimes (j p)
	(declare (type Array-Index j))
	(setf sum 0.0d0)
	(dotimes (k m)
	  (declare (type Array-Index k))
	  (incf sum (* (aref a0 i k) (aref a1 k j))))
	(setf (aref result i j) sum))))
  result)

;;;=======================================================
;;;     T
;;;   a0  * a1

(defun float-matrix-inner-product (a0 a1
				   &key (result (az:make-float-matrix
						 (array-dimension a0 1)
						 (array-dimension a1 1))))

  "Overwrites <result> with the matrix inner product of a0 and a1."

  (declare (type az:Float-Matrix a0 a1 result)
	   (:returns result))

  (assert (= 2 (array-rank a0) (array-rank a1) (array-rank result)))
  (assert (= (array-dimension a0 0) (array-dimension a1 0))) 
  (assert (= (array-dimension a0 1) (array-dimension result 0)))
  (assert (= (array-dimension a1 1) (array-dimension result 1)))
  (assert (not (or (eq a0 result) (eq a1 result))))
   (let ((n (array-dimension result 0))
	(p (array-dimension result 1))
	(m (array-dimension a1 0)))
    (dotimes (i n)
      (dotimes (j p)
	(let ((sum 0.0d0))
	  (setf (aref result i j) 
	    (dotimes (k m sum) (incf sum (* (aref a0 k i) (aref a1 k j)))))))))
  result)

;;;-------------------------------------------------------
;;;          T
;;;   a0 * a1

(defun float-matrix-outer-product (a0 a1
				   &key
				   (result (az:make-float-matrix
					    (array-dimension a0 0)
					    (array-dimension a1 0))))
  
  "Overwrites <result> with the matrix inner product of a0 and a1."
  
  (declare (type az:Float-Matrix a0 a1 result)
	   (:returns result))
  (assert (= 2 (array-rank a0) (array-rank a1) (array-rank result)))
  (assert (= (array-dimension a0 1) (array-dimension a1 1))) 
  (assert (= (array-dimension a0 0) (array-dimension result 0)))
  (assert (= (array-dimension a1 0) (array-dimension result 1)))
  (assert (not (or (eq a0 result) (eq a1 result))))
  (let ((n (array-dimension result 0))
	(p (array-dimension result 1))
	(m (array-dimension a1 1)))
    (dotimes (i n)
      (dotimes (j p)
	(let ((sum 0.0d0))
	  (setf (aref result i j) 
	    (dotimes (k m sum) (incf sum (* (aref a0 i k) (aref a1 j k)))))))))
  result)

;;;=======================================================

(defun matrix*vector (a v
		      &key
		      (result (az:make-float-vector (array-dimension a 0))))
  
  "Overwrites <result> with the matrix multiply of a and v."
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector v result)
	   (:returns result))
  (assert (= 2 (array-rank a)))
  (assert (= 1 (array-rank v) (array-rank result)))
  (assert (= (array-dimension a 1) (length v)))
  (assert (= (array-dimension a 0) (length result)))
  (assert (not (eq v result)))
  (let ((m (length v)))
    (dotimes (i (length result))
      (let ((sum 0.0d0))
	(setf (aref result i) 
	  (dotimes (k m sum) (incf sum (* (aref a i k) (aref v k))))))))
  result)

;;;-------------------------------------------------------
;;;    T
;;;   a * v

(defun matrix^t*vector (a v
			&key
			(result (az:make-float-vector (array-dimension a 0))))
  
  "Overwrites <result> with the matrix multiply of a and v."
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector v result)
	   (:returns result))
  
  (assert (= 2 (array-rank a)))
  (assert (= 1 (array-rank v) (array-rank result)))
  (assert (= (array-dimension a 0) (length v)))
  (assert (= (array-dimension a 1) (length result)))
  (assert (not (eq v result)))

  (let ((m (length v)))
    (dotimes (i (length result))
      (let ((sum 0.0d0))
	(setf (aref result i) 
	  (dotimes (k m sum) (incf sum (* (aref a k i) (aref v k))))))))
  result)

;;;-------------------------------------------------------
;;; Overwrites a1 with a0 * (d) where d is a vector standing
;;; for the diagonal of a 2d array (ith col of a gets multiplied
;;; by di).

(defun matrix*diagonal (a d
			&key (result
			      (az:make-float-array (array-dimensions a))))
  
  (declare (type az:Float-Matrix a result)
	   (type az:Float-Vector d))
  
  (assert (= 2 (array-rank a) (array-rank result)))
  (assert (= 1 (array-rank d)))
  (assert (= (array-dimension a 1) (length d)))

  (if (eq a result)
    (matrix*diagonal! a d)
    ;; else
    (let ((m (array-dimension a 0)))
      (dotimes (i (length d))
	(dotimes (j m)
	  (setf (aref result j i) (* (aref a j i) (aref d i)))))))
  result)

;;;-------------------------------------------------------
;;; Overwrites a1 with (d) * a where d is a vector standing
;;; for the diagonal of a 2d array (ith row of a gets multiplied
;;; by di).

(defun diagonal*matrix (d a
			&optional
			(result (az:make-float-array (array-dimensions a))))
    
  (declare (type az:Float-Vector d)
	   (type az:Float-Matrix a result))

  (assert (= 1 (array-rank d)))
  (assert (= 2 (array-rank a) (array-rank result)))
  (assert (= (length d) (array-dimension a 0)))

  (if (eq a result)
    (diagonal*matrix! d a)
    ;; else
    (let ((n (array-dimension a 1)))
      (dotimes (i (length d))
	(dotimes (j n)
	  (setf (aref result i j) (* (aref a i j) (aref d i)))))))
  a1)

;;;-------------------------------------------------------
;;; Overwrites a with a * (d) where d is a vector standing
;;; for the diagonal of a 2d array (ith col of a gets multiplied
;;; by di).

(defun matrix*diagonal! (a d)
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector d))

  (assert (= 2 (array-rank a)))
  (assert (= 1 (array-rank d)))
  (assert (= (array-dimension a 1) (length d)))

  (let ((m (array-dimension a 0)))
    (dotimes (i (length d)) (dotimes (j m) (mulf (aref a j i) (aref d i)))))
  a)

;;;-------------------------------------------------------
;;; Overwrites a with (d) * a where d is a vector standing
;;; for the diagonal of a 2d array (ith row of a gets multiplied
;;; by di).

(defun diagonal*matrix! (d a)
  
  (declare (type az:Float-Vector d)
	   (type az:Float-Matrix a))

  (assert (= 1 (array-rank d)))
  (assert (= 2 (array-rank a)))
  (assert (= (length d) (array-dimension a 0)))

  (let ((n (array-dimension a 1)))
    (dotimes (i (length d)) (dotimes (j n) (mulf (aref a i j) (aref d i)))))
  a)

