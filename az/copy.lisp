;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: :Az; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Az)

;;;=======================================================
;;; A general purpose protocol for copying objects
;;;=======================================================

(defgeneric copy (original &key result)
  
  #-sbcl (declare (type T original result)
		  (:returns result))

  (:documentation
   "Copying is something that many think should be provided
automatically by an object system.  Copying is harder than it first
appears, because it's impossible to define, in general, where the
copying should stop.  There are two extreme alternatives, (1) to
merely copy pointers at the top level and (2) to follow all pointers,
copying recursively.  The second isn't practical; it will almost
always result in copying the entire image, if you are really serious
about following all pointers, of all kinds.  Unfortunately, the first
doesn't usually capture what you want.

<copy> is a base generic function that users can specialize to control
the copying Lisp objects, including, but not restricted to, instances
of CLOS classes.  However, we expect that most cases will be dealt
with by specializing one of the functions below, that are called by
the default method for <copy>, rather than <copy> itself.

A method for <copy> should produce an object that is similar to
<original>, in whatever sense of ``similar'' is appropriate for
<original> (and <result>).

The keyword <result> argument allows the user to pass in a
preallocated object to hold the copy.  In this case, a method for
<copy> changes the state of <result> to make it similar to <original>.
This is done in the default method by calling <copy-to!> (see below).

If no <result> is supplied, a method for <copy> creates a new object.
The default method does this by calling <new-copy> (see below).

Note that it is not assumed that <original> and <result> are the same
type.  When <result> is not the same type as <original>, <copy> is
interpreted as coercion.

A method for <copy> is expected to signal an error if given an
<original> and <result> that it doesn't know how to handle.  This is
done in the default method by calling <verify-copy-result> (see
below)."))

(defmethod copy ((original T) &key result)

  "If <result> is supplied (and <result> is not eq to <original>),
then the default method for <copy> does a destructive <copy-to!> of
<original> to <result>.  <result> must be an object that is capable
of holding a copy of <original>, but it need not be exactly the same
type as <original>. For example, one might write a method for
copying Lists to Vectors.

If <result> is not supplied, the default method calls <new-copy>."

  (declare (type T original result)
	   (:returns result))
  (if result
      (if (eq original result)
	  result
	;; else
	(copy-to! original result))
    ;; else
    (new-copy original))) 
  
;;;-------------------------------------------------------

(defgeneric new-copy (original)
  #-sbcl (declare (type T original)
		  (:returns (type (type-of original))))
  (:documentation
   "<new-copy> returns a new object that is a copy of <original>."))

(defmethod new-copy ((original Standard-Object))
  "The default method for Standard-Object does a shallow copy,
ie., copies the slot pointers from <original> to the new object."
  (declare (type Standard-Object original)
	   (:returns (type (type-of original) new-instance)))
  (let ((init-list ())
	(slots&vals (all-slots original)))
    (loop (when (null slots&vals) (return t))
	  (let ((key (intern (string (pop slots&vals)) :keyword))
		;; have to get the keyword that corresponds
		;; to the slot name.
		(val (pop slots&vals)))
	    (push val init-list)
	    (push key init-list)))
    (apply #'make-instance (class-of original) init-list)))

;;;-------------------------------------------------------

(defgeneric copy-to! (original result)
  #-sbcl (declare (type T original result)
		  (:returns result))
  (:documentation
   "<copy-to!> overwrites <result> with the state of <original>.
Methods should signal an error if <result> is not able to hold
a copy of <original>, for what ever reason."))

(defmethod copy-to! ((original Standard-Object)
		     (result Standard-Object))

  "The default method for Standard-Objects calls the generic function
<verify-copy-result?> to ensure that <result> is an object able to
hold a copy of <original> and <copy-slots-to!> to do a shallow copy."

  (declare (type Standard-Object original result)
	   (:returns result))
  (unless (eq original result)
    (assert (verify-copy-result? original result))
    (copy-slots-to! original result))
  result)
  
;;;-------------------------------------------------------

(defgeneric verify-copy-result? (original result)
  #-sbcl (declare (type T original result)
		  (:returns (type az:Boolean)))
  (:documentation

   "<verify-copy-result?> checks to see that <result> is an
object able to hold a copy of <original>.  In general, <result> need
not be of the same class (or type) as <original>."))

(defmethod verify-copy-result? ((original Standard-Object)
				(result Standard-Object))
  "The default method for Standard-Objects only allows copying
 to instances of the same class."
  (declare (type Standard-Object original result)
	   (:returns (type az:Boolean)))
  (eq (class-of original) (class-of result)))

;;;-------------------------------------------------------

(defgeneric copy-slots-to! (original result)
  #-sbcl (declare (type Standard-Object original result)
		  (:returns result))
  (:documentation
   "Do a shallow copy of <original> to <result>, assuming
that <result> has at least the slots in <original>.  (It may have
other slots as well.)"))

(defmethod copy-slots-to! ((original Standard-Object)
			   (result Standard-Object))

  "Do a shallow copy of <original> to <result>, assuming
that <result> has at least the slots in <original>.  (It may have
other slots as well.)"
  (declare (type Standard-Object original result)
	   (:returns result))
  (let ((init-list ()) ;; &rest result are dangerous!
	(slots&vals (all-slots original)))
    (loop
      (when (null slots&vals) (return t))
      (let ((key (intern (string (pop slots&vals)) :keyword))
	    ;; have to get the keyword that corresponds
	    ;; to the slot name.
	    (val (pop slots&vals)))
	(push val init-list)
	(push key init-list)))
    (apply #'reinitialize-instance result init-list))
  result) 
    


