;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Az -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved.
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;=======================================================

(in-package :Az)

;;;=======================================================

(defun day-of-week-name (day-number)
  "Translate the day number to a 3 char abbreviation."
  (declare (type (Integer 0 6) day-number)
	   (:returns (type (String 3))))
  (ecase day-number 
    (0 "Mon") (1 "Tue") (2 "Wed") (3 "Thu") (4 "Fri") (5 "Sat") (6 "Sun")))

(defun month-name (month-number)
  "Translate the month number to a 3 char abbreviation."
  (declare (type (Integer 0 11) month-number)
	   (:returns (type (String 3))))
  (ecase month-number 
    (0 "Jan") (1 "Feb") (2 "Mar") (3 "Apr") (4 "May") (5 "Jun")
    (6 "Jul") (7 "Aug") (8 "Sep") (9 "Oct") (10 "Nov") (11 "Dec")))

(defun print-date (&optional (stream *standard-output*))
  "Print the current time and date in a human readable format."
  (declare (type (or Stream az:Boolean) stream)
	   (:returns (type String)))
  (multiple-value-bind
    (second minute hour date month year day-of-week 
     daylight-savings-p time-zone)
      (get-decoded-time)
    (declare (ignore day-of-week daylight-savings-p time-zone))
    (format stream "~%~a-~a-~a ~a:~a:~a~%"
	    year month date hour minute second)))

(defun print-system-description (&optional (stream *standard-output*))

    "Print a comprehensive description of the current hardware and
software environment."

  (declare (type (or Stream az:Boolean) stream)
	   (:returns (type String)))
  (print-date stream)
  (format stream "~%Site: ~a." (long-site-name))
  (format stream "~%Machine: ~a ~a ~%~a"
	  (machine-type) (machine-instance) (machine-version))
  (format stream "~%Lisp: ~a ~%~a."
	  (lisp-implementation-type) (lisp-implementation-version))
  (format stream "~%Software: ~a ~%~a." (software-type) (software-version))
  (let ((*print-length* nil))
    #-sbcl (declare (special *print-length*)) ; $$$ causes package lock err
    (format stream "~%Features: ~a.~%" *features*)))

;;;==================================================================

(defun first-elt (seq)
  "A <first> that works on Sequences."
  (declare (type Sequence seq)
	   (:returns (type T)))
  (etypecase seq
    (List (first seq))
    (Vector (aref seq 0))))

(defun last-elt (seq)

  "Get the last element in a Sequence (not the last Cons in a List
like <last>)."  

  (declare (type Sequence seq)
	   (:returns (type T)))
  (etypecase seq
    (List (first (last seq)))
    (Vector (aref seq (- (length seq) 1)))))
    
;;;==================================================================



