;;;-*- Package: :Arizona; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Arizona) 

;;;============================================================

(deftype File-Descriptor () 'Positive-Fixnum
	 "A unix file descriptor.")

;;;============================================================

#+:cmu
(defun print-unix-error (s)
  (declare (type Simple-String s)
	   (ignore s))
  (warn "~&~s~%" (unix:get-unix-error-msg)))

#+:excl
(ff:defforeign 'print-unix-error
    :entry-point (ff:convert-to-lang "perror")
    :language :C
    :arguments '(Simple-String)
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :void
    )

(defun warn-with-unix-error-msg (format-string &rest format-args)
  (print-unix-error "")
  (apply #'warn format-string format-args))

;;;============================================================

(defun unix-environment-var (stringable)
  (declare (type (Or String Symbol) stringable))
  (let ((s (string stringable)))
    (declare (type String s))
    #+:cmu
    (cdr (assoc (intern s :keyword) ext:*environment-list*))
    #+:excl
    (system:getenv s)))

;;;============================================================
;;; network byte order functions

(declaim (inline htonl ntohl htons ntohs))

(defun htonl (x)
  #+:cmu (ext:htonl x)
  #+:excl (ipc::htonl x))

(defun ntohl (x)
  #+:cmu (ext:ntohl x)
  #+:excl (ipc::ntohl x))

(defun htons (x)
  #+:cmu (ext:htons x)
  #+:excl (ipc::htons x))

(defun ntohs (x)
  #+:cmu (ext:ntohs x)
  #+:excl (ipc::ntohs x))

;;;============================================================

(declaim (inline %unix-close))

#+:cmu
(defun %unix-close (fd) (if (unix:unix-close fd) 0 -1))

#+:excl
(ff:defforeign '%unix-close
    :entry-point (ff:convert-to-lang "close")
    :language :C
    :arguments '(Fixnum) ;; file descriptor int
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum) 
 
(defun unix-close (fd)
  (declare (type File-Descriptor fd))
  (when (minusp (unix-close fd))
    (print-unix-error "")
    (error "~&problem closing fd ~s~%" fd)))

;;;------------------------------------------------------------

(declaim (inline %unix-open))

#+:cmu
(defun %unix-open (path) (if (unix:unix-open path unix:o_create ) 0 -1))

#+:excl
(ff:defforeign '%unix-open
    :entry-point (ff:convert-to-lang "open")
    :language :C
    :arguments '(Simple-String) ;; pathname
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum) 
 
(defun unix-open (pathstring)
  (declare-check (type String pathstring))
  (when (minusp (unix-open pathstring))
    (print-unix-error "")
    (error "~&problem closing fd ~s~%" fd)))

;;;------------------------------------------------------------

(defmacro with-unix-open-file ((fd pathstring) &body body)
  (declare-check (type Symbol fd))
  `(let ((,fd (unix-open ,pathstring)))))
 
;;;------------------------------------------------------------

#+:cmu
(defun %unix-read (fd buffer nbytes)
  (declare (type File-Descriptor fd)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) buffer)
	   (type Positive-Fixnum nbytes))
  (unix:unix-read fd (sys:vector-sap buffer) nbytes))

#+:excl
(ff:defforeign '%unix-read
    :entry-point (ff:convert-to-lang "read")
    :language :C
    :arguments '(Fixnum ;; fd file descriptor int
		 (Simple-Array (Unsigned-Byte 8) (*)) ;; buffer
		 Fixnum ;; nbytes
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; bytes actually read
    )

(defun unix-read (fd buffer nbytes))

;;;------------------------------------------------------------

#+:cmu
(defun %unix-write (fd buffer nbytes)
  (declare (type File-Descriptor fd)
	   (type (Simple-Array (Unsigned-Byte 8) (*)) buffer)
	   (type Positive-Fixnum nbytes))
  (unix:unix-write fd (sys:vector-sap buffer) 0 nbytes))

#+:excl
(ff:defforeign '%unix-write
    :entry-point (ff:convert-to-lang "write")
    :language :C
    :arguments '(Fixnum ;; fd file descriptor int
		 (Simple-Array (Unsigned-Byte 8) (*)) ;; buffer
		 Fixnum ;; nbytes
		 )
    :print t
    :arg-checking t
    :call-direct t
    :callback nil
    :return-type :Fixnum ;; bytes written
    )

(defun unix-write (fd buffer nbytes))

