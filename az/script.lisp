;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :cl-user; -*-
(in-package :cl-user)


       
(load ""
      :verbose t
      :print t
      :update-entry-points t
      :foreign-files (list ;;"../foreign/sgi/tools.o"
			   ;;"../foreign/sgi/accelerators.o"
			   "../foreign/sgi/dummy_calls.o"
			   "../lapack/sgi/lapack.a"
			   "../lapack/sgi/blas.a"
			   ;;"../npsol_interface/sgi/c-npsol.o"
			   ;;"../npsol_interface/sgi/option.o"
			   ;;"../npsol/sgi/dpnpsol.a"
			   "/usr/lib/libF77_G0.a"
			   "/usr/lib/libI77_G0.a"
			   ;;"/usr/lib/libisam_G0.a"
			   ))
