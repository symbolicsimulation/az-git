;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Az; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================
 
(in-package :Az)

;;;=======================================================

(defparameter *instance-pool-table* (make-hash-table))

;;;-------------------------------------------------------

(defgeneric borrow-instance (c &rest options)
  #-sbcl (declare (type (or Symbol Class) c)
		  (type List options)
		  (:returns (type c)))
  (:documentation
   "Borrow an instance of the class <c> from a global resource,
initializing it with the supplied <options>."))

(defmethod borrow-instance ((class Symbol) &rest options)
  "Find the class corresponding to <class> and recurse."
  (declare (type Symbol class)
	   (type List options)
	   (:returns (type class)))
  (apply #'borrow-instance (find-class class) options))

(defmethod borrow-instance ((class Class) &rest options)
  "If an instance of <class> is in the resource, pop it out and
apply <reinitialize-instance> to it and <options>.  If no instance is
in the resource, apply <make-instance> to <class> and <options>."
  (declare (type Class class)
	   (type List options)
	   (:returns (type Class)))
  (let ((instance (pop (gethash class *instance-pool-table* nil))))
    (if (null instance)
	(apply #'make-instance class options)
      (apply #'reinitialize-instance instance options))))

;;;-------------------------------------------------------

(defgeneric return-instance (instance c &optional errorp)
  #-sbcl (declare (type Standard-Object instance)
		  (type (or Symbol Class) c)
		  (type T errorp)
		  (:returns t))
  (:documentation
   "Return the <instance> of class <c> to the resource. If
<errorp> is not nil and <instance> is not an instance of <c>, an error
is signalled."))

(defmethod return-instance ((instance Standard-Object)
			    (class Symbol)
			    &optional (errorp t))
    "Find the class corresponding to <class> and recurse."
  (declare (type Standard-Object instance)
	   (type Symbol class)
	   (type T errorp)
	   (:returns t))
  (return-instance instance (find-class class) errorp))

(defmethod return-instance ((instance Standard-Object)
			    (class Class)
			    &optional (errorp t))

  "Return the <instance> of class <class> to the resource. If <errorp>
is not nil and <instance> is not an instance of <class>, an error is
signalled."

  (declare (type Standard-Object instance)
	   (type Class class)
	   (type T errorp)
	   (:returns t))
  (unless (or (typep instance (class-name class)) (not errorp))
    (error "Trying to return ~a to the instance pool of ~a." instance class))
  (let ((pool (gethash class *instance-pool-table* :not-initialized)))
    (if (eq pool :not-initialized)
	(setf (gethash class *instance-pool-table*) (list instance))
      (push instance (gethash class *instance-pool-table*))))
  t)

;;;-------------------------------------------------------

(defgeneric clear-instance-pool (c)
  #-sbcl (declare (type (or Symbol Class) c)))

(defmethod clear-instance-pool ((c Symbol))
  (clear-instance-pool (find-class c)))

(defmethod clear-instance-pool ((cl Class))
  (setf (gethash cl *instance-pool-table*) ()))

(defun clear-all-instance-pools () (clrhash *instance-pool-table*))

;;;-------------------------------------------------------
;;;
;;; The Symbolics <using-resource> does an <unwind-protect> to ensure
;;; deallocation. I prefer to leave the borrowed instance allocated in
;;; case of abnormal exit from the <body>.

(defmacro with-borrowed-instance ((name class &rest options) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((cl (gensym))
	(return-name (gensym)))
    `(let* ((,cl ,class)
	    (,return-name (borrow-instance ,cl ,@options))
	    (,name ,return-name))
       (multiple-value-prog1 (progn ,@body)
	 (return-instance ,return-name ,cl)))))

(defmacro with-borrowed-instances (specs &body body)
  (if (= (length specs) 1)
      `(with-borrowed-instance ,(first specs)
	 ,@body)
      `(with-borrowed-instance ,(first specs)
	 (with-borrowed-instances ,(rest specs)
	   ,@body))))


