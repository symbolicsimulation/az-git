;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Az -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Az)

;;;=======================================================

(eval-when (compile load eval)
  #-(and :excl :allegro-v4.0) (require :Foreign)
  #+(and :excl :allegro-v4.0) (cltl1:require :Foreign))

#+(and :excl :sgi) ;; assuming lapack and npsol loaded at lisp build time
(time
 (load ""
       :foreign-files
       (list
	#+:accelerators user::*tools*
	#+:accelerators user::*accelerators*)))
 
;;;=======================================================

#+(and :excl :sun4)
(dolist (s '("__prod_b10000" "__carry_out_b10000"
             "__prod_65536_b10000" "__unpack_quadruple"
             "__unpacked_to_decimal"))
  (ff:remove-entry-point s))

#+(and :excl :sun4)
(time
 (load ""
       :foreign-files
       (list
	;; user::*missing*
	#+:accelerators user::*tools*
	#+:accelerators user::*accelerators*
	;; lapack
	#+:lapack user::*dummy_calls*
	#+:lapack user::*lapack*
	#+:lapack user::*blas*
	;; get the right libraries, not trivial on Suns
	;; npsol
	;;#+:npsol user::*c-npsol*
	;;#+:npsol user::*option*
	;;#+:npsol user::*dpnpsol*
	#+(or :lapack :npsol) user::*fortran-lib*
	#+(or :lapack :npsol) user::*fortran-io-lib*
	user::*c-math*)))

;;;=======================================================

#+:cmu
(defvar *foreign-loaded?* nil "Can only load foreign files once in CMUCL.")

#+:cmu
(unless *foreign-loaded?*
    (alien:load-foreign
     (list user::*c-math*
	   #+:accelerators user::*tools*
	   #+:accelerators user::*accelerators*
	   ;; lapack
	   #+:lapack user::*dummy_calls*
	   #+:lapack user::*lapack*
	   #+:lapack user::*blas*
	   ;; get the right libraries, not trivial on Suns
	   ;; npsol
	   #+:npsol user::*c-npsol*
	   #+:npsol user::*option*
	   #+:npsol user::*dpnpsol*
	   user::*missing*
	   #+(or :lapack :npsol) user::*fortran-lib*))
    (setf *foreign-loaded?* t))


