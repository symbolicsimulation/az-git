;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Az -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved.
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;=======================================================

(in-package :Az)

;;;=======================================================
;;; for documenting types of returned values

(declaim (declaration :returns))

;;;=======================================================
;;; Handy Types
;;;=======================================================

#-sbcl (deftype Boolean () '(Member t nil)) ; $$$$ yuck.  should check properly. -jm

(deftype Funcallable () '(Or Symbol Function))

;;;--------------------------------------------------------

(deftype Array-Index ()
  "The subset of Integer that can be used as an array index."
  '(Integer 0 #.array-dimension-limit))

(deftype Positive-Fixnum ()
  "Actually, this means non-negative Fixnum."
  '(Integer 0 #.most-positive-fixnum))

;;;--------------------------------------------------------

(deftype Positive-Real ()
  "Actually, non-negative Real."
  '(Real 0 *))

(deftype Real-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Real ,dims))

(deftype Real-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Real (,len)))

(deftype Real-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Real (,d0 ,d1)))

;;;--------------------------------------------------------

(defun double-float? (x) (typep x 'Double-Float))
(defun float-list? (l) (and (listp l) (every #'double-float? l)))

(deftype Float-List ()
  "A list of Double-Floats."
  '(satisfies float-list?))

(deftype Positive-Float ()
  "A non-negative Double-Float."
  `(Double-Float 0.0d0 *))

(defun positive-float? (x)
  (typep x 'Positive-Float))

(defun positive-float-list? (l)
  (and (listp l) (every #'double-float? l)))

(deftype Positive-Float-List ()
  "A list of Positive-Floats."
  '(satisfies positive-float-list?))

(deftype Float-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Double-Float ,dims))

(deftype Float-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Double-Float (,len)))

(deftype Float-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Double-Float (,d0 ,d1)))

;;;--------------------------------------------------------

(deftype Fixnum-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Fixnum ,dims))

(deftype Fixnum-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Fixnum (,len)))

(deftype Fixnum-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Fixnum (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Array-Index-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Array-Index ,dims))

(deftype Array-Index-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Array-Index (,len)))

(deftype Array-Index-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Array-Index (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Bit-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Bit ,dims))

;; Bit-Vector already defined, and we want Simple-Bit-Vector anyway.

(deftype Bit-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Bit (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Card4  () '(Unsigned-Byte  4))

(deftype Card4-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Card4 ,dims))

(deftype Card4-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Card4 (,len)))

(deftype Card4-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Card4 (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Card8  () '(Unsigned-Byte  8))

(deftype Card8-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Card8 ,dims))

(deftype Card8-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Card8 (,len)))

(deftype Card8-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Card8 (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Card16 () '(Unsigned-Byte 16))

(deftype Card16-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Card16 ,dims))

(deftype Card16-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Card16 (,len)))

(deftype Card16-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Card16 (,d0 ,d1)))

(defun card16? (x) (typep x 'Card16))

(defun card16-list? (l) (and (listp l) (every #'card16? l)))

(deftype Card16-List ()
  "A list of Card16s."
  '(satisfies Card16-list?))

;;;-------------------------------------------------------------

(deftype Card24 () '(Unsigned-Byte 24))

(deftype Card24-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Card24 ,dims))

(deftype Card24-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Card24 (,len)))

(deftype Card24-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Card24 (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Card28 () '(Unsigned-Byte 28))

(deftype Card28-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Card28 ,dims))

(deftype Card28-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Card28 (,len)))

(deftype Card28-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Card28 (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Card32 () '(Unsigned-Byte 32))

(deftype Card32-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Card32 ,dims))

(deftype Card32-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Card32 (,len)))

(deftype Card32-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Card32 (,d0 ,d1)))

;;;-------------------------------------------------------------

(deftype Int16 () '(Signed-Byte 16))

(deftype Int16-Array (&optional dims)
  (when (null dims) (setf dims '*))
  `(Simple-Array Int16 ,dims))

(deftype Int16-Vector (&optional len)
  (when (null len) (setf len '*))
  `(Simple-Array Int16 (,len)))

(deftype Int16-Matrix (&optional d0 d1)
  (when (null d0) (setf d0 '*))
  (when (null d1) (setf d1 '*))
  `(Simple-Array Int16 (,d0 ,d1)))

(defun int16? (x) (typep x 'Int16))
(defun int16-list? (l) (and (listp l) (every #'int16? l)))

(deftype Int16-List     () "A list of Int16s."     '(satisfies int16-list?))

(deftype Int16-Sequence ()
  "A sequence of Int16s." 
  `(or Int16-Vector Int16-List))


