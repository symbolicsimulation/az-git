;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package :Az;  -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Az)

;;;=======================================================
;;; PCL/CLOS consistency
;;;=======================================================

;; to get around a mysterious pcl-structure bug
#+(and :cmu (not :cmu17) :pcl) 
(eval-when (compile load eval)
  (print
   (clos::find-structure-class 'cl::string-output-stream)))


#+:pcl
(eval-when (compile load eval)
  ;; have compiled classes and methods added to the environment
  ;; at compile time:
  (pushnew 'compile clos::*defclass-times*)
  (pushnew 'compile clos::*defgeneric-times*)
  (pushnew 'compile clos::*defmethod-times*)
  )

#+(and :pcl (not :cmu))
(defun clos:slot-definition-name (slotd) (clos::slotd-name slotd))
#+(and :pcl (not :cmu))
(defun clos:slot-definition-type (slotd) (clos::slotd-type slotd))

;;; redefine defgeneric to handle :method options
#+:pcl
(defmacro clos:defgeneric (function-specifier lambda-list &body options)
  (flet ((method-option? (list) (eq (first list) :method))
         (make-defmethod (method-option)
           `(clos:defmethod ,function-specifier ,@(rest method-option))))
    (let ((method-options (remove-if-not #'method-option? options))
          (options (remove-if #'method-option? options)))
      `(prog1
	   ,(clos::expand-defgeneric function-specifier lambda-list options)
	 ,@(mapcar #'make-defmethod method-options)))))


;;;=======================================================
;;; CLOS utilities
;;;=======================================================

(defun empty-slot? (object slot-name)
  "Is specified slot unbound or is its value nil?"
  (declare (type Standard-Object object)
	   (type Symbol slot-name)
	   (:returns (type Boolean)))
  (or (not (slot-boundp object slot-name))
      (null (slot-value object slot-name))))

(defun slots-boundp (object slots)
  "Are all the <slots> bound?"
  (declare (type Standard-Object object)
	   (type List slots)
	   (:returns (type Boolean)))
  (every #'(lambda (slot) (slot-boundp object slot)) slots))

;;;-------------------------------------------------------

(defgeneric all-slots (self)
  #-sbcl (declare (type Standard-Object self)
		  (:returns (type List)))
  (:documentation
   "Returns a list of slot-name --- slot-value pairs 
for each bound slot."))

(defmethod all-slots ((self Standard-Object))
  "Returns a list of slot-name --- slot-value pairs for each bound slot."
  (declare (type Standard-Object self)
	   (:returns (type List)))
  (mapcan
   #'(lambda (slot-definition)
       (let ((name (slot-definition-name slot-definition)))
	 (when (slot-boundp self name)
	   (list name (slot-value self name)))))
   (class-slots (class-of self))))

;;;-------------------------------------------------------

(defgeneric all-slot-values (self)
  #-sbcl (declare (type Standard-Object self)
		  (:returns (type List)))
  (:documentation
   "Returns a list of the slot-values for each bound slot."))

(defmethod all-slot-values ((self Standard-Object))
  (declare (type Standard-Object self)
	   (:returns (type List)))
  "Returns a list slot-values for each bound slot."
  (mapcar
   #'(lambda (slot-definition)
       (let ((name (slot-definition-name slot-definition)))
	 (when (slot-boundp self name)
	   (slot-value self name))))
   (class-slots (class-of self))))

;;;-------------------------------------------------------
;;; Just the values of the slots that are "interesting",
;;; whatever that means.

(defgeneric interesting-slot-values (self)
  #-sbcl (declare (type Standard-Object self)
		  (:returns (type List)))
  (:documentation
   "Returns a list slot-values for each ``interesting'' slot,
where ``interesting'' is defined by the method used."))

;;; 

(defmethod interesting-slot-values ((self Standard-Object))
  "By default, ``interesting'' means those slots that contain other objects."
  (declare (type Standard-Object self)
	   (:returns (type List)))
  (labels ((standard-object? (x) 
	     (typep x 'Standard-Object))
	   (interesting-object? (x) 
	     (or (standard-object? x)
		 (and (not (null x))
		      (typep x 'Sequence)
		      (every #'standard-object? x)))))
    (delete-if-not #'interesting-object? (all-slot-values self))))

;;;=======================================================

(defgeneric all-slot-names (class)
  #-sbcl (declare (type (or Symbol Class) class)
		  (:returns (type List)))
  (:documentation
   "Returns a list of the names of all the slots."))

(defmethod all-slot-names ((class Symbol))
  "A method for class names."

  (declare (type Symbol class)
	   (:returns (type List)))
  (all-slot-names (find-class class)))

(defmethod all-slot-names ((class Class))
  (declare (type Class class)
	   (:returns (type List)))
  (with-collection
      (dolist (slot-definition (class-slots class))
	(collect (slot-definition-name slot-definition)))))
	
;;;------------------------------------------------------------------------

(defgeneric slot-names-of-type (class a-type)
  #-sbcl (declare (type Class class)
		  (type Symbol a-type)
		  (:returns (type List)))
  (:documentation
   "Returns a list of the names of the slots whose declared type
equals <a-type>."))

(defmethod slot-names-of-type ((class Class) a-type)
  "Returns a list of the names of the slots whose declared type
equals <a-type>."
  (declare (type Class class)
	   (type Symbol a-type)
	   (:returns (type List)))
  (with-collection
      (dolist (slot-definition (class-slots Class))
	(when (eql (slot-definition-type slot-definition) a-type)
	  (collect (slot-definition-name slot-definition))))))
	
;;;------------------------------------------------------------------------

(defgeneric slot-names-of-subtypes (class a-type)
  #-sbcl (declare (type Class class)
		  (type Symbol a-type)
		  (:returns (type List)))
  (:documentation
   "Returns a list of the names of the slots whose declared type
is a subtype of <a-type>."))

(defmethod slot-names-of-subtypes ((class Class) a-type)
  "Returns a list of the names of the slots whose declared type
is a subtypep <a-type>."
  (declare (type Class class)
	   (type Symbol a-type)
	   (:returns (type List)))
  (with-collection
      (dolist (slot-definition (class-slots class))
	(when (subtypep (slot-definition-type slot-definition) a-type)
	  (collect (slot-definition-name slot-definition))))))
	
;;;------------------------------------------------------------------------

(defgeneric numerical-slot-names (class)
  #-sbcl (declare (type Class class)))

(defmethod numerical-slot-names ((class Class))
  (with-collection
      (dolist (slot-definition (class-slots Class))
	(when (subtypep (slot-definition-type slot-definition) 'Number)
	  (collect (slot-definition-name slot-definition))))))

;;;------------------------------------------------------------------------

(defgeneric non-numerical-slot-names (class)
  #-sbcl (declare (type Class class)))

(defmethod non-numerical-slot-names ((class Class))
  (with-collection
      (dolist (slot-definition (class-slots Class))
	(unless (subtypep (slot-definition-type slot-definition) 'Number)
	  (collect (slot-definition-name slot-definition))))))

;;;=======================================================

(defgeneric all-subclasses (class)
  #-sbcl (declare (type (or Symbol Class) class)
		  (:returns (type List)))
  (:documentation
   "Returns a list of all the subclasses of <c>."))

(defmethod all-subclasses ((class-name Symbol))
  "Find the Class with name <class-name> and recurse."
  (declare (type Symbol class-name)
	   (:returns (type List)))
  (all-subclasses (find-class class-name)))

(defun all-subclasses1 (class)
  (let ((direct-subclasses (reverse (clos::class-direct-subclasses class)))) ; $$$$ added package name -jm
    (unless (null direct-subclasses)
      (mapcan #'(lambda (cl)
		  (cons cl (mapcan #'all-subclasses1 direct-subclasses)))
	      direct-subclasses))))

(defmethod all-subclasses ((class Class))
  "Returns a list of all the subclasses of <class>."
  (declare (type Class class)
	   (:returns (type List)))
  (delete-duplicates (all-subclasses1 class)))

(defmethod all-subclasses ((class Standard-Class))
  "Returns a list of all the subclasses of <class>."
  (declare (type Standard-Class class)
	   (:returns (type List)))
  (delete-duplicates (all-subclasses1 class)))
    
;;;=======================================================

(defgeneric all-superclasses (class)
  #-sbcl (declare (type (or Symbol Class) class)
		  (:returns (type List)))
  (:documentation
   "Returns a list of all the superclasses of <c>."))

(defmethod all-superclasses ((class-name Symbol))
  "Find the class with name <class-name> and recurse."
  (declare (type Symbol class-name)
	   (:returns (type List)))
  (all-superclasses (find-class class-name)))

(defmethod all-superclasses ((class Class))
  "Returns a list of all the superclasses of <c>."
  (declare (type Class class)
	   (:returns (type List)))
  (let ((direct-superclasses (reverse (clos:class-direct-superclasses class))))
    (unless (null direct-superclasses)
      (remove-duplicates
       (mapcan #'(lambda (cl)
		   (cons cl (mapcan #'all-superclasses direct-superclasses)))
	       direct-superclasses)))))

;;;-------------------------------------------------------

(defgeneric subclass-tree (class)
  #-sbcl (declare (type (or Symbol Class) class)))

(defmethod subclass-tree ((class-name Symbol))
  (subclass-tree (find-class class-name)))

(defmethod subclass-tree ((class Class))
  
  (let ((class-direct-subclasses
	 (reverse (class-direct-subclasses class))))
    (if (null class-direct-subclasses)
	class
      (cons class (list (mapcar #'subclass-tree class-direct-subclasses))))))

;;;-------------------------------------------------------

(defgeneric all-classes-between (c0 c1)
  #-sbcl (declare (type (or Symbol Class) c0 c1)))

(defmethod all-classes-between ((c0 Symbol)
				(c1 Symbol))
  (all-classes-between (find-class c0) (find-class c1)))

(defmethod all-classes-between ((class0 Class)
				(c1 Symbol))
  (all-classes-between class0 (find-class c1)))

(defmethod all-classes-between ((c0 Symbol)
				(class1 Class))
  (all-classes-between (find-class c0) class1))

(defmethod all-classes-between ((class0 Class)
				(class1 Class))
  `(,class0
    ,@(delete-duplicates
       (let ((c0 (class-name class0))
	     (c1 (class-name class1)))
	 (cond ((subtypep c0 c1)
	       
		(intersection (all-superclasses class0)
			      (all-subclasses class1)))
	       ((subtypep c1 c0)
		(intersection (all-subclasses class0)
			      (all-superclasses class1)))
	       (t
		()))))
    ,class1))

;;;-------------------------------------------------------

(defgeneric nth-generation-superclasses (class n)
  #-sbcl (declare (type (or Symbol Class) class)
		  (type Integer n)))

(defmethod nth-generation-superclasses ((class-name Symbol) (n Integer))
  (nth-generation-superclasses (find-class class-name) n))

(defmethod nth-generation-superclasses ((class Class) (n Integer))
  (if (= 1 n)
      (class-direct-superclasses class)
    ;; else
    (delete-duplicates
     (mapcan #'(lambda (cl)
		 (nth-generation-superclasses cl (- n 1)))
	     (class-direct-superclasses class)))))

;;;-------------------------------------------------------

(defgeneric nth-generation-subclasses (class n)
  #-sbcl (declare (type (or Symbol Class) class)
		  (type Integer n)))

(defmethod nth-generation-subclasses ((class-name Symbol) (n Integer))
  (nth-generation-subclasses (find-class class-name) n))
  
(defmethod nth-generation-subclasses ((class Class) (n Integer))
  (if (= 1 n)
      (class-direct-subclasses class)
    ;; else
    (delete-duplicates
     (mapcan #'(lambda (cl)
		 (nth-generation-subclasses cl (- n 1)))
	     (class-direct-subclasses class)))))

;;;-------------------------------------------------------

(defgeneric n-generation-superclasses (class n)
  #-sbcl (declare (type (or Symbol Class) class)
		  (type Integer n)))

(defmethod n-generation-superclasses ((class-name Symbol) (n Integer))
  (n-generation-superclasses (find-class class-name) n))

(defmethod n-generation-superclasses ((class Class) (n Integer))
  (let ((direct-supers (class-direct-superclasses class)))
    (if (= 1 n)
	direct-supers
      ;; else
      (delete-duplicates
       (mapcan #'(lambda (cl)
		   (cons cl (n-generation-superclasses cl (- n 1))))
	       direct-supers)))))

;;;-------------------------------------------------------

(defgeneric n-generation-subclasses (class n)
  #-sbcl (declare (type (or Symbol Class) class)
		  (type Integer n)))

(defmethod n-generation-subclasses ((class-name Symbol) (n Integer))
  (n-generation-subclasses (find-class class-name) n))
  
(defmethod n-generation-subclasses ((class Class) (n Integer))
  (let ((direct-subs (class-direct-subclasses class)))
    (if (= 1 n)
	direct-subs
      ;; else
      (delete-duplicates
       (mapcan #'(lambda (cl)
		   (cons cl (n-generation-subclasses cl (- n 1))))
	       direct-subs)))))

;;;-------------------------------------------------------

(defgeneric class-relatives (class n)
  #-sbcl (declare (type (or Symbol Class) class)
		  (type Integer n)))

(defmethod class-relatives ((class-name Symbol) (n Integer))
  (class-relatives (find-class class-name) n))
  
(defmethod class-relatives ((class Class) (n Integer))
  (let* ((ancestors (n-generation-superclasses class n))
	 (descendants (n-generation-subclasses class n))
	 (ancestors-descendants
	  (mapcan #'(lambda (cl)
		      (cons cl (n-generation-subclasses cl (+ 1 n))))
		  ancestors))
	 (descendants-ancestors
	  (mapcan #'(lambda (cl)
		      (cons cl (n-generation-superclasses cl (+ 1 n))))
		  descendants))
	 (relatives (delete-duplicates
		     (nconc ancestors-descendants descendants-ancestors))))
    (delete class relatives)))

;;;-------------------------------------------------------

(defgeneric class-neighbors (class n)
  #-sbcl (declare (type (or Symbol Class) class)
		  (type Integer n)))

(defmethod class-neighbors ((class-name Symbol) (n Integer))
  (class-neighbors (find-class class-name) n))
  
(defmethod class-neighbors ((class Class) (n Integer))
  (delete class
	  (delete-duplicates
	   (let ((direct-subs (class-direct-subclasses class))
		 (direct-supers (class-direct-superclasses class)))
	     (flet ((recurse (cl) (cons cl (class-neighbors cl (- n 1)))))
	       (if (= 1 n)
		   (nconc direct-subs direct-supers)
		 ;; else
		 (nconc (mapcan #'recurse direct-subs)
			(mapcan #'recurse direct-supers))))))))

;;;============================================================

(defclass Arizona-Object (Standard-Object) ()
  (:documentation
   "A base class for all Arizona objects, just to help organize the
class hierarchy a little."))


(defmethod print-object ((object Arizona-Object) stream)
 
  "This method for <print-object> provides a special ``look''
 to make it a little easier to recognize Arizona objects."

  (declare (type Stream stream)
	   (:returns (type String)))
  (printing-random-thing
   (object stream)
   (format stream "~:(~a~)" (class-name (class-of object)))))



