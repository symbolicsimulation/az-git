;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Az -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Az)

;;;=======================================================
;;; These are lisp substitutes for functions that are intended to be
;;; implemented by foreign function calls, where that's feasible.

#-(and :excl :accelerators)
(eval-when (compile load eval)
(defmacro c_sqrt (x)
  (let ((xx (gensym)))
    `(let ((,xx ,x))
       (declare (optimize (safety 0) (speed 3))
		(type Double-Float ,xx))
       (sqrt ,xx))))

(defmacro a_sqrt (a)
  `(locally
       (declare (optimize (safety 0) (speed 3))
		(type (Float-Vector 1) ,a))
     (setf (aref ,a 0) (sqrt (aref ,a 0)))
     ,a))

(defmacro c_len2 (a r)
  (let ((x (gensym))
	(y (gensym)))
    `(locally
	 (declare (optimize (safety 0) (speed 3))
		  (type (Float-Vector 2) ,a)
		  (type (Float-Vector 1) ,r))
       (let ((,x (aref ,a 0))
	     (,y (aref ,a 1)))
	 (declare (type Double-Float ,x ,y))
	 (setf (aref ,r 0) (+ (* ,x ,x) (* ,y ,y)))
	 ,r))))

(defmacro c_trunc (x ix)
  `(locally
       (declare (optimize (safety 0) (speed 3))
		(type (Float-Vector 1) ,x)
		(type (Fixnum-Vector 1) ,ix))
     (setf (aref ,ix 0) (truncate (aref ,x 0)))
     ,ix))

(defmacro c_trunc2 (v iv)
  `(locally
       (declare (optimize (safety 0) (speed 3))
		(type (Float-Vector 2) ,v)
		(type (Fixnum-Vector 2) ,iv))
     (setf (aref ,iv 0) (truncate (aref ,v 0)))
     (setf (aref ,iv 1) (truncate (aref ,v 1)))
     ,iv))

;;; A special calculation for drawing arrows 

(defun arrowhead_points (p0 p1 angle length)
  (declare (optimize (safety 0) (speed 3))
	   (type (Fixnum-Vector 2) p0 p1)
	   (type Double-Float angle)
	   (type Fixnum length))
  (let* ((dx (fl (- (aref p1 0) (aref p0 0))))
	 (dy (fl (- (aref p1 1) (aref p0 1))))
	 (theta (if (= 0.0d0 dx dy) 0.0d0 (atan dy dx)))
	 (alpha (- pi angle))
	 (beta0 (- theta alpha))
	 (beta1 (+ theta alpha)))
    (declare (type Double-Float dy dx theta alpha beta0 beta1))
    (setf (aref p0 0)
      (the Fixnum
	(+ (aref p1 0) (the Fixnum (truncate (* length (cos beta0)))))))
    (setf (aref p0 1)
      (the Fixnum
	(+ (aref p1 1) (the Fixnum (truncate (* length (sin beta0)))))))
    (setf (aref p1 0)
      (the Fixnum
	(+ (aref p1 0) (the Fixnum (truncate (* length (cos beta1)))))))
    (setf (aref p1 1)
      (the Fixnum
	(+ (aref p1 1) (the Fixnum (truncate (* length (sin beta1))))))))
  (values))

)

;;;============================================================

#+(and :excl :accelerators)
(ff:defforeign-list
    (list
     (list 'c_sqrt
	   :entry-point (ff:convert-to-lang "sqrt")
	   :language :C
	   :arguments '(Double-Float)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil
	   :return-type :double-float)

     (list 'a_sqrt
	   :entry-point (ff:convert-to-lang "a_sqrt")
	   :language :C
	   :arguments '((Float-Vector 1))
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'c_len2
	   :entry-point (ff:convert-to-lang "c_len2")
	   :language :C
	   :arguments
	   '((Float-Vector 2) ;; 2d vector
	     (Float-Vector 1)) ;; length of vector
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'c_trunc
	   :entry-point (ff:convert-to-lang "c_trunc")
	   :language :C
	   :arguments '((Float-Vector 1) ;; input vector
			(Fixnum-Vector 1)) ;; output vector
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'c_trunc2
	   :entry-point (ff:convert-to-lang "c_trunc2")
	   :language :C
	   :arguments '((Float-Vector 2) ;; input vector
			(Fixnum-Vector 2)) ;; output vector
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

;;;=======================================================

     (list 'arrowhead_points
	   :entry-point (ff:convert-to-lang "arrowhead_points")
	   :language :C
	   :arguments
	   '( ;; input
	     (Fixnum-Vector 2) ;; p0
	     (Fixnum-Vector 2) ;; p1
	     Double-Float ;; angle
	     Fixnum ;; length
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'read_byte_array
	   :entry-point (ff:convert-to-lang "read_byte_array")
	   :language :C
	   :arguments
	   '((Simple-String) ;; pathname-
	     Fixnum ;; nskip
	     Fixnum ;; nbytes
	     az:Card8-Array ;; byte array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

	
     (list 'zero_byte_array
	   :entry-point (ff:convert-to-lang "zero_byte_array")
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Card8-Array ;; array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'unit_byte_array
	   :entry-point (ff:convert-to-lang "unit_byte_array")
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Card8-Array ;; array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'zero_unsigned_short_array
	   :entry-point (ff:convert-to-lang "zero_unsigned_short_array")
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Card16-Array ;; array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)
	  
     (list 'zero_unsigned_long_array
	   :entry-point (ff:convert-to-lang "zero_unsigned_long_array")
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Fixnum-Array ;; array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'zero_long_array
	   :entry-point (ff:convert-to-lang "zero_long_array")
	   :language :C
	   :arguments
	   '(Fixnum ;; n
	     az:Fixnum-Array ;; array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)
     
     (list 'integrate_long_int_array
	   :entry-point (ff:convert-to-lang "integrate_long_int_array")
	   :language :C
	   :arguments
	   '(az:Card28 ;; n
	     az:Card28-Array ;; input
	     az:Card28-Array ;; output
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)
     (list 'read_partial_image
	   :entry-point (ff:convert-to-lang "read_partial_image")
	   :language :C
	   :arguments
	   '((Simple-String) ;; pathname
	     Fixnum ;; nskip
	     Fixnum ;; rowlen
	     Fixnum ;; xstart
	     Fixnum ;; xend
	     Fixnum ;; ystart
	     Fixnum ;; yend
	     az:Card8-Array ;; byte array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)

     (list 'read_padded_image
	   :entry-point (ff:convert-to-lang "read_padded_image")
	   :language :C
	   :arguments
	   '((Simple-String) ;; pathname
	     Fixnum ;; header-bytes
	     Fixnum ;; nrows
	     Fixnum ;; ncols
	     Fixnum ;; padded-ncols
	     az:Card8-Array ;; padded band array
	     )
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t
	   :return-type :void)))

