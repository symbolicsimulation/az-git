\section{Assorted Common Lisp Tools}
\label{tools-section}

The Arizona-Tools module
provides
assorted general purpose utilities for Common Lisp programming.
Collecting these tools in one place saves users from reinventing 
many minor variations of the same wheel
and encourages programming style consistent with the conventions
suggested in section\ref{style-section}.

\subsection{System Compiling and Loading Tools}
\label{system-tools}

The Arizona-Tools module provides
several functions that can be used as the basis for a primitive defsystem
facility.
They are in the {\sf :User} package, 
so that they can be loaded before the {\sf :Arizona-Tools} package is defined.
If the source file (extension ``.lisp'' only)
is newer than the binary (extensions ``.bin'' and ``.fasl'' are supported),
{\sf user:compile-if-needed} compiles the source file.
It loads the binary whether the source is compiled or not.
{\sf user:compile-all-if-needed} iterates over {\sf files}
concatenating a directory string onto the front before calling
{\sf user:compile-if-needed}.
{\sf user:load-all} simply loads all the binaries without compiling.

\subsection{Runtime type checking}

{\sf az:declare-check} has a syntax like {\sf declare};
it is a macro that expands into type checking forms
for each type declaration.
It does not also expand into declarations, because macros are not allowed 
to expand into declarations (see \cite{Stee90}, p. 217).
{\sf az:declare-check} forms are analyzed
 by the Definitions module \cite{McDo91h}
as though they were {\sf declare} forms.

\subsection{Variations on {\sf setf}}

{\sf az:mulf}, {\sf az:divf}, {\sf az:maxf}, and {\sf az:minf}
are obvious variations on {\sf incf, decf}, etc.
{\sf az:multiple-value-setf} is to {\sf multiple-value-setq} 
as {\sf setf} is to {\sf setq}.

\subsection{Iteration tools}

{\sf az:with-collection} returns a list of whatever is collected 
within its scope
by calls to {\sf az:collect}, in the order that {\sf az:collect}
is called.
This is cleaner than a common idiom for the same purpose
that uses {\sf let}, {\sf push}, and {\sf nreverse}.
This was extracted somehow from the iteration constructs in PCL \cite{Bobr86};
we should probably come up with a more precise definition of what it does.

\subsection{Macro Writing Tools}

{\sf az:once-only} assures that the forms given as vars are evaluated in the
proper order, once only. 
It's useful for writing macros.
It was also extracted from PCL\cite{Bobr86} somehow.

\subsection{Bug report tools}

These functions are useful for generating text in output files for recording
for bug reports, benchmarking, etc.:
{\sf az:day-of-week-name} translates a zero-based day number
to a 3 character day name abbreviation (eg. ``Mon'').
{\sf az:month-number} translates a zero-based month number
to a 3 character month name abbreviation (eg. ``Jul'').
{\sf az:print-date} prints the current date in a human readable format.
{\sf az:print-system-description} attempts to print a verbose
comprehensive description of the current software/hardware environment.

\subsection{Program monitoring}

{\sf az:if-reentered}
provides a very simple way of detecting whether a piece
of code has been reentered or not.  
This was inspired by the desire
to have MAC window event functions call Slate event functions, 
without having to
worry that errors in the canvas event functions would cause unbreakable
infinite loops.

{\sf az:time-body} is a macro that returns the ``internal run time'' 
taken by the execution of the forms in its body.
Note that this means that the values returned by these forms are lost.

\subsection{Copy protocol}

Copying is something that many think should be 
provided automatically by an object system.
Copying is harder than it first appears,
because it's impossible to define, in general,
where the copying should stop.
There are two extreme alternatives, (1) to merely copy pointers
at the top level and (2) to follow all pointers, copying recursively.
The second isn't practical; it will almost always result in copying
the entire image, if you are really serious about following all
pointers, of all kinds.
Unfortunately, the first doesn't usually capture what you want.

\begin{itemize}

\item 
{\sf az:copy} is a base generic function that users can specialize
to control the copying Lisp objects, including, but not restricted to,
instances of CLOS classes.
However, we expect that most cases will be dealt with by specializing
one of the functions below, that are called by the default method
for {\sf az:copy}, rather than {\sf az:copy} itself.

A method for {\sf az:copy} should produce an object that is similar
to {\sf original}, 
in whatever sense of ``similar'' is appropriate for {\sf original}
(and {\sf result}).

The keyword {\sf result} argument allows the user to pass in
a preallocated object to hold the copy. 
In this case, a method for {\sf az:copy} changes the state of {\sf result}
to make it similar to {\sf original}.
This is done in the default method by calling {\sf az:copy-to!} (see below).

If no {\sf result} is supplied, a method for {\sf az:copy}
creates a new object.
The default method does this by calling {\sf az:new-copy} (see below).

Note that it is not assumed that {\sf original} and {\sf result}
are the same type.
When {\sf result} is not the same type as {\sf original},
{\sf az:copy} is interpreted as coercion.

A method for {\sf az:copy} is expected to signal an error
if given an {\sf original} and {\sf result} that it doesn't know how to handle.
This is done in the default method 
by calling {\sf az:verify-copy-result} (see below).

\item
{\sf az:new-copy} differs from {\sf az:copy} in that always
creates a new object to hold the copy.

\item 
A method for {\sf az:copy-to!} destructively changes the state of {\sf result}
to be similar to {\sf original},
in whatever sense is appropriate 
for the combination of {\sf original} and {\sf result}.
It should signal an error if given a combination it does not understand.

\item
A method for {\sf az:verify-copy-result?} tests to see if it's valid to copy
{\sf original} to {\sf result}.
If {\sf errorp} is {\sf nil}, it acts as a predicate, returning {\sf t}
or {\sf nil}, if the copying is valid or not.
If {\sf errorp} is {\sf t}, it signals an error if the copying
is not valid, and returns {\sf t} if the copying is valid.

\item
{\sf az:copy-slots-to!} is called by 
 the default method for {\sf az:copy-to!}.
It copies the slots (the pointers) from {\sf original} to {\sf result},
by extracting an initarg list from {\sf original}
and calling  
{\sf reinitialize-instance} on {\sf result}.
\end{itemize}

\subsection{Kill protocol}

The purpose of killing is to prevent an object from being used again,
and possibly to free up resources associated with that object.

A typical example is when a window in some non-lisp window system
is destroyed;
the lisp object that represented that window in the lisp environment
should be killed to prevent lisp from hanging up the window system
by trying to use a destroyed window.
Other objects related to the window may want to be killed as well to free
resources no longer needed.

The default method for {\sf az:kill} for CLOS instances 
is to change the class of the instance to {\sf az:Dead-Object}.
This will cause undefined method errors the next time a generic
function is called on the dead instance, but
generic functions that encounter dead objects frequently can be
given methods to allow them to deal with the situation more gracefully.
Changing the class to {\sf az:Dead-Object}
will cause the data in the slots of the killed instance to
become unreferenced and eventually get reclaimed by the garbage collector.

\subsection{Resource Protocol}

The generic functions, 
{\sf az:borrow-instance} and 
{\sf az:return-instance},
and the macro
{\sf az:with-borrowed-instance},
are used to implement a specializable protocol
that allows a class to provide a resource of instances of itself.
See the discussion of the Resource protocol 
in section~\ref{resources}.
