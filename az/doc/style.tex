%%% -*- Mode: Lisp; Syntax: Common-Lisp; -*-

\section{Style Conventions}
\label{style-section}

Some of these points are trivial; a few are actually
very important. 
These are NOT intended to be rules, but rather
suggestions for default conventions, to be followed unless there's a
good reason not to.  
Most are pretty arbitrary, even the important
ones---what's important is not that particular choice, but making one
choice and being consistent about it.  
The real point is to to make it
easier to understand little pieces of each other's code, to avoid
misunderstanding.  

\subsection{Modules and Packages}
\begin{itemize}

\item Each module should have its own directory and its own package.

\item The package should be defined in a separate {\sf package.lisp}
         file in the module's directory.

\item The module's directory should contain a {\sf load.lisp}
         which, when loaded, loads the module, and a {\sf compile.lisp}
         which, when loaded, compiles and loads the module.

\item In general, a module's package should not :use other packages,
         except Lisp (and maybe CLOS or PCL).

\item Packages should have reasonable short nicknames, to encourage
         people not to {\sf :use} them in another package.

\end{itemize}


\subsection{Abstract Data Types}
\label{abstract-data-types}

An abstract data type is defined by a functional interface,
its (user) protocol.
An abstract data type can be implemented by a CLOS class,
a defstruct, or other Lisp data type(s). 
In general, the user should not be able to depend on an abstract type
being implemented by defclass, defstruct, or some other means, but
should stick to the provided functional interface and complain
to the author if the interface isn't rich enough.
The fact that a type is defined by a defclass
should only be made public when the idea is to allow users to specialize
the type by defining their own subclasses.

\begin{itemize}

\item An abstract data type should be documented with two protocols:
         a user's protocol and an implementor's protocol.

\item The user's protocol should consist of safe functions;
         that is, they should report an error rather than give unpredictable
         results when called with invalid arguments.

\item The user's protocol (for the type {\sf Foo}) should include:
        \begin{itemize}

        \item {\sf (foo? foo0)} (or perhaps {\sf (foo-p foo0)})
         tests whether {\sf foo0} is a valid
         representation of the {\sf Foo} type.

        \item {\sf (make-foo \&rest options)} makes a new {\sf Foo}.

        {\sf options} is a ``property'' list of keyword--value pairs
        that determine how the new {\sf Foo} is to be initialized.

        \item {\sf (equal-foos? foo0 foo1)} tells if {\sf foo0}
         and {\sf foo1} are equivalent in whatever sense is
         appropriate for {\sf Foo}'s.

        \item {\sf (foo-bla foo0)} for getting the {\sf bla}
         property of {\sf foo0}.

        \item {\sf (setf (foo-bla foo0) new-bla)} for setting the {\sf bla}
         property of {\sf foo0}.

        \item {\sf (copy-foo foo0 \&key result)}
        returns a copy of {\sf foo0}.

        If {\sf result} is not supplied, {\sf copy-foo}
        makes a new {\sf Foo} for the copy. 
        If {\sf result} is supplied, and is capable of holding
        a copy of {\sf foo0}, the state of {\sf result} is modified
        to make it equivalent to {\sf foo0}
        (in the sense of {\sf equal-foos?}).
        
        If {\sf result} is not capable of holding a copy of {\sf foo0},
        an error should usually be signalled.

        However, in some cases, it may be desirable to have an {\sf error-p}
        argument which, when null, indicates that a new {\sf Foo}
        should be made and returned silently
        when the supplied {\sf result} is incorrect.
        
         Note that {\sf result} need not be the same type
         of object as {\sf foo0}.
         For example, \\ {\sf (copy-sequence a-list a-vector)}.
        \end{itemize}

\item Type names should be capitalized in code for readability.

\end{itemize}

\subsection{Immutable Object Types}
\label{immutable-object-types}

It's often useful to define an abstract type so that
its instances can be assumed to be immutable, that is,
whose internal state cannot be changed
(or, at least, is very difficult to change).
For example, we can easily cache the value(s)
of a frequently called, otherwise expensive function
whose arguments are all immutable objects.


\subsection{Limiting garbage: informal Result and Resource protocols}

Scientific functions frequently produce as results large data structures
(usually arrays) that are only needed temporarily. 
For example, consider evaluating an expression like:
\begin{verbatim}
(add-vector (scale-vector a x) y)
\end{verbatim}
where {\sf a} is a {\sf Single-Float} and {\sf x} and {\sf y}
are {\sf (Vector Single-Float (n))}, for some large {\sf n}.
The result of {\sf (scale-vector a x)} will be a large vector
of floats that immediately becomes garbage.
The Series extension to CL (Appendix A, \cite{Stee90}) is one attempt
to solve this problem;
it makes it possible to evaluate expressions like the one above
as implicit loops, 
without actually ever constructing the intermediate vector.
However, Series is not a solution for us, because it only solves
the problem for {\sf Sequences}.
We need a standard idiom for dealing with similar expressions
in arbitrary user-defined data types.
Our proposed answer is a style convention, followed in Arizona,
based on optional result arguments and resources of instances
of data types that are frequently used ion a temporary fashion.

\subsection{Optional Results}
        
\begin{itemize}
\item Functions that return a sizable result may take
an {\sf \&key} result argument. For example:
{\sf (compose map0 map1 :result map2)}

\item A function that follows the result protocol
should test a supplied {\sf :result}
to ensure that it is valid and to signal an error
if it is not.

\item The value returned should be {\sf eq} to the supplied
{\sf :result} if an error is not signaled. 

(This may be violated in some circumstances.
For example, it might be convenient to have {\sf compose}
accept arguments that are either arrays or numbers
and to have {\sf (compose x y :result z)} simply ignore {\sf z}
if {\sf x}, {\sf y}, and {\sf z} are all numbers.)

\item The user can indicate a destructive
operation by supplying a {\sf :result} that is {\sf eq}
to one of the other arguments. 
That is, {\sf (compose map0 map1 :result map1)},
is supposed to overwrite {\sf map1} with the
composition of {\sf map0} and {\sf map1}.
The function is not required to support such
destructive operations, but it should test 
for {\sf eq}-ness between the {\sf :result}
and the other arguments, and signal an error
if it does not support the corresponding destructive operation.

(This may also be violated when the result is a number or something similar.)

\end{itemize}


We may extend the Result protocol to functions returning multiple values.

\subsection{Resources}
\label{resources}

Suppose {\sf Bla} is an abstract data type
whose instances are sizable
objects.
Then {\sf Bla} may elect to follow the Resource protocol:
\begin{itemize}

\item {\sf (borrow-bla \&rest options)} gets a {\sf Bla}
 from the {\sf Bla} resource, or makes a new
 one if the resource is empty.
{\sf options} is a ``property'' list of keyword--value pairs
that determine how the borrowed {\sf Foo} is to be initialized.

\item {\sf (return-bla bla0)} returns the {\sf bla0}
to the resource. This must be used with care,
because any further reference to {\sf bla0}
will be incorrect. However, most functions will
not test to see if their arguments have been
returned to a resource.

\item {\sf (in-bla-resource? bla0)} can be used to test
if {\sf bla0} thinks it's in the {\sf Bla} resource,
which implies that something's wrong,
the reference to it that was passed to {\sf in-bla-resource?}
persisted after {\sf bla0} was returned.

\item {\sf (with-borrowed-bla (name \&rest options) \&body)}
is a macro that borrows a {\sf Bla}, 
initializes it using {\sf options},
binds it to {\sf name},
and then returns it to the resource on exit, returning
the values returned by the last form in {\sf body}.
Like {\sf return-bla}, it must be used with care.

\item {\sf (with-borrowed-blas (names \&rest options) \&body)}
is a macro that borrows a {\sf Bla} for each {\sf name} in {\sf names}, 
initializes each one using {\sf options},
binds each to the corresponding name,
and returns them to the resource on exit,
returning the values returned by the last form in {\sf body}.
Like {\sf with-borrowed-bla}, it must be used with care.

\end{itemize}

A sample implementation for vectors:

\begin{verbatim}
(defparameter *vector-resource* (make-hash-table :test #'eql))

(defun borrow-vector (length)
  (declare (special *vector-resource*))
  (check-type length Fixnum)
  (let ((v-list (gethash length *vector-resource* ())))
    (cond (v-list
           (setf (gethash length *vector-resource*) (rest v-list))
           (first v-list))
          (t (make-long-enough-vector length)))))

(defun return-vector (v)
  (declare (special *vector-resource*))
  (check-type v Vector)
  (push v (gethash (length v) *vector-resource* ())))

(defun in-vector-resource? (v)
  (find v (gethash (length v) *vector-resource* ())))

(defmacro with-borrowed-vector ((name length) &body body)
  ;; use {\sf return-name} so we deallocate the right thing,
  ;; even if the user re-assigns {\sf name}.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-vector ,length))
            (,name ,return-name))
       (multiple-value-prog1
         (progn ,@body)
         (return-vector ,return-name)))))

(defmacro with-borrowed-vectors ((names length) &body body)
  (let* ((return-names (mapcar #'(lambda (name) (gensym (string name)))
                               names))
         (borrowings (mapcar #'(lambda (return-name)
                                 `(,return-name (borrow-vector ,length)))
                             return-names))
         (returnings (mapcar #'(lambda (return-name)
                                 `(return-vector ,return-name))
                             return-names))
         (bindings (mapcar #'(lambda (name return-name) `(,name ,return-name))
                           names return-names)))
    `(let* (,@borrowings ,@bindings)
       (multiple-value-prog1
         (progn ,@body)
         ,@returnings))))
\end{verbatim}

\subsection{Miscellaneous}

\begin{itemize}

\item Integer intervals should be specified by a {\sf start}---{\sf end}
        pair as in the CL sequence functions;
         the convention is that the interval includes {\sf start}
         but not {\sf end}. For example, {\sf start=1, end=5}
         specifies the sequence {\sf 1 2 3 4}.

         {\it It is very important to abide by this
         strictly.} Otherwise it's impossible to eradicate
         fencepost errors (off by 1), especially from code dealing
         with bitmap displays. 

         This convention may seem unnatural
         at first, but, in my experience, it tends to simplify
         code. For example, the length of the interval
         is {\sf (- end start)}. If you partition an interval into
         subintervals, by {\sf i0 i1 i2 i3} then the first
         subinterval is {\sf i0 i1}, the second is {\sf i1 i2},
         and so on.

\item Names should be zero-origin wherever at all reasonable.
         For example, {\sf (defun plus (a0 a1) ...)}
         rather than {\sf (defun plus (a1 a2) ...)}.
         The reason for this is that, in my opinion, zero-origin
         is unnatural, but CL forces us to use it for arrays and sequences.
         Because it's easy to get confused, it's important
         to be consistent everywhere.

\item Predicate names should end with a {\sf \#/?}.
      The usual CL convention, ending with {\sf -p} is not as expressive,
      and is easily confused with an accessor for a {\sf p}
      property (eg. the parameter of a binomial distribution object).

\item Don't mix {\sf \&key} and {\sf \&optional} arguments;
         in general, don't use {\sf \&optional} arguments.

\item Top-level (user entry) functions should use {\sf \&key}
         freely. If a frequently called function, {\sf foo}, has an elaborate
         {\sf \&key-\&rest} argument list, make a separate {\sf \%foo}
         function that is called by {\sf foo} and has all required
         arguments.

\item Top-level (user entry) functions should be safe against
         invalid arguments; that is, they should signal errors
         for invalid arguments rather than just returning unpredictable
         results. Because it's hard to know just what assumptions are
         crucial, they should {\sf assert} as many predicates as possible
         about their argument values. For efficiency, provide an unsafe
         {\sf \%foo}, that is called by the safe {\sf foo} and can be called
         by other functions that take the responsibility for passing valid
         arguments.

\item Common errors should be signaled by calling a specially defined
         function (eg. \\ {\sf missing-method-error}), rather than just
         calling {\sf error}, {\sf cerror}, or {\sf warn}. In addition
         to making for more readable code, this should make it possible
         to adapt to more sophisticated CL support for signalling and
         handling conditions that's likely to appear in the future.

\end{itemize}

\subsection{Style Conventions for CLOS}

\begin{itemize}

\item Generic functions that dispatch on only one argument
 should have that as the 1st argument, unless
 there's a good reason not to.

\item A generic function that has only one method should be an
 ordinary function that uses \\ {\sf az:type-check}
 to ensure the validity of its argument(s).

\item Use of {\sf :before}, {\sf :after} and other combined methods
 should be avoided. The only clearly valid use is in
 initialization methods, because the initialization protocol
 was designed with them in mind.

\end{itemize}

