\section{Overview}

This document provides an introduction to a system 
called Arizona,
now under development at the U. of Washington.
It consists of this overview section,
a section outlining suggested style conventions to be followed
by contributors to Arizona,
followed by a sections describing the most basic module in Arizona,
called Arizona-Tools.

Arizona is intended to be a portable, public-domain collection 
of tools supporting scientific computing, quantitative graphics, 
and data analysis,
implemented in Common Lisp
and CLOS (the Common Lisp Object System)
\cite{CLOS88,Fran88,Keen88,Mill90,Stee84,Stee90,Tata87,Wins88}.

Discussion of the philosophy underlying Arizona can be found in
\cite{McDo85a,McDo85b,McDo86b,McDo87a,McDo88a,wxs87a,wxs87b}.
Briefly, the design is motivated by our belief that
an ideal system for scientific computing and data analysis should have:
\begin{itemize}
\item One language that can be used for both for line-by-line interaction
      or defining compiled procedures.
\item Minimal overhead in adding new compiled procedures 
      (or other definitions).
\item A language that supports a wide variety of abstractions
      and the definition of new kinds of abstractions.
\item Programming tools (editor, debugger, browsers, metering
        and monitoring tools).
\item Automatic memory management (dynamic space allocation 
      and garbage collection).
\item Portability over many types of workstations and operating systems.
\item A community of users and developers.
\item Access to traditional Fortran scientific subroutine libraries
      or equivalents.
\item A representation of scientific data directly in the data structures
      of the language.
\item Comprehensive numerical, graphical, and statistical functionality.
\item Device independent static output graphics.
\item Window based interactive graphics.
\item Support for efficient and concurrent access to large databases.
\item Documentation and tutorials, both paper and on-line.
\end{itemize}
One reason Arizona is based on Common Lisp is that 
the first nine points (through ``access to Fortran'')
come for free with standard Common Lisp environments.
The remaining six are the research aspects of Arizona.

\subsection{Existing Modules}

Arizona is divided into a number of modules 
with limited interdependencies,
to permit individual modules
to stabilize and be ``released'' and used independently.
The word ``module'' is used here informally.
The definitions in a module will usually be in a package
defined for that module
and the source code for those definitions 
will usually be found in files in a single directory.

A number of the modules in Arizona are described below.
Current modules in alpha release are
Arizona-Tools,
Definitons, Announcements, Actors, Geometry,
Slate, Chart, Graph, Browser, 
and an interface between Lisp 
and the Fortran optimization package NPSOL.
Four other modules have seen some use but are likely
to have more significant changes before any sort of release:
Clay, Basic-Math, Probability, and Cactus.

\subsubsection{Arizona-Tools}

The Arizona-Tools module, described in more detail 
in section~\ref{tools-section},
provides
assorted general purpose utilities for Common Lisp programming.
Collecting these tools in one place saves users from reinventing 
many minor variations of the same wheel
and encourages programming style consistent with the conventions
suggested in section\ref{style-section}.

For example, Arizona-Tools defines protocols for
copying, saving to file, and destroying CLOS objects,
and default implementations of those protocols,
which are some essential facilities for a primitive database
using persistent CLOS objects.

\subsubsection{Definitions}

The Definitions module \cite{McDo91h}
provides the beginnings of a database for Common Lisp
source code objects.
The major use of this database at present is in automatically
typesetting reference manuals (using Latex \cite{Lamp85}), 
examples of which are the reference manual sections of
\cite{McDo91h,McDo91h,McDo91k,McDo91l,McDo91m,McDo91n,McDo91p}
and this report.
It is inspired in part by the Definition Groups of Bobrow et. al \cite{Bobr87}
and the {\sf USER-MANUAL} of Kantrowitz \cite{Kant91}.

The primary purpose of Definitions is to make possible
convenient runtime access 
to information available in Common Lisp source code,
that is lost in the normal process of reading, evaluating, and/or compiling.

Evaluating some Lisp definitions, such as {\sf defclass},
results in a first class Lisp object 
with a reasonable and reasonably portable
protocol for extracting useful information,
such as the direct sub- and super-classes.
However, most Lisp definitions, such as {\sf defun},
while producing identifiable objects,
have limited facilities for extracting useful information;
usually the documentation string is all that is available.
And other definitions, such as {\sf defstruct},
do not even produce an identifiable object.

The Definitions module provides functions
to read source files and
create a Definition object
for each lisp definition in those files,
retaining the complete original defining lisp form.

\subsubsection{Announcements}

The Announcements module \cite{McDo91m} provides a simple mechanism 
for maintaining dependencies between objects
through message broadcasting, 
similar to the Field \cite{Reis90a} and Forest environments
It is derived from 
a Common Lisp implementation of Events/Mediators \cite{Sull90a}
by Mark Niehaus for the Prism system \cite{Kale90a}.
Like Field and Forest, 
it can be thought of as a mechanism for 
{\it implicit invocation} as discussed in \cite{Garl91a,Sull91a}.

The basic idea is to allow an object (the {\it announcer}
to notify other objects (the {\it audience})
of a change of state (or some other event)
without the announcer having to know of which objects need to be
notified of which events.

An {\it announcement} is defined with a call to {\sf defannouncement}.
Any announcer object can {\sf announce} any defined announcement,
which results in every member of the audience for that announcer-announcement
pair being notified.
Any object can join the audience for a particular announcer-announcement
pair with a call to {\sf join-audience}.

\subsubsection{Actors}

The Actors module \cite{McDo91p}
is primarily intended as a mechanism for interpreting and distributing
asynchronous (input) events in a multiprocessing Lisp environment.
It is inspired in part by the actor message passing paradigm
of Carl Hewitt (as described in \cite{Agha86}).

An {\sf Actor} is an object with a message queue;
each actor handles its messages in its own, separate, thread of computation,
at least conceptually in parallel with other actors
and in parallel with any read-eval-print loops 
or other I/O processes
that may exist.

Actors contain multiple {\sf Role} objects; 
the {\it current role} determines 
how an actor handles a message at any given time.
A common response to a message is to simply change the current role.
An actor can be thought of as an implementation of a finite state machine
for handling (input) events;
each role can be thought of as a state in a state-transition diagram
and the events that cause changes of role are the transitions.

Actually, a role is more accurately thought 
of as a fairly independent subgraph of a state-transition diagram.
The advantage of 
encapsulating more-or-less independent chunks of behavior in role objects
is that they can then be composed in different ways in various
subclasses of {\sf Actor},
making it easier to implement new graphical user interfaces.

One important subclass of {\sf Actor} is {\sf Interactor}, 
in the Slate package.
Interactors are actors whose queues receive messages
corresponding to the low level mouse and keyboard events received
by windows.

Another important subclass is {\sf Coordinator},
in the Clay package.
Coordinators are objects that relationships 
between interactive diagrams and the diagrams' subject --- 
the underlying data 
that the diagram represents.
``Coordinator'' is really just another word for ``Mediator''
as used in \cite{Sull90a}.
Coordinators receive messages from diagrams requesting operations
to be performed on their subjects
(which originate from user input)
and they also receive messages (via Announcements \cite{McDo91m})
from subjects that indicate
that diagrams need to be updated to reflect changes in the
state of a subject.

\subsubsection{Geometry}

The Geometry module \cite{McDo91j} is intended to support 
common geometric calculations
arising in graphics, numerical linear algebra, optimization,
and scientific computing in general.
The goal is to allow geometric computation using
abstractions that directly represent high level mathematical 
concepts like affine spaces, vectors, and linear transformations,
while retaining the level of performance provided by traditional
scientific subroutine packages like Linpack\cite{Dong79}.
It will be derived from Cactus \cite{McDo91t,McDo89b}
and improved with ideas from work by DeRose \cite{DeRo88,DeRo89a,DeRo89b}
and Segal \cite{Sega89}.

However, the current release of the Geometry module
does not support general geometric calculations.
It consists of two submodules specialized for
high performance in simple graphics calculations,
needed by the Slate and Chart modules discussed below.
The Screen Geometry submodule supports calculations in a discrete
two-dimensional coordinate system called Screen Space, ie. a bitmapped display.
The Chart Geometry submodule supports calculations in a 
continuous (float) two-dimensional coordinate system,
called Chart Space,
a natural world space for simple scientific diagrams. 
In addition, the Geometry module provides affine mappings between
the Chart and Screen spaces.

\subsubsection{Slate}

Slate \cite{McDo91k} is a low level ``device-independent'' graphics package 
that has been ported to a number
of Common Lisp platforms, window systems, and other graphics devices.
It is intended to be used by developers 
of higher level scientific and statistical graphics systems
rather than end users.
Slate is something like a simplified version of CLX, ported
to run on window systems and graphics devices other than X11.


Some of the design goals of Slate are:
\begin{itemize}

\item It should be robust against errors in the calls from a higher level
      graphics system.

\item It should be easy to port to a new window system,
      hardcopy device, or Lisp compiler.

\item It should permit high perfomance implementations.

\item Unless there's a good reason to do otherwise,
      the design should be as close to X as possible,
      because we expect the X port to be the most important.

\end{itemize}

The basic abstraction in Slate is) the {\it slate}.
Slates are surfaces that can be drawn on and can receive input
of various kinds.
To make porting easy, the imaging model is fairly primitive;
Slates are essentially bitmaps of some finite depth.

At present the set of drawing operations
allows us to outline, fill, or tile simple geometric shapes
like points, line segments, or polygons,
draw characters and strings in a variety of fonts and colors,
and copy rectangular sets of pixels from one slate to another.
The behavior of all the drawing operations in Slate 
can be described in the following way:
The drawing operation changes the values of a set of destination pixels
in the slate.
For each destination pixel, a source pixel value is calculated
and the destination pixel is set to some simple function 
(at present some boolean combination of the bits) of the source
and destination pixel values.

A typical drawing operation takes three types of arguments:
a {\it pen}, which is an abstraction that encapsulates
boolean operation, color, font, line style, etc.,
a slate to draw on,
and a specification for the set of destination pixels.
Sets of destination pixels are specified
using the abstractions for discrete screen geometry
provided by the Geometry package (see \cite{McDo91j}).

Slate provides seperate abstractions for the more complex pen parameters,
like colors,
boolean operations,
fonts,
line styles,
and tiling patterns.

There are two basic kinds of slates, standard, visible slates
and {\it invisible slates.}
All drawing operations work ``in the same way''
on both visible and invisible slates
An invisible slate serves roughly the same purpose as a pixmap in X.
It gives us a surface to draw on that can be copied rapidly
to multiple places on one or more visible slates.

Another major abstraction in Slate in the {\it screen}.
The screen object captures information about the device a slate
is actually displayed on that isn't specific to any slate,
such as, the number of bits per pixel.
What color the user sees for a given pixel value is determined
by the current {\it colormap} of the slate's screen.

\subsubsection{Chart}

Chart \cite{McDo91l} is a simple example of a (slightly) higher level graphics
package built on top of Slate.
It is a quick and dirty approximation to S style graphics
\cite{Beck84a,Beck84b,Beck88a}.
What we mean by ``S style graphics'' is output-only,
line and point plots,
with labels and tic marks,
where locations are specified in an arbitrary 2d world coordinate
system and the scaling to screen coordinates is done more or less
automatically.

\subsubsection{Graph}

The Graph module \cite{McDo91n}
provides a simple protocol for data structures used to represent
graphs (networks), graph nodes, and graph edges,
and a sample implementation of that protocol.

\subsubsection{Browser}

The Browser module \cite{McDo91n} provides a generic graph browser,
implemented using Clay,
and three examples of how the browser can be specialized to
particular applications: 
a CLOS class hierarchy browser,
a browser for graph of related CLOS objects,
where the edges in the graph correspond to slot references from
one object to another,
and a pedigree browser.

\subsubsection{Lisp-NPSOL Interface}

The NPSOL package \cite{McDo91i}
provides a number of functions for calling
the Fortran optimization package NPSOL  
\cite{Gill86a,Gill86b,Murt78,Murt82,Murt87}
from Common Lisp, using the foreign function interface
provided with Franz Allegro Common Lisp \cite{Fran91a}.

\subsubsection{Clay}

Clay \cite{McDo91u} is the beginnings of a system 
designed to support drawing and interacting with
2d (and 3d) pictures for visualizing scientific data,
both the observational data that's the usual domain of statisticians
and the computed ``data'' arising in computer experiments
and algorithm animation.  
It provides a toolkit of standard plot
components and mechanisms for pasting components together to make it
easy for the user to improvise new kinds of plots.  
All plot components obey a clearly defined protocol.  
This means that there is a
consistent user interface and that users can reliably define new types
of plot components that serve in the existing user interface.

\subsubsection{Basic Math}

The Basic Math module \cite{McDo91w}
consists of things that can be reasonably implemented
with Common Lisp functions and primitive Common Lisp data structures;
it does not use CLOS.
Included in Basic Math are:
machine constants,
special functions (eg. beta, gamma)
extended vector operations
(analogous to the BLAS \cite{Laws79} used in Linpack \cite{Dong79}),
evaluation and interpolation (eg. generic continued fractions)
1d numerical integration,
and basic random number generators.

\subsubsection{Probability}

The Probability module \cite{McDo91v} supports
inference and Monte Carlo simulation (including bootstrapping)
in a unified framework
through a protocol for {\tt Probability-Measure} classes.
Probability measure objects are responsible for
generating samples from themselves,
computing their quantiles,
and computing the probabilities of appropriate
sets, including tail probabilities.
The defined probability measure classes includes the standard
one- and higher-dimensional parametric densities and discrete distributions,
and non-parametric measures, either resulting from density estimates
or the empirical measure of a data set.
(It's worth noting that simple descriptive statistics like mean, median,
etc., are generic functions in the probability measure protocol
and are applied to data sets by viewing them as empirical distributions.)

\subsubsection{Cactus}

Cactus \cite{McDo89b,McDo91x} 
is a system for numerical linear algebra 
and optimization implemented in CLOS.
Cactus is designed to closely model the abstractions used in a course
on finite dimensional (vector) spaces \cite{Noll87}.
It provides representations for a variety of {\it spaces,} 
{\it points} (elements of some space), 
and {\it mappings} between spaces.
The linear algebra core of Cactus deals with
vector spaces, vectors, and linear transformations;
more general spaces, points, and mappings are used
are used to support constrained optimization
at a similar level of abstraction \cite{Luen69}.

The implementation of Cactus is, in part, 
an experiment to see how closely a program 
can follow natural mathematical abstractions,
without losing the level of performance in large numerical problems
provided by traditional Fortran subroutine libraries.
To evaluate the results of this experiment,
I have compared, in \cite{McDo89b},
the design, implementation, and performance of
Cactus on certain standard numerical linear algebra problems
with Linpack \cite{Dong79}, 
a classic high-quality Fortran package for solving
systems of linear equations and related problems.

The result of the experiment was that the overhead in 
using a high level of abstraction is modest ---
the runtimes of Cactus versions of common matrix decompositions
range from about the same as the corresponding Linapck routines
to perhaps about 50\% longer.
At the same time, the object-oriented design of Cactus
eases code reuse and customization in ways that are not possible
in Fortran.

\subsection{Current Ports}

This release has had very limited testing in:
\begin{itemize}
\item Franz Allegro CL 4.0 with CLXr5, X11r4, 
on Sun MIcrosystems Sparcstation 2 and 330.
\end{itemize}

\subsection{Getting Arizona}

This release of Arizona is available via anonymous FTP
from belgica.stat.washington.edu \\ 
(128.95.17.57)
in the directory /pub/az.
Each subsystem is available in its own compressed tar file
(eg. geometry.tar.Z).
In addition, the Latex files making up this document are in doc.tar.Z.

Remember to use binary mode when ftping the compressed tar files.
Copy the tar files into the directory you want to be the Arizona
root directory.
Uncompress and extract the tar files to create a copy of the Arizona
directory tree.
Edit files.lisp in the Arizona root directory to be consistent
with your choice.
Edit all the make.lisp and load.lisp files in the various subdirectories
to be consistent with your choice of Arizona root directory.
You may also need to make changes to reflect what version of PCL
you have and where it is kept, 
if it's not automatically part of your lisp image.

Then start up your lisp and load (don't compile it) a make.lisp
from one of the subdirectories to compile and load all 
that subsystem and all the other subsystems it depends on.
In this release, loading chart/make.lisp will compile and load
everything.

Once everything is compiled, users without permission to modify the 
Arizona root directory can safely load Chart, Slate, or any other
system by loading the appropriate load.lisp.
See the Tools module described section~\ref{tools-section} 
for more details on making and loading.