\documentstyle[code,twoside]{article}
\pagestyle{headings}

\topmargin=-0.2in
\textwidth=6.0in
\textheight=8.0in
\oddsidemargin=0.25in
\evensidemargin=0.25in

\begin{document}

\title{Announcements: an implementation of implicit invocation}

\author{{\sc John Alan McDonald}\\
        Dept. of Statistics,
	University of Washington\\
         }
\date{Oct 1991}

\maketitle

\begin{abstract}
This report describes the Announcements module, 
which provides a mechanism for implicit invocation
(i.e. broadcast message sending) for Common Lisp.
\end{abstract}


\section{Overview}

Inspired by Events/Mediators of Sullivan and Notkin \cite{Sull90a},
ideas about implicit invocation of Garlan and Notkin \cite{Garl91a},
the broadcast message sending of Field system of Reiss \cite{Reis90a},
a Common Lisp implementation of Events/Mediators
in the Prism system by Mark Niehaus,
and dissatifaction with some aspects of earlier work by this author
in the Antelope system \cite{McDo86b}
and the Plot Windows system of Stuetzle \cite{wxs87a,wxs87b}..

The major suggestion is to make Events global named objects in the
same sense as functions, classes, packages, etc.  The normal way of
announcing and registering with an event would be by name, rather than
by a reference to a literal Event object. The normal way of defining
an Event would be with a {\sf defevent} form rather than a direct call to
{\sf make-instance}. The {\sf defevent} form could possibly take a lambda list
that would allow us to specify, parse, and check the args with which
the event is announced. Note that this does not prevent continuing to
use literal references to Event objects, just as one can use direct
references to function and class objects.

It seems to me that the current implementation requires the objects
participating in an Event interface to know too much about one
another. In my case, it requires the graph object to keep pointers to
the events it wants to announce. It also requires the graph
presentation to know how to get a hold of the events that the
particular graph might announce, so that it can register its interest.
And the event interface isn't "published" in the code; it's inside the
slots and initialization of the graph and the graph presentation.

Using global {\sf defevent} forms would make it possible to publish an
event interface without explicit reference to either the event
announcer or its listeners --- which means that the same event
interface can be used in a variety of contexts. For example, I'd guess
that the most common event interface is essentially having objects
announce 'state-changed and letting their dependents figure out how to
update.

A disadvantage of making Events global is that many objects would be
announcing the same Event, eg., all buttons would announce the same
{\sf 'button-press-event}.  A typical listener is only interested being
notified of a {\sf 'button-press-event} for a particular button.

This can be fixed by giving {\sf announce}, {\sf register}, 
and {\sf unregister}
another argument, the ``announcer''. The Event object will be
responsible for dispatching notification of the event to only those
who have registered interest in the event and its announcer.  This has
the side effect of making {\sf announce} more like an inverse of {\sf send},
as in flavors, which for some reason appeals to me.

For example:
\begin{code}
(defevent 'state-changed ())

(register graph 'state-changed graph-presentation #'subject-state-changed)

(announce graph 'state-changed)
\end{code}


Scenario 1) I'm implementing graphical presentations of an existing
object or data structure designed by someone else.

I want to set up automatic dependency maintainance with the minimum
amount of knowledge about the internals of alien objects and minimal
changes (preferably none) to the structure and behavior of those
objects.

I can't add slots to an alien data structure (it might not be a class
or structure instance and it's a bad idea in any case).  To associate
event objects with arbitrary data structure I would have to use some
kind of global table that maps arbitrary objects to events of a
certain type, which is almost the same as what I proposed, depending
on whether you dispatch on object or event type first.

This leaves open the question of how I arrange to have events
announced at the appropriate time. My belief is that it's easier to
arrange for events to be announced at the right time without messing
with the internals of alien code than it is to add additional data to
an alien structure.  For example, if I'm using a passive alien data
structure in my code, then I can announce events in my code whenever I
modify the data structure.  More generally, I can use {\sf advise} or
after methods to cause alien functions to announce events without
otherwise changing their behavior.

Scenario 2) Someone else wants to use my presentation to display their
data structure. They don't want to know about Event objects.  It's a
lot easier to tell them to just
\begin{code}
(announce your-object :state-changed changed-args)
\end{code}
when they want the picture to update, than it is to tell them that
they have to make-instance of Event, save that event somewhere,
provide the right method for my generic function so my presentation
can get a hold of that event to register with it, and announce that
event object when they want the display to update.

I went through this a couple days ago with another user of the graph
browser, and he's still confused. Partially, he's confused because
he's new to CLOS programming and partially because he hasn't spent the
hour or two it would take to understand the underlying ideas.
However, in my opinion, he shouldn't have to understand that much
about events in order to use them, at least in this trivial case.

I didn't put this very well. 

My major motivation for making events global is that I would like to
be able to define an event interface without reference to the objects
that are going to participate in it.

One reason is that I'd like the set of events to be defined before
both the announcers and the receivers. Right now, there's an
asymmetry. The event interface is defined in the announcer class. It's
relatively straightforward to implement a receiver to deal with an
existing announcer. But it's hard to find out what events to announce
if you want to implement a new announcer to go with an existing
receiver.

A more fundamental, and also more subjective, reason is that I think
there are likely to be a relatively small number of conceptually
distinct event interfaces (eg. a generic :state-changed event) and it
would be a good idea to represent these abstract event interfaces by
independent objects.

I agree that it's important to make it explicit in the code who is
intending to announce an event, as well as who is interested in
receiving it. This could be done by having announcers register
themselves with the event as well as the receivers. It could also be
done more declaratively, with some top level form that would state
that all objects of type Foo intend to announce events of type Bar to
be received by objects of type Baz.

Another thing to note is that having global named events doesn't
preclude using private anonymous events in exactly the way they are
used now.  It would still be possible to announce an event, rather
than the name of an event, to bypass the first level of dispatching.
It would also be easy to make events smart enough to skip the next
level of dispatching if they only had one possible announcer.

(A side point:

My policy is that the exact slots an object has is not advertized to
the world. Rather, what's advertized is a functional interface to an
abstract data type that may be implemented as a class instance, a
structure instance, or a primitive Lisp object.  I think this is a
generally accepted style.  For example, it's what Keene advocates in
``Object-Oriented Programming in Common Lisp'' \cite{Keen88}.

It's true that CLOS doesn't provide much explicit support for this
kind of encapsulation. I don't trust comments either. The best
convention I've come up with is to represent the functional interface
by a set of defgenerics that are loaded before any class or structure
definitions and to organize later method definitions according to
which functional interface they are implementing.)

The minor points:

"Signal" is a function in the Condition system (CLTL2 \cite{Stee90}, ch. 29);
"event" isn't in the index, so it's probably better.  The advantage of
long names is that the inconvenience makes it less likely that
somebody has used it already. Whether eliminating a small danger of
confusion is worth the constant annoyance of mistyping long names is
open to question.

As far as Conenctors goes, what I really wanted was to reduce the
conceptual overhead, rather than performance overhead. didn't want to
have to know how Connectors are represented in order to register with
an event.


\vfill
\pagebreak

\section{Reference Manual}
\label{reference-manual}
\pagestyle{myheadings}
\input{announcements-ref}

\bibliographystyle{plain} 

\bibliography{/belgica-2g/jam/bib/arizona,/belgica-2g/jam/bib/cs,/belgica-2g/jam/bib/cactus,/belgica-2g/jam/bib/constraint,/belgica-2g/jam/bib/database,/belgica-2g/jam/bib/layout,/belgica-2g/jam/bib/lisp,/belgica-2g/jam/bib/image,/belgica-2g/jam/bib/buja,/belgica-2g/jam/bib/mcdonald,/belgica-2g/jam/bib/friedman,/belgica-2g/jam/bib/wxs}


\end{document}