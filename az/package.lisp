;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :cl-user)

#+:pcl
(eval-when (compile load eval)
  (let* ((package (find-package :pcl))
	 (nicknames (package-nicknames package))
	 (name (package-name package)))
    (unless (member "CLOS" nicknames :test #'string-equal)
      (rename-package package name (cons "CLOS" (copy-list nicknames))))
    #+:cmu17
    (pushnew :clos *features*)))

#+:pcl (pushnew :returns pcl::*non-variable-declarations*)
#+:pcl
(eval-when (compile load eval)
  ;; PCL exports consistent with CLTL2:
  (export '(clos::allocate-instance
	    #-:cmu17 clos::class
	    clos::class-direct-subclasses
	    clos::class-direct-superclasses
	    clos::class-slots
	    clos::slot-definition-name
	    clos::slot-definition-type
	    #-:cmu17 clos::generic-function)
	  :clos))


(defpackage :Az
  (:use :Common-Lisp #+:clos :CLOS)
  
  (:export 
   while
   until
   for
	    
   -double-float-radix-
   -double-float-digits-
   -single-float-radix-
   -single-float-digits-
   -smallest-positive-magnitude-
   -largest-magnitude-
   -smallest-relative-spacing-
   -largest-relative-spacing-
   -log10-double-float-radix-
   -log-smallest-positive-magnitude-
   -log-largest-magnitude-
   -log-smallest-relative-spacing-
   -pi/2- -2pi- -lnpi- -sqrt2pi- -lnsqrt2pi-
   -large-double-float- -small-double-float-
   large? small? sign sq abs-difference
   l2-dist2-xy l2-dist-xy
   l2-norm2-xy l2-norm-xy
   %row-major-index
   under/overflow-danger?
   examine

   empty-slot?
   all-subclasses
   all-superclasses
   all-slot-values
   interesting-slot-values

   Arizona-Object 

   copy
   new-copy
   verify-copy-result?
   copy-to!

   equal?

   c_sqrt
   a_sqrt
   c_len2
   c_trunc
   c_trunc2
   arrowhead_points
	    
   Dead-Object
   kill-object

   borrow-instance
   return-instance
   with-borrowed-instance
   ;;with-borrowed-instances

   ;; Timestamping az for lazy evaluation
   Timestamp
   make-timestamp
   equal-timestamps? copy-timestamp
   stamp-time!
   timestamp< timestamp<= timestamp= timestamp>= timestamp>
   format-timestamp

   without-gc
   large-allocation
   with-gc-tuned-for-large-allocation
   type-check
   declare-check
   clean-declare
   bound
   fl
   mulf divf maxf minf negf;; like incf
   deletef

   Boolean
   Funcallable
   Array-Index
   Positive-Fixnum
   Positive-Real

   Real-Array
   Real-Vector
   Real-Matrix

   Float-Array
   Float-Vector
   Float-Matrix
   Float-List
   Positive-Float
   Positive-Float-List

   Fixnum-Array
   Fixnum-Vector
   Fixnum-Matrix

   Array-Index-Array
   Array-Index-Vector
   Array-Index-Matrix

   Bit-Array
   Bit-Matrix

   Card4
   Card4-Array
   Card4-Vector
   Card4-Matrix

   Card8
   Card8-Array
   Card8-Vector
   Card8-Matrix

   Card16
   Card16-Array
   Card16-Vector
   Card16-Matrix
   Card16-List

   Card24
   Card24-Array
   Card24-Vector
   Card24-Matrix	    

   Card28
   Card28-Array
   Card28-Vector
   Card28-Matrix	    

   Card32
   Card32-Array
   Card32-Vector
   Card32-Matrix	    

   Int16
   Int16-Array
   Int16-Vector
   Int16-Matrix
   Int16-List
   Int16-Sequence

   array-data
	    
   zero-real-vector
   zero-real-array
   fill-real-array
   make-real-array
   make-real-vector
   make-real-matrix

   zero-card8-vector
   zero-card8-array
   fill-card8-array
   unit-card8-array
   unit-card8-vector
   read-card8-array
   write-card8-array
   make-card8-array
   make-card8-vector
   make-card8-matrix

   zero-card16-vector
   zero-card16-array
   fill-card16-array
   make-card16-array
   make-card16-vector
   make-card16-matrix

   zero-card24-vector
   zero-card24-array
   fill-card24-array
   make-card24-array
   make-card24-vector
   make-card24-matrix

   zero-card28-vector
   zero-card28-array
   fill-card28-array
   make-card28-array
   make-card28-vector
   make-card28-matrix
   ;; a couple specialized functions:
   iota-card28-vector
   integrate-card28-vector
   max-card28-vector

   zero-card32-vector
   zero-card32-array
   fill-card32-array
   make-card32-array

   zero-int16-vector
   zero-int16-array
   fill-int16-array
   make-int16-array
   make-int16-vector
   make-int16-matrix
   linear-mix-int16-vectors
   add-int16-vectors
   sub-int16-vectors
   scale-int16-vector
   negate-int16-vector

   zero-array-index-array
   fill-array-index-array
   make-array-index-array
   make-array-index-vector
   make-array-index-matrix
	    	
   zero-fixnum-vector
   zero-fixnum-array
   fill-fixnum-array
   make-fixnum-array
   make-fixnum-vector
   make-fixnum-matrix
	    
   zero-float-vector
   zero-float-array
   fill-float-array
   make-float-array
   make-float-matrix
   make-float-vector
   borrow-float-vector
   return-float-vector
   random-float-vector
   fill-random-float-vector
   with-borrowed-float-vector
   with-borrowed-float-vectors
   copy-float-array
   transpose-float-matrix
   float-vector-len2
   float-matrix*vector
   float-matrix*matrix
   linear-mix-float-vectors
   add-float-vectors
   sub-float-vectors
   scale-float-vector
   negate-float-vector
   random-float-matrix
   fill-random-float-matrix

   flsq
	    
   copy-vector

   equal-array-dimensions?
   copy-array-contents
   print-array
	    
   if-reentered
   time-body
   cache-value
   with-collection
   collect
   once-only
   day-of-week-name
   month-name
   print-date
   print-system-description
   first-elt
   last-elt
   multiple-value-setf
   printing-random-thing
	    
   lazy-gethash
   Alist-Table
   Table
   make-alist-table
   getalist
   lazy-getalist
   lookup
   lazy-lookup
   remove-entry))

