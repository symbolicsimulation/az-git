;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package :Az -*-
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Az)

;;;============================================================
;;; Counter based implementation:

(declaim (type Fixnum *macro-clicks* *micro-clicks* -micro-click-period-))
(defparameter *macro-clicks* 0)
(defparameter *micro-clicks* 0)
(defconstant -micro-click-period- most-positive-fixnum)

;;;============================================================

(eval-when (compile load eval)
  
(defstruct (Timestamp
	    (:conc-name "")
	    (:constructor %make-timestamp)
	    (:copier %timestamp-copier)
	    (:print-function print-timestamp))

  "Timestamps are used for, among other things, lazy maintainence
of dependencies. Typically, the dependent object will compare
its timestamp to the timestamp of the independent object
and only do updating computation if the timestamp of the
independent object is newer. For such uses, we only need to record
the order of critical events and not the absolute time.
So we implement timestamps with a global counter, rather than
attempting record realtime. This also has the advantage
of eliminating problems caused by the finite and sometimes
coarse resolution of system clocks. Timestamps can record
up to <most-positive-fixnum>^2 without overflow problems,
which should be enough for most applications."

  ;; We use two quantities to get sufficient accuracy, but avoid
  ;; wrap-around problems.

  (timestamp-macro-clicks most-negative-fixnum :type Fixnum)
  (timestamp-micro-clicks most-negative-fixnum :type Fixnum)	 

  ;; At certain times, the timestamp may be invalid.  The purpose of
  ;; {\tt timestamp-lock} is to prevent anyone from reading or writing
  ;; the timestamp while it isn't valid.
  ;; updated.  {\tt nil} corresponds to unlocked; otherwise the value of
  ;; the lock will be some representation for the identity of the
  ;; process that currently owns the object.
	 
  (timestamp-lock nil :type T)

  ;; The {\tt timestamp-lock-level} is intended to help avoid deadlocks.
  ;; See chapter 3 of \cite{Keen88}.

  (timestamp-lock-level 0 :type Number)

  ;; {\tt timestamp-info} is intended to hold information about how the
  ;; timestamped object was changed, to allow the possibilty
  ;; of shortcutting some expensive updating operations.	 

  (timestamp-info nil :type List)))


;;;------------------------------------------------------------------------

(defun %stamp-time! (timestamp)
  (declare (optimize (safety 0) (speed 3))
	   (special *macro-clicks* *micro-clicks*)
	   (type Timestamp timestamp)
	   (type Fixnum *macro-clicks* *micro-clicks*))
  (atomic
   (incf *micro-clicks*)
   (when (>= *micro-clicks* -micro-click-period-)
     (setf *micro-clicks* 0)
     (incf *macro-clicks*))
   (setf (timestamp-macro-clicks timestamp) *macro-clicks*)
   (setf (timestamp-micro-clicks timestamp) *micro-clicks*))
  (values timestamp))
  
(defun stamp-time! (timestamp)

  "Mark the timestamp with the current time (actually increment the
global counter and mark the timestamp with the current count)."

  (declare (type Timestamp timestamp)
	   (:returns timestamp))
  (%stamp-time! timestamp))

;;;------------------------------------------------------------------------

(defun make-timestamp (&key (timestamp-info nil))
  "Constructor function for Timestamps."
  (declare (type List timestamp-info)
	   (:returns (type Timestamp)))
  (let ((timestamp (%make-timestamp :timestamp-info timestamp-info)))
    (%stamp-time! timestamp)))

;;;------------------------------------------------------------------------

(defun print-timestamp (timestamp stream depth)
  "Printer function for Timestamps."
  (declare (type Timestamp timestamp)
	   (type Stream stream)
	   (type Fixnum depth)
	   (ignore depth)
	   (:returns (type String)))
  (format stream "{T ~d ~d}"
	  (timestamp-macro-clicks timestamp)
	  (timestamp-micro-clicks timestamp)))

(declaim (Inline %copy-timestamp))

(defun %copy-timestamp (timestamp result)
  (declare (type Timestamp timestamp result))
  (atomic
   (setf (the Fixnum (timestamp-macro-clicks result))
     (the Fixnum (timestamp-macro-clicks timestamp)))
   (setf (the Fixnum (timestamp-micro-clicks result))
     (the Fixnum (timestamp-micro-clicks timestamp))))
  result)

(defmethod copy ((timestamp Timestamp) &key (result (%make-timestamp)))
  "Copier function for Timestamps."
  (declare (type Timestamp timestamp result)
	   (:returns result))
  (%copy-timestamp timestamp result)
  result)

;;;------------------------------------------------------------------------
;;; Comparing Timestamps
;;;------------------------------------------------------------------------

(declaim (inline %timepstamp= 
		 %timestamp<
		 %timestamp>
		 %timestamp<=
		 %timestamp>=)) 

(defun %timestamps= (timestamp0 timestamp1)
  (declare (type Timestamp timestamp0 timestamp1))
  (atomic
    (and (= (the Fixnum (timestamp-macro-clicks timestamp0))
	    (the Fixnum (timestamp-macro-clicks timestamp1)))
	 (= (the Fixnum (timestamp-micro-clicks timestamp0))
	    (the Fixnum (timestamp-micro-clicks timestamp1))))))

(defun timestamp= (timestamp0 timestamp1)
  "Equality predicate for Timestamps."
  (declare (type Timestamp timestamp0 timestamp1)
	   (:returns (type az:Boolean)))
  (%timestamps= timestamp0 timestamp1))

(defmethod equal? ((timestamp0 Timestamp) (timestamp1 Timestamp))
  "Equality predicate for Timestamps."
  (declare (type Timestamp timestamp0 timestamp1)
	   (:returns (type az:Boolean)))
  (%timestamps= timestamp0 timestamp1))
	  
(defun %timestamp< (timestamp0 timestamp1)
  (declare (optimize (safety 0) (speed 3))
	   (type Timestamp timestamp0 timestamp1))
  (atomic
   (let ((u0 (timestamp-macro-clicks timestamp0))
	 (u1 (timestamp-macro-clicks timestamp1)))
     (declare (type Fixnum u0 u1))
     (or (< u0 u1)
	 (and (= u0 u1)
	      (< (timestamp-micro-clicks timestamp0)
		 (timestamp-micro-clicks timestamp1)))))))

(defun timestamp< (timestamp0 timestamp1)
  "Did <timestamp0> come strictly before <timestamp1>?"
  (declare (type Timestamp timestamp0 timestamp1)
		 (:returns (type az:Boolean)))
  (%timestamp< timestamp0 timestamp1))

(defun %timestamp> (timestamp0 timestamp1)      
  (timestamp< timestamp1 timestamp0)) 

(defun timestamp> (timestamp0 timestamp1)
  "Did <timestamp0> come after <timestamp1>?"
  (declare (type Timestamp timestamp0 timestamp1)
		 (:returns (type az:Boolean)))
  (%timestamp> timestamp0 timestamp1))

(defun %timestamp<= (timestamp0 timestamp1)  
   (not (timestamp< timestamp1 timestamp0)))

(defun timestamp<= (timestamp0 timestamp1)  
  "Did <timestamp0> come before or at the same time as <timestamp1>?"
  (declare (type Timestamp timestamp0 timestamp1)
		 (:returns (type az:Boolean)))
  (%timestamp<= timestamp0 timestamp1))
  
(defun %timestamp>= (timestamp0 timestamp1) 
  (not (timestamp< timestamp0 timestamp1)))

(defun timestamp>= (timestamp0 timestamp1) 
  "Did <timestamp0> come after or at the same time as <timestamp1>?"
  (declare (type Timestamp timestamp0 timestamp1)
		 (:returns (type az:Boolean)))
  (%timestamp>= timestamp0 timestamp1))

