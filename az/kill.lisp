;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Az; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================
 
(in-package :Az)

;;;=======================================================

(defclass Dead-Object (Arizona-Object) ()
  (:documentation
   "The default method for <kill-object> changes the class to
Dead-Object."))
 
;;;=======================================================

(defgeneric kill-object (object)
  #-sbcl (declare (type T object)
		  (:returns (type T object)))
  (:documentation 	 
   "The purpose of killing is to prevent an object from
being used again, and possibly to free up resources associated with
that object.

A typical example is when a window in some non-lisp window system is
destroyed; the lisp object that represented that window in the lisp
environment should be killed to prevent lisp from hanging up the
window system by trying to use a destroyed window.  Other objects
related to the window may want to be killed as well to free resources
no longer needed."))

;;;=======================================================

(defmethod kill-object ((object Null))
  "Don't need to do anything to kill nil."
  (declare (type Null object)
	   (:returns nil))
  nil)

(defmethod kill-object ((object Standard-Object)) 

  "The default method for <kill-object> for CLOS instances is to
change the class of the instance to Dead-Object.  This will cause
undefined method errors the next time a generic function is called on
the dead instance, but generic functions that encounter dead objects
frequently can be given methods to allow them to deal with the
situation more gracefully.  Changing the class to Dead-Object will
permit the data in the slots of the killed instance to become
unreferenced garbage and eventually get reclaimed."

  (declare (type Standard-Object object) 
	   (:returns (type Dead-Object object)))
  (change-class object 'Dead-Object))
