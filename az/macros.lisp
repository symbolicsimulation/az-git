;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Az -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved.
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;=======================================================

(in-package :Az)

;;;============================================================
;;; Collection primitives
;;;============================================================

(defmacro with-collection (&body body)

  "<az:with-collection> returns a list of whatever is collected
within its scope by calls to <az:collect>, in the order that {\sf
az:collect> is called.  This is cleaner than a common idiom for the
same purpose that uses <let>, <push>, and <nreverse>."

  (let ((result (gensym))
	(tail (gensym))
	(collectable (gensym))
	(form (gensym)))
    `(let ((,result nil)
	   (,tail nil))
       (macrolet ((collect (,form)
		    "The FORM is evaluated first so that COLLECT nests
                    properly, i.e., The test to determine if this is the 
                    first value collected should be done after the value
                    itself is generated in case it does collection as well."
		    (list 'let (list (list ',collectable ,form))
			  '(if ,tail
			    (rplacd ,tail (setq ,tail (list ,collectable)))
			    (setq ,result (setq ,tail (list ,collectable))))
			  ',tail)))
	 ,@body
	 ,result))))

;;;============================================================
;;; A macro-writing tool
;;;============================================================

(defmacro once-only (vars &body body)

  "<once-only> assures that the forms given as vars are evaluated in the
proper order, once only. Used in the body of macro definitions."

  ;; Taken from Zeta Lisp.
  (let* ((gensym-var (gensym))
	 (run-time-vars (gensym))
	 (run-time-vals (gensym))
	 (expand-time-val-forms
	  (mapcar #'(lambda (var)
		      `(if (or (symbolp ,var) (constantp ,var))
			   ,var
			 (let ((,gensym-var (gensym)))
			   (push ,gensym-var ,run-time-vars)
			   (push ,var ,run-time-vals)
			   ,gensym-var)))
		  vars)))
    `(,'let* (,run-time-vars
	      ,run-time-vals
	      (wrapped-body
	       (,'let
		   ,(az:with-collection
			(do ((var vars (cdr var))
			     (expand-time-val-form
			      expand-time-val-forms
			      (cdr expand-time-val-form)))
			    ((null var))
			  (collect (list (car var)
					 (car expand-time-val-form)))))
		 ,@body)))
       `(,'let
	    ,(az:with-collection
		 (do ((run-time-var (reverse ,run-time-vars)
				    (cdr run-time-var))
		      (run-time-val (reverse ,run-time-vals)
				    (cdr run-time-val)))
		     ((null run-time-var))
		   (collect (list (car run-time-var) (car run-time-val)))))
	  ,wrapped-body))))

;;;=======================================================

(defmacro while (test &body body)
  `(loop (unless ,test (return))
	 ,@body))

(defmacro until (test &body body)
  `(loop (when ,test (return))
	 ,@body))

(defmacro for (specs &body body)

  ;; this little hack is so that we don't need double parens when
  ;; there's only on iteration variable
  (when (atom (first specs)) (setf specs (list specs)))

  (flet ((stepper (spec) `(,(first spec) ,(second spec) (+ ,(first spec) 1)))
	 (endtest (spec) `(>= ,(first spec) ,(third spec)))
	 (decl (spec) `(type az:Array-Index ,(first spec))))
  
    `(do ,(mapcar #'stepper specs)
	 ,(mapcar #'endtest specs)
       (declare ,@(mapcar #'decl specs))
       ,@body)))

;;;=======================================================

(defmacro without-gc (&body body)
  #+:excl `(let ((excl:*global-gc-behavior* :warn)) ,@body)
  #+:cmu `(sys:without-gcing ,@body)
  #-(or :excl :cmu) `(progn ,@body))

(defmacro large-allocation (&body body)
  #+:excl `(excl:tenuring ,@body)
  #+:cmu `(sys:without-gcing ,@body)
  #-(or :excl :cmu) `(progn ,@body))

(defmacro with-gc-tuned-for-large-allocation (&body body)

  "<with-gc-tuned-for-large-allocation> is supposed to adjust the Lisp
implementation's gc parameters to be temporarily more efficient at
allocating a lot of data with a long lifetime, as in system loading.
For example, the kind of thing one wants to do is turn off scavenging
in a multi-generation gc and arrange for the data that's allocated in
during the extent of <with-gc-tuned-for-large-allocation> to be
immediately tenured."

  #-:excl
  `(progn ,body)

  #+:excl
  `(progn
     (excl:gc t)
     (let ((old-gc-spread (sys:gsgc-parameter :generation-spread))
	   #-:allegro-v4.1 (excl:*record-source-files* nil)
	   (excl:*tenured-bytes-limit* nil))
       (declare (special
		 #-:allegro-v4.1 excl:*record-source-files*
		 excl:*tenured-bytes-limit*))
       (unwind-protect
	   (progn
	     (setf (sys:gsgc-parameter :generation-spread) 1)
	     ,@body)
	 (progn
	   (excl:gc :tenure)
	   (excl:gc :tenure)
	   (excl:gc t)
	   (setf (sys:gsgc-parameter :generation-spread) old-gc-spread))))))

;;;=======================================================================

(defmacro type-check (type &rest args)

  "A <check-type> that takes arguments more like declarations, eg,
(declare (type Integer x y))."

  (let* ((args (remove-duplicates args))
 	 (variables (remove-if #'constantp args))
	 (runtime-checks (mapcar #'(lambda (arg)
				     `(check-type ,arg ,type))
				 variables)))
    ;; test constants at macro expand time:
    (dolist (constant (remove-if-not #'constantp args))
      ;; need to <eval> because <constant> might be a <defconstant>
      ;; rather than a literal constant
      (assert (typep (eval constant) type)))
    (unless (null runtime-checks) `(progn ,@runtime-checks))))

(defmacro declare-check (&rest decls)

  "This macro generates type checking forms and has a syntax like <declare>.
Unfortunately, we can't easily have it also generate the declarations."

  `(progn
     ,@(mapcar #'(lambda (d) `(type-check ,(second d) ,@(cddr d)))
	       ;; ignore non-type declarations
	       (remove-if-not #'(lambda (d) (eq (first d) 'type))
			      decls))))

;;;-------------------------------------------------------

(defun prune-decls (var-list)
  (remove-duplicates (remove-if #'constantp var-list)))

(defun clean-declare (&rest clauses)
  (let ((result ()))
    (dolist (clause clauses)
      (if (eql (first clause) 'type)
	  (let ((type (second clause))
		(vars (prune-decls (cddr clause))))
	    (unless (null vars)
	      (push `(type ,type ,@vars) result)))
	;; else
	(push clause result)))
    `(declare (optimize (safety 1) (space 3) (speed 0))
      ,@(delete-duplicates (nreverse result) :test #'equal))))

;;;============================================================

(defmacro if-reentered (reentered-yes-code reentered-no-code)

  "<if-reentered> provides a very simple way of detecting whether a
piece of code has been reentered or not.  This was inspired by the
desire to have mac window event fns call canvas event fns, without
having to worry that errors in the canvas event fns would cause
unbreakable infinite loops.

Michael Sannella, 1993"

  (let* ((gensym-var (gensym)))
    `(let ()
       (declare (special ,gensym-var))
       (if (and (boundp ',gensym-var) ,gensym-var)
	   ,reentered-yes-code
         (unwind-protect
	     (progn (setq ,gensym-var t)
		    ,reentered-no-code)
           (setq ,gensym-var nil))))))

;;;======================================================

(defmacro time-body (&body body)
  "Returns the ``internal run time'' taken by the execution of <body>.
Note that this means that the value returned by <body> is lost."
  (let ((t0 (gensym)))
    `(let ((,t0 (get-internal-run-time)))
       ,@body
       (/ (- (get-internal-run-time) ,t0)
	  internal-time-units-per-second))))

;;;==================================================================
;;; useful numerical macros
;;;==================================================================

(defmacro fl (x)
  "Coerce a number to a floating point number of the default precision
(usually Double-Float)."
  (if (numberp x) (float x 1.0d0) `(float ,x 1.0d0)))

;;;==================================================================

(defmacro bound (xmin x xmax)
  "Clip <x> to the closed interval [xmin,xmax]."
  `(max ,xmin (min ,x ,xmax)))

;;;==================================================================

(define-modify-macro mulf (&rest terms) * "Like <incf>")
(define-modify-macro divf (&rest terms) / "Like <incf>")
(define-modify-macro maxf (&rest terms) max "Like <incf>")
(define-modify-macro minf (&rest terms) min "Like <incf>")
(define-modify-macro negf () - "Like <incf>")

(defmacro deletef (item place #||&key (test #'eq)||#)
  "Delete in place."
  `(setf ,place (delete ,item ,place #|| :test ,test ||#)))

(defmacro multiple-value-setf (places expression)
  "<multiple-value-setf> is to <multiple-value-setq> as 
<setf> is to <setq>."
  (let (names assignments)
   (dolist (place places)
     (let ((name (gensym)))
     (push name names)
     (push `(setf ,place ,name) assignments)))
   `(multiple-value-bind ,(nreverse names) ,expression
      ,@(nreverse assignments))))

;;;==================================================================
;;; Printing random things in a standard format:
;;;==================================================================

(defun printing-random-thing-internal (thing stream)
  "Used by printing random things. Borrowed from PCL's *-low.lisp files."
  #+:cmu
  (format stream "{~X}" (sys:%primitive c:make-fixnum thing))
  #+:coral
  (prin1 (ccl::%ptr-to-int thing) stream)
  #+(and dec vax common)
  (format stream "~X" (system::%sp-pointer->fixnum thing))
  #+:excl
  (format stream "~X" (excl::pointer-to-fixnum thing))
  #+:gclisp
  (multiple-value-bind (offaddr baseaddr) (sys:%pointer thing)
    (princ baseaddr stream)
    (princ ", " stream)
    (princ offaddr stream))
  #+Genera
  (format stream "~X" (si:%pointer thing))
  #+HP-HPLabs 
  (format stream "~X" (prim:@inf thing))          
  #+IBCL
  (format stream "~X" (si:address thing))                
  #+KCL
  (format stream "~X" (si:address thing))                 
  #+Lucid
  (format stream "~X" (%pointer thing))
  #+TI
  (format stream "~X" (si:%pointer thing))
  #+Xerox
  (let ((*print-base* 8))
    (princ (il:\\hiloc thing) stream)
    (princ "," stream)
    (princ (il:\\loloc thing) stream))
  #-(or :cmu :coral (and dec vax common) :excl Genera HP-HPLabs IBCL KCL
	Lucid TI Xerox)
  (prin1 thing stream)
  )


(defmacro printing-random-thing ((thing s) &body body)
  "Similar to printing-random-object in the lisp machine but much simpler
and machine independent. (Borrowed from PCL)."
  (let ((stream (gensym)))
    `(let ((,stream ,s))
       (format ,stream "#<")
       ,@body
       (format ,stream " ")
       (printing-random-thing-internal ,thing ,stream)
       (format ,stream ">"))))