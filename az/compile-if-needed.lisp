;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: User; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :cl-user)

;;;============================================================
;;; Using strings rather than pathnames to avoid spurious
;;; introduction of upcase file types in symbolics

(defun compile-if-needed (path-string)
  (declare (type String path-string))
  (let ((bin (concatenate 'String path-string
                          #+symbolics ".bin"
                          #+:coral ".fasl"
			  #+:excl ".fasl"))
        (source (concatenate 'String path-string ".lisp"))
        #+:coral
        (*warn-if-redefine-kernel* nil))
    #+:coral
    (declare (special *warn-if-redefine-kernel*))
    #-genera (setf bin (pathname bin))
    #-genera (setf source (pathname source))
    (unless (probe-file source) (error "No file named ~s" source))
    (unless (and (probe-file bin)
		 (< (file-write-date source) (file-write-date bin)))
      #+:coral (format t  "~%Compiling ~s" source)
      (compile-file source :output-file bin))
    (load bin)))

(defun compile-all (directory files)
  (declare (type String directory)
	   (type List files))
  (mapc 
    #'(lambda (f)
	 (compile-if-needed (concatenate 'String directory f)))
    files))

(defun load-all (directory files)
  (declare (type String directory)
	   (type List files))
  (mapc 
    #'(lambda (f)
	 (load (concatenate 'String directory f)))
    files))
