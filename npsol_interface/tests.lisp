;;; -*- Mode:Lisp; Syntax:Common-Lisp; Package: :Npsol -*-

(in-package :Npsol)      

;;;=======================================================

(defun objfn1 (mode n x objf objgrd nstate)
  (declare (optimize (safety 0) (speed 3))
	   (ignore mode n nstate))

  #||
  (print "Inside objfn1")
  (map nil #'print (list mode n x objf objgrd nstate))
  (finish-output)
  ||#

  (setf (aref objf 0)
    (+ (- (* (aref x 1) (aref x 5)))
       (* (aref x 0) (aref x 6))
       (- (* (aref x 2) (aref x 6)))
       (- (* (aref x 4) (aref x 7)))
       (* (aref x 3) (aref x 8))
       (* (aref x 2) (aref x 7))))
  (setf (aref objgrd 0) (aref x 6))
  (setf (aref objgrd 1) (- (aref x 5)))
  (setf (aref objgrd 2) (- (aref x 7) (aref x 6)))
  (setf (aref objgrd 3) (aref x 8))
  (setf (aref objgrd 4) (- (aref x 7)))
  (setf (aref objgrd 5) (- (aref x 1)))
  (setf (aref objgrd 6) (- (aref x 0) (aref x 2)))
  (setf (aref objgrd 7) (- (aref x 2) (aref x 4)))
  (setf (aref objgrd 8) (aref x 3)))

(declaim (inline sq))
(defun sq (x) (declare (optimize (safety 0) (speed 3))) (* x x))

(defun confn1 (mode ncnln n nrowj needc x c cjac nstate)
  (declare (optimize (safety 0) (speed 3))
	   (ignore mode nrowj))

  ;;(print "Inside confn1")
  ;;(print (list mode ncnln n nrowj needc x c cjac nstate))
  ;;(finish-output)

  (let ((n (aref n 0))
	(ncnln (aref ncnln 0))
	(nstate (aref nstate 0)))
    (when (= 1 nstate)
      (dotimes (j n)
	(dotimes (i ncnln)
	  (setf (aref cjac j i) 0.0d0))))

    (when (> (aref needc 0) 0)
      (setf (aref c 0) (+ (sq (aref x 0)) (sq (aref x 5))))
      (setf (aref cjac 0 0) (* 2.0d0 (aref x 0)))
      (setf (aref cjac 5 0) (* 2.0d0 (aref x 5))))
 
    (when (> (aref needc 1) 0)
      (setf (aref c 1) (+ (sq (- (aref x 1) (aref x 0)))
			  (sq (- (aref x 6) (aref x 5)))))
      (setf (aref cjac 0 1) (* -2.0d0 (- (aref x 1) (aref x 0))))
      (setf (aref cjac 1 1) (*  2.0d0 (- (aref x 1) (aref x 0))))
      (setf (aref cjac 5 1) (* -2.0d0 (- (aref x 6) (aref x 5))))
      (setf (aref cjac 6 1) (*  2.0d0 (- (aref x 6) (aref x 5)))))

    (when (> (aref needc 2) 0)
      (setf (aref c 2) (+ (sq (- (aref x 2) (aref x 0)))
			  (sq (aref x 5))))
      (setf (aref cjac 0 2) (* -2.0d0 (- (aref x 2) (aref x 0))))
      (setf (aref cjac 2 2) (*  2.0d0 (- (aref x 2) (aref x 0))))
      (setf (aref cjac 5 2) (*  2.0d0 (aref x 5))))

    (when (> (aref needc 3) 0)
      (setf (aref c 3) (+ (sq (- (aref x 0) (aref x 3)))
			  (sq (- (aref x 5) (aref x 7)))))
      (setf (aref cjac 0 3) (*  2.0d0 (- (aref x 0) (aref x 3))))
      (setf (aref cjac 3 3) (* -2.0d0 (- (aref x 0) (aref x 3))))
      (setf (aref cjac 5 3) (*  2.0d0 (- (aref x 5) (aref x 7))))
      (setf (aref cjac 7 3) (* -2.0d0 (- (aref x 5) (aref x 7)))))

    (when (> (aref needc 4) 0)
      (setf (aref c 4) (+ (sq (- (aref x 0) (aref x 4)))
			  (sq (- (aref x 5) (aref x 8)))))
      (setf (aref cjac 0 4) (*  2.0d0 (- (aref x 0) (aref x 4))))
      (setf (aref cjac 4 4) (* -2.0d0 (- (aref x 0) (aref x 4))))
      (setf (aref cjac 5 4) (*  2.0d0 (- (aref x 5) (aref x 8))))
      (setf (aref cjac 8 4) (* -2.0d0 (- (aref x 5) (aref x 8)))))

    (when (> (aref needc 5) 0)
      (setf (aref c 5) (+ (sq (aref x 1)) (sq (aref x 6))))
      (setf (aref cjac 1 5) (*  2.0d0 (aref x 1)))
      (setf (aref cjac 6 5) (*  2.0d0 (aref x 6))))

    (when (> (aref needc 6) 0)
      (setf (aref c 6) (+ (sq (- (aref x 2) (aref x 1)))
			  (sq (aref x 6))))
      (setf (aref cjac 1 6) (* -2.0d0 (- (aref x 2) (aref x 1))))
      (setf (aref cjac 2 6) (*  2.0d0 (- (aref x 2) (aref x 1))))
      (setf (aref cjac 6 6) (*  2.0d0 (aref x 6))))

    (when (> (aref needc 7) 0)
      (setf (aref c 7) (+ (sq (- (aref x 3) (aref x 1)))
			  (sq (- (aref x 7) (aref x 6)))))
      (setf (aref cjac 1 7) (* -2.0d0 (- (aref x 3) (aref x 1))))
      (setf (aref cjac 3 7) (*  2.0d0 (- (aref x 3) (aref x 1))))
      (setf (aref cjac 6 7) (* -2.0d0 (- (aref x 7) (aref x 6))))
      (setf (aref cjac 7 7) (*  2.0d0 (- (aref x 7) (aref x 6)))))

    (when (> (aref needc 8) 0)
      (setf (aref c 8) (+ (sq (- (aref x 1) (aref x 4)))
			  (sq (- (aref x 6) (aref x 8)))))
      (setf (aref cjac 1 8) (*  2.0d0 (- (aref x 1) (aref x 4))))
      (setf (aref cjac 4 8) (* -2.0d0 (- (aref x 1) (aref x 4))))
      (setf (aref cjac 6 8) (*  2.0d0 (- (aref x 6) (aref x 8))))
      (setf (aref cjac 8 8) (* -2.0d0 (- (aref x 6) (aref x 8)))))

    (when (> (aref needc 9) 0)
      (setf (aref c 9) (+ (sq (- (aref x 3) (aref x 2)))
			  (sq (aref x 7))))
      (setf (aref cjac 2 9) (* -2.0d0 (- (aref x 3) (aref x 2))))
      (setf (aref cjac 3 9) (*  2.0d0 (- (aref x 3) (aref x 2))))
      (setf (aref cjac 7 9) (*  2.0d0 (aref x 7))))

    (when (> (aref needc 10) 0)
      (setf (aref c 10) (+ (sq (- (aref x 4) (aref x 2)))
			   (sq (aref x 8))))
      (setf (aref cjac 2 10) (* -2.0d0 (- (aref x 4) (aref x 2))))
      (setf (aref cjac 4 10) (*  2.0d0 (- (aref x 4) (aref x 2))))
      (setf (aref cjac 8 10) (*  2.0d0 (aref x 8))))

    (when (> (aref needc 11) 0)
      (setf (aref c 11) (+ (sq (aref x 3)) (sq (aref x 7))))
      (setf (aref cjac 3 11) (*  2.0d0 (aref x 3)))
      (setf (aref cjac 7 11) (*  2.0d0 (aref x 7))))

    (when (> (aref needc 12) 0)
      (setf (aref c 12) (+ (sq (- (aref x 3) (aref x 4)))
			   (sq (- (aref x 8) (aref x 7)))))
      (setf (aref cjac 3 12) (*  2.0d0 (- (aref x 3) (aref x 4))))
      (setf (aref cjac 4 12) (* -2.0d0 (- (aref x 3) (aref x 4))))
      (setf (aref cjac 7 12) (* -2.0d0 (- (aref x 8) (aref x 7))))
      (setf (aref cjac 8 12) (*  2.0d0 (- (aref x 8) (aref x 7)))))

    (when (> (aref needc 13) 0)
      (setf (aref c 13) (+ (sq (aref x 4)) (sq (aref x 8))))
      (setf (aref cjac 4 13) (*  2.0d0 (aref x 4)))
      (setf (aref cjac 8 13) (*  2.0d0 (aref x 8))))))

;;;*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;;*     FILE NPMAIN FORTRAN
;;;*
;;;*     Sample program for NPSOL Version 4.02  August 1986.
;;;*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defconstant zero 0.0d0) ;; PARAMETER (ZERO = 0.0, ONE = 1.0)
(defconstant one 1.0d0)
(defconstant -one -1.0d0)
(defconstant bigbnd 1.0d15) ;; BIGBND = 1.0D+15
(defconstant -bigbnd -1.0d15)

(defun tn ()
  (declare (optimize (safety 0) (speed 3)))
  (let* ( ;; *     Set the declared array dimensions.
	 ;; *     NROWA  = the declared row dimension of  A.
	 ;; *     NROWJ  = the declared row dimension of  CJAC.
	 ;; *     NROWR  = the declared row dimension of  R.
	 ;; *     MAXN   = maximum no. of variables allowed for.
	 ;; *     MAXBND = maximum no. of variables + linear & nonlinear constrnts.
	 ;; *     LIWORK = the length of the integer work array.
	 ;; *     LWORK  = the length of the double precision work array.
	 ;;       PARAMETER         (NROWA  =  5, NROWJ  =  20, NROWR =   10,
	 ;;      $                   MAXN   =  9, LIWORK =  70, LWORK = 1000,
	 ;;      $                   MAXBND =  MAXN + NROWA + NROWJ)
	 (nrowa 5) (nrowj 20) (nrowr 10) (maxn 9) (liwork 70) (lwork 1000)
	 (maxbnd (+ maxn nrowa nrowj))
	 (istate (make-fixnum-array maxbnd)) ;; INTEGER ISTATE(MAXBND)
	 (iwork (make-fixnum-array liwork)) ;; INTEGER IWORK(LIWORK)
	 ;; DOUBLE PRECISION A(NROWA,MAXN)
	 (a (make-double-array (list maxn nrowa) :initial-element 0.0d0))
	 ;; DOUBLE PRECISION BL(MAXBND), BU(MAXBND)
	 (bl (make-double-array maxbnd :initial-element -bigbnd))
	 (bu (make-double-array maxbnd :initial-element bigbnd))
	 ;; DOUBLE PRECISION C(NROWJ), CJAC(NROWJ,MAXN), CLAMDA(MAXBND)
	 (c (make-double-array nrowj))
	 (cjac (make-double-array (list maxn nrowj)))
	 (clamda (make-double-array maxbnd))
	 ;; DOUBLE PRECISION OBJGRD(MAXN), R(NROWR,MAXN), X(MAXN)
	 (objgrd (make-double-array maxn))
	 (r (make-double-array (list maxn nrowr)))
	 (x (make-double-array maxn))
	 ;; DOUBLE PRECISION WORK(LWORK)
	 (work (make-double-array lwork))
	 ;; *     Set the actual problem dimensions.
	 ;; *     N      = the number of variables.
	 ;; *     NCLIN  = the number of general linear constraints (may be 0).
	 ;; *     NCNLN  = the number of nonlinear constraints (may be 0).
	 (n 9) ;; N = 9
	 (nclin 4) ;; NCLIN  = 4
	 (ncnln 14) ;; NCNLN  = 14
	 (nbnd (+ n nclin ncnln)) ;; NBND   = N + NCLIN + NCNLN
	 ;; *     ------------------------------------------------------------------
	 ;; *     Assign file numbers and the data arrays.
	 ;; *     NOUT   = the unit number for printing.
	 ;; *     IOPTNS = the unit number for reading the options file.
	 ;; *     Bounds  .ge.    BIGBND  will be treated as plus  infinity.
	 ;; *     Bounds  .le.  - BIGBND  will be treated as minus infinity.
	 ;; *     A      = the linear constraint matrix.
	 ;; *     BL     = the lower bounds on  x,  a'x  and  c(x).
	 ;; *     BU     = the upper bounds on  x,  a'x  and  c(x).
	 ;; *     X      = the initial estimate of the solution.
	 ;; *     ------------------------------------------------------------------
	 ;; (nout 6) ;; NOUT   = 6
	 (ioptns "../npsol/npmain.dat")) ;; IOPTNS = 5

    (print (cons a (sys:pointer-storage-type a)))
    ;; *     Set the matrix  A.
    ;; DO 40 J = 1, N
    ;;    DO 30 I = 1, NCLIN
    ;;       A(I,J) = ZERO
    ;;    30    CONTINUE
    ;;    40 CONTINUE
    ;; Taken care by :initial-element above
    (setf (aref a 0 0) -one) ;; A(1,1) = -ONE
    (setf (aref a 1 0) one) ;; A(1,2) =  ONE
    (setf (aref a 1 1) -one) ;; A(2,2) = -ONE
    (setf (aref a 2 1) one) ;; A(2,3) =  ONE
    (setf (aref a 2 2) one) ;; A(3,3) =  ONE
    (setf (aref a 3 2) -one) ;; A(3,4) = -ONE
    (setf (aref a 3 3) one) ;; A(4,4) =  ONE
    (setf (aref a 4 3) -one) ;; A(4,5) = -ONE
    (print-array a)
    (finish-output)

    ;; *     Set the bounds.
    ;; DO 50 J  =  1, NBND
    ;;    BL(J) = -BIGBND
    ;;    BU(J) =  BIGBND
    ;;    50 CONTINUE
    ;; taken care of by :initial-element above
    (setf (aref bl 0) zero) ;; BL(1)  =  ZERO
    (setf (aref bl 2) -one) ;; BL(3)  = -ONE
    (setf (aref bl 4) zero) ;; BL(5)  =  ZERO
    (setf (aref bl 5) zero) ;; BL(6)  =  ZERO
    (setf (aref bl 6) zero) ;; BL(7)  =  ZERO 
    (setf (aref bu 2) one) ;; BU(3)  =  ONE
    (setf (aref bu 7) zero) ;; BU(8)  =  ZERO
    (setf (aref bu 8) zero) ;; BU(9)  =  ZERO

    ;; *     Set lower bounds of zero for all four linear constraints.
    ;; DO 60 J  =  N+1,  N+NCLIN
    ;;    BL(J) =  ZERO
    ;;    60 CONTINUE
    (do ((i n (+ i 1)))
	((>= i (+ n nclin)))
      (setf (aref bl i) zero))
    
    ;; *     Set upper bounds of one for all 14 nonlinear constraints.
    ;; DO 70 J  =  N + NCLIN + 1,  NBND
    ;;    BU(J) =  ONE
    ;;    70 CONTINUE
    (do ((i (+ n nclin) (+ i 1)))
	((>= i nbnd))
      (setf (aref bu i) one))

    ;; * Set the initial estimate of  X.
    (setf (aref x 0)  0.100000d0) ;; X(1)   =  .1
    (setf (aref x 1)  0.125000d0) ;; X(2)   =  .125
    (setf (aref x 2)  0.666666d0) ;; X(3)   =  .666666
    (setf (aref x 3)  0.142857d0) ;; X(4)   =  .142857
    (setf (aref x 4)  0.111111d0) ;; X(5)   =  .111111
    (setf (aref x 5)  0.200000d0) ;; X(6)   =  .2
    (setf (aref x 6)  0.250000d0) ;; X(7)   =  .25
    (setf (aref x 7) -0.200000d0) ;; X(8)   = -.2
    (setf (aref x 8) -0.250000d0) ;; X(9)   = -.25

    ;; *     ------------------------------------------------------------------
    ;; *     Read the options file.
    ;; *     ------------------------------------------------------------------
    ;; CALL NPFILE( IOPTNS, INFORM )
    ;; IF (INFORM .NE. 0) THEN
    ;;    WRITE (NOUT, 3000) INFORM
    ;;    STOP
    ;; END IF
    (read-npsol-option-file ioptns)
    ;;(fflush nout)

    ;; 
    ;; *     ------------------------------------------------------------------
    ;; *     Solve the problem.
    ;; *     ------------------------------------------------------------------
    ;; 
    ;; CALL NPSOL ( N, NCLIN, NCNLN, NROWA, NROWJ, NROWR,
    ;;      $             A, BL, BU,
    ;;      $             CONFN1, OBJFN1,
    ;;      $             INFORM, ITER, ISTATE,
    ;;      $             C, CJAC, CLAMDA, OBJF, OBJGRD, R, X,
    ;;      $             IWORK, LIWORK, WORK, LWORK )
    ;; 

    (let ((objf (make-double-array 1))
	  (a-n (make-fixnum-array 1 :initial-element n))
	  (a-nclin (make-fixnum-array 1 :initial-element nclin))
	  (a-ncnln (make-fixnum-array 1 :initial-element ncnln))
	  (a-nrowa (make-fixnum-array 1 :initial-element nrowa))
	  (a-nrowj (make-fixnum-array 1 :initial-element nrowj))
	  (a-nrowr (make-fixnum-array 1 :initial-element nrowr))
	  (a-inform (make-fixnum-array 1 :initial-element nrowr))
	  (a-iter (make-fixnum-array 1 :initial-element nrowr))
	  (a-leniw (make-fixnum-array 1 :initial-element liwork))
	  (a-lenw (make-fixnum-array 1 :initial-element lwork)))
      (print-array x)
      (finish-output)
      (lisp-npsol (gensym)
	       a-n a-nclin a-ncnln a-nrowa a-nrowj a-nrowr
	       a bl bu
	       #'confn1 #'objfn1
	       a-inform a-iter istate
	       c cjac clamda objf objgrd r x
	       iwork a-leniw work a-lenw)
      ) 

    ;; IF (INFORM .GT. 0) GO TO 900
    ;; 
    ;; *     ------------------------------------------------------------------
    ;; *     The following is for illustrative purposes only.
    ;; *     A second run solves the same problem,  but defines the objective
    ;; *     and constraints via the subroutines OBJFN2 and CONFN2.  Some
    ;; *     objective derivatives and the constant Jacobian elements are not
    ;; *     supplied.
    ;; *     We do a warm start using
    ;; *              ISTATE    (the working set)
    ;; *              CLAMDA    (the Lagrange multipliers)
    ;; *              R         (the Hessian approximation)
    ;; *     from the previous run, but with a slightly perturbed starting
    ;; *     point.  The previous option file must have specified
    ;; *              Hessian    Yes
    ;; *     for R to be a useful approximation.
    ;; *     ------------------------------------------------------------------
    ;; 
    ;; DO 100 J = 1, N
    ;;    X(J)  = X(J) + 0.1
    ;;   100 CONTINUE
    ;; 
    ;; *     The previous parameters are retained and updated.
    ;; 
    ;; CALL NPOPTN( '   Derivative level               0')
    ;; CALL NPOPTN( '   Verify                        No')
    ;; CALL NPOPTN( '   Warm Start')
    ;; CALL NPOPTN( '   Major iterations              20')
    ;; 
    ;; CALL NPOPTN( '   Major print level             10')
    ;; 
    ;; CALL NPSOL ( N, NCLIN, NCNLN, NROWA, NROWJ, NROWR,
    ;;      $             A, BL, BU,
    ;;      $             CONFN2, OBJFN2,
    ;;      $             INFORM, ITER, ISTATE,
    ;;      $             C, CJAC, CLAMDA, OBJF, OBJGRD, R, X,
    ;;      $             IWORK, LIWORK, WORK, LWORK )
    ;; 
    ;; IF (INFORM .GT. 0) GO TO 900
    ;; STOP
    ;; 
    ;; *     -----------
    ;; *     Error exit.
    ;; *     -----------
    ;; 
    ;;   900 WRITE (NOUT, 3010) INFORM
    ;; STOP
    ;; 
    ;;  3000 FORMAT(/ ' NPFILE terminated with  INFORM =', I3)
    ;;  3010 FORMAT(/ ' NPSOL  terminated with  INFORM =', I3)
    ;; 
    ;; *     End of the example program for NPSOL.
    ;; 
    ;;  END
    ;; *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ))

(defun tn-1 ()
  (declare (optimize (safety 0) (speed 3)))
  (let* ( ;; *     Set the declared array dimensions.
	 ;; *     NROWA  = the declared row dimension of  A.
	 ;; *     NROWJ  = the declared row dimension of  CJAC.
	 ;; *     NROWR  = the declared row dimension of  R.
	 ;; *     MAXN   = maximum no. of variables allowed for.
	 ;; *     MAXBND = maximum no. of variables + linear & nonlinear constrnts.
	 ;; *     LIWORK = the length of the integer work array.
	 ;; *     LWORK  = the length of the double precision work array.
	 ;;       PARAMETER         (NROWA  =  5, NROWJ  =  20, NROWR =   10,
	 ;;      $                   MAXN   =  9, LIWORK =  70, LWORK = 1000,
	 ;;      $                   MAXBND =  MAXN + NROWA + NROWJ)
	 (nrowa 5) (nrowj 20) (maxn 9)
	 (maxbnd (+ maxn nrowa nrowj))
	 (a (make-double-array (list maxn nrowa) :initial-element 0.0d0))
	 (bl (make-double-array maxbnd :initial-element -bigbnd))
	 (bu (make-double-array maxbnd :initial-element bigbnd))
	 (x (make-double-array maxn))
	 ;; *     Set the actual problem dimensions.
	 ;; *     N      = the number of variables.
	 ;; *     NCLIN  = the number of general linear constraints (may be 0).
	 ;; *     NCNLN  = the number of nonlinear constraints (may be 0).
	 (n 9) ;; N = 9
	 (nclin 4) ;; NCLIN  = 4
	 (ncnln 14) ;; NCNLN  = 14
	 (nbnd (+ n nclin ncnln)) ;; NBND   = N + NCLIN + NCNLN
	 ;; *     ------------------------------------------------------------------
	 ;; *     Assign file numbers and the data arrays.
	 ;; *     NOUT   = the unit number for printing.
	 ;; *     IOPTNS = the unit number for reading the options file.
	 ;; *     Bounds  .ge.    BIGBND  will be treated as plus  infinity.
	 ;; *     Bounds  .le.  - BIGBND  will be treated as minus infinity.
	 ;; *     A      = the linear constraint matrix.
	 ;; *     BL     = the lower bounds on  x,  a'x  and  c(x).
	 ;; *     BU     = the upper bounds on  x,  a'x  and  c(x).
	 ;; *     X      = the initial estimate of the solution.
	 ;; *     ---------------------------------------------------------
	 (nout 6) ;; NOUT   = 6
	 (ioptns "/belgica-2g/jam/az/npsol/npsol/npmain.dat")) ;; IOPTNS = 5

    ;; *     Set the matrix  A.
    ;; DO 40 J = 1, N
    ;;    DO 30 I = 1, NCLIN
    ;;       A(I,J) = ZERO
    ;;    30    CONTINUE
    ;;    40 CONTINUE
    ;; Taken care by :initial-element above
    (setf (aref a 0 0) -one) ;; A(1,1) = -ONE
    (setf (aref a 1 0) one) ;; A(1,2) =  ONE
    (setf (aref a 1 1) -one) ;; A(2,2) = -ONE
    (setf (aref a 2 1) one) ;; A(2,3) =  ONE
    (setf (aref a 2 2) one) ;; A(3,3) =  ONE
    (setf (aref a 3 2) -one) ;; A(3,4) = -ONE
    (setf (aref a 3 3) one) ;; A(4,4) =  ONE
    (setf (aref a 4 3) -one) ;; A(4,5) = -ONE
    (print-array a)
    (finish-output)

    ;; *     Set the bounds.
    ;; DO 50 J  =  1, NBND
    ;;    BL(J) = -BIGBND
    ;;    BU(J) =  BIGBND
    ;;    50 CONTINUE
    ;; taken care of by :initial-element above
    (setf (aref bl 0) zero) ;; BL(1)  =  ZERO
    (setf (aref bl 2) -one) ;; BL(3)  = -ONE
    (setf (aref bl 4) zero) ;; BL(5)  =  ZERO
    (setf (aref bl 5) zero) ;; BL(6)  =  ZERO
    (setf (aref bl 6) zero) ;; BL(7)  =  ZERO 
    (setf (aref bu 2) one) ;; BU(3)  =  ONE
    (setf (aref bu 7) zero) ;; BU(8)  =  ZERO
    (setf (aref bu 8) zero) ;; BU(9)  =  ZERO

    ;; *     Set lower bounds of zero for all four linear constraints.
    ;; DO 60 J  =  N+1,  N+NCLIN
    ;;    BL(J) =  ZERO
    ;;    60 CONTINUE
    (do ((i n (+ i 1)))
	((>= i (+ n nclin)))
      (setf (aref bl i) zero))
    
    ;; *     Set upper bounds of one for all 14 nonlinear constraints.
    ;; DO 70 J  =  N + NCLIN + 1,  NBND
    ;;    BU(J) =  ONE
    ;;    70 CONTINUE
    (do ((i (+ n nclin) (+ i 1)))
	((>= i nbnd))
      (setf (aref bu i) one))

    ;; * Set the iniial estimate of  X.
    (setf (aref x 0)  0.100000d0) ;; X(1)   =  .1
    (setf (aref x 1)  0.125000d0) ;; X(2)   =  .125
    (setf (aref x 2)  0.666666d0) ;; X(3)   =  .666666
    (setf (aref x 3)  0.142857d0) ;; X(4)   =  .142857
    (setf (aref x 4)  0.111111d0) ;; X(5)   =  .111111
    (setf (aref x 5)  0.200000d0) ;; X(6)   =  .2
    (setf (aref x 6)  0.250000d0) ;; X(7)   =  .25
    (setf (aref x 7) -0.200000d0) ;; X(8)   = -.2
    (setf (aref x 8) -0.250000d0) ;; X(9)   = -.25

    ;; *     ------------------------------------------------------------------
    ;; *     Read the options file.
    ;; *     ------------------------------------------------------------------
    ;; CALL NPFILE( IOPTNS, INFORM )
    ;; IF (INFORM .NE. 0) THEN
    ;;    WRITE (NOUT, 3000) INFORM
    ;;    STOP
    ;; END IF
    (read-npsol-option-file ioptns)
    (fflush nout)

    ;; 
    ;; *     ------------------------------------------------------------------
    ;; *     Solve the problem.
    ;; *     ------------------------------------------------------------------
    ;; 
    ;; CALL NPSOL ( N, NCLIN, NCNLN, NROWA, NROWJ, NROWR,
    ;;      $             A, BL, BU,
    ;;      $             CONFN1, OBJFN1,
    ;;      $             INFORM, ITER, ISTATE,
    ;;      $             C, CJAC, CLAMDA, OBJF, OBJGRD, R, X,
    ;;      $             IWORK, LIWORK, WORK, LWORK )
    ;; 

    (print-array x)
    (finish-output)
    (npsol-minimize-1 () #'objfn1 #'confn1 n nclin ncnln x a bl bu)

    ;; IF (INFORM .GT. 0) GO TO 900
    ;; 
    ;; *     ------------------------------------------------------------------
    ;; *     The following is for illustrative purposes only.
    ;; *     A second run solves the same problem,  but defines the objective
    ;; *     and constraints via the subroutines OBJFN2 and CONFN2.  Some
    ;; *     objective derivatives and the constant Jacobian elements are not
    ;; *     supplied.
    ;; *     We do a warm start using
    ;; *              ISTATE    (the working set)
    ;; *              CLAMDA    (the Lagrange multipliers)
    ;; *              R         (the Hessian approximation)
    ;; *     from the previous run, but with a slightly perturbed starting
    ;; *     point.  The previous optiona file must have specified
    ;; *              Hessian    Yes
    ;; *     for R to be a useful approximation.
    ;; *     ------------------------------------------------------------------
    ;; 
    ;; DO 100 J = 1, N
    ;;    X(J)  = X(J) + 0.1
    ;;   100 CONTINUE
    ;; 
    ;; *     The previous parameters are retained and updated.
    ;; 
    ;; CALL NPOPTN( '   Derivative level               0')
    ;; CALL NPOPTN( '   Verify                        No')
    ;; CALL NPOPTN( '   Warm Start')
    ;; CALL NPOPTN( '   Major iterations              20')
    ;; 
    ;; CALL NPOPTN( '   Major print level             10')
    ;; 
    ;; CALL NPSOL ( N, NCLIN, NCNLN, NROWA, NROWJ, NROWR,
    ;;      $             A, BL, BU,
    ;;      $             CONFN2, OBJFN2,
    ;;      $             INFORM, ITER, ISTATE,
    ;;      $             C, CJAC, CLAMDA, OBJF, OBJGRD, R, X,
    ;;      $             IWORK, LIWORK, WORK, LWORK )
    ;; 
    ;; IF (INFORM .GT. 0) GO TO 900
    ;; STOP
    ;; 
    ;; *     -----------
    ;; *     Error exit.
    ;; *     -----------
    ;; 
    ;;   900 WRITE (NOUT, 3010) INFORM
    ;; STOP
    ;; 
    ;;  3000 FORMAT(/ ' NPFILE terminated with  INFORM =', I3)
    ;;  3010 FORMAT(/ ' NPSOL  terminated with  INFORM =', I3)
    ;; 
    ;; *     End of the example program for NPSOL.
    ;; 
    ;;  END
    ;; *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ))