;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Npsol; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Npsol)

;;;=======================================================

#|| assuming binaries for npsol and interface already loaded
(defparameter *this-dir*
    (namestring
     (make-pathname :directory (pathname-directory *load-truename*))))

(defparameter *npsol-object-dir*
    (concatenate 'String
      user::*az-dir* "npsol/" user::*arch* "/"))

(defparameter *npsol-interface-dir*
    (concatenate 'String *this-dir* user::*arch* "/"))


#+(and :excl :fortran :sun4)
(load ""
      :foreign-files
      (list
       (concatenate 'String *npsol-object-dir* "dpmach.o")
       (concatenate 'String *npsol-object-dir* "dplscode.o")
       (concatenate 'String *npsol-object-dir* "dpnpcode.o")
       (concatenate 'String *npsol-interface-dir* "c-npsol.o")
       (concatenate 'String *npsol-interface-dir* "option.o")
       (concatenate 'String *npsol-interface-dir* "missing.o")
       #+:sun4 "/usr/local/lang/SC1.0/libF77.a"
       #+:sun4 "/usr/local/lang/SC1.0/libm.a"
       )
      ;;:system-libraries '("F77" "m")
      )
||#

#+:excl
(ff:defforeign-list
    (list 
     #+:sun4
     (list 'f_init
	   :entry-point  "_f_init"
	   :arguments nil
	   :print t
	   :call-direct t
	   :callback nil
	  )

     #+:sun4
     (list 'fflush
	   :arguments '(Fixnum)
	   :language :Fortran
	   :entry-point (ff:convert-to-lang "flush" :language :fortran)
	   :call-direct t
	   :callback nil
	   :print t
	   :arg-checking t)

     (list 'option
	   :entry-point (ff:convert-to-lang "option" :language :fortran)
	   :language :fortran
	   :arguments '(Simple-String)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil
	   :return-type :void)

     (list 'c_npsol
	   :entry-point (ff:convert-to-lang "c_npsol" :language :C)
	   :language :C
	   :arguments
	   '((az:Fixnum-Vector 1) ;; N
	     (az:Fixnum-Vector 1) ;; NCLIN
	     (az:Fixnum-Vector 1) ;; NCNLN
	     (az:Fixnum-Vector 1) ;; NROWA
	     (az:Fixnum-Vector 1) ;; NROWJ
	     (az:Fixnum-Vector 1) ;; NROWR
	     az:Float-Matrix ;; A
	     az:Float-Vector ;; BL
	     az:Float-Vector ;; BU
	     (az:Fixnum-Vector 1) ;; INFORM
	     (az:Fixnum-Vector 1) ;; ITER
	     az:Fixnum-Vector ;; ISTATE
	     az:Float-Vector ;; C
	     az:Float-Matrix ;; CJAC
	     az:Float-Vector ;; CLAMDA
	     (az:Float-Vector 1) ;; OBJF
	     az:Float-Vector ;; OBJGRD
	     az:Float-Matrix ;; R
	     az:Float-Vector ;; X
	     az:Fixnum-Vector ;; IW
	     (az:Fixnum-Vector 1) ;; LENIW
	     az:Float-Vector ;; W
	     (az:Fixnum-Vector 1) ;; LENW
	     Fixnum ;; *objfn-index*
	     Fixnum ;; *confn-index*
	     Fixnum ;; Integer ;; *objfn-ptr*
	     Fixnum ;; Integer ;; *confn-ptr*
	     (az:Fixnum-Vector 1) ;; *mode*
	     (az:Fixnum-Vector 1) ;; *n*
	     az:Float-Vector ;; *x*
	     (az:Float-Vector 1) ;; *objf* 
	     az:Float-Vector ;; *objgrd*
	     (az:Fixnum-Vector 1) ;; *nstate*
	     (az:Fixnum-Vector 1) ;; *ncnln*
	     (az:Fixnum-Vector 1) ;; *nrowj*
	     az:Fixnum-Vector ;; *needc*
	     az:Float-Vector ;; *c*
	     az:Float-Matrix ;; *cjac*
	     )
	   :call-direct t
	   :callback t
	   :print t
	   :arg-checking t
	   :return-type :void)))

