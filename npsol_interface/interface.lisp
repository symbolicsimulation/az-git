;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Npsol; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Npsol)

;;;=======================================================

(defun sample-objective-function (mode n x objf objgrd nstate)
  
  "A sample Lisp objective function, to document the arglist, etc.
See the NPSOL Guide, section 4.1, for more details."
  
  (declare (type (az:Fixnum-Vector 1) mode n nstate)
	   (type az:Float-Vector x objgrd)
	   (type (az:Float-Vector 1) objf))
  
  (format t "~%Lisp objective function:")
  (map nil #'print (list mode n x objf objgrd nstate))
  (finish-output *standard-output*))

(defun sample-constraint-function (mode ncnln n nrowj needc x c cjac nstate)
  
   "A sample Lisp constraint function, to document the arglist, etc.
See the NPSOL Guide, section 4.2, for more details."
   
  (declare (type (az:Fixnum-Vector 1) mode ncnln n nrowj nstate)
	   (type az:Fixnum-Vector needc)
	   (type az:Float-Vector x c)
	   (type az:Float-Matrix cjac))
  
  (format t "~%Lisp constraint function:")
  (map nil #'print (list mode ncnln n nrowj needc x c cjac nstate))
  (finish-output *standard-output*))

;;;=======================================================
;;; global variables for data shared between lisp and c/fortran

(defparameter *npsol-objfn* #'sample-objective-function)

(defparameter *cb-mode* (make-fixnum-array 1))

(defparameter *cb-n* (make-fixnum-array 1))

(defparameter *cb-x* (make-double-array 1))
(defun allocate-cb-x (n)
  (setf *cb-x* (ensure-large-enough-vector *cb-x* n)))

(defparameter *cb-objf* (make-double-array 1))

(defparameter *cb-objgrd* (make-double-array 1))
(defun allocate-cb-objgrd (n)
  (setf *cb-objgrd* (ensure-large-enough-vector *cb-objgrd* n)))

(defparameter *cb-nstate* (make-fixnum-array 1))

(ff:defun-c-callable lisp-objfn ()
  (declare (Special
	    *npsol-objfn*
	    *cb-mode* *cb-n* *cb-x* *cb-objf* *cb-objgrd* *cb-nstate*))
  (format t "~%Lisp wrapper for true objective function:")
  (print 
   (list *npsol-objfn* 
		 *cb-mode* *cb-n* *cb-x* *cb-objf* *cb-objgrd* *cb-nstate*))
  (finish-output)
  (unless (all-tenured? 
	   (list *npsol-objfn* 
		 *cb-mode* *cb-n* *cb-x* *cb-objf* *cb-objgrd* *cb-nstate*))
    (break))
  (funcall *npsol-objfn*
	   *cb-mode* *cb-n* *cb-x* *cb-objf* *cb-objgrd* *cb-nstate*))

(defvar *objfn-index* nil)
(defvar *objfn-ptr* nil)
(defvar *objfn-old-ptr* nil)
(multiple-value-setq (*objfn-ptr* *objfn-index* *objfn-old-ptr*)
  (ff:register-function #'lisp-objfn))

;;;-------------------------------------------------------

(defparameter *npsol-confn* #'sample-constraint-function)

(defparameter *cb-ncnln* (make-fixnum-array 1))
(defparameter *cb-nrowj* (make-fixnum-array 1))

(defparameter *cb-needc* (make-fixnum-array 1))
(defun allocate-cb-needc (n)
  (setf *cb-needc* (ensure-large-enough-fixnum-vector *cb-needc* n)))

(defparameter *cb-c* (make-double-array 1))
(defun allocate-cb-c (n) (setf *cb-c* (ensure-large-enough-vector *cb-c* n)))


(defparameter *cb-cjac* (make-double-array '(1 1)))
(defun allocate-cb-cjac (n m)
  (setf *cb-cjac* (ensure-large-enough-mtx *cb-cjac* (max 1 n) (max 1 m))))

(ff:defun-c-callable lisp-confn ()
 (declare (Special
	   *npsol-confn*
	   *cb-mode* *cb-ncnln* *cb-n* *cb-nrowj* *cb-needc*
	   *cb-x* *cb-c* *cb-cjac* *cb-nstate*))
 ;;(format t "~%Lisp wrapper for true constraint function:")
 ;;(finish-output)
 (funcall *npsol-confn*
	  *cb-mode* *cb-ncnln* *cb-n* *cb-nrowj* *cb-needc*
	  *cb-x* *cb-c* *cb-cjac* *cb-nstate*))

(defvar *confn-index* nil)
(defvar *confn-ptr* nil)
(defvar *confn-old-ptr* nil)
(multiple-value-setq (*confn-ptr* *confn-index* *confn-old-ptr*)
  (ff:register-function #'lisp-confn))

;;;=======================================================

(defparameter *option-arg-string* (make-string 128 :initial-element #\Space))

(defun set-npsol-option (s)
  "<set-npsol-option> can be used to set options for future calls to
NPSOL. <s> must be a string containing one of the legal NPSOL options
as described in the NPSOL User's Guide, section 5."
  (declare (type String s)
		 (:returns t))
   ;; 128 is the length of the string arg declared in OPTION
  (assert (< (length s) 127))
  (fill *option-arg-string* #\Space)
  (setf (subseq *option-arg-string* 0 (length s)) s)
  ;; to be safe set the first char after the option string to be
  ;; #\*, which indicates the beginning of a comment to NPOPTN.
  ;;(setf (aref *option-arg-string* (+ 1 (length s))) #\*)
  (option *option-arg-string*)
  t)

(defun set-npsol-options (ls)
  (dolist (s ls) (set-npsol-option s)))

(defun set-standard-npsol-options ()
  (set-npsol-options '("Print Level = 1"
		       "Verify Level = 3"
		       "Derivative Level = 0"
		       "Linesearch Tolerance  = 0.01"
		       "Major Iteration Limit = 20"
		       "Optimality Tolerance = 1.0e-6"
		       "Infinite Bound = 1.0d15")))

(defun read-npsol-option-file (path)
  (let ((line ""))
    (with-open-file (str path :direction :input)
      ;; find the BEGIN line
      (loop (setf line (read-line str))
	    (when (string-equal 'begin (read-from-string line nil nil))
	      (return)))
      ;; read the options until the END line
      (loop (setf line (read-line str))
	    (when (string-equal 'end (read-from-string line nil nil))
	      (return))
	    (set-npsol-option line)))))

;;;=======================================================

(defun length-of-float-workspace (n nclin ncnln)
  (if (= 0 ncnln)
      (if (= 0 nclin)
	  (* 20 n)
	  ;; else
	  (+ (* 2 n n)
	     (* 20 n)
	     (* 11 nclin)))
      ;; else
      (+ (* 2 n n)
	 (* n nclin)
	 (* 2 n ncnln)
	 (* 20 n)
	 (* 11 nclin)
	 (* 21 ncnln))))

(defun length-of-fixnum-workspace (n nclin ncnln)
  (+ (* 3 n) nclin (* 2 ncnln)))

;;;=======================================================
;;; Check the arguments before venturing off into the wilderness

(defun npsol-check-args (n nclin ncnln nrowa nrowj nrowr
			 a bl bu
			 confn objfn 
			 inform iter istate
			 c cjac clamda objf objgrd r x
			 iw leniw w lenw
			 cb-mode cb-n cb-x
			 cb-objf cb-objgrd
			 cb-nstate
			 cb-ncnln cb-nrowj
			 cb-needc cb-c cb-cjac)
  (declare (type (az:Fixnum-Vector 1)
		 n nclin ncnln nrowa nrowj nrowr inform iter leniw lenw
		 cb-mode cb-n cb-nstate
		 cb-ncnln cb-nrowj)
	   (type az:Fixnum-Vector istate iw cb-needc)
	   (type (az:Float-Vector 1) objf cb-objf)
	   (type az:Float-Vector
		 bl bu c clamda objgrd x cb-x cb-objgrd cb-c)
	   (type az:Float-Matrix a cjac r cb-cjac)
	   (type Function confn)
	   (type Function objfn)
	   (ignore cb-nrowj cb-ncnln cb-nstate cb-objf cb-n cb-mode objf
		   iter inform objfn confn))

  (setf (aref nrowa 0) (array-dimension a 1))
  (setf (aref nrowj 0) (array-dimension cjac 1))
  (setf (aref nrowr 0) (array-dimension r 1))
  (let* ((nn (aref n 0))
	 (nnclin (aref nclin 0))
	 (nncnln (aref ncnln 0))
	 (nnrowa (aref nrowa 0))
	 (nnrowj (aref nrowj 0))
	 (nnrowr (aref nrowr 0))
	 (nleniw (aref leniw 0))
	 (nlenw (aref lenw 0))
	 (n+nclin+ncnln (+ nn nnclin nncnln)))
    (assert (>= nleniw (length-of-fixnum-workspace nn nnclin nncnln)))
    (assert (>= nlenw (length-of-float-workspace nn nnclin nncnln)))
    (assert (<= nn (array-dimension a 0)))
    (assert (<= nnclin nnrowa))
    (assert (= nnrowa (array-dimension a 1)))
    ;;(assert (<= nn (array-dimension cjac 0)))
    ;;(assert (<= nn (array-dimension cb-cjac 0)))
    (assert (<= nncnln nnrowj))
    (assert (>= nnrowj (array-dimension cjac 1)))
    (assert (>= nnrowj (array-dimension cb-cjac 1)))
    (assert (<= nn (array-dimension r 0)))
    (assert (<= nn nnrowr))
    ;;(assert (= nnrowr (array-dimension r 1)))
    (assert (<= nn (length x))) 
    (assert (<= nn (length cb-x)))
    (assert (<= nn (length objgrd))) 
    (assert (<= nn (length cb-objgrd)))
    (assert (<= nncnln (length cb-needc)))
    (assert (<= nncnln (length c)))
    (assert (<= nncnln (length cb-c)))
    (assert (<= n+nclin+ncnln (length bl)))
    (assert (<= n+nclin+ncnln (length bu))) 
    (assert (<= n+nclin+ncnln (length clamda))) 
    (assert (<= n+nclin+ncnln (length istate)))
    (assert (<= nleniw (length iw)))
    (assert (<= nlenw (length w)))))
  
;;;=======================================================

(defun lisp-npsol (lock-name
		   n nclin ncnln nrowa nrowj nrowr
		   a bl bu
		   confn objfn 
		   inform iter istate
		   c cjac clamda objf objgrd r x
		   iw leniw w lenw)

  "<lisp-npsol> is a basic function that more or less directly calls
NPSOL.  For details of the meaning of the arguments see the NPSOL
User's Guide.  I have retained the Fortran argument names so that the
correspondence should be obvious.

All the arguments to <lisp-npsol> are Simple-Arrays, where NPSOL would
take a scalar argument, <lisp-npsol> takes a Simple-Array of rank 1
and length 1.

When setting up two-dimensional arrays to pass to <lisp-npsol>, keep
in mind the fact that Fortran stores multidimensional arrays in column
major order while Common Lisp stores in row major order.  My interface
does not do any transposing (because of space and time costs) so it is
up to the calling functions to create, for example, the constraint
matrix as the transpose of the way it is described in the NPSOL User's
Guide.

<lisp-npsol> has one argument that does not correspond to any argument
to NPSOL: <lock-name>.  The reason for <lock-name> is to permit NPSOL
to be used safely in multiple Lisp processes.  My interface for
callbacks from Fortran to Lisp objective and constraint functions is
unfortunately implemented in a way that would cause simulataneous
calls to <lisp-npsol> to interfere.  In other words, only one call to
<lisp-npsol> can be active at any given time.  <lock-name> must be a
symbol that is unique to a particular process calling <lisp-npsol>; an
easy way to do this is to put a call to <gensym> in each call to
<lisp-npsol>."
  
  (declare (optimize (safety 0) (speed 3))
	   (type Symbol lock-name)
	   (type (az:Fixnum-Vector 1)
		 n nclin ncnln nrowa nrowj nrowr
		 inform iter leniw lenw)
	   (type az:Fixnum-Vector istate iw)
	   (type az:Float-Matrix a cjac r)
	   (type az:Float-Vector
		 bl bu c clamda objgrd x w)
	   (type (az:Float-Vector 1) objf)
	   (type az:Funcallable confn objfn)
	   (:returns (values x objf iter istate)))
  (with-npsol-locked (lock-name)
    (setf *cb-x* (allocate-cb-x (length x)))
    (setf *cb-objgrd* (allocate-cb-objgrd (length x)))
    (setf *cb-needc* (allocate-cb-needc (aref ncnln 0)))
    (setf *cb-c* (allocate-cb-c (length c)))
    (setf *cb-cjac* (apply #'allocate-cb-cjac (array-dimensions cjac)))

    (npsol-check-args n nclin ncnln nrowa nrowj nrowr
		      a bl bu
		      confn objfn 
		      inform iter istate
		      c cjac clamda objf objgrd r x
		      iw leniw w lenw
		      *cb-mode* *cb-n*
		      *cb-x*
		      *cb-objf*
		      *cb-objgrd*
		      *cb-nstate*
		      *cb-ncnln* *cb-nrowj*
		      *cb-needc* *cb-c*
		      *cb-cjac*)
    (excl:without-interrupts
	(let ((excl:*global-gc-behavior* :warn))

	  ;; retain pointers to space used to pass arguments back to
	  ;; lisp objective function
	  (setf *npsol-confn* confn)
	  (setf *npsol-objfn* objfn)

	  (tenure-all! 
	   (list
	    n nclin ncnln nrowa nrowj nrowr
	    a bl bu
	    inform iter istate
	    c cjac clamda objf objgrd r x
	    iw leniw w lenw
	    *confn-index* *objfn-index*
	    *confn-ptr* *objfn-ptr*
	    *cb-mode* *cb-n* *cb-x* *cb-objf* *cb-objgrd* *cb-nstate*
	    *cb-ncnln* *cb-nrowj* *cb-needc* *cb-c* *cb-cjac*))


	  #+:sun4 (f_init)

	  #||
	  (print confn)
	  (print objfn)
	  (print-array x)
	  (finish-output)
	  ||#

	  (c_npsol n nclin ncnln nrowa nrowj nrowr
		   a bl bu
		   inform iter istate
		   c cjac clamda objf objgrd r x
		   iw leniw w lenw
		   *confn-index* *objfn-index*
		   *confn-ptr* *objfn-ptr*
		   *cb-mode* *cb-n* *cb-x* *cb-objf* *cb-objgrd* *cb-nstate*
		   *cb-ncnln* *cb-nrowj* *cb-needc* *cb-c* *cb-cjac*)
	  ;; (fflush 6)
	  (values x objf iter istate)))))

;;;=======================================================

(defparameter *n* (make-fixnum-array 1))
(defun allocate-n (k)
  (setf (aref *n* 0) k)
  *n*)

(defparameter *nclin* (make-fixnum-array 1))
(defun allocate-nclin (k)
  (setf (aref *nclin* 0) k)
  *nclin*)

(defparameter *ncnln* (make-fixnum-array 1))
(defun allocate-ncnln (k)
  (setf (aref *ncnln* 0) k)
  *ncnln*)

(defparameter *nrowa* (make-fixnum-array 1))
(defun allocate-nrowa (k)
  (setf (aref *nrowa* 0) k)
  *nrowa*)

(defparameter *nrowj* (make-fixnum-array 1))
(defun allocate-nrowj (k)
  (setf (aref *nrowj* 0) k)
  *nrowj*)

(defparameter *nrowr* (make-fixnum-array 1))
(defun allocate-nrowr (k)
  (setf (aref *nrowr* 0) k)
  *nrowr*)

(defparameter *inform* (make-fixnum-array 1))
(defun allocate-inform (k)
  (setf (aref *inform* 0) k)
  *inform*)

(defparameter *iter* (make-fixnum-array 1))
(defun allocate-iter (k)
  (setf (aref *iter* 0) k)
  *iter*)

(defparameter *istate* (make-fixnum-array 1))
(defun allocate-istate (k)
  (setf *istate* (fill (ensure-large-enough-fixnum-vector *istate* k) 0)))

(defparameter *c* (make-double-array 1))
(defun allocate-c (k)
  (setf *c* (fill (ensure-large-enough-vector *c* k) 0.0d0)))

(defparameter *cjac* (make-double-array '(1 1)))
(defun allocate-cjac (n m)
  (setf *cjac* (zero-mtx
		(ensure-large-enough-mtx *cjac* (max 1 n) (max 1 m)))))

(defparameter *clamda* (make-double-array 1))
(defun allocate-clamda (k)
  (setf *clamda* (fill (ensure-large-enough-vector *clamda* k) 0.0d0)))

(defparameter *objf* (make-double-array 1))
(defun allocate-objf ()
  (setf (aref *objf* 0) most-positive-double-float)
  *objf*)

(defparameter *objgrd* (make-double-array 1))
(defun allocate-objgrd (k)
  (setf *objgrd* (fill (ensure-large-enough-vector *objgrd* k) 0.0d0)))

(defparameter *r* (make-double-array '(0 0)))
(defun allocate-r (n m)
  (setf *r* (zero-mtx (ensure-large-enough-mtx *r* n m))))

(defparameter *leniw* (make-fixnum-array 1))
(defun allocate-leniw (k)
  (setf (aref *leniw* 0) k)
  *leniw*)

(defparameter *iw* (make-fixnum-array 1))
(defun allocate-iw (k)
  (setf *iw* (fill (ensure-large-enough-fixnum-vector *iw* k) 0)))

(defparameter *lenw* (make-fixnum-array 1))
(defun allocate-lenw (k)
  (setf (aref *lenw* 0) k)
  *lenw*)

(defparameter *w* (make-double-array 1))
(defun allocate-w (k)
  (setf *w* (fill (ensure-large-enough-vector *w* k) 0.0d0)))

;;;------------------------------------------------------------

(defun npsol-minimize (options objfn confn
		       n-variables n-linear-constraints n-nonlinear-constraints
		       x a bl bu)

  "For most users, <npsol-minimize> is the preferred way to call
NPSOL.  

<npsol-minimize> has a minimal argument list.  Other arguments
required by NPSOL (eg. working space arrays) are allocated as needed,
with some attempt to reuse arrays allocated in previous calls to
<npsol-minimize>.

When setting up two-dimensional arrays to pass to <npsol-minimize>,
keep in mind the fact that Fortran stores multidimensional arrays in
column major order while Common Lisp stores in row major order.

<npsol-minimize> is safe under multiprocessing.

<npsol-minimize> takes an <options> argument (a list of strings) which
determines the setting of NPSOL options (eg. Print Level).  NPSOL
options can be set directly with calls to <set-npsol-option> before
calls to <npsol-minimize> or <lisp-npsol>.

The value of <objfn> must be either a Lisp function or a symbol with
appropriately defined function value.  <sample-objective-function> is
an example provided to document the expected calling sequence.  See
the NPSOL User's Guide for more details of the expected behavior of
the objective function.

The value of <confn> must be either a Lisp function or a symbol with
appropriately defined function value.  <sample-constraint-function> is
an example provided to document the expected calling sequence.  See
the NPSOL User's Guide for more details of the expected behavior of
the constraint function.

Input variables:

options -- a list of NPSOL option strings

objfn -- lisp objective function

confn -- lisp nonlinear constraint function

n-variables -- number of variables

n-linear-constraints -- number of linear (affine) constraints 

n-nonlinear-constraints -- number of nonlinear constraints

x -- starting point, current position

a -- linear constraint array, columns are constraint vectors

bl -- lower bounds for both variables and constraints

bu -- upper bounds for both variables and constraints

A more sophisticated interface might also take:

istate -- initial state of constraints

c -- final values of nonlinear constraint functions

cjac -- jacobian of nonlinear constraints, nil if no
        nonlinear constraints

clamda -- QP multipliers 

objgrd -- the gradient

r -- cholesky triangle

iw -- integer workspace

w -- float workspace

Output:

x -- the final position

objf -- the final value

objgrd -- the final value of the gradient

inform -- Code for termination state

iter -- number of iterations
"
  (declare (optimize (safety 0) (speed 3))
	   (type List options)
	   (type az:Funcallable objfn confn )
	   (type (Integer 1 #.array-dimension-limit) n-variables)
	   (type (Integer 0 #.array-dimension-limit)
		 n-linear-constraints n-nonlinear-constraints)
	   (type az:Float-Vector x bl bu)
	   (type az:Float-Matrix a))
  (declare (type List options)
		 (type az:Funcallable objfn confn )
		 (type (Integer 1 #.array-dimension-limit) n-variables)
		 (type (Integer 0 #.array-dimension-limit)
		       n-linear-constraints n-nonlinear-constraints)
		 (type az:Float-Vector x bl bu)
		 (type az:Float-Matrix a))

  (let* ((lock-name (gensym))
	 ;;(n-variables (length x))
	 ;;(n-linear-constraints (array-dimension a 1))
	 (n+nclin+ncnln
	  (+ n-variables n-linear-constraints n-nonlinear-constraints))
	 (nleniw (length-of-fixnum-workspace
		  n-variables n-linear-constraints n-nonlinear-constraints))
	 (nlenw (length-of-float-workspace
		 n-variables n-linear-constraints n-nonlinear-constraints)))
    (with-npsol-locked (lock-name)
      (set-npsol-options options)
      (let ((n (allocate-n n-variables))
	    (nclin (allocate-nclin n-linear-constraints))
	    (ncnln (allocate-ncnln n-nonlinear-constraints))
	    (nrowa (allocate-nrowa n-linear-constraints))
	    (nrowj (allocate-nrowj n-nonlinear-constraints))
	    (nrowr (allocate-nrowr n-variables))
	    (inform (allocate-inform 0))
	    (iter (allocate-iter 0))
	    (leniw (allocate-leniw nleniw))
	    (lenw (allocate-lenw nlenw))
	    (istate (allocate-istate n+nclin+ncnln))
	    (c (allocate-c (max 1 n-nonlinear-constraints)))
	    (cjac (allocate-cjac n-variables (max 1 n-nonlinear-constraints)))
	    (clamda (allocate-clamda n+nclin+ncnln))
	    (objgrd (allocate-objgrd n-variables))
	    (r (allocate-r n-variables n-variables))
	    (iw (allocate-iw nleniw))
	    (w (allocate-w nlenw))
	    (objf (allocate-objf)))
	(lisp-npsol lock-name n nclin ncnln nrowa nrowj nrowr
		    a bl bu confn objfn inform iter istate
		    c cjac clamda objf objgrd r x iw leniw w lenw)
	(values x (aref objf 0) objgrd (aref inform 0) (aref iter 0))))))



   
