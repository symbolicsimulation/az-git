\documentstyle[code,twoside]{article}
\pagestyle{headings}

\topmargin=-0.2in
\textwidth=6.0in
\textheight=8.0in
\oddsidemargin=0.25in
\evensidemargin=0.25in


\begin{document}

\title{Calling NPSOL from Common Lisp}

\author{{\sc John Alan McDonald}
\thanks{This work was supported in part 
the Dept. of Energy under contract FG0685-ER25006
and by NIH grant LM04174 from the National
Library of Medicine.}
\\
        Depts. of Statistics, Computer Science and Engineering, \\
        University of Washington\\
         }
\date{October, 1991}

\maketitle

\begin{abstract}
This report describes an interface for calling
the Fortran optimization package NPSOL 
using the foreign function interface of 
Franz Allegro Common Lisp.
\end{abstract}

\section{Overview}

NPSOL 
\footnote{NPSOL is available for license 
through the Office of Technology Licensing,
Stanford University, Stanford CA 94305.}
\cite{Gill86a,Gill86b,Murt78,Murt82,Murt87} 
is a collection of Fortran subroutines 
designed for the solution of nonlinear programming problems
\cite{Flet89,Gill81,Luen69,Luen84}.

This report describes an interface for calling NPSOL,
using the foreign function interface
provided with Franz Allegro Common Lisp \cite{Fran91a},
to minimize an objective function written in Common Lisp,
subject to constraints that are also represented by a Common Lisp function.
This report assumes the reader is familiar 
with nonlinear programming,
Fortran, 
common conventions in Fortran scientific subroutines 
(the Linpack manual \cite{Dong79} is well written and provides examples
of good Fortran coding style),
NPSOL \cite{Gill86b},
and
Common Lisp \cite{Stee90}.

For most users, {\sf npsol-minimize} is the preferred way to call NPSOL.
However, at present, {\sf npsol-minimize} does not support nonlinear
constraints, so {\sf lisp-npsol} is necessary for more complicated problems.

{\sf npsol-minimize} has a minimal argument list of natural Lisp arguments
(ie. scalar arguments like {\sf n} are passed as {\sf Fixnums},
rather than as {\sf (Simple-Array Fixnum (1))} as in {\sf lisp-npsol} below).
Other arguments required by NPSOL (eg. working space arrays)
are allocated as needed, with some attempt to reuse
arrays allocated in previous calls to {\sf npsol-minimize}.

When setting up two-dimensional arrays to pass to 
{\sf npsol-minimize} or {\sf lisp-npsol},
keep in mind the fact that Fortran stores multidimensional
arrays in column major order while
Common Lisp stores in row major order.
My interface does not do any transposing 
(because of space and time costs)
so it is up to the calling functions to create,
for example, the constraint matrix as the transpose
of the way it is described in the NPSOL User's Guide.

{\sf npsol-minimize} is safe under multiprocessing.

{\sf npsol-minimize} takes an {\sf options} argument which determines
the setting of NPSOL options (eg. Print Level).
NPSOL options can be set directly with calls to {\sf set-npsol-option}
before calls to {\sf npsol-minimize} or {\sf lisp-npsol}.

{\sf npsol-minimize} takes one argument whose value
must be either a Lisp function or a symbol with an appropriately defined
function value.
{\sf sample-objective-function}
is an example provided to document the expected calling sequence.
{\sf sample-constraint-function}
is an example of a nonlinear constraint function
(for {\sf lisp-npsol}).
See the NPSOL User's Guide 
for more details of the expected behavior of the objective
and constraint functions.

{\sf lisp-npsol} is a basic function that more or less directly calls NPSOL.
For details of the meaning of the arguments
see the NPSOL User's Guide \cite{Gill86b};
I have retained the Fortran argument names 
so that the correspondence should be obvious.

All the arguments to {\sf lisp-npsol} are Simple-Arrays,
where NPSOL would take a scalar argument,
{\sf lisp-npsol} takes a Simple-Array of rank 1 and length 1.

{\sf lisp-npsol} has one argument that does not correspond to any argument
to NPSOL: {\sf lock-name}.
The reason for {\sf lock-name} is to permit NPSOL
to be used safely in multiple Lisp processes.
My interface for callbacks from Fortran
to Lisp objective and constraint functions
is unfortunately implemented in a way that would cause
simulataneous calls to {\sf lisp-npsol} to interfere.
In other words, only one call to {\sf lisp-npsol} can be active
at any given time.
{\sf lock-name} must be a symbol 
that is unique to a particular process calling {\sf lisp-npsol};
an easy way to do this is to put a call to {\sf gensym} 
in each call to {\sf lisp-npsol}.

\vfill
\pagebreak

\section{Reference Manual}
\label{reference-manual}
\pagestyle{myheadings}
\input{reference-manual}

\vfill
\pagebreak

\pagestyle{headings}
\bibliographystyle{plain} 

\bibliography{../../b/arizona,../../b/cs,../../b/cactus,../../b/constraint,../../b/database,../../b/layout,../../b/lisp,../../b/image,../../b/buja,../../b/mcdonald,../../b/friedman,../../b/wxs}


\end{document}