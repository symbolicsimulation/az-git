

\noindent\framebox[6in]{np:lisp-npsol\hfill {\sf Function}}
\markboth{np:lisp-npsol}{np:lisp-npsol}

\noindent{\bf Documentation:}

{\tt<}lisp-npsol{\tt>} is a basic function that more or less directly calls
NPSOL.  For details of the meaning of the arguments see the NPSOL
User's Guide.  I have retained the Fortran argument names so that the
correspondence should be obvious.

All the arguments to {\tt<}lisp-npsol{\tt>} are Simple-Arrays, where NPSOL would
take a scalar argument, {\tt<}lisp-npsol{\tt>} takes a Simple-Array of rank 1
and length 1.

When setting up two-dimensional arrays to pass to {\tt<}lisp-npsol{\tt>}, keep
in mind the fact that Fortran stores multidimensional arrays in column
major order while Common Lisp stores in row major order.  My interface
does not do any transposing (because of space and time costs) so it is
up to the calling functions to create, for example, the constraint
matrix as the transpose of the way it is described in the NPSOL User's
Guide.

{\tt<}lisp-npsol{\tt>} has one argument that does not correspond to any argument
to NPSOL: {\tt<}lock-name{\tt>}.  The reason for {\tt<}lock-name{\tt>} is to permit NPSOL
to be used safely in multiple Lisp processes.  My interface for
callbacks from Fortran to Lisp objective and constraint functions is
unfortunately implemented in a way that would cause simulataneous
calls to {\tt<}lisp-npsol{\tt>} to interfere.  In other words, only one call to
{\tt<}lisp-npsol{\tt>} can be active at any given time.  {\tt<}lock-name{\tt>} must be a
symbol that is unique to a particular process calling {\tt<}lisp-npsol{\tt>}; an
easy way to do this is to put a call to {\tt<}gensym{\tt>} in each call to
{\tt<}lisp-npsol{\tt>}.
\vskip 1em

\noindent{\bf Usage:} 

{\sf (np:lisp-npsol lock-name n nclin ncnln nrowa nrowj nrowr a bl bu confn objfn inform iter istate c cjac clamda objf objgrd r x iw leniw w lenw)}
\vskip 1em

\noindent{\bf Arguments:}
\begin{code}{\sf
lock-name --- Symbol
n --- (Simple-Array Fixnum (1))
nclin --- (Simple-Array Fixnum (1))
ncnln --- (Simple-Array Fixnum (1))
nrowa --- (Simple-Array Fixnum (1))
nrowj --- (Simple-Array Fixnum (1))
nrowr --- (Simple-Array Fixnum (1))
a --- (Simple-Array Double-Float (* *))
bl --- (Simple-Array Double-Float (*))
bu --- (Simple-Array Double-Float (*))
confn --- (Or Symbol Function)
objfn --- (Or Symbol Function)
inform --- (Simple-Array Fixnum (1))
iter --- (Simple-Array Fixnum (1))
istate --- (Simple-Array Fixnum (*))
c --- (Simple-Array Double-Float (*))
cjac --- (Simple-Array Double-Float (* *))
clamda --- (Simple-Array Double-Float (*))
objf --- (Simple-Array Double-Float (1))
objgrd --- (Simple-Array Double-Float (*))
r --- (Simple-Array Double-Float (* *))
x --- (Simple-Array Double-Float (*))
iw --- (Simple-Array Fixnum (*))
leniw --- (Simple-Array Fixnum (1))
w --- (Simple-Array Double-Float (*))
lenw --- (Simple-Array Fixnum (1))
}\end{code}

\noindent{\bf Returns:}
\begin{code}{\rm
\cd{\sf x}
\cd{\sf objf}
\cd{\sf iter}
\cd{\sf istate}
}\end{code}

\noindent{\bf Source:} /belgica-2g/jam/az/npsol/interface.lisp
\vskip 1em

\noindent\framebox[6in]{:Npsol\hfill {\sf Package}}
\markboth{:Npsol}{:Npsol}

\noindent{\bf Usage:} 

{\sf (in-package :Npsol)}
\vskip 1em

\noindent{\bf Source:} /belgica-2g/jam/az/npsol/package.lisp
\vskip 1em

\noindent\framebox[6in]{np:npsol-minimize\hfill {\sf Function}}
\markboth{np:npsol-minimize}{np:npsol-minimize}

\noindent{\bf Documentation:}

For most users, {\tt<}npsol-minimize{\tt>} is the preferred way to call
NPSOL.  

{\tt<}npsol-minimize{\tt>} has a minimal argument list.  Other arguments
required by NPSOL (eg. working space arrays) are allocated as needed,
with some attempt to reuse arrays allocated in previous calls to
{\tt<}npsol-minimize{\tt>}.

When setting up two-dimensional arrays to pass to {\tt<}npsol-minimize{\tt>},
keep in mind the fact that Fortran stores multidimensional arrays in
column major order while Common Lisp stores in row major order.

{\tt<}npsol-minimize{\tt>} is safe under multiprocessing.

{\tt<}npsol-minimize{\tt>} takes an {\tt<}options{\tt>} argument (a list of strings) which
determines the setting of NPSOL options (eg. Print Level).  NPSOL
options can be set directly with calls to {\tt<}set-npsol-option{\tt>} before
calls to {\tt<}npsol-minimize{\tt>} or {\tt<}lisp-npsol{\tt>}.

The value of {\tt<}objfn{\tt>} must be either a Lisp function or a symbol with
appropriately defined function value.  {\tt<}sample-objective-function{\tt>} is
an example provided to document the expected calling sequence.  See
the NPSOL User's Guide for more details of the expected behavior of
the objective function.

The value of {\tt<}confn{\tt>} must be either a Lisp function or a symbol with
appropriately defined function value.  {\tt<}sample-constraint-function{\tt>} is
an example provided to document the expected calling sequence.  See
the NPSOL User's Guide for more details of the expected behavior of
the constraint function.

Input variables:

options -- a list of NPSOL option strings

objfn -- lisp objective function

confn -- lisp nonlinear constraint function

n-variables -- number of variables

n-linear-constraints -- number of linear (affine) constraints 

n-nonlinear-constraints -- number of nonlinear constraints

x -- starting point, current position

a -- linear constraint array, columns are constraint vectors

bl -- lower bounds for both variables and constraints

bu -- upper bounds for both variables and constraints

A more sophisticated interface might also take:

istate -- initial state of constraints

c -- final values of nonlinear constraint functions

cjac -- jacobian of nonlinear constraints, nil if no
        nonlinear constraints

clamda -- QP multipliers 

objgrd -- the gradient

r -- cholesky triangle

iw -- integer workspace

w -- float workspace

Output:

x -- the final position

objf -- the final value

objgrd -- the final value of the gradient

inform -- Code for termination state

iter -- number of iterations

\vskip 1em

\noindent{\bf Usage:} 

{\sf (np:npsol-minimize options objfn confn n-variables n-linear-constraints n-nonlinear-constraints x a bl bu)}
\vskip 1em

\noindent{\bf Arguments:}
\begin{code}{\sf
options --- List
objfn --- (Or Symbol Function)
confn --- (Or Symbol Function)
n-variables --- (Integer 1 16777216)
n-linear-constraints --- (Integer 0 16777216)
n-nonlinear-constraints --- (Integer 0 16777216)
x --- (Simple-Array Double-Float (*))
a --- (Simple-Array Double-Float (* *))
bl --- (Simple-Array Double-Float (*))
bu --- (Simple-Array Double-Float (*))
}\end{code}

\noindent{\bf Source:} /belgica-2g/jam/az/npsol/interface.lisp
\vskip 1em

\noindent\framebox[6in]{np:sample-constraint-function\hfill {\sf Function}}
\markboth{np:sample-constraint-function}{np:sample-constraint-function}

\noindent{\bf Documentation:}

A sample Lisp constraint function, to document the arglist, etc.
See the NPSOL Guide, section 4.2, for more details.
\vskip 1em

\noindent{\bf Usage:} 

{\sf (np:sample-constraint-function mode ncnln n nrowj needc x c cjac nstate)}
\vskip 1em

\noindent{\bf Arguments:}
\begin{code}{\sf
mode --- (Simple-Array Fixnum (1))
ncnln --- (Simple-Array Fixnum (1))
n --- (Simple-Array Fixnum (1))
nrowj --- (Simple-Array Fixnum (1))
needc --- (Simple-Array Fixnum (*))
x --- (Simple-Array Double-Float (*))
c --- (Simple-Array Double-Float (*))
cjac --- (Simple-Array Double-Float (* *))
nstate --- (Simple-Array Fixnum (1))
}\end{code}

\noindent{\bf Source:} /belgica-2g/jam/az/npsol/interface.lisp
\vskip 1em

\noindent\framebox[6in]{np:sample-objective-function\hfill {\sf Function}}
\markboth{np:sample-objective-function}{np:sample-objective-function}

\noindent{\bf Documentation:}

A sample Lisp objective function, to document the arglist, etc.
See the NPSOL Guide, section 4.1, for more details.
\vskip 1em

\noindent{\bf Usage:} 

{\sf (np:sample-objective-function mode n x objf objgrd nstate)}
\vskip 1em

\noindent{\bf Arguments:}
\begin{code}{\sf
mode --- (Simple-Array Fixnum (1))
n --- (Simple-Array Fixnum (1))
x --- (Simple-Array Double-Float (*))
objf --- (Simple-Array Double-Float (1))
objgrd --- (Simple-Array Double-Float (*))
nstate --- (Simple-Array Fixnum (1))
}\end{code}

\noindent{\bf Source:} /belgica-2g/jam/az/npsol/interface.lisp
\vskip 1em

\noindent\framebox[6in]{np:set-npsol-option\hfill {\sf Function}}
\markboth{np:set-npsol-option}{np:set-npsol-option}

\noindent{\bf Documentation:}

{\tt<}set-npsol-option{\tt>} can be used to set options for future calls to
NPSOL. {\tt<}s{\tt>} must be a string containing one of the legal NPSOL options
as described in the NPSOL User's Guide, section 5.
\vskip 1em

\noindent{\bf Usage:} 

{\sf (np:set-npsol-option s)}
\vskip 1em

\noindent{\bf Arguments:}
\begin{code}{\sf
s --- String
}\end{code}

\noindent{\bf Returns:}
\begin{code}{\rm
\cd{\sf t}
}\end{code}

\noindent{\bf Source:} /belgica-2g/jam/az/npsol/interface.lisp
\vskip 1em