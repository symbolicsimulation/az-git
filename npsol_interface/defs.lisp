r;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Npsol; -*-
;;;
;;; Copyright 1993. John Alan McDonald and Michael Sannella.
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Npsol)

;;;=======================================================

(declaim (declaration :returns))

;;;=============================================================

(defun print-array (a)
  (let ((*print-pretty* t)
	(*print-array* t))
    (declare (Special *print-pretty* *print-pretty*))
    (print a)))

(defun make-fixnum-array (dimensions &key (initial-element 0))
  (make-array  ;; excl:make-static-array
   dimensions
   :initial-element initial-element
   :element-type 'Fixnum))

(defun make-double-array (dimensions &key (initial-element 0.0d0))
  (make-array ;; excl:make-static-array
   dimensions
   :initial-element initial-element
   :element-type 'Double-Float))

(defun ensure-large-enough-fixnum-vector (v n)
  (declare (type az:Fixnum-Vector v)
	   (type az:Array-Index n))
  (unless (>= (array-dimension v 0) n) (setf v (make-fixnum-array n)))
  v)

(defun ensure-large-enough-vector (v n)
  (declare (type az:Float-Vector v)
	   (type az:Array-Index n))
  (unless (>= (array-dimension v 0) n) (setf v (make-double-array n)))
  v)

(defun ensure-large-enough-mtx (m nrows ncols)
  (declare (type az:Float-Matrix m)
	   (type az:Array-Index nrows ncols))
  (unless (and (>= (array-dimension m 0) nrows)
	       (>= (array-dimension m 1) ncols))
    (setf m (make-double-array (list nrows ncols))))
  m)

(defun zero-mtx (mtx)
  (declare (optimize (safety 0) (speed 3))
	   (type az:Float-Matrix mtx))
  (let ((ncols (array-dimension mtx 1)))
    (declare (type az:Array-Index ncols))
    (dotimes (i (array-dimension mtx 0))
      (declare (type az:Array-Index i))
      (dotimes (j ncols)
	(declare (type az:Array-Index j))
	(setf (aref mtx i j) 0.0d0)))
    mtx))

;;;=======================================================================
;;; CHECK-TYPE with an argument order more like
;;; (declare (type Integer x y))

(defmacro type-check (type &rest args)
  (let* ((args (remove-duplicates args))
	 (variables (remove-if #'constantp args))
	 (runtime-checks (mapcar #'(lambda (arg) `(check-type ,arg ,type))
				 variables)))
    ;; test constants at macro expand time:
    (dolist (constant (remove-if-not #'constantp args))
      ;; need to <eval> because <constant> might be a <defconstant>
      ;; rather than a literal constant
      (assert (typep (eval constant) type)))
    (unless (null runtime-checks) `(progn ,@runtime-checks))))

(defmacro declare-check (&rest decls)

  "This macro generates type checking forms and has a syntax like <declare>.
Unfortunately, we can't easily have it also generate the declarations."

  `(progn
     ,@(mapcar #'(lambda (d) `(type-check ,(second d) ,@(cddr d)))
	       ;; ignore non-type declarations
	       (remove-if-not #'(lambda (d) (eq (first d) 'type))
			      decls))))

;;;============================================================
;;; Safety under multiprocessing:
;;;============================================================
;;; <atomic> is used by the locking mechanism below.
;;;
;;; It is inspired by PCL's without-interrupts.
;;;
;;; Begin quote from PCL's low.lisp:
;;;
;;; without-interrupts
;;; 
;;; OK, Common Lisp doesn't have this and for good reason.  But For all of
;;; the Common Lisp's that PCL runs on today, there is a meaningful way to
;;; implement this.  WHAT I MEAN IS:
;;;
;;; I want the body to be evaluated in such a way that no other code that is
;;; running PCL can be run during that evaluation.  I agree that the body
;;; won't take *long* to evaluate.  That is to say that I will only use
;;; without interrupts around relatively small computations.
;;;
;;; INTERRUPTS-ON should turn interrupts back on if they were on.
;;; INTERRUPTS-OFF should turn interrupts back off.
;;; These are only valid inside the body of WITHOUT-INTERRUPTS.
;;;
;;; OK?
;;;
;;; 
;;; (defmacro without-interrupts (&body body)
;;;   `(macrolet ((interrupts-on () ())
;;; 	      (interrupts-off () ()))
;;;      (progn ,.body)))
;;; 
;;; End quote from PCL's low.lisp.

;;; I've simplified the PCL {\tt without-interupts} to dispense with the
;;; fine-grained control provided by {\tt interrupts-on} and {\tt
;;; interrupts-off}.  The first ({\tt coral}) implementation should be
;;; good enough for any single threaded Lisp environment.  It may be the
;;; best we can do in some multi-threaded Lisps.

;;; We can borrow other ports from PCL from when we need them.

(eval-when (compile load eval)

#+:coral ;; borrowed from PCL's low.lisp
(defmacro atomic (&body body) `(progn ,@body))

#+genera 
(defmacro atomic (&body body) `(scl:without-interrupts ,@body))

#+:excl ;; borrowed from PCL's excl-low.lisp
(defmacro atomic (&body body)
  `(let ((excl::*without-interrupts* 0))
     (declare (Special excl::*without-interrupts*))
      ,@body))
)

;;;=======================================================
;;; Locking mechanism to prevent simultaneous,
;;; multiple calls to c_npsol.
;;; This means, in particular, that c_npsol can't be called
;;; recursively, which one might want to do if, for example,
;;; the value of the objective function was itself the minimum
;;; in some optimization problem. However, since you can't do this
;;; in the original Fortran, it's not much of a loss.

(defparameter *npsol-lock* nil)

(defun get-npsol-lock (name)
  (loop
    (atomic (when (null *npsol-lock*) (setf *npsol-lock* name)))
    (when (eq *npsol-lock* name) (return))))

(defun release-npsol-lock (name)
  (atomic (when (eq *npsol-lock* name) (setf *npsol-lock* nil))))

(defmacro with-npsol-locked ((name) &body body)
  `(unwind-protect (progn (get-npsol-lock ,name) ,@body)
     (release-npsol-lock ,name)))

;;;=======================================================

#-(version>= 4 0)
(defun tenured? (x)
  (let ((ptr (excl::pointer-to-fixnum x)))
    (excl:gc)
    (eql ptr (excl::pointer-to-fixnum x))))

#-(version>= 4 0)
(defun all-tenured? (obj-list)
  (let ((ptr-list (mapcar #'excl::pointer-to-fixnum obj-list)))
    (excl:gc)
    (every #'(lambda (ptr obj) (eql ptr (excl::pointer-to-fixnum obj)))
	   ptr-list obj-list)))

#-(version>= 4 0)
(defun tenure! (x)
  (loop (when (tenured? x) (return))))

#-(version>= 4 0)
(defun tenure-all! (obj-list)
  (loop (when (all-tenured? obj-list) (return))))

#+(version>= 4 0)
(defun tenured? (x)
  (print (cons x (sys:pointer-storage-type x)))
  (finish-output)
  (let ((storage-type (sys:pointer-storage-type x)))
    (or (eq :tenured storage-type)
	(eq :immediate storage-type)
	(eq :static storage-type))))

#+(version>= 4 0)
(defun all-tenured? (obj-list) (every #'tenured? obj-list))

#+(version>= 4 0)
(defun tenure! (x)
  (loop
    (when (tenured? x) (return))
    (excl:gc :tenure)))

#+(version>= 4 0)
(defun tenure-all! (obj-list)
  (loop
    (when (all-tenured? obj-list) (return))
    (excl:gc :tenure)))

