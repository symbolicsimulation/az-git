;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(defpackage :Npsol
  (:use :Common-Lisp)
  (:nicknames :np)
  (:export 
   sample-objective-function
   sample-constraint-function
   set-npsol-option
   set-standard-npsol-options
   lisp-npsol
   npsol-minimize
   tn))





