*==================================================================
* A wrapper for NPOPTN
*==================================================================
*     
*     This seems to be needed for calling from lisp,
*     which can't seem to pass variable length strings correctly.
*     For this to work reliably, the lisp string should be terminated
*     with a #\*, to keep NPOPTN from reading garbage 

      subroutine option (string)
      character*128 string
      call NPOPTN(string)
      return
      end

