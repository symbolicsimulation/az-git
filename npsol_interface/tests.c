void c_calls_lisp (fun, index)
     long (*fun) ();
     int index;
{
  (*fun)(); /* direct call */
  lisp_call(index);  /* call using index */
}
  
