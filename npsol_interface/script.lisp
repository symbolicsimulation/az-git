;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :NPSOL -*-
;;;=======================================================

(in-package :Npsol)

;;;=======================================================

(progn
(defparameter *test-x* (make-double-array 3))
(defparameter *test-a* (make-double-array '(3 3)))
(defparameter *test-bl* (make-double-array 8))
(defparameter *test-bu* (make-double-array 8)))

(tn)
(tn-1)