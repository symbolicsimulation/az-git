;;;-*- Package: :Actors; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;; 
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty --- about the software, its performance, or its
;;; conformity to any specification --- is given or implied.
;;;
;;;============================================================

(in-package :Actors)

;;;============================================================

(defclass Role (Actor-Object)
	  ((role-actor
	    :type Actor
	    :reader role-actor
	    :initarg :role-actor))
  (:documentation
   "Roles provide multiple behaviors for actors."))

;;;============================================================
;;; Role protocol:
;;;============================================================

(defgeneric role-interesting-msgs (role)
  #-sbcl (declare (type (or Null Role) role))
  (:documentation 
   "When not null, this is a hint that the actor will ignore
any other msgs."))

(defmethod role-interesting-msgs ((role Role)) nil)
(defmethod role-interesting-msgs ((role Null)) nil)

;;;------------------------------------------------------------

(defgeneric handle-msg (role q)
  #-sbcl (declare (type Role role)
		  (type Queue q))
  (:documentation
   "Handle the next msg in the queue."))

(defmethod handle-msg ((role Role) q)
  "The default method for <handle-msg> assumes that a msg is a list,
whose first element is a function. It applies the function to
arguments consisting of the Role object and the rest of the msg list."
  (declare (type Queue q))
  (let ((msg (dequeue q)))
    (cond ((typep (role-actor role) 'az:Dead-Object)
	   ;; actor is dead, ignore any remaining msgs
	   t)
	  (msg ;; there is a msg
	   (apply (first msg) role msg))
	  (t ;; the queue must be empty, give a warning
	   (cerror "Ignore error" "~%Empty msg queue ~s:~s" role q)))))
 
;;;-----------------------------------------------------------

(defun skip-to-last (role msg-name)

  "Jump to the last msg named <msg-name> on the queue, throwing away
all the <msg-name> msgs ahead of it. This is useful, for example, in
mouse tracking, when one may only want to use the most recently
reported position."
  
  (declare (optimize (safety 0) (speed 3))
	   (type Role role)
	   (type Symbol msg-name))

  (let ((queue (actor-msg-queue (role-actor role)))
	(msg nil))
    (declare (type Queue queue))
    (loop (let ((next-msg (dequeue queue)))
	    (unless (eq (first next-msg) msg-name)
	      (unless (null next-msg) (queue-push next-msg queue))
	      (return))
	    (setf msg next-msg)))
    msg))
 
;;;============================================================

(defclass Actor (Actor-Object)
	  ((actor-msg-queue
	    :type Queue
	    :reader actor-msg-queue
	    :initform (make-queue))
	   (actor-current-role
	    :type (or Null Role)
	    :accessor actor-current-role
	    :initform nil)
	   (actor-default-role
	    :type Role
	    :accessor actor-default-role
	    :initarg :actor-default-role))
  (:documentation
   "An Actor has a msg queue and coordinates multiple Roles."))

;;;============================================================
;;; Global references to actors, for message handling process
;;;============================================================

(defvar *actors-on-stage* ()
  "These Actors are handling messages." )

(defvar *actors-off-stage* ()
  "These Actors are not handling messages.")

(defun enter-stage (actor)
  (declare (type Actor actor))
  (atomic
   (setf *actors-off-stage* (delete actor *actors-off-stage*))
   (pushnew actor *actors-on-stage*)))

(defun exit-stage (actor)
  (declare (type Actor actor))
  (atomic
   (setf *actors-on-stage* (delete actor *actors-on-stage*))
   (pushnew actor *actors-off-stage*)))

;;;============================================================
;;; Actor protocol:
;;;============================================================

(defgeneric build-roles (actor)
  #-sbcl (declare (type Actor actor))
  (:documentation
   "<build-roles> is called to initialize an actor.  Its job
is to construct all the Role objects that the actor may need."))

;;;-----------------------------------------------------------

(defmethod initialize-instance :after ((actor Actor) &rest initargs)
  (declare (ignore initargs))
  (build-roles actor))

;;;-----------------------------------------------------------

(defgeneric actor-msgs? (actor)
  #-sbcl (declare (optimize (safety 0) (speed 3))
		  (type (or Actor az:Dead-Object) actor)
		  (:returns az:Boolean))
  (:documentation
   "Does the actor have any msgs waiting to be handled?

We need to have methods for both Actors and az:Dead-Objects,
in case someone keeps a reference to an actor that's been killed.")

  (:method
   ((actor Actor))
   (declare (optimize (safety 0) (speed 3)))
   (not (%queue-empty? (the Queue (actor-msg-queue actor)))))

  (:method
   ((actor az:Dead-Object))
   (declare (optimize (safety 0) (speed 3)))
   nil))

(defun any-actor-msgs? ()
  (declare (optimize (safety 0) (speed 3))
	   (special *actors-on-stage*)
	   (:returns az:Boolean))
  (dolist (actor *actors-on-stage* nil)
    (declare (type Actor actor))
    (when (actor-msgs? actor) (return t))))

;;;-----------------------------------------------------------

(defparameter *id* 0)
(declaim (type Fixnum *id*))
(defun new-id () 
  (declare (special *id*))
  (incf *id*))

;;;-----------------------------------------------------------

(defgeneric send-msg (msg-name sender receiver msg-id &rest args)
  #-sbcl (declare (type az:Funcallable msg-name)
		  (type T sender receiver)
		  (type Symbol msg-id)
		  (type List args))
  (:documentation "<send-msg> is used to send a <msg> to an Actor."))

(defmethod send-msg (msg-name sender receiver msg-id &rest args)
  "The default method for <send-msg> applys <msg-name> to 
(receiver msg-name sender receiver msg-id . args)."
  (declare (type az:Funcallable msg-name)
	   (type T sender receiver)
	   (type Msg-Id msg-id)
	   (type List args))
  (apply msg-name receiver 
	 msg-name sender receiver msg-id args))

(defmethod send-msg (msg-name sender (receiver Actor) msg-id 
		     &rest args)
  "The default method for Actors enqueues the list (msg . args) on the
actor's msg queue."
  (declare (type az:Funcallable msg-name)
	   (type T sender)
	   (type Actor receiver)
	   (type Msg-Id msg-id)
	   (type List args))
  (enqueue (list* msg-name sender receiver msg-id args)
	   (actor-msg-queue receiver)))

;;;-----------------------------------------------------------

(defgeneric send-interrupt (msg-name sender receiver msg-id &rest args)
  #-sbcl (declare (type az:Funcallable msg-name)
		  (type T sender receiver)
		  (type Symbol msg-id)
		  (type List args))
  (:documentation "<send-interrupt> is used to send a <msg> to an Actor,
ahead of any pending messages."))

(defmethod send-interrupt (msg-name sender receiver msg-id &rest args)
  "The default method for <send-interrupt> applys <msg-name> to 
(receiver msg-name sender receiver msg-id . args)."
  (declare (type az:Funcallable msg-name)
	   (type T sender receiver)
	   (type Keyword msg-id)
	   (type List args))
  (apply msg-name receiver 
	 msg-name sender receiver msg-id args))

(defmethod send-interrupt (msg-name sender (receiver Actor) msg-id 
			   &rest args)
  "The default method for Actors pushes the msg on the front of
receiver's msg queue."
  (declare (type az:Funcallable msg-name)
	   (type T sender)
	   (type Actor receiver)
	   (type Keyword msg-id)
	   (type List args))
  (queue-push (list* msg-name sender receiver msg-id args)
	      (actor-msg-queue receiver)))

;;;-----------------------------------------------------------

(defgeneric start-msg-handling (actor)
  #-sbcl (declare (type Actor actor))
  (:documentation
   "<start-msg-handling> (re)starts the actor's
msg handling, putting the actor into its default role."))

(defmethod start-msg-handling ((actor Actor))
  (atomic
   (stop-msg-handling actor)
   (setf (actor-current-role actor) (actor-default-role actor))
   (enter-stage actor)))

;;;-----------------------------------------------------------

(defgeneric stop-msg-handling (actor)
  #-sbcl (declare (type Actor actor))
  (:documentation
   "<stop-msg-handling> halts the actor's msg handling."))

(defmethod stop-msg-handling ((actor Actor))
  (exit-stage actor))

;;;-----------------------------------------------------------
;;; Killing
;;;-----------------------------------------------------------

(defmethod az:kill-object :before ((actor Actor))
  "A dead Actor is neither on or off stage."
  (stop-msg-handling actor)
  (setf *actors-off-stage* (delete actor *actors-off-stage*))
  (setf *actors-on-stage* (delete actor *actors-on-stage*))
  (values))

;;;-----------------------------------------------------------


