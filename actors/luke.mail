I have frequently been confused by the use of ``message sending''
as a metaphor for both polymorphic function calls and communication
between multiple processes. I think it's important to keep these

Probably because I program in CLOS, I've fallen into the habit of
equating polymorphism with a powerful rule for function application
and reserving message sending for inter-process communication, where
``process'' is interpreted loosely.


>>My current thinking is that I want a mechanism somewhere between your
>>announcements and Field that allows a very limited form of pattern
>>matching, but gives good enough performance for implementing linking
>>on points using this facility. Here is a rough outline.

>>An object that wants to respond to an announcement registers a pattern
>>consisting of a list of things, along with a message selector to be
>>used if a match occurs, something like

>>	(send y :register-response (list x 'hey) :ding)

>>Another object then sends an announcement with, say,

>>	(send x :announce 'hey 1)

>>The announcement is (x 'hey 1), and matches a pattern of n elements if
>>all the first n elements of the announcement are eql to the pattern
>>elements; so this matches y's pattern of (x 'hey). The convention then
>>is to send the response message with the unmatched elements of the
>>announcement, so

>>	(send y :ding 1)

>>in this example. It would be easy in principle to allow wild cards in
>>either the announcement or the pattern, but I'm not sure what this
>>would do to performance. (To support linking in the way I would like,
>>I think it is essential that O(N) matches can be found in O(N) time,
>>no matter what patterns have been registered -- I probably need to
>>look at some data base literature to find out how to think about
>>this.) The system as is, without wild cards, should allow me to redo
>>my current linking scheme, which is a bit convoluted, by having a plot
>>do

>>	(send self :announce 'point-state-changed i 'selected)

>>if point i is selected, and having linked plots do

>>	(send self :register-response
>>              (list p 'point-state-changed)
>>              :adjust-point-state)

>>to make sure it is notified. I think this can be done with only
>>minimal performance loss over the current version, and it is much
>>easier to customize. For example, if you want points in a plot to
>>represent separate point objects, just have the point object
>>corresponding to plot point 3 in plot p do

>>	(send self :register-response
>>              (list p 'point-state-changed 3)
>>              :adjust-state)

>>without any change to the basic plots.

>>Clearly the idea of only matching the beginning is a restriction;
>>maybe it is possible to be more flexible without taking a serious
>>performance hit.  One possible convention would be to specify the
>>positions that need to be matched exactly and to have the remaining
>>arguments be passed on to the response message in the order they were
>>given.

>>On asynchronous/synchronous announcing, it seems to me at this point
>>that the most important use of asynchronous announcements is for the
>>announcer to be able to post an announcement but have it delayed until
>>it is finished with its current processing. For example, in my
>>regression models I want to be able to announce a change whenever one
>>of the inputs is adjusted, but I don't want the announcement broadcast
>>until I am finished making all the changes i want to make. One way to
>>think about this is that the announcement is being made in a thread
>>that has an event queue associated with it, and what asynchronous
>>posting of an announcement does it to put something in the announcer's
>>queue that then gets processed when event processing resumes. It would
>>also be possible to use some sort of suspend/resume calls, but that
>>seems less clean.

>>Once the announcement is processed, if the receiver is in another
>>thread with another event queue, the processing of the announcement
>>might have to be queued up in the receiver's queue as a matter of
>>implementation.  Something along these lines is probably going to be
>>necessary for interprocess communication. It would be possible to ask
>>for this explicitly when registering a response, but I'm not convinced
>>this is really useful at this level -- it seems to me that it belongs
>>in a lower level of a communications protocol.

>>At any rate, to support the delayed sending I am thinking of having
>>an object do

>>	(send x :post-announcement 'hey 1)

>>Then when the event queue of the thread where this occurs resumes
>>processing events it checks for pending announcements and processes
>>them.  To avoid piling up announcements, sending the same announcement
>>(with all elements eql) puts only one in the queue of pending
>>announcements.

>>(It might also be useful to have some convention with asynchronous
>>announcements about not sending the same message twice to avoid race
>>conditions, but I'm not sure that is necessary yet.)

>>That's about it -- not very coherent yet, but hopefully it will make a
>>bit mere sense, at least to me, in the next few days.

>>luke


