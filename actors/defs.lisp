;;;-*- Package: :Actors; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;; 
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty --- about the software, its performance, or its
;;; conformity to any specification --- is given or implied.
;;;
;;;============================================================

(in-package :Actors) 

;;;============================================================

(defclass Actor-Object (az:Arizona-Object) ()
  (:documentation "A root for all the classes in the Actor package."))

;;;============================================================

(deftype Msg-Id () 
  "A type for message identifiers. The combination
of sender and the message identifier uniquely identifies a message."
  '(Integer 1 #.most-positive-fixnum))