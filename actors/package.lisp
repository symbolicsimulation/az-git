;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 

;;;=======================================================================

(in-package :cl-user)

(defpackage :Actors
  (:use :Common-Lisp #+:clos :CLOS)
  (:nicknames :Ac)
  (:export
   Msg-Id
   
   Queue
   print-queue
   make-queue
   queue-empty?
   non-queue-empty?
   queue-clear
   enqueue
   dequeue
   queue-length
   queue-first
   queue-push
   queue-delete
   queue-find
   queue-find-if
   move-to-queue-head

   atomic

   Actor
   actor-current-role
   actor-default-role
   build-roles
   actor-msg-queue
   new-id
   send-msg
   ;;update-actor-msg-queue
   enter-stage
   exit-stage
   start-msg-handling
   stop-msg-handling
	    
   Role
   role-actor
   role-interesting-msgs
   handle-msg
   skip-to-last

   Interactor	
   Top-Level-Interactor
   window-top-level-interactor
   interactor-window

   Interactor-Role
   interactor-role-cursor
   interactor-busy-cursor
   with-interactor-busy

   host-display
	    
   ;; event handler function names
   button-press
   button-release
   key-press
   key-release
   motion-notify
   enter-notify
   leave-notify

   Focus-Change-Kind
   focus-in
   focus-out
	    
   keymap-notify
   mapping-notify

   exposure
   graphics-exposure
   no-exposure

   circulate-notify
   configure-notify
   create-notify
   destroy-notify
   gravity-notify
   map-notify
   reparent-notify
   unmap-notify
   visibility-notify

   circulate-request
   colormap-notify
   configure-request
   map-request
   resize-request

   client-message
   destroy-request
   property-notify
   selection-clear
   selection-notify
   selection-request

   event-loop
   exit-event-loop
   ))

;;;============================================================

(declaim (declaration :returns))



