;;;-*- Package: :Actors; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald 
;;; All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Actors)

;;;============================================================
;;; This file is supposed to contain load time initializations:
;;;============================================================

(eval-when (eval load)
  #+:excl
  (mp:process-run-function "Actor event loop" 'actor-event-loop))



