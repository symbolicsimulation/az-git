;;;-*- Package: :Actors; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Actors) 

;;;============================================================

(defun new-id () (gentemp "ID_" :Keyword))

;;;------------------------------------------------------------

(defun print-hashtable (key value)
  (print (list key value))
  (when (typep value 'Hash-Table)
	(maphash #'print-hashtable value)))

(defun display-table (server)
  (print-hashtable t (message-name-table server)))
 
;;;============================================================
;;; Message Audiences
;;;============================================================

(deftype Wildcard () '(Member :*))
(deftype Meta-Wildcard () '(Member :* :**))

;;;============================================================

(defvar *msg-name-table* (make-hash-table :test #'eq)

  "The *msg-name-table* table is used to map a message (based on the
msg-name, sender, and receiver) to a list of audience members, who
also receive the message.  The *msg-name-table* itself maps message
names (including :*) to sender tables. A sender table maps sender ids
(including :*) to receiver tables. A receiver table maps receiver ids
(including :*) to audience id lists.")

;;;============================================================

(defun msg-audience (msg-name sender receiver)

  "Return a list of the actor that should have this msg delivered to them."

  (declare (type (or az:Funcallable Wildcard) msg-name)
	   (type (or Actor Wildcard) sender receiver)
	   (:returns (type List)))

  (let* ((n-table (gethash msg-name *msg-name-table* nil))
	 (*-table (unless (eq msg-name :*)
		    (gethash :* *msg-name-table* nil)))
	 (ns-table (unless (null n-table) (gethash sender n-table nil)))
	 (n*-table (unless (or (eq sender :*) (null n-table))
		     (gethash :* n-table nil)))
	 (*s-table (unless (null *-table) (gethash sender *-table nil)))
	 (**-table (unless (or (eq receiver :*) (null *-table))
		     (gethash :* *-table nil))))
    (delete-duplicates
     (nconc
      (unless (eq receiver :*) receiver)
      (unless (null ns-table)
	(concatenate 'List
	  (gethash reciever ns-table nil) (gethash :* ns-table nil)))
      (unless (null n*-table)
	(concatenate 'List
	  (gethash receiver n*-table nil) (gethash :* n*-table nil)))
      (unless (null *s-table)
	(concatenate 'List
	  (gethash receiver *s-table nil) (gethash :* *s-table nil)))
      (unless (null **-table)
	(concatenate 'List
	  (gethash receiver **-table nil) (gethash :* **-table nil)))))))

;;;------------------------------------------------------------

(defun join-audience (actor msg-name sender receiver)

  "A call to <join-audience> will cause <actor> to receive a copy of
any message sent which matches the join template --- the triple of
<msg-name>, <sender>, and <receiver>.  <msg-name> is either a
az:Funcallable or :*, and <sender> and <receiver> are either Actors or
:*. A message matches a template if the three fields match; the fields
match if they are eq or if the template's value is the wild card :*."

  (declare (type Actor actor)
	   (type (or az:Funcallable Wildcard) msg-name)
	   (type (or Actor Wildcard) sender receiver))

  (let ((s-table (gethash msg-name *msg-name-table* nil)))
    (declare (type (or Null Hash-Table) s-table))
    (when (null s-table)
      (setf s-table (make-hash-table :test #'eq))
      (setf (gethash msg-name n-table) s-table))
    (let ((r-table (gethash sender s-table nil)))
      (declare (type (or Null Hash-Table) r-table))
      (when (null r-table)
	(setf r-table (make-hash-table :test #'eq))
	(setf (gethash sender s-table) r-table))
      (let ((audience (gethash receiver r-table nil)))
	(declare (type List audience))
	(setf (gethash receiver r-table) (pushnew actor audience))))))

;;;------------------------------------------------------------

(defun leave-audience (actor msg-name sender receiver)

  "A call to leave-audience says that <actor> should no longer receive
copies of messages matching one or more join templates.  The semantics
of <leave-audience> can be modeled as follows: Suppose that, for each
actor, the message dispatcher keeps a list of all the join templates
passed in calls to join-audience. When leave-audience is called, the
dispatcher deletes all those join templates belonging to <actor> that
match the leave template.  A join template matches the leave template
if the three fields match.  A field of the leave template matches a
field of a join template if they are eq or if the field of the
<leave-template> is the meta wildcard :**.

The reason for the meta wildcard :** in leave templates is to allow
<actor> to differentiate between deleting a specific join template
that contains :* wildcards and deleting sets of join templates
specified with a meta wild cards."

  (declare (type Actor actor)
	   (type (or az:Funcallable Meta-Wildcard) msg-name)
	   (type (or Actor Meta-Wildcard) sender receiver))

  (if (eq msg-name :**)
    ;; then remove it for all msg-names
    (maphash #'(lambda (key value)
		 ;; recurse on every message name that's in the table
		 (declare (ignore value))
		 (leave-audience actor key sender receiver))
	     *msg-name-table*)
    ;; else
    (labels ((remove-from-rtab (a rtab receiver)
	       (declare (type Actor a receiver)
			(type (or Null Hash-Table) rtab))
	       (unless (null rtab)
		 (if (eq receiver :**)
		   ;; then recurse
		   (maphash #'(lambda (key value)
				(declare (ignore value))
				(remove-from-rtab a rtab key))
			    rtab)
		   ;; else
		   (let ((audience (gethash receiver rtab nil)))
		     (declare (type List audience))
		     (unless (null audience)
		       (setf (gethash receiver rtab) (delete a audience)))))))

	     (remove-from-stab (a stab sender receiver)
	       (declare (type Actor a sender receiver)
			(type (or Null Hash-Table) stab))
	       (unless (null stab)
		 (if (eq sender :**)
		   ;; then recurse
		   (maphash #'(lambda (key value)
				(declare (ignore value))
				(remove-from-stab a stab key receiver))
			    stab)
		   ;; else
		   (remove-from-rtab a (gethash sender stab nil) receiver)))))
      
      (remove-from-stab
       actor (gethash msg-name *msg-name-table* nil) sender receiver))))
 
;;;============================================================
;;; Sending
;;;============================================================

(defun deliver-msg (msg actor &key (interrupt? nil))
  "<deliver-msg> adds <msg> to the back of the actor's queue,
unless it is an interrupt msg, in which case it goes on the front."
  (declare (type List msg)
	   (type Actor actor))
  (if interrupt?
    (queue-push msg (actor-msg-queue receiver))
    ;; else
    (enqueue msg (actor-msg-queue receiver))))


(defun send-msg (msg-name sender receiver msg-id &rest args)
  "Send a msg to <receiver> and the rest of the audience."
  (declare (type az:Funcallable msg-name)
	   (type Actor sender)
	   (type (or Actor Wildcard) receiver)
	   (type Keyword msg-id)
	   (type List args))
  (let ((msg (list* msg-name sender receiver msg-id (copy-list args))))
    (dolist (actor (msg-audience msg-name sender receiver))
      (deliver-msg msg actor))))

(defun send-interrupt (msg-name sender receiver msg-id &rest args)
  "Send an interrupt msg to <receiver> and the rest of the audience."
  (declare (type az:Funcallable msg-name)
	   (type Actor sender)
	   (type (or Actor Wildcard) receiver)
	   (type Keyword msg-id)
	   (type List args))
  (let ((msg (list* msg-name sender receiver msg-id (copy-list args))))
    (dolist (actor (msg-audience msg-name sender receiver))
      (deliver-msg msg actor :interrupt? t))))

  
