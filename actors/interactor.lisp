;;;-*- Package: :Actors; Syntax: Common-Lisp; Mode: Lisp -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;;
;;;============================================================

(in-package :Actors) 

;;;============================================================
;;; Actors to handle input to CLX Windows
;;;============================================================

(defclass Interactor (Actor)
	  ((interactor-window
	    :type xlib:Window
	    :accessor interactor-window
	    :initarg :interactor-window))
  (:documentation
   "Interactors are Actors whose msg queues contain msgs corresponding
to CLX input events."))

(defclass Top-Level-Interactor (Interactor)
	  ()
  (:documentation
   "There is a unique Top-Level-Interactor associated with each
xlib:Window. For every CLX event in an xlib:Window, the Actor package's 
CLX event dispatcher sends a corresponding message to the window's
Top-Level-Interactor. The Top-Level-Interactor is responsible for
either handling the message or forwarding it to some other Interactor."))

;;;============================================================
;;; Get current Interactor for CLX  window
;;;============================================================
;;; This is good when there are a small number of live top-level-interactors;
;;; it might need to be rewritten if using many top-level-interactors at a time
;;; is common.

(defvar *window-top-level-interactor-table*
    (az:make-alist-table :test #'xlib:window-equal))

(defun window-top-level-interactor (window)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Window window))
  (az:lookup window *window-top-level-interactor-table*))

(defsetf window-top-level-interactor (window) (top-level-interactor)
  `(atomic
    (locally
	(declare (type (or Null Top-Level-Interactor) ,top-level-interactor)))
    (cond ((null ,top-level-interactor)
	   (az:remove-entry ,window *window-top-level-interactor-table*))
	  (t
	   (setf (interactor-window ,top-level-interactor) ,window)
	   (setf (az:lookup ,window *window-top-level-interactor-table*)
	     ,top-level-interactor)))))

;;;============================================================
;;; Roles for input to CLX windows
;;;============================================================

(defclass Interactor-Role (Role) ()
  (:documentation "Modes for interactor input handling."))

;;;============================================================

(defgeneric interactor-role-cursor (interactor role)
  #-sbcl (declare (type Interactor interactor)
	   (type (or Interactor-Role Null) role)
	   (:returns (type xlib:Cursor)))
  (:documentation
   "Returns the cursor to be used when this Role is current."))

(defmethod interactor-role-cursor ((interactor Interactor)
				   (role Interactor-Role))
  "The default cursor"
  (xlt:window-cursor (interactor-window interactor) :target))

(defmethod interactor-role-cursor ((interactor Interactor) (role Null))
  "When no role is current, we get the skull&crossbones cursor."
  (xlt:window-cursor (interactor-window interactor) :pirate))

(defmethod interactor-role-cursor ((interactor az:Dead-Object) (role T))
  "If the Interactor has been killed, we get the skull&crossbones cursor."
  (xlt:window-cursor (interactor-window interactor) :pirate))

(defmethod interactor-role-cursor ((interactor T) (role az:Dead-Object))
  "If the Role has been killed, we get the skull&crossbones cursor."
  (xlt:window-cursor (interactor-window interactor) :pirate))

(defgeneric interactor-busy-cursor (interactor)
  
  #-sbcl (declare (type Interactor interactor)
	   (:returns (type xlib:Cursor)))
  
  (:method
   ((interactor Interactor))
   "The default method returns the watch cursor."
   (xlt:window-cursor (interactor-window interactor) :watch))
  
  (:documentation
   "Returns the cursor to be used when this Interactor is
too busy to respond to input quickly."))

(defmacro with-interactor-busy ((actor) &body body)

  "Let the user know that the actor is too busy to respond to input
by switching to the busy cursor."
  
  (let ((actor0 (gensym))
	(window (gensym))
	(display (gensym)))
    `(let* ((,actor0 ,actor)
	    (,window (interactor-window ,actor0))
	    (,display (xlib:window-display ,window)))
       (declare (type Interactor ,actor0)
		(type xlib:Window ,window)
		(type xlib:Display ,display))
       (unwind-protect
	   (progn
	     (setf (xlib:window-cursor ,window)
	       (interactor-busy-cursor ,actor0))
	     (xlib:display-force-output ,display)
	     ,@body)
	 (progn
	   (setf (xlib:window-cursor ,window)
	     (interactor-role-cursor ,actor0 (actor-current-role ,actor0)))
	   (xlib:display-force-output ,display))))))

;;;============================================================

(defmethod role-interesting-msgs ((role Interactor-Role))

  "What msgs does this interactor want to know about?

The implication is that other msgs can be masked off by whatever
system generates the msgs. There is no guarantee that other msgs won't
be sent to this interactor."

  '(enter-notify
    leave-notify
    map-notify
    unmap-notify
    motion-notify
    button-press
    button-release
    key-press
    key-release
    exposure
    visibility-notify
    configure-notify))

;;;============================================================
;;; keyboard and pointer events 
;;;============================================================

(defgeneric button-press (role 
			  msg-name sender receiver id
			  code x y state time root root-x root-y child
			  same-screen-p)
	    
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))
	    
  (:documentation
   "<button-press> means one of the pointer (mouse) buttons has 
been pressed. See the CLX documentation for details of the arguments."))
	    
(defmethod button-press (role 
			 msg-name sender receiver id
			 code x y state time root root-x root-y child 
			 same-screen-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore role msg-name sender receiver id
		   code x y state time root root-x root-y child same-screen-p))
   
  (values t))

;;;-------------------------------------------------------------

(defgeneric button-release (role 
			    msg-name sender receiver id
			    code x y state time root root-x root-y child 
			    same-screen-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))
  (:documentation
   "<button-release> means one of the pointer (mouse) buttons has 
been released. See the CLX documentation for details of the arguments."))

(defmethod button-release (role 
			   msg-name sender receiver id
			   code x y state time root root-x root-y child 
			   same-screen-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore role msg-name sender receiver id
		   code x y state time root root-x root-y child same-screen-p))
  (values t))
 
;;;-------------------------------------------------------------

(defgeneric key-press (role 
		       msg-name sender receiver id
		       code x y state time root root-x root-y child 
		       same-screen-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))
  (:documentation
   "<key-press> means a key on the keyboard has been pressed.
See the CLX documentation for interpretation of the arguments."))

(defmethod key-press ((role Interactor-Role) 
		      msg-name sender receiver id
		      code x y state time root root-x root-y child 
		      same-screen-p)
  "The default method takes us out of the event loop for #\Q or #\q.
It discards all pending messages and returns to the default role
on receiving a control-c."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender receiver id
		   x y time root-x root-y child same-screen-p))
  (let* ((display (xlib:window-display root))
	 (char (xlib:keycode->character display code state))
	 (modifiers (xlib:make-state-keys state)))
    (declare (type xlib:Display display)
	     (type (or Character Symbol) char)
	     (type List modifiers))
    (when (and (member :control modifiers)
	       (member char '(#\c #\C #+:excl #\control-\c)))
      ;; then it's an interrupt, discard all other msgs
      ;; and revert to the default role
      (let ((actor (role-actor role)))
	(declare (type Interactor actor))
	(queue-clear (actor-msg-queue actor))
	(setf (actor-current-role actor) (actor-default-role actor))))
    (when (member char '(#\q #\Q)) (throw 'exit-event-loop char)))
  (values t))

;;;-------------------------------------------------------------

(defgeneric key-release (role 
			 msg-name sender receiver id
			 code x y state time root root-x root-y child 
			 same-screen-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))
  (:documentation
   "<key-release> means a key on the keyboard has been release.
See the CLX documentation for interpretation of the arguments."))

(defmethod key-release (role 
			msg-name sender receiver id
			code x y state time root root-x root-y child 
			same-screen-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card8 code)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore role msg-name sender receiver id
		   code x y state time root root-x root-y child same-screen-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric motion-notify (role 
			   msg-name sender receiver id
			   hint-p x y state time root root-x root-y child 
			   same-screen-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Boolean hint-p)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))
  (:documentation
   "<motion-notify> means the pointer has moved within the
window. See the CLX documentation for the interpretation of the
arguments."))

(defmethod motion-notify (role 
			  msg-name sender receiver id
			  hint-p x y state time root root-x root-y child 
			  same-screen-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Boolean hint-p)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore role msg-name sender receiver id
		   hint-p x y state time root root-x root-y child 
		   same-screen-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric enter-notify (role 
			  msg-name sender receiver id
			  x y mode kind focus-p state time root root-x root-y 
			  child same-screen-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Int16 x y)
	   (type (Member :normal :grab :ungrab) mode)
	   (type (Member :ancestor :virtual :inferior :nonlinear
			 :nonlinear-virtual)
		 kind)
	   (type xlib:Boolean focus-p)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))

  (:documentation
   "This occurs when the pointer enters a window.  See the
CLX documentation for the interpretation of the arguments."))

(defmethod enter-notify :before ((role Interactor-Role)
				 msg-name sender receiver id
				 x y mode kind focus-p state time
				 root root-x root-y child same-screen-p)
  "This before method ensures that the input focus is on the window."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Int16 x y)
	   (type (Member :normal :grab :ungrab) mode)
	   (type (Member :ancestor :virtual :inferior :nonlinear
			 :nonlinear-virtual)
		 kind)
	   (type xlib:Boolean focus-p)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore msg-name sender receiver id
		   x y mode kind state time root-x root-y child
		   same-screen-p))
  (unless focus-p
    (xlib:set-input-focus (xlib:window-display root)
			  (interactor-window (role-actor role))
			  :none))
  (values t))

(defmethod enter-notify (role
			 msg-name sender receiver id
			 x y mode kind focus-p state time
			 root root-x root-y child same-screen-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Int16 x y)
	   (type (Member :normal :grab :ungrab) mode)
	   (type (Member :ancestor :virtual :inferior :nonlinear
			 :nonlinear-virtual)
		 kind)
	   (type xlib:Boolean focus-p)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore role msg-name sender receiver id
		   x y mode kind focus-p state time root root-x root-y child
		   same-screen-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric leave-notify (role 
			  msg-name sender receiver id
			  x y mode kind focus-p state time root root-x root-y 
			  child same-screen-p)

  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Int16 x y)
	   (type (Member :normal :grab :ungrab) mode)
	   (type (Member :ancestor :virtual :inferior :nonlinear
			 :nonlinear-virtual)
		 kind)
	   (type xlib:Boolean focus-p)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p))

  (:documentation
   "This occurs when the pointer leaves a window.  See the
CLX documentation for the interpretation of the arguments."))

(defmethod leave-notify (role
			 msg-name sender receiver id
			 x y mode kind focus-p state time root root-x root-y 
			 child same-screen-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Int16 x y)
	   (type (Member :normal :grab :ungrab) mode)
	   (type (Member :ancestor :virtual :inferior :nonlinear
			 :nonlinear-virtual)
		 kind)
	   (type xlib:Boolean focus-p)
	   (type xlib:Card16 state)
	   (type xlib:Card32 time)
	   (type xlib:Window root)
	   (type xlib:Int16 root-x root-y)
	   (type (or xlib:Window Null) child)
	   (type xlib:Boolean same-screen-p)
	   (ignore role msg-name sender receiver id
		   x y mode kind focus-p state time root root-x root-y
		   child same-screen-p))
  (values t))

;;;=============================================================
;;; input focus events
;;;=============================================================

(defgeneric focus-in (role 
		      msg-name sender receiver id
		      mode kind)

  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type (Member :normal :grab :ungrab) mode)
	   (type (Member :ancestor :virtual :inferior :nonlinear
			 :nonlinear-virtual)
		 kind))

  (:documentation
   "-focus- is short for keyboard focus; after :focus-in,
all keyboard input is directed at this interactor, until :focus-out."))


(defmethod focus-in (role 
		     msg-name sender receiver id
		     mode kind) 
  "The default method does nothing."
  (declare (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (ignore role msg-name sender receiver id
		   mode kind))
  (values t))

(defgeneric focus-out (role 
		       msg-name sender receiver id
		       mode kind)

  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type (Member :normal :grab :ungrab) mode)
	   (type (Member :ancestor :virtual :inferior :nonlinear
			 :nonlinear-virtual)
		 kind))

  (:documentation
   "-focus- is short for keyboard focus; after :focus-in,
all keyboard input is directed at this interactor, until :focus-out."))

(defmethod focus-out (role 
		      msg-name sender receiver id
		      mode kind)
  "The default method does nothing."
  (declare (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (ignore role msg-name sender receiver id 
		   mode kind))
  (values t))

;;;=============================================================
;;; exposure events
;;;=============================================================

(defgeneric exposure (role 
		      msg-name sender receiver id
		      x y width height count)

  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Card16 x y width height count))

  (:documentation
   "In X, :exposure events occur whenever a rectangular region in a
window is exposed to view. They tend to be redundant, in that
:exposure events tend to come several at a time, often together with a
:visibility-notify or :configure-notify event. For the present, the
CLX driver will report them to interactors as they happen.  In the
future, we might consider cleaning things up by repackaging, for
example, a :visibility-notify and a following flurry of :exposure
events into a single interactor event, that contains a list of all the
rects that need to be repainted."))

(defmethod exposure (role
		     msg-name sender receiver id
		     x y width height count)
  (declare (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (ignore role msg-name sender receiver id
		   x y width height count))
  (values t))

;;;=============================================================
;;; window state events
;;;=============================================================

(defgeneric circulate-notify (role 
			      msg-name sender receiver id
			      circulated-window place)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window circulated-window)
	   (type (Member :top :bottom) place))

  (:documentation
   "This means the <circulated-window> has either been moved
either in front of or behind its siblings in the window hierarchy.
The Interactor receiving this message will probably correspond to the
<circulated-window>'s parent, though that's not perfectly clear from
the CLX documentation. I presume there will also be :visibility-notify
and :exposure events that provide the interactor with what it needs to
redisplay itself and that, therefore, we can ignore these."))

(defmethod circulate-notify (role 
			     msg-name sender receiver id
			     circulated-window place)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window circulated-window)
	   (type (Member :top :bottom) place)
	   (ignore role msg-name sender receiver id 
		   circulated-window place))
  (values t))

;;;-------------------------------------------------------------

(defgeneric configure-notify (role 
			      msg-name sender receiver id
			      x y width height border-width
			      above-sibling override-redirect-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 width height border-width)
	   (type (Or Null xlib:Window) above-sibling)
	   (type xlib:Boolean override-redirect-p))
  (:documentation
   "This signals a change to a window's size, position,
border, or stacking order. See the CLX documentation for the
interpretation of the arguments."))

(defmethod configure-notify (role
			     msg-name sender receiver id
			     x y width height border-width
			     above-sibling override-redirect-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 width height border-width)
	   (type (Or Null xlib:Window) above-sibling)
	   (type xlib:Boolean override-redirect-p)
	   (ignore role msg-name sender receiver id
		   x y width height border-width above-sibling
		   override-redirect-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric create-notify (role 
			   msg-name sender receiver id
			   created-window x y width height
			   border-width override-redirect-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window created-window)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 width height border-width)
	   (type xlib:Boolean override-redirect-p))
  (:documentation
   "This is meant to signal to a client that a window has
been created. It doesn't make sense to send this to the <created-window>'s
Interactor, who can't be active until the window is created.  It's
conceivable that this might be used by the parent window's Interactor
to wait until the child Interactor's window is ready before returning
from some window creation function.  See the CLX documentation for the
interpretation of the arguments."))

(defmethod create-notify (role 
			  msg-name sender receiver id
			  created-window
			  x y width height border-width override-redirect-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window created-window)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 width height border-width)
	   (type xlib:Boolean override-redirect-p)
	   (ignore role msg-name sender receiver id
		   created-window x y width height border-width 
		   override-redirect-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric destroy-notify (role 
			    msg-name sender receiver id
			    destroyed-window)

  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window destroyed-window))

  (:documentation
   "This is sent to notofy a client that a window has been
destroyed.  It might be sent to an Interactor other than the one whose
window has been destroyed, which is the reason for the
<destroyed-window> argument."))

(defmethod destroy-notify (role 
			   msg-name sender receiver id
			   destroyed-window)

  "The default method for <destroy-notify> kills the
Interactor whose window was destroyed."

  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window destroyed-window)
	   (ignore role msg-name sender receiver id))
  
  (let ((interactor (window-top-level-interactor destroyed-window)))
    (when interactor
      (setf (window-top-level-interactor destroyed-window) nil)
      (az:kill-object interactor)))
  (values t))

;;;-------------------------------------------------------------

(defgeneric destroy-request (role 
			     msg-name sender receiver id
			     window-to-be-destroyed)

  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-destroyed))

  (:documentation
   "This message does not directly correspond to a standard
CLX event. Rather, it is generated from a ``destroy-request''
client-message event. In most window managers, a request to destroy a
window is not handled directly, but forwarded to the client that owns
the window by means of a destroy-request client message. It's then up
to the client to actually destroy the window or to ignore the request,
if it wants to prevent the window from being destroyed."))



(defmethod destroy-request (role 
			    msg-name sender receiver id
			    window-to-be-destroyed)
  "Destroy the window, which should generate a :destroy-notify event.
But kill the window's interactor first, so that it doesn't
try to do anything to a destroyed window."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-destroyed)
	   (ignore role msg-name sender receiver id))
  (let ((display (xlib:window-display window-to-be-destroyed))
	(interactor (window-top-level-interactor window-to-be-destroyed)))
    (when interactor
      (setf (window-top-level-interactor window-to-be-destroyed) nil)
      (az:kill-object interactor))
    (xlib:destroy-window window-to-be-destroyed)
    (xlib:display-force-output display))
  (values t))

;;;-------------------------------------------------------------

(defgeneric gravity-notify (role 
			    msg-name sender receiver id
			    moved-window x y)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window moved-window)
	   (type xlib:Int16 x y))
  (:documentation
   "This occurs when <moved-window> is moved in response to a
change of size of its parent, if it has the right kind of gravity.
The event is most likely reported to the parent. See the CLX documentation
for the interpretation of the arguments."))

(defmethod gravity-notify (role 
			   msg-name sender receiver id
			   moved-window x y)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window moved-window)
	   (type xlib:Int16 x y)
	   (ignore role msg-name sender receiver id
		   moved-window x y))
  (values t))

;;;-------------------------------------------------------------

(defgeneric map-notify (role 
			msg-name sender receiver id
			mapped-window override-redirect-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window mapped-window)
	   (type xlib:Boolean override-redirect-p))
  (:documentation
   "``Mapping'' is perhaps the most prominent of many
examples of poorly chosen terminology in X (most of which are due to
poorly chosen concepts). The clearest evidence of a poor concept is
when you can't explain it in one sentence, using other concepts that
are either commonly understood or can themselves be explained in one
sentence. However, interactors want to know about these events, so
they can go dormant when their windows are unmapped (eg. iconified)
and wake up when the window is mapped.  The event may be reported to
some window other than the one that is mapped, which is the reason for
the <mapped-window> argument.  See the CLX documentation for the
interpretation of the arguments."))

(defmethod map-notify (role 
		       msg-name sender receiver id
		       mapped-window
		       override-redirect-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window mapped-window)
	   (type xlib:Boolean override-redirect-p)
	   (ignore role msg-name sender receiver id 
		   mapped-window override-redirect-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric reparent-notify (role 
			     msg-name sender receiver id
			     reparented-window parent x y
			     override-redirect-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window reparented-window parent)
	   (type xlib:int16 x y)
	   (type xlib:Boolean override-redirect-p))
  (:documentation
   "This event occurs when the window hierarchy is changed.
The event may be reported to some window other than the one that is
mapped, which is the reason for the <reparented-window> argument.  See
the CLX documentation for the interpretation of the arguments."))

(defmethod reparent-notify (role 
			    msg-name sender receiver id
			    reparented-window
			    parent x y override-redirect-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window reparented-window parent)
	   (type xlib:int16 x y)
	   (type xlib:Boolean override-redirect-p)
	   (ignore role msg-name sender receiver id 
		   reparented-window parent x y override-redirect-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric unmap-notify (role 
			  msg-name sender receiver id
			  unmapped-window configure-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window unmapped-window)
	   (type xlib:Boolean configure-p))
  (:documentation
   "``Mapping'' is perhaps the most prominent of many
examples of poorly chosen terminology in X (most of which are due to
poorly chosen concepts). The clearest evidence of a poor concept is
when you can't explain it in one sentence, using other concepts that
are either commonly understood or can themselves be explained in one
sentence. However, interactors want to know about these events, so
they can go dormant when their windows are unmapped (eg. iconified)
and wake up when the window is mapped.  The event may be reported to
some window other than the one that is mapped, which is the reason for
the <mapped-window> argument.  See the CLX documentation for the
interpretation of the arguments."))

(defmethod unmap-notify (role 
			 msg-name sender receiver id
			 unmapped-window configure-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window unmapped-window)
	   (type xlib:Boolean configure-p)
	   (ignore role msg-name sender receiver id 
		   unmapped-window configure-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric visibility-notify (role 
			       msg-name sender receiver id
			       state)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type (Member :unobscured :partially-obscured :fully-obscured)
		 state))
  (:documentation
   "This event is generated whenever the visibility of a
window changes. It seems to be redundant, because it's always (?)
accompanied by :exposure events. In the future, we might consider
cleaning things up by repackaging, for example, a :visibility-notify
and a following flurry of :exposure events into a single interactor
event that contains a list of all the rects that need to be repainted.
See the CLX documentation for the interpretation of the arguments."))

(defmethod visibility-notify (role 
			      msg-name sender receiver id
			      state)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type (Member :unobscured :partially-obscured :fully-obscured)
		 state)
	   (ignore role msg-name sender receiver id 
		   state))
  (values t))    
 
;;;============================================================
;;; Structure control events
;;;============================================================

(defgeneric circulate-request (role 
			       msg-name sender receiver id
			       circulated-window place)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window circulated-window)
	   (type (Member :top :bottom) place))
  (:documentation
   "This event is supposed to be handled by window managers,
who get to decide whether windows actually get circulated or not.  It
shouldn't be relevant to interactors. See the CLX documentation for
interpretation of the arguments."))

(defmethod circulate-request (role 
			      msg-name sender receiver id
			      circulated-window place)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window circulated-window)
	   (type (Member :top :bottom) place)
	   (ignore role msg-name sender receiver id 
		   circulated-window place))
  (values t))

;;;-------------------------------------------------------------

(defgeneric colormap-notify (role 
			     msg-name sender receiver id
			     colormap new-p installed-p)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type (Or Null xlib:Colormap) colormap)
	   (type xlib:Boolean new-p installed-p))
  (:documentation
   "This event is generated when the colormap assocaited
witha window is changed, installed, or uninstalled.  An Interactor
might want to receive these, to ensure that its colormap is installed
or to redraw itself with new pixel values chosen based on the current
colormap. See the CLX documentation for interpretation of the
arguments."))

(defmethod colormap-notify (role 
			    msg-name sender receiver id
			    colormap new-p installed-p)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type (Or Null xlib:Colormap) colormap)
	   (type xlib:Boolean new-p installed-p)
	   (ignore role msg-name sender receiver id 
		   colormap new-p installed-p))
  (values t))

;;;-------------------------------------------------------------

(defgeneric configure-request (role 
			       msg-name sender receiver id
			       window-to-be-configured
			       x y width height border-width
			       stack-mode above-sibling value-mask)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-configured)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 width height border-width)
	   (type (Member :above :top-if :bottom-if :opposite)
		 stack-mode)
	   (type (Or Null xlib:Window) above-sibling)
	   (type xlib:Mask16 value-mask))
  (:documentation
   ":configure-request is generated when one client attempts
to change the configuration of (another) client`s window.
:configure-request events are usually handled by a window manager, who
can modify or refuse the request.  Interactors can use these events to
prevent themselves from being reshaped, if they so desire.  See the
CLX documentation for the interpretation of the arguments."))

(defmethod configure-request (role 
			      msg-name sender receiver id
			      window-to-be-configured
			      x y width height
			      border-width stack-mode above-sibling value-mask)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-configured)
	   (type xlib:Int16 x y)
	   (type xlib:Card16 width height border-width)
	   (type (Member :above :top-if :bottom-if :opposite) stack-mode)
	   (type (Or Null xlib:Window) above-sibling)
	   (type xlib:Mask16 value-mask)
	   (ignore role msg-name sender receiver id 
		   window-to-be-configured x y width height border-width
		   stack-mode above-sibling value-mask))
  (values t))
  
;;;-------------------------------------------------------------

(defgeneric map-request (role 
			 msg-name sender receiver id
			 window-to-be-mapped)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-mapped))
  (:documentation
   "This event is generated when a client maps a window.  It
is usually handled by window managers.  Interactors may want to use it
to prevent a window from being mapped, or to ensure some other
computation takes place before a window is mapped."))

(defmethod map-request (role 
			msg-name sender receiver id
			window-to-be-mapped)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-mapped)
	   (ignore role msg-name sender receiver id 
		   window-to-be-mapped))
  (values t))

;;;-------------------------------------------------------------

(defgeneric resize-request (role 
			    msg-name sender receiver id
			    window-to-be-resized width height)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-resized)
	   (type xlib:Card16 width height))
  (:documentation
   "This event is generated when a client changes the width
or height of a window.  It is usually handled by window managers.
Interactors may want to use it to prevent a window from being resized,
control the changes to width or height, or to ensure some other
computation takes place before a window is resized."))

(defmethod resize-request (role
			   msg-name sender receiver id
			   window-to-be-resized
			   width height)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window window-to-be-resized)
	   (type xlib:Card16 width height)
	   (ignore role msg-name sender receiver id 
		   window-to-be-resized width height))
  (values t))

;;;============================================================
;;; Client communication events
;;;============================================================

(defun delete-window-client-message? (window data)
  (= (elt data 0)
     (xlib::atom-id :wm_delete_window (xlib:window-display window))))

(defgeneric client-message (role 
			    msg-name sender receiver id
			    type format data)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword type)
	   (type (Member 8 16 32) format)
	   (type Sequence #||(Sequence Integer)||# data))
  (:documentation
   "These are random events sent by one client to another.
we can't ignore them completely, because, for example, twm's
delete-window is handled by having the window manager send a client
message to the window to be deleted.  It's then up to the client that
owns the window to actually destroy the window.  It's probably better
if these get translated by the CLX device driver into more meaningful
interactor events (eg. :destroy-request). To really get this straight,
we'll have to dig into the ICCCM.  

See the CLX documentation for the interpretation of the arguments."))

(defmethod client-message ((role Interactor-Role) 
			   msg-name sender receiver id
			   type format data)
  "The default method translates destroy-request messages
and ignores everything else."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword type)
	   (type (Member 8 16 32) format)
	   (type Sequence #||(Sequence Integer)||# data)
	   (ignore msg-name sender receiver id 
		   type format))
  (let* ((interactor (role-actor role))
	 (window (interactor-window interactor)))
    (declare (type Actor interactor)
	     (type xlib:Window window))
    (when (delete-window-client-message? window data)
      (send-msg 'destroy-request window interactor (new-id) window)))
  (values t))

;;;-------------------------------------------------------------

(defgeneric property-notify (role 
			     msg-name sender receiver id
			     atom state time)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword atom)
	   (type (Member :new-value :deleted) state)
	   (type xlib:Timestamp time))
  (:documentation
   "This indicates that some random property of a window has
changed.  See the CLX documentation for the interpretation of the
arguments."))

(defmethod property-notify (role 
			    msg-name sender receiver id
			    atom state time)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword atom)
	   (type (Member :new-value :deleted) state)
	   (type xlib:Timestamp time)
	   (ignore role msg-name sender receiver id
		   atom state time))
  (values t))

;;;-------------------------------------------------------------

(defgeneric selection-clear (role 
			     msg-name sender receiver id
			     selection time)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword selection)
	   (type xlib:Timestamp time))
  (:documentation
   "This event is reported to the owner of the <selection>
when its ownership changes.  See the CLX documentation for the
interpretation of the arguments."))

(defmethod selection-clear  ((role Interactor-Role) 
			     msg-name sender receiver id
			     selection time)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword selection)
	   (type xlib:Timestamp time)
	   (ignore msg-name sender receiver id
		   selection time))
  (values t))

;;;-------------------------------------------------------------

(defgeneric selection-notify (role 
			      msg-name sender receiver id
			      selection target property time)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword selection target)
	   (type (Or Null Keyword) property)
	   (type xlib:Timestamp time))
  (:documentation
   "This event is sent to a client who calls
<convert-selection>. See the CLX documentation for the interpretation
of the arguments."))

(defmethod  selection-notify ((role Interactor-Role)
			      msg-name sender receiver id
			      selection target property time)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type Keyword selection target)
	   (type (Or Null Keyword) property)
	   (type xlib:Timestamp time)
	   (ignore msg-name sender receiver id
		   selection target property time))
  (values t))

;;;-------------------------------------------------------------

(defgeneric selection-request (role 
			       msg-name sender receiver id
			       requestor selection target property time)
  #-sbcl (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window requestor)
	   (type Keyword selection target)
	   (type (Or Null Keyword) property)
	   (type xlib:Timestamp time))
  (:documentation
   "This event is sent to the owner of a selection when a
client calls <convert-selection>. See the CLX documentation for the
interpretation of the arguments."))

(defmethod selection-request (role
			      msg-name sender receiver id
			      requestor selection target property time)
  "The default method does nothing."
  (declare (type Interactor-Role role)
	   (type az:Funcallable msg-name)
	   (type T sender)
	   (type Interactor receiver)
	   (type Msg-Id id)
	   (type xlib:Window requestor)
	   (type Keyword selection target)
	   (type (Or Null Keyword) property)
	   (type xlib:Timestamp time)
	   (ignore role msg-name sender receiver id
		   requestor selection target property time))
  (values t))

;;;============================================================
;;; Interactor methods
;;;============================================================

(defmethod build-roles ((interactor Interactor))
  (setf (actor-default-role interactor)
    (make-instance 'Interactor-Role :role-actor interactor)))

;;;------------------------------------------------------------

(defmethod (setf actor-current-role) ((role Interactor-Role)
				      (interactor Interactor))
  (atomic
   (setf (xlib:window-cursor (interactor-window interactor))
     (interactor-role-cursor interactor role))
   (setf (xlib:window-event-mask (interactor-window interactor))
     (xlt:event-list->event-mask (role-interesting-msgs role)))
   (setf (slot-value interactor 'actor-current-role) role)
   (xlt:drawable-force-output (interactor-window interactor))))

(defmethod (setf actor-current-role) ((role Null) (interactor Interactor))
  (atomic
   (setf (xlib:window-cursor (interactor-window interactor))
     (interactor-role-cursor interactor role))
   (setf (xlib:window-event-mask (interactor-window interactor)) nil)
   (setf (slot-value interactor 'actor-current-role) role)
   (xlt:drawable-force-output (interactor-window interactor))))

;;;------------------------------------------------------------

(defmethod start-msg-handling ((actor Interactor))
  (atomic
   (stop-msg-handling actor)
   (setf (actor-current-role actor) (actor-default-role actor))
   (enter-stage actor)))


(defmethod start-msg-handling ((actor Top-Level-Interactor))
  "Make sure there's only one Interactor for each window."
  (let* ((window (interactor-window actor))
	 (old  (window-top-level-interactor window)))
    (declare (type xlib:Window window)
	     (type (or Null Interactor) old))
    (atomic
     (unless (or (eq old actor) (null old))
       (setf (window-top-level-interactor window) nil)
       (az:kill-object old))
     (stop-msg-handling actor)
     (setf (actor-current-role actor) (actor-default-role actor))
     (setf (window-top-level-interactor (interactor-window actor)) actor)
     (enter-stage actor))))




