;;;-*- Package: :Actors; Syntax: Common-Lisp; Mode: Lisp -*-; 
;;;
;;; Copyright 1993. John Alan McDonald All Rights Reserved. 
;;; 
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty --- about the software, its performance, or its
;;; conformity to any specification --- is given or implied.
;;;
;;;============================================================

(in-package :Actors)

;;;============================================================
;;;
;;; This is inspired by PCL's without-interrupts.
;;;

;;; Begin quote from PCL's low.lisp:
;;;
;;; without-interrupts
;;; 
;;; OK, Common Lisp doesn't have this and for good reason.  But For all of
;;; the Common Lisp's that PCL runs on today, there is a meaningful way to
;;; implement this.  WHAT I MEAN IS:
;;;
;;; I want the body to be evaluated in such a way that no other code that is
;;; running PCL can be run during that evaluation.  I agree that the body
;;; won't take *long* to evaluate.  That is to say that I will only use
;;; without interrupts around relatively small computations.
;;;
;;; INTERRUPTS-ON should turn interrupts back on if they were on.
;;; INTERRUPTS-OFF should turn interrupts back off.
;;; These are only valid inside the body of WITHOUT-INTERRUPTS.
;;;
;;; OK?
;;;
;;; 
;;; (defmacro without-interrupts (&body body)
;;;   `(macrolet ((interrupts-on () ())
;;; 	      (interrupts-off () ()))
;;;      (progn ,.body)))
;;; 
;;; End quote from PCL's low.lisp.

;;; I've simplified the PCL {\tt without-interupts} to dispense with the
;;; fine-grained control provided by {\tt interrupts-on} and {\tt
;;; interrupts-off}.  The first ({\tt coral}) implementation should be
;;; good enough for any single threaded Lisp environment.  It may be the
;;; best we can do in some multi-threaded Lisps.

;;; We can borrow other ports from PCL from when we need them.

(defmacro atomic (&body body)

  "<atomic> is inspired by PCL's <without-interrupts>.

From PCL's low.lisp:

 WITHOUT-INTERRUPTS

 OK, Common Lisp doesn't have this and for good reason.  But For all of
 the Common Lisp's that PCL runs on today, there is a meaningful way to
 implement this.  WHAT I MEAN IS:

 I want the body to be evaluated in such a way that no other code that
 is running PCL can be run during that evaluation.  I agree that the
 body won't take *long* to evaluate.  That is to say that I will only
 use without interrupts around relatively small computations.

 INTERRUPTS-ON should turn interrupts back on if they were on.
 INTERRUPTS-OFF should turn interrupts back off.  These are only valid
 inside the body of WITHOUT-INTERRUPTS.  OK?

I've simplified the PCL <without-interupts> to dispense with the
fine-grained control provided by <interrupts-on> and <interrupts-off>.
The first (<coral>) implementation should be good enough for any
single threaded Lisp environment.  It may be the best we can do in
some multi-threaded Lisps."

  #+:coral
  `(progn ,@body)

  #+genera 
  `(scl:without-interrupts ,@body)

  #+:cmu
  `(progn ,@body)
  ;;`(sys:without-interrupts ,@body)
 
  #+:excl
  `(let ((excl::*without-interrupts* 0))
     (declare (special excl::*without-interrupts*))
     ,@body))

;;; These versions preserve the fine grained control given by
;;; PCL's {\tt without-interrupts}

;;; #+:coral ;; borrowed from PCL's low.lisp
;;; (defmacro atomic (&body body)
;;;   `(macrolet ((interrupts-on () ())
;;; 	      (interrupts-off () ()))
;;;      (progn ,@body)))
;;; 
;;; #+genera ;; borrowed from PCL's genera-low.lisp
;;; (defmacro atomic (&body body)
;;;   `(let ((outer-scheduling-state si:inhibit-scheduling-flag)
;;; 	 (si:inhibit-scheduling-flag t))
;;;      (macrolet ((interrupts-on  ()
;;; 		  '(when (null outer-scheduling-state)
;;; 		     (setq si:inhibit-scheduling-flag nil)))
;;; 		(interrupts-off ()
;;; 		  '(setq si:inhibit-scheduling-flag t)))
;;;        (progn outer-scheduling-state)
;;;        ,.body)))
;;; 
;;; #+:excl ;; borrowed from PCL's excl-low.lisp
;;; (defmacro atomic (&body body)
;;;   `(let ((outer-interrupts excl::*without-interrupts*)
;;; 	 (excl::*without-interrupts* 0))
;;;      (macrolet ((interrupts-on  ()
;;; 		  '(unless outer-interrupts
;;; 		     (setq excl::*without-interrupts* nil)))
;;; 		(interrupts-off ()
;;; 		  '(setq excl::*without-interrupts* 0)))
;;;        ,.body)))

