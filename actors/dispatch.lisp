;;;-*- Package: :Actors; Syntax: Common-Lisp; Mode: Lisp;  -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based on this software are permitted.  This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Actors)

;;;============================================================

(defun interrupt-key-press? (msg window args)
  (declare (optimize (safety 0) (speed 3))
	   (type Symbol msg)
	   (type xlib:Window window)
	   (type List args))
  (when (eq msg 'key-press)
    (let* ((state (fourth args))
	   (modifiers (xlib:make-state-keys state)))
      (declare (type xlib:Card16 state)
	       (type List modifiers))
      (and (member :control modifiers)
	   (member (xlib:keycode->character
		    (xlib:window-display window) (first args) state)
		   '(#\c #\C #+:excl #\control-\c #+:excl #\control-\C))))))

(defun send-event-msg (msg window &rest args)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Window window))
  (let ((interactor (window-top-level-interactor window)))
    (xlib:discard-current-event (xlib:window-display window))
    (unless (or (null interactor) (typep interactor 'az:Dead-Object))
      ;; then this is an event to a interactor and we can remove it
      ;; from the queue and dispatch the interactor version of the event
      ;; to the appropriate interactor's input queue.
      (apply 
       (if (interrupt-key-press? msg window args) #'send-interrupt #'send-msg)
       msg window interactor (new-id) (copy-list args))))
  t)

(defun dispatch-events (display maxevents)
  (declare (optimize (safety 0) (speed 3))
	   (type xlib:Display display)
	   (type (Integer 0 255) maxevents))
  (let ((nevents 1))
    (declare (type (Integer 0 255) nevents))
    (xlib:event-case
     (display :force-output-p t :peek-p nil :timeout 0 :discard-p nil)
     ;;------------------------------------------------------------
     ;; keyboard and pointer events -------------------------------
     ;;------------------------------------------------------------
     (:button-press
      (window code x y state time root root-x root-y child same-screen-p)
      ;; :button-xxx means one of the pointer (mouse) buttons has changed.
      (send-event-msg 'button-press window code x y state
		      time root root-x root-y child same-screen-p)
      (> (incf nevents) maxevents))
     (:button-release
      (window code x y state time root root-x root-y child same-screen-p)
      ;; :button-xxx means one of the pointer (mouse) buttons has changed.
      (send-event-msg 'button-release window code x y state
		      time root root-x root-y child same-screen-p)
      (> (incf nevents) maxevents))
     (:key-press
      (window code x y state time root root-x root-y child same-screen-p)
      ;; :key-xxx means a key on the keyboard has changed.
      (send-event-msg 'key-press window code x y state
		      time root root-x root-y child same-screen-p)
      (> (incf nevents) maxevents))
     (:key-release
      (window code x y state time root root-x root-y child same-screen-p)
      ;; :key-xxx means a key on the keyboard has changed.
      (send-event-msg 'key-release window code x y state
		      time root root-x root-y child same-screen-p)
      (> (incf nevents) maxevents))

     (:motion-notify
      (window hint-p x y state time root root-x root-y child same-screen-p)
      ;; :motion-notify means the pointer has moved within the window.
      ;; X has complicated options for specifying how many and
      ;; under what conditions :motion-notify events get generated.
      ;; For the moment, we'll try reporting all :motion-notify events.
      (send-event-msg 'motion-notify window hint-p x y state
		      time root root-x root-y child same-screen-p)
      (> (incf nevents) maxevents))

     (:enter-notify
      (window x y mode kind focus-p state time root root-x root-y child
	      same-screen-p)
      ;; This occurs when the pointer enters the window.
      (send-event-msg 'enter-notify window x y mode kind focus-p state
		      time root root-x root-y child same-screen-p)
      (> (incf nevents) maxevents))
     (:leave-notify
      (window x y mode kind focus-p state time root root-x root-y child
	      same-screen-p)
      ;; These occur when the pointer enters or leaves the window.
      (send-event-msg 'leave-notify window x y mode kind focus-p state
		      time root root-x root-y child same-screen-p)
      (> (incf nevents) maxevents))

     ;;------------------------------------------------------------
     ;; input focus events ----------------------------------------
     ;;------------------------------------------------------------
     ((:focus-in :focus-out)
      (event-key window mode kind) 
      ;; "focus" is short for keyboard focus; after :focus-in,
      ;; all keyboard input is directed at this interactor, until :focus-out.
      (send-event-msg (find-symbol (symbol-name event-key) :Actors)
		      window mode kind)
      (> (incf nevents) maxevents))

     ;;------------------------------------------------------------
     ;; keyboard and pointer state events -------------------------
     ;;------------------------------------------------------------
     (:keymap-notify
      (keymap)
      ;; NOTE: there appears to be an error in the CLX docs.
      ;; There is no <window> or <event-window> parameter
      ;; for :keymap-notify, so we can't dispatch it to an Top-Level-Interactor.
      ;; 
      ;; This is sent automatically after an :enter-notify
      ;; or :focus-in event. It reports the current state of the
      ;; keyboard. Perhaps the CLX driver should package
      ;; the initial keyboard state together with the :enter-notify
      ;; or :focus-in event. However, it's not so easy to imagine
      ;; how a interactor would use the information. The keyboard state
      ;; can be queried directly at anytime. And changes in the state
      ;; are signalled with :key-press and :key-release events.
      (warn "~%received ~s, which should be masked off."
	    (list :keymap-notify keymap))
      (> (incf nevents) maxevents))

     (:mapping-notify
      (request start count) 
      ;; (More awful naming, though it's really window "mapping"
      ;; that has the wrong name!)
      ;; In X, these events occur to report changes to the mapping
      ;; between physical buttons and keys and logical buttons and keys.
      ;; There should be no reason for interactors to know about these,
      ;; but the CLX driver should handle them by updating any information
      ;; it caches about the key and button mapping.
      (warn "~%received ~s, which should be masked off."
	    (list :mapping-notify request start count))
      (> (incf nevents) maxevents))
     
     ;;------------------------------------------------------------
     ;; exposure events -------------------------------------------
     ;;------------------------------------------------------------
     (:exposure
      (window x y width height count)
      ;; In X, :exposure events occur whenever a rectangular
      ;; region in a window is exposed to view. They tend to be
      ;; redundant, in that :exposure events tend to come several
      ;; at a time, often together with a :visibility-notify
      ;; or :configure-notify event. For the present, the CLX
      ;; driver will report them to interactors as they happen.
      ;; In the future, we might consider cleaning things up
      ;; by repackaging, for example, a :visibility-notify
      ;; and a following flurry of :exposure events into
      ;; a single interactor event, that contains a list
      ;; of all the rects that need to be repainted.
      (send-event-msg 'exposure window x y width height count))

     (:graphics-exposure
      (event-window x y width height count major minor)
      ;; This is pretty obscure. :graphics-exposure means that someone
      ;; tried to bitblt from a part of a window that was covered
      ;; by another window, so the needed bits are not immediately available.
      (send-event-msg 'graphics-exposure event-window x y width height
		      count major minor)
      (> (incf nevents) maxevents))

     (:no-exposure
      (event-window major minor)
      ;; This is even more obscure. :no-exposure indicates that the source
      ;; region of the bitblt was completely uncovered, ie. all bits were
      ;; available without doing anything. Why would anyone ever need this!?
      (send-event-msg 'no-exposure event-window major minor)
      (> (incf nevents) maxevents))

     ;;------------------------------------------------------------
     ;; window state events ---------------------------------------
     ;;------------------------------------------------------------
     (:circulate-notify
      (event-window window place)
      ;; This means the window has either been moved either in front
      ;; of or behind its siblings in the window hierarchy. I presume
      ;; there will also be :visibility-notify and :exposure events
      ;; that provide the interactor with what it needs to redisplay
      ;; itself and that, therefore, we can ignore these.
      (send-event-msg 'circulate-notify event-window window place)
      (> (incf nevents) maxevents))
      
     (:configure-notify
      (window x y width height border-width above-sibling
	      override-redirect-p)
      ;; This signals a change to a window's size, position,
      ;; border, or stacking order. 
      (send-event-msg 'configure-notify window x y width height
		      border-width above-sibling override-redirect-p)
      (> (incf nevents) maxevents))

     (:create-notify
      (parent window x y width height border-width override-redirect-p)
      ;; This is meant to signal to a client that a window has been
      ;; created. It doesn't make sense to send this to the window's
      ;; Top-Level-Interactor, who can't be active until the window is created.
      ;; It's conceivable that this might be used by the parent window's
      ;; Top-Level-Interactor to wait until the child Top-Level-Interactor's
      ;; window is ready
      ;; before returning from some window creation function.
      (send-event-msg 'create-notify parent window x y width height
		      border-width override-redirect-p)
      (> (incf nevents) maxevents))

     (:destroy-notify
      (event-window window)
      ;; We interpret this to mean that whatever window
      ;; (or hardcopy device or other stream that a interactor
      ;; uses to display itself) is, for some reason, no longer
      ;; available and therefore the interactor should be killed.
      (send-event-msg 'destroy-notify event-window window))

     (:gravity-notify
      (event-window window x y)
      ;; This occurs when a window is moved in response
      ;; to a change of size of its parent, if it has the right
      ;; kind of gravity. 
      (send-event-msg 'gravity-notify event-window window x y)
      (> (incf nevents) maxevents))

     (:map-notify
      (event-window window override-redirect-p)
      ;; "Mapping" is perhaps the most prominent of many examples
      ;; of poorly chosen terminology in X (most of which are due
      ;; to poorly chosen concepts). The clearest evidence of a poor
      ;; concept is when you can't explain it in one sentence, using
      ;; other concepts that are either commonly understood or can
      ;; themselves be explained in one sentence. However, interactors
      ;; want to know about these events, so they can go dormant when
      ;; their windows are unmapped (eg. iconified) and wake up when
      ;; the window is mapped. 
      (send-event-msg 'map-notify event-window window override-redirect-p)
      (> (incf nevents) maxevents))

     (:reparent-notify
      (event-window window parent x y override-redirect-p)
      ;; This will probably never happen to an interactor's window. 
      (send-event-msg 'reparent-notify event-window window parent x y
		      override-redirect-p)
      (> (incf nevents) maxevents))

     (:unmap-notify
      (event-window window configure-p)
      ;; "Mapping" is perhaps the most prominent of many examples
      ;; of poorly chosen terminology in X (most of which are due
      ;; to poorly chosen concepts). The clearest evidence of a poor
      ;; concept is when you can't explain it in one sentence, using
      ;; other concepts that are either commonly understood or can
      ;; themselves be explained in one sentence. However, interactors
      ;; want to know about these events, so they can go dormant when
      ;; their windows are unmapped (eg. iconified) and wake up when
      ;; the window is mapped. 
      (send-event-msg 'unmap-notify event-window window configure-p)
      (> (incf nevents) maxevents))

     (:visibility-notify
      (window state)
      ;; This event is generated whenever the visibility of a window
      ;; changes. It seems to be redundant, because it's always (?)
      ;; accompanied by :exposure events. In the future, we might
      ;; consider cleaning things up by repackaging, for example,
      ;; a :visibility-notify and a following flurry of :exposure
      ;; events into a single interactor event that contains a list
      ;; of all the rects that need to be repainted.
      (send-event-msg 'visibility-notify window state)
      (> (incf nevents) maxevents))

     ;;------------------------------------------------------------
     ;; Structure control events ----------------------------------
     ;;------------------------------------------------------------
     (:circulate-request
      (parent window place)
      ;; This event is supposed to be handled by window managers,
      ;; who get to decide whether windows actually get circulated or not.
      ;; It shouldn't be relevant to interactors. 
      (send-event-msg 'circulate-request parent window place))

     (:colormap-notify
      (window colormap new-p installed-p)
      ;; An Interactor might want to receive these,
      ;; to ensure that its colormap is installed. 
      (send-event-msg 'colormap-notify window colormap new-p installed-p)
      (> (incf nevents) maxevents))

     (:configure-request
      (parent window x y width height border-width stack-mode
	      above-sibling value-mask)
      ;; In X, :configure-request is generated when one client
      ;; attempts to change the configuration of (another) client`s
      ;; window. :configure-request events are usually handled
      ;; by a window manager, who can modify or refuse the request.
      ;; We'll ignore these for now, but we might want to include them
      ;; in the future, so that interactors can prevent themselves from
      ;; being reshaped, if they so desire.
      (send-event-msg 'configure-request parent window x y width height
		      border-width stack-mode above-sibling value-mask)
      (> (incf nevents) maxevents))

     (:map-request
      (parent window)
      ;; This is usually handled by the window manager, who can revise
      ;; the size or position of the window before mapping it. 
      (send-event-msg 'map-request parent window)
      (> (incf nevents) maxevents))
      
     (:resize-request
      (window width height) 
      ;; Like :configure-request, these are meant to be handled
      ;; by the window manager. Interactors can use these to prevent
      ;; themselves from being reshaped, if they so desire.
      (send-event-msg 'resize-request window width height)
      (> (incf nevents) maxevents))

     ;;------------------------------------------------------------
     ;; Client communication events -------------------------------
     ;;------------------------------------------------------------
     (:client-message
      (window type format data)
      ;; These are random events sent by one client to another.
      ;; we can't ignore them completely, because, for example,
      ;; delete-window is handled by having the window manager
      ;; send a client message to the window to be deleted.
      ;; It's then up to the client that owns the window
      ;; to actually destroy the window.
      ;; It's probably better if these get translated
      ;; by the CLX device driver into more meaningful interactor events
      ;; (eg. :destroy-request). To really get this straight,
      ;; we'll have to dig into the ICCCM.
      (send-event-msg 'client-message window type format data)
      (> (incf nevents) maxevents))

     (:property-notify
      (window atom state time) 
      ;; This indicates that some random property of a window has changed.
      (send-event-msg 'property-notify window atom state time)
      (> (incf nevents) maxevents))

     (:selection-clear
      (window selection time) 
      (send-event-msg 'selection-clear window selection time)
      (> (incf nevents) maxevents))
     (:selection-notify
      (window selection target property time) 
      (send-event-msg 'selection-notify window selection target property time)
      (> (incf nevents) maxevents))
     (:selection-request
      (window requestor selection target property time)
      (send-event-msg 'selection-request window requestor selection
		      target property time)
      (> (incf nevents) maxevents))

     ;;------------------------------------------------------------
     ;; Unknown events --------------------------------------------
     ;;------------------------------------------------------------
     (otherwise
      (event-key)
      (warn "~%received, an unexpected CLX event." event-key)))))

