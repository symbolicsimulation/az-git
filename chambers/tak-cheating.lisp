(proclaim '(optimize speed (space 0) (safety 0) (compilation-speed 0)))

(defun tak (x y z)
  (declare (fixnum x y z))
  (if (< y x)
      (tak (tak (- x 1) y z)
	   (tak (- y 1) z x)
	   (tak (- z 1) x y))
      z))
(proclaim '(function tak (fixnum fixnum fixnum) fixnum))

(defun tak-it ()
  (if (/= (tak 18 12 6) 7) (error "tak results are incorrect")))

(defun tak-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
      (tak-it)))
