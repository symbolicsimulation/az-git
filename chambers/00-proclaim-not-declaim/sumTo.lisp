(defun sumTo (self x)
  (do ((i self (+ i 1))
       (sum 0 (+ sum i)))
      ((> i x) sum)))

(defun sumTo-it ()
  (do ((i 1 (+ i 1)))
      ((> i 100) nil)
    (sumTo 1 10000)))

(defun sumTo-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (sumTo-it)))
