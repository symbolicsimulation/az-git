(defun recurse (x)
  (if (> x 0)
      (progn
       (recurse (- x 1))
       (recurse (- x 1)))
      nil))

(defun recurse-it ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (recurse 14)))

(defun recurse-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (recurse-it)))
