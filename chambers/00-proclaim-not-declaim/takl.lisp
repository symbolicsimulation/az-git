(defun listn (n)
  (if (= n 0) nil
      (cons n (listn (- n 1)))))

(defun shorterp (x y)
  (and (not (eq y nil))
       (or (eq x nil)
	   (shorterp (cdr x) (cdr y)))))

(defun takl (x y z)
  (if (shorterp y x)
      (takl (takl (cdr x) y z)
	    (takl (cdr y) z x)
	    (takl (cdr z) x y))
      z))

(defun takl-it ()
  (if (not (equal (takl (listn 18) (listn 12) (listn 6)) (listn 7)))
      (error "takl results are incorrect")))

(defun takl-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (takl-it)))
