(proclaim '(optimize speed (space 0) (safety 0) (compilation-speed 0)))

(defun recurse (x)
  (declare (fixnum x))
  (if (> x 0)
      (progn
       (recurse (- x 1))
       (recurse (- x 1)))
      nil))

(defun recurse-it ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
      (recurse 14)))

(defun recurse-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
      (recurse-it)))
