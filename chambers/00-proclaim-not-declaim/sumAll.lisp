(defconstant sumAll-v (make-array (list 100000) :initial-element 5))

(defun sumAll (v)
  (do ((i 0 (+ i 1))
       (sum 0 (+ sum (svref v i))))
      ((>= i (array-dimension v 0)) sum)))

(defun sumAll-it ()
  (sumAll sumAll-v))

(defun sumAll-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (sumAll-it)))
