(defconstant atAllPut-v (make-array (list 100000)))

(defun atAllPut (v x)
  (do ((i 0 (+ i 1)))
      ((>= i (array-dimension v 0)) nil)
    (setf (svref v i) x)))

(defun atAllPut-it ()
  (atAllPut atAllPut-v 7))

(defun atAllPut-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (atAllPut-it)))
