(proclaim '(optimize speed (space 0) (safety 0) (compilation-speed 0)))

(defconstant atAllPut-v (make-array (list 100000)))
(proclaim '(type (simple-vector fixnum) atAllPut-v))

(defun atAllPut (v x)
  (declare (type (simple-vector fixnum) v) (fixnum x))
  (do ((i 0 (+ i 1)))
      ((>= i (array-dimension v 0)) nil)
      (declare (fixnum i))
      (setf (svref v i) x)))

(defun atAllPut-it ()
  (atAllPut atAllPut-v 7))

(defun atAllPut-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
      (atAllPut-it)))
