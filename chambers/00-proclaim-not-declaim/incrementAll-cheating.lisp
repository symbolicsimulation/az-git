(proclaim '(optimize speed (space 0) (safety 0) (compilation-speed 0)))

(defconstant incrAll-v (make-array (list 100000) :initial-element 5))
(proclaim '(type (simple-vector fixnum) incrAll-v))

(defun incrementAll (v)
  (declare (type (simple-vector fixnum) v))
  (do ((i 0 (+ i 1)))
      ((>= i (array-dimension v 0)) nil)
      (declare (fixnum i))
      (setf (svref v i) (+ (svref v i) 1))))

(defun incrementAll-it ()
  (incrementAll incrAll-v))

(defun incrementAll-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
      (incrementAll-it)))
