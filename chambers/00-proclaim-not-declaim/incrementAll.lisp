(defconstant incrAll-v (make-array (list 100000) :initial-element 5))

(defun incrementAll (v)
  (do ((i 0 (+ i 1)))
      ((>= i (array-dimension v 0)) nil)
    (setf (svref v i) (+ (svref v i) 1))))

(defun incrementAll-it ()
  (incrementAll incrAll-v))

(defun incrementAll-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (incrementAll-it)))
