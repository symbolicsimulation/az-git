(defun sumFromTo (self l x)
  (do ((i l (+ i 1))
       (sum self (+ sum i)))
      ((> i x) sum)))

(defun sumFromTo-it ()
  (do ((i 1 (+ i 1)))
      ((> i 100) nil)
    (sumFromTo 0 1 10000)))

(defun sumFromTo-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (sumFromTo-it)))
