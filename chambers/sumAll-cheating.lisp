(proclaim '(optimize speed (space 0) (safety 0) (compilation-speed 0)))

(defconstant sumAll-v (make-array (list 100000) :initial-element 5))
(proclaim '(type (simple-vector fixnum) sumAll-v))

(defun sumAll (v)
  (declare (type (simple-vector fixnum) v))
  (do ((i 0 (+ i 1))
       (sum 0 (+ sum (svref v i))))
      ((>= i (array-dimension v 0)) sum)
      (declare (fixnum i sum))))


(defun sumAll-it ()
  (sumAll sumAll-v))

(defun sumAll-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
      (sumAll-it)))
