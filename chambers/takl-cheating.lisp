(proclaim '(optimize speed (space 0) (safety 0) (compilation-speed 0)))

(defun listn (n)
  (declare (fixnum n))
  (if (= n 0) nil
      (cons n (listn (- n 1)))))
(proclaim '(function listn (fixnum) list))

(defun shorterp (x y)
  (declare (list x y))
  (and (not (eq y nil))
       (or (eq x nil)
	   (shorterp (cdr x) (cdr y)))))

(defun takl (x y z)
  (declare (list x y z))
  (if (shorterp y x)
      (takl (takl (cdr x) y z)
	    (takl (cdr y) z x)
	    (takl (cdr z) x y))
      z))
(proclaim '(function takl (list list list) list))

(defun takl-it ()
  (if (not (equal (takl (listn 18) (listn 12) (listn 6)) (listn 7)))
      (error "takl results are incorrect")))

(defun takl-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
      (takl-it)))
