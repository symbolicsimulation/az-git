(proclaim '(optimize speed (space 0) (safety 0) (compilation-speed 0)))

;; random number stuff
(defvar seed 0)
(proclaim '(fixnum seed))
(defun initrand () (setq seed 74555))
(defun rand ()
  (setq seed (logand (+ (* seed 1309) 13849) 65535))
  seed)
(proclaim '(function rand () fixnum))
(proclaim '(inline rand))

;; array declarations
(defconstant tree-sortelements 5000)
(defconstant tree-sortlist (make-array (list (+ tree-sortelements 1))))
(proclaim '(type (simple-vector fixnum) tree-sortlist))

;; initialization stuff
(defvar biggest 0)
(defvar littlest 0)
(proclaim '(fixnum biggest littlest))

(defun initarr ()
  (initrand)
  (setq biggest -1000000)
  (setq littlest 1000000)
  (do ((i 1 (+ i 1)))
      ((> i tree-sortelements) nil)
      (declare (fixnum i))
      (let ((temp (rand)))
	(declare (fixnum temp))
	(setf (svref tree-sortlist i)
	      (- (- temp (* (floor temp 100000) 100000)) 50000))
	(if (> (svref tree-sortlist i) biggest)
	    (setq biggest (svref tree-sortlist i)))
	(if (< (svref tree-sortlist i) littlest)
	    (setq littlest (svref tree-sortlist i))))))

(defstruct node left right (val :type fixnum))

(defun create-node (n) (make-node :val n))
(proclaim '(function create-node (fixnum) node))
(proclaim '(inline create-node))

(defun insert (n tr)
  (declare (fixnum n) (type node tr))
  (cond ((> n (node-val tr))
	 (if (null (node-left tr))
	     (setf (node-left tr) (create-node n))
	     (insert n (node-left tr))))
	((< n (node-val tr))
	 (if (null (node-right tr))
	     (setf (node-right tr) (create-node n))
	     (insert n (node-right tr))))
	(t (error "adding duplicate value to tree")))
  nil)

(defun check-tree (tr)
  (declare (type node tr))
  (let ((result t))
    (if (not (null (node-left tr)))
	(if (<= (node-val (node-left tr)) (node-val tr))
	    (setq result nil)
	  (setq result (and (check-tree (node-left tr)) result))))
    (if (not (null (node-right tr)))
	(if (>= (node-val (node-right tr)) (node-val tr))
	    (setq result nil)
	  (setq result (and (check-tree (node-right tr)) result))))
    result))

(defun tree ()
  (initarr)
  (let ((tr (create-node (svref tree-sortlist 1))))
    (declare (type node tr))
    (do ((i 2 (+ i 1)))
	((> i tree-sortelements) nil)
	(declare (fixnum i))
      (insert (svref tree-sortlist i) tr))
    (if (not (check-tree tr)) (error "tree")))
  nil)

(defun tree-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
      (declare (fixnum i))
    (tree)))
