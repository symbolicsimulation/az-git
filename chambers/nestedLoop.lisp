(defun nestedLoop ()
  (let ((sum 0))
    (do ((i 1 (+ i 1)))
	((> i 100) sum)
	(do ((j 1 (+ j 1)))
	    ((> j 100) nil)
	    (setq sum (+ sum 1))))))

(defun nestedLoop-it ()
  (do ((i 1 (+ i 1)))
      ((> i 100) nil)
    (nestedLoop)))

(defun nestedLoop-test ()
  (do ((i 1 (+ i 1)))
      ((> i 10) nil)
    (nestedLoop-it)))
