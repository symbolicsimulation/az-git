;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================
;;; Geometric Objects
;;;============================================================

(defclass Geometric-Object (Standard-Object)
	  ((home
	    :type T
	    :accessor home
	    :initarg :home
	    :documentation
	    "The home space of the object."))  
  (:documentation
   "This is a root class for all geometric objects which correspond to
subsets of some space.  It should not be instantiated.  All geometric objects
have a location, ie., x and y coordinates, and a home, which is where
they ``live''."))

;;;-------------------------------------------------------
;;; Metrics
;;;-------------------------------------------------------

(defgeneric l2-dist2 (g0 g1)
  #-sbcl (declare (type Geometric-Object g0 g1))
  (:documentation "The squared l2 distance between the two objects."))

(defgeneric l2-dist (g0 g1)
  #-sbcl (declare (type Geometric-Object g0 g1))
  (:documentation "The l2 distance between the two objects."))

(defmethod l2-dist ((g0 Geometric-Object) (g1 Geometric-Object))
  "The default method just takes the square root of l2-dist2."
  (sqrt (l2-dist2 g0 g1)))

(defgeneric l1-dist (g0 g1)
  #-sbcl (declare (type Geometric-Object g0 g1))
  (:documentation "The l1 distance between the two objects."))

(defgeneric sup-dist (g0 g1)
  #-sbcl (declare (type Geometric-Object g0 g1))
  (:documentation "The sup distance between the two objects."))

;;;------------------------------------------------------------

(defgeneric %intersect? (home o0 o1)
  #-sbcl (declare (type Flat-Space home)
		  (type Geometric-Object o0 o1))
  (:documentation
   "Do the two geometric objects which live in <home>  intersect?"))

(defgeneric intersect? (o0 o1)
  #-sbcl (declare (type Geometric-Object o0 o1))
  (:documentation
   "Do the two geometric objects intersect?"))

(defmethod intersect? ((o0 Geometric-Object) (o1 Geometric-Object))
  (and (eq (home o0) (home o1))
       (%intersect? (home o0) o0 o1)))

(defgeneric intersect2 (o0 o1 &key result)
  #-sbcl (declare (type Geometric-Object o0 o1 result)
		  (:returns (type (or Null Geometric-Object))))
  (:documentation
   "Compute the intersection of the two geometric objects.
Return it in <result>, unless the intersection is empty,
in which case return nil. If <result> is not of a correct type
to hold the intersection, signal an error."))
