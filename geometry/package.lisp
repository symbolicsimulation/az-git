;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: User -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :cl-user)

(defpackage :Geometry
  (:use :Common-Lisp #+:clos :CLOS)
  (:nicknames :G)
  (:export 
   ;; spaces:
   element?
   make-element
   random-element
   random-point-in-rect
   borrow-element
   return-element
   with-borrowed-element
   with-borrowed-elements
   flat-space?
   vector-space?

   Flat-Space
   translation-space
   dimension
   make-rect
   possible-point-rect
   random-rect
   borrow-rect
   return-rect
   with-borrowed-rect
   with-borrowed-rects
	    
   Affine-Space

   Vector-Space
   dual-space
   zero-element? 
   make-zero-element
   make-canonical-basis-element
   borrow-canonical-basis-element
   with-canonical-basis-element

   Array-Space
   Rn
   Byte-Flat
   Byte-Grid
   Byte-Lattice
   Int-Flat
   Int-Grid
   Int-Lattice
	    
   *screen-vector-space*
   *screen-space*

    
   Direct-Sum-Space
   space0
   space1
   direct-sum
   Direct-Sum-Point

   ;; points, vectors and other geometric objects
   Geometric-Object
   home
   intersect?
   intersect2

   Point
   Point-List
   coordinate
   coords
   x
   y
   w
   h
   fill-randomly!
   affine-mix
   sub
   add
   l2-dist2
   l2-dist
   l1-dist
   sup-dist
   convex-mix

   zero!
   scale
   linear-mix
   negate
   Vector
   inner-product
   l2-norm2
   l2-norm
   l1-norm
   sup-norm
   dual-vector
   project
   embed
   subspace?
   proper-subspace?
   spanning-space

   Point-Set
   points
   Point-Cloud
	    
   Rect
   Rect-List
   xmax ymax
   left inner-left outer-left
   right inner-right outer-right
   top inner-top outer-top
   bottom inner-bottom outer-bottom
   origin
   extent

   Flat-Map
   make-linear-map
   domain
   codomain
   transform 
   inverse-transform
   transpose
   pseudo-inverse
   identity!
   tensor-product

   Affine-Map
   dom-origin
   jacobian
   cod-origin
   Diagonal-Affine-Map
   dom-extent
   cod-extent
   update-caches
   set-grid-map
   Int-Grid-Map
   Int-Grid-Flip
   Int-Float-Flip


   Matrix
   read-matrix-from-file

   Gauss
   Givens
   Householder
   Pivot
	    
   compose
   rotate-map

   ;; decompositions
   inverse-lu-decompose
   inverse-qr-decompose
   qr-decompose
   inverse-lq-decompose
   lq-decompose
   cholesky-decompose
   eigen-decompose
   singular-value-decompose 

   determinant


   iclip

   QR-Decomposition
   qr-solve
   qr-inverse
	    
   Tics
   tic-min
   tic-max
   tic-inc
   tic-n
   tic-values
   tic-labels
   tic-length
   get-nice-tics
   nice-rect))
