;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================

(defclass Point-Set (Geometric-Object)
	  ((points
	    :type Sequence
	    :accessor points
	    :initarg :points))
  (:documentation
   "A set of points in some space. The set is represented by a
sequence whose elements are Point objects."))


