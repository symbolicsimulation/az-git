;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================
;;;		     Point
;;;=======================================================

(defclass Point (Geometric-Object)
	  ((coords
	    :type Simple-Array
	    :accessor coords
	    :initarg :coords
	    :documentation "The coordinates of this point."))
  (:documentation "A class for elements of Spaces."))

;;;-------------------------------------------------------
;;; Related types
;;;-------------------------------------------------------

(defun point-list? (list)
  (and (listp list)
       (every #'(lambda (p) (typep p 'Point))
	      list)))

(deftype Point-List () '(satisfies point-list?))

;;;-------------------------------------------------------
;;; Basic object protocol:
;;;-------------------------------------------------------

(defmethod print-object ((p Point) stream)
  (if (and (slot-boundp p 'coords) (slot-boundp p 'home))
    (let ((*print-array* t))
      #-sbcl (declare (special *print-array*))
      (format stream "#<P ~a ~a>" (coords p) (home p)))
    (az:printing-random-thing (p stream) (format stream "P"))))

(defmethod az:copy ((p Point) &key (result (make-element (home p))))
  (declare (type Point p result)
	   (:returns (type Point result)))
  (assert (eq (home p) (home result)))
  (az:copy (coords p) :result (coords result))
  result)

(defmethod az:verify-copy-result? ((p0 Point) (p1 Point))
  (eq (home p0) (home p1)))
  
(defmethod az:copy-to! ((p0 Point) (result Point))
  (az:copy (coords p0) :result (coords result))
  result)

(defmethod az:equal? ((p0 Point) (p1 Point))
  "The equality predicate for Points."
  (declare (type Point p0 p1)
	   (:returns (type (Member T Nil))))
  (az:equal? (coords p0) (coords p1)))

;;;-------------------------------------------------------
;;; Coordinate Accessors
;;;-------------------------------------------------------

(defgeneric coord-start (p)
  #-sbcl (declare (type Point p))
  (:documentation
   "The range of non-zero coordinates for sparse representations:
 
The coordinates from 0 below <start> are implicit zeros.
The coordinates from <end> below (dimension (parent-space p0))
are implicit zeros."))

(defmethod coord-start ((p Point))
  "Forward to the <home> space."
  (coord-start (home p)))

(defgeneric coord-end (p)
  (:documentation
   "The range of non-zero coordinates for sparse representations:
 
The coordinates from 0 below <start> are implicit zeros.
The coordinates from <end> below (dimension (parent-space p0))
are implicit zeros."))

(defmethod coord-end ((p Point)) 
  "Forward to the <home> space."
  (coord-end (home p)))
 
(defgeneric coordinate (p i))
(defmethod coordinate ((p Point) i)
  (aref (coords p) (- i (coord-start p))))

(defgeneric (setf coordinate) (new p i))
(defmethod (setf coordinate) (new (p Point) i)
  (setf (aref (coords p) (- i (coord-start p))) new))

;;;-------------------------------------------------------

(declaim (inline %x))
(defun %x (p)
  (declare (type Point p)
	   (:returns (type Real)))
  (aref (coords p) 0))

(defgeneric x (p)
  #-sbcl (declare (type Point p)
		  (:returns (type Real)))
  (:documentation "The x coordinate of the location of the p."))

(defmethod x ((p Point))
  (declare (type Point p)
	   (:returns (type Real)))
  (%x p))

(defsetf %x (p) (value)
  `(setf (aref (coords ,p) 0) ,value))

(defgeneric (setf x) (value p)
  #-sbcl (declare (type Real value)
		  (type Point p)
		  (:returns (type Real value)))
  (:documentation "Set the x coordinate of p."))

(defmethod (setf x) (value (p Point))
  (declare (type Real value)
	   (type Point p)
	   (:returns (type Real value)))
  (setf (%x p) value))

;;;-------------------------------------------------------

(declaim (inline %y))
(defun %y (p)
  (declare (type Point p)
	   (:returns (type Real)))
  (aref (coords p) 1))

(defgeneric y (p)
  #-sbcl (declare (type Point p)
		  (:returns (type Real)))
  (:documentation "The y coordinate of the location of the p."))

(defmethod y ((p Point))
  (declare (type Point p)
	   (:returns (type Real)))
  (%y p))

(defsetf %y (p) (value)
  `(setf (aref (coords ,p) 1) ,value))

(defgeneric (setf y) (value p)
  #-sbcl (declare (type Real value)
		  (type Point p)
		  (:returns (type Real value)))
  (:documentation "Set the y coordinate of p."))

(defmethod (setf y) (value (p Point))
  (declare (type Real value)
	   (type Point p)
	   (:returns (type Real value)))
  (setf (%y p) value))
 
;;;-------------------------------------------------------
;;; Construction
;;;-------------------------------------------------------

(defgeneric fill-randomly! (x &key min max)
  #-sbcl (declare (type (or Array Point) x)))

(defmethod fill-randomly! ((p0 Point) &key (min 0.0d0) (max 1.0d0))
  (fill-randomly! (coords p0) :min min :max max)
  p0)

;;;-------------------------------------------------------
;;; Relationship to Spaces
;;;-------------------------------------------------------

(defmethod dimension ((p0 Point)) (dimension (home p0)))

;;;-------------------------------------------------------

(defgeneric element? (p sp))

(defmethod element? ((p Point) (sp Flat-Space)) (eq (home p) sp))

(defmethod element? ((p Point) (space Vector-Space))
  (subspace? (home p) space))

;;;-------------------------------------------------------

(defmethod parent-space ((p Point)) (parent-space (home p)))

;;;-------------------------------------------------------

(defgeneric return-element (p sp &optional errorp)
  #-sbcl (declare (type Point p)
		  (type Flat-Space sp)))

(defmethod return-element ((p Point) (sp Flat-Space)
			   &optional (errorp t))
  
  ;; This is an example of the slightly confusing analogy between
  ;; spaces and classes. It seems too bad that we have to explicitly test
  ;; <v> for membership in <sp> rather than just using dispatching the
  ;; dispatching mechanism somehow. The problem is that objects know what
  ;; class they are an instance of, but <v> can't know what vector space
  ;; it's a member of, because it might be a member  of more than one
  ;; vector space.
  
  (if (element? p sp)
    (push p (element-resource sp))
    ;; else
    (when errorp
      (error "Trying to return ~a to the resource of ~a, ~%~
              but ~a is not a member of ~a."
	     p sp p sp))))

(defmacro with-borrowed-element ((name sp) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-element ,sp))
	    (,name ,return-name))
       (multiple-value-prog1
	 (progn ,@body)
	 (return-element ,return-name ,sp)))))

(defmacro with-borrowed-elements (specs &body body)
  (if (= (length specs) 1)
      `(with-borrowed-element ,(first specs)
	 ,@body)
      `(with-borrowed-element ,(first specs)
	 (with-borrowed-elements ,(rest specs)
	   ,@body))))

;;;------------------------------------------------------
;;; Algebra
;;;------------------------------------------------------

(defgeneric linear-mix (a0 p0 a1 p0 &key result)
  #-sbcl (declare (type Real a0 a1)
		  (type (or Array Point) p0 p1 result))
  (:documentation "Compute the linear combination of two points."))

(defmethod linear-mix :before ((a0 Number) (p0 Point) (a1 Number) (p1 Point)
			       &key (result (make-element (home p0))))
  (assert (typep (home p0) 'Vector-Space))
  (assert (eq (home p0) (home p1)))
  (assert (eq (home p0) (home result))))

(defmethod linear-mix ((a0 Number) (p0 Point) (a1 Number) (p1 Point)
		       &key (result (make-element (home p0))))
  (declare (type Point result))
  (linear-mix a0 (coords p0) a1 (coords p1) :result (coords result))
  result)

;;;------------------------------------------------------

(defgeneric add (p0 p1 &key result)
  #-sbcl (declare (type (or Array Point) p0 p1 result))
  (:documentation "Add two points, overwriting <result>,
which may be eq to either <p0> or <p1>."))

(defmethod add :before ((p0 Point) (p1 Point)
			&key (result (make-element (home p0))))
  (let ((home0 (home p0))
	(home1 (home p1))
	(homer (home result)))
    (etypecase home0
      (Vector-Space
       (assert (and (eq home0 (translation-space home1))
		    (eq home1 homer))))
      (Affine-Space
       (assert (and (eq (translation-space home0) home1)
		    (eq home0 homer)))))))
     
(defmethod add ((p0 Point) (p1 Point) &key (result (make-element (home p0))))
  (declare (type Point p0 p1 result))
  (add (coords p0) (coords p1) :result (coords result))
  result)

;;;------------------------------------------------------

(defgeneric sub (p0 p1 &key result)
  #-sbcl (declare (type (or Array Point) p0 p1 result)))

(defmethod sub :before ((p0 Point) (p1 Point)
			&key (result (make-element (home p0))))
  (let ((home0 (home p0))
	(home1 (home p1))
	(homer (home result)))
    (etypecase home1
      (Vector-Space
       (assert (and (eq home1 (translation-space home0))
		    (eq home0 homer))))
      (Affine-Space
       (assert (and (eq home0 home1)
		    (eq (translation-space home0) homer)))))))

(defmethod sub ((p0 Point) (p1 Point)
		&key (result (make-element (translation-space (home p0)))))
  (declare (type Point p0 p1 result))
  (sub (coords p0) (coords p1) :result (coords result))
  result)

;;;------------------------------------------------------

(defgeneric scale (a p &key result)
  #-sbcl (declare (type Real a)
		  (type (or Array Point) p result)))

(defmethod scale :before ((a Number) (p Point) 
			  &key (result (make-element (home p))))
  (assert (typep (home p) 'Vector-Space))
  (assert (eq (home p) (home result))))

(defmethod scale ((a Number) (p Point) &key (result (make-element (home p))))  
  (scale a (coords p) :result (coords result))
  result)

(defgeneric zero! (p)
  #-sbcl (declare (type (or Array Point) p)
		  (:returns (type Point p)))
  (:documentation 
   "Destructively zero the point <p>. 
This is only strictly correct for vectors."))

(defmethod zero! ((p Point))
  (zero! (coords p))
  p)

(defgeneric negate (p &key result)
  #-sbcl (declare (type (or Array Point) p result))
  (:documentation
   "Return the negative of <p> in <result>.
This is only strictly correct for vectors."))

(defmethod negate :before ((p Point) &key (result (make-element (home p))))
  (assert (typep (home p) 'Vector-Space))
  (assert (eq (home p) (home result))))

(defmethod negate ((p Point) &key (result (make-element (home p))))
  (negate (coords p) :result (coords result))
  result)

;;;------------------------------------------------------

(defgeneric affine-mix (a0 p0 a1 p1 &key result)
  #-sbcl (declare (type Real a0 a1)
		  (type (or Array Point) p0 p1 result))
  (:documentation
   "Compute the indicated affine combination and return it in result. 
a0 + a1 must equal 1, to some reasonable precision."))

(defmethod affine-mix :before (a0 (p0 Point) a1 (p1 Point)
			       &key (result (make-element (home p0))))
  (assert (= 1.0d0 (+ a0 a1)))
  (assert (typep (home p0) 'Flat-Space))
  (assert (eq (home p0) (home p1)))
  (assert (eq (home p0) (home result))))

(defmethod affine-mix (a0 (p0 Point) a1 (p1 Point)
		       &key (result (make-element (home p0))))
  (declare (type Real a0 a1)
	   (type Point p0 p1 result))
  (linear-mix a0 (coords p0) a1 (coords p1) :result (coords result))
  result)

;;;------------------------------------------------------

(defgeneric convex-mix (a0 p0 a1 p1 &key result)
  #-sbcl (declare (type Real a0 a1)
		  (type (or Array Point) p0 p1 result))
  (:documentation
   "Compute the indicated convex combination and return it in result. 
a0 and a1 must be positive and a0 + a1 must equal 1, to some reasonable
precision."))

(defmethod convex-mix :before ((a0 Number) (p0 Point) (a1 Number) (p1 Point)
			       &key (result (make-element (home p0))))
  (assert (and (<= 0.0d0 a0 1.0d0)
	       (<= 0.0d0 a1 1.0d0)))
  (assert (= 1.0d0 (+ a0 a1)))
  (assert (typep (home p0) 'Flat-Space))
  (assert (eq (home p0) (home p1)))
  (assert (eq (home p0) (home result))))

(defmethod convex-mix ((a0 Number) (p0 Point) (a1 Number) (p1 Point)
		       &key (result (make-element (home p0))))
  (linear-mix a0 (coords p0) a1 (coords p1) :result (coords result))
  result)

;;;-------------------------------------------------------
;;; Metrics
;;;-------------------------------------------------------

(defmethod l2-dist2 ((p0 Point) (p1 Point))
  (l2-dist2 (coords p0) (coords p1)))

(defmethod l1-dist ((p0 Point) (p1 Point))
  (l1-dist (coords p0) (coords p1)))

(defmethod sup-dist ((p0 Point) (p1 Point))
  (sup-dist (coords p0) (coords p1)))

;;;-------------------------------------------------------

(defmethod dual-vector ((p0 Point) &key result)
  (cond
   ((eq p0 result) result)
   ((null result) (az:copy p0))
   (t (az:copy p0 :result result))))

;;;-------------------------------------------------------

(defmethod fill-with-canonical-basis-vector! ((p0 Point) i)
  (zero! p0)
  (setf (coordinate p0 i) 1.0d0)
  p0)

;;;-------------------------------------------------------
;;; Metrics
;;;-------------------------------------------------------

(defgeneric inner-product (p0 p1)
  #-sbcl (declare (type Point p0 p1)))

(defmethod inner-product ((p0 Point) (p1 Point))
  (inner-product (coords p0) (coords p1)))

(defgeneric l2-norm2 (p)
  #-sbcl (declare (type Point p))
  (:documentation "The squared l2 norm of <p>."))

(defmethod l2-norm2 ((p Point))
  (l2-norm2 (coords p)))

(defgeneric l2-norm (p)
  #-sbcl (declare (type Point p))
  (:documentation "The l2 norm of <p>."))

(defmethod l2-norm ((p Point))
  "The default method just takes the square root of l2-norm2."
  (sqrt (l2-norm p)))

(defgeneric l1-norm (p)
  #-sbcl (declare (type Point p))
  (:documentation "The l1 norm of <p>."))

(defmethod l1-norm ((p Point))
  (l1-norm (coords p)))

(defgeneric sup-norm (p)
  #-sbcl (declare (type Point p))
  (:documentation "The sup norm of <p>."))

(defmethod sup-norm ((p Point))
  (sup-norm (coords p)))

;;;------------------------------------------------------------

(defmethod project ((p0 Point) (sp Vector-Space)
		    &key (result (make-zero-element sp)))
  (assert (eq (home p0) sp))
  (assert (eq (home result) sp))
  (az:copy p0 :result result))

(defmethod project ((p0 Point) (sp Block-Subspace)
		    &key (result (make-zero-element sp)))
  (assert (subspace? sp (home p0)))
  (assert (eq (home result) sp))
  (let ((s (- (coord-start result) (coord-start p0)))
	(e (- (coord-end result) (coord-start p0))))
    (v<-v! (coords result) (coords p0) :end0 (- e s) :start1 s :end1 e))
  result)

;;;------------------------------------------------------------

(defmethod embed ((p0 Point) (sp Vector-Space)
		  &key (result (make-zero-element sp)))
  (assert (element? p0 sp))
  (assert (eq (home result) sp))
  (let* ((s (coord-start p0))
	 (e (coord-end p0))
	 (n (- e s))
	 (d (dimension result))
	 (1d (coords result)))
    (when (> s 0) (v<-x! 1d 0.0d0 :end s))
    (v<-v! 1d (coords p0) :start0 s :end0 e :end1 n)
    (when (> d e) (v<-x! 1d 0.0d0 :start e :end d)))
  result)

(defmethod embed ((p0 Point) (sp Block-Subspace)
		  &key (result (make-zero-element sp)))
  (assert (element? p0 sp))
  (assert (eq (home result) sp))
  (let* ((s (- (coord-start p0) (coord-start result)))
	 (e (- (coord-end p0) (coord-start result)))
	 (n (- e s))
	 (d (dimension result))
	 (1d (coords result)))
    (when (> s 0) (v<-x! 1d 0.0d0 :end s))
    (v<-v! 1d (coords p0) :start0 s :end0 e :end1 n)
    (when (> d e) (v<-x! 1d 0.0d0 :start e :end d)))
  result)

;;;============================================================
;;; Classes for elements of subspaces
;;;============================================================

(defclass SubPoint (Point) ()
  (:documentation
   "A parent class for sparse representations of elements of
certain subspaces."))

;;;------------------------------------------------------------

(defmethod linear-mix ((a0 Number) (p0 Subpoint) (a1 Number) (p1 Subpoint)
		       &key (result (make-element
				     (spanning-space (home p0) (home p1)))))  
  (let ((p0-borrowed? nil)
	(p1-borrowed? nil)
	(result-space (home result)))
    (unless (eq (home p0) result-space)
      (setf p0-borrowed? t)
      (setf p0 (embed p0 result-space :result (borrow-element result-space))))
    (unless (eq (home p1) result-space)
      (setf p1-borrowed? t)
      (setf p1 (embed p1 result-space :result (borrow-element result-space))))
    (linear-mix a0 (coords p0) a1 (coords p1) :result (coords result))
    (when p0-borrowed? (return-element p0 result-space))
    (when p1-borrowed? (return-element p1 result-space)))
  result)

;;;------------------------------------------------------------

(defmethod sub ((p0 Subpoint) (p1 Subpoint)
		&key (result
		      (make-element (spanning-space (home p0) (home p1)))))
  (linear-mix 1.0d0 p0 -1.0d0 p1 :result result)
  result)

;;;------------------------------------------------------------

(defmethod elementwise-product! ((p0 Subpoint) (p1 Subpoint))
  (assert (subspace? (home p0) (home p1)))
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (ds (- s0 s1))
	 (n (- (coord-end p0) s0))
	 (n+ds (+ ds n))
	 (pa0 (coords p0))
	 (pa1 (coords p1)))
    (v<-x! pa1 0.0d0 :end ds)
    (v*v! pa1 pa0 :start0 ds :end0 n+ds :end1 n)
    (v<-x! pa1 0.0d0 :start n+ds :end (coord-end p1))))

(defmethod inner-product ((p0 Subpoint) (p1 Subpoint))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v.v (coords p0) (coords p1)
	   :start0 (- smax s0) :end0 (- emin s0)
	   :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l1-dist  ((p0 Subpoint) (p1 Subpoint))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v-l1-dist (coords p0) (coords p1)
		 :start0 (- smax s0) :end0 (- emin s0)
		 :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l2-dist2 ((p0 Subpoint) (p1 Subpoint))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v-l2-dist2 (coords p0) (coords p1)
		  :start0 (- smax s0) :end0 (- emin s0)
		  :start1 (- smax s1) :end1 (- emin s1)))) )

(defmethod sup-dist ((p0 Subpoint) (p1 Subpoint))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v-sup-dist (coords p0) (coords p1)
		  :start0 (- smax s0) :end0 (- emin s0)
		  :start1 (- smax s1) :end1 (- emin s1)))))

;;;=======================================================
;;;	       Block-Point
;;;=======================================================

(defclass Block-Point (Subpoint)
	  ((home
	    :type Block-Subspace
	    :accessor home
	    :initarg :home))
  (:documentation
   "a class for elements of a Block-Subspace."))

;;;-------------------------------------------------------
;;; Space
;;;-------------------------------------------------------

(defmethod element? ((p Block-Point) (sp Vector-Space))
  (eq (parent-space (home p)) sp)) 

(defmethod element? ((p Block-Point) (sp Block-Subspace))
  (subspace? (home p) sp))

;;;-------------------------------------------------------
;;; Basic Object protocol
;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((p0 Block-Point) (result Block-Point))
  (<= (dimension (home p0)) (length (coords result))))
  
(defmethod az:copy-to! ((p0 Block-Point) (result Block-Point))
  (setf (home result) (home p0))
  (az:copy-to! (coords result) (coords p0))
  result)

(defmethod az:verify-copy-result? ((p0 Block-Point) (result Point))
  (element? p0 (home result)))

(defmethod az:copy-to! ((p0 Block-Point) (result Point))
  (let ((s (coord-start p0))
	(e (coord-end p0))
	(n (dimension result)))
    (when (> s 0) (v<-x! (coords result) 0.0d0 :end s))
    (v<-v! (coords result) (coords p0)
	      :start0 s :end0 e
	      :end1 (- e s))
    (when (< e n) (v<-x! (coords result) 0.0d0 :start e :end n)))  
  result) 

;;;-------------------------------------------------------
;;; Projections on subspaces:
;;;-------------------------------------------------------

(defmethod ortho-project ((p Point) (subspace Block-Subspace)
			  &key (result (make-element subspace)))
  (assert (element? p (parent-space subspace)))
  (assert (eq (home result) subspace))
  (v<-v! (coords result) (coords p)
	 :end0 (dimension subspace)
	 :start1 (coord-start subspace) :end1 (coord-end subspace))
  result)

(defmethod scaled-ortho-project ((p Point) (subspace Block-Subspace) 
				 (a Number)
				 &key (result (make-element subspace)))
  (assert (element? p (parent-space subspace)))
  (assert (eq (home result) subspace))
  (v<-v*x! (coords result) (coords p) a
	   :end0 (dimension subspace)
	   :start1 (coord-start subspace) :end1 (coord-end subspace))
  result)

;;; trivial cases for compatibility:

(defmethod ortho-project ((p Block-Point) (subspace Block-Subspace)
			  &key (result (make-element subspace)))
  (assert (element? p (parent-space subspace)))
  (assert (eq (home result) subspace))
  (let* ((s (coord-start p))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (pa (coords p))
	 (ra (coords result)))
    (v<-v! ra pa :end0 n :start1 ds :end1 n+ds))
  result)

(defmethod scaled-ortho-project ((p Block-Point) (subspace Block-Subspace) 
				 (a Number)
				 &key (result (make-element subspace)))
  (assert (element? p (parent-space subspace)))
  (assert (eq (home result) subspace))
  (let* ((s (coord-start p))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (pa (coords p))
	 (ra (coords result)))
    (v<-v*x! ra pa a :end0 n :start1 ds :end1 n+ds))
  result)


(defmethod ortho-project ((p Point) (space Vector-Space)
			  &key (result (make-element space)))
  (az:copy p :result result)
  result)

(defmethod scaled-ortho-project ((p Point) (space Vector-Space) (a Number)
				 &key (result (make-element space)))
  (scale a p :result result)
  result)

;;;=======================================================
;;;	       Point-Canonical
;;;=======================================================

(defclass Canonical-Point (Subpoint)
	  ((home
	    :type Canonical-Subspace
	    :accessor home
	    :initarg :home))
  (:documentation
   "a class for elements of a Canonical-Subspace."))

;;;------------------------------------------------------------

(defmethod element? ((p Canonical-Point) (sp Vector-Space))
  (eq (parent-space (home p)) sp)) 

(defmethod element? ((p Canonical-Point) (sp Canonical-Subspace))
  (subspace? (home p) sp))

;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((p0 Canonical-Point)
				   (result Canonical-Point))
  (<= (dimension (home p0)) (length (coords result))))
  
(defmethod az:copy-to! ((p0 Canonical-Point) (result Canonical-Point))
  (let* ((home (home p0))
	 (n (dimension home)))
    (setf (home result) home)
    (v<-v! (coords result) (coords p0) :end0 n :end1 n))
  result)

(defmethod az:verify-copy-result? ((p0 Canonical-Point) (result Point))
  (element? p0 (home result)))

(defmethod az:copy-to! ((p0 Canonical-Point) (result Point))
  (let ((s (coord-start p0))
	(e (coord-end p0))
	(n (dimension result)))
    (when (> s 0) (v<-x! (coords result) 0.0d0 :end s))
    (v<-v! (coords result) (coords p0) :start0 s :end0 e :end1 (- e s))
    (when (< e n) (v<-x! (coords result) 0.0d0 :start e :end n)))  
  result) 

;;;-------------------------------------------------------
;;; Projections on subspaces:
;;;-------------------------------------------------------

(defmethod scaled-ortho-project ((p Point) (subspace Canonical-Subspace) 
				 (a Number)
				 &key (result (make-element subspace)))
  (assert (element? p (parent-space subspace)))
  (assert (eq (home result) subspace))
  (v<-v*x! (coords result) (coords p) a
	   :end0 (dimension subspace)
	   :start1 (coord-start subspace) :end1 (coord-end subspace))
  result)

;;; trivial cases for compatibility:

(defmethod ortho-project ((p Canonical-Point) (subspace Canonical-Subspace)
			  &key (result (make-element subspace)))
  (assert (element? p (parent-space subspace)))
  (assert (eq (home result) subspace))
  (let* ((s (coord-start p))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (pa (coords p))
	 (ra (coords result)))
    (v<-v! ra pa :end0 n :start1 ds :end1 n+ds))
  result)

(defmethod scaled-ortho-project ((p Canonical-Point)
				 (subspace Canonical-Subspace)
				 (a Number)
				 &key (result (make-element subspace)))
  (assert (element? p (parent-space subspace)))
  (assert (eq (home result) subspace))
  (let* ((s (coord-start p))
	 (sr (coord-start subspace))
	 (ds (- sr s))
	 (n (- (coord-end subspace) sr))
	 (n+ds (+ ds n))
	 (pa (coords p))
	 (ra (coords result)))
    (v<-v*x! ra pa a :end0 n :start1 ds :end1 n+ds))
  result)

;;;------------------------------------------------------------

(defmethod az:new-copy ((p0 Canonical-Point))
  (let ((result (make-element (home p0))))
    (az:copy (coords p0) :result (coords result))
    result))

;;;------------------------------------------------------------
;;; slow (because they use <coordinate>) but simple and reliable
;;; methods for projection and embedding:

(defmethod project ((p0 Point) (sp Canonical-Subspace)
		    &key (result (make-element sp)))
  (assert (element? result sp))
  (assert (subspace? sp (home p0)))
  (dolist (index (index-list sp))
    (setf (coordinate result index) (coordinate result index)))
  result)

(defmethod embed ((p0 Canonical-Point) (sp Vector-Space)
		  &key (result (make-element sp)))
  (assert (element? p0 sp))
  (assert (eq (home result) sp))
  (zero! result)
  (dolist (index (index-list (home p0)))
    (setf (coordinate result index) (coordinate result index)))
  result)

;;;------------------------------------------------------------

(defmethod coordinate ((p0 Canonical-Point) i)
  (aref (coords p0) (position i (index-list (home p0)))))

(defmethod (setf coordinate) ((new Number) (p0 Canonical-Point) i)
  (setf (aref (coords p0) (position i (index-list (home p0)))) new))

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (p0 Canonical-Point)
		       (a1 Number) (p1 Canonical-Point)
		       &key (result (make-element (home p0))))  
  (let ((p0-borrowed? nil)
	(p1-borrowed? nil)
	(result-space (home result)))
    (unless (eq (home p0) result-space)
      (setf p0-borrowed? t)
      (setf p0 (embed p0 result-space :result (borrow-element result-space))))
    (unless (eq (home p1) result-space)
      (setf p1-borrowed? t)
      (setf p1 (embed p1 result-space :result (borrow-element result-space))))
    (linear-mix a0 (coords p0) a1 (coords p1) :result (coords result))
    (when p0-borrowed? (return-element p0 result-space))
    (when p1-borrowed? (return-element p1 result-space)))
  result)

;;;------------------------------------------------------------

(defmethod inner-product ((p0 Canonical-Point) (p1 Canonical-Point))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v.v (coords p0) (coords p1)
	   :start0 (- smax s0) :end0 (- emin s0)
	   :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l1-dist  ((p0 Canonical-Point) (p1 Canonical-Point))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v-l1-dist (coords p0) (coords p1)
		 :start0 (- smax s0) :end0 (- emin s0)
		 :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod l2-dist2 ((p0 Canonical-Point) (p1 Canonical-Point))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v-l2-dist2 (coords p0) (coords p1)
		  :start0 (- smax s0) :end0 (- emin s0)
		  :start1 (- smax s1) :end1 (- emin s1)))))

(defmethod sup-dist ((p0 Canonical-Point) (p1 Canonical-Point))  
  (let* ((s0 (coord-start p0))
	 (s1 (coord-start p1))
	 (smax (max s0 s1))
	 (emin (min (coord-end p0) (coord-end p1))))
    (unless (>= smax emin)
      (v-sup-dist (coords p0) (coords p1)
		  :start0 (- smax s0) :end0 (- emin s0)
		  :start1 (- smax s1) :end1 (- emin s1)))))
 
;;;=======================================================
;;;		   Direct-Sum-Point
;;;=======================================================

(defclass Direct-Sum-Point (Point)
	  ((pt0
	    :type Point
	    :accessor pt0
	    :initarg :pt0)
	   (pt1
	    :type Point
	    :accessor pt1
	    :initarg :pt1))
  (:documentation
   "a class for elements of Direct-Sum-Space's"))

;;;-------------------------------------------------------
;;; Basic Object Protocol
;;;-------------------------------------------------------

(defmethod az:new-copy ((p Direct-Sum-Point))
  (make-instance (class-of p)
		 :home (home p)
		 :pt0 (az:copy (pt0 p))
		 :pt1 (az:copy (pt1 p))))

(defmethod az:copy-to! ((p Direct-Sum-Point) (result Direct-Sum-Point))
  (setf (home result) (home p))
  (az:copy (pt0 p) :result (pt0 result))
  (az:copy (pt1 p) :result (pt1 result))
  result)

;;;-------------------------------------------------------
;;; Relation to space
;;;-------------------------------------------------------

(defmethod home ((p Direct-Sum-Point))
  (direct-sum (home (pt0 p)) (home (pt1 p))))

;;;-------------------------------------------------------

(defmethod element? ((p Direct-Sum-Point) (sp Direct-Sum-Space))
  (and (element? (pt0 p) (space0 sp))
       (element? (pt1 p) (space1 sp))))

;;;-------------------------------------------------------

(defmethod dimension ((p Direct-Sum-Point))
  (+ (dimension (pt0 p)) (dimension (pt1 p))))

;;;-------------------------------------------------------
;;; Construction
;;;-------------------------------------------------------

(defmethod fill-randomly! ((p Direct-Sum-Point) &key (min -1.0d0) (max 1.0d0))
  (fill-randomly! (pt0 p) :min min :max max)
  (fill-randomly! (pt1 p) :min min :max max)
  p)

;;;------------------------------------------------------------

(defmethod fill-with-canonical-basis-point! ((p Direct-Sum-Point) k)
  (zero! p)
  (let ((n (dimension (pt0 p))))
    (if (< k n)
	(setf (pt0 p) (fill-with-canonical-basis-point! (pt0 p) k))
	(setf (pt1 p) (fill-with-canonical-basis-point! (pt1 p) (- k n)))))
  p)

;;;------------------------------------------------------------
;;; Coordinates
;;;------------------------------------------------------------

(defmethod coordinate ((p Direct-Sum-Point) k)
  
  (let ((n (dimension (pt0 p))))
    (if (< k n)
	(coordinate (pt0 p) k)
	(coordinate (pt1 p) (- k n)))))

(defmethod (setf coordinate) (new (p Direct-Sum-Point) k)
  
  (let ((n (dimension (pt0 p))))
    (if (< k n)
	(setf (coordinate (pt0 p) k) new)
	(setf (coordinate (pt1 p) (- k n)) new))))

;;;------------------------------------------------------------
;;; Algebra
;;;------------------------------------------------------------

(defmethod zero! ((p Direct-Sum-Point))
  (zero! (pt0 p))
  (zero! (pt1 p))
  p)

(defmethod negate ((p Direct-Sum-Point) &key (result (az:copy p)))
  (negate (pt0 p) :result (pt0 result))
  (negate (pt1 p) :result (pt1 result))
  result)


(defmethod scale ((a Number) (p Direct-Sum-Point) &key (result (az:copy p))) 
  (scale a (pt0 p) :result (pt0 result))
  (scale a (pt1 p) :result (pt1 result))
  result)

(defmethod linear-mix ((a0 Number) (p0 Direct-Sum-Point)
		       (a1 Number) (p1 Direct-Sum-Point)
		       &key (result (az:copy p0)))
  (linear-mix a0 (pt0 p0) a1 (pt0 p1) :result (pt0 result))
  (linear-mix a0 (pt1 p0) a1 (pt1 p1) :result (pt1 result))
  result)

;;;------------------------------------------------------------

(defmethod affine-mix ((a0 Number) (p0 Direct-Sum-Point)
		       (a1 Number) (p1 Direct-Sum-Point)
		       &key (result (make-element (home p0))))
  (affine-mix a0 (pt0 p0) a1 (pt0 p1) :result (pt0 result))
  (affine-mix a0 (pt1 p0) a1 (pt1 p1) :result (pt1 result))
  result)

;;;------------------------------------------------------------

(defmethod sub ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point)
		&key (result (make-element (translation-space (home p0)))))
  (sub (pt0 p0) (pt0 p1) :result (pt0 result))
  (sub (pt1 p0) (pt1 p1) :result (pt1 result))
  result)

;;;------------------------------------------------------------
;;; Metrics
;;;------------------------------------------------------------

(defmethod inner-product ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point))
  (+ (inner-product (pt0 p0) (pt0 p1)) (inner-product (pt1 p0) (pt1 p1))))

(defmethod l2-dist2 ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point))
  (+ (l2-dist2 (pt0 p0) (pt0 p1)) (l2-dist2 (pt1 p0) (pt1 p1))))

(defmethod l1-dist ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point))  
  (+ (l1-dist (pt0 p0) (pt0 p1)) (l1-dist (pt1 p0) (pt1 p1))))

(defmethod sup-dist ((p0 Direct-Sum-Point) (p1 Direct-Sum-Point))  
  (max (sup-dist (pt0 p0) (pt0 p1)) (sup-dist (pt1 p0) (pt1 p1))))

(defmethod l2-norm2 ((p Direct-Sum-Point))
  (+ (l2-norm2 (pt0 p)) (l2-norm2 (pt1 p))))

(defmethod l1-norm ((p Direct-Sum-Point))
  (+ (l1-norm (pt0 p)) (l1-norm (pt1 p))))

(defmethod sup-norm ((p Direct-Sum-Point))
  (max (sup-norm (pt0 p)) (sup-norm (pt1 p))))




