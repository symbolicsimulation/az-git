;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)

;;;============================================================
;;; Rects
;;;============================================================

(defclass Rect (Geometric-Object)
	  ((origin
	    :type Point
	    :accessor origin
	    :initarg :origin
	    :documentation
	    "The origin, or minimal corner, of this rect")
	   (extent
	    :type Point
	    :accessor extent
	    :initarg :extent
	    :documentation
	    "The diagonal vector from the minimal corner 
             to the maximal corner of the rect. Its coordinates
             are the width, height, depth, etc., of the rect."))
  (:documentation
   "This is a root class for n-dimensional axis-parallel rectangles."))

;;;------------------------------------------------------------
;;; Related types
;;;------------------------------------------------------------

(defun rect-list? (list)
  (and (listp list)
       (every #'(lambda (r) (typep r 'Rect))
	      list)))

(deftype Rect-List () '(satisfies rect-list?))

;;;------------------------------------------------------------
;;; Basic Object Protocol
;;;------------------------------------------------------------

(defmethod print-object ((r Rect) stream)
  (if (and (slot-boundp r 'home) (slot-boundp (home r) 'name))
    (progn
      (format stream "#<R")
      (when (and (slot-boundp r 'origin)
		 (slot-boundp r 'extent)
		 (typep (origin r) 'Point)
		 (slot-boundp (origin r) 'coords)
		 (typep (extent r) 'Point)
		 (slot-boundp (extent r) 'coords))
	(format stream " ~a ~a" (coords (origin r)) (coords (extent r))))
      (format stream " in ~a>" (name (home r))))
    (az:printing-random-thing
     (r stream)
     (format stream "R")
     (when (and (slot-boundp r 'origin)
		(slot-boundp r 'extent)
		(typep (origin r) 'Point)
		(slot-boundp (origin r) 'coords)
		(typep (extent r) 'Point)
		(slot-boundp (extent r) 'coords))
       (format stream " ~a ~a" (coords (origin r)) (coords (extent r)))))))

(defmethod az:copy ((r Rect) &key (result (make-rect (home r))))
  (declare (type Rect r result))
  (assert (eq (home r) (home result)))
  (az:copy (origin r) :result (origin result))
  (az:copy (extent r) :result (extent result))
  result)
  
(defmethod az:equal? ((r0 Rect) (r1 Rect))
  "The equality predicate for Geometric Objects."
  (declare (type Rect r0 r1)
	   (:returns (type (Member T Nil))))
  (and (az:equal? (origin r0) (origin r1))
       (az:equal? (extent r0) (extent r1))))
 
;;;------------------------------------------------------------
;;; Construction
;;;------------------------------------------------------------

(defun parse-float-interval-specs (x xmax dx)
  (cond ((null xmax) (when (null x) (setf x 0.0d0))
		     (when (null dx) (setf dx 0.0d0))
		     (locally (declare (type Double-Float x)
				       (type az:Positive-Float dx))
		       (setf xmax (+ x dx))))
	((null x) (when (null dx) (setf dx 0))
		  (locally (declare (type Double-Float xmax)
				    (type  az:Positive-Float dx))
		    (setf x (- xmax dx))))
	((null dx) (locally (declare (type Double-Float x xmax))
		     (assert (>= xmax x))
		     (setf dx (- xmax x))))
	(t (locally (declare (type Double-Float x xmax)
			     (type az:Positive-Float dx))
	     (assert (= dx (- xmax x))))))
  (values x xmax dx))

(defun parse-int-interval-specs (x xmax dx)
  (cond ((null xmax) (when (null x) (setf x 0))
		     (when (null dx) (setf dx 0))
		     (locally (declare (type az:Int16 x)
				       (type az:Card16 dx))
		       (setf xmax (+ x dx))))
	((null x) (when (null dx) (setf dx 0))
		  (locally (declare (type az:Int16 xmax)
				    (type az:Card16 dx))
		    (setf x (- xmax dx))))
	((null dx) (locally (declare (type az:Int16 x xmax))
		     (assert (>= xmax x))
		     (setf dx (- xmax x))))
	(t (locally (declare (type az:Int16 x xmax)
			     (type az:Card16 dx))
	     (assert (= dx (- xmax x))))))
  (values x xmax dx))
 
;;;------------------------------------------------------------

(defmethod set-rect-coords ((space Flat-Space) r
			    &key
			    (width nil) (right nil) (left nil)
			    (height nil) (bottom nil) (top nil))
  (declare (type Rect r))
  (multiple-value-bind
      (left right width) (parse-float-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(bottom top height) (parse-float-interval-specs bottom top height)
      (declare (ignore top))
      (setf (x r) left)
      (setf (y r) bottom)
      (setf (w  r) width)
      (setf (h r) height)))
  r)

(defmethod set-rect-coords ((space Int-Flat) r
			    &key
			    (width nil) (right nil) (left nil)
			    (height nil) (bottom nil) (top nil))
  (declare (type Rect r))
  (multiple-value-bind
      (left right width) (parse-int-interval-specs left right width)
    (declare (ignore right))
    (multiple-value-bind
	(top bottom height) (parse-int-interval-specs top bottom height)
      (declare (ignore bottom))
      (setf (x r) left)
      (setf (y r) top)
      (setf (w  r) width)
      (setf (h r) height)))
  r)

;;;------------------------------------------------------------

(defmethod make-rect ((home Flat-Space)
		      &key
		      (width nil) (right nil) (left nil)
		      (height nil) (bottom nil) (top nil))
  			
  "The constructor function for Rects in space.

The Rect is optionally initialized by specifying any two of <:left>,
<:right>, and <:width>, and any two of <:top>, <:bottom>, and
<:height>.

A screen Rect is a screen rectangle whose sides are parallel to
the coordinate axes. Other names are commonly used for this,
including ``rectangle'' and ``region''. We use Rect because we
want to reserve ``rectangle'' for general rectangles and ``region'' for
more general specifications of sets of pixels. We welcome any
suggestions for names that are better than Rect. (One we
have considered is nD-Interval, but it seems too verbose.)

We use a coordinate system where x increases from left to right,
and, unfortrunately, like most window systems, y from top to bottom.

Width and height must be non-negative.  Zero width and zero height
are taken to imply an empty rect. A width and height of 1
means the Rect covers one pixel. Empty rects still
have a location.

<Rect>s have the following generalized accessors: <x>, <xmax>,
<y>, <ymax>, <left>, <top>, <right>, <bottom>, <w>, and <h>.  At
present, all rects are represented internally by <x>, <width>, <y>,
and <height> ``slots''. For <Rect>s, <x> is equivalent to <left>,
and <y> to <top>.

The external interface for making <Rect>'s (make-rect)
takes any two of :left, :right, and :height and any two of :top,
:bottom, and :width.  The internal representation may be changed at
any time.

Because of the pernicious danger of fence post errors, we give a
careful description of which pixels the various coordinates refer to,
and also give name to common alternative specifications: 
The (<left>, <right>) and (<top>, <bottom>) pairs are specifications of
integer intervals like the (start, end) parameters to the CL sequence
functions.  <x> is the coordinate of the leftmost column that
intersects the Rect and <top> the coordinate of the topmost row
that intersects the rect.  <right> is the coordinate of the
first column not intersecting the Rect to the right and
<bottom> is the coordinate of the first row below the Rect not
intersecting it.  (See figure below.)  This means that <w> = <right> -
<x> is the number of columns that intersect the rect, <h> =
<bottom> - <top> is the number of rows that intersect the rect,
and <w>*<h> is the number of pixels in the rect.

\begin{verbatim}
   <x>
   |    <right>
   |    |
   v    v
  ooooooo 
  oxxxxxo <- <top>
  oxxxxxo
  oxxxxxo 
  ooooooo <- <bottom>
\end{verbatim}

Implementation of drawing operations will require interfacing with
other parameterizations; to make this a little easier, we give
standard names to commonly occurring coordinates:

\begin{verbatim}
  outer-left
  |inner-left
  ||   inner-right
  ||   |outer-right
  ||   ||
  vv   vv
  ooooooo <-outer-top
  oxxxxxo <-inner-top
  oxxxxxo
  oxxxxxo <-inner-bottom
  ooooooo <-outer-bottom
\end{verbatim}

For example, implementations of the drawing operations will often
need to transform to the analogous parameterization in top-to-bottom
coordinates. This means converting our (inner-left, inner-bottom,
width, height) representation to the coordinates of the (inner-left,
inner-top, width, height) in the top-to-bottom coordinates."

  (declare (type (or Null Real) left right top bottom)
	   (type (or Null Real) width height)
	   (:returns (type Rect)))

  (set-rect-coords home (make-instance 'Rect
			  :home home
			  :origin (make-element home)
			  :extent (make-element (translation-space home)))
		   :width width :right right :left left
		   :height height :bottom bottom :top top))

;;;------------------------------------------------------------
;;; Rect resources
;;;------------------------------------------------------------

(defgeneric return-rect (p sp &optional errorp)
  #-sbcl (declare (type Rect p)
		  (type Flat-Space sp)))

(defmethod return-rect ((p Rect) (sp Flat-Space) &optional (errorp t))
  
  ;; This is an example of the slightly confusing analogy between
  ;; spaces and classes. It seems too bad that we have to explicitly test
  ;; <v> for membership in <sp> rather than just using dispatching the
  ;; dispatching mechanism somehow. The problem is that objects know what
  ;; class they are an instance of, but <v> can't know what vector space
  ;; it's a member of, because it might be a member  of more than one
  ;; vector space.
  
  (if (eq (home p) sp)
    (push p (rect-resource sp))
    ;; else
    (when errorp
      (error "Trying to return ~a to the resource of ~a, ~%~
              but ~a is not a member of ~a."
	     p sp p sp))))

(defmacro with-borrowed-rect ((name home &rest options) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-rect ,home ,@options))
	    (,name ,return-name))
       (multiple-value-prog1
	   (progn ,@body)
	 (return-rect ,return-name ,home)))))

(defmacro with-borrowed-rects (specs &body body)
  (if (= (length specs) 1)
      `(with-borrowed-rect ,(first specs)
	 ,@body)
      `(with-borrowed-rect ,(first specs)
	 (with-borrowed-rects ,(rest specs)
	   ,@body))))
 
;;;------------------------------------------------------------
;;; Coordinate Aliases
;;;------------------------------------------------------------

(defmethod x ((r Rect))
  (declare (type Rect r)
	   (:returns (type Real)))
  (aref (coords (origin r)) 0))

(defmethod (setf x) (value (r Rect))
  (declare (type Real value)
	   (type Rect r)
	   (:returns (type Real)))
  (setf (aref (coords (origin r)) 0) value))

(defmethod y ((r Rect))
  (declare (type Rect r)
	   (:returns (type Real)))
  (aref (coords (origin r)) 1))

(defmethod (setf y) (value (r Rect))
  (declare (type Real value)
	   (type Rect r)
	   (:returns (type Real)))
  (setf (aref (coords (origin r)) 1) value))

(defmethod w ((r Rect))
  (declare (type Rect r)
	   (:returns (type Real)))
  (aref (coords (extent r)) 0))

(defmethod (setf w) (value (r Rect))
  (declare (type Real value)
	   (type Rect r)
	   (:returns (type Real)))
  (setf (aref (coords (extent r)) 0) value))

(defmethod h ((r Rect))
  (declare (type Rect r)
	   (:returns (type Real)))
  (aref (coords (extent r)) 1))

(defmethod (setf h) (value (r Rect))
  (declare (type Real value)
	   (type Rect r)
	   (:returns (type Real)))
  (setf (aref (coords (extent r)) 1) value))

;;;------------------------------------------------------------

(defun xmax (r)
  "The right side of <r>."
  (declare (type Rect r)
	   (:returns (type Real)))
  (+ (the Real (x r)) (the az:Positive-Real (w r))))

(defun set-xmax  (r x)
  (declare (type Rect r)
	   (type Real x))
  (setf (x r) (- x (the Real (w r)))))

(defsetf xmax (r) (x)
  "Changing any of the border coordinates of a Rect is interpreted as
a translation."
  `(set-xmax ,r ,x))

;;;------------------------------------------------------------
;;; Setting the ymax of a Rect is interpreted as a
;;; translation, rather than a stretching, to make ymax
;;; behave like ymin.

(defun ymax (r)
  "The bottom of <r>."
  (declare (type Rect r))
  (+ (the Real (y r)) (the az:Positive-Real (h r))))

(defun set-ymax  (r x)
  (declare (type Rect r)
	   (type Real x))
  (setf (y r) (- x (the Real (h r)))))

(defsetf ymax (r) (y)
  "Changing any of the border coordinates of a Rect is interpreted as
a translation."
  `(set-ymax ,r ,y))

;;;------------------------------------------------------------

(defun left (r)
  "``Left'' is an alias for ``x''."
  (declare (type Rect r)
	   (:returns (type Real)))
  (x r))

(defsetf left (r) (x) `(setf (x ,r) ,x))

(defun right (r)
  "``Right'' is an alias for ``xmax''."
  (declare (type Rect r)
	   (:returns (type Real)))
  (xmax r))

(defsetf right (r) (x)
  "Changing any of the border coordinates of a Rect is interpreted as
a translation."
  `(setf (xmax ,r) ,x))
 
;;;------------------------------------------------------------

(defun top (r)
  "``Top'' is an alias for ``ymin''."
  (declare (type Rect r)
	   (:returns (type Real)))
  (y r))

(defsetf top (r) (y)
  "Changing any of the border coordinates of a Rect is interpreted as
a translation."
  `(setf (y ,r) ,y))
 
(defun bottom (r)
  "``Bottom'' is an alias for ``ymax''."
  (declare (type Rect r)
	   (:returns (type Real)))
  (ymax r))

(defsetf bottom (r) (y)
  "Changing any of the border coordinates of a Rect is interpreted as
a translation."
  `(setf (ymax ,r) ,y))

;;;------------------------------------------------------------
;;; Coordinate Aliases for Grid spaces
;;;------------------------------------------------------------
;;; "left" alias for "x"

(defun inner-left (r)
  "Returns the x coordinate of the left most pixel inside <r>."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (x r))

(defsetf inner-left (r) (x)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(setf (x ,r) ,x))

(defun outer-left (r)
  "Returns the x coordinate of the first pixel outside <r> on the
left."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (- (the az:Int16 (x r)) 1))

(defsetf outer-left (r) (x)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  (az:once-only (x) `(progn (setf (x ,r) (+ 1 ,x)) ,x)))

;;;------------------------------------------------------------

(defun outer-right (r)
  "Returns the x coordinate of the first pixel outside <r> on the right."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (xmax r))

(defsetf outer-right (r) (x)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(setf (xmax ,r) ,x))

(defun inner-right (r)
  "Returns the x coordinate of the right most pixel inside <r>."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (- (the az:Int16 (xmax r)) 1))

(defsetf inner-right (r) (x)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  (az:once-only (x) `(progn (setf (xmax ,r) (- ,x 1)) ,x)))

;;;------------------------------------------------------------
;;; "top" alias for "y"

(defun inner-top (r)
  "Returns the y coordinate of the top most pixel inside <r>."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (y r))

(defsetf inner-top (r) (y)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(setf (y ,r) ,y))

(defun outer-top (r)
  "Returns the y coordinate of the first pixel outside <r> on the
top."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (- (the az:Int16 (y r)) 1))

(defsetf outer-top (r) (y)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  (az:once-only (y) `(progn (setf (y ,r) (+ 1 ,y)) ,y)))

;;;------------------------------------------------------------
;;; "bottom" alias for "ymax"

(defun outer-bottom (r)
  "Returns the y coordinate of the first pixel outside <r> on the
bottom."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (ymax r))

(defsetf outer-bottom (r) (y)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  `(setf (ymax ,r) ,y))

(defun inner-bottom (r)
  "Returns the y coordinate of the bottom most pixel inside <r>."
  (declare (type Rect r)
	   (:returns (type az:Int16)))
  (- (the az:Int16 (ymax r)) 1))

(defsetf inner-bottom (r) (y)
  "Changing any of the border coordinates of a rect is interpreted as
a translation."
  (az:once-only (y) `(progn (setf (ymax ,r) (- ,y 1)) ,y)))

;;;-------------------------------------------------------
;;; Metrics
;;;-------------------------------------------------------

(defun int-l2-dist2 (r0 r1)
  "The L2 distance (squared) between the closest two points of the two rects."
  (declare (type Rect r0 r1)
	   (:returns (type az:Card16)))
  (let ((xmin0 (x r0))
	(xmax0 (xmax r0))
	(ymin0 (y r0))
	(ymax0 (ymax r0))
	(xmin1 (x r1))
	(xmax1 (xmax r1))
	(ymin1 (y r1))
	(ymax1 (ymax r1))
	(dist 0))
    (declare (type az:Int16 xmin0 xmax0 ymin0 ymax0 xmin1 xmax1 ymin1 ymax1)
	     (type az:Card16 dist))
    ;; perform reflections as needed so that r0 is below and xmin of r1
    (when (> xmin0 xmin1)
      (psetf xmin0 (- xmax0)
	     xmax0 (- xmin0))
      (psetf xmin1 (- xmax1)
	     xmax1 (- xmin1)))
    (when (> ymin0 ymin1)
      (psetf ymin0 (- ymax0)
	     ymax0 (- ymin0))
      (psetf ymin1 (- ymax1)
	     ymax1 (- ymin1)))
    ;; calculate distance
    (when (< xmax0 xmin1)
      (let ((dx (- xmin1 xmax0)))
	(declare (type az:Int16 dx))
	(incf dist (* dx dx))))
    (when (< ymax0 ymin1)
      (let ((dy (- ymin1 ymax0)))
	(declare (type az:Int16 dy))
	(incf dist (* dy dy))))
    dist))

(defmethod float-l2-dist2 (r0 r1)
  "The L2 distance (squared) between the closest two points of the two rects."
  (declare (type Rect r0 r1)
	   (:returns (type az:Positive-Float)))
  (let ((p0 (coords (origin r0)))
	(p1 (coords (origin r1)))
	(v0 (coords (extent r0)))
	(v1 (coords (extent r1)))
	(dist 0.0d0))
    (declare (type az:Float-Vector p0 p1 v0 v1)
	     (type az:Positive-Float dist))
    (dotimes (i (length p0))
      (declare (type az:Array-Index i))
      (let* ((xmin0 (aref p0 i))
	     (xmax0 (+ xmin0 (aref v0 i)))
	     (xmin1 (aref p1 i))
	     (xmax1 (+ xmin1 (aref v1 i))))
	(declare (type Double-Float xmin0 xmax0 xmin1 xmax1))
	;; perform reflections as needed so that r0 is left of r1
	(when (> xmin0 xmin1)
	  (psetf xmin0 (- xmax0)
		 xmax0 (- xmin0))
	  (psetf xmin1 (- xmax1)
		 xmax1 (- xmin1)))
	;; calculate distance
	(when (< xmax0 xmin1)
	  (let ((dx (- xmin1 xmax0)))
	    (declare (type Double-Float dx))
	    (incf dist (* dx dx))))))
    dist))

(defmethod l2-dist2 ((r0 Rect) (r1 Rect))
  "The L2 distance (squared) between the closest two points of the two rects."
  (declare (type Rect r0 r1)
	   (:returns (type az:Positive-Real)))
  (assert (eq (home r0) (home r1)))
  (etypecase (home r0)
    (Int-Grid (int-l2-dist2 r0 r1))
    (Int-Lattice (int-l2-dist2 r0 r1))
    (Flat-Space (float-l2-dist2 r0 r1))))
 
;;;------------------------------------------------------------
;;; Geometry
;;;------------------------------------------------------------

(defmethod %intersect? ((home Flat-Space) (r Rect) (p Point))
  "Test whether a point and a rect intersect."
  (declare (type Flat-Space home)
	   (type Point p)
	   (type Rect r)
	   (:returns (type az:Boolean)))
  (%intersect? home p r)
  t)

(defmethod %intersect? ((home Flat-Space) (p Point) (r Rect))
  "Test whether two rects intersect."
  (declare (type Flat-Space home)
	   (type Point p)
	   (type Rect r)
	   (:returns (type az:Boolean)))
  (let ((c (coords p))
	(o (coords (origin r)))
	(e (coords (extent r))))
    (declare (type az:Float-Vector c o e))
    ;; The rects intersect if all of the coordinate intervals intersect 
    (dotimes (i (length c))
      (declare (type az:Array-Index i))
      (let ((oi (aref o i)))
	(declare (type Double-Float oi))
	(unless (<= oi (aref c i) (+ oi (aref e i)))
	  (return-from %intersect? nil)))))
  t)

(defmethod %intersect? ((home Int-Flat) (p Point) (r Rect))
  "Test whether two rects intersect."
  (declare (type Int-Flat home)
	   (type Point p)
	   (type Rect r)
	   (:returns (type az:Boolean)))
  (let ((c (coords p))
	(o (coords (origin r)))
	(e (coords (extent r))))
    (declare (type az:Int16-Vector c o e))
    ;; The rects intersect if all of the coordinate intervals intersect 
    (dotimes (i (length c))
      (declare (type az:Array-Index i))
      (let ((oi (aref o i))
	    (ci (aref c i)))
	(declare (type az:Int16 oi ci))
	(unless (and (<= oi ci) (< ci (+ oi (aref e i))))
	  (return-from %intersect? nil)))))
  t)

(defmethod %intersect? ((home Flat-Space) (r0 Rect) (r1 Rect))
  "Test whether two rects intersect."
  (declare (type Rect r0 r1)
	   (:returns (type az:Boolean)))
  (let ((p0 (coords (origin r0)))
	(p1 (coords (origin r1)))
	(v0 (coords (extent r0)))
	(v1 (coords (extent r1))))
    (declare (type az:Float-Vector p0 p1 v0 v1))
    ;; The rects intersect if all of the coordinate intervals intersect 
    (dotimes (i (length p0))
      (declare (type az:Array-Index i))
      (when (let ((dl (- (aref p1 i) (aref p0 i))))
	      (declare (type Double-Float dl))
	      (or (>= dl (aref v0 i))
		  (<= dl (- (aref v1 i)))))
	(return-from %intersect? nil))))
  t)

(defmethod %intersect? ((home Int-Flat) (r0 Rect) (r1 Rect))
  "Test whether two rects intersect."
  (declare (type Rect r0 r1)
	   (:returns (type (Member t nil))))
  (let ((p0 (coords (origin r0)))
	(p1 (coords (origin r1)))
	(v0 (coords (extent r0)))
	(v1 (coords (extent r1))))
    (declare (type az:Int16-Vector p0 p1 v0 v1))
    ;; The rects intersect if all of the coordinate intervals intersect 
    (dotimes (i (length p0))
      (declare (type az:Array-Index i))
      (when (let ((dl (- (aref p1 i) (aref p0 i))))
	      (declare (type az:Int16 dl))
	      (or (>= dl (aref v0 i))
		  (<= dl (- (aref v1 i)))))
	(return-from %intersect? nil)))
    t))

;;;------------------------------------------------------------

(defmethod %intersect3 ((home Flat-Space) (r0 Rect) (r1 Rect) (result Rect))

  "Returns the rect that is the intersection.
Note that it's ok for <result> to be <r0> or r1>."

  (declare (type Flat-Space home)
	   (type Rect r0 r1 result)
	   (:returns result))
  (let ((p0 (coords (origin r0)))
	(p1 (coords (origin r1)))
	(pr (coords (origin result)))
	(v0 (coords (extent r0)))
	(v1 (coords (extent r1)))
	(vr (coords (extent result))))
      
    (declare (type az:Float-Vector p0 p1 v0 v1))
    ;; The rects intersect if all of the coordinate intervals intersect 
    (dotimes (i (length p0))
      (declare (type az:Array-Index i))
      (let* ((x0 (aref p0 i))
	     (x1 (aref p1 i))
	     (x (max x0 x1))
	     (w (- (min (+ x0 (aref v0 i)) (+ x1 (aref v1 i))) x)))
	(declare (type Double-Float x0 x1 x w))
	;; return the intersection in <result>
	(cond ((minusp w)
	       ;; Then there's no intersection in this coordinate.
	       ;; Choose an arbitrary origin coordinate and set the
	       ;; extent coordinate to 0
	       (setf (aref pr i) (* 0.5d0 (+ x0 x1)))
	       (setf (aref vr i) 0.0d0))
	      (t 
	       (setf (aref pr i) x)
	       (setf (aref vr i) w))))))
  result)

(defmethod %intersect3 ((home Int-Flat) (r0 Rect) (r1 Rect) (result Rect))
  "Returns the rect that is the intersection.
This assumes 2d.
Note that it's ok for <result> to be <r0> or r1>."
  (declare (type Int-Flat home)
	   (type Rect r0 r1 result)
	   (:returns result))
  (let* ((x0 (x r0))
	 (y0 (y r0))
	 (x1 (x r1))
	 (y1 (y r1))
	 (x (max x0 x1))
	 (y (max y0 y1))
	 (w (- (min (+ (w r0) x0) (+ (w r1) x1)) x))
	 (h (- (min (+ (h r0) y0) (+ (h r1) y1)) y)))
    (declare (type az:Int16 x0 y0 x1 y1 x y w h))
    (if (or (minusp w) (minusp h)) 
      ;; then no intersection
      (progn 
	(setf (w result) 0)
	(setf (h result) 0))
      ;; else return the intersection in <result>
      (progn
	(setf (x result) x)
	(setf (y result) y)
	(setf (w result) w)
	(setf (h result) h))))
  result)

(defmethod intersect2 ((r0 Rect) (r1 Rect) &key (result (make-rect (home r0))))
  (%intersect3 (home r0) r0 r1 result)
  result)
  
;;;------------------------------------------------------------

(defgeneric center (r &key result)
  #-sbcl (declare (type Rect r)
		  (type Point result)
		  (:returns (type Point result)))
  (:documentation
   "Return a point at the center of <r>."))

(defmethod center ((r Rect) &key (result (make-element (home r))))
  (declare (type Rect r)
	   (type Point result)
	   (:returns (type Point result)))
  (linear-mix 1.0d0 (coords (origin r)) 0.5d0 (coords (extent r))
	      :result (coords result))
  result)

;;;============================================================

(defun possible-point-rect-xy (width height margin enclosing-rect
			       &key (result
				     (g:make-rect (g:home enclosing-rect))))
  (declare (type az:Card16 width height margin)
	   (type g:Rect enclosing-rect result))
  (setf (g:x result) (+ (g:x enclosing-rect) margin))
  (setf (g:y result) (+ (g:y enclosing-rect) margin))
  (setf (g:w result) (- (g:w enclosing-rect) margin margin width))
  (setf (g:h result) (- (g:h enclosing-rect) margin margin height))
  result)

(defun possible-point-rect (extent margin enclosing-rect
			    &key (result
				  (g:make-rect (g:home enclosing-rect))))
  (declare (type g:Point extent)
	   (type az:Card16 margin)
	   (type g:Rect enclosing-rect result))
  (setf (g:x result) (+ (g:x enclosing-rect) margin))
  (setf (g:y result) (+ (g:y enclosing-rect) margin))
  (setf (g:w result) (- (g:w enclosing-rect) margin margin (g:x extent)))
  (setf (g:h result) (- (g:h enclosing-rect) margin margin (g:y extent)))
  result)

(defun random-point-in-rect (enclosing-rect
		     &key (result (g:make-element (g:home enclosing-rect))))
  (declare (type g:Rect enclosing-rect)
	   (type g:Point result))
  (assert (eq (g:home enclosing-rect) (g:home result)))
  (setf (g:x result) (+ (g:x enclosing-rect) (random (g:w enclosing-rect))))
  (setf (g:y result) (+ (g:y enclosing-rect) (random (g:w enclosing-rect))))
  result)

(defun random-rect (enclosing-rect
		    &key (result (g:make-rect (g:home enclosing-rect))))
  (declare (type g:Rect enclosing-rect)
	   (type g:Rect result))
  (setf (g:x result) (+ (g:x enclosing-rect) (random (g:w enclosing-rect))))
  (setf (g:y result) (+ (g:y enclosing-rect) (random (g:h enclosing-rect))))
  result)


