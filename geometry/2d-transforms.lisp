;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)
	
;;;============================================================

(defclass 2d-Affine-Map (2d-Object Affine-Mapping)
	  ((domain
	    :type T
	    :accessor domain
	    :initform nil
	    :documentation
	    "If not nil, the 2d-Affine-Map should only accept elements 
             of its domain as the first argument to <transform> and 
             as the result arguyment to <inverse-transform>.")
	   (codomain
	    :type T
	    :accessor codomain
	    :initform nil
	    :documentation
	    "If not nil, the 2d-Affine-Map should only accept elements 
             of its codomain as the first argument to <inverse-transform> 
             and as the result argument to <transform>."))
  (:documentation
   "This is a root class for 2d-Affine-Maps between 2d spaces. 
    It should not be instantiated."))

;;;============================================================

(defclass Screen-Translation (Screen-Vector 2d-Affine-Map) ()
  (:documentation
   "This is a quasi-affine transformation. It maps a souce rect to a
destination rect by truncation.  The source top left corner is mapped
to the dest top left corner and the source bottom right corner is
mapped to approximately the destination bottom right corner by scaling
and truncation."))

;;;------------------------------------------------------------

(defmethod transform ((map Screen-Translation) (p Screen-Point)
		       &key (result (make-screen-point)))
  (declare (type Screen-Point result)
	   (:returns (type Screen-Point result)))
  (setf (x result) (+ (x p) (x map)))
  (setf (y result) (+ (y p) (y map)))
  result)

(defmethod transform ((map Screen-Translation) (r Screen-Rect)
		       &key (result (make-screen-rect)))
  (declare (type Screen-Rect result)
	   (:returns (type Screen-Rect result)))
 
  (setf (x result) (+ (x r) (x map)))
  (setf (y result) (+ (y r) (y map)))
  (unless (eq r result)
    (setf (w result) (w r))
    (setf (h result) (h r)))
  result)

(defmethod inverse-transform ((map Screen-Translation) (p Screen-Point)
			       &key (result (make-screen-point)))
  (declare (type Screen-Point result)
	   (:returns (type Screen-Point result)))
  (setf (x result) (- (x p) (x map)))
  (setf (y result) (- (y p) (y map)))
  result)

(defmethod inverse-transform ((map Screen-Translation) (r Screen-Rect)
			       &key (result (make-screen-rect)))
  (declare (type Screen-Rect result)
	   (:returns (type Screen-Rect result)))
 
  (setf (x result) (- (x r) (x map)))
  (setf (y result) (- (y r) (y map)))
  (unless (eq r result)
    (setf (w result) (w r))
    (setf (h result) (h r)))
  result)
 
;;;============================================================

(defclass Screen-Affine-Map (Screen-Object 2d-Affine-Map)
	  ((src
	    :type Screen-Rect
	    :accessor src
	    :initform (make-screen-rect)
	    :documentation "The source rect")
	   (dst
	    :type Screen-Rect
	    :accessor dst
	    :initform (make-screen-rect)
	    :documentation "The destination rect")
	   (x-cache
	    :type az:Card16-Vector
	    :accessor x-cache
	    :initform (az:make-card16-vector 0)
	    :documentation
	    "This vector caches the dst x coordinates 
             indexed by src x coordinates. In other words,
             the x coordinate of the transformed (xsrc,ysrc)
             is (aref x-cache xsrc), as long as (xsrc,ysrc) is within the
             initial src rect.")
	   (y-cache
	    :type az:Card16-Vector
	    :accessor y-cache
	    :initform (az:make-card16-vector 0)
	    :documentation
	    "This vector caches the dst y coordinates 
             indexed by src y coordinates. In other words,
             the y coordinate of the transformed (xsrc,ysrc)
             is (aref y-cache ysrc), as long as (xsrc,ysrc) is within the
             initial src rect."))
  (:documentation
   "This is a quasi-affine transformation. It maps a souce rect to a
destination rect by truncation.  The source top left corner is mapped
to the dest top left corner and the source bottom right corner is
mapped to approximately the destination bottom right corner by scaling
and truncation."))

;;;------------------------------------------------------------
;;; Construction
;;;------------------------------------------------------------

(defgeneric set-screen-affine-map (map src dst)
  (declare (type Screen-Affine-Map map)
	   (type Screen-Rect src dst)
	   (:returns (type Screen-Affine-Map)))
  (:documentation
   "Adjust the coordinates and caches of the <map> 
    so that it maps <src> to <dst>."))

(defmethod set-screen-affine-map ((map Screen-Affine-Map) src dst)
  "Adjust the coordinates of the <map> so that it maps <src> to <dst>."
  (declare (type Screen-Affine-Map map)
	   (type Screen-Rect src dst)
	   (:returns (type Screen-Affine-Map)))
  (az:copy src :result (src map))
  (az:copy dst :result (dst map))
  (let* ((wsrc (w src))
	 (hsrc (h src))
	 (wdst (w dst))
	 (hdst (h dst))
	 (x-cache (az:make-card16-vector wsrc))
	 (y-cache (az:make-card16-vector hsrc)))
    (setf (x-cache map) x-cache)
    (setf (y-cache map) y-cache)
    (dotimes (x wsrc) (setf (aref x-cache x) (floor (* x wdst) wsrc)))
    (dotimes (y hsrc) (setf (aref y-cache y) (floor (* y hdst) hsrc))))
  map)

;;;------------------------------------------------------------
;;; Transformation
;;;------------------------------------------------------------

(defmethod transform ((map Screen-Affine-Map) (p Screen-Point)
		       &key (result (make-screen-point)))
  (declare (type Screen-Point result)
	   (:returns (type Screen-Point result)))
  (let ((src (src map))
	(dst (dst map)))
    (declare (type Screen-Rect src dst))
    (setf (x result)
      (+ (x dst) (truncate (* (w dst) (- (x p) (x src))) (w src))))
    (setf (y result)
      (+ (y dst) (truncate (* (h dst) (- (y p) (y src))) (h src))))))

(defmethod transform ((map Screen-Affine-Map) (r Screen-Rect)
		       &key (result (make-screen-rect)))
  (declare (type Screen-Rect result)
	   (:returns (type Screen-Rect result)))
  (let* ((src (src map))
	 (dst (dst map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (xmin (x r)) (xmax (+ xmin (w r)))
	 (ymin (y r)) (ymax (+ ymin (h r))))
    (declare (type Screen-Rect src dst)
	     (type az:Int16 xsrc ysrc xdst ydst xmin xmax ymin ymax)
	     (type az:Card16 wsrc hsrc wdst hdst))

    (setf xmin (floor (* (- xmin xsrc) wdst) wsrc))
    (setf xmax (floor (* (- xmax xsrc) wdst) wsrc))
    (setf (x result) (+ xmin xdst))
    (setf (w result) (- xmax xmin))

    (setf ymin (floor (* (- ymin ysrc) hdst) hsrc))
    (setf ymax (floor (* (- ymax ysrc) hdst) hsrc))
    (setf (y result) (+ ymin ydst))
    (setf (h result) (- ymax ymin))))

;;;------------------------------------------------------------

(defmethod inverse-transform ((map Screen-Affine-Map) (p Screen-Point)
			       &key (result (make-screen-point)))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  (declare (type Screen-Point result)
	   (:returns (type Screen-Point result)))
  (let* ((src (src map))
	 (dst (dst map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (xmin (x p))
	 (ymin (y p)))
    (declare (type Screen-Rect src dst)
	     (type az:Int16 xsrc ysrc xdst ydst xmin ymin)
	     (type az:Card16 wsrc hsrc wdst hdst))

    (setf xmin (ceiling (* wsrc (- xmin xdst)) wdst))
    (setf (x result) (+ xmin xsrc))

    (setf ymin (ceiling (* hsrc (- ymin ydst)) hdst))
    (setf (y result) (+ ymin ysrc))))

(defmethod inverse-transform ((map Screen-Affine-Map) (r Screen-Rect)
			       &key (result (make-screen-rect)))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  (declare (type Screen-Rect result)
	   (:returns (type Screen-Rect result)))
  (let* ((src (src map))
	 (dst (dst map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (xmin (x r)) (xmax (+ xmin (w r)))
	 (ymin (y r)) (ymax (+ ymin (h r))))
    (declare (type Screen-Rect src dst)
	     (type az:Int16 xsrc ysrc xdst ydst xmin xmax ymin ymax)
	     (type az:Card16 wsrc hsrc wdst hdst))

    (setf xmin (ceiling (* wsrc (- xmin xdst)) wdst))
    (setf xmax (ceiling (* wsrc (- xmax xdst)) wdst))
    (setf (x result) (+ xmin xsrc))
    (setf (w result) (- xmax xmin))

    (setf ymin (ceiling (* hsrc (- ymin ydst)) hdst))
    (setf ymax (ceiling (* hsrc (- ymax ydst)) hdst))
    (setf (y result) (+ ymin ysrc))
    (setf (h result) (- ymax ymin))))

;;;============================================================

(defclass Screen-Flip (Screen-Affine-Map) ()
  (:documentation
   "This is a quasi-affiine transformation. It maps a souce rect to a
destination rect by truncation, inverting the y coordinate.  The
source top left corner is mapped to the dest bottom left corner and
the source bottom right corner is mapped to approximately the
destination top right corner by scaling and truncation."))

;;;------------------------------------------------------------
;;; Construction
;;;------------------------------------------------------------

(defmethod set-screen-affine-map ((map Screen-Flip) src dst)
  "Adjust the coordinates of the <map> so that it maps <src> to <dst>."
  (declare (type Screen-Flip map)
	   (type Screen-Rect src dst)
	   (:returns (type Screen-Flip)))
  (az:copy src :result (src map))
  (az:copy dst :result (dst map))
  (let* ((wsrc (w src))
	 (hsrc (h src))
	 (wdst (w dst))
	 (hdst (h dst))
	 (x-cache (az:make-card16-vector wsrc))
	 (y-cache (az:make-card16-vector hsrc)))
    (setf (x-cache map) x-cache)
    (setf (y-cache map) y-cache)
    (dotimes (x wsrc) ;; x is not flipped
      (setf (aref x-cache x) (floor (* x wdst) wsrc)))
    (dotimes (y hsrc) ;; y is flipped
      (setf (aref y-cache y) (floor (* (- hsrc 1 y) hdst) hsrc))))
  map)
 
;;;------------------------------------------------------------
;;; Transformation
;;;------------------------------------------------------------

(defmethod transform ((map Screen-Flip) (p Screen-Point)
		       &key (result (make-screen-point)))

  "To be precise, this is implemented as though the src rect was: [1]
translated so that its origin is at zero, [2] flipped, [3] scaled with
flooring to the dst size, and [4] translated so that the dst rect has
the correct origin."

  (declare (type Screen-Point result)
	   (:returns (type Screen-Point result)))
  (let ((src (src map))
	(dst (dst map)))
    (declare (type Screen-Rect src dst))
    (setf (x result)
      (+ (x dst) (floor (* (- (x p) (x src)) (w dst)) (w src))))
    (setf (y result)
      (+ (y dst)
	 (floor (* (- (h src) 1 (- (y p) (y src))) (h dst)) (h src))))))

;;;------------------------------------------------------------

(defmethod transform ((map Screen-Flip) (r Screen-Rect)
		       &key (result (make-screen-rect)))

  "To be precise, this is implemented as though the src rect was: [1]
translated so that its origin is at zero, [2] flipped, [3] scaled with
flooring to the dst size, and [4] translated so that the dst rect has
the correct origin."
  
  (declare (type Screen-Rect result)
	   (:returns (type Screen-Rect result)))
  
  (let* ((src (src map))
	 (dst (dst map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (xmin (- (x r) xsrc))
	 (xmax (+ xmin (w r)))
	 ;; flip y interval here
	 (ymin (- (h src) (- (ymax r) ysrc)))
	 (ymax (+ ymin (h r))))
    (declare (type Screen-Rect src dst)
	     (type az:Int16 xsrc ysrc xdst ydst xmin xmax ymin ymax)
	     (type az:Card16 wsrc hsrc wdst hdst))

    (setf xmin (floor (* xmin wdst) wsrc))
    (setf xmax (floor (* xmax wdst) wsrc))
    (setf (x result) (+ xmin xdst))
    (setf (w result) (- xmax xmin))

    (setf ymin (floor (* ymin hdst) hsrc))
    (setf ymax (floor (* ymax hdst) hsrc))
    (setf (y result) (+ ymin ydst))
    (setf (h result) (- ymax ymin))))

;;;------------------------------------------------------------

(defmethod inverse-transform ((map Screen-Flip) (p Screen-Point)
			       &key (result (make-screen-point)))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map.

  To be precise, this is implemented as though the dst rect was: [1]
translated so that its origin is at zero, [2] scaled with ceiling to
the src size, [3] flipped, and [4] translated so that the src rect has
the correct origin."

  (declare (type Screen-Point result)
	   (:returns (type Screen-Point result)))
  (let* ((src (src map)) (dst (dst map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (xmin (x p)) (ymin (y p)))
    (declare (type Screen-Rect src dst)
	     (type az:Int16 xsrc ysrc xdst ydst xmin ymin)
	     (type az:Card16 wsrc hsrc wdst hdst))
    (setf (x result) (+ (ceiling (* wsrc (- xmin xdst)) wdst) xsrc))
    (setf (y result) (+ (- hsrc (ceiling (* hsrc (- ymin ydst)) hdst)) ysrc))))


(defmethod inverse-transform ((map Screen-Flip) (r Screen-Rect)
			       &key (result (make-screen-rect)))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map.

  To be precise, this is implemented as though the dst rect was: [1]
translated so that its origin is at zero, [2] scaled with ceiling to
the src size, [3] flipped, and [4] translated so that the src rect has
the correct origin."

  (declare (type Screen-Rect result)
	   (:returns (type Screen-Rect result)))
  (let* ((src (src map))
	 (dst (dst map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (w (w r))
	 (xmin (x r))
	 (xmax (+ xmin w))
	 (h (h r))
	 (ymin (y r))
	 (ymax (+ ymin h)))
    (declare (type Screen-Rect src dst)
	     (type az:Int16 xsrc ysrc xdst ydst xmin xmax ymin ymax)
	     (type az:Card16 wsrc hsrc wdst hdst w h))

    (setf xmin (ceiling (* wsrc (- xmin xdst)) wdst))
    (setf xmax (ceiling (* wsrc (- xmax xdst)) wdst))
    (setf (x result) (+ xmin xsrc))
    (setf (w result) (- xmax xmin))

    (setf ymin (ceiling (* hsrc (- ymin ydst)) hdst))
    (setf ymax (ceiling (* hsrc (- ymax ydst)) hdst))
    (setf (y result) (+ (- hsrc ymax) ysrc)) ;; flip y interval here
    (setf (h result) (- ymax ymin))))

;;;============================================================

(defclass SC-Flip (Screen-Affine-Map)
	  ((src
	    :type Screen-Rect
	    :accessor src
	    :initform (make-screen-rect)
	    :documentation "The source rect")
	   (dst
	    :type Chart-Rect
	    :accessor dst
	    :initform (make-chart-rect)
	    :documentation "The destination rect")
	   (x-cache
	    :type az:Card16-Vector
	    :accessor x-cache
	    :initform (az:make-float-vector 0)
	    :documentation
	    "This vector caches the dst x coordinates 
             indexed by src x coordinates. In other words,
             the x coordinate of the transformed (xsrc,ysrc)
             is (aref x-cache xsrc), as long as (xsrc,ysrc) is within the
             initial src rect.")
	   (y-cache
	    :type az:Card16-Vector
	    :accessor y-cache
	    :initform (az:make-float-vector 0)
	    :documentation
	    "This vector caches the dst y coordinates 
             indexed by src y coordinates. In other words,
             the y coordinate of the transformed (xsrc,ysrc)
             is (aref y-cache ysrc), as long as (xsrc,ysrc) is within the
             initial src rect."))
  (:documentation
   "This is a quasi-affiine transformation. It maps a source
screen (int16) rect to a destination chart (float) rect by truncation,
inverting the y coordinate.  The source top left corner is mapped to
the dest bottom left corner and the source bottom right corner is
mapped to approximately the destination top right corner by scaling
and truncation."))

;;;------------------------------------------------------------
;;; Construction
;;;------------------------------------------------------------

(defmethod set-screen-affine-map ((map SC-Flip) src dst)
  "Adjust the coordinates of the <map> so that it maps <src> to <dst>."
  (declare (type SC-Flip map)
	   (type Screen-Rect src)
	   (type Chart-Rect dst)
	   (:returns (type SC-Flip)))
  (az:copy src :result (src map))
  (az:copy dst :result (dst map))
  map)
 
;;;------------------------------------------------------------
;;; Transformation
;;;------------------------------------------------------------

(defmethod transform ((map SC-Flip) (p Screen-Point)
		       &key (result (make-chart-point)))

  "To be precise, this is implemented as though the src rect was: [1]
translated so that its origin is at zero, [2] flipped, [3] scaled with
flooring to the dst size, and [4] translated so that the dst rect has
the correct origin."

  (declare (type Chart-Point result)
	   (:returns (type Chart-Point result)))
  (let ((src (src map))
	(dst (dst map)))
    (declare (type Screen-Rect src)
	     (type Chart-Rect dst))
    (setf (x result)
      (+ (x dst) (/ (* (- (x p) (x src)) (w dst)) (w src))))
    (setf (y result)
      (+ (y dst) (/ (* (- (h src) 1 (- (y p) (y src))) (h dst)) (h src))))))

;;;------------------------------------------------------------

(defmethod transform ((map SC-Flip) (r Screen-Rect)
		       &key (result (make-chart-rect)))

  "To be precise, this is implemented as though the src rect was: [1]
translated so that its origin is at zero, [2] flipped, [3] scaled with
flooring to the dst size, and [4] translated so that the dst rect has
the correct origin."
  
  (declare (type Chart-Rect result)
	   (:returns (type Chart-Rect result)))
  
  (let* ((src (src map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (dst (dst map))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (xmin (az:fl (- (x r) xsrc)))
	 (xmax (+ xmin (w r)))
	 ;; flip y interval here
	 (ymin (az:fl (- (h src) (- (ymax r) ysrc))))
	 (ymax (+ ymin (h r))))
    (declare (type Screen-Rect src)
	     (type Chart-Rect dst)
	     (type az:Int16 xsrc ysrc)
	     (type az:Card16 wsrc hsrc)
	     (type Double-Float xdst ydst wdst hdst xmin xmax ymin ymax))

    (setf xmin (/ (* xmin wdst) wsrc))
    (setf xmax (/ (* xmax wdst) wsrc))
    (setf (x result) (+ xmin xdst))
    (setf (w result) (- xmax xmin))

    (setf ymin (/ (* ymin hdst) hsrc))
    (setf ymax (/ (* ymax hdst) hsrc))
    (setf (y result) (+ ymin ydst))
    (setf (h result) (- ymax ymin))))

;;;------------------------------------------------------------

(defmethod inverse-transform ((map SC-Flip) (p Chart-Point)
			       &key (result (make-screen-point)))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map.

  To be precise, this is implemented as though the dst rect was: [1]
translated so that its origin is at zero, [2] scaled with ceiling to
the src size, [3] flipped, and [4] translated so that the src rect has
the correct origin."

  (declare (type Screen-Point result)
	   (:returns (type Screen-Point result)))
  (let* ((src (src map)) (dst (dst map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (xmin (x p)) (ymin (y p)))
    (declare (type Screen-Rect src)
	     (type Chart-Rect dst)
	     (type az:Int16 xsrc ysrc)
	     (type az:Card16 wsrc hsrc)
	     (type Double-Float xdst ydst wdst hdst xmin ymin))
    (setf (x result) (+ (ceiling (* wsrc (- xmin xdst)) wdst) xsrc))
    (setf (y result) (+ (- hsrc (ceiling (* hsrc (- ymin ydst)) hdst)) ysrc))))


(defmethod inverse-transform ((map SC-Flip) (r Chart-Rect)
			       &key (result (make-screen-rect)))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map.

  To be precise, this is implemented as though the dst rect was: [1]
translated so that its origin is at zero, [2] scaled with ceiling to
the src size, [3] flipped, and [4] translated so that the src rect has
the correct origin."

  (declare (type Screen-Rect result)
	   (:returns (type Screen-Rect result)))
  (let* ((src (src map))
	 (xsrc (x src)) (wsrc (w src)) (ysrc (y src)) (hsrc (h src))
	 (dst (dst map))
	 (xdst (x dst)) (wdst (w dst)) (ydst (y dst)) (hdst (h dst))
	 (w (w r))
	 (xmin (x r))
	 (xmax (+ xmin w))
	 (h (h r))
	 (ymin (y r))
	 (ymax (+ ymin h))
	 (xrmin 0) (xrmax 0) (yrmin 0) (yrmax 0))
    (declare (type Screen-Rect src)
	     (type Chart-Rect dst)
	     (type az:Int16 xsrc ysrc xrmin xrmax yrmin yrmax)
	     (type az:Card16 wsrc hsrc)
	     (type Double-Float xdst ydst wdst hdst xmin ymin w h xmax ymax))

    (setf xrmin (floor (* wsrc (- xmin xdst)) wdst))
    (setf xrmax (ceiling (* wsrc (- xmax xdst)) wdst))
    (setf (x result) (+ xrmin xsrc))
    (setf (w result) (- xrmax xrmin))

    (setf yrmin (floor (* hsrc (- ymin ydst)) hdst))
    (setf yrmax (ceiling (* hsrc (- ymax ydst)) hdst))
    (setf (y result) (+ (- hsrc yrmax) ysrc)) ;; flip y interval here
    (setf (h result) (- yrmax yrmin))))





