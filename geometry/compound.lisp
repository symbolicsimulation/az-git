;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================
;;; Compound Maps:
;;;=======================================================

(defclass Product-Map (Flat-Map)
	  ((factors
	    :type List
	    :initarg :factors
	    :reader factors)
	   (reversed-factors
	    :type List
	    :initarg :reversed-factors)))

;;;-------------------------------------------------------

(defmethod codomain ((t0 Product-Map))
  (codomain (az:first-elt (factors t0))))

(defmethod domain ((t0 Product-Map))
  (domain (az:last-elt (factors t0))))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Product-Map) slot-names &rest inits)
  (declare (ignore slot-names inits))
  (assert (not (null (factors t0))))
  ;; check for matching domains and codomains
  (let ((dom (domain (first (factors t0)))))
    (flet ((mate? (f)
	     (assert (eql dom (codomain f)))
	     (setf dom (domain f))))
      (map nil #'mate? (rest (factors t0)))))
  ;; discover properties:
  (setf (properties t0)
    (nconc
     (properties t0)
     (mapcan #'(lambda (p) (when (every p (factors t0)) (list p)))
	     *properties-of-linear-maps-preserved-by-products*)))
  (delete-duplicates (properties t0)))

;;;-------------------------------------------------------
;;; lazy evaluation of <reversed-factors>:

(defmethod (setf factors) ((fs List) (t0 Product-Map))
  #||(let ((vspace (codomain (first fs))))
    (setf fs (delete-if #'identity-map? fs))
    (when (null fs) (setf fs (list (the-identity-map vspace)))))||#
  (slot-makunbound  t0 'reversed-factors)
  (setf (slot-value t0 'factors) fs)
  (factors t0))

(defmethod reversed-factors ((t0 Product-Map))
  (unless (slot-boundp t0 'reversed-factors)
    (setf (slot-value t0 'reversed-factors) (reverse (factors t0))))
  (slot-value t0 'reversed-factors))

;;;-------------------------------------------------------

(defun check-factors (t0 prop)
  (declare (type Product-Map t0)
	   (type Symbol prop))
  (if *verify?*
      (every #'(lambda (f) (funcall prop f)) (factors t0))
      (member prop (properties t0))))

(defmethod modifier? ((t0 Product-Map))
  (check-factors t0 'modifier?))

(defmethod orthogonal? ((t0 Product-Map))
  (check-factors t0 'orthogonal?))

(defmethod orthogonal-rows? ((t0 Product-Map))
  (check-factors t0 'orthogonal-rows?))

(defmethod orthogonal-columns? ((t0 Product-Map))
  (check-factors t0 'orthogonal-columns?))

;;;-------------------------------------------------------

(defmethod add-property! ((t0 Product-Map) (prop Symbol))
  (case prop
    ((modifier? orthogonal? orthogonal-rows? orthogonal-columns?)
      (pushnew prop (properties t0)))
    (otherwise
      (warn "adding unknown property ~a" prop)))
  t0)

(defmethod delete-property! ((t0 Product-Map) (prop Symbol))
  (case prop
    ((modifier? orthogonal? orthogonal-rows? orthogonal-columns?) 
     (setf (properties t0) (delete prop (properties t0))))
    (otherwise
      (warn "deleting unknown property ~a" prop)
      (setf (properties t0) (delete prop (properties t0))))))

;;;-------------------------------------------------------
;;; collect the leaves fo the factor tree:

(defmethod harvest-factors ((t0 Linear-Map)) (list t0))

(defmethod harvest-factors ((t0 Product-Map))
  (let ((all-factors ()))
    (map nil
	 #'(lambda (f)
	     (setf all-factors (nconc all-factors (harvest-factors f))))
	 (factors t0))
    all-factors))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Product-Map) &key result)
  (when (not (or (null result) (eq t0 result)))	     
    (warn "Optional Result ~a ignored." result))
  (if (eq t0 result)
      (transpose! t0)
      ;; else
      (make-instance
	'Product-Map
	:factors (map 'List #'transpose (reversed-factors t0)))))

(defmethod transpose! ((t0 Product-Map))
  (setf (factors t0) (nreverse (factors t0))) 
  (map nil #'transpose! (factors t0))
  t0)

;;;=======================================================
;;; Generalization of Cholesky decompose.

(defclass Symmetric-Outer-Product (Symmetric)
     ((left-factor
       :type Linear-Map
       :initarg :left-factor
       :accessor left-factor)))

(defmethod codomain ((t0 Symmetric-Outer-Product)) (codomain (left-factor t0)))
(defmethod domain  ((t0 Symmetric-Outer-Product)) (codomain (left-factor t0)))
  
;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Symmetric-Outer-Product))
  (make-instance 'Symmetric-Outer-Product
    :domain (domain t0)
    :codomain (codomain t0)
    :left-factor (az:copy (left-factor t0))))

(defmethod az:copy-to! ((t0 Symmetric-Outer-Product)
			(result Symmetric-Outer-Product))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (az:copy (left-factor t0) :result (left-factor result))
  (reinitialize-instance result)
  result)

;;;=======================================================

(defclass Symmetric-Inner-Product (Symmetric)
     ((right-factor
       :type Linear-Map
       :initarg :right-factor
       :accessor right-factor)))

(defmethod codomain ((t0 Symmetric-Inner-Product)) (domain (right-factor t0)))
(defmethod domain ((t0 Symmetric-Inner-Product)) (domain (right-factor t0)))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Symmetric-Inner-Product))
  (make-instance 'Symmetric-Inner-Product
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :right-factor (az:copy (right-factor t0))))

(defmethod az:copy-to! ((t0 Symmetric-Inner-Product)
			(result Symmetric-Inner-Product))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (az:copy (right-factor t0) :result (right-factor result))
  (reinitialize-instance result)
  result)

;;;=======================================================

(defclass Orthogonal-Similarity-Product (Linear-Map)
     ((middle-factor
       :type Linear-Map
       :initarg :middle-factor
       :accessor middle-factor)
      (right-factor
       :type Linear-Map
       :initarg :right-factor
       :accessor right-factor)))

;;;-------------------------------------------------------

(defmethod codomain ((t0 Orthogonal-Similarity-Product))
  (codomain (right-factor t0)))

(defmethod domain   ((t0 Orthogonal-Similarity-Product))
  (domain (right-factor t0)))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Orthogonal-Similarity-Product)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (assert (orthogonal-rows? (right-factor t0)))
  (assert (eql (codomain (middle-factor t0)) (domain (right-factor t0)))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Orthogonal-Similarity-Product))
  (make-instance 'Orthogonal-Similarity-Product
		 :right-factor (az:copy (right-factor t0))
		 :middle-factor (az:copy (middle-factor t0))))

(defmethod az:copy-to! ((t0 Orthogonal-Similarity-Product)
			(result Orthogonal-Similarity-Product))
  (az:copy (right-factor t0) :result (right-factor result))
  (az:copy (middle-factor t0) :result (middle-factor result))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------

(defmethod transpose ((t0 Orthogonal-Similarity-Product) &key result)
  (when (not (or (null result) (eq t0 result)))	     
    (warn "Optional Result ~a ignored." result))
  (if (eq t0 result)
      (transpose! t0)
      ;; else
      (make-instance 'Orthogonal-Similarity-Product
		     :right-factor (az:copy (right-factor t0))
		     :middle-factor (transpose (middle-factor t0)))))

(defmethod transpose! ((t0 Orthogonal-Similarity-Product))
  (setf (middle-factor t0) (transpose! (middle-factor t0)))
  t0)

;;;=======================================================

(defclass Tensor-Product (Linear-Map)
	  ((v-codomain
	    :type Point
	    :accessor v-codomain
	    :initarg :v-codomain)
	   (v-domain
	    :type Point
	    :accessor v-domain
	    :initarg :v-domain)))

;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 Tensor-Product) slots &rest inits)
  (declare (ignore slots inits))
  (unless (slot-boundp t0 'v-domain)
    (setf (v-domain t0) (make-zero-element (domain t0))))
  (unless (slot-boundp t0 'v-codomain)
    (setf (v-codomain t0) (make-zero-element (codomain t0))))
  (assert (element? (v-codomain t0) (codomain t0)))
  (assert (element? (v-domain t0) (domain t0))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Tensor-Product))
  (make-instance 'Tensor-Product
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :v-codomain (az:copy (v-codomain t0))
		 :v-domain (az:copy (v-domain t0))))

(defmethod az:copy-to! ((t0 Tensor-Product) (result Tensor-Product))
  (az:copy (slot-value t0 'v-codomain) :result (v-codomain result))
  (az:copy (slot-value t0 'v-domain) :result (v-domain result))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------

(defmethod transpose ((t0 Tensor-Product)
		      &key (result (make-instance 'Tensor-Product)))
  (setf (v-codomain result) (v-domain t0))
  (setf (v-domain result) (v-codomain t0))
  (setf (codomain result) (domain t0))
  (setf (domain result) (codomain t0))
  result)

;;;=======================================================

(defclass Symmetric-Tensor-Product (Symmetric)
	  ((v-domain
	    :type Point
	    :accessor v-domain
	    :initarg :v-domain)))

;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 Symmetric-Tensor-Product)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (unless (slot-boundp t0 'v-domain)
    (setf (v-domain t0) (make-zero-element (domain t0))))
  (assert (element? (v-domain t0) (domain t0))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Symmetric-Tensor-Product))
  (make-instance 'Symmetric-Tensor-Product :vec (az:copy (v-domain t0))))

(defmethod az:copy-to! ((t0 Symmetric-Tensor-Product)
			(result Symmetric-Tensor-Product))
  (az:copy (v-domain t0) :result (v-domain result))
  (reinitialize-instance result)
  result)
  
;;;=======================================================

(defmethod tensor-product ((v-codomain Point) (v-domain Point)
			   &key
			   (domain (home v-domain))
			   (codomain (home v-codomain))
			   (result
			    (if (and (eq codomain domain)
				     (eq v-codomain v-domain))
			      (make-instance 'Symmetric-Tensor-Product
				:codomain codomain :domain domain )
			      (make-instance 'Tensor-Product
				:domain domain :codomain codomain))))
  (cond ((typep result 'Symmetric-Tensor-Product)
	 (assert (and (eq codomain domain) (eq v-codomain v-domain)))
	 (assert (element? v-domain (domain result)))
	 (setf (v-domain result) v-domain))
	(t
	 (assert (element? v-domain (domain result)))
	 (assert (element? v-codomain (codomain result)))
	 (setf (v-domain result) v-domain)
	 (setf (v-codomain result) v-codomain)))
  result)


;;;=======================================================

(defclass 2x2-Direct-Sum-Map (Flat-Map)
	  ((domain
	    :type Direct-Sum-Space
	    :accessor domain
	    :initarg :domain)
	   (codomain
	    :type Direct-Sum-Space     
	    :accessor codomain
	    :initarg :codomain)
	   (t00
	    :type Flat-Map
	    :accessor t00
	    :initarg :t00)
	   (t01
	    :type Flat-Map
	    :accessor t01
	    :initarg :t01)
	   (t10
	    :type Flat-Map
	    :accessor t10
	    :initarg :t10)
	   (t11
	    :type Flat-Map
	    :accessor t11
	    :initarg :t11)))

;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 2x2-Direct-Sum-Map)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (let ((xc (space0 (codomain t0)))
	(yc (space1 (codomain t0)))
	(xd (space0 (domain t0)))
	(yd (space1 (domain t0))))
    (if (slot-boundp t0 't00)
      (assert (and (eq (codomain (t00 t0)) (space0 (codomain t0)))
		   (eq (codomain (t00 t0)) (space0 (codomain t0)))))
      (setf (t00 t0) (make-linear-map xc xd)))
    (if (slot-boundp t0 't01)
      (assert (and (eq (codomain (t01 t0)) (space0 (codomain t0)))
		   (eq (domain   (t01 t0)) (space1 (domain t0)))))
      (setf (t01 t0) (make-linear-map xc yd)))
    (if (slot-boundp t0 't10)
      (assert (and (eq (codomain (t10 t0)) (space1 (codomain t0)))
		   (eq (domain   (t10 t0)) (space0 (domain t0)))))
      (setf (t10 t0) (make-linear-map yc xd)))
    (if (slot-boundp t0 't11)
      (assert (and (eq (codomain (t11 t0)) (space1 (codomain t0)))
		   (eq (domain   (t11 t0)) (space1 (domain t0)))))
      (setf (t11 t0) (make-linear-map yc yd)))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 2x2-Direct-Sum-Map))
  (make-instance '2x2-Direct-Sum-Map
    :domain (domain t0)
    :codomain (codomain t0)
    :t00 (az:copy (t00 t0))
    :t01 (az:copy (t01 t0))
    :t10 (az:copy (t10 t0))
    :t11 (az:copy (t11 t0))))

(defmethod az:copy-to! ((t0 2x2-Direct-Sum-Map) (result 2x2-Direct-Sum-Map))
  (az:copy (t00 t0) :result (t00 result))
  (az:copy (t01 t0) :result (t01 result))
  (az:copy (t10 t0) :result (t10 result))
  (az:copy (t11 t0) :result (t11 result))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------

(defmethod identity! ((t0 2x2-Direct-Sum-Map))
  (setf (t00 t0) (identity! (t00 t0)))
  (setf (t01 t0) (zero! (t01 t0)))
  (setf (t10 t0) (zero! (t10 t0)))
  (setf (t11 t0) (identity! (t11 t0)))
  t0)

;;;=======================================================
;;; It's more efficient to use the usual tensor product
;;; representation for tensor-products of direct-sum's
;;; than what's below:
#|| 
(defmethod tensor-product ((v-cod Direct-Sum-Point) (v-dom Direct-Sum-Point)
			   &key
			   result
			   (domain (home v-dom))
			   (codomain (home v-cod)))
  (let ((x-cod (x v-cod))
	(x-dom (x v-dom))
	(y-cod (y v-cod))
	(y-dom (y v-dom)))
    (cond
     ((null result)
      (setf result
	(make-instance '2x2-Direct-Sum-Map
	  :domain domain
	  :codomain codomain
	  :t00 (tensor-product x-cod x-dom)
	  :t01 (tensor-product x-cod y-dom)
	  :t10 (tensor-product y-cod x-dom)
	  :t11 (tensor-product y-cod y-dom))))
     (t
      (az:type-check 2x2-Direct-Sum-Map result)
      (setf (t00 result) (tensor-product x-cod x-dom (t00 result)))
      (setf (t01 result) (tensor-product x-cod y-dom (t01 result)))
      (setf (t10 result) (tensor-product y-cod x-dom (t10 result)))
      (setf (t11 result) (tensor-product y-cod y-dom (t11 result))))))
  result)
||#
;;;=======================================================
