;;; -*-Mode: Lisp; Syntax: Common-Lisp; Package:Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================
;;;	 Extended V operations
;;;=======================================================
;;;
;;; The idea here is to define a set of operations on 1d arrays and 1d
;;; portions of 2d arrays. They are analogs of the Fortran BLAS used in
;;; Linpack and elsewhere. There are 2 purposes for doing this:
;;;
;;; 1) The implementation of the operations can be specialized
;;;  for a particular Lisp environment to get high performance. In the
;;;  Symbolics this means using array register declarations. It might
;;;  also mean using an array processor. In Lisps with particularly
;;;  bad Floating point implementations, it might mean calling Fortran,
;;;  C or assembly routines to avoid boxing.
;;;
;;; 2) It may simplify common iterations and reduce our dependence
;;;  on the LOOP macro (especially if we provide some S or APL-like
;;;  syntatic sugar to make it look like implied iterations).
;;;
;;; We will define a number of generalized 1d array operations.A
;;; generalized 1d array is: a (partial) 1d array, (partial) row of 2d
;;; array, or (partial) column of 2d array.
;;; 
;;; As a general policy, the burden of type, size, bounds checking, etc.
;;; is left to the caller. These routines are supposed to be as fast as
;;; possible and using them means that you are willing to sacrifice
;;; safety for speed.
;;;
;;;=======================================================

(defmacro vlength (vtype array)
  (ecase vtype
    ((:vec :vector :col :column) `(array-dimension ,array 0))
    (:row                        `(array-dimension ,array 1))
    ((:diag :diagonal)           `(min (array-dimension ,array 0)
				       (array-dimension ,array 1)))))
(defun vref (vtype v i on)
  (ecase vtype
    ((:vec :vector) `(aref ,v ,i))
    (:row `(aref ,v ,on ,i))
    ((:col :column) `(aref ,v ,i ,on))
    ((:diag :diagonal) `(aref ,v ,i ,i))))

(defun vdeclaration (vtype)
  (ecase vtype
    ((:vec :vector) 'az:Float-Vector)
    ((:row :col :column :diag :diagonal) 'az:Float-Matrix)))

;;;=======================================================

(defun vop-arg-checks (v vtype start end on)
  `(progn
     (assert (member ,vtype '(:vec :vector :row :col :column :diag :diagonal)))
     (ecase ,vtype
       ((:vec :vector)
	(az:type-check az:Float-Vector ,v))
       ((:row :col :column :diag :diagonal)
	(az:type-check az:Float-Matrix ,v)))
     (az:type-check az:Array-Index ,start ,end ,on)
     (assert (<= ,start ,end (vlength ,vtype ,v)))
     (unless (member ,vtype '(:vec :vector))
       (assert (<= ,on (ecase ,vtype
			 (:row              (array-dimension ,v 0))
			 ((:col :column)    (array-dimension ,v 1))
			 ((:diag :diagonal) (min (array-dimension ,v 0)
						 (array-dimension ,v 1)))))))))

;;;=======================================================
;;;	Find the maximum element
;;;=======================================================

(defmacro v-abs-max-index (v
			   &key
			   (vtype :vec)
			   (start 0)
			   (end `(vlength ,vtype ,v))
			   (on 0)
			   (safe? t))
  (az:once-only
   (v start end on)
   (let ((i (gensym))
	 (imax (gensym))
	 (max (gensym))
	 (val (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v ,vtype ,start ,end ,on))
	(let ((,v ,v)
	      (,imax 0)
	      (,val 0.0d0)
	      (,max 0.0d0))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype) ,v)
	    `(type az:Array-Index ,start ,end ,on ,imax)
	    `(type Double-Float ,val ,max))
	  ,on ;; eliminate errors if it's not used by <vref>
	  (az:for (,i ,start ,end)
		  (setf ,val (abs ,(vref vtype v i on)))
		  (when (> ,val ,max)
		    (setf ,imax ,i)
		    (setf ,max ,val)))
	  ,imax)))))

;;;=======================================================
;;; L-infinity Norm
;;;=======================================================

(defmacro v-abs-max (v
		     &key
		     (vtype :vec)
		     (start 0)
		     (end `(vlength ,vtype ,v))
		     (on 0)
		     (safe? t)) 
  (az:once-only
   (v start end on)
   (let ((i (gensym))
	 (max (gensym))
	 (val (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v ,vtype ,start ,end ,on))
	(let ((,v ,v)
	      (,val 0.0d0)
	      (,max 0.0d0))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype) ,v)
	    `(type az:Array-Index ,start ,end ,on)
	    `(type Double-Float ,max ,val))
	  ,on ;; eliminate errors if it's not used by <vref>
	  (az:for (,i ,start ,end)
		  (setf ,val (abs ,(vref vtype v i on)))
		  (when (> ,val ,max)
		    (setf ,max ,val)))
	  ,max)))))

(defmacro v-sup-norm (v
		      &key
		      (vtype :vec)
		      (start 0)
		      (end `(vlength ,vtype ,v))
		      (on 0)
		      (safe? t))
  `(v-abs-max ,v :vtype ,vtype :start ,start :end ,end :on ,on :safe? ,safe?))

;;;=======================================================
;;; L2 Norm
;;;=======================================================

(defmacro v-l2-norm2 (v
		      &key
		      (vtype :vec)
		      (start 0)
		      (end `(vlength ,vtype ,v))
		      (on 0)
		      (safe? t))
  (az:once-only
   (v start end on)
   (let ((i (gensym))
	 (sum (gensym))
	 (val (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v ,vtype ,start ,end ,on))
	(let ((,v ,v)
	      (,val 0.0d0)
	      (,sum 0.0d0))
	  ,(az:clean-declare `(type ,(vdeclaration vtype) ,v)
			     `(type az:Array-Index ,start ,end ,on)
			     `(type Double-Float ,val ,sum))
	  ,on ;; eliminate errors if it's not used by <vref>
	  (az:for (,i ,start ,end)
		  (setf ,val ,(vref vtype v i on))
		  (incf ,sum (* ,val ,val)))
	  ,sum)))))

(defmacro v-l2-norm (v
		     &key
		     (vtype :vec)
		     (start 0)
		     (end `(vlength ,vtype ,v))
		     (on 0)
		     (safe? t)) 
  `(sqrt
    (v-l2-norm2
     ,v :start ,start :end ,end :vtype ,vtype :on ,on :safe? ,safe?)))

;;;=======================================================
;;; L1 Norm
;;;=======================================================

(defmacro v-l1-norm (v
		     &key
		     (vtype :vec)
		     (start 0)
		     (end `(vlength ,vtype ,v))
		     (on 0)
		     (safe? t))
  (az:once-only
   (v start end on)
   (let ((i (gensym))
	 (sum (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v ,vtype ,start ,end ,on))
	(let ((,v ,v)
	      (,sum 0.0d0))
	  ,(az:clean-declare `(type ,(vdeclaration vtype) ,v)
			     `(type az:Array-Index ,start ,end ,on)
			     `(type Double-Float ,sum))
	  ,on ;; eliminate errors if it's not used by <vref>
	  (az:for (,i ,start ,end)
		  (incf ,sum (abs ,(vref vtype v i on))))
	  ,sum)))))

;;;=======================================================
;;; Fill with a constant
;;;=======================================================

(defmacro v<-x! (v x
		 &key
		 (vtype :vec)
		 (start 0)
		 (end `(vlength ,vtype ,v))
		 (on 0)
		 (safe? t))
  (az:once-only
   (v start end on x)
   (let ((i (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v ,vtype ,start ,end ,on))
	,(when (and safe? (not (typep x 'Double-Float)))
	   `(az:type-check Double-Float ,x))
	(let ((,v ,v))
	  ,(az:clean-declare `(type ,(vdeclaration vtype) ,v)
			     `(type az:Array-Index ,start ,end ,on)
			     `(type Double-Float ,x))
	  ,on ;; eliminate errors if it's not used by <vref>
	  (az:for (,i ,start ,end) (setf ,(vref vtype v i on) ,x))
	  ,v)))))

;;;=======================================================
;;; Increment elements
;;;=======================================================

(defmacro v+x! (v x
		&key
		(vtype :vec)
		(start 0)
		(end `(vlength ,vtype ,v))
		(on 0)
		(safe? t))
  (az:once-only
   (v start end on x)
   (let ((i (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v ,vtype ,start ,end ,on))
	,(when (and safe? (not (typep x 'Double-Float)))
	   `(az:type-check Double-Float ,x))
	(let ((,v ,v))
	  ,(az:clean-declare `(type ,(vdeclaration vtype) ,v)
			     `(type az:Array-Index ,start ,end ,on)
			     `(type Double-Float ,x))
	  ,on ;; eliminate errors if it's not used by <vref>
	  (az:for (,i ,start ,end) (incf ,(vref vtype v i on) ,x))
	  ,v)))))

;;;=======================================================
;;; Scale elements
;;;=======================================================

(defmacro v*x! (v x
		&key
		(vtype :vec)
		(start 0)
		(end `(vlength ,vtype ,v))
		(on 0)
		(safe? t))
  (az:once-only
   (v start end on x)
   (let ((i (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v ,vtype ,start ,end ,on))
	,(when (and safe? (not (typep x 'Double-Float)))
	   `(az:type-check Double-Float ,x))	
	(locally
	  ,(az:clean-declare `(type ,(vdeclaration vtype) ,v)
			     `(type az:Array-Index ,start ,end ,on)
			     `(type Double-Float ,x))
	  ,on ;; eliminate errors if it's not used by <vref>
	  (az:for (,i ,start ,end) (az:mulf ,(vref vtype v i on) ,x))
	  ,v)))))

;;;=======================================================
;;; Dot Product
;;;=======================================================

(defmacro v.v (v0 v1
	       &key
	       (vtype0 :vec)
	       (start0 0)
	       (end0 `(vlength ,vtype0 ,v0))
	       (on0 0)
	       (vtype1 :vec)
	       (start1 0)
	       (end1 `(vlength ,vtype1 ,v1))
	       (on1 0)
	       (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym))
	 (sum (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))) 
	      (,sum 0.0d0))
	  ,(az:clean-declare `(type ,(vdeclaration vtype0) ,v0)
			     `(type ,(vdeclaration vtype1) ,v1)
			     `(type az:Array-Index
				    ,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			     `(type az:Positive-Float ,sum))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (incf ,sum (* ,(vref vtype0 v0 i0 on0)
				,(vref vtype1 v1 i1 on1))))
	  ,sum)))))

;;;=======================================================
;;; L1 Distance
;;;=======================================================

(defmacro v-l1-dist (v0 v1
		     &key
		     (vtype0 :vec)
		     (start0 0)
		     (end0 `(vlength ,vtype0 ,v0))
		     (on0 0)
		     (vtype1 :vec)
		     (start1 0)
		     (end1 `(vlength ,vtype1 ,v1))
		     (on1 0)
		     (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym))
	 (sum (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))) 
	      (,sum 0.0d0))
	  ,(az:clean-declare `(type ,(vdeclaration vtype0) ,v0)
			     `(type ,(vdeclaration vtype1) ,v1)
			     `(type az:Array-Index
				    ,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			     `(type az:Positive-Float ,sum))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (incf ,sum (abs (the Double-Float
				    (- ,(vref vtype0 v0 i0 on0)
				       ,(vref vtype1 v1 i1 on1))))))
	  ,sum)))))

;;;=======================================================
;;; L2 Distance
;;;=======================================================

(defmacro v-l2-dist2 (v0 v1
		      &key
		      (vtype0 :vec)
		      (start0 0)
		      (end0 `(vlength ,vtype0 ,v0))
		      (on0 0)
		      (vtype1 :vec)
		      (start1 0)
		      (end1 `(vlength ,vtype1 ,v1))
		      (on1 0)
		      (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym))
	 (val (gensym))
	 (sum (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1)))
	      (,val 0.0d0)
	      (,sum 0.0d0))
	  ,(az:clean-declare `(type ,(vdeclaration vtype0) ,v0)
			     `(type ,(vdeclaration vtype1) ,v1)
			     `(type az:Array-Index
				    ,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			     `(type az:Positive-Float ,sum))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (setf ,val (- ,(vref vtype0 v0 i0 on0)
				,(vref vtype1 v1 i1 on1)))
		  (incf ,sum (* ,val ,val)))
	  ,sum)))))

(defmacro v-l2-dist (v0 v1
		     &key
		     (vtype0 :vec)
		     (start0 0)
		     (end0 `(vlength ,vtype0 ,v0))
		     (on0 0)
		     (vtype1 :vec)
		     (start1 0)
		     (end1 `(vlength ,vtype1 ,v1))
		     (on1 0)
		     (safe? t))

  `(sqrt
    (%v-l2-dist2 ,v0 ,v1
		 :start0 ,start0 :end0 ,end0 :vtype0 ,vtype0 :on0 ,on0
		 :start1 ,start1 :end1 ,end1 :vtype1 ,vtype1 :on1 ,on1
		 :safe? ,safe?)))

;;;=======================================================
;;; Sup Distance
;;;=======================================================

(defmacro v-sup-dist (v0 v1
		      &key
		      (vtype0 :vec)
		      (start0 0)
		      (end0 `(vlength ,vtype0 ,v0))
		      (on0 0)
		      (vtype1 :vec)
		      (start1 0)
		      (end1 `(vlength ,vtype1 ,v1))
		      (on1 0)
		      (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym))
	 (val (gensym))
	 (max (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1)))
	      (,val 0.0d0)
	      (,max 0.0d0))
	  ,(az:clean-declare `(type ,(vdeclaration vtype0) ,v0)
			     `(type ,(vdeclaration vtype1) ,v1)
			     `(type az:Array-Index
				    ,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
			     `(type Double-Float ,val)
			     `(type az:Positive-Float ,max))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (setf ,val (abs (the Double-Float
				    (- ,(vref vtype0 v0 i0 on0)
				       ,(vref vtype1 v1 i1 on1)))))
		  (when (>= ,val ,max) (setf ,max ,val)))
	  ,max)))))

;;;=======================================================
;;; Copy elements
;;;=======================================================

(defmacro v<-v! (v0 v1
		 &key
		 (vtype0 :vec)
		 (start0 0)
		 (end0 `(vlength ,vtype0 ,v0))
		 (on0 0)
		 (vtype1 :vec)
		 (start1 0)
		 (end1 `(vlength ,vtype1 ,v1))
		 (on1 0)
		 (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype0) ,v0)
	    `(type ,(vdeclaration vtype1) ,v1)
	    `(type az:Array-Index ,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (setf ,(vref vtype0 v0 i0 on0)
		    ,(vref vtype1 v1 i1 on1)))
	  ,v0)))))

;;;=======================================================
;;; Swap elements
;;;=======================================================

(defmacro v<->v! (v0 v1
		  &key
		  (vtype0 :vec)
		  (start0 0)
		  (end0 `(vlength ,vtype0 ,v0))
		  (on0 0)
		  (vtype1 :vec)
		  (start1 0)
		  (end1 `(vlength ,vtype1 ,v1))
		  (on1 0)
		  (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype0) ,v0)
	    `(type ,(vdeclaration vtype1) ,v1)
	    `(type az:Array-Index ,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (rotatef ,(vref vtype0 v0 i0 on0) ,(vref vtype1 v1 i1 on1)))
	  ,v0)))))

;;;=======================================================
;;; Elementwise addition
;;;=======================================================

(defmacro v+v! (v0 v1
		&key
		(vtype0 :vec)
		(start0 0)
		(end0 `(vlength ,vtype0 ,v0))
		(on0 0)
		(vtype1 :vec)
		(start1 0)
		(end1 `(vlength ,vtype1 ,v1))
		(on1 0)
		(safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	  ,(az:clean-declare `(type ,(vdeclaration vtype0) ,v0)
			     `(type ,(vdeclaration vtype1) ,v1)
			     `(type az:Array-Index
				    ,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (incf ,(vref vtype0 v0 i0 on0)
			,(vref vtype1 v1 i1 on1)))
	  ,v0))))) 

;;;=======================================================
;;; Elementwise subtraction
;;;=======================================================

(defmacro v-v! (v0 v1
		&key
		(vtype0 :vec)
		(start0 0)
		(end0 `(vlength ,vtype0 ,v0))
		(on0 0)
		(vtype1 :vec)
		(start1 0)
		(end1 `(vlength ,vtype1 ,v1))
		(on1 0)
		(safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	  ,(az:clean-declare `(type ,(vdeclaration vtype0) ,v0)
			     `(type ,(vdeclaration vtype1) ,v1)
			     `(type az:Array-Index
				    ,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (decf ,(vref vtype0 v0 i0 on0)
			,(vref vtype1 v1 i1 on1)))
	  ,v0))))) 

;;;=======================================================
;;; Elementwise multiplication
;;;=======================================================

(defmacro v*v! (v0 v1
		&key
		(vtype0 :vec)
		(start0 0)
		(end0 `(vlength ,vtype0 ,v0))
		(on0 0)
		(vtype1 :vec)
		(start1 0)
		(end1 `(vlength ,vtype1 ,v1))
		(on1 0)
		(safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1)
   (let ((i0 (gensym))
	 (i1 (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype0) ,v0)
	    `(type ,(vdeclaration vtype1) ,v1)
	    `(type az:Array-Index ,start0 ,end0 ,on0 ,start1 ,end1 ,on1))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (az:mulf ,(vref vtype0 v0 i0 on0)
			   ,(vref vtype1 v1 i1 on1)))
	  ,v0)))))

;;;=======================================================
;;; Variations on the blas Saxpy
;;;=======================================================
;;;
;;; These functions overwrite the specified elements of their 1st
;;; argument with a scalar times the correspomnding element of the
;;; second arg.

(defmacro v<-v*x! (v0 v1 x
		   &key
		   (vtype0 :vec)
		   (start0 0)
		   (end0 `(vlength ,vtype0 ,v0))
		   (on0 0)
		   (vtype1 :vec)
		   (start1 0)
		   (end1 `(vlength ,vtype1 ,v1))
		   (on1 0)
		   (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1 x)
   (let ((i0 (gensym))
	 (i1 (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype0) ,v0)
	    `(type ,(vdeclaration vtype1) ,v1)
	    `(type az:Array-Index ,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
	    `(type Double-Float ,x))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (setf ,(vref vtype0 v0 i0 on0)
		    (* ,(vref vtype1 v1 i1 on1) ,x)))
	  ,v0)))))

;;;=======================================================
;;; blas Saxpy
;;;=======================================================
;;;
;;; These functions overwrite the specified elements of their 1st
;;; argument with the sum of each element and x scalar times the
;;; correspomnding element of the second arg.

(defmacro v+v*x! (v0 v1 x
		  &key
		  (vtype0 :vec)
		  (start0 0)
		  (end0 `(vlength ,vtype0 ,v0))
		  (on0 0)
		  (vtype1 :vec)
		  (start1 0)
		  (end1 `(vlength ,vtype1 ,v1))
		  (on1 0)
		  (safe? t))
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1 x)
   (let ((i0 (gensym))
	 (i1 (gensym)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1))))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype0) ,v0)
	    `(type ,(vdeclaration vtype1) ,v1)
	    `(type az:Array-Index ,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
	    `(type Double-Float ,x))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (incf ,(vref vtype0 v0 i0 on0)
			(* ,(vref vtype1 v1 i1 on1) ,x)))
	  ,v0)))))

;;;=======================================================
;;;	      blas Srot
;;;=======================================================

(defmacro v-rot-v! (v0 v1 c s
		    &key
		    (vtype0 :vec)
		    (start0 0)
		    (end0 `(vlength ,vtype0 ,v0))
		    (on0 0)
		    (vtype1 :vec)
		    (start1 0)
		    (end1 `(vlength ,vtype1 ,v1))
		    (on1 0)
		    (safe? t)) 
  (az:once-only
   (v0 start0 end0 on0 v1 start1 end1 on1 c s)
   (let* ((i0 (gensym))
	  (i1 (gensym))
	  (val0 (gensym))
	  (val1 (gensym))
	  (access0 (vref vtype0 v0 i0 on0))
	  (access1 (vref vtype1 v1 i1 on1)))
     `(progn
	,(when safe? `(vop-arg-checks ,v0 ,vtype0 ,start0 ,end0 ,on0))
	,(when safe? `(vop-arg-checks ,v1 ,vtype1 ,start1 ,end1 ,on1))
	(let (,@(if (eql v0 v1) `((,v0 ,v0)) `((,v0 ,v0) (,v1 ,v1)))
	      (,val0 0.0d0)
	      (,val1 0.0d0))
	  ,(az:clean-declare
	    `(type ,(vdeclaration vtype0) ,v0) 
	    `(type ,(vdeclaration vtype1) ,v1)
	    `(type az:Array-Index ,start0 ,end0 ,on0 ,start1 ,end1 ,on1)
	    `(type Double-Float ,c ,s ,val0 ,val1))
	  ,on0 ,on1 ;; eliminate errors if not used by <vref>
	  (az:for ((,i0 ,start0 ,end0)
		   (,i1 ,start1 ,end1))
		  (setf ,val0 ,access0)
		  (setf ,val1 ,access1)
		  (setf ,access0 (+ (* ,s ,val1) (* ,c ,val0)))
		  (setf ,access1 (- (* ,c ,val1) (* ,s ,val0))))
	  ,v0)))))

;;;=======================================================
;;;	       experimental 2d array ops
;;;=======================================================

(defun abs-max-2d (v)
  (declare (type az:Float-Matrix v)) 
  (let ((v v)
	(val 0.0d0)
	(max 0.0d0))
    (declare (optimize (safety 1) (speed 0) (space 3))
	     (type az:Float-Matrix v)
	     (type (Double-Float 0.0d0) val max))
    (dotimes (i (array-dimension v 0))
      (declare (type az:Array-Index i))
      (dotimes (j (array-dimension v 1))
	(declare (type az:Array-Index j))
	(setf val (abs (aref v i j)))
	(when (> val max)
	  (setf max val))))
    max))




