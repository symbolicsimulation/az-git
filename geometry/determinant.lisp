;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================

(defmethod determinant :before ((t0 Linear-Map))
  (assert (square? t0)))

;;;=======================================================

(defmethod determinant ((t0 Matrix-Super)) (determinant (copy-to-matrix t0)))

;;;-------------------------------------------------------

(defmethod determinant ((t0 Matrix))
  (cond
   ((positive-definite? t0) (az:flsq (determinant (left-triangular-sqrt t0))))
   ;; triangular includes diagonal matrices
   ((triangular? t0)
    (let ((det 1.0d0) (2d (coords t0)))
      (az:for (i (coord-start (domain t0)) (coord-end (domain t0)))
	      (setf det (* det (aref 2d i i))))
      det))
   ;;((symmetric? t0) (determinant (eigen-decompose t0)))
   ((upper-hessenberg? t0) (determinant (qr-decompose t0)))
   (t (determinant (lq-decompose t0)))))

;;;-------------------------------------------------------

(defmethod determinant ((t0 Inverse-Upper-Triangular-Matrix))
  (let ((det (determinant (source t0))))
    (assert (not (zerop det)))
    (/ 1.0d0 (determinant (source t0)))))

(defmethod determinant ((t0 Inverse-Lower-Triangular-Matrix))
  (let ((det (determinant (source t0))))
    (assert (not (zerop det)))
    (/ 1.0d0 (determinant (source t0)))))

;;;-------------------------------------------------------

(defmethod determinant ((t0 Product-Map))
  (let ((det 1.0d0))
    (map nil
	 #'(lambda (f) (setf det (* det (determinant f))))
	 (factors t0))
    det))

(defmethod determinant ((t0 Symmetric-Inner-Product))
  (az:flsq (determinant (right-factor t0))))

(defmethod determinant ((t0 Symmetric-Outer-Product))
  (az:flsq (determinant (left-factor t0))))

(defmethod determinant ((t0 Orthogonal-Similarity-Product))
  (determinant (middle-factor t0)))

;;;-------------------------------------------------------
;;; Are these  true?

(defmethod determinant ((t0 Gauss))        1.0d0)
(defmethod determinant ((t0 Givens))       1.0d0)
(defmethod determinant ((t0 Householder)) -1.0d0)

;;;-------------------------------------------------------

(defmethod determinant ((t0 Pivot))
  (if (= (i0 t0) (i1 t0)) 1.0d0 -1.0d0))

;;;-------------------------------------------------------

(defmethod determinant ((t0 Identity-Map)) 1.0d0)

;;;-------------------------------------------------------

(defmethod determinant ((t0 1d-Annihilator))
  (if (every #'zerop (vec t0))
      ;; then it's  an identity Map
      1.0d0
      ;; else it's singular, so:
      0.0d0))
