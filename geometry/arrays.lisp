;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================

(defmethod home ((a Array)) (array-space (array-dimensions a)))

(defmethod coords ((a Array)) (az:array-data a))

;;;-------------------------------------------------------
;;; Algebra
;;;-------------------------------------------------------

(defmethod linear-mix (x0 (a0 Array) x1 (a1 Array)
		       &key (result (az:copy a0)))
  (assert (az:equal-array-dimensions? a0 a1 result))
  (let ((v0 (az:array-data a0))
	(v1 (az:array-data a1))
	(vr (az:array-data result)))
    ;; optimize common special cases
    (cond
     ((and (typep v0 'az:Float-Vector)
	   (typep v1 'az:Float-Vector)
	   (typep vr 'az:Float-Vector))
      (az:linear-mix-float-vectors (az:fl x0) v0 (az:fl x1) v1 :result vr))
     ((and (typep v0 'az:Int16-Vector)
	   (typep v1 'az:Int16-Vector)
	   (typep v1 'az:Int16-Vector))
      (az:linear-mix-int16-vectors (az:fl x0) v0 (az:fl x1) v1 :result vr))
     (t ;; dont assume anything by default
      (dotimes (i (length v0))
	(declare (type az:Array-Index i))
	(setf (aref vr i) (+ (* x0 (aref v0 i)) (* x1 (aref v1 i))))))))
  result)

(defmethod add ((a0 Array) (a1 Array) &key (result (az:copy a0)))
  "Overwrites result with the sum of a0 and a1."
  (assert (az:equal-array-dimensions? a0 a1 result))
  (let ((v0 (az:array-data a0))
	(v1 (az:array-data a1))
	(vr (az:array-data result)))
    ;; optimize common special cases
    (cond ((and (typep v0 'az:Float-Vector)
		(typep v1 'az:Float-Vector)
		(typep vr 'az:Float-Vector))
	   (az:add-float-vectors v0 v1 :result vr))
	  ((and (typep v0 'az:Int16-Vector)
		(typep v1 'az:Int16-Vector)
		(typep v1 'az:Int16-Vector))
	   (az:add-int16-vectors v0 v1 :result vr))
	  (t ;; dont assume anything by default
	   (dotimes (i (length v0))
	     (declare (type az:Array-Index i))
	     (setf (aref vr i) (+ (aref v0 i) (aref v1 i)))))))
  result)

(defmethod sub ((a0 Array) (a1 Array) &key (result (az:copy a0)))
  "Overwrites result with the sum of a0 and a1."
  (assert (az:equal-array-dimensions? a0 a1 result))
  (let ((v0 (az:array-data a0))
	(v1 (az:array-data a1))
	(vr (az:array-data result)))
    ;; optimize common special cases
    (cond ((and (typep v0 'az:Float-Vector)
		(typep v1 'az:Float-Vector)
		(typep vr 'az:Float-Vector))
	   (az:sub-float-vectors v0 v1 :result vr))
	  ((and (typep v0 'az:Int16-Vector)
		(typep v1 'az:Int16-Vector)
		(typep vr 'az:Int16-Vector))
	   (az:sub-int16-vectors v0 v1 :result vr))
	  (t ;; dont assume anything by default
	   (dotimes (i (length v0))
	     (declare (type az:Array-Index i))
	     (setf (aref vr i) (- (aref v0 i) (aref v1 i)))))))
  result)

(defmethod scale (x (a Array) &key (result (az:copy a)))
  (declare (type Real x)
	   (type Simple-Array a result))
  (let ((v (az:array-data a))
	(r (az:array-data result)))
    (cond ((and (typep v 'az:Float-Vector)
		(typep r 'az:Float-Vector))
	   (az:scale-float-vector (az:fl x) v :result r))
	  ((and (typep v 'az:Int16-Vector)
		(typep r 'az:Int16-Vector))
	   (az:scale-int16-vector (az:fl x) v :result r))
	  (t ;; dont assume anything by default
	   (dotimes (i (length v))
	     (declare (type az:Array-Index i))
	     (setf (aref r i) (* (az:fl x) (aref v i)))))))
  result)

(defmethod negate ((a Array) &key (result (az:copy a)))
  (declare (type Simple-Array a result))
  (let ((v (az:array-data a))
	(r (az:array-data result)))
    (cond ((and (typep v 'az:Float-Vector)
		(typep r 'az:Float-Vector))
	   (az:negate-float-vector v :result r))
	  ((and (typep v 'az:Int16-Vector)
		(typep r 'az:Int16-Vector))
	   (az:negate-int16-vector v :result r))
	  (t ;; dont assume anything by default
	   (dotimes (i (length v))
	     (declare (type az:Array-Index i))
	     (setf (aref r i) (* -1.0d0 (aref v i)))))))
  result)

(defmethod zero! ((a Array))
  (let ((v (az:array-data a)))
    (etypecase v
      (az:Float-Vector (az:zero-float-vector (az:array-data a)))
      (az:Int16-Vector (az:zero-int16-vector (az:array-data a)))
      (az:Fixnum-Vector (az:zero-fixnum-vector (az:array-data a)))
      (az:Card8-Vector (az:zero-card8-vector (az:array-data a)))
      (az:Card16-Vector (az:zero-card16-vector (az:array-data a)))
      (az:Card24-Vector (az:zero-card24-vector (az:array-data a)))
      (az:Card28-Vector (az:zero-card28-vector (az:array-data a)))
      (az:Card32-Vector (az:zero-card32-vector (az:array-data a)))
      (az:Real-Vector (az:zero-real-vector (az:array-data a)))))
  a)

;;;=======================================================
;;; Matrix functions
;;;=======================================================

(defun diagonal-zeros? (a)
  (az:type-check az:Float-Matrix a)
  (dotimes (i (min (array-dimension a 0) (array-dimension a 1)))
    (when (zerop (aref a i i)) (return-from diagonal-zeros? t)))
  nil)
  
(declaim (Inline determinant-2x2 determinant-3x3))

(defun determinant-2x2 (a00 a01
			a10 a11)

  (az:type-check Double-Float a00 a01 a10 a11)

  (- (* a00 a11) (* a01 a10)))

(defun determinant-3x3 (a00 a01 a02
			a10 a11 a12
			a20 a21 a22)

  (az:type-check Double-Float a00 a01 a02  a10 a11 a12 a20 a21 a22)

  (+ (- (* a00 (- (* a11 a22) (* a21 a12)))
	(* a01 (- (* a10 a22) (* a20 a12))))
     (* a02 (- (* a10 a21) (* a20 a11)))))

;;;=======================================================

(defun svd-nash (x
		 &optional
		 (u (az:make-float-matrix (array-dimension x 0)
					  (array-dimension x 1)))
		 (s (az:make-float-vector (array-dimension x 1)))
		 (v (az:make-float-matrix (array-dimension x 1)
					  (array-dimension x 1))))

  "Nash Svd code from Steve Peters & Catherine Hurley.
Singular-value decomposition by means of orthogonalization by plane
rotations.  Taken from Nash and Shlien: -Partial Svd Algorithms.-

<x> (array float (m n)) is the array to be decomposed.
The optional arguments may pre-allocate storage.
<u> (array float (m n)) is overwritten with the left singular vectors.
<s> (array float (n)) is overwritten with the singular values.
<v> (array float (n n)) is overwritten with the right singular vectors.

<u> may be <eq> to <x>, in which case <x> will be overwritten."

  (declare (type az:Float-Matrix x u v)
	   (type az:Float-Vector s)
	   (:returns (values (type az:Float-Matrix u)
			     (type az:Float-Vector s)
			     (type az:Float-Matrix v))))
  
  (assert (= 2 (array-rank x) (array-rank u) (array-rank v)))
  (assert (= 1 (array-rank s)))
  (assert (equal (array-dimensions x) (array-dimensions u)))
  (assert (= (array-dimension x 1)
	     (array-dimension v 0)
	     (array-dimension v 1)
	     (length s)))
  (assert (not (eq x v)))
  
  (let* ((m (array-dimension x 0))
	 (n (array-dimension x 1))
	 (slimit (max (truncate n 4) 6))
	 (nt n)
	 (rcount n))
    ;; initialize v to identity
    (dotimes (i (array-dimension v 0))
      (dotimes (j (array-dimension v 1))
	(setf (aref v i j) (if (= i j) 1.0d0 0.0d0))))
    (az:copy x :result u)
    
    (when (= n 1)
      (setf (aref s 0) (v-l2-norm u :end m :vtype :col))
      (unless (zerop (aref s 0))
	(v*x! u (/ 1.0d0 (aref s 0)) :end m :vtype :col))
      (return-from svd-nash (values u s v)))
    
    ;; The main loop: Repeatedly sweep over all pairs of columns in u,
    ;; rotating as needed, until no rotations in a complete sweep are
    ;; effective. Check the opportunity for rank reduction at the
    ;; conclusion of each sweep.
    
    (loop with eps^2 = (az:flsq az:-small-double-float-)
	for scount upfrom 1
	while (> rcount 0)
	when (> scount slimit) do (error "Too many sweeps.")
	do
	  (setf rcount (truncate (* nt (- nt 1)) 2))
	  (loop for j from 0 to (- nt 2) do
		(loop with cc and ss and val
		    for k from (+ j 1) below nt
		    for p = (v.v u u
				 :end0 m :vtype0 :col :on0 j
				 :end1 m :vtype1 :col :on1 k)
		    for q = (v-l2-norm2 u :end m :vtype :col :on j)
		    for r = (v-l2-norm2 u :end m :vtype :col :on k)
		    do (assert (not (zerop r)))
		       (setf (aref s j) q)
		       (setf (aref s k) r)
		       (cond
			((< q r)
			 (setf q (- (/ q r) 1.0d0))
			 (setf p (/ p r))
			 (setf val (sqrt (+ (* 4.0 p p) (* q q))))
			 (assert (not (zerop val)))
			 (setf ss (sqrt (* 0.5 (- 1.0d0 (/ q val)))))
			 (if (< p 0.0d0) (setf ss (- ss)))
			 (assert (not (zerop ss)))
			 (setf cc (/ p (* val ss)))
			 (v-rot-v! u u cc ss
				   :end0 m :vtype0 :col :on0 j
				   :end1 m :vtype1 :col :on1 k)
			 (v-rot-v! v v cc ss
				   :end0 n :vtype0 :col :on0 j
				   :end1 n :vtype1 :col :on1 k))
			((or (<= (* q r) eps^2)
			     (<= (* (/ p q) (/ p r)) az:-small-double-float-))
			 (decf rcount))
			(t
			 (assert (not (zerop q)))
			 (setf r (- 1.0d0 (/ r q)))
			 (setf p (/ p q))
			 (setf val (sqrt (+ (* 4.0 p p) (* r r))))
			 (assert (not (zerop val)))
			 (setf cc (sqrt (* 0.5 (+ 1.0d0 (/ r val)))))
			 (assert (not (zerop cc)))
			 (setf ss (/ p (* val cc)))
			 (v-rot-v! u u cc ss
				   :end0 m :vtype0 :col :on0 j
				   :end1 m :vtype1 :col :on1 k)
			 (v-rot-v! v v cc ss
				   :end0 n :vtype0 :col :on0 j
				   :end1 n :vtype1 :col :on1 k)))))	    
	  (loop while (and (>= nt 3)
			   (<= (/ (aref s (- nt 1))
				  (+ (aref s 0) az:-small-double-float-))
			       az:-small-double-float-))
	      do (decf nt)))       
    ;; finish the decomposition by returning all n singular values, 
    ;; and by normalizing those columns of u judged to be non-zero.
    (dotimes (j n)
      (let ((q (sqrt (aref s j))))
	(setf (aref s j) q)
	(if (<= j nt)
	  (assert (not (zerop q)))
	  (v*x! u (/ q) :end m :vtype :col :on j)))))
  (values u s v))

;;;-------------------------------------------------------
;;; Back substitution:
;;; Treats $A$ as upper triangular.
;;; Computes $A^{-1}v_d$ and puts the result in $v_r$.

(defun back-substitution (a vd
			  &optional
			  (vr (az:make-float-vector (array-dimension a 1))))

  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 0) (length vd)))
  (assert (= (array-dimension a 1) (length vr)))
  (assert (not (eq vd vr)))

  (let ((m (array-dimension a 0))
	(n (array-dimension a 1)))
    (do ((i+1 (min m n) (- i+1 1)))
	((<= i+1 0))
      (let* ((i (- i+1 1))
	     (sum (aref vd i)))
	(do ((j (- m 1) (- j 1)))
	    ((< j i+1))
	  (decf sum (* (aref a i j) (aref vr j))))
	(assert (not (zerop (aref a i i))))
	(setf (aref vr i) (/ sum (aref a i i))))))
  vr) 

;;;-------------------------------------------------------
;;; Transposed Back substitution:
;;; Treats $A$ as lower triangular.
;;; Computes $(A^{\dagger})^{-1} v_d$ and puts the result in $v_r$.

(defun transposed-back-substitution (a vd
				     &optional
				     (vr (az:make-float-vector
					  (array-dimension a 0))))
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 1) (length vd)))
  (assert (= (array-dimension a 0) (length vr)))
  (assert (not (eq vd vr)))

  (let ((m (array-dimension a 1))
	(n (array-dimension a 0)))
    (do ((i+1 (min m n) (- i+1 1)))
	((<= i+1 0))
      (let* ((i (- i+1 1))
	     (sum (aref vd i)))
	(do ((j (- m 1) (- j 1)))
	    ((< j i+1))
	  (decf sum (* (aref a j i) (aref vr j))))
	(assert (not (zerop (aref a i i))))
	(setf (aref vr i) (/ sum (aref a i i))))))
  vr) 

;;;-------------------------------------------------------
;;; Forward elimination:
;;; Treats $A$ as though it was lower triangular.
;;; Computes $A^{-1}v_d$ and puts the result in $v_r$.

(defun forward-elimination (a vd
			    &optional
			    (vr (az:make-float-vector (array-dimension a 1))))
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 0) (length vd)))
  (assert (= (array-dimension a 1) (length vr)))
  (assert (not (eq vd vr)))

  (dotimes (i (min (array-dimension a 0) (array-dimension a 1)))
    (assert (not (zerop (aref a i i))))
    (setf (aref vr i)
      (/ (- (aref vd i) (v.v a vr
			     :end0 i :vtype0 :row :on0 i 
			     :end1 i))
	 (aref a i i))))
  vr) 

;;;-------------------------------------------------------
;;; Transposed Forward elimination:
;;; Treats $A$ as upper triangular.
;;; Computes $(A^{\dagger})^{-1} v_d$ and puts the result in $v_r$.

(defun transposed-forward-elimination (a vd
				       &optional
				       (vr (az:make-float-vector
					    (array-dimension a 0))))
  
  (declare (type az:Float-Matrix a)
	   (type az:Float-Vector vd vr))
  (assert (= (array-dimension a 1) (length vd)))
  (assert (= (array-dimension a 0) (length vr)))
  (assert (not (eq vd vr)))

  (dotimes (i (min (array-dimension a 0) (array-dimension a 1)))
    (assert (not (zerop (aref a i i))))
    (setf (aref vr i)
      (/ (- (aref vd i) (v.v a vr
			     :end0 i :vtype0 :col :on0 i
			     :end1 i))
	 (aref a i i))))
  vr) 

;;;=======================================================
;;; Dennis and Schnabel A3.3.1
;;;
;;; Compute a lower bound on the $L_1$ condition of the tranformation
;;; represented by the upper triangle of $A$.

(defun estimate-condition-of-upper-triangle (a)
  (az:type-check az:Float-Matrix a)
  (dotimes (i (min (array-dimension a 0) (array-dimension a 1))))
  (assert (not (diagonal-zeros? a)))
  (let ((n (min (array-dimension a 0) (array-dimension a 1)))
	(condition 0.0d0)
	(x0 (/ 1.0d0 (aref a 0 0))))
    ;; initialize <condition> to max l1 norm of columns
    (dotimes (j n) (az:maxf condition
			 (v-l1-norm a :end (+ j 1) :vtype :col :on j)))
    ;; implicitly compute $x = (A^{\dagger})^{-1} e$,
    ;; where $e_i = \pm 1$ is chosen as we go.
    (az:with-borrowed-float-vectors ((x p pm) n)
      (setf (aref x 0) x0)
      (v<-v*x! p a x0
		:start0 1 :end0 n
		:start1 1 :end1 n :vtype1 :row)
      (do ((j 1 (+ j 1)))
	  ((>= j n))
	(let* ((ajj (aref a j j))
	       (pj (aref p j))
	       (xp (/ (- 1.0d0 pj) ajj))
	       (xm (/ (- -1.0d0 pj) ajj))
	       (temp (abs xp))
	       (tempm (abs xm)))
	  (do ((i (+ j 1) (+ i 1)))
	      ((>= i n))
	    (setf (aref pm i) (+ (aref p i) (* xm (aref a j i))))
	    (incf tempm (abs (/ (aref pm i) (aref a i i))))
	    (incf (aref p i) (* xp (aref a j i)))
	    (incf temp (abs (/ (aref p i) (aref a i i)))))
	  (cond ((>= temp tempm) ;; $e_j = 1$
		 (setf (aref x j) xp))
		(t ;; $e_j = -1$
		 (setf (aref x j) xm)
		 (v<-v! p pm
			 :start0 (+ j 1) :end0 n
			 :start1 (+ j 1) :end1 n)))))
      (az:divf condition (v-l1-norm x))
      (az:mulf condition (v-l1-norm (back-substitution a x p))))
    condition))

;;;-------------------------------------------------------
;;; modified from Dennis and Schnabel A3.3.1
;;;
;;; Compute a lower bound on the $L_1$ condition of the tranformation
;;; represented by the lower triangle of $M$. This was created
;;; by transposing all references to $M$ in the preceeding function.

(defun estimate-condition-of-lower-triangle (a)
  (az:type-check az:Float-Matrix a)
  (assert (not (diagonal-zeros? a)))
  (let ((n (min (array-dimension a 0) (array-dimension a 1)))
	(condition 0.0d0)
	(x0 (/ 1.0d0 (aref a 0 0))))
    ;; initialize <condition> to max l1 norm of rows
    (dotimes (j n) (az:maxf condition
			 (v-l1-norm a :end (+ j 1) :vtype :row :on j)))
    ;; implicitly compute $x = (A^{\dagger})^{-1} e$,
    ;; where $e_i = \pm 1$ is chosen as we go.
    (az:with-borrowed-float-vectors ((x p pm) n)
      (setf (aref x 0) x0)
      (v<-v*x! p a x0
		:start0 1 :end0 n
		:start1 1 :end1 n :vtype1 :col :on1 0)
      (do ((j 1 (+ j 1)))
	  ((>= j n))
	(let* ((ajj (aref a j j))
	       (pj (aref p j))
	       (xp (/ (- 1.0d0 pj) ajj))
	       (xm (/ (- -1.0d0 pj) ajj))
	       (temp (abs xp))
	       (tempm (abs xm)))
	  (do ((i (+ j 1) (+ i 1)))
	      ((>= i n))
	    (setf (aref pm i) (+ (aref p i) (* xm (aref a i j))))
	    (incf tempm (abs (/ (aref pm i) (aref a i i))))
	    (incf (aref p i) (* xp (aref a i j)))
	    (incf temp (abs (/ (aref p i) (aref a i i)))))
	  (cond ((>= temp tempm) ;; $e_j = 1$
		 (setf (aref x j) xp))
		(t ;; $e_j = -1$
		 (setf (aref x j) xm)
		 (v<-v! p pm
			 :start0 (+ j 1) :end0 n
			 :start1 (+ j 1) :end1 n)))))
      (az:divf condition (v-l1-norm x))
      (az:mulf condition (v-l1-norm (transposed-back-substitution a x p))))
    condition))

;;;=======================================================
;;; Golub & VanLoan Alg 5.2-1
;;;
;;; Given $A$ square, symmetric, and positive definite, computes lower
;;; triangular $L$ such that $A = LL^{\dagger}$, overwriting the
;;; optional argument, if provided.

(defun cholesky (a
		 &optional
		 (l (az:copy a) l-supplied?))

  (when (and l-supplied? (not (eq a l)))
    (assert (az:equal-array-dimensions? a l))
    (az:copy a :result l))

  (cholesky! l)) 

;;; Given $A$ square, symmetric, and positive definite, computes lower
;;; triangular $L$ such that $A = LL^{\dagger}$, overwriting $A$
;;; with $L$.

(defun cholesky! (a)
  (az:type-check az:Float-Matrix a)
  (assert (= (array-dimension a 0) (array-dimension a 1)))
  (let ((n (array-dimension a 0)))
    (declare (type az:Array-Index n))
    (dotimes (k n)
      (declare (type az:Array-Index k))
      (setf (aref a k k) (sqrt (- (aref a k k)
				  (v-l2-norm2 a :end k :vtype :row :on k))))
      (do ((i (+ k 1) (+ i 1)))
	  ((>= i n))
	(declare (type az:Array-Index i))
	(setf (aref a i k)
	      (/ (- (aref a i k) (v.v a a
				       :end0 k :vtype0 :row :on0 i
				       :end1 k :vtype1 :row :on1 k))
		 (aref a k k)))))
    (dotimes (i (- n 1))
      (declare (type az:Array-Index i))
      (v<-x! a 0.0d0 :start (+ i 1) :end n :vtype :row :on i)))
  a)

;;;-------------------------------------------------------
;;; Based on Golub & VanLoan Alg 5.2-1
;;; 
;;; Given 2-dimensional $A$ is square, symmetric, and positive definite.
;;; Computes unit lower triangular 2-dimensional $L$ and 1-dimensional
;;; $D$ such that $A = LDL^{\dagger}$, overwriting the optional
;;; arguments, if provided. (This saves $n$ square roots over {\tt
;;; cholesky}. See Gill-Murray-Wright Sec 2.2.5.2; Lawson-Hanson
;;; Exercise 19.40)

(defun cholesky-ldlt (a
		      &optional
		      (l (az:copy a) l-supplied?)
		      (d (az:make-float-vector (array-dimension a 0))))

  (when (and l-supplied? (not (eq a l)))
    (assert (az:equal-array-dimensions? a l))
    (az:copy a :result l))

  (cholesky-ldlt! l d))

;;; Given 2-dimensional $A$ is square, symmetric, and positive definite.
;;; Computes unit lower triangular 2-dimensional $L$ and 1-dimensional
;;; $D$ such that $A = LDL^{\dagger}$, overwriting $A$ with $L$.  A
;;; 1-dimensional array to hold $D$ is required.  (This saves $n$ square
;;; roots over {\tt cholesky}. See Gill-Murray-Wright Sec 2.2.5.2;
;;; Lawson-Hanson Exercise 19.40)

(defun cholesky-ldlt! (a d)
  (az:type-check az:Float-Matrix a)
  (assert (= (array-dimension a 0) (array-dimension a 1) (length d)))
  (let ((n (length d))
	(dk 0.0d0)
	(aik 0.0d0))
    (declare (type az:Array-Index n)
	     (type Double-Float dk aik))
    (dotimes (k n)
      (declare (type az:Array-Index k))
      (setf dk (aref a k k))
      (dotimes (i k)
	(declare (type az:Array-Index i))
	(decf dk (* (aref d i) (az:flsq (aref a k i)))))
      (setf (aref d k) dk)
      (setf (aref a k k) 1.0d0)
      (do ((i (+ k 1) (+ i 1)))
	  ((>= i n))
	(declare (type az:Array-Index i))
	(setf aik (aref a i k))
	(dotimes (j k)
	  (declare (type az:Array-Index j))
	  (decf aik (* (aref d j) (aref a i j) (aref a k j))))
	(assert (not (zerop dk)))
	(setf (aref a i k) (/ aik dk))))
    ;; Enforce lower triangularity---not strictly necessary, but safe.
    (dotimes (i (- n 1))
      (declare (type az:Array-Index i))
      (v<-x! a 0.0d0 :start (+ i 1) :end n :vtype :row :on i)))
  (values a d))

;;;-------------------------------------------------------
;;; 2-dimensional, square, lower triangular $L_0$ and
;;; 1-dimensional $D_0$ are taken to be the factors of an
;;; $LDL_{\dagger}$ Cholesky decomposition. $v$ is a vector
;;; representing the elementary tensor product $v \otimes v$.
;;; This function the $LDL_{\dagger}$ decomposition of
;;; $(L_0 D_0 L_0^{\dagger}) + (v0 \otimes v0)$,
;;; using the optional arguments $L_1$ and $D_1$ to hold the result,
;;; and using $v_1$ as temporary storage.

(defun update-cholesky-ldlt (l0 d0 v0
			     &optional
			     (l1 (az:copy l0) l1-supplied?)
			     (d1 (az:copy d0) d1-supplied?)
			     (v1 (az:copy v0) v1-supplied?))

  (az:type-check az:Float-Matrix l0 l1)
  (az:type-check az:Float-Vector d0 v0 d1 v1)
  (assert (= (array-dimension l0 0)
	     (array-dimension l0 1)
	     (length d0)
	     (length v0)))

  (when (and l1-supplied? (not (eq l0 l1)))
    (assert (az:equal-array-dimensions? l0 l1))
    (az:copy l0 :result l1))

  (when (and d1-supplied? (not (eq d0 d1)))
    (assert (= (length d0) (length d1)))
    (v<-v! d1 d0))

  (when (and v1-supplied? (not (eq v0 v1)))
    (assert (= (length v0) (length v1)))
    (v<-v! v1 v0)) 

  (update-cholesky-ldlt! l1 d1 v1))

;;; 2-dimensional, square, lower triangular $L$ and 1-dimensional $D$
;;; are taken to be the factors of an $LDL_{\dagger}$ Cholesky
;;; decomposition. $v$ is a vector representing the elementary tensor
;;; product $v \otimes v$.  This function returns $L$ and $D$,
;;; overwritten with the $LDL_{\dagger}$ decomposition of
;;; $(LDL^{\dagger}) + (v \otimes v)$. It destroys the contents of $v$.
;;; See Gill-Murray-Wright p 42.

(defun update-cholesky-ldlt! (l d v)

  (az:type-check az:Float-Matrix l)
  (az:type-check az:Float-Vector d v)
  (assert (notany #'zerop d))
  (assert (= (array-dimension l 0)
	     (array-dimension l 1)
	     (length d)
	     (length v)))

  (let ((n (length d))
	(tj-1 1.0d0))	
    (declare (type az:Array-Index n)
	     (type Double-Float tj-1))
    (dotimes (j n)
      (declare (type az:Array-Index j))
      (let* ((dj (aref d j))
	     (vj (aref v j))
	     (tj (+ tj-1 (/ (az:flsq vj) dj)))
	     (b (progn (assert (not (zerop tj)))
		       (/ vj (* dj tj)))))
	(assert (not (zerop tj-1)))
	(setf (aref d j) (/ (* dj tj) tj-1))
	(do ((k (+ j 1) (+ k 1)))
	    ((>= k n))
	  (declare (type az:Array-Index k))
	  (let ((v+1 (- (aref v k) (* vj (aref l k j)))))
	    (declare (type Double-Float v+1))
	    (setf (aref v k) v+1)
	    (incf (aref l k j) (* b v+1))))
	(setf tj-1 tj))))
  (values l d)) 

;;;-------------------------------------------------------
;;; 2-dimensional, square, lower triangular $L_0$ and
;;; 1-dimensional $D_0$ are taken to be the factors of an
;;; $LDL_{\dagger}$ Cholesky decomposition. $v$ is a vector
;;; representing the elementary tensor product $v \otimes v$.
;;; This function computes the $LDL_{\dagger}$ decomposition of
;;; $(L_0 D_0 L_0^{\dagger}) - (v0 \otimes v0)$,
;;; using the optional arguments $L_1$ and $D_1$ to hold the result,
;;; and using $v_1$ as temporary storage.
;;; 
;;; Note: this is trickier than updating, because there's no guarantee,
;;; in general, that the downdated matrix is still positive definite and
;;; that the $LDL_{\dagger}$ decomposition still exists.

(defun downdate-cholesky-ldlt (l0 d0 v0
			       &optional
			       (l1 (az:copy l0) l1-supplied?)
			       (d1 (az:copy d0) d1-supplied?)
			       (v1 (az:copy v0) v1-supplied?))

  (az:type-check az:Float-Matrix l0 l1)
  (az:type-check az:Float-Vector d0 v0 d1 v1)
  (assert (= (array-dimension l0 0)
	     (array-dimension l0 1)
	     (length d0)
	     (length v0)))
  
  (when (and l1-supplied? (not (eq l0 l1)))
    (assert (az:equal-array-dimensions? l0 l1))
    (az:copy l0 :result l1))

  (when (and d1-supplied? (not (eq d0 d1)))
    (assert (= (length d0) (length d1)))
    (v<-v! d1 d0))

  (when (and v1-supplied? (not (eq v0 v1)))
    (assert (= (length v0) (length v1)))
    (v<-v! v1 v0)) 

  (downdate-cholesky-ldlt! l1 d1 v1))

;;; 2-dimensional, square, lower triangular $L$ and 1-dimensional $D$
;;; are taken to be the factors of an $LDL_{\dagger}$ Cholesky
;;; decomposition. $v$ is a vector representing the elementary tensor
;;; product $v \otimes v$.  This function returns $L$ and $D$,
;;; overwritten with the $LDL_{\dagger}$ decomposition of
;;; $(LDL^{\dagger}) - (v \otimes v)$. It destroys the contents of $v$.
;;; See Gill-Murray-Wright p 43.
;;; 
;;; Note: this is trickier than updating, because there's no guarantee,
;;; in general, that the downdated matrix is still positive definite and
;;; that the $LDL_{\dagger}$ decomposition still exists.


(defun downdate-cholesky-ldlt! (l d v)
  (az:type-check az:Float-Matrix l)
  (az:type-check az:Float-Vector d v)
  (assert (= (array-dimension l 0)
	     (array-dimension l 1)
	     (length d)
	     (length v)))
  (warn "<downdate-cholesky-ldlt!> not implemented yet")
  (values l d)) 
