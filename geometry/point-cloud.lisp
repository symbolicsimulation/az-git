;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================

(defclass Point-Cloud (Geometric-Object)
	  ((coords
	   :type Simple-Vector
	   :accessor coords))
  
  (:documentation
   "A compact, coordinate-wise representation of a set of points in
some space. <coords> is a simple-vector of either float or integer
arrays (depending on the <home> space of the Point-Cloud). The ith
coordinate of the jth point is (aref (aref coords i) j)."))


