;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================

(defclass Matrix-Super (Linear-Map)
	  ((bands-start
	    :type az:Array-Index
	    :initarg :bands-start
	    :accessor bands-start)
	   (bands-end
	    :type az:Array-Index
	    :initarg :bands-end
	    :accessor bands-end))
  (:documentation
  "Ancestor class for Matrix and Restricted-Matrix.
The purpose is to allow methods to be shared;
making either Matrix or Restricted-Matrix
a subclass of the other results in things being inherited that
shouldn't be.

To do: add a Constant-Matrix class that doesn't allow any of its
slot values to be altered (as far as possible).  Eg. (coords
constant-matrix) would return a copy of the coords; (setf
(coords constant-matrix) x) would generate an error.  We can then
use information about properties (eg. positive-definite?)  of
Constant-Matrix's will reasonable safety. Have modifiable matrices
return nil or the equivalent (eg. maximum possible bandwidths) for
all the property predicates."))

;;;-------------------------------------------------------

(defmethod col ((t0 Matrix-Super) (i Integer)
		&key (result (make-element (codomain t0))))

  (let ((cod (codomain t0)))
    (az:type-check Vector-Space cod)
    (assert (and (element? result cod)
		 (eq (home result) cod)))
    (v<-v! (coords result) (coords t0)
	   :end0 (dimension cod) 
	   :start1 (coord-start cod) :end1 (coord-end cod)
	   :vtype1 :col :on1 i))
  result)

(defmethod row ((t0 Matrix-Super) (i Integer)
                &key (result (make-element (domain t0))))
  (let ((dom (domain t0)))
    (az:type-check Vector-Space dom)
    (assert (and (element? result dom)
		 (eq (home result) dom)))
    (v<-v! (coords result) (coords t0)
	   :end0 (dimension dom)
	   :start1 (coord-start dom) :end1 (coord-end dom)
	   :vtype1 :row :on1 i))
  result)

;;;-------------------------------------------------------

(defmethod (setf col) ((v Point) (t0 Matrix-Super) (i Integer))
  (let* ((cod (codomain t0))
	 (s0 (coord-start v))
	 (e0 (coord-end v))
	 (s1 (coord-start cod))
	 (e1 (coord-end cod))
	 (n (- e0 s0))
	 (1d (coords v))
	 (2d (coords t0)))
    (az:type-check Vector-Space cod)
    (assert (element? v cod))
    (when (> s0 s1) (v<-x! 2d 0.0d0 :start s1 :end s0 :vtype :col :on i))
    (v<-v! 2d 1d :start0 s0 :end0 e0 :vtype0 :col :on0 i :end1 n)
    (when (> e1 e0) (v<-x! 2d 0.0d0 :start e1 :end e0 :vtype :col :on i))) 
  v)

(defmethod (setf row) ((v Point) (t0 Matrix-Super) (i Integer))
  (let* ((dom (domain t0))
	 (s0 (coord-start v))
	 (e0 (coord-end v))
	 (s1 (coord-start dom))
	 (e1 (coord-end dom))
	 (n (- e0 s0))
	 (1d (coords v))
	 (2d (coords t0)))
    (az:type-check Vector-Space dom)
    (assert (element? v dom))
    (when (> s0 s1) (v<-x! 2d 0.0d0 :start s1 :end s0 :vtype :row :on i))
    (v<-v! 2d 1d :start0 s0 :end0 e0 :vtype0 :row :on0 i :end1 n)
    (when (> e1 e0) (v<-x! 2d 0.0d0 :start e1 :end e0 :vtype :row :on i))) 
  v)

;;;-------------------------------------------------------
;;; Bandedness
;;;-------------------------------------------------------
;;;
;;; The band coordinate is (- j i) for any x_ij that's in the band.
;;; This means that sub-diagonal bands have negative coordinates.
;;; <bands-start> is the band coordinate of the start band that is not all
;;; zeros, eg., for a upper triangular matrix <bands-start> = 0, for an
;;; upper hessenberg matrix it's -1.  <bands-end> is the band
;;; coordinate of the start band that is all zeros after the non-zero
;;; bands, eg., for a lower triangular matrix <bands-end> = 1, for a
;;; lower hessenberg it's 2.  The reason for this  asymmetry is to be
;;; consistent with the specifications for integer intervals used in the
;;; CL sequence functions and the Basic-Math v functions.

(defvar *coerce-bands?* nil)

;;;-------------------------------------------------------    

(defmethod min-band-coord ((t0 Matrix-Super))
  (- 1 (dimension (codomain t0))))

(defmethod max-band-coord ((t0 Matrix-Super))
  (dimension (domain t0)))

;;;-------------------------------------------------------


(defmethod min-bands-start? ((t0 Matrix-Super) &key (start (bands-start t0)))
  (<= start (min-band-coord t0)))

(defmethod max-bands-end? ((t0 Matrix-Super) &key (end (bands-end t0)))
  (>= end (max-band-coord t0)))

(defmethod trivial-bands? ((t0 Matrix-Super)
			   &key
			   (start (bands-start t0))
			   (end (bands-end t0)))
  (and (min-bands-start? t0 :start start)
       (max-bands-end? t0 :end end)))

;;;-------------------------------------------------------
;; These could be speeded up by operating directly on the <coords>.
;; However, using the <row> "accessor" is easier, safer, and more modular.

(defmethod verify-bands? ((t0 Matrix-Super)
			  &key
			  (start (bands-start t0))
			  (end (bands-end t0))
			  (eps 10.0d0))
  (unless (trivial-bands? t0)
    (let ((x 0.0d0)
	  (n (dimension (domain t0)))
	  (norm (l1-norm t0))
	  (s (coord-start (domain t0)))
	  i-s)
      (with-borrowed-element (v (domain t0))
	(let ((1d (coords v)))
	  (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	    (setf v (row t0 i :result v))
	    (setf i-s (- i s))
	    (setf
	      x
	      (+ (v-abs-max 1d :end (az:bound 0 (+ i-s start) n)) 
		 (v-abs-max 1d :start (az:bound 0 (+ i-s end) n) :end n)))
	    (unless (az:small? x (* (+ 1.0d0 norm) eps))
	      (return-from verify-bands? nil)))))))
  t)

(defmethod coerce-bands! ((t0 Matrix-Super)
			  &key
			  (start (bands-start t0))
			  (end (bands-end t0)))
  (let ((n (dimension (domain t0))))
    (with-borrowed-element (v (domain t0))
      (let ((1d (coords v))
	    (s (coord-start (domain t0)))
	    i-s)      
	(az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	  (setf v (row t0 i :result v))
	  (setf i-s (- i s))
	  (v<-x! 1d 0.0d0 :end (az:bound 0 (+ i-s start) n))
	  (v<-x! 1d 0.0d0 :start (az:bound 0 (+ i-s end) n) :end n)
	  (setf (row t0 i) v))))))

;;;-------------------------------------------------------

(defmethod set-bandedness! ((t0 Matrix-Super) (prop Symbol)
			    &key (coerce? nil))
  (case prop
    (upper-triangular? (setf (bands-start t0) 0))
    (lower-triangular? (setf (bands-end t0) 1))
    (diagonal? (setf (bands-start t0) 0) (setf (bands-end t0) 1))
    (upper-hessenberg? (setf (bands-start t0) -1))
    (lower-hessenberg? (setf (bands-end t0) 2))
    (tridiagonal? (setf (bands-start t0) -1) (setf (bands-end t0) 2))
    (upper-bidiagonal? (setf (bands-start t0) 0) (setf (bands-end t0) 2))
    (lower-bidiagonal? (setf (bands-end t0) 1) (setf (bands-start t0) -1))
    (otherwise (warn "unknown bandedness nickname ~a" prop)))
  (cond (coerce? (coerce-bands! t0))
	(*verify?* (assert (verify-bands? t0))))
  t0)

(defmethod set-default-bandedness! ((t0 Matrix-Super))
  (setf (bands-start t0) (min-band-coord t0))
  (setf (bands-end t0) (max-band-coord t0)))

;;;=======================================================
;;;		       Properties
;;;=======================================================
;;; Each property symbol should have a function definition which is a
;;; generic function whose methods are predicates that test the
;;; property. 

(defmethod add-property! ((t0 Matrix-Super) (prop Symbol))
  (if (member prop *bandedness-abbreviations*)
    (set-bandedness! t0 prop)
    ;; else
    (case prop
      (positive-definite? 
       (setf (properties t0) (delete 'negative-definite? (properties t0)))
       (pushnew 'symmetric? (properties t0))
       (pushnew 'positive-definite? (properties t0)))
      (negative-definite?
       (setf (properties t0) (delete 'positive-definite? (properties t0)))
       (pushnew 'symmetric? (properties t0))
       (pushnew 'negative-definite? (properties t0)))
      ((symmetric? unit-diagonal? orthogonal?
	orthogonal-rows? orthogonal-columns?)
       (pushnew prop (properties t0)))
      (otherwise
       (warn "trying to add unknown property ~a" prop))))
  t0)

(defmethod delete-property! ((t0 Matrix-Super) (prop Symbol))
  (setf (properties t0)
    (case prop
      (positive-definite? 
       (delete 'positive-definite? (properties t0)))
      (negative-definite?
       (delete 'negative-definite? (properties t0)))
      (symmetric?
       (delete 'positive-definite? 
	       (delete 'negative-definite?
		       (delete 'symmetric-definite? (properties t0)))))
      ((orthogonal? orthogonal-rows? orthogonal-columns? 
	unit-diagonal?)
       (delete prop (properties t0)))
      (otherwise
       (warn "deleting unknown property ~a" prop)
       (delete prop (properties t0))))))


;;;-------------------------------------------------------

(defmethod positive-definite? ((t0 Matrix-Super))
  (member 'positive-definite? (properties t0)))

(defmethod negative-definite? ((t0 Matrix-Super))
  (member 'negative-definite? (properties t0)))

(defmethod orthogonal? ((t0 Matrix-Super))
  (member 'orthogonal? (properties t0)))

(defmethod orthogonal-rows? ((t0 Matrix-Super))
  (or (orthogonal? t0)
      (member 'orthogonal-rows? (properties t0))))

(defmethod orthogonal-columns? ((t0 Matrix-Super))
  (or (orthogonal? t0)
      (member 'orthogonal-columns? (properties t0))))

(defmethod symmetric? ((t0 Matrix-Super))
  (if *verify?*
    (and (automorphic? t0)
	 (let ((2d (coords t0))
	       (ioff (coord-start (codomain t0)))
	       (joff (coord-start (domain t0))))
	   (loop for i from 0 below (dimension (codomain t0))
	       always
		 (loop for j from 0 below i
		     for 2d.ij = (aref 2d (+ i ioff) (+ j joff))
		     for 2d.ji = (aref 2d (+ j ioff) (+ i joff))
		     always
		       (or (az:small? (+ (abs 2d.ij) (abs 2d.ji)) 1.0d0)
			   (az:small? (- 2d.ij 2d.ji)
				      (+ 1.0d0 (abs 2d.ij) (abs 2d.ji))))))))
    ;; else check cache
    (member 'symmetric? (properties t0))))

;;;-------------------------------------------------------

(defmethod unit-diagonal? ((t0 Matrix-Super))
  (if *verify?*  
      (let ((2d (coords t0))
	    (i0 (coord-start (codomain t0)))
	    (j0 (coord-start (domain t0))))
	(block :test
	  (az:for (i (coord-start (domain t0)) (coord-end (domain t0)))
	    (unless (az:small? (- 1.0d0 (aref 2d (+ i i0) (+ i j0))) 1.0d0)
	      (return-from :test nil)))
	  (return-from :test t)))
      ;; else check the cache
      (member 'unit-diagonal? (properties t0))))
  
;;;=======================================================

(defmethod tail! ((t0 Matrix-Super))
  (restrict! t0 (tail-space (codomain t0)) (tail-space (domain t0))))

(defmethod codomain-tail! ((t0 Matrix-Super))
  (restrict! t0 (tail-space (codomain t0)) (domain t0)))

(defmethod domain-tail! ((t0 Matrix-Super))
  (restrict! t0 (codomain t0) (tail-space (domain t0))))

(defmethod restrict-to-square ((t0 Matrix-Super))
  (cond
    ((strictly-embedding? t0)
     (let* ((n (dimension (domain t0)))
	    (cod (block-subspace (codomain t0)
				 (coord-start (codomain t0))
				 (+ (coord-start (codomain t0)) n))))
       (make-instance 'Product-Map
		      :factors
		      (list (make-instance
			      'Block-Embedding
			      :domain cod :codomain (codomain t0))
			    (restrict t0 cod (domain t0))))))
    ((strictly-projecting? t0)
     (let* ((m (dimension (codomain t0)))
	    (dom (block-subspace (domain t0)
				 (coord-start (domain t0))
				 (+ (coord-start (domain t0)) m))))
       (make-instance 'Product-Map
		      :factors
		      (list (restrict t0 (codomain t0) dom)
			    (make-instance
			      'Block-Projection
			      :domain (domain t0) :codomain dom)))))))
    
;;;=======================================================

(defmethod index-of-abs-max-in-col ((t0 Matrix-Super) j)
  (v-abs-max-index
    (coords t0)
    :start (coord-start (codomain t0))
    :end (coord-end (codomain t0))
    :vtype :col :on j))

;;;=======================================================

(defmethod zero! ((t0 Matrix-Super))  
  (let* ((2d (coords t0))
	 (start (coord-start (domain t0)))
	 (end (coord-end (domain t0))))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
      (v<-x! 2d 0.0d0 :start start :end end :vtype :row :on i)))
  t0)

;;;-------------------------------------------------------

(defmethod transpose ((t0 Matrix-Super)
		      &key (result (make-instance (class-of t0)
				     :domain (dual-space (codomain t0))
				     :codomain (dual-space (domain t0)))))
  (az:type-check Matrix-Super result)
  (assert (eql (domain t0) (codomain result)))
  (assert (eql (domain result) (codomain t0)))

  (let ((cod (codomain t0))
	(dom (domain t0)))
    (cond ((symmetric? t0)
	   (return-from transpose (az:copy t0 :result result)))
	  ((eq t0 result)
	   (return-from transpose (transpose! t0)))
	  ((projecting? t0)
	   (with-borrowed-element (v dom)
	     (az:for (i (coord-start cod) (coord-end cod))
		     (setf (col result i) (row t0 i :result v)))))
	  ((embedding? t0)
	   (with-borrowed-element (v cod)
	     (az:for (i (coord-start dom) (coord-end dom))
		     (setf (row result i) (col t0 i :result v)))))
	  (t (error "shouldn't ever get here."))))
  
  (cond ((orthogonal? t0) (add-property! result 'orthogonal?))
	((orthogonal-columns? t0) (add-property! result 'orthogonal-rows?))
	((orthogonal-rows? t0) (add-property! result 'orthogonal-columns?)))
  (setf (bands-end result) (- 1 (bands-start t0)))
  (setf (bands-start  result) (- 1 (bands-end t0)))
  result) 

;;;=======================================================
;;;			 Matrix
;;;=======================================================

(defclass Matrix (Matrix-Super)
	  ((coords
	    :type Array
	    :initarg :coords
	    :reader coords)))

;;;-------------------------------------------------------
;;; a pseudo slot, for compatability with Restricted-Matrix's

(defmethod source ((t0 Matrix)) t0)

;;;-------------------------------------------------------
;;;	   make- functions and initialization
;;;-------------------------------------------------------
  
(defmethod shared-initialize :after ((t0 Matrix)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (unless (slot-boundp t0 'coords)	   
    (setf (slot-value t0 'coords)
      (az:make-float-matrix (dimension (codomain t0))
			    (dimension (domain t0)))))
  (unless (slot-boundp t0 'bands-start)
    (setf (bands-start t0) (min-band-coord t0)))
  (unless (slot-boundp t0 'bands-end)
    (setf (bands-end   t0) (max-band-coord t0)))
  (assert (= (array-dimension (coords t0) 1)
	     (dimension (domain t0))))
  (assert (= (array-dimension (coords t0) 0)
	     (dimension (codomain t0))))
  (assert (verify-bands? t0))
  (assert-properties t0))

;;;-------------------------------------------------------

(defmethod diagonal-block? ((t0 Matrix)) t)

;;;-------------------------------------------------------

(defmethod (setf bands-start) ((start Integer) (t0 Matrix))
  (setf (slot-value t0 'bands-start)
	(az:bound (min-band-coord t0) start (max-band-coord t0)))
  start)

(defmethod (setf bands-end) ((end Integer) (t0 Matrix))
  (setf (slot-value t0 'bands-end)
	(az:bound (min-band-coord t0) end (max-band-coord t0)))
  end)

;;;-------------------------------------------------------

(defmethod restrict ((t0 Matrix) (codomain Vector-Space) (domain Vector-Space)
		     &key (result (make-instance 'Restricted-Matrix)))
  (setf (slot-value result 'source) t0)
  (setf (slot-value result 'domain) domain)
  (setf (slot-value result 'codomain) codomain)
  result)

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Matrix))
  (make-instance 'Matrix
    :domain (domain t0)
    :codomain (codomain t0)
    :coords (az:copy (coords t0))
    :bands-start (bands-start t0)
    :bands-end   (bands-end   t0)
    :properties (copy-seq (properties t0))))

(defmethod az:copy-to! ((t0 Matrix) (result Matrix))
  (az:copy (coords t0) :result (coords result))
  (setf (bands-start result) (bands-start t0))
  (setf (bands-end   result) (bands-end   t0))
  (setf (properties result) (copy-seq (properties t0)))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------
;;; coerce an arbitrary Map into a Matrix

(defmethod copy-to-matrix ((t0 Linear-Map)
			   &key (result (make-instance 'Matrix 
					  :domain (domain t0)
					  :codomain (codomain t0))))
  (when (eq t0 result) (warn "Copying ~s to itself." t0))
  (assert (eql (domain t0) (domain result)))
  (assert (eql (codomain t0) (codomain result)))
  (with-borrowed-element (v (domain t0))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	    (setf (row result i) (row t0 i :result v))))
  (setf (bands-start result) (bands-start t0))
  (setf (bands-end result) (bands-end t0))
  (map nil #'(lambda (prop)
	       (when (funcall prop t0) (add-property! result prop)))
       *properties-of-linear-maps-preserved-by-copy-to-matrix*)
  result)

(defmethod copy-to-matrix ((t0 Matrix) &key (result nil))
  (when (eq t0 result) (warn "Copying ~s to itself." t0))
  (if (null result)
    (setf result (az:copy t0))
    (az:copy t0 :result result))
  result)

;;;=======================================================

(defmethod copy-to-array ((t0 Linear-Map)
			  &key (result (az:make-float-matrix
					(dimension (codomain t0))
					(dimension (domain t0)))))

  (assert (eql (dimension (domain t0)) (array-dimension result 1)))
  (assert (eql (dimension (codomain t0)) (array-dimension result 0)))
  (let* ((dom (domain t0))
	 (cod (codomain t0))
	 (s (coord-start dom))
	 (e (coord-end dom))
	 (e-s (- e s)))
    (with-borrowed-element (v dom)
      (let ((1d (coords v)))
	(az:for (i (coord-start cod) (coord-end cod))
		(row t0 i :result v)
		(v<-v! result 1d
		       :start0 s :end0 e :vtype0 :row :on0 i
		       :end1 e-s)))))
  result)

;;;-------------------------------------------------------

(defmethod copy-to-array ((t0 Matrix)
			  &key
			  (result (az:make-float-matrix
				   (dimension (codomain t0))
				   (dimension (domain t0)))))
  (when (not (eq result (coords t0)))
    (assert (eql (dimension (domain t0)) (array-dimension result 1)))
    (assert (eql (dimension (codomain t0)) (array-dimension result 0)))
    (az:copy (coords t0) :result result))
  result) 

;;;-------------------------------------------------------

(defun make-matrix (nrows &optional (ncols nrows))
  (make-instance 'Matrix
    :coords (az:make-float-matrix nrows ncols)
    :domain (Rn ncols)
    :codomain (Rn nrows)))

(defun read-matrix-from-file (pathname nrows &optional (ncols nrows))
  (with-open-file (st pathname :direction :input)
    (let ((2d (az:make-float-matrix nrows ncols)))
      (dotimes (i nrows) (dotimes (j ncols) (setf (aref 2d i j) (read st))))
      (make-instance 'Matrix
	:coords 2d
	:domain (Rn ncols)
	:codomain (Rn nrows)))))

(defun write-matrix-to-file (pathname t0)
  (az:type-check Matrix-Super t0)
  (with-open-file (st pathname :direction :output)
    (let ((2d (coords t0)))
      (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	(az:for (j (coord-start (domain t0)) (coord-end (domain t0)))
	  (print (aref 2d i j) st))))))

;;;-------------------------------------------------------

(defun make-matrix-from-array (a)
  (make-instance 'Matrix
		 :coords a
		 :domain (Rn (array-dimension a 1))
		 :codomain (Rn (array-dimension a 0))))

;;;-------------------------------------------------------

(defun make-identity-matrix (vspace)
  (let* ((dim (dimension vspace))
	 (t0 (make-matrix dim)))
    (identity! t0)
    (add-property! t0 'positive-definite?)
    t0))

;;;-------------------------------------------------------
;;; make a Map from <vs1> to <vs0>,
;;; following convention of codomain first.

(defmethod make-linear-map ((vs0 Vector-Space) (vs1 Vector-Space))
  (make-matrix (dimension vs0) (dimension vs1)))

;;;-------------------------------------------------------

(defmethod transpose! ((t0 Matrix))
  (cond
    ((symmetric? t0) t0)
    ((automorphic? t0)
     (let ((2d (coords t0)))
       (dotimes (i (dimension (codomain t0)))
	 (dotimes (j i)
	   (rotatef (aref 2d i j) (aref 2d j i)))))
     (let ((start (bands-start t0))
	   (end (bands-end t0)))
       (setf (bands-end t0) (- 1 start))
       (setf (bands-start  t0) (- 1 end)))
     t0)
    (t  ;; don't know how to re-use the space
     (transpose t0))))

;;;-------------------------------------------------------

(defmethod identity! ((t0 Matrix))
  
  (let ((2d (coords t0))
	(n (dimension (domain t0))))
    (dotimes (i (dimension (codomain t0)))
      (v<-x! 2d 0.0d0 :end n :vtype :row :on i)
      (when (< i n) (setf (aref 2d i i) 1.0d0))))
  t0)

;;;=======================================================
;;;		       Restricted-Matrix
;;;=======================================================
;;;
;;; Restricted-Matrix's are typically used for efficiency within
;;; decompose algorithms and are usually only composed with Modifier
;;; Maps.
;;; 
;;; A Restricted-Matrix is a Map between block
;;; subspaces of the domain and codomain of its source. The codomain
;;; and domain of a Restricted-Matrix are usually Block-Subspaces,
;;; but in the boundary case, where the submatrix covers its source,
;;; they are the same as the codomain and domain of the source.

(defclass Restricted-Matrix (Matrix-Super Indirect-Map) ())

;;;-------------------------------------------------------

(defmethod coords ((t0 Restricted-Matrix)) (coords (source t0)))

(defmethod (setf codomain) ((new Vector-Space)
			    (t0 Restricted-Matrix))
  (unless (eql new (codomain t0))
    (let ((delta-bc (- (coord-start new) (coord-start (codomain t0)))))
      (incf (bands-start t0) delta-bc)
      (incf (bands-end   t0) delta-bc))
    (setf (slot-value t0 'codomain) new))
  new)

(defmethod (setf domain) ((new Vector-Space)
			  (t0 Restricted-Matrix))
  (unless (eql new (domain t0))
    (let ((delta-bc (- (coord-start (domain t0)) (coord-start new))))
      (incf (bands-start t0) delta-bc)
      (incf (bands-end   t0) delta-bc))
    (setf (slot-value t0 'domain) new))
  new)

;;;-------------------------------------------------------

(defmethod print-object ((t0 Restricted-Matrix) str)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 str)
	(format str "~@[~a ~]~:(~a~) ~a->~a"
		(map-adjective t0)
		(class-name (class-of t0))
		(domain t0)
		(codomain t0)))
      (call-next-method)))

;;;-------------------------------------------------------

(defmethod restrict! ((t0 Restricted-Matrix)
		      (codomain Vector-Space)
		      (domain Vector-Space))
  (unless (and (eql codomain (codomain t0)) (eql domain (domain t0)))
    (setf (slot-value t0 'codomain) codomain)
    (setf (slot-value t0 'domain) domain)
    (setf (bands-start t0) (min-bands-start t0))
    (setf (bands-end t0) (max-bands-end t0)))
  t0)

(defmethod restrict ((t0 Restricted-Matrix)
		     (codomain Vector-Space)
		     (domain Vector-Space)
		     &key (result (make-instance 'Restricted-Matrix)))
  (setf (slot-value result 'source) t0)
  (setf (slot-value result 'domain) domain)
  (setf (slot-value result 'codomain) codomain)
  result)

;;;-------------------------------------------------------    
;;; Infer the maximum bandedness of a Restricted-Matrix from
;;; the bandedness of its source:

(defmethod min-bands-start ((t0 Restricted-Matrix))
  (az:bound (min-band-coord t0)
	    (+ (bands-start (source t0))
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord t0)))

(defmethod max-bands-end ((t0 Restricted-Matrix))
  (az:bound (min-band-coord t0)
	    (+ (bands-end (source t0))
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord t0)))

;;;-------------------------------------------------------    
;;; Compute the implied minimum bandedness of the source Matrix from
;;; the bandedness of a Restricted-Matrix:

(defmethod source-max-bands-start ((t0 Restricted-Matrix))
  (az:bound (min-band-coord (source t0))
	    (+ (bands-start t0)
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord (source t0))))

(defmethod source-min-bands-end ((t0 Restricted-Matrix))
  (az:bound (min-band-coord (source t0))
	    (+ (bands-end t0)
	       (- (coord-start (codomain t0)) (coord-start (domain t0))))
	    (max-band-coord (source t0))))

;;;-------------------------------------------------------

(defmethod (setf bands-start) ((start Integer) (t0 Restricted-Matrix))
  (setf (slot-value t0 'bands-start)
	(az:bound (min-band-coord t0) start (max-band-coord t0)))
  (setf (bands-start (source t0))
	(min (source-max-bands-start t0) (bands-start (source t0))))
  start)

(defmethod (setf bands-end) ((end Integer) (t0 Restricted-Matrix))
  (setf (slot-value t0 'bands-end)
	(az:bound (min-band-coord t0) end (max-band-coord t0)))
  (setf (bands-end (source t0))
	(max (source-min-bands-end t0) (bands-end (source t0))))
  end)

;;;-------------------------------------------------------

(defmethod diagonal-block? ((t0 Restricted-Matrix))
  (and (= (coord-start (codomain t0)) (coord-start (domain t0)))
       (= (coord-end (codomain t0)) (coord-end (domain t0)))))

;;;-------------------------------------------------------
;;; Infer whatever we can that is true of the submatrix
;;; based on its source:

(defmethod shared-initialize :after ((t0 Restricted-Matrix)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (unless (slot-boundp t0 'bands-start)
    (setf (slot-value t0 'bands-start) (min-bands-start t0)))
  (unless (slot-boundp t0 'bands-end)
    (setf (slot-value t0 'bands-end) (max-bands-end t0)))
  (let ((source (source t0))) 
    (cond
     ;; the boundary case---t0 covers its source
     ((and (eql (codomain t0) (codomain source))
	   (eql (domain t0) (domain source)))
      (setf (properties t0) (copy-list (properties source))))
     (t
      (delete-all-properties! t0)
      ;; in the general case, symmetry is the only inference
      ;; at present.
      (when (and (diagonal-block? t0) (symmetric? source))
	(add-property! t0 'symmetric?))))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Restricted-Matrix))
  (make-instance 'Restricted-Matrix
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :source (source t0)) )

(defmethod az:copy-to! ((t0 Restricted-Matrix) (result Restricted-Matrix))
  (assert (eql (domain t0) (domain result)))
  (assert (eql (codomain t0) (codomain result)))
  (setf (source result) (source t0))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------

(defun make-restricted-matrix (t0
			       &key
			       (start-row 0)
			       (end-row (dimension (codomain t0)))
			       (start-col 0)
			       (end-col (dimension (domain t0))))
  (az:type-check Matrix t0)
  (make-instance 'Restricted-Matrix
    :source t0
    :codomain (block-subspace (codomain t0) start-row end-row)
    :domain   (block-subspace (domain   t0) start-col end-col)))

;;;=======================================================

(defclass Inverse (Indirect-Map) ())

;;;-------------------------------------------------------

(defmethod domain   ((t0 Inverse)) (codomain (source t0)))
(defmethod codomain ((t0 Inverse)) (domain   (source t0)))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Inverse))
  (make-instance (class-of t0) :source (source t0)))

(defmethod az:copy-to! ((t0 Inverse) (result Inverse))
  (setf (source result) (az:copy (source t0)))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------

(defmethod print-object ((t0 Inverse) str)
  (if (az::slots-boundp t0 '(codomain domain source))
      (az:printing-random-thing (t0 str)
	(format str
		"~@[~a ~]~:(~a~) ~a->~a of ~a"
		(map-adjective t0)
		(class-name (class-of t0))
		(domain t0) (codomain t0)
		(source t0)))
      (call-next-method)))

;;;=======================================================

(defclass Inverse-Upper-Triangular-Matrix (Inverse) ())

(defclass Inverse-Lower-Triangular-Matrix (Inverse) ())

;;;=======================================================

(defmethod transpose ((t0 Inverse-Upper-Triangular-Matrix) 
		      &key (result
			    (make-instance 'Inverse-Lower-Triangular-Matrix)))
  (az:type-check Inverse-Lower-Triangular-Matrix result)
  (setf (source result) (transpose (source t0)))
  result) 

;;;-------------------------------------------------------

(defmethod transpose ((t0 Inverse-Lower-Triangular-Matrix) 
		      &key (result
			    (make-instance 'Inverse-Upper-Triangular-Matrix)))
  (az:type-check Inverse-Upper-Triangular-Matrix result)
  (setf (source result) (transpose (source t0)))
  result) 

