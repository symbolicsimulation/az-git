;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================

#||
#+:cmu
(alien:def-alien-routine ("dgeqrf_" dgeqrf) c-call:Void
  (m (* c-call:Int))
  (n (* c-call:Int))
  (a (Array Double-Float))
  (lda (* c-call:Int))
  (tau (Array Double-Float))
  (work (Array Double-float))
  (lwork (* c-call:Int))
  (info (array c-call:Int 1)))
||#

#+:excl
(ff:defforeign-list
    (list
     
     (list 'DGEQRF
	   :entry-point (ff:convert-to-lang "dgeqrf" :language :Fortran)
	   :language :Fortran
	   :arguments '(Fixnum		; m
			Fixnum		; n
			az:Float-Matrix ; a
			Fixnum		; lda
			az:Float-Vector ; tau
			az:Float-Vector ; work
			Fixnum		; lwork
			(az:Fixnum-Vector 1) ; info
			)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil)

     (list 'DGEQPF
	   :entry-point (ff:convert-to-lang "dgeqpf" :language :Fortran)
	   :language :Fortran
	   :arguments '(Fixnum		; m
			Fixnum		; n
			az:Float-Matrix ; a
			Fixnum		; lda
			az:Fixnum-Vector ; jpvt
			az:Float-Vector ; tau
			az:Float-Vector ; work
			Fixnum		; lwork
			(az:Fixnum-Vector 1) ; info
			)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil)
     
     (list 'DORGQR
	   :entry-point (ff:convert-to-lang "dorgqr" :language :Fortran)
	   :language :Fortran
	   :arguments '(Fixnum		; m
			Fixnum		; n
			Fixnum		; k
			az:Float-Matrix ; a
			Fixnum		; lda
			az:Float-Vector ; tau
			az:Float-Vector ; work
			Fixnum ; lwork
			(az:Fixnum-Vector 1) ; info
			)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil)
     
     (list 'dormqr
	   :entry-point (ff:convert-to-lang "dormqr" :language :Fortran)
	   :language :Fortran
	   :arguments '((Simple-String 1)	; side
			(Simple-String 1)	; ltrans
			Fixnum		; m
			Fixnum		; n
			Fixnum		; k
			az:Float-Matrix ; a
			Fixnum		; lda
			az:Float-Vector ; tau
			az:Float-Array ; c
			Fixnum		; ldc
			az:Float-Vector ; work
			Fixnum ; lwork
			(az:Fixnum-Vector 1) ; info
			)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil)

     
     (list 'dtrtri
	   :entry-point (ff:convert-to-lang "dtrtri" :language :Fortran)
	   :language :Fortran
	   :arguments '((Simple-String 1)	; uplo
			(Simple-String 1)	; diag
			Fixnum		; n
			az:Float-Matrix ; a
			Fixnum		; lda
			(az:Fixnum-Vector 1) ; info
			)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil)

       
     (list 'dtrtrs
	   :entry-point (ff:convert-to-lang "dtrtrs" :language :Fortran)
	   :language :Fortran
	   :arguments '((Simple-String 1)	; uplo
			(Simple-String 1)	; trans
			(Simple-String 1)	; diag
			Fixnum		; n
			Fixnum		; nrhs
			az:Float-Matrix ; a
			Fixnum		; lda
			az:Float-Vector ; b
			Fixnum		; ldb
			(az:Fixnum-Vector 1) ; info
			)
	   :print t
	   :arg-checking t
	   :call-direct t
	   :callback nil)

     ))

;;;=======================================================

(defclass QR-Decomposition (Standard-Object)
	  ((m
	    :type az:Card16
	    :reader m
	    :initarg :m)
	   (n
	    :type az:Card16
	    :reader n
	    :initarg :n)
	   (qr
	    :type az:Float-Matrix
	    :reader qr
	    :initarg :qr)
	   (tau
	    :type az:Float-Vector
	    :reader tau
	    :initarg :tau)
	   (c
	    :type az:Float-Vector
	    :reader c
	    :initarg :c)
	   (work
	    :type az:Float-Vector
	    :reader work
	    :initarg :work)	   
	   (info
	    :type (az:Fixnum-Vector 1)
	    :reader info
	    :initform (az:make-fixnum-vector 1)))
  (:documentation
   "A data structure used to hold an LAPACK representation of a
QR decomposition."))

;;;------------------------------------------------------- 

(defmethod initialize-instance :after ((qrd QR-Decomposition) &rest initargs)
  (declare (type QR-Decomposition qrd)
	   (ignore initargs))
  (let ((m (m qrd))
	(n (n qrd)))
    (declare (type az:Card16 m n))
    (setf (slot-value qrd 'qr) (az:make-float-matrix n m))
    (setf (slot-value qrd 'tau) (az:make-float-vector n))
    (setf (slot-value qrd 'c) (az:make-float-vector m))
    (setf (slot-value qrd 'work) (az:make-float-vector (lwork qrd)))))

(defun lwork (qrd)
  (declare (type QR-Decomposition qrd))
  (print
  (cond
   ((and (= 5000 (m qrd)) (= 150 (n qrd))) 4800)
   ((and (= 4102 (m qrd)) (= 163 (n qrd))) 5216)
   (t (* 6 (n qrd))))))

;;;-------------------------------------------------------

(defmethod qr-decompose ((a Array))

  "Compute a QR-Decompostion of <a>, using Lapack."
  
  (declare (type az:Float-Matrix a))
  
  (let* ((m (array-dimension a 0))
	 (n (array-dimension a 1))
	 (qrd (make-instance 'QR-Decomposition :m m :n n))
	 (qr (qr qrd))
	 (tau (tau qrd))
	 (info (info qrd))
	 (work (work qrd))
	 (lwork (length work)))

    (declare (type az:Card16 m n)
	     (type QR-Decomposition qrd)
	     (type az:Float-Matrix qr)
	     (type az:Float-Vector tau work)
	     (type (az:Fixnum-Vector 1) info))
    
    (az:transpose-float-matrix a :result (qr qrd))
    (time
     (dgeqrf m n qr m tau work lwork info)
     )
    (unless (<= (aref work 0) (/ lwork n))
      (warn "~&Optimal block size for DGEQRF is ~f.~%" (aref work 0)))
    (unless (zerop (aref info 0))
      (error "~&QR decompose (via DGEQRF) failed, info = ~d.~%" (aref info 0)))
    (values qrd)))

;;;-------------------------------------------------------

(defun qr-solve (qrd b f r2norm)

  "Solve Af = b for f, where <qrd> is an Lapack qr decomposition of A."
  
  (declare (optimize (safety 0) (speed 3))
	   (type QR-Decomposition qrd)
	   (type az:Float-Vector b f r2norm))
  (let ((m (m qrd))
	(n (n qrd))
	(qr (qr qrd))
	(c (c qrd))
	(work (work qrd))
	(info (info qrd))
	(r2 0.0d0))
    (declare (type az:Card16 m n)
	     (type az:Float-Matrix qr)
	     (type az:Float-Vector c work)
	     (type (az:Fixnum-Vector 1) info)
	     (type Double-Float r2))

    ;; compute -Q^T b-
    (dotimes (i m)
      (declare (type az:Card16 i))
      (setf (aref c i) (aref b i)))
    (dormqr "L" "T" m 1 n qr m (tau qrd) c m work 1 info)
    (unless (= (aref work 0) 1.0d0)
      (warn "~&Optimal block size for dormqr is ~f.~%" (aref work 0)))
    (unless (zerop (aref info 0))
      (error "~&QT*b (via dormqr) failed, info = ~d.~%" (aref info 0)))
    
    ;; compute residual norm by summing last m-n elts of c
    (setf r2 0.0d0)
    (do ((i n (+ i 1)))
	((>= i m))
      (declare (type az:Card16 i))
      (setf r2 (+ r2 (the Double-Float (az:flsq (aref c i))))))
    (setf (aref r2norm 0) r2)
    
    (dtrtrs "U" "N" "N" n 1 qr m c n info)
    (unless (zerop (aref info 0))
      (error "~&solving Rf = b (via dtrtrs) failed, info = ~d.~%"
	     (aref info 0)))
    (dotimes (i n)
      (declare (type az:Card16 i))
      (setf (aref f i) (aref c i)))
    (values)))

;;;=======================================================

(defun qr-inverse (a)
  "Compute the generalized inverse of A, using its qr decomposition."
  (declare (optimize (safety 0) (speed 3))
	   (type az:Float-Matrix a))
  (let* ((m (array-dimension a 0))
	 (n (array-dimension a 1))
	 (qr (az:make-float-matrix n m :initial-element 0.0d0))
	 (a-t (az:make-float-matrix m n :initial-element 0.0d0))
	 (a- (az:make-float-matrix n m :initial-element 0.0d0))
	 (tau (az:make-float-vector n :initial-element 0.0d0))
	 (nb 6)
	 (lwork (* nb m))
	 (work (az:make-float-vector lwork :initial-element 0.0d0))
	 (info (az:make-fixnum-vector 1))
	 (qt (az:make-float-matrix m m :initial-element 0.0d0))
	 (q1t (az:make-float-matrix n m :initial-element 0.0d0))
	 (q2t (az:make-float-matrix (- m n) m :initial-element 0.0d0)))

    ;; compute qr decomposition using dgeqrf
    (az:transpose-float-matrix a :result qr)
    (dgeqrf m n qr m tau work lwork info)
    (unless (<= (aref work 0) nb)
      (warn "~&Optimal block size for DGEQRF is ~f.~%" (aref work 0)))
    (unless (zerop (aref info 0)) (error "~&DGEQRF info=~d.~%" (aref info 0)))
    ;; compute R inverse using DTRTRI
    (dotimes (i n)
      (declare (type az:Card16 i))
      (dotimes (j (+ i 1))
	(declare (type az:Card16 j))
	(setf (aref a-t i j) (aref qr i j))))
    (dtrtri "U" "N" n a-t n info)
    (unless (= 0 (aref info 0)) (error "~&DTRTRI info=~d.~%" (aref info 0)))
    ;; compute generalized inverse of A as R-1*QT, using DORMQR
    (dormqr "R" "T" n m m qr m tau a-t n work lwork info)
    (unless (<= (aref work 0) nb)
      (warn "~&Optimal block size for DORMQR is ~f.~%" (aref work 0)))
    (unless (zerop (aref info 0))(error "~&DORMQR info=~d.~%" (aref info 0)))
    ;; compute QT, Q1T, and Q2T using DORGQR
    (dotimes (i n)
      (declare (type az:Card16 i))
      (dotimes (j m)
	(declare (type az:Card16 j))
	(setf (aref qt i j) (aref qr i j))))
    (dorgqr m m n qt m tau work lwork info)
    (unless (<= (aref work 0) nb)
      (warn "~&Optimal block size for DORGQR is ~f.~%" (aref work 0)))
    (unless (zerop (aref info 0)) (error "~&DORGQR info=~d.~%" (aref info 0)))
    ;; extract q1t from qt
    (dotimes (i n)
      (declare (type az:Card16 i))
      (dotimes (j m)
	(declare (type az:Card16 j))
	(setf (aref q1t i j) (aref qt i j))))
    ;; extract q2t from qt
    (dotimes (i (- m n))
      (declare (type az:Card16 i))
      (dotimes (j m)
	(declare (type az:Card16 j))
	(setf (aref q2t i j) (aref qt (+ i n) j))))
    (values (az:transpose-float-matrix a-t :result a-) q1t q2t)))

