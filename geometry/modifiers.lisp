;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================
;;; These mixins imply somethings about the intended use:
;;;=======================================================
;;; Modifier Maps are frequently used to destructively alter
;;; a Matrix or a Vector. Examples are Householder, Gauss, Givens,
;;; Diagonal...  The reason for making this a class is that (method
;;; compose (Modifier Matrix)) and (method compose (Matrix Modifier))
;;; can be defined once and can just call the destructive, specialized
;;; methods such as (method compose-right! (Diagonal Matrix)) and
;;; (method compose-left! (Matrix Diagonal)) and similarly
;;; transform can be defined in terms of destructive transform!.
;;;
;;; Not surprisingly, the Modifier protocol adds <transform!>,
;;; <compose-right!>, and <compose-left!> to the Linear-Map
;;; protocol.

(defclass Modifier (Linear-Map) ())

;;;-------------------------------------------------------

(defmethod modifier? ((t0 Modifier)) t)

;;;=======================================================

(defclass Symmetric-Modifier  (Symmetric Modifier) ())

;;;=======================================================

(defclass Identity-Map (Symmetric-Modifier) ())

;;;-------------------------------------------------------

(defun identity-map? (t0) (typep t0 'Identity-Map))
  
;;;-------------------------------------------------------

(defmethod orthogonal? ((t0 Identity-Map)) t)
(defmethod idempotent? ((t0 Identity-Map)) t)
(defmethod positive-definite? ((t0 Identity-Map)) t)
(defmethod unit-diagonal? ((t0 Identity-Map)) t)
(defmethod bands-end ((t0 Identity-Map)) 1)
(defmethod bands-start ((t0 Identity-Map)) 0)

;;;-------------------------------------------------------
#||latex

  Modifier Maps can be considered to be defined on
  the natural minimal block subspace that corresponds to those elements
  that they affect. The modifier Maps that are actually used
  are direct sums of the minimal Map and the identity.
  {\tt Extend!} is used to adjust the domain and range 
  of Modifier Maps, by, in effect, direct summing with
  other identities. It extends the domain and codomain to a super
  space of the block subspace on which they are initially defined.

||#


(defmethod extend! ((a Identity-Map)
		    &optional
		    (vspace (parent-space (domain a))))
  (assert (subspace? (domain a) vspace))
  (the-identity-map vspace))

;;;-------------------------------------------------------

(defvar *Vector-Space-Identity-Map-table* (make-hash-table)
  "Ensure only one Identity Map for each vector space.")

(defmethod the-identity-map ((vs Vector-Space))
  "Ensure only one Identity Map for each vector space."
  (let ((identity-map (gethash vs *Vector-Space-Identity-Map-Table*)))
    (when (null identity-map)
      (setf identity-map
	    (make-instance 'Identity-Map :domain vs :codomain vs))
      (setf (gethash vs *Vector-Space-Identity-Map-Table*) identity-map))
    identity-map)) 

(defmethod transpose ((t0 Identity-Map)
		      &key result)
  (unless (or (null result) (eq result t0))
    (error "Can't pass a :result to (transpose (Identity-Map))."))
  (the-identity-map (dual-space (domain t0))))

;;;=======================================================
#||latex

 A Gauss Map has the form:
 \begin{displaymath}
  I -  \alpha \otimes e_k
 \end{displaymath}
 where $\otimes$ means tensor product, $e_k$ is the $k$th canonical
 basis vector, $\alpha$ is a vector whose first $k$ elements are 0.
 Gauss Maps are usually chosen to zero the elements of a 
 vector after <start> in LU decomposes. $\alpha$ is represented by
 a vec that only holds the {\tt start} below {\tt end} non-zero
 elements of $\alpha$.
 
||#

(defclass Gauss (Modifier)
	  ((vec
	    :type Block-Point
	    :initarg :vec
	    :reader vec)))

;;;-------------------------------------------------------
;;; A Gauss Map zeros the elements after <k>:

(defmethod k ((t0 Gauss)) (- (coord-start (vec t0)) 1))

;;;-------------------------------------------------------
;;; some properties of Gauss Maps:

;;; they're unit lower triangular:
(defmethod bands-end ((t0 Gauss)) 1)

(defmethod unit-diagonal? ((t0 Gauss)) t)

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Gauss)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (automorphic? t0)))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Gauss))
  (make-instance 'Gauss
		 :vec (az:copy (vec t0))
		 :domain (domain t0)
		 :codomain (codomain t0)))

(defmethod az:copy-to! ((t0 Gauss) (result Gauss))
  (setf (slot-value result 'vec) (az:copy (vec t0)))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------
#||latex

  Modifier Maps can be considered to be defined on
  the natural minimal block subspace that corresponds to those elements
  that they affect. The modifier Maps that are actually used
  are direct sums of the minimal Map and the identity.
  {\tt Extend!} is used to adjust the domain and range 
  of Modifier Maps, by, in effect, direct summing with
  other identities. It extends the domain and codomain to a super
  space of the block subspace on which they are initially defined.

||#

(defmethod extend! ((t0 Gauss)
		    &optional
		    (vspace (parent-space (domain t0))))
  (assert (subspace? (domain t0) vspace))
  (setf (domain t0) vspace)
  (setf (codomain t0) vspace)
  (reinitialize-instance t0) 
  t0)

;;;-------------------------------------------------------

(defun make-gauss-for (vspace v
		       &optional
		       (k (coord-start vspace))
		       (end (coord-end vspace))
		       &key
		       result)
  (assert (<= (coord-start vspace) k end (coord-end vspace)))
  (assert (element? v vspace))
  (assert (not (zerop (coordinate v k))))
  (let ((alpha (/ (coordinate v k)))
	(subspace (block-subspace vspace (+ k 1) end)))
    (cond
     ((null result)
      (setf result (make-instance 'Gauss
		     :codomain vspace
		     :domain vspace
		     :vec (make-element subspace))))
     (t
      (assert (typep result 'Gauss))
      (assert (>= (dimension (vec result)) (dimension subspace)))
      (setf (home (vec result)) subspace)
      (setf (slot-value result 'codomain) vspace)
      (setf (slot-value result 'domain) vspace)
      (setf (slot-value result 'vec) (vec result))
      (reinitialize-instance result)
      ))
    (scaled-ortho-project v subspace alpha :result (vec result))
    result))

;;;=======================================================
  #||latex

  A Transpose-Gauss Map has the form:
  \begin{displaymath}
  I -  e_k \otimes \alpha 
  \end{displaymath}
  where $\otimes$ means tensor product, $e_k$ is the $k$th canonical
  basis vector, $\alpha$ is a vector whose first $k$ elements are 0.
  This class is just for convenience, so we can easily transpose
  Gauss Maps.
 
  ||#

(defclass Transpose-Gauss (Modifier)
	  ((vec
	    :type Block-Point
	    :initarg :vec
	    :reader vec)))

;;;-------------------------------------------------------
;;; A Gauss Map zeros the elements after <k>:

(defmethod k ((t0 Transpose-Gauss)) (- (coord-start (vec t0)) 1))

;;;-------------------------------------------------------
;;; some properties of Transpose-Gauss Maps:

;;; they're unit upper triangular:
(defmethod bands-start ((t0 Transpose-Gauss)) 0)

(defmethod unit-diagonal? ((t0 Transpose-Gauss)) t)

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Transpose-Gauss)
				     slot-names
				     &rest initargs
				     &key
				     (start 1)
				     (end (dimension (domain t0)))
				     &allow-other-keys)
  (declare (ignore initargs))
  slot-names
  (unless (slot-boundp t0 'vec)
    (setf (slot-value t0 'vec)
      (make-element (block-subspace (domain t0) start end))))
  (assert (automorphic? t0)))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Transpose-Gauss))
  (make-instance 'Transpose-Gauss
		 :vec (az:copy (vec t0))
		 :domain (domain t0)
		 :codomain (codomain t0)))

(defmethod az:copy-to! ((t0 Transpose-Gauss) (result Transpose-Gauss))
  (setf (slot-value result 'vec) (az:copy (vec t0)))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------

(defmethod transpose ((t0 Gauss)
		      &key (result (make-instance 'Transpose-Gauss)))
  (az:type-check Transpose-Gauss result)
  (assert (eql (dual-space (domain t0)) (codomain result)))
  (assert (eql (dual-space (codomain t0)) (domain result)))
  (setf (slot-value result 'domain) (codomain t0))
  (setf (slot-value result 'codomain) (domain t0))
  (setf (slot-value result 'vec) (dual-vector (vec t0)))
  (reinitialize-instance result)
  result)

;;;=======================================================
  #||latex

  A {\bf Householder Map} from $\Re^n$ to $\Re^n$ has the form:
  \begin{displaymath}
  I - \frac{2}{\|\v{\nu}\|^2} \v{\nu} \otimes \v{\nu}
  \end{displaymath}

  Householder Maps are usually chosen to zero the elements of
  a v (a vector or a row or column of a Matrix) for a contiguous range of 
  indices, say, from {\tt (+ start 1)} below {\tt end}. To save space and, more
  important, to make it easy to adjust the domain and codomain of the
  Householder, $\nu$ is represented by a vec of length {\tt (- end start)}
  which hold exactly those elements of $\nu$ that are non-zero.

  The representation caches $\frac{2}{\|\v{\nu}\|^2}$ 
  in the {\tt -2/norm2} slot.

  ||#

(defclass Householder (Symmetric-Modifier)
	  ((vec
	    :type Point
	    :initarg :vec
	    :reader vec)
	   (-2/norm2
	    :type Double-Float
	    :accessor -2/norm2
	    :initarg :-2/norm2)
	   ;; the leading element of the vector the Househholder
	   ;; was designed to zero
	   (leading-elt
	    :type Double-Float
	    :accessor leading-elt
	    :initform 0.0d0)))
  
;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Householder)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  
  (assert (automorphic? t0))
  (assert (element? (vec t0) (domain t0)))

  (unless (slot-boundp t0 '-2/norm2)
    (let ((norm2 (l2-norm2 (vec t0))))
      (assert (plusp norm2))
      (setf (-2/norm2 t0) (/ -2.0d0 norm2)))))

;;;-------------------------------------------------------

(defun affected-space (t0)
  (az:type-check Householder t0)
  (home (vec t0)))

(defmethod orthogonal? ((t0 Householder)) t)

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Householder))
  (make-instance 'Householder
		 :vec (az:copy (vec t0))
		 :codomain (codomain t0)
		 :domain (domain t0)
		 :-2/norm2 (-2/norm2 t0)) )

(defmethod az:copy-to! ((t0 Householder) (result Householder))
  (setf (slot-value result 'vec) (az:copy (vec t0)))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (setf (slot-value result '-2/norm2) (-2/norm2 t0))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------
#||latex

  Modifier Maps can be considered to be defined on
  the natural minimal block subspace that corresponds to those elements
  that they affect. The modifier Maps that are actually used
  are direct sums of the minimal Map and the identity.
  {\tt Extend!} is used to adjust the domain and range 
  of Modifier Maps, by, in effect, direct summing with
  other identities. It extends the domain and codomain to a super
  space of the block subspace on which they are initially defined.

||#

(defmethod extend! ((a Householder)
		    &optional
		    (vspace (parent-space (domain a))))
  (assert (subspace? (domain a) vspace))
  (setf (domain a) vspace)
  (setf (codomain a) vspace)
  (reinitialize-instance a)
  a)

;;;-------------------------------------------------------
#||latex

  Returns a Householder Map, $H$ such that
  $(H\v{v})_i = 0$ for $i = start+1,\ldots,end-1.$

||#

;;; l1 stabilized version 

(defun make-householder (vspace v start end &key (result nil))
  (assert (<= (coord-start vspace) start end (coord-end vspace)))
  (assert (< start end))
  (let* ((a (l1-norm v))
	 (subspace (block-subspace vspace start end))
	 1/norm1 x0 norm2 norm vt -2/norm2)
    (unless (null result)
      (assert (typep result 'Householder))
      (assert (eq (home (vec result)) subspace)))
    (cond
     ((az:small? a) (setf result (the-identity-map vspace)));; the trivial case
     (t
      (setf 1/norm1 (/ 1.0d0 (+ a (abs (coordinate v start)))))
      (setf vt (if (null result)
		   (make-element subspace)
		 (vec result)))
      ;; scale by 1/norm1 for better numerical properties
      ;; see Press et al. Ch. 11.2
      ;; (golub&vanloan suggest 1/norm$, alg. 3.3-1)
      (scaled-ortho-project v subspace 1/norm1 :result vt)
      (setf norm2 (l2-norm2 vt))
      (assert (plusp norm2))
      (setf norm (sqrt norm2))
      (setf x0 (coordinate vt start))
      (incf (coordinate vt start) (* norm (az:sign x0)))
      (setf -2/norm2 (/ -1.0d0 (+ norm2 (* (abs x0) norm))))
      (if (null result)
	  (setf result (make-instance 'Householder 
			 :vec vt
			 :domain vspace
			 :codomain vspace
			 :-2/norm2 -2/norm2))
	;; else
	(progn
	  (setf (slot-value result 'vec) vt)
	  (setf (slot-value result 'domain) vspace)
	  (setf (slot-value result 'codomain) vspace)
	  (setf (slot-value result '-2/norm2) -2/norm2)
	  (reinitialize-instance result)
	  )))))
  result)


;;;=======================================================
;;; A Givens Map has the form:
;;;
;;; (1   .     .      )                         
;;; (    .     .      )
;;; (....c.....s......) i0 
;;; (       1         ) 
;;; (...-s.....c......) i1
;;; (    .     .      )
;;; (    .     .    1 )
;;;      i0    i1
;;;
;;; Givens Maps are usually chosen to zero
;;; the <i1>th elements of a vector. <c> and <s> are
;;; (cos <theta>) and (sin <theta>).
;;;

(defclass Givens (Modifier)
	  ((i0 :type az:Array-Index :initarg :i0 :accessor i0)
	   (i1 :type az:Array-Index :initarg :i1 :accessor i1)
	   (c  :type Double-Float :initarg :c  :accessor c)
	   (s  :type Double-Float :initarg :s  :accessor s)))

;;;-------------------------------------------------------
;;; ensure that i0 <= i1

(defmethod shared-initialize :after ((t0 Givens) slot-names &rest initargs)
  (declare (ignore initargs))
  slot-names
  (unless (<= (i0 t0) (i1 t0))
    (rotatef (i0 t0) (i1 t0))
    (setf (s t0) (- (s t0))))
  (assert (automorphic? t0))
  (assert (< (- (coord-start (domain t0)) 1) (i0 t0) (coord-end (domain t0))))
  (assert (< (- (coord-start (domain t0)) 1) (i1 t0) (coord-end (domain t0)))))

(defmethod orthogonal? ((t0 Givens)) t)

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Givens))
  (make-instance 'Givens
		 :domain (domain t0)
		 :codomain (codomain t0)
		 :i0 (i0 t0)
		 :i1 (i1 t0)
		 :c (c t0)
		 :s (s t0)))

(defmethod az:copy-to! ((t0 Givens) (result Givens))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (setf (slot-value result 'i0) (i0 t0))
  (setf (slot-value result 'i1) (i1 t0))
  (setf (slot-value result 'c) (c t0))
  (setf (slot-value result 's) (s t0))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------
#||latex

  Modifier Maps can be considered to be defined on
  the natural minimal block subspace that corresponds to those elements
  that they affect. The modifier Maps that are actually used
  are direct sums of the minimal Map and the identity.
  {\tt Extend!} is used to adjust the domain and range 
  of Modifier Maps, by, in effect, direct summing with
  other identities. It extends the domain and codomain to a super
  space of the block subspace on which they are initially defined.

||#

(defmethod extend! ((a Givens)
		    &optional
		    (vspace (parent-space (domain a))))
  (assert (subspace? (domain a) vspace))
  (setf (domain a) vspace)
  (setf (codomain a) vspace)
  (reinitialize-instance a)
  a)

;;;-------------------------------------------------------
;;; after Golub and VanLoan, Alg 3.4-1

(defun make-givens-for (vspace x0 x1 i0 i1)
  (az:type-check Vector-Space vspace)
  (az:type-check Double-Float x0 x1)
  (az:type-check az:Array-Index i0 i1)
  (cond ((or (az:small? x1 x0) (= i0 i1))
	 (the-identity-map vspace)) 
	;; else
	(t
	 (let (c s)
	   (if (> (abs x1) (abs x0))
	       (let ((tmp (/ x0 x1)))
		 (setf s (/ (sqrt (+ 1.0d0 (* tmp tmp)))))
		 (setf c (* tmp s)))
	       ;; else
	       (progn
		 (assert (not (zerop x0)))
		 (let ((tmp (/ x1 x0)))
		   (setf c (/ (sqrt (+ 1.0d0 (* tmp tmp)))))
		   (setf s (* tmp c)))))
	   (make-instance 'Givens :s s :c c :i0 i0 :i1 i1
			  :domain vspace :codomain vspace)))))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Givens) &key (result (az:copy t0)))
  (when (not (eq t0 result))
    (az:type-check Givens result)
    (assert (eql (dual-space (domain t0)) (codomain result)))
    (assert (eql (dual-space (codomain t0)) (domain result)))
    (setf result (az:copy t0 :result result))) 
  (setf (s result) (- (s result)))
  result)

(defmethod transpose! ((t0 Givens)) (setf (s t0) (- (s t0))) t0)

;;;=======================================================

(defclass Diagonal-Vector (Symmetric-Modifier)
	  ((vec
	    :type Point
	    :initarg :vec
	    :reader vec)))

;;;-------------------------------------------------------

(defmethod bands-start ((t0 Diagonal-Vector)) 0)
(defmethod bands-end   ((t0 Diagonal-Vector)) 1)

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Diagonal-Vector)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (unless (slot-boundp t0 'vec)
    (setf (slot-value t0 'vec) (make-element (domain t0))))
  (assert (element? (vec t0) (codomain t0)))
  (assert (element? (vec t0) (domain t0))))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Diagonal-Vector))
  (make-instance 'Diagonal-Vector :codomain (codomain t0) :domain (domain t0)))

(defmethod az:copy-to! ((t0 Diagonal-Vector) (result Diagonal-Vector))
  (az:copy (vec t0) :result (vec result))
  result)

;;;=======================================================
;;; Permutations
;;;=======================================================

(defclass Pivot (Symmetric-Modifier)
	  ((i0 :type az:Array-Index :initarg :i0 :accessor i0)
	   (i1 :type az:Array-Index :initarg :i1 :accessor i1)))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Pivot)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (assert (< (- (coord-start (domain t0)) 1) (i0 t0) (coord-end (domain t0))))
  (assert (< (- (coord-start (domain t0)) 1) (i1 t0) (coord-end (domain t0)))))

(defmethod orthogonal? ((t0 Pivot)) t)
 
;;;-------------------------------------------------------

(defmethod print-object ((t0 Pivot) str)
  (if (az::slots-boundp t0 '(codomain domain i0 i1))
      (az:printing-random-thing (t0 str)
	(format str "~@[~a ~]~:(~a~) ~a->~a ~d<->~d"
		(map-adjective t0)
		(class-name (class-of t0))
		(domain t0) (codomain t0)
		(i0 t0) (i1 t0)))
      (call-next-method)))

;;;-------------------------------------------------------

(defmethod symmetric?  ((t0 Pivot)) t)
(defmethod bands-start ((t0 Pivot)) (- (abs (- (i0 t0) (i1 t0)))))
(defmethod bands-end   ((t0 Pivot)) (+ (abs (- (i0 t0) (i1 t0))) 1))

;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 Pivot))
  (make-instance 'Pivot
		 :codomain (codomain t0)
		 :domain (domain t0)
		 :i0 (i0 t0)
		 :i1 (i1 t0)))

(defmethod az:copy-to! ((t0 Pivot) (result Pivot))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (setf (slot-value result 'i0) (i0 t0))
  (setf (slot-value result 'i1) (i1 t0))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------
#||latex

  Modifier Maps can be considered to be defined on
  the natural minimal block subspace that corresponds to those elements
  that they affect. The modifier Maps that are actually used
  are direct sums of the minimal Map and the identity.
  {\tt Extend!} is used to adjust the domain and range 
  of Modifier Maps, by, in effect, direct summing with
  other identities. It extends the domain and codomain to a super
  space of the block subspace on which they are initially defined.

||#


(defmethod extend! ((a Pivot) &optional (vspace (parent-space (domain a))))
  (assert (subspace? (domain a) vspace))
  (setf (domain a) vspace)
  (setf (codomain a) vspace)
  (reinitialize-instance a)
  a)

;;;-------------------------------------------------------

(defun make-pivot (vs i0 i1)
  (if (= i0 i1)
      (the-identity-map vs)
      ;; else
      (make-instance 'Pivot :domain vs :codomain vs :i0 i0 :i1 i1)))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Pivot) &key (result (az:copy t0)))
  (unless (eq t0 result) (az:copy t0 :result result))
  result)

;;;=======================================================
  #||latex

  An {\bf 1d-Annihilator Map} from $\Re^n$ to $\Re^n$
  has the form:
  \begin{displaymath}
  A = I - \frac{1}{\|\v{\nu}\|^2} \v{\nu} \otimes \v{\nu}
  \end{displaymath}
  $A$ is the orthogonal projection on the $n-1$ dimensional subspace
  orthogonal to $\v{\nu}$.
  1d-Annihilator Maps are used to construct Maps
  with known kernels, for testing purposes. The representation and
  methods are essentially the same as for Householders.

  ||#

(defclass 1d-Annihilator (Symmetric-Modifier)
	  ((vec
	    :type Block-Point
	    :initarg :vec
	    :reader vec)
	   (-1/norm2 :type Double-Float :accessor -1/norm2)))
  
;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 1d-Annihilator)
				     slot-names
				     &rest initargs)
  (declare (ignore initargs))
  slot-names
  (unless (slot-boundp t0 'vec)
    (setf (slot-value t0 'vec) (make-element (domain t0)))
    (setf (coordinate (vec t0) 0) 1.0d0))
  (assert (element? (vec t0) (codomain t0)))
  (assert (element? (vec t0) (domain t0)))
  (let ((norm2 (l2-norm2 (vec t0))))
    (assert (plusp norm2))
    (setf (-1/norm2 t0) (/ -1.0d0 norm2))))
  
(defmethod orthogonal? ((t0 1d-Annihilator)) t)
 
;;;-------------------------------------------------------

(defmethod az:new-copy ((t0 1d-Annihilator))
  (make-instance '1d-Annihilator
		 :vec (az:copy (vec t0))
		 :codomain (codomain t0)
		 :domain (domain t0)))

(defmethod az:copy-to! ((t0 1d-Annihilator) (result 1d-Annihilator))
  (setf (slot-value result 'domain) (domain t0))
  (setf (slot-value result 'codomain) (codomain t0))
  (setf (slot-value result 'vec) (vec t0))
  (reinitialize-instance result)
  result)

;;;-------------------------------------------------------
#||latex

  Returns a 1d-Annihilator Map for {\tt v}.

||#

(defun make-1d-Annihilator-for (v)
  (make-instance '1d-Annihilator
		  :vec (az:copy v)
		  :domain (Rn (dimension v))
		  :codomain (Rn (dimension v))))






