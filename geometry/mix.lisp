;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================

(defmethod scale! ((a Number) (t0 Matrix-Super))
  (when (< a 0)
    (cond ((positive-definite? t0)
	   (delete-property! t0 'positive-definite?)
	   (add-property! t0 'negative-definite?))
	  ((negative-definite? t0)
	   (delete-property! t0 'negative-definite?)
	   (add-property! t0 'positive-definite?) )))
  (let ((2d (coords t0))
	(s (coord-start (domain t0)))
	(e (coord-end (domain t0))))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
      (v*x! 2d a :start s :end e :vtype :row :on i))) 
  t0)

(defmethod scale! ((a Number) (t0 2x2-Direct-Sum-Map))
  (when (< a 0)
    (cond ((positive-definite? t0)
	   (delete-property! t0 'positive-definite?)
	   (add-property! t0 'negative-definite?))
	  ((negative-definite? t0)
	   (delete-property! t0 'negative-definite?)
	   (add-property! t0 'positive-definite?) )))
  (scale! a (t00 t0)) (scale! a (t01 t0))
  (scale! a (t10 t0)) (scale! a (t11 t0))
  t0)

;;;=======================================================
;;; Adding linear Maps:
;;;=======================================================

(defmethod add ((t0 Matrix-Super) (t1 Matrix-Super)
		&key (result (make-instance 'Matrix
			       :domain (domain t1)
			       :codomain (codomain t1)
			       :coords (az:make-float-matrix
					(dimension (domain t1))
					(dimension (codomain t1))))))
  (cond ((eq t0 result) (add-right! t1 result))
	((eq t1 result) (add-right! t0 result))
	(t
	 (az:type-check Matrix result) 
	 (assert (eql (domain t0)   (domain result)))
	 (assert (eql (codomain t0) (codomain result)))
	 (az:copy t0 :result result)
	 (add-right! t1 result)))
  result)

;;;-------------------------------------------------------

(defmethod add-left! ((t0 Matrix-Super) (t1 Matrix-Super))
  (add-right! t1 t0))

;;;-------------------------------------------------------

(defmethod add-right! :before ((t0 Linear-Map) (t1 Linear-Map))
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (codomain t0) (codomain t1))))

;;;-------------------------------------------------------

(defmethod add-right! ((t0 Matrix-Super) (t1 Matrix-Super))
  (let ((ar0 (coords t0))
	(ar1 (coords t1))
	(start (coord-start (domain t0)))
	(end (coord-end (domain t0))))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
	    (v+v! ar1 ar0
		  :start0 start :end0 end :vtype0 :row :on0 i
		  :start1 start :end1 end :vtype1 :row :on1 i)))
  t1)

;;;-------------------------------------------------------

(defmethod add-right! ((t0 Diagonal-Vector) (t1 Matrix-Super))
  (let ((v (coords (vec t0)))
	(2d (coords t1)))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0))) 
	    (incf (aref 2d i i) (aref v i))))
  t1)

(defmethod add-left! ((t0 Matrix-Super) (t1 Diagonal-Vector))
  (add-right! t1 t0)
  t1)

(defmethod add-to! ((t0 Matrix-Super) (t1 Diagonal-Vector)
		    (result Matrix-Super))
  (cond ((eq t0 result) (add-left! t0 t1))
	(t (az:copy t0 :result result)
	   (add-left! t1 result)))
  result)

(defmethod add-to! ((t0 Diagonal-Vector) (t1 Matrix-Super)
		    (result Matrix-Super))
  (cond ((eq t1 result) (add-right! t0 t1))
	(t (az:copy t1 :result result)
	   (add-left! t0 result)))
  result)

;;;=======================================================
;;; Subtracting linear Maps:
;;;=======================================================

(defmethod sub ((t0 Matrix-Super) (t1 Matrix-Super)
		&key (result (make-instance 'Matrix
			       :domain (domain t1)
			       :codomain (codomain t1))))
  (cond ((eq t0 result) (sub-left! result t1))
	((eq t1 result) (sub-right! t0 result))
	(t
	 (az:type-check Matrix result) 
	 (assert (eql (domain t0)   (domain result)))
	 (assert (eql (codomain t0) (codomain result)))
	 (az:copy t0 :result result)
	 (sub-left! result t1)))
  result)

;;;-------------------------------------------------------

(defmethod sub-left! ((t0 Matrix-Super) (t1 Matrix-Super))
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (codomain t0) (codomain t1)))
  (let ((ar0 (coords t0))
	(ar1 (coords t1))
	(start (coord-start (domain t0)))
	(end (coord-end (domain t0))))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
      (v-v! ar0 ar1
		:start0 start :end0 end :vtype0 :row :on0 i
		:start1 start :end1 end :vtype1 :row :on1 i)))
  t0)

;;;-------------------------------------------------------

(defmethod sub-right! ((t0 Matrix-Super) (t1 Matrix-Super))
  (assert (eql (domain t0) (domain t1)))
  (assert (eql (codomain t0) (codomain t1)))
  (let ((ar0 (coords t0))
	(ar1 (coords t1))
	(start (coord-start (domain t0)))
	(end (coord-end (domain t0))))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0)))
      (v*x! ar1 -1.0d0 :start start :end  end :vtype :row :on i)
      (v+v! ar1 ar0
		:start0 start :end0 end :vtype0 :row :on0 i
		:start1 start :end1 end :vtype1 :row :on1 i)))
  t1)

;;;=======================================================
;;; General Mixtures:
;;;=======================================================
;;; ???? Are eq results handled properly?

(defmethod linear-mix :before ((a0 Number) (t0 Linear-Map)
			       (a1 Number) (t1 Linear-Map)
			       &key (result (make-element (home t0))))
  (let ((space (home t0)))
    (az:type-check Vector-Space space)
    (assert (element? t1 space))
    (assert (element? result space))))

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (t0 Tensor-Product)
		       (a1 Number) (t1 Tensor-Product)
		       &key (result (az:copy t0)))
  (let ((v0-c (v-codomain t0))
	(v1-c (v-codomain t1))
	(v0-d (v-domain t0))
	(v1-d (v-domain t1)))
    (cond
     ((typep result 'Tensor-Product) 
      (assert (eq (domain t0) (domain result)))
      (setf (v-codomain result)
	(linear-mix a0 v0-c a1 v1-c :result (v-codomain result)))
      (setf (v-domain   result)
	(linear-mix a0 v0-d a1 v1-d :result (v-domain result))))
     ((typep result 'Matrix)
      (with-borrowed-elements ((v-c (codomain t0))
			       (v-d (domain t0)))
	(linear-mix a0 v0-c a1 v1-c :result v-c)
	(linear-mix a0 v0-d a1 v1-d :result v-d)
	(let ((n (dimension (domain t0)))
	      (2d (coords result)))
	  (dotimes (i n)
	    (let ((alpha (aref v-c i)))
	      (v<-v*x! 2d v-d alpha
			  :end0 n :vtype0 :row :on0 i
			  :end1 n))))))
     (t
      (error "Can't handle this class of result."))))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (t0 Symmetric-Tensor-Product)
		       (a1 Number) (t1 Symmetric-Tensor-Product)
		       &key (result (make-instance 'Symmetric-Tensor-Product
				      :domain (domain t0))))
  (let ((v0 (v-domain t0))
	(v1 (v-domain t1)))
    (cond
     ((typep result 'Symmetric-Tensor-Product) 
      (setf (v-domain result)
	(linear-mix a0 v0 a1 v1 :result (v-domain result))))
     ((typep result 'Matrix)
      (let ((n (dimension (domain t0)))
	    (2d (coords result)))
	(with-borrowed-element (v (domain t0))
	  (linear-mix a0 v0 a1 v1 :result v)
	  (dotimes (i n)
	    (v<-v*x! 2d v (aref v i)
		     :end0 n :vtype0 :row :on0 i
		     :end1 n)))))
     (t
      (error "Can't handle this class of result."))))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (t0 Tensor-Product)
		       (a1 Number) (t1 Matrix-Super)
		       &key (result (az:copy t1)))
  (setf result (cond ((typep result 'Matrix-Super)
		      (scale a1 t1 :result result))
		     (t (error "Can't handle this class of result."))))
  (let* ((2d (coords result))
	 (v-cod (v-codomain t0))
	 (v-dom (v-domain t0))
	 (1d-dom (coords v-dom))
	 (s (coord-start v-dom))
	 (e (coord-end v-dom)) 
	 (n (- e s)))
    (az:for (i (coord-start v-cod) (coord-end v-cod))
	    (v+v*x! 2d 1d-dom (* a0 (coordinate v-cod i))
		    :start0 s :end0 e :vtype0 :row :on0 i
		    :start1 0 :end1 n)))
  result)

(defmethod linear-mix ((a0 Number) (t0 Matrix-Super)
		       (a1 Number) (t1 Tensor-Product)
		       &key (result (az:copy t0)))
  (linear-mix a1 t1 a0 t0 :result result)
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (t0 Symmetric-Tensor-Product)
		       (a1 Number) (t1 Matrix)
		       &key (result (az:copy t1)))
  (setf result (cond ((typep result 'Matrix) (scale a1 t1 :result result))
		     (t (error "Can't handle this class of result."))))
  (let* ((2d (coords result))
	 (v-dom (v-domain t0))
	 (1d-dom (coords v-dom))
	 (s (coord-start v-dom))
	 (e (coord-end v-dom)) 
	 (n (- e s)))
    (az:for (i s e)
	    (v+v*x! 2d 1d-dom (* a0 (coordinate v-dom i))
		    :start0 s :end0 e :vtype0 :row :on0 i
		    :end1 n)))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (t0 2x2-Direct-Sum-Map)
		       (a1 Number) (t1 2x2-Direct-Sum-Map)
		       &key (result (az:copy t0)))
  (cond ((typep result '2x2-Direct-Sum-Map) 
	 (assert (eq (codomain t0) (codomain result)))
	 (assert (eq (domain t0) (domain result))))
	(t (error "Can't handle this class of result.")))
  (linear-mix a0 (t00 t0) a1 (t00 t1) :result (t00 result))
  (linear-mix a0 (t01 t0) a1 (t01 t1) :result (t01 result))
  (linear-mix a0 (t10 t0) a1 (t10 t1) :result (t10 result))
  (linear-mix a0 (t11 t0) a1 (t11 t1) :result (t11 result))
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (t0 Symmetric-Tensor-Product)
		       (a1 Number) (t1 2x2-Direct-Sum-Map)
		       &key (result (az:copy t1)))
  (cond ((typep result '2x2-Direct-Sum-Map) 
	 (assert (eq (codomain t0) (codomain result)))
	 (assert (eq (domain t0) (domain result))))
	(t (error "Can't handle this class of result.")))
  (let ((space0 (space0 (domain t0)))
	(space1 (space1 (domain t0)))
	(v0 (x (v-domain t0)))
	(v1 (y (v-domain t0))))
    (az:with-borrowed-instance
	(tii 'Symmetric-Tensor-Product
	     :codomain space0 :domain space0 :v-domain v0)
      (linear-mix a0 tii a1 (t00 t1) :result (t00 result))
      (setf (slot-value tii 'domain) space1)
      (setf (slot-value tii 'v-domain) v1)
      (reinitialize-instance tii)
      (linear-mix a0 tii a1 (t11 t1) :result (t11 result)))
    (az:with-borrowed-instance
	(tij 'Tensor-Product
	     :codomain space0 :domain space1
	     :v-codomain v0 :v-domain v1)
      (linear-mix a0 tij a1 (t01 t1) :result (t01 result))
      (setf (slot-value tij 'domain) space0)
      (setf (slot-value tij 'v-domain) v0)
      (setf (slot-value tij 'codomain) space1)
      (setf (slot-value tij 'v-codomain) v1)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (t10 t1) :result (t10 result))))
  result)

(defmethod linear-mix ((a0 Number) (t0 2x2-Direct-Sum-Map)
		       (a1 Number) (t1 Symmetric-Tensor-Product)
		       &key (result (az:copy t0)))
  (linear-mix a1 t1 a0 t0 :result result)
  result)

;;;-------------------------------------------------------

(defmethod linear-mix ((a0 Number) (t0 Tensor-Product)
		       (a1 Number) (t1 2x2-Direct-Sum-Map)
		       &key (result (az:copy t1)))
  (cond ((typep result '2x2-Direct-Sum-Map) 
	 (assert (eq (codomain t0) (codomain result)))
	 (assert (eq (domain t0) (domain result))))
	(t (error "Can't handle this class of result.")))
  (let ((xsd (space0 (domain t0)))
	(ysd (space1 (domain t0)))
	(xd (x (v-domain t0)))
	(yd (y (v-domain t0)))
	(xsc (space0 (codomain t0)))
	(ysc (space1 (codomain t0)))
	(xc (x (v-codomain t0)))
	(yc (y (v-codomain t0))))

    (az:with-borrowed-instance (tij 'Tensor-Product
				    :codomain xsc :v-codomain xc
				    :domain xsd :v-domain xd) 
      (linear-mix a0 tij a1 (t00 t1) :result (t00 result))

      (setf (slot-value tij 'codomain) xsc)
      (setf (slot-value tij 'v-codomain) xc)
      (setf (slot-value tij 'domain) ysd)
      (setf (slot-value tij 'v-domain) yd)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (t01 t1) :result (t01 result))

      (setf (slot-value tij 'codomain) ysc)
      (setf (slot-value tij 'v-codomain) yc)
      (setf (slot-value tij 'domain) xsd)
      (setf (slot-value tij 'v-domain) xd)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (t10 t1) :result (t10 result))
      
      (setf (slot-value tij 'codomain) ysc)
      (setf (slot-value tij 'v-codomain) yc)
      (setf (slot-value tij 'domain) ysd)
      (setf (slot-value tij 'v-domain) yd)
      (reinitialize-instance tij)
      (linear-mix a0 tij a1 (t11 t1) :result (t11 result))))
  
  result)

(defmethod linear-mix ((a0 Number) (t0 2x2-Direct-Sum-Map)
		       (a1 Number) (t1 Tensor-Product)
		       &key (result (az:copy t0)))
  (linear-mix a1 t1 a0 t0 :result result)
  result)



