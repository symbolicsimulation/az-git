;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;============================================================

(in-package :Geometry)
	
;;;============================================================
;;; Clip x to the integer interval specified by [start,end),
;;; half open as in the CL sequence functions:

(defun iclip (x start end)
  (declare (type az:Int16 x start end))
  (min (max start x) (- end 1)))

;;;============================================================

