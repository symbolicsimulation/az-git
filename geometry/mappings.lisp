;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================
;;;	 Maps between Spaces
;;;=======================================================

(defclass Flat-Map (Point)
	  ((domain 
	    :type Flat-Space
	    :initarg :domain 	
	    :accessor domain)
	   (codomain 	
	    :type Flat-Space
	    :initarg :codomain 	
	    :accessor codomain
	    :documentation
   "Calling it {\tt codomain} rather than {\tt range} is a little pedantic,
but it's sometimes useful to distinguish the two concepts:
Suppose $T$ is a 4x3 {\sf Matrix} taking $R^3 \mapsto R^4$.
The {\tt codomain} of $T$ is $R^4$, but its {\tt range} is the 
3-dimensional subspace of $R^4$ resulting from $T(R^3)$.")
	   (properties 
	    :type List
	    :initarg :properties 
	    :accessor properties
	    :initform nil))

  (:documentation
   "A parent class for affine and linear maps between affine and linear
(vector) spaces."))

;;;-------------------------------------------------------
;;; Basic object protocol

;;;-------------------------------------------------------

(defgeneric map-adjective (t0)
  #-sbcl (declare (type Flat-Map t0)
		  (:returns (type (or Symbol String) adjective)))
  (:documentation
   "Produce a descriptive term to be included when the map is printed."))

(defmethod map-adjective ((t0 Flat-Map)) nil)

;;;-------------------------------------------------------

(defmethod print-object ((t0 Flat-Map) stream)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 stream)
	(format stream
		"~@[~a ~]~:(~a~) ~a->~a"
		(map-adjective t0)
		(class-name (class-of t0))
		(domain t0) (codomain t0)))
      (call-next-method))) 

;;;-------------------------------------------------------

(defmethod az:verify-copy-result? ((t0 Flat-Map) (result Flat-Map))
  (and (eq (domain t0) (domain result))
       (eq (codomain t0) (codomain result))
       (verify-map-copy-result? t0 result)))

(defmethod verify-map-copy-result? ((t0 Flat-Map) (result Flat-Map))
  (typep result (class-name (class-of t0))))

;;;-------------------------------------------------------

(defgeneric identity! (t0)
  #-sbcl (declare (type Flat-Map t0)
		  (:returns t0))
  (:documentation
   "Destructively modify t0 so that it is equivalent to the identity map
on its domain. t0 must be automorphic."))

;;;-------------------------------------------------------
;;; Properties/Predicates for Flat-Maps
;;;-------------------------------------------------------

(defvar *verify?* nil
  "Should properties be computed from scratch? If nil, the
property cache is trusted.")

(defmethod assert-properties ((t0 Flat-Map))
  (map nil #'(lambda (prop) (assert (funcall prop t0)))
       (properties t0)))

(defmethod delete-all-properties! ((t0 Flat-Map))
  (setf (properties t0) ()))

;;;-------------------------------------------------------

(defgeneric modifier? (t0)
  #-sbcl (declare (type Flat-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "<modifier?> is really a property of the representation rather
than the map itself. A modifier is a representation of an automorphic map 
for which there are efficient algorithms for <transform> (and <compose>)
that overwrite the Point being transformed. Most modifiers are instances
of some subclass of Modifier, but various compound mappings may also
be modifiers, if their components are modifiers."))

(defmethod modifier? ((t0 Flat-Map)) 
   "The default <nil> returned here means more precisely 
``don't know'' rather than that the Flat-Map is not a modifier  
Might adopt a two value return, like <subtypep>, in the future."
  nil)

;;;-------------------------------------------------------

(defgeneric linear? (t0)
  #-sbcl (declare (type Flat-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation "Is this a linear map?"))

(defmethod linear? ((t0 Flat-Map)) 
  (declare (type Flat-Map t0)
	   (:returns (type az:Boolean)))
  nil)

;;;-------------------------------------------------------

(defgeneric idempotent? (t0)
  #-sbcl (declare (type Flat-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "An automorphic map is idempotent if it is equal to its square. 
To be precise, $T$ is idempotent iff $T*T=T$."))

(defmethod idempotent? ((t0 Flat-Map)) 
  "The default <nil> returned here means more precisely 
``don't know'' rather than that the Flat-Map is not idempotent.  
Might adopt a two value return, like <subtypep>, in the future."
  nil)

;;;-------------------------------------------------------

(defgeneric identity? (t0)
  #-sbcl (declare (type Flat-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "Is t0 a representation of the identity map on its domain?"))

(defmethod identity? ((t0 Flat-Map)) 
  "The default <nil> returned here means more precisely 
``don't know'' rather than that the Flat-Map is not the identity.  
Might adopt a two value return, like <subtypep>, in the future."
  nil)

;;;-------------------------------------------------------
;;; Non-generic properties (implemented as ordinary function prediactes
;;;-------------------------------------------------------

(defun square? (t0)
  "The dimensions of a square map's domain and codomain are the same."
  (declare (type Flat-Map t0)
	   (:returns (type az:Boolean)))
  (= (dimension (domain t0)) (dimension (codomain t0))))

(defun automorphic? (t0) 
  "An automorphic map takes a space to itself. In other words,
the domain and codomain are identical."
  (declare (type Flat-Map t0)
	   (:returns (type az:Boolean)))
  (eq (domain t0) (codomain t0)))

(defun embedding? (t0)
  "The dimension of an embedding map's codomain is no less 
than the dimension of its domain."
  (declare (type Flat-Map t0)
	   (:returns (type az:Boolean)))
  (<= (dimension (domain t0)) (dimension (codomain t0))))

(defun strictly-embedding? (t0)
  "The dimension of an embedding map's codomain is strictly greater 
than the dimension of its domain."
  (declare (type Flat-Map t0)
	   (:returns (type az:Boolean)))
  (< (dimension (domain t0)) (dimension (codomain t0))))

(defun projecting? (t0)
  "The dimension of a projecting map's domain is  greater 
than the dimension of its codomain."
  (declare (type Flat-Map t0)
	   (:returns (type az:Boolean)))
  (>= (dimension (domain t0)) (dimension (codomain t0))))

(defun strictly-projecting? (t0)
  "The dimension of an strictly projecting map's codomain is strictly less 
than the dimension of its domain."
  (declare (type Flat-Map t0)
	   (:returns (type az:Boolean)))
  (> (dimension (domain t0)) (dimension (codomain t0))))

;;;=======================================================
;;; Indirect Maps
;;;=======================================================

(defclass Indirect-Map (Flat-Map)
	  ((source 
	    :type Linear-Map
	    :initarg :source 
	    :accessor source)))

;;;-------------------------------------------------------
;;; Indirect-Map's calculate their codomain
;;; and domain from their state, rather than using slots.

(defmethod print-object ((t0 Indirect-Map) stream)
  (az:printing-random-thing (t0 stream)
    (format stream "~@[~a ~]~:(~a~) ~a->~a"
	    (map-adjective t0)
	    (class-name (class-of t0))
	    (domain t0) (codomain t0)))) 

;;;=======================================================
;;;		 Linear Maps
;;;=======================================================

(defclass Linear-Map (Flat-Map) ()
  (:documentation
   "Linear Maps are Flat Maps between vector spaces that are {\it linear:} 
 
$T(a\v{x} + b\v{y}) = aT(\v{x}) + bT(\v{y}).$

The basic protocol for Linear-Map's requires 4 methods
implementing its algebra, <transform> and 2 algebraic operations:
<compose> and <linear-mix> (linear combination).  It also includes <scale>,
<add>, <sub>, <transpose>, and <pseudo-inverse>."))

;;;-------------------------------------------------------
;;; Basic Object Protocol
;;;-------------------------------------------------------

(defmethod print-object ((t0 Linear-Map) str)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 str)
	(format str "~@[~a ~]~:(~a~) ~a->~a"
		(map-adjective t0)
		(class-name (class-of t0))
		(domain t0) (codomain t0)))
      (call-next-method)))

;;;-------------------------------------------------------
;;; Relation to Linear Map spaces
;;;-------------------------------------------------------

(defmethod element? ((t0 Linear-Map) (space Linear-Map-Space))
  (and (typep t0 'Linear-Map)
       (eq (codomain t0) (codomain space))
       (eq (domain t0) (domain space))))

(defmethod home ((t0 Linear-Map))
  (the-linear-map-space (codomain t0) (domain t0)))


;;;-------------------------------------------------------
;;; Coordinate accessors
;;;-------------------------------------------------------

(defmethod col ((t0 Linear-Map) (i Integer)
		&key (result (make-element (codomain t0))))
  (assert (element? result (codomain t0)))
  (with-canonical-basis-element (vi (domain t0) i)
    (transform t0 vi :result result))
  result)

;;;-------------------------------------------------------

(defmethod row ((t0 Linear-Map) (i Integer)
		&key (result (make-element (dual-space (domain t0)))))
  (assert (element? result (dual-space (domain t0))))
  (with-canonical-basis-element (vi (dual-space (codomain t0)) i)
    (transform-transpose t0 vi :result result))
  result) 

;;;-------------------------------------------------------
;;; defaults:

(defmethod bands-start ((t0 Linear-Map))
  (- 1 (dimension (codomain t0))))

(defmethod bands-end ((t0 Linear-Map))
  (dimension (domain t0)))

;;;-------------------------------------------------------
;;; Properties
;;;-------------------------------------------------------
  
(defmethod linear? ((t0 Linear-Map)) t)

;;;------------------------------------------------------- 

(defgeneric unit-diagonal? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation 
   "A linear map is unit-diagonal if the corresponding matrix has ones
for diagonal elements. There is probably a geometric interpretation 
for this that offers more intuition for why it matters. 

Note that <unit-diagonal?> does not imply <diagonal?>!"))

(defmethod unit-diagonal? ((t0 Linear-Map)) 

  "The default <nil> returned here means more precisely 
``don't know'' rather than ``isn't''.

Might adopt a two value return, like <subtypep>, in the future."
     
  (declare (type Linear-Map t0)
	   (:returns nil))
  nil)

;;;------------------------------------------------------- 

(defmethod identity? ((t0 Linear-Map))
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (diagonal? t0) (unit-diagonal? t0)))

;;;------------------------------------------------------- 

(defgeneric symmetric? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "A square linear map is symmetric if it equals its transpose."))

(defmethod symmetric? ((t0 Linear-Map)) 

  "The default <nil> returned here means more precisely 
``don't know'' rather than ``isn't''.

Might adopt a two value return, like <subtypep>, in the future."
  
  (declare (type Linear-Map t0)
	   (:returns nil))
  nil) 

;;;------------------------------------------------------- 

(defgeneric orthogonal? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "A square linear map is orthogonal 
if its transpose is both its left and right inverse, ie., 
if $T*T^{\dagger} = I$ on the codomain of $T$
and $T^{\dagger}*T = I$ on the domain of $T$."))

(defmethod orthogonal? ((t0 Linear-Map)) 
  "The default <nil> returned here means more precisely 
``don't know'' rather than ``isn't''.

Might adopt a two value return, like <subtypep>, in the future."
  
  (declare (type Linear-Map t0)
	   (:returns nil))
  nil)

(defgeneric orthogonal-rows? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "A projecting (dimension reducing) linear map has orthogonal-rows
if its transpose is a right inverse, ie.,
if $T*T^{\dagger} = I$ on the codomain of $T$."))

(defmethod orthogonal-rows? ((t0 Linear-Map)) 
  "By default, a map has orthogonal-rows if it's orthogonal,
otherwise we can't tell."
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (orthogonal? t0))

(defgeneric orthogonal-columns? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "An embedding (dimension increasing) linear map has orthogonal-columns
if its transpose is a left inverse, ie.,
if $T^{\dagger}*T = I$ on the domain of $T$."))

(defmethod orthogonal-columns? ((t0 Linear-Map)) 
  "By default, a map has orthogonal-columns if it's orthogonal,
otherwise we can't tell."
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (orthogonal? t0))

;;;------------------------------------------------------- 

(defgeneric positive-definite? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "An automorphic map, $T$, is positive definite
if $<v,Tv> > 0$ for all non-zero $v$ in the domain of $T$."))

(defmethod positive-definite? ((t0 Linear-Map)) 
  "By default, we can't tell if a map is positive definite."
  (declare (type Linear-Map t0)
	   (:returns nil))
  nil)

(defgeneric positive-semi-definite? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "An automorphic map, $T$, is positive semidefinite
if $<v,Tv> >= 0$ for all non-zero $v$ in the domain of $T$."))

(defmethod positive-semi-definite? ((t0 Linear-Map))
  "By default, a map is positive semidefinite if it's positive definite,
otherwise we can't tell."
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (positive-definite? t0))

;;;------------------------------------------------------- 

(defgeneric negative-definite? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "An automorphic map, $T$, is negative definite
if $<v,Tv> < 0$ for all non-zero $v$ in the domain of $T$."))

(defmethod negative-definite? ((t0 Linear-Map)) 
  "By default, we can't tell if a map is negative definite."
  (declare (type Linear-Map t0)
	   (:returns nil))
  nil)

(defgeneric negative-semi-definite? (t0)
  #-sbcl (declare (type Linear-Map t0)
		  (:returns (type az:Boolean)))
  (:documentation
   "An automorphic map, $T$, is negative semidefinite
if $<v,Tv> <= 0$ for all non-zero $v$ in the domain of $T$."))

(defmethod negative-semi-definite? ((t0 Linear-Map))
  "By default, a map is negative semidefinite if it's negative definite,
otherwise we can't tell."
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (negative-definite? t0))


;;;-------------------------------------------------------
;;; nicknames for common band patterns:
;;;-------------------------------------------------------

(defparameter *bandedness-abbreviations*
    '(upper-triangular? lower-triangular? diagonal?
      upper-hessenberg? lower-hessenberg? tridiagonal?
      upper-bidiagonal? lower-bidiagonal?))

(defparameter *properties-of-linear-maps*
    '(symmetric? positive-definite? negative-definite?
      orthogonal? orthogonal-columns? orthogonal-rows?
      modifier? unit-diagonal?))

(defparameter *properties-of-linear-maps-preserved-by-products*
    '(orthogonal? orthogonal-columns? orthogonal-rows?
      modifier?))

(defparameter *properties-of-linear-maps-preserved-by-copy-to-matrix*
    '(orthogonal? orthogonal-columns? orthogonal-rows?
      symmetric? positive-definite? negative-definite? unit-diagonal?))

;;;-------------------------------------------------------

(defun upper-hessenberg? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (>= (bands-start t0) -1)
       (if *verify?* (verify-bands? t0 :start -1) t)))

(defun lower-hessenberg? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (<= (bands-end t0) 2)
       (if *verify?* (verify-bands? t0 :end 2) t)))

(defun hessenberg? (t0) 
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (or (lower-hessenberg? t0) (upper-hessenberg? t0)))

(defun upper-triangular? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (>= (bands-start t0) 0)
       (if *verify?* (verify-bands? t0 :start 0) t)))

(defun lower-triangular? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (<= (bands-end t0) 1)
       (if *verify?* (verify-bands? t0 :end 1) t)))

(defun triangular? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (or (upper-triangular? t0) (lower-triangular? t0)))

(defmethod diagonal? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (upper-triangular? t0) (lower-triangular? t0))
       (if *verify?* (verify-bands? t0 :start 0 :end 1) t)) 

(defun unit-upper-triangular? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (unit-diagonal? t0) (upper-triangular? t0)))

(defun unit-lower-triangular? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (unit-diagonal? t0) (lower-triangular? t0)))

(defun upper-bidiagonal? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (lower-hessenberg? t0) (upper-triangular? t0)))

(defun lower-bidiagonal? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (upper-hessenberg? t0) (lower-triangular? t0)))

(defun tridiagonal? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (lower-hessenberg? t0) (upper-hessenberg? t0)))

(defun symmetric-tridiagonal? (t0)
  (declare (type Linear-Map t0)
	   (:returns (type az:Boolean)))
  (and (symmetric? t0) (tridiagonal? t0)))

;;;-------------------------------------------------------

(defmethod map-adjective ((t0 Linear-Map))
  (let ((*verify?* nil)) ;; don't verify just for printing 
    (declare (special *verify?* ))
    (cond
      ((and (positive-definite? t0)
	    (diagonal? t0))
       "Positive-Definite-Diagonal")
      ((diagonal? t0) "Diagonal")
      ((and (positive-definite? t0)
	    (symmetric-tridiagonal? t0))
       "Positive-Definite-Tridiagonal")
      ((positive-definite? t0) "Positive-Definite")
      ((symmetric-tridiagonal? t0)
       "Symmetric-Tridiagonal")
      ((symmetric? t0) "Symmetric")
      ((upper-bidiagonal? t0) "Upper-Bidiagonal")
      ((lower-Bidiagonal? t0) "Lower-Bidiagonal")
      ((upper-triangular? t0) "Upper-Triangular")
      ((lower-triangular? t0) "Lower-Triangular")
      ((tridiagonal? t0) "Tridiagonal")
      ((upper-hessenberg? t0) "Upper-Hessenberg")
      ((lower-hessenberg? t0) "Lower-Hessenberg"))))

;;;=======================================================  

(defclass Symmetric (Linear-Map) ()
  (:documentation
   "This is a parent class for all linear map representations
that by their nature are guaranteed to always be symmetric."))

(defmethod shared-initialize :after ((t0 Symmetric) slot-names &rest initargs)
  (declare (ignore slot-names initargs))
  (when (and (slot-boundp t0 'codomain)
	     (slot-boundp t0 'domain))
    (assert (eq (codomain t0) (domain t0))))
  t) 

;;;-------------------------------------------------------

(defmethod symmetric? ((t0 Symmetric)) t)

;;;-------------------------------------------------------

(defmethod transpose ((t0 Symmetric) &key (result (az:copy t0)))
  (when (not (eq t0 result))
    (assert (typep result (class-name (class-of t0))))
    (assert (eql (domain t0) (domain result)))
    (az:copy t0 :result result))
  result)

(defmethod transpose! ((t0 Symmetric)) t0)

;;;=======================================================

(defclass Block-Projection (Linear-Map)
	  ((codomain 
	    :type Block-Subspace
	    :initarg :codomain
	    :accessor codomain)
	   (domain 
	    :type Vector-Space
	    :initarg :domain
	    :accessor domain))
  (:documentation
   "A {\sf Block-Projection} projects on the subspace spanned
by a the {\tt [start,end} canonical basis vectors."))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Block-Projection)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  (assert (proper-subspace? (codomain t0) (domain t0))))

;;;-------------------------------------------------------

(defmethod orthogonal-rows? ((t0 Block-Projection)) t)

;;;-------------------------------------------------------

(defun make-block-projection (domain codomain)
  (make-instance 'Block-Projection :domain domain :codomain codomain))

;;;=======================================================

(defclass Block-Embedding (Linear-Map)
	  ((codomain 
	    :type Vector-Space 
	    :initarg :codomain
	    :accessor codomain)
	   (domain 
	    :type Block-Subspace
	    :initarg :domain
	    :accessor domain))
  (:documentation
   "A {\sf Block-Embedding} maps the canonical basis vectors
of its domain to the {\tt start,end} canonical basis vectors
of its codomain. {\sf Block-Embedding} the pseudo-inverse
of {\sf Block-Projection}."))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((t0 Block-Embedding)
				     slot-names
				     &rest initargs)
  (declare (ignore slot-names initargs))
  ;; make sure the embedding is complete
  (assert (proper-subspace? (domain t0) (codomain t0))))

;;;-------------------------------------------------------

(defmethod orthogonal-columns? ((t0 Block-Embedding)) t)

;;;-------------------------------------------------------

(defun make-block-embedding (domain codomain)
  (make-instance 'Block-Embedding :domain domain :codomain codomain))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Block-Embedding)
		      &key (result (make-instance 'Block-Projection
				     :domain (codomain t0)
				     :codomain (domain t0))))
  (az:type-check Block-Projection result)
  (assert (eql (dual-space (domain t0)) (codomain result)))
  (assert (eql (dual-space (codomain t0)) (domain result)))
  result)

(defmethod transpose! ((t0 Block-Embedding))
  (let ((dom (domain t0))
	(cod (codomain t0)))
    (setf (domain t0) (dual-space cod))
    (setf (codomain t0) (dual-space dom)))
  (change-class t0 'Block-Projection))

;;;-------------------------------------------------------

(defmethod transpose ((t0 Block-Projection)
		      &key (result (make-instance 'Block-Embedding
				     :domain (dual-space (codomain t0))
				     :codomain (dual-space (domain t0)))))
  (az:type-check Block-Embedding result)
  (assert (eql (dual-space (domain t0)) (codomain result)))
  (assert (eql (dual-space (codomain t0)) (domain result)))
  result) 

(defmethod transpose! ((t0 Block-Projection))
  (let ((dom (domain t0))
	(cod (codomain t0)))
    (setf (domain t0) (dual-space cod))
    (setf (codomain t0) (dual-space dom)))
  (change-class t0 'Block-Embedding))

;;;============================================================

(defclass Affine-Map (Flat-Map)
	  ((dom-origin
	    :type (or Null Point)
	    :accessor dom-origin
	    :initarg :dom-origin)
	   (jacobian
	    :type (or Null Linear-Map)
	    :accessor jacobian
	    :initarg :jacobian
	    :initform nil)
	   (cod-origin
	    :type (or Null Point)
	    :accessor cod-origin
	    :initarg :cod-origin))
  
  (:documentation
   "This class provides a representation for arbitrary affine maps.
To apply it to a point in its domain, first subtract the dom-origin to
get a vector in the domain's translation space. Then apply the linear
jacobian to the vector to get a vector in the codomain's translation
space. Then add the cod-origin to get a point in the codomain.

If either dom-origin or cod-origin is nil, it is treated as though
it were the origin of the domain's or codomain's coordinate system.
The unnecessary additions and subtractions of zero can be skipped.

If the jacobian is nil, it is treated as though it were the 
natural identity between the translation space, which allows 
the coordinates to simply be copied without any arithmetic operations."))

;;;--------------------------------------------------------------
;;; Construction
;;;--------------------------------------------------------------

(defmethod initialize-instance :after ((map Affine-Map) &rest inits)
  (declare (ignore inits))
  (unless (slot-boundp map 'dom-origin)
    (setf (slot-value map 'dom-origin) (make-element (domain map))))
  (unless (slot-boundp map 'cod-origin)
    (setf (slot-value map 'cod-origin) (make-element (codomain map)))))
 
;;;============================================================

(defclass Diagonal-Affine-Map (Affine-Map)
	  ((dom-extent 	 
	    :type (or Null Point) 
	    :accessor dom-extent 	
	    :initarg :dom-extent) 
	   (cod-extent
	    :type (or Null Point) 	
	    :accessor cod-extent 	
	    :initarg :cod-extent))
  (:documentation 
   "This class provides a representation for affine maps whose
jacobian is diagonal. The jacobian slot is ignored. The jacobian is
represented by two vectors, <dom-extent>, in the translation space of
the the domain, and <cod-extent>, in the translation space of the
codomain.  The two vector slots correspond to a diagonal jacobian
matrix whose ith diagonal element would be (/ (aref cod-extent i)
(aref dom-extent i)).

The reason for having two slots rather than just one holding the
result of the divisions, is (a) for convenience and (b) so that it can
be used in discrete as well as continuous spaces. The convenience of
this representation is due to the fact that we frequently want to
specify affine maps as taking a given rect in the domain to a given
rect in the codomain. In that case, all we need to do is copy the
domain and codomain rects' origins and extents to the corresponding 
slots.

If either of these two vector slots is nil, then it is to be treated
as vector of all 1's, and unnecessary divides and multiplies can be
skipped."))

;;;--------------------------------------------------------------
;;; Construction
;;;--------------------------------------------------------------

(defmethod initialize-instance :after ((map Diagonal-Affine-Map) &rest inits)
  (declare (ignore inits))
  (assert (square? map))
  (unless (slot-boundp map 'dom-extent)
    (setf (slot-value map 'dom-extent) 
      (make-element (translation-space (domain map)))))
  (unless (slot-boundp map 'cod-extent)
    (setf (slot-value map 'cod-extent) 
      (make-element (translation-space (codomain map))))))

;;;============================================================
;;; Maps between discrete spaces
;;;============================================================

(defclass Int-Grid-Map (Diagonal-Affine-Map)
	  ((caches
	    :type (Simple-Array az:Int16-Vector (*))
	    :reader caches
	    :documentation
	    "This vector holds a set of coordinate caches that
provides a fast way to apply the map to points in the dom rect.
Suppose p is a point in the dom rect with ith coordinates xi. Suppose
the origin of the dom rec has ith coordinate oi. Then the ith coord
of (transform map p) is (aref (aref caches i) (- xi oi))."))

  (:documentation
   "This is a quasi-affine transformation. It maps a souce rect to a
destination rect by truncation.  The source top left corner is mapped
to the dest top left corner and the source bottom right corner is
mapped to approximately the destination bottom right corner by scaling
and truncation."))

;;;------------------------------------------------------------
;;; Construction
;;;------------------------------------------------------------

(defmethod initialize-instance :after ((map Int-Grid-Map) &rest inits)
  (declare (ignore inits))
  (setf (slot-value map 'caches) (make-array (dimension (domain map)))))

;;;------------------------------------------------------------

(defgeneric update-caches (map)
  #-sbcl (declare (type Int-Grid-Map map)
		  (:returns (type Int-Grid-Map)))
  (:documentation "Fill caches of the <map>."))

;;;------------------------------------------------------------

(defgeneric set-grid-map (map dom cod)
  #-sbcl (declare (type Int-Grid-Map map)
		  (type Rect dom cod)
		  (:returns (type Int-Grid-Map)))
  (:documentation
   "Adjust the coordinates and caches of the <map> 
    so that it maps <dom> to <cod>."))

(defmethod set-grid-map :before ((map Int-Grid-Map) dom cod)
  "Make sure <dom> and <cod> are rects in the right spaces."
  (az:declare-check (type Int-Grid-Map map)
		    (type Rect dom cod)
		    (:returns (type Int-Grid-Map)))
  (assert (eq (home dom) (domain map)))
  (assert (eq (home cod) (codomain map))))

(defmethod set-grid-map ((map Int-Grid-Map) dom cod)
  "Adjust the coordinates of the <map> so that it maps <dom> to <cod>."
  (declare (type Int-Grid-Map map)
	   (type Rect dom cod)
	   (:returns (type Int-Grid-Map)))
  (az:copy (origin dom) 
	   :result (or (dom-origin map)
		       (make-element (domain map))))
  (az:copy (extent dom) 
	   :result (or (dom-extent map)
		       (make-element (translation-space (domain map)))))
  (az:copy (origin cod) 
	   :result (or (cod-origin map)
		       (make-element (codomain map))))
  (az:copy (extent cod) 
	   :result (or (cod-extent map)
		       (make-element (translation-space (codomain map)))))
  (update-caches map)
  map)

;;;============================================================

(defclass Int-Grid-Flip (Int-Grid-Map) ()
  (:documentation
   "This is a quasi-affiine transformation. It maps a souce rect to a
destination rect by truncation, inverting the y coordinate.  The
source top left corner is mapped to the dest bottom left corner and
the source bottom right corner is mapped to approximately the
destination top right corner by scaling and truncation.

Int-Grid-Flips will only work correctly between 2d spaces,
or, to be more precise, only affect the first 2 coordinates."))

;;;============================================================

(defclass Int-Float-Flip (Int-Grid-Flip) ()
  (:documentation
   "This is a quasi-affiine transformation. It maps a source
screen (int16) rect to a destination (float) rect,
inverting the y coordinate.  The source top left corner is mapped to
the dest bottom left corner and the source bottom right corner is
mapped to approximately the destination top right corner by scaling
and truncation."))


 
