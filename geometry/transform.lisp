;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;============================================================
;;; Generic functions related to <transform>
;;;============================================================

(defgeneric transform (map object &key result)
  #-sbcl (declare (type Flat-Map map)
		  (type Geometric-Object object result)
		  (:returns result))
  (:documentation
   "Apply the <map> to <object> and return the answer in <result>,
unless the is something like a number, in which case, just return the
answer. If possible, methods should check that <object> is in the
domain of <map> and that <result> is in the codomain of <map>."))

(defgeneric transform! (t0 vd)
  #-sbcl (declare (type Flat-Map t0)
		  (type Geometric-Object vd)))

(defgeneric transform-to! (t0 vd result)
  #-sbcl (declare (type Flat-Map t0)
		  (type Geometric-Object vd result)
		  (:returns result)))

;;;-------------------------------------------------------

(defgeneric inverse-transform (map object &key result)
  #-sbcl (declare (type Flat-Map map)
		  (type Geometric-Object object result)
		  (:returns result))
  (:documentation
   "Apply the the inverse of <map> to <object> and return the answer
in <result>, unless the is something like a number, in which case just
return the answer. If possible, methods should check that <object> is
in the codomain of <map> and that <result> is in the domain of <map>.

The reason this generic function is defined is that is sometimes
possible to efficiently apply the inverse of a map without actually
creating an object to represent the inverse. And, even when this isn't
the case, the map may cache its inverse, which means that the user
doesn't have to explicitly create and keep a pointer to it."))

;;;-------------------------------------------------------

(defgeneric transform-transpose (map object &key result)
  #-sbcl (declare (type Linear-Map map)
		  (type Geometric-Object object result)
		  (:returns result))
  (:documentation
   "Apply the transpose of <map> to <object> and return the answer
in <result>, unless the is something like a number, in which case,
just return the answer. If possible, methods should check that <object>
is in the codomain of <map> and that <result> is in the domain of <map>."))

(defgeneric transform-transpose-to! (t0 vd result)
  #-sbcl (declare (type Flat-Map t0)
		  (type Geometric-Object vd result)
		  (:returns result)))

(defgeneric transform-transpose! (t0 vd)
  #-sbcl (declare (type Linear-Map t0)
		  (type Geometric-Object vd)))

;;;=======================================================
;;; Default methods
;;;=======================================================

(defmethod transform ((t0 Flat-Map) (vd Point)
		      &key (result (make-element (codomain t0))))
  
  (assert (element? vd (domain t0)))
  (assert (element? result (codomain t0)))
  (if (eq vd result)
      ;; then try to do it in place
      (transform! t0 vd)
      ;; else do it the usual way
      (transform-to! t0 vd result))
  result) 

;;;-------------------------------------------------------

(defmethod transform! ((t0 Flat-Map) (vd Point))
  "an inefficient default method for non-destructive Maps"
  (assert (element? vd (domain t0)))
  (assert (element? vd (codomain t0)))

  (with-borrowed-element (result (codomain t0))
    (transform-to! t0 vd result)
    (az:copy result :result vd))
  vd)

(defmethod transform-to! ((t0 Flat-Map) (vd Block-Point) (result Point))
  "an inefficient default method for transforming Block-Subspaces"
  (with-borrowed-element (v (domain t0))
    (transform-to! t0 (embed vd (domain t0) :result v) result))
  result)
 
;;;-------------------------------------------------------
;;; Methods for linear maps
;;;-------------------------------------------------------

(defmethod transform-transpose ((t0 Linear-Map) (vd Point)
				&key (result
				      (make-element (dual-space (domain t0)))))
  (assert (element? vd (dual-space (codomain t0))))
  (assert (element? result (dual-space (domain t0))))
  (cond ((eq vd result) (transform-transpose! t0 vd))
	((symmetric? t0) (transform-to! t0 vd result))
	(t (transform-transpose-to! t0 vd result)))
  result)

(defmethod transform-transpose! ((t0 Linear-Map) (vd Point))

  (assert (and (element? vd (dual-space (codomain t0)))
	       (element? vd (dual-space (domain t0)))))
  (with-borrowed-element (result (dual-space (domain t0)))
    (transform-transpose-to! t0 vd result)
    (az:copy result :result vd))
  vd)

;;;-------------------------------------------------------
;;; symmetric maps
;;;-------------------------------------------------------  

(defmethod transform-transpose! ((t0 Symmetric) (vd Point))
  (transform! t0 (dual-vector vd :result vd))) 

(defmethod transform-transpose-to! ((t0 Symmetric) (vd Point) (result Point))
  (transform t0 (dual-vector vd :result vd)
	     :result (dual-vector result :result result))
  (dual-vector result :result result)
  result) 

;;;-------------------------------------------------------
;;; Matrices
;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Matrix-Super) (vd Point) (vr Point))
  (assert (subspace? (home vd) (domain t0)))
  (assert (eq (home vr) (codomain t0)))

  (let* ((2d (coords t0))
	 (1dd (coords vd))
	 (s (coord-start vd))
	 (e (coord-end vd))
	 (n (- e s)))
    (az:for (i (coord-start (codomain t0)) (coord-end (codomain t0))) 
	    (setf (coordinate vr i)
	      (v.v 2d 1dd
		   :start0 s :end0 e :vtype0 :row :on0 i
		   :end1 n))))
  vr)

(defmethod transform-transpose-to! ((t0 Matrix-Super) (vd Point) (vr Point))
  ;; a temporary restriction:
  (assert (subspace? (home vd) (dual-space (codomain t0))))
  (assert (eq (home vr) (dual-space (domain t0))))
  
  (let* ((2d (coords t0))
	 (1dd (coords vd))
	 (s (coord-start vd))
	 (e (coord-end vd))
	 (n (- e s)))
    (az:for (i (coord-start (domain t0)) (coord-end (domain t0))) 
	    (setf (coordinate vr i)
	      (v.v 2d 1dd
		   :start0 s :end0 e :vtype0 :col :on0 i
		   :end1 n))))
  vr)

(defmethod transform-to! ((t0 Matrix) (vd Point) (vr Point))
  (let ((2d (coords t0))
	(1dd (coords vd))
	(1dr (coords vr))
	(m (dimension (codomain t0)))
	(n (dimension (domain t0))))
    (cond
     ((upper-triangular? t0)
      (dotimes (i (min m n))
	(setf (aref 1dr i)
	  (v.v 2d 1dd
	       :start0 i :end0 n :vtype0 :row :on0 i
	       :start1 i :end1 n)))
      (when (> m n)
	(v<-x! 1dr 0.0d0 :start n :end m)))
     ((lower-triangular? t0)
      (dotimes (i (min m n))
	(let ((i+1 (+ i 1)))
	  (setf (aref 1dr i)
	    (v.v 2d 1dd
		 :end0 i+1 :vtype0 :row :on0 i
		 :end1 i+1))))
      (az:for (i n m)
	      (setf (aref 1dr i) (v.v 2d 1dd
				      :end0 n :vtype0 :row :on0 i
				      :end1 n))))
     (t
      (dotimes (i (dimension (codomain t0)))
	(setf (aref 1dr i)
	  (v.v 2d 1dd
	       :end0 n :vtype0 :row :on0 i
	       :end1 n))))))
  vr)

;;;-------------------------------------------------------
;;; Inverses of triangular matrices.
;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Inverse-Upper-Triangular-Matrix)
			  (vd Point) (vr Point))
  "Back substitution."
  (assert (not (diagonal-zeros? (coords (source t0)))))
  (let* ((t- (source t0))
	 (2d (coords t-))
	 (m (dimension (codomain t0)))
	 (n (dimension (domain t0)))
	 (end (min m n))
	 (e-1 (- end 1))
	 (1dr (coords vr))
	 (1dd (coords vd)))
    (setf (aref 1dr e-1) (/ (aref 1dd e-1) (aref 2d e-1 e-1)))
    (loop for i downfrom  (- e-1 1) to 0
	for i+1 = (+ i 1)
	do (setf (aref 1dr i)
	     (/ (- (aref 1dd i)
		   (v.v 2d 1dr
			:start0 i+1 :end0 end :vtype0 :row :on0 i
			:start1 i+1 :end1 end))
		(aref 2d i i))))
    (when (< end m) (v<-x! 1dr 0.0d0 :start end :end m)))
  vr) 

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Inverse-Upper-Triangular-Matrix)
				    (vd Point) (vr Point))
  "Transposed Forward elimination."
  (assert (not (diagonal-zeros? (coords (source t0)))))
  (let* ((t- (source t0))
	 (2d (coords t-))
	 (1dr (coords vr))
	 (1dd (coords vd))
	 (m (dimension (dual-space (codomain t0))))
	 (n (dimension (dual-space (domain t0)))))
    (setf (aref 1dr 0) (/ (aref 1dd 0) (aref 2d 0 0)))
    (az:for (i 1 (min m n))
	    (setf (aref 1dr i)
	      (/ (- (aref 1dd i) (v.v 2d 1dr
				      :end0 i :vtype0 :col :on0 i
				      :end1 i))
		 (aref 2d i i))))
    (when (> n m) (v<-x! 1dr 0.0d0 :start m :end n)))
  vr)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Inverse-Lower-Triangular-Matrix)
			  (vd Point) (vr Point))
  "Forward elimination."
  (assert (not (diagonal-zeros? (coords (source t0)))))
  (let* ((t- (source t0))
	 (2d (coords t-))
	 (m (dimension (codomain t0)))
	 (n (dimension (domain t0)))
	 (1dr (coords vr))
	 (1dd (coords vd)))
    (setf (aref 1dr 0) (/  (aref 1dd 0) (aref 2d 0  0)))
    (az:for (i 1 (min m n))
	    (setf (aref 1dr i)
	      (/ (- (aref 1dd i) (v.v 2d 1dr
				      :end0 i :vtype0 :row :on0 i
				      :end1 i))
		 (aref 2d i i))))
    (when (> m n) (v<-x! 1dr 0.0d0 :start n :end m)))
  
  vr)

;;;-------------------------------------------------------

(defmethod transform-transpose-to! ((t0 Inverse-Lower-Triangular-Matrix)
				    (vd Point) (vr Point))
  "Transposed Back substitution."
  (assert (not (diagonal-zeros? (coords (source t0)))))
  (let* ((t- (source t0))
	 (2d (coords t-))
	 (m (dimension (dual-space (codomain t0))))
	 (n (dimension (dual-space (domain t0))))
	 (end (min m n))
	 (e-1 (- end 1))
	 (1dr (coords vr))
	 (1dd (coords vd)))
    (setf (aref 1dr e-1) (/ (aref 1dd e-1) (aref 2d e-1 e-1)))
    (loop for i downfrom  (- e-1 1) to 0
	for i+1 = (+ i 1)
	do (setf (aref 1dr i)
	     (/ (- (aref 1dd i)
		   (v.v 2d 1dr
			:start0 i+1 :end0 end :vtype0 :col :on0 i
			:start1 i+1 :end1 end))
		(aref 2d i i))))
    (when (< end n) (v<-x! 1dr 0.0d0 :start end :end n)))
  vr) 

;;;-------------------------------------------------------
;;; Modifiers. 
;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Modifier) (vd Point) (result Point))
  (transform! t0 (az:copy vd :result result))
  result)

(defmethod transform-transpose-to! ((t0 Modifier) (vd Point) (result Point))
  (transform-transpose! t0 (az:copy vd :result result))
  result)

;;;-------------------------------------------------------

(defmethod transform! :before ((t0 Modifier) (vd Point))
  (assert (element? vd (domain t0)))
  (assert (element? vd (codomain t0))))

(defmethod transform-transpose! :before ((t0 Modifier) (vd Point))
  (assert (element? vd (dual-space (domain t0))))
  (assert (element? vd (dual-space (codomain t0)))))

;;;-------------------------------------------------------

(defmethod transform! ((t0 Identity-Map) (vd Point))
  (assert (element? vd (domain t0)))
  vd) 

;;;-------------------------------------------------------

(defmethod transform! ((t0 Gauss) (vd Point))
  (linear-mix 1.0d0 vd (- (coordinate vd (k t0))) (vec t0) :result vd))

(defmethod transform-transpose! ((t0 Gauss) (vd Point))
  (decf (coordinate vd (k t0)) (inner-product (vec t0) vd))
  vd) 

;;;-------------------------------------------------------

(defmethod transform! ((t0 Transpose-Gauss) (vd Point))
  (decf (coordinate vd (k t0)) (inner-product (vec t0) vd)) 
  vd) 

(defmethod transform-transpose! ((t0 Transpose-Gauss) (vd Point))
  (linear-mix 1.0d0 vd (- (coordinate vd (k t0))) (vec t0) :result vd)) 

;;;-------------------------------------------------------

(defmethod transform! ((t0 Householder) (vd Point))
  (linear-mix 1.0d0 vd (* (-2/norm2 t0) (inner-product vd (vec t0))) (vec t0)
	      :result vd)) 

(defmethod transform-transpose! ((t0 Householder) (vd Point))
  (transform! t0 (dual-vector vd :result vd))
  (dual-vector vd :result vd))

;;;-------------------------------------------------------

(defmethod transform! ((t0 Givens) (vd Point))
  
  (let* ((i0 (i0 t0))
	 (i1 (i1 t0))
	 (vd.i0 (coordinate vd i0))
	 (vd.i1 (coordinate vd i1))
	 (c (c t0))
	 (s (s t0)))
    (setf (coordinate vd i0) (+ (* c vd.i0) (* s vd.i1)))
    (setf (coordinate vd i1) (- (* c vd.i1) (* s vd.i0))))
  vd)

(defmethod transform-transpose! ((t0 Givens) (vd Point))

  (dual-vector vd :result vd)
  (let* ((i0 (i0 t0))
	 (i1 (i1 t0))
	 (vd.i0 (coordinate vd i0))
	 (vd.i1 (coordinate vd i1))
	 (c (c t0))
	 (s (- (s t0))))
    (setf (coordinate vd i0) (+ (* c vd.i0) (* s vd.i1)))
    (setf (coordinate vd i1) (- (* c vd.i1) (* s vd.i0))))
  (dual-vector vd :result vd))

;;;-------------------------------------------------------

(defmethod transform! ((t0 Diagonal-Vector) (vd Point))
  (elementwise-product! (vec t0) vd))

;;;-------------------------------------------------------

(defmethod transform! ((t0 Pivot) (vd Point))
  (rotatef (coordinate vd (i0 t0)) (coordinate vd (i1 t0)))
  vd)

;;;-------------------------------------------------------

(defmethod transform! ((t0 1d-Annihilator) (vd Point))
  (linear-mix 1.0d0 vd (* (-1/norm2 t0) (inner-product (vec t0) vd)) (vec t0)
	      :result vd))
 
;;;-------------------------------------------------------
;;; Products and other compound maps
;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Product-Map) (vd Point) (result Point))
  
  (let ((v0 vd) v1 (vs0 (domain t0)) vs1)
    (map nil #'(lambda (f)
		 (setf vs1 (codomain f))
		 (setf v1 (borrow-element vs1))
		 (transform f v0 :result v1)
		 (unless (eql vd v0) (return-element v0 vs0))
		 (setf v0 v1)
		 (setf vs0 vs1))
	 (reversed-factors t0))
    (az:copy v1 :result result)
    (unless (eql vd v0) (return-element v0 vs0)))
  result)

(defmethod transform-transpose-to! ((t0 Product-Map) (vd Point)
				    (result Point))
  (let ((v0 vd)
	v1
	(vs0 (dual-space (codomain t0))) vs1)
    (map nil
      #'(lambda (f)
	  (setf vs1 (dual-space (domain f)))
	  (setf v1 (borrow-element vs1))
	  (transform-transpose f v0 :result v1)
	  (unless (eql vd v0) (return-element v0 vs0))
	  (setf v0 v1)
	  (setf vs0 vs1))
      (factors t0))
    (az:copy v1 :result result)
    (unless (eql vd v0) (return-element v0 vs0)))
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Symmetric-Outer-Product) (vd Point)
			  (result Point))
  (let ((lf (left-factor t0)))
    (with-borrowed-element (v (domain lf))
      (transform lf (transform-transpose lf vd :result v)
		 :result result)))
  result)

(defmethod transform-transpose-to! ((t0 Symmetric-Outer-Product) (vd Point)
				    (result Point))
  (let ((lf (left-factor t0)))
    (with-borrowed-element (v (domain lf))
      (transform lf (transform-transpose lf vd :result v)
		 :result result)))
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Symmetric-Inner-Product) (vd Point)
			  (result Point))
  (let ((rf (right-factor t0)))
    (with-borrowed-element (v (codomain rf))
      (transform-transpose rf (transform rf vd :result v) :result result)))
  result)

(defmethod transform-transpose-to! ((t0 Symmetric-Inner-Product) (vd Point)
				    (result Point))
  (let ((rf (right-factor t0)))
    (with-borrowed-element (v (domain rf))
      (transform-transpose rf (transform rf vd :result v) :result result)))
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Orthogonal-Similarity-Product) (vd Point)
			  (result Point))
  (with-borrowed-elements ((v0 (codomain (right-factor t0)))
			   (v1 (domain (right-factor t0))))
    (transform-transpose
     (right-factor t0)
     (transform (middle-factor t0) (transform (right-factor t0) vd :result v0)
		:result v1)
     :result result))
  result)

(defmethod transform-transpose-to! ((t0 Orthogonal-Similarity-Product)
				    (vd Point) (result Point))
  (with-borrowed-elements ((v0 (codomain (right-factor t0)))
			   (v1 (codomain (right-factor t0))))
    (transform-transpose
     (right-factor t0) 
     (transform-transpose
      (middle-factor t0) (transform (right-factor t0) vd :result v0)
      :result v1)
     :result result))
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Tensor-Product) (vd Point) (result Point))
  (scale (inner-product (v-domain t0) vd) (v-codomain t0) :result result)
  result)

(defmethod transform-transpose-to! ((t0 Tensor-Product) (vd Point)
				    (result Point))
  (scale (inner-product (v-codomain t0) vd) (v-domain t0) :result result)
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Symmetric-Tensor-Product) (vd Point)
			  (result Point))
  (scale (inner-product (v-domain t0) vd) (v-domain t0) :result result)
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 2x2-Direct-Sum-Map) (vd Direct-Sum-Point)
			  (result Direct-Sum-Point))
  (with-borrowed-element (v0 (space0 (codomain t0)))
    (setf (x result) (add (transform (t00 t0) (x vd) :result v0)
			   (transform (t01 t0) (y vd) :result (x result))
			   :result (x result))))
  (with-borrowed-element (v1 (space1 (codomain t0)))
    (setf (y result) (add (transform (t10 t0) (x vd) :result v1)
			  (transform (t11 t0) (y vd) :result (y result))
			  :result (y result))))
  result) 

;;;-------------------------------------------------------
;;; Projections and embeddings
;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Block-Projection) (vd Point) (result Point))
  (project vd (codomain t0) :result result)
  result) 

(defmethod transform-transpose-to! ((t0 Block-Projection) (vd Point)
				    (result Point))
  "same code as (transform-to! Block-Embedding Point Point)"
  (embed (dual-vector vd :result vd)
	 (dual-space (domain t0))
	 :result (dual-vector result :result result))
  (dual-vector result :result result)
  result)

;;;-------------------------------------------------------

(defmethod transform-to! ((t0 Block-Embedding) (vd Point) (result Point))
  (embed vd (codomain t0) :result result)
  result)

(defmethod transform-transpose-to! ((t0 Block-Embedding) (vd Point)
				    (result Point))
  "same code as (transform-to! Block-Projection Point Point)"
  (project (dual-vector vd :result vd)
	   (dual-space (domain t0))
	   :result (dual-vector result :result result))
  (dual-vector result :result result)
  result)

;;;------------------------------------------------------------
;;; Affine Maps
;;;------------------------------------------------------------

(defmethod transform ((map Diagonal-Affine-Map) (p Point)
		       &key (result (make-element (codomain map))))
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((od (coords (dom-origin map)))
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map)))
	 (ec (coords (cod-extent map)))
	 (pc (coords p))
	 (rc (coords result)))
    (dotimes (i (dimension (domain map)))
      (setf (aref rc i)
	(+ (aref oc i) 
	   (/ (* (- (aref pc i) (aref od i)) 
		 (aref ec i)) 
	      (aref ed i))))))
  result)

(defmethod transform ((map Diagonal-Affine-Map) (r Rect)
		       &key (result (make-rect (codomain map))))
  (declare (type Rect result)
	   (:returns (type Rect result)))
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (or (coords (origin r)))
	 (er (coords (extent r)))
	 (oresult (coords (origin result)))
	 (eresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((xdom (aref od i))
	     (ed (aref ed i))
	     (ec (aref ec i))
	     (xmin (aref or i))
	     (xmax (+ xmin (aref er i))))
	(setf xmin (/ (* (- xmin xdom) ec) ed))
	(setf xmax (/ (* (- xmax xdom) ec) ed))
	(setf (aref oresult i) (+ xmin (aref oc i)))
	(setf (aref eresult i) (- xmax xmin)))))
  result)

;;;------------------------------------------------------------

(defmethod inverse-transform ((map Diagonal-Affine-Map) (p Point)
			       &key (result (make-element (domain map))))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((xdom (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (xmin (coords p))
	 (xr (coords result)))
    (dotimes (i (dimension (domain map)))
      (setf (aref xr i) 
	(+ (aref xdom i) 
	   (/ (* (- (aref xmin i) (aref oc i)) 
		 (aref ed i))
	      (aref ec i))))))
  result)

(defmethod inverse-transform ((map Diagonal-Affine-Map) (r Rect)
			       &key (result (make-rect (domain map))))
  
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  
  (declare (type Rect result)
	   (:returns (type Rect result)))
  
  (let* ((xdom (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (xr (coords (origin r))) 
	 (wr (coords (extent r)))
	 (xresult (coords (origin result))) 
	 (wresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((xri (aref xr i))
	     (edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (xmin (/ (* edi (- xri oci)) 
		      eci))
	     (xmax (/ (* edi (- (+ xri (aref wr i)) oci)) 
		      eci)))
	(setf (aref xresult i) (+ xmin (aref xdom i)))
	(setf (aref wresult i) (- xmax xmin)))))
    
  result)

;;;------------------------------------------------------------
;;; Discrete Affine Maps
;;;------------------------------------------------------------

(defmethod update-caches ((map Int-Grid-Map))
  (declare (type Int-Grid-Map map))
  (let ((ed (coords (dom-extent map)))
	(oc (coords (cod-origin map)))
	(ec (coords (cod-extent map))))
    (declare (type az:Int16-Vector ed ec))
    (dotimes (i (dimension (domain map)))
      (declare (type az:Card16 i))
      (let* ((edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (cache (az:make-int16-vector edi)))
	(declare (type az:Int16 edi oci eci)
		 (type az:Int16-Vector cache))
	(setf (aref (caches map) i) cache)
	(dotimes (xi edi) 
	  (declare (type az:Int16 xi))
	  (setf (aref cache xi) (+ oci (floor (* xi eci) edi)))))))
  map)

(defmethod transform ((map Int-Grid-Map) (p Point)
		       &key (result (make-element (codomain map))))
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((od (coords (dom-origin map)))
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map)))
	 (ec (coords (cod-extent map)))
	 (pc (coords p))
	 (rc (coords result)))
    (dotimes (i (dimension (domain map)))
      (setf (aref rc i) (+ (aref oc i) 
			   (floor (* (aref ec i) (- (aref pc i) (aref od i))) 
				  (aref ed i))))))
  result)

(defmethod inverse-transform ((map Int-Grid-Map) (p Point)
			       &key (result (make-element (domain map))))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (pc (coords p))
	 (rc (coords result)))
    (dotimes (i (dimension (domain map)))
      (setf (aref rc i) (+ (aref od i) 
			   (ceiling (* (aref ed i) (- (aref pc i) (aref oc i)))
				  (aref ec i))))))
  result)
 
(defmethod transform ((map Int-Grid-Map) (r Rect)
		       &key (result (make-rect (codomain map))))
  (declare (type Rect result)
	   (:returns (type Rect result)))
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (or (coords (origin r)))
	 (er (coords (extent r)))
	 (oresult (coords (origin result)))
	 (eresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((odi (aref od i))
	     (edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (rmin (aref or i))
	     (rmax (+ rmin (aref er i))))
	(setf rmin (floor (* (- rmin odi) eci) edi))
	(setf rmax (floor (* (- rmax odi) eci) edi))
	(setf (aref oresult i) (+ rmin oci))
	(setf (aref eresult i) (- rmax rmin)))))
  result)

(defmethod inverse-transform ((map Int-Grid-Map) (r Rect)
			       &key (result (make-rect (domain map))))
  
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  
  (declare (type Rect result)
	   (:returns (type Rect result)))
  
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (or (coords (origin r))) 
	 (er (coords (extent r)))
	 (oresult (coords (origin result))) 
	 (eresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((odi (aref od i))
	     (edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (rmin (aref or i))
	     (rmax (+ rmin (aref er i))))
	(setf rmin (ceiling (* (- rmin oci) edi) eci))
	(setf rmax (ceiling (* (- rmax oci) edi) eci))
	(setf (aref oresult i) (+ rmin odi))
	(setf (aref eresult i) (- rmax rmin)))))
    
  result)
 
;;;------------------------------------------------------------
;;; Discrete Y flips
;;;------------------------------------------------------------

(defmethod update-caches ((map Int-Grid-Flip))
  (declare (type Int-Grid-Flip map))
  (let ((ed (coords (dom-extent map)))
	(oc (coords (cod-origin map)))
	(ec (coords (cod-extent map))))
    (declare (type az:Int16-Vector ed ec))
    (dotimes (i (dimension (domain map)))
      (declare (type az:Card16 i))
      (let* ((edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (cache (az:make-int16-vector edi)))
	(declare (type az:Int16 edi oci eci)
		 (type az:Int16-Vector cache))
	(setf (aref (caches map) i) cache)
	(if (= i 1)
	  ;; y cache is flipped
	  (dotimes (xi edi) 
	    (setf (aref cache xi) (+ oci (floor (* eci (- edi 1 xi)) edi))))
	  ;; else
	  (dotimes (xi edi) 
	    (declare (type az:Int16 xi))
	    (setf (aref cache xi) (+ oci (floor (* eci xi) edi))))))))
  map)

(defmethod transform ((map Int-Grid-Flip) (p Point)
		       &key (result (make-element (codomain map))))
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((od (coords (dom-origin map)))
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map)))
	 (ec (coords (cod-extent map)))
	 (pc (coords p))
	 (rc (coords result)))
    (dotimes (i (dimension (domain map)))
      (let ((x (- (aref pc i) (aref od i)))
	    (edi (aref ed i)))
	;; y coord is flipped
	(when (= i 1) (setf x (- edi 1 x)))
	(setf (aref rc i) (+ (aref oc i) (floor (* (aref ec i) x) edi))))))
  result)

(defmethod inverse-transform ((map Int-Grid-Flip) (p Point)
			       &key (result (make-element (domain map))))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (pc (coords p))
	 (rc (coords result)))
    (dotimes (i (dimension (domain map)))
      (let* ((edi (aref ed i))
	     (x (ceiling (* edi (- (aref pc i) (aref oc i))) (aref ec i))))
	;; y coord is flipped
	(when (= i 1) (setf x (- edi x)))
	(setf (aref rc i) (+ (aref od i) x)))))
  result)
 
(defmethod transform ((map Int-Grid-Flip) (r Rect)
		       &key (result (make-rect (codomain map))))
  (declare (type Rect result)
	   (:returns (type Rect result)))
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (or (coords (origin r)))
	 (er (coords (extent r)))
	 (oresult (coords (origin result)))
	 (eresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((odi (aref od i))
	     (edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (ori (aref or i))
	     (eri (aref er i))
	     ;; y coord is flipped
	     (rmin (if (= i 1) (- edi (- (+ ori eri) odi)) ori))
	     (rmax (+ rmin eri)))
	(setf rmin (floor (* (- rmin odi) eci) edi))
	(setf rmax (floor (* (- rmax odi) eci) edi))
	(setf (aref oresult i) (+ rmin oci))
	(setf (aref eresult i) (- rmax rmin)))))
  result)

(defmethod inverse-transform ((map Int-Grid-Flip) (r Rect)
			       &key (result (make-rect (domain map))))
  
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  
  (declare (type Rect result)
	   (:returns (type Rect result)))
  
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (or (coords (origin r))) 
	 (er (coords (extent r)))
	 (oresult (coords (origin result))) 
	 (eresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((odi (aref od i))
	     (edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (rmin (aref or i))
	     (rmax (+ rmin (aref er i))))
	(setf rmin (ceiling (* (- rmin oci) edi) eci))
	(setf rmax (ceiling (* (- rmax oci) edi) eci))
	;; y coord is flipped
	(setf (aref oresult i) (+ odi (if (= i 1) (- edi rmax) rmin)))
	(setf (aref eresult i) (- rmax rmin)))))
    
  result)
 
;;;------------------------------------------------------------
;;; Int -> Float Maps
;;;------------------------------------------------------------

(defmethod update-caches ((map Int-Float-Flip))
  "Float maps don't cache."
  (declare (type Int-Float-Flip map))
  map)

(defmethod transform ((map Int-Float-Flip) (p Point)
		       &key (result (make-element (codomain map))))
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((od (coords (dom-origin map)))
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map)))
	 (ec (coords (cod-extent map)))
	 (pc (coords p))
	 (rc (coords result)))
    (dotimes (i (dimension (domain map)))
      (let ((x (- (aref pc i) (aref od i)))
	    (edi (aref ed i)))
	;; y coord is flipped
	(when (= i 1) (setf x (- edi 1 x)))
	(setf (aref rc i) (+ (aref oc i) (/ (* (aref ec i) x) edi))))))
  result)

(defmethod inverse-transform ((map Int-Float-Flip) (p Point)
			       &key (result (make-element (domain map))))
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  (declare (type Point result)
	   (:returns (type Point result)))
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (pc (coords p))
	 (rc (coords result)))
    (dotimes (i (dimension (domain map)))
      (let* ((edi (aref ed i))
	     (x (ceiling (* edi (- (aref pc i) (aref oc i))) (aref ec i))))
	;; y coord is flipped
	(when (= i 1) (setf x (- edi x)))
	(setf (aref rc i) (+ (aref od i) x)))))
  result)
 
(defmethod transform ((map Int-Float-Flip) (r Rect)
		       &key (result (make-rect (codomain map))))
  (declare (type Rect result)
	   (:returns (type Rect result)))
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (or (coords (origin r)))
	 (er (coords (extent r)))
	 (oresult (coords (origin result)))
	 (eresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((odi (aref od i))
	     (edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (ori (aref or i))
	     (eri (aref er i))
	     ;; y coord is flipped
	     (rmin (if (= i 1) (- edi (- (+ ori eri) odi)) ori))
	     (rmax (+ rmin eri)))
	(setf rmin (/ (* (- rmin odi) eci) edi))
	(setf rmax (/ (* (- rmax odi) eci) edi))
	(setf (aref oresult i) (+ rmin oci))
	(setf (aref eresult i) (- rmax rmin)))))
  result)

(defmethod inverse-transform ((map Int-Float-Flip) (r Rect)
			       &key (result (make-rect (domain map))))
  
  "This is not, strictly speaking, an inverse, because transform,
followed by inverse-transform (or vice-versa) is not the identity map."
  
  (declare (type Rect result)
	   (:returns (type Rect result)))
  
  (let* ((od (coords (dom-origin map))) 
	 (ed (coords (dom-extent map)))
	 (oc (coords (cod-origin map))) 
	 (ec (coords (cod-extent map)))
	 (or (coords (origin r))) 
	 (er (coords (extent r)))
	 (oresult (coords (origin result))) 
	 (eresult (coords (extent result))))
    (dotimes (i (dimension (domain map)))
      (let* ((odi (aref od i))
	     (edi (aref ed i))
	     (oci (aref oc i))
	     (eci (aref ec i))
	     (rmin (aref or i))
	     (rmax (+ rmin (aref er i))))
	(setf rmin (ceiling (* (- rmin oci) edi) eci))
	(setf rmax (ceiling (* (- rmax oci) edi) eci))
	;; y coord is flipped
	(setf (aref oresult i) (+ odi (if (= i 1) (- edi rmax) rmin)))
	(setf (aref eresult i) (- rmax rmin)))))
    
  result)

