;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: Geometry -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted. This software is made available
;;; AS IS, and no warranty---about the software, its performance, or its
;;; conformity to any specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Geometry)

;;;=======================================================
;;;		    Abstract Spaces
;;;=======================================================

(defclass Flat-Space (Standard-Object)
	  ((dimension 
	    :type az:Array-Index 
	    :initarg :dimension 
	    :reader dimension)
	   (element-resource 
	    :type List 
	    :initform ()
	    :accessor element-resource)
	   (name
	    :type String
	    :reader name
	    :initarg :name
	    :initform (string (gensym "Sp")))
	   (rect-resource
	    :type List
	    :initform ()
	    :accessor rect-resource))
  (:documentation
   "This is a parent class for finite dimensional real affine spaces
whose translation space has an inner product. It would more correct,
but too verbose and pedantic for the present needs,
to call it something like ``Euclidean-Space''."))

;;;-------------------------------------------------------

(defmethod print-object ((space Flat-Space) stream)
  (if (slot-boundp space 'name)
    (format stream "#<~:(~a~) ~a>" (class-name (class-of space)) (name space))
    (az:printing-random-thing
     (space stream)
     (format stream "~:(~a~)" (class-name (class-of space))))))
 
;;;-------------------------------------------------------

(defmethod element? ((x T) (sp Flat-Space))
  "by default nothing is an element of any space"
  (declare (:returns nil))
  nil)

;;;-------------------------------------------------------
;;; default coordinate range:

(defmethod coord-start ((sp Flat-Space)) 0)
(defmethod coord-end ((sp Flat-Space)) (dimension sp))

;;;-------------------------------------------------------

(defgeneric make-element (sp)
  #-sbcl (declare (type Flat-Space sp)))

(defmethod random-element ((sp Flat-Space) &key (min -1.0d0) (max 1.0d0))
  (fill-randomly! (make-element sp) :min min :max max))

;;;-------------------------------------------------------

(defgeneric borrow-element (sp)
  #-sbcl (declare (type Flat-Space sp)
		  (:returns (type Point))))

(defmethod borrow-element ((sp Flat-Space))
  
  (if (null (element-resource sp))
    (make-element sp)
    ;; else
    (pop (element-resource sp))))

;;;-------------------------------------------------------

(defgeneric translation-space (sp)
  #-sbcl (declare (type Flat-Space sp))
  (:documentation
   "The <translation-space> is the vector space of differences of
elements of the Flat space."))

;;;-------------------------------------------------------

(defgeneric make-rect (space &rest options)
  #-sbcl (declare (type Flat-Space space)
		  (:returns (type Rect)))
  (:documentation "Make an axis parallel rectangle that lives in <space>."))

(defgeneric borrow-rect (sp &rest options)
  #-sbcl (declare (type Flat-Space sp)
		  (:returns (type Point))))

(defmethod borrow-rect ((sp Flat-Space) &rest options)
  
  (if (null (rect-resource sp))
    (apply #'make-rect sp options)
    ;; else
    (apply #'set-rect-coords sp (pop (rect-resource sp)) options)))

;;;=======================================================
;;;		     Vector Spaces
;;;=======================================================

(defclass Vector-Space (Flat-Space)
	  ((block-subspace-table
	    :type Hash-Table)
	   (canonical-subspace-table
	    :type Hash-Table))
  (:documentation
   "Vector-Space is actually a class for finite-dimensional 
real inner-product spaces, rather than general vector spaces." ))

;;;-------------------------------------------------------

(defmethod translation-space ((sp Vector-Space))
  "A Vector-Space is its own translation space."
  (declare (:returns sp))
  sp)

(defgeneric dual-space (sp)
  #-sbcl (declare (type Vector-Space sp)))

(defmethod dual-space ((sp Vector-Space))
  "By default, a Vector-Space is its own dual space."
  sp)

(defgeneric zero-element? (x sp)
  #-sbcl (declare (type Vector-Space sp)))

(defgeneric make-zero-element (sp)
  #-sbcl (declare (type Vector-Space sp)))

;;;-------------------------------------------------------
;;; Space-Subspace relations
;;;-------------------------------------------------------

(defgeneric parent-space (sp))

(defmethod parent-space ((sp Vector-Space)) sp)

;;;-------------------------------------------------------

(defgeneric subspace? (sp0 sp1))

(defmethod subspace? ((sub Vector-Space) (sp Vector-Space))
  " <nil> means ``don't know'' rather than ``isn't''."
;;; might change to 2 value return like <subtypep>

  (or (eq sub sp)
      (proper-subspace? sub sp)))

;;;-------------------------------------------------------

(defgeneric proper-subspace? (spo sp1))

(defmethod proper-subspace? ((sub Vector-Space) (sp Vector-Space)) nil)

;;;-------------------------------------------------------

(defgeneric spanning-space (sp0 sp1))

(defmethod spanning-space ((sp0 Vector-Space) (sp1 Vector-Space))
  "If <sp0> and <sp1> have the same parent space, return the smallest
subspace of that parent that contains both of them."
  (assert (eq (parent-space sp0)
	      (parent-space sp1)))
  (cond ((eq sp0 sp1) sp0)
	((proper-subspace? sp0 sp1) sp1)
	((proper-subspace? sp1 sp0) sp0)
	;; this isn't the smallest answer, but it's good enough.
	(t (parent-space sp0))))

;;;-------------------------------------------------------

(defgeneric make-linear-map (codomain domain)
  #-sbcl (declare (type Vector-Space codomain domain))
  (:documentation
   "Make a Map from <domain> to <codomain>,
following convention of codomain first."))


(defmethod make-element ((sp Vector-Space))
  (make-instance 'Point
    :home sp 
    :coords (az:make-float-vector (dimension sp))))

(defmethod make-zero-element ((sp Vector-Space))
  (zero! (make-element sp)))

;;;-------------------------------------------------------

(defgeneric make-canonical-basis-element (sp i)
  #-sbcl (declare (type Vector-Space sp)
		  (type Integer i)))

(defgeneric borrow-canonical-basis-element (sp i)
  #-sbcl (declare (type Vector-Space sp)
		  (type Integer i)))

(defmethod borrow-canonical-basis-element ((sp Vector-Space)
					   (i Integer))
  (let ((v (borrow-element sp)))
    (zero! v)
    (setf (coordinate v i) 1.0d0)
    v))

(defmacro with-canonical-basis-element ((vname sp i) &body body)
  ;; use <return-name> so we deallocate the right thing,
  ;; even if the user re-assigns <name>.
  (let ((return-name (gensym)))
    `(let* ((,return-name (borrow-canonical-basis-element ,sp ,i))
	    (,vname ,return-name))
       (multiple-value-prog1
	   (progn ,@body)
	 (return-element ,return-name ,sp)))))

;;;-------------------------------------------------------
;;; Space-Subspace relations
;;;-------------------------------------------------------

(defmethod block-subspace-table ((sp Vector-Space))
  (unless (slot-boundp sp 'block-subspace-table)
    (setf (slot-value sp 'block-subspace-table)
      (make-hash-table :test #'eql)))
  (slot-value sp 'block-subspace-table))
      
;;;-------------------------------------------------------

(defmethod canonical-subspace-table ((sp Vector-Space))
  (unless (slot-boundp sp 'canonical-subspace-table)
    (setf (slot-value sp 'canonical-subspace-table)
      (make-hash-table :test #'equal)))
  (slot-value sp 'canonical-subspace-table))
      
;;;-------------------------------------------------------

(defmethod tail-space ((sp Vector-Space))
  "A frequently occurring special case (isn't this a lot like cdr?)"
  (block-subspace sp (+ (coord-start sp ) 1) (coord-end sp)))

;;;=======================================================
;;; Vector-Subspace
;;;=======================================================

(defclass Vector-Subspace (Vector-Space)
	  ((parent-space
	    :type Vector-Space
	    :reader parent-space
	    :initarg :parent-space))
  (:documentation "A parent class for Vector subspaces"))

;;;=======================================================
;;;		   Block-SubSpace
;;;=======================================================
;;; A block subspace is one spanned by the [start,end)
;;; canonical basis vectors.

(defclass Block-SubSpace (Vector-Subspace)
	  ((parent-space
	    :type Vector-Space
	    :initarg :parent-space
	    :reader   parent-space)
	   (coord-start
	    :type az:Array-Index
	    :initarg :coord-start
	    :reader coord-start)
	   (coord-end
	    :type az:Array-Index
	    :initarg :coord-end
	    :reader coord-end)))

;;;-------------------------------------------------------

(defmethod dimension ((sp Block-SubSpace))
  (- (coord-end sp) (coord-start sp)))

;;;-------------------------------------------------------

(defmethod print-object ((vs Block-SubSpace) str)
  (format str "(BSS ~a ~d ~d)"
	  (parent-space vs) (coord-start vs) (coord-end vs)))

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((sp Block-SubSpace) slot-names
				     &rest initargs)
  (declare (ignore initargs slot-names))
  (assert (< -1
	     (coord-start sp)
	     (coord-end sp)
	     (+ (dimension (parent-space sp)) 1))))
 
;;;-------------------------------------------------------

(defmethod make-element ((sp Block-Subspace))
  (make-instance 'Block-Point
    :home sp
    :coords (az:make-float-vector (dimension sp))))

(defmethod make-zero-element ((sp Block-Subspace))
  (make-instance 'Block-Point
    :home sp
    :coords (az:make-float-vector (dimension sp))))

;;;-------------------------------------------------------

(defmethod proper-subspace? ((sub Block-SubSpace) (sp Vector-Space))
  (eql (parent-space sub) sp))

(defmethod proper-subspace? ((sub Block-SubSpace) (sp Block-SubSpace))
  (and (eql (parent-space sub) (parent-space sp))
       (<= (coord-start sp) (coord-start sub) (coord-end sub) (coord-end sp))))

;;;-------------------------------------------------------

(defmethod spanning-space ((sp0 Block-SubSpace) (sp1 Vector-Space))
  (if (subspace? sp0 sp1)
    sp1
    (error "No space spans ~s and ~s." sp0 sp1)) )

(defmethod spanning-space ((sp0 Vector-Space) (sp1 Block-SubSpace))
  (if (subspace? sp1 sp0)
    sp0
    (error "No space spans ~s and ~s." sp0 sp1)) )

(defmethod spanning-space ((sp0 Block-SubSpace) (sp1 Block-SubSpace))
  "If <sp0> and <sp1> have the same parent space, return the smallest
subspace of that parent that contains both of them."
  (if (eq (parent-space sp0) (parent-space sp1))
    (block-subspace (parent-space sp0)
		    (min (coord-start sp0) (coord-start sp1))
		    (max (coord-end sp0) (coord-end sp1)))
    (error "No space spans ~s and ~s." sp0 sp1)))

;;;=======================================================

(defmethod block-subspace ((sp Vector-Space) start
			   &optional (end (dimension sp)))

  "To ensure there is only one Block-SubSpace for a given
<parent-space>, <start>, and <end>."

  (cond
   ((and (= start 0) (= end (dimension sp))) ;; trivial case
    sp)
   (t
    (assert (< -1 start end (+ (dimension sp) 1)))
    (let* ((starttable (block-subspace-table sp))
	   (endtable (gethash start starttable)))
      (when (null endtable)
	(setf endtable (make-hash-table :test #'eql))
	(setf (gethash start starttable) endtable))
      (let ((subspace (gethash end endtable)))
	(when (null subspace)
	  (setf subspace (make-instance 'Block-Subspace
			   :parent-space sp
			   :coord-start start
			   :coord-end end))
	  (setf (gethash end endtable) subspace))
	subspace)))))

(defmethod block-subspace ((sp Block-SubSpace) start
			   &optional (end (dimension sp)))
  "The block-subspace of a Block-SubSpace is referred back
to the parent-space."
  (cond
   ((and (= start (coord-start sp)) (= end (coord-end sp)))
    ;; trivial case
    sp)
   (t
    (block-subspace (parent-space sp) start end))))

;;;=======================================================
;;;		   Canonical-SubSpace
;;;=======================================================

(defclass Canonical-SubSpace (Vector-Subspace)
	  ((parent-space
	    :type Vector-Space
	    :initarg :parent-space
	    :reader parent-space)
	   (index-list
	    :type List
	    :initarg :index-list
	    :reader index-list))
  (:documentation
   "A Canonical subspace is one spanned by the canonical
basis vectors whose indices are listed in <index-list>."))

;;;-------------------------------------------------------

(defmethod dimension ((sp Canonical-SubSpace)) (length (index-list sp)))

;;;-------------------------------------------------------

(defmethod print-object ((sp Canonical-SubSpace) str)
  #-sbcl (format str "(CSS ~a ~d ~d)" (parent-space sp) (index-list sp))
  #+sbcl (format str "(CSS ~a ~d ~d)" sp (parent-space sp) (index-list sp)) ; $$$$ argcount wrong in orig -jm
  )

;;;-------------------------------------------------------

(defmethod shared-initialize :after ((sp Canonical-SubSpace) slot-names
				     &rest initargs)
  (declare (ignore initargs slot-names))
  (assert (every #'(lambda (i) (< -1 i (dimension (parent-space sp))))
		 (index-list sp))))

;;;-------------------------------------------------------

(defmethod make-element ((sp Canonical-Subspace))
  (make-instance 'Canonical-Point
    :home sp
    :coords (az:make-float-vector (dimension sp))))

(defmethod make-zero-element ((sp Canonical-Subspace))
  (make-instance 'Canonical-Point
    :home sp
    :coords (az:make-float-vector (dimension sp))))

;;;-------------------------------------------------------

(defmethod proper-subspace? ((sub Canonical-SubSpace) (sp Vector-Space))
  (eql (parent-space sub) sp))

(defmethod proper-subspace? ((sub Canonical-SubSpace) (sp Canonical-SubSpace))
  (and (eql (parent-space sub) (parent-space sp))
       (not (eq sub sp))
       (subsetp (index-list sub) (index-list sp))))

;;;-------------------------------------------------------
;;; If <sp0> and <sp1> have the same parent space, return the smallest
;;; subspace of that parent that contains both of them.

(defmethod spanning-space ((sp0 Canonical-SubSpace) (sp1 Vector-Space))
  (if (subspace? sp0 sp1)
    sp1
    (error "No space spans ~s and ~s." sp0 sp1)))

(defmethod spanning-space ((sp0 Vector-Space)
			   (sp1 Canonical-SubSpace))
  (if (subspace? sp1 sp0)
    sp0
    (error "No space spans ~s and ~s." sp0 sp1)))

(defmethod spanning-space ((sp0 Canonical-SubSpace) (sp1 Canonical-SubSpace))
  (if (eq (parent-space sp0) (parent-space sp1))
    (canonical-subspace (parent-space sp0)
			(union (index-list sp0) (index-list sp1)))
    (error "No space spans ~s and ~s." sp0 sp1)))

;;;=======================================================

(defmethod canonical-subspace ((sp Vector-Space) index-list)
  "To ensure there is only one Canonical-SubSpace for a given
<parent-space>, <start>, and <end>."
  (setf index-list (sort (delete-duplicates index-list) #'<))
  (assert (every #'(lambda (i) (< -1 i (dimension sp)))
		 index-list))
  (cond
   ((loop for index in index-list
	for i from 0
	always (= i index))
     sp)
   (t 
    (let ((subspace (gethash index-list (canonical-subspace-table sp))))
      (when (null subspace)
	(setf subspace (make-instance 'Canonical-Subspace
			 :parent-space sp
			 :index-list index-list))
	(setf (gethash index-list (canonical-subspace-table sp)) subspace))
      subspace))))

(defmethod canonical-subspace ((sp Canonical-SubSpace) index-list)
  "The canonical-subspace of a Canonical-SubSpace is referred back 
to the parent-space."
  (cond ((equal index-list (index-list sp)) sp) ;; trivial case
	(t (canonical-subspace (parent-space sp) index-list))))
 
;;;=======================================================
;;; Vector Spaces of Linear Mappings
;;;=======================================================

(defclass Linear-Map-Space (Vector-Space)
	  ((codomain
	    :type Vector-Space
	    :reader codomain
	    :initarg :codomain)
	   (domain
	    :type Vector-Space
	    :reader domain
	    :initarg :domain)))

;;;-------------------------------------------------------

(defmethod print-object ((t0 Linear-Map-Space) stream)
  (if (az::slots-boundp t0 '(codomain domain))
      (az:printing-random-thing (t0 stream)
	(format stream
		"~:(~a~) ~a->~a"
		(class-name (class-of t0))
		(domain t0) (codomain t0)))
      (call-next-method))) 

;;;-------------------------------------------------------

(defmethod dimension ((space Linear-Map-Space))
  (* (dimension (codomain space))
     (dimension (domain space))))

(defmethod make-element ((space Linear-Map-Space))
  (make-linear-map (codomain space) (domain space)))

;;;-------------------------------------------------------
;;; To ensure there is only one Rn space of a given dimension.

(defparameter *linear-map-space-table* (make-hash-table))

(defun the-linear-map-space (cod dom)

  (az:type-check Vector-Space cod dom)

  (let ((dom-table (gethash cod *linear-map-space-table* nil)))
    (when (null dom-table) (setf dom-table (make-hash-table)))
    (let ((space (gethash dom dom-table nil)))
      (when (null space)
	(setf space
	      (make-instance 'Linear-Map-Space :codomain cod :domain dom))
	(setf (gethash dom dom-table) space))
      space)))

;;;============================================================
;;;		     Affine Spaces
;;;============================================================

(defclass Affine-Space (Flat-Space)
	  ((translation-space
	    :type Vector-Space
	    :reader translation-space
	    :initarg :translation-space))
  (:documentation "Flat Spaces that are not Vector spaces."))

;;;=======================================================
;;; Array spaces
;;;=======================================================

(defclass Array-Space (Vector-Space)
	  ((dims
	    :type List
	    :reader dims
	    :initarg :dims
	    :documentation
	    "A list of the dimensions of the arrays that are
             elements of this space."))
  (:documentation
   "The float arrays of given dimensions form a vector space."))

;;;-------------------------------------------------------

(defmethod dimension ((space Array-Space)) (apply #'* (dims space)))

(defmethod make-element ((space Array-Space))
  (az:make-float-array (dims space)))

;;;-------------------------------------------------------

(defparameter *array-space-table* (make-hash-table :test #'equal)
  "To ensure there is only one Array space of a given dimension.")

(defun array-space (dims)
  (let ((sp (gethash dims *array-space-table* nil)))
    (when (null sp)
      (setf sp (make-instance 'Array-Space :dims dims))
      (setf (gethash dims *array-space-table*) sp))
    sp))

;;;=======================================================
;;;		      Rn
;;;=======================================================

(defparameter *rn-table* (make-hash-table)
  "This table holds a set of standard euclidean vector spaces,
intended to represent R^n.")

(defun rn (dim)
  "To ensure there is only one Rn space of a given dimension."
  (let ((sp (gethash dim *rn-table* nil)))
    (when (null sp)
      (setf sp (make-instance 'Vector-Space :dimension dim
			      :name (format nil "R^~d" dim)))
      (setf (gethash dim *rn-table*) sp))
    sp))

;;;=======================================================
;;;		    Direct-Sum-Space
;;;=======================================================

(defclass Direct-Sum-Space (Flat-Space)
	  ((space0
	    :type Flat-Space
	    :accessor space0
	    :initarg :space0)
	   (space1
	    :type Flat-Space
	    :accessor space1
	    :initarg :space1))
  (:documentation
   "A class of Flat Spaces, resulting from direct sums of other spaces."))

;;;-------------------------------------------------------
;;; To ensure there is only one Direct-Sum-Vector-Space
;;; for a given pair of summands.

(defvar *direct-sum-space-table* (make-hash-table :test #'eq))

(defmethod direct-sum ((xsp Flat-Space) (ysp Flat-Space))
  (let ((inner-table (gethash xsp *direct-sum-space-table* nil))
	(dsp nil))
    (cond ((null inner-table)
	   (setf inner-table (make-hash-table :test #'eq))
	   (setf (gethash xsp *direct-sum-space-table*) inner-table))
	  (t
	   (setf dsp (gethash ysp inner-table nil))))
    (when (null dsp)
      (setf dsp (make-instance 'Direct-Sum-Space :space0 xsp :space1 ysp))
      (setf (gethash ysp inner-table nil) dsp))
    dsp))

;;;-------------------------------------------------------

(defmethod dimension ((vs Direct-Sum-Space))
  (+ (dimension (space0 vs)) (dimension (space1 vs))))

;;;------------------------------------------------------------

(defmethod make-element ((vs Direct-Sum-Space))
  (make-instance 'Direct-Sum-Point
    :home vs
    :v0 (make-element (space0 vs))
    :v1 (make-element (space1 vs))))

(defmethod make-zero-element ((vs Direct-Sum-Space))
  (make-instance 'Direct-Sum-Point
    :home vs
    :v0 (make-zero-element (space0 vs))
    :v1 (make-zero-element (space1 vs))))

;;;-------------------------------------------------------

(defmethod make-linear-map ((codomain Direct-Sum-Space)
			    (domain Direct-Sum-Space))
  (make-instance '2x2-Direct-Sum-Map
    :codomain codomain
    :domain domain
    :t00 (make-linear-map (space0 codomain) (space0 domain))
    :t01 (make-linear-map (space0 codomain) (space1 domain))
    :t10 (make-linear-map (space1 codomain) (space0 domain))
    :t11 (make-linear-map (space1 codomain) (space1 domain))))

;;;-------------------------------------------------------

(defmethod translation-space ((sp Direct-Sum-Space))
  (direct-sum (translation-space (space0 sp))
	      (translation-space (space1 sp))))

;;;=======================================================
;;; Discrete Spaces
;;;=======================================================

(defclass Byte-Flat (Flat-Space) ())

(defclass Byte-Grid (Byte-Flat Affine-Space) ()
  (:documentation
   "Instances of this class represent regular rectangular 
grids of points, whose elements have coordinates which are bytes,
ie., integers from 0 to 255 (unsigned-byte 8)."))

;;;-------------------------------------------------------

(defmethod make-element ((grid Byte-Grid))
  (make-instance 'Point
    :home grid
    :coords (az:make-card8-vector (dimension grid))))

;;;=======================================================

(defclass Byte-Lattice (Byte-Flat Vector-Space) ()
  (:documentation
   "Instances of this class represent regular rectangular 
lattices of vectors whose elements have coordinates which are bytes,
8 bit integers from 0 to 255 (unsigned-byte 8)."))

;;;-------------------------------------------------------

(defmethod make-element ((lattice Byte-Lattice))
  (make-instance 'Point
    :home lattice
    :coords (az:make-card8-vector (dimension lattice))))

;;;=======================================================

(defclass Int-Flat (Flat-Space) ())

(defclass Int-Grid (Int-Flat Affine-Space) ()
  (:documentation
   "Instances of this class represent regular rectangular 
grids of points, whose elements have coordinates which 
are 16 bit integers, from -32768 to 32767 (signed-byte 16)."))

;;;-------------------------------------------------------

(defmethod make-element ((grid Int-Grid))
  (make-instance 'Point
    :home grid
    :coords (az:make-int16-vector (dimension grid))))

;;;=======================================================

(defclass Int-Lattice (Int-Flat Vector-Space) ()
  (:documentation
   "Instances of this class represent regular rectangular 
lattices of vectors whose elements have coordinates which
are 16 bit integers, from -32768 to 32767 (signed-byte 16)."))

;;;-------------------------------------------------------

(defmethod make-element ((lattice Int-Lattice))
  (make-instance 'Point
    :home lattice
    :coords (az:make-int16-vector (dimension lattice))))

;;;============================================================
;;; Standard Screen Spaces
;;;============================================================

(defvar *screen-vector-space*
    (make-instance 'Int-Lattice
      :dimension 2
      :name "Screen Vector Space")
  "A default screen space for convenience in graphics ops.")

(defvar *screen-space* (make-instance 'Int-Grid
			 :dimension 2
			 :name "Screen Space"
			 :translation-space *screen-vector-space*)
  "A default screen space for convenience in graphics ops.")


