;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :Definitions -*-

(in-package :Definitions)

(mk:compile-system :Definitions)

;;;===========================================================

(defun p ()
  (labels ((acceptable? (def)
	     (and (exported-definition? def)
		  (eq (symbol-package (definition-symbol def)) 	
		      (find-package
		       :parrot)))) 
	   (print-defs (in-paths out-path)
	     (print-definitions 
	      (sort (delete-if-not #'acceptable? 
				   (read-definitions-from-files in-paths))
		    #'definition-alpha<)
	      out-path))) 
    (user::cs :parrot) 
    (print-defs '("../parrot/package" 
		  "../parrot/foreign"
		  "../parrot/defs"
		  "../parrot/atomic"
		  "../parrot/kill" 
		  "../parrot/socket"
		  "../parrot/chunk"
		  "../parrot/identifier"
		  "../parrot/message"
		  "../parrot/connection"
		  "../parrot/tool" 	
		  "../parrot/tool-connection")
		"../parrot/doc/tool-ref.tex")
    (print-defs '("../parrot/dispatcher"
		  "../parrot/dispatcher-connection")
		"../parrot/doc/server-ref.tex")
    (print-defs '("../parrot/eval")
		"../parrot/doc/eval-ref.tex")
    (unix-shell "( cd ../parrot/doc; latex tr )")))


(defun p1 ()
  (labels ((acceptable? (def)
	     (and (exported-definition? def)
		  (eq (symbol-package (definition-symbol def)) 	
		      (find-package :parrot)))) 
	   (print-defs (in-paths out-path)
	     (print-definitions 
	      (sort (delete-if-not #'acceptable? 
				   (read-definitions-from-files in-paths))
		    #'definition-alpha<)
	      out-path))) 
    (user::cs :parrot) 
    (print-defs '("../parrot/transport-defs" 
		  "../parrot/transport-calls")
		"../parrot/doc/transport-ref.tex")
    (unix-shell "( cd ../parrot/doc; latex transport )")))

;;;============================================================

(labels ((acceptable? (def) (and (exported-definition? def)
				 (eq (symbol-package (definition-symbol def)) 
				     (find-package :msg))))
	 (print-defs (in-paths out-path)
	   (print-definitions 
	    (sort (delete-if-not #'acceptable?
				 (read-definitions-from-files in-paths))
		  #'definition-alpha<) 
	    out-path)))
  (user::cs :msg)
  (print-defs '("../msg/package" 
		"../msg/foreign" 
		"../msg/defs" 
		"../msg/message" 
		"../msg/connection" 
		"../msg/client")
	      "../msg/doc/client-ref.tex")
  (print-defs '("../msg/server") "../msg/doc/server-ref.tex")
  (print-defs '("../msg/eval") "../msg/doc/eval-ref.tex")
  (excl:shell "( cd ../msg/doc; latex tr )"))


;;;============================================================

(defparameter *brick-defs*
    (sort
     (delete-if
      #'(lambda (def) (typep def 'Method-Definition))
      (delete-if-not
       #'exported-definition?
       (read-definitions-from-files '("../brick/package"
				      "../brick/defs"
				      "../brick/simplex"
				      "../brick/brick"
				      "../brick/functionals"
				      "../brick/inequalities"
				      "../brick/constraints"
				      "../brick/solve"
				      ;;"../brick/clx-tools"
				      ;;"../brick/test-brick"
				      ;;"../brick/tests"
				      ))))
     #'definition-alpha<))

(progn
 (print-definitions *brick-defs* "../brick/doc/ref.tex")
 nil)

;;;============================================================

(defparameter *actor-defs*
    (sort
     (delete-if-not
      #'exported-definition?
      ;;#'(lambda (def) (exported-definition? def :package :actors))
      (read-definitions-from-files ' ("../actors/package"
				      "../actors/defs"
				      "../actors/atomic"
				      "../actors/queues"
				      "../actors/clx-tools"
				      "../actors/actors"
				      "../actors/interactor"
				      "../actors/event-loop"
				      "../actors/initializations"
				      )))
     #'definition-alpha<))

(progn
 (print-definitions *actor-defs* "../actors/doc/ref.tex")
 nil)

;;;============================================================

(defparameter *slate-defs*
    (sort
     (delete-if-not
      #'exported-definition?
      (read-definitions-from-files'("../slate/package.lisp"
				    "../slate/slate-object.lisp"
				    "../slate/boolean-operations.lisp"
				    "../slate/colors.lisp"
				    "../slate/colormaps.lisp"
				    "../slate/fonts.lisp"
				    "../slate/screens.lisp"
				    "../slate/slates.lisp"
				    "../slate/tiles.lisp"
				    "../slate/line-styles.lisp"
				    "../slate/fill-styles.lisp"
				    "../slate/pens.lisp"
				    "../slate/drawing.lisp"
				    "../slate/input-events.lisp"
				    "../slate/interactor-role.lisp"
				    "../slate/interactor.lisp"
				    "../slate/input-slates.lisp"
				    "../slate/input-toolkit.lisp"
				    #+:coral "../slate/mac.lisp"
				    #+symbolics "../slate/stv.lisp"
				    #+:excl "../slate/clxscreens.lisp"
				    #+:excl "../slate/clx-slates.lisp"
				    #+:excl "../slate/clx-ps-dump.lisp"
				    #+:excl "../slate/clx-drawing.lisp"
				    #+:excl "../slate/clx-events.lisp"
				    #+:excl "../slate/clx-input.lisp"
				    #+:excl "../slate/clx-menu.lisp"
				    "../slate/initializations.lisp"
				    "../slate/tests.lisp"
				    )))
     #'definition-alpha<))

(progn
 (print-definitions *slate-defs* "../slate/doc/ref.tex")
 nil)
 
;;;============================================================

(defparameter *xlite-defs*
    (sort
     (delete-if-not
      #'exported-definition?
      (read-definitions-from-files '("../xlite/package"
				     "../xlite/defs"
				     "../xlite/displays"
				     "../xlite/colors"
				     "../xlite/paint-4-4"
				     "../xlite/drawables"
				     "../xlite/fonts"
				     "../xlite/gcontexts"
				     "../xlite/graphics"
				     "../xlite/images"
				     "../xlite/cursors"
				     "../xlite/events"
				     ;;"../xlite/menu"
				     "../xlite/hardcopy"
				     "../xlite/ps-hardcopy"
				     )))
     #'definition-alpha<))

(progn
 (print-definitions *xlite-defs* "../xlite/doc/ref.tex")
 nil)

;;;============================================================

(defparameter *npsol-defs*
    (print-definitions
     (sort
      (delete-if
       #'(lambda (def)
	   (let ((symbol (definition-symbol def)))
	     (string-equal (symbol-name symbol)
			   "SET-STANDARD-NPSOL-OPTIONS")))
       (delete-if-not
	#'exported-definition?
	(read-definitions-from-files
	 '("/belgica-2g/jam/az/npsol/package"
	   "/belgica-2g/jam/az/npsol/defs"
	   "/belgica-2g/jam/az/npsol/foreign"
	   "/belgica-2g/jam/az/npsol/interface"
	   "/belgica-2g/jam/az/npsol/tests"))))
      #'definition-alpha<)
     "/belgica-2g/jam/az/npsol/doc/reference-manual.tex"))

;;;============================================================

(defparameter *az-defs*
    (print-definitions
     (sort
      (delete-if-not
       #'exported-definition?
       (read-definitions-from-files
	'("../az/package"
	  "../az/types"
	  "../az/macros"
	  "../az/defs"
	  "../az/tools"
	  "../az/equal"
	  "../az/copy"
	  "../az/kill"
	  "../az/arrays"
	  "../az/tables"
	  "../az/foreign-load"
	  "../az/foreign"
	  "../az/atomic"
	  "../az/timestamp"
	  "../az/clos-additions"
	  "../az/resources")))
      #'definition-alpha<)
     "../az/doc/ref.tex"))

;;;============================================================

(progn
(defparameter *browser-defs*
    (remove-if-not #'exported-definition?
		   (read-definitions-from-files
		    '("../browser/package"
		      "../browser/class"
		      "../browser/examiner"
		      "../browser/pedigree"
		      "../browser/horse"
		      "../graph/package.lisp"
		      "../graph/protocol.lisp"
		      "../graph/implementation.lisp"
		      ))))

(defparameter *clay-defs*
    (remove-if-not #'exported-definition?
		   (read-definitions-from-files
		    '("../clay/package"
		      "../clay/defs"
		      "../clay/announcements"
		      "../clay/lens"
		      "../clay/paint"
		      "../clay/stroke"
		      "../clay/diagram"
		      "../clay/layout-solver"
		      "../clay/interactive"
		      "../clay/role"
		      "../clay/mediator"
		      "../clay/root"
		      "../clay/leaf"
		      "../clay/leaves"
		      "../clay/label"
		      "../clay/button"
		      "../clay/toggle"
		      "../clay/radio-button"
		      "../clay/labeled-button"
		      "../clay/menu-items"
		      "../clay/menu"
		      "../clay/control-panel"
		      "../clay/palette"
		      "../clay/plot"
		      "../clay/initializations"
		      ))))

(defparameter *layout-defs*
    (remove-if-not #'exported-definition?
		   (read-definitions-from-files
		    '("../layout/package"
		      "../layout/defs"
		      "../layout/graph"
		      "../layout/node"
		      "../layout/edge"
		      "../layout/solver"
		      #+:npsol "../layout/npsol"
		      #+:npsol "../layout/npsol-solver"
		      ;; #+:npsol "../layout/nlc-solver"
		      "../layout/constraints"
		      "../layout/attach-edges"
		      "../layout/distance"
		      "../layout/weights"
		      "../layout/node-spring"
		      ;;"../layout/node-repulsion"
		   ;;"../layout/edge-spring"
		      ;;"../layout/edge-repulsion"
		   ;;"../layout/homotopy"
		      ;;"../layout/homotopy-3d"
		   ;;"../layout/crusher"
		      ;;"../layout/digraph"
		   ;;"../layout/dag"
		      "../layout/mediator"
		      "../layout/input"
		      "../layout/menus"
		      "../layout/tests"
		      ))))
)

(progn
 (print-definitions *browser-defs* "../browser/doc/ref.tex")
 nil)

(progn
  (print-definitions
   (sort
    (concatenate 'List
      *browser-defs*
      (remove-if-not #'(lambda (d) (member (df:definition-symbol d)
					   '(clay:diagram-menu-items
					     clay:diagram-control-panel-items
					     lay:Graph-Diagram
					     lay:make-graph-diagram
					     lay:graph-menu-items
					     lay:graph-node-menu-items
					     lay:graph-edge-menu-items
					     lay:graph-control-panel-items
					     lay:graph-node-control-panel-items
					     lay:graph-edge-control-panel-items
					     )))
		     (concatenate 'List *clay-defs* *layout-defs*)))
    #'definition-alpha<)
   "../browser/doc/reference-manual.tex")
  nil)

;;;============================================================

(defparameter *def-files* '("package.lisp"
			    "defs.lisp"
			    "definition.lisp"
			    "pack.lisp"
			    "global.lisp"
			    "fun.lisp"
			    "type.lisp"
			    "build.lisp"
			    "read.lisp"
			    "filter.lisp"
			    "sort.lisp"
			    "print.lisp"))

(defparameter *defs* (df:read-definitions-from-files *def-files*))

(df:print-definitions
 (sort (remove-if-not #'df:exported-definition? df::*defs*)
       #'df:definition-alpha<)
 "doc/reference-manual.tex")

(df:print-definitions
 (sort
  (remove-if-not #'(lambda (d)
		     (member (df:definition-symbol d)
			     '(df:definition-symbol
			       df:print-definitions
			       df:definition-alpha<
			       df:exported-definition?
			       df:read-definitions-from-files)))
		 df::*defs*)
  #'df:definition-alpha<)
 "doc/user-manual.tex")

;;;============================================================

(defparameter *geometry-defs*
    (print-definitions
     (sort (delete-if-not #'exported-definition?
			  (read-definitions-from-files
			   '("~jam/az/geometry/package.lisp"
			     "~jam/az/geometry/defs.lisp"
			     "~jam/az/geometry/v-ops.lisp"
			     "~jam/az/geometry/geometric-objects.lisp"
			     "~jam/az/geometry/spaces.lisp"
			     "~jam/az/geometry/points.lisp"
			     "~jam/az/geometry/arrays.lisp"
			     "~jam/az/geometry/point-set.lisp"
			     "~jam/az/geometry/point-cloud.lisp"
			     "~jam/az/geometry/rects.lisp"
			     "~jam/az/geometry/mappings.lisp"
			     "~jam/az/geometry/matrix.lisp"
			     "~jam/az/geometry/modifiers.lisp"
			     "~jam/az/geometry/compound.lisp"
			     "~jam/az/geometry/mix.lisp"
			     "~jam/az/geometry/compose.lisp"
			     "~jam/az/geometry/transform.lisp"
			     "~jam/az/geometry/lapack.lisp"
			     "~jam/az/geometry/decompose.lisp"
			     "~jam/az/geometry/pseudo-inverse.lisp"
			     "~jam/az/geometry/determinant.lisp"
			     "~jam/az/geometry/metrics.lisp"
			     "~jam/az/geometry/tics.lisp"
			     )))
	   #'definition-alpha<)
     "~jam/az/geometry/doc/ref.tex"))

;;;============================================================

(defparameter *announcement-defs*
    (print-definitions
     (sort
      (delete-if
       #'(lambda (def) (typep def 'Method-Definition))
       (delete-if-not
	#'exported-definition?
	(read-definitions-from-files
	 '("/belgica-2g/jam/az/announcements/package.lisp"
	   "/belgica-2g/jam/az/announcements/defs.lisp"
	   "/belgica-2g/jam/az/announcements/announcements.lisp"))))
      #'definition-alpha<)
     "/belgica-2g/jam/az/announcements/doc/ref.tex"))

;;;============================================================

(defparameter *graph-defs*
    (print-definitions
     (sort
      (delete-if
       #'(lambda (def) (typep def 'Method-Definition))
       (delete-if-not
	#'exported-definition?
	(read-definitions-from-files
	 '("/belgica-2g/jam/az/graph/package.lisp"
	   "/belgica-2g/jam/az/graph/protocol.lisp"
	   "/belgica-2g/jam/az/graph/implementation.lisp"))))
      #'definition-alpha<)
     "/belgica-2g/jam/az/graph/doc/ref.tex"))

  
