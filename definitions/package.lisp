;;; -*- Syntax: Common-Lisp; Mode: Lisp; Package: :cl-user; -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software and preparation of derivative works
;;; based upon this software are permitted.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================================

(in-package :cl-user)

(defpackage :Definitions
  (:use :Common-Lisp #+:clos Clos #+:pcl :pcl)
  (:nicknames :Df)
  (:export 
   type-check
   declare-check
   fix-latex-string
   Definition
   definition-definee
   definition-form
   definition-definer
   definition-method-qualifier
   definition-name
   definition-name->string
   definition-symbol
   definition-symbol-name
   definition-lambda-list
   definition-documentation
   definition-declarations
   definition-returns
   definition-type-declarations
   definition-slots
   definition-parents
   definition-children
   definition-arg-types
   definition-initial-value
   definition-usage
   lambda-list-specializers
   lambda-list-keyword-args
   lambda-list-rest-arg-name
   lambda-list-optional-args
   lambda-list-arg-names
   lambda-list-required-arg-names
   lambda-list-optional-arg-names
   lambda-list-required-args
   lambda-list-keyword-arg-names
   lambda-list-whole-arg
   Package-Definition
   Global-Variable-Definition
   Constant-Definition
   Parameter-Definition
   Variable-Definition
   Lambda-List-Definition
   Function-Definition
   Macro-Definition
   Setf-Definition
   Generic-Function-Definition
   Method-Definition
   Setf-Method-Definition
   User-Type-Definition
   Type-Definition
   Class-Definition
   Structure-Definition
   definer-class
   definition-class-nice-name
   make-slot-accessor-definitions
   make-subdefinitions
   make-definitions
   read-definitions-from-file
   read-definitions-from-files
   exported-definition?
   definition-alpha<
   definition<
   print-definition-headline
   print-definition-documentation
   print-definition-usage
   print-definition-arg-types
   print-definition-returns
   print-definition-parents
   print-definition-children
   print-definition-source-path
   print-definition
   print-definitions))
