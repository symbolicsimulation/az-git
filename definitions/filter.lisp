;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Definitions -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software eparation of derivative works
;;; based upon this software are permitted.
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Definitions)

;;;=======================================================
;;; filtering
;;;=======================================================

(defun exported-symbol? (s &key (package (symbol-package s)))
  "Is the symbol <s> exported from its package?"
  (declare (type Symbol s)
	   (:returns (type (Member t nil))))
  (multiple-value-bind (s1 status) (find-symbol (symbol-name s) package)
    (and s1 (eq :external status))))

(defun exported-definition? (def &key
				 (package
				  (symbol-package (definition-symbol def))))
  "Has the <definition-symbol> of <def> been exported?"
  (declare (type Definition def)
	   (:returns (type (Member t nil))))
  (exported-symbol? (definition-symbol def) :package package))    

