;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Definitions -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software eparation of derivative works
;;; based upon this software are permitted.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Definitions)

;;;=======================================================

(defclass Global-Variable-Definition (Definition) ()
  (:documentation
   "An abstract super class for global variable definitions."))

(defun definition-initial-value (def)
  "The initial value supplied for a global variable (or constant) definition."
  (declare (type Definition def)
	   (:returns (type T)))
  (if (typep def 'Global-Variable-Definition)
      (third (definition-form def))
    ;; else
    ()))

(defclass Constant-Definition  (Global-Variable-Definition) ()
  (:documentation
   "A definition class for <defconstant>."))

(defclass Parameter-Definition (Global-Variable-Definition) ()
  (:documentation
   "A definition class for <defparameter>."))

(defclass Variable-Definition  (Global-Variable-Definition) ()
  (:documentation
   "A definition class for <defvar>."))

