
\section{Introduction}

This report describes the Definitions module, 
which provides the beginnings of a database for Common Lisp
source code objects.
The major use of this database at present is in automatically
typesetting reference manuals (using Latex \cite{Lamp85}), 
an example of which is section~\ref{reference-manual}.
It is inspired in part by the Definition Groups of Bobrow et. al \cite{Bobr87}
and the {\sf USER-MANUAL} of Kantrowitz \cite{Kant91}.

The Definitions module is
a component of a system called Arizona,
now under development at the U. of Washington.
Arizona is intended to be a portable, public-domain collection 
of tools supporting scientific computing, quantitative graphics, 
and data analysis,
implemented in Common Lisp
and CLOS (the Common Lisp Object System) \cite{Stee90}.
This document assumes the reader is familiar with Common Lisp and CLOS.
An overview of Arizona is given in \cite{McDo88b}
and an introduction to the current release is in \cite{McDo91g}.

The primary purpose of Definitions is to make possible
convenient runtime access 
to information available in Common Lisp source code,
that is lost in the normal process of reading, evaluating, and/or compiling.

Evaluating some Lisp definitions, such as {\sf defclass},
results in a first class Lisp object 
with a reasonable and reasonably portable
protocol for extracting useful information,
such as the direct sub- and super-classes.
(At least, this should be true once the MOP is stable \cite{Bobr88}.)
However, most Lisp definitions, such as {\sf defun},
while producing identifiable objects,
have limited facilities for extracting useful information;
usually the documentation string is all that is available.
And other definitions, such as {\sf defstruct},
do not even produce an identifiable object.

The Definitions module provides functions
to read source files and
create a Definition object
for each lisp definition in those files,
retaining the complete original defining lisp form.
Note that the ``defining lisp form'' is not quite the
same thing as the original source code. 
The ``defining lisp form'' is a value returned by {\sf read},
which means, for example, that all comments are removed.
The reason for simply using {\sf read} is that it is much easier
than parsing the source myself, deciding where to put top level comments,
etc.
In the future, I may replace the read functions by ones
that captured all the free text source, comments, white space, and all,
as well as applying {\sf read} to this source text 
to get a readily analyzable lisp object.

\subsection{Creating Reference Manuals}

The present limited user interface to creating
a reference manual from Definitions
consists of the function 
{\sf read-definitions-from-files}, 
which returns a list of Definitions that can be filtered with
{\sf exported-definition?}
and sorted with {\sf definition-alpha<}
before being printed in a fixed Latex format
with {\sf print-definitions}.

Note that the definitions must be loaded in the current
environment before {\sf read-definitions-from-files}
is called to ``re-read'' the definitions.

The simple user interface is documented more formally 
in section~\ref{reference-manual}, which was produced by evaluating:
\begin{code}
(defparameter *def-files* '("package.lisp"
			    "exports.lisp"
			    "defs.lisp"
			    "definition.lisp"
			    "pack.lisp"
			    "global.lisp"
			    "fun.lisp"
			    "type.lisp"
			    "build.lisp"
			    "read.lisp"
			    "filter.lisp"
			    "sort.lisp"
			    "print.lisp"))

(defparameter *defs* (df:read-definitions-from-files *def-files*))

(df:print-definitions
 (sort (remove-if-not #'df:exported-definition? *defs*)
       #'df:definition-alpha<)
 "doc/reference-manual.tex")
\end{code}

\subsection{Coding conventions}
\label{style}

Using the above functions on typical lisp code will produce 
reference manual entries that are not very informative.
The manual entries will be more worthy of the paper they consume
if the programmer follows certain coding conventions.

All important definitions should have a substantial documentation string,
which should emphasize describing the definition at a conceptual level
and should not contain information that can be extracted from
the definition form (such as the lambda list of a function).

Types for all (non-specialized) arguments should be specified
in a declaration.
Generally speaking, any documentation that can be reasonably
represented in a declaration should, 
even if it means defining new declaration types.
Although, at present, only {\sf type} and {\sf :returns}
declarations are relevant to the Definitions module,
other declarations may be analyzed in future extensions.

\subsubsection{Declaring returned values}

Information about returned values should be documented by
including a {\sf :returns} declaration, 
which is a declaration type
that has a special meaning to the Definitions module.
A {\sf :returns} declaration can be used to specify
the type(s) of returned value(s),
and/or the names of the variable(s) whose value(s) 
that are returned,
or the actual returned value(s), when it is constant.
Some examples of the use {\sf :returns} declarations are:
\begin{code}
(:returns nil)
(:returns result)
(:returns (type List))
(:returns (type List result))
(:returns (values x y z))
(:returns (values (type Boolean) (type Boolean)))
(:returns (values (type Fixnum left)
		  (type Fixnum top)
		  (type Positive-Fixnum left)
		  (type Positive-Fixnum width)))
\end{code}
In order to escape compiler warnings,
the programmer should place
\begin{code}
(declaim (declaration :returns))
\end{code}
somewhere that will cause it to be evaluated
before any code that contains a {\sf :returns} declaration.

\subsubsection{Type checking forms}
  
Sometimes one would prefer to have type checking forms
at the top of a function, rather than declarations.
The Arizona-Tools package \cite{McDo91g} provides
a macro called {\sf az:declare-check} which has the 
same syntax as {\sf declare} but expands into 
{\sf check-type} expressions.
(Unfortunately it is not easy to also have it expand
into the equivalent declarations as well.)
The Definitions treats any form whose first item is
a symbol whose print name is {\sf string-equal}
to {\sf ``DECLARE-CHECK''} the same as it treats {\sf declare} forms.
Using the symbol's print name makes it possible
for programmers who do not have access to the Arizona-Tools
package to define their own {\sf declare-check} macros
and have them treated appropriately.


