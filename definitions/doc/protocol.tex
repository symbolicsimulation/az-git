\section{Definition Protocol}
\label{protocol}

In this section we discuss the builtin Definition classes
and their protocol.
The purpose is to allow relatively sophisticated Lisp programmers
to customize and extend The Definition Objects module.

The major way to extend the Definition Objects module 
is to define new subclasses of {\sf df:Definition} 
or one of the subclasses listed in section~\ref{classes} that correspond
to new definition macros (eg. {\sf defannouncement} \cite{McDo91m}).

The protocol for instances of a Definition class,
described in more detail in section~\ref{protocol},
consists of a few functions for {\it reading} files
and {\it building} or instantiating Definitions,
a large number of functions for {\it parsing} 
and extracting useful information from the defining forms,
a few functions for {\it filtering} and {\it sorting}
sequences of Definitions,
and 
a few functions implementing a standard format 
for {\it printing} Definitions to a Latex file.

The major way to customize the behavior of Definitions
is to add new generic functions to the protocol that, for example,
print definitions in a different format.

\subsection{Builtin Classes}
\label{classes}

The Definitions module provides classes corresponding to
the following Common Lisp definition macros (see figure~\ref{classes-fig}:
{\sf defclass},
{\sf defconstant},
{\sf defgeneric},
{\sf defmacro},
{\sf defmethod},
{\sf defpackage},
{\sf defparameter},
{\sf defsetf},
{\sf defstruct},
{\sf deftype},
and 
{\sf defun}.
At present, it is missing classes for the following definition macros:
{\sf define-compiler-macro},
{\sf define-condition},
{\sf define-declaration},
{\sf define-method-combination},
{\sf define-modify-macro},
and
{\sf define-setf-method}.

All the built in classes provide the same state:
a single, read-only attribute {\sf definition-form}.
The extensive protocol of generic functions 
for extracting useful information from the {\sf definition-form}
is discussed in section~\ref{Parsing}.

\begin{figure}
\centering
\setlength{\unitlength}{1.0in}
\begin{picture}(5.0,3.5)
\put(0,0){\special{psfile=classes.ps
		hscale=1.0 vscale=1.0 hoffset=0.0 voffset=0.0}}
\end{picture}
\caption {Hierarchy of built in definition classes. \label{classes-fig} }
\end{figure}

\subsection{Reading}
\label{Reading}

The ``protocol'' for reading definitions consists of
{\sf read-definitions-from-file},
which reads the definitions in a single file and returns a list
of the resulting Definitions,
and
{\sf read-definitions-from-files},
which merely concatenates together the results of calling
{\sf read-definitions-from-file} on each file in a list.

Note that definitions are assumed to have been loaded into the environment
before they are ``re-read'' with {\sf read-definitions-from-file}.

\subsection{Building}
\label{Building}

The basic function for making Definition objects is {\sf make-definitions}.
It is responsible for building a Definition object for every
``defined'' Lisp object that would result from the evaluation
of its {\sf form} argument.
Evaluating a {\sf defun} form results in a single defined Lisp object ---
the function,
but evaluating other forms, such as a {\sf defstruct},
may result in the creation of a number of Lisp objects 
(constructor, predicate, accessor functions, etc.)
that should be recorded in a definition database.

For {\sf progn}-like top level forms, 
{\sf make-definitions} simply recursive applies itself to each subform.

For definition forms (ones that begin a symbol {\sf def}something),
{\sf make-definitions} simply makes an instance of the 
{\sf definer-class} of the {\sf definition-definer}
(the first element) of the form.

{\sf definer-class} is a function that maps definition symbols (definers)
to Definition classes. 
The mapping can be extended with {\sf setf} to associate new Definition
classes with new definers. 
For example:
\cd{(setf (df:definer-class 'az:defannouncement) 'az:Announcment-Definition)}
would cause an {\sf az:announcement-Definition} object
to be instantiated for every {\sf az:defannouncement} form that is read.

The new Definition is instantiated with two arguments 
whose values are retained:
the {\sf :definition-form}, an object returned by {\sf read},
and the {\sf :definition-path}, 
the pathname of the file from which the definition was read.

After instantiating the appropriate Definition class, 
{\sf make-definitions} applys the generic function {\sf make-subdefinitions} 
to the new Definition to get Definitions 
corresponding to implied constructor, predicate, accessor functions, etc.
For Definitions that have slots ---
by default only {\sf Structure-Definition}s and {\sf Class-Definition}s ---
{\sf make-subdefinitions} calls the generic function
{\sf make-slot-accessor-definitions}.

\subsection{Parsing}
\label{Parsing}

The bulk of the protocol for Definition classes is
a set of generic functions that extract useful information
from the original {\sf definition-form}.

{\sf Definition-definee} attempts to return the lisp object 
that resulted from evaluating the definition, if possible.

There are a variety of possibilities for the ``name'' of a definition.
{\sf Definition-name} returns a symbol for most definitions,
a list of the form {\sf (:method foo Number Array)}
for method definitions, 
or a list of the form {\sf (setf foo)}
for definitions of setf functions and methods.
{\sf Definition-name->string} returns
a representation of the {\sf definition-name} as a string, 
which makes it possible to produce consistent capitalization.
{\sf Definition-symbol} returns the obvious choice of symbol
associated with the {\sf definition-name},
eg., {\sf foo} in {\sf (setf foo)}.
{\sf Definition-symbol-name} is just shorthand for
\cd{(symbol-name (definition-symbol def))}.
{\sf Definition-class-nice-name} returns a short string 
characterizing ther type of the Definition, eg., ``Class''
for {\sf Class-Definition}.

{\sf Definition-initial-value} returns the initial value
specified for definitions like {\sf defparameter},
or {\sf nil} if there is no initial value.

{\sf Definition-method-qualifier} returns the method qualifier
(eg. {\sf around} or {\sf after}) for Method-Definitions.

{\sf Definition-lambda-list} returns the raw lambda list
associated with the definition 
(or null for definitions that no lambda list, such as {\sf defclass}).
For convenience, the Definition module provides functions
for parsing raw lambda lists:
{\sf lambda-list-arg-names},
{\sf lambda-list-whole-arg},
{\sf lambda-list-required-args},
{\sf lambda-list-required-arg-names},
{\sf lambda-list-specializers},
{\sf lambda-list-optional-args},
{\sf lambda-list-optional-arg-names},
{\sf lambda-list-rest-arg},
{\sf lambda-list-keyword-args},
and
{\sf lambda-list-keyword-arg-names}.

{\sf Definition-documentation} extracts the documentation string
associated with the definition, if any.

{\sf Definition-declarations} returns
the ``top level'' declarations associated with the definition.
{\sf Definition-type-declarations} returns just the type declarations.
{\sf Definition-returns} extracts the declarations that begin with
{\sf :returns}, as discussed in section~\ref{style}.

{\sf Definition-arg-types} looks at the specializers in the
lambda list and the type declarations to come up with a list of 
expected types for the arguments to the definition.

{\sf Definition-parents} returns a list of the names of 
the parents (for a class or structure definition).
For new Definition classes, it may return anything
that is reasonably thought of as a name of a ``parent'' of the definition.

{\sf Definition-children} returns a list of subclasses for a class definition.
Unfortunately, it's not easy to find the children of a structure definition.
For new Definition classes, it may return anything
that is reasonably thought of as a name of a ``child'' of the definition.

{\sf definition-slots} returns a list of slot specs, 
which need to be interpreted in a Definition class specific manner
(structure slot specs are different from class slot specs).

\subsection{Filtering}
\label{Filtering}

The only support for filtering a Definition list
at present is {\sf exported-definition?},
which simply tests to see if the {\sf definition-symbol} is exported.

\subsection{Sorting}
\label{Sorting}

The support for ordering definitions consists of the function
{\sf definition-alpha<}, 
which orders definitions alphabetically by {\sf definition-symbol-name},
and calls {\sf definition<} to resolve ties.
{\sf Definition<} sorts definitions with identical
{\sf definition-symbol-name}s by type,
putting type definitions before all others,
generic functions before their methods,
and methods roughly in order of invocation ---
first around, then before, then primary, and finally after methods ---
with around and before methods ordered most specific first
and with primary and after methods ordered most specific last.

\subsection{Printing}
\label{Printing}

{\sf Print-definition} produces Latex for a reference manual entry for
the definition.
{\sf Print-definitions} produces reference manual entries
for all the definitions in a list.

The print functions are designed to produce Latex output.
They expect the code document style, 
originally due to Olin Shivers of CMU,
in \~jam/az/definition/doc/code.sty.
{\sf Fix-latex-string} is used ubiquitously to insert escape
characters so that characters that are special to Latex (eg. \verb.\.)
print more-or-less literally.

{\sf Print-definition-headline} produces a boxed, eye-catching
headline intended to highlight the start of an entry in a reference manual.

{\sf Print-definition-documentation} prints the documentation string subentry.
Because Latex special characters are escaped,
Latex formatting instructions cannot be included
in documentation strings. 
This may change in the future, most likely by the addition of a keyword
to indicate whether Latex special characters should be escaped
or passed through raw.
 
{\sf Definition-usage} returns a string 
that should be the printed representation of an example of how
to ``call'' the definition. 
Methods for {\sf definition-usage} should be careful to use
the actual argument names, to be consistent with the arg types 
and returns subentries.
{\sf Print-definition-usage} prints the usage subentry.

{\sf Print-definition-arg-types} prints an itemized list
associating argument names with their expected types.
{\sf Print-definition-returns} prints an itemized list of 
return value names and/or types.

{\sf Print-definition-parents} and 
{\sf print-definition-children} print the names of the definition's parents
and children, respectively.

{\sf print-definition-source-path} 
prints the pathname of the file from which the definition was read.
