\section{Extending the basic system}

There are two basic ways to extend the ways in which
the Definitions module can generate manual entries:
(1) new functions can be added to the Definition protocol
(2) the existing protocol can be implemented for a new Definition class
(corresponding to a user defined definition macro).

\subsection{Additions to the protocol}

\subsubsection{Expected Additions}

It is expected that users will define new protocol functions
to customize filtering, sorting, and formatting behavior,
using the existing parsing functions.
It is expected that users will occasionally define new parsing functions.
It is not expected that users will define new reading and building functions.

\subsubsection{Censoring}

A good system for producing manuals would aid encapsulation
by censoring output to remove private information.

For example, a standard style in Lisp programming
is to offer a functional interface to an abstract type 
and suppress all details of how the abstract type is implemented 
(see for example, Keene \cite{Keen88} and McDonald \cite{McDo91g}).
In this situation,
one would like a documentation formatting system to automatically hide
the existence of methods,
the existence of slots,
whether a given function is generic or not,
whether a given type is implemented 
as a class or a structure or some other Lisp type,
and so on.

The present Definitions module does very little censoring of this type.
It does not report slots, 
but otherwise displays too much information about classes vs. structures,
class inheritance, generic vs. ordinary functions, method combination,
etc., to be very useful for documenting a functional interface.

I expect that one of the first improvements to the Definitions module
will be the addition of protocol for censoring.

\subsubsection{Grouping related definitions}

The current Definitions module treats each Definition object
as an independent entity.
This means, for example, that a reference manual will contain
separate, and somewhat redundant, entries for a generic function
and all its methods.
This is also the reason
that the actual definitions must be loaded before
the Definition objects can be created and printed;
in order to compute some relationships between Definition objects
it is necessary to refer to actual defined objects.
For example, we cannot determine a class's direct subclasses
by examining the {\sf defclass} form, but must query the class object itself.

I expect that the Definitions module will be extended with functions
that operate on collections of Definition objects 
to extract information about relationship between Definition objects
and that Definition objects will be extended with state that allows them
to keep track of other, related definitions of various kinds.

\subsection{New definitions}

Adding support for a new definition macro requires
defining a new Definition class and then making sure
that the Definition protocol is implemented for that
class, either by inheritance or by writing new methods where
appropriate.

\subsubsection{Conventions}

To make it easy to extend the Definitions module,
new definition macros should follow certain conventions.
Basically, whenever possible, a new definition macro
should model its syntax on the most similar existing definition,
and, where there is more than one possible model,
it's best to use the more modern choice (eg. use {\sf defclass}
as a model rather than {\sf defstruct}).

The name of a definition macro should begin with {\sf def}
or {\sf define-}, as in, for example, {\sf defclass}.

Evaluating the definition should create an identifiable object.
For example, evaluating a {\sf defclass} creates an class object.

The defined object should have a name which preferably is a symbol,
but in special circumstances might be a list or some other lisp object.
It's often a good idea to restrict the names to be keywords, 
unless it will be natural to associate the defined objects
with different packages.
For example, it's natural to organize class names by putting them
in different packages, but the package names themselves are,
for all practical purposes, keywords.

There should be a function, like {\sf find-class}
which returns the defined object, given the name.
Generally speaking, it's best if the mapping from names
to defined objects is represented through some global table.
An acceptable alternative is to keep the defined object
on the property list of the name, 
if the name is restricted to be a symbol.
The principal advantage of one global table 
is that it is easier to find and operate on
all the objects created by a given definition macro.
A common alternative --- to be avoided --- is to represent the mapping
by making the defined object the {\sf symbol-value} of the name.
This is non-robust, 
because the global binding of a symbol is easy to change accidentally 
and even easier to inadvertently shadow by local bindings.

The definition macro should permit a documentation string to be supplied.

Definitions that include variables, 
either as function arguments
or as slots or instance variables,
should allow declarations for the types of those variables,
and, if possible, allow documentation strings to be associated
with each variable.








