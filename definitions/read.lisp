;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Definitions -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software eparation of derivative works
;;; based upon this software are permitted.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Definitions)

;;;=======================================================
;;; reading
;;;=======================================================

(defun read-definitions-from-file (path)

  "Read all the forms in the file corresponding to <path>, and create
and return a list of definition objects corresponding to the result of
evaluating those forms (for which <definer-class> does not return
<nil>)."

  (declare (type (or String Pathname) path)
	   (:returns (type List)))

  (setf path (merge-pathnames (merge-pathnames (pathname path))
			      (pathname "foo.lisp")))
  (let ((definitions ())
	(eof (gensym))
	(form ())
	(old-package *package*))
    #-sbcl (declare (special *package*)) ; $$$$ Causes package lock error -jm
    (unwind-protect
	(with-open-file
	 (s path :direction :input)
	 (loop
	  (setf form (read s nil eof nil))
	  (setf definitions
		(nconc
		 (cond ((eq form eof) (return))
		       ((atom form) nil)
		       ((eq (first form) 'in-package)
			(setf *package* (find-package (second form)))
			nil)
		       (t (make-definitions form path)))
		 definitions))))
      (setf *package* old-package))
    definitions))

(defun read-definitions-from-files (paths)

  "Call <read-definitions-from-file> on each path in <paths>,
concatenating together the results."

  (declare (type List paths)
	   (:returns (type List)))

  (mapcan #'read-definitions-from-file paths))

