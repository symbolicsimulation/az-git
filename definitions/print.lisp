;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Definitions -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software eparation of derivative works
;;; based upon this software are permitted.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Definitions)

;;;=======================================================
;;; Nice definition names

(defgeneric definition-class-nice-name (def)
  #-sbcl (declare (type Definition def)
		  (:returns (type String)))
	    (:documentation
	     "Returns a short string for the type of the definition,
eg. ``Class'' for <Class-Definition>."))
 
(defmethod definition-class-nice-name ((def Class-Definition))
  (declare (:returns "Class"))
  "Class")
(defmethod definition-class-nice-name ((def Constant-Definition))
  (declare (:returns "Constant"))
  "Constant")
(defmethod definition-class-nice-name ((def Function-Definition))
  (declare (:returns "Function"))
  "Function")
(defmethod definition-class-nice-name ((def Generic-Function-Definition))
  (declare (:returns ))
  "Generic Function")
(defmethod definition-class-nice-name ((def Macro-Definition))
  (declare (:returns "Macro"))
  "Macro")
(defmethod definition-class-nice-name ((def Method-Definition))
  (declare (:returns (type String)))
  (format nil "~:(~a~) Method"
	  (symbol-name (definition-method-qualifier def))))
(defmethod definition-class-nice-name ((def Package-Definition))
  (declare (:returns "Package"))
  "Package")
(defmethod definition-class-nice-name ((def Parameter-Definition))
  (declare (:returns "Parameter"))
  "Parameter")
(defmethod definition-class-nice-name ((def Setf-Definition))
  (declare (:returns "Setf"))
  "Setf")
(defmethod definition-class-nice-name ((def Structure-Definition))
  (declare (:returns "Structure"))
  "Structure")
(defmethod definition-class-nice-name ((def Type-Definition))
  (declare (:returns "Type"))
  "Type")
(defmethod definition-class-nice-name ((def Variable-Definition))
  (declare (:returns "Variable"))
  "Variable")

;;;-------------------------------------------------------

(defmethod print-object ((def Definition) stream)
  "A generic method for printing Definition objects."
  (declare (type Stream stream)
	   (:returns def))
  (format stream "#<Def: ~a ~s>"
	  (definition-class-nice-name def)
	  (definition-name def))
  def)

;;;=======================================================

(defun fix-latex-string (s0)
  
  "Massage a string so that TeX special characters will come out
as something reasonable.
See \cite{Lamp86} pp. 15, 65."

  (declare (type String s0)
	   (:returns (type String)))
  (with-output-to-string (out)
    (dotimes (i (length s0))
      (let ((c (char s0 i)))
	(case c
	  ((#\# #\$ #\% #\& #\_ #\{ #\})
	   ;; all these need is a \
	   (write-char #\\ out)
	   (write-char c out))
	  ((#\~ #\^ #\\)
	   ;; these need to be in verbatim environment
	   (format out "\\verb.~c." c)
	   )
	  ((#\< #\> #\|)
	   ;; force these into a \tt font,
	   ;; where they print as the should
	   (format out "{\\tt~c}" c))
	  (otherwise
	   (write-char c out)))))))

;;;=======================================================

(defgeneric print-definition-headline (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print a Headline for the definition."))

(defmethod print-definition-headline ((def Definition) 
				      &key
				      (stream *standard-output*))

  "Print a Headline for the definition."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(name-string (fix-latex-string (definition-name->string def))))

    ;; name box
    (format stream "~%~%\\noindent")
    (format stream "\\framebox[6in]{~a" name-string)
    (format stream "\\hfill {\\sf ~a}}" (definition-class-nice-name def))
    (format stream "~%\\markboth{~a}{~a}" name-string name-string)
    def))

;;;=======================================================

(defgeneric print-definition-usage (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print a description of how to ``call'' the definition."))

(defmethod print-definition-usage ((def Definition) 
				      &key
				      (stream *standard-output*))

  "Print a description of how to ``call'' the definition."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t))
    (format stream "~%~%\\noindent{\\bf Usage:} {\\sf ~a}"
	    (fix-latex-string (definition-usage def)))
    (format stream "~%\\vskip .5em"))
  def)

;;;=======================================================

(defgeneric print-definition-arg-types (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print a list of args and their expected types."))

(defmethod print-definition-arg-types ((def Definition) 
					  &key
					  (stream *standard-output*))

  "Print a list of args and their expected types."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(arg-types (definition-arg-types def)))
    (when (> (length arg-types) 1)
      (format stream "~%~%\\noindent{\\bf Arguments:}")
      (format stream "~%\\begin{tightcode}{\\sf")
      (dolist (arg-type arg-types)
	(format stream "~%~a --- ~:(~s~)"
		(first arg-type)
		(second arg-type)))
      (format stream "}\\end{tightcode}"))
    (when (= (length arg-types) 1) ;; print it all on one line
      (format stream "~%~%\\noindent{\\bf Arguments:}")
      (format stream "\\cd{\\sf ~a --- ~:(~s~)}"
	      (first (first arg-types))
	      (second (first arg-types))))
    (format stream "~%\\vskip .5em"))
  def)

;;;=======================================================

(defgeneric print-definition-returns (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print a list of returned values and/or their types."))

(defun format-return-spec (return-spec stream)
  (cond ((atom return-spec)
	 (format stream "\\cd{\\sf ~a}" return-spec))
	((eq (first return-spec) 'type)
	 (ecase (length return-spec)
	   (2 (format stream "\\cd{\\sf ~:(~s~)}" (second return-spec)))
	   (3 (format stream "\\cd{\\sf ~a --- ~:(~s~)}"
		      (third return-spec) (second return-spec)))))
	(t (error "Invalid :returns spec"))))

(defmethod print-definition-returns ((def Definition) 
					&key
					(stream *standard-output*))

  "Print a list of returned values and/or their types."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(returns (definition-returns def)))
    (when returns
      (format stream "~%~%\\noindent{\\bf Returns:}")
      (cond ((and (listp (second returns))
		  (eq (first (second returns)) 'values))
	     (format stream "~%\\begin{tightcode}{\\rm")
	     (if (rest (second returns))
	       (dolist (return-spec (rest (second returns)))
		 (format stream "~%")
		 (format-return-spec return-spec stream))
	       ;; else
	       (format stream " no values."))
	     (format stream "}\\end{tightcode}"))
	    (t ;; else a single return value
	     (format-return-spec (second returns) stream)))
      (format stream "~%\\vskip .5em")))
  def)

;;;=======================================================

(defgeneric print-definition-parents (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print a list of parent definitions."))

(defmethod print-definition-parents ((def Definition) 
					&key
					(stream *standard-output*))

  "Print a list of parent definitions."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(parents (definition-parents def)))

    (when parents
      (format stream "~%~%\\noindent{\\bf Parents:} \\cd{\\rm ~a}"
	      (fix-latex-string
	       (format nil "~{~:(~s~) ~}" parents)))
      (format stream "~%\\vskip .5em")))
  def)


;;;=======================================================

(defgeneric print-definition-children (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print a list of parent definitions."))

(defmethod print-definition-children ((def Definition) 
				       &key
				       (stream *standard-output*))

  "Print a list of parent definitions."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

    (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(children (definition-children def)))

     (when children
      (format stream "~%~%\\noindent{\\bf Children:} \\cd{\\rm ~a}"
	      (fix-latex-string
	       (format nil "~{~:(~s~) ~}" children)))
      (format stream "~%\\vskip .5em")))
  def)

;;;=======================================================

(defgeneric print-definition-documentation (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream)
		  (:returns (type Definition def)))
  (:documentation
   "Print the documentation string for the definition.
Because Latex special characters are escaped, Latex formatting
instructions cannot be included in documentation strings.  This may
change in the future, most likely by the addition of a keyword to
indicate whether Latex special characters should be escaped or passed
through raw."))

(defmethod print-definition-documentation ((def Definition) 
					   &key
					   (stream *standard-output*))

  "Print the documentation string for the definition."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(documentation (fix-latex-string (definition-documentation def))))
    (when (and documentation (not (zerop (length documentation))))
      (format stream "~%~%\\noindent{\\bf Documentation:}")
      (format stream " ~a" documentation)
      (format stream "~%\\vskip .5em")))
  def)

;;;=======================================================

(defgeneric print-definition-source-path (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print the pathname from which this definition was read."))

(defmethod print-definition-source-path ((def Definition) 
					 &key
					 (stream *standard-output*))

  "Print the pathname from which this definition was read."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(path (definition-path def)))
    (when path
      (format stream "~%~%\\noindent{\\bf Source:} ~a"
	      (fix-latex-string (format nil "~a" path)))
      (format stream "~%\\vskip .5em")))
  def)


;;;=======================================================

(defgeneric print-definition (def &key stream)
  #-sbcl (declare (type Definition def)
		  (type Stream stream))
  (:documentation
   "Print a definition object in latex."))

(defmethod print-definition ((def Definition) &key (stream *standard-output*))

  "Prints out the reference manual entry for a definition object in Latex."

  (declare (type Definition def)
	   (type Stream stream)
	   (:returns def))

  (let ((*print-case* :downcase)
	(*print-circle* nil)
	#+:excl (excl:*print-nickname* t)
	(name-string (fix-latex-string (definition-name->string def)))
	)

    (format stream "~&\\index{~a}" name-string)
    (format stream "~%\\noindent\\begin{minipage}{6in}")
    (print-definition-headline def :stream stream)
    (print-definition-documentation def :stream stream)
    (print-definition-usage def :stream stream)
    (print-definition-arg-types def :stream stream)
    (print-definition-returns def :stream stream)
    (print-definition-parents def :stream stream)
    (print-definition-children def :stream stream)
    ;; (print-definition-source-path def :stream stream)
    (format stream "~%\\end{minipage}")
    (format stream "~%\\vskip 1em")
    (format stream "~%\\markboth{~a}{~a}" name-string name-string)
    def))

;;;=======================================================

(defun print-definitions (defs path)

  "Print a latex representation of the definition objects in <defs>
on the file corresponding to <path>."

  (declare (type List defs)
	   (type (or String Pathname) path)
	   (:returns defs))

  (with-open-file (s path
		   :direction :output
		   :if-does-not-exist :create
		   :if-exists :supersede)
    (dolist (def defs) (print-definition def :stream s)))

  (values defs))
