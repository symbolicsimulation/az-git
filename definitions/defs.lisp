;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Definitions -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software eparation of derivative works
;;; based upon this software are permitted.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Definitions)

;;;=======================================================

(declaim (declaration :returns))

;;;=======================================================
;;; PCL/CLOS consistency
;;;=======================================================

#+:pcl
(eval-when (compile load eval)
  (unless (search "92" pcl::*pcl-system-date*)
    (pushnew :pcl<92 *features*)))

#+:pcl
(eval-when (compile load eval)
  (let* ((package (find-package :pcl))
	(nicknames (package-nicknames package))
	(name (package-name package)))
    (unless (member "CLOS" nicknames :test #'string-equal)
      (rename-package package name (cons "CLOS" (copy-list nicknames))))))

#+:pcl<92
(eval-when (compile load eval)
  ;; have compiled classes and methods added to the environment
  ;; at compile time:
  (pushnew 'compile pcl::*defclass-times*)
  (pushnew 'compile pcl::*defgeneric-times*)
  (pushnew 'compile pcl::*defmethod-times*)
  )

#+:pcl
(eval-when (compile load eval)
  ;; PCL exports consistent with CLTL2:
  (export '(pcl::allocate-instance
	    pcl::class-direct-subclasses
	    pcl::class-direct-superclasses
	    pcl::class-slots
	    pcl::slot-definition-name
	    pcl::slot-definition-type
	    pcl::generic-function)
	  :pcl))

#+:pcl<92
(defun pcl:slot-definition-name (slotd) (pcl::slotd-name slotd))
#+:pcl<92
(defun pcl:slot-definition-type (slotd) (pcl::slotd-type slotd))

#+:cmu17
(defmethod class-direct-subclasses ((class Standard-Class))
  "This method seems to be missing."
  (class-direct-subclasses (kernel:class-pcl-class class)))

;;;=======================================================

(defun find-documentation-option (form)
  (let ((doc (second 
	      (find-if #'(lambda (item)
			   (and (listp item)
				(eq (first item) :documentation)))
		       form))))
    (if (stringp doc) doc "")))

;;;=======================================================

(defmacro type-check (type &rest args)

  "A <check-type> that takes arguments more like declarations, eg,
(declare (type Integer x y))"

  (let* ((args (remove-duplicates args))
 	 (variables (remove-if #'constantp args))
	 (runtime-checks (mapcar #'(lambda (arg)
				     `(check-type ,arg ,type))
				 variables)))
    ;; test constants at macro expand time:
    (dolist (constant (remove-if-not #'constantp args))
      ;; need to <eval> because <constant> might be a <defconstant>
      ;; rather than a literal constant
      (assert (typep (eval constant) type)))
    (unless (null runtime-checks) `(progn ,@runtime-checks))))

;;;-------------------------------------------------------

(defmacro declare-check (&rest decls)

  "This macro generates type checking forms and has a syntax like <declare>.
Unfortunately, we can't easily have it also generate the declarations."

  `(progn
     ,@(mapcar #'(lambda (d) `(type-check ,(second d) ,@(cddr d)))
	       ;; ignore non-type declarations
	       (remove-if-not #'(lambda (d) (eq (first d) 'type))
			      decls))))

;;;=======================================================

(defun unix-shell (command)
  "Useful for calling TeX, etc., from CL."
  (declare (type String command))
  #+:excl (excl:shell command)
  #+:cmu (ext:run-program "sh" (list "-c" command)
			  :wait t :output t :input t))

(defun print-hash (table) 
  (maphash #'(lambda (k v) (print (cons k v)))
	   table))
