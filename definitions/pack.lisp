;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: :Definitions -*-
;;;
;;; Copyright 1993. John Alan McDonald. All Rights Reserved. 
;;;
;;; Use and copying of this software eparation of derivative works
;;; based upon this software are permitted.
;;; 
;;; This software is made available AS IS, and no warranty---about the
;;; software, its performance, or its conformity to any
;;; specification---is given or implied.
;;; 
;;;=======================================================

(in-package :Definitions)

;;;=======================================================

(defclass Package-Definition (Definition) ()
  (:documentation
   "A definition class for <defpackage>."))

(defmethod definition-documentation ((def Package-Definition))
  "Returns the value of the :documentation option or an empty string."
  (declare (type Package-Definition def)
	   (:returns (type String)))
  (find-documentation-option (nthcdr 2 (definition-form def))))

(defmethod definition-name->string ((def Package-Definition))
  "Package name strings should be capitalized."
  (declare (type Package-Definition def)
	   (:returns (type String)))
  (format nil "~:(~s~)" (definition-name def)))

(defmethod definition-usage ((def Package-Definition))
  "The example of package use is a call to <in-package>."
  (declare (type Package-Definition def)
	   (:returns (type String)))
  (format nil "(in-package ~a)" (definition-name->string def)))

