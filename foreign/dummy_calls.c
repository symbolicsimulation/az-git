#include <stdio.h>
#include <math.h>

/* Dummy calls to library functions,  */
/* to make sure everything is loaded. */

/*
void mkidxname (){}
void mkdatname (){}
*/

/* currently used lapack calls */
extern void dgeqrf_(int * m, int * n, 
		    double * a, int * lda,
		    double * tau,
		    double * work, int * lwork, 
		    int * info);

extern void dgeqpf_(int * m, int * n,
		    double * a, int * lda,
		    int * jpvt,
		    double * tau,
		    double * work, int * lwork,
		    int * info);

extern void dorgqr_(int * m, int * n,
		    int * k, 
		    double * a, int * lda,
		    double * tau, 
		    double * work, int * lwork, 
		    int * info);

extern void dormqr_(char * side,
		    char * ltrans,
		    int * m, int * n,
		    int * k, 
		    double * a, int * lda,
		    double * tau, 
		    double * c, int * ldc,
		    double * work, int * lwork, 
		    int * info);

extern void dtrtri_(char * uplo,
		    char * diag,
		    int * n,
		    double * a, int * lda,
		    int * info);

extern void dtrtrs_(char * uplo,
		    char * trans,
		    char * diag,
		    int * n,
		    int * nrhs,
		    double * a, int * lda,
		    double * b, int * ldb,
		    int * info);
  
void lapack_dummy_calls ()
{
  int * m; 
  int * n;
  int * k;
  double * a; 
  int * lda;
  double * tau;
  double * work; 
  int * lwork; 
  int * info;
  int * jpvt;
  char * side;
  char * ltrans;
  char * uplo;
  char * trans;
  int * nrhs;
  double * b;
  int * ldb;
  char * diag;
  double * c;
  int * ldc;

  dgeqrf_(m, n, 
	  a, lda,
	  tau,
	  work, lwork, 
	  info);

  dgeqpf_(m, n,
	  a, lda,
	  jpvt,
	  tau,
	  work, lwork,
	  info);

  dorgqr_(m, n,
	  k, 
	  a, lda,
	  tau, 
	  work, lwork, 
	  info);

  dormqr_(side,
	  ltrans,
	  m, n,
	  k, 
	  a, lda,
	  tau, 
	  c, ldc,
	  work, lwork, 
	  info);

  dtrtri_(uplo,
	  diag,
	  n,
	  a, lda,
	  info);

  dtrtrs_(uplo,
	  trans,
	  diag,
	  n,
	  nrhs,
	  a, lda,
	  b, ldb,
	  info);
}

/* make sure the C sqrt is loaded */
void sqrt_dummy (x) double x; { sqrt(x); }

