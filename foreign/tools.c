#include 	<fcntl.h>
#include 	<stdio.h>
#include	<math.h>
#include        <memory.h>

/****************************************************************/
/* Fast arithmetic                                              */
/****************************************************************/

void a_sqrt (x)
     double *x;
{ *x = sqrt(*x); }

void c_len2 (x,d)
     double *x, *d;
{ *d = sqrt(x[0]*x[0] + x[1]*x[1]);}

void c_trunc (f, i)
     double *f;
     long int *i;
{ *i = *f; }

void c_trunc2 (fx, ix)
     double fx[2];
     long int ix[2];
{ ix[0] = fx[0]; ix[1] = fx[1]; }

/****************************************************************/
/* A special calculation for drawing arrows                     */
/****************************************************************/

void arrowhead_points (p0, p1, angle, length)
     long int *p0, *p1, length;
     double angle;
{ double dx = (double) (p1[0] - p0[0]);
  double dy = (double) (p1[1] - p0[1]);
  double theta = ((0.0 == dx == dy) ? 0.0 : atan2(dy,dx));
  double alpha = M_PI - angle;
  double beta0 = theta - alpha;
  double beta1 = theta + alpha;
  double dlength = length;
  
  p0[0] = p1[0] + (long int) (dlength * cos(beta0));
  p0[1] = p1[1] + (long int) (dlength * sin(beta0));
  p1[0] = p1[0] + (long int) (dlength * cos(beta1));
  p1[1] = p1[1] + (long int) (dlength * sin(beta1)); }
  
/****************************************************************/
/* Zooming Images                                               */
/****************************************************************/

void magnify_image0 (mag, m0, n0, input, n1, output)

     long int mag, m0, n0, n1;
     unsigned char *input, *output;

{ long int i, j, k, l, n0mag, n1mag;

  for (i=0; i<m0; i++) {
    for (j=0; j<n0; j++) {
      for (k=0; k<mag; k++) {
	for (l=0; l<mag; l++) {
	  output[n1*((i*mag)+k) + (j*mag)+k] = *input; }}
      input++; } } }


void magnify_image1 (mag, m0, n0, input, n1, output)

     long int mag, m0, n0, n1;
     unsigned char *input, *output;

{ long int i, j, k, l, n0mag, n1mag;

  n1mag = n1*mag;
  n0mag = n0*mag;
  for (i=0; i<m0*n1mag; i=i+n1mag)
    { for (j=i; j<i+n0mag; j=j+mag)
	{ for (k=j; k<j+n1mag; k=k+n1)
	    { for (l=k; l<k+mag; l++)
		{ output[l] = *input; }
	      }
	    input++;
	  }
      }
}

void magnify_image (mag, height, width, n0, input, n1, output)

     long int mag, height, width, n0, n1;
     unsigned char *input, *output;

{ long int inskip, outskip, stripe_len;
  unsigned char *ip, *input_end, *line_end, *stripe_end, *step_end;

  inskip = n0 - width;
  if( mag == 1) {
    outskip = n1 - width;
    input_end = input + height*n0;
    while (input < input_end) { /* do each input scanline */
      line_end = input + width;
      while (input < line_end) { *output++ = *input++; }
      input  += inskip;
      output += outskip; } }
  else {
    outskip = n1 - width*mag;
    stripe_len = n1*mag;
    input_end = input + height*n0;
    while (input < input_end) { /* do each input scanline */
      line_end = input + width;
      stripe_end = output + stripe_len;
      while (output < stripe_end) { /* do each output scanline */
	for (ip=input; ip<line_end; ip++) { /* do each input pixel */
	  step_end = output + mag;
	  while (output < step_end) { *output++ = *ip; }}
	output += outskip; }
      input = line_end + inskip; }}}
