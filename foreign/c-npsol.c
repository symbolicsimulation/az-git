#include <stdio.h>

/*-----------------------------------------------------------
 * C globals 
 */

/* index "pointers" to registered lisp objective functions */

int CB_objfn_index, CB_confn_index; 
long (*CB_objfn_ptr) (), (*CB_confn_ptr) ();

/* pointers to data portion of tenured global arrays in lisp,
 * used as a crude substitute for being able to pass 
 * arrays back to the lisp objective function.
 */

int *CB_mode, *CB_n, *CB_nstate;
double *CB_x, *CB_objf, *CB_objgrd;

/* more pointers to data portion of tenured global arrays in lisp,
 * used to pass arrays back to the lisp nonlinear constraint function.
 */

int *CB_ncnln, *CB_nrowj, *CB_needc;
double *CB_c, *CB_cjac;

/*-----------------------------------------------------------
 * The objective function that's handed to Fortran:
 */

int objfn(mode, n, x, objf, objgrd, nstate)
int *mode;
int *n;
double *x, *objf, *objgrd;
int *nstate;
{
  int i;

  /* copy input args into global arrays */
  *CB_mode   = *mode;	
  *CB_n      = *n;	
  *CB_nstate = *nstate;
  for(i=0; i<*n; i++)
    { CB_x[i] = x[i];
      CB_objgrd[i] = objgrd[i];
    }

  /*
  printf("\nC wrapper for objective function:");
  printf("\nmode=%d, n=%d, nstate=%d", *mode, *n, *nstate);
  printf("\nx= ");
  for(i=0; i<*n; i++) { printf("%g ", x[i]); }
  fflush(stdout);
  */

  (*CB_objfn_ptr)();
  /*lisp_call(CB_objfn_index);*/

  /* copy the results into the output args from the lisp arrays */
  *mode = *CB_mode;	
  *objf = *CB_objf;
  for(i=0; i<*n; i++) { objgrd[i] = CB_objgrd[i]; }

  /*
  printf("\nAfter calling lisp:");
  printf("\nmode=%d, objf=%g.", *mode, *objf);
  printf("\nobjgrd= ");
  for(i=0; i<*n; i++) { printf("%g ", objgrd[i]); }
  fflush(stdout);
  */

  return 0;
}

/*-----------------------------------------------------------
 * The constraint function that's handed to Fortran:
 */

int confn(mode, ncnln, n, nrowj, needc, x, c, cjac, nstate)

int *mode;
int *ncnln, *n, *nrowj, *needc;
double *x, *c, *cjac;
int *nstate;
{
  int i,j,k;

  /* copy input args into global arrays */
  *CB_mode   = *mode;	
  *CB_ncnln  = *ncnln;	
  *CB_n      = *n;	
  *CB_nrowj  = *nrowj;	
  *CB_nstate = *nstate;
  for(i=0; i<*ncnln; i++) { CB_needc[i] = needc[i]; }
  for(i=0; i<*n; i++) { CB_x[i] = x[i]; }

  /*
    printf("\nC wrapper for constraint function:");
    printf("\nmode=%d, ncnln=%d, n=%d, nrowj=%d, nstate=%d",
    *mode, *ncnln, *n, *nrowj, *nstate);
    printf("\nx= ");
    for(i=0; i<*n; i++) { printf("%g ", x[i]); }
    printf("\nneedc= ");
    for(i=0; i<*ncnln; i++) { printf("%d ", needc[i]); }
    printf("\nCB_confn_index=%d",CB_confn_index);
    printf("\n");
    fflush(stdout);
    */

  (*CB_confn_ptr) ();
  /*lisp_call(CB_confn_index);
    
    printf("\nAfter calling lisp:");
    fflush(stdout);
    */

  /* copy the results into the output args from the lisp arrays */
  *mode = *CB_mode;	
  for(i=0; i<*ncnln; i++) { c[i] = CB_c[i]; }
  for(i=0; i<*n; i++) { 
    k = i*(*nrowj);
    for(j=0;j<*ncnln;j++) { cjac[k] = CB_cjac[k]; k++; }
  }

  /*
    printf("\nAfter calling lisp:");
    printf("\nmode=%d.", *mode);
    printf("\nc= ");
    for(i=0; i<*ncnln; i++) { printf("%g ", c[i]); }
    printf("\ncjac= ");
    for(i=0; i<*n; i++) {
    k = i*(*nrowj);
    printf("\n");
    for(j=0;j<*ncnln;j++) { printf("%g ", cjac[k]); k++; }
    }
    fflush(stdout);
    */

  return 0;
}

/*-----------------------------------------------------------
 * The C wrapper for the Fortran NPSOL. The C wrapper is called
 * from Lisp, sets things up for the call back to a lisp objective

 * function, and then calls NPSOL.
 */

void c_npsol(n, nclin, ncnln, nrowa, nrowj, nrowr,
	     a, bl, bu,
	     inform, iter, istate,
	     c, cjac, clamda, objf, objgrd, r, x,
	     iw, leniw, w, lenw,
	     confn_index, objfn_index,
	     confn_ptr, objfn_ptr,
	     cb_mode, cb_n,
	     cb_x, cb_objf, cb_objgrd,  cb_nstate,
	     cb_ncnln, cb_nrowj,
	     cb_needc, cb_c, cb_cjac)
/* actual arguments passed to NPSOL */
int *n, *nclin, *ncnln, *nrowa, *nrowj, *nrowr;
double *a, *bl, *bu;
int *inform, *iter, *istate;
double *c, *cjac, *clamda, *objf, *objgrd, *r, *x;
int *iw, *leniw;
double *w;
int *lenw;
/* indexes of registered lisp functions to call to compute objective
 * value and constraints
 */
int confn_index, objfn_index;
long (*objfn_ptr) (), (*confn_ptr) ();

/* pointers to arrays used to pass data back to lisp */
int *cb_mode, *cb_n, *cb_nstate, *cb_ncnln, *cb_nrowj, *cb_needc;
double *cb_x, *cb_objf, *cb_objgrd, *cb_c, *cb_cjac;
{
  extern  npsol_();
  extern int objfn(), confn();
  int i;

  /*
    printf("\nC_NPSOL: ");
    printf("n=%d, x= ", *n);
    for(i=0; i<*n; i++) { printf("%g ", x[i]); }
    printf("\nobjfn=%d, confn=%d", objfn_index, confn_index);
    */

  /* copy info for call back to lisp into global variables: */
  CB_objfn_index = objfn_index;
  CB_confn_index = confn_index;
  CB_objfn_ptr = objfn_ptr;
  CB_confn_ptr = confn_ptr;
  CB_mode   = cb_mode;
  CB_n      = cb_n;
  CB_nstate = cb_nstate;
  CB_x      = cb_x;
  CB_objf   = cb_objf;
  CB_objgrd = cb_objgrd;
  CB_ncnln  = cb_ncnln;
  CB_nrowj  = cb_nrowj;
  CB_needc  = cb_needc;
  CB_c      = cb_c;
  CB_cjac   = cb_cjac;

  npsol_(n, nclin, ncnln, nrowa, nrowj, nrowr,
	 a, bl, bu,
	 confn, objfn, inform, iter, istate,
	 c, cjac, clamda, objf, objgrd, r, x,
	 iw, leniw, w, lenw);

}



