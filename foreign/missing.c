/* routines that are missing from libraries, but seem unused */

/* CMUCL, Sparcstation, Sunos */
void d_sign () {}
void c_conv_i () {}
void i_conv_c () {}

/* Excl, Sparcstation */
void __class_quadruple() {} 
void __nox_double_to_decimal() {}
void __nox_single_to_decimal() {}
void __nox_quadruple_to_decimal() {}

/* Excl, Decstation 5000, Ultrix, F77 */
void pow_di () {}
void catopen () {}
void catgets () {}
void catclose () {}
